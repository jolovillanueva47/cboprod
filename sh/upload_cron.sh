#!/bin/sh

echo "upload_cron.sh executing"

UPLOADS_DIR="/app/ebooks/uploads/"
CONTENT_DIR="/app/ebooks/content/"
ARCHIVE_DIR="/app/ebooks/archive/"

ITERATE_SCRIPT="/app/jboss-5.1.0.GA/bin/iterate_cron.sh"
JBOSS_HOME="/app/jboss-5.1.0.GA"


UPLOADED_FILE_LOG="/app/service/content-cleaner/isbnlist.txt"
UPLOADED_FILE_LOG1="/app/ebooks/solr/production/indexer/bin/isbnlist.txt"
UPLOADED_FILE_LOG2="/app/service/xslt-converter/isbnlist.txt"
UPLOADED_FILE_LOG3="/app/ebooks/solr/production/indexer/bin/isbnlist-3.4.txt"

LOCK=/app/ebooks/lockfile

echo "upload_cron.sh checking for lockfile"

if [ -f $LOCK ]; then
  chmod 777 $LOCK
  exit
fi
touch $LOCK

echo "upload_cron.sh creating isbn.txts"

find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG
find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG1
find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG2
find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG3


echo "upload_cron.sh executing iterate script"

find "$UPLOADS_DIR"*.zip -exec /usr/bin/php /app/service/php/content_extractor.php {} \; \
-exec mv {} "$ARCHIVE_DIR" \; \
-exec $ITERATE_SCRIPT {} \; \
-exec java -Dcontent.dir="$CONTENT_DIR" \
-Duploads.dir="$UPLOADS_DIR" \
-classpath "$JBOSS_HOME"/server/production/deploy/application.ear/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar \
org.cambridge.ebooks.production.load.ValidatorMessage {} \;

cd /app/service/content-cleaner
./content-cleaner.sh

cd /app/service/xslt-converter
./xsltConvert.sh

cd /app/ebooks/solr/production/indexer/bin/
./solr-cbo-production-indexer-3.4.sh

echo "upload_cron.sh done, check individual logs for further detailes"

