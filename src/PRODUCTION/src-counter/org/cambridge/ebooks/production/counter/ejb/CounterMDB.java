package org.cambridge.ebooks.production.counter.ejb;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBContext;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.counter.ejb.jpa.CounterBean;
import org.cambridge.ebooks.production.counter.util.DateUtil;

@MessageDriven(name = "CounterMDB", activationConfig = {		
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/counter/logger") }
		)	
@TransactionManagement(TransactionManagementType.BEAN)	
/**
 * @author mmanalo
 */
public class CounterMDB implements MessageListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1852339708617688013L;

	public static final String BR1 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport1";
	
	public static final String BR2 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport2";
	
	public static final String BR3 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport3";
	
	public static final String BR4 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport4";
	
	public static final String BR5 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport5";
	
	public static final String BR6 = "org.cambridge.ebooks.production.counter.ejb.jpa.BookReport6";

	public static final String TIMESTAMP_BEAN = "timestamp";
	
	public static final String PERSISTENCE_NAME = "CounterReportService";
	
	@SuppressWarnings("unused")
	@PersistenceUnit(unitName = PERSISTENCE_NAME )
	private EntityManagerFactory emf;
	
	@PersistenceContext
	private EntityManager em;	

	@Resource
	private MessageDrivenContext mdc;
	
    @SuppressWarnings("unused")
	@Resource
    private SessionContext sc;
    
    @Resource
    private EJBContext context;
	
	/**
	 * implemented method
	 */
	public void onMessage(Message recvMsg) {			
		Message payload = (Message) recvMsg;
		UserTransaction ut = context.getUserTransaction();		
		try {			
			ut.begin();			
			
			//recreate the message object
			CounterBean cb = createCounterBean(payload);			
			
			//logs the record (regards to business logic)
			logRecord(cb, ut);				
		} catch (NumberFormatException e) {			
			e.printStackTrace();
		} catch (JMSException e) {			
			e.printStackTrace();
			mdc.setRollbackOnly();
		} catch (ParseException e) {			
			e.printStackTrace();
		} catch (Exception e) {			
			e.printStackTrace();			
		} finally {
			
		}					
	}	

	/**
	 * recreates the message into an object
	 * @param payload
	 * @return
	 * @throws NumberFormatException
	 * @throws JMSException
	 * @throws ParseException
	 */
	private CounterBean createCounterBean(Message payload) throws NumberFormatException, JMSException, ParseException{
		CounterBean cb = new CounterBean();
		cb.setLogType(Integer.parseInt(payload
				.getStringProperty(CounterBean.LOG_TYPE)));
		cb.setService(payload.getStringProperty(CounterBean.SERVICE));
		cb.setIsbn(payload.getStringProperty(CounterBean.ISBN));
		cb.setIssn(payload.getStringProperty(CounterBean.ISSN));
		cb.setTitle(payload.getStringProperty(CounterBean.TITLE));
		cb.setPublisher(payload.getStringProperty(CounterBean.PUBLISHER));
		cb.setPlatform(payload.getStringProperty(CounterBean.PLATFORM));
		cb.setSessionId(payload.getStringProperty(CounterBean.SESSION_ID));
		cb.setComponentId(payload.getStringProperty(CounterBean.COMPONENT_ID));
		cb.setIsSearch(payload.getStringProperty(CounterBean.IS_SEARCH));
		cb.setTimestamp(DateUtil.convStrToTs(payload
				.getStringProperty(CounterBean.TIMESTAMP)));
		cb.setBodyId(Integer.parseInt(payload.getStringProperty(CounterBean.BODY_ID)));
		System.out.println("payload ts = " + payload.getStringProperty(CounterBean.TIMESTAMP));
		
		return cb;
	}
	
	/**
	 * retrives the class path of the type of report
	 * @param cb
	 * @return
	 */
	private String getClassPath(CounterBean cb) {
		int logType = cb.getLogType();
		String classPath;
		switch(logType) {
			case 1: classPath = BR1; break;
			case 2: classPath = BR2; break;
			case 3: classPath = BR3; break;
			case 4: classPath = BR4; break;
			case 5: classPath = BR5; break;
			case 6: classPath = BR6; break;
			default: classPath = null;
		}		
		return classPath;
	}
		
	/**
	 * business logic for logging the record
	 * @param cb
	 * @throws Exception
	 */
	private void logRecord(CounterBean cb, UserTransaction ut) throws Exception{		
		try{			
			//System.out.println("logId = " + cb.getLogId());
			//System.out.println("timestamp = " + cb.getTimestamp());
			BigDecimal logId = (BigDecimal) findMaxRecord(cb);
			//System.out.println("findMaxlogId = " + logId);
			if(logId == null){					
				insertNewRecord(cb);
			}else{
				Object bookReport = em.find(newInstance(cb).getClass(), logId.intValue());
				String tsStr = BeanUtils.getProperty(bookReport, TIMESTAMP_BEAN);
				Timestamp ts = DateUtil.convStrToTs(tsStr);	
				//System.out.println("ts = " + ts);
				//System.out.println("timestamp check");
				//System.out.println(Math.abs((ts.getTime() - cb.getTimestamp().getTime())/1000));
				if(Math.abs((ts.getTime() - cb.getTimestamp().getTime())/1000) <= cb.getCheckTime() ){
					BeanUtils.setProperty(bookReport, TIMESTAMP_BEAN, cb.getTimestamp());
					em.merge(bookReport);
					//System.out.println("MERGED");
				}else{
					insertNewRecord(cb);
				}
			}	
			ut.commit();
		}catch(Exception e){
			e.printStackTrace();
			ut.rollback();			
		}
	}
	
	/**
	 * inserts a new record into the DB
	 * @param cb
	 * @throws Exception
	 */
	private void insertNewRecord(CounterBean cb)throws Exception {		
		Object bean = newInstance(cb);
		BeanUtils.copyProperties(bean, cb);		
		em.persist(bean);
	}
	
	/**
	 * Create a new instance
	 * @param cb
	 * @return
	 * @throws Exception
	 */
	private Object newInstance(CounterBean cb) throws Exception {
		return Class.forName(getClassPath(cb)).newInstance();
	}
	
	/**
	 * finds the record to update if exists
	 * @param cb
	 * @return
	 */
	private Object findMaxRecord(CounterBean cb){
		int logType = cb.getLogType();
		Query query = null;
		
		//tables 5 and 6 have a distinct way to check max records
		//is search is required to find the proper max record
		if(logType == 5 || logType == 6){
			query = em.createNativeQuery(
					"select LOG_ID " +
					"from BOOK_REPORT" + cb.getLogType() + " a, " +
					    "(select max(TIMESTAMP) TIMESTAMP " + 
					    "from BOOK_REPORT" + cb.getLogType() + " " +
					    "where SESSION_ID = ?1  " +
					      "and IS_SEARCH = ?2 ) b " +
					"where a.TIMESTAMP = b.TIMESTAMP " +
					"and SESSION_ID = ?1 " 
					);
			query.setParameter(1, cb.getSessionId());
			query.setParameter(2, cb.getIsSearch());
		}else{
			query = em.createNativeQuery(
					"select LOG_ID " +
					"from BOOK_REPORT" + cb.getLogType() + " a, " +
					    "(select max(TIMESTAMP) TIMESTAMP " + 
					    "from BOOK_REPORT" + cb.getLogType() + " " +
					    "where SESSION_ID = ?1 ) b " +
					"where a.TIMESTAMP = b.TIMESTAMP " +
					"and SESSION_ID = ?1 " 
					);
			query.setParameter(1, cb.getSessionId());
		}
		try {
			return query.getSingleResult();
		} catch (javax.persistence.NoResultException e) {			
			//e.printStackTrace();
		}
		return null;
	}

	@PostConstruct
    void init() {
		Logger.getLogger( CounterMDB.class ).info( " Initialize EJB " );		
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger(  CounterMDB.class ).info( " Destroy EJB " );
    	em.close();
    }
}