#!/bin/sh
# Appserver
#INDEXER_PROPERTIES="/app/ebooks/indexer/properties/indexer.properties"
#JBOSS_HOME="/app/jboss019-4.2.3.GA.JL"
#HOST_IP="192.168.7.14"

INDEXER_PROPERTIES="/app/ebooks/indexer/properties/indexer.properties"
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.224"

NOW=`date '+top.books.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"

java -Dprovider.url="$HOST_IP":1099 \
-Dindexer.properties="$INDEXER_PROPERTIES" \
-classpath "$JBOSS_HOME"/server/online/deploy/ebooks.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar:"$JBOSS_HOME"/server/default/lib/ojdbc14.jar:"$JBOSS_HOME"/server/default/lib/lucene-core-2.4.0.jar:"$JBOSS_HOME"/server/online/lib/lucene-highlighter-2.4.0.jar:"$JBOSS_HOME"/server/online/lib/cuplib.jar:"$JBOSS_HOME"/server/online/lib/log4j.jar:"$JBOSS_HOME"/server/default/lib/mail.jar \
org.cambridge.ebooks.online.topbooks.TopBooks \; >> $LOGFILE
