#!/bin/sh
CONTENT_DIR="/app/ebooks/content/"
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.178"

NOW=`date '+ebooks.index.log.%Y-%e-%m'`
LOGFILE="/app/ebooks/log/rerun_asset_$NOW.txt"

java -Dcontent.dir="$CONTENT_DIR" \
-Dprovider.url="$HOST_IP":1099 \
-classpath "$JBOSS_HOME"/server/default/deploy/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar:"$JBOSS_HOME"/server/default/lib/ojdbc14.jar:"$JBOSS_HOME"/server/default/lib/cuplib.jar \
org.cambridge.ebooks.production.util.AssetMain \; >> $LOGFILE