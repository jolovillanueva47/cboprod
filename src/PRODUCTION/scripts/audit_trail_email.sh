#!/bin/sh
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.224"

NOW=`date '+audit.trail.email.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"

java -classpath /app/jboss-4.2.3.GA/server/online/deploy/ebooks.war/WEB-INF/classes/:/app/jboss-4.2.3.GA/server/default/deploy/production.war/WEB-INF/classes/:/app/jboss-4.2.3.GA/client/jbossall-client.jar:/app/jboss-4.2.3.GA/server/default/lib/ojdbc14.jar:/app/jboss-4.2.3.GA/server/default/lib/freemarker.jar org.cambridge.ebooks.production.email.AuditTrailEmail \; >> $LOGFILE