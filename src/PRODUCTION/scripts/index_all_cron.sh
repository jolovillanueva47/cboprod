#!/bin/sh

NOW=`date '+ebooks.all.index.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"


/usr/bin/java -Xms128m -Xmx512m -Djavax.xml.parsers.DocumentBuilderFactory=net.sf.saxon.dom.DocumentBuilderFactoryImpl -Djavax.xml.parsers.SAXParserFactory=net.sf.saxon.aelfred.SAXParserFactoryImpl -Djavax.xml.xpath.XPathFactory=net.sf.saxon.xpath.XPathFactoryImpl -cp /app/ebooks/lib/cuplib.jar:/app/ebooks/lib/saxon-aelfred.jar:/app/ebooks/lib/saxon9-xpath.jar:/app/ebooks/lib/saxon9-dom.jar:/app/ebooks/lib/saxon9.jar:/app/ebooks/lib/lucene-core-2.4.0.jar:/app/ebooks/references/classes:/app/ebooks/lib/log4j.jar:/app/ebooks/lib/freemarker.jar:/app/ebooks/lib/HTTPClient.jar:/app/ebooks/indexer org.cambridge.ebooks.indexer.index.Main all
