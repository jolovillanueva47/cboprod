#!/bin/sh

echo "upload_cron.sh executing"

UPLOADS_DIR="/app/ebooks/uploads/"
CONTENT_DIR="/app/ebooks/content/"
ARCHIVE_DIR="/app/ebooks/archive/"

ITERATE_SCRIPT="/app/jboss-5.1.0.GA/bin/iterate_cron.sh"
JBOSS_HOME="/app/jboss-5.1.0.GA"

NOW=`date '+ebooks.index.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"
UPLOADED_FILE_LOG="/app/service/content-cleaner/isbnlist.txt"
UPLOADED_FILE_LOG2="/app/service/keywords-to-db/isbnlist.txt"
UPLOADED_FILE_LOG3="/app/ebooks/solr/production/indexer/bin/isbnlist.txt"

LOCK=/app/ebooks/lockfile

echo "upload_cron.sh checking for lockfile"

if [ -f $LOCK ]; then
  chmod 777 $LOCK
  exit
fi
touch $LOCK

echo "upload_cron.sh creating isbn.txts"

find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG
find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG2
find "$UPLOADS_DIR"*.zip >> $UPLOADED_FILE_LOG3

#execute php for extracting and moving zip
#/app/php/bin/php /app/apache2/cgi-bin/php/isbn13_parser.php

#-exec unzip -o -d "$CONTENT_DIR" {} \; \
#-exec mv {} "$ARCHIVE_DIR" \; \

echo "upload_cron.sh executing iterate script"

find "$UPLOADS_DIR"*.zip \
-exec /app/php/bin/php /app/apache2/cgi-bin/php/content_extractor.php {} \; \
-exec mv {} "$ARCHIVE_DIR" \; \
-exec $ITERATE_SCRIPT {} \; \
-exec java -Dcontent.dir="$CONTENT_DIR" \
-Duploads.dir="$UPLOADS_DIR" \
-classpath "$JBOSS_HOME"/server/production/deploy/application.ear/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar \
org.cambridge.ebooks.production.load.ValidatorMessage {} \; >> $LOGFILE

echo "upload_cron.sh executing indexer"

cd /app/ebooks/solr/production/indexer/bin/
./solr-cbo-production-indexer.sh

#cd /app/service/keywords-to-db
#./keywordsToDb.sh

cd /app/service/content-cleaner
./content-cleaner.sh

echo "upload_cron.sh done, check individual logs for further detailes"

#/usr/bin/java -Xms128m -Xmx512m -Djavax.xml.parsers.DocumentBuilderFactory=net.sf.saxon.dom.DocumentBuilderFactoryImpl -Djavax.xml.parsers.SAXParserFactory=net.sf.saxon.aelfred.SAXParserFactoryImpl -Djavax.xml.xpath.XPathFactory=net.sf.saxon.xpath.XPathFactoryImpl -cp /app/ebooks/lib/cuplib.jar:/app/ebooks/lib/saxon-aelfred.jar:/app/ebooks/lib/saxon9-xpath.jar:/app/ebooks/lib/saxon9-dom.jar:/app/ebooks/lib/saxon9.jar:/app/ebooks/lib/lucene-core-2.4.0.jar:/app/ebooks/references/classes:/app/ebooks/lib/log4j.jar:/app/ebooks/lib/freemarker.jar:/app/ebooks/lib/HTTPClient.jar:/app/ebooks/indexer org.cambridge.ebooks.indexer.index.Main >> $LOGFILE