#!/bin/sh
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.178"

NOW=`date '+batch.indexer.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"

nohup java -Xms128m -Xmx512m \
-Dindex.dir=/app/ebooks/bookindex/ \
-Dcontent.dir=/app/ebooks/content/ \
-Dcontent.url=../content/ \
-Dmissing.cover.standard=images/default_cover/missing_image.jpg \
-Dmissing.cover.thumbnail=images/default_cover/missing_image_small.jpg \
-Dno.cover.standard=images/default_cover/no_image.jpg \
-Dno.cover.thumbnail=images/default_cover/no_image_small.jpg \
-Dcorrupt.cover.standard=images/default_cover/corrupted_image.jpg \
-Dcorrupt.cover.thumbnail=images/default_cover/corrupted_image_small.jpg \
-classpath "$JBOSS_HOME"/server/default/deploy/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar:"$JBOSS_HOME"/server/default/lib/ojdbc14.jar:"$JBOSS_HOME"/server/default/lib/lucene-core-2.4.0.jar:"$JBOSS_HOME"/server/default/deploy/search-ejb.jar:"$JBOSS_HOME"/server/default/lib/cuplib.jar:"$JBOSS_HOME"/server/default/lib/jsf-impl.jar:"$JBOSS_HOME"/server/default/lib/servlet-api.jar:"$JBOSS_HOME"/server/default/lib/jsf-api.jar:"$JBOSS_HOME"/server/default/lib/log4j.jar org.cambridge.ebooks.production.online.indexer.OnlineIndexer \; >> $LOGFILE &