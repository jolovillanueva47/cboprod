#!/bin/sh
INDEXER_PROPERTIES="/app/ebooks/indexer/properties/indexer.properties"
CONTENT_DIR="/app/ebooks/content/"
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.179"

NOW=`date '+ebooks.index.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/Embargo_$NOW.txt"

java -Dcontent.dir="$CONTENT_DIR" \
-Dprovider.url="$HOST_IP":1099 \
-Dindexer.properties="$INDEXER_PROPERTIES" \
-classpath "$JBOSS_HOME"/server/default/deploy/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar:"$JBOSS_HOME"/server/default/lib/ojdbc14.jar \
org.cambridge.ebooks.production.embargo.UpdateEmbargoMain \; >> $LOGFILE
