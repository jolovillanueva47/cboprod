#!/bin/sh
# auto delete lock file if lock file timestamp is 4hrs less than current timestamp
JBOSS_HOME="/app/jboss-4.2.3.GA"

NOW=`date '+ebooks.index.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/auto_delete_lock_$NOW.txt"

java -classpath "$JBOSS_HOME"/server/default/deploy/production.war/WEB-INF/classes/ \
org.cambridge.ebooks.production.util.AutoDeleteLock \; >> $LOGFILE
