#!/bin/bash

DIR1="/app/ebooks/content/"
XPDF="/app/xpdf/bin/pdftotext"

STARTED=`date`

echo "Convert pdf to text"
echo "start time: "$STARTED

for dirs in `find $DIR1 -type d`
do
	if [ $dirs != $DIR1 ]; then
		
		dir=`basename $dirs`
		
		for phy_file in `find $dirs -name "*.pdf"`
		do
			text_file=$dirs/`basename $phy_file .pdf`".txt"
			
			if test -s $text_file
			then
				if test $phy_file -nt $text_file
				then
					$XPDF $phy_file
				fi
			else
				$XPDF $phy_file
			fi
		done
		
	fi
done

FINISHED=`date`
echo "end time: "$FINISHED