#!/bin/sh
# $1 - EISBN
# $2 - CONTENT DIR
# $3 - ASSET FOLDER + EISBN + _cur/Digital/CBO/

NOW=`date '+ebooks.asset.store.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/asset.zip.$NOW.txt"

echo zip -r $3$1".zip" $1 -i \*CBO.pdf \*CBO.jpg *$1.xml \*manifest.xml >> $LOGFILE

cd $2
zip -r $3$1".zip" $1 -i \*CBO.pdf \*.jpg *$1.xml \*manifest.xml >> $LOGFILE
