#!/bin/bash

ZIP=`basename $1 _*`
ISBN=`echo ${ZIP%%_*p}`

DIR1="/app/ebooks/content/"$ISBN
XPDF="/app/xpdf/bin/pdftotext"

#STARTED=`date`

echo $DIR1
echo $ISBN
echo $XPDF
#echo "start time: "$STARTED
		
for phy_file in `find $DIR1 -name "*.pdf"`
do
	text_file=$DIR1/`basename $phy_file .pdf`".txt"
	
	if test -s $text_file
	then
		if test $phy_file -nt $text_file
		then
			echo "pdf is newer "$phy_file
			$XPDF $phy_file
		fi
	else
		echo $text_file" does not exist"
		echo "phy_file: "$phy_file
		$XPDF $phy_file
	fi
done

#FINISHED=`date`
#echo "end time: "$FINISHED