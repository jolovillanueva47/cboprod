#!/bin/sh
CONTENT_DIR="/app/ebooks/content/"
JBOSS_HOME="/app/jboss-4.2.3.GA"
HOST_IP="192.168.60.179"
DEF_USERID="1735724"
DEF_USERNAME="ebkmanila"

NOW=`date '+ebooks.index.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"
LOCK=/app/ebooks/lockfile

if [ -f $LOCK ]; then
  exit
fi
touch $LOCK

find "$CONTENT_DIR"* -type d \
-exec java -Dcontent.dir="$CONTENT_DIR" \
-Dprovider.url="$HOST_IP":1099 \
-Ddefault.userid="$DEF_USERID" \
-Ddefault.username="$DEF_USERNAME" \
-classpath "$JBOSS_HOME"/server/default/deploy/production.war/WEB-INF/classes/:"$JBOSS_HOME"/client/jbossall-client.jar \
org.cambridge.ebooks.production.load.ValidatorMessage {} \; >> $LOGFILE

/usr/bin/java -Xms128m -Xmx512m -Djavax.xml.parsers.DocumentBuilderFactory=net.sf.saxon.dom.DocumentBuilderFactoryImpl -Djavax.xml.parsers.SAXParserFactory=net.sf.saxon.aelfred.SAXParserFactoryImpl -Djavax.xml.xpath.XPathFactory=net.sf.saxon.xpath.XPathFactoryImpl -cp /app/ebooks/lib/cuplib.jar:/app/ebooks/lib/saxon-aelfred.jar:/app/ebooks/lib/saxon9-xpath.jar:/app/ebooks/lib/saxon9-dom.jar:/app/ebooks/lib/saxon9.jar:/app/ebooks/lib/lucene-core-2.4.0.jar:/app/ebooks/references/classes:/app/ebooks/lib/log4j.jar:/app/ebooks/lib/freemarker.jar:/app/ebooks/lib/HTTPClient.jar:/app/ebooks/indexer org.cambridge.ebooks.indexer.index.Main >> $LOGFILE
