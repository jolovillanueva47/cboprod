#!/bin/sh
CONTENT_DIR="/app/ebooks/content"

NOW=`date '+check.new.content.log.%Y-%d-%m'`
LOGFILE="/app/ebooks/log/$NOW.txt"

LOCAL=`date | awk '{print $6}'`
START_DATE=`TZ=${LOCAL}+0:05 date +%Y%m%d%H%M`
END_DATE=`TZ=${LOCAL}+0 date +%Y%m%d%H%M`

touch -t $START_DATE $CONTENT_DIR/start_date >> $LOGFILE

touch -t $END_DATE $CONTENT_DIR/end_date >> $LOGFILE

for phy_file in `find /app/ebooks/content/* -newer /app/ebooks/content/start_date -a ! -newer /app/ebooks/content/end_date -a -type d -print -o -type d -prune`
do
	ISBN=`basename $phy_file`
	
	echo "LOCAL: "$LOCAL >> $LOGFILE
	echo "START_DATE: "$START_DATE >> $LOGFILE
	echo "END_DATE: "$END_DATE >> $LOGFILE
	echo "mkdir /app/ebooks/isbnerrors/"$ISBN >> $LOGFILE
	
	mkdir "/app/ebooks/isbnerrors/"$ISBN >> $LOGFILE
done