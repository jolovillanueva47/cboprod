package org.cambridge.ebooks.production.referenceResolver.daily.main;

import java.sql.Timestamp;

import org.cambridge.ebooks.production.referenceResolver.daily.bean.ReferenceResolverInfoBean;
import org.cambridge.ebooks.production.util.DateUtil;

public class DailyLogger {
	public static void createLogHeader(StringBuffer sb){
		sb.append("***************************************** \n");
		sb.append("Start creating reference file(s). Start time is " + new Timestamp(new java.util.Date().getTime()) + "\n");
	}
	
	public static void append(StringBuffer sb, String str){
		sb.append(str);
	}
	
	
	
	public static void createLogBody(StringBuffer sb, ReferenceResolverInfoBean b){
		sb.append(b.toString() + "\n");
	}
	
	public static void createLogEnd(StringBuffer sb){
		sb.append("********************** FINISHED \n");
	}	
	
	public static String generateFilename(String... args){
		int size = args.length;
		String loc = args[size-1];		
			
		/*generate date format*/
		String currDate = DateUtil.getCurrentDate("yyyy-MMMMM-dd");
		
		String filename = "ReferenceResolver.create.log." + currDate;
		
		return loc + filename;
	}
}
