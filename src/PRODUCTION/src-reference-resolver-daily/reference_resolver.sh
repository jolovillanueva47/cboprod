#!/bin/sh
jar_path="/app/ebooks/reference-resolver-daily/"
properties_path="/app/ebooks/solr/online/solr_daily/bin/"

NOW=`date '+reference.resolver.log.%Y%m%d'`
DATETODAY=`date '+%Y%m%d'`
LOGFILE="/app/ebooks/reference-resolver-daily/log/$NOW.txt"

#java -Xms128m -Xmx2048m -jar "$jar_path"crossref-submission-daily.jar "null" $deploy_path_online"properties-service.xml" $deploy_path_prod"properties-service.xml" $jar_path"log/" >> $LOGFILE &
/app/jdk1.6.0_20/bin/java -Xms128m -Xmx2048m -jar "$jar_path"reference-resolver-daily.jar "null" $properties_path"properties.txt"  >> $LOGFILE

#After generating scripts, change permission to executable
chmod +x out/*.sh

#create the folders with the current date
if [ -f out/old/$DATETODAY ]; then
	echo out/old/$DATETODAY Exists...
else 
	mkdir out/old/$DATETODAY
fi

#if [ -f out/logs/$DATETODAY ]; then
#	echo out/logs/$DATETODAY Exists...
#else 
#	mkdir out/logs/$DATETODAY
#fi

#mv out/*.sh out/old/$DATETODAY/*.sh
cd $jar_path"out"
for i in `find *.sh`
do
	sh $i
    mv $i old/$DATETODAY/.
done


#mv *.out out/logs/$DATETODAY/*.out
cd $jar_path"out"
for i in `find *.log`
do
echo $i
	mv $i logs/.
done

#give output that the task is complete
echo "Daily Reference Resolver for " $DATETODAY " completed."