package org.cambridge.ebooks.production.ejb.asset.util;


/**
 * @author Karlson A. Mulingtapang
 * Loader.java - Loader Constants
 */
public class Loader {
	public static final String XPATH_PDF_ALL
	= "book/content-items/descendant::content-item/pdf/attribute::filename";
	
	public static final String XPATH_CONTENT_ITEMS = "book/content-items/descendant::content-item/attribute::id";
	
	public static final String XPATH_ABSTRACT_IMAGES 	
	= "book/content-items/descendant::content-item/abstract/attribute::alt-filename";
	
	public static final String XPATH_COVER_IMAGES
	= "book/metadata/descendant::cover-image/attribute::filename";	
	
	public static final String XPATH_COVER_STANDARD 
	= "book/metadata/descendant::cover-image[attribute::type=\"standard\"]/attribute::filename";
	
	public static final String XPATH_COVER_THUMB
	= "book/metadata/descendant::cover-image[attribute::type=\"thumb\"]/attribute::filename";
	
	public static final String XPATH_CHAPTER_IDS
	= "book/content-items/descendant::content-item[attribute::type=\"chapter\"]/attribute::id";
	
	public static final String XPATH_BOOK_ID = "book/attribute::id";
	
	public static final String XPATH_EISBN = "book/metadata/isbn/text()";
	
	public static final String XPATH_BOOK_TITLE = "book/metadata/main-title";
	
	public static final String XPATH_SERIES_CODE = "book/metadata/series/attribute::code";
	
	public static final String XPATH_APPLICATION_PDF = "package/descendant::file[attribute::datatype=\"application/pdf\"]/attribute::filename";
	public static final String XPATH_ABSTRACT_IMAGE = "package/descendant::file[attribute::datatype=\"abstract_image\"]/attribute::filename";
	public static final String XPATH_TITLE_IMAGE = "package/descendant::file[attribute::datatype=\"title_image\"]/attribute::filename";
	public static final String XPATH_PDF_METADATA = "package/descendant::file[attribute::datatype=\"pdf_metadata\"]/attribute::filename";
	
	public static final String BASE_CONTENT_PATH = System.getProperty("content.dir").trim();
	
	public static final String MANIFEST_FILENAME = "MANIFEST.txt";
	public static final String MANIFEST_XML = "manifest.xml";
	
	public static final String VALIDATION_OUTPUT_FILENAME = "VALIDATION.txt";
	
	public static final String CONTENT_ITEMS_LIST_OUTPUT_FILENAME = "CONTENT_ITEMS.txt";
	
	public static final String SEPARATOR = "------------------------------------------------------------<br />";
	
	public static final String NEW_LINE = "<br />";
	
	public static final String DTD_FILE_PATH = System.getProperty("dtd.file.path").trim();
	
}

