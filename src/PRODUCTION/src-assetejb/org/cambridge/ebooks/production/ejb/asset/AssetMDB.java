package org.cambridge.ebooks.production.ejb.asset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.ejb.asset.util.ContentFiles;
import org.cambridge.ebooks.production.ejb.asset.util.Loader;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@MessageDriven(name = "AssetMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/asset/logger") })
@TransactionManagement(TransactionManagementType.BEAN)
public class AssetMDB implements MessageListener {

	private static final long serialVersionUID = 1L;

	@Resource
	private MessageDrivenContext mdc;

	private Template manifestTemplate;
	private Template assetTemplate;
	private Template assetZipTemplate;
	
	private static final String BASE_CONTENT_PATH 	= System.getProperty("content.dir").trim(); 
	private static final String ASSET_SPOOL_DIR 	= System.getProperty("asset.spool.dir").trim();
	private static final String ASSET_TEMPLATE 		= System.getProperty("asset.template").trim();
	private static final String ASSET_ZIP_SCRIPT 	= System.getProperty("asset.zip.script").trim();
	private static final String BATCH_SCRIPTS_DIR   = System.getProperty("dir.batch.scripts").trim();
	
	private String eisbn;
	
	public void onMessage(Message recvMsg) {
		Logger.getLogger(AssetMDB.class).info("On Message Asset EJB called");
		try {
			if (recvMsg instanceof Message) {
				Message payload = (Message)recvMsg;

				eisbn = payload.getStringProperty("EISBN");

				//create Isbn folder in the published folder
				//UtilityIO.executeCommand("mkdir -p " + ASSET_SPOOL_DIR + eisbn + "_cur/Digital/CBO/");
				
				// create the delivery note in the content.dir folder
				File file = new File(BASE_CONTENT_PATH + eisbn);
				createDeliveryNote(file, eisbn);
				
				//get content items from header file
				List<Item> xmlContents = getContentFiles();
				Collections.sort(xmlContents);
				
				
				//createAssetZipperScript(String assetSpoolDir, String eisbn, String baseContentPath, List<String> files)
				//create batch script
				System.out.println("Creating batch script...");
				createAssetZipperScript(ASSET_SPOOL_DIR, eisbn, BASE_CONTENT_PATH, xmlContents);

				
				// zip the content.dir folder and place it in the published folder
				//ProcessBuilder pb = new ProcessBuilder(ASSET_ZIP_SCRIPT, eisbn, BASE_CONTENT_PATH, ASSET_SPOOL_DIR + eisbn + "_cur/Digital/CBO/", xmlContents.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
//				try {
					
					//String filesToZip =  xmlContents.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
					
					//System.out.println("TO EXCUTE: " + ASSET_ZIP_SCRIPT + " " + eisbn + " " + BASE_CONTENT_PATH + " " + ASSET_SPOOL_DIR + eisbn + "_cur/Digital/CBO/" + " " + filesToZip);
					
					//Runtime.getRuntime().exec(ASSET_ZIP_SCRIPT + " " + eisbn + " " + BASE_CONTENT_PATH + " " + ASSET_SPOOL_DIR + eisbn + "_cur/Digital/CBO/" + " " + "*9780511484445toc_pv-v_CBO.pdf");
					//UtilityIO.executeCommand("echo Executed from java app...");
				
//					List<String> command = Arrays.asList(commandWithArgs);
//					int flag = process.waitFor();
//					
//					if(flag == 0)
//						System.out.println("Command executed successfully: " + command);
//					else
//						System.out.println("Failed to execute command: " + command);
					
					
					
					
					//pb.start();
//				} catch (IOException e) {
//					Logger.getLogger(AssetMDB.class).info("onMessage() IOException: "+e.getMessage());
//					e.printStackTrace();
//				} 
//				catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}				
			}
		} catch (JMSException exc) {
			Logger.getLogger(AssetMDB.class).error("An exception occured while processing message "	+ exc.getMessage());
			mdc.setRollbackOnly();
		}
	}

	/**
	 * Creates the manifest file to list all the contents in the zip file
	 * 
	 * @param contentFolder - Folder path to the contents of the book
	 * @param eisbn
	 */
	public void createDeliveryNote(File contentFolder, String eisbn) {
		try {
			ArrayList<Item> items = new ArrayList<Item>();
			Item item =null;

			// get the file names of the contents of the book 
			// to be listed on the delivery note
			if (contentFolder.listFiles() != null) {
				File[] files = contentFolder.listFiles();
				for (File file : files) {
					
					
					if(file.getName().endsWith(".txt") || 
					   file.getName().endsWith("ref.html") || 
					   file.getName().endsWith("ref.xml") || 
					   file.getName().endsWith("manifest.xml")
					   
					){
						// Ignore these files
					}else if(file.getName().endsWith(".xml")){
						item = new Item();
						item.setFileName(file.getName());
						item.setDataType("pdf_metadata");
						items.add(item);
					}else if(file.getName().endsWith("i.jpg") ||
							file.getName().endsWith("t.jpg")){
						item = new Item();
						item.setFileName(file.getName());
						item.setDataType("title_image");
						items.add(item);
					}else if(file.getName().endsWith("CBO.pdf")){
						item = new Item();
						item.setFileName(file.getName());
						item.setDataType("application/pdf");
						items.add(item);
					}else if(file.getName().endsWith("CBO.jpg")){
						item = new Item();
						item.setFileName(file.getName());
						item.setDataType("abstract_image");
						items.add(item);
					}else {
						// Ignore these files
					}
					
				}
				
				//remove orphan files		
				List<Item> xmlContents = getContentFiles();
//				Collections.sort(xmlContents);
//				Collections.sort(items);
				
				items.retainAll(xmlContents);

				/* Create data-model */
				Map<String, Serializable> root = new HashMap<String, Serializable>();
				root.put("referenceID", eisbn);
				root.put("items", items);

				/* Merge data-model with template */
				OutputStream f1 = new FileOutputStream(BASE_CONTENT_PATH + eisbn + "/" + "manifest.xml");
				Writer output = new OutputStreamWriter(f1);
				try {
					manifestTemplate.process(root, output);
				} catch (TemplateException e) {
					Logger.getLogger(AssetMDB.class).info("createDeliveryNote() TemplateException: "+e.getMessage());
					e.printStackTrace();
				}
				output.flush();
				output.close();

				f1.flush();
				f1.close();

				Logger.getLogger(AssetMDB.class).info(" New Manifest file created: " + BASE_CONTENT_PATH + eisbn + "/" + "manifest.xml");
			}
		} catch (FileNotFoundException e) {
			Logger.getLogger(AssetMDB.class).info("createDeliveryNote() FileNotFoundException: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.getLogger(AssetMDB.class).info("createDeliveryNote() IOException: "+e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Gets the last number and appends it to the filename of the
	 * delivery note
	 * @return Returns the last number of the delivery note
	 */
	public int getCounter() {
		File spoolFolder = new File(ASSET_SPOOL_DIR);
		int ctr = 1;
		if (spoolFolder.listFiles() != null) {
			File[] files = spoolFolder.listFiles();
			for (File file : files) {
				if (file.getName().endsWith(".xml")) {
					int start = file.getName().lastIndexOf('_');
					int end = file.getName().lastIndexOf('.');
					int xmlCtr = Integer.parseInt(file.getName().substring(
							start + 1, end));
					if (xmlCtr > ctr)
						ctr = xmlCtr;
				}
			}
		}

		return ctr + 1;
	}
	
	private List<Item> getContentFiles(){
		
		ArrayList<Item> items = new ArrayList<Item>();
		
		File manifest = new File(BASE_CONTENT_PATH + eisbn + "/" + eisbn + ".xml");
		
		List<String> pdfs =  ContentFiles.getContentFiles(Loader.XPATH_PDF_ALL, manifest);
		for(String fileName : pdfs)
		{
			Item item = new Item();
			item.setFileName(fileName);
			item.setDataType("application/pdf");
			items.add(item);
		}
		
		List<String> abstractImages =  ContentFiles.getContentFiles(Loader.XPATH_ABSTRACT_IMAGES, manifest);
		for(String fileName : abstractImages)
		{
			Item item = new Item();
			item.setFileName(fileName);
			item.setDataType("abstract_image");
			items.add(item);
		}
		
		List<String> thumb =  ContentFiles.getContentFiles(Loader.XPATH_COVER_THUMB, manifest);
		for(String fileName : thumb)
		{
			Item item = new Item();
			item.setFileName(fileName);
			item.setDataType("title_image");
			items.add(item);
		}
		
		List<String> standard =  ContentFiles.getContentFiles(Loader.XPATH_COVER_STANDARD, manifest);
		for(String fileName : standard)
		{
			Item item = new Item();
			item.setFileName(fileName);
			item.setDataType("title_image");
			items.add(item);
		}
		
		Item item = new Item();
		item.setFileName(eisbn + ".xml");
		item.setDataType("pdf_metadata");
		items.add(item);
		
		
		return items;
	}
	
	
	private void createAssetZipperScript(String assetSpoolDir, String eisbn, String baseContentPath, List<Item> files){
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		String date = formatter.format(new Date());
		File shellScript = new File(BATCH_SCRIPTS_DIR + "asset_zip_" + date + ".sh");
		PrintWriter pwriter = null;
		String filesToZip = (files != null && !files.isEmpty()) ? files.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "") : "";
		
		try 
		{
			if(!shellScript.exists())
			{
				pwriter = new PrintWriter(new FileWriter(shellScript, true));
				pwriter.println("#!/bin/sh \n");
				pwriter.println("cd " + baseContentPath + " \n");
				pwriter.flush();
				pwriter.close();
			}
			
			pwriter = new PrintWriter(new FileWriter(shellScript, true));
			pwriter.println("if [ -f " +  assetSpoolDir + eisbn + "_cur/Digital/CBO/" + eisbn + ".zip ]; then");
			pwriter.println("\trm -r " + assetSpoolDir + eisbn + "_cur/Digital/CBO/" + eisbn + ".zip");
			pwriter.println("fi");
			pwriter.println("mkdir -p  " + assetSpoolDir + eisbn + "_cur/Digital/CBO");
			pwriter.print("zip -r " + assetSpoolDir + eisbn + "_cur/Digital/CBO/" + eisbn + ".zip " + eisbn );
			pwriter.println(" -i " + filesToZip + " \\*manifest.xml >> $LOGFILE\n");
			pwriter.flush();
			pwriter.close();
			
			System.out.println("Batch script created? [" + shellScript.exists() + "]");
		} 
		catch (IOException e)
		{
			//TODO Auto-generated catch block
			System.err.println(AssetMDB.class + " createAssetZipperScript() " + e.getMessage());
		}
		
				
	}
	

	@PostConstruct
	void init() {
		Logger.getLogger(AssetMDB.class).info(" Initialize Asset EJB ");

		/* Create and adjust the configuration */
		Configuration cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File(ASSET_TEMPLATE));
			cfg.setObjectWrapper(new DefaultObjectWrapper());

			/* Get or create a template */
			manifestTemplate = cfg.getTemplate("manifest.ftl");
			assetTemplate = cfg.getTemplate("asset.ftl");
			assetZipTemplate = cfg.getTemplate("asset_zip.ftl");

		} catch (IOException e) {
			Logger.getLogger(AssetMDB.class).info("init() IOException: "+e.getMessage());
			e.printStackTrace();
		}
	}

	@PreDestroy
	void cleanUp() {
		Logger.getLogger(AssetMDB.class).info(" Destroy Asset EJB ");
	}
	
	
}