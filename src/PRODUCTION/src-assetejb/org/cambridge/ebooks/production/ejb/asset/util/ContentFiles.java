package org.cambridge.ebooks.production.ejb.asset.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author rvillamor
 */

public class ContentFiles {

	
	
	public static List<String> getContentFiles(String xpathExpression, File filename){
		List<String> result = new ArrayList<String>();
		try 
		{
			NodeList nList = getContentFilesNodeList(xpathExpression, filename);
			for(int index=0; index<nList.getLength(); index++)
			{
				Node node = nList.item(index);
				result.add(node.getNodeValue());
			}
		}
		catch(Exception ex)
		{
			System.err.println("[ERROR] " + ContentFiles.class + "getManifestFileNamesByType(String, String)");
		}
		
		return result;
	}
	
		
	private static NodeList getContentFilesNodeList(String xpathExpression, File filename) throws ParserConfigurationException, 
	IOException, SAXException, XPathExpressionException {
		
		DocumentBuilderFactory domFactory = null;		
		DocumentBuilder builder = null;		
		Document doc = null;
		XPathFactory xpFactory = null;
		XPath xPath = null;
		XPathExpression xExpression = null;
		Object result = null;
		
		domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		
		
		builder = domFactory.newDocumentBuilder();
		
		//to ignore dtd file
//		builder.setEntityResolver(new EntityResolver() {
//	        public InputSource resolveEntity(String publicId, String systemId)
//	                throws SAXException, IOException {
//	            if (systemId.contains("manifest.dtd")) {
//	                return new InputSource(new StringReader(""));
//	            } else {
//	                return null;
//	            }
//	        }
//		}
//		);
		
		doc = builder.parse(filename);
		
		xpFactory = XPathFactory.newInstance();
		xPath = xpFactory.newXPath();
		
		xExpression = xPath.compile(xpathExpression);
		
		result = xExpression.evaluate(doc, XPathConstants.NODESET);
		
		return (NodeList) result;
	}
			

}
