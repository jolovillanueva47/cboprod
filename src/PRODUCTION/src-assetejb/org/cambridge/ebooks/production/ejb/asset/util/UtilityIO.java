package org.cambridge.ebooks.production.ejb.asset.util;

import java.io.IOException;

import org.apache.log4j.Logger;

public class UtilityIO {
	
	/**
	 * Executes a Unix command
	 * 
	 * @param command - Unix command
	 */
	public static void executeCommand(String command)
	{
		Process p;
		try{
			p=Runtime.getRuntime().exec(command);

			int i=p.waitFor();
			if(i==0)
				System.out.println("Command executed successfully: "+command);
			else
				System.out.println("Failed to execute command: "+command);
		}catch(IOException ioe){
			Logger.getLogger(UtilityIO.class).info("IOException"+ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			Logger.getLogger(UtilityIO.class).info("InterruptedException"+e.getMessage());
			e.printStackTrace();
		}
	}
}
