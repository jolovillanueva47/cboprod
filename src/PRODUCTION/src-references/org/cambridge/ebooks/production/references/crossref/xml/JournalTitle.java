package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class JournalTitle {

	private String match;
	private String journalTitle;
	
	public JournalTitle(String match, String journalTitle) {
		this.match = match;
		this.journalTitle = journalTitle;
	}
	
	public String getMatch() {
		return match;
	}
	public String getJournalTitle() {
		return journalTitle;
	}
	
	
}
