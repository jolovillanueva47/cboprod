/*
 * Created on Oct 3, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.production.references.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author Administrator
 *
// * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataSource {

//	static private OracleDataSource ods;

	private static String driverType = ReferenceProperties.DB_DRIVERTYPE;
	private static String serverName = ReferenceProperties.DB_SERVERNAME;
	private static String databaseName = ReferenceProperties.DB_DATABASENAME;
	private static String portNumber = ReferenceProperties.DB_PORTNUMBER;
	private static String user = ReferenceProperties.DB_USER;
	private static String password = ReferenceProperties.DB_PASSWORD;
	
	static {
		try {			
			Class.forName("oracle.jdbc.driver.OracleDriver");			
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public static Connection getConnection() {
		Connection con = null;
		try {
			System.out.println("=== Get Connection ===");
			System.out.println("driver type:" + driverType);
			System.out.println("server name:" + serverName);
			System.out.println("database name:" + databaseName);
			System.out.println("port number:" + portNumber);			
			
			con = DriverManager.getConnection(driverType + ":@" + serverName + ":" + portNumber + ":" + databaseName, user, password);
		} catch (Exception e) {
			System.err.println(e);
		}
		return con;
	}
	
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
