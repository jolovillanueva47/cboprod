package org.cambridge.ebooks.production.references.google.books;

import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.util.Misc;

public class GoogleBooksResolver {

	public static Citation resolveGoogleBooksLink(Citation c) {
		
		StringBuffer sb = new StringBuffer();
		
		//http://books.google.com/books?as_q=&num=10&btnG=Google+Search&as_epq=&as_oq=&as_eq=&as_brr=0&as_pt=ALLTYPES&lr=&as_vt=titulo+ng+aklat&as_auth=awtor+ng+masa&as_pub=tagapablis+ng+masa&as_sub=&as_drrb_is=b&as_minm_is=0&as_miny_is=1990&as_maxm_is=0&as_maxy_is=1990&as_isbn=&as_issn=
		
		
		sb.append("&title=");
		if (c.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK) && Misc.isNotEmpty(c.getUnformattedBookTitle())) {
			sb.append(c.getUnformattedBookTitle());
		} else if (c.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
			if (Misc.isNotEmpty(c.getUnformattedArticleTitle())) {
				sb.append(c.getUnformattedArticleTitle());
			} else if (Misc.isNotEmpty(c.getUnformattedJournalTitle())) {
				sb.append(c.getUnformattedJournalTitle());
			}
		}
		
		if (c.getAuthors() != null && c.getAuthors().size() > 0) {
			sb.append("&as_auth=");
			for (String s : c.getAuthors()) {
				sb.append(s);
			}
		}
		
		if (Misc.isNotEmpty(c.getPublisherName())) {
			sb.append("&as_pub=");
			sb.append(c.getPublisherName());
		}
		
		if (Misc.isNotEmpty(c.getYear())) {
			sb.append("&as_minm_is=0&as_miny_is=");
			sb.append(c.getYear());
			sb.append("&as_maxm_is=0&as_maxy_is=");
			sb.append(c.getYear());
		}
		
		c.setGoogleBooksLink(escapeQuotes(sb.toString()));
		
		return c;
	}
	
	private static String escapeQuotes(String s) {
		if (s != null && s.length() > 0) {
			s = s.replace("'", "\\'").replace("\"", "\\\"");
		}
		return s;
	}
}
