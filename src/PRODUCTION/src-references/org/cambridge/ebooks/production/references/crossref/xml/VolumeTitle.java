package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class VolumeTitle {

	private String match;
	private String volumeTitle;
	
	public VolumeTitle(String match, String volumeTitle) {
		this.match = match;
		this.volumeTitle = volumeTitle;
	}
	
	public String getMatch() {
		return match;
	}
	public String getVolumeTitle() {
		return volumeTitle;
	}
}
