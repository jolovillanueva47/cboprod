package org.cambridge.ebooks.production.references.util;

import java.io.File;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * @author Karlson A. Mulingtapang
 * XmlTransformer.java - Transforms xml using JAXP
 */
public class XmlTransformer {
	private File xmlFile;
	private File xsltFile;
	private File resultFile;
	private Source xmlSource;
	private Source xsltSource;
	private Result result;
	
	private XmlTransformer() {		
	}
	
	/**
	 * Creates an instance of XmlTransformer
	 * @return XmlTransformer
	 */
	public static XmlTransformer newInstance() {
		return new XmlTransformer();
	}
	
	/**
	 * Transform a source xml file with the specifications defined in a source xslt file
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 * @throws NullPointerException
	 */
	public void transform(String contentXml, String contentId) throws TransformerConfigurationException, 
	TransformerException, NullPointerException {
		
		if(this.xmlFile == null) {
			throw new NullPointerException("[NULL]:xmlFile");
		}
		
		if(this.xsltFile == null) {
			throw new NullPointerException("[NULL]:xsltFile");
		}
		
		xmlSource = new StreamSource(this.xmlFile);
		xsltSource = new StreamSource(this.xsltFile);
		result = new StreamResult(this.resultFile);
		
		TransformerFactory transFact = TransformerFactory.newInstance();
		Transformer trans = transFact.newTransformer(this.xsltSource);
		trans.setParameter("content_xml", contentXml); 
		trans.setParameter("content_id", contentId); 
		trans.transform(this.xmlSource, this.result);
	}

	/**
	 * @return the xmlFile
	 */
	public File getXmlFile() {
		return xmlFile;
	}

	/**
	 * @param xmlFile the xmlFile to set
	 */
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}

	/**
	 * @return the xsltFile
	 */
	public File getXsltFile() {
		return xsltFile;
	}

	/**
	 * @param xsltFile the xsltFile to set
	 */
	public void setXsltFile(File xsltFile) {
		this.xsltFile = xsltFile;
	}

	/**
	 * @return the resultFile
	 */
	public File getResultFile() {
		return resultFile;
	}

	/**
	 * @param resultFile the resultFile to set
	 */
	public void setResultFile(File resultFile) {
		this.resultFile = resultFile;
	}	
}
