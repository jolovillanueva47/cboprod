package org.cambridge.ebooks.production.references.elements;

import java.util.List;

/**
 * 
 * @author jgalang
 * 
 */

public class References {

	private String bookId;
	private String eisbn;
	private String parentId;
	private String parentPosition;
	private String typeAttribute;
	private String referenceTitle;
	
	private String bookTitle;
	private String unformattedBookTitle;
	private String contentAbstract;
	private String contentDoi;
	private String publishedOnlineDate;
	
	private List<References> subReferences;
	private List<Citation> allCitations;
	private List<Citation> citations;
	private List<String> keywords;

	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getEisbn() {
		return eisbn;
	}
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentPosition() {
		return parentPosition;
	}
	public void setParentPosition(String parentPosition) {
		this.parentPosition = parentPosition;
	}
	public String getTypeAttribute() {
		return typeAttribute;
	}
	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}
	public List<References> getSubReferences() {
		return subReferences;
	}
	public void setSubReferences(List<References> subReferences) {
		this.subReferences = subReferences;
	}
	public String getReferenceTitle() {
		return referenceTitle;
	}
	public void setReferenceTitle(String referenceTitle) {
		this.referenceTitle = referenceTitle;
	}
	public String getUnformattedBookTitle() {
		return unformattedBookTitle;
	}
	public void setUnformattedBookTitle(String unformattedBookTitle) {
		this.unformattedBookTitle = unformattedBookTitle;
	}
	public List<Citation> getCitations() {
		return citations;
	}
	public void setCitations(List<Citation> citations) {
		this.citations = citations;
	}
	public List<Citation> getAllCitations() {
		return allCitations;
	}
	public void setAllCitations(List<Citation> allCitations) {
		this.allCitations = allCitations;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getContentAbstract() {
		return contentAbstract;
	}
	public void setContentAbstract(String contentAbstract) {
		this.contentAbstract = contentAbstract;
	}
	public String getContentDoi() {
		return contentDoi;
	}
	public void setContentDoi(String contentDoi) {
		this.contentDoi = contentDoi;
	}
	public String getPublishedOnlineDate() {
		return publishedOnlineDate;
	}
	public void setPublishedOnlineDate(String publishedOnlineDate) {
		this.publishedOnlineDate = publishedOnlineDate;
	}
	public List<String> getKeywords() {
		return keywords;
	}
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}
	
	
}
