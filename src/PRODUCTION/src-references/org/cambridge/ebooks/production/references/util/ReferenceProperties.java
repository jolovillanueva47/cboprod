package org.cambridge.ebooks.production.references.util;

import static org.cambridge.ebooks.production.references.util.PropertiesManager.getProperty;

/**
 * 
 * @author jgalang
 * 
 */

public interface ReferenceProperties {
	
	public static final String REFERENCES_PROPERTIES_FILE = PropertiesManager.getPropertyFile();
	public static final String ISO_UNI_TABLE_PROPERTIES_FILE = "iso_uni_table.properties";
	
	public static final String CITATION_TYPE_OTHER = "other";
	public static final String CITATION_TYPE_BOOK = "book";
	public static final String CITATION_TYPE_JOURNAL = "journal";
	
	public static final String INDEX_DIR = getProperty("index.dir");
	public static final String REFERENCES_DIR = getProperty("references.dir");
	public static final String TEMPLATES_DIR = getProperty("templates.dir");
	public static final String XML_TMP_DIR = getProperty("xml.tmp.dir");
	public static final String CONTENT_DIR = getProperty("content.dir");
	public static final String REFERENCES_TEMPLATE = getProperty("reference.template");
	public static final String RESOLVER_REPORT_TEMPLATE = getProperty("resolver.report.template");
	public static final String EBOOKS_CONTEXT_PATH = getProperty("ebooks.context.path");
		
	public static final String XML_VERSION = getProperty("crossref.xml.version");
	public static final String XML_ENCODING = getProperty("crossref.xml.encoding");
	public static final String QUERYBATCH_VERSION = getProperty("crossref.querybatch.version");
	public static final String QUERYBATCH_XMLNS = getProperty("crossref.querybatch.xmlns");
	public static final String QUERYBATCH_XMLNSXSI = getProperty("crossref.querybatch.xmlnsxsi");
	public static final String RESPONSE_EMAIL = getProperty("crossref.response.email");
	public static final String QUERY_TEMPLATE = getProperty("crossref.query.template");
	public static final String USERNAME = getProperty("crossref.username");
	public static final String PASSWORD = getProperty("crossref.password");
	public static final String QUERY_FORMAT = getProperty("crossref.query.format");
	public static final String QUERY_SITE = getProperty("crossref.query.site");
	public static final String ORG_SITE = getProperty("crossref.org.site");
	public static final String ORG_PORT = getProperty("crossref.org.port");
	public static final String REFERENCE_URL = getProperty("crossref.reference.url");
	public static final String REFERENCES_XSLT_FILE = getProperty("reference.xslt");
	public static final String REFERENCES_XSLT_ALL_FILE = getProperty("reference.xslt.all");
	
	public static String DB_DRIVERTYPE = getProperty("db.drivertype");
	public static String DB_SERVERNAME = getProperty("db.servername");
	public static String DB_NETWORKPROTOCOL = getProperty("db.networkprotocol");
	public static String DB_DATABASENAME = getProperty("db.databasename");
	public static String DB_PORTNUMBER = getProperty("db.portnumber");
	public static String DB_USER = getProperty("db.user");
	public static String DB_PASSWORD = getProperty("db.password");	
	public static String DB_ROWPREFETCH = getProperty("db.rowprefetch");
	
	public static String JDBC_DRIVER = getProperty("jdbc.driver");
	public static String JDBC_PORT = getProperty("jdbc.port");
	public static String JDBC_SID = getProperty("jdbc.sid");
	public static String JDBC_USER = getProperty("jdbc.user");
	public static String JDBC_PASS = getProperty("jdbc.pass");
	public static String JDBC_URL = getProperty("jdbc.url");
	
}
