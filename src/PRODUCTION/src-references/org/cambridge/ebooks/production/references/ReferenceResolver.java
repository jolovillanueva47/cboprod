package org.cambridge.ebooks.production.references;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.cambridge.ebooks.production.indexer.ProductionSOLRIndexer;
import org.cambridge.ebooks.production.references.crossref.CrossRefQuerySender;
import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.elements.Pattern;
import org.cambridge.ebooks.production.references.elements.References;
import org.cambridge.ebooks.production.references.google.books.GoogleBooksResolver;
import org.cambridge.ebooks.production.references.google.scholar.GoogleScholarResolver;
import org.cambridge.ebooks.production.references.jdbc.DBInfo;
import org.cambridge.ebooks.production.references.openurl.OpenURLResolver;
import org.cambridge.ebooks.production.references.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.references.util.StringUtil;
import org.cambridge.ebooks.production.references.util.XMLDOMParser;
import org.cambridge.ebooks.production.references.util.XPathWorker;
import org.cambridge.ebooks.production.references.util.XmlTransformer;
import org.w3c.dom.Document;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * @author jgalang
 * modified - arvin - index references to SOLR Core - 20140313
 * 
 */
public class ReferenceResolver implements ReferenceProperties {
	
	
	public static void resolveCitations(String bookId, boolean resolveThirdParty, ResolverReport report) throws Exception {
		resolveCitations(bookId, CitationIndexWorker.getBookIsbn(bookId), resolveThirdParty, report);
	}
	
	public static void resolveCitations(String bookId, String eisbn, boolean resolveThirdParty, ResolverReport report) throws Exception {
		Map<String, List<References>> contentReferences = CitationXmlWorker.getReferencesMap(bookId, eisbn);
		
		if (resolveThirdParty) {
			resolveThirdPartyLinks(contentReferences, report);
		}
		
		List<References> allReferences = new ArrayList<References>();
		
		if (contentReferences != null && contentReferences.size() > 0) {
			for (String contentId : contentReferences.keySet()) {
				
				generateHtmlPerContent(bookId, eisbn, contentReferences.get(contentId));
				allReferences.addAll(contentReferences.get(contentId));
			}
		}
		
//		String refFile = CONTENT_DIR + eisbn + "/" + bookId + "ref.html";
		String refFile = IsbnContentDirUtil.getPath(eisbn) + "/" + bookId + "ref.html";
		generateFinalHtml(bookId, refFile, eisbn, bookId, allReferences);
		
		//arvin - index references to SOLR Core - 20140313
		index(eisbn,allReferences);
 			
	}
	
	//arvin - index references to SOLR Core - 20140313
	private static void index(String eisbn, List<References> allReferences) {
		File[] isbnsDir = null;
		isbnsDir = new File[]{ new File(IsbnContentDirUtil.getPath(eisbn))};
		ProductionSOLRIndexer p = new ProductionSOLRIndexer();
		p.indexRef(isbnsDir,allReferences);	
	}
	
	private static ResolverReport resolveThirdPartyLinks(Map<String, List<References>> references, ResolverReport report) throws SQLException {
 
		Connection conn = DBInfo.getConnection();
		if (references != null && references.size() > 0) {
			for (String contentId : references.keySet()) {
				for (References referenceList : references.get(contentId)) {
					
					if (referenceList != null && referenceList.getAllCitations() != null && referenceList.getAllCitations().size() > 0) {
						for (Citation citation : referenceList.getAllCitations()) {
							
							citation = GoogleScholarResolver.resolveGoogleScholarLink(citation);
							report.incrementGoogleScholar();
							
							citation = OpenURLResolver.resolveOpenURLLink(citation);
							report.incrementOpenUrl();
							
							String citationType = citation.getType();
							
							if (citation.forQuery()) {
								
								try {
									
									if (!citationType.equals(CITATION_TYPE_JOURNAL)) {
										citation = GoogleBooksResolver.resolveGoogleBooksLink(citation);
										report.incrementGoogleBooks();
									}
									
									
									citation = CrossRefQuerySender.resolveCrossRefReference(citation);
				
									//alacerna - Start querying the CrossRefDoi against the database
									if(citation.getCrossRefDoi()!= null){
										String doiLink = citation.getCrossRefDoi();
										if(doiLink.contains("/")){	
											int doiLinkIndex = doiLink.indexOf("/");
											String crossRefDoi = doiLink.substring(doiLinkIndex+1);		
											System.out.println(crossRefDoi);
											if(crossRefDoi.startsWith("S")||crossRefDoi.startsWith("CBO")||crossRefDoi.startsWith("CCO")
										||crossRefDoi.startsWith("CHO")||crossRefDoi.startsWith("SSO")||crossRefDoi.startsWith("UPO")){
												List<String> testThis = search(crossRefDoi,doiLink,conn);
												StringBuffer stringBuffer = null;
												StringBuffer stringBuffer2 = null;
												if (null != testThis && !testThis.isEmpty()){
													if(testThis.size()==1){
														stringBuffer = new StringBuffer("http://journals.cambridge.org/abstract_");
														stringBuffer2 = new StringBuffer("CJOLink");
														citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
														citation.setLinkType(stringBuffer2.toString());
													}
													if(testThis.size()==2){
														if(testThis.get(1).equalsIgnoreCase("CBO")){
															stringBuffer = new StringBuffer("http://ebooks.cambridge.org/ref/id/");
															stringBuffer2 = new StringBuffer("CBOLink");
															citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
															citation.setLinkType(stringBuffer2.toString());
														}else if (testThis.get(1).equalsIgnoreCase("UPO")){
															stringBuffer = new StringBuffer("http://universitypublishingonline.org/ref/id/");
															stringBuffer2 = new StringBuffer("UPOLink");
															citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
															citation.setLinkType(stringBuffer2.toString());
														}else if (testThis.get(1).equalsIgnoreCase("CCO")){
															stringBuffer = new StringBuffer("http://universitypublishingonline.org/ref/id/companions/");
															stringBuffer2 = new StringBuffer("CCOLink");
															citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
															citation.setLinkType(stringBuffer2.toString());
														}else if (testThis.get(1).equalsIgnoreCase("CHO")){
															stringBuffer = new StringBuffer("http://universitypublishingonline.org/ref/id/histories/");
															stringBuffer2 = new StringBuffer("CHOLink");
															citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
															citation.setLinkType(stringBuffer2.toString());
														}else if (testThis.get(1).equalsIgnoreCase("SSO")){
															stringBuffer = new StringBuffer("http://universitypublishingonline.org/ref/id/shakespeare/");
															stringBuffer2 = new StringBuffer("SSOLink");
															citation.setCambridgeLInk(stringBuffer.append(testThis.get(0)).toString());
															citation.setLinkType(stringBuffer2.toString());
											  }	
										   }
										  }
										 }
										}
									}
									if (citation.getCrossRefStatus().equals(Citation.STATUS_RESOLVED)) {
										report.incrementCrossRef();
									}
									
									if (citationType.equals(CITATION_TYPE_BOOK)) {
										report.incrementBook();
									} else if (citationType.equals(CITATION_TYPE_JOURNAL)) {
										report.incrementJournal();
									}
										
								} catch (Exception e) {
									System.err.println("ERROR!: " + citation.getCitationId());
									e.printStackTrace();
								}
								
							} else {
								report.incrementOther();
							}
						}
					}
				}
				
			}
		}
		if (!conn.isClosed()){
			conn.close();	
		}
		return report;
	}
	
	private static List<String> search(String crossRefDoi, String doiLink, Connection conn) {
		
		return DBInfo.search(crossRefDoi,doiLink,conn);
		
		
	}

	public static void generateHtmlPerContent(String bookId, String eisbn, List<References> referencesList) {
		if (referencesList != null && referencesList.size() > 0) {
//			String refFile = CONTENT_DIR + eisbn + "/" + referencesList.get(0).getParentId() + "ref.html";
			String refFile = IsbnContentDirUtil.getPath(eisbn) + "/" + referencesList.get(0).getParentId() + "ref.html";
			generateFinalHtml(referencesList.get(0).getParentId(), refFile, eisbn, bookId, referencesList);
		}
	}

	public static void generateFinalHtml(String filename, String refPath, String eisbn, String bookId, List<References> referencesList) {
				
		try {
			
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_DIR));
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("referencesList", referencesList);
			
//			File dir = new File(CONTENT_DIR + eisbn);
			File dir = new File(IsbnContentDirUtil.getPath(eisbn));
			if (!dir.isDirectory()) {
				dir.mkdir();
			}			
			
			File f = new File(refPath);
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
	                (new FileOutputStream(f),XML_ENCODING));			
			
			Template xml = cfg.getTemplate(REFERENCES_TEMPLATE);
	        xml.process(root, bw);	 	        
			
	        System.out.println("Generated file: " + f.getName());
			
		} catch (IOException e) {
			System.err.println(e);
		} catch (TemplateException e) {
			System.err.println("Template Exception: " + e);
		}
	}
	
	public static void generateHtml(String filename, String eisbn, String bookId, List<Citation> citations, boolean isAllReference) {
		
//		if(isAllReference) {
//			String filePathWOExtension = CONTENT_DIR + eisbn + "/" + filename;
//			ExportCitation.createExportCitationForAllReference(citations, filePathWOExtension);
//		}
		
//		String refFile = CONTENT_DIR + eisbn + "/" + filename + "ref.xml";
		String refFile = IsbnContentDirUtil.getPath(eisbn) + "/" + filename + "ref.xml";
//		String resultPath = CONTENT_DIR + eisbn + "/" + filename + "ref.html";	
		String resultPath = IsbnContentDirUtil.getPath(eisbn) + "/" + filename + "ref.html";
		
//		File headerFile = new File(CONTENT_DIR + eisbn + "/" + eisbn + ".xml");
		File headerFile = new File(IsbnContentDirUtil.getPath(eisbn) + "/" + eisbn + ".xml");
		File resultFile = new File(resultPath);
		File refXsltFile = null;
		if(isAllReference) {
			refXsltFile = new File(REFERENCES_XSLT_ALL_FILE);
		} else {
			refXsltFile = new File(REFERENCES_XSLT_FILE);
		}
		
		generateXml(filename, refFile, eisbn, bookId, citations);  	        
		
        XmlTransformer xmlTrans = XmlTransformer.newInstance();
		xmlTrans.setXmlFile(headerFile);
		xmlTrans.setXsltFile(refXsltFile);
		xmlTrans.setResultFile(resultFile);
		
		try {
			xmlTrans.transform(refFile, filename);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
        System.out.println("Generated HTML: " + resultFile.getName());		
	}
	
	public static void generateXml(String filename, String refPath, String eisbn, String bookId, List<Citation> citations) {
		try {
			Collections.sort(citations);
			
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_DIR));
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("citationList", citations);
			
//			File dir = new File(CONTENT_DIR + eisbn);
			File dir = new File(IsbnContentDirUtil.getPath(eisbn));
			if (!dir.isDirectory()) {
				dir.mkdir();
			}			
			
			File f = new File(refPath);
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
	                (new FileOutputStream(f),XML_ENCODING));			
			
			Template xml = cfg.getTemplate(REFERENCES_TEMPLATE);
	        xml.process(root, bw);	 	        
			
	        System.out.println("Generated file: " + f.getName());
			
		} catch (IOException e) {
			System.err.println(e);
		} catch (TemplateException e) {
			System.err.println("Template Exception: " + e);
		}
	}
	
	private static String getBookIdFromEisbn(String eisbn) {
//		System.out.println(CONTENT_DIR + eisbn + File.separator + eisbn + ".xml");
		System.out.println(IsbnContentDirUtil.getPath(eisbn) + eisbn + ".xml");
//		Document doc = XMLDOMParser.getXmlDOM(CONTENT_DIR + eisbn + File.separator + eisbn + ".xml");
		Document doc = XMLDOMParser.getXmlDOM(IsbnContentDirUtil.getPath(eisbn) + eisbn + ".xml");
		return XPathWorker.getXPath(doc, Pattern.BOOK_ID);
	}
	
	private static String[] processArgs(String[]args, ResolverReport report){
		
		if(args.length > 0){
			report.setBatchName(args[0]);
			
			if(args[1].endsWith(".txt")){
				String filePath = args[1];
				String str;
				
				ArrayList<String> list = new ArrayList<String>();
				list.add(args[0]);
				
				try {
					BufferedReader in = new BufferedReader(new FileReader(filePath));
					
					while((str = in.readLine()) !=null){
						list.add(str);
					}
					
					
					args = new String[list.size()];
					int counter = 0;
					for(String isbn: list){
						args[counter] = isbn;
						counter++;
					}
					in.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
		}
		return args;
	}

	public static void main(String[] args) {
		System.out.println("BATCH START");
		ResolverReport report = new ResolverReport();
		
		report.setResolverStart(System.currentTimeMillis());
		
		args = processArgs(args, report);
	
		if (args[0].equals("all")) {
			
			Map<String, String> allBooks = CitationIndexWorker.getAllBooks();
			boolean resolveThirdParty = (args.length > 0 && args[1].equals("1"));
			
			for (String s : allBooks.keySet()) {
				try {
					resolveCitations(s, allBooks.get(s), resolveThirdParty, report);
				} catch (Exception e) {
					System.err.println("Error in book!: " + s);
				}
			}
			
		} else if (args[0].equals("create")){
			
			System.out.println("*******************************************");
			System.out.println("Resolving References (no third party links)");
			System.out.println("*******************************************");

			for (int c = 1 ; c<args.length ; c++) {
			
				String isbn = args[c];
				
				File toDelete = null;
				try {
					String dummyDTD = IsbnContentDirUtil.getPath(isbn).substring(0, IsbnContentDirUtil.getPath(isbn).length() - 2)+"header.dtd";
					StringUtil.copyfile(ReferenceProperties.CONTENT_DIR+"header.dtd", dummyDTD);
					toDelete = new File (dummyDTD);
					
					System.out.println("Searching book Id");
					String bookId = getBookIdFromEisbn(isbn);
					System.out.println("BOOK_ID: " + bookId);
					
					resolveCitations(bookId, isbn, false, report);
				} catch (Exception e) {
					System.err.println("Error in book!: " + isbn);
					System.err.println(e.getStackTrace());
				} finally {
					if(toDelete != null)
						toDelete.delete();
				}
				
			}
			System.out.println("*********************************************");
			System.out.println("Resolved References (no third party links)");
			System.out.println("*********************************************");
		}
		else if (args[0].equals("query")){
			System.out.println("*********************************************");
			System.out.println("Resolving References (with third party links)");
			System.out.println("*********************************************");
			
			for (int c = 1 ; c<args.length ; c++) {
				
				String isbn = args[c];
				
				File toDelete = null;
				try {
					String dummyDTD = IsbnContentDirUtil.getPath(isbn).substring(0, IsbnContentDirUtil.getPath(isbn).length() - 2)+"header.dtd";
					StringUtil.copyfile(ReferenceProperties.CONTENT_DIR+"header.dtd", dummyDTD);
					toDelete = new File (dummyDTD);
					
					System.out.println("Searching book Id");
					String bookId = getBookIdFromEisbn(isbn);
					System.out.println("BOOK_ID: " + bookId);
					
					resolveCitations(bookId, isbn, true, report);
				} catch (Exception e) {
					System.err.println("Error in book!: " + isbn);
					e.printStackTrace();
					System.err.println(e.getMessage());
				} finally {
					if(toDelete != null)
						toDelete.delete();
				}
				
			}
		}
		ResolverReportWorker.generateReport(report);
		
		

		/*try {
			
//			resolveCitations("CBO9780511471001", false); //MULTIPLE CHAPTERS CITATIONS. 8500+ Citations!
//			resolveCitations("CBO9780511470776", true, false); //INTRODUCTORY MUON SCIENCE. MULTIPLE CHAPTERS WITH CROSSREF RESOLVED CITATIONS. 362 Citations
//			resolveCitations("CBO9780511481314", true); //WITH HYPERLINKS IN FULL TEXT & CROSSREF RESOLVED CITATIONS. 446 Citations
//			resolveCitations("CBO9780511481376", true); //SINGLE CHAPTER, NO AMPERSAND. 321 Citations
//			resolveCitations("CBO9780511485985", true); //NESTED REFERENCES. CROSSREF. 927 Citations
//			resolveCitations("CBO9780511482113", false);
//			resolveCitations("CBO9780511487521", false);
			
//			resolveCitations("CBO9780511494352", true, false);
//			
//			resolveCitations("CBO9780511481413", true, false);//ANCIENT MESSENIANS
//			resolveCitations("CBO9780511494352", true, false); // SERIES, SUB-CHAPTERS, CROSSREF. 628 Citations.
//			resolveCitations("CBO9780511482120", true, false);// ANCIENT ANGER
			
		*/
		
	}
}
