package org.cambridge.ebooks.production.references.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBInfo {
	
	public static Connection getConnection(){
		return JDBCUtil.getConnection();
		
	}

	public static List<String> search(String crossRefDoi, String doiLink, Connection conn) {
	
		
		PreparedStatement st = null;
		ResultSet rs = null;
		List<String> listOfDOI = null;
		try {
				
			listOfDOI = new ArrayList<String>();
				String sql = "select file_id from ORACJOC.article where doi = ? ";
				String sql1= "select a.book_id,c.cbo_product_group from oraebooks.ebook a, ORAEBOOKS.ebook_isbn_data_load c where a.isbn= c.isbn and a.doi = ?";
				
				if(crossRefDoi.startsWith("S")){
				st = conn.prepareStatement(sql);
				st.setString(1, doiLink);
				
				rs = st.executeQuery();
				
				if (null != rs){
					while (rs.next()) {
						listOfDOI.add(rs.getString("file_id"));
					}
				}
				}
				
				if(crossRefDoi.startsWith("CBO")||crossRefDoi.startsWith("CCO")||crossRefDoi.startsWith("CHO")||crossRefDoi.startsWith("SSO")){
					st = conn.prepareStatement(sql1);
					st.setString(1, doiLink);
					
					rs = st.executeQuery();
					
					if (null != rs){
						while (rs.next()) {
							listOfDOI.add(rs.getString("book_id"));
							listOfDOI.add(rs.getString("cbo_product_group"));
						}
					}	
				}
			
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			try {
				rs.close();
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
		
		return listOfDOI;
	}
}
