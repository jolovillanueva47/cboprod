package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class SeriesTitle {

	private String match;
	private String seriesTitle;
	
	public SeriesTitle(String match, String seriesTitle) {
		this.match = match;
		this.seriesTitle = seriesTitle;
	}
	
	public String getMatch() {
		return match;
	}
	public String getSeriesTitle() {
		return seriesTitle;
	}
}
