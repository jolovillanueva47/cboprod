package org.cambridge.ebooks.production.references.util;

/**
 * 
 * @author jgalang
 * 
 */

public class DateDisplayUtil {

	private long years;
	private long days;
	private long hours;
	private long minutes;
	private long seconds;
	
	public DateDisplayUtil(long time) {
		long secondInMillis = 1000;
		long minuteInMillis = secondInMillis * 60;
		long hourInMillis = minuteInMillis * 60;
		long dayInMillis = hourInMillis * 24;
		long yearInMillis = dayInMillis * 365;

		years = time / yearInMillis;
		time = time % yearInMillis;
		days = time / dayInMillis;
		time = time % dayInMillis;
		hours = time / hourInMillis;
		time = time % hourInMillis;
		minutes = time / minuteInMillis;
		time = time % minuteInMillis;
		seconds = time / secondInMillis;
	}

	public long getYears() {
		return years;
	}

	public long getDays() {
		return days;
	}

	public long getHours() {
		return hours;
	}

	public long getMinutes() {
		return minutes;
	}

	public long getSeconds() {
		return seconds;
	}
}
