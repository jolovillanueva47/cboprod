package org.cambridge.ebooks.production.references;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
/**
 * 
 * @author jgalang
 * 
 */

public class CitationIndexWorker implements ReferenceProperties{
	
	private static final EBookSearchWorker worker = new EBookSearchWorker();
//	private static final String FIELD_ID = "ID";
//	private static final String FIELD_BOOK_ID = "BOOK_ID";
//	private static final String FIELD_PARENT_ID = "PARENT_ID";
//	private static final String FIELD_ISBN = "ISBN";
//	private static final String FIELD_TYPE = "TYPE";
//	private static final String FIELD_ELEMENT = "ELEMENT";
//	private static final String FIELD_AUTHOR = "AUTHOR";
//	private static final String FIELD_JOURNAL_TITLE = "JOURNAL_TITLE";
//	private static final String FIELD_ARTICLE_TITLE = "ARTICLE_TITLE";
//	private static final String FIELD_BOOK_TITLE = "BOOK_TITLE";
//	private static final String FIELD_VOLUME = "VOLUME";
//	private static final String FIELD_START_PAGE = "START_PAGE";
//	private static final String FIELD_YEAR = "YEAR";
//	private static final String FIELD_PUBLISHER_NAME = "PUBLISHER_NAME";
//	private static final String FIELD_PUBLISHER_LOC = "PUBLISHER_LOC";
//	private static final String FIELD_ISSUE = "ISSUE";
//	private static final String FIELD_CITATION = "CITATION";
//	private static final String FIELD_POSITION = "POSITION";
//	private static final String FIELD_TITLE = "TITLE";
//	private static final String FIELD_DOI = "DOI";
//	private static final String FIELD_ONLINE_DATE = "ONLINE_DATE";
//	private static final String FIELD_ABSTRACT = "ABSTRACT";
//	private static final String FIELD_KEYWORD = "KEYWORD";
//	
//	private static final String ELEMENT_AUTHOR = "author";
//	private static final String ELEMENT_JOURNAL_TITLE = "journal-title";
//	private static final String ELEMENT_ARTICLE_TITLE = "article-title";
//	private static final String ELEMENT_BOOK_TITLE = "book-title";
//	private static final String ELEMENT_VOLUME = "volume";
//	private static final String ELEMENT_START_PAGE = "startpage";
//	private static final String ELEMENT_YEAR = "year";
//	private static final String ELEMENT_PUBLISHER_NAME = "publisher-name";
//	private static final String ELEMENT_PUBLISHER_LOC = "publisher-loc";
//	private static final String ELEMENT_ISSUE = "issue";
//	private static final String ELEMENT_CONTENT_ITEM = "content-item";
//	private static final String ELEMENT_DOI = "doi";
//	private static final String ELEMENT_ONLINE_DATE = "online-date";
//	private static final String ELEMENT_KEYWORD = "keyword";
	
	public static String getBookIsbn(String bookId) {
		String result = "";
		List<BookMetadataDocument> docs = worker.searchBookCore("book_id:" + bookId + " AND content_type:book"); //IndexSearchUtil.searchIndex("ELEMENT:isbn AND BOOK_ID:" + bookId);
		if(docs != null && !docs.isEmpty())			
			result = docs.get(0).getIsbn();
		
		return result;
	}
	
	public static Map<String, String> getAllBooks() {
		Map<String, String> result = new LinkedHashMap<String, String>();
		List<BookMetadataDocument> docs = worker.searchBookCore("content_type:book"); //IndexSearchUtil.searchIndex("ELEMENT:isbn AND PARENT:metadata");
		for (BookMetadataDocument doc : docs) {
			result.put(doc.getBookId(), doc.getIsbn());
		}
		return result;
	}
	
//	public static List<Citation> getCitationListFromBookId(String bookId, String eisbn) {
//		List<Citation> citations = new ArrayList<Citation>();
//		
//		List<Document> docs = IndexSearchUtil.searchIndex("ELEMENT:citation AND BOOK_ID:" + bookId);
//		int c = 0;
//		System.out.print("\n[" + bookId + "] Processing lucene documents...");
//		for (Document doc : docs) {
//			
////			System.out.println("\n[" + bookId + "] [DOC:" + ++c + "] Processing lucene document...");
//			
//			Citation citation = new Citation();
//			
//			citation.setBookId(bookId);
//			citation.setBookEisbn(eisbn);
//			citation.setType(doc.get(FIELD_TYPE));
//			citation.setCitationId(doc.get(FIELD_ID));
//			citation.setParentId(doc.get(FIELD_PARENT_ID));
//			citation.setFullText(doc.get(FIELD_CITATION));
//			citation.setParentPosition(getParentPosition(citation.getParentId()));
//			
//			if (citation.forQuery()) {
//				citation = resolveCitationDetails(citation);
//			}
//			
////			System.out.print(" OK. Citation Id = " + citation.getCitationId() + " " + citation.getParentPosition());
//			
//			citations.add(citation);
//			c++;
//		}
//		System.out.println("Complete. " + c + " citations.");
//		
//		return citations;
//	}
//	
//	public static String getParentPosition(String parentId) {
//		List<Document> docs = IndexSearchUtil.searchIndex("ELEMENT:content-item AND ID:" + parentId);
//		
//		try {
//			return docs.get(0).get(FIELD_POSITION);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		return "";
//	}
//
//	public static Citation resolveCitationDetails(Citation citation) {
//		
//		List<Document> docs = IndexSearchUtil.searchIndex(
//				"(PARENT:citation AND PARENT_ID:" + citation.getCitationId() 
//				+ ") OR (ID:" + citation.getParentId() 
//				+ ") OR (BOOK_ID:" + citation.getBookId() 
//				+ " AND (ELEMENT:online-date OR ELEMENT:doi " 
//				+ "OR ELEMENT:content-item OR ELEMENT:keyword-group OR ELEMENT:keyword))");
//		List<String> authors = new ArrayList<String>();
//		List<String> keywords = new ArrayList<String>();
//		
//		for (Document doc : docs) {
//			String element = doc.get(FIELD_ELEMENT);
//			if (element.equals(ELEMENT_AUTHOR)) {
//				citation.setAuthor(doc.get(FIELD_AUTHOR));
//				authors.add(doc.get(FIELD_AUTHOR));
//			} else if (element.equals(ELEMENT_JOURNAL_TITLE)) {
//				citation.setJournalTitle(doc.get(FIELD_JOURNAL_TITLE));
//			} else if (element.equals(ELEMENT_ARTICLE_TITLE)) {
//				citation.setArticleTitle(doc.get(FIELD_ARTICLE_TITLE));
//			} else if (element.equals(ELEMENT_BOOK_TITLE)) {
//				citation.setBookTitle(doc.get(FIELD_BOOK_TITLE));
//			} else if (element.equals(ELEMENT_CONTENT_ITEM)) {				
//				if(doc.get(FIELD_TITLE) != null && !doc.get(FIELD_TITLE).equals("")) {
//					citation.setTitle(doc.get(FIELD_TITLE));
//				}
//				
//				if(doc.get(FIELD_ABSTRACT) != null 
//						&& !doc.get(FIELD_ABSTRACT).equals("") 
//						&& doc.get(FIELD_ID).equals(citation.getParentId())) {
//					citation.setAbstractText(doc.get(FIELD_ABSTRACT));
//				}
//			} else if (element.equals(ELEMENT_START_PAGE)) {
//				citation.setStartPage(doc.get(FIELD_START_PAGE));
//			} else if (element.equals(ELEMENT_VOLUME)) {
//				citation.setVolume(doc.get(FIELD_VOLUME));
//			} else if (element.equals(ELEMENT_YEAR)) {
//				citation.setYear(doc.get(FIELD_YEAR));
//			} else if (element.equals(ELEMENT_PUBLISHER_NAME)) {
//				citation.setPublisherName(doc.get(FIELD_PUBLISHER_NAME));
//			} else if (element.equals(ELEMENT_PUBLISHER_LOC)) {
//				citation.setPublisherName(doc.get(FIELD_PUBLISHER_LOC));
//			} else if (element.equals(ELEMENT_ISSUE)) {
//				citation.setIssue(doc.get(FIELD_ISSUE));
//			} else if (element.equals(ELEMENT_DOI)) {
//				citation.setDoi(doc.get(FIELD_DOI));
//			} else if (element.equals(ELEMENT_ONLINE_DATE)) {
//				citation.setPublishedOnlineDate(doc.get(FIELD_ONLINE_DATE));
//			} else if (element.equals(ELEMENT_KEYWORD)) {
//				keywords.add(doc.get(FIELD_KEYWORD));
//			} else {
//				// do nothing
////				System.out.println("[WARNING] Unused stored field: " + element);
//			}
//		}
//		
//		citation.setAuthors(authors);
//		citation.setKeywords(keywords);
//		
//		return citation;
//	}
}
