package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class QueryBatch {

	private String version;
	private String xmlns;
	private String xmlnsXsi;
	private Head head;
	private Body body;
	
	public QueryBatch(String version, String xmlns, String xmlnsXsi, Head head, Body body) {
		this.version = version;
		this.xmlns = xmlns;
		this.xmlnsXsi = xmlnsXsi;
		this.head = head;
		this.body = body;
	}
	
	public String getVersion() {
		return version;
	}
	public String getXmlns() {
		return xmlns;
	}
	public String getXmlnsXsi() {
		return xmlnsXsi;
	}

	public Head getHead() {
		return head;
	}

	public Body getBody() {
		return body;
	}
	
}
