package org.cambridge.ebooks.production.references.elements;

public interface Pattern {

	public static final String BOOK_ID =  "book/attribute::id";
	public static final String REFERENCES_XPATH =  "book/content-items/descendant::content-item/references";
	public static final String CITATION_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation";
}
