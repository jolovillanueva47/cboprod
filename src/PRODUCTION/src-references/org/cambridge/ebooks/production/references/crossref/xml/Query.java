package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Query {

	private String key;
	private String enableMultipleHits;
	private String forwardMatch;
	private JournalTitle journalTitle;
	private VolumeTitle volumeTitle;
	private SeriesTitle seriesTitle;
	private String author;
	private String volume;
	private String firstPage;
	private String year;
	
	public Query(
			String key,
			String enableMultipleHits,
			String forwardMatch,
			JournalTitle journalTitle,
			VolumeTitle volumeTitle,
			SeriesTitle seriesTitle,
			String author,
			String volume,
			String firstPage,
			String year
			) {
		this.key = key;
		this.enableMultipleHits = enableMultipleHits;
		this.forwardMatch = forwardMatch;
		this.journalTitle = journalTitle;
		this.volumeTitle = volumeTitle;
		this.seriesTitle = seriesTitle;
		this.author = author;
		this.volume = volume;
		this.firstPage = firstPage;
		this.year = year;
	}
	
	public String getKey() {
		return key;
	}
	public String getEnableMultipleHits() {
		return enableMultipleHits;
	}
	public String getForwardMatch() {
		return forwardMatch;
	}
	public JournalTitle getJournalTitle() {
		return journalTitle;
	}
	public String getAuthor() {
		return author;
	}
	public String getVolume() {
		return volume;
	}
	public String getFirstPage() {
		return firstPage;
	}
	public String getYear() {
		return year;
	}

	public VolumeTitle getVolumeTitle() {
		return volumeTitle;
	}

	public SeriesTitle getSeriesTitle() {
		return seriesTitle;
	}
	
	
}
