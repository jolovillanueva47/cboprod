package org.cambridge.ebooks.production.references.crossref;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.cambridge.ebooks.production.references.crossref.xml.Root;
import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.util.PropertiesManager;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.references.util.XPathWorker;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * @author jgalang
 * 
 */

public class CrossRefQuerySender implements ReferenceProperties {
	
	private static final String XPATH_STATUS = "/crossref_result/query_result/body/query/attribute::status";
	private static final String XPATH_DOI = "/crossref_result/query_result/body/query/doi/text()";
	private static final String XML_CONTENT_TYPE = "text/xml;charset=UTF-8";
	
	private static Properties iso2uni, uni2iso;
	static {
		iso2uni = PropertiesManager.getProperties(ISO_UNI_TABLE_PROPERTIES_FILE);
		StringWriter s = new StringWriter();
		iso2uni.list(new PrintWriter(s));
		
		uni2iso = new Properties();
		Enumeration<Object> e = iso2uni.keys();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			uni2iso.setProperty(iso2uni.getProperty(key), key);
		}
	}
	private static DocumentBuilderFactory factory = null;
	static {
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();
				//factory.setIgnoringComments(true);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public static Citation resolveCrossRefReference(Citation citation) {
		File file = processXmlQuery(citation);
		return postXmlQuery(citation, file);
	}
	
	public static File processXmlQuery(Citation citation) {
		File file = null;
		try {
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_DIR));

			Map<String, Root> root = new HashMap<String, Root>();
			Root xmlRoot = CrossRefXMLQueryWorker.getRoot(citation);
			root.put("root", xmlRoot);
			
			file = new File(
					XML_TMP_DIR
					+ citation.getBookEisbn()
					+ ".tmp.xml");
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
	                (new FileOutputStream(file),XML_ENCODING));
			
			Template xml = cfg.getTemplate(QUERY_TEMPLATE);
	        xml.process(root, bw);
	        
	        bw.close();
		} catch (IOException e) {
			System.err.println(e);
		} catch (TemplateException e) {
			System.err.println("bookId: " + citation.getBookId() + " citationId: " + citation.getCitationId() + " " + e);
		}
		return file;
	}
	
	public static String readFileAsString(File f) throws IOException {
		StringBuilder fileData = new StringBuilder();
        BufferedReader reader = new BufferedReader(
                new FileReader(f));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
	}
	
	public static Citation postXmlQuery(Citation citation, File f) {
		try {
			
			StringBuilder content = new StringBuilder(readFileAsString(f).replace(" & "," and "));
			
			// iso2uni
		    Pattern pattern = Pattern.compile("&([^#].+?);");
		    Matcher matcher = pattern.matcher(content);
		    while(matcher.find()) {
		    	String group1 = matcher.group(1);
		    	String replacement = "&#" + iso2uni.getProperty(group1, group1) + ";";
		    	String replaced = matcher.replaceFirst(replacement);
			    content = new StringBuilder(replaced);
			    matcher.reset(content);
		    }
		    System.out.println("\nCitation Id: "+ citation.getCitationId() + " Posting XML to CrossRef..."/* + content.toString()*/);
		    content = new StringBuilder(URLEncoder.encode( content.toString(), "utf-8"));
		    
		    
		    StringBuilder paramBody = new StringBuilder();
		    paramBody.append("usr=");
		    paramBody.append(USERNAME);
		    paramBody.append("&pwd=");
		    paramBody.append(PASSWORD);
		    paramBody.append("&format=");
		    paramBody.append(QUERY_FORMAT);
		    paramBody.append("&qdata=");
		    paramBody.append(content);
		    
		    URL url = new URL( "http", ORG_SITE, Integer.valueOf(ORG_PORT), QUERY_SITE);
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("POST");
		    conn.setAllowUserInteraction(false); 
	        conn.setDoOutput(true); // To send data
	        conn.setRequestProperty( "Content-type", "application/x-www-form-urlencoded" );
	        conn.setRequestProperty( "Content-length", Integer.toString(paramBody.length()));	
		    
	        // Get the output stream to POST form data
            OutputStream rawOutStream =  conn.getOutputStream();
            PrintWriter pw = new PrintWriter(rawOutStream);
            
            pw.print(paramBody);
            pw.flush();
            pw.close();
            
		    // Read Result from doi.crossref.org
            InputStream rawInStream = conn.getInputStream();
            
            
            if (conn.getContentType().equals(XML_CONTENT_TYPE)) {
            	
            	Document doc = factory.newDocumentBuilder().parse(rawInStream);
//              String resultStatus = XPathWorker.getXPath(doc, XPATH_STATUS);
            	String resultStatus = "";
                
                
                NodeList list = doc.getElementsByTagName("query");
                Node node1 = list.item(0);
                
                NamedNodeMap attributes = node1.getAttributes();
                resultStatus = attributes.getNamedItem("status").toString().replace("status=", "").replace("\"","");
                
                citation.setCrossRefStatus(resultStatus);
                
                if (resultStatus.equals(Citation.STATUS_RESOLVED)) {
                	String doi = ""; //XPathWorker.getXPath(doc, XPATH_DOI);
                	
                	NodeList list2 = doc.getElementsByTagName("doi");
                	Node node2 = list2.item(0);
                	doi = node2.getTextContent();        			
                	citation.setCrossRefDoi(doi);
                	citation.setCrossRefLink(REFERENCE_URL + doi);
                	System.out.println("Citation Id: "+ citation.getCitationId() + " Resolved DOI: " + doi);
                } else {
                	System.out.println("Citation Id: "+ citation.getCitationId() + " Unresolved reference. Status: " + resultStatus);
                }
                
            } else {
            	System.out.println("Response not an XML.");
            	System.out.println("Response Type: " + conn.getContentType());
//            	System.err.println("%%%%%%" + xmlQuery);
            	System.err.println("citationId: " + citation.getCitationId());
            	citation.setCrossRefStatus(Citation.STATUS_ERROR);
                BufferedReader br = new BufferedReader(new InputStreamReader(rawInStream));
                String line = br.readLine();
                while (line != null) {
                	System.out.println("******" + line);
                	line = br.readLine();
                }
                br.close();
            }
		    
		} catch (UnsupportedEncodingException e) {
			System.err.println("1: " + e);
		} catch (ProtocolException e) {
			System.err.println("2: " + e);
		} catch (MalformedURLException e) {
			System.err.println("3: " + e);
		} catch (IOException e) {
			System.err.println("4: " + e);
		} catch (ParserConfigurationException e) {
			System.err.println("5: " + e);
		} catch (SAXException e) {
			System.err.println("6: " + e);
		} finally {
			f.delete();
		}
		
		return citation;
	}
	
	private static void printFile(String path, String text) throws IOException {
		Writer output = null;	    
	    File file = new File(path);
	    output = new BufferedWriter(new FileWriter(file));
	    output.write(text);
	    output.close();
	    System.out.println("File "+ path +" has been written");  
	}
}
