package org.cambridge.ebooks.production.references.google.scholar;

import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.util.Misc;

public class GoogleScholarResolver {

	public static Citation resolveGoogleScholarLink(Citation c) {
		StringBuffer sb = new StringBuffer();
		
		//http://scholar.google.com/scholar?q=%22author%3AH+author%3AMast%22Mortality and causes of death after first ischemic stroke: the Northern Manhattan Stroke Study%22

		if (c.forQuery()) {

			if (c.getAuthors() != null && c.getAuthors().size() > 0) {
				sb.append("&author=");
				for (String s : c.getAuthors()) {
					sb.append(s);
				}
			}
			
			sb.append("&title=");
			if (c.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK) && Misc.isNotEmpty(c.getUnformattedBookTitle())) {
				sb.append(c.getUnformattedBookTitle());
			} else if (c.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
				if (Misc.isNotEmpty(c.getUnformattedArticleTitle())) {
					sb.append(c.getUnformattedArticleTitle());
				} else if (Misc.isNotEmpty(c.getUnformattedJournalTitle())) {
					sb.append(c.getUnformattedJournalTitle());
				}
			}
			
			if (Misc.isNotEmpty(c.getPublisherLoc())) {
				sb.append("&as_publication=");
				sb.append(c.getPublisherLoc());
			}
			
			if (Misc.isNotEmpty(c.getYear())) {
				sb.append("&as_ylo=");
				sb.append(c.getYear());
				sb.append("&as_yhi=");
				sb.append(c.getYear());
			}
			
			c.setGoogleScholarLink(escapeQuotes(sb.toString()));
			
		} else {
			sb.append("q=");
			sb.append(c.getFullText());
		}

		c.setGoogleScholarLink(escapeQuotes(sb.toString()));
		
		return c;
	}
	
	private static String escapeQuotes(String s) {
		if (s != null && s.length() > 0) {
			s = s.replace("'", "\\'").replace("\"", "\\\"");
		}
		return s;
	}
}
