package org.cambridge.ebooks.production.references.openurl;

import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.util.Misc;

public class OpenURLResolver {

	public static Citation resolveOpenURLLink(Citation c) {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("&amp;genre=");
		sb.append(c.getType());
		
		if (c.getType().equals("book")) {
			
			sb.append("&amp;title=");
			sb.append(convertApostrophe(c.getUnformattedBookTitle()));
			
		} else if (c.getType().equals("journal")) {
			
			sb.append("&amp;title=");
			sb.append(convertApostrophe(c.getUnformattedJournalTitle()));
			
		} else if (c.getType().equals("other")) { }
		
		
		if (Misc.isNotEmpty(c.getStartPage())) {	
			sb.append("&amp;spage=");
			sb.append(c.getStartPage());
		}
		
		if (Misc.isNotEmpty(c.getVolume())) {
			sb.append("&amp;volume=");
			sb.append(c.getVolume());
		}
		
		/*sb.append("&amp;isbn=");
		sb.append(c.getBookEisbn());
		sb.append("&amp;title=");
		sb.append(c.getUnformattedBookTitle());
		sb.append("&amp;TB_iframe=true&amp;width=730&amp;height=350");*/
		
		///ebooks/openUrlResolver.jsf?keepThis=true&amp;isbn=9780511482120&amp;title=Ancient%20Anger&amp;TB_iframe=true&amp;width=730&amp;height=350
		
		c.setOpenURLLink(sb.toString());
		
		return c;
	}
	
	public static String convertApostrophe(String str){
		String newStr = str;
		try {
			newStr.replaceAll("'", "\'");
		} catch (Exception e){
			System.out.println("error str = "+str);
			e.printStackTrace();
		}
		return newStr;
	}
}