package org.cambridge.ebooks.production.references.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.cambridge.util.Misc;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLDOMParser {

	private static DocumentBuilderFactory factory = null;
	static {
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();
				//factory.setIgnoringComments(true);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public static Document getXmlDOM(String filename) { 
		Document doc = null;
		File file = new File(filename);
		try { 
			doc = factory.newDocumentBuilder().parse(file);			
		} catch (IOException ioe) { 
			System.err.println("Exception... ");
		} catch (Exception exc) { 
			System.err.println(exc.getMessage());
		}
		return doc;
	}
	

	
	public static String getChildNodeTree(final String parentName, Node node){
		
		StringBuilder result = new StringBuilder();
		StringBuilder attributes = new StringBuilder();
		String root = null;
		
		if(node != null && node.hasChildNodes())
		{
			NodeList nodeList = node.getChildNodes();
			if( node.hasAttributes() && !excludeTags().contains(node.getNodeName()) )
			{
				NamedNodeMap nmap = node.getAttributes();
				for(int index=0; index<nmap.getLength(); index++)
				{
					Node attributeNode = nmap.item(index);
					if(attributeNode != null && !attributeNode.getNodeName().equals("xmlns:xml"))
					{
						attributes.append(" ").append(attributeNode.getNodeName()).append("=\"").append(attributeNode.getTextContent()).append("\"");						
					}

				}
			}
			
			root = node.getNodeName();
			
			for(int index=0; index<nodeList.getLength(); index++)
			{
				Node nodeFromList = nodeList.item(index);	
				if(nodeFromList != null )
				{
					
					if(!nodeFromList.hasChildNodes())
					{
						result.append(nodeFromList.getTextContent());
					}
					else
					{
						result.append(getChildNodeTree(parentName, nodeFromList));
					}
				}
			}
			
			if(root != null && !root.equalsIgnoreCase(parentName) && !excludeTags().contains(root))
			{
				result.insert(0, "[LTHAN]" + root + attributes.toString() + "[GTHAN]").append("[LTHAN]/" + root + "[GTHAN]");	
			}
			
		}
		
		if(node != null && node.getNodeName().equals(parentName))
		{
			result = new StringBuilder(convertToUTF8(result.toString()));
			result = new StringBuilder(result.toString().replace("[LTHAN]", "<").replace("[GTHAN]", ">"));
			result = new StringBuilder(correctHtmlTags(result.toString()));
		}
		
		return result.toString();
		
	}

	public static String correctHtmlTags(String text) {		
		String output = "";
		try {
			output = text.replaceAll("<italic>", "<i>")
			.replaceAll("</italic>", "</i>")
			.replaceAll("<block>", "<blockquote id=\"indent\">")
			.replaceAll("</block>", "</blockquote>")
			.replaceAll("<underline>", "<u>")
			.replaceAll("</underline>", "</u>")
			.replaceAll("<bold>", "<b>")
			.replaceAll("</bold>", "</b>")
			.replaceAll("<list-item>", "<li>")
			.replaceAll("</list-item>", "</li>")
			.replaceAll("<list", "<ul")
			.replaceAll("style=\"", "style=\"list-style:")
			.replaceAll("style=\"list-style:bullet", "style=\"padding: 20px 20px 20px 30px; list-style:disc !important;")
			.replaceAll("style=\"list-style:number", "style=\"padding: 20px 20px 20px 30px; list-style:decimal !important;")
			.replaceAll("style=\"list-style:none", "style=\"padding: 20px 20px 20px 30px; list-style:none !important;")
			.replaceAll("</list>", "</ul>")
			.replaceAll("<small-caps>", "<span style='font-variant:small-caps'>")
			.replaceAll("</small-caps>", "</span>")
			.replaceAll("<source>", "<p align=\"right\">")
			.replaceAll("</source>", "</p>");
		} catch (Exception e) {
			output = "";
		}
		
		return output;
	}
	
	public static List<String> excludeTags(){
		String[] tags = {"book-title", "journal-title", "chapter-title",
				         "volume", "issue", "article-title", "startpage",
				         "author", "name", "forenames", "surname", "name-link",
				         "suffix", "collab", "affiliation",	"publisher-name", 
				         "publisher-loc", "year" };
		
		return Arrays.asList(tags);
	}
	
	public static String cleanTextNode(String val) {
		if (Misc.isNotEmpty(val)) {
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll("\\s+", " ");  //.trim()
		}
		return val;
	}
	
	public static synchronized String convertToUTF8(String toUTF8){
		StringBuilder utf8 = new StringBuilder();
		if(toUTF8 != null && toUTF8.length() > 0) {
			utf8 = new StringBuilder(toUTF8);
			try {
				//if( !isUTF8(utf8.toString()) ) //removed because of UTF-8 checking issue on solaris
				//{
					boolean isDone = false;
					while(!isDone) {
						int index = 0;
						char[] c = utf8.toString().toCharArray();
						for(index=0; index<c.length; index++) {
							int intValue = (int)c[index];
							if(intValue == 38 || intValue == 60 || intValue == 62 || intValue > 127) { //0 -127 common, no need to check
								String stringValue = Character.toString(c[index]);
								String temp = new String(stringValue.getBytes("ISO-8859-1"), "UTF-8");
								if(temp != null) {
									if(temp.equals("?") || stringValue.equals("<") || stringValue.equals(">") || intValue > 160) { //160 up are special characters
										utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
										break;
									} else if(temp.equals("&")) {
										if((c.length == 1) || (index != c.length - 1 && c[index + 1] != '#') || 
										   (index == c.length - 1)) {
											utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
											break;
										}
										
									}
								}
							}
						}
						
						if(index == utf8.toString().length()) isDone = true;
					}
				//}
				
			} 
			catch (Exception e) {
				System.err.println("[Exception] convertToUTF8() " + e.getMessage());
			}
			
		}
		return utf8.toString();
	}
	
	public static List<Node> getChildNodes(Node node, final String nodeName){
		List<Node> nodes = new ArrayList<Node>();
		if(node != null && node.hasChildNodes()) {
			if(nodeName != null && nodeName.length() > 0) {
				NodeList nodeList = node.getChildNodes();
				for(int index=0; index<nodeList.getLength(); index++) {
					Node returnedNode = nodeList.item(index);
					if(returnedNode != null && nodeName.equalsIgnoreCase(returnedNode.getNodeName())) {
						nodes.add(returnedNode);
					}
				}
			}	
		}
		return nodes;
	}
	
	public static Node getParent(Node node, String parentName){
		if(node != null && node.getParentNode() != null ) {
			if(parentName != null && parentName.length() > 0) {
				Node parent = node.getParentNode();
				if(parent.getNodeName().equals(parentName)) {
					return parent;
				} else {
					return getParent(parent, parentName);
				}
			}
		}
		
		return null;
	}
	
	public static String getNodeTextContent(Node node){
		try {
			String content = node.getTextContent();
			return content;
		} catch(Exception ex) {
			return "";
		}
	}
	
}
