package org.cambridge.ebooks.production.references.jdbc;

/*
 * @author - alacerna
 * connection to database
 * **/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.cambridge.ebooks.production.references.util.ReferenceProperties;

public class JDBCUtil {
	
	public static Connection getConnection() {
		Connection connection = null; 
		try { 
			// Load the JDBC driver 
//			String driverName = "oracle.jdbc.driver.OracleDriver"; 
//			Class.forName(driverName);
//			
//			// Create a connection to the database			
//			String serverName = "ora-west-3.cup.cam.ac.uk"; 
//			String portNumber = "1521"; 
//			String sid = "t03satis"; 
//			String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid; 
//			String username = "oraebooks" ;
//			String password = "oraebooks"; 
			
			// Load the JDBC driver 
			String driverName = ReferenceProperties.JDBC_DRIVER;//"oracle.jdbc.driver.OracleDriver";
//			//String driverName = "oracle.jdbc.driver.OracleDriver";

			Class.forName(driverName);
//			
//			// Create a connection to the database			
			String url= ReferenceProperties.JDBC_URL;
			String username =  ReferenceProperties.JDBC_USER;//"username"; 
			String password =  ReferenceProperties.JDBC_PASS;//"password"; 
			System.out.println("Connecting to database.....");
			connection = DriverManager.getConnection(url, username, password); 
			System.out.println("Connectin Successful!!");	
			
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			e.printStackTrace();
		} catch (SQLException e) { 
			// Could not connect to the database
			e.printStackTrace();
		}		
		return connection;
	}
	

}
