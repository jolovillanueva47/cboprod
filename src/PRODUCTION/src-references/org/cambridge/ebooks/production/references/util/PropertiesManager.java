package org.cambridge.ebooks.production.references.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * 
 * @author jgalang
 * 
 */

public class PropertiesManager {
	
	public static Properties getProperties(String fileName) {
		Properties property = null;
		try {
			InputStream is = PropertiesManager.class.getResourceAsStream(fileName);
			property = new Properties();
			property.load(is);	
		} catch (Exception e) {
			System.err.println("Error : can't read the properties file. \n Make sure " +  fileName  + " is in the CLASSPATH");
		}
		return property;
	}

	public static String getProperty(String propertyName) {
		String result = null;
		try {
			InputStream is = PropertiesManager.class.getResourceAsStream(ReferenceProperties.REFERENCES_PROPERTIES_FILE);
			Properties property = new Properties();
			property.load(is);
			result = property.getProperty(propertyName);
			
			if (result == null || result.trim().length() < 1) {
				throw new IOException(("'" + propertyName + "' not found on the properties file."));
			}
		} catch (IOException e) {
			System.err.println(e);
		} catch (Exception e) {
			System.err.println("Error : can't read the properties file. \n Make sure " +  ReferenceProperties.REFERENCES_PROPERTIES_FILE  + " is in the CLASSPATH");
		}
		return result;
	}
	
	public static String getPropertyFile(){
		StringBuilder propertyFile = new StringBuilder();
		try {
			String hostname = InetAddress.getLocalHost().getHostName();
			propertyFile.append(hostname).append(".references.properties");
			
			System.out.println("Property File:" + propertyFile);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return propertyFile.toString();
	}
//	public static void main(String[] args) {
//		System.out.println(getPropertyFile());
//		String JDBC_DRIVER = getProperty("jdbc.driver");
//		System.out.println(JDBC_DRIVER);
//	}
}
