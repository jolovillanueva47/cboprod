package org.cambridge.ebooks.production.references.util;

import java.io.File;

import org.apache.commons.lang.StringUtils;

public class IsbnContentDirUtil {
	
	/**
	 * http://en.wikipedia.org/wiki/International_Standard_Book_Number
 	 * http://en.wikipedia.org/wiki/File:ISBN_Details.svg
	 */
	
	//private static final String CONTENT_DIR = System.getProperty("content.dir").trim();
	private static final String CONTENT_DIR = ReferenceProperties.CONTENT_DIR;
	private static final String SEPARATOR = File.separator;
	
	public static String getPath(String isbn) {
		
		String ean = isbn.substring(0, 3);
		String group = isbn.substring(3, 5);
		String publisher = isbn.substring(5, 9);
		String title = isbn.substring(9, 12);
		String checkDigit = isbn.substring(12, 13);
		
		String isbnPath = 
			getContentDir() + 
			ean + SEPARATOR + 
			group + SEPARATOR + 
			publisher + SEPARATOR + 
			title + SEPARATOR + 
			checkDigit + SEPARATOR;
		
		File f = new File(isbnPath);
		return f.isDirectory() ? isbnPath : getContentDir() + isbn + SEPARATOR;
	}
	
	private static String getContentDir() {
		String contentDir = CONTENT_DIR;		
		if(!StringUtils.endsWith(CONTENT_DIR, "/")) {
			contentDir = CONTENT_DIR + "/";
		}		
		return contentDir;
	}
}
