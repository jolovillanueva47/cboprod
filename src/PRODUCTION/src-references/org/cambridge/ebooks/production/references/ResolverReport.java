package org.cambridge.ebooks.production.references;


public class ResolverReport {

	private String batchName;
	private int crossRef;
	private int openUrl;
	private int googleBooks;
	private int googleScholar;
	private int book;
	private int journal;
	private int other;
	private long resolverStart;
	private long resolverEnd;

	public int getTotal() {
		return book + journal + other;
	}
	
	public void incrementCrossRef(){crossRef++;}
	public void incrementOpenUrl(){openUrl++;}
	public void incrementGoogleBooks(){googleBooks++;}
	public void incrementGoogleScholar(){googleScholar++;}
	public void incrementBook(){book++;}
	public void incrementJournal(){journal++;}
	public void incrementOther(){other++;}
	
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public int getCrossRef() {
		return crossRef;
	}
	public void setCrossRef(int crossRef) {
		this.crossRef = crossRef;
	}
	public int getOpenUrl() {
		return openUrl;
	}
	public void setOpenUrl(int openUrl) {
		this.openUrl = openUrl;
	}
	public int getGoogleBooks() {
		return googleBooks;
	}
	public void setGoogleBooks(int googleBooks) {
		this.googleBooks = googleBooks;
	}
	public int getGoogleScholar() {
		return googleScholar;
	}
	public void setGoogleScholar(int googleScholar) {
		this.googleScholar = googleScholar;
	}
	public int getBook() {
		return book;
	}
	public void setBook(int book) {
		this.book = book;
	}
	public int getJournal() {
		return journal;
	}
	public void setJournal(int journal) {
		this.journal = journal;
	}
	public int getOther() {
		return other;
	}
	public void setOther(int other) {
		this.other = other;
	}
	public long getResolverStart() {
		return resolverStart;
	}
	public void setResolverStart(long resolverStart) {
		this.resolverStart = resolverStart;
	}
	public long getResolverEnd() {
		return resolverEnd;
	}
	public void setResolverEnd(long resolverEnd) {
		this.resolverEnd = resolverEnd;
	}
	
	
}
