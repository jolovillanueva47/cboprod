package org.cambridge.ebooks.production.references.elements;

import java.util.List;

import org.cambridge.ebooks.production.references.util.ReferenceProperties;

/**
 * 
 * @author jgalang
 * 
 */

public class Citation implements Comparable<Citation> {
	
	public static final String STATUS_RESOLVED = "resolved";
	public static final String STATUS_ERROR = "error";

	private String citationId;
	private String bookId;
	private String parentId;
	private String parentPosition;
	private String bookEisbn;
	private String type;
	private String author;
	private String journalName;
	private String journalTitle;
	private String articleTitle;
	private String bookTitle;
	private String chapterTitle;
	private String title;
	private String unformattedArticleTitle;
	private String unformattedJournalTitle;
	private String unformattedBookTitle;
	private String unformattedChapterTitle;
	private String unformattedTitle;
	private String unformattedFullText;
	private String startPage;
	private String endPage;
	private String volume;
	private String year;
	private String publisherName;
	private String publisherLoc;
	private String publishedOnlineDate;
	private String issue;
	private String fullText;
	private	String issn;
	private String abstractText;
	private String doi;
	
	private String crossRefLink;
	private String crossRefStatus;
	private String crossRefDoi;
	
	private String openURLLink;
	private String googleScholarLink;
	private String googleBooksLink;
	
	private List<String> authors;
	private List<String> keywords;
	
	//alacerna - for DPB-4839
	private String cambridgeLInk;
	private String linkType;
	
	public int compareTo(Citation o) {
		return Integer.valueOf(this.parentPosition).compareTo(Integer.valueOf(o.parentPosition));
	}
	
	public boolean forQuery() {
		return type != null && type.length() > 0 && !type.equals(ReferenceProperties.CITATION_TYPE_OTHER);
	}

	public String getCitationId() {
		return citationId;
	}

	public void setCitationId(String citationId) {
		this.citationId = citationId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getBookEisbn() {
		return bookEisbn;
	}

	public void setBookEisbn(String bookEisbn) {
		this.bookEisbn = bookEisbn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getJournalTitle() {
		return journalTitle;
	}

	public void setJournalTitle(String journalTitle) {
		this.journalTitle = journalTitle;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getStartPage() {
		return startPage;
	}

	public void setStartPage(String startPage) {
		this.startPage = startPage;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublisherLoc() {
		return publisherLoc;
	}

	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getCrossRefLink() {
		return crossRefLink;
	}

	public void setCrossRefLink(String crossRefLink) {
		this.crossRefLink = crossRefLink;
	}

	public String getCrossRefStatus() {
		return crossRefStatus;
	}

	public void setCrossRefStatus(String crossRefStatus) {
		this.crossRefStatus = crossRefStatus;
	}

	public String getCrossRefDoi() {
		return crossRefDoi;
	}

	public void setCrossRefDoi(String crossRefDoi) {
		this.crossRefDoi = crossRefDoi;
	}

	public String getParentPosition() {
		return parentPosition;
	}

	public void setParentPosition(String parentPosition) {
		this.parentPosition = parentPosition;
	}

	public String getOpenURLLink() {
		return openURLLink;
	}

	public void setOpenURLLink(String openURLLink) {
		this.openURLLink = openURLLink;
	}

	public String getGoogleScholarLink() {
		return googleScholarLink;
	}

	public void setGoogleScholarLink(String googleScholarLink) {
		this.googleScholarLink = googleScholarLink;
	}

	public String getGoogleBooksLink() {
		return googleBooksLink;
	}

	public void setGoogleBooksLink(String googleBooksLink) {
		this.googleBooksLink = googleBooksLink;
	}

	public String getUnformattedJournalTitle() {
		return unformattedJournalTitle;
	}

	public void setUnformattedJournalTitle(String unformattedJournalTitle) {
		this.unformattedJournalTitle = unformattedJournalTitle;
	}

	public String getUnformattedArticleTitle() {
		return unformattedArticleTitle;
	}

	public void setUnformattedArticleTitle(String unformattedArticleTitle) {
		this.unformattedArticleTitle = unformattedArticleTitle;
	}

	public String getUnformattedBookTitle() {
		return unformattedBookTitle;
	}

	public void setUnformattedBookTitle(String unformattedBookTitle) {
		this.unformattedBookTitle = unformattedBookTitle;
	}

	public String getUnformattedTitle() {
		return unformattedTitle;
	}

	public void setUnformattedTitle(String unformattedTitle) {
		this.unformattedTitle = unformattedTitle;
	}

	public String getChapterTitle() {
		return chapterTitle;
	}

	public void setChapterTitle(String chapterTitle) {
		this.chapterTitle = chapterTitle;
	}

	public String getUnformattedChapterTitle() {
		return unformattedChapterTitle;
	}

	public void setUnformattedChapterTitle(String unformattedChapterTitle) {
		this.unformattedChapterTitle = unformattedChapterTitle;
	}

	/**
	 * @return the publishedOnlineDate
	 */
	public String getPublishedOnlineDate() {
		return publishedOnlineDate;
	}

	/**
	 * @param publishedOnlineDate the publishedOnlineDate to set
	 */
	public void setPublishedOnlineDate(String publishedOnlineDate) {
		this.publishedOnlineDate = publishedOnlineDate;
	}

	/**
	 * @return the authors
	 */
	public List<String> getAuthors() {
		return authors;
	}

	/**
	 * @param authors the authors to set
	 */
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	/**
	 * @return the keywords
	 */
	public List<String> getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the issn
	 */
	public String getIssn() {
		return issn;
	}

	/**
	 * @param issn the issn to set
	 */
	public void setIssn(String issn) {
		this.issn = issn;
	}

	/**
	 * @return the endPage
	 */
	public String getEndPage() {
		return endPage;
	}

	/**
	 * @param endPage the endPage to set
	 */
	public void setEndPage(String endPage) {
		this.endPage = endPage;
	}

	/**
	 * @return the abstractText
	 */
	public String getAbstractText() {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the journalName
	 */
	public String getJournalName() {
		return journalName;
	}

	/**
	 * @param journalName the journalName to set
	 */
	public void setJournalName(String journalName) {
		this.journalName = journalName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public String getUnformattedFullText() {
		return unformattedFullText;
	}

	public void setUnformattedFullText(String unformattedFullText) {
		this.unformattedFullText = unformattedFullText;
	}

	public void setCambridgeLInk(String cambridgeLInk) {
		this.cambridgeLInk = cambridgeLInk;
	}

	public String getCambridgeLInk() {
		return cambridgeLInk;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public String getLinkType() {
		return linkType;
	}

	
}
