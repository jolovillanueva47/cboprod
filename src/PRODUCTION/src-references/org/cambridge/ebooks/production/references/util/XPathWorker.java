package org.cambridge.ebooks.production.references.util;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XPathWorker {

	public static NodeList searchForNode(final Node xmlDom,final String xpathExpression) { 
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodes = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);
			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODESET);
			
			
			nodes = (NodeList) result;
			
			
		} catch (XPathExpressionException ex) { 
			
			System.err.println("oopss!" + ex.getMessage());
		}
		
		
		return nodes;
	}
	
	public static String getXPath(Document doc, String listItemXPath) {
		
		if(listItemXPath != null && !"".equals(listItemXPath)){
			NodeList nl = searchForNode(doc, listItemXPath);

			if (nl.getLength() > 0) {
				return nl.item(0).getNodeValue();
			}
		}
			
		return null;
	}
}
