package org.cambridge.ebooks.production.references.crossref;

import org.cambridge.ebooks.production.references.crossref.xml.Body;
import org.cambridge.ebooks.production.references.crossref.xml.Head;
import org.cambridge.ebooks.production.references.crossref.xml.JournalTitle;
import org.cambridge.ebooks.production.references.crossref.xml.Query;
import org.cambridge.ebooks.production.references.crossref.xml.QueryBatch;
import org.cambridge.ebooks.production.references.crossref.xml.Root;
import org.cambridge.ebooks.production.references.crossref.xml.SeriesTitle;
import org.cambridge.ebooks.production.references.crossref.xml.VolumeTitle;
import org.cambridge.ebooks.production.references.crossref.xml.Xml;
import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;

/**
 * 
 * @author jgalang
 * 
 */

public class CrossRefXMLQueryWorker implements ReferenceProperties{
	
	private static final String DEFAULT_ENABLE_MULTIPLE_HITS = "false";
	private static final String DEFAULT_FORWARD_MATCH = "true";
	private static final String DEFAULT_MATCH = "fuzzy";

	public static Root getRoot(Citation citation) {
		Root root = new Root(
				new Xml(XML_VERSION, XML_ENCODING),
				new QueryBatch(
						QUERYBATCH_VERSION,
						QUERYBATCH_XMLNS,
						QUERYBATCH_XMLNSXSI,
						getHead(citation),
						getBody(citation)
						)
				);
		return root;
	}
	
	public static Head getHead(Citation citation) {
		Head head = new Head(
				RESPONSE_EMAIL,
				citation.getBookEisbn()
				);
		return head;
	}
	
	public static Body getBody(Citation citation) {
		Body body = new Body(
				new Query(
						citation.getCitationId(),
						DEFAULT_ENABLE_MULTIPLE_HITS,
						DEFAULT_FORWARD_MATCH,
						getJournalTitle(citation),
						getVolumeTitle(citation),
						getSeriesTitle(citation),
						citation.getAuthor(),
						citation.getVolume(),
						citation.getStartPage(),
						citation.getYear()						
						)
				);
		return body;	
	}
	
	public static JournalTitle getJournalTitle(Citation citation) {
		JournalTitle journalTitle = null;
		String jTitle = citation.getUnformattedJournalTitle();
		String bTitle = citation.getUnformattedBookTitle();
		
		boolean hasTitle = !((jTitle == null || jTitle.trim().length() < 1) && (bTitle == null || bTitle.trim().length() < 1));
		if (hasTitle) {
			journalTitle = new JournalTitle(
					DEFAULT_MATCH,
					citation.getType().equals(CITATION_TYPE_JOURNAL) ? citation.getUnformattedJournalTitle() : citation.getUnformattedBookTitle()
					);	
		}
		return journalTitle;
	}
	
	public static VolumeTitle getVolumeTitle(Citation citation) {
		VolumeTitle volumeTitle = null;
		String bTitle = citation.getUnformattedBookTitle();
		
		boolean hasTitle = !(bTitle == null || bTitle.trim().length() < 1);
		if (hasTitle && citation.getType().equals(CITATION_TYPE_BOOK)) {
			volumeTitle = new VolumeTitle(
					DEFAULT_MATCH,
					citation.getUnformattedBookTitle()
					);
		}
		return volumeTitle;
	}
	
	public static SeriesTitle getSeriesTitle(Citation citation) {
		SeriesTitle seriesTitle = null;
		String bTitle = citation.getUnformattedBookTitle();
		
		boolean hasTitle = !(bTitle == null || bTitle.trim().length() < 1);
		if (hasTitle && citation.getType().equals(CITATION_TYPE_BOOK)) {
			seriesTitle = new SeriesTitle(
					DEFAULT_MATCH,
					citation.getUnformattedBookTitle()
					);
		}
		return seriesTitle;
	}
}
