package org.cambridge.ebooks.production.references;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.references.util.StringUtil;

/**
 * @author Karlson A. Mulingtapang
 * ExportCitation.java - Export Citation
 */
public class ExportCitation {

	private static final String[] EXPORT_CITATION_FORMATS = {"ASCII", 
		"Biblioscape", "BibTex", "CSV", "Endnote", "HTML", "Medlars", 
		"Papyrus", "ProCite", "ReferenceManager", "RefWorks", "RIS"} ;	
	
	private static final String WITH_ABSTRACT_SUFFIX = "_WA";
	private static final String WITHOUT_ABSTRACT_SUFFIX = "_WOA";
	
	private static BufferedWriter[] asciiWriter = new BufferedWriter[2];
	private static BufferedWriter[] biblioscapeWriter = new BufferedWriter[2];
	private static BufferedWriter[] bibTexWriter = new BufferedWriter[2];
	private static BufferedWriter csvWriter;
	private static BufferedWriter[] endnoteWriter = new BufferedWriter[2];
	private static BufferedWriter[] htmlWriter = new BufferedWriter[2];
	private static BufferedWriter[] medlarsWriter = new BufferedWriter[2];
	private static BufferedWriter[] papyrusWriter = new BufferedWriter[2];
	private static BufferedWriter[] proCiteWriter = new BufferedWriter[2];
	private static BufferedWriter[] referenceManagerWriter = new BufferedWriter[2];
	private static BufferedWriter[] refWorksWriter = new BufferedWriter[2];
	private static BufferedWriter risWriter;
	
	public static void createExportCitationFile(Citation citation, String filePathWithoutExtension) { 			
		if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK) 
				|| citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
		
			for(int i = 0; i < EXPORT_CITATION_FORMATS.length ; i++) {
				try {
					buildFile(EXPORT_CITATION_FORMATS[i], 
							citation, 
							filePathWithoutExtension,
							1,
							1,
							1);
				} catch (IOException e) {
					e.printStackTrace();
				}			
			}
		}
	}
	
	public static void createExportCitationForAllReference(List<Citation> citations, String filePathWithoutExtension) {
		int citationCount = 1;
		int bookJournalCount = 1;
		int citationSize = citations.size();
		for(Citation citation : citations) {			
			if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK) 
					|| citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
				
				for(int i = 0; i < EXPORT_CITATION_FORMATS.length ; i++) {
					try {
						buildFile(EXPORT_CITATION_FORMATS[i], 
								citation, 
								filePathWithoutExtension,
								citationCount,
								bookJournalCount,
								citationSize);
					} catch (IOException e) {
						e.printStackTrace();
					}			
				}
				bookJournalCount++;
			}
			citationCount++;
		}
	}
	
	private static void buildFile(String fileFormat, 
			Citation citation, String filePathWithoutExtension, int citationCount, int bookJournalCount, int citationSize)
		throws IOException {
		
		String extension = "";
		String delimeter = "";
//		BufferedWriter out = null;
		if(fileFormat.equals("ASCII")) {
			extension = ".txt";
			delimeter = ",";		
			
			if(bookJournalCount == 1) {
				asciiWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-ASCII" + extension));
				
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-ASCII" + extension));				
			}
			asciiFile(delimeter, true, asciiWriter[0], citation);
			if(citationCount == citationSize) {
				asciiWriter[0].flush();
				asciiWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				asciiWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-ASCII" + extension));
				            
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-ASCII" + extension));				
			}
			asciiFile(delimeter, false, asciiWriter[1], citation);
			if(citationCount == citationSize) {
				asciiWriter[1].flush();
				asciiWriter[1].close();
			}
		} else if(fileFormat.equals("Biblioscape")) {		
			extension = ".txt";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				biblioscapeWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-Biblioscape" + extension));
//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
//					+ WITH_ABSTRACT_SUFFIX + "-Biblioscape" + extension));				
			}
			biblioscapeFile(delimeter, true, biblioscapeWriter[0], citation);
			if(citationCount == citationSize) {
				biblioscapeWriter[0].flush();
				biblioscapeWriter[0].close();
			}
			if(bookJournalCount == 1) {
				biblioscapeWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-Biblioscape" + extension));
				
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-Biblioscape" + extension));
			}
			biblioscapeFile(delimeter, false, biblioscapeWriter[1], citation);
			if(citationCount == citationSize) {
				biblioscapeWriter[1].flush();
				biblioscapeWriter[1].close();
			}
		} else if(fileFormat.equals("BibTex")) {			
			extension = ".bib";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				bibTexWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-BibTex" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-BibTex" + extension));
			}
			bibTexFile(delimeter, true, bibTexWriter[0], citation);
			if(citationCount == citationSize) {
				bibTexWriter[0].flush();
				bibTexWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				bibTexWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-BibTex" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-BibTex" + extension));
			}
			bibTexFile(delimeter, false, bibTexWriter[1], citation);
			if(citationCount == citationSize) {
				bibTexWriter[1].flush();
				bibTexWriter[1].close();
			}
		} else if(fileFormat.equals("CSV")) {			
			extension = ".csv";
			delimeter = ",";
			
			if(bookJournalCount == 1) {
				csvWriter = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-CSV" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-CSV" + extension));				
			}
			csvFile(delimeter, csvWriter, citation);
			if(citationCount == citationSize) {
				csvWriter.flush();
				csvWriter.close();
			}
		} else if(fileFormat.equals("Endnote")) {		
			extension = ".enw";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				endnoteWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-Endnote" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-Endnote" + extension));
			}
			endNoteFile(delimeter, true, endnoteWriter[0], citation);
			if(citationCount == citationSize) {
				endnoteWriter[0].flush();
				endnoteWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				endnoteWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-Endnote" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-Endnote" + extension));
			}
			endNoteFile(delimeter, false, endnoteWriter[1], citation);
			if(citationCount == citationSize) {
				endnoteWriter[1].flush();
				endnoteWriter[1].close();
			}
		} else if(fileFormat.equals("HTML")) {			
			extension = ".htm";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				htmlWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-HTML" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-HTML" + extension));				
			}
			HTMLFile(delimeter, true, htmlWriter[0], citation);
			if(citationCount == citationSize) {
				htmlWriter[0].flush();
				htmlWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				htmlWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-HTML" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-HTML" + extension));				
			}
			HTMLFile(delimeter, false, htmlWriter[1], citation);
			if(citationCount == citationSize) {
				htmlWriter[1].flush();
				htmlWriter[1].close();		
			}
		} else if(fileFormat.equals("Medlars")) {
			extension = ".txt";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				medlarsWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-Medlars" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-Medlars" + extension));				
			}
			medlarsFile(delimeter, true, medlarsWriter[0], citation);
			if(citationCount == citationSize) {
				medlarsWriter[0].flush();
				medlarsWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				medlarsWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-Medlars" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-Medlars" + extension));				
			}
			medlarsFile(delimeter, false, medlarsWriter[1], citation);
			if(citationCount == citationSize) {
				medlarsWriter[1].flush();
				medlarsWriter[1].close();
			}
		} else if(fileFormat.equals("Papyrus")) {			
			extension = ".txt";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				papyrusWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-Papyrus" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-Papyrus" + extension));				
			}
			papyrusFile(delimeter, true, papyrusWriter[0], citation);
			if(citationCount == citationSize) {
				papyrusWriter[0].flush();
				papyrusWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				papyrusWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-Papyrus" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-Papyrus" + extension));				
			}
			papyrusFile(delimeter, false, papyrusWriter[1], citation);
			if(citationCount == citationSize) {
				papyrusWriter[1].flush();
				papyrusWriter[1].close();
			}
		} else if(fileFormat.equals("ProCite")) {
			extension = ".ris";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				proCiteWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-ProCite" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-ProCite" + extension));				
			}
			proCiteFile(delimeter, true, proCiteWriter[0], citation);
			if(citationCount == citationSize) {
				proCiteWriter[0].flush();
				proCiteWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				proCiteWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-ProCite" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-ProCite" + extension));				
			}
			proCiteFile(delimeter, false, proCiteWriter[1], citation);
			if(citationCount == citationSize) {
				proCiteWriter[1].flush();
				proCiteWriter[1].close();
			}
		} else if(fileFormat.equals("ReferenceManager")) {
			extension = ".ris";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				referenceManagerWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-ReferenceManager" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-ReferenceManager" + extension));				
				
			}
			refManFile(delimeter, true, referenceManagerWriter[0], citation);
			if(citationCount == citationSize) {
				referenceManagerWriter[0].flush();
				referenceManagerWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				referenceManagerWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-ReferenceManager" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-ReferenceManager" + extension));				
			}
			refManFile(delimeter, false, referenceManagerWriter[1], citation);
			if(citationCount == citationSize) {
				referenceManagerWriter[1].flush();
				referenceManagerWriter[1].close();
			}
		} else if(fileFormat.equals("RefWorks")) {
			extension = ".txt";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				refWorksWriter[0] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITH_ABSTRACT_SUFFIX + "-RefWorks" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITH_ABSTRACT_SUFFIX + "-RefWorks" + extension));				
			}
			refWorksFile(delimeter, true, refWorksWriter[0], citation);
			if(citationCount == citationSize) {
				refWorksWriter[0].flush();
				refWorksWriter[0].close();
			}
			
			if(bookJournalCount == 1) {
				refWorksWriter[1] = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-RefWorks" + extension));
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-RefWorks" + extension));				
			}
			refWorksFile(delimeter, false, refWorksWriter[1], citation);
			if(citationCount == citationSize) {
				refWorksWriter[1].flush();
				refWorksWriter[1].close();
			}
		} else if(fileFormat.equals("RIS")) {
			extension = ".ris";
			delimeter = "\n";
			
			if(bookJournalCount == 1) {
				risWriter = new BufferedWriter(new FileWriter(filePathWithoutExtension 
						+ WITHOUT_ABSTRACT_SUFFIX + "-RIS" + extension));			
	//			out = new BufferedWriter(new FileWriter(filePathWithoutExtension 
	//					+ WITHOUT_ABSTRACT_SUFFIX + "-RIS" + extension));					
			}
			risFile(delimeter, risWriter, citation);			
			if(citationCount == citationSize) {
				risWriter.flush();
				risWriter.close();
			}
		}
	}
	
	private static void asciiFile(String delimeter, boolean displayAbstract, 
			BufferedWriter out, Citation citation) throws IOException {
                
        // TYPE
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("TY - BOOK" + delimeter);            
            // Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            }    
        	out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	
        	// Publisher
            out.write("PB - " + getData(citation.getPublisherName(), "Publisher", false) + delimeter);
            
            // Online Publication Date
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
            // Start Page
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            
            // Abstract
            if(displayAbstract) {  
            	out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	        	
            }        
            
            if(StringUtil.isNotEmpty(citation.getBookId())) {	// Web URL
    	        out.write("UR - http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);        
            }
            out.write("ER - " + delimeter);
            out.write("\n\n");            
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("TY - JOUR" + delimeter);            
            // Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            }    
        	out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	
        	// Online Publication Date
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
            // Volume
    		out.write("VL - " + getData(citation.getVolume(), "Volume", false) + delimeter);
    		
            // Issue      
    		out.write("IS - " + getData(citation.getIssue(), "Issue", false) + delimeter);
    		
    		// Start Page
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            
//            // End Page
//            if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
//            	out.write("EP  - " + getData(citation.getEndPage(), "End Page", false) + delimeter);
//            }        
            
            // Abstract
            if(displayAbstract) {  
            	out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	        	
            }        
            
            if(StringUtil.isNotEmpty(citation.getBookId())) {	// Web URL
    	        out.write("UR - http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);        
            }
            out.write("ER - " + delimeter);
            out.write("\n\n");
        } 
    }
	
	private static void biblioscapeFile(String delimeter, boolean displayAbstract, 
		BufferedWriter out, Citation citation) throws IOException {
                
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("--RT-- eBook" + delimeter);            
        	out.write("--TI-- " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            // Authors       
        	for(String author : getList(citation.getAuthors(), "Authors")){            
        		out.write("--AU-- " + getData(author, "Author", false) + delimeter);
        	}  
        	out.write("--YP-- " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);

            // Start Page
            out.write("--PS-- " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            
            // Abstract
            if(displayAbstract) {        	
        		out.write("--AB-- " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	
            } 
            
            out.write("\n\n");
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("--RT-- Journal Article" + delimeter);            
        	out.write("--TI-- " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            // Authors       
        	for(String author : getList(citation.getAuthors(), "Authors")){            
        		out.write("--AU-- " + getData(author, "Author", false) + delimeter);
        	}  
        	out.write("--YP-- " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
        	// Volume
            out.write("--VL-- " + getData(citation.getVolume(), "Volume", false) + delimeter);

            // Issue      
            out.write("--NB-- " + getData(citation.getIssue(), "Issue", false) + delimeter);
            
            // Start Page
            out.write("--PS-- " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            
            // Abstract
            if(displayAbstract) {        	
        		out.write("--AB-- " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	
            } 
            
            out.write("\n\n");
        }
    }
	
	private static void bibTexFile(String delimeter, boolean displayAbstract,
			BufferedWriter out,	Citation citation) throws IOException {
                
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("@book{," + delimeter);            

            // Authors
            out.write("author = {");        
            int authorCounter = 0;
            for(String author : getList(citation.getAuthors(), "Authors")){
            	if(authorCounter == 0) {
            		out.write(getData(author, "Author", false));
            	} else {
            		out.write(" and " + getData(author, "Author", false));
            	}
            	authorCounter++;
            }
            
            out.write("}," + delimeter);        
            out.write("title = {" + StringUtil.removeHTMLTags(getData(citation.getTitle(), "Book Title", false) 
            		+ "}," + delimeter)); 
            
            out.write("book = {" + getData(citation.getBookTitle(), "Book Title", false) + "}," + delimeter);  
        	out.write("publisher = {" + getData(citation.getPublisherName(), "Publisher Name", false) + "}," + delimeter);
        	out.write("startPage = {" + getData(citation.getStartPage(), "Start Page", false) + "}," + delimeter);
        	out.write("year = {" + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + "}," + delimeter);
            out.write("doi = {" + getData(citation.getDoi(), "DOI", false) + "}," + delimeter);
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("url = {http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + "}," + delimeter);
            }
            // Abstract
            if(displayAbstract) {     
            	out.write("abstract = { " + getData(citation.getAbstractText(), "Abstract", false) + " }" + delimeter);        	
            } 
            
            out.write("}\n");
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("@article{," + delimeter);

            // Authors
            out.write("author = {");        
            int authorCounter = 0;
            for(String author : getList(citation.getAuthors(), "Authors")){
            	if(authorCounter == 0) {
            		out.write(getData(author, "Author", false));
            	} else {
            		out.write(" and " + getData(author, "Author", false));
            	}
            	authorCounter++;
            	
            	out.write("}," + delimeter);        
                out.write("title = {" + StringUtil.removeHTMLTags(getData(citation.getTitle(), "Book Title", false) 
                		+ "}," + delimeter)); 
            }
            
            out.write("journal = {" + getData(citation.getJournalTitle(), "Journal Name", false) + "}," + delimeter);
        	
        	// Volume
            out.write("volume = {" + getData(citation.getVolume(), "Volume", false) + "}," + delimeter);
            
            // Issue      
            out.write("number = {" + getData(citation.getIssue(), "Issue", false) + "}," + delimeter);
            out.write("startPage = {" + getData(citation.getStartPage(), "Start Page", false) + "}," + delimeter);	
            out.write("year = {" + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + "}," + delimeter);
            out.write("doi = {" + getData(citation.getDoi(), "DOI", false) + "}," + delimeter);
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("url = {http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + "}," + delimeter);
            }
            // Abstract
            if(displayAbstract) {     
            	out.write("abstract = { " + getData(citation.getAbstractText(), "Abstract", false) + " }" + delimeter);        	
            } 
            
            out.write("}\n");
        }  
    }
	
	private static void csvFile(String delimeter, BufferedWriter out, Citation citation)
		throws IOException {
		
        // Source        
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	// Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("\"" + getData(author, "Author", false) + "\"" + delimeter);        	
            }   
            
            // Title        	
            out.write("\""+((getData(citation.getTitle(), "Book Title", false).indexOf("\"")==-1) 
            		? getData(citation.getTitle(), "Book Title", false) 
            		: getData(citation.getTitle(), "Book Title", false).replaceAll("\"","\"\"")) + "\"" + delimeter);
            
            // Published online date
            out.write(getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
        	// Title
        	out.append(getData(citation.getBookTitle(), "Book Title", false) + delimeter);
            
            // Publisher
            out.write(getData(citation.getPublisherName(), "Publisher", false) + delimeter);
            
            out.write("pp " + getData(citation.getStartPage(), "Start Page", false) + delimeter);  
            
            // DOI
            out.write(getData(citation.getDoi(), "DOI", false) + delimeter);
            
            out.write("\n");   
            
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	// Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("\"" + getData(author, "Author", false) + "\"" + delimeter);        	
            }   
            
            // Title        	
            out.write("\""+((getData(citation.getTitle(), "Book Title", false).indexOf("\"")==-1) 
            		? getData(citation.getTitle(), "Book Title", false) 
            		: getData(citation.getTitle(), "Book Title", false).replaceAll("\"","\"\"")) + "\"" + delimeter);
            
            // Published online date
            out.write(getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
        	// Journal name
            out.append(getData(citation.getJournalTitle(), "Journal Name", false) + delimeter); 
            
            out.append(getData(citation.getVolume(), "Volume", false) + delimeter); 
            
            out.append(getData(citation.getIssue(), "Issue", false) + delimeter); 

            // Pages
            out.write("pp " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
        	
        	// DOI
            out.write(getData(citation.getDoi(), "DOI", false) + delimeter);  
            
            out.write("\n");   
        }              
    }
	
	private static void endNoteFile(String delimeter, boolean displayAbstract, 
			BufferedWriter out, Citation citation) throws IOException {
                
        // Title
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {        	
        	out.write("%0 eBook" + delimeter);   
        	out.write("%B " + getData(citation.getBookTitle(), "Book Title", false) + delimeter);            
            out.write("%I " + getData(citation.getPublisherName(), "Publisher", false) + delimeter);
            // Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("%A " + getData(author, "Author", false) + delimeter);        	
            }
            
            out.write("%D " + getData(citation.getYear(), "Year", false) + delimeter);
            
            // Title
            out.write("%T " + StringUtil.removeHTMLTags(getData(citation.getTitle(), "Book Title", false)) + delimeter);
            out.write("%P " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            if(displayAbstract) {     	
        		out.write("%X " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            if(StringUtil.isNotEmpty(citation.getBookId())) {
    	        // URL
    	        out.write("%U http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            out.write("%[ " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
            out.write("\n\n"); 
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("%0 Journal Article" + delimeter);
        	out.write("%J " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("%A " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("%D " + getData(citation.getYear(), "Year", false) + delimeter);
            // Title
            out.write("%T " + StringUtil.removeHTMLTags(getData(citation.getTitle(), "Book Title", false)) + delimeter);
            // Volume
            out.write("%V " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue
            out.write("%N " + getData(citation.getIssue(), "Issue", false) + delimeter);
            
            out.write("%P " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            if(displayAbstract) {     	
        		out.write("%X " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            if(StringUtil.isNotEmpty(citation.getBookId())) {
    	        // URL
    	        out.write("%U http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            out.write("%[ " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            
            out.write("\n\n"); 
        }     
    }
	
	private static void HTMLFile(String delimeter, boolean displayAbstract, 
			BufferedWriter out, Citation citation) throws IOException {
                
        out.write("<html>" + delimeter);
        out.write("<head>" + delimeter);
        
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {   
        	// Page Title        	
        	out.write("<title>"+ getData(citation.getTitle(), "Book Title", false) +"</title>" + delimeter);
            out.write("</head>" + delimeter);
            out.write("<body bgcolor=\"#FFFFFF\" text=\"#000000\">" + delimeter);
            // Title
            out.write("<p><b>Title:</b> "+ getData(citation.getTitle(), "Book Title", false) +" <br/>");        
            out.write("<b>Author(s):</b>");
            // Authors        
            int authorCounter = 0;
            for(String author : getList(citation.getAuthors(), "Authors")){
            	if(authorCounter == 0) {
            		out.write(getData(author, "Author", false));
            	} else {
            		out.write("<br/>" + getData(author, "Author", false));
            	}
            	authorCounter++;
            }
            out.write("<br/>" + delimeter);
            out.write("<b>Publication date:</b> "+ getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) +" <br/>");
        	out.write("<b>Publisher:</b> " + getData(citation.getPublisherName(), "Publisher Name", false) + "<br/>" + delimeter);
        	out.write("<b>Publication:</b> " + getData(citation.getPublisherLoc(), "Publication", false) + "<br/>" + delimeter);
        	// Page
            out.write("<b>Start page:</b>" + getData(citation.getStartPage(), "Start Page", false) + "<br/>" + delimeter);
            if(StringUtil.isNotEmpty(citation.getBookId())) {
    	        out.write("<b>URL:</b> <a href=\"http://ebooks.cambridge.org/ebook.jsf?bid=" 
    	        		+ citation.getBookId() + "\">http://ebooks.cambridge.org/ebook.jsf?bid=" 
    	        		+ citation.getBookId() + "</a></p>");
            }
            if(displayAbstract) { 
        		out.write("<p><b>Abstract:</b> <p>" + getData(citation.getAbstractText(), "Abstract", false) 
        				+ "</p></p>" + delimeter);
            }
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	// Page Title        	
        	out.write("<title>"+ getData(citation.getTitle(), "Book Title", false) +"</title>" + delimeter);            
            out.write("</head>" + delimeter);
            out.write("<body bgcolor=\"#FFFFFF\" text=\"#000000\">" + delimeter);
            // Title
            out.write("<p><b>Title:</b> "+ getData(citation.getTitle(), "Book Title", false) +" <br/>");        
            out.write("<b>Author(s):</b>");
            // Authors        
            int authorCounter = 0;
            for(String author : getList(citation.getAuthors(), "Authors")){
            	if(authorCounter == 0) {
            		out.write(getData(author, "Author", false));
            	} else {
            		out.write("<br/>" + getData(author, "Author", false));
            	}
            	authorCounter++;
            }
            out.write("<br/>" + delimeter);
            out.write("<b>Publication date:</b> "+ getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) +" <br/>");
            
        	// Volume
            out.write("<b>Volume:</b> " + getData(citation.getVolume(), "Volume", false) + "<br/>" + delimeter);
            
            // Issue
            out.write("<b>Issue:</b> " + getData(citation.getIssue(), "Issue", false) + "<br/>" + delimeter);
            // Page
            out.write("<b>Start page:</b>" + getData(citation.getStartPage(), "Start Page", false) + "<br/>" + delimeter);
            if(StringUtil.isNotEmpty(citation.getBookId())) {
    	        out.write("<b>URL:</b> <a href=\"http://ebooks.cambridge.org/ebook.jsf?bid=" 
    	        		+ citation.getBookId() + "\">http://ebooks.cambridge.org/ebook.jsf?bid=" 
    	        		+ citation.getBookId() + "</a></p>");
            }
            if(displayAbstract) { 
        		out.write("<p><b>Abstract:</b> <p>" + getData(citation.getAbstractText(), "Abstract", false) 
        				+ "</p></p>" + delimeter);
            }
        }  else {
        	out.write("</head>" + delimeter);
            out.write("<body bgcolor=\"#FFFFFF\" text=\"#000000\">" + delimeter);
        }
        
        out.write("</body>" + delimeter);
        out.write("</html>" + delimeter);
    }
	
	private static void medlarsFile(String delimeter, boolean displayAbstract,
    	BufferedWriter out, Citation citation) throws IOException {
                
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {  
        	// Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            } 
            
            // Title
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	out.write("PT - eBook" + delimeter);
        	out.write("DP - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
        	out.write("TA - " + getData(citation.getBookTitle(), "Book Title", false) + delimeter);
        	out.write("PG - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
        	out.write("AID - " + getData(citation.getDoi(), "DOI", false) + " [doi]" + delimeter);
        	if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("4099 - http://ebooks.cambridge.org/ebook.jsp?bid=" + citation.getBookId() + delimeter);
            }
        	out.write("SO - " + getData(citation.getBookTitle(), "Book Title", false) + " " 
        			+ getData(citation.getPublisherName(), "Publisher Name", false) + "(" 
        			+ getData(citation.getPublisherLoc(), "Publisher Location", false) + "):" 
        			+ getData(citation.getStartPage(), "Start Page", false) + delimeter);
        	// Abstract
            if(displayAbstract) {     	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	     	
            } 
            
            out.write("\n\n");   
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	// Authors       
            for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            } 
            
            // Title
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	out.write("PT - Journal Article" + delimeter);
        	out.write("DP - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);        	
        	out.write("TA - " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
            
            // Volume
            out.write("VI - " + getData(citation.getVolume(), "Volume", false) + delimeter);

            // Issue
            out.write("IP - " + getData(citation.getIssue(), "Issue", false) + delimeter);
            
            out.write("PG - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
        	out.write("AID - " + getData(citation.getDoi(), "DOI", false) + " [doi]" + delimeter);
        	if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("4099 - http://ebooks.cambridge.org/ebook.jsp?bid=" + citation.getBookId() + delimeter);
            }
        	out.write("SO - " + getData(citation.getJournalTitle(), "Journal Name", false) + " " 
        			+ getData(citation.getVolume(), "Volume", false) + "(" 
        			+ getData(citation.getIssue(), "Issue", false) + "):" 
        			+ getData(citation.getStartPage(), "Start Page", false) + delimeter);
        	// Abstract
            if(displayAbstract) {     	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	     	
            } 

            out.write("\n\n");    
        }             
    }
	
	private static void papyrusFile(String delimeter, boolean displayAbstract,
    		BufferedWriter out, Citation citation) throws IOException {        
        
        // Type
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("TY - BOOK" + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("A1 - " + getData(author, "Author", false) + delimeter);        	
            }        
            
            out.write("T1 - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            out.write("Y1 - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);            
            // Abstract
            if(displayAbstract) {             	
            	out.write("N1 - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            out.write("ER - " + delimeter);
            out.write("\n\n");
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("TY - JOUR" + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("A1 - " + getData(author, "Author", false) + delimeter);        	
            }        
            
            out.write("T1 - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            out.write("JO - " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
            out.write("Y1 - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            // Volume
            out.write("VL - " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue
            out.write("IS - " + getData(citation.getIssue(), "Issue", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);           
            // Abstract
            if(displayAbstract) {             	
            	out.write("N1 - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            out.write("ER - " + delimeter);
            out.write("\n\n");
        }
    }
	
	private static void proCiteFile(String delimeter, boolean displayAbstract,
    		BufferedWriter out, Citation citation)
    	throws IOException {
        
        // Type
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("TY - BOOK" + delimeter);        	
        	out.write("PB - " + getData(citation.getPublisherName(), "Publisher Name", false) + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("AU - " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            // Date
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);            
            // Abstract
            if(displayAbstract) {           	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	
            }
            
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("UR - http://ebooks.cambridge.org/ebook.jsp?bid=" + citation.getBookId() + delimeter);
            }        
            out.write("ER - " + delimeter);
            out.write("\n\n");
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("TY - JOUR" + delimeter);
        	out.write("JO - " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("AU - " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            // Date
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            // Volume
            out.write("VL - " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue
            out.write("IS - " + getData(citation.getIssue(), "Issue", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
           
            // Abstract
            if(displayAbstract) {           	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);        	
            }
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("UR - http://ebooks.cambridge.org/ebook.jsp?bid=" + citation.getBookId() + delimeter);
            }        
            out.write("ER - " + delimeter);
            out.write("\n\n");
        }  
    }
	
	private static void refManFile(String delimeter, boolean displayAbstract,
    		BufferedWriter out, Citation citation) throws IOException {
        		
        // Type
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("TY - BOOK" + delimeter);
            out.write("PB - " + getData(citation.getPublisherName(), "Publisher Name", false) + delimeter);
            // Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("AU - " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);            
            // Abstract
            if(displayAbstract) {          	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("UR - http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            
            out.write("ER - " + delimeter);
            out.write("\n\n");
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("TY - JOUR" + delimeter);
        	out.write("JO - " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
        	// Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("AU - " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            // Volume
            out.write("VL - " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue
            out.write("IS - " + getData(citation.getIssue(), "Issue", false) + delimeter);
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);            
            // Abstract
            if(displayAbstract) {          	
        		out.write("AB - " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("UR - http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            
            out.write("ER - " + delimeter);
            out.write("\n\n");
        } 
    }
    
    private static void refWorksFile(String delimeter, boolean displayAbstract, 
    		BufferedWriter out, Citation citation) throws IOException {
        
        // Type
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("RT eBook" + delimeter);
        	out.write("T1 " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	
        	if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        		out.write("PB " + getData(citation.getPublisherName(), "Publisher Name", false) + delimeter);
            }   
            
            // Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("A1 " + getData(author, "Author", false) + delimeter);        	
            }
            
            out.write("YR " + getData(citation.getYear(), "Year", false) + delimeter);
            out.write("SP " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            // Abstract
            if(displayAbstract) {     
            	out.write("AB " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("LK http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            out.write("RD " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            out.write("\n\n");   
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {        	
        	out.write("RT Journal" + delimeter);
            out.write("JF " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
            out.write("T1 " + getData(citation.getTitle(), "Book Title", false) + delimeter);
        	
        	if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        		out.write("PB " + getData(citation.getPublisherName(), "Publisher Name", false) + delimeter);
            }   
            
            // Authors        
            for(String author : getList(citation.getAuthors(), "Authors")){        	
            	out.write("A1 " + getData(author, "Author", false) + delimeter);        	
            }
            out.write("YR " + getData(citation.getYear(), "Year", false) + delimeter);
            // Volume
            out.write("VO " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue
            out.write("IS " + getData(citation.getIssue(), "Issue", false) + delimeter);
            out.write("SP " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            // Abstract
            if(displayAbstract) {     
            	out.write("AB " + getData(citation.getAbstractText(), "Abstract", false) + delimeter);
            }
            if(StringUtil.isNotEmpty(citation.getBookId())) {
            	out.write("LK http://ebooks.cambridge.org/ebook.jsf?bid=" + citation.getBookId() + delimeter);
            }
            out.write("RD " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);
            out.write("\n\n");   
        }
    }
	
	private static void risFile(String delimeter, BufferedWriter out, Citation citation) throws IOException {      
                
        // Type
        if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_BOOK)) {
        	out.write("TY - BOOK" + delimeter);
        	// Authors
       	    for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            }
            
            // Date Primary / Published Year
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);     
            
            // Title
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
//            // Keywords        
//    		for(String keyword : getList(citation.getKeywords(), "Keywords")) {
//    			out.write("KW - " + keyword + delimeter);
//    		}
    		// Start Page
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);
            out.write("ER  - " + delimeter);
        } else if(citation.getType().equals(ReferenceProperties.CITATION_TYPE_JOURNAL)) {
        	out.write("TY - JOUR" + delimeter);
        	// Authors
       	    for(String author : getList(citation.getAuthors(), "Authors")){            
                out.write("AU - " + getData(author, "Author", false) + delimeter);
            }
            
            // Date Primary / Published Year
            out.write("PY - " + getData(citation.getPublishedOnlineDate(), "Online Publication Date", false) + delimeter);     
            
            // Title
            out.write("TI - " + getData(citation.getTitle(), "Book Title", false) + delimeter);
            out.write("JF - " + getData(citation.getJournalTitle(), "Journal Name", false) + delimeter);
//            // Keywords        
//    		for(String keyword : getList(citation.getKeywords(), "Keywords")) {
//    			out.write("KW - " + keyword + delimeter);
//    		}
    		// Start Page
            out.write("SP - " + getData(citation.getStartPage(), "Start Page", false) + delimeter);            
            // Volume
            out.write("VL - " + getData(citation.getVolume(), "Volume", false) + delimeter);
            
            // Issue      
            out.write("IS  - " + getData(citation.getIssue(), "Issue", false) + delimeter);
            out.write("ER  - " + delimeter);
        }
    }	
	
	private static String getData(String data, String dataName, boolean hasDataName) {
		if(StringUtil.isEmpty(data)) {
			return "No " + dataName;
		} else {
			if(hasDataName) {
				return dataName + " " + data;
			} else {
				return data;
			}
		}
	}
	
	private static List<String> getList(List<String> list, String dataName) {
		if(list == null) {
			List<String> data = new ArrayList<String>();
			data.add("No " + dataName);
			return data;
		} else {
			return list;
		}
	}
}
