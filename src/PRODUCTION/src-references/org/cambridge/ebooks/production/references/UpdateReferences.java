package org.cambridge.ebooks.production.references;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.references.util.DataSource;

public class UpdateReferences {
	public static void main(String arg[]) {
		
		System.out.println("start main");
		Connection con = DataSource.getConnection();
		try {
			Statement stmt = con.createStatement();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM  ");
			sql.append("ebook ");
			sql.append("WHERE status = 7 ORDER BY isbn");
			ResultSet rs = stmt.executeQuery(sql.toString());
			List<String> isbnList = new ArrayList<String>();

			isbnList.add("Date: "+ String.valueOf(System.currentTimeMillis()));
			
			int i = 0;
			while (  rs != null && rs.next() ) { 
				String isbn = rs.getString( "ISBN" );
				isbnList.add(isbn);
				i++;
			}
			//System.out.println("total="+i);
	
			ReferenceResolver.main(isbnList.toArray(new String[isbnList.size()]));
						
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally{
			DataSource.closeConnection(con);	
		}
		
	}

}


