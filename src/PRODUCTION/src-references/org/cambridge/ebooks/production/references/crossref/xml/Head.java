package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Head {

	private String emailAddress;
	private String doiBatchId;
	
	public Head(String emailAddress, String doiBatchId) {
		this.emailAddress = emailAddress;
		this.doiBatchId = doiBatchId;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getDoiBatchId() {
		return doiBatchId;
	}
	
	
}
