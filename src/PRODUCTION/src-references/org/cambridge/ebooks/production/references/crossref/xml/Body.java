package org.cambridge.ebooks.production.references.crossref.xml;


/**
 * 
 * @author jgalang
 * 
 */

public class Body {

	private Query query;

	public Body(Query query) {
		this.query = query;
	}
	
	public Query getQuery() {
		return query;
	}
	
}
