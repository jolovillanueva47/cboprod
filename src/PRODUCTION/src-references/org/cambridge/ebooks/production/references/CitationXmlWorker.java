package org.cambridge.ebooks.production.references;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.references.elements.Pattern;
import org.cambridge.ebooks.production.references.elements.References;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.references.util.XMLDOMParser;
import org.cambridge.ebooks.production.references.util.XPathWorker;
import org.cambridge.ebooks.production.references.util.IsbnContentDirUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * 
 * @author jgalang
 * 
 */

public class CitationXmlWorker implements ReferenceProperties {

	public static Map<String, List<References>> getReferencesMap(String bookId, String eisbn) {
		
//		Document doc = XMLDOMParser.getXmlDOM(CONTENT_DIR + eisbn + File.separator + eisbn + ".xml");
		Document doc = XMLDOMParser.getXmlDOM(IsbnContentDirUtil.getPath(eisbn) + File.separator + eisbn + ".xml");
		
		return getReferencesPerContent(doc, bookId, eisbn);
	}
	
	public static Map<String, List<References>> getReferencesPerContent(Node xmlDom, String bookId, String eisbn) {
		Map<String, List<References>> result = null;
		
		NodeList nodeList = XPathWorker.searchForNode(xmlDom, Pattern.REFERENCES_XPATH);
		if (nodeList != null && nodeList.getLength() > 0) {
			
			result = new LinkedHashMap<String, List<References>>();
			
			for(int index=0; index<nodeList.getLength(); index++) {
				
				References root = processMetadata(nodeList.item(index), eisbn);
				root = processReferencesNode(nodeList.item(index), bookId, eisbn, root, root);
				
				if (result.get(root.getParentId()) == null) {
					List<References> newList = new ArrayList<References>();
					newList.add(root);
					result.put(root.getParentId(), newList);
				} else {
					result.get(root.getParentId()).add(root);
				}
				
			}
			
		}
		
		return result;
	}
	
	private static References processReferencesNode(Node node, String bookId, String eisbn, References reference, References root) {
		
		if (node != null && reference != null && "references".equalsIgnoreCase(node.getNodeName())) {
			
			reference.setBookId(bookId);
			
			
			if (node.hasAttributes()) {
				NamedNodeMap nmap = node.getAttributes();
				reference.setTypeAttribute(XMLDOMParser.getNodeTextContent(nmap.getNamedItem("type")));
			}
			
			if (node.hasChildNodes()) {
				
				NodeList children = node.getChildNodes();
				for (int childIndex=0; childIndex<children.getLength(); childIndex++) {
					Node child = children.item(childIndex);
					String nodeName = child.getNodeName();
					if ("references".equals(nodeName)) {
						
						if (reference.getSubReferences() == null) reference.setSubReferences(new ArrayList<References>());
						References newRef = new References();
						reference.getSubReferences().add(processReferencesNode(child, bookId, eisbn, newRef, root));
						
					} else if ("title".equals(nodeName)) {
						
						reference.setReferenceTitle(child.getTextContent());
						
					} else if ("citation".equals(nodeName)) {

						if (root.getAllCitations() == null) root.setAllCitations(new ArrayList<Citation>());
						if (reference.getCitations() == null) reference.setCitations(new ArrayList<Citation>());

						Citation citation = processCitationNode(child, bookId);
						citation.setBookEisbn(eisbn);
						citation.setParentId(root.getParentId());
						citation.setParentPosition(root.getParentPosition());
						citation.setAbstractText(root.getContentAbstract());
						citation.setTitle(root.getBookTitle());
						citation.setDoi(root.getContentDoi());
						citation.setPublishedOnlineDate(root.getPublishedOnlineDate());
						citation.setKeywords(root.getKeywords());
						
						reference.getCitations().add(citation);
						root.getAllCitations().add(citation);
						
					}
				}
				
			}
			
		}
		
		return reference;
	}
	
	public static References processMetadata(Node child, String eisbn) {
		References references = new References();
		
		references.setEisbn(eisbn);
		
		Node parent = XMLDOMParser.getParent(child, "content-item");
		if (parent != null && "content-item".equalsIgnoreCase(parent.getNodeName())) {
			
			references = processBookMetadata(references, parent);
			
			if (parent.hasAttributes()) {
				NamedNodeMap nmap = parent.getAttributes();
				references.setParentId(XMLDOMParser.getNodeTextContent(nmap.getNamedItem("id")));
				references.setParentPosition(XMLDOMParser.getNodeTextContent(nmap.getNamedItem("position")));
			}
			
			if (parent.hasChildNodes()) {
				
				NodeList contentMetadata = parent.getChildNodes();
				for (int item=0; item<contentMetadata.getLength(); item++) {
					
					Node node = contentMetadata.item(item);
					String nodeName = node.getNodeName();
					
					/*if ("abstract".equalsIgnoreCase(nodeName)) {
						references.setContentAbstract(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("abstract", node)));
						System.out.println(references.getContentAbstract());
					} else*/ if ("doi".equalsIgnoreCase(nodeName)) {
						references.setContentDoi(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("doi", node)));
					}
					
				}
				
				references.setKeywords(new ArrayList<String>());
				NodeList keywords = XPathWorker.searchForNode(parent, "keyword-group/descendant::kwd-text");
				for (int kItem=0; kItem<keywords.getLength(); kItem++) {
					references.getKeywords().add(keywords.item(kItem).getNodeValue());
				}
				
			}
			
		}
		
		return references;
	}
	
	private static References processBookMetadata(References references, Node xmlDom) {
		
		Node root = XMLDOMParser.getParent(xmlDom, "book");
		if (root != null && root.hasChildNodes()) {
			
			NodeList childNodes = root.getChildNodes();
			for (int item=0; item<childNodes.getLength(); item++) {
				
				if ("metadata".equals(childNodes.item(item).getNodeName())) {
					
					Node metadata = childNodes.item(item);
					if (metadata.hasChildNodes()) {
						NodeList metadataNodeList = metadata.getChildNodes();
						for (int mIndex=0; mIndex<metadataNodeList.getLength(); mIndex++) {
							
							Node node = metadataNodeList.item(mIndex);
							String nodeName = node.getNodeName();
							
							if (nodeName != null && "main-title".equalsIgnoreCase(nodeName)) {
								
								references.setBookTitle(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("main-title", node)));
								references.setUnformattedBookTitle(XMLDOMParser.getNodeTextContent(node));
								
							} else if (nodeName != null && "pub-dates".equalsIgnoreCase(nodeName)) {
								if (node.hasChildNodes()) {
									for (int pIndex=0; pIndex<node.getChildNodes().getLength(); pIndex++) {
										Node pubDate = node.getChildNodes().item(pIndex);
										if ("online-date".equalsIgnoreCase(pubDate.getNodeName())) {
											references.setPublishedOnlineDate(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("online-date", pubDate)));
											break;
										}
									}
								}
							}
							
						}
						
					}
					
					break;
				}
				
			}
		}
		return references;
	}
	
	private static Citation processCitationNode(Node node, String bookId) {
		Citation citation = null;
		
		if (node != null && "citation".equalsIgnoreCase(node.getNodeName())) {
			
			citation = new Citation();
			citation.setBookId(bookId);
			citation.setFullText(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("citation", node)));
			
			if (node.hasAttributes()) {
				NamedNodeMap nmap = node.getAttributes();
				citation.setCitationId(XMLDOMParser.getNodeTextContent(nmap.getNamedItem("id")));
				citation.setType(XMLDOMParser.getNodeTextContent(nmap.getNamedItem("type")));
			}
			
			if (node.hasChildNodes()) {
				
				NodeList children = node.getChildNodes();
				for (int childIndex=0; childIndex<children.getLength(); childIndex++) {
					
					Node child = children.item(childIndex);
					String nodeName = child.getNodeName();
					
					if ("author".equalsIgnoreCase(nodeName)) {
						
						List<String> authors = processAuthorNode(child);
						if (authors != null && authors.size() > 0) {
							citation.setAuthor(authors.get(0));
							citation.setAuthors(authors);
						}
						
					} else if ("book-title".equalsIgnoreCase(nodeName)) {
						
						citation.setBookTitle(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("book-title", child)));
						citation.setUnformattedBookTitle(XMLDOMParser.getNodeTextContent(child));
						
					} else if ("journal-title".equalsIgnoreCase(nodeName)) {
						
						citation.setJournalTitle(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("journal-title", child)));
						citation.setUnformattedJournalTitle(XMLDOMParser.getNodeTextContent(child));
												
					} else if ("chapter-title".equalsIgnoreCase(nodeName)) {
						
						citation.setChapterTitle(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("chapter-title", child)));
						citation.setUnformattedChapterTitle(XMLDOMParser.getNodeTextContent(child));
						
					} else if ("volume".equalsIgnoreCase(nodeName)) {
						
						citation.setVolume(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("volume", child)));
						
					} else if ("issue".equalsIgnoreCase(nodeName)) {
						
						citation.setIssue(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("issue", child)));
						
					} else if ("article-title".equalsIgnoreCase(nodeName)) {
						
						citation.setArticleTitle(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("article-title", child)));
						citation.setUnformattedArticleTitle(XMLDOMParser.getNodeTextContent(child));
						
					} else if ("startpage".equalsIgnoreCase(nodeName)) {
						
						citation.setStartPage(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("startpage", child)));
						
					} else if ("endpage".equalsIgnoreCase(nodeName)) {
						
						citation.setEndPage(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("endpage", child)));
						
					} else if ("publisher-name".equalsIgnoreCase(nodeName)) {
						
						citation.setPublisherName(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("publisher-name", child)));
						
					} else if ("publisher-loc".equalsIgnoreCase(nodeName)) {
						
						citation.setPublisherLoc(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("publisher-loc", child)));
						
					} else if ("year".equalsIgnoreCase(nodeName)) {
						
						citation.setYear(XMLDOMParser.cleanTextNode(XMLDOMParser.getChildNodeTree("year", child)));
						
					}
					
				}
			}
			
		}
		
		return citation;
	}
	
	private static List<String> processAuthorNode(Node node) {
		ArrayList<String> result = null;
		
		if(node.hasChildNodes()){
		
			result = new ArrayList<String>();			
			
			NodeList childNodes = node.getChildNodes();
			for(int i=0; i<childNodes.getLength(); i++) {
				Node cnode = childNodes.item(i);
				
				if(cnode != null && "name".equalsIgnoreCase(cnode.getNodeName())) {
					StringBuilder author = new StringBuilder();
					
					List<Node> namelinks = XMLDOMParser.getChildNodes(cnode, "name-link");	
					for(int ctr=0; ctr<namelinks.size(); ctr++) {
						Node namelink = namelinks.get(ctr);
						author.append(namelink.getTextContent()).append(" ");
					}
					
					List<Node> surnames = XMLDOMParser.getChildNodes(cnode, "surname");	
					for(int ctr=0; ctr<surnames.size(); ctr++) {
						Node surname = surnames.get(ctr);
						author.append(surname.getTextContent()).append(" ");
					}
					
					List<Node> suffixes = XMLDOMParser.getChildNodes(cnode, "suffix");	
					for(int ctr=0; ctr<suffixes.size(); ctr++) {
						Node suffix = suffixes.get(ctr);
						author.append(suffix.getTextContent()).append(" ");
					}
					
					List<Node> forenames = XMLDOMParser.getChildNodes(cnode, "forenames");
					StringBuilder fn = new StringBuilder();
					for(int ctr=0; ctr<forenames.size(); ctr++) {
						Node forename = forenames.get(ctr);
						fn.append(forename.getTextContent()).append(" ");
					}
					
					if((!namelinks.isEmpty() || !surnames.isEmpty() || !suffixes.isEmpty()) && !forenames.isEmpty())
						author.insert(author.length() -1, fn.insert(0, ", "));
					
					result.add(XMLDOMParser.convertToUTF8(XMLDOMParser.cleanTextNode(author.toString())));
					
				}
			}
			
		}
		return result;
	}
	
}