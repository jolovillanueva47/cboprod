package org.cambridge.ebooks.production.references.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Root {

	private Xml xml;
	private QueryBatch queryBatch;
	
	public Root(Xml xml, QueryBatch queryBatch) {
		this.xml = xml;
		this.queryBatch = queryBatch;
	}
	
	public Xml getXml() {
		return xml;
	}
	public QueryBatch getQueryBatch() {
		return queryBatch;
	}
	
	
}
