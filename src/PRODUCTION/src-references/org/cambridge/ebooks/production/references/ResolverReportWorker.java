package org.cambridge.ebooks.production.references;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.cambridge.ebooks.production.references.util.DateDisplayUtil;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class ResolverReportWorker implements ReferenceProperties {

	public static void generateReport(ResolverReport report) {
		
		report.setResolverEnd(System.currentTimeMillis());
		long elapsedTime = report.getResolverEnd() - report.getResolverStart();
		DateDisplayUtil elapsed = new DateDisplayUtil(elapsedTime);
		
		StringBuilder sb = new StringBuilder("\nBATCH: " + report.getBatchName() + " finished in:");
		if (elapsed.getDays() > 0) {
			sb.append(elapsed.getDays());
			sb.append(elapsed.getDays()>1 ? " days, " : " day, ");
		}
		if (elapsed.getHours() > 0) {
			sb.append(elapsed.getHours());
			sb.append(elapsed.getHours()>1 ? " hours, " : " hour, ");
		}
		
		sb.append(elapsed.getMinutes());
		sb.append(elapsed.getMinutes()>1 ? " minutes, " : " minute, ");
		
		sb.append("and " + elapsed.getSeconds());
		sb.append(elapsed.getSeconds()>1 ? " seconds." : " second.");			
		
		try {
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_DIR));
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("report", report);
			
			File f = new File(REFERENCES_DIR + report.getBatchName());
						
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
	                (new FileOutputStream(f),XML_ENCODING));			
			
			Template xml = cfg.getTemplate(RESOLVER_REPORT_TEMPLATE);
	        xml.process(root, bw);
			
	        System.out.println("Generated report: " + f.getName());
			
		} catch (IOException e) {
			System.err.println(e);
		} catch (TemplateException e) {
			System.err.println("Template Exception: " + e);
		}
		
		
		
		System.out.println(sb.toString());
		
	}
}
