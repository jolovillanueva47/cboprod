package org.cambridge.ebooks.production.trail.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table ( name = "AUDIT_TRAIL_EBOOKS" )
public class EBookAudit {
	
	@Id
	@GeneratedValue(generator="EBOOK_AUDIT_TRAIL_SEQ")
    @SequenceGenerator(name="EBOOK_AUDIT_TRAIL_SEQ", sequenceName="EBOOK_AUDIT_TRAIL_SEQ", allocationSize=1)
	@Column( name = "AUDIT_EBOOKS_ID")
	private int auditEbooksId;

	@Column( name = "STATUS")
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date auditDate;
	
	@Column( name = "EISBN")	
	private String eisbn;
	
	@Column( name = "SERIES_CODE")
	private String seriesCode;
	
	@Column( name = "TITLE")
	private String title;
	
	@Column( name = "USERNAME")
	private String username;
	
	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public String getSeriesId() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public int getAuditEbooksId() {
		return auditEbooksId;
	}
	
	public void setAuditEbooksId(int auditEbooksId) {
		this.auditEbooksId = auditEbooksId;
	}

	public String getStatus() {
		return Status.getStatus(Integer.parseInt(status)).getDescription();
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}
