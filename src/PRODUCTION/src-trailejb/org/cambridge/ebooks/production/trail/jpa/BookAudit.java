package org.cambridge.ebooks.production.trail.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table ( name = "AUDIT_TRAIL_BOOKS" )
public class BookAudit {
	
	@Id
	@GeneratedValue(generator="BOOK_AUDIT_TRAIL_SEQ")
    @SequenceGenerator(name="BOOK_AUDIT_TRAIL_SEQ", sequenceName="BOOK_AUDIT_TRAIL_SEQ", allocationSize=1)
	@Column( name = "AUDIT_ID")
	private int auditId;

	@Column( name = "CHAPTER_ID")
	private String chapterId;
	
	@Column( name = "CONTENT_ID")
	private String contentId;
	
	@Column( name = "STATUS")
	private String status;
	
	@Column( name = "REMARKS")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date auditDate;
	
	@Column( name = "EISBN")	
	private String eisbn;
	
	@Column( name = "SERIES_CODE")
	private String seriesCode;
	
	@Column( name = "FILENAME")
	private String filename;
	
	@Column( name = "LOAD_TYPE")
	private String loadType;
	
	@Column( name = "USERNAME")
	private String username;
	
	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public int getAuditId() {
		return auditId;
	}
	
	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}

	public String getChapterId() {
		return chapterId;
	}

	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	
	public String getStatus() {
		return Status.getStatus(Integer.parseInt(status)).getDescription();
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getLoadType() {
		return loadType;
	}

	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}

	
	
}
