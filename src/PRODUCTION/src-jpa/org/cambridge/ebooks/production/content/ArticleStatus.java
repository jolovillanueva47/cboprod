package org.cambridge.ebooks.production.content;

import java.util.LinkedHashMap;

//import org.cambridge.production.exception.InvalidArticleStatusException;
//import org.cambridge.production.user.AccessRights;

public class ArticleStatus {

	public static final int PRE_LOAD				 	= -1;
	public static final int PRE_PROCESS				 	= -2;
	public static final int PROCESS				 		= -3;
	public static final int LOAD				 		= -4;
	public static final int LOADED_FOR_PROOFREADING 	= 0;
	public static final int RELOADED_WITH_CORRECTIONS 	= 1;
	public static final int PROOFREAD_ACCEPTED 			= 2;
	public static final int PROOFREAD_WITH_ERROR 		= 3;
	public static final int APPROVED 					= 4;
	public static final int REVIEWED_WITH_ERROR 		= 5;
	public static final int RELOADED_FOR_APPROVER 		= 6;
	public static final int PUBLISHED 					= 7;
	public static final int UNPUBLISHED 				= 8;
	public static final int EMBARGO		 				= 9;
	public static final int DELETED						= 100;

	public static final String S_PRE_LOAD				 	= String.valueOf(PRE_LOAD);
	public static final String S_PRE_PROCESS				= String.valueOf(PRE_PROCESS);
	public static final String S_PROCESS				 	= String.valueOf(PROCESS);
	public static final String S_LOAD				 		= String.valueOf(LOAD);
	public static final String S_LOADED_FOR_PROOFREADING 	= String.valueOf(LOADED_FOR_PROOFREADING);
	public static final String S_RELOADED_WITH_CORRECTIONS 	= String.valueOf(RELOADED_WITH_CORRECTIONS);
	public static final String S_PROOFREAD_ACCEPTED 		= String.valueOf(PROOFREAD_ACCEPTED);
	public static final String S_PROOFREAD_WITH_ERROR 		= String.valueOf(PROOFREAD_WITH_ERROR);
	public static final String S_APPROVED 					= String.valueOf(APPROVED);
	public static final String S_REVIEWED_WITH_ERROR 		= String.valueOf(REVIEWED_WITH_ERROR);
	public static final String S_RELOADED_FOR_APPROVER 		= String.valueOf(RELOADED_FOR_APPROVER);
	public static final String S_PUBLISHED 					= String.valueOf(PUBLISHED);
	public static final String S_UNPUBLISHED 				= String.valueOf(UNPUBLISHED);
	public static final String S_EMBARGO	 				= String.valueOf(EMBARGO);
	public static final String S_DELETED 					= String.valueOf(DELETED);

	public static final String T_PRE_LOAD				 	= "Pre-Load";
	public static final String T_PRE_PROCESS				= "Pre-Process";
	public static final String T_PROCESS				 	= "Process";
	public static final String T_LOAD				 		= "Load";
	public static final String T_LOADED_FOR_PROOFREADING 	= "Loaded for Proofreading";
	public static final String T_RELOADED_WITH_CORRECTIONS 	= "Reloaded w/ Corrections";
	public static final String T_PROOFREAD_ACCEPTED 		= "Proofread Accepted";
	public static final String T_PROOFREAD_WITH_ERROR 		= "Proofread w/ Error";
	public static final String T_APPROVED 					= "Approved";
	public static final String T_REVIEWED_WITH_ERROR 		= "Reviewed w/ Error";
	public static final String T_RELOADED_FOR_APPROVER 		= "Reloaded for Approver";
	public static final String T_PUBLISHED 					= "Live";
	public static final String T_UNPUBLISHED 				= "Unpublished";
	public static final String T_EMBARGO 					= "Embargo";
	public static final String T_DELETED 					= "Deleted";

	private int status;
	private int newStatus;
	private LinkedHashMap validStatuses=new LinkedHashMap();
	
	public ArticleStatus() {
	}
	
	public ArticleStatus(int s) {
		status = s;
	}

	public ArticleStatus(String s) {
		status = Integer.parseInt(s);
	}

	public LinkedHashMap getValidStatuses() {
		return validStatuses;
	}

//	public void setValidStatuses(AccessRights a) {
//		switch (getStatus()) {
//			case LOADED_FOR_PROOFREADING:
//				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//				if (a.getCanProofread()) {
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//				
//			case RELOADED_WITH_CORRECTIONS:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//				}
//				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//				if (a.getCanProofread()) {
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//
//			case PROOFREAD_WITH_ERROR:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//				}
//				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//				if (a.getCanProofread()) {
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//
//			case PROOFREAD_ACCEPTED:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//				}
//				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//
//			case APPROVED:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				validStatuses.put(T_APPROVED, S_APPROVED);
//				validStatuses.put(T_EMBARGO, S_EMBARGO);
//				if (a.getCanApprove()) {
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//
//			case EMBARGO:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				validStatuses.put(T_APPROVED, S_APPROVED);
//				validStatuses.put(T_EMBARGO, S_EMBARGO);
//				if (a.getCanApprove()) {
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//				
//			case REVIEWED_WITH_ERROR:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//				}
//				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//				if (a.getCanApprove()) {
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				break;
//
//			case RELOADED_FOR_APPROVER:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//				}
//				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				break;
//
//
//			case PUBLISHED:
//				validStatuses.put(T_PUBLISHED, S_PUBLISHED);
//				break;
//
//			case UNPUBLISHED:
//				if (a.getCanProofread()) {
//					validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
//					validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
//					validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
//					validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
//				}
//				if (a.getCanApprove()) {
//					validStatuses.put(T_APPROVED, S_APPROVED);
//					validStatuses.put(T_EMBARGO, S_EMBARGO);
//					validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
//					validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
//				}
//				validStatuses.put(T_UNPUBLISHED, S_UNPUBLISHED);
//				break;			
//		}
//	}

	public void setPreLoadStatus() {
		switch (getStatus()) {
			default:

			case DELETED:
				validStatuses.put(T_DELETED, S_DELETED);
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;

			case LOADED_FOR_PROOFREADING:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;

			case RELOADED_WITH_CORRECTIONS:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;

			case PROOFREAD_WITH_ERROR:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				this.setStatus(ArticleStatus.RELOADED_WITH_CORRECTIONS);
				break;

			case PROOFREAD_ACCEPTED:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;

			case APPROVED:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;

			case REVIEWED_WITH_ERROR:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				this.setStatus(ArticleStatus.RELOADED_FOR_APPROVER);
				break;

			case RELOADED_FOR_APPROVER:
				validStatuses.put(T_LOADED_FOR_PROOFREADING, S_LOADED_FOR_PROOFREADING);
				validStatuses.put(T_RELOADED_WITH_CORRECTIONS, S_RELOADED_WITH_CORRECTIONS);
				validStatuses.put(T_PROOFREAD_WITH_ERROR, S_PROOFREAD_WITH_ERROR);
				validStatuses.put(T_PROOFREAD_ACCEPTED, S_PROOFREAD_ACCEPTED);
				validStatuses.put(T_APPROVED, S_APPROVED);
				validStatuses.put(T_EMBARGO, S_EMBARGO);
				validStatuses.put(T_REVIEWED_WITH_ERROR, S_REVIEWED_WITH_ERROR);
				validStatuses.put(T_RELOADED_FOR_APPROVER, S_RELOADED_FOR_APPROVER);
				break;


			case PUBLISHED:
				validStatuses.put(T_PUBLISHED, S_PUBLISHED);
				break;

			case UNPUBLISHED:
				validStatuses.put(T_UNPUBLISHED, S_UNPUBLISHED);
				break;			

		}
	}

//	public void validateNewStatus() throws InvalidArticleStatusException {
//
//		if (newStatus == ArticleStatus.PUBLISHED) {
//			if (status != ArticleStatus.APPROVED && status != ArticleStatus.EMBARGO &&
//					status != ArticleStatus.UNPUBLISHED) {
//				throw new InvalidArticleStatusException("An article must be approved first before it can be published online.");
//			}
//		}
//
//		if (newStatus == ArticleStatus.APPROVED && 
//				status == ArticleStatus.PUBLISHED) {
//			newStatus = status;
//		}
//	}

	public int getStatus() {
		return status;
	}

	public static String getStatus(String status) {
		String result = null;

		switch (Integer.parseInt(status)) {
			default:
				

			case PRE_LOAD:
				result = T_PRE_LOAD;
				break;
				
			case PRE_PROCESS:
				result = T_PRE_PROCESS;
				break;
				
			case PROCESS:
				result = T_PROCESS;
				break;

			case LOADED_FOR_PROOFREADING:
				result = T_LOADED_FOR_PROOFREADING;
				break;
				
			case RELOADED_WITH_CORRECTIONS:
				result = T_RELOADED_WITH_CORRECTIONS;
				break;
	
			case PROOFREAD_WITH_ERROR:
				result = T_PROOFREAD_WITH_ERROR;
				break;
	
			case PROOFREAD_ACCEPTED:
				result = T_PROOFREAD_ACCEPTED;
				break;
	
			case APPROVED:
				result = T_APPROVED;
				break;
	
			case REVIEWED_WITH_ERROR:
				result = T_REVIEWED_WITH_ERROR;
				break;
	
			case RELOADED_FOR_APPROVER:
				result = T_RELOADED_FOR_APPROVER;
				break;
	
	
			case PUBLISHED:
				result = T_PUBLISHED;
				break;
	
			case UNPUBLISHED:
				result = T_UNPUBLISHED;
				break;		
				
			case EMBARGO:
				result = T_EMBARGO;
				break;		

			case DELETED:
				result = T_DELETED;
				break;					
	
		}

		return result;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(int newStatus) {
		this.newStatus = newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = Integer.parseInt(newStatus);
	}
	
}
