package org.cambridge.ebooks.production.content;

public class AuditTrail {

	public static String LOG_IN = String.valueOf(1);
	public static String LOG_OUT = String.valueOf(2);
	public static String PASSWORD_MAINTENANCE = String.valueOf(3);
	public static String VIEW_STATUS_OF_FILES = String.valueOf(4);
	public static String VIEW_CONTENTS = String.valueOf(5);
	public static String DELETE_CONTENTS = String.valueOf(6);
	public static String RUN_REPORTS = String.valueOf(7);
	public static String VIEW_DTD_FILES = String.valueOf(8);
	public static String MANAGE_SYSTEM_USERS = String.valueOf(9);
	public static String MANAGE_THIRD_PARTY_DETAILS = String.valueOf(19);
	public static String RESOLVE_ERRROS = String.valueOf(10);
	public static String MAKE_FILE_AVAILABLE_FOR_USERS = String.valueOf(11);
	public static String LOAD_DATA_FILES = String.valueOf(12);
	public static String LOAD_COVER_IMAGES = String.valueOf(13);
	public static String VIEW_ARCHIVE_FILES = String.valueOf(14);
	public static String PROOF_CONTENTS = String.valueOf(15);
	public static String APPROVE_CONTENTS = String.valueOf(16);
	public static String NOTIFY_INTERESTED_PARTIES = String.valueOf(17);	
	public static String RESOLVED_LINKS = String.valueOf(18);	
	
	public static String T_LOG_IN = "Login";
	public static String T_LOG_OUT = "Logout";
	public static String T_PASSWORD_MAINTENANCE = "Password Maintenance";
	public static String T_VIEW_STATUS_OF_FILES = "View Status of Files";
	public static String T_VIEW_CONTENTS = "View Contents";
	public static String T_DELETE_CONTENTS = "Delete Content";
	public static String T_RUN_REPORTS = "Run Reports";
	public static String T_VIEW_DTD_FILES = "View DTD Files";
	public static String T_MANAGE_SYSTEM_USERS = "Manage System Users";
	public static String T_MANAGE_THIRD_PARTY_DETAILS = "Manage Third Party Details";
	public static String T_RESOLVE_ERRROS = "Resolve Errors";
	public static String T_MAKE_FILE_AVAILABLE_FOR_USERS = "Make File Available to Users";
	public static String T_LOAD_DATA_FILES = "Load Data Files";
	public static String T_LOAD_COVER_IMAGES = "Load Cover Images";
	public static String T_VIEW_ARCHIVE_FILES = "View Archived Files";
	public static String T_PROOF_CONTENTS = "Proof Contents";
	public static String T_APPROVE_CONTENTS = "Approve Contents";
	public static String T_NOTIFY_INTERESTED_PARTIES = "Notify Interested Parties";	
	public static String T_RESOLVED_LINKS = "Resolve Links";	

	public static String getStatus(String status) {
		String result = null;

		int i = 0;
		
		try {
			i = Integer.parseInt(status);
		} catch(Exception e) {}
		
		switch (i) {
			default:
				result = String.valueOf(0);
				break;

			case 1:
				result = T_LOG_IN;
				break;
				
			case 2:
				result = T_LOG_OUT;
				break;
				
			case 3:
				result = T_PASSWORD_MAINTENANCE;
				break;

			case 4:
				result = T_VIEW_STATUS_OF_FILES;
				break;
				
			case 5:
				result = T_VIEW_CONTENTS;
				break;
	
			case 6:
				result = T_DELETE_CONTENTS;
				break;
	
			case 7:
				result = T_RUN_REPORTS;
				break;

			case 8:
				result = T_VIEW_DTD_FILES;
				break;

			case 9:
				result = T_MANAGE_SYSTEM_USERS;
				break;

			case 19:
				result = T_MANAGE_THIRD_PARTY_DETAILS;
				break;

			case 10:
				result = T_RESOLVE_ERRROS;
				break;

			case 11:
				result = T_MAKE_FILE_AVAILABLE_FOR_USERS;
				break;

			case 12:
				result = T_LOAD_DATA_FILES;
				break;

			case 13:
				result = T_LOAD_COVER_IMAGES;
				break;

			case 14:
				result = T_VIEW_ARCHIVE_FILES;
				break;

			case 15:
				result = T_PROOF_CONTENTS;
				break;

			case 16:
				result = T_APPROVE_CONTENTS;
				break;

			case 17:
				result = T_NOTIFY_INTERESTED_PARTIES;
				break;

			case 18:
				result = T_RESOLVED_LINKS;
				break;

		}

		return result;
	}	
}
