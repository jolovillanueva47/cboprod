package org.cambridge.ebooks.production.jpa.audittrail;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.content.ArticleStatus;
import org.cambridge.ebooks.production.jpa.user.User;

//@NamedNativeQueries({
//	@NamedNativeQuery(
//	        name = UserAuditTrail.SEARCH_USER_AUDIT,
//	        query = "SELECT AT.user_id, AT.audit_id, AT.action, AT.remarks, " +
//	        		"AT.audit_date, " +
//	        		"AU.username " +
//	        		"FROM user_audit_trail AT, cupadmin_user AU WHERE ROWNUM <= 350 " +
//	        		"AND AU.user_id = AT.user_id " +
//	        		"AND AU.username <> 'testkoto' AND AU.username <> 'jcayetano' " +
//	        		"AND AU.username LIKE ?1 " +
//	        		"AND AT.audit_date >= TO_DATE( ?2 ,'DD-Mon-YY') " +
//	        		"AND AT.audit_date <= TO_DATE( ?3 ,'DD-Mon-YY') " +
//	        		"ORDER BY TO_CHAR(AT.audit_date, 'DD-Mon-YYYY HH24:MI:SS') DESC" ,
//	        resultSetMapping = "userAuditResult"
//	),
//	@NamedNativeQuery(
//	        name = UserAuditTrail.SEARCH_USER_TEST,
//	        query = "SELECT AT.user_id, AT.audit_id, AT.action, AT.audit_date, AT.remarks " +
//	        		"FROM user_audit_trail AT WHERE ROWNUM <= 100 " ,
//	        resultSetMapping = "userAuditResult"
//	)
//})
//
//@SqlResultSetMappings({
//	@SqlResultSetMapping(
//	    name = "userAuditResult", 
//	    entities = {
//	        @EntityResult(
//	            entityClass = UserAuditTrail.class, 
//	            fields = {	            	
//	            	@FieldResult(name = "userId",		column = "USER_ID"),
//	            	@FieldResult(name = "audit_id",		column = "AUDIT_ID"),
//	                @FieldResult(name = "action", 		column = "ACTION"),
//	                @FieldResult(name = "audit_date", 	column = "AUDIT_DATE"),
//	                @FieldResult(name = "remarks", 		column = "REMARKS"),
//	            }
//	        )
//	    }
//	)
//})

@Entity
@Table ( name = "USER_AUDIT_TRAIL" )
public class UserAuditTrail {

	public static final String SEARCH_USER_AUDIT = "searchUserAudit";
	public static final String SEARCH_USER_TEST = "searchUserTest";
	
	
//	@Column( name = "USER_ID")
//	private String userId;
	
	@Id
	@Column( name = "AUDIT_ID")
	private int audit_id;
	
	@Column( name = "ACTION")
	private String action;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date audit_date;
	
	@Column( name = "REMARKS")
	private String remarks;
	
	//@OneToOne(mappedBy = "userAuditTrail")
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}

	public int getAudit_id() {
		return audit_id;
	}

	public void setAudit_id(int audit_id) {
		this.audit_id = audit_id;
	}

	public String getAction() {
		return ArticleStatus.getStatus(action);
	}

	public void setAction(String action) {
		this.action = ArticleStatus.getStatus(action);
	}

	public Date getAudit_date() {
		return audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
