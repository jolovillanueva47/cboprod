package org.cambridge.ebooks.production.jpa.thirdparty;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.util.Misc;

/**
 * 
 * @author jgalang
 * 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = ThirdParty.SEARCH_ALL_THIRD_PARTIES,
	        query = "SELECT TP.THIRD_PARTY_ID, TP.THIRD_PARTY_USERNAME, TP.THIRD_PARTY_PASSWORD," + 
	        		" TP.THIRD_PARTY_NAME, TP.THIRD_PARTY_EMAIL, TP.THIRD_PARTY_WEBSITE," + 
	        		" TP.THIRD_PARTY_CONTENT, TP.THIRD_PARTY_COMPRESSION, TP.THIRD_PARTY_JOURNALS," +
	        		" TP.THIRD_PARTY_ACTIVE, TP.THIRD_PARTY_MODIFIED_BY, TP.THIRD_PARTY_MODIFIED_DATE," +
	        		" TP.THIRD_PARTY_FOLDER" +
	        		" FROM THIRD_PARTY_DELIVERY TP ORDER BY UPPER(TP.THIRD_PARTY_NAME)" ,
	        resultSetMapping = "searchThirdPartiesResult"
	)
})

@SqlResultSetMapping(
    name = "searchThirdPartiesResult", 
    entities = { 
        @EntityResult(
            entityClass = ThirdParty.class, 
            fields = {
                @FieldResult(name = "thirdPartyId", column = "THIRD_PARTY_ID"),
                @FieldResult(name = "thirdPartyUsername", column = "THIRD_PARTY_USERNAME"),
                @FieldResult(name = "thirdPartyPassword", column = "THIRD_PARTY_PASSWORD"),
                @FieldResult(name = "thirdPartyEmail", column = "THIRD_PARTY_EMAIL"),
                @FieldResult(name = "thirdPartyName", column = "THIRD_PARTY_NAME"),
                @FieldResult(name = "thirdPartyWebsite", column = "THIRD_PARTY_WEBSITE"),
                @FieldResult(name = "thirdPartyContent", column = "THIRD_PARTY_CONTENT"),
                @FieldResult(name = "thirdPartyCompression", column = "THIRD_PARTY_COMPRESSION"),
                @FieldResult(name = "thirdPartyJournals", column = "THIRD_PARTY_JOURNALS"),
                @FieldResult(name = "thirdPartyActive", column = "THIRD_PARTY_ACTIVE"),
                @FieldResult(name = "thirdPartyModifiedBy", column = "THIRD_PARTY_MODIFIED_BY"),
                @FieldResult(name = "thirdPartyModifiedDate", column = "THIRD_PARTY_MODIFIED_DATE"),
                @FieldResult(name = "thirdPartyFolder", column = "THIRD_PARTY_FOLDER"),
                @FieldResult(name = "thirdPartyDeliveryType", column = "THIRD_PARTY_DELIVERY_TYPE"),
                @FieldResult(name = "thirdPartyInactiveDate", column = "THIRD_PARTY_INACTIVE_DATE")
            }
        )
    }
)

@Entity
@Table ( name = "THIRD_PARTY_DELIVERY" )
public class ThirdParty {
	private static final Log4JLogger LogManager = new Log4JLogger(ThirdParty.class);
	
	public static final String THIRD_PARTY_ACTIVE = "Y";
	public static final String THIRD_PARTY_INACTIVE = "N";
	public static final String SEARCH_ALL_THIRD_PARTIES = "searchAllThirdParties";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	
	public ThirdParty() {
		content = new ThirdPartyContent();
	}
	
	@Id
    @GeneratedValue(generator="THIRD_PARTY_SEQ")
    @SequenceGenerator(name="THIRD_PARTY_SEQ", sequenceName="THIRD_PARTY_SEQ", allocationSize=1)
	@Column( name = "THIRD_PARTY_ID")
	private int thirdPartyId;

	@Column( name = "THIRD_PARTY_USERNAME")
	private String thirdPartyUsername;

	@Column( name = "THIRD_PARTY_PASSWORD")
	private String thirdPartyPassword;

	@Column( name = "THIRD_PARTY_EMAIL")
	private String thirdPartyEmail;

	@Column( name = "THIRD_PARTY_NAME")
	private String thirdPartyName;

	@Column( name = "THIRD_PARTY_WEBSITE")
	private String thirdPartyWebsite;

	@Column( name = "THIRD_PARTY_CONTENT")
	private String thirdPartyContent;

	@Column( name = "THIRD_PARTY_COMPRESSION")
	private String thirdPartyCompression;

	@Column( name = "THIRD_PARTY_JOURNALS")
	private String thirdPartyJournals;

	@Column( name = "THIRD_PARTY_ACTIVE")
	private String thirdPartyActive = "Y";

	@Column( name = "THIRD_PARTY_MODIFIED_BY")
	private String thirdPartyModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "THIRD_PARTY_MODIFIED_DATE")
	private Date thirdPartyModifiedDate;

	@Column( name = "THIRD_PARTY_FOLDER")
	private String thirdPartyFolder;

	@Column( name = "THIRD_PARTY_DELIVERY_TYPE")
	private String thirdPartyDeliveryType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "THIRD_PARTY_INACTIVE_DATE")
	private Date thirdPartyInactiveDate;
	
	@Transient
	private ThirdPartyContent content;
	
	@Transient
	private boolean active;
	
	@Transient
	private String displayInactiveDate;

	public int getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(int thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public String getThirdPartyUsername() {
		return thirdPartyUsername;
	}

	public void setThirdPartyUsername(String thirdPartyUsername) {
		this.thirdPartyUsername = thirdPartyUsername;
	}

	public String getThirdPartyPassword() {
		return thirdPartyPassword;
	}

	public void setThirdPartyPassword(String thirdPartyPassword) {
		this.thirdPartyPassword = thirdPartyPassword;
	}

	public String getThirdPartyEmail() {
		return thirdPartyEmail;
	}

	public void setThirdPartyEmail(String thirdPartyEmail) {
		this.thirdPartyEmail = thirdPartyEmail;
	}

	public String getThirdPartyName() {
		return thirdPartyName;
	}

	public void setThirdPartyName(String thirdPartyName) {
		this.thirdPartyName = thirdPartyName;
	}

	public String getThirdPartyWebsite() {
		return thirdPartyWebsite;
	}

	public void setThirdPartyWebsite(String thirdPartyWebsite) {
		this.thirdPartyWebsite = thirdPartyWebsite;
	}

	public String getThirdPartyContent() {
		return thirdPartyContent;
	}

	public void setThirdPartyContent(String thirdPartyContent) {
		this.thirdPartyContent = thirdPartyContent;
	}

	public String getThirdPartyCompression() {
		return thirdPartyCompression;
	}

	public void setThirdPartyCompression(String thirdPartyCompression) {
		this.thirdPartyCompression = thirdPartyCompression;
	}

	public String getThirdPartyJournals() {
		return thirdPartyJournals;
	}

	public void setThirdPartyJournals(String thirdPartyJournals) {
		this.thirdPartyJournals = thirdPartyJournals;
	}

	public String getThirdPartyActive() {
		return thirdPartyActive;
	}

	public void setThirdPartyActive(String thirdPartyActive) {
		this.thirdPartyActive = thirdPartyActive;
	}

	public String getThirdPartyModifiedBy() {
		return thirdPartyModifiedBy;
	}

	public void setThirdPartyModifiedBy(String thirdPartyModifiedBy) {
		this.thirdPartyModifiedBy = thirdPartyModifiedBy;
	}

	public Date getThirdPartyModifiedDate() {
		return thirdPartyModifiedDate;
	}

	public void setThirdPartyModifiedDate(Date thirdPartyModifiedDate) {
		this.thirdPartyModifiedDate = thirdPartyModifiedDate;
	}

	public String getThirdPartyFolder() {
		return thirdPartyFolder;
	}

	public void setThirdPartyFolder(String thirdPartyFolder) {
		this.thirdPartyFolder = thirdPartyFolder;
	}

	public String getThirdPartyDeliveryType() {
		return thirdPartyDeliveryType;
	}

	public void setThirdPartyDeliveryType(String thirdPartyDeliveryType) {
		this.thirdPartyDeliveryType = thirdPartyDeliveryType;
	}

	public Date getThirdPartyInactiveDate() {
		return thirdPartyInactiveDate;
	}

	public void setThirdPartyInactiveDate(Date thirdPartyInactiveDate) {
		this.thirdPartyInactiveDate = thirdPartyInactiveDate;
	}

	public ThirdPartyContent getContent() {
		return content;
	}

	public void setContent(ThirdPartyContent content) {
		this.content = content;
	}

	public boolean isActive() {
		active = thirdPartyActive.equals(ThirdParty.THIRD_PARTY_ACTIVE);
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		this.thirdPartyActive = active ? ThirdParty.THIRD_PARTY_ACTIVE : THIRD_PARTY_INACTIVE;
	}

	public String getDisplayInactiveDate() {
		if (thirdPartyInactiveDate != null) {
			displayInactiveDate = thirdPartyInactiveDate.toString();
		}
		return displayInactiveDate;
	}

	public void setDisplayInactiveDate(String displayInactiveDate) {
		this.displayInactiveDate = displayInactiveDate;
		if (Misc.isNotEmpty(displayInactiveDate)) {
			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			try {
				this.thirdPartyInactiveDate = df.parse(displayInactiveDate);
			} catch (ParseException pe) {
				LogManager.error(ThirdParty.class, "Error parsing date.");
			}
		}
	}
	
}
