package org.cambridge.ebooks.production.jpa.audittrail;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.content.ArticleStatus;

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "bookResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = Books.class, 
	            fields = {
	            	@FieldResult(name = "component_id",	column = "COMPONENT_ID"),
	                @FieldResult(name = "eisbn", 		column = "JOURNAL_ID"),
	                @FieldResult(name = "chapterId", 	column = "FILE_ID"),
	                @FieldResult(name = "series", 		column = "SERIES"),
	                @FieldResult(name = "published", 	column = "PUBLISHED"),
	                @FieldResult(name = "status", 		column = "STATUS"),
	                @FieldResult(name = "modifiedDate",	column = "MODIFIED_DATE"),
	            }
	        )
	    }
	)
})

@Entity
@Table ( name = "BOOKS" )
public class Books {
	
	//@Id
	@Column( name = "COMPONENT_ID")
	private int component_id;
	
	@Column( name = "EISBN")
	private String eisbn;
	
	@Id
	@Column( name = "CHAPTER_ID", columnDefinition="VARCHAR2(40)")
	private String chapterId;
	
	@Column( name = "SERIES_CODE")
	private String seriesId;
	
	@Column( name = "PUBLISHED")
	private String published;
	
	@Column( name = "STATUS")
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="MODIFIED_DATE")
    private Date modifiedDate;
	
	@OneToMany( mappedBy = "books")
	private List<BookAuditTrail> bookAuditTrail;

	public List<BookAuditTrail> getBookAuditTrail() {
		return bookAuditTrail;
	}

	public void setBookAuditTrail(List<BookAuditTrail> bookAuditTrail) {
		this.bookAuditTrail = bookAuditTrail;
	}

	public int getComponent_id() {
		return component_id;
	}

	public void setComponent_id(int component_id) {
		this.component_id = component_id;
	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public String getChapterId() {
		return chapterId;
	}

	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getStatus() {
		return ArticleStatus.getStatus(status);
	}

	public void setStatus(String status) {
		this.status = ArticleStatus.getStatus(status);
	}
}
