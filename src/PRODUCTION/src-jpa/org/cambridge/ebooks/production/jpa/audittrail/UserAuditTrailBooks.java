package org.cambridge.ebooks.production.jpa.audittrail;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.user.User;

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = UserAuditTrailBooks.SEARCH_USER_AUDIT,
	        query = "SELECT AT.user_id, AT.audit_id, AT.action, AT.remarks, " +
	        		"AT.audit_date, " +
	        		"AU.username " +
	        		"FROM user_audit_trail_books AT, cupadmin_user AU WHERE " +
	        		//"ROWNUM <= 350 AND " +
	        		"AU.user_id = AT.user_id " +
	        		//"AND AU.username <> 'testkoto' AND AU.username <> 'jcayetano' " +
	        		"AND AU.username LIKE ?1 " +
	        		"AND AT.audit_date >= TO_DATE( ?2 ,'DD-Mon-YY') " +
	        		"AND AT.audit_date <= TO_DATE( ?3 ,'DD-Mon-YY') + 1 " +
	        		"ORDER BY AT.audit_date DESC" ,
	        resultSetMapping = "userAuditResult"
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "userAuditResult", 
	    entities = {
	        @EntityResult(
	            entityClass = UserAuditTrailBooks.class, 
	            fields = {	            	
	            	@FieldResult(name = "userId",		column = "USER_ID"),
	            	@FieldResult(name = "auditId",		column = "AUDIT_ID"),
	                @FieldResult(name = "action", 		column = "ACTION"),
	                @FieldResult(name = "auditDate", 	column = "AUDIT_DATE"),
	                @FieldResult(name = "remarks", 		column = "REMARKS"),
	            }
	        )
	    }
	)
})

@Entity
@Table ( name = "USER_AUDIT_TRAIL_BOOKS" )
public class UserAuditTrailBooks {

	public static final String SEARCH_USER_AUDIT = "searchUserAudit";
	
	@Id
	@GeneratedValue(generator="USER_AUDIT_TRAIL_SEQ")
    @SequenceGenerator(name="USER_AUDIT_TRAIL_SEQ", sequenceName="USER_AUDIT_TRAIL_SEQ", allocationSize=1)
	@Column( name = "AUDIT_ID")
	private int auditId;
	
	@Column( name = "ACTION")
	private String action;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date auditDate;
	
	@Column( name = "REMARKS")
	private String remarks;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getAuditId() {
		return auditId;
	}

	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}

	public String getAction() {
		return AuditTrail.getStatus(action);
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
