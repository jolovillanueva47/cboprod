package org.cambridge.ebooks.production.jpa.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 * @author jgalang
 * 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = EBookAccess.SEARCH_EBOOK_ACCESS_BY_ID, 
	        query = "SELECT ?1 AS user_id, book_id FROM cbo_cupadmin_ebook_access WHERE user_id = ?1", 
	        resultSetMapping = "searchEbookAccessResult"
	),
	@NamedNativeQuery(
			name = EBookAccess.DELETE_EBOOK_ACCESS_BY_ID,
			query = "DELETE FROM CBO_CUPADMIN_EBOOK_ACCESS ea" + 
			" WHERE ea.user_id IN (?1)"
	),
	@NamedNativeQuery(
	        name = EBookAccess.SEARCH_EBOOK_ACCESS_BY_ID_AND_ISBN, 
	        query = "SELECT ea.user_id, ea.book_id, ea.modified_date, ea.modified_by " +
	        		"FROM cbo_cupadmin_ebook_access ea " +
	        		"JOIN ebook eb " +
	        		"ON (ea.book_id = eb.book_id) " +
	        		"WHERE eb.isbn = ?1 " +
	        		"AND ea.user_id = ?2", 
	        resultSetMapping = "searchEbookAccessResult"
	)
})

@SqlResultSetMapping(
        name = "searchEbookAccessResult", 
        entities = { 
                @EntityResult(
                        entityClass = EBookAccess.class, 
                        fields = {
                            @FieldResult(name = "userId", column = "USER_ID"),
                            @FieldResult(name = "bookId", column = "BOOK_ID"),
                            @FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
                            @FieldResult(name = "modifiedBy", column = "MODIFIED_BY")
                        }
                ) 
        }
)

@Entity
@Table(name = "CBO_CUPADMIN_EBOOK_ACCESS")
public class EBookAccess {
	
	public static final String SEARCH_EBOOK_ACCESS_BY_ID = "ebookAccessSearchById";
	public static final String SEARCH_EBOOK_ACCESS_BY_ID_AND_ISBN = "ebookAccessSearchByUserIdAndIsbn";
	public static final String DELETE_EBOOK_ACCESS_BY_ID = "ebookAccessDeleteById";
	public static final String ALL_EBOOKS = "ALL_EBOOKS";
	public static final String ALL_PUBLISHER_EBOOKS = "ALL_PUBLISHER_EBOOKS";
	public static final String LIMITED_ACCESS = "LIMITED_ACCESS";
	public static final EBookAccess ALL_EBOOKS_ACCESS = new EBookAccess(ALL_EBOOKS);
	public static final EBookAccess ALL_PUBLISHER_EBOOKS_ACCESS = new EBookAccess(ALL_PUBLISHER_EBOOKS);
	public static final EBookAccess LIMITED_EBOOKS_ACCESS = new EBookAccess(LIMITED_ACCESS);
	
	@Id
	@Column( name = "USER_ID")
	private String userId;
	
	@Id
	@Column( name = "BOOK_ID")
	private String bookId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column( name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Transient
	private String bookTitle;
	
	@Transient
	private String eisbn;
	
	public EBookAccess() {}
	
	public EBookAccess(String bookId) {
		this.bookId = bookId;
	}
	
	public boolean equals(Object o) { 
		boolean result = false;
		if (o instanceof EBookAccess) {
			EBookAccess eba = (EBookAccess) o;
			if ( eba.getBookId().equals(this.getBookId()) )  { 
				result = true;
			}
		}
		return result;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	
	

}
