package org.cambridge.ebooks.production.jpa.publisher;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Transient;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = PublisherIsbn.COUNT_ISBN,
			query = "SELECT COUNT(publisher_id) " + 
					"FROM ebook_publisher_isbn " + 
					"WHERE publisher_id = ?1 " +
					"AND active = 'Y'"
	),
	
	@NamedNativeQuery(
			name = PublisherIsbn.SEARCH_PUBLISHERID_BY_BOOKID,
			query = "SELECT pi.publisher_id  " + 
					"FROM ebook_publisher_isbn pi, (SELECT isbn FROM ebook WHERE book_id = ?1) eb " + 
					"WHERE (eb.isbn = pi.isbn OR eb.isbn LIKE REPLACE(pi.isbn, '*', '%')) " +
					"AND ROWNUM = 1"
	),
	
	@NamedNativeQuery(
	        name = PublisherIsbn.SELECT_ACTIVE_ISBN_BY_PUB_ID,
	        query = "SELECT isbn_counter, publisher_id, isbn, exclude " +
	        		"FROM ebook_publisher_isbn " +
	        		"WHERE publisher_id = ?1 " +
	        		"AND active = 'Y' " +
	        		"ORDER BY isbn ASC",
	        resultSetMapping = "searchIsbnResult"
	),
	
	@NamedNativeQuery(
	        name = PublisherIsbn.SEARCH_PUBLISHER_OF_ISBN,
	        query = "SELECT publisher_id " + 
	        		"FROM ebook_publisher_isbn " + 
	        		"WHERE isbn = ?1 " +
	        		"AND rownum = 1",
	        resultSetMapping = "searchIsbnResult"
	),
	
	@NamedNativeQuery(
	        name = PublisherIsbn.SEARCH_BY_NOT_ACTIVE_ISBN,
	        query = "SELECT * " + 
	        		"FROM ebook_publisher_isbn " + 
	        		"WHERE isbn = ?1 AND publisher_id = ?2 AND active = 'N'",
	        resultSetMapping = "searchIsbnResult"
	),
	
	@NamedNativeQuery(
	        name = PublisherIsbn.SEARCH_BY_ISBN_PUBLISHER_ID,
	        query = "SELECT * " + 
	        		"FROM ebook_publisher_isbn " + 
	        		"WHERE isbn = ?1 AND publisher_id = ?2",
	        resultSetMapping = "searchIsbnResult"
	)
		
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchIsbnResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = PublisherIsbn.class, 
	            fields = {
	                @FieldResult(name = "isbnCounter", column = "ISBN_COUNTER"),
	                @FieldResult(name = "isbn", column = "ISBN"),
	                @FieldResult(name = "publisherId", column = "PUBLISHER_ID"),
	                @FieldResult(name = "exclude", column = "EXCLUDE")
	            }
	        ) 
	    }
	)
})


@Entity
@Table ( name = "EBOOK_PUBLISHER_ISBN" )
public class PublisherIsbn implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static final String COUNT_ISBN = "countIsbn";
	public static final String SELECT_ISBN_BY_ID = "searchIsbnById";
	public static final String SELECT_ACTIVE_ISBN_BY_PUB_ID = "searchActiveIsbnByPubId";

	public static final String SORT_BY_ISBN = "email";

	public static final String SEARCH_PUBLISHERID_BY_BOOKID = "searchPublisherIdByBookId";
	public static final String SEARCH_PUBLISHER_OF_ISBN = "searchPublisherOfIsbn";
	
	public static final String SEARCH_BY_NOT_ACTIVE_ISBN = "searchByIsbn";
	
	public static final String SEARCH_BY_ISBN_PUBLISHER_ID = "searchBookByIsbnPublisherId";
	
	
	@Id
	@GeneratedValue(generator="EBOOK_PUBLISHER_ISBN_SEQ")
    @SequenceGenerator(name="EBOOK_PUBLISHER_ISBN_SEQ", sequenceName="EBOOK_PUBLISHER_ISBN_SEQ", allocationSize=1)
	@Column( name = "ISBN_COUNTER")
	private String isbnCounter;

	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "PUBLISHER_ID")
	private String publisherId;
	
	private String active;
	
	private String exclude = "N";
	
	@Transient
	private boolean excludeBoolean;
	
//	@ManyToOne
//	@JoinColumn(name="PUBLISHER_ID", insertable=false, updatable=false)
//	private Publisher publisher;
	
//	public Publisher getPublisher() {
//		return publisher;
//	}
//
//	public void setPublisher(Publisher publisher) {
//		this.publisher = publisher;
//	}
	
//	@OneToOne
//	@JoinColumn(name="ISBN",  insertable=false, updatable=false)
//	private EBook ebook;
//	
//	public EBook getEbook() {
//		return ebook;
//	}
//
//	public void setEbook(EBook ebook) {
//		this.ebook = ebook;
//	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public String getIsbnCounter() {
		return isbnCounter;
	}

	public void setIsbnCounter(String isbnCounter) {
		this.isbnCounter = isbnCounter;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getExclude() {
		return exclude;
	}

	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

	public boolean isExcludeBoolean() {
		return "Y".equalsIgnoreCase(exclude) ? true : false;
	}

	public void setExcludeBoolean(boolean excludeBoolean) {
		this.excludeBoolean = excludeBoolean;
	}

	public boolean equals(Object anotherObject){
		if(anotherObject instanceof PublisherIsbn)
			if(((PublisherIsbn)anotherObject).getPublisherId().equals(this.getPublisherId()))
				return true;
		
		return false;
	}
	
	public int hashCode(){
		//return !"".equals(this.publisherId) ? (this.publisherId != null ? this.publisherId.length() : 1) : 0;
		return Integer.parseInt(this.publisherId);
	}
}
