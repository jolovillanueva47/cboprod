package org.cambridge.ebooks.production.jpa.audittrail;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.content.ArticleStatus;

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "articleResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = Article.class, 
	            fields = {
	            	@FieldResult(name = "component_id",	column = "COMPONENT_ID"),
	                @FieldResult(name = "journal_id", 	column = "JOURNAL_ID"),
	                @FieldResult(name = "file_id", 		column = "FILE_ID"),
	                @FieldResult(name = "series_id", 	column = "SERIES_CODE"),
	                @FieldResult(name = "volume_id", 	column = "VOLUME_ID"),
	                @FieldResult(name = "issue_id", 	column = "ISSUE_ID"),
	                @FieldResult(name = "load_type", 	column = "LOAD_TYPE"),
	                @FieldResult(name = "status", 		column = "STATUS"),
	                @FieldResult(name = "modified_date",column = "MODIFIED_DATE"),
	                @FieldResult(name = "title",		column = "TITLE"),
	            }
	        )
	    }
	)
})

@Entity
@Table ( name = "ARTICLE" )
public class Article {
	
	//@Id
	@Column( name = "COMPONENT_ID")
	private int component_id;
	
	@Column( name = "JOURNAL_ID")
	private String journal_id;
	
	@Id
	@Column( name = "FILE_ID", columnDefinition="VARCHAR2(20)")
	private String file_id;
	
	@Column( name = "SERIES_CODE")
	private String series_id;
	
	@Column( name = "VOLUME_ID")
	private String volume_id;
	
	@Column( name = "ISSUE_ID")
	private String issue_id;
	
//	@Column( name = "LOAD_TYPE")
//	private String load_type;
	
	@Column( name = "STATUS")
	private String status;
	
	@Column( name = "TITLE")
	private String title;
	
	@Column( name = "FORTHCOMING")
	private String forthcoming;
	
	@Column( name = "PUBLISHED")
	private String published;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="MODIFIED_DATE")
    private Date modified_date;
	
//	@OneToOne(mappedBy="article")
//	private ArticleAuditTrail articleAuditTrail;
//	
//	public ArticleAuditTrail getArticleAuditTrail() {
//		return articleAuditTrail;
//	}
//
//	public void setArticleAuditTrail(ArticleAuditTrail articleAuditTrail) {
//		this.articleAuditTrail = articleAuditTrail;
//	}
	
	@ManyToMany( mappedBy = "article")
	private List<ArticleAuditTrail> articleAuditTrail;

	public List<ArticleAuditTrail> getArticleAuditTrail() {
		return articleAuditTrail;
	}

	public void setArticleAuditTrail(List<ArticleAuditTrail> articleAuditTrail) {
		this.articleAuditTrail = articleAuditTrail;
	}

	public int getComponent_id() {
		return component_id;
	}

	public void setComponent_id(int component_id) {
		this.component_id = component_id;
	}

	public String getJournal_id() {
		return journal_id;
	}

	public void setJournal_id(String journal_id) {
		this.journal_id = journal_id;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getSeries_id() {
		return series_id;
	}

	public void setSeries_id(String series_id) {
		this.series_id = series_id;
	}

	public String getVolume_id() {
		return volume_id;
	}

	public void setVolume_id(String volume_id) {
		this.volume_id = volume_id;
	}

	public String getIssue_id() {
		return issue_id;
	}

	public void setIssue_id(String issue_id) {
		this.issue_id = issue_id;
	}

//	public String getLoad_type() {
//		return load_type;
//	}
//
//	public void setLoad_type(String load_type) {
//		this.load_type = load_type;
//	}
	
	public Date getModified_date() {
		return modified_date;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getForthcoming() {
		return forthcoming;
	}

	public void setForthcoming(String forthcoming) {
		this.forthcoming = forthcoming;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getStatus() {
		return ArticleStatus.getStatus(status);
	}

	public void setStatus(String status) {
		this.status = ArticleStatus.getStatus(status);
	}
	
	
}
