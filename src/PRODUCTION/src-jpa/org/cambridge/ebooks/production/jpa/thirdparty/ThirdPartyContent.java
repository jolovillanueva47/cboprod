package org.cambridge.ebooks.production.jpa.thirdparty;

import java.io.Serializable;

/**
 * 
 * @author C2
 *
 */

public class ThirdPartyContent implements Serializable{
	
	private static final String PDF = "PDF";
	private static final String SGML_H = "SGML_H";
	private static final String SGML_S = "SGML_S";
	private static final String XML_H = "XML_H";
	private static final String XML_W = "XML_W";
	private static final String HTML_W = "HTML_W";
	private static final String XML_R = "XML_R";
	
	private boolean hasPDF;
	private boolean hasSGML_H;
	private boolean hasSGML_S;
	private boolean hasXML_H;
	private boolean hasXML_W;
	private boolean hasHTML_W;
	private boolean hasXML_R;
	
	//PDF,SGML_H,SGML_S,XML_H,XML_W,HTML_W,REF,XML_R
	public ThirdPartyContent() {
		this(new String());
	}
	
	public ThirdPartyContent(String content) {
		String[] contentArray = content.split(",");
		for (String s : contentArray) {
			resolveContent(s);
		}
	}

	public void resolveContent(String contentCode) {
		if(contentCode.equals(PDF)) { hasPDF = true; }
		else if (contentCode.equals(SGML_H)) { hasSGML_H = true; }
		else if (contentCode.equals(SGML_S)) { hasSGML_S = true; }
		else if (contentCode.equals(XML_H)) { hasXML_H = true; }
		else if (contentCode.equals(XML_W)) { hasXML_W = true; }
		else if (contentCode.equals(HTML_W)) { hasHTML_W = true; }
		else if (contentCode.equals(XML_R)) { hasXML_R = true; }
	}
	
	public String getContentAsString() {
		StringBuilder content = new StringBuilder();
		if (hasPDF) content.append(PDF + ",");
		if (hasSGML_H) content.append(SGML_H + ",");
		if (hasSGML_S) content.append(SGML_S + ",");
		if (hasXML_H) content.append(XML_H + ",");
		if (hasXML_W) content.append(XML_W + ",");
		if (hasHTML_W) content.append(HTML_W + ",");
		if (hasXML_R) content.append(XML_R + ",");
		if (content.length()>0) 
			content = new StringBuilder(content.substring(0, content.length()-1));
		return content.toString();
	}

	public boolean isHasPDF() {
		return hasPDF;
	}

	public void setHasPDF(boolean hasPDF) {
		this.hasPDF = hasPDF;
	}

	public boolean isHasSGML_H() {
		return hasSGML_H;
	}

	public void setHasSGML_H(boolean hasSGML_H) {
		this.hasSGML_H = hasSGML_H;
	}

	public boolean isHasSGML_S() {
		return hasSGML_S;
	}

	public void setHasSGML_S(boolean hasSGML_S) {
		this.hasSGML_S = hasSGML_S;
	}

	public boolean isHasXML_H() {
		return hasXML_H;
	}

	public void setHasXML_H(boolean hasXML_H) {
		this.hasXML_H = hasXML_H;
	}

	public boolean isHasXML_W() {
		return hasXML_W;
	}

	public void setHasXML_W(boolean hasXML_W) {
		this.hasXML_W = hasXML_W;
	}

	public boolean isHasHTML_W() {
		return hasHTML_W;
	}

	public void setHasHTML_W(boolean hasHTML_W) {
		this.hasHTML_W = hasHTML_W;
	}

	public boolean isHasXML_R() {
		return hasXML_R;
	}

	public void setHasXML_R(boolean hasXML_R) {
		this.hasXML_R = hasXML_R;
	}
	
	
}
