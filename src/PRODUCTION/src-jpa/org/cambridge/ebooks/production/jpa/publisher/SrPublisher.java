package org.cambridge.ebooks.production.jpa.publisher;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = SrPublisher.COUNT_PUBLISHERS,
			query = "SELECT COUNT(PUBLISHER_CODE) " + 
					"FROM SR_PUBLISHER" 
	),

	@NamedNativeQuery(
	        name = SrPublisher.SELECT_PUBLISHERS,
	        query = "SELECT PUBLISHER_CODE, PUBLISHER_LEGEND, " +
//	        		"PUBLISHER_ACTIVE_FLAG, " +
	        		"PUBLISHER_EMAIL, CROSSREF_LOGIN, CROSSREF_PASSWORD " +
	        		"FROM SR_PUBLISHER " /*+
	        		"WHERE PUBLISHER_ACTIVE_FLAG = 'Y' "*/,
	        resultSetMapping = "searchSrPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = SrPublisher.SEARCH_PUBLISHER_BY_ID,
	        query = "SELECT PUBLISHER_CODE, PUBLISHER_LEGEND, " +
//	        		"PUBLISHER_ACTIVE_FLAG, " +
	        		"PUBLISHER_EMAIL, CROSSREF_LOGIN, CROSSREF_PASSWORD " +
	        		"FROM SR_PUBLISHER " +
	        		"WHERE PUBLISHER_CODE = ?1 " /*+
	        		"AND PUBLISHER_ACTIVE_FLAG = 'Y'"*/,
	        resultSetMapping = "searchSrPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = SrPublisher.SEARCH_PUBLISHER_BY_ISBN,
	        query = "SELECT PUBLISHER_CODE, PUBLISHER_LEGEND, " +
//	        		"PUBLISHER_ACTIVE_FLAG, " +
	        		"PUBLISHER_EMAIL, CROSSREF_LOGIN, CROSSREF_PASSWORD " +
	        		"FROM SR_PUBLISHER " +
	        		"WHERE PUBLISHER_CODE = " +
	        		"   (SELECT publisher_code from SR_CBO_PRODUCT_GROUP_ORA where CBO_PRODUCT_GROUP = " +
	        		"      (select CBO_PRODUCT_GROUP from core_isbn_cbo where ean_number = ?1))" /*+
	        		"AND PUBLISHER_ACTIVE_FLAG = 'Y'"*/,
	        resultSetMapping = "searchSrPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = SrPublisher.SEARCH_PUBLISHER_PREFIX,
	        query = "SELECT PUBLISHER_CODE, URL_PREFIX " +
	        		"FROM SR_PUBLISHER " +
	        		"WHERE PUBLISHER_CODE = ?1" ,
	        resultSetMapping = "searchSrPublisherPrefixResult"
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchSrPublisherResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = SrPublisher.class, 
	            fields = {
	                @FieldResult(name = "publisher_code", column = "PUBLISHER_CODE"),
	                @FieldResult(name = "publisher_name", column = "PUBLISHER_LEGEND"),
//	                @FieldResult(name = "active", column = "PUBLISHER_ACTIVE_FLAG"),
	                @FieldResult(name = "email", column = "PUBLISHER_EMAIL"),
	                @FieldResult(name = "username", column = "CROSSREF_LOGIN"),
	                @FieldResult(name = "password", column = "CROSSREF_PASSWORD")
	            }
	        ) 
	    }
	), 
	
	@SqlResultSetMapping(
		    name = "searchSrPublisherPrefixResult", 
		    entities = { 
		        @EntityResult(
		            entityClass = SrPublisher.class, 
		            fields = {
		                @FieldResult(name = "publisher_code", column = "PUBLISHER_CODE"),
		                @FieldResult(name = "prefix", column = "URL_PREFIX")
		            }
		        ) 
		    }
		)
})

@Entity
@Table ( name = "SR_PUBLISHER")
public class SrPublisher implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String COUNT_PUBLISHERS = "countPublishers";
	public static final String SELECT_PUBLISHERS = "selectSrPublishers";
	public static final String SEARCH_PUBLISHER_BY_ID = "selectPublishersById";
	public static final String SEARCH_PUBLISHER_BY_ISBN = "selectPublishersByISBN";
	public static final String SEARCH_PUBLISHER_PREFIX = "selectPublishersPrefix";
	
	@Id
	@Column (name = "PUBLISHER_CODE")
	private String publisher_code;
	
	@Column (name = "PUBLISHER_LEGEND")
	private String publisher_name;
	
	@Column (name = "PUBLISHER_ACTIVE_FLAG")
	private String active;
	
	@Column (name="PUBLISHER_EMAIL")
	private String email;
	
	@Column (name = "CROSSREF_LOGIN")
	private String username;
	
	@Column (name = "CROSSREF_PASSWORD")
	private String password;
	
	@Column (name = "URL_PREFIX")
	private String prefix;
	
	public SrPublisher() {
	}
	
	public SrPublisher(String publisher_code, String publisher_name) {
		this.publisher_code = publisher_code;
		this.publisher_name = publisher_name;
	}

	public String getPublisher_code() {
		return publisher_code;
	}

	public void setPublisher_code(String publisher_code) {
		this.publisher_code = publisher_code;
	}

	public String getPublisher_name() {
		return publisher_name;
	}

	public void setPublisher_name(String publisher_name) {
		this.publisher_name = publisher_name;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	

}
