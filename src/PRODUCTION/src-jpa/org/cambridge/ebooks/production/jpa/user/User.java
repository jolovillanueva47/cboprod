package org.cambridge.ebooks.production.jpa.user;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.cambridge.ebooks.production.jpa.audittrail.UserAuditTrailBooks;
import org.cambridge.ebooks.production.util.Password;
/**
 * 
 * @author jgalang
 * 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
			name = User.COUNT_CUPADMIN_USERS,
			query = "SELECT COUNT(CU.USER_ID)" + 
					" FROM CUPADMIN_USER CU LEFT JOIN CBO_CUPADMIN_ACCESS CA ON CU.user_id=CA.user_id" + 
					" WHERE CU.TYPE=1"
	),
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_BY_LOGIN,
	        query = "SELECT user_id, username, password, email, first_name, last_name," +
	        		" department, access_list, prefs, active" +
	        		" FROM cupadmin_user CU LEFT JOIN CBO_CUPADMIN_ACCESS CUA" +
	        		" USING (user_id)" +
	        		" WHERE username = ?1" +
	        		" AND password = ?2" +
	        		" AND active = 'Y'" +
	        		" AND type = 1" ,
	        resultSetMapping = "searchUserResult"
	), 
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_SORT_BY_USERNAME,
	        query = "SELECT CU.user_id,CU.username, CU.password,CU.email,CU.first_name,CU.last_name," +
					" CU.department, CU.active, CA.access_list" +
			   		" FROM CUPADMIN_USER CU LEFT JOIN CBO_CUPADMIN_ACCESS CA ON CU.user_id=CA.user_id" +
			   		" WHERE CU.type=1" +
			   		" ORDER BY CU.username ASC",
	        resultSetMapping = "searchUserResult"
	), 
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_SORT_BY_EMAIL,
	        query = "SELECT CU.user_id,CU.username, CU.password,CU.email,CU.first_name,CU.last_name," +
					" CU.department, CU.active, CA.access_list" +
			   		" FROM CUPADMIN_USER CU LEFT JOIN CBO_CUPADMIN_ACCESS CA ON CU.user_id=CA.user_id" +
			   		" WHERE CU.type=1" +
			   		" ORDER BY CU.email ASC",
	        resultSetMapping = "searchUserResult"
	), 
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_SORT_BY_FIRSTNAME,
	        query = "SELECT CU.user_id,CU.username, CU.password,CU.email,CU.first_name,CU.last_name," +
					" CU.department, CU.active, CA.access_list" +
			   		" FROM CUPADMIN_USER CU LEFT JOIN CBO_CUPADMIN_ACCESS CA ON CU.user_id=CA.user_id" +
			   		" WHERE CU.type=1" +
			   		" ORDER BY CU.first_name ASC",
	        resultSetMapping = "searchUserResult"
	), 
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_SORT_BY_LASTNAME,
	        query = "SELECT CU.user_id,CU.username, CU.password,CU.email,CU.first_name,CU.last_name," +
					" CU.department, CU.active, CA.access_list" +
			   		" FROM CUPADMIN_USER CU LEFT JOIN CBO_CUPADMIN_ACCESS CA ON CU.user_id=CA.user_id" +
			   		" WHERE CU.type=1" +
			   		" ORDER BY CU.last_name ASC",
	        resultSetMapping = "searchUserResult"
	),
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_BY_ID,
	        query = "SELECT user_id, username, password, email, first_name, last_name," +
	        		" department, active, access_list, prefs, type" +
	        		" FROM cupadmin_user CU LEFT JOIN CBO_CUPADMIN_ACCESS CA" +
	        		" USING (user_id)" +
	        		" WHERE user_id = ?1",
	        resultSetMapping = "searchUserResult"
	),
	
	@NamedNativeQuery(
	        name = User.SEARCH_CUPADMIN_USER_BY_EMAIL,
	        query = "SELECT user_id, username, password, email, first_name, last_name," +
	        		" department, active, access_list, prefs, type" +
	        		" FROM cupadmin_user CU LEFT JOIN CBO_CUPADMIN_ACCESS CA" +
	        		" USING (user_id)" +
	        		" WHERE email = ?1",
	        resultSetMapping = "searchUserResult"
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchUserResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = User.class, 
	            fields = {
	                @FieldResult(name = "userId", column = "USER_ID"),
	                @FieldResult(name = "username", column = "USERNAME"),
	                @FieldResult(name = "password", column = "PASSWORD"),
	                @FieldResult(name = "email", column = "EMAIL"),
	                @FieldResult(name = "firstName", column = "FIRST_NAME"),
	                @FieldResult(name = "lastName", column = "LAST_NAME"),
	                @FieldResult(name = "department", column = "DEPARTMENT"),
	                @FieldResult(name = "active", column = "ACTIVE"),
	            }
	        ) 
	    }
	)
})


@Entity
@Table ( name = "CUPADMIN_USER" )
public class User {
	
	public static final String USER_ACTIVE = "Y";
	public static final String USER_INACTIVE = "N";
	public static final int PASSWORD_MIN_LENGTH = 6;
	public static final int PASSWORD_MAX_LENGTH = 9;
	public static final String COUNT_CUPADMIN_USERS = "cupadminUserCount";
	public static final String SEARCH_CUPADMIN_USER_BY_LOGIN = "cupadminUserSearchByLogin";
	public static final String SEARCH_CUPADMIN_USER_BY_ID = "cupadminUserSearchById";
	public static final String SEARCH_CUPADMIN_USER_BY_EMAIL = "cupadminUserSearchByEmail";
	public static final String SEARCH_CUPADMIN_USER_SORT_BY_USERNAME = "cupadminUserSearchSortByUsername";
	public static final String SEARCH_CUPADMIN_USER_SORT_BY_EMAIL = "cupadminUserSearchSortByEmail";
	public static final String SEARCH_CUPADMIN_USER_SORT_BY_FIRSTNAME = "cupadminUserSearchSortByFirstName";
	public static final String SEARCH_CUPADMIN_USER_SORT_BY_LASTNAME = "cupadminUserSearchSortByLastName";
	public static final String USERNAME_CONSTRAINT = "USERNAME_TYPE_CONSTRAINT";
	public static final String EMAIL_CONSTRAINT = "EMAIL_TYPE_CONSTRAINT";
	public static final String SORT_BY_USERNAME = "Username";
	public static final String SORT_BY_EMAIL = "Email";
	public static final String SORT_BY_FIRST_NAME = "First Name";
	public static final String SORT_BY_LAST_NAME = "Last Name";
	
	public User() {
		accessRights = new AccessRights(
				AccessRights.getDefaultAccessRights().keySet().toArray(new String[]{}));
		password = Password.obfuscate(new String(Password.generateStrongPassword(6)));
	}
	
	@Id
    @GeneratedValue(generator="BODY_SEQ")
    @SequenceGenerator(name="BODY_SEQ", sequenceName="BODY_SEQ", allocationSize=1)
	@Column( name = "USER_ID")
	private String userId;

	@Column( name = "USERNAME", unique = true)
	private String username;
	
	@Column( name = "PASSWORD")
	private String password;
	
	@Column( name = "EMAIL", unique = true)
	private String email;
	
	@Column( name = "FIRST_NAME")
	private String firstName;
	
	@Column( name = "LAST_NAME")
	private String lastName;
	
	@Column( name = "DEPARTMENT")
	private String department;
	
	@Column( name = "TYPE")
	private int type = 1;
	
	@Column( name = "ACTIVE")
	private String active;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="MODIFIED_DATE")
    private Date modifiedDate;
	
	@Column( name = "MODIFIED_BY")
	private String modifiedBy;
	
	@OneToOne(mappedBy = "user", cascade={CascadeType.REMOVE})
	private CupadminAccess access;
	
	@Transient
	private AccessRights accessRights;
	
	@Transient
	private String displayActive;
	
	@OneToMany( mappedBy = "user")
	private List<UserAuditTrailBooks> userAuditTrail;
	
	public List<UserAuditTrailBooks> getUserAuditTrail() {
		return userAuditTrail;
	}
	
	public void setUserAuditTrail(List<UserAuditTrailBooks> userAuditTrail) {
		this.userAuditTrail = userAuditTrail;
	}
		
	public CupadminAccess getAccess() {
		return access;
	}
	public void setAccess(CupadminAccess access) {
		this.access = access;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public AccessRights getAccessRights() {
		return accessRights;
	}
	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getDisplayActive() {
		displayActive = USER_ACTIVE.equals(active) ? "Yes" : "No";
		return displayActive;
	}
	public void setDisplayActive(String displayActive) {
		active = "Yes".equals(displayActive) ? USER_ACTIVE : USER_INACTIVE;
		this.displayActive = displayActive;
	}
	
	
	
}
