package org.cambridge.ebooks.production.jpa.common;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.ebook.EBookComponent;


/**
 * 
 * @author rvillamor
 *               
 *
 */

@NamedNativeQueries( {

    @NamedNativeQuery(
            name  = EBook.COUNT_SEARCH_ALL,
            query = "SELECT COUNT (*) FROM" +
                    " (SELECT e.book_id, e.isbn, e.series_code, e.modified_date, e.modified_by, e.title, e.title_alphasort," +
                    " DECODE(ec.book_id, null, 200, e.status) status, e.proofing_url, e.proofing_url_date" +
                    " FROM ebook e" +
                    " LEFT OUTER JOIN ebook_content ec" +
                    " ON (e.book_id = ec.book_id)" +
                    " AND ec.type='xml')"
    ),
    @NamedNativeQuery(
            name  = EBook.SEARCH_ALL,
            query = "SELECT e.book_id, e.isbn, e.series_code, e.modified_date, e.modified_by, e.title, e.title_alphasort," +
                    " DECODE(ec.book_id, null, 200, e.status) status, e.proofing_url, e.proofing_url_date" +
                    " FROM ebook e" +
                    " LEFT OUTER JOIN ebook_content ec" +
                    " ON (e.book_id = ec.book_id)" +
                    " AND ec.type='xml'" +
                    " ORDER BY" +
                    " CASE status" +
                    " WHEN 8 THEN 6.5" +
                    " WHEN 5 THEN 1.5" +
                    " WHEN 6 THEN 3.6" +
                    " WHEN 100 THEN 0.1" +
                    " WHEN 3 THEN 1.3" +
                    " ELSE status" +
                    " END," +
                    " title_alphasort",
            resultSetMapping =  EBook.EBOOK_RESULT_SET_MAPPING
    ),
    
    @NamedNativeQuery(
            name  = EBook.SEARCH_ALL_BY_BOOK_ID,
            query = "SELECT * FROM ebook e" +
                    " WHERE e.book_id = ?1", 
                    resultSetMapping =  EBook.EBOOK_RESULT_SET_MAPPING
    ),
    
    @NamedNativeQuery(
            name  = EBook.SEARCH_ALL_BY_BOOK_ID_WITH_MODEL,
            query = "SELECT * FROM ebook e" +
                    " WHERE e.book_id = ?1", 
                    resultSetMapping =  EBook.EBOOK_RESULT_SET_MAPPING_MODEL
    ),
    
    @NamedNativeQuery(
            name  = EBook.SEARCH_EBOOK_BY_ISBN,
            query = "SELECT * FROM ebook e " +
                    "WHERE e.isbn = ?1", 
                    resultSetMapping =  EBook.EBOOK_RESULT_SET_MAPPING
    ),
    
    @NamedNativeQuery(
            name = EBook.UPDATE_EBOOK_BY_BOOK_ID,
            query = "UPDATE EBOOK"
                    + " SET ISBN = ?1, MODIFIED_DATE = ?2, MODIFIED_BY = ?3"
                    + " WHERE BOOK_ID = ?4"
    ),
    
    @NamedNativeQuery(
            name = EBook.UPDATE_EBOOK_STATUS_BY_BOOK_ID,
            query = "UPDATE EBOOK"
                    + " SET STATUS = ?1 WHERE BOOK_ID = ?2"
    ),
    
    @NamedNativeQuery(
            name = EBook.UPDATE_EBOOK_MODEL_BY_BOOK_ID,
            query = "UPDATE EBOOK " + 
                    "SET MODEL_TYPE = ?1 WHERE BOOK_ID = ?2"
    ),
    
    @NamedNativeQuery(
            name = EBook.UPDATE_EBOOK_DETAILS_BY_BOOK_ID,
            query = "UPDATE EBOOK " + 
                    "SET STATUS = ?1, TITLE= ?2, TITLE_ALPHASORT = ?3 WHERE BOOK_ID = ?4"
    ),
    
    @NamedNativeQuery(
            name = EBook.UPDATE_EBOOK_PROOFING_CODE,
            query = "UPDATE EBOOK"
                    + " SET PROOFING_CODE = ?1, PROOFING_EXPIRY_DATE = ?2 WHERE BOOK_ID = ?3"
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_STATUS_SUMMARY,
            
            query = "SELECT status, COUNT(status) total FROM (                                        " +
            "SELECT DECODE(ec.book_id, null, 200, e.status) status  " +
            "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
            "                                                          FROM audit_trail_ebooks " +
            "                                                          WHERE (status = 0 OR status = 7) " +  
            "                                                          GROUP BY eisbn) at " +   
            "WHERE (ec.type='xml' " + 
            "       AND e.isbn IN ( " +
            "              SELECT ean_number as isbn " +
            "              FROM core_isbn_cbo " +
            "              WHERE cbo_product_group IN (" +
            "                   SELECT cbo_product_group" +
            "                   FROM sr_cbo_product_group_ora " +
            "                   WHERE publisher_code = ?1 " +
            "              )" +
            "       )" +
            ") "+
            
            "AND (ec.book_id(+) = e.book_id) " +
            "AND (at.eisbn(+) = e.isbn) " +
            "AND (dl.isbn(+) = at.eisbn) )  z " +
            "GROUP BY z.status " +
            "ORDER BY z.status " 
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_STATUS_SUMMARY_ALL,
            
            query = "SELECT status, COUNT(status) total FROM (                                        " +
            "SELECT DECODE(ec.book_id, null, 200, e.status) status  " +
            "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
            "                                                          FROM audit_trail_ebooks " +
            "                                                          WHERE (status = 0 OR status = 7) " +  
            "                                                          GROUP BY eisbn) at " +   
            "WHERE (ec.type='xml' " + 
            "       AND e.isbn IN ( " +
            "              SELECT ean_number as isbn " +
            "              FROM core_isbn_cbo " +
            "       )" +
            ") "+
            "AND (ec.book_id(+) = e.book_id) " +
            "AND (at.eisbn(+) = e.isbn) " +
            "AND (dl.isbn(+) = at.eisbn) )  z " +
            "GROUP BY z.status " +
            "ORDER BY z.status " 
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_REPORT,
            query = "SELECT e.book_id, e.isbn, e.title, e.title_alphasort, DECODE(ec.book_id, null, 200, e.status) status, " +
                    "       TO_CHAR(dl.pub_date_print, 'YYYY') pub_date_print, " +
                    "       TO_CHAR(DECODE(DECODE(ec.book_id, null, 200, e.status), 7, at.max_date, null), 'DD-MON-YYYY') online_pub_date," +
                    "       TO_CHAR(DECODE(e.status, 0, at.min_date, null), 'DD-MON-YYYY') online_loaded_date, dl.publisher_code, dl.cbo_product_group " + //added ", dl.publisher_code, dl.cbo_product_group"  
                    "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
                    "                                                          FROM audit_trail_ebooks " +
                    "                                                          WHERE (status = 0 OR status = 7) " +  
                    "                                                          GROUP BY eisbn) at " +   
                    "WHERE (ec.type='xml' " + 
                    "       AND e.isbn IN ( " +
                    "              SELECT ean_number as isbn " +
                    "              FROM core_isbn_cbo " +
                    "              WHERE cbo_product_group IN (" +
                    "                   SELECT cbo_product_group" +
                    "                   FROM sr_cbo_product_group_ora " +
                    "                   WHERE publisher_code = ?1 " +
                    "              )" +
                    "       )" +
                    ") "+

                    
                    "AND (ec.book_id(+) = e.book_id) " +
                    "AND (at.eisbn(+) = e.isbn) " +
                    "AND (dl.isbn(+) = at.eisbn) " +
                    " ORDER BY  " +
                    " CASE e.status  " +
                    " WHEN 8 THEN 6.5 " +
                    " WHEN 5 THEN 1.5 " +
                    " WHEN 6 THEN 3.6 " + 
                    " WHEN 100 THEN 0.1 " +
                    " WHEN 3 THEN 1.3 " +
                    " ELSE e.status " + 
                    " END, e.title_alphasort "
            
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_REPORT_ALL,
            query = "SELECT e.book_id, e.isbn, e.title, e.title_alphasort, DECODE(ec.book_id, null, 200, e.status) status, " +
                    "       TO_CHAR(dl.pub_date_print, 'YYYY') pub_date_print, " +
                    "       TO_CHAR(DECODE(DECODE(ec.book_id, null, 200, e.status), 7, at.max_date, null), 'DD-MON-YYYY') online_pub_date," +
                    "       TO_CHAR(DECODE(e.status, 0, at.min_date, null), 'DD-MON-YYYY') online_loaded_date, dl.publisher_code, dl.cbo_product_group " + //added ", dl.publisher_code, dl.cbo_product_group"  
                    "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
                    "                                                          FROM audit_trail_ebooks " +
                    "                                                          WHERE (status = 0 OR status = 7) " +  
                    "                                                          GROUP BY eisbn) at " +   
                    "WHERE (ec.type='xml' " + 
                    
                    "       AND e.isbn IN ( " +
                    "              SELECT ean_number as isbn " +
                    "              FROM core_isbn_cbo " +
                    "       )" +
                    ") "+
                    "AND (ec.book_id(+) = e.book_id) " +
                    "AND (at.eisbn(+) = e.isbn) " +
                    "AND (dl.isbn(+) = at.eisbn) " +
                    " ORDER BY  " +
                    " CASE e.status  " +
                    " WHEN 8 THEN 6.5 " +
                    " WHEN 5 THEN 1.5 " +
                    " WHEN 6 THEN 3.6 " + 
                    " WHEN 100 THEN 0.1 " +
                    " WHEN 3 THEN 1.3 " +
                    " ELSE e.status " + 
                    " END, e.title_alphasort "
            
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_REPORT_COUNT,
            query = "SELECT COUNT(e.book_id) " + 
            "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
            "                                                          FROM audit_trail_ebooks " +
            "                                                          WHERE (status = 0 OR status = 7) " +  
            "                                                          GROUP BY eisbn) at " +   
            "WHERE (ec.type='xml' " + 
            
            "       AND e.isbn IN ( " +
            "              SELECT ean_number as isbn " +
            "              FROM core_isbn_cbo " +
            "              WHERE cbo_product_group IN (" +
            "                   SELECT cbo_product_group" +
            "                   FROM sr_cbo_product_group_ora " +
            "                   WHERE publisher_code = ?1 " +
            "              )" +
            "       )" +
            ") "+

            "AND (ec.book_id(+) = e.book_id) " +
            "AND (at.eisbn(+) = e.isbn) " +
            "AND (dl.isbn(+) = at.eisbn) " 
    ),
    
    @NamedNativeQuery(
            name = EBook.EBOOK_REPORT_COUNT_ALL,
            query = "SELECT COUNT(e.book_id) " + 
            "FROM ebook e, ebook_content ec, ebook_isbn_data_load dl, (SELECT eisbn, MAX(audit_date) max_date, MIN(audit_date) min_date " +  
            "                                                          FROM audit_trail_ebooks " +
            "                                                          WHERE (status = 0 OR status = 7) " +  
            "                                                          GROUP BY eisbn) at " +   
            "WHERE (ec.type='xml' " + 
            "       AND e.isbn IN ( " +
            "              SELECT ean_number as isbn " +
            "              FROM core_isbn_cbo " +
            "       )" +
            ") "+
            "AND (ec.book_id(+) = e.book_id) " +
            "AND (at.eisbn(+) = e.isbn) " +
            "AND (dl.isbn(+) = at.eisbn) " 
    )
    
} )

@SqlResultSetMappings({
@SqlResultSetMapping( 
    name = EBook.EBOOK_RESULT_SET_MAPPING,
    entities = {
        @EntityResult (
            entityClass = EBook.class,
            fields = {
                @FieldResult( name = "bookId",    column = "BOOK_ID"),
                @FieldResult( name = "isbn",      column = "ISBN"),
                @FieldResult( name = "status",    column = "STATUS"),
                @FieldResult( name = "seriesId",  column = "SERIES_CODE"),
                @FieldResult( name = "title",          column = "TITLE"),
                @FieldResult( name = "titleAlphasort",    column = "TITLE_ALPHASORT"),
                @FieldResult( name = "modifiedDate", column = "MODIFIED_DATE"),
                @FieldResult( name = "proofingUrl", column = "PROOFING_CODE"),
                @FieldResult( name = "proofingUrlDate", column = "PROOFING_EXPIRY_DATE")
            }
        )
    }
 ),

 @SqlResultSetMapping( 
 name = EBook.EBOOK_RESULT_SET_MAPPING_MODEL,
    entities = {
        @EntityResult (
            entityClass = EBook.class,
            fields = {
                @FieldResult( name = "bookId",    column = "BOOK_ID"),
                @FieldResult( name = "isbn",      column = "ISBN"),
                @FieldResult( name = "status",    column = "STATUS"),
                @FieldResult( name = "seriesId",  column = "SERIES_CODE"),
                @FieldResult( name = "title",          column = "TITLE"),
                @FieldResult( name = "titleAlphasort",    column = "TITLE_ALPHASORT"),
                @FieldResult( name = "modifiedDate", column = "MODIFIED_DATE"),
                @FieldResult( name = "modelType", column = "MODEL_TYPE"),
                @FieldResult( name = "proofingUrl", column = "PROOFING_CODE"),
                @FieldResult( name = "proofingUrlDate", column = "PROOFING_EXPIRY_DATE")
            }
        )
    }
)
})

@Entity
@Table(name = "EBOOK")
public class EBook implements EBookComponent {

    public static final String COUNT_SEARCH_ALL = "countSearchAllEBookSQL";
    public static final String SEARCH_ALL = "searchAllEBookSQL";
        
    public static final String SEARCH_ALL_BY_BOOK_ID = "searchAllEBookByBookIdSQL";
    public static final String SEARCH_ALL_BY_BOOK_ID_WITH_MODEL = "searchAllEBookByBookIdSQLwithModel";
    public static final String UPDATE_EBOOK_BY_BOOK_ID = "updateEbookByBookId";
    public static final String UPDATE_EBOOK_STATUS_BY_BOOK_ID = "updateEBookStatusByBookId";
    public static final String UPDATE_EBOOK_DETAILS_BY_BOOK_ID = "updateEBookDetailsByBookId";
    public static final String UPDATE_EBOOK_PROOFING_CODE = "updateEBookProofingURLByBookId";
    public static final String UPDATE_EBOOK_MODEL_BY_BOOK_ID = "updateEBookModelByBookId";
    public static final String EBOOK_RESULT_SET_MAPPING = "searchEBookResult";
    public static final String EBOOK_RESULT_SET_MAPPING_MODEL = "searchEBookResultWithModel";
    
    public static final String EBOOK_STATUS_SUMMARY = "ebookStatusSummary";
    public static final String EBOOK_STATUS_SUMMARY_ALL = "ebookStatusSummaryAll";
    public static final String EBOOK_REPORT = "ebookReport";
    public static final String EBOOK_REPORT_ALL = "ebookReportAll";
    public static final String EBOOK_REPORT_COUNT = "ebookReportCount";
    public static final String EBOOK_REPORT_COUNT_ALL = "ebookReportCountAll";
    public static final String SEARCH_EBOOK_BY_ISBN = "searchEBookByIsbn";
    
    @Id
    @Column(name = "BOOK_ID")
    private String bookId;

    @Column(name = "ISBN")
    private String isbn;

    @Column(name = "STATUS")
    private int status;

    @Column(name = "SERIES_CODE")
    private String seriesId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "TITLE_ALPHASORT")
    private String titleAlphasort;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODEL_TYPE")
    private String modelType;

    @Column(name = "PROOFING_CODE")
    private String proofingCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PROOFING_EXPIRY_DATE")
    private Date proofingExpiryDate;

    @OneToMany(mappedBy = "book")
    private List<EBookContent> bookChapter;

    @OneToOne
    @JoinTable(name = "EBOOK", joinColumns = @JoinColumn(name = "ISBN", referencedColumnName = "ISBN"), inverseJoinColumns = @JoinColumn(name = "ISBN", referencedColumnName = "ISBN"))
    private List<EBookIsbnDataLoad> bookData;

    public List<EBookContent> getBookChapter() {
        return bookChapter;
    }

    public void setBookChapter(List<EBookContent> bookChapter) {
        this.bookChapter = bookChapter;
    }

    public String getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(String seriesId) {
        this.seriesId = seriesId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the modifiedDate
     */
    public Date getModifiedDate() {
        return modifiedDate;
    }

    /**
     * @param modifiedDate
     *            the modifiedDate to set
     */
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getTitleAlphasort() {
        return titleAlphasort;
    }

    public void setTitleAlphasort(String titleAlphasort) {
        this.titleAlphasort = titleAlphasort;
    }

    public List<EBookIsbnDataLoad> getBookData() {
        return bookData;
    }

    public void setBookData(List<EBookIsbnDataLoad> bookData) {
        this.bookData = bookData;
    }

    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof EBookComponent) {
            EBookComponent b = (EBookComponent) o;
            if (b.getBookId().equals(this.getBookId())) {
                result = true;
            }
        }
        return result;
    }

    public String getProofingCode() {
        return proofingCode;
    }

    public void setProofingCode(String proofingCode) {
        this.proofingCode = proofingCode;
    }

    public Date getProofingExpiryDate() {
        return proofingExpiryDate;
    }

    public void setProofingExpiryDate(Date proofingExpiryDate) {
        this.proofingExpiryDate = proofingExpiryDate;
    }
}
