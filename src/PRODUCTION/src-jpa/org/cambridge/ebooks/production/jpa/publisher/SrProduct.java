package org.cambridge.ebooks.production.jpa.publisher;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = SrProduct.COUNT_PRODUCTS,
			query = "SELECT COUNT(CBO_PRODUCT_GROUP) " + 
					"FROM SR_CBO_PRODUCT_GROUP_ORA" 
	),

	@NamedNativeQuery(
	        name = SrProduct.SELECT_PRODUCTS,
	        query = "SELECT CBO_PRODUCT_GROUP, CBO_PRODUCT_GROUP_LEGEND, PUBLISHER_CODE " +
	        		"FROM SR_CBO_PRODUCT_GROUP_ORA " +
	        		"ORDER BY CBO_PRODUCT_GROUP",
	        resultSetMapping = "searchSrProductResult"
	),
	
	@NamedNativeQuery(
	        name = SrProduct.FILTER_BY_PUBLISHER,
	        query = "SELECT CBO_PRODUCT_GROUP, CBO_PRODUCT_GROUP_LEGEND, PUBLISHER_CODE " +
	        		"FROM SR_CBO_PRODUCT_GROUP_ORA " +
	        		"WHERE PUBLISHER_CODE = ?1 ",
	        resultSetMapping = "searchSrProductResult"
	),
	
	@NamedNativeQuery(
	        name = SrProduct.GET_PUBLISHER,
	        query = "SELECT CBO_PRODUCT_GROUP, CBO_PRODUCT_GROUP_LEGEND, PUBLISHER_CODE " +
	        		"FROM SR_CBO_PRODUCT_GROUP_ORA " +
	        		"WHERE CBO_PRODUCT_GROUP = ?1 ",
	        resultSetMapping = "searchSrProductResult"
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchSrProductResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = SrProduct.class, 
	            fields = {
	                @FieldResult(name = "product_code", column = "CBO_PRODUCT_GROUP"),
	                @FieldResult(name = "product_name", column = "CBO_PRODUCT_GROUP_LEGEND"),
	                @FieldResult(name = "publisher_code", column = "PUBLISHER_CODE")
	            }
	        ) 
	    }
	)
})

@Entity
@Table ( name = "SR_CBO_PRODUCT_GROUP_ORA")
public class SrProduct implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String COUNT_PRODUCTS = "countProducts";
	public static final String SELECT_PRODUCTS = "selectProducts";
	public static final String FILTER_BY_PUBLISHER = "filterByPublisher";
	public static final String GET_PUBLISHER = "getPublisher";
	
	public SrProduct(){}
	public SrProduct(String product_code, String product_name, String publisher_code){
		this.product_code = product_code;
		this.product_name = product_name;
		this.publisher_code = publisher_code;
	}
	
	@Id
	@Column (name = "CBO_PRODUCT_GROUP")
	private String product_code;
	
	@Column (name = "CBO_PRODUCT_GROUP_LEGEND")
	private String product_name;
	
	@Column (name = "PUBLISHER_CODE")
	private String publisher_code;

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getPublisher_code() {
		return publisher_code;
	}

	public void setPublisher_code(String publisher_code) {
		this.publisher_code = publisher_code;
	}
	
	
}
