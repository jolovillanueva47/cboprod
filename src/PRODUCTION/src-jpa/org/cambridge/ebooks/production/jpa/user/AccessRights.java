package org.cambridge.ebooks.production.jpa.user;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;

import org.cambridge.ebooks.production.common.Status;


public class AccessRights implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* Production Administrator Access Rights */
	public static final int CAN_MANAGE_USERS			= 1;
	public static final int CAN_MANAGE_THIRD_PARTIES	= 2;
	public static final int CAN_VIEW_DTD				= 3;
	public static final int CAN_MANAGE_DELIVERY_CHANGES	= 12;
	public static final int CAN_VIEW_AUDIT_TRAIL		= 13;
	public static final int CAN_REGISTER_DOI			= 14;
//	public static final int CAN_SPD						= 19; //CJO SPECIFIC
	public static final int CAN_MANAGE_PUBLISHERS		= 30; //added for publisher module
	
	/* Default User Access Rights */
	public static final int CAN_PROOFREAD				= 4; /* Proofread accepted & Proofread with error */
	public static final int CAN_APPROVE					= 5; /* Approved & Reviewed with error */
	public static final int CAN_VIEW_ARCHIVE			= 6;
	public static final int CAN_DELIVER_DATA			= 7; /* Enable Deliver dropdown & button */
	public static final int CAN_DELETE					= 8; /* Enable Delete checkbox & button */
	public static final int CAN_PUBLISH					= 9; /* Enable Publish checkbox & button */
	public static final int CAN_LOAD_DATA				= 10;
	public static final int CAN_RUN_REPORTS				= 11;
	public static final int CAN_CITATION_REPORTS		= 18;
	public static final int CAN_SEARCH					= 15;
	public static final int CAN_PUBLISH_SCRIPT  		= 16;
	public static final int CAN_SITE_RELOAD		  		= 17;
	public static final int CAN_REVERT_STATUS			= 20;
	
	private boolean canManageUsers;
	private boolean canManageThirdParties;
	private boolean canViewDtd;
	private boolean canDeliveryChanges;
	private boolean canViewAuditTrail;
	private boolean canProofread;
	private boolean canApprove;
	private boolean canViewArchive;
	private boolean canDeliverData;
	private boolean canDelete;
	private boolean canPublish;
	private boolean canLoadData;
	private boolean canRunReports;
	private boolean canCitationReports;
	private boolean canSearch;
	private boolean canRegisterDOI;
	private boolean canPublishScript;
	private boolean canSiteReload;
//	private boolean canSPD;
	private boolean canRevertStatus;
	private boolean canManagePublishers;
	
	public AccessRights(String[] rights) {
		resolveRights(rights);
	}


	public AccessRights(String rights) {
		resolveRights(rights);
	}
	
	public boolean isChangeStatusAllowed(Status status) {
		switch (status) {
			
		case NOTLOADED:			//-1 "Not yet Loaded"
			return false;
		
		case LOADED:			//0 "Loaded for Proofreading"
			return false;
		
		case RELOADED:			//1 "Reloaded with Corrections"
			return false;
		
		case PROOFED:			//2 "Proofread Accepted"
			return canProofread;
		
		case REJECTED:			//3 "Proofread with Error"
			return canProofread;
		
		case APPROVE:			//4 "Approved"
			return canApprove;
		
		case DISAPPROVED:		//5 "Reviewed with Error"
			return canApprove;
		
		case REAPPROVE:			//6 "Reloaded for Approver"
			return false;
		
		case PUBLISHED:			//7 "Published"
			return canPublish;
		
		case UNPUBLISHED:		//8 "Unpublished"
			return canPublish;
		
		case EMBARGO:			//9 "Embargo"
			return canPublish;
		
		case RETURNTOUPLOADER:	//11 "Return to Uploader"
			return canApprove;
		
		case DELETED:			//100 "Deleted"
			return canDelete;
			
		default:
			return false;
		}
	}
	
	public String getRightsAsString() {
		StringBuilder rights = new StringBuilder();
		if (canManageUsers) rights.append(CAN_MANAGE_USERS + ",");
		if (canManageThirdParties) rights.append(CAN_MANAGE_THIRD_PARTIES + ",");
		if (canViewDtd) rights.append(CAN_VIEW_DTD + ",");
		if (canDeliveryChanges) rights.append(CAN_MANAGE_DELIVERY_CHANGES + ",");
		if (canViewAuditTrail) rights.append(CAN_VIEW_AUDIT_TRAIL + ",");
		if (canProofread) rights.append(CAN_PROOFREAD + ",");
		if (canApprove) rights.append(CAN_APPROVE + ",");
		if (canViewArchive) rights.append(CAN_VIEW_ARCHIVE + ",");
		if (canDeliverData) rights.append(CAN_DELIVER_DATA + ",");
		if (canDelete) rights.append(CAN_DELETE + ",");
		if (canPublish) rights.append(CAN_PUBLISH + ",");
		if (canLoadData) rights.append(CAN_LOAD_DATA + ",");
		if (canRunReports) rights.append(CAN_RUN_REPORTS + ",");
		if (canCitationReports) rights.append(CAN_CITATION_REPORTS + ",");
		if (canSearch) rights.append(CAN_SEARCH + ",");
		if (canRegisterDOI) rights.append(CAN_REGISTER_DOI + ",");
		if (canPublishScript) rights.append(CAN_PUBLISH_SCRIPT + ",");
		if (canSiteReload) rights.append(CAN_SITE_RELOAD + ",");
		if (canRevertStatus) rights.append(CAN_REVERT_STATUS + ",");
		if (canManagePublishers) rights.append(CAN_MANAGE_PUBLISHERS + ",");
//		if (canSPD) rights.append(CAN_SPD + ",");
		
		if (rights.length()>0)
			rights = new StringBuilder(rights.substring(0, rights.length()-1));
		return rights.toString();
	}

	private void resolveRights(String[] rights) {
		if (rights != null) {
			for (int i=0; i<rights.length; i++) {
				int r = 0;
				try {
					r = Integer.parseInt(rights[i]);
					resolveRights(r);
				} catch (Exception e) {
				}
			}
		}
	}


	private void resolveRights(String rights) {
		if (rights != null) {
			StringTokenizer a = new StringTokenizer(rights, ",");
			while (a.hasMoreTokens()) {
				int r = 0;
				try {
					r = Integer.parseInt(a.nextToken());
					resolveRights(r);
				} catch (Exception e) {
				}
			}
		}
	}
		
	private void resolveRights(int right) {
		switch(right) {
			case CAN_MANAGE_USERS:
				canManageUsers = true;
				break;
				
			case CAN_MANAGE_THIRD_PARTIES:
				canManageThirdParties = true;
				break;

			case CAN_VIEW_DTD:
				canViewDtd = true;
				break;
				
			case CAN_MANAGE_DELIVERY_CHANGES:
				canDeliveryChanges = true;
				break;

			case CAN_PROOFREAD:
				canProofread = true;
				break;
				
			case CAN_APPROVE:
				canApprove = true;
				break;
				
			case CAN_VIEW_ARCHIVE:
				canViewArchive = true;
				break;
				
			case CAN_DELIVER_DATA:
				canDeliverData = true;
				break;

			case CAN_VIEW_AUDIT_TRAIL:
				canViewAuditTrail = true;
				break;

			case CAN_REGISTER_DOI:
				canRegisterDOI = true;
				break;				

				
			case CAN_DELETE:
				canDelete = true;
				break;
				
			case CAN_PUBLISH:
				canPublish = true;
				break;
				
			case CAN_LOAD_DATA:
				canLoadData = true;
				break;
				
			case CAN_RUN_REPORTS:
				canRunReports = true;
				break;

			case CAN_CITATION_REPORTS:
				canCitationReports = true;
				break;

			case CAN_SEARCH:
				canSearch = true;
				break;
				
			case CAN_PUBLISH_SCRIPT:
				canPublishScript = true;
				break;
				
			case CAN_SITE_RELOAD:
				canSiteReload = true;
				break;
				
			case CAN_REVERT_STATUS:
				canRevertStatus = true;
				break;
				
			case CAN_MANAGE_PUBLISHERS:
				canManagePublishers = true;
				break;				

//			case CAN_SPD:
//				canSPD = true;
//				break;

			default: 
				break;
		}
	}
	
	public static LinkedHashMap<String, String> getDefaultAccessRights(){
	    
	    LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
//*	    map.put("10","Load Data");
//		map.put("19","Load SPD");
//*		map.put("4","Proofread Content");
//*		map.put("5","Approve Content");
//*		map.put("9","Publish Data");
//*		map.put("7","Deliver Data");
		map.put("6","View Archive Data");
		map.put("11","Run Reports");
//		map.put("18","Citation Reports");
//*		map.put("8","Delete Data");
//		map.put("15","Search");
		return map;
	}
	
	public static LinkedHashMap<String, String> getAdministratorAccessRights(){
	    
	    LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
	    map.put("3","View/Download DTD");
		map.put("1","Manage System Users");
		map.put("2","Manage Third Parties");
		map.put("12","Manage Proprietary Third Party");
		map.put("13","View Audit Trail");
		map.put("14","Register DOI");
		map.put("16","Publish Script");
		map.put("17","Site Reload");
		map.put("30","Manage Publishers");
		return map;
	}
	
	public boolean getCanApprove() {
		return canApprove;
	}

	public void setCanApprove(boolean canApprove) {
		this.canApprove = canApprove;
	}

	public boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	public boolean getCanDeliverData() {
		return canDeliverData;
	}

	public void setCanDeliverData(boolean canDeliverData) {
		this.canDeliverData = canDeliverData;
	}

	public boolean getCanLoadData() {
		return canLoadData;
	}

	public void setCanLoadData(boolean canLoadData) {
		this.canLoadData = canLoadData;
	}

	public boolean getCanManageThirdParties() {
		return canManageThirdParties;
	}

	public void setCanManageThirdParties(boolean canManageThirdParties) {
		this.canManageThirdParties = canManageThirdParties;
	}

	public boolean getCanManageUsers() {
		return canManageUsers;
	}

	public void setCanManageUsers(boolean canManageUsers) {
		this.canManageUsers = canManageUsers;
	}

	public boolean getCanProofread() {
		return canProofread;
	}

	public void setCanProofread(boolean canProofread) {
		this.canProofread = canProofread;
	}

	public boolean getCanPublish() {
		return canPublish;
	}

	public void setCanPublish(boolean canPublish) {
		this.canPublish = canPublish;
	}

	public boolean getCanRunReports() {
		return canRunReports;
	}

	public void setCanRunReports(boolean canRunReports) {
		this.canRunReports = canRunReports;
	}

	public boolean getCanViewArchive() {
		return canViewArchive;
	}
	
	public void setCanViewArchive(boolean canViewArchive) {
		this.canViewArchive = canViewArchive;
	}

	public boolean getCanViewDtd() {
		return canViewDtd;
	}

	public void setCanViewDtd(boolean canViewDtd) {
		this.canViewDtd = canViewDtd;
	}

	public boolean getCanDeliveryChanges() {
		return canDeliveryChanges;
	}

	public void setCanDeliveryChanges(boolean canDeliveryChanges) {
		this.canDeliveryChanges = canDeliveryChanges;
	}

	public boolean getCanViewAuditTrail() {
		return canViewAuditTrail;
	}

	public void setCanViewAuditTrail(boolean canViewAuditTrail) {
		this.canViewAuditTrail = canViewAuditTrail;
	}
	
	public boolean getCanSearch() {
		return canSearch;
	}
	
	public void setCanSearch(boolean canSearch) {
		this.canSearch = canSearch;
	}
	
	public boolean getCanPublishScript() {
		return canPublishScript;
	}
	
	public void setCanPublishScript(boolean canPublishScript) {
		this.canPublishScript = canPublishScript;
	}

	public boolean getCanRegisterDOI() {
		return canRegisterDOI;
	}

	public void setCanRegisterDOI(boolean canRegisterDOI) {
		this.canRegisterDOI = canRegisterDOI;
	}

	public boolean getCanSiteReload() {
		return canSiteReload;
	}

	public void setCanSiteReload(boolean canSiteReload) {
		this.canSiteReload = canSiteReload;
	}


	public boolean getCanCitationReports() {
		return canCitationReports;
	}


	public void setCanCitationReports(boolean canCitationReports) {
		this.canCitationReports = canCitationReports;
	}


	public boolean getCanRevertStatus() {
		return canRevertStatus;
	}


	public void setCanRevertStatus(boolean canRevertStatus) {
		this.canRevertStatus = canRevertStatus;
	}


	public boolean isCanManagePublishers() {
		return canManagePublishers;
	}


	public void setCanManagePublishers(boolean canManagePublishers) {
		this.canManagePublishers = canManagePublishers;
	}


//	public boolean getCanSPD() {
//		return canSPD;
//	}
//
//
//	public void setCanSPD(boolean canSPD) {
//		this.canSPD = canSPD;
//	}
	
}
