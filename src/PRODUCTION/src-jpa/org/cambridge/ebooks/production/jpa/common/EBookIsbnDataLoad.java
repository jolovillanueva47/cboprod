package org.cambridge.ebooks.production.jpa.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author jmendiola
 *			   
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
			name = EBookIsbnDataLoad.GET_PRODUCT_CODES,
			query = "SELECT ISBN, CBO_PRODUCT_GROUP, SUB_PRODUCT_GROUP_CODE " +
					"FROM EBOOK_ISBN_DATA_LOAD " +
					"WHERE ISBN = ?1 ",
			resultSetMapping = "searchEBookIsbnDataLoadResult"
	)
})
@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchEBookIsbnDataLoadResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = EBookIsbnDataLoad.class, 
	            fields = {
	                @FieldResult(name = "isbn", column = "ISBN"),
	                @FieldResult(name = "product", column = "CBO_PRODUCT_GROUP"),
	                @FieldResult(name = "subProduct", column = "SUB_PRODUCT_GROUP_CODE")
	            }
	        ) 
	    }
	)
})

@Entity
@Table(name = "EBOOK_ISBN_DATA_LOAD")
public class EBookIsbnDataLoad  {
	
	public static final String GET_PRODUCT_CODES = "getProductCodes";
	
	@Id
	@JoinColumn(name="isbn", nullable = false)
	@Column(name = "ISBN")
	private String isbn;
	
	//start
	
	@Column(name="PUBLISHER_CODE")
	private String publisher;
	
	@Column(name="CBO_PRODUCT_GROUP")
	private String product;
	
	@Column(name="SUB_PRODUCT_GROUP_CODE")
	private String subProduct;
	
	@Column(name="ONLINE_FLAG")
	private String onlineFlag;
	//end
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "PUB_DATE_PRINT")
	private Date pubPrint;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "PUB_DATE_ONLINE")
	private Date pubOnline;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Date getPubPrint() {
		return pubPrint;
	}

	public void setPubPrint(Date pubPrint) {
		this.pubPrint = pubPrint;
	}

	public Date getPubOnline() {
		return pubOnline;
	}

	public void setPubOnline(Date pubOnline) {
		this.pubOnline = pubOnline;
	}

	//added
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProduct() {
		return product;
	}
	
	public String getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(String subProduct) {
		this.subProduct = subProduct;
	}

	public String getOnlineFlag() {
		return onlineFlag;
	}

	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}
	
	//added
}
