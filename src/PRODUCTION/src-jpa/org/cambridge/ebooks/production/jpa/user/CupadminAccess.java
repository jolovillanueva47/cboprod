package org.cambridge.ebooks.production.jpa.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author jgalang
 * 
 */
@SqlResultSetMapping(
	name = "searchSystemUsersResult", 
	entities = { 
		@EntityResult(
	    	entityClass = User.class, 
	    	fields = {
	    		@FieldResult(name = "accessList", 	column = "access_list")
	    	}
		) 
	}
)
	
	
@Entity
@Table ( name = "CBO_CUPADMIN_ACCESS" )
public class CupadminAccess {
	
	@Id
	@Column( name = "USER_ID")
	private String userId;
	
	@Column( name = "ACCESS_LIST")
	private String accessList;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="MODIFIED_DATE")
    private Date modifiedDate;
	
	@Column( name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column( name = "PREFS")
	private String prefs;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccessList() {
		return accessList;
	}

	public void setAccessList(String accessList) {
		this.accessList = accessList;
	}

	public String getPrefs() {
		return prefs;
	}

	public void setPrefs(String prefs) {
		this.prefs = prefs;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
