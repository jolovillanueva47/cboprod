package org.cambridge.ebooks.production.jpa.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = SrEBookIsbn.GET_PRODUCT_CODE,
			query = "SELECT EAN_NUMBER, CBO_PRODUCT_GROUP " + 
					"FROM CORE_ISBN_CBO " +
					"WHERE EAN_NUMBER = ?1 ",
			resultSetMapping = "searchSrEBookIsbnResult"
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchSrEBookIsbnResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = SrEBookIsbn.class, 
	            fields = {
	                @FieldResult(name = "isbn", column = "EAN_NUMBER"),
	                @FieldResult(name = "productGroup", column = "CBO_PRODUCT_GROUP")
	            }
	        ) 
	    }
	)
})

@Entity
@Table ( name = "CORE_ISBN_CBO")
public class SrEBookIsbn {
	public static final String GET_PRODUCT_CODE = "getProductCode";
	
	@Id
	@Column (name = "EAN_NUMBER")
	private String isbn;
	
	@Column (name = "CBO_PRODUCT_GROUP")
	private String productGroup;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	
	

}
