package org.cambridge.ebooks.production.jpa.publisher;

//import java.util.List;

//import javax.faces.model.SelectItem;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

/**
 * 
 * @author jmendiola
 *  
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = PublisherMemberBean.GET_MEMBER,
		query = "SELECT *" +
				" FROM EBOOK_PUBLISHER_MEMBER" +
				" WHERE USER_ID = ?1",
		resultSetMapping = "publisherMemberResult"
	),
	
	@NamedNativeQuery(
		name = PublisherMemberBean.GET_ALL,
		query = "SELECT *" +
				" FROM EBOOK_PUBLISHER_MEMBER",
		resultSetMapping = "publisherMemberResult"
	),
	
	@NamedNativeQuery(
		name = PublisherMemberBean.UPDATE_MEMBER,
		query = "UPDATE EBOOK_PUBLISHER_MEMBER" +
				" SET PUBLISHER_ID = ?1"+
				" WHERE USER_ID = ?2"
	),
	
	@NamedNativeQuery(
			name = PublisherMemberBean.PUBLISHER_MEMBER_BY_USER_ID,
			query = "SELECT * " +
					"FROM EBOOK_PUBLISHER_MEMBER " +
					"WHERE user_id = ?1",
			resultSetMapping = "publisherMemberResult"
	)
})


@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "publisherMemberResult",
		entities = {
			@EntityResult(
				entityClass = PublisherMemberBean.class,
				fields = {
					@FieldResult(name = "publisher_id", column="PUBLISHER_ID"),
					@FieldResult(name = "user_id", column="USER_ID"),
				} //fields
			) //EntityResult
		} // entities
	) //SqlResultSetMapping
}) //SqlResultSetMappings

@Entity
@Table ( name = "EBOOK_PUBLISHER_MEMBER" )
public class PublisherMemberBean {
	
	public static final String GET_ALL = "PublisherMemberBean.selectAll";
	public static final String GET_MEMBER = "PublisherMemberBean.selectOne";
	public static final String ADD_MEMBER = "PublisherMemberBean.insertOne";
	public static final String UPDATE_MEMBER = "PublisherMemberBean.updateOne";
	public static final String PUBLISHER_MEMBER_BY_USER_ID = "publisherMemberByUserId";

	@Column (name="PUBLISHER_ID")
	private String publisher_id;
	
	@Id
	@Column (name="USER_ID")
	private String user_id;

	public String getPublisher_id() {
		return publisher_id;
	}

	public void setPublisher_id(String publisherId) {
		publisher_id = publisherId;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String userId) {
		user_id = userId;
	}

	
}
