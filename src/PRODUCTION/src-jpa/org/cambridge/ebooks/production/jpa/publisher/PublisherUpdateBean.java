package org.cambridge.ebooks.production.jpa.publisher;

/**
 * 
 * @author jmendiola
 * 
 */

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.ebook.EBookManagedBean;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

public class PublisherUpdateBean {
	private static final Log4JLogger LogManager = new Log4JLogger(PublisherUpdateBean.class);
	
	private Publisher publisher = new Publisher();
	private List<Publisher> publisherList;
	private SelectItem[] publisherList_Name;
	private PublisherMemberBean member = new PublisherMemberBean();
//	private int publisherList_NameSize;
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public void initPublisherDetails(ActionEvent event){
		LogManager.info(PublisherUpdateBean.class, "Initializing publisher details..");
		//System.out.println("GRAAAAAAAAAAAAAAAAAAAAAAAAAAWWWWWWWWWWWWWWWWWRRRRRRRRRRRRRRRRRR");
		try{
//			publisher = PersistenceUtil.searchEntity(new Publisher(), Publisher.GET_DETAILS, "1000");
			
//			System.out.println("publisher_id ="+publisher.getPublisher_id());
//			System.out.println("publisher_name ="+publisher.getPublisher_name());
//			System.out.println("publisher_email ="+publisher.getPublisher_email());
//			System.out.println("publisher_logo ="+publisher.getPublisher_logo());
			
			publisherList = PersistenceUtil.searchList(new Publisher(), Publisher.SELECT_PUBLISHERS);
			publisherList_Name = new SelectItem[publisherList.size()+1];
			int x = 1;
			//publisherList_Name[0] = new SelectItem("0","Please select publisher here");
			publisherList_Name[0] = new SelectItem("-1","All Publishers");
			for (Publisher pdb: publisherList){
				publisherList_Name[x] = pdb.getPublisher_name_si();
//				System.out.println("publisherList_SI[" + x + "] = "+publisherList_Name[x]);
//				System.out.println("pdb_id ="+pdb.getPublisher_id());
//				System.out.println("pdb_name ="+pdb.getPublisher_name());
//				System.out.println("pdb_email ="+pdb.getPublisher_email());
//				System.out.println("pdb_logo ="+pdb.getPublisher_logo());
				x++;
			}
		} catch (Exception e){
			System.out.println("fail :(");
		}
	}
	
	public void initAllPublishers(){
		LogManager.info(PublisherUpdateBean.class, "getAllPublishers()..");
		try
		{
			publisherList = PersistenceUtil.searchList(new Publisher(), Publisher.SELECT_PUBLISHERS);
			publisherList_Name = new SelectItem[publisherList.size()+1];
			int x = 1;
			//publisherList_Name[0] = new SelectItem("0","Please select publisher here");
			publisherList_Name[0] = new SelectItem("-1","All Publishers");
			for (Publisher pdb: publisherList)
			{
				publisherList_Name[x] = pdb.getPublisher_name_si();
				x++;
			}
		} 
		catch (Exception e){
			System.out.println("failed :(");
		}
	}
	
	public void initPublisherById(String publisherId){
		LogManager.info(PublisherUpdateBean.class, "getublisherById()..");
		try
		{
			publisher = PersistenceUtil.searchEntity(new Publisher(), Publisher.SEARCH_PUBLISHER_BY_ID, publisherId);
			publisherList_Name = null;
			//publisherList_Name[0] = publisher.getPublisher_name_si();
		} 
		catch (Exception e){
			System.out.println("failed :(");
		}
	}
	
	public void insertPublisherMember(PublisherMemberBean pbm){
		EntityManager em = emf.createEntityManager();
		
		try {
			em.getTransaction().begin();
			
			member = new PublisherMemberBean();
			member.setPublisher_id(pbm.getPublisher_id());
			member.setUser_id(pbm.getUser_id());
			
			em.persist(member);
			
			em.getTransaction().commit();
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	//TODO:
	public void updatePublisherMember(PublisherMemberBean pmb){
		try{
			if(pmb.getPublisher_id() != null && pmb.getUser_id() != null)
			{
				PersistenceUtil.updateEntity(PublisherMemberBean.UPDATE_MEMBER, pmb.getPublisher_id(), pmb.getUser_id());
				LogManager.info(PublisherUpdateBean.class, "Update of publisher details completed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getUserPublisher(String userId){
		String publisherId = "";
		PublisherMemberBean memberBean = new PublisherMemberBean();
		Publisher publisher = HttpUtil.getPublisherFromCurrentSession();
		
		if(publisher != null)
		{
			publisherId = publisher.getPublisherId();
		}
		else
		{
			memberBean = PersistenceUtil.searchEntity(new PublisherMemberBean(), PublisherMemberBean.GET_MEMBER, userId);
			if (memberBean != null) 
				publisherId = memberBean.getPublisher_id();
			else
				publisherId = EBookManagedBean.NO_PUBLISHER_FILTER;
		}
		
		LogManager.info(PublisherUpdateBean.class, "getUserPublisher: publisherId = " + publisherId);
		
		return publisherId;
	}
	
	public boolean isAlreadyAmember(String userId){
		try
		{
			PublisherMemberBean publisherMember = PersistenceUtil.searchEntity(new PublisherMemberBean(),PublisherMemberBean.PUBLISHER_MEMBER_BY_USER_ID, userId);
			if(publisherMember != null) return true;
		} 
		catch (Exception e) 
		{
			LogManager.info(PublisherUpdateBean.class, "isAlreadyAmember().. " + e.getMessage());
		}
		
		return false;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public List<Publisher> getPublisherList() {
		return publisherList;
	}

	public void setPublisherList(List<Publisher> publisherList) {
		this.publisherList = publisherList;
	}

	public PublisherMemberBean getMember() {
		return member;
	}

	public void setMember(PublisherMemberBean member) {
		this.member = member;
	}

	public SelectItem[] getPublisherList_Name() {
		return publisherList_Name;
	}

	public void setPublisherList_Name(SelectItem[] publisherListSI) {
		publisherList_Name = publisherListSI;
	}
	
	public int getPublisherList_NameSize() {
		if(publisherList_Name == null)
			return 0;
		else
		{
			return publisherList_Name.length;
		}
	}
	 
}
