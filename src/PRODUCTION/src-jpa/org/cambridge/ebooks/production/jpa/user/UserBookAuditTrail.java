package org.cambridge.ebooks.production.jpa.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;


/**
 * 
 * @author jgalang
 * 
 */

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "userBookAuditTrailResult", 
	    entities = {
	        @EntityResult(
	            entityClass = UserBookAuditTrail.class, 
	            fields = {
	            	@FieldResult(name = "eisbn", column = "EISBN")
	            }
	        )
	    }
	)
})

@Entity
@Table(name="AUDIT_TRAIL_BOOKS")
public class UserBookAuditTrail {

	@Id
	@Column(name="EISBN")
	private String eisbn;

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	
}
