package org.cambridge.ebooks.production.jpa.audittrail;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.content.ArticleStatus;
import org.cambridge.ebooks.production.jpa.user.User;

//@NamedNativeQueries({
//	@NamedNativeQuery(
//	        name = ArticleAuditTrail.RETRIEVE_ARTICLE_AUDIT_TRAIL,
//	        query = "SELECT DISTINCT AT.user_id, AT.audit_id, A.journal_id, A.component_id, A.file_id, A.title, " +
//	        		"A.volume_id, A.issue_id, AU.username, A.status, TO_CHAR(A.modified_date, 'DD-Mon-YYYY HH24:MI:SS '), " +
//	        		"AT.load_type, A.forthcoming, A.published, A.series_code, AT.remarks " +
//	        		"FROM article A, article_audit_trail AT, cupadmin_user AU  " +
//	        		"WHERE ROWNUM <= 800  " +
//	        		"AND AU.username <> 'testkoto' AND AU.username <> 'jcayetano'  " +
//	        		"AND AT.file_id = A.file_id " +
//	        		"AND AU.user_id = AT.user_id " +
//	        		"AND A.journal_id LIKE ?1 " +
//	        		"AND A.file_id LIKE ?2 " +
//	        		"AND A.series_code LIKE ?3 " +
//	        		"AND A.volume_id LIKE ?4 " +
//	        		"AND A.issue_id LIKE ?5 " +
//	        		"AND AU.username LIKE ?6 " +
//	        		"AND A.published LIKE ?7 " +
//	        		"AND A.forthcoming LIKE ?8 " +
//	        		"AND AT.audit_date >= TO_DATE( ?9 ,'DD-Mon-YYYY')  " +
//	        		"AND AT.audit_date <= TO_DATE( ?10 ,'DD-Mon-YYYY') + 1 " +
//	        		"ORDER BY A.file_id, A.volume_id, A.issue_id " ,
//	        resultSetMapping = "articleAuditTrailResult"
//	),
//	
//	@NamedNativeQuery(
//	        name = ArticleAuditTrail.RETRIEVE_ARTICLE_AUDIT_TRAIL2,
//	        query = "SELECT DISTINCT AT.user_id, AT.audit_id, A.journal_id, A.component_id, A.file_id, A.title, " +
//	        		"A.volume_id, A.issue_id, AU.username, AT.status, TO_CHAR(AT.audit_date, 'DD-Mon-YYYY HH24:MI:SS '), " +
//	        		"AT.load_type, A.forthcoming, A.published, A.series_code, AT.remarks " +
//	        		"FROM article A, article_audit_trail AT, cupadmin_user AU  " +
//	        		"WHERE ROWNUM <= 800  " +
//	        		"AND AU.username <> 'testkoto' AND AU.username <> 'jcayetano'  " +
//	        		"AND AT.file_id = A.file_id " +
//	        		"AND AU.user_id = AT.user_id " +
//	        		"AND A.journal_id LIKE ?1 " +
//	        		"AND A.file_id LIKE ?2 " +
//	        		"AND A.series_code LIKE ?3 " +
//	        		"AND A.volume_id LIKE ?4 " +
//	        		"AND A.issue_id LIKE ?5 " +
//	        		"AND AU.username LIKE ?6 " +
//	        		"AND A.published LIKE ?7 " +
//	        		"AND A.forthcoming LIKE ?8 " +
//	        		"AND AT.audit_date >= TO_DATE( ?9 ,'DD-Mon-YYYY')  " +
//	        		"AND AT.audit_date <= TO_DATE( ?10 ,'DD-Mon-YYYY') + 1 " +
//	        		"ORDER BY A.file_id, A.volume_id, A.issue_id, AT.audit_id " ,
//	        resultSetMapping = "articleAuditTrailResult"
//	),
//	
//	@NamedNativeQuery(
//	        name = ArticleAuditTrail.RETRIEVE_ARTICLE_AUDIT_TRAIL3,
//	        query = "select distinct " +
//	        		"AU.user_id, " +
//	        		"case " +
//	        		"when B.published = 'N' then 0 " +
//	        		"else AT.audit_id " +
//	        		"end audit_id, " +
//	        		"B.eisbn, B.chapter_id, " +
//	        		"AU.username, " +
//	        		"case " +
//	        		"when B.published = 'N' then B.status " +
//	        		"else AT.status " +
//	        		"end status, " +
//	        		"case " +
//	        		"when B.published = 'N' then B.modified_date " +
//	        		"else AT.audit_date " +
//	        		"end audit_date, " +
//	        		"B.published, B.series_code, AT.remarks " +
//	        		"from audit_trail_books AT, books B, cupadmin_user AU " +
//	        		"where AT.chapter_id = B.chapter_id " +
//	        		"and AU.user_id = (case when AT.user_id is NULL then '432084' else AT.user_id end) " +
//	        		"and AU.username <> 'testkoto' and AU.username <> 'jcayetano' " +
//	        		"AND AT.audit_date >= TO_DATE( '5-Mar-2009' ,'DD-Mon-YYYY') " +
//	        		"AND AT.audit_date <= TO_DATE( '10-Mar-2009' ,'DD-Mon-YYYY') + 1 " +
//	        		"and ROWNUM <=200 " +
//	        		"order by B.chapter_id, " +
//	        		"case " +
//	        		"when B.published = 'N' then 0 " +
//	        		"else AT.audit_id " +
//	        		"end " ,
//	        resultSetMapping = "articleAuditTrailResult4"
//	),
//	@NamedNativeQuery(
//	        name = ArticleAuditTrail.RETRIEVE_ARTICLE_AUDIT_TRAIL4,
//	        query = "select distinct " +
//	        		"AU.user_id, " +
//	        		"case " +
//	        		"when A.forthcoming = 'Y' and A.published = 'N' then 0 " +
//	        		"else AT.audit_id " +
//	        		"end audit_id, " +
//	        		"A.journal_id, A.component_id, A.file_id, A.title, A.volume_id, " +
//	        		"A.issue_id, AU.username, " +
//	        		"case " +
//	        		"when A.forthcoming = 'Y' and A.published = 'N' then A.status " +
//	        		"else AT.status " +
//	        		"end status, " +
//	        		"case " +
//	        		"when A.forthcoming = 'Y' and A.published = 'N' then A.modified_date " +
//	        		"else AT.audit_date " +
//	        		"end audit_date, " +
//	        		"AT.load_type, A.forthcoming, A.published, A.series_code, AT.remarks " +
//	        		"from article_audit_trail AT, article A, cupadmin_user AU " +
//	        		"where AT.file_id = A.file_id " +
//	        		"and AU.user_id = (case when AT.user_id is NULL then '432084' else AT.user_id end) " +
//	        		"and AU.username <> 'testkoto' and AU.username <> 'jcayetano' " +
//	        		"AND AT.audit_date >= TO_DATE( '5-Mar-2007' ,'DD-Mon-YYYY') " +
//	        		"AND AT.audit_date <= TO_DATE( '5-Aug-2007' ,'DD-Mon-YYYY') + 1 " +
//	        		"and ROWNUM <=200 " +
//	        		"order by A.file_id, A.volume_id, A.issue_id, " +
//	        		"case " +
//	        		"when A.forthcoming = 'Y' and A.published = 'N' then 0 " +
//	        		"else AT.audit_id " +
//	        		"end " ,
//	        resultSetMapping = "articleAuditTrailResult3"
//	)
//})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "articleAuditTrailResult", 
	    entities = {
	        @EntityResult(
	            entityClass = ArticleAuditTrail.class, 
	            fields = {
	            	@FieldResult(name = "audit_id", 	column = "AUDIT_ID"),
	                @FieldResult(name = "status", 		column = "STATUS"),
	                @FieldResult(name = "load_type", 	column = "LOAD_TYPE"),
	                @FieldResult(name = "remarks", 		column = "REMARKS"),
	                @FieldResult(name = "audit_date", 	column = "AUDIT_DATE"),
	            }
	        )
	    }
	),
	@SqlResultSetMapping(
	    name = "articleAuditTrailResult2", 
	    entities = {
	        @EntityResult(
	            entityClass = ArticleAuditTrail.class, 
	            fields = {
	            	@FieldResult(name = "audit_id", 	column = "AUDIT_ID"),
	            	@FieldResult(name = "file_id", 		column = "FILE_ID"),
	                @FieldResult(name = "user_id", 		column = "USER_ID"),
	                @FieldResult(name = "status", 		column = "STATUS"),
	                @FieldResult(name = "load_type", 	column = "LOAD_TYPE"),
	                @FieldResult(name = "remarks", 		column = "REMARKS"),
	                @FieldResult(name = "audit_date", 	column = "AUDIT_DATE"),
	            }
	        )
	    }
	),
	@SqlResultSetMapping(
		    name = "articleAuditTrailResult3", 
		    entities = {
		        @EntityResult(
		            entityClass = ArticleAuditTrail.class, 
		            fields = {
		            	@FieldResult(name = "audit_id", 	column = "AUDIT_ID"),
		                @FieldResult(name = "status", 		column = "STATUS"),
		                @FieldResult(name = "load_type", 	column = "LOAD_TYPE"),
		                @FieldResult(name = "remarks", 		column = "REMARKS"),
		                @FieldResult(name = "audit_date", 	column = "AUDIT_DATE"),
		            }
		        ),
		        @EntityResult(
		            entityClass = Article.class, 
		            fields = {
		            	@FieldResult(name = "component_id",	column = "COMPONENT_ID"),
		                @FieldResult(name = "journal_id", 	column = "JOURNAL_ID"),
		                @FieldResult(name = "file_id", 		column = "FILE_ID"),
		                @FieldResult(name = "series_id", 	column = "SERIES_CODE"),
		                @FieldResult(name = "volume_id", 	column = "VOLUME_ID"),
		                @FieldResult(name = "issue_id", 	column = "ISSUE_ID"),
		                @FieldResult(name = "status", 		column = "STATUS"),
		                @FieldResult(name = "modified_date",column = "MODIFIED_DATE"),
		                @FieldResult(name = "title",		column = "TITLE"),
		                @FieldResult(name = "forthcoming",	column = "FORTHCOMING"),
		                @FieldResult(name = "published",	column = "PUBLISHED"),
		            }
		        ),
		        @EntityResult(
	        		entityClass = User.class, 
		            fields = {
	        			@FieldResult(name = "userId", 		column = "USER_ID"),
		                @FieldResult(name = "username", 	column = "USERNAME"),
		            }
		        )
		    }
		)
})


@Entity
@Table ( name = "ARTICLE_AUDIT_TRAIL" )
public class ArticleAuditTrail {

//	public static final String RETRIEVE_ARTICLE_AUDIT_TRAIL = "retrieveArticleAuditTrail";
//	public static final String RETRIEVE_ARTICLE_AUDIT_TRAIL2 = "retrieveArticleAuditTrail2";
//	public static final String RETRIEVE_ARTICLE_AUDIT_TRAIL3 = "retrieveArticleAuditTrail3";
//	public static final String RETRIEVE_ARTICLE_AUDIT_TRAIL4 = "retrieveArticleAuditTrail4";
	
	//@Id
	@Column( name = "AUDIT_ID")
	private int audit_id;
	
	@Id
	@Column( name = "FILE_ID")
	private String file_id;

//	@Column( name = "USER_ID")
//	private String user_id;
	
	@Column( name = "STATUS")
	private String status;
	
	@Column( name = "LOAD_TYPE")
	private String load_type;
	
	@Column( name = "REMARKS")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date audit_date;
	
//	@OneToOne
//	@PrimaryKeyJoinColumn
	
	//@JoinColumn(name = "FILE_ID", columnDefinition="VARCHAR2(20)", referencedColumnName="FILE_ID")
	@ManyToMany
	private List<Article> article;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getAudit_date() {
		return audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}
	
	public List<Article> getArticle() {
		return article;
	}

	public void setArticle(List<Article> article) {
		this.article = article;
	}

	public int getAudit_id() {
		return audit_id;
	}

	public void setAudit_id(int audit_id) {
		this.audit_id = audit_id;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getStatus() {
		return ArticleStatus.getStatus(status);
	}

	public void setStatus(String status) {
		this.status = ArticleStatus.getStatus(status);
	}

	public String getLoad_type() {
		return load_type;
	}

	public void setLoad_type(String load_type) {
		this.load_type = load_type;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
