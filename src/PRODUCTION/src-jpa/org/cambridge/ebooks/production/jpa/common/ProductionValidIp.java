package org.cambridge.ebooks.production.jpa.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@NamedNativeQueries( {
	@NamedNativeQuery(
			name  = ProductionValidIp.SELECT_ALL_IP,
			query = "SELECT * FROM oraebooks.ebook_production_valid_ip " +
					"order by ip_address",
			        resultSetMapping =  ProductionValidIp.IP_ADDRESS_RESULT
	)
}
)

@SqlResultSetMapping( 
	name = ProductionValidIp.IP_ADDRESS_RESULT,
	entities = {
		@EntityResult (
			entityClass = ProductionValidIp.class,
			fields = {
				@FieldResult( name = "ip_address",    column = "IP_ADDRESS"),
				@FieldResult( name = "exclude",      column = "EXCLUDE")
			}
		)
	}
 )

@Entity
@Table(name = "EBOOK_PRODUCTION_VALID_IP")
public class ProductionValidIp {
	
	public static final String SELECT_ALL_IP = "selectAllIP";
	public static final String IP_ADDRESS_RESULT = "productionValipIpResult";
	@Id
	@Column(name = "IP_ADDRESS")
	private String ip_address;
	
	@Column(name = "EXCLUDE")
	private boolean exclude;
	
	public ProductionValidIp(){}
	public ProductionValidIp(String ip, boolean ex){
		this.ip_address = ip;
		this.exclude = ex;
	}
	
	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public boolean isExclude() {
		return exclude;
	}

	public void setExclude(boolean exclude) {
		this.exclude = exclude;
	}


	
	
}
