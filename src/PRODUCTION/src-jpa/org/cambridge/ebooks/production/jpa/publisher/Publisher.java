package org.cambridge.ebooks.production.jpa.publisher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * 
 * @author rvillamor
 * 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
			name = Publisher.COUNT_PUBLISHERS,
			query = "SELECT COUNT(publisher_id) " + 
					"FROM ebook_publisher_details" 
	),
//	name = PublisherDetailsBean.GET_ALL		
	@NamedNativeQuery(
	        name = Publisher.SELECT_PUBLISHERS,
	        query = "SELECT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " +
	        		"FROM ebook_publisher_details " +
	        		"WHERE active = 'Y' " +
	        		"ORDER BY publisher_name",
	        resultSetMapping = "searchPublisherResult"
	),
	
//	name = PublisherDetailsBean.GET_DETAILS
	@NamedNativeQuery(
	        name = Publisher.SEARCH_PUBLISHER_BY_ID,
	        query = "SELECT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " +
	        		"FROM ebook_publisher_details " +
	        		"WHERE publisher_id = ?1 " +
	        		"AND active = 'Y'",
	        resultSetMapping = "searchPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = Publisher.SEARCH_PUB_ORDERBY_NAME_ASC,
	        query = "SELECT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " +
    				"FROM ebook_publisher_details " +
    				"WHERE active = 'Y' " +
    				"ORDER BY publisher_name ASC",
	        resultSetMapping = "searchPublisherResult"
	),
	

	@NamedNativeQuery(
	        name = Publisher.SEARCH_PUB_ORDERBY_EMAIL_ASC,
	        query = "SELECT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " +
    				"FROM ebook_publisher_details " +
    				"WHERE active = 'Y' " +
    				"ORDER BY publisher_email ASC",
	        resultSetMapping = "searchPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = Publisher.SEARCH_PUBLISHER_BY_USERID,
	        query = "SELECT DISTINCT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " +
    				"FROM ebook_publisher_details pd " +
    				"JOIN ebook_publisher_member pm " +
    				"USING (publisher_id) " +
    				"WHERE pm.user_id = ?1 " +
    				"AND pd.active = 'Y' " +
    				"ORDER BY pd.publisher_email ASC",
	        resultSetMapping = "searchPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = Publisher.SEARCH_PUBLISHER_BY_NAME_OR_EMAIL,
	        query = "SELECT * " +
    				"FROM ebook_publisher_details " +
    				"WHERE (publisher_name = ?1 OR publisher_email = ?2) AND active = 'N'",
	        resultSetMapping = "searchPublisherResult"
	),
	
	@NamedNativeQuery(
	        name = Publisher.SEARCH_MAX_PUBLISHER,
	        query = "SELECT publisher_id, publisher_name, publisher_email, publisher_logo, modified_by, modified_date " + 
	        		"FROM ebook_publisher_details " +
	        		"WHERE active = 'Y' " +
	        		"AND rownum = 1",
//	        		"WHERE UPPER(publisher_name) = 'CBO'",
	        resultSetMapping = "searchPublisherResult"
	),

})

@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "searchPublisherResult", 
	    entities = { 
	        @EntityResult(
	            entityClass = Publisher.class, 
	            fields = {
	                @FieldResult(name = "publisherId", column = "PUBLISHER_ID"),
	                @FieldResult(name = "name", column = "PUBLISHER_NAME"),
	                @FieldResult(name = "email", column = "PUBLISHER_EMAIL"),
	                @FieldResult(name = "logo", column = "PUBLISHER_LOGO"),
	                @FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
	                @FieldResult(name = "modifiedDate", column = "MODIFIED_DATE")
	            }
	        ) 
	    }
	)
})


@Entity
@Table ( name = "EBOOK_PUBLISHER_DETAILS" )
public class Publisher implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String COUNT_PUBLISHERS = "publisherCount";

	//public static final String SEARCH_ALL_PUBLISHER = "searchAllPublisher";
	public static final String SELECT_PUBLISHERS = "selectPublishers";
	public static final String SEARCH_BY_PUBLISHER = "searchByPublisher";
	public static final String SEARCH_PUBLISHER_BY_EMAIL = "publisherSearchByEmail";
	public static final String SEARCH_PUBLISHER_BY_ID = "publisherSearchById";
	public static final String SEARCH_PUB_ORDERBY_NAME_ASC = "selectPubOrderByNameAsc";
	public static final String SEARCH_PUB_ORDERBY_EMAIL_ASC = "selectPubOrderByEmailAsc";
	public static final String SEARCH_PUBLISHER_BY_USERID = "selectPubByUserId";
	public static final String SEARCH_MAX_PUBLISHER = "selectMaxPublisher";
	public static final String SEARCH_PUBLISHER_BY_NAME_OR_EMAIL = "selectPublisherByNameOrEmail";
	
	public static final String SORT_BY_PUBLISHER = "Publisher";
	public static final String SORT_BY_EMAIL = "Email";
	
	public Publisher() {}
	public Publisher(String publisherId) { this.publisherId = publisherId; }
	
	@Id
    @GeneratedValue(generator="EBOOK_PUBLISHER_DETAILS_SEQ")
    @SequenceGenerator(name="EBOOK_PUBLISHER_DETAILS_SEQ", sequenceName="EBOOK_PUBLISHER_DETAILS_SEQ", allocationSize=1)
	@Column( name = "PUBLISHER_ID")
	private String publisherId;

	@Column( name = "PUBLISHER_NAME", unique = true)
	private String name;
	
	@Column( name = "PUBLISHER_EMAIL", unique = true)
	private String email;
	
	@Column( name = "PUBLISHER_LOGO", unique = true)
	private String logo;
	   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="MODIFIED_DATE")
    private Date modifiedDate;
	
	@Column( name = "MODIFIED_BY")
	private String modifiedBy;
	
	private String active;
	
//	@OneToMany(mappedBy="publisher", cascade=CascadeType.ALL)
	@Transient
	private List<PublisherIsbn> publisherIsbn = new ArrayList<PublisherIsbn>();
	
	@Transient
	private String isbn;
	
	@Transient
	private String isbns;
		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	public List<PublisherIsbn> getPublisherIsbn() {
		return publisherIsbn;
	}
	
	public void setPublisherIsbn(List<PublisherIsbn> publisherIsbn) {
		this.publisherIsbn = publisherIsbn;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getIsbns() {
		return isbns;
	}
	public void setIsbns(String isbns) {
		this.isbns = isbns;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	public SelectItem getPublisher_name_si(){
		return new SelectItem(publisherId, name);
	}
	
	public SelectItem getPublisher_id_si(){
		return new SelectItem(publisherId);
	}
		
	
}
