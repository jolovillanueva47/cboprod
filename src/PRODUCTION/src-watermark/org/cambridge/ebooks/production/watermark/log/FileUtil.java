package org.cambridge.ebooks.production.watermark.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtil {
	public static void generateFile(StringBuffer sb, String path) throws IOException{
		FileWriter fstream = new FileWriter(path);
		try{
			BufferedWriter out = new BufferedWriter(fstream);
			try{
				out.write(sb.toString());			
			}finally{
				out.close();
			}
		}finally{
			fstream.close();
		}	    
	}
	
	public static void generateFileWithRename(StringBuffer sb, String path) throws IOException{		
		String newFilename = newFileName(path);		
		generateFile(sb, newFilename);				
	}
	
	
	
	
	/* private methods */
	
	
	private static final String REGEX_SPLIT_PATTERN = "--(?=[0-9]{1,})";
	
	
	
	private static String newFileName(String path){
		String _path = path;
		File file = new File(_path);
		if(file.isFile()){
			if(patternFound(REGEX_SPLIT_PATTERN, _path )){
				String[] getCount = _path.split(REGEX_SPLIT_PATTERN, 2);				
				String acCount = getCount[1];
				int acCountInt = Integer.parseInt(acCount);				
				_path = getCount[0] + "--" + (acCountInt + 1);
			}else{				
				_path = _path + "--" + 1;
			}
			_path = newFileName(_path);
		}		
		return _path;
	}
	
	private static boolean patternFound(String pattern, String input){
		Pattern p = Pattern.compile(pattern);
	    Matcher m = p.matcher(input);
	    return m.find();
	}
	
	public static void main(String[] args) throws IOException {
		StringBuffer sb = new StringBuffer();		
		generateFileWithRename(sb, "C:/trash/text.txt--1");		
		
		//System.out.println(patternFound(REGEX_SPLIT_PATTERN,"C:/trash/text.txt--1"));
		//String[] test = "C:/trash/text.txt--1".split("--(?=[0-9]{1,})");
		//System.out.println(test[0]);
		//System.out.println(test[1]);
	}
}
