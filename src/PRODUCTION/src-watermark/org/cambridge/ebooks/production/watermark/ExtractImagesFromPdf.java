package org.cambridge.ebooks.production.watermark;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import org.cambridge.ebooks.production.watermark.bean.PdfJpegFilenameBean;
import org.cambridge.ebooks.production.watermark.log.DailyLogger;

public class ExtractImagesFromPdf {

	public static void extract(File sourceFolder, ArrayList<PdfJpegFilenameBean> toBeProcessedList, 
			String convertCmd, String gsCmd, StringBuffer sb) {
		String copyrightStatement = "Cambridge Books Online \u00A9 Cambridge University Press, "+getYear();
		
		try{
			createImages(sourceFolder, toBeProcessedList, convertCmd, gsCmd, copyrightStatement, sb);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void createImages(File sourceFolder, ArrayList<PdfJpegFilenameBean> toBeProcessedList, String convertCmd, String gsCmd,String copyrightStatement, StringBuffer sb) throws IOException, InterruptedException {
//		String [] pdfFilenameList  = getPdfFileNames(sourceFolder);
		for(PdfJpegFilenameBean fileBean : toBeProcessedList){
			DailyLogger.append(sb, "-- Images Processing START: " + fileBean.getPdfFilename());
			//create the TIF files
			String cmd = gsCmd + " -dSAFER -dBATCH -dNOPAUSE -sOutputFile="+tifName(fileBean.getPdfFilename())+
				" -sDEVICE=tiff24nc -dTextAlphaBits=1 -dGraphicsAlphaBits=1 -r600 -dFirstPage=1 -dLastPage=1 -sCompression=pack "+fileBean.getPdfFilename();
			executeCommandLine(sourceFolder, cmd, sb);
			
			// merged watermark and convert commands
			String [] cmd2 = {convertCmd , tifName(fileBean.getPdfFilename()), 
				"-pointsize", "20", "-geometry", "960x2000", "-font", System.getProperty("source.location")+"arial.ttf", "-undercolor", "white",
				"-gravity", "South", "-annotate", "+0+35", "Cambridge Books Online \u00A9 Cambridge University Press, "+getYear()+"",
				"-flatten", //"-roll", "+0+50",
				"-density", "120", "-quality", "95", "-depth", "8", "-type", "TrueColor", fileBean.getJpegFilename()};
			executeCommandLine(sourceFolder, cmd2, sb);
			DailyLogger.append(sb, "-- Images Processing END: " + fileBean.getPdfFilename()+"\n");
		}
		
		//delete the tif files in the folder
		deleteTifs(sourceFolder, getTifFileNames(sourceFolder));
		
	}
	
	public static String tifName(String pdfFilename){
		return pdfFilename.replace(".pdf", ".tif");
	}
	
//	public static String jpgName(String pdfFilename){
//		return pdfFilename.substring(0, pdfFilename.indexOf("_")) + "_abstract_CBO.jpg";
//	}
	
	public static String[] getPdfFileNames(File sourceFolder){
		FilenameFilter filter = new FileFilter("pdf");
		String s[] = sourceFolder.list(filter);
//		for (int i=0; i < s.length; i++) { 
//			System.out.println(s[i]);
//		}
		return s;
	}
	
	public static void deleteTifs(File sourceFolder, String[] fileList){
		for(String tifFile: fileList){
			File toDelete = new File(sourceFolder+System.getProperty("file.separator")+tifFile);
			if(toDelete.isFile()){
				toDelete.delete();
			}
		}
	}
	public static String[] getTifFileNames(File sourceFolder){
		FilenameFilter filter = new FileFilter("tif");
		return sourceFolder.list(filter);
	}
	
	public static void executeCommandLine(File targetFolder, String cmd, StringBuffer sb) throws IOException, InterruptedException{
		Process convert = Runtime.getRuntime().exec(cmd, null, targetFolder);
			
			//CODEBLOCK#2 START 
			//This code will ensure that the previous command will finish before processing anything else
			int result = convert.waitFor();
			
			//incase it doesn't, this code will catch the error
			if (result != 0) {
				InputStreamReader reader = new InputStreamReader(convert.getErrorStream());
				StringBuilder buffer = new StringBuilder();
				char[] buf = new char[16384];
				while (true) {
					int read = reader.read(buf, 0, 16384);
					if (read == -1)
						break;
					buffer.append(buf, 0, read);
				}
				
				throw new IOException("Error in conversion: " + buffer.toString());
			}
			//CODEBLOCK#2 END
			
			else {
				DailyLogger.append(sb, "DONE:" + cmd); //Verify that the processing finished first before doing any more java stuff
			}
	}
	
	public static void executeCommandLine(File targetFolder, String[] cmd, StringBuffer sb) throws IOException, InterruptedException{
		Process convert = Runtime.getRuntime().exec(cmd, null, targetFolder);
		//CODEBLOCK#2 START 
		//This code will ensure that the previous command will finish before processing anything else
		int result = convert.waitFor();
		
		//incase it doesn't, this code will catch the error
		if (result != 0) {
			InputStreamReader reader = new InputStreamReader(
					convert.getErrorStream());
			StringBuilder buffer = new StringBuilder();
			char[] buf = new char[16384];
			while (true) {
				int read = reader.read(buf, 0, 16384);
				if (read == -1)
					break;
				buffer.append(buf, 0, read);
			}
			
			throw new IOException("Error in conversion: " + buffer.toString());
		}
		//CODEBLOCK#2 END
			
		else {
			String log = "DONE: "; //Verify that the processing finished first before doing any more java stuff
			for(String str:cmd){
				log += str+" ";
			}
			DailyLogger.append(sb, log); 
		}
	}
	
	public static int getYear(){ 
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR);
	}

}
