package org.cambridge.ebooks.production.watermark.log;

import java.sql.Timestamp;

public class DailyLogger {
	public static void createLogHeader(StringBuffer sb){
		String txt = "***************************************** \n";
		txt+= "CBO Watermark Jar Executing on: " + new Timestamp(new java.util.Date().getTime()) + "\n";
		sb.append(txt);
		System.out.println(txt);
	}
	
	public static void append(StringBuffer sb, String str){
		System.out.println(str);
		sb.append(str+"\n");
	}
	
//	public static void createLogBody(StringBuffer sb, CrossrefInfoBean b){
//		sb.append(b.toString() + "\n");
//	}
	
	public static void createLogEnd(StringBuffer sb){
		String txt = "********************** FIN: "+ new Timestamp(new java.util.Date().getTime()) + "\n";
		sb.append(txt);
		System.out.println(txt);
	}	
	
	public static String generateFilename(String loc){
		/*generate date format*/
		String currDate = DateUtil.getCurrentDate("yyyy-MMMMM-dd");
		
		String filename = "cbo.watermark." + currDate;
		
		return loc + filename;
	}
}
