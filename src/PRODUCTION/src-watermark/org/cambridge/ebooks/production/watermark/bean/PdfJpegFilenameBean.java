package org.cambridge.ebooks.production.watermark.bean;

public class PdfJpegFilenameBean {
	private String pdfFilename;
	private String jpegFilename;
	
	public PdfJpegFilenameBean(String pdfFilename, String jpegFilename) {
		super();
		this.pdfFilename = pdfFilename;
		this.jpegFilename = jpegFilename;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getJpegFilename() {
		return jpegFilename;
	}

	public void setJpegFilename(String jpegFilename) {
		this.jpegFilename = jpegFilename;
	}
	
	

}
