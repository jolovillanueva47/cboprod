package org.cambridge.ebooks.production.watermark;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.cambridge.ebooks.production.watermark.log.*;
import org.cambridge.ebooks.production.watermark.bean.PdfJpegFilenameBean;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WatermarkMain {
	
	public static void main(String[]args) throws IOException, Exception{
		StringBuffer sb = new StringBuffer();
		DailyLogger.createLogHeader(sb);
		try {
			init(args);
			ArrayList<String> toProcessList = getIsbnList(args);
			
			String convertCmd = System.getProperty("cmd.convert");
			String gsCmd = System.getProperty("cmd.gs");
			
			DailyLogger.append(sb, "--- Start Processing ---");
			for(String isbn : toProcessList){
				DailyLogger.append(sb, "\n*** ISBN: "+isbn);
				String folderPath = IsbnContentDirUtil.getPath(isbn);
				String xmlFilename = isbn + ".xml";
				if(new File(folderPath).isDirectory()){
					ArrayList<PdfJpegFilenameBean> toBeProcessedList = getPdfFilenameList(folderPath + xmlFilename, sb);
					ExtractImagesFromPdf.extract(new File(folderPath), toBeProcessedList, convertCmd, gsCmd, sb);
				} else {
					DailyLogger.append(sb, "ERROR: "+isbn + " does not exist!");
				}
			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			DailyLogger.append(sb, "ERROR: "+ e.getMessage());
		} finally {
			DailyLogger.createLogEnd(sb);
//			FileUtil.generateFileWithRename(sb, DailyLogger.generateFilename(System.getProperty("log.location")));
		}
		System.out.println("Watermark Done!");
		
	}
	
	public static void init(String[]args) throws Exception {
		areAllDirFiles(args);
		initProperties(args);
	}
	
	public static ArrayList<String> getIsbnList(String[]args) throws IOException{
		BufferedReader input =  new BufferedReader(new FileReader(args[1]));
    	String line = null;
    	
    	ArrayList<String> isbnList = new ArrayList<String>();
    	while (( line = input.readLine()) != null){
    		isbnList.add(line.trim());
		}
		return isbnList;
	}
	
	private static void areAllDirFiles(String[] args) throws Exception{
		for(String loc : args){
			File file = new File(loc);
			if(!(file.isDirectory() || file.isFile() || "null".equals(loc))){
				throw new Exception(loc + " is not a directory!");
			}			
		}		
	}
	
	private static void initProperties(String[] args) throws FileNotFoundException, IOException {		
		Properties properties = new Properties(System.getProperties());		
		properties.load(new FileInputStream(args[0]));
		System.setProperties(properties);		
		
		//required for log4j
//		System.setProperty("jboss.server.home.dir", "/app/jboss-5.1.0.GA/server/production/log");
		
	}

	
	public static ArrayList<PdfJpegFilenameBean> getPdfFilenameList(String xmlPath, StringBuffer sb) throws SAXException, IOException, ParserConfigurationException{
		DailyLogger.append(sb, "--- Parsing XML...");
		
		ArrayList<PdfJpegFilenameBean> toBeProcessedList = new ArrayList<PdfJpegFilenameBean>();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse (new File(xmlPath));
		
		NodeList contentItemList = doc.getElementsByTagName("content-item");
		
		// This next section will get all the childnodes that are in <content-item>
		int size= contentItemList.getLength();
		for(int x=0; x<size; x++) {
			NodeList contentItemTagList = contentItemList.item(x).getChildNodes();
			String pdfFilename = "";
			String jpegFilename = "";
			
			// Check if both <pdf> and <abstract> exists in the <content-item>
			int tagListCount = contentItemTagList.getLength();
			for(int y=0; y<tagListCount; y++){
				if(contentItemTagList.item(y).getNodeName().equals("pdf")){
					pdfFilename = contentItemTagList.item(y).getAttributes().getNamedItem("filename").getTextContent();
				} else if(contentItemTagList.item(y).getNodeName().equals("abstract")){
					jpegFilename = contentItemTagList.item(y).getAttributes().getNamedItem("alt-filename").getTextContent();
				}
			}
			if(!pdfFilename.isEmpty() && !jpegFilename.isEmpty()){
				toBeProcessedList.add(new PdfJpegFilenameBean(pdfFilename, jpegFilename));
				DailyLogger.append(sb, "Detected: " + pdfFilename + " - " + jpegFilename);
//				System.out.println("pdfFilename = "+pdfFilename);
//				System.out.println("jpegFilename = "+jpegFilename);
//				System.out.println("");
			}
		}
		DailyLogger.append(sb, "--- Parsing Done!");
		return toBeProcessedList;
	}

}
