<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" />
	<xsl:param name="content_xml" />
	<xsl:param name="content_id" />

	<xsl:template match="/">
		<xsl:apply-templates select="/book/content-items/content-item" />
	</xsl:template>

	<xsl:template match="content-item">
		<xsl:apply-templates select="references" />
		<xsl:if test="content-item">
			<xsl:apply-templates select="content-item" />
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="references">
		<div class="contentWrapper">
			<p>
				<xsl:apply-templates select="title" />
				<xsl:if test="@type">
					<b>Reference Type:</b>&#160;<xsl:value-of select="@type" />
				</xsl:if>
			</p>
			<xsl:apply-templates select="citation" />
			<xsl:if test="references">
				<div style="padding-left:25px;">
					<xsl:apply-templates select="references" />
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="citation">
		<xsl:variable name="citationId" select="@id"/>
		<p class="refEntry">
			<xsl:value-of select="document($content_xml)/references/citation[attribute::id=($citationId)]" disable-output-escaping="yes" />
			<xsl:apply-templates select="a" />
		</p>
	</xsl:template>

	<xsl:template match="a">
		<xsl:variable name="target" select="@target" />
		<xsl:variable name="href" select="@href" />
		<span class="refIcon">
			<span class="icon">
				<a target="($target)" href="($href)" title="CrossRef reference">
					<img src="images/reficon_crossRef.gif" alt="CrossRef reference" />
				</a>
			</span>
       	</span>
	</xsl:template>
	
	<xsl:template match="title">
		<b>Reference Title:</b>&#160;<xsl:value-of select="." /><br />
	</xsl:template>	
	
</xsl:stylesheet>