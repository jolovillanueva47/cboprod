<?xml version="${root.xml.version}" encoding="${root.xml.encoding}"?>
<query_batch version="${root.queryBatch.version}" xmlns="${root.queryBatch.xmlns}" xmlns:xsi="${root.queryBatch.xmlnsXsi}">
	<head>
		<email_address>${root.queryBatch.head.emailAddress}</email_address>
		<doi_batch_id>${root.queryBatch.head.doiBatchId}</doi_batch_id>
	</head>
	<body>
		<#assign query=root.queryBatch.body.query>
		<query key="${query.key}" enable-multiple-hits="${query.enableMultipleHits}" forward-match="${query.forwardMatch}">
			<#if (query.journalTitle)??>
			<journal_title match="${query.journalTitle.match}">${query.journalTitle.journalTitle}</journal_title>
			</#if>
			<#if (query.seriesTitle)??>
			<series_title match="${query.seriesTitle.match}">${query.seriesTitle.seriesTitle}</series_title>
			</#if>
			<#if (query.volumeTitle)??>
			<volume_title match="${query.volumeTitle.match}">${query.volumeTitle.volumeTitle}</volume_title>
			</#if>
			<#if (query.author)??>
			<author>${query.author}</author>
			</#if>
			<#if (query.volume)??>
			<volume>${query.volume}</volume>
			</#if>
			<#if (query.firstPage)??>
			<first_page>${query.firstPage}</first_page>
			</#if>
			<#if (query.year)??>
			<year>${query.year}</year>
			</#if>
		</query>
	</body>
</query_batch>