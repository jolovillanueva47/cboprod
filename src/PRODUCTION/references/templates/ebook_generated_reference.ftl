<#macro recurse_macro node>
<#if node?size &gt; 0>
<#list node as references>
	<#if (references.referenceTitle)??><p class="refTitle"><span class="onlineHidden" style="font-weight: bold;">Reference Title: </span>${references.referenceTitle}</p></#if>
	<#if (references.typeAttribute)??><p class="refTitle onlineHidden"><span style="font-weight: bold;">Reference Type: </span>${references.typeAttribute}</p></#if>
	<#if (references.citations)?? && references.citations?size &gt; 0>
	<#list references.citations as citation>
	<p class="refEntry">
		<span>${citation.fullText}</span>
		<span class="refIcon">
			<#if (citation.crossRefLink)??><span class="icon"><a target="_new" href="${citation.crossRefLink}"><img src="images/reficon_crossRef.gif" alt="CrossRef reference" /></a></span></#if>
			<#if (citation.openURLLink)??><span class="icon"><a href="javascript:openUrl('${citation.openURLLink}');"><img src="images/reficon_openURL.gif" alt="OpenURL" /></a></span></#if>
			<#if (citation.googleScholarLink)??><span class="icon"><a href="javascript:googleScholar('${citation.googleScholarLink}');"><img src="images/reficon_googleScholar.gif" alt="Google Scholar reference" /></a></span></#if>
			<#if (citation.googleBooksLink)??><span class="icon"><a href="javascript:googleBooks('${citation.googleBooksLink}');"><img src="images/reficon_googleBooks.gif" alt="Google Books reference" /></a></span></#if>
		</span>
	</p>
	</#list>
	</#if>
	<#if (references.subReferences)?? && references.subReferences?size &gt; 0>
	<@recurse_macro node=references.subReferences/>
	</#if>
</#list>
</#if>
</#macro>

<#if referencesList?size &gt; 0>
<@recurse_macro node=referencesList/>
</#if>