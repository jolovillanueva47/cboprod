package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.cambridge.ebooks.production.util.SubnetUtils;

public class Client {

	Client() throws Exception {

		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"org.jnp.interfaces.NamingContextFactory");
		properties.put(Context.URL_PKG_PREFIXES,
				"org.jboss.naming:org.jnp.interfaces");
		properties.put(Context.PROVIDER_URL, "localhost:1099");
		InitialContext ctx = new InitialContext(properties);

		Queue queue = (Queue) ctx.lookup("queue/ebooks/logger");
		QueueConnectionFactory qcf = (QueueConnectionFactory) ctx
				.lookup("ConnectionFactory");

		// flag to start timer
		sendMessage(qcf, queue, "start ejb");

		for (int i = 1; i <= 1000; i++) {
			sendMessage(qcf, queue, "Message Driven Bean");
		}
		
		// flag to end timer
		sendMessage(qcf, queue, "end ejb");
		
	}

	public static boolean isSubnetRange(){
		File file = new File("C:\\subnet_ips.txt");
		FileReader fr;
		boolean subnetRange = false;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String str;
			SubnetUtils su;
			String remoteIp = "131.111.159.249";
			while((str = br.readLine()) != null ){
				su = new SubnetUtils("131.111.159.0/24");
				if(su.getInfo().isInRange(remoteIp)){
					subnetRange = true;
				}
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return subnetRange;
	}
	public static void main(String[] args) throws Exception {
		//new Client();
		System.out.println("isSubentRange: "+isSubnetRange());
	}

	public void sendMessage(QueueConnectionFactory qcf, Queue queue, String remarks)
			throws Exception {
		QueueConnection qc = qcf.createQueueConnection();
		try {
			QueueSession qs = qc.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			QueueSender sender = qs.createSender(queue);
			Message payload = qs.createMapMessage();

			payload.setStringProperty("CONTENT_ID", "123456");
			payload.setStringProperty("EISBN", "1234567");
			payload.setStringProperty("SERIES_CODE", "12345678");
			payload.setStringProperty("STATUS", "14");
			payload.setStringProperty("REMARKS", remarks);
			payload.setStringProperty("FILENAME", "filename");
			payload.setStringProperty("LOAD_TYPE", "loadtype");
			payload.setStringProperty("USERNAME", "testkoto");
			payload.setStringProperty("TYPE", "Book");

			sender.send(payload);
			sender.close();
			qs.close();

		} finally {
			qc.close();
		}
	}
}