package org.cambridge.ebooks.production.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.util.Log4JLogger;

public enum Status {
	
	NOTLOADED(-1, "Not yet Loaded"),           
	LOADED(0, "Loaded for Proofreading"),     //*
	RELOADED(1, "Reloaded with Corrections"), //*
	PROOFED(2, "Proofread Accepted"),         //*
	REJECTED(3, "Proofread with Error"), 
	APPROVE(4, "Approved"),                   //*
	DISAPPROVED(5, "Reviewed with Error"), 
	REAPPROVE(6, "Reloaded for Approver"),    //*
	//PUBLISHED(7, "Published"),                //*
	//UNPUBLISHED(8, "Unpublished"),            //*
	PUBLISHED(7, "Content Signed Off"),                //*
	UNPUBLISHED(8, "Correction Required"),            //*
	EMBARGO(9, "Embargo"),                    //*
	RETURN(10, "Return to Approver"),         //*
	RETURNTOUPLOADER(11, "Return to Uploader"),//*
	DELETED(100, "Deleted"),                  //*
	RELOAD(200, "Reload"),                    
	RESET(300, "Reset");                     //*        
	
	private int status;
	private String description;
	private static final Log4JLogger LogManager = new Log4JLogger(Status.class);
	Status(int status, String description) {
		this.status = status;
		this.description = description;
	}
	
	public String getStatus() { return String.valueOf(status); }
	
	public static Status getStatus(int status) {
		Status s = NOTLOADED;
		if (status == Integer.parseInt(LOADED.getStatus())) s = LOADED;
		else if (status == Integer.parseInt(RELOADED.getStatus())) s = RELOADED;
		else if (status == Integer.parseInt(PROOFED.getStatus())) s = PROOFED;
		else if (status == Integer.parseInt(REJECTED.getStatus())) s = REJECTED;
		else if (status == Integer.parseInt(APPROVE.getStatus())) s = APPROVE;
		else if (status == Integer.parseInt(DISAPPROVED.getStatus())) s = DISAPPROVED;
		else if (status == Integer.parseInt(REAPPROVE.getStatus())) s = REAPPROVE;
		else if (status == Integer.parseInt(PUBLISHED.getStatus())) s = PUBLISHED;
		else if (status == Integer.parseInt(UNPUBLISHED.getStatus())) s = UNPUBLISHED;
		else if (status == Integer.parseInt(EMBARGO.getStatus())) s = EMBARGO;
		else if (status == Integer.parseInt(RETURN.getStatus())) s = RETURN;
		else if (status == Integer.parseInt(DELETED.getStatus())) s = DELETED;
		else if (status == Integer.parseInt(RELOAD.getStatus())) s = RELOAD;
		else if (status == Integer.parseInt(RETURNTOUPLOADER.getStatus())) s = RETURNTOUPLOADER;
		return s;
	}
	
	public String getDescription() { return description; }
	
	public static final LinkedHashMap<String, String> ALL_OPTIONS = new LinkedHashMap<String, String>();
	static {
		ALL_OPTIONS.put(LOADED.getDescription(), LOADED.getStatus());
		ALL_OPTIONS.put(RELOADED.getDescription(), RELOADED.getStatus());
		ALL_OPTIONS.put(PROOFED.getDescription(), PROOFED.getStatus());
		ALL_OPTIONS.put(REJECTED.getDescription(), REJECTED.getStatus());
		ALL_OPTIONS.put(APPROVE.getDescription(), APPROVE.getStatus());
		ALL_OPTIONS.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());
		ALL_OPTIONS.put(REAPPROVE.getDescription(), REAPPROVE.getStatus());
		ALL_OPTIONS.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
		ALL_OPTIONS.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
		ALL_OPTIONS.put(EMBARGO.getDescription(), EMBARGO.getStatus());
		ALL_OPTIONS.put(RETURN.getDescription(), RETURN.getStatus());
		ALL_OPTIONS.put(DELETED.getDescription(), DELETED.getStatus());
		ALL_OPTIONS.put(RELOAD.getDescription(), RELOAD.getStatus());
		ALL_OPTIONS.put(RETURNTOUPLOADER.getDescription(), RETURNTOUPLOADER.getStatus());
	}
	
	public static LinkedHashMap<String, String> getOptions(Status status, boolean hasRevert) {
		LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
		if (hasRevert) {
			switch (status) {
//			case NOTLOADED:
//				break;
			case LOADED:
				break;
			case RELOADED:
				break;
			case PROOFED:
				options.put(LOADED.getDescription(), LOADED.getStatus());
				break;
			case REJECTED:
				options.put(LOADED.getDescription(), LOADED.getStatus());
				break;
			case APPROVE:
				options.put(PROOFED.getDescription(), PROOFED.getStatus());
				break;
			case DISAPPROVED:
				options.put(PROOFED.getDescription(), PROOFED.getStatus());
				break;
			case REAPPROVE:
				break;
			case PUBLISHED:
				options.put(APPROVE.getDescription(), APPROVE.getStatus());
				break;
			case UNPUBLISHED:
				options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
				break;
			case EMBARGO:
				options.put(APPROVE.getDescription(), APPROVE.getStatus());
				break;			
			case DELETED:
				options.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
				options.put(APPROVE.getDescription(), APPROVE.getStatus());
				break;
			case RELOAD:
				break;
			case RETURN:
				break;
			case RETURNTOUPLOADER:
				break;
			default:
				options.put(NOTLOADED.getDescription(), NOTLOADED.getStatus());
			}
		}
		options.putAll(getOptions(status));
		return options;
	}
	
	public static LinkedHashMap<String, String> getOptions(Status status) {
		LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
		switch (status) {
//		case NOTLOADED:
//			options.put(NOTLOADED.getDescription(), NOTLOADED.getStatus());
//			break;
		case LOADED:
			options.put(LOADED.getDescription(), LOADED.getStatus());
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case RELOADED:
			options.put(RELOADED.getDescription(), RELOADED.getStatus());
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case PROOFED:
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());			
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case REJECTED:
			// user needs to reload and system set to RELOADED
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case APPROVE:
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(EMBARGO.getDescription(), EMBARGO.getStatus());
			options.put(RETURN.getDescription(), RETURN.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case DISAPPROVED:
			// user needs to reload and system set to REAPPROVE
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());
			options.put(RETURNTOUPLOADER.getDescription(), RETURNTOUPLOADER.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case REAPPROVE:
			options.put(REAPPROVE.getDescription(), REAPPROVE.getStatus());
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());
			options.put(RETURNTOUPLOADER.getDescription(), RETURNTOUPLOADER.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case PUBLISHED:
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
			options.put(RETURN.getDescription(), RETURN.getStatus());
			break;
		case UNPUBLISHED:
			options.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case EMBARGO:
			options.put(EMBARGO.getDescription(), EMBARGO.getStatus());
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case RETURN:
			options.put(RETURN.getDescription(), RETURN.getStatus());
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(EMBARGO.getDescription(), EMBARGO.getStatus());
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());	
			options.put(RETURNTOUPLOADER.getDescription(), RETURNTOUPLOADER.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case DELETED:
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case RELOAD:
			options.put(RELOAD.getDescription(), RELOAD.getStatus()); 
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		case RETURNTOUPLOADER:
			options.put(RETURNTOUPLOADER.getDescription(), RETURNTOUPLOADER.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			break;
		default:
			options.put(NOTLOADED.getDescription(), NOTLOADED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());  //request to be added for all status other than published
			
		}
		return options;
	}
	
	public static Status getBookStatus(int status[]) {
		Status result = NOTLOADED;
		int newStatus = 999;
		for (int i : status) {
			if (newStatus < i) newStatus = i;
		}
		return result;
	}
	
	public static int getBookStatus(List<EBookContent> ebookContents) {		
		float newStatus = 999.0f;
		final float UNPUBLISHED = 6.5f;
		final float REVIEWED_W_ERROR = 1.5f;
		final float RETURN_TO_UPLOADER = 3.5f;
		final float RELOADED_FOR_APPROVER = 3.6f;
		final float RETURN_TO_APPROVER = 3.7f;
		final float PROOFREAD_W_ERROR = 1.3f;
		final float DELETED = 0.1f;
		for(EBookContent ebookContent: ebookContents) {
			float contentStatus = ebookContent.getStatus();
			
			switch (ebookContent.getStatus()) {			
			case 8:
				contentStatus = UNPUBLISHED;
				break;
			case 5:
				contentStatus = REVIEWED_W_ERROR;
				break;
			case 6:
				contentStatus = RELOADED_FOR_APPROVER;
				break;
			case 100:
				contentStatus = DELETED;
				break;
			case 3:
				contentStatus = PROOFREAD_W_ERROR;
				break;
			case 10:
				contentStatus = RETURN_TO_APPROVER;
				break;
			case 11:
				contentStatus = RETURN_TO_UPLOADER;
				break;			
			default:
				contentStatus = ebookContent.getStatus();
				break;
			} 
			
			// exclude part-title without defined pdf in xml
			try {
				if((ebookContent.getType().equals("part-title") && contentStatus == -1) || 
						ebookContent.getFileName() == null || ebookContent.getFileName().equals("")) {
					continue;
				}
			} catch (NullPointerException e) {
				LogManager.error(Status.class, "[NullPointerException] " + e.getLocalizedMessage());
				continue;
			}
			
			if(newStatus > contentStatus) {
				newStatus = contentStatus;
			}
		}
		
		if(newStatus == UNPUBLISHED) {
			newStatus = 8;
		} else if(newStatus == REVIEWED_W_ERROR) {
			newStatus = 5;
		} else if(newStatus == RELOADED_FOR_APPROVER) {
			newStatus = 6;
		} else if(newStatus == DELETED) {
			newStatus = 100;
		} else if(newStatus == PROOFREAD_W_ERROR) {
			newStatus = 3;
		} else if(newStatus == RETURN_TO_APPROVER) {
			newStatus = 10;
		} else if(newStatus == RETURN_TO_UPLOADER) {
			newStatus = 11;
		}	
		return (int)newStatus;
	}
	
	public static int getBookStatusFromContentListPage(List<EBookContentBean> ebookContents) {		
		float newStatus = 999.0f;
		final float UNPUBLISHED = 6.5f;
		final float REVIEWED_W_ERROR = 1.5f;
		final float RETURN_TO_UPLOADER = 3.5f;
		final float RELOADED_FOR_APPROVER = 3.6f;
		final float RETURN_TO_APPROVER = 3.7f;
		final float PROOFREAD_W_ERROR = 1.3f;
		final float DELETED = 0.1f;
		for(EBookContentBean ebookContent: ebookContents) {
			float contentStatus = ebookContent.getEbookContent().getStatus();
			
			switch (ebookContent.getEbookContent().getStatus()) {			
			case 8:
				contentStatus = UNPUBLISHED;
				break;
			case 5:
				contentStatus = REVIEWED_W_ERROR;
				break;
			case 6:
				contentStatus = RELOADED_FOR_APPROVER;
				break;
			case 100:
				contentStatus = DELETED;
				break;
			case 3:
				contentStatus = PROOFREAD_W_ERROR;
				break;
			case 10:
				contentStatus = RETURN_TO_APPROVER;
				break;
			case 11:
				contentStatus = RETURN_TO_UPLOADER;
				break;				
			default:
				contentStatus = ebookContent.getEbookContent().getStatus();
				break;
			} 
			
			// exclude part-title without defined pdf in xml
			try {
				if((ebookContent.getType().equals("part-title") && contentStatus == -1) || 
						ebookContent.getFileName() == null || ebookContent.getFileName().equals("")) {
					continue;
				}
			} catch (NullPointerException e) {
				LogManager.error(Status.class, "[NullPointerException] " + e.getLocalizedMessage());				
			}
			
			if(newStatus > contentStatus) {
				newStatus = contentStatus;
			}
			LogManager.info(Status.class, "--- " + newStatus);
		}
		
		if(newStatus == UNPUBLISHED) {
			newStatus = 8;
		} else if(newStatus == REVIEWED_W_ERROR) {
			newStatus = 5;
		} else if(newStatus == RELOADED_FOR_APPROVER) {
			newStatus = 6;
		} else if(newStatus == DELETED) {
			newStatus = 100;
		} else if(newStatus == PROOFREAD_W_ERROR) {
			newStatus = 3;
		} else if(newStatus == RETURN_TO_APPROVER) {
			newStatus = 10;
		} else if(newStatus == RETURN_TO_UPLOADER) {
			newStatus = 11;
		}
		
		return (int)newStatus;
	}
	
	public static Boolean isApprove(int status[]) {
		boolean approve = true;
		for (int i: status) {
			if (i != Integer.parseInt(APPROVE.getStatus()) && 
				i != Integer.parseInt(PUBLISHED.getStatus()) &&
				i != Integer.parseInt(DELETED.getStatus()) ) approve = false;
		}
		return approve;
	}
	
	public static int getNextStatus(int status) {
		String result = String.valueOf(status);
		if (status == Integer.parseInt(REJECTED.getStatus())) {
			result = RELOADED.getStatus();
		} else if (status == Integer.parseInt(DISAPPROVED.getStatus())) {
			result = REAPPROVE.getStatus();
		} else if (status == Integer.parseInt(RETURNTOUPLOADER.getStatus())) {
			result = REAPPROVE.getStatus();
		} 
		return Integer.parseInt(result);
	}
	
	private static boolean isItemAlreadyAdded(List<SelectItem> itemList, SelectItem item){
		boolean result = false;
		
		if(itemList != null && !itemList.isEmpty() && item != null)
		{
			for(SelectItem i : itemList)
			{
				if(i.getDescription().equalsIgnoreCase(item.getDescription()))
				{
					result = true;
					break;
				}
			}
		}
		
		return result;
		
	}
	
	public static SelectItem[] mergeContentUserStatus(SelectItem[] userStatuses, List<EBookContentBean> ebookContentList){
		List<SelectItem> result = new ArrayList<SelectItem>();
		
		try
		{
			result.add(new SelectItem(Status.RESET.getStatus(), "---  Content Status  ---", "---  Content Status  ---", false));
			for(EBookContentBean ebookContentBeans : ebookContentList) {
				
				//LogManager.info("Inside status: " + ebookContentBeans.getFileName());
				
				// PID 81717 - modified the loop...
				// iterate through wrapped insert bean
				if ( (ebookContentBeans.getOptions() == null) && (ebookContentBeans.getEbookInsertBean().getOptions().length > 0) ) {
					
					for(SelectItem item : ebookContentBeans.getEbookInsertBean().getOptions()) {
						for(SelectItem userItem : userStatuses) {
							if(userItem.getDescription().equalsIgnoreCase(item.getDescription())) {
								if(!isItemAlreadyAdded(result, item))
									result.add(item);
							}
						}
					}
					
				} else {
				
					for(SelectItem item : ebookContentBeans.getOptions())
					{
						for(SelectItem userItem: userStatuses)
							if(userItem.getDescription().equalsIgnoreCase(item.getDescription()))
							{
								if(!isItemAlreadyAdded(result, item))
								{
									result.add(item);
								}
							}
					}
					
				} // else
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Status.mergeContentUserStatus() " + e.getMessage());
		}
		
		return result.toArray(new SelectItem[0]);
	}	
}
