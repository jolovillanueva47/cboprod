package org.cambridge.ebooks.production.ip;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectBoolean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.common.LogManager;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.common.ProductionValidIp;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

public class ValidIpWorker {
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	private ArrayList<ProductionValidIp> ipList = new ArrayList<ProductionValidIp>();
	private boolean hasErrors;
	private ArrayList<ProductionValidIp> toDeleteList = new ArrayList<ProductionValidIp>();
	private ArrayList<ProductionValidIp> toUpdateList = new ArrayList<ProductionValidIp>();
	private HashSet<String> tmpDeleteList = new HashSet<String>();
	private HashSet<String> tmpUpdateList = new HashSet<String>();
	private ArrayList<String> ipListString = new ArrayList<String>();
	
	private final String[] ipRange = new String[]{
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}/\\d{1,3})"
	};
	
	private UIInput uiIpAddress;
	private UISelectBoolean uiExclude;
	
	public void initValidIp(ActionEvent event){
		LogManager.info(ValidIpWorker.class, "Initializing Valid Ip Worker...");
	}
	
	public void initValidIpWorker(){
		toDeleteList.clear();
		toUpdateList.clear();
		tmpDeleteList.clear();
		tmpUpdateList.clear();
		ipListString.clear();
		hasErrors = false;
		if(uiIpAddress != null)
			uiIpAddress.resetValue();
		
		this.ipList = PersistenceUtil.searchList(new ProductionValidIp(), ProductionValidIp.SELECT_ALL_IP);
		
		for(ProductionValidIp ip : ipList){
			ipListString.add(ip.getIp_address());
		}
		
		if(uiIpAddress != null){
			uiIpAddress.resetValue();
		}
	}
	
	public void updateValidIp(ActionEvent event){
		String[] listOfIps = uiIpAddress.getValue().toString().split(",");
//		String ipAddressDisplay = uiIpAddressDisplay.toString();
		
		EntityManager em = emf.createEntityManager();
		
		boolean shouldRefresh = false;
		
		try{
			em.getTransaction().begin();
			if(! toDeleteList.isEmpty()) {
				//delete entries from the table
				for (ProductionValidIp ip: toDeleteList){
					ProductionValidIp delIp = em.find(ProductionValidIp.class, ip.getIp_address());
					em.remove(delIp);
					
					FacesMessage messageDelete = new FacesMessage(ip.getIp_address() + " has been deleted");
					messageDelete.setSeverity(FacesMessage.SEVERITY_WARN);
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage("deleteBox", messageDelete);
				}
				shouldRefresh = true;
			}
			
			if(! toUpdateList.isEmpty()) {
				//update entries from the table
				for (ProductionValidIp ip: toUpdateList){
					ProductionValidIp updIp = em.find(ProductionValidIp.class, ip.getIp_address());
					updIp.setExclude(! updIp.isExclude());

					FacesMessage messageUpdate = new FacesMessage(ip.getIp_address() + " has been updated");
					messageUpdate.setSeverity(FacesMessage.SEVERITY_INFO);
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage("excludeBox", messageUpdate);
				}
				shouldRefresh = true;
			}
			
			if(listOfIps.length > 0 && ! listOfIps[0].equals("")){
				//adding new values to the table
				for(String str: listOfIps){
					if(this.ipListString.contains(str.trim())){
						FacesMessage messageInvalidIp = new FacesMessage(str + " is already in the database");
						messageInvalidIp.setSeverity(FacesMessage.SEVERITY_ERROR);
						FacesContext context = FacesContext.getCurrentInstance();
						context.addMessage("invalid", messageInvalidIp);
					}
					else {
						if(getIpPattern(str.trim()) == 0 || getIpPattern(str.trim()) == 1){
							ProductionValidIp newIp = new ProductionValidIp(str.trim(), false);
							em.persist(newIp);
							
							FacesMessage message = new FacesMessage(str + " has been added");
							message.setSeverity(FacesMessage.SEVERITY_INFO);
							FacesContext context = FacesContext.getCurrentInstance();
							context.addMessage("valid", message);
							this.ipListString.add(newIp.getIp_address().trim());
							
						} else {
							FacesMessage messageInvalidIp = new FacesMessage(str + " is not a valid IP address");
							messageInvalidIp.setSeverity(FacesMessage.SEVERITY_ERROR);
							FacesContext context = FacesContext.getCurrentInstance();
							context.addMessage("invalid", messageInvalidIp);
						}
					}
				}
				shouldRefresh = true;
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			JPAUtil.close(em);
		}
		
		if(shouldRefresh)
			initValidIpWorker();
	}
	
	public void addToDeleteList(ValueChangeEvent event) {
		String ipAddressToDelete = (String) event.getComponent().getAttributes().get("ipAddress");

		if ((Boolean)event.getNewValue()) {
			if (tmpDeleteList.add(ipAddressToDelete)) {
				ProductionValidIp delIp = new ProductionValidIp(ipAddressToDelete, false);
				toDeleteList.add(delIp);
//				System.out.println(ipAddressToDelete +" is now on the delete list");
			}
		}
		
	}
	
	public void addToUpdateList(ValueChangeEvent event) {
		String ipAddressToUpdate = (String) event.getComponent().getAttributes().get("ipAddress");
		String updateVal = uiExclude.getValue().toString();
		
		if (tmpUpdateList.add(ipAddressToUpdate)) {
			ProductionValidIp delIp = new ProductionValidIp(ipAddressToUpdate, toBoolean(updateVal));
			toUpdateList.add(delIp);
//			System.out.println(ipAddressToUpdate +" is now on the update list");
		}
	}

	private boolean toBoolean(String input){
		boolean val = false;
		if(input.equals("true")){
			val = true;
		}
		return val;
	}
	
	public int getIpPattern(String ip) {
		int flag = 100;

		for (int i = 0; i < ipRange.length; i++) {
			if (ip.matches(ipRange[i])) {
				flag = i;
			}
		}
		return flag;
	}
	
	public String updateValidIpNav() {
		return RedirectTo.VALID_IP;
	}

	public List<ProductionValidIp> getIpList() {
		return ipList;
	} 

	public void setIpList(ArrayList<ProductionValidIp> ipList) {
		this.ipList = ipList;
	}

	public UIInput getUiIpAddress() {
		return uiIpAddress;
	}

	public void setUiIpAddress(UIInput uiIpAddress) {
		this.uiIpAddress = uiIpAddress;
	}
	
	public UISelectBoolean getUiExclude() {
		return uiExclude;
	}

	public void setUiExclude(UISelectBoolean uiExclude) {
		this.uiExclude = uiExclude;
	}

	public boolean isHasErrors() {
		return hasErrors;
	}

	public void setHasErrors(boolean hasErrors) {
		this.hasErrors = hasErrors;
	}
}
