package org.cambridge.ebooks.production.grid;
/**
 * 
 * @author Joseph Mendiola
 * 
 */
import java.util.ArrayList;

import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.PersistenceUtil;

public class UserListGridImplementation extends GridImplementation {
	@Override
	<T> ArrayList<T> getList() {
		ArrayList<T> userList = (ArrayList<T>) PersistenceUtil.searchList(new User(), User.SEARCH_CUPADMIN_USER_SORT_BY_USERNAME);
		return userList;
	}
	
	@Override
	<T> String[] getAttributes() {
		String [] array = {"username", "email", "lastName", "firstName", "active"};
		return array;
	}

}
