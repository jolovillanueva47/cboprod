package org.cambridge.ebooks.production.grid;
/**
 * 
 * @author Joseph Mendiola
 * 
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.production.util.Log4JLogger;

public class GridServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Log4JLogger LogManager = new Log4JLogger(GridServlet.class);
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		getJsonDataTables(request, response);
	}        
		
	private void getJsonDataTables(HttpServletRequest request, HttpServletResponse response) throws IOException{
		ArrayList<Object> objList;
		
		try {
			
			String className = request.getParameter("className");
			Object obj = Class.forName(className).newInstance();
			GridImplementation gridList = (GridImplementation) obj;
			objList = gridList.getList();
			String[] attributeNames = gridList.getAttributes();
			
			DataTablesGrid dg = new DataTablesGrid();
			dg.setsEcho("3");
			dg.setiTotalRecords("3");
			dg.setiTotalDisplayRecords("3");
			
			List<String[]> aaData = new ArrayList<String[]>();
			
			for(Object rawr: objList){
				StringBuffer sb = new StringBuffer();
				for (int x = 0; x < attributeNames.length; x++){
					sb.append(BeanUtils.getProperty(rawr, attributeNames[x])+",");
				}
				String[] attributes = sb.toString().split(",");
				aaData.add(attributes);
			}

			dg.setAaData(aaData);
			JSONObject json = JSONObject.fromObject(dg);
			System.out.println(json);
			response.setContentType("application/json");
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());

		} catch (Exception e) {
			LogManager.error(GridServlet.class, e.getMessage());
		}
	}
	
}
