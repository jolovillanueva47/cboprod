package org.cambridge.ebooks.production.grid;
/**
 * 
 * @author Joseph Mendiola
 * 
 */
import java.util.ArrayList;

public abstract class GridImplementation {
	abstract <T> ArrayList<T> getList();
	abstract <T> String[] getAttributes();
}
