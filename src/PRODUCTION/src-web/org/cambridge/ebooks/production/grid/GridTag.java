package org.cambridge.ebooks.production.grid;
/**
 * 
 * @author Joseph Mendiola
 * 
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;

public class GridTag extends TagSupport{
	private static final Log4JLogger LogManager = new Log4JLogger(GridTag.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dataSource;
	
	private String tableHeaders;
	private String []tableHeadersArray;
	
	private String tableHeaderSizes;
	private String []tableHeaderSizesArray;
	
	private String className;
	
	@Override
	public int doStartTag() throws JspException {
		// TODO Auto-generated method stub
		JspWriter out = pageContext.getOut();
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		request.setAttribute("className", this.getClassName());
		
		try {
			out.print(writeJsp());
		} catch (Exception e) {
			LogManager.error(GridTag.class, e.getMessage());
		}
		
		return SKIP_BODY;
	}
	
	private void initParameters(){
		tableHeadersArray = tableHeaders.split(",");
		tableHeaderSizesArray = tableHeaderSizes.split(",");
	}
	
	private String writeJsp(){
		StringBuffer sb = new StringBuffer();
		initParameters();
		
		sb.append("<style type=\"text/css\">\n");
		sb.append("<!--\n\t@import \"css/demo_page.css\";\n");
		sb.append("\t@import \"css/demo_table.css\";\n-->\n");
		sb.append("</style>\n");
		sb.append("<script src=\"<%=request.getContextPath() %>/js/jquery/jquery.dataTables.js\" type=\"text/javascript\"></script>\n");
		sb.append("<script type=\"text/javascript\" charset=\"utf-8\">\n");
		sb.append(outDataSource());
		sb.append("</script>\n");
		
		sb.append("<div id=\"container\">\n");
		sb.append("\t<h1>Live example</h1>\n");
		sb.append("\t<div id=\"dynamic\">\n");
		sb.append("\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\"example\">\n");
		
		sb.append(outTableHeader());

		sb.append("\t\t<tbody></tbody>\n");
		
		sb.append(outTableFooter());

		sb.append("\t\t</table>\n");
		sb.append("\t\t</div>\n");
		sb.append("\t<div class=\"spacer\"></div>\n");
		sb.append("</div>\n");
		
		System.out.println("className = "+className);
		
		return sb.toString(); 
	}
	
	private String outDataSource(){
		StringBuffer sb = new StringBuffer();
		sb.append("$(document).ready(function() {\n" +
				"\t$('#example').dataTable( {\n" +
				"\t\"bProcessing\": true,\n" +
				"\t\"sAjaxSource\": '" + this.getDataSource() +"?className="+this.getClassName()+"'} \n" +
				"\t);} \n);\n");
		return sb.toString(); 
	}
	
	private String outTableHeader(){
		StringBuffer sb = new StringBuffer();
		sb.append("\t\t<thead>\n");
		sb.append("\t\t<tr>\n");
		
		for(int x=0; tableHeadersArray.length > x; x++){
			sb.append("\t\t\t<th width=\"");
			sb.append(tableHeaderSizesArray[x]);
			sb.append("%\">");
			sb.append(tableHeadersArray[x]);
			sb.append("</th>\n");
		}

		sb.append("\t\t</tr>\n");
		sb.append("\t\t</thead>\n");
		return sb.toString();
	}
	
	private String outTableFooter(){
		StringBuffer sb = new StringBuffer();
		sb.append("\t\t<tfoot>\n");
		sb.append("\t\t<tr>\n");
		for(int x=0; tableHeadersArray.length > x; x++){
			sb.append("\t\t\t<th>"+tableHeadersArray[x]+"</th>\n");
		}
		sb.append("\t\t</tr>\n");
		sb.append("\t\t</tfoot>\n");
		return sb.toString();
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getTableHeaders() {
		return tableHeaders;
	}

	public void setTableHeaders(String tableHeaders) {
		this.tableHeaders = tableHeaders;
	}

	public String getTableHeaderSizes() {
		return tableHeaderSizes;
	}

	public void setTableHeaderSizes(String tableHeaderSizes) {
		this.tableHeaderSizes = tableHeaderSizes;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	
}
