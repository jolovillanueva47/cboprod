package org.cambridge.ebooks.production.grid;
/**
 * 
 * @author Bryan Manalo
 * 			edited by Joseph Mendiola
 * 
 */
import java.util.List;

public class DataTablesGrid {
	private String sEcho;
	private String iTotalRecords;
	private String iTotalDisplayRecords;
	private List<String[]> aaData;
	
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public String getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(String iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public String getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(String iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<String[]> getAaData() {
		return aaData;
	}
	public void setAaData(List<String[]> aaData) {
		this.aaData = aaData;
	}
	
	
	
}
