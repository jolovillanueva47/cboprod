package org.cambridge.ebooks.production.user;

import java.util.ArrayList;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistencePager;

/**
 * 
 * @author jgalang
 * 
 */

public class SystemUserBean {
	private static final Log4JLogger LogManager = new Log4JLogger(SystemUserBean.class);
	private PersistencePager<User> pager;
	private ArrayList<User> userList;
	
	private String pagingText;
	private int goToPage;
	private int resultsPerPage;
	private ArrayList<SelectItem> pages;
	private String sortBy;
	private String tmpSortBy;
	
	public String updateSystemUser() {
		return RedirectTo.SYSTEM_USERS_UPDATE;
	}
	
	public void initNewUserSearch(ActionEvent event) {
		LogManager.info(SystemUserBean.class, "Init search users.");
		// PID 81236
		setTmpSortBy(sortBy = User.SORT_BY_USERNAME);
		initPaging();
	}
	
	public void initPaging() {
		goToPage = 0;
		resultsPerPage = 10;
		String reportQuery = null;
		if (sortBy.equals(User.SORT_BY_USERNAME)) {
			reportQuery = User.SEARCH_CUPADMIN_USER_SORT_BY_USERNAME;
		} else if (sortBy.equals(User.SORT_BY_EMAIL)) {
			reportQuery = User.SEARCH_CUPADMIN_USER_SORT_BY_EMAIL;			
		} else if (sortBy.equals(User.SORT_BY_FIRST_NAME)) {
			reportQuery = User.SEARCH_CUPADMIN_USER_SORT_BY_FIRSTNAME;			
		} else if (sortBy.equals(User.SORT_BY_LAST_NAME)) {
			reportQuery = User.SEARCH_CUPADMIN_USER_SORT_BY_LASTNAME;
		}
		pager = new PersistencePager<User>(
				User.COUNT_CUPADMIN_USERS, 
				resultsPerPage, 
				reportQuery
		);
		initGoToPage();
		LogManager.info(SystemUserBean.class, pager.getMaxResults() + " results");
	}
	
	private void initGoToPage() {
		pages = new ArrayList<SelectItem>();
		for (int p=0 ; p<pager.getMaxPages() ; p++) {
			pages.add(new SelectItem(p, String.valueOf(p+1)));
		}
	}

	public ArrayList<User> getUserList() {
		LogManager.info(SystemUserBean.class, "Acquiring System Users...");
		//RETURN ALL: userList = PersistenceUtil.searchList(new User(), User.SEARCH_CUPADMIN_SYSTEM_USER, new String());
		userList = pager.getCurrentResults();
		return userList;
	}
	
	public void setPagerValues(ActionEvent event) {
		LogManager.info(SystemUserBean.class, "Set pager values.");
		resultsPerPage = pager.getPageSize();
		goToPage = pager.getCurrentPage();
		sortBy = getTmpSortBy();
	}
	
	public void setSortBy(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Sort by " + (String) event.getNewValue());
		// PID 81236
		setTmpSortBy(sortBy = (String) event.getNewValue());
		initPaging();
	}
	
	public void setCurrentPage(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Go to page " + (Integer) event.getNewValue());
		pager.setCurrentPage((Integer) event.getNewValue());
		goToPage = pager.getCurrentPage();
	}
	
	public void setPageSize(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Set size " + (Integer) event.getNewValue());
		pager.setPageSize((Integer) event.getNewValue());
		initGoToPage();
	}
	
	public void nextPage(ActionEvent event) {
		pager.next();
		goToPage = pager.getCurrentPage();
	}
	
	public void previousPage(ActionEvent event) {
		pager.previous();
		goToPage = pager.getCurrentPage();
	}
	
	public void firstPage(ActionEvent event) {
		pager.first();
		goToPage = pager.getCurrentPage();
	}
	
	public void lastPage(ActionEvent event) {
		pager.last();
		goToPage = pager.getCurrentPage();
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public String getPagingText() {
		pagingText = "Page " + (pager.getCurrentPage()+1) + " of " + pager.getMaxPages();
		return pagingText;
	}

	public void setPagingText(String pagingText) {
		this.pagingText = pagingText;
	}

	public int getGoToPage() {
		return goToPage;
	}

	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public ArrayList<SelectItem> getPages() {
		return pages;
	}

	public void setPages(ArrayList<SelectItem> pages) {
		this.pages = pages;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getTmpSortBy() {
		return tmpSortBy;
	}

	public void setTmpSortBy(String tmpSortBy) {
		this.tmpSortBy = tmpSortBy;
	}
}