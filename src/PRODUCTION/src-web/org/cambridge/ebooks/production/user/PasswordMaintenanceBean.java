package org.cambridge.ebooks.production.user;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.Password;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.util.Misc;

/**
 * 
 * @author jgalang
 * 
 */
public class PasswordMaintenanceBean {
	private static final Log4JLogger LogManager = new Log4JLogger(PasswordMaintenanceBean.class);
	private static final String NO_OLD_PASSWORD = "Please enter your old password.";
	private static final String NO_NEW_PASSWORD = "Please enter your new password.";
	private static final String NO_CONFIRM_NEW_PASSWORD = "Please confirm your new password.";
	private static final String PASSWORDS_DO_NOT_MATCH = "Your password entries did not match.";
	private static final String INVALID_LENGTH = "Your password must be at least six characters and no more than nine.";
	private static final String INCORRECT_OLD_PASSWORD = "Old password is incorrect.";
	private static final String SUCCESSFUL_UPDATE = "You have successfully changed your password.";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );

	private UIInput oldPassword;
	private UIInput newPassword;
	private UIInput confirmPassword; 
	
	private String alert;

	public void validatePassword(FacesContext context, UIComponent component, Object value) {
		validateOldPassword(context, component, oldPassword.getValue());
		validateNewPassword(context, component, newPassword.getValue());
		validateConfirmPassword(context, component, confirmPassword.getValue());
		validateCorrectPassword(context, component, oldPassword.getValue());
	}
	
	public void validateOldPassword(FacesContext context, UIComponent component, Object value) {
		LogManager.info(PasswordMaintenanceBean.class, "Change password validating...");
		String oldPass = (String) value;
		boolean empty = Misc.isEmpty(oldPass);
		if (empty) {
			FacesMessage message = new FacesMessage(NO_OLD_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
	}
	
	public void validateNewPassword(FacesContext context, UIComponent component, Object value) {
		String newPass = (String) value;
		
		boolean empty = Misc.isEmpty(newPass);
		if (empty) {
			FacesMessage message = new FacesMessage(NO_NEW_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
		
		boolean validLength = newPass.length() >= User.PASSWORD_MIN_LENGTH && newPass.length() <= User.PASSWORD_MAX_LENGTH;
		if (!validLength) {
			FacesMessage message = new FacesMessage(INVALID_LENGTH);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
	
	public void validateConfirmPassword(FacesContext context, UIComponent component, Object value) {
		String confirmPass = (String) value;
		String newPass = (String) newPassword.getValue();
		
		boolean empty = Misc.isEmpty(confirmPass);
		if (empty) {
			FacesMessage message = new FacesMessage(NO_CONFIRM_NEW_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
		
		boolean passwordsMatch = confirmPass.equals(newPass);
		if (!passwordsMatch) {
			FacesMessage message = new FacesMessage(PASSWORDS_DO_NOT_MATCH);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
	
	public void validateCorrectPassword(FacesContext context, UIComponent component, Object value) {
		String oldPass = (String) value;
		User user = HttpUtil.getUserFromCurrentSession();
		boolean incorrectOldPassword = !oldPass.equals(Password.deobfuscate(user.getPassword()));
		if (incorrectOldPassword) {
			FacesMessage message = new FacesMessage(INCORRECT_OLD_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
	
	public void updatePassword(ActionEvent event) {
		LogManager.info(PasswordMaintenanceBean.class, "Validation Ok. Now updating user...");
		String newPass = (String) newPassword.getValue();

		EntityManager em = emf.createEntityManager();
		try {
			User user = HttpUtil.getUserFromCurrentSession();
			user = PersistenceUtil.searchEntity(new User(), User.SEARCH_CUPADMIN_USER_BY_ID, user.getUserId());
			user.setPassword(Password.obfuscate(newPass));
			if (user != null && user.getUserId().length() > 0) {
				user.setAccessRights(new AccessRights(user.getAccess().getAccessList()));
			}
			
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
	
			HttpSession session = HttpUtil.getHttpSession(true);
			session.setAttribute(SessionAttribute.USER, user);
			
			//create user audit trail
			EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, user.getUserId(), "PASSWORD MAINTENANCE" );
			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(SUCCESSFUL_UPDATE);
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			
			LogManager.info(PasswordMaintenanceBean.class, "Update Complete.");
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public String updatePasswordNav() {
		return RedirectTo.PASSWORD_UPDATE_SUCCESS;
	}
	
	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}
	
	
	public UIInput getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(UIInput oldPassword) {
		this.oldPassword = oldPassword;
	}

	public UIInput getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(UIInput newPassword) {
		this.newPassword = newPassword;
	}

	public UIInput getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(UIInput confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
