package org.cambridge.ebooks.production.user;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * 
 * @author jgalang
 * 
 */

public class LogoutBean {
	private static final Log4JLogger LogManager = new Log4JLogger(LogoutBean.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );
	
	public String logout() {
		HttpSession session = HttpUtil.getHttpSession(false);
		
		//create user audit trail
		User user = HttpUtil.getUserFromCurrentSession();
		EventTracker.sendEvent( AuditTrail.LOG_OUT, user.getUserId(), null );

		LogManager.info(LogoutBean.class, user.getUsername() + " is logging out. " + session.getId());
//		session.removeAttribute(SessionAttribute.USER);
		if (session != null) {
			
			session.invalidate();
		}
		
		LogManager.info(LogoutBean.class, "Logout successful.");
		
		return RedirectTo.LOGOUT;
	}
}
