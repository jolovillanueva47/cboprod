package org.cambridge.ebooks.production.user;

import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.MailManager;
import org.cambridge.ebooks.production.util.MailManager.EmailUtilException;
import org.cambridge.ebooks.production.util.Password;
import org.cambridge.util.Misc;

/**
 * 
 * @author jgalang
 * 
 */

public class SystemUserMailer {
	private static final Log4JLogger LogManager = new Log4JLogger(SystemUserMailer.class);
	public class MailBody {
		private String name;
		private String username;
		private String password;
		public MailBody(String name, String username, String password) {
			this.name = name;
			this.username = username;
			this.password = password;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
	}
	
	private static final String FROM = "eBooks Production Administrator";
	private static final String[] EMAIL_ADMIN_BCC = new String[] {System.getProperty("email.admin"), System.getProperty("email.admin.bcc")};
	private static final String SUBJECT = "eBooks Production - User Login";
	private static final String TEMPLATE_DIR = System.getProperty("templates.dir");
	private static final String TEMPLATE_FILE = System.getProperty("mail.user.info.template");
	private static final String OUTPUT_DIR = System.getProperty("mail.store.path");
	
	private MailBody mb;
	
	private SystemUserMailer(User user) {
		boolean hasName = Misc.isNotEmpty((user.getFirstName()+user.getLastName()).trim());
		mb = new MailBody(
				hasName ? user.getFirstName() + " " + user.getLastName() : user.getUsername(),
				user.getUsername(), 
				Password.deobfuscate(user.getPassword()));
	}
	
	public static void sendMail(User user) {
		SystemUserMailer mailer = new SystemUserMailer(user);
		try {
			MailManager.storeMail(
					TEMPLATE_DIR,
					TEMPLATE_FILE,
					OUTPUT_DIR,
					FROM,
					new String[] {user.getEmail()},
					null,
					EMAIL_ADMIN_BCC,
					SUBJECT,
					mailer.mb);
		} catch (EmailUtilException e) {
			LogManager.error(SystemUserMailer.class, "Error storing mail file: " + e.getMessage());
		}
	}
}
