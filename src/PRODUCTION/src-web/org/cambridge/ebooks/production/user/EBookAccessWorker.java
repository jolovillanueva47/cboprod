package org.cambridge.ebooks.production.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.jpa.user.EBookAccess;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * 
 * @author jgalang
 * @modified rvillamor 041310 - insertEBookAccess
 */

public class EBookAccessWorker {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookAccessWorker.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static void insertEBookAccess(String userId, List<EBookBean> bookList, String modifiedBy) {
		clearEBookAccessById(userId);
		
		EntityManager em = emf.createEntityManager();
		
		try {
			//long start = System.currentTimeMillis();
			boolean startTransaction = (bookList != null && !bookList.isEmpty());
			if(startTransaction)
			{
				em.getTransaction().begin();
				for (EBookBean eBookBean : bookList) 
				{
					EBookAccess eba = new EBookAccess();
					eba.setUserId(userId);
					eba.setBookId(eBookBean.getBookId());
					eba.setModifiedDate(new Date());
					eba.setModifiedBy(modifiedBy);
					em.persist(eba);
				}
				em.getTransaction().commit();
			}
			
			//long end = System.currentTimeMillis() - start;
			//System.out.println((end * 0.001) * 0.0166666667);
		} 
		catch (Exception e) {
			LogManager.error(EBookAccessWorker.class, "Error trying to insert ebook access.");
			e.printStackTrace();
		} 
		finally {
			JPAUtil.close(em);
		}
	}
	
	public static ArrayList<EBookAccess> getEBookAccessById(String userId) {
		return PersistenceUtil.searchList(new EBookAccess(), EBookAccess.SEARCH_EBOOK_ACCESS_BY_ID, userId);
	}
	
	public static void clearEBookAccessById(String userId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			
			Query query = em.createNamedQuery(EBookAccess.DELETE_EBOOK_ACCESS_BY_ID);
			query.setParameter(1, userId);
			query.executeUpdate();
			
			em.getTransaction().commit();
		} 
		catch (Exception e) {
			LogManager.error(EBookAccessWorker.class, "Error trying to delete ebook access.");
			e.printStackTrace();
		}
		finally {
			JPAUtil.close(em);
		}
	}
}
