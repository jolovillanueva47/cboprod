package org.cambridge.ebooks.production.user;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.common.ProductionValidIp;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.jpa.user.CupadminAccess;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.MailManager;
import org.cambridge.ebooks.production.util.Password;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.SubnetUtils;
import org.cambridge.util.Misc;

/**
 * @author jgalang
 * 
 */

public class LoginBean {
    private static String INVALID_IP         = "Your IP address is not recognized.";
    private static String INVALID_USER       = "Invalid Username or Password.";
    private static String INVALID_EMAIL      = "Email is invalid.";
    private static String UNREGISTERED_EMAIL = "The email address has not been registered.";
    private static String LOGIN_INFO_SENT    = "Your login information has been sent to your email.";
    private static String VALID_IPS;
    
    private static final Logger LOGGER = Logger.getLogger(LoginBean.class);
    private UIInput inputUsername;
    private UIInput inputPassword;
    private UIInput inputEmail;
    
    public int getMessageSize() {
        FacesContext context = FacesContext.getCurrentInstance();
        int counter = 0;
        for(Iterator<?> itr = context.getMessages(); itr.hasNext() ; itr.next()) counter++;
        return counter;
    }
    
    public void validateDetails(FacesContext context, UIComponent component, Object value) {
        //CUSTOM VALIDATION (length, valid characters) GOES HERE. IF NEEDED.
    }

    public String login() {
        LOGGER.info("login() start");
        String redirect = "";
        try{
            ArrayList<ProductionValidIp> ipList = PersistenceUtil.searchList(new ProductionValidIp(), ProductionValidIp.SELECT_ALL_IP);
            ArrayList<ProductionValidIp> subnetList = new ArrayList<ProductionValidIp>(); 
            
            StringBuffer validIps = new StringBuffer();
            
            for(ProductionValidIp ip: ipList){
                if (! ip.isExclude()){
                    if (ip.getIp_address().contains("/")){
                        subnetList.add(ip);
                    } else {
                        validIps.append(ip.getIp_address() + " ");
                    }
                }
            }
            
            VALID_IPS = validIps.toString().trim();
            
            redirect = RedirectTo.LOGIN_FAIL;
            String username = (String) inputUsername.getValue();
            
            User user = findUser(username, (String) inputPassword.getValue());
            boolean userAuthenticated = user != null && Misc.isNotEmpty(user.getUserId());
            if (userAuthenticated) {
                if(isValidIp(subnetList)){
                    LOGGER.info("Login successful.");
                    
                    //get publisher details
                    Publisher publisher = PersistenceUtil.searchEntity(new Publisher(), Publisher.SEARCH_PUBLISHER_BY_USERID, user.getUserId());
                    
                    redirect = RedirectTo.LOGIN_SUCCESS;
                    HttpSession session = HttpUtil.getHttpSession(true);
                    session.setAttribute(SessionAttribute.USER, user);
                    session.setAttribute(SessionAttribute.EBOOK_ACCESS, EBookAccessWorker.getEBookAccessById(user.getUserId()));
                    session.setAttribute(SessionAttribute.LOGIN_TIME, System.currentTimeMillis());
                    session.setAttribute(SessionAttribute.PUBLISHER, publisher);
                    
                    //create user audit trail
                    EventTracker.sendEvent( AuditTrail.LOG_IN, user.getUserId(), null);
                    
                    LOGGER.info("User " + username + " has been set on the session.");
                }else{
                    redirect = RedirectTo.LOGIN_FAIL;
                    FacesContext context = FacesContext.getCurrentInstance();
                    FacesMessage message = new FacesMessage(INVALID_IP);
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    context.addMessage("password", message);
                }
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                FacesMessage message = new FacesMessage(INVALID_USER);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage("password", message);
            }
            
            LOGGER.info("login() end");
        } catch (Exception e){
            LOGGER.error("Error Logging in: "+e.getMessage());
        }
        return redirect;
    }
    
    public static String getRemoteIp(final HttpServletRequest request) throws UnknownHostException {
        LOGGER.info("Getting IP from True-Client-IP...");
        String ipaddress = (String)request.getHeader("True-Client-IP");
        
        if ((ipaddress) == null ) {
            LOGGER.info("True-Client-IP not found, Getting IP from X-Cluster-Client-Ip...");
            ipaddress=(String)request.getHeader("X-Cluster-Client-Ip");
        }   
        
        if ((ipaddress) == null  && request.getHeader("x-forwarded-for") != null) {
            LOGGER.info("X-Cluster-Client-Ip not found, Getting IP from x-forwarded-for...");
            ipaddress=request.getHeader("x-forwarded-for");
        }
        
        if ((ipaddress) == null  && InetAddress.getByName(request.getRemoteAddr()) != null  ){
            LOGGER.info("x-forwarded-for not found, Getting IP from request.getRemoteAddr()...");
            ipaddress=InetAddress.getByName(request.getRemoteAddr()).toString();
        }
        
        if ((ipaddress) == null ){
            LOGGER.info("request.getRemoteAddr() not found, Getting IP from HttpUtil.getAttributeFromCurrentSession(\"checkedIp\")...");
            ipaddress=(String) HttpUtil.getAttributeFromCurrentSession("checkedIp");
        }
        
        LOGGER.info("IP found! IP: "+ipaddress.replaceAll("/", ""));
        
        return ipaddress.replaceAll("/", "");
    }
    
    public static boolean isSubnetRange(String remoteIp, ArrayList<ProductionValidIp> subnetList){
        LOGGER.info("isSubnetRange() start: "+remoteIp);
        boolean subnetRange = false;
        SubnetUtils su;
        
        for(ProductionValidIp subnet: subnetList){
            su = new SubnetUtils(subnet.getIp_address().trim());
            
            if( su.getInfo().isInRange(remoteIp)){
                subnetRange = true;
                break;
            }
        }
    
        LOGGER.info("isSubnetRange() end: "+subnetRange);
        
        return subnetRange;
    }
    
    public static boolean isValidIp(ArrayList<ProductionValidIp> subnetList) throws UnknownHostException {
        LOGGER.info("isValidIp() start");
        
        String ipAddress = getRemoteIp(HttpUtil.getHttpServletRequest());//(String) HttpUtil.getAttributeFromCurrentSession("checkedIp");
        
        LOGGER.info("**User IP: "+ipAddress);
        LOGGER.info("**isSubnetRange(ipAddress): "+isSubnetRange(ipAddress, subnetList));
        
        if(VALID_IPS.contains(ipAddress) || isSubnetRange(ipAddress, subnetList)){
            LOGGER.info("isValidIp() end... valid ip true ");
            return true;
        }else{
            LOGGER.info("isValidIp() end... valid ip false ");
            return false;
        }
    }
    
    public User findUser(String username, String password) {
        LOGGER.info("User: " + username + " is logging in...");
        User user = PersistenceUtil.searchEntity(new User(), User.SEARCH_CUPADMIN_USER_BY_LOGIN, username, Password.obfuscate(password));
        if (user != null && user.getUserId().length() > 0) {
            CupadminAccess rights = user.getAccess();
            if (rights == null) rights = new CupadminAccess();
            user.setAccessRights(new AccessRights(rights.getAccessList()));
        }
        return user;
    }
    
    public void resetEmail(ActionEvent event) {
        inputEmail.resetValue();
    }
    
    public void validateEmail(FacesContext context, UIComponent component, Object value) {
        String email = (String) inputEmail.getSubmittedValue();
        
        boolean validEmail = MailManager.isValidEmailAddress(email);
        if (!validEmail) {
            FacesMessage message = new FacesMessage(INVALID_EMAIL);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
    
    public void emailLoginDetails(ActionEvent event) {
        String email = (String) inputEmail.getValue();
        LOGGER.info("Retrieving details for " + email);
        User user = PersistenceUtil.searchEntity(new User(), User.SEARCH_CUPADMIN_USER_BY_EMAIL, email);
        if (user != null && user.getUserId().length() > 0) {
            SystemUserMailer.sendMail(user);
            
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(LOGIN_INFO_SENT);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("email", message);
            
            LOGGER.info("Mail stored.");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(UNREGISTERED_EMAIL);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("email", message);
        }
    }

    public UIInput getInputPassword() {
        return inputPassword;
    }

    public void setInputPassword(UIInput inputPassword) {
        this.inputPassword = inputPassword;
    }
    
    public UIInput getInputUsername() {
        return inputUsername;
    }

    public void setInputUsername(UIInput inputUsername) {
        this.inputUsername = inputUsername;
    }

    public UIInput getInputEmail() {
        return inputEmail;
    }

    public void setInputEmail(UIInput inputEmail) {
        this.inputEmail = inputEmail;
    }
}
