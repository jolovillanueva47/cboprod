package org.cambridge.ebooks.production.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectBoolean;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.audittrail.BookAuditTrailBean;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.ebook.EBookDAO;
import org.cambridge.ebooks.production.ebook.EBookManagedBean;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.publisher.PublisherMemberBean;
import org.cambridge.ebooks.production.jpa.publisher.PublisherUpdateBean;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.jpa.user.CupadminAccess;
import org.cambridge.ebooks.production.jpa.user.EBookAccess;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.jpa.user.UserBookAuditTrail;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.MailManager;
import org.cambridge.ebooks.production.util.Password;
import org.cambridge.ebooks.production.util.PersistencePager;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.util.Misc;
/**
 * 
 * @author jgalang
 * 
 */

public class SystemUserUpdateBean {
	private static final Log4JLogger LogManager = new Log4JLogger(SystemUserUpdateBean.class);
	public class SystemUserUpdateException extends ValidatorException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SystemUserUpdateException(FacesMessage facesMessage) {
			super(facesMessage);
			cleanupLists();
		}
	}
	
	private static final EBookSearchWorker worker = new EBookSearchWorker();
	private static final String NO_USERNAME = "Please enter Username.";
	private static final String NO_PASSWORD = "Please enter Password.";
	private static final String NO_CONFIRM_PASSWORD = "Please confirm your password.";
	private static final String NO_EMAIL = "Please enter Email Address.";
	private static final String NO_FIRST_NAME = "Please enter first name.";
	private static final String NO_LAST_NAME = "Please enter last name.";
	private static final String NO_PUBLISHER = "Please select a publisher";
	private static final String INVALID_PASSWORD = "Your password must be at least six characters and no more than nine.";
	private static final String PASSWORDS_DO_NOT_MATCH = "Your password entries did not match.";
	private static final String INVALID_EMAIL = "Email is invalid.";
	private static final String EMAIL_EXISTS = "Email already exists.";
	private static final String USERNAME_EXISTS = "Username already exists.";

//TODO: -C2- private static final String CREATE_SUCCESSFUL = "User has been successfully created. Login information has been sent to the email address.";
	private static final String CREATE_SUCCESSFUL = "User has been successfully created.";
	private static final String UPDATE_SUCCESSFUL = "User details have been successfully updated.";
	private static final String UPDATE = "Update";
	private static final String CREATE = "Save";
	
	// PID 81236
	private static final String USER_ID = "userId";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	private UIInput uiUsername;
	private UIInput uiPassword;
	private UIInput uiConfirmPassword;
	private UIInput uiEmail;
	private UIInput uiFirstName;
	private UIInput uiLastName;
	
	private UIInput uiAuditEisbn;
	private UIInput uiAuditSeriesId;
	private UIInput uiAuditUsername;
	private UIInput uiAuditStatus;
	
	private String displayPassword;
	private String updateType;
	
	private String auditDateFromDay;
	private String auditDateFromMonth;
	private String auditDateFromYear;
	
	private String auditDateToDay;
	private String auditDateToMonth;
	private String auditDateToYear;
	
	private String publisherId;
	private UISelectOne uiPublisherId; //jubs 20100224
	private UISelectBoolean uiCanManagePublishers;
	private boolean canManagePublishers;
	
	private UISelectOne uiAuditDateFromDay;
	private UISelectOne uiAuditDateFromMonth;
	private UISelectOne uiAuditDateFromYear;

	private UISelectOne uiAuditDateToDay;
	private UISelectOne uiAuditDateToMonth;
	private UISelectOne uiAuditDateToYear;
	
	private SelectItem[] dayItems;
	private SelectItem[] yearItems;
	
	private String pagingText;
	private int goToPage;
	private int resultsPerPage;
	private ArrayList<SelectItem> pages;
	
	private UISelectOne uiGoToPage;
	private UISelectOne uiResultsPerPage;
	
	private User user = new User();
	private PublisherUpdateBean publisherBean = new PublisherUpdateBean();
	private PublisherMemberBean publisherMember = new PublisherMemberBean();
	
	private boolean hasErrors;
	
	private List<EBookBean> ebookAccessList;
	
	private PersistencePager<UserBookAuditTrail> pager;
	private ArrayList<UserBookAuditTrail> bookAuditList;
	private ArrayList<EBookBean> bookResultList;
	private ArrayList<String> removeFromAccessList;
	private ArrayList<EBookBean> insertToAccessList;
	
	public void initNewSystemUser(ActionEvent event) {
		initFormValues();
		updateType = CREATE;
		user = new User();
		bookResultList = null;//"null" won't display anything. Displays "No books found." when size is 0.
		ebookAccessList = new ArrayList<EBookBean>();
		insertToAccessList = new ArrayList<EBookBean>();
		removeFromAccessList = new ArrayList<String>();
		
		initSelectPublisher();
		//publisherBean.initPublisherDetails(null);
	}
	
	public void initSystemUser(ActionEvent event) {
		initFormValues();
		updateType = UPDATE;
		String userId = (String) event.getComponent().getAttributes().get(USER_ID);
		user = PersistenceUtil.searchEntity(new User(), User.SEARCH_CUPADMIN_USER_BY_ID, userId);
		
		CupadminAccess rights = user.getAccess();
		if (rights == null) rights = new CupadminAccess();
		user.setAccessRights(new AccessRights(rights.getAccessList()));
		
		bookResultList = null;//"null" won't display anything. Displays "No books found." when size is 0.
		
		ebookAccessList = worker.ebookTitles(EBookAccessWorker.getEBookAccessById(user.getUserId()));//EBookIndexSearch.ebookTitles(EBookAccessWorker.getEBookAccessById(user.getUserId()));
		Collections.sort(ebookAccessList);
		
		insertToAccessList = new ArrayList<EBookBean>();
		removeFromAccessList = new ArrayList<String>();
		
		// init publisher module //jubs 20100302
		//publisherBean.initPublisherDetails(null);
		initSelectPublisher();
		publisherId = publisherBean.getUserPublisher(user.getUserId());
		canManagePublishers = user.getAccessRights().isCanManagePublishers();
	}
	
	public void initFormValues() {
		if (uiUsername != null) 
			uiUsername.resetValue();
		if (uiPassword != null)
			uiPassword.resetValue();
		if (uiConfirmPassword != null)
			uiConfirmPassword.resetValue();
		if (uiEmail != null)
			uiEmail.resetValue();
		if (uiFirstName != null)
			uiFirstName.resetValue();
		if (uiLastName != null)
			uiLastName.resetValue();
		
		initAuditFields();
	}
	
	public void initAuditFields(){		
		bookAuditList = null;
		pager = null;
		
		if(uiAuditEisbn != null)
			uiAuditEisbn.resetValue();
		if(uiAuditSeriesId != null)
			uiAuditSeriesId.resetValue();
		if(uiAuditUsername != null)
			uiAuditUsername.resetValue();
		if(uiAuditDateFromDay != null)
			uiAuditDateFromDay.resetValue();
		if(uiAuditDateFromMonth != null)
			uiAuditDateFromMonth.resetValue();
		if(uiAuditDateFromYear != null)
			uiAuditDateFromYear.resetValue();
		if(uiAuditDateToDay != null)
			uiAuditDateToDay.resetValue();
		if(uiAuditDateToMonth != null)
			uiAuditDateToMonth.resetValue();
		if(uiAuditDateToYear != null)
			uiAuditDateToYear.resetValue();
		if(uiPublisherId != null)
			uiPublisherId.resetValue();
		if(uiCanManagePublishers != null)
			uiCanManagePublishers.resetValue();
		if(uiAuditStatus != null)
			uiAuditStatus.resetValue();
		if(publisherId != null)
			publisherId = "";
		if(canManagePublishers)
			canManagePublishers = false;
		
		SelectItem[] days = new SelectItem[31];
		for(int i=0; i<31; i++){
			days[i] = new SelectItem(Integer.toString(i+1),Integer.toString(i+1));
		}
		setDayItems(days);
		
		SelectItem[] years = new SelectItem[3];
		Calendar calendar = Calendar.getInstance();
		int ctr=0;
		for(int start = calendar.get(Calendar.YEAR) - 2; start < calendar.get(Calendar.YEAR)+1; start++){
			years[ctr] = new SelectItem(Integer.toString(start),Integer.toString(start));
			ctr++;
		}
		setYearItems(years);
		
		setAuditDateFromDay(String.valueOf(calendar.get(Calendar.DATE))); 
		setAuditDateFromYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String fromMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateFromMonth(fromMonth);
		
		setAuditDateToDay(String.valueOf(calendar.get(Calendar.DATE)));
		setAuditDateToYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String toMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateToMonth(toMonth);
        
        setPublisherId(publisherId);
	}
	
	public void initPaging() {
		if (uiResultsPerPage != null) 
			uiResultsPerPage.resetValue();
		if (uiGoToPage != null) 
			uiGoToPage.resetValue();
		
		goToPage = 0;
		resultsPerPage = 10;
		initGoToPage();
	}
	
	private void initGoToPage() {
		pages = new ArrayList<SelectItem>();
		for (int p=0 ; p<pager.getMaxPages() ; p++) {
			pages.add(new SelectItem(p, String.valueOf(p+1)));
		}
	}
	
	public void setPagerValues(ActionEvent event) {
		LogManager.info(SystemUserUpdateBean.class, "Set pager values.");
		resultsPerPage = pager.getPageSize();
		goToPage = pager.getCurrentPage();
//		sortBy = tmpSortBy;
	}
	
	public void setCurrentPage(ValueChangeEvent event) {
		LogManager.info(SystemUserUpdateBean.class, "Go to page " + (Integer) uiGoToPage.getValue());
		pager.setCurrentPage((Integer) uiGoToPage.getValue());
		goToPage = pager.getCurrentPage();
		resolveBookResultList();
	}
	
	public void setPageSize(ValueChangeEvent event) {
		LogManager.info(SystemUserUpdateBean.class, "Set size " + (Integer) uiResultsPerPage.getValue());
		pager.setPageSize((Integer) uiResultsPerPage.getValue());
		initGoToPage();
		resolveBookResultList();
	}
	
	public void nextPage(ActionEvent event) {
		pager.next();
		goToPage = pager.getCurrentPage();
		resolveBookResultList();
	}
	
	public void previousPage(ActionEvent event) {
		pager.previous();
		goToPage = pager.getCurrentPage();
		resolveBookResultList();
	}
	
	public void firstPage(ActionEvent event) {
		pager.first();
		goToPage = pager.getCurrentPage();
		resolveBookResultList();
	}
	
	public void lastPage(ActionEvent event) {
		pager.last();
		goToPage = pager.getCurrentPage();
		resolveBookResultList();
	}

	public String getPagingText() {
		pagingText = "Page " + (pager.getCurrentPage()+1) + " of " + pager.getMaxPages();
		return pagingText;
	}
	
	public void validateDetails(FacesContext context, UIComponent component, Object value) {
		LogManager.info(SystemUserUpdateBean.class, "User detail validating...");
		
		String username = (String) uiUsername.getValue();
		boolean emptyUsername = Misc.isEmpty(username);
		if (emptyUsername) {
			FacesMessage message = new FacesMessage(NO_USERNAME);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}
		
		String password = (String) uiPassword.getValue();
		boolean emptyPassword = Misc.isEmpty(password);
		if (emptyPassword) {
			FacesMessage message = new FacesMessage(NO_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}
		
		String confirmPassword = (String) uiConfirmPassword.getValue();
		boolean emptyConfirmPassword = Misc.isEmpty(confirmPassword);
		if (emptyConfirmPassword) {
			FacesMessage message = new FacesMessage(NO_CONFIRM_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}
		
		String email = (String) uiEmail.getValue();
		boolean emptyEmail = Misc.isEmpty(email);
		if (emptyEmail) {
			FacesMessage message = new FacesMessage(NO_EMAIL);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}
		
		String firstName = (String) uiFirstName.getValue();
		boolean emptyFirstName = Misc.isEmpty(firstName);
		if (emptyFirstName) {
			FacesMessage message = new FacesMessage(NO_FIRST_NAME);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}
		
		String lastName = (String) uiLastName.getValue();
		boolean emptyLastName = Misc.isEmpty(lastName);
		if (emptyLastName) {
			FacesMessage message = new FacesMessage(NO_LAST_NAME);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);	
		}

		boolean validLength = password.length() >= User.PASSWORD_MIN_LENGTH && password.length() <= User.PASSWORD_MAX_LENGTH;
		if (!validLength) {
			FacesMessage message = new FacesMessage(INVALID_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);
		}
		
		boolean passwordsMatch = password.equals(confirmPassword);
		if (!passwordsMatch) {
			FacesMessage message = new FacesMessage(PASSWORDS_DO_NOT_MATCH);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);
		}
		
		boolean validEmail = MailManager.isValidEmailAddress(email);
		if (!validEmail) {
			FacesMessage message = new FacesMessage(INVALID_EMAIL);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new SystemUserUpdateException(message);
		}

		if(uiCanManagePublishers != null && uiPublisherId != null)
		{
			boolean canManagePublishers = Boolean.parseBoolean(uiCanManagePublishers.getValue().toString());
			if(canManagePublishers)
			{
				String publisherId 	= (String)uiPublisherId.getSubmittedValue();
				if(publisherId != null && publisherId.equals("0"))
				{
					FacesMessage message = new FacesMessage(NO_PUBLISHER);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new SystemUserUpdateException(message);	
				}
			}
		}
		
	
	}
		
	public void updateSystemUser(ActionEvent event) {
		LogManager.info(SystemUserUpdateBean.class, updateType + " user...");
		hasErrors = false;
		
		EntityManager em = emf.createEntityManager();
		User userUpdate = null;
		if (updateType.equals(CREATE)) {
			userUpdate = user;
		} else if (updateType.equals(UPDATE)) {
			userUpdate = em.find(User.class, user.getUserId());
			userUpdate.setActive(user.getActive());
			userUpdate.setDepartment(user.getDepartment());
			userUpdate.setEmail(user.getEmail());
			userUpdate.setFirstName(user.getFirstName());
			userUpdate.setLastName(user.getLastName());
			userUpdate.setUsername(user.getUsername());
		}
		
		User currentUser = HttpUtil.getUserFromCurrentSession();
		Date today = new Date();
		
		userUpdate.setPassword(Password.obfuscate(displayPassword));
		userUpdate.setModifiedDate(today);
		userUpdate.setModifiedBy(currentUser.getUsername());
		userUpdate.setAccessRights(user.getAccessRights());
		
		try {
			
			em.getTransaction().begin();
			// em.merge(userUpdate);
			// PID 81236 -->
			if (updateType.equals(CREATE))
				em.persist(userUpdate);
			else if (updateType.equals(UPDATE))
				em.merge(userUpdate);
			// <--
			em.getTransaction().commit();
			
			LogManager.info(SystemUserUpdateBean.class, "User persisted. Persisting access...");
			
			CupadminAccess ca = new CupadminAccess();
			ca.setModifiedDate(today);
			ca.setModifiedBy(currentUser.getUsername());
			ca.setAccessList(user.getAccessRights().getRightsAsString());
			ca.setUserId(userUpdate.getUserId());
			
			em.getTransaction().begin();
			em.merge(ca);
			em.getTransaction().commit();

			EBookAccessWorker.insertEBookAccess(userUpdate.getUserId(), ebookAccessList, currentUser.getUsername());
			
			// --jubs add "insert publisher" 20100222
			publisherMember.setUser_id(user.getUserId());
			
			if(uiPublisherId != null)
			{
				publisherId = (String)uiPublisherId.getValue();
			}
			else
			{
				Publisher publisher = HttpUtil.getPublisherFromCurrentSession();
				if(publisher == null)
					publisherId = EBookManagedBean.NO_PUBLISHER_FILTER;
				else
					publisherId = publisher.getPublisherId();
			}

			
			publisherMember.setPublisher_id(publisherId);
//			System.out.println("publisherMember ID = "+user.getUserId());
//			System.out.println("publisherPublisher ID = "+publisherId);
			
			if(updateType.equals(CREATE) || !publisherBean.isAlreadyAmember(user.getUserId())){
				publisherBean.insertPublisherMember(publisherMember);
			} else {
				publisherBean.updatePublisherMember(publisherMember);
			}
			
			if (userUpdate.getUserId().equals(currentUser.getUserId())) {
				HttpSession session = HttpUtil.getHttpSession(true);
				session.setAttribute(SessionAttribute.USER, userUpdate);
			}
			
			//create user audit trail
			if(updateType.equals(CREATE)){
				EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Add new user - ".concat((String) userUpdate.getUsername()));
			}else{
				EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Update user - ".concat(userUpdate.getUsername()));
			}
			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = null;
			if (updateType.equals(CREATE)) {
				message = new FacesMessage(CREATE_SUCCESSFUL);
				
				SystemUserMailer.sendMail(userUpdate);
				
			} else if (updateType.equals(UPDATE)) {
				message = new FacesMessage(UPDATE_SUCCESSFUL);
			}
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			LogManager.info(SystemUserUpdateBean.class, updateType + "_User complete: " + userUpdate.getUsername());
			
		} catch (PersistenceException pe) {
			hasErrors = true;
			String errorMessage = pe.getCause().getMessage();
			LogManager.error(SystemUserUpdateBean.class, "****************\n" + errorMessage + "\n****************");
			if (errorMessage.contains("ORA-00001")) {
				if (errorMessage.contains(User.USERNAME_CONSTRAINT)) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage message = new FacesMessage(USERNAME_EXISTS);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					LogManager.error(SystemUserUpdateBean.class, "Caught SQLException: " + USERNAME_EXISTS);
				} else if (errorMessage.contains(User.EMAIL_CONSTRAINT)) {
					//TODO: -C2- NO EXISTING CONSTRAINT ON DB
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage message = new FacesMessage(EMAIL_EXISTS);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					LogManager.error(SystemUserUpdateBean.class, "Caught SQLException: " + EMAIL_EXISTS);
				}
			}
		} finally {
			JPAUtil.close(em);
		}
	}
	
	public void updateBookAuditList(ActionEvent event) {
		LogManager.info(BookAuditTrailBean.class, "Acquiring Book Audit Trail List...");

		String eisbn 		= (String) uiAuditEisbn.getSubmittedValue();
		String seriesId 	= (String) uiAuditSeriesId.getSubmittedValue();
		String username 	= (String) uiAuditUsername.getSubmittedValue();
		String auditStatus	= (String) uiAuditStatus.getSubmittedValue();
		String dateFrom 	= uiAuditDateFromDay.getSubmittedValue() + "-" + uiAuditDateFromMonth.getSubmittedValue()+ "-" + uiAuditDateFromYear.getSubmittedValue();
		String dateTo 		= uiAuditDateToDay.getSubmittedValue() + "-" + uiAuditDateToMonth.getSubmittedValue() + "-" + uiAuditDateToYear.getSubmittedValue();
		
		String publisherId 	= null;
		if(uiPublisherId != null)
		{
			publisherId 	= (String) uiPublisherId.getSubmittedValue();
		}
		else
		{
			Publisher publisher = HttpUtil.getPublisherFromCurrentSession();
			if(publisher != null)
				publisherId = publisher.getPublisherId();
		}
		
		StringBuilder startSql = new StringBuilder(
				"SELECT" +
				" DISTINCT AT.eisbn" +
				" FROM audit_trail_ebooks AT" +
				" WHERE"
			);

		//sql conditions
		
		List<String> params = new ArrayList<String>();
		params.add(dateFrom);
		params.add(dateTo);
		
		int i = 1;
		StringBuilder conditions = new StringBuilder(
				" AT.audit_date >= TO_DATE( ?" + (i++) + " ,'DD-MON-YYYY')" +
				" AND AT.audit_date <= TO_DATE( ?" + (i++) + " ,'DD-MON-YYYY') "
			);
				
		if (eisbn != null && eisbn.length() != 0)
		{
			conditions.append(" AND AT.EISBN = ?" + (i++));
			params.add(eisbn);
		}
		
		if (seriesId != null && seriesId.length() != 0)
		{
			conditions.append(" AND AT.SERIES_CODE = ?" + (i++));
			params.add(seriesId);
		}
		
		if (username != null && username.length() != 0)
		{
			conditions.append(" AND AT.USERNAME = ?" + (i++));
			params.add(username);
		}
		
		if (auditStatus != null && auditStatus.length() != 0)
		{
			conditions.append(" AND AT.STATUS = ?" + (i++));
			params.add(auditStatus);
		}


					
		StringBuilder sql = startSql.append(conditions);
		
		StringBuilder sqlWrapper = new StringBuilder();
		sqlWrapper.append( "SELECT DISTINCT a.eisbn FROM(" + sql + ")a "                     );
		sqlWrapper.append( "JOIN ebook_publisher_isbn b "                         );
		sqlWrapper.append( "ON(a.eisbn = b.isbn OR a.eisbn LIKE REPLACE(b.isbn, '*', '%')) " );
		sqlWrapper.append( "AND b.active = 'Y' "                                             );
		sqlWrapper.append( "AND b.exclude = 'N' "                                            );
		
		int paramPublisherId = 0;
		boolean hasPublisherFilter = false;
		if (publisherId != null && publisherId.length() != 0 && !publisherId.equals("-1"))
		{
			paramPublisherId = i++;
			hasPublisherFilter = true;
			
			params.add(publisherId);
			sqlWrapper.append(" AND b.publisher_id = ?" + paramPublisherId + " ");
		}
		
						
		StringBuilder sqlWrapper2 = new StringBuilder();
		sqlWrapper2.append("SELECT * FROM (" + sqlWrapper.toString() + ")a ");
		sqlWrapper2.append("WHERE a.eisbn NOT IN ( ");
		sqlWrapper2.append(                  "SELECT e.isbn ");
		sqlWrapper2.append(                  "FROM ebook e ");
		sqlWrapper2.append(                  "RIGHT OUTER JOIN (SELECT isbn FROM ebook_publisher_isbn WHERE exclude = 'Y' AND active = 'Y' " + 
				       (hasPublisherFilter ? "AND publisher_id = ?" + (paramPublisherId) + " " : "" ) +	" ) p ");
		sqlWrapper2.append(                  "ON(e.isbn = p.isbn OR e.isbn LIKE REPLACE(p.isbn, '*', '%')) ");
		sqlWrapper2.append(" )");

		LogManager.info(BookAuditTrailBean.class, "Dynamic Query: " + sqlWrapper2);

		int c = (Integer)uiResultsPerPage.getSubmittedValue() == null ? 10 : (Integer)uiResultsPerPage.getSubmittedValue();
//		pager = new PersistencePager<UserBookAuditTrail>(c, countSql.toString(), sql.toString(), "userBookAuditTrailResult", params); //editted persistence pager
		pager = new PersistencePager<UserBookAuditTrail>(c, sqlWrapper2.toString(), "userBookAuditTrailResult", params.toArray(new String[0]));
		
		initPaging();
		resolveBookResultList();
	}
	
	

	
	private void resolveBookResultList() {
		bookAuditList = pager.getCurrentResults();
		String[] eisbns = new String[bookAuditList.size()];
		int j = 0;
		for (UserBookAuditTrail ubat : bookAuditList) {
			eisbns[j++] = ubat.getEisbn();
		}
		
		bookResultList = worker.ebookTitlesByEisbn(eisbns);//EBookIndexSearch.ebookTitlesByEisbn(eisbns);
		Collections.sort(bookResultList);
		cleanupLists();
		LogManager.info(SystemUserUpdateBean.class, "bookAuditList: " + bookAuditList.size() + " bookResultList: " + bookResultList.size());
	}
	
	public void addToRemoveFromAccessList(ValueChangeEvent event) {
		if ((Boolean)event.getNewValue()) {
			
			FacesContext context = FacesContext.getCurrentInstance();  
			Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap(); 
			String bookId = (String)(event.getComponent().getAttributes().get("bookId") != null ?  event.getComponent().getAttributes().get("bookId") : requestMap.get("bookId"));
			
			removeFromAccessList.add(bookId);
		}
	}
	
	public void addToInsertToAccessList(ValueChangeEvent event) {
		if ((Boolean)event.getNewValue()) {
			EBookBean ebb = (EBookBean) event.getComponent().getAttributes().get("eBookBean");
			insertToAccessList.add(ebb);
		}
	}
	
	public void setAllEbooksToAccessList(ActionEvent event) {	
		ebookAccessList = new ArrayList<EBookBean>();
		ebookAccessList.add(new EBookBean(EBookAccess.ALL_EBOOKS));
		cleanupLists();
	}
	
	public void setAllOrgEbooksToAccessList(ActionEvent event) {	
		ebookAccessList = new ArrayList<EBookBean>();
		ebookAccessList.add(new EBookBean(EBookAccess.ALL_PUBLISHER_EBOOKS));
		cleanupLists();
	}
	
	public void removeAllEbooksToAccessList(ActionEvent event) {
		ebookAccessList = new ArrayList<EBookBean>();
		cleanupLists();
	}
	
	
	public void clearBookResultAndAccessList(ActionEvent event){
		bookResultList = new ArrayList<EBookBean>();
		removeAllEbooksToAccessList(event);
	}
	
	public void insertEbooksToAccessList(ActionEvent event) {
		LogManager.info(SystemUserUpdateBean.class, "Inserting " + insertToAccessList.size() + " selected ebooks...");
		
		boolean allEbooksAccess = ebookAccessList != null 
			&& ebookAccessList.size() == 1
			&& ebookAccessList.get(0).getBookId().equals(EBookAccess.ALL_EBOOKS);
		
		boolean allPublisherEbooksAccess = ebookAccessList != null 
		&& ebookAccessList.size() == 1
		&& ebookAccessList.get(0).getBookId().equals(EBookAccess.ALL_PUBLISHER_EBOOKS);
		
		String publisherId 	= null;
		if(uiPublisherId != null)
			publisherId 	= (String) uiPublisherId.getSubmittedValue();
		else
		{
			Publisher publisher = HttpUtil.getPublisherFromCurrentSession();
			if(publisher != null)
				publisherId = publisher.getPublisherId();
		}
		
		boolean clearEbookAccessList = allEbooksAccess || 
		                               allPublisherEbooksAccess || 
		                               clearEbookAccessList(publisherId);
		
		if (clearEbookAccessList)
			ebookAccessList = new ArrayList<EBookBean>();
					
		for (EBookBean ebb : insertToAccessList) {
			if (!ebookAccessList.contains(ebb)) {
				ebookAccessList.add(ebb);
				Collections.sort(ebookAccessList);
			}
		}
		cleanupLists();
	}
		
	public void removeEbooksFromAccessList(ActionEvent event) {
		LogManager.info(SystemUserUpdateBean.class, "Removing " + removeFromAccessList.size() + " selected ebooks...");
		for (String s : removeFromAccessList) {
			ebookAccessList.remove(new EBookBean(s));
		}
		cleanupLists();
	}
	
	private void cleanupLists() {
		removeFromAccessList = new ArrayList<String>();
		insertToAccessList = new ArrayList<EBookBean>();
	}
	
	public int getBookResultListSize() {
		int size = -1;
		if (bookResultList != null) 
			size = bookResultList.size();
		return size;
	}
	
	public int getEbookAccessListSize() {
		int size = -1;
		if (ebookAccessList != null) 
			size = ebookAccessList.size();
		return size;
	}
	
	public static final SelectItem[] monthMap=
	{
		new SelectItem("Jan","Jan"),
		new SelectItem("Feb","Feb"),
		new SelectItem("Mar","Mar"),
		new SelectItem("Apr","Apr"),
		new SelectItem("May","May"),
		new SelectItem("Jun","Jun"),
		new SelectItem("Jul","Jul"),
		new SelectItem("Aug","Aug"),
		new SelectItem("Sep","Sep"),
		new SelectItem("Oct","Oct"),
		new SelectItem("Nov","Nov"),
		new SelectItem("Dec","Dec")
	};
	
	public SelectItem[] getMonthMap(){
		return monthMap;
	}
	
	public String getMonths(int i) {
    	String months[] = {"Jan","Feb","Mar","Apr","May","Jun",
    					   "Jul","Aug","Sep","Oct","Nov","Dec"};    	
    	return months[i];    	
    }
	
	public String updateSystemUserNav() {
		return hasErrors ? RedirectTo.SYSTEM_USERS_UPDATE : RedirectTo.SYSTEM_USERS;
	}
	
	public String getDisplayPassword() {
		displayPassword = Misc.isEmpty(user.getPassword()) ? "" : Password.deobfuscate(user.getPassword());
		return displayPassword;
	}

	public void setDisplayPassword(String displayPassword) {
		this.displayPassword = displayPassword;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UIInput getUiUsername() {
		return uiUsername;
	}

	public void setUiUsername(UIInput uiUsername) {
		this.uiUsername = uiUsername;
	}

	public UIInput getUiPassword() {
		return uiPassword;
	}

	public void setUiPassword(UIInput uiPassword) {
		this.uiPassword = uiPassword;
	}

	public UIInput getUiConfirmPassword() {
		return uiConfirmPassword;
	}

	public void setUiConfirmPassword(UIInput uiConfirmPassword) {
		this.uiConfirmPassword = uiConfirmPassword;
	}

	public UIInput getUiEmail() {
		return uiEmail;
	}

	public void setUiEmail(UIInput uiEmail) {
		this.uiEmail = uiEmail;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public UIInput getUiFirstName() {
		return uiFirstName;
	}

	public void setUiFirstName(UIInput uiFirstName) {
		this.uiFirstName = uiFirstName;
	}

	public UIInput getUiLastName() {
		return uiLastName;
	}

	public void setUiLastName(UIInput uiLastName) {
		this.uiLastName = uiLastName;
	}

	public UIInput getUiAuditEisbn() {
		return uiAuditEisbn;
	}

	public void setUiAuditEisbn(UIInput uiAuditEisbn) {
		this.uiAuditEisbn = uiAuditEisbn;
	}

	public UIInput getUiAuditSeriesId() {
		return uiAuditSeriesId;
	}

	public void setUiAuditSeriesId(UIInput uiAuditSeriesId) {
		this.uiAuditSeriesId = uiAuditSeriesId;
	}

	public UIInput getUiAuditUsername() {
		return uiAuditUsername;
	}

	public void setUiAuditUsername(UIInput uiAuditUsername) {
		this.uiAuditUsername = uiAuditUsername;
	}

	public String getAuditDateFromDay() {
		return auditDateFromDay;
	}

	public void setAuditDateFromDay(String auditDateFromDay) {
		this.auditDateFromDay = auditDateFromDay;
	}

	public String getAuditDateFromMonth() {
		return auditDateFromMonth;
	}

	public void setAuditDateFromMonth(String auditDateFromMonth) {
		this.auditDateFromMonth = auditDateFromMonth;
	}

	public String getAuditDateFromYear() {
		return auditDateFromYear;
	}

	public void setAuditDateFromYear(String auditDateFromYear) {
		this.auditDateFromYear = auditDateFromYear;
	}

	public String getAuditDateToDay() {
		return auditDateToDay;
	}

	public void setAuditDateToDay(String auditDateToDay) {
		this.auditDateToDay = auditDateToDay;
	}

	public String getAuditDateToMonth() {
		return auditDateToMonth;
	}

	public void setAuditDateToMonth(String auditDateToMonth) {
		this.auditDateToMonth = auditDateToMonth;
	}

	public String getAuditDateToYear() {
		return auditDateToYear;
	}

	public void setAuditDateToYear(String auditDateToYear) {
		this.auditDateToYear = auditDateToYear;
	}

	public SelectItem[] getDayItems() {
		return dayItems;
	}

	public void setDayItems(SelectItem[] dayItems) {
		this.dayItems = dayItems;
	}

	public SelectItem[] getYearItems() {
		return yearItems;
	}

	public void setYearItems(SelectItem[] yearItems) {
		this.yearItems = yearItems;
	}

	public boolean isHasErrors() {
		return hasErrors;
	}

	public void setHasErrors(boolean hasErrors) {
		this.hasErrors = hasErrors;
	}

	public List<EBookBean> getEbookAccessList() {
		return ebookAccessList;
	}

	public void setEbookAccessList(ArrayList<EBookBean> ebookAccessList) {
		this.ebookAccessList = ebookAccessList;
	}

	public ArrayList<UserBookAuditTrail> getBookAuditList() {
		return bookAuditList;
	}

	public void setBookAuditList(ArrayList<UserBookAuditTrail> bookAuditList) {
		this.bookAuditList = bookAuditList;
	}

	public UISelectOne getUiAuditDateFromDay() {
		return uiAuditDateFromDay;
	}

	public void setUiAuditDateFromDay(UISelectOne uiAuditDateFromDay) {
		this.uiAuditDateFromDay = uiAuditDateFromDay;
	}

	public UISelectOne getUiAuditDateFromMonth() {
		return uiAuditDateFromMonth;
	}

	public void setUiAuditDateFromMonth(UISelectOne uiAuditDateFromMonth) {
		this.uiAuditDateFromMonth = uiAuditDateFromMonth;
	}

	public UISelectOne getUiAuditDateFromYear() {
		return uiAuditDateFromYear;
	}

	public void setUiAuditDateFromYear(UISelectOne uiAuditDateFromYear) {
		this.uiAuditDateFromYear = uiAuditDateFromYear;
	}

	public UISelectOne getUiAuditDateToDay() {
		return uiAuditDateToDay;
	}

	public void setUiAuditDateToDay(UISelectOne uiAuditDateToDay) {
		this.uiAuditDateToDay = uiAuditDateToDay;
	}

	public UISelectOne getUiAuditDateToMonth() {
		return uiAuditDateToMonth;
	}

	public void setUiAuditDateToMonth(UISelectOne uiAuditDateToMonth) {
		this.uiAuditDateToMonth = uiAuditDateToMonth;
	}

	public UISelectOne getUiAuditDateToYear() {
		return uiAuditDateToYear;
	}

	public void setUiAuditDateToYear(UISelectOne uiAuditDateToYear) {
		this.uiAuditDateToYear = uiAuditDateToYear;
	}

	public ArrayList<EBookBean> getBookResultList() {
		return bookResultList;
	}

	public void setBookResultList(ArrayList<EBookBean> bookResultList) {
		this.bookResultList = bookResultList;
	}

	public int getGoToPage() {
		return goToPage;
	}

	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public ArrayList<SelectItem> getPages() {
		return pages;
	}

	public void setPages(ArrayList<SelectItem> pages) {
		this.pages = pages;
	}

	public void setPagingText(String pagingText) {
		this.pagingText = pagingText;
	}

	public UISelectOne getUiGoToPage() {
		return uiGoToPage;
	}

	public void setUiGoToPage(UISelectOne uiGoToPage) {
		this.uiGoToPage = uiGoToPage;
	}

	public UISelectOne getUiResultsPerPage() {
		return uiResultsPerPage;
	}

	public void setUiResultsPerPage(UISelectOne uiResultsPerPage) {
		this.uiResultsPerPage = uiResultsPerPage;
	}

	public PublisherUpdateBean getPublisherBean() {
		return publisherBean;
	}

	public void setPublisherBean(PublisherUpdateBean publisherBean) {
		this.publisherBean = publisherBean;
	}

	public UISelectOne getUiPublisherId() {
		return uiPublisherId;
	}

	public void setUiPublisherId(UISelectOne uiPublisherId) {
		this.uiPublisherId = uiPublisherId;
	}

	public PublisherMemberBean getPublisherMember() {
		return publisherMember;
	}

	public void setPublisherMember(PublisherMemberBean publisherMember) {
		this.publisherMember = publisherMember;
	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public UISelectBoolean getUiCanManagePublishers() {
		return uiCanManagePublishers;
	}

	public void setUiCanManagePublishers(UISelectBoolean uiCanManagePublishers) {
		this.uiCanManagePublishers = uiCanManagePublishers;
	}

	public boolean isCanManagePublishers() {
		return canManagePublishers;
	}

	public void setCanManagePublishers(boolean canManagePublishers) {
		this.canManagePublishers = canManagePublishers;
	}
		
	public UIInput getUiAuditStatus() {
		return uiAuditStatus;
	}

	public void setUiAuditStatus(UIInput uiAuditStatus) {
		this.uiAuditStatus = uiAuditStatus;
	}
	
	private boolean clearEbookAccessList(String publisherIdToCompare){
		if(ebookAccessList != null && !ebookAccessList.isEmpty() && publisherIdToCompare != null && publisherIdToCompare.length() != 0)
		{
			EBookBean ebookBean = ebookAccessList.get(0);
			String publisherId = EBookDAO.getPublisherIdByBookId(ebookBean.getBookId());
			
			if(publisherIdToCompare.equals(publisherId))
				return false;
			else
				return true;
		}
		return false;
	}
	
	private void initSelectPublisher(){
		Publisher lpublisher = HttpUtil.getPublisherFromCurrentSession();
		if(lpublisher != null)
		{
			if(!"-1".equals(lpublisher.getPublisherId()))
				publisherBean.initPublisherById(lpublisher.getPublisherId());
			else
				publisherBean.initAllPublishers();
		}
		else //temporary until not all users are added to a publisher
		{
			publisherBean.initAllPublishers();
		}
	}

	
}
