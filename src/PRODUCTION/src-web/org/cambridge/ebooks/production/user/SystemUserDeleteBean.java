package org.cambridge.ebooks.production.user;

/**
 * 
 * @author jgalang
 * 
 */

import java.util.ArrayList;
import java.util.HashSet;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

public class SystemUserDeleteBean {
	private static final Log4JLogger LogManager = new Log4JLogger(SystemUserDeleteBean.class);
	public static final String DELETE_SUCCESSFUL = "User has been successfully deleted.";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );
	
	public ArrayList<User> deleteList = new ArrayList<User>();
	
	public HashSet<String> deleteSetIds = new HashSet<String>();
	
	public String deleteSystemUsersNav() {
		return RedirectTo.SYSTEM_USERS_DELETE;
	}
	
	public void addToList(ValueChangeEvent event) {
		String userId = (String) event.getComponent().getAttributes().get("userId");
		String username = (String) event.getComponent().getAttributes().get("username");
		String email = (String) event.getComponent().getAttributes().get("email");
		if ((Boolean)event.getNewValue()) {
			if (deleteSetIds.add(userId)) {
				User user = new User();
				user.setUserId(userId);
				user.setUsername(username);
				user.setEmail(email);
				deleteList.add(user);
			}
		}
	}
	
	public void confirmDelete(ActionEvent event) {
		LogManager.info(SystemUserDeleteBean.class, "Attempting to delete " + deleteList.size() + " user/s...");
	}
	
	public void deleteSystemUsers(ActionEvent event) {
		HashSet<String> idList = (HashSet<String>) event.getComponent().getAttributes().get("deleteSetIds");
		LogManager.info(SystemUserDeleteBean.class, "Removing " + idList.size() + " user/s...");
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();		
		StringBuffer buffer = new StringBuffer();
		User currentUser = HttpUtil.getUserFromCurrentSession();
		for (String s : idList) {
			buffer.append(s).append(",");
			
			User user = em.find(User.class, s);
			
			//create user audit trail
			EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Delete user - ".concat(user.getUsername()));
			
			em.remove(user);
		}
		em.getTransaction().commit();
		em.close();
		clearDeleteList();
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message = new FacesMessage(DELETE_SUCCESSFUL);
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		context.addMessage("alertMessage", message);
		LogManager.info(SystemUserDeleteBean.class, "User deleted.");
	}
		
	public void cancelDelete(ActionEvent event) {
		clearDeleteList();
	}
	
	private void clearDeleteList() {
		deleteList = new ArrayList<User>();
		deleteSetIds = new HashSet<String>();
	}
	
	public String deleteSystemUsers() {
		return RedirectTo.SYSTEM_USERS;
	}

	public boolean getDeleteListEmpty() {
		return deleteList == null || deleteList.size() == 0;
	}

	public ArrayList<User> getDeleteList() {
		return deleteList;
	}

	public void setDeleteList(ArrayList<User> deleteList) {
		this.deleteList = deleteList;
	}

	public HashSet<String> getDeleteSetIds() {
		return deleteSetIds;
	}

	public void setDeleteSetIds(HashSet<String> deleteSetIds) {
		this.deleteSetIds = deleteSetIds;
	}
}
