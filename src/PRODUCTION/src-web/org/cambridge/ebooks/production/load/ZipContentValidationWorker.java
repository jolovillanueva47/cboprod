package org.cambridge.ebooks.production.load;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.ejb.util.XmlValidator;
import org.jboss.util.file.Files;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ZipContentValidationWorker {
	
	private static final Logger LOGGER = Logger.getLogger(ZipContentValidationWorker.class);
	
	private static final String HEADER_DTD = System.getProperty("dtd.file.path");
	private static final String SLASH = System.getProperty("file.separator");

	public static String validateHeader(String path, String fname, String fileId) {
		String validXML = "Zip Validation Complete";
		String headerFilePath = "";
		try {
			headerFilePath = extractHeader(path.replace(fname, ""), fname, fileId);
			
			LOGGER.info("headerFilePath: " + headerFilePath);

			XmlValidator xmlValidator = XmlValidator.newInstance();
			xmlValidator.setXmlFilePath(headerFilePath);
			xmlValidator.setDtdFilePath(HEADER_DTD);
			xmlValidator.validate();
		} catch (ZipException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			validXML = e.getMessage();
		} catch (TransformerException e) {
			e.printStackTrace();
		} finally {
			if (!headerFilePath.isEmpty()) {
				System.out.println("delete dummy header result = "
						+ Files.delete(headerFilePath));
			}
		}
		return validXML;
	}
	
	public static String validateManifestAndContents(String path, String fname, String fileId){
		File manifest = null;
		String output = "Zip Validation Complete";
		String manifestFile = "";
		try {
			manifestFile = extractManifest(path.replace(fname, ""), fname, fileId);
			LOGGER.info("manifest file path: " + manifestFile);
			manifest = new File(manifestFile);
			
			if(manifest.isFile()){
				// Manifest exists, validate contents
				output = validateContents(path.replace(fname, "") + fname.replace(".zip", "") + "_" + fileId + ".zip", manifest);
			}
		} catch (ZipException e) {
			output = e.getMessage();
		} catch (IOException e) {
			output = e.getMessage();
		} catch (NullPointerException e){
			output = "Manifest does not exist.";
		} catch (SAXException e) {
			output = e.getMessage();
		} catch (ParserConfigurationException e){
			output = e.getMessage();
		}
		finally {
			if(!manifestFile.isEmpty()){
				System.out.println("Delete dummy manifest file result = "+Files.delete(manifestFile));
			}
		}
		return output;
	}
	
	private static String validateContents(String zipFile, File manifest) throws SAXException, IOException, ParserConfigurationException{
		ArrayList<String> xmlList = getXmlList(manifest);
		ArrayList<String> zipList = getZipList(zipFile);
		
		String output = "Invalid";
		
		if(xmlList.size() == zipList.size()){
			output = "Zip Validation Complete";
		} else {
			if (zipList.size() > xmlList.size()){
				output = "Missing files in XML declaration = ";
				for (String left:getMissing(zipList, xmlList)){
					output = output + " " + left;
				}
			} else {
				output = "Missing files in Zip file = ";
				for (String left:getMissing(xmlList, zipList)){
					output = output + " " + left;
				}
			}
		}
		
		return output;
	}
	
	private static ArrayList<String> getMissing(ArrayList<String> complete, ArrayList<String> incomplete){
		for(String removeFromList: incomplete){
			complete.remove(removeFromList);
		}
		return complete;
	}
	
	private static ArrayList<String>getXmlList(File manifest) throws SAXException, IOException, ParserConfigurationException{
		ArrayList<String> xmlList = new ArrayList<String>();
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		
		docBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse (manifest);
		
		
		NodeList nodeList = doc.getElementsByTagName("file");
        for(int x=0,size= nodeList.getLength(); x<size; x++) {
        	xmlList.add(nodeList.item(x).getAttributes().getNamedItem("filename").getNodeValue());
        }
		return xmlList;
	}
	
	@SuppressWarnings("rawtypes")
	public static ArrayList<String> getZipList(String sourcefile) {
		ArrayList<String> zipList = new ArrayList<String>();
		
		try {
			ZipFile zipFile = new ZipFile(sourcefile);
			Enumeration zipEntries = zipFile.entries();
			String filename = "";
			String isbn = getIsbn(sourcefile); 
			while (zipEntries.hasMoreElements()) {
				filename = ((ZipEntry) zipEntries.nextElement())
				.getName().replace("/", SLASH).replace(isbn+SLASH, "");
				if(! filename.trim().isEmpty() && ! filename.trim().equals("manifest.xml"))
					zipList.add(filename);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return zipList;
	}
	
	
	private static String extractHeader(String folder, String fname, String fileId) throws ZipException, IOException{
		return extractFile(folder, getIsbn(folder + fname), fname, fileId, ".xml");
	}
	
	private static String extractManifest(String folder, String fname, String fileId) throws ZipException, IOException{
		return extractFile(folder, "manifest", fname, fileId, ".xml");
	}

	private static String extractFile(String folder, String fileToExtract, String zipFileName, String fileId, String extension)
			throws ZipException, IOException, NullPointerException {

		String zipFile = folder + zipFileName.replace(".zip", "") + "_" + fileId + ".zip";
		LOGGER.info("*****zip file = "+zipFile);

		ZipFile zf = new ZipFile(zipFile);
		int BUFFER = 2048;
		int currentByte;
		String output;

		try {
			ZipEntry header = zf.getEntry((getIsbn(zipFile) + "/" + fileToExtract + extension));
			BufferedInputStream br = new BufferedInputStream(
					zf.getInputStream(header));

			byte data[] = new byte[BUFFER];

			// write the current file to disk
			output = folder + fileToExtract + "_" + fileId + extension;
			FileOutputStream fos = new FileOutputStream(output);
			BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
			
			// read and write until last byte is encountered
			while ((currentByte = br.read(data, 0, BUFFER)) != -1) {
				dest.write(data, 0, currentByte);
			}

			dest.flush();
			dest.close();
			fos.close();
			br.close();
		} finally {
			zf.close();
		}
		return output;
	}

	private static String getIsbn(String filename) {
		filename = filename.replace("/", SLASH);
		
		return filename.substring(filename.lastIndexOf(SLASH) + 1, 
				filename.lastIndexOf(SLASH) + 14);
	}
	
}
