package org.cambridge.ebooks.production.load;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class UploadPostCheckServlet extends HttpServlet {
	/**
	 * 
	 */
	
	private static final Logger LOGGER = Logger.getLogger(UploadPostCheckServlet.class);
	
	private static final long serialVersionUID = 1L;
	private static final String CANNOT_BE_PROCESSED = "File cannot be processed";
	private static final String INVALID_XML = "The Header XML is invalid. ";
	private static final String TMP_UPLOAD = System.getProperty("upload.tmp");
	private static final String DIR_UPLOAD = System.getProperty("upload.dir");

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String isbn = request.getParameter("isbn");
		String fileId = request.getParameter("fileid");
		
		LOGGER.info("getParameter isbn: " + isbn);
		LOGGER.info("getParameter fileid: " + fileId);
		
//		String path = "/app/ebooks/uploads/tmp";
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		String uploadMessage = "Zip Validation Complete";
		String additionalmessage = "";
		
		// do further checking: Validate Header XML
		String msg = "";
		if (!(msg = ZipContentValidationWorker.validateHeader(TMP_UPLOAD+isbn, isbn, fileId)).equals("Zip Validation Complete")) {
			uploadMessage = CANNOT_BE_PROCESSED;
			additionalmessage = ": " + INVALID_XML + msg;
		}
		
		// do further checking: Verify Manifest exists
		if ((uploadMessage.equals("Zip Validation Complete")) && !(msg = ZipContentValidationWorker.validateManifestAndContents(TMP_UPLOAD + isbn, isbn, fileId)).equals("Zip Validation Complete")) {
			uploadMessage = CANNOT_BE_PROCESSED;
			additionalmessage = ": " +msg;
		}
		
		// file validation completed, transfer to main upload folder
		// 1st param format: /app/ebooks/upload/tmp/978xxxxxxxxxx_001FileIdHere.zip
		movefile(TMP_UPLOAD+(isbn.replace(".zip", "") + "_" + fileId + ".zip"), DIR_UPLOAD+(isbn.replace(".zip", "")+"_"+dateTimeNow()+".zip"));
		
		out.print(additionalmessage.isEmpty() ? uploadMessage : additionalmessage);
		out.flush();
		out.close();
		
	}
	
	private static String dateTimeNow(){
		Calendar dateNow = Calendar.getInstance();
		String str = String.valueOf(dateNow.get(Calendar.YEAR)) +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)+1), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + 
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0");
		
		return str;
	}
	
	
	public static void movefile(String srFile, String dtFile) {
		try {
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);

			// For Append the file.
			// OutputStream out = new FileOutputStream(f2,true);

			// For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			System.out.println("File copied.");
			if (f1.delete()) {
				System.out.println("Source file deleted.");
			} else {
				System.out.println("[ERROR] Source file not deleted!!");
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage() + " in the specified directory.");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
