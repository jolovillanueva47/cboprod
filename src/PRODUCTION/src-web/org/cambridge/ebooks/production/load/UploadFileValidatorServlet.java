package org.cambridge.ebooks.production.load;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.publisher.PublisherMemberBean;
import org.cambridge.ebooks.production.jpa.user.EBookAccess;
import org.cambridge.ebooks.production.util.PersistenceUtil;

@SuppressWarnings("unchecked")
public class UploadFileValidatorServlet extends HttpServlet {

	private static final long serialVersionUID = -4593749126886770125L;
	private static final String CAN_BE_UPLOADED = "CAN_BE_UPLOADED";
	private static final String CANNOT_BE_UPLOADED = "CANNOT_BE_UPLOADED";
	private static final String NOT_ASSIGNED = "Book not assigned to user.";
	
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

 		String isbn = request.getParameter("isbn");
		String userid = request.getParameter("userid");
//		String path = request.getParameter("path");
		String uploadMessage = "";
		String additionalmessage = "";

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		if ((isbn != null && isbn.length() > 0)
				&& (userid != null && userid.length() > 0)) {
			List<EBookAccess> accessList = null;
			HttpSession session = request.getSession(false);
			if (session != null)
				accessList = (ArrayList<EBookAccess>) session.getAttribute(SessionAttribute.EBOOK_ACCESS);

			// check user access to book
			if (accessList != null) {
				boolean hasFullAccess = accessList
						.contains(EBookAccess.ALL_EBOOKS_ACCESS);
				boolean hasOrgAccess = accessList
						.contains(EBookAccess.ALL_PUBLISHER_EBOOKS_ACCESS);
				boolean hasIsbnAccess = false;

				if (hasFullAccess) {
					hasIsbnAccess = true;
				} else if (hasOrgAccess) {
					Publisher publisher = (Publisher) session
							.getAttribute(SessionAttribute.PUBLISHER);
					if (publisher != null) {
						String isbnPublisherId = isbnPublisherId(isbn);
						String memberPublisherId = publisher.getPublisherId();
						if (isbnPublisherId.equals(memberPublisherId)) {
							hasIsbnAccess = true;
						}
					}
				} else {
					EBookAccess ebookAccess = PersistenceUtil.searchEntity(new EBookAccess(), EBookAccess.SEARCH_EBOOK_ACCESS_BY_ID_AND_ISBN, isbn, userid);
					if (ebookAccess != null) {
						if (ebookAccess.getBookId().contains(isbn))
							hasIsbnAccess = true;
					}
				}

				if (hasIsbnAccess) {
					EBook ebook = PersistenceUtil.searchEntity(new EBook(), EBook.SEARCH_EBOOK_BY_ISBN, isbn);
					if (ebook != null) {
						if (EBookBean.canBeUploaded(Status.getStatus(ebook.getStatus())))
							uploadMessage = CAN_BE_UPLOADED;
						else {
							uploadMessage = CANNOT_BE_UPLOADED;
							additionalmessage = Status.getStatus(ebook.getStatus()).getDescription();
						}
					} else {// assume book not yet in database
						if (hasFullAccess) {
							uploadMessage = CAN_BE_UPLOADED;
						} else {
							uploadMessage = CANNOT_BE_UPLOADED;
							additionalmessage = NOT_ASSIGNED;
						}
					}

//					String msg = "";
//					if (uploadMessage == CAN_BE_UPLOADED) {
//						// do further checking: Validate Header XML
//						if (!(msg = ZipContentValidationWorker.validateHeader(path, isbn + ".zip")).equals("Valid")) {
//							uploadMessage = CANNOT_BE_UPLOADED;
//							additionalmessage = INVALID_XML + msg;
//						}
//						
//						// do further checking: Verify Manifest exists
//						if (uploadMessage == CAN_BE_UPLOADED && !(msg = ZipContentValidationWorker.validateManifestAndContents(path, isbn + ".zip")).equals("Valid")) {
//							uploadMessage = CANNOT_BE_UPLOADED;
//							additionalmessage = msg;
//						}
//					}
				} else {
					uploadMessage = CANNOT_BE_UPLOADED;
					additionalmessage = NOT_ASSIGNED;
				}
			}
		}

		out.print(uploadMessage + ":" + additionalmessage);
		out.flush();
		out.close();
	}

	private String isbnPublisherId(String isbn) {
		final String NO_PUBLISHER_ID = "";
		PublisherMemberBean member = PersistenceUtil.searchEntity(new PublisherMemberBean(), PublisherMemberBean.GET_MEMBER, isbn);
		if (member != null)
			return member.getPublisher_id();
		else
			return NO_PUBLISHER_ID;
	}

}
