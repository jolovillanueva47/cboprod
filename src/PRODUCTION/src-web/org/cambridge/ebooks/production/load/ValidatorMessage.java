package org.cambridge.ebooks.production.load;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * @author Karlson A. Mulingtapang
 * Validator.java - Validate upload then create a text file
 */
public class ValidatorMessage {	
	private static final String UPLOAD_PATH = System.getProperty("uploads.dir");
	private static final String DEF_USERID = System.getProperty("default.userid");
	private static final String DEF_USERNAME = System.getProperty("default.username");
	
	private String userid;
	private String username;
	private Properties properties;
	private InitialContext ctx;
	private Queue queue;
	private QueueConnectionFactory qcf;
	
	public ValidatorMessage() throws Exception {		
		properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
						"org.jnp.interfaces.NamingContextFactory");
		properties.put(Context.URL_PKG_PREFIXES,
						"org.jboss.naming:org.jnp.interfaces");
		//properties.put(Context.PROVIDER_URL, System.getProperty("provider.url"));
		properties.put(Context.PROVIDER_URL, getHostAddress() + ":1099");
		ctx = new InitialContext(properties);

		queue = (Queue) ctx.lookup("queue/uploader/logger");
		qcf = (QueueConnectionFactory) ctx.lookup("ConnectionFactory");
	}	
	
	public static void main(String[] args) throws Exception {	
		System.out.println("=== Start Validator Message ===");
		String filename = args[0];			
		ValidatorMessage validator = new ValidatorMessage();
		QueueConnection qc = null;
		QueueSession qs = null;
		QueueSender sender = null;	
		Message message = null;
		
		try {				
			System.out.println("=== args[0]:" + filename);
			String parsedFilename = validator.getHeaderFilename(filename, true);
			validator.getSessionOutput(parsedFilename);
			
			qc = validator.getQcf().createQueueConnection();
			
			qs = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			sender = qs.createSender(validator.getQueue());	
			message = qs.createMapMessage();
			
			message.setStringProperty("FILENAME", parsedFilename);
			
			// use default
			if(DEF_USERID != null) {
				System.out.println("=== USERID:" + DEF_USERID);
				message.setStringProperty("USERID", DEF_USERID);				
			} else {
				System.out.println("=== USERID:" + validator.getUserid());
				message.setStringProperty("USERID", validator.getUserid());				
			}
			
			if(DEF_USERNAME != null) {
				System.out.println("=== USERNAME:" + DEF_USERNAME);
				message.setStringProperty("USERNAME", DEF_USERNAME);
			} else {
				System.out.println("=== USERNAME:" + validator.getUsername());
				message.setStringProperty("USERNAME", validator.getUsername());
			}
			
			sender.send(message);
		} catch (Exception e) {			
			e.printStackTrace();
		} finally {
			sender.close();
			qs.close();
			qc.close();			
		}
	}
	
	public String getHeaderFilename(String filename, boolean excludeExtension) {
		String result = "";
//		int index = filename.indexOf("_");
		int index = filename.lastIndexOf("/");
		if(index > -1) {
			result = filename.substring(index + 1);
			int index2 = result.indexOf("_");
			if(index2 > -1) {
				result = result.substring(0, index2);
			} else {
				index2 = result.indexOf(".zip");
				result = result.substring(0, index2);
			}
			if(!excludeExtension) {			
				result = result + ".xml";
			} 
		} else {
			int index2 = filename.lastIndexOf("/");
			result = filename.substring(index2 + 1);
			if(!excludeExtension) {			
				result = result + ".xml";
			} 
		}
//		if(index > -1) {
//			String filepath = filename.substring(0, index);
//			int index2 = filepath.lastIndexOf("/");
//			result = filepath.substring(index2 + 1);
//			if(!excludeExtension) {			
//				result = result + ".xml";
//			} 
//		} else {
//			int index2 = filename.lastIndexOf("/");
//			result = filename.substring(index2 + 1);
//			if(!excludeExtension) {			
//				result = result + ".xml";
//			} 
//		}
		System.out.println("=== Header filename:" + result);
		return result;
	}
	
	public void getSessionOutput(String filename) {
		BufferedReader br = null;
		try {
			if(new File(getSessionOutputPath(filename)).exists()) {
				br = new BufferedReader(new FileReader(getSessionOutputPath(filename)));				
				setUserid(br.readLine());
				setUsername(br.readLine());				
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getSessionOutputPath(String filename) {		
		return UPLOAD_PATH + filename + ".txt";
	}

	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the queue
	 */
	public Queue getQueue() {
		return queue;
	}

	/**
	 * @param queue the queue to set
	 */
	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	/**
	 * @return the qcf
	 */
	public QueueConnectionFactory getQcf() {
		return qcf;
	}

	/**
	 * @param qcf the qcf to set
	 */
	public void setQcf(QueueConnectionFactory qcf) {
		this.qcf = qcf;
	}
	
	
	private static String getHostAddress() throws UnknownHostException{
		java.net.InetAddress i = java.net.InetAddress.getLocalHost();
		return i.getHostAddress(); // IP address only
	}
	
}
