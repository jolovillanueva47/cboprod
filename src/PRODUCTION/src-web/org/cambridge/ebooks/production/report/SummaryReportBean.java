package org.cambridge.ebooks.production.report;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.ebook.EBookDAO;
import org.cambridge.ebooks.production.ebook.EBookManagedBean;
import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.ebook.content.EBookContentWorker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.StringUtil;

/**
 * @author Karlson A. Mulingtapang
 * SummaryReportBean.java - Summary generation
 */
public class SummaryReportBean {
	private static final Log4JLogger LogManager = new Log4JLogger(SummaryReportBean.class);
	private final static String EBOOK_MANAGED_BEAN_ATTR = "eBookManagedBean";
	private final static String BOOK_ID_ATTR = "bookId";
	private final static String ISBN_ATTR = "isbn";	
	private final static String BOOK_TITLE_ATTR = "bookTitle";

	private String bookId;
	private String isbn;
	private String bookTitle;
	
	private List<EBookContentBean> eBookContents;
	
//	private EBookManagedBean eBookManagedBean;
	private EBookContentWorker eBookContentWorker = new EBookContentWorker();
	private PrintWriter printWriter;	
	
	/**
	 * Download csv format of eBook list
	 * @param event ActionEvent
	 */
	public void downloadEBookSummary(ActionEvent event) {			
//		setEBookManagedBean((EBookManagedBean)event.getComponent().getAttributes().
//				get(EBOOK_MANAGED_BEAN_ATTR));
		
		try {
			setPrintWriter(getPrintWriter("eBooks_summary", "csv", "UTF-8"));
			buildEBookSummaryFile();
		} catch (IOException e) {
			LogManager.error(SummaryReportBean.class, "---[IOException]:" + e.getMessage());
			//e.printStackTrace();
		} catch (Exception e) {
			LogManager.error(SummaryReportBean.class, "---[Exception]:" + e.getMessage());
			//e.printStackTrace();
		}
	}
	
	/**
	 * Download csv format of eBook content list
	 * @param event ActionEvent
	 * @throws SQLException 
	 */
	public void downloadEBookContentSummary(ActionEvent event) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();  
		Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap(); 
		String bookId = (String)(event.getComponent().getAttributes().get(BOOK_ID_ATTR) != null ?  event.getComponent().getAttributes().get(BOOK_ID_ATTR) : requestMap.get(BOOK_ID_ATTR));
	
		setBookId(bookId);	
		setIsbn((String)event.getComponent().getAttributes().get(ISBN_ATTR));			
		setBookTitle((String)event.getComponent().getAttributes().get(BOOK_TITLE_ATTR));
		setEBookContents((List<EBookContentBean>)getEBookContentWorker().ebookContents(getBookId()));
		
		try {
			setPrintWriter(getPrintWriter(getIsbn() + "_summary", "csv", "UTF-8"));
			buildEBookContentsSummaryFile();
		} catch (IOException e) {
			LogManager.error(SummaryReportBean.class, "---[IOException]:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			LogManager.error(SummaryReportBean.class, "---[Exception]:" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Get eBook others
	 * @return Map<String, EBookContentBean>
	 */
	private Map<String, EBookContentBean> getEBookOthers() {
		Map<String, EBookContentBean> eBookOthers = new HashMap<String, EBookContentBean>(); 
		return getEBookContentWorker().initEbookOthers(eBookOthers, getBookId());
	}
	
	/**
	 * Set response header and content type of file to be downloaded
	 * @param filename String
	 * @param extension String
	 * @param encoding String
	 * @return PrintWriter
	 * @throws IOException
	 */
	private PrintWriter getPrintWriter(String filename, String extension, String encoding) throws IOException {		
		HttpServletResponse response = 
			(HttpServletResponse)FacesContext.
			getCurrentInstance().getExternalContext().getResponse();
		response.setHeader("Content-Disposition", "attachment;filename=" + filename + "." + extension);		
		response.setContentType(getContentType(extension));		
		response.setCharacterEncoding(encoding); 
		
		return response.getWriter();
	}
	
	/**
	 * Get response content type
	 * @param extension String
	 * @return String
	 */
	private String getContentType(String extension) {
		String contentType = "";
		if(extension.equals("csv")) {
			contentType = "application/csv";
		} else if(extension.equals("xls")) {
			contentType = "application/vnd.ms-excel";
		} else if(extension.equals("html")) {
			contentType = "text/html";
		}
		
		return contentType;
	}
	
	/**
	 * Build eBook summary information of file to be downloaded 
	 * @throws IOException
	 */
//	private void buildEBookSummaryFile() throws Exception, IOException {
//		try {
//			
//			List<EBook> ebookListFromDB = EBookDAO.searchAllEBooks();
////			List<EBookBean> eBookListFromIndex = EBookIndexSearch.ebookTitles();
//			List<EBookBean> ebookList = EBookWorker.wrapToEBookBean(ebookListFromDB);
//			
//			getPrintWriter().println("Number of EBooks: " + ebookList.size());
//			getPrintWriter().print("\nISBN,Main Title,Status\n\n");
//			for(EBookBean eBook : ebookList) {
//				String title = eBook.getTitle().replaceAll("&#8211;", "-");
//				
//				title = StringUtil.stripTags(title);
//				
//				if(title.contains(",")){
//					title = "\"" + title + "\"";
//				}				
//				
//				getPrintWriter().print("=\"" + eBook.getIsbn() + "\"" + ","); 
//				getPrintWriter().print(StringUtil.parseHtmlUnicodeString(title) + ","); 
//				getPrintWriter().print(eBook.getStatusDescription() + "\n");
//			}
//			getPrintWriter().flush();
//		} catch (IOException e) {
//			throw e;
//		} catch(Exception e) {
//			throw e;
//		} finally {		
//			getPrintWriter().close();
//		}
//	}
	
	//TODO: fixme! rvillamor
	private void buildEBookSummaryFile() throws Exception, IOException {
	try {
		
		//List<EBook> ebookListFromDB = EBookDAO.searchAllEBooks();
		EBookManagedBean ebookBean = HttpUtil.getManagedBean(new EBookManagedBean(), "bookBean");
		String publisherFilter = ebookBean.getSrPublisherFilter();
		//System.out.println("publisherFilter: " + ebookBean.getPublisherFilter());
		
		List<EBookBean> ebookList = EBookDAO.summaryReport(publisherFilter);
		
		Map<Integer, Integer> summary = new LinkedHashMap<Integer, Integer>();
		
		summary.put(Integer.parseInt(Status.NOTLOADED.getStatus()), 0);
		summary.put(Integer.parseInt(Status.LOADED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.RELOADED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.PROOFED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.REJECTED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.APPROVE.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.DISAPPROVED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.REAPPROVE.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.PUBLISHED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.UNPUBLISHED.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.EMBARGO.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.RETURN.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.RETURNTOUPLOADER.getStatus()), 0); 
		summary.put(Integer.parseInt(Status.DELETED.getStatus()), 0);
		
		summary.putAll(EBookDAO.statusSummary(publisherFilter));
		Iterator<Integer> it = summary.keySet().iterator();
		
		Map<String, Integer> summaries = totalSummaries(summary);
		Iterator<String> summariesIterator = summaries.keySet().iterator();
		boolean spaceUsed = false;
		
		getPrintWriter().print("ISBN,Main Title,Status,Publisher,Product,Print Pub Date,Online Pub Date,Online Loaded Date\n\n");
		//start - added " Publisher,Product, "
		int statusesLength = summary.size();
		int booksLength = ebookList.size();
		if(statusesLength > booksLength)
		{
			for(int index=0; index<statusesLength; index++)
			{
				if(index < booksLength)
				{
					
					String title = ebookList.get(index).getTitle().replaceAll("&#8211;", "-");
					
					title = StringUtil.stripTags(title);
					
					if(title.contains(",")){
						title = "\"" + title + "\"";
					}				
					
					if(it.hasNext())
					{
						getPrintWriter().print(ebookList.get(index).getIsbn() + ","); 
						getPrintWriter().print(StringUtil.parseHtmlUnicodeString(title) + ","); 
						getPrintWriter().print(ebookList.get(index).getStatusDescription() + ",");
						
						//start
						getPrintWriter().print(ebookList.get(index).getPublisher() + ",");
						getPrintWriter().print(ebookList.get(index).getProduct() + ",");
						//end
						
						getPrintWriter().print("=\"" + ebookList.get(index).getPubPrintDate() + "\"" + ","); 
						getPrintWriter().print("=\"" + ebookList.get(index).getPubOnlineDate()  + "\"" + ","); 
						getPrintWriter().print("=\"" + ebookList.get(index).getLoadedOnlineDate()  + "\"" + ",");
						
						Integer status = it.next();
						getPrintWriter().print(" , " + Status.getStatus(status).getDescription() + ", " + summary.get(status) + "\n");
					}
					else
					{
									
						getPrintWriter().print(ebookList.get(index).getIsbn() + ","); 
						getPrintWriter().print(StringUtil.parseHtmlUnicodeString(title) + ","); 
						getPrintWriter().print(ebookList.get(index).getStatusDescription() + ",");
						
						//start
						getPrintWriter().print(ebookList.get(index).getPublisher() + ",");
						getPrintWriter().print(ebookList.get(index).getProduct() + ",");
						//end
						
						getPrintWriter().print("=\"" + ebookList.get(index).getPubPrintDate() + "\"" + ","); 
						getPrintWriter().print("=\"" + ebookList.get(index).getPubOnlineDate()  + "\"" + ","); 
						
						if(summariesIterator.hasNext())
						{
							if(!spaceUsed)
							{
								getPrintWriter().print("=\"" + ebookList.get(index).getLoadedOnlineDate()  + "\"" + "\n");
								spaceUsed = true;
							}
							else
							{
								getPrintWriter().print("=\"" + ebookList.get(index).getLoadedOnlineDate()  + "\"" + ",");
								String summaryTitle = summariesIterator.next();
								getPrintWriter().print(" , " + summaryTitle + ", " + summaries.get(summaryTitle) + "\n");
							}
						}
						else
							getPrintWriter().print("=\"" + ebookList.get(index).getLoadedOnlineDate()  + "\"" + "\n");

					}
				}
				else
				{
					
					if(it.hasNext())
					{
						Integer status = it.next();
						getPrintWriter().print(" , , , , , , , , , "  + Status.getStatus(status).getDescription() + ", " + summary.get(status) + "\n");
					}
					
					
				}
			}
			
			while(summariesIterator.hasNext())
			{
				String summaryTitle = summariesIterator.next();
				getPrintWriter().print("\n" + " , , , , , , , , , " + summaryTitle + ", " + summaries.get(summaryTitle));
				
			}
		}
		else
		{
			for(EBookBean eBook : ebookList) 
			{
				String title = eBook.getTitle().replaceAll("&#8211;", "-");
				
				title = StringUtil.stripTags(title);
				
				if(title.contains(",")){
					title = "\"" + title + "\"";
				}				
				
				if(it.hasNext())
				{
									
					getPrintWriter().print(eBook.getIsbn() + ","); 
					getPrintWriter().print(StringUtil.parseHtmlUnicodeString(title) + ","); 
					getPrintWriter().print(eBook.getStatusDescription() + ",");
					
					//start
					getPrintWriter().print(eBook.getPublisher() + ",");
					getPrintWriter().print(eBook.getProduct() + ",");
					//end
					
					getPrintWriter().print("=\"" + eBook.getPubPrintDate() + "\"" + ","); 
					getPrintWriter().print("=\"" + eBook.getPubOnlineDate()  + "\"" + ","); 
					getPrintWriter().print("=\"" + eBook.getLoadedOnlineDate()  + "\"" + ",");
					
					Integer status = it.next();
					getPrintWriter().print(" , " + Status.getStatus(status).getDescription() + ", " + summary.get(status) + "\n");
				}
				else
				{
						
					getPrintWriter().print(eBook.getIsbn() + ","); 
					getPrintWriter().print(StringUtil.parseHtmlUnicodeString(title) + ","); 
					getPrintWriter().print(eBook.getStatusDescription() + ",");
					
					//start
					getPrintWriter().print(eBook.getPublisher() + ",");
					getPrintWriter().print(eBook.getProduct() + ",");
					//end
					
					getPrintWriter().print("=\"" + eBook.getPubPrintDate() + "\"" + ","); 
					getPrintWriter().print("=\"" + eBook.getPubOnlineDate()  + "\"" + ","); 
					
					if(summariesIterator.hasNext())
					{
						if(!spaceUsed)
						{
							getPrintWriter().print("=\"" + eBook.getLoadedOnlineDate()  + "\"" + "\n");
							spaceUsed = true;
						}
						else
						{
							getPrintWriter().print("=\"" + eBook.getLoadedOnlineDate()  + "\"" + ",");
							String summaryTitle = summariesIterator.next();
							getPrintWriter().print(" , " + summaryTitle + ", " + summaries.get(summaryTitle) + "\n");
						}
					}
					else
						getPrintWriter().print("=\"" + eBook.getLoadedOnlineDate()  + "\"" + "\n");

				}
			}
		}
		
		
		getPrintWriter().flush();
	} catch (IOException e) {
		LogManager.error(SummaryReportBean.class, "---[IOException]: buildEBookSummaryFile() " + e.getMessage());
	} catch(Exception e) {
		LogManager.error(SummaryReportBean.class, "---[Exception]: buildEBookSummaryFile() " + e.getMessage());
	} finally {		
		getPrintWriter().close();
	}
}
	
	/**
	 * Build eBook contents summary information of file to be downloaded 
	 * @throws IOException
	 * @throws Exception
	 */
	private void buildEBookContentsSummaryFile() throws IOException, Exception {		
		try {			
			getPrintWriter().println("ISBN: " + getIsbn());
			getPrintWriter().println("Book Title: " + StringUtil.parseHtmlUnicodeString(getBookTitle()));
			getPrintWriter().println("Number of Contents: " + getEBookContentCount());
			getPrintWriter().print("\nContent title,Status,Embargo,Remarks\n\n");
			
			writeEBookOtherToOutputStream("xml");
			
			for(EBookContentBean eBookContent : eBookContents) {				
				String title = eBookContent.getTitle();
				title = StringUtil.stripTags(title);
				
				if(title.contains(",")){
					title = "\"" + title + "\"";
				}			
				
				title = StringUtil.parseHtmlUnicodeString(title);
				
				if(eBookContent.isNotLoadedPartTitle()) {
					getPrintWriter().print(title + ",None\n");
				} else {
					getPrintWriter().print(title + ",");
					getPrintWriter().print(Status.getStatus(eBookContent.getEbookContent().getStatus()).
							getDescription() + ",");
					if(eBookContent.getEbookContent().getEmbargoDate() == null) {
						getPrintWriter().print("None" + ",");
					} else {
						getPrintWriter().print(eBookContent.getEbookContent().getEmbargoDate().toString() + ",");
					}
					getPrintWriter().print(eBookContent.getEbookContent().getRemarks() + "\n");
				}				
			}

			writeEBookOtherToOutputStream("image_standard");
			writeEBookOtherToOutputStream("image_thumb");
			
			getPrintWriter().flush();
		} catch (IOException e) {
			throw e;
		} finally {
			getPrintWriter().close();
		}
	}
	
	/**
	 * Write eBook other summary to output stream
	 * @param key String
	 * @throws IOException
	 */
	private void writeEBookOtherToOutputStream(String key) throws IOException {
		if(getEBookOthers().containsKey(key)) {				
			if(key.equals("xml")) {
				getPrintWriter().print(getEBookOthers().get(key).
						getEbookContent().getFileName() + " (header.xml),");
			} else {
				getPrintWriter().print(getEBookOthers().get(key).
						getEbookContent().getFileName() + ",");
			}
			getPrintWriter().print(Status.getStatus(getEBookOthers().get(key).
					getEbookContent().getStatus()).getDescription() + ",");
			if(getEBookOthers().get(key).getEbookContent().getEmbargoDate() == null) {
				getPrintWriter().print("None,");
			} else {
				getPrintWriter().print(getEBookOthers().get(key).getEbookContent()
						.getEmbargoDate().toString() + ",");
			}
			getPrintWriter().print(getEBookOthers().get(key).getEbookContent().getRemarks() + "\n");
		}
	}
	
	/**
	 * Get eBook content count
	 * @return int
	 */
	private int getEBookContentCount() {
		int contentCount = getEBookContents().size();
		if(getEBookOthers().containsKey("xml")) {
			contentCount++;
		}
		
		if(getEBookOthers().containsKey("image_standard")) {
			contentCount++;
		}
		
		if(getEBookOthers().containsKey("image_thumb")) {
			contentCount++;
		}
		
		return contentCount;
	}

	/**
	 * @return the eBookContents
	 */
	private List<EBookContentBean> getEBookContents() {
		return eBookContents;
	}

	/**
	 * @param bookContents the eBookContents to set
	 */
	private void setEBookContents(List<EBookContentBean> bookContents) {
		eBookContents = bookContents;
	}

	/**
	 * @return the eBookContentWorker
	 */
	private EBookContentWorker getEBookContentWorker() {
		return eBookContentWorker;
	}

	/**
	 * @return the bookId
	 */
	private String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	private void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the isbn
	 */
	private String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	private void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the printWriter
	 */
	private PrintWriter getPrintWriter() {
		return printWriter;
	}

	/**
	 * @param printWriter the printWriter to set
	 */
	private void setPrintWriter(PrintWriter printWriter) {
		this.printWriter = printWriter;
	}

	/**
	 * @return the eBookManagedBean
	 */
//	private EBookManagedBean getEBookManagedBean() {
//		return eBookManagedBean;
//	}

	/**
	 * @param bookManagedBean the eBookManagedBean to set
	 */
//	private void setEBookManagedBean(EBookManagedBean bookManagedBean) {
//		eBookManagedBean = bookManagedBean;
//	}

	/**
	 * @return the bookTitle
	 */
	private String getBookTitle() {
		return bookTitle;
	}

	/**
	 * @param bookTitle the bookTitle to set
	 */
	private void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	
	
	private Map<String, Integer> totalSummaries(Map<Integer, Integer> statusAndTotal){
		
		int totalLoaded = 0;
		int proofreadAbove = 0;
		int approveAbove = 0;
		Map<String, Integer> result = new LinkedHashMap<String, Integer>();
		
		if(statusAndTotal != null && !statusAndTotal.isEmpty())
		{
			List<Integer> excludeForTotalProofread = Arrays.asList(new Integer[]{-1, 0, 1, 3, 100});
			List<Integer> excludeForTotalApproved = Arrays.asList(new Integer[]{-1, 0, 1, 2, 3, 5, 6, 100});
			
			Set<Map.Entry<Integer, Integer>> entry = statusAndTotal.entrySet();
			for(Map.Entry<Integer, Integer> m : entry)
			{
				if(!excludeForTotalProofread.contains(m.getKey()))
					proofreadAbove += m.getValue();
				
				if(!excludeForTotalApproved.contains(m.getKey()))
					approveAbove += m.getValue();
				
				totalLoaded += m.getValue();
			}
			
			result.put("Total Loaded", totalLoaded);
			result.put("Total Proofread or above", proofreadAbove);
			result.put("Total Approved or above", approveAbove);
			result.put("Total Published", statusAndTotal.get(7));
		}
		
		return result;
	}
}
