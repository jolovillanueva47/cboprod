package org.cambridge.ebooks.production.delivery.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class DoiData {

	private String doi;
	private String resource;
	
	public DoiData(String doi, String resource) {
		this.doi = doi;
		this.resource = resource;
	}
	
	public String getDoi() {
		return doi;
	}
	public String getResource() {
		return resource;
	}
}
