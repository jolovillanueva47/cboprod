package org.cambridge.ebooks.production.delivery.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Root {

	private Xml xml;
	private DoiBatch doiBatch;
	
	public Root(Xml xml, DoiBatch doiBatch) {
		this.xml = xml;
		this.doiBatch = doiBatch;
	}
	
	public Xml getXml() {
		return xml;
	}
	public DoiBatch getDoiBatch() {
		return doiBatch;
	}
	
	
}
