package org.cambridge.ebooks.production.delivery.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Pages {

	private String firstPage;
	private String lastPage;
	
	public Pages(String firstPage, String lastPage) {
		this.firstPage = firstPage;
		this.lastPage = lastPage;
	}
	
	public String getFirstPage() {
		return firstPage;
	}
	public String getLastPage() {
		return lastPage;
	}
}
