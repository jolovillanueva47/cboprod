package org.cambridge.ebooks.production.delivery.crossref.xml;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author jgalang
 * 
 */

public class Head {
	private String doiBatchId;
	private String timestamp;
	private Depositor depositor;
	private String registrant;
	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyyMMddkkmmss");
	
	public Head(String doiBatchId, Date time, Depositor depositor, String registrant) {
		this.doiBatchId = doiBatchId;
		this.timestamp = FORMATTER.format(time);
		this.depositor = depositor;
		this.registrant = registrant;
	}
	
	public String getDoiBatchId() {
		return doiBatchId;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public String getRegistrant() {
		return registrant;
	}

	public Depositor getDepositor() {
		return depositor;
	}
	
	
	
}
