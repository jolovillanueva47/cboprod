package org.cambridge.ebooks.production.delivery;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.delivery.crossref.CrossRefDeliveryWorker;
import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.jpa.delivery.DoiBatch;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdPartyDeliveryStatus;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.ebooks.production.util.XMLParser;
import org.cambridge.util.Misc;

public class EBookDeliveryWorker {
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );

	public static final String XML_ONLY = "xmlOnly";
	public static final String XML_AND_SEND = "xmlSend";

	public String deliverEBook(String bookId, String eisbn, List<EBookContentBean> ebookContentList,
			boolean deliverToCrossRef, String deliveryOption) {
		
		String crossRefXmlFileName = null;
		
		String username = HttpUtil.getUserFromCurrentSession().getUsername();
		Date date = new Date();
		
		EntityManager em = emf.createEntityManager();
		try {
			DoiBatch db = new DoiBatch(new Date(), username);
			em.getTransaction().begin();
			em.persist(db);
			em.getTransaction().commit();
	
			boolean withError = false;
			String errorMessage = "";
			if (deliverToCrossRef) {
				crossRefXmlFileName = CrossRefDeliveryWorker.processEBookXml(bookId, eisbn, ebookContentList, username, db.getDoiBatchId(), date, deliveryOption);
				XMLParser parser = new XMLParser();
				errorMessage = parser.validatateCrossRef(eisbn, crossRefXmlFileName);
				withError = Misc.isNotEmpty(errorMessage);
	
				if (deliveryOption.equals(EBookDeliveryWorker.XML_AND_SEND)) {
					
					if (!withError) {
						int responseCode = CrossRefDeliveryWorker.sendDeposit(eisbn, crossRefXmlFileName);
						withError = !(responseCode==200);
					}
					
					ThirdPartyDeliveryStatus deliveryStatus = new ThirdPartyDeliveryStatus();
					deliveryStatus.setThirdPartyId(ThirdPartyDeliveryStatus.THIRD_PARTY_ID_CROSSREF);
					deliveryStatus.setContentId(bookId);
					deliveryStatus.setFileType(ThirdPartyDeliveryStatus.FILE_TYPE_XML);
					deliveryStatus.setDepositor(username);
					deliveryStatus.setStatus(withError);
					deliveryStatus.setDeliverDate(date);
					deliveryStatus.setQueueDate(null);
					deliveryStatus.setReason(withError ? null : errorMessage);
					deliveryStatus.setDoiBatchId(db.getDoiBatchId());
					
					
					em.getTransaction().begin();
					em.persist(deliveryStatus);
					em.getTransaction().commit();
				}
			}
	
	
			if (withError && errorMessage.length()>0) {
	//			FacesContext context = FacesContext.getCurrentInstance();
	//			FacesMessage message = new FacesMessage("Error writing XML: " + errorMessage);
	//			message.setSeverity(FacesMessage.SEVERITY_INFO);
	//			context.addMessage("alertMessage", message);
				System.err.println("EBookDeliveryWorker.deliverEBook(String, String, List<EBookContentBean>, boolean, String) message: " + errorMessage);
			} else if (deliveryOption.equals(EBookDeliveryWorker.XML_AND_SEND)) {
	//			FacesContext context = FacesContext.getCurrentInstance();
	//			FacesMessage message = new FacesMessage(EBookDeliveryBean.DELIVER_SUCCESSFUL);
	//			message.setSeverity(FacesMessage.SEVERITY_INFO);
	//			context.addMessage("alertMessage", message);
				System.out.println("Sending Successful for bookId: "+bookId);
				renameFileToSuccess(eisbn, crossRefXmlFileName);
			}
			
			return crossRefXmlFileName;
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	private void renameFileToSuccess(String isbn, String crossRefXmlFileName){
		File oldname = new File(IsbnContentDirUtil.getPath(isbn) + crossRefXmlFileName);
		
		String newfilename = oldname.toString().replace("my", "ms").replace("mn", "ms");
		
		File newname = new File(newfilename);
		
		if(oldname.renameTo(newname)){
			System.out.println("crossref file renamed to: "+newname.getName());
		} else {
			System.out.println("***failed to rename crossref file!");
		}
	}
}
