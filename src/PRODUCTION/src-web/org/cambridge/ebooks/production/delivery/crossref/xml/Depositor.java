package org.cambridge.ebooks.production.delivery.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class Depositor {

	private String depositorName;
	private String depositorEmail;
	
	public Depositor(String depositorName, String depositorEmail) {
		this.depositorName = depositorName;
		this.depositorEmail = depositorEmail;
	}
	
	public String getDepositorName() {
		return depositorName;
	}
	public String getDepositorEmail() {
		return depositorEmail;
	}
}
