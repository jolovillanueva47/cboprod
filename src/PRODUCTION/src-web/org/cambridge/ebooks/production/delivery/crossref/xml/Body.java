package org.cambridge.ebooks.production.delivery.crossref.xml;


/**
 * 
 * @author jgalang
 * 
 */

public class Body {
	private Book book;
	
	public Body(Book book) {
		this.book = book;
	}

	public Book getBook() {
		return book;
	}
}
