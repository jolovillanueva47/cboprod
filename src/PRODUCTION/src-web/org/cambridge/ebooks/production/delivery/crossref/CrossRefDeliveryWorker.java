package org.cambridge.ebooks.production.delivery.crossref;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.cambridge.ebooks.production.util.DateUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.delivery.EBookDeliveryBean;
import org.cambridge.ebooks.production.delivery.crossref.xml.Root;
import org.cambridge.ebooks.production.document.EbookDetailsBean;
import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.jpa.publisher.SrPublisher;
import org.cambridge.ebooks.production.util.CrossRefProperties;
import org.cambridge.ebooks.production.util.EBooksConfiguration;
import org.cambridge.ebooks.production.util.EBooksProperties;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import HTTPClient.ModuleException;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class CrossRefDeliveryWorker implements CrossRefProperties{
	private static final Log4JLogger LogManager = new Log4JLogger(CrossRefDeliveryWorker.class);
	private static final String TEMPLATES_DIR = EBooksConfiguration.getProperty(EBooksProperties.TEMPLATES_DIR);
	
	public static String processEBookXml(
			String bookId,
			String eisbn,
			List<EBookContentBean> ebookContentList, 
			String username, 
			int doiBatchId, 
			Date deliverDate,
			String deliveryOption) {
		return processCrossRefXml(bookId, eisbn, ebookContentList, username, doiBatchId, deliverDate, null, deliveryOption);
	}
	
//	public static String processChapterXml(
//			String bookId,
//			String eisbn,
//			List<EBookContentBean> ebookContentList, 
//			String username, 
//			int doiBatchId, 
//			Date deliverDate, 
//			String contentId,
//			String deliveryOption) {
//		return processCrossRefXml(bookId, eisbn, ebookContentList, username, doiBatchId, deliverDate, contentId, deliveryOption);
//	}

	public static String processCrossRefXml(
			String bookId,
			String eisbn,
			List<EBookContentBean> ebookContentList, 
			String username, 
			int doiBatchId, 
			Date deliverDate, 
			String contentId,
			String deliveryOption) {
		
		String file = "";
		try {
			
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());

			cfg.setServletContextForTemplateLoading(
					HttpUtil.getHttpSession(false).getServletContext(), 
					TEMPLATES_DIR
				);

			Map<String, Root> root = new HashMap<String, Root>();
			Root xmlRoot = CrossRefXMLWorker.getRoot(username, doiBatchId, deliverDate, ebookContentList, 
					new EbookDetailsBean(bookId), contentId);
			root.put("root", xmlRoot);
			
			String sent = "y";
			if(deliveryOption.equals("xmlOnly")){
				sent = "n";
			}
			
			String filename = IsbnContentDirUtil.getPath(eisbn) + "/" + eisbn + "_" + DateUtil.getCurrentDate("yyyyMMddkkmm") + "m" + sent + ".crossref.xml";
			
			File f = new File(filename);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(f),XML_ENCODING));
			
			Template xml = cfg.getTemplate(DELIVERY_TEMPLATE);
	        xml.process(root, bw);
			
			file = f.getName();
	        
	        bw.close();
	        
	        formatXMLElement(filename, "span", "scp", true);
	        
		} catch (IOException e) {
			LogManager.error(EBookDeliveryBean.class, e.getMessage());
		} catch (TemplateException e) {
			LogManager.error(EBookDeliveryBean.class, e.getMessage());
		}
		return file;
	}

	public static int sendDeposit(String eisbnFolder, String XMLFile) {
		
		int responseCode = 0;
		
		try {

			String xmlCompletePath = IsbnContentDirUtil.getPath(eisbnFolder) + "/" + XMLFile;
			
			LogManager.info(CrossRefDeliveryWorker.class, "Posting to http://" + ORG_SITE + ":" + ORG_PORT);

			LogManager.info(CrossRefDeliveryWorker.class, "Sending: " + xmlCompletePath);
			HTTPClient.NVPair[] uploadOpts = new HTTPClient.NVPair[2];
			HTTPClient.NVPair[] uploadFileOpts = new HTTPClient.NVPair[1];

			uploadOpts[0] = new HTTPClient.NVPair("operation", "doMDUpload");
			uploadOpts[1] = new HTTPClient.NVPair("area", ORG_TEST_AREA); // live | test

			uploadFileOpts[0] = new HTTPClient.NVPair("fname", xmlCompletePath);

			HTTPClient.NVPair[] ct_hdr = new HTTPClient.NVPair[1];

			byte[] uploadBytes;

			HTTPClient.HTTPConnection httpConn = new HTTPClient.HTTPConnection(
					ORG_SITE, Integer.valueOf(ORG_PORT));

			uploadBytes = HTTPClient.Codecs.mpFormDataEncode(uploadOpts,
					uploadFileOpts, ct_hdr);
			
			HTTPClient.CookieModule.setCookiePolicyHandler(null);
			
			HTTPClient.HTTPResponse httpResp = null;
			//TODO JUBS MARKER sender part
			
			SrPublisher srp = PersistenceUtil.searchEntity(new SrPublisher(), SrPublisher.SEARCH_PUBLISHER_BY_ISBN, eisbnFolder);
			
			String username = srp.getUsername();
			String password = srp.getPassword();
			
			String postText = UPLOAD_SITE 
				+ "?login_id=" + username//USERNAME
				+ "&login_passwd=" + password//PASSWORD 
				+ "&area=" + ORG_TEST_AREA
				+ "&fuzzy=" + ORG_FUZZY;
			
			System.out.println(postText);
			
			boolean debug = false;
			
			if(! debug){
				httpResp = httpConn.Post(
						postText
					, uploadBytes
					, ct_hdr);
	
				responseCode = httpResp.getStatusCode();
			} else {
				responseCode = 200;
			}

			LogManager.info(CrossRefDeliveryWorker.class, "httpResp status is " + responseCode);

			httpConn.stop();
		} catch (IOException e) {
			LogManager.error(EBookDeliveryBean.class, e.getMessage());
		} catch (ModuleException e) {
			LogManager.error(EBookDeliveryBean.class, e.getMessage());
		}
		
		return responseCode;
	}
	
	public static void formatXMLElement(String filepath, String elementName, String replacement, boolean removeAttributes) {

		try 
		{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setNamespaceAware(true);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(filepath);

			NodeList nList = doc.getElementsByTagName(elementName);
			if(nList != null && nList.getLength() > 0)
			{
				for (int index = 0; index < nList.getLength(); index++)
				{
					doc.renameNode(nList.item(index), null, replacement);
					if (removeAttributes) 
					{
						NamedNodeMap nmap = nList.item(index).getAttributes();
						for (int ctr = 0; ctr < nmap.getLength(); ctr++) 
						{
							nList.item(index).getAttributes().removeNamedItem(nmap.item(ctr).getNodeName());
						}
					}
				}

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(filepath));
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(source, result);

				//remove the SCP tag. crude solution @_@
				removeScpTag(filepath);
				
				System.out.println("Done");
			}
		}
		catch (Exception ex)
		{
			System.out.println("[ERROR] formatXMLElement() " + ex.getMessage());
		}
	}

	public static void removeScpTag(String filePath){
		try {
			String str;
			StringBuffer sb = new StringBuffer();
			BufferedReader in = new BufferedReader(new FileReader(filePath));
			while ((str = in.readLine()) != null) {
				if(str.contains("<scp xmlns=\"\">") || str.contains("</scp>")){
					str = str.replace("<scp xmlns=\"\">", "").replace("</scp>", "");
				}
				sb.append(str +"\n");
			}
			in.close();
			printFile(filePath, sb.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static void printFile(String path, String text) throws IOException {
		Writer output = null;	    
	    File file = new File(path);
	    output = new BufferedWriter(new FileWriter(file));
	    output.write(text);
	    output.close();
	    System.out.println("File "+ path +" has been written");  
	}
}
