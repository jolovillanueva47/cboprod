package org.cambridge.ebooks.production.delivery.crossref.xml;

import java.util.ArrayList;

/**
 * 
 * @author jgalang
 * 
 */

public class BookMetadata {

	private String language;
	private ArrayList<PersonName> contributors;
	private ArrayList<String> titles;
	private SeriesMetadata seriesMetadata;
	private String volume;
	private String editionNumber;
	private PublicationDate printPublicationDate;
	private PublicationDate onlinePublicationDate;
	private String isbn;
	private String hardback;
	private String paperback;
	private Publisher publisher;
	private DoiData doiData;
	private String subTitle;
	
	public BookMetadata(
			String language,
			ArrayList<PersonName> contributors,
			ArrayList<String> titles,
			String subTitle,
			SeriesMetadata seriesMetadata,
			String volume,
			String editionNumber,
			PublicationDate printPublicationDate,
			PublicationDate onlinePublicationDate,
			String isbn,
			String hardback,
			String paperback,
			Publisher publisher,
			DoiData doiData
		) {
		this.language = language;
		this.contributors = contributors;
		this.titles = titles;
		this.subTitle = subTitle;
		this.seriesMetadata = seriesMetadata;
		this.volume = volume;
		this.editionNumber = editionNumber;
		this.printPublicationDate = printPublicationDate;
		this.onlinePublicationDate = onlinePublicationDate;
		this.isbn = isbn;
		this.hardback = hardback;
		this.paperback = paperback;
		this.publisher = publisher;
		this.doiData = doiData;
	}

	public ArrayList<PersonName> getContributors() {
		return contributors;
	}
	
	public ArrayList<String> getTitles() {
		return titles;
	}

	public String getLanguage() {
		return language;
	}
	
	public String getSubTitle() {
		return subTitle;
	}
	
	public SeriesMetadata getSeriesMetadata() {
		return seriesMetadata;
	}

	public String getVolume() {
		return volume;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public PublicationDate getPrintPublicationDate() {
		return printPublicationDate;
	}

	public PublicationDate getOnlinePublicationDate() {
		return onlinePublicationDate;
	}

	public String getIsbn() {
		return isbn;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public DoiData getDoiData() {
		return doiData;
	}
	
	public String getPaperback() {
		return paperback;
	}

	public void setPaperback(String paperback) {
		this.paperback = paperback;
	}

	public String getHardback() {
		return hardback;
	}

	public void setHardback(String hardback) {
		this.hardback = hardback;
	}
}
