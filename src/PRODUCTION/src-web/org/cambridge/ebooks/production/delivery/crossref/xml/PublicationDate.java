package org.cambridge.ebooks.production.delivery.crossref.xml;


/**
 * 
 * @author jgalang
 * 
 */

public class PublicationDate {

	private String mediaType;
	private String month;
	private String day;
	private String year;
	
	public PublicationDate(String mediaType, String month, String day, String year) {
		this.mediaType = mediaType;
		this.month = month;
		this.day = day;
		this.year = year;
	}
	
	public String getMediaType() {
		return mediaType;
	}
	public String getMonth() {
		return month;
	}
	public String getDay() {
		return day;
	}
	public String getYear() {
		return year;
	}
	
	
}
