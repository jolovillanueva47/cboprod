package org.cambridge.ebooks.production.delivery;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.ebook.content.EBookContentWorker;

public class BatchDelivery {

	public static void main(String[] args) {
		try {
			run();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void run() throws SQLException {

		try {
			System.out.println("Start Delivery");
			BufferedReader in = new BufferedReader(new FileReader("/app/ebooks/published_22Dec2009.txt"));
//			BufferedReader in = new BufferedReader(new FileReader("C:/Documents and Settings/jgalang/Desktop/published_200.txt"));
			String str;
			int i=0;
			while ((str = in.readLine()) != null) {
				String bookId = str.substring(0, str.indexOf("	"));
				String eisbn = str.substring(str.indexOf("	")+1);

				System.out.println("[" + ++i + "] " + bookId);
				
				List<EBookContentBean> ebookContentList = (new EBookContentWorker()).ebookContents(bookId); //removed: ,true
				
				EBookDeliveryWorker worker = new EBookDeliveryWorker();
				worker.deliverEBook(bookId, eisbn, ebookContentList, true, EBookDeliveryWorker.XML_AND_SEND);
			}
			
			in.close();
			System.out.println("Finished Delivery");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
