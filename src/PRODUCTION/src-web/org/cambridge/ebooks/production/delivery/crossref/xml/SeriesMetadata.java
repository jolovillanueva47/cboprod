package org.cambridge.ebooks.production.delivery.crossref.xml;

import java.util.ArrayList;

/**
 * 
 * @author jgalang
 * 
 */

public class SeriesMetadata {

	private ArrayList<String> titles;
	private String isbn;
	private DoiData doiData;
	
	public SeriesMetadata(ArrayList<String> titles, String isbn, DoiData doiData) {
		this.titles = titles;
		this.isbn = isbn;
		this.doiData = doiData;
	}

	public ArrayList<String> getTitles() {
		return titles;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public DoiData getDoiData() {
		return doiData;
	}
}
