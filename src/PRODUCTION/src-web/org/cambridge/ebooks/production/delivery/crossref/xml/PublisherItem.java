package org.cambridge.ebooks.production.delivery.crossref.xml;

/**
 * 
 * @author jgalang
 * 
 */

public class PublisherItem {

	private String itemNumber;
	
	public PublisherItem(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public String getItemNumber() {
		return itemNumber;
	}
}
