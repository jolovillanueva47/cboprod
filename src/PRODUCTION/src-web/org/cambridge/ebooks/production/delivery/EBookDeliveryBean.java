package org.cambridge.ebooks.production.delivery;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.production.ebook.EBookDAO;
import org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean;
import org.cambridge.ebooks.production.jpa.delivery.DoiBatch;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdPartyDeliveryStatus;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * 
 * @author jgalang
 * 
 */

public class EBookDeliveryBean implements ActionListener {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookDeliveryBean.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );

	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd-MMM-yyyy");
	public static final String DELIVER_SUCCESSFUL = " The contents of the e-book will be delivered to the recipients in a few moments.";
	
	private String crossRefDateDelivered;
	private String crossRefBatchId;
	private String depositorName;
	private String deliveryOption = EBookDeliveryWorker.XML_ONLY;
	private String crossRefXmlFileName;
	
	//USED BY JSF ON LOAD (ebook_chapter.jsp - inputHidden)
	private String bookId;
	private String ebookDeliveryStatus;
	
	private boolean deliverToCrossRef;
	
	//USED BY JSF ON LOAD (ebook_chapter.jsp - inputHidden)
	public String getEbookDeliveryStatus() {
		LogManager.info(EBookDeliveryBean.class, "getEbookDeliveryStatus()session: "+
				HttpUtil.getHttpSession(false).getAttribute("bookId"));
		
		
		ThirdPartyDeliveryStatus deliveryStatus = PersistenceUtil.searchEntity(
				new ThirdPartyDeliveryStatus(), 
				ThirdPartyDeliveryStatus.SEARCH_EBOOK_DELIVERY_STATUS, 
				new String[]{(String) HttpUtil.getHttpSession(false).getAttribute("bookId")});
		
		initFieldValues(deliveryStatus);
		return null;
	}
	
	//INIT CONTENT DELIVERY (ebook_chapter_details.jsp)
	public void processAction(ActionEvent event) {
		LogManager.info(EBookDeliveryBean.class, "processAction()... ");
		
		EBookContentManagedBean bookBean = (EBookContentManagedBean) event.getComponent().getAttributes().get("bookContentBean");
		EbookChapterDetailsBean chapterBean = (EbookChapterDetailsBean) event.getComponent().getAttributes().get("chapterBean");		
		
		ThirdPartyDeliveryStatus deliveryStatus = PersistenceUtil.searchEntity(
				new ThirdPartyDeliveryStatus(), 
				ThirdPartyDeliveryStatus.SEARCH_CHAPTER_DELIVERY_STATUS, 
				new String[]{bookBean.getBookId(), chapterBean.getContentId()});
		
		initFieldValues(deliveryStatus);
	}
	
	public void initFieldValues(ThirdPartyDeliveryStatus deliveryStatus) {
		resetFields();
		if (deliveryStatus != null) {
			depositorName = deliveryStatus.getDepositor();
			crossRefDateDelivered = FORMATTER.format(deliveryStatus.getDeliverDate());
			crossRefBatchId = String.valueOf(deliveryStatus.getDoiBatchId());
			LogManager.info(EBookDeliveryBean.class, "last deposit found: " + depositorName + " " + crossRefDateDelivered + " " + crossRefBatchId);
		}
	}
	
	public void resetFields() {
		depositorName = "";
		crossRefDateDelivered = "";
		crossRefBatchId = "";
	}
	
	public void deliverEBook(ActionEvent event) throws SQLException {
		LogManager.info(EBookDeliveryBean.class,"deliveryOption: " + deliveryOption);
		LogManager.info(EBookDeliveryBean.class,"crossref: " + deliverToCrossRef);
		
		EBookContentManagedBean bean = (EBookContentManagedBean) event.getComponent().getAttributes().get("bookContentBean");
		
		// DO NOT SEND CCO CHO SSO -- 86035 REMOVED since its no longer needed -jubs 20130131 
//		String productCode = EBookDAO.searchAllEBooksByBookId(getBookId()).get(0).getBookData().get(0).getProduct();
//		if(productCode.equals("CCO") || productCode.equals("CHO") || productCode.equals("SSO")) {
//			crossRefXmlFileName = "noFile";
//		} else {
			EBookDeliveryWorker worker = new EBookDeliveryWorker();
			crossRefXmlFileName = worker.deliverEBook(bean.getBookId(), bean.getEisbn(), bean.getEbookContentList(true), deliverToCrossRef, deliveryOption);
//		}
	}
	
	public void deliverContent(ActionEvent event) {
		LogManager.info(EBookDeliveryBean.class,"deliveryOption: " + deliveryOption);
		LogManager.info(EBookDeliveryBean.class,"crossref: " + deliverToCrossRef);
		
		EBookContentManagedBean bookBean = (EBookContentManagedBean) event.getComponent().getAttributes().get("bookContentBean");
		EbookChapterDetailsBean chapterBean = (EbookChapterDetailsBean) event.getComponent().getAttributes().get("chapterBean");
		String username = HttpUtil.getUserFromCurrentSession().getUsername();
		
		EntityManager em = emf.createEntityManager();
		try {
			DoiBatch db = new DoiBatch(new Date(), username);
			em.getTransaction().begin();
			em.persist(db);
			em.getTransaction().commit();
			
			chapterBean.getContentId();
		}
		finally {
			JPAUtil.close(em);
		}

		
	}
		
	public void setEbookDeliveryStatus(String ebookDeliveryStatus) {
		this.ebookDeliveryStatus = ebookDeliveryStatus;
	}

	public String getBookId() {
		bookId = HttpUtil.getHttpServletRequestParameter("id");
		return bookId;
	}
	
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getCrossRefDateDelivered() {
		return crossRefDateDelivered;
	}

	public void setCrossRefDateDelivered(String crossRefDateDelivered) {
		this.crossRefDateDelivered = crossRefDateDelivered;
	}

	public String getCrossRefBatchId() {
		return crossRefBatchId;
	}

	public void setCrossRefBatchId(String crossRefBatchId) {
		this.crossRefBatchId = crossRefBatchId;
	}

	public String getDepositorName() {
		return depositorName;
	}

	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	public boolean isDeliverToCrossRef() {
		return deliverToCrossRef;
	}

	public void setDeliverToCrossRef(boolean deliverToCrossRef) {
		this.deliverToCrossRef = deliverToCrossRef;
	}

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getCrossRefXmlFileName() {
		return crossRefXmlFileName;
	}

	public void setCrossRefXmlFileName(String crossRefXmlFileName) {
		this.crossRefXmlFileName = crossRefXmlFileName;
	}
	
	
}
