package org.cambridge.ebooks.production.util;

import javax.persistence.EntityManager;

/**
 * 
 * @author jlonceras
 *
 */
public class JPAUtil {
	
	private JPAUtil() {}
	
	public static void close(EntityManager em) {
		if (em != null && em.isOpen()) {
			em.close();
		}
	}
}
