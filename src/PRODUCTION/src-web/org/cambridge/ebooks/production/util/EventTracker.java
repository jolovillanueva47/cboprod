package org.cambridge.ebooks.production.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.common.ServiceLocatorException;
import org.cambridge.ebooks.production.embargo.bean.EBook;
import org.cambridge.ebooks.production.embargo.bean.EBookContent;



/**
 * @author andreau
 * @modified by rvillamor
 * @modified by jmendiola
 *
 */
public class EventTracker {
	private static final Log4JLogger LogManager = new Log4JLogger(EventTracker.class);
	public static final String AUDIT_REPOSITORY = System.getProperty("audit.trail.path");
	public static final String NEW_LINE = System.getProperty("line.separator");
	public static final String FILETYPE = ".csv";
	public static final String COMMA = ",";

	public static void sendEvent( String action, String userId, String remarks)  {
        try {
        	

        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");  
            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
        	payload.setStringProperty("ACTION", 	action 	);
            payload.setStringProperty("USER_ID", 	userId 	);
            payload.setStringProperty("REMARKS",	remarks );
            payload.setStringProperty("TYPE",		"User" 	);
            
            try { 
	            messageProducer.send( payload );
	            writeToCsv(payload, "USER_AUDIT_TRAIL_BOOKS");
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
        } catch (JMSException je) {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
        	System.out.println("je: "+je);
        } catch (ServiceLocatorException sle) {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb:" + sle.getMessage() ); 
        }
    }
	
	public static void sendBookEvent( String contentId, String isbn, String seriesCode,
									  String status, String remarks, String username,
									  String filename, String loadType)  {
        try {
        	
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
            payload.setStringProperty("CONTENT_ID",	contentId 	);
            payload.setStringProperty("EISBN",		isbn 		);
            payload.setStringProperty("SERIES_CODE",seriesCode	);
        	payload.setStringProperty("STATUS", 	status 		);            
            payload.setStringProperty("REMARKS",	remarks 	);
            payload.setStringProperty("FILENAME", 	filename 	);
            payload.setStringProperty("LOAD_TYPE", 	loadType 	);
            payload.setStringProperty("USERNAME", 	username 	);
            payload.setStringProperty("TYPE",		"Book" 		);
            
            try { 
	            messageProducer.send( payload );
	            writeToCsv(payload, "AUDIT_TRAIL_BOOKS");
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
        } catch (JMSException je) {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
        	System.out.println("je: "+je);
        } catch (ServiceLocatorException sle) {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb:" + sle.getMessage() ); 
        }
    }
	
	@Deprecated
	public static void sendEBookEvent( 	String isbn, String seriesCode, String title,
			  							String status, String username)  {
		try {			
			
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
			
			MessageProducer messageProducer = session.createProducer(destination);
			Message payload = session.createMapMessage();
			
			payload.setStringProperty("EISBN",		isbn 		);
			payload.setStringProperty("SERIES_CODE",seriesCode	);
			payload.setStringProperty("TITLE",		title		);
			payload.setStringProperty("STATUS", 	status 		);            
			payload.setStringProperty("USERNAME", 	username 	);
			payload.setStringProperty("TYPE",		"EBook_D" 	); //EBook changed to EBook_D because of the new method sendEBookEvent(String bookId, String status, String username)
		
			try { 
				messageProducer.send( payload );
				writeToCsv(payload, "");
			} finally { 
				messageProducer.close();
				session.close();
				connection.close();	
			}
		} catch (JMSException je) {
			LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
			System.out.println("je: "+je);
		} catch (ServiceLocatorException sle) {
			LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb:" + sle.getMessage() ); 
		}
	}
	
	
	public static void sendEBookEvent(String bookId, String status, String username) {
		try 
		{
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
            
			MessageProducer messageProducer = session.createProducer(destination);
			Message payload = session.createMapMessage();

			payload.setStringProperty("BOOKID", bookId);
			payload.setStringProperty("STATUS", status);
			payload.setStringProperty("USERNAME", username);
			payload.setStringProperty("TYPE", "EBook");

			try 
			{
				messageProducer.send(payload);
				writeToCsv(payload, "AUDIT_TRAIL_EBOOKS");
			} 
			finally 
			{
				messageProducer.close();
				session.close();
				connection.close();
			}
		} 
		catch (JMSException je) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage());
			System.out.println("je: " + je);
		} 
		catch (ServiceLocatorException sle) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb:" + sle.getMessage());
		}
	}
	
	
	public static void sendClientEBookEvent( EBook book )  {
		try {	

			Queue queue = EjbUtil.getQueue("queue/AuditTrail");
			QueueSession qs = EjbUtil.getQueueSession();
			QueueSender sender = qs.createSender(queue);
			
			Message payload = qs.createMapMessage();
			
			payload.setStringProperty("EISBN",		book.getIsbn() 		);
			payload.setStringProperty("SERIES_CODE",book.getSeriesCode());
			payload.setStringProperty("TITLE",		book.getTitle()		);
			payload.setStringProperty("STATUS", 	"7" 				);            
			payload.setStringProperty("USERNAME", 	"auto generated" 	);
			payload.setStringProperty("TYPE",		"EBook_D" 			);
			
			try { 
				sender.send(payload);
				writeToCsv(payload, "");
			} finally { 
				sender.close();
				qs.close();
			}
		} catch (Exception je) {
			System.out.println("EventWorker sendEBookEvent Error: "+je);
		} 
	}
	
	public static void sendClientBookContentEvent( EBookContent content)  {
        try {
        	
        	Queue queue = EjbUtil.getQueue("queue/AuditTrail");
			QueueSession qs = EjbUtil.getQueueSession();
			QueueSender sender = qs.createSender(queue);
			
			Message payload = qs.createMapMessage();
            
            payload.setStringProperty("CONTENT_ID",	content.getContentId() 	);
            payload.setStringProperty("EISBN",		content.getIsbn() 		);
            payload.setStringProperty("SERIES_CODE",content.getSeriesCode()	);
        	payload.setStringProperty("STATUS", 	content.getStatus() 	);            
            payload.setStringProperty("REMARKS",	content.getRemarks() 	);
            payload.setStringProperty("FILENAME", 	content.getFilename() 	);
            payload.setStringProperty("LOAD_TYPE", 	content.getLoadType() 	);
            payload.setStringProperty("USERNAME", 	"auto generated" 	);
            payload.setStringProperty("TYPE",		"Book" 		);
            
            try { 
				sender.send(payload);
				writeToCsv(payload, "AUDIT_TRAIL_BOOKS");
			} finally { 
				sender.close();
				qs.close();
			}
        } catch (Exception je) {
        	System.out.println("EventTracker sendBookEvent Error: "+je);
        } 
    }
	
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try {
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try {
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
	
	private static void writeToCsv(Message payload, String tableName) throws JMSException{
		File f = new File(AUDIT_REPOSITORY + tableName + "-" + dateTimeNow(0) + FILETYPE);
		try {
			Map<String, String> map = new HashMap<String, String>();
			checkTodaysLog(f,tableName);
		
			Enumeration<?> list = payload.getPropertyNames();
			System.out.println(list.toString());
			String fieldName = "";
			
			if(tableName.equalsIgnoreCase("AUDIT_TRAIL_EBOOKS")){
				org.cambridge.ebooks.production.jpa.common.EBook ebook = PersistenceUtil.searchEntity(
						new org.cambridge.ebooks.production.jpa.common.EBook(), 
						org.cambridge.ebooks.production.jpa.common.EBook.SEARCH_ALL_BY_BOOK_ID, 
						payload.getStringProperty("BOOKID"));
				
				map.put("EISBN", ebook.getIsbn());
				map.put("SERIES_CODE", ebook.getSeriesId());
				map.put("TITLE", ebook.getTitle());
			}
			
			while(list.hasMoreElements()){
				fieldName = list.nextElement().toString();
				System.out.println(fieldName + " = " + payload.getStringProperty(fieldName));
				map.put(fieldName, payload.getStringProperty(fieldName));
			}
			
			writeToCsv(map, tableName, f);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void writeToCsv(Map<String, String> map, String tableName, File f) throws IOException{
		StringBuffer sb = new StringBuffer();
		if (tableName.equalsIgnoreCase("USER_AUDIT_TRAIL_BOOKS")){
			sb.append(map.get("USER_ID")+COMMA);
			sb.append(map.get("ACTION")+COMMA);
			sb.append(dateTimeNow(2)+COMMA);
			sb.append(map.get("REMARKS")+NEW_LINE);
		} else if (tableName.equalsIgnoreCase("AUDIT_TRAIL_EBOOKS")){
			sb.append(map.get("EISBN")+COMMA);
			sb.append(map.get("STATUS")+COMMA);
			sb.append(map.get("SERIES_CODE")+COMMA);
			sb.append(map.get("TITLE")+COMMA);
			sb.append(dateTimeNow(1)+COMMA);
			sb.append(map.get("USERNAME")+NEW_LINE);
		} else if (tableName.equalsIgnoreCase("AUDIT_TRAIL_BOOKS")){
			sb.append(map.get("USER_ID")+COMMA);
			sb.append(map.get("STATUS")+COMMA);
			sb.append(dateTimeNow(1)+COMMA);
			sb.append(map.get("REMARKS")+COMMA);
			sb.append(map.get("CHAPTER_ID")+COMMA);
			sb.append(map.get("EISBN")+COMMA);
			sb.append(map.get("SERIES_CODE")+COMMA);
			sb.append(map.get("USERNAME")+COMMA);
			sb.append(map.get("FILENAME")+COMMA);
			sb.append(map.get("LOAD_TYPE")+COMMA);
			sb.append(map.get("CONTENT_ID")+NEW_LINE);
		}
		appendToFile(sb, f.getAbsolutePath());
	}
	
	private static void checkTodaysLog(File f, String tableName) throws IOException{
		boolean isNew = f.createNewFile();
		if(isNew){
			StringBuffer sb = new StringBuffer();
			if (tableName.equalsIgnoreCase("USER_AUDIT_TRAIL_BOOKS")){
				sb.append("USER_ID"+COMMA);
				sb.append("ACTION"+COMMA);
				sb.append("AUDIT_DATE"+COMMA);
				sb.append("REMARKS"+NEW_LINE);
			} else if (tableName.equalsIgnoreCase("AUDIT_TRAIL_EBOOKS")){
				sb.append("EISBN"+COMMA);
				sb.append("STATUS"+COMMA);
				sb.append("SERIES_CODE"+COMMA);
				sb.append("TITLE"+COMMA);
				sb.append("AUDIT_DATE"+COMMA);
				sb.append("USERNAME"+NEW_LINE);
			} else if (tableName.equalsIgnoreCase("AUDIT_TRAIL_BOOKS")){
				sb.append("USER_ID"+COMMA);
				sb.append("STATUS"+COMMA);
				sb.append("AUDIT_DATE"+COMMA);
				sb.append("REMARKS"+COMMA);
				sb.append("CHAPTER_ID"+COMMA);
				sb.append("EISBN"+COMMA);
				sb.append("SERIES_CODE"+COMMA);
				sb.append("USERNAME"+COMMA);
				sb.append("FILENAME"+COMMA);
				sb.append("LOAD_TYPE"+COMMA);
				sb.append("CONTENT_ID"+NEW_LINE);
			}
			appendToFile(sb, f.getAbsolutePath());
		}
	}
	
	public static void appendToFile(StringBuffer sb, String path) throws IOException {
		FileWriter fstream = new FileWriter(path, true);
		try {
			BufferedWriter out = new BufferedWriter(fstream);
			try {
				out.append(sb.toString());
			} finally {
				out.close();
			}
		} finally {
			fstream.close();
		}
	}
	
	private static String dateTimeNow(int flag){
		Calendar dateNow = Calendar.getInstance();
		String str = "";
		switch(flag){
		case 1: //DD-MMM-YY format
			str = StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0")+ 
				threeLetterMonth(dateNow.get(Calendar.MONTH)) +
				String.valueOf(dateNow.get(Calendar.YEAR)).substring(2);
			break;
		case 2: //DD-MMM-YY hh.mm.ss format
			str = StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0")+ 
				threeLetterMonth(dateNow.get(Calendar.MONTH)) +
				String.valueOf(dateNow.get(Calendar.YEAR)).substring(2) + " " +
				StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + "." +
				StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") + "." +
				StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0");
			break;
		default: //YYYYMMDD format
			str = String.valueOf(dateNow.get(Calendar.YEAR)) +
				StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)), 2, "0") +
				StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0");
			break;
		}
		return str;
	}
	
	private static String threeLetterMonth(int iMonth){
		String val = "";
		switch(iMonth){
			case 1: val = "-JAN-"; 	break;
			case 2: val = "-FEB-"; 	break;
			case 3: val = "-MAR-"; 	break;
			case 4: val = "-APR-"; 	break;
			case 5: val = "-MAY-"; 	break;
			case 6: val = "-JUN-"; 	break;
			case 7: val = "-JUL-"; 	break;
			case 8: val = "-AUG-"; 	break;
			case 9: val = "-SEP-"; 	break;
			case 10: val = "-OCT-";	break;
			case 11: val = "-NOV-";	break;
			case 12: val = "-DEC-";	break;
				
		}
		return val;
	}
}
