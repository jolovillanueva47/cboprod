package org.cambridge.ebooks.production.util;

import javax.el.ELResolver;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.publisher.SrPublisher;
import org.cambridge.ebooks.production.jpa.user.User;

/**
 * 
 * @author jgalang
 * 
 */

public class HttpUtil {
	
	public static HttpSession getHttpSession(boolean createNew) { 
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(createNew);
	}
	
	public static HttpServletRequest getHttpServletRequest() { 
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	public static HttpServletResponse getHttpServletResponse() { 
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}
	
	public static String getHttpServletRequestParameter(String param) { 
		if(StringUtil.isNotEmpty(param))
		{
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String result = request.getParameter(param);
			
			if(StringUtil.isNotEmpty(result))
				return result;
		}
		
		return null;
	}
	
	public static User getUserFromCurrentSession() {
		return getUserFromSession(getHttpSession(false));
	}
	
	public static User getUserFromSession(HttpSession session) {
		return (User) session.getAttribute(SessionAttribute.USER);
	}
	
	public static Publisher getPublisherFromCurrentSession() {
		return getPublisherFromSession(getHttpSession(false));
	}
	
	public static Publisher getPublisherFromSession(HttpSession session) {
		return (Publisher)session.getAttribute(SessionAttribute.PUBLISHER);
	}
	
	public static SrPublisher getSrPublisherFromCurrentSession() {
		return getSrPublisherFromSession(getHttpSession(false));
	}
	
	public static SrPublisher getSrPublisherFromSession(HttpSession session) {
		return (SrPublisher)session.getAttribute(SessionAttribute.SRPUBLISHER);
	}
	
	public static Object getAttributeFromCurrentSession(String attribute) {
		return getHttpSession(false).getAttribute(attribute);
	}
	
	/**
	 * get Object either from Session or request
	 * @param attribute
	 * @return
	 */
	public static Object getAttributeFromRequestOrSession(String attribute) {
		HttpServletRequest request = getHttpServletRequest();
		Object obj = request.getAttribute(attribute);
		
		if( obj != null ){
			return obj;
		}
		
		HttpSession session = getHttpSession(false);
		obj = session.getAttribute(attribute);
		
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getManagedBean(T beanName, String valueBinding) { 
		T result = null;
		
		if(StringUtil.isNotEmpty(valueBinding) && beanName != null)
		{
			FacesContext fcontext = FacesContext.getCurrentInstance();
			ELResolver resolver = fcontext.getApplication().getELResolver();
			result = (T)resolver.getValue(fcontext.getELContext(), null, valueBinding);
		}
		
		return result;
	}


}
