
package org.cambridge.ebooks.production.util;


public class EBooksProperties {
	
	public static final String INDEX = "index.dir";
	public static final String TEMPLATES_DIR = "templates.dir";
	public static final String EBOOKS_RESOURCE_URL = "ebooks.resource.url";
	public static final String UPO_RESOURCE_URL = "upo.resource.url";
	public static final String CCO_RESOURCE_URL = "cco.resource.url";
	public static final String CHO_RESOURCE_URL = "cho.resource.url";
	public static final String SSO_RESOURCE_URL = "sso.resource.url";
	public static final String CLR_RESOURCE_URL = "clr.resource.url";
	
	public static final String VALID_IPS_PATH = "valid.ips.path";
	public static final String SUBNET_IPS_PATH = "subnet.ips.path";
	
}
