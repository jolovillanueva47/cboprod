package org.cambridge.ebooks.production.util;


import java.util.Set;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;

import org.cambridge.common.ServiceLocatorException;

/**
 * @author
 *
 */
public class TrailWorker {
	
	public enum EventObject { 
		USER,
		ORGANISATION,
		IMAGES,
		CHAPTER,
		SUBSCRIPTION
	}
	
	/**
	 * Add here all the actions  
	 * that you want to be tracked.
	 */
	public enum Actions { 
		TIME_SPENT,
		POPULAR_SEARCH,
		LOGIN,
		SUCCESSFUL_SUBSCRIPTION_PER_COUNTRY,
		SUCCESSFUL_SUBSCRIPTION_PER_STATE,
		CANCELLED_SHOPPING_BASKET,
		FAILED_SUBSCRIPTION_PER_COUNTRY,
		POPULAR_IMAGE,
		SECTION_REQUEST;
	}
	
	public static void trackEvent( Actions action, EventObject object, String value, Set<String> orgBodyIdSet, HttpSession session ) { 
		
		if ( orgBodyIdSet != null ) { 
			for ( String orgBodyId : orgBodyIdSet ) { 
				trackEvent( action, object, value, orgBodyId, session );
			}
		}
	}
	
	public static void trackEvent( Actions action, EventObject object, String value, String bodyId, HttpSession session ) { 
//		if ( action == null || object == null || StringUtil.isEmpty(value)  ) {
//			LogManager.error(TrailWorker.class, "One of the parameters is empty..."  );
//		} else { 
			
			sendEvent( action, object, value, bodyId, session.getId() );
				
//		}
	}
		
	private static void sendEvent( Actions action, EventObject object, String value, String bodyId, String sessionId )  {
        try {
        	
            ConnectionFactory connectionFactory = getJmsConnectionFactory( "ConnectionFactory" );

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination( "queue/stahl/logger" );
            
            MessageProducer messageProducer = session.createProducer(destination);
             
            Message payload = session.createMapMessage();
            payload.setStringProperty("ACTION", action.toString() );
            payload.setStringProperty("LABEL", 	object.toString() );
            payload.setStringProperty("VALUE", 	value );
            payload.setStringProperty("BODYID", bodyId );
            payload.setStringProperty("SESSIONID", sessionId );
            
            try { 
	            messageProducer.send( payload );
	            LogManager.info( TrailWorker.class,  "Message sent to MDB.." );
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
        } catch (JMSException je) {
        	LogManager.error( TrailWorker.class, " Message not sent to counter-ejb: " + je.getMessage() );
        } catch (ServiceLocatorException sle) {
        	LogManager.error( TrailWorker.class, " Message not sent to counter-ejb:" + sle.getMessage() ); 
        }
    }

	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try {
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try {
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
}
