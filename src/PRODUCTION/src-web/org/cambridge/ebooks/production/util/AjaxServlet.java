package org.cambridge.ebooks.production.util;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

public class AjaxServlet extends HttpServlet {
	
	public static final String CLASS_NAME = "CLASS_NAME";
	
	public static final String VAR_NAME = "VAR_NAME";
	
	public static final String CONTEXT = "CONTEXT";
	
	public static final String SESSION = "SESSION";
	
	public static final String METHOD= "METHOD";
	
	public static final String REQUEST = "REQUEST";
	
	private static final String COMMA = ",";
	
	public static final String GET = "GET";
	
	public static final String SET = "SET";
	
	public static final String RESP_CONTENT = "text/plaint";
	
	 

	/**
	 * 
	 */
	private static final long serialVersionUID = -1345170456309677790L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {	
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {		
		try {
			recreateObjectInContext(req);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		System.out.println("AJAX");
		resp.setContentType(RESP_CONTENT);
		Writer writer = resp.getWriter();
		writer.write("Success");		
	}
	
	private void recreateObjectInContext(HttpServletRequest req) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, InvocationTargetException, NoSuchMethodException{
		
		String className = req.getParameter(CLASS_NAME);
		String varName = req.getParameter(VAR_NAME);
		String context = req.getParameter(CONTEXT);
		
		Object object;
 		
 		req.getSession().getId();
		//get Object
		if(REQUEST.equalsIgnoreCase(context)){
			object = req.getAttribute(varName);			
		}else{
			object = req.getSession().getAttribute(varName);
		}
		
		//create object if null		
		if(object == null){
			object = Class.forName(className).newInstance();
			if(REQUEST.equalsIgnoreCase(context)){
				req.setAttribute(varName, object);
			}else{
				req.getSession().setAttribute(varName, object);
			}
		}
		
		//execute the method
		//format get(set),methodName,value
		String method = req.getParameter(METHOD);
		String[] methodArr = method.split(COMMA);
		if(GET.equalsIgnoreCase(methodArr[0])){
			BeanUtils.getProperty(object, methodArr[1]);
		}else{			
			BeanUtils.setProperty(object, methodArr[1], req);
		}
		
		
	}

}
