package org.cambridge.ebooks.production.util;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.jpa.user.User;

public class Log4JLogger {
	private Logger LOGGER;
	private User user;
	
	public Log4JLogger(Class<?>name) {
		this.LOGGER = Logger.getLogger(name);
	}
	
	public void info(Class<?> name, String message){
		processMessage(message);
	}
	
	public void info(String message){
		processMessage(message);
	}
	
	public void error(Class<?> name, String message){
		processMessage(message);
	}
	
	public void error(String message){
		processMessage(message);
	}
	
	public void processMessage(String message){
		refreshUser();
		if(this.user == null || this.user.equals(null)){
			printNoUser(message);
		} else {
			printWithUser(message);
		}
	}
	
	public void printWithUser(String message){
		LOGGER.error("[" + user.getUsername() +"]: "+message);
	}
	
	public void printNoUser(String message){
		LOGGER.error(message);
	}
	
	public void refreshUser(){
		try{
			this.user = (User) (HttpUtil.getHttpSession(true).getAttribute(SessionAttribute.USER));
		} catch (NullPointerException ex){
			LOGGER.error("no user found in session");
			this.user = null;
		}
	}

}
