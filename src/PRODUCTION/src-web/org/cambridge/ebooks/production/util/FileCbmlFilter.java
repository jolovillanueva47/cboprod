package org.cambridge.ebooks.production.util;

import java.io.File;
import java.io.FilenameFilter;

public class FileCbmlFilter implements FilenameFilter {
	String filename;
	public FileCbmlFilter(String extension){
		this.filename = extension;
	}
	
	public boolean accept(File dir, String name){
		return name.contains(filename);
	}
}
