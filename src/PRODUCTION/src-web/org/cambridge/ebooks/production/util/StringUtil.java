package org.cambridge.ebooks.production.util;

import java.util.List;


/**
 * 
 * @author cmcastro
 *
 */

public class StringUtil {

	private static  final String REAL_NUMBER = "^[-+]?\\d+(\\.\\d+)?$";

	public static boolean isNumeric(String val) {
	   return isEmpty( val ) ? false : val.matches(REAL_NUMBER);
	}

	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
	
	public static String nvl(String value, String replacement) { 
		return isEmpty(value) ? replacement : value;
	}
	
	public static String nvl(String value ) { 
		return isEmpty(value) ? "" : value;
	}
	
	public static <T> String nvl(T value) { 
		return ((T) (isEmpty(value) ? "" : value)).toString();
	}
	
	public static <T> String nvl(T value, T replacement) { 
		return ((T) (isEmpty(value) ? replacement : value)).toString();
	}
	
	public static boolean validateLength(Object obj, int minLength, int maxLength) { 
		boolean result = true;
		if ( obj  instanceof String ) { 
			String value = ( String ) obj;
			if ( isNotEmpty(value) ) { 
				result = value.length() >= minLength && value.length() <= maxLength;
			}
		}
		return result;
	}	
	
	public static String addPadding(final String value, final String padding ) {
		boolean ifValueIsLongerThanThePadding = nvl(value).length() > nvl(padding).length();
		if ( ifValueIsLongerThanThePadding ) { 
			return value;
		}
		String result = nvl(padding) + nvl(value);
		return result.substring( 0 + nvl(value).length(), result.length() );
	}
	
	public static String  join(String [] array, char separator) {
		if (array == null) {
            return null;
        }
        int arraySize = array.length;
        StringBuilder  result = new StringBuilder ();
        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                result.append(separator);
            }
            if (array[i] != null) {
                result.append(array[i]);
            }
        }
        return result.toString();
    }
	
	public static String[] split(String value){
		if(isEmpty(value)) return null;
		value = value.replace("[", "").replace("]", "");
		return value.split(",");
	}
	
	/**
	 * Strip html tags
	 * @param text String source text
	 * @return String
	 */
	public static String stripTags(String text) {
		if(text == null || text.equals("")){
			return text;
		}
		
		return text.replaceAll("<i>", "").
		replaceAll("</i>", "").
		replaceAll("<b>", "").
		replaceAll("</b>", "").
		replaceAll("<blockquote>", "").
		replaceAll("</blockquote>", "").
		replaceAll("<u>", "").
		replaceAll("</u>", "").
		replaceAll("<span style='font-variant:small-caps'>", "").
		replaceAll("</span>", "").
		replaceAll("<sup>", "").
		replaceAll("</sup>", "").
		replaceAll("<sub>", "").
		replaceAll("</sub>", "");
	}
	
	/**
	 * Correct tags relevant to html tags
	 * @param text String source text
	 * @return String
	 */
	public static String correctHtmlTags(String text) {		
		String output = "";
		try {
			output = text.replaceAll("<italic>", "<i>").
			replaceAll("</italic>", "</i>").
			replaceAll("<block>", "<blockquote id=\"indent\">").
			replaceAll("</block>", "</blockquote>").
			replaceAll("<underline>", "<u>").
			replaceAll("</underline>", "</u>").
			replaceAll("<bold>", "<b>").
			replaceAll("</bold>", "</b>").
			replaceAll("<list-item>", "<li>").
			replaceAll("</list-item>", "</li>").
			replaceAll("<list", "<ul").
			replaceAll("style=\"", "style=\"list-style:").
			replaceAll("style=\"list-style:bullet", "style=\"padding: 20px 20px 20px 30px; list-style:disc !important;").
			replaceAll("style=\"list-style:number", "style=\"padding: 20px 20px 20px 30px; list-style:decimal !important;").
			replaceAll("style=\"list-style:none", "style=\"padding: 20px 20px 20px 30px; list-style:none !important;").
			replaceAll("</list>", "</ul>").
			replaceAll("<small-caps>", "<span style='font-variant:small-caps'>").
			replaceAll("</small-caps>", "</span>").
			replaceAll("<source>", "<p align=\"right\">").
			replaceAll("</source>", "</p>");
		} catch (Exception e) {
			output = "";
		}
		
		return output;
	}
	
	/**
	 * Convert html unicode to readable text
	 * @param text String
	 * @return String
	 * @throws Exception
	 */
	public static String parseHtmlUnicodeString(String text) throws Exception {
		String result = text;
		boolean isUnicodeStart = false;
		int unicodeStartIndx = 0;
		int unicodeEndIndx = 0;
		for(int i = 0 ; i < text.length() - 1; i++){
			char char1 = text.charAt(i);
			char char2 = text.charAt(i + 1);
			
			if(char1 == '&' && char2 == '#') {
				isUnicodeStart = true;
				unicodeStartIndx = i;				
			}
			
			if(isUnicodeStart && char2 == ';') {
				isUnicodeStart = false;
				unicodeEndIndx = i + 1;
				
				int unicode = 
					Integer.parseInt(text.substring(unicodeStartIndx + 2, unicodeEndIndx));
				
				result = result.replaceAll("&#" + unicode + ";", String.valueOf((char)unicode));
			}
		}
		
		return result;
	}
	
	public static String stripHTMLTags(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("(?i)(<[\\s]*[^>]*(" +
				"a|abbr|acronym|address|applet|area|b|base|basefont|bdo|big|blockquote|body|" +
				"br|button|caption|center|cite|code|col|colgroup|dd|del|dfn|" +
				"dir|div|dl|dt|em|fieldset|font|form|frame|frameset|" +
				"h1|h2|h3|h4|h5|h6|head|html|i|iframe|img|input|ins|isindex|kbd|label|" +
				"legend|li|link|map|menu|meta|noframes|noscript|object|ol|optgroup|option|" +
				"p|param|pre|q|s|samp|script|select|small|span|strike|strong|style|sub|sup|" +
				"table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var|xmp)[^>]*>)", "");
	}
	
	public static String stripUnicodes(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("&#[^;]+;", " ");
	}
	
	public static String formatTitle(String title){
		return title.replace("&amp;", "&")
				.replace("&lt;", "<")
				.replace("&gt;", ">")
				.replace("&#60;", "<")
				.replace("&#62;", ">")
				.replace("&apos;", "&#x27;");
	}
	
	public static String formatName(String name) {
		String result = "";
		String[] names = name.split(",");
		if(names.length > 1) {
			result = names[1].trim() + " " + names[0].trim();
		} else {
			result = name;
		}
		
		return result;
	}
	
	public static String getContributorDisplay(List<String> contributors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		boolean isEtAl = false;
		if(contributors != null && contributors.size() > 0) {
			if (contributors.size() > 4 && !isEtAl) {
				result.append(formatName(contributors.get(0)));
				result.append(" et al.");
				isEtAl = true;
			}
			if (!isEtAl) {
				for(String contributor : contributors) {
					result.append(formatName(contributor));
					result.append(", ");
				}
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ',') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	public static String getContributorsAlphasort(List<String> contributors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		if(contributors != null && contributors.size() > 0) {			
			for(String contributor : contributors) {
				result.append(formatName(contributor));
				result.append(";");
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ';') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	public static String getContributors(List<String> contributors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		if(contributors != null && contributors.size() > 0) {			
			for(String contributor : contributors) {
				result.append(contributor);
				result.append(";");
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ';') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	
	public static String join(List<String> list, String encapsulate, String delimiter){
		encapsulate = encapsulate == null ? "" : encapsulate;
		delimiter = delimiter == null ? "," : delimiter;
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<list.size(); i++){
			if(i < list.size()-1){
				sb.append(encapsulate + list.get(i) + encapsulate + delimiter);
			}else{
				sb.append(encapsulate + list.get(i) + encapsulate);
			}
		}
		
		return sb.toString();
	}
	
	
	public static String mdashUnicodeToHtml(String input) {
		return input.replace("\u2014", "&mdash;");
	}
	
}
