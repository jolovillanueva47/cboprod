package org.cambridge.ebooks.production.util;

import java.io.File;
import java.io.FilenameFilter;

public class FileFilter implements FilenameFilter {
	String extension;
	public FileFilter(String extension){
		this.extension = extension;
	}
	
	public boolean accept(File dir, String name){
		return name.endsWith(extension);
	}
}
