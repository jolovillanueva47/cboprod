package org.cambridge.ebooks.production.util;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class AutoDeleteLock {

	public static String lockFilePath = "/app/ebooks/lockfile";
	public static String manifestFilePath = "/app/ebooks/content/MANIFEST.txt";
	public static String writeLockFilePath = "/app/ebooks/bookindex/write.lock";
	//public static String lockFilePath = "c:/dru/dump/testing.txt";
	
	/**
	 * Deletes lock file if lock file timestamp is 4 hours
	 * less than current timestamp
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		File file = new File(writeLockFilePath);
		
		if(file.exists()){
			Date writeLockFile = new Date(file.lastModified());
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, -4);
			Date earlyHour = cal.getTime();
			
			if(writeLockFile.compareTo(earlyHour) <= 0 ){
				System.out.println("Write Lock file("+writeLockFile+")\n " +
						"is lesser than Early Hour("+earlyHour+")");
				executeCommand("rm "+lockFilePath);
				executeCommand("rm "+manifestFilePath);
				executeCommand("rm "+writeLockFilePath);
			}
		}
	}

	/**
	 * Executes a Unix command
	 * 
	 * @param command - Unix command
	 */
	public static void executeCommand(String command)
	{
		Process p;
		try{
			p=Runtime.getRuntime().exec(command);

			int i=p.waitFor();
			if(i==0)
				System.out.println("Command executed successfully: "+command);
			else
				System.out.println("Failed to execute command: "+command);
		}catch(IOException ioe){
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
