package org.cambridge.ebooks.production.util;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * 
 * @author jgalang
 * @param <T>
 * TODO: -C2- Needs refinement/validation/throw exception for 0, undefined, etc... results.
 * 
 */

public class PersistencePager<T> {
	
	public static enum QueryType {
		NAMED,NATIVE;
	}

	private long maxResults;
	private int maxPages;
	private int currentPage;
	private int pageSize;
	private String reportQueryString;
	private String resultSetMapping;
	private String reportQueryName;
	private String[] params;
	
	private QueryType queryType;

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );
	
	private PersistencePager(QueryType queryType, int pageSize, String count, String report, String... params) {
		this.queryType = queryType;
		this.pageSize = pageSize;
		this.params = params;
		if (queryType.equals(QueryType.NAMED)) {
			this.reportQueryName = report;
		} else if (queryType.equals(QueryType.NATIVE)) {
			this.reportQueryString = report;
		}
		
		EntityManager em = emf.createEntityManager();
		Query query = null;
		try {
			query = queryType.equals(QueryType.NAMED) ? em.createNamedQuery(count) : em.createNativeQuery(count);
	        if ( params != null && params.length > 0 ) { 
	        	int ctr = 1;
		        for ( String param : params ) { 
		        	query.setParameter( ctr++, param); 
		        }
	        }
	
	        BigDecimal v  = (BigDecimal)query.getSingleResult();
			
			currentPage = 0;
			maxResults = v.longValue();
			resolveMaxPages();
		}
		finally {
			JPAUtil.close(em);
		}

	}
	public PersistencePager(int pageSize, String reportQueryString, String resultSetMapping, String... params) {  
		this(QueryType.NATIVE, pageSize, getCountQueryString(reportQueryString), reportQueryString, params);
		this.resultSetMapping = resultSetMapping;
	}
	public PersistencePager(String countQueryName, int pageSize, String reportQueryName, String... params) {
		this(QueryType.NAMED, pageSize, countQueryName, reportQueryName, params);
	}
	
	private static String getCountQueryString(String reportQueryString) {
		return "SELECT COUNT(*) FROM (" + reportQueryString + ")";
	}
	
	public ArrayList<T> getCurrentResults(String letterFilter) {
		for (int x=0; x < params.length; x++){
			if(params[x].startsWith("^")){
				params[x] = "^" + letterFilter;
			}
		}
		return getCurrentResults();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> getCurrentResults() {
		EntityManager em = emf.createEntityManager();
		Query query  = null;
		try {
			query = queryType.equals(QueryType.NAMED) ? em.createNamedQuery( reportQueryName ) :
				em.createNativeQuery( reportQueryString, resultSetMapping );
	        if ( params != null && params.length > 0 ) { 
	        	int ctr = 1;
		        for ( String param : params ) { 
		        	query.setParameter( ctr++, param); 
		        }
	        }
			
			return (ArrayList<T>) query.setFirstResult(currentPage * pageSize).setMaxResults(pageSize).getResultList();
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	private void resolveMaxPages() {
		maxPages = (int) ((maxResults%pageSize>0) ? (maxResults/pageSize)+1 : maxResults/pageSize);
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
		currentPage = 0;
		resolveMaxPages();
	}
	
	public void next() {
		currentPage++;
		if (currentPage == maxPages) {
			currentPage = maxPages-1;
		}
	}
	
	public void previous() {
		currentPage--;
		if (currentPage < 0) {
			currentPage = 0;
		}
	}
	
	public void first() {
		currentPage = 0;
	}
	
	public void last() {
		currentPage = maxPages-1;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getMaxResults() {
		return maxResults;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getMaxPages() {
		return maxPages;
	}
	
}
