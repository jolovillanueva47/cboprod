package org.cambridge.ebooks.production.util;

import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * 
 * @author cmcastro
 * @author C2
 * @author rvillamor
 * 
 */

public class PersistenceUtil {
	
	public enum PersistentUnits {
		EBOOKS("eBooksService");
		
		private PersistentUnits(String name ) { 
			this.name = name;
		}
		
		private String name;
		
		public String toString() { 
			return name;
		}
	}

	private static PersistentUnits BookService = PersistentUnits.EBOOKS;
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static <T> void updateEntity(Object obj) {
		EntityManager em = emf.createEntityManager();
		em.merge((T) obj);
		try {
			em.getTransaction().commit();
		} catch (Exception e) {
			LogManager.error(PersistenceUtil.class, "Error in updating entity. " + e.getMessage());
		} finally {
			JPAUtil.close(em);
		}
	}
	
	public static void updateEntity(String namedQuery, String... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		Query query = null;
		try {
			em.getTransaction().begin();
			query = em.createNamedQuery( namedQuery );
			
			if ( params != null && params.length > 0 ) { 
				int ctr = 1;
			    for ( String param : params ) { 
			    	query.setParameter( ctr++, param); 
			    }
			}
			
			query.executeUpdate();
			em.getTransaction().commit();
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public static void updateEntity(String namedQuery, Object... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		Query query = null;
		try {
			em.getTransaction().begin();
			
			query = em.createNamedQuery( namedQuery );
			if ( params != null && params.length > 0 ) { 
				int ctr = 1;
			    for ( Object param : params ) { 
			    	query.setParameter( ctr++, param); 
			    }
			}
			
			query.executeUpdate();
			em.getTransaction().commit();
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public static <T> ArrayList<T> searchList( T t, String namedQuery, String... params ) { 
		ArrayList<T> result = new ArrayList<T>();
		
		EntityManager em = emf.createEntityManager();
		Query query = null;
        try { 
	        query = em.createNamedQuery( namedQuery );
	        if ( params != null && params.length > 0 ) { 
	        	int ctr = 1;
		        for ( String param : params ) { 
		        	query.setParameter( ctr++, param); 
		        }
	        }
        
        	result = (ArrayList<T>) query.getResultList();
        } 
        catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + BookService.toString() + " named query is: " + namedQuery );
        } 
        finally {
        	JPAUtil.close(em);
        }
		return  result;
	}
	
	
	public static <T> T searchEntity(T t, String namedQuery, String... params ) { 
		T result = t;
		
		EntityManager em = emf.createEntityManager();
		Query query = null;
        try { 
        	query = em.createNamedQuery( namedQuery );
	        if ( params != null && params.length > 0 ) { 
	        	int ctr = 1;
		        for ( String param : params ) { 
		        	query.setParameter( ctr++, param); 
		        }
	        }
        
        	result = (T) query.getSingleResult();
        }
        catch (NoResultException nre ) { 
        	result = null;
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + BookService.toString() + " named query is: " + namedQuery );
        }
        finally {
        	JPAUtil.close(em);
        }
		return  result;
	}
	
	
	public static <T> void updateEntities( T t, String namedQuery, String... params ) { 
		
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
        Query query = em.createNamedQuery( namedQuery );
        
        
        if ( params != null && params.length > 0 ) { 
        	int ctr = 1;
	        for ( String param : params ) { 
	        	query.setParameter( ctr++, param); 
	        }
        }
        
        try { 
        	query.executeUpdate();
        	em.getTransaction().commit();
        } catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, "Update failed.. " + " named query is: " + namedQuery );
        } finally {
        	JPAUtil.close(em);
        }
		
	}
	
	public static <T> ArrayList<T> dynamicSearchList( T t, String queryStr, String resultSetMapping, String... params ) { 
		
		return  dynamicSearchList(t, queryStr, resultSetMapping, new ArrayList<String>(Arrays.asList(params)));
	}
	
	public static <T> ArrayList<T> dynamicSearchList( T t, String queryStr, String resultSetMapping, ArrayList<String> params ) { 
		ArrayList<T> result = new ArrayList<T>();
		EntityManager em = emf.createEntityManager(); 
		Query query = null;
        try {
        	query = em.createNativeQuery( queryStr, resultSetMapping );		
        	if ( params != null && params.size() > 0 ) { 
        		int ctr = 1;
        		for(String param : params){
        			query.setParameter(ctr++, param);
        		}
        	}
		
        	result = (ArrayList<T>) query.getResultList();
        } 
        catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + BookService.toString() + " dynamic query is: " + queryStr );
        } 
        catch(Exception ex){
        	if(ex.getMessage().contains("ORA-01839"))
        		LogManager.error(PersistenceUtil.class, " Selected invalid maximum day of month! " + queryStr );
        	else
        		LogManager.error(PersistenceUtil.class, queryStr  + " message: " + ex.getMessage());
        } 
        finally {
        	JPAUtil.close(em);
        }
        
    	return result;
	}
	
}
