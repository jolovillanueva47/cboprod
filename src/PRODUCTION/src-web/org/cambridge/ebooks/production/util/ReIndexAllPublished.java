package org.cambridge.ebooks.production.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.cambridge.ebooks.production.embargo.bean.EBook;

public class ReIndexAllPublished {

	public static void main(String[] args) {
		Connection con = DataSource.getConnection();
		try 
		{
			if(con != null)
			{
				Statement stmt = con.createStatement();
				
				// Get EBooks where status equals publish
				ResultSet rs = stmt.executeQuery("select isbn, book_id from ebook WHERE status = 7");
				EBook ebook;
				int ctr = 1;
			    while (rs.next()){
			    	ebook = getEBook(rs);
			    	System.out.println("reindex: "+ebook.getBookId());
			    	//SearchIndexWorker.sendClientMessage(ebook.getBookId(), "publish");
			    	ctr++;
//			    	if(ctr == 2)
//			    		break;
			    }
			    System.out.println("Number of books published: "+ctr);
			}
		}
		catch(SQLException sqle)
		{
			try {
				con.rollback();
			} catch (SQLException e) {
				System.out.println(ReIndexAllPublished.class + " [SQLException] " + "error in rollback.");
			}
		}
		catch (Exception e) 
		{
			System.out.println(ReIndexAllPublished.class + " [Exception] " + e.getMessage());
		}
		finally
		{
			DataSource.closeConnection(con);
		}
	}
	
	public static EBook getEBook(ResultSet rs){
		EBook ebook = new EBook();
		try {
			ebook.setIsbn(rs.getString("ISBN"));
			ebook.setBookId(rs.getString("BOOK_ID"));
		} catch (SQLException e) {
			System.out.println(ReIndexAllPublished.class + " [SQLException] " + e.getMessage());
		}
		return ebook;
	}
}
