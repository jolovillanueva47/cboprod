package org.cambridge.ebooks.production.util;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.common.ServiceLocatorException;

/**
 * @author andreau
 *
 */
public class SearchIndexWorker {

	public static final String UNPUBLISH = "UNPUBLISH";
	public static final String PUBLISH = "PUBLISH";
	
	public static void sendMessage( String bookId, String type)  {
        try {
        	
            ConnectionFactory connectionFactory = getJmsConnectionFactory( "ConnectionFactory" );

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination( "queue/search/logger" );            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
        	payload.setStringProperty("BOOK_ID", bookId);
        	payload.setStringProperty("TYPE", type);
        	
            try { 
	            messageProducer.send( payload );	            
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
        } catch (JMSException je) {
        	LogManager.error( SearchIndexWorker.class, " Message not sent to search-ejb: " + je.getMessage() );
        } catch (ServiceLocatorException sle) {
        	LogManager.error( SearchIndexWorker.class, " Message not sent to search-ejb:" + sle.getMessage() ); 
        }
    }
	
	public static void sendClientMessage( String bookId, String type)  {
        try {
        	
        	Queue queue = EjbUtil.getQueue("queue/search/logger");
			QueueSession qs = EjbUtil.getQueueSession();
			QueueSender sender = qs.createSender(queue);
			
			Message payload = qs.createMapMessage();
            
        	payload.setStringProperty("BOOK_ID", bookId);
        	payload.setStringProperty("TYPE", type);
        	
        	try {
				sender.send(payload);
			} finally { 
				sender.close();
				qs.close();
			}
        } catch (Exception e) {
        	System.out.println("SearchIndexWorker Error: "+e);
        } 
    }
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try {
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try {
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
}
