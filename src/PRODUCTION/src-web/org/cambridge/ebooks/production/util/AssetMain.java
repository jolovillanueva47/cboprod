package org.cambridge.ebooks.production.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AssetMain {
	
	public static void deliverAssetFromDb() {
		Connection con = DataSource.getConnection();
		try 
		{
			if(con != null)
			{
				Statement stmt = con.createStatement();
				
				// Get EBooks where status equals publish
				ResultSet rs = stmt.executeQuery("select isbn from ebook WHERE status = 7");
				
			    while (rs.next()){
			    	AssetWorker.sendClientAssetEvent(rs.getString("ISBN"));
			    }
			}
		}
		catch(SQLException sqle)
		{
			try {
				con.rollback();
			} catch (SQLException e) {
				System.out.println(ReIndexAllPublished.class + " [SQLException] " + "error in rollback.");
			}
		}
		catch (Exception e) 
		{
			System.out.println(ReIndexAllPublished.class + " [Exception] " + e.getMessage());
		}
		finally
		{
			DataSource.closeConnection(con);
		}
	}
	
	public static void deliverAssetFromFile() {
		File file = new File("/app/jboss-4.2.3.GA/server/default/asset/temp/isbn_list.txt");
		//File file = new File("C:/dru/task/CBO Production/MANTIS/Asset Store/Empty Folders/rerun asset store/isbn_list.txt");
		
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			while((line = br.readLine()) != null){
				AssetWorker.sendClientAssetEvent(line);
				//System.out.println(line);
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void findRemainingIsbns() {
		//File file = new File("/app/jboss-4.2.3.GA/server/default/asset/temp/isbn_list.txt");
		File file = new File("C:/dru/task/CBO Production/MANTIS/Asset Store/Empty Folders/rerun asset store/published_in_folder.txt");
		
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			StringBuffer isbnInfolder = new StringBuffer();	
			String line;
			while((line = br.readLine()) != null){
				isbnInfolder.append(line+" ");
			}
			//System.out.println("isbnInfolder: "+isbnInfolder);
			File file2 = new File("C:/dru/task/CBO Production/MANTIS/Asset Store/Empty Folders/rerun asset store/published_in_db.txt");
			
			fr = new FileReader(file2);
			br = new BufferedReader(fr);
			
			String isbn;
			while((isbn = br.readLine()) != null){
				//System.out.println(isbn);
				if(!isbnInfolder.toString().contains(isbn)){
					//AssetWorker.sendClientAssetEvent(isbn);
					System.out.println("Asset: "+isbn);
				}
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//deliverAssetFromDb();
		deliverAssetFromFile();
	}
}
