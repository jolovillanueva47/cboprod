///**
// * 
// */
//package org.cambridge.ebooks.production.util;
//
///**
// * @author rvillamor
// *
// */
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.WhitespaceAnalyzer;
//import org.apache.lucene.document.Document;
//import org.apache.lucene.queryParser.ParseException;
//import org.apache.lucene.queryParser.QueryParser;
//import org.apache.lucene.search.HitCollector;
//import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.search.Query;
//import org.apache.lucene.search.ScoreDoc;
//import org.apache.lucene.store.FSDirectory;
//
//public class IndexSearchUtil {
//	
//	public static final String INDEX_DIR = EBooksConfiguration.getProperty(EBooksProperties.INDEX);
//	
//	
//	public static Document searchIndex(final String indexDir, final String querystr, int docPage) {
//		
//		int maxResult = 1;
//		Document doc = null;
//		
//		try 
//		{
//			Analyzer analyzer = new WhitespaceAnalyzer();
//			FSDirectory index = FSDirectory.getDirectory(indexDir);
//			IndexSearcher s = new IndexSearcher(index);
//			Query q = new QueryParser("BOOK_ID", analyzer).parse(querystr);
//			
//			HitPageCollector hpc = new HitPageCollector(docPage,maxResult); 
//			s.search(q, hpc); 
//			ScoreDoc[] sd = hpc.getScores(); 
//			
//			doc = s.doc(sd[0].doc); 
//
//			s.close();
//		}
//
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			LogManager.info(IndexSearchUtil.class," [IOException]: searchIndex() "	+ e.getMessage());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			LogManager.error(IndexSearchUtil.class," [ParseException]: searchIndex() "	+ e.getMessage());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			LogManager.error(IndexSearchUtil.class," [Exception]: searchIndex() "	+ e.getMessage());
//		}
//
//		return doc;
//
//	}
//	
//	
//	public static List<Document> searchIndex(final String indexDir, final String querystr) {
//		List<Document> result = new ArrayList<Document>();
//
//		try {
//			Analyzer analyzer = new WhitespaceAnalyzer();
//			FSDirectory index = FSDirectory.getDirectory(indexDir);
//			Query q = new QueryParser("BOOK_ID", analyzer).parse(querystr);
//
//			IndexSearcher searcher = new IndexSearcher(index);
//			AllDocCollector collector = new IndexSearchUtil.AllDocCollector();
//			searcher.search(q, collector);
//			List<ScoreDoc> hits = collector.getHits();
//
//			for (ScoreDoc score : hits) {
//				result.add(searcher.doc(score.doc));
//			}
//
//			searcher.close();
//		}
//
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			LogManager.info(IndexSearchUtil.class," [IOException]: searchIndex() "	+ e.getMessage());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			LogManager.error(IndexSearchUtil.class," [ParseException]: searchIndex() "	+ e.getMessage());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			LogManager.error(IndexSearchUtil.class," [Exception]: searchIndex() "	+ e.getMessage());
//		}
//
//		return result;
//
//	}
//	
//
//	public static List<Document> searchIndex(final String querystr) {
//		List<Document> result = new ArrayList<Document>();
//
//		try {
//			Analyzer analyzer = new WhitespaceAnalyzer();
//			FSDirectory index = FSDirectory.getDirectory(INDEX_DIR);
//			Query q = new QueryParser("BOOK_ID", analyzer).parse(querystr);
//
//			IndexSearcher searcher = new IndexSearcher(index);
//			AllDocCollector collector = new IndexSearchUtil.AllDocCollector();
//			searcher.search(q, collector);
//			List<ScoreDoc> hits = collector.getHits();
//
//			for (ScoreDoc score : hits) {
//				result.add(searcher.doc(score.doc));
//			}
//
//			searcher.close();
//		}
//
//		catch (IOException e) {
//			System.err.println(" [IOException]: searchIndex() "	+ e.getMessage());
//		} catch (ParseException e) {
//			System.err.println(" [ParseException]: searchIndex() "	+ e.getMessage());
//		} catch (Exception e) {
//			System.err.println(" [Exception]: searchIndex() "	+ e.getMessage());
//		}
//
//		return result;
//
//	}
//	
//	private static class  AllDocCollector extends HitCollector {
//		List<ScoreDoc> docs = new ArrayList<ScoreDoc>();
//
//		public void collect(int doc, float score) {
//			if (score > 0.0f) {
//				docs.add(new ScoreDoc(doc, score));
//			}
//		}
//
//		public void reset() {
//			docs.clear();
//		}
//
//		public List<ScoreDoc> getHits() {
//			return docs;
//		}
//	}
//	
//	
//	
//	
//	
//	public static void main(String[] arg){
//		  //String search = "BOOK_ID:CBO9780511470936";
//		  //String search = "ELEMENT:content-item";
//		  //List<Document> docs = searchIndex("D:/cbo/bookindex", search);
//		 // Document result = searchIndex("D:/cbo/bookindex", search, 1);
//		  //System.out.println("CONTENT-ID: " + result.get("ID") + " TITLE: " + result.get("TITLE"));
//		  //System.out.println("hits: " + searchIndex("D:/cbo/bookindex", search).size());
//	}
//}
//
//
//

