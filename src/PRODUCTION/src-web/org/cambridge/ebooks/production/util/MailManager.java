package org.cambridge.ebooks.production.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * @author jgalang
 * 
 */

public class MailManager {
	
	public class EmailUtilException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public EmailUtilException(String message) {
			super(message);
		}
	}
	
	private static final String EMAIL_PATTERN_REGEX = ".+@.+\\.[a-z]+"; 
	private static final String NO_FROM = "Sender or \"from\" value must not be null or empty.";
	private static final String NO_TO = "Recipient/\"to\" value must not be null or empty.";
	private static final String NO_SUBJECT = "Subject or \"subject\" must not be null or empty";
	private static final String NO_BODY_TEMPLATE = "Body file must not be null.";
	private static final String NO_OUTPUT_DIR = "Output directory must not be null.";
	private static final String OUTPUT_DIR_NOT_FOUND = "Output folder does not exist: ";
	private static final String INVALID_EMAIL_ADDRESS = "Invalid email address. ";
	
	private String from;
	private String[] to;
	private String[] cc;
	private String[] bcc;
	private String subject;
	private String attachment;
	private Object body;
	private String templateDir;
	private String templateFile;
	private String outputDir;
	private File outputDirFile;
	
	private MailManager(
			String from, 
			String[] to, 
			String[] cc, 
			String[] bcc, 
			String subject,
			String attachment,
			Object body,
			String templateDir,
			String templateFile,
			String outputDir) {
		this.from = from.trim();
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject.trim();
		this.attachment = attachment;
		this.body = body;
		this.templateDir = templateDir;
		this.templateFile = templateFile;
		this.outputDir = outputDir;
		
		outputDirFile = new File(outputDir);
	}
	
	public void buildEmail(ServletContext servletContext) throws EmailUtilException {
		try {
			validateDetails();
			
			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			
			if(servletContext == null) {
				cfg.setServletContextForTemplateLoading(
						HttpUtil.getHttpSession(false).getServletContext(), templateDir);
			} else {
				cfg.setServletContextForTemplateLoading(servletContext, templateDir);
			}
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("from", from);
			root.put("to", getRecipientsAsString(to));
			root.put("subject", subject);
			if(attachment != null) {
				root.put("attachment", attachment);
			}
			root.put("body", body);
			
			if (cc != null && cc.length > 0)
				root.put("cc", getRecipientsAsString(cc));
			
			if (bcc != null && bcc.length > 0)
				root.put("bcc", getRecipientsAsString(bcc));
			
			File f = new File(outputDir + (new Date()).toString().replace(" ", "_").replace(":", "_") + ".mail");
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(f)));
			
			Template xml = cfg.getTemplate(templateFile);
	        xml.process(root, bw);
			
	        bw.close();
		} catch (EmailUtilException e) {
			throw e;
		} catch (IOException e) {
			throw new EmailUtilException("IOException: " + e.getMessage());
		} catch (TemplateException e) {
			throw new EmailUtilException("[FREEMARKER] Error writing file: " + e.getMessage());
		}
	}
	
	public void buildEmail() throws EmailUtilException {
		try {
			validateDetails();
			
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(new File(templateDir));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("from", from);
			root.put("to", getRecipientsAsString(to));
			root.put("subject", subject);
			
			if(attachment != null) {
				root.put("attachment", attachment);
			}
			root.put("body", body);
			
			if (cc != null && cc.length > 0)
				root.put("cc", getRecipientsAsString(cc));
			
			if (bcc != null && bcc.length > 0)
				root.put("bcc", getRecipientsAsString(bcc));
			
			File f = new File(outputDir + (new Date()).toString().replace(" ", "_").replace(":", "_") + ".mail");
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(f)));
			
			Template xml = cfg.getTemplate(templateFile);
	        xml.process(root, bw);
			
	        bw.close();
		} catch (EmailUtilException e) {
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw new EmailUtilException("IOException: " + e.getMessage());
		} catch (TemplateException e) {
			throw new EmailUtilException("[FREEMARKER] Error writing file: " + e.getMessage());
		}
	}
	
	public void validateDetails() throws EmailUtilException{
		
		if (from == null || from.length() < 1) 
			throw new EmailUtilException(NO_FROM);
		
		if (to == null || to.length < 1) 
			throw new EmailUtilException(NO_TO);
		
		if (subject == null || subject.length() < 1)
			throw new EmailUtilException(NO_SUBJECT);
		
		if (templateFile == null || templateFile.length() < 1)
			throw new EmailUtilException(NO_BODY_TEMPLATE);
		
		if (outputDir == null || outputDir.length() < 1)
			throw new EmailUtilException(NO_OUTPUT_DIR);

		if (!outputDirFile.exists() || !outputDirFile.isDirectory())
			throw new EmailUtilException(OUTPUT_DIR_NOT_FOUND + outputDir);
		
		for (String recipient : to) {
			if (!isValidEmailAddress(recipient))
				throw new EmailUtilException(INVALID_EMAIL_ADDRESS + "TO: " + recipient);
		}
		
		if (cc != null && cc.length > 0) {
			for (String recipient : cc) {
				if (!isValidEmailAddress(recipient))
					throw new EmailUtilException(INVALID_EMAIL_ADDRESS + "CC: " + recipient);
			}	
		}
		
		if (bcc != null && bcc.length > 0) {
			for (String recipient : bcc) {
				if (!isValidEmailAddress(recipient))
					throw new EmailUtilException(INVALID_EMAIL_ADDRESS + "BCC: " + recipient);
			}	
		}
		
	}
	
	private static String getRecipientsAsString(String[] recipients) {
		StringBuilder sb = new StringBuilder();
		for (String s : recipients) {
			sb.append(s + ",");
		}
		return sb.toString().substring(0, sb.length()-1);
	}
	
	public static boolean isValidEmailAddress(String email) {
		//Set the email pattern string
        Pattern p = Pattern.compile(EMAIL_PATTERN_REGEX);
        Matcher m = p.matcher(email);
        return m.matches();
	}
	
	public static void storeMail(
			String templateDir,
			String templateFile, 
			String outputDir, 
			String from, 
			String[] to, 
			String[] cc, 
			String[] bcc, 
			String subject, 
			Object body) throws EmailUtilException {
		
		storeMail(templateDir, templateFile, outputDir, from, to, cc, bcc, subject, null, body, null);
	}
	
	public static void storeMail(
			String templateDir,
			String templateFile, 
			String outputDir, 
			String from, 
			String[] to, 
			String[] cc, 
			String[] bcc, 
			String subject,
			String attachment,
			Object body,
			ServletContext servletContext) throws EmailUtilException {
		
		MailManager mail = new MailManager(
				from,
				to,
				cc,
				bcc,
				subject,
				attachment,
				body,
				templateDir,
				templateFile,
				outputDir
		);
		mail.buildEmail(servletContext);		
	}
	
	public static void storeMail(
			String templateDir,
			String templateFile, 
			String outputDir, 
			String from, 
			String[] to, 
			String[] cc, 
			String[] bcc, 
			String subject,
			String attachment,
			Object body
			) throws EmailUtilException {
		
		MailManager mail = new MailManager(
				from,
				to,
				cc,
				bcc,
				subject,
				attachment,
				body,
				templateDir,
				templateFile,
				outputDir
		);
		mail.buildEmail();		
	}
}
