package org.cambridge.ebooks.production.util;

import java.io.FileInputStream;
import java.util.Properties;

public class ApplicationProperties {


	public static String CHAPTER_LANDING_INDEX 	= "chapter.landing.index.dir";
	public static String BOOK_LANDING_INDEX 	= "book.landing.index.dir";
	public static String SEARCH_INDEX 			= "search.index.dir";
	public static String INDEX 					= "index.dir";
	public static String STOPWORDS 				= "file.stopwords";
	public static String SUBJECT_LANDING_INDEX	= "subject.index.dir";
	public static String SERIES_LANDING_INDEX	= "series.index.dir";
	
	private static final String indexerProperties = "/app/jboss-4.2.3.GA/server/default/deploy/production.war/WEB-INF/classes/org/cambridge/ebooks/production/util/application.properties";
	//private static final String indexerProperties = "C:/eclipse-jee-ganymede-SR1-win32/eclipse/workspace/cbo/PRODUCTION/src-web/org/cambridge/ebooks/production/util/application.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
}
