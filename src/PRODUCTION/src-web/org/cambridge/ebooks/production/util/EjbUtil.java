package org.cambridge.ebooks.production.util;

import java.net.UnknownHostException;
import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.common.ServiceLocatorException;

/**
 * Util for getting the JMS stuff
 * @author mmanalo
 * @modified by rvillamor
 */
public class EjbUtil {
	
	/**
	 * gets the Jms Connection Factory
	 * @param jmsConnectionFactoryJndiName
	 * @return
	 * @throws ServiceLocatorException
	 */
	public static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory)ctx.lookup(jmsConnectionFactoryJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	/**
	 * gets the JmsDestination
	 * @param jmsDestinationJndiName
	 * @return
	 * @throws ServiceLocatorException
	 */
	public static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
	
	/**
	 * gets the Queue
	 * @param logger defined in the jms folder
	 * @return Queue
	 * @throws Exception
	 */
	public static Queue getQueue(String logger)throws Exception{
		Queue queue = null;
		InitialContext ctx;
		try {
			ctx = EjbUtil.getInitialContext();
			queue = (Queue) ctx.lookup(logger);
		} catch (Exception e) {
			throw new Exception(e);
		}
		
		return queue;
	}
	
	/**
	 * gets the InitialContext
	 * @return	InitialContext
	 * @throws Exception
	 */
	public static InitialContext getInitialContext()throws Exception{
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		properties.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
		//properties.put(Context.PROVIDER_URL, "localhost:1099");
		//properties.put(Context.PROVIDER_URL, System.getProperty("provider.url")); // appserver
		properties.put(Context.PROVIDER_URL, getHostAddress() + ":1099"); // appserver
		
		InitialContext ctx = null;
		try {
			ctx = new InitialContext(properties);
		} catch (Exception e) {
			throw new Exception(e);
		}
		
		return ctx;
	}
	
	/**
	 * gets the QueueSession
	 * @return QueueSession
	 * @throws Exception
	 */
	public static QueueSession getQueueSession()throws Exception{
		QueueSession qs = null;
		try{
			QueueConnectionFactory qcf = (QueueConnectionFactory) getInitialContext().lookup("ConnectionFactory");
			QueueConnection qc = qcf.createQueueConnection();
			qs = qc.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);
		}catch(Exception e){
			throw new Exception(e);
		}
		return qs;
	}
	
	private static String getHostAddress() throws UnknownHostException{
		java.net.InetAddress i = java.net.InetAddress.getLocalHost();
		return i.getHostAddress(); // IP address only
	}
}
