package org.cambridge.ebooks.production.util;

public class CrumbBean {

	private String label;
	private String href;
	private String styleClass;
	

	public CrumbBean(String label) {
		this(label, "");
	}

	public CrumbBean(String label, String href) {
		this.label = label;
		this.href = href;
	}
	
	/**
	 * @return Returns the href.
	 */
	public String getHref() {
		return href;
	}
	/**
	 * @param href The href to set.
	 */
	public void setHref(String href) {
		this.href = href;
	}
	/**
	 * @return Returns the label.
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label The label to set.
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return Returns the styleClass.
	 */
	public String getStyleClass() {
		return styleClass;
	}
	/**
	 * @param styleClass The styleClass to set.
	 */
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
}