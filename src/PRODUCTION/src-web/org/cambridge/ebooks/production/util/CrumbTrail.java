/*
 * Created on Jan 10, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cambridge.ebooks.production.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jcg
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CrumbTrail {

	public static String ATTRIBUTE_NAME = "crumbtrail";
	private List trail;
	
	public CrumbTrail() {
		trail = new ArrayList();
	}

	public CrumbTrail(HttpServletRequest request) {
		this();
		this.save(request);
	}

	public void add(CrumbBean c) {
		trail.add(c);
	}
	
	public void add(String label) {
		trail.add(new CrumbBean(label));
	}

	public void add(String label, String href) {
		trail.add(new CrumbBean(label, href));
	}

	public List getTrail() {
		return trail;
	}
	
	public void save(HttpServletRequest request) {
		request.setAttribute(ATTRIBUTE_NAME, this);
	}
		
}
