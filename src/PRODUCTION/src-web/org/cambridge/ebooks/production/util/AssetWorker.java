package org.cambridge.ebooks.production.util;


import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.common.ServiceLocatorException;

/**
 * @author andreau
 *
 */
public class AssetWorker {

	public static void sendAssetEvent( String eisbn )  {
        try {
        	
            ConnectionFactory connectionFactory = getJmsConnectionFactory( "ConnectionFactory" );

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination( "queue/asset/logger" );            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
        	payload.setStringProperty( "EISBN",	eisbn );
            
            try { 
	            messageProducer.send( payload );	            
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
        } catch (JMSException je) {
        	LogManager.error( EventTracker.class, " Message not sent to asset-ejb: " + je.getMessage() );
        	System.out.println("je: "+je);
        } catch (ServiceLocatorException sle) {
        	LogManager.error( EventTracker.class, " Message not sent to asset-ejb:" + sle.getMessage() ); 
        }
    }
	
	public static void sendClientAssetEvent( String eisbn )  {
        try {
        	
        	Queue queue = EjbUtil.getQueue("queue/asset/logger");
			QueueSession qs = EjbUtil.getQueueSession();
			QueueSender sender = qs.createSender(queue);
			
			Message payload = qs.createMapMessage();
            
        	payload.setStringProperty( "EISBN",	eisbn );
            
        	try { 
				sender.send(payload);
			} finally { 
				sender.close();
				qs.close();
			}
        } catch (Exception je) {
        	System.out.println("je: "+je);
        }
    }
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try {
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try {
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} catch (ClassCastException cce) {
		   throw new ServiceLocatorException(cce);
		} catch (NamingException ne) {
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
}
