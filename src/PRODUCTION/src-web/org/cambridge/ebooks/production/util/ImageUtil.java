package org.cambridge.ebooks.production.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean;


public class ImageUtil {
	private static final Log4JLogger LogManager = new Log4JLogger(ImageUtil.class);
	private static final String BOOKBEAN_NAME = "bookContentBean";

	/**
	 * validates if the image exists, replaces with a default no cover when it
	 * does not exists
	 * 
	 * @param image
	 * @return
	 */
	public static String setImagePath(String image, String noImageFile, String isbn, String imageDirPath, Class<?> className) {
//		String imagePath = contentDir + isbn + "/" + image;
		String output = noImageFile;
		if(isbn != null ){
			String imagePath = IsbnContentDirUtil.getFilePath(isbn, image);
			String imagePathIsbnBreakdown = IsbnContentDirUtil.getFilePathIsbnBreakdown(isbn, image) + image;
			File file = new File(imagePath);
			
			LogManager.info(className, "Image Path Isbn Breakdown: " + imagePathIsbnBreakdown);
			LogManager.info(className, "Finding file in 1: " + imageDirPath);
			
			LogManager.info(className, "Finding file in 2: " + imagePath);
			LogManager.info(className, "Can I read the file? : " + file.canRead());
			
			if (file.canRead()) {
				output = imageDirPath + imagePathIsbnBreakdown;
			}
		}
		return output;
	}
	
	/**
	 * same as setImagepath except that this gets the isbn from request attribute for the bean
	 * @param image
	 * @param noImageFile
	 * @param contentDir
	 * @param imageDirPath
	 * @param className
	 * @return
	 */
	public static String setImagePath(String image, String noImageFile, String imageDirPath, Class<?> className) {
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		EBookContentManagedBean bean = (EBookContentManagedBean) request
				.getAttribute(BOOKBEAN_NAME);
		String eisbn = "";
		if(bean != null){
			eisbn = bean.getEisbn();
		}else{
			HttpSession session = HttpUtil.getHttpSession(false);
			bean = (EBookContentManagedBean) session.getAttribute(BOOKBEAN_NAME);
			if(bean != null){
				eisbn = bean.getEisbn();
			}
		}
		
		return setImagePath(image, noImageFile, eisbn, 
				imageDirPath, className);
		
	}
}
