package org.cambridge.ebooks.production.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static final String getCurrentDate(String pattern){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);		
		return sdf.format(date);
	}
	
	public static String dateTimePlusXdays(int x, int year, int month, int date){
        Calendar dateNow = Calendar.getInstance();
        dateNow.set(year, month, date);
        dateNow.add(Calendar.DATE, x);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Date date1 = dateNow.getTime();
        String str = sdf.format(date1);
        return str;
    }
}
