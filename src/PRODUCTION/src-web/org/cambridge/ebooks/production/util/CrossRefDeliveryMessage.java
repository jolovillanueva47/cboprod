package org.cambridge.ebooks.production.util;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.common.ServiceLocatorException;

/**
 * @author rvillamor
 *
 */

public class CrossRefDeliveryMessage {

	public static void sendDeliveryEvent(String bookId, String username)  {
        try 
        {
            ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/crossref/logger");            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
        	payload.setStringProperty("BOOK_ID", bookId);
        	payload.setStringProperty("USERNAME", username);

            try 
            { 
	            messageProducer.send(payload);	            
            } 
            finally 
            { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
            
        } 
        catch (JMSException je) 
        {
        	LogManager.error(CrossRefDeliveryMessage.class, "[JMSException] Message not sent to asset-ejb: " + je.getMessage());
        	System.out.println("[JMSException]: " + je);
        } 
        catch (ServiceLocatorException sle) 
        {
        	LogManager.error(CrossRefDeliveryMessage.class, "[ServiceLocatorException] Message not sent to asset-ejb:" + sle.getMessage()); 
        }
    }
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory)ctx.lookup(jmsConnectionFactoryJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
}
