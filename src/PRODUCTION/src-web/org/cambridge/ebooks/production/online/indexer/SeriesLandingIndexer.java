//package org.cambridge.ebooks.production.online.indexer;
////TODO:SOLR-ify this
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//
//import org.apache.lucene.document.Document;
//import org.apache.lucene.document.Field;
//import org.apache.lucene.index.CorruptIndexException;
//import org.apache.lucene.index.IndexWriter;
//import org.cambridge.ebooks.production.searchejb.document.EbookDetailsBean;
//import org.cambridge.ebooks.production.searchejb.document.RoleAffBean;
//import org.cambridge.util.Misc;
//
//public class SeriesLandingIndexer {
//
////	private String currentBookId;
//	
////	public SeriesLandingIndexer(EbookDetailsBean ebook, IndexWriter writer) throws CorruptIndexException, IOException{
////		indexSeriesLandingPerBook(ebook, writer);
////	}
//	
//	public static void indexSeriesLandingPerBook(EbookDetailsBean ebook, IndexWriter writer) throws CorruptIndexException, IOException{
//		//System.out.println("--- SERIES INDEX START ---");
//		Date today = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHMMss");
//		String timestamp = sdf.format(today);
//		
//		Document doc = new Document();
//		
//		// ebook
//		doc.add(new Field("TIMESTAMP", timestamp, Field.Store.YES, Field.Index.NOT_ANALYZED));
//		
//		if(Misc.isNotEmpty(ebook.getMainTitle())) { 	
//			doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED));
//			doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//		} else {
//			doc.add(new Field("BOOK_TITLE", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
//			doc.add(new Field("BOOK_TITLE_ALPHASORT", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
//		}
//		
//		if(Misc.isNotEmpty(ebook.getEisbn())) 		
//			doc.add(new Field("EISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NOT_ANALYZED));
//		else
//			doc.add(new Field("EISBN", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
//		
//		if(Misc.isNotEmpty(ebook.getSeriesCode())) 		
//			doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.ANALYZED));
//		else
//			doc.add(new Field("SERIES_CODE", "null", Field.Store.YES, Field.Index.ANALYZED));
//		
//		if(Misc.isNotEmpty(ebook.getSeries())) 		
//			doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NOT_ANALYZED));
//		else
//			doc.add(new Field("SERIES_NAME", "null",Field.Store.YES, Field.Index.NOT_ANALYZED));
//		
//		if(Misc.isNotEmpty(ebook.getVolume())) { 		
//			doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
//			doc.add(new Field("VOLUME_ALPHASORT", ebook.getVolume(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//		} else {
//			doc.add(new Field("VOLUME", "null", Field.Store.YES, Field.Index.ANALYZED));
//			doc.add(new Field("VOLUME_ALPHASORT", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
//		} 
//					
//		if(Misc.isNotEmpty(ebook.getOnlineDate())) { 		
//			doc.add(new Field("PUBLICATION_DATE", IndexerUtil.removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.ANALYZED));
//			doc.add(new Field("PUBLICATION_DATE_ALPHASORT", IndexerUtil.removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
//		} else {
//			doc.add(new Field("PUBLICATION_DATE", "null", Field.Store.YES, Field.Index.ANALYZED));
//			doc.add(new Field("PUBLICATION_DATE_ALPHASORT", "null",Field.Store.YES, Field.Index.NOT_ANALYZED));
//		}
//		
//		if(Misc.isNotEmpty(ebook.getBlurb())) 	
//			doc.add(new Field("BOOK_BLURB", ebook.getBlurb(),Field.Store.YES, Field.Index.NOT_ANALYZED));
//		else
//			doc.add(new Field("BOOK_BLURB", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
//		
//		if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
//			List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
//			StringBuffer authorsDisplay = new StringBuffer();
//			for (Iterator<RoleAffBean> it = authorAffList.iterator();it.hasNext();) {
//				RoleAffBean bean = it.next();
//				authorsDisplay.append(RoleAffBean.fixAuthorName(bean.getAuthor()));
//				if (it.hasNext()) {
//					authorsDisplay.append(", ");
//				}
//			}
//			doc.add(new Field("BOOK_AUTHORS", authorsDisplay.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//			doc.add(new Field("BOOK_AUTHORS_ALPHASORT", authorsDisplay.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//		}
//			
//		writer.addDocument(doc);			
//		//System.out.println("--- SERIES INDEX END ---");
//	}
////	public String getCurrentBookId() {
////		return currentBookId;
////	}
////
////	public void setCurrentBookId(String currentBookId) {
////		this.currentBookId = currentBookId;
////	}
//}
