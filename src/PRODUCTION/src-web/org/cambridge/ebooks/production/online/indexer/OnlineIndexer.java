//package org.cambridge.ebooks.production.online.indexer;
////TODO:SOLR-ify this
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
//import org.apache.lucene.index.CorruptIndexException;
//import org.apache.lucene.index.IndexWriter;
//import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.store.Directory;
//import org.apache.lucene.store.FSDirectory;
//import org.cambridge.ebooks.production.online.indexer.properties.OnlineIndexerProperties;
//import org.cambridge.ebooks.production.searchejb.content.SearchForIndented;
//import org.cambridge.ebooks.production.searchejb.document.EbookChapterDetailsBean;
//import org.cambridge.ebooks.production.searchejb.document.EbookDetailsBean;
//import org.cambridge.ebooks.production.searchejb.ebook.EBookIndexSearch;
//
//
//public class OnlineIndexer {
//	private static Map<String, String[]> eBookMap;
//	private String currentBookId;
//	
////	public static final String[] STOP_WORDS = {
////		"about", "after", "all", "also", "an", "and",
////		"any", "are", "as", "at", "be", "because",
////		"been", "but", "by", "can", "co", "corp",
////		"could", "for", "from", "had", "has", "have",
////		"he", "her", "his", "if", "in", "inc", "into",
////		"is", "it", "its", "last", "more", "most", "mr",
////		"mrs", "ms", "mz", "no", "not", "of", "on", "one",
////		"only", "or", "other", "out", "over", "says",
////		"she", "so", "some", "such", "than", "that", "the",
////		"their", "there", "they", "this", "to", "up",
////		"was", "we", "were", "when", "which", "who", 
////		"will", "with", "would"
////	};
//	
//	static {				
//		try {
//			eBookMap = getEBookMap();
//		} catch (Exception e) {
//			System.out.println(OnlineIndexer.class + " [Exception] " + e.getMessage());
//			e.printStackTrace();
//		}
//	}
//	
//	public static void main(String[] args) {
//		OnlineIndexer oi = new OnlineIndexer();
//		System.out.println("[OnlineIndexer] main");
//		
//		if(args.length > 1){
//			for(int i=0; i< args.length; i++){
//				if(args[i].equals("search")){
//					oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.SEARCH_INDEX), args[i]);
//				}else if(args[i].equals("book")){
//					oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.BOOK_LANDING_INDEX), args[i]);
//				}else if(args[i].equals("chapter")){
//					oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.CHAPTER_LANDING_INDEX), args[i]);
//				}else if(args[i].equals("series")){
//					oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.SERIES_LANDING_INDEX), args[i]);
//				}else if(args[i].equals("subject")){
//					oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.SUBJECT_LANDING_INDEX), args[i]);
//				}
//			}
//		}else{
//			System.out.println("[ERROR] Indicate Index Type(search/book/chapter/series/subject)");
//		}
//		
//	}
//	
//	public void reindexFromFile(String indexDirPath, String type){
//		try {
//			System.out.println("[OnlineIndexer] Start: ["+type+"]");
//			long totalStartTime = System.currentTimeMillis();
//			
//			IndexWriter writer = null;
//			
//			if(type.equals("search")){				
//				writer = IndexerUtil.getIndexWriter(indexDirPath, 20, new SnowballAnalyzer("English"));
//			} else {
//				writer = IndexerUtil.getIndexWriter(indexDirPath, 20);
//			}
//			
//			Directory indexDir = FSDirectory.getDirectory(System.getProperty("index.dir"));
//			System.out.println("bookindex: "+System.getProperty("index.dir"));
//			IndexSearcher searcher = new IndexSearcher(indexDir);
//			// searcher index
//			
//			for(String bookId : eBookMap.keySet()){
//				long startTime = System.currentTimeMillis();
//				
//				// search index for ebook details
//				setCurrentBookId(bookId);
//				System.out.println("bookId: "+bookId);
//				
//				EbookDetailsBean ebook = new EbookDetailsBean(searcher, bookId);
//				// overload ebookdetails(searcherindex, bookid)
//					
//				// search index for chapter details
//				List<String> contentIds = new ArrayList<String>();
//				contentIds = EBookIndexSearch.getContentIds(searcher, bookId);				
//				HashMap<String, String> eContentMap = SearchForIndented.setIndentedHashmap(searcher, bookId);
//				HashMap<String, String> tlMap = SearchForIndented.setTopLevelHashmap(searcher, bookId);					
//				List<EbookChapterDetailsBean> chapters = new ArrayList<EbookChapterDetailsBean>();
//				EbookChapterDetailsBean ebookChapter;
//				
//				for(String id : contentIds){
//					ebookChapter = new EbookChapterDetailsBean();
//					ebookChapter.show(searcher, id);					
//					chapters.add(ebookChapter);
//				
//				}
//				// search index for chapter contents
//				HashMap<String, String> contents = new HashMap<String,String>();
//				contents = IndexerUtil.getContents(searcher, chapters);
//				System.out.println("[OnlineIndexer] bookId read End: "+((System.currentTimeMillis() - startTime) / 1000));
//				
//				if(type.equals("search")){
//					AdvanceSearchIndexer.indexDocs(ebook, chapters, contents, eContentMap, writer);
////					new AdvanceSearchIndexer(ebook, chapters, contents, eContentMap, writer);
//				}else if(type.equals("book")){
//					BookLandingIndexer.createBookLandingIndex(ebook, chapters, contents, tlMap, writer);
////					new BookLandingIndexer(ebook, chapters, eContentMap, tlMap, writer);
//				}else if(type.equals("chapter")){
//					ChapterLandingIndexer.createChapterLandingIndex(ebook, chapters, eContentMap, tlMap, writer);
////					new ChapterLandingIndexer(ebook, chapters, eContentMap, tlMap, writer);
//				}else if(type.equals("series")){
//					SeriesLandingIndexer.indexSeriesLandingPerBook(ebook, writer);
////					new SeriesLandingIndexer(ebook, writer);
//				}else if(type.equals("subject")){
//					SubjectLandingIndexer.indexSubjectLandingPerBook(ebook, writer);
////					new SubjectLandingIndexer(ebook, writer);
//				}
//				System.out.println("[OnlineIndexer] bookId write End: "+((System.currentTimeMillis() - startTime) / 1000));
//			}
//			
//			// close searcher
//			searcher.close();
//			closeWriter(writer);
//			indexDir.close();
//			System.out.println("[OnlineIndexer] End: "+type);
//			System.out.println("[OnlineIndexer] seconds: "+((System.currentTimeMillis() - totalStartTime)/1000));
//		} catch (Exception e) {
//			System.out.println("[OnlineIndexer] Exception - Failed to Index:"+type+" \n "+e.getMessage());
//			e.printStackTrace();
//		}
//	}
//	
//	public void closeWriter(IndexWriter writer) throws CorruptIndexException, IOException{
//		writer.setUseCompoundFile(true);
//		writer.optimize();
//		writer.close();
//	}
//		
//	public static Map<String, String[]> getEBookMap() throws IOException {
//		File file = new File(System.getProperty(OnlineIndexerProperties.ISBN_LIST));
//		
//		FileReader fr;
//		BufferedReader br;		
//		String[] liveDate = new String[2];
//		Map<String, String[]> eBookMap = new LinkedHashMap<String, String[]>();
//		
//		fr = new FileReader(file);
//		br = new BufferedReader(fr);
//		
//		String line;
//		while((line = br.readLine()) != null){
//			String[] s = line.split(",");
//			if(s.length > 0 && s.length < 4) {
//				liveDate[0] = s[1].trim();
//				liveDate[1] = s[2].trim();
//				eBookMap.put(s[0].trim(), liveDate);
//			}
//		}
//		return eBookMap;
//	}
//	
//	public static String getEBookLiveDate(String bookId) {
//		return eBookMap.get(bookId)[1];
//	}
//	
//	public static String getEBookLiveDateNum(String bookId) {
//		return eBookMap.get(bookId)[0];
//	}
//	
//	public String getCurrentBookId() {
//		return currentBookId;
//	}
//
//	public void setCurrentBookId(String currentBookId) {
//		this.currentBookId = currentBookId;
//	}
//	
//}
