//package org.cambridge.ebooks.production.online.indexer;
////TODO:SOLR-ify this
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Iterator;
//import java.util.List;
//
//import org.apache.lucene.document.Document;
//import org.apache.lucene.document.Field;
//import org.apache.lucene.index.CorruptIndexException;
//import org.apache.lucene.index.IndexWriter;
//import org.cambridge.ebooks.production.online.indexer.properties.OnlineIndexerProperties;
//import org.cambridge.ebooks.production.searchejb.content.IndexSearchUtil;
//import org.cambridge.ebooks.production.searchejb.document.EbookChapterDetailsBean;
//import org.cambridge.ebooks.production.searchejb.document.EbookDetailsBean;
//import org.cambridge.ebooks.production.searchejb.document.PublisherNameBean;
//import org.cambridge.ebooks.production.searchejb.document.RoleAffBean;
//import org.cambridge.ebooks.production.searchejb.document.SubjectBean;
//import org.cambridge.ebooks.production.searchejb.util.EncodingUtil;
//import org.cambridge.ebooks.production.searchejb.util.StringUtil;
//import org.cambridge.util.Misc;
//
//public class BookLandingIndexer {
//	
////	private String currentBookId;
//	
////	public BookLandingIndexer(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
////			HashMap<String, String> contents, HashMap<String,String> tlMap, IndexWriter writer) throws Exception{
////		createBookLandingIndex(ebook, chapters, contents, tlMap, writer);
////	}
//	
//	/**
//	 * main method for creating book landing Index
//	 * @param ebook
//	 * @param chapters
//	 * @param contents
//	 * @throws Exception 
//	 */
//	public static void createBookLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
//			HashMap<String, String> contents, HashMap<String,String> tlMap, IndexWriter writer) throws Exception{
//		// Book Landing Page Indexer	
//		//System.out.println("--- BOOK LANDING INDEX START ---");
//		
//        //Per book
//		indexBookLandingPerBook(ebook, writer);			
//		
//		// Per author	        
//		indexBookLandingPerAuthor(ebook, false, null, writer);		
//		
//		// Per subject	        
//		indexBookPerSubject(ebook, false, null, writer);			
//					
//		// Per content-item	        
//        indexBookPerContent(ebook, chapters, contents, tlMap, writer);
//        
//        // Per contributor
//        indexChapterPerContributor(ebook.getBookId(), writer);
//        
//        //System.out.println("--- BOOK LANDING INDEX END ---");
//	}
//	
//	/**
//	 * indexes contributor per contentItem
//	 * @param bookId
//	 * @throws IOException 
//	 * @throws CorruptIndexException 
//	 */
//	private static void indexChapterPerContributor(String bookId, IndexWriter writer) throws CorruptIndexException, IOException {
//				
//			//IndexerUtil.checkIfIndexIsLocked(OnlineIndexerProperties.INDEX);			
//			
//			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(OnlineIndexerProperties.INDEX), 
//					"ELEMENT: contributor AND BOOK_ID: " + bookId);		
//			for(Document doc : docs ){						
//				String name = doc.get(IndexerUtil.CONTRIBUTOR);
//				String aff = doc.get(IndexerUtil.AFFILIATION);
//				Document newDoc = new Document();
//				if(Misc.isNotEmpty(name)) 				
//					newDoc.add(new Field(IndexerUtil.CONTRIBUTOR, name, Field.Store.YES, Field.Index.NO));
//				if(Misc.isNotEmpty(aff)) 				
//					newDoc.add(new Field(IndexerUtil.AFFILIATION, aff, Field.Store.YES, Field.Index.NO));
//				
//				//DOC_TYPE
//				newDoc.add(new Field(IndexerUtil.DOC_TYPE, IndexerUtil.BOOK_CONTRIBUTOR, Field.Store.YES, Field.Index.NOT_ANALYZED));
//				//BOOK_ID
//				newDoc.add(new Field(IndexerUtil.BOOK_ID, bookId, Field.Store.YES, Field.Index.NOT_ANALYZED));
//				newDoc.add(new Field(IndexerUtil.CONTENT_ID, doc.get(IndexerUtil.PARENT_ID), Field.Store.YES, Field.Index.ANALYZED));
//				
//				writer.addDocument(newDoc);
//			}
//		
//	}
//	
//	private static void indexBookPerContent(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList,
//			HashMap<String, String> contents, HashMap<String,String> tlMap, IndexWriter writer) throws CorruptIndexException, IOException{		
//		if(chaptersList != null && chaptersList.size() > 0)	
//		{
//			Document doc;
//			for(EbookChapterDetailsBean chapter : chaptersList) 
//			{
//				doc = new Document();
//				
//					doc.add(new Field("DOC_TYPE", "content_item",Field.Store.YES, Field.Index.NOT_ANALYZED));
//					
//					// ebook
//					if(Misc.isNotEmpty(ebook.getBookId()))
//						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
//					if(Misc.isNotEmpty(ebook.getEisbn())) 		
//						doc.add(new Field("ONLINE_ISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NO));
//					
//					// chapter
//					if(Misc.isNotEmpty(chapter.getContentId())) 		
//						doc.add(new Field("CONTENT_ID", chapter.getContentId(),Field.Store.YES, Field.Index.ANALYZED));
//					if(Misc.isNotEmpty(chapter.getDoi())) {
//						doc.add(new Field("CHAPTER_DOI", chapter.getDoi(), Field.Store.YES, Field.Index.NO));
//					}
//					if(Misc.isNotEmpty(chapter.getHeadingTitle()))		
//						doc.add(new Field("CONTENT_TITLE", chapter.getHeadingTitle(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
//						doc.add(new Field("CONTENT_LABEL", chapter.getHeadingLabel(), Field.Store.YES, Field.Index.NO));
//					}
//					if(Misc.isNotEmpty(chapter.getContentType())) {
//						doc.add(new Field("CONTENT_TYPE", chapter.getContentType(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//					}
//					if(Misc.isNotEmpty(chapter.getHeadingSubtitle()))	
//						doc.add(new Field("CONTENT_SUB_TITLE", chapter.getHeadingSubtitle(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(chapter.getPdfFilename()))		
//						doc.add(new Field("PDF", chapter.getPdfFilename(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(chapter.getPageStart()))			
//						doc.add(new Field("PAGE", chapter.getPageStart(),Field.Store.YES, Field.Index.NO));
//					
//					indexContentContributor(chapter.getContentId(), doc);
//					
////					if(chapter.getContributors().size() > 0) {
////						doc.add(new Field("CONTENT_CONTRIBUTORS", 
////								StringUtil.getContributorDisplay(chapter.getContributors()), Field.Store.YES, Field.Index.NO));
////					}
//					
////					if(Misc.isNotEmpty(chapter.getContributorNameModified()))				
////						doc.add(new Field("CONTRIBUTOR", chapter.getContributorNameModified(),Field.Store.YES, Field.Index.NO));
//					
//					//added for view abstract text and image
//					if(Misc.isNotEmpty(chapter.getAbstractContent()))				
//						doc.add(new Field("CONTENT_EXTRACT", chapter.getAbstractContent(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(chapter.getAbstractImage()))				
//						doc.add(new Field("CONTENT_IMAGE_EXTRACT", chapter.getAbstractImage(),Field.Store.YES, Field.Index.NO));
//					
//					
//					//getIfContains Subchapter
//					doc.add(new Field("CONTENT_SUB_CHAPTERS", IndexerUtil.hasSubchapters(contents, chapter),Field.Store.YES, Field.Index.NO));
//					
//					//if it is top level
//					doc.add(new Field("BOOK_LEVEL", IndexerUtil.isTopLevel(tlMap, chapter), Field.Store.YES, Field.Index.ANALYZED));
//					
//					// TOC_ITEM
//					if(Misc.isNotEmpty(chapter.getToc())) {
//						doc.add(new Field("TOC_ITEM", 
//								chapter.getToc().replaceAll("<em>", "")
//								.replaceAll("</em>", "")
//								.replaceAll("<br />", "=DEL="), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS)
//						);
//					}
//					
//					writer.addDocument(doc);
//				
//			}
//		}
//	}
//	
//	private static void indexContentContributor(String contentId, Document document) {
//		try {			
//			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(OnlineIndexerProperties.INDEX), 
//					"ELEMENT: contributor AND PARENT_ID: " + contentId);	
//			
//			List<String> contributors = new ArrayList<String>();
//			
//			for(Document doc : docs){						
//				String name = doc.get(IndexerUtil.CONTRIBUTOR);
//				if(Misc.isNotEmpty(name)) { 
//					contributors.add(name);
//				}
//			}
//			
//			String contributorDisplay = StringUtil.getContributorDisplay(contributors);
//			String contributor = StringUtil.getContributors(contributors);
//			if(Misc.isNotEmpty(contributor)) {
//				document.add(new Field("CONTENT_CONTRIBUTORS", contributor.trim(), Field.Store.YES, Field.Index.NO));
//				document.add(new Field("CONTENT_CONTRIBUTORS_DISPLAY", contributorDisplay.trim(), Field.Store.YES, Field.Index.NO));
//			}
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}
//	
//	/**
//	 * Index book Per Subject
//	 * @param ebook
//	 * @throws IOException 
//	 * @throws CorruptIndexException 
//	 */
//	private static void indexBookPerSubject(EbookDetailsBean ebook, boolean addContentId, 
//			String contentId, IndexWriter writer) throws CorruptIndexException, IOException{		
//		
//			if(!Misc.isEmpty(ebook.getSubjectList())) {
//				List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
//				subjectList = ebook.getSubjectList();
//				
//				for(SubjectBean subject : subjectList){
//					Document doc = new Document();
//					doc.add(new Field("DOC_TYPE", "subject",Field.Store.YES, Field.Index.NOT_ANALYZED));
//					if(Misc.isNotEmpty(ebook.getBookId()))		
//						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
//					if(Misc.isNotEmpty(subject.getSubject())) 	
//						doc.add(new Field("SUBJECT", subject.getSubject(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(subject.getCode())) 		
//						doc.add(new Field("SUBJECT_CODE", subject.getCode(),Field.Store.YES, Field.Index.NO));
//					
//					//add contentId
//					addContentId(doc, addContentId, contentId);
//					
//					writer.addDocument(doc);
//				}				
//			}
//	}
//	
//	private static void indexBookLandingPerAuthor(EbookDetailsBean ebook, boolean addContentId, 
//			String contentId, IndexWriter writer) throws CorruptIndexException, IOException{
//		Document doc = null;		
//		
//			if(!Misc.isEmpty(ebook.getAuthorAffList())) {
//				List<RoleAffBean> authorAffList = new ArrayList<RoleAffBean>();
//				authorAffList = ebook.getAuthorAffList();
//				for(RoleAffBean roleAff : authorAffList){
//					doc = new Document();
//					doc.add(new Field("DOC_TYPE", "book_author",Field.Store.YES, Field.Index.NOT_ANALYZED));
//					
//					if(Misc.isNotEmpty(ebook.getBookId()))			
//						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
//					if(Misc.isNotEmpty(roleAff.getAuthor())) 		
//						doc.add(new Field("NAME", roleAff.getAuthor(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(roleAff.getFixAuthor())) {
//						doc.add(new Field("FIX_NAME", roleAff.getFixAuthor(),Field.Store.YES, Field.Index.NO));
//					}
//					if(Misc.isNotEmpty(roleAff.getAffiliation())) 	
//						doc.add(new Field("AFFILIATION", roleAff.getAffiliation(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(roleAff.getPosition())) 		
//						doc.add(new Field("POSITION", roleAff.getPosition(),Field.Store.YES, Field.Index.NO));
//					if(Misc.isNotEmpty(roleAff.getRole())) 			
//						doc.add(new Field("ROLE", roleAff.getRole(),Field.Store.YES, Field.Index.NO));
//										
//					//add contentId
//					addContentId(doc, addContentId, contentId);
//					
//					writer.addDocument(doc);
//				}
//			}
//			
//			
//		
//	}
//	
//	/**
//	 * adds content id to doc
//	 * @param doc
//	 * @param addContentId
//	 * @param contentId
//	 */
//	private static void addContentId(Document doc, boolean addContentId, String contentId) {
//		if(addContentId){
//			if(Misc.isNotEmpty(contentId)){
//				doc.add(new Field("CONTENT_ID", contentId, Field.Store.YES, Field.Index.ANALYZED));
//			}
//		}
//	}
//	
//	private static void indexBookLandingPerBook(EbookDetailsBean ebook, IndexWriter writer) throws CorruptIndexException, IOException{
//		Document doc;
//			
//		doc = new Document();
//			// ebook
//			doc.add(new Field("DOC_TYPE", "book_details",Field.Store.YES, Field.Index.NOT_ANALYZED));
//			
//			if(Misc.isNotEmpty(ebook.getBookId())) 		
//				doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
//			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
//				doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
//				doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(), Field.Store.YES, Field.Index.NO));
//			}
//			if(Misc.isNotEmpty(ebook.getSubTitle())) 	
//				doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getSeriesCode())) 	
//				doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getSeries())) 		
//				doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getSeriesNumber())) 		
//				doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getCopyrightStatement())) 	
//				doc.add(new Field("COPYRIGHT", IndexerUtil.removeBr(ebook.getCopyrightStatement()),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getPrintDate())) 	
//				doc.add(new Field("PRINT_PUBLICATION_DATE",IndexerUtil.removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getOnlineDate())) 	
//				doc.add(new Field("ONLINE_PUBLICATION_DATE", IndexerUtil.removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getDoi())) 		
//				doc.add(new Field("BOOK_DOI", ebook.getDoi(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getEisbn())) 		
//				doc.add(new Field("ONLINE_ISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getHardback())) 	
//				doc.add(new Field("HARDBACK_ISBN", ebook.getHardback(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getPaperback())) 	
//				doc.add(new Field("PAPERBACK_ISBN", ebook.getPaperback(),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getVolume())) { 		
//				doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
//			} 
//			if(Misc.isNotEmpty(ebook.getOtherVolume())) { 		
//				doc.add(new Field("OTHER_VOLUME", ebook.getOtherVolume(), Field.Store.YES, Field.Index.ANALYZED));
//			} 
//			
//			//additional
//			if(Misc.isNotEmpty(ebook.getEditionNumber())) { 	
//				doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED));
//			}
//			if(Misc.isNotEmpty(ebook.getEdition())) {
//				doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED));
//			}
//			if(Misc.isNotEmpty(ebook.getStandardImage()))
//				doc.add(new Field("COVER_IMAGE", ebook.getStandardImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getThumbImage())) 	
//				doc.add(new Field("COVER_IMAGE_THUMBNAIL", ebook.getThumbImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
//			if(Misc.isNotEmpty(ebook.getBlurb())) 	
//				doc.add(new Field("BOOK_BLURB", EncodingUtil.convertToUTF8(ebook.getBlurb()),Field.Store.YES, Field.Index.NO));
//			
//			// publisher 
//			if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
//				List<PublisherNameBean> publisherList = ebook.getPubNameList();
//				StringBuffer publisherDisplay = new StringBuffer();
//				for (Iterator<PublisherNameBean> it = publisherList.iterator();it.hasNext();) {
//					PublisherNameBean bean = it.next();
//					publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
//					if (it.hasNext()) {
//						publisherDisplay.append(", ");
//					}
//				}
//				doc.add(new Field("PUBLISHER", publisherDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED));
//			}
//			
//			String liveDate = OnlineIndexer.getEBookLiveDate(ebook.getBookId());
//			if(Misc.isNotEmpty(liveDate)) {
//				doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED));
//			}
//			
//			writer.addDocument(doc);
//	}
//	
////	public String getCurrentBookId() {
////		return currentBookId;
////	}
////
////	public void setCurrentBookId(String currentBookId) {
////		this.currentBookId = currentBookId;
////	}
//}
