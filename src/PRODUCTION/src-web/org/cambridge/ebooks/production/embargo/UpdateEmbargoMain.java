package org.cambridge.ebooks.production.embargo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.cambridge.ebooks.production.embargo.bean.EBook;
import org.cambridge.ebooks.production.embargo.bean.EBookContent;
import org.cambridge.ebooks.production.util.DataSource;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;

public class UpdateEmbargoMain {

	//public static final String CONTENT_ITEMS = "C:/dru/task/CBO Production/Embargo/content/";
//	public static final String CONTENT_ITEMS = System.getProperty("content.dir");
	
	public static void main(String[] args) {
		Connection con = DataSource.getConnection();
		try 
		{
			if(con != null)
			{
				Statement stmt = con.createStatement();
				
				// Get EBook Contents where status equals Embargo
				ResultSet rs = stmt.executeQuery("select ec.isbn, ec.content_id, ec.status, ec.remarks, ec.type, e.series_code from ebook e, ebook_content ec WHERE (ec.status = 9 AND ec.embargo_date < SYSDATE AND ec.isbn = e.isbn)");
			    Set<String> isbnSet = new HashSet<String>();
				String isbn;
				EBookContent content;
			    while (rs.next()){
			    	// Audit Trail
			    	content = getEBookContent(rs);
			    	content.setStatus("7");
			    	EventTracker.sendClientBookContentEvent(content);
			    	
			    	isbnSet.add(content.getIsbn());
			    }
				System.out.println("EBook contents with status embargo: "+isbnSet);
				
				// Update Status of EBook Contents from Embargo to Publish 
				int ctr = stmt.executeUpdate("update ebook_content set status = 7, embargo_date = null WHERE (status = 9 AND embargo_date < SYSDATE)");
				System.out.println(ctr+" rows affected while updating ebook_content to publish");
			    
			    File file;
			    FileReader fr;
			    BufferedReader br;
			    Iterator<String> itr = isbnSet.iterator();
			    while(itr.hasNext()){
			    	
			    	// Get Content Ids from Content_items.txt
			    	isbn = itr.next();
//			    	file = new File(CONTENT_ITEMS+isbn+"/CONTENT_ITEMS.txt");
			    	file = new File(IsbnContentDirUtil.getPath(isbn)+"CONTENT_ITEMS.txt");
			    	
				    fr = new FileReader(file);
				    br = new BufferedReader(fr);
				    String contentId;
				    Set<String> statusSet = new HashSet<String>();
				    while((contentId = br.readLine()) != null){
				    	// Get status of all contents
				    	rs = stmt.executeQuery("select status from ebook_content WHERE content_id = '"+contentId+"'");
				    	while(rs.next()){
				    		statusSet.add(rs.getString("STATUS"));
				    	}
				    }
				    
				    // If the status of all contents is PUBLISH
				    // then SET EBook status to PUBLISH
				    if(statusSet.size() == 1){
				    	if(statusSet.contains("7")){
				    		ctr = stmt.executeUpdate("update ebook set status = 7 where isbn = '"+isbn+"'");
				    		
				    		rs = stmt.executeQuery("select * from ebook where isbn = "+isbn);
				    		EBook ebook = null;
				    		while(rs.next()){
				    			ebook = getEBook(rs);
				    			
				    			// Audit Trail
				    			EventTracker.sendClientEBookEvent(ebook);
				    			
				    			// Asset Worker
					    		//AssetWorker.sendClientAssetEvent(isbn);
					    		
					    		// Update Index for EBooks Online
					    		// SearchIndexWorker.sendMessage(ebook.getBookId(), "publish");
					    		
					    		System.out.println(ctr+" rows affected while updating ebook status to publish: "+ebook.getBookId());
				    		}
				    	}
				    }
			    }
			}
		} 
		catch(SQLException sqle)
		{
			try {
				con.rollback();
			} catch (SQLException e) {
				System.out.println(UpdateEmbargoMain.class + " [SQLException] " + "error in rollback.");
			}
		}
		catch (Exception e) 
		{
			System.out.println(UpdateEmbargoMain.class + " [Exception] " + e.getMessage());
		}
		finally
		{
			DataSource.closeConnection(con);
		}
	}
	
	public static EBook getEBook(ResultSet rs){
		EBook ebook = new EBook();
		try {
			ebook.setIsbn(rs.getString("ISBN"));
			ebook.setBookId(rs.getString("BOOK_ID"));
			ebook.setTitle(rs.getString("TITLE"));
			ebook.setSeriesCode(rs.getString("SERIES_CODE"));
		} catch (SQLException e) {
			System.out.println(UpdateEmbargoMain.class + " [SQLException] " + e.getMessage());
		}
		return ebook;
	}
	
	public static EBookContent getEBookContent(ResultSet rs){
		EBookContent content = new EBookContent();
		try {
			content.setContentId(rs.getString("CONTENT_ID"));
			content.setIsbn(rs.getString("ISBN"));
			content.setLoadType(rs.getString("TYPE"));
			content.setRemarks(rs.getString("REMARKS"));
			content.setStatus(rs.getString("STATUS"));
			content.setSeriesCode(rs.getString("SERIES_CODE"));
		} catch (SQLException e) {
			System.out.println(UpdateEmbargoMain.class + " [SQLException] " + e.getMessage());
		}		
		return content;
	}

}
