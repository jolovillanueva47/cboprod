/*
 * Created on Jan 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.production.embargo.properties;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ServicesProperties {
	
	public static String DRIVERTYPE = "db.drivertype";
	public static String SERVERNAME = "db.servername";
	public static String DATABASENAME = "db.databasename";
	public static String PORTNUMBER = "db.portnumber";
	public static String USER = "db.user";
	public static String PASSWORD = "db.password";	

	public static String DIR_SCRIPT = "dir.shell.script";
	
	//private static final String indexerProperties = "C:/dru/eclipse J2EE/workspace/cbo/SERVICES/service.properties";
	private static final String indexerProperties = System.getProperty("indexer.properties");
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
