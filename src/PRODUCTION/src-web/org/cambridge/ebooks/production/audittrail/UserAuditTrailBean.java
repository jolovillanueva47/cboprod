package org.cambridge.ebooks.production.audittrail;

import java.util.ArrayList;
import java.util.Calendar;

import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.audittrail.UserAuditTrailBooks;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;

public class UserAuditTrailBean {
	private static final Log4JLogger LogManager = new Log4JLogger(UserAuditTrailBean.class);
	private UIInput userName;
	
	public ArrayList<UserAuditTrailBooks> userAuditList;
	
	private String auditDateFromDay;
	private String auditDateFromMonth;
	private String auditDateFromYear;
	
	private String auditDateToDay;
	private String auditDateToMonth;
	private String auditDateToYear;
	
	private SelectItem[] dayItems;
	private SelectItem[] yearItems;
	
	public void initUserAuditList(ActionEvent event) {
		initFields();
	}
	
	public String updateUserAuditTrail() {
		return RedirectTo.USER_AUDIT_TRAIL_UPDATE;
	}
	
	public void updateUserList(ActionEvent event) {
		LogManager.info(UserAuditTrailBean.class, "Acquiring User Audit Trail List...");
		String username = (String) userName.getValue();
		if(username.length() == 0)
			username = "%";
		
		String dateFrom = auditDateFromDay + "-" + auditDateFromMonth + "-" + auditDateFromYear;
		String dateTo = auditDateToDay + "-" + auditDateToMonth + "-" + auditDateToYear;
		
		userAuditList = PersistenceUtil.searchList(new UserAuditTrailBooks(), UserAuditTrailBooks.SEARCH_USER_AUDIT, username, dateFrom, dateTo);
	}

	public ArrayList<UserAuditTrailBooks> getUserAuditList() {
		return userAuditList;
	}

	public void setUserAuditList(ArrayList<UserAuditTrailBooks> userAuditList) {
		this.userAuditList = userAuditList;
	}
	
	public SelectItem[] getYearItems(){
		SelectItem[] si = new SelectItem[3];
		Calendar calendar = Calendar.getInstance();
		int ctr=0;
		for(int start = calendar.get(Calendar.YEAR) - 2; start < calendar.get(Calendar.YEAR)+1; start++){
			si[ctr] = new SelectItem(Integer.toString(start),Integer.toString(start));
			ctr++;
		}
		setYearItems(si);
		
		return yearItems;
	}
	
	public void setYearItems(SelectItem[] yearItems){
		this.yearItems = yearItems;
	}
	
	public SelectItem[] getDayItems(){
		SelectItem[] si = new SelectItem[31];
		for(int i=0; i<31; i++){
			si[i] = new SelectItem(Integer.toString(i+1),Integer.toString(i+1));
		}
		setDayItems(si);
		
		return dayItems;
	}
	
	public void initFields(){
		userAuditList = null;
			
		if(userName != null)
			userName.resetValue();
		
		SelectItem[] days = new SelectItem[31];
		for(int i=0; i<31; i++){
			days[i] = new SelectItem(Integer.toString(i+1),Integer.toString(i+1));
		}
		setDayItems(days);
		
		SelectItem[] years = new SelectItem[3];
		Calendar calendar = Calendar.getInstance();
		int ctr=0;
		for(int start = calendar.get(Calendar.YEAR) - 2; start < calendar.get(Calendar.YEAR)+1; start++){
			years[ctr] = new SelectItem(Integer.toString(start),Integer.toString(start));
			ctr++;
		}
		setYearItems(years);
		
		setAuditDateFromDay(String.valueOf(calendar.get(Calendar.DATE))); 
		setAuditDateFromYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String fromMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateFromMonth(fromMonth);
		
		setAuditDateToDay(String.valueOf(calendar.get(Calendar.DATE)));
		setAuditDateToYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String toMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateToMonth(toMonth);
	}
	
	public void setDayItems(SelectItem[] dayItems){
		this.dayItems = dayItems;
	}
	
	public static final SelectItem[] monthMap=
	{
		new SelectItem("Jan","Jan"),
		new SelectItem("Feb","Feb"),
		new SelectItem("Mar","Mar"),
		new SelectItem("Apr","Apr"),
		new SelectItem("May","May"),
		new SelectItem("Jun","Jun"),
		new SelectItem("Jul","Jul"),
		new SelectItem("Aug","Aug"),
		new SelectItem("Sep","Sep"),
		new SelectItem("Oct","Oct"),
		new SelectItem("Nov","Nov"),
		new SelectItem("Dec","Dec")
	};
	
	public SelectItem[] getMonthMap(){
		return monthMap;
	}
	
	public String getMonths(int i) {
    	String months[] = {"Jan","Feb","Mar","Apr","May","Jun",
    					   "Jul","Aug","Sep","Oct","Nov","Dec"};    	
    	return months[i];    	
    }
	
	public String getAuditDateFromYear() {		
		return auditDateFromYear;
	}

	public void setAuditDateFromYear(String auditDateFromYear) {
		this.auditDateFromYear = auditDateFromYear;
	}

	public String getAuditDateFromDay() {        
		return auditDateFromDay;
	}

	public void setAuditDateFromDay(String auditDateFromDay) {
		this.auditDateFromDay = auditDateFromDay;
	}
	
	public String getAuditDateFromMonth() {
		return auditDateFromMonth;
	}

	public void setAuditDateFromMonth(String auditDateFromMonth) {
		this.auditDateFromMonth = auditDateFromMonth;
	}
	
	public String getAuditDateToYear() {
		return auditDateToYear;
	}

	public void setAuditDateToYear(String auditDateToYear) {
		this.auditDateToYear = auditDateToYear;
	}

	public String getAuditDateToDay() {
		return auditDateToDay;
	}

	public void setAuditDateToDay(String auditDateToDay) {
		this.auditDateToDay = auditDateToDay;
	}
	
	public String getAuditDateToMonth() {
		return auditDateToMonth;
	}

	public void setAuditDateToMonth(String auditDateToMonth) {
		this.auditDateToMonth = auditDateToMonth;
	}
	
	public UIInput getUserName() {
		return userName;
	}

	public void setUserName(UIInput userName) {
		this.userName = userName;
	}
}
