package org.cambridge.ebooks.production.audittrail;

import java.util.ArrayList;
import java.util.Calendar;

import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.audittrail.BookAuditTrail;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;

public class BookAuditTrailBean {
	private static final Log4JLogger LogManager = new Log4JLogger(BookAuditTrailBean.class);
	private UIInput eisbnUI;
	private UIInput contentIdUI;
	private UIInput seriesCodeUI;
	private UIInput userNameUI;
	private UIInput statusUI;
	
	public ArrayList<BookAuditTrail> bookAuditList;
	
	private String auditDateFromDay;
	private String auditDateFromMonth;
	private String auditDateFromYear;
	
	private String auditDateToDay;
	private String auditDateToMonth;
	private String auditDateToYear;
	
	private SelectItem[] dayItems;
	private SelectItem[] yearItems;
	
	public void initBookAuditList(ActionEvent event) {
		initFields();
	}
	
	public String updateBookAuditTrail() {
		return RedirectTo.BOOK_AUDIT_TRAIL_UPDATE;
	}
	
	public void updateBookAuditList(ActionEvent event) {
		LogManager.info(BookAuditTrailBean.class, "Acquiring Book Audit Trail List...");
		//get values from the form
		String eisbn 		= (String)eisbnUI.getValue();
		String contentId	= (String)contentIdUI.getValue();
		String seriesCode 	= (String)seriesCodeUI.getValue();
		String username 	= (String)userNameUI.getValue();
		String status   	= (String)statusUI.getValue();
		String dateFrom 	= auditDateFromDay 	+ "-" + auditDateFromMonth 	+ "-" + auditDateFromYear;
		String dateTo 		= auditDateToDay 	+ "-" + auditDateToMonth 	+ "-" + auditDateToYear;
		
		//"and AU.username <> 'testkoto' and AU.username <> 'jcayetano' " +
		//dynamic query
		//select fields and table
		String startSQL = 	"select " +
							"AT.filename, " +
							"AT.load_type, " +
							"AT.username, " +
							"AT.audit_id, " +
							"AT.content_id, " +
							"AT.eisbn, " +
							"AT.series_code, " +	        		
							"AT.status, " +
							"AT.audit_date, " +
							"remarks " +
							"from audit_trail_books AT WHERE ";	        		
		
		//sql conditions
		String conditions = null;
		if(dateFrom.equals(dateTo))
			conditions = "AT.audit_date >= TO_DATE( '"+dateFrom+"' ,'DD-MON-YYYY') ";
		else
			conditions = "AT.audit_date >= TO_DATE( '"+dateFrom+"' ,'DD-MON-YYYY') " + "AND AT.audit_date <= TO_DATE( '"+dateTo+"' ,'DD-MON-YYYY') ";
			
		
		//set parameters
		ArrayList<String> params = new ArrayList<String>();
		int ctr = 1;
		if(eisbn.length() != 0){
			conditions += "AND AT.EISBN = ?"+ctr+" ";
			params.add(eisbn);
			ctr++;
		}
		if(contentId.length() != 0){
			conditions += "AND AT.CONTENT_ID = ?"+ctr+" ";
			params.add(contentId);
			ctr++;
		}
		if(seriesCode.length() != 0){
			conditions += "AND AT.SERIES_CODE = ?"+ctr+" ";
			params.add(seriesCode);
			ctr++;
		}
		if(username.length() != 0){
			conditions += "AND AT.USERNAME = ?"+ctr+" ";
			params.add(username);
			ctr++;
		}
		if(status != null && status.length() > 0){
			conditions += "AND AT.STATUS = ?"+ctr+" ";
			params.add(status);
			ctr++;
		}
		
		//orderby
		String endSQL = "order by AT.content_id, AT.audit_date ";
		
		String sql = startSQL + conditions + endSQL;
		
		//LogManager.info(BookAuditTrailBean.class, "Dynamic Query: "+sql);

		bookAuditList = PersistenceUtil.dynamicSearchList(new BookAuditTrail(), sql, "bookAuditTrailResult", params);
	}
	
	public void initFields(){
		bookAuditList = null;
		
		if(eisbnUI != null)
			eisbnUI.resetValue();
		if(contentIdUI != null)
			contentIdUI.resetValue();
		if(seriesCodeUI != null)
			seriesCodeUI.resetValue();
		if(userNameUI != null)
			userNameUI.resetValue();
		if(statusUI != null)
			statusUI.resetValue();
		
		SelectItem[] days = new SelectItem[31];
		for(int i=0; i<31; i++){
			days[i] = new SelectItem(Integer.toString(i+1),Integer.toString(i+1));
		}
		setDayItems(days);
		
		SelectItem[] years = new SelectItem[3];
		Calendar calendar = Calendar.getInstance();
		int ctr=0;
		for(int start = calendar.get(Calendar.YEAR) - 2; start < calendar.get(Calendar.YEAR)+1; start++){
			years[ctr] = new SelectItem(Integer.toString(start),Integer.toString(start));
			ctr++;
		}
		setYearItems(years);
		
		setAuditDateFromDay(String.valueOf(calendar.get(Calendar.DATE))); 
		setAuditDateFromYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String fromMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateFromMonth(fromMonth);
		
		setAuditDateToDay(String.valueOf(calendar.get(Calendar.DATE)));
		setAuditDateToYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String toMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateToMonth(toMonth);
	}
	
	public ArrayList<BookAuditTrail> getBookAuditList() {
		return bookAuditList;
	}

	public void setBookAuditList(ArrayList<BookAuditTrail> bookAuditList) {
		this.bookAuditList = bookAuditList;
	}
	
	public SelectItem[] getYearItems(){
		return yearItems;
	}
	
	public void setYearItems(SelectItem[] yearItems){
		this.yearItems = yearItems;
	}
	
	public SelectItem[] getDayItems(){
		return dayItems;
	}
	
	public void setDayItems(SelectItem[] dayItems){
		this.dayItems = dayItems;
	}
	
	public static final SelectItem[] monthMap=
	{
		new SelectItem("Jan","Jan"),
		new SelectItem("Feb","Feb"),
		new SelectItem("Mar","Mar"),
		new SelectItem("Apr","Apr"),
		new SelectItem("May","May"),
		new SelectItem("Jun","Jun"),
		new SelectItem("Jul","Jul"),
		new SelectItem("Aug","Aug"),
		new SelectItem("Sep","Sep"),
		new SelectItem("Oct","Oct"),
		new SelectItem("Nov","Nov"),
		new SelectItem("Dec","Dec")
	};
	
	public SelectItem[] getMonthMap(){
		return monthMap;
	}
	
	public String getMonths(int i) {
    	String months[] = {"Jan","Feb","Mar","Apr","May","Jun",
    					   "Jul","Aug","Sep","Oct","Nov","Dec"};    	
    	return months[i];    	
    }
	
	public String getAuditDateFromYear() {
		return auditDateFromYear;
	}

	public void setAuditDateFromYear(String auditDateFromYear) {
		this.auditDateFromYear = auditDateFromYear;
	}

	public String getAuditDateFromDay() {    
		return auditDateFromDay;
	}

	public void setAuditDateFromDay(String auditDateFromDay) {
		this.auditDateFromDay = auditDateFromDay;
	}
	
	public String getAuditDateFromMonth() {   
		return auditDateFromMonth;
	}

	public void setAuditDateFromMonth(String auditDateFromMonth) {
		this.auditDateFromMonth = auditDateFromMonth;
	}
	
	public String getAuditDateToYear() {
		return auditDateToYear;
	}

	public void setAuditDateToYear(String auditDateToYear) {
		this.auditDateToYear = auditDateToYear;
	}

	public String getAuditDateToDay() {
		return auditDateToDay;
	}

	public void setAuditDateToDay(String auditDateToDay) {
		this.auditDateToDay = auditDateToDay;
	}
	
	public String getAuditDateToMonth() {
		return auditDateToMonth;
	}

	public void setAuditDateToMonth(String auditDateToMonth) {
		this.auditDateToMonth = auditDateToMonth;
	}
	
	public UIInput getContentIdUI() {
		return contentIdUI;
	}

	public void setContentIdUI(UIInput contentIdUI) {
		this.contentIdUI = contentIdUI;
	}
	
	public UIInput getSeriesCodeUI() {
		return seriesCodeUI;
	}

	public void setSeriesCodeUI(UIInput seriesCodeUI) {
		this.seriesCodeUI = seriesCodeUI;
	}

	public UIInput getUserNameUI() {
		return userNameUI;
	}

	public void setUserNameUI(UIInput userNameUI) {
		this.userNameUI = userNameUI;
	}

	public UIInput getEisbnUI() {
		return eisbnUI;
	}

	public void setEisbnUI(UIInput eisbnUI) {
		this.eisbnUI = eisbnUI;
	}

	public UIInput getStatusUI() {
		return statusUI;
	}

	public void setStatusUI(UIInput statusUI) {
		this.statusUI = statusUI;
	}	
	
}
