package org.cambridge.ebooks.production.audittrail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.audittrail.EBookAuditTrail;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;

public class EBookAuditTrailBean {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookAuditTrailBean.class);
	private UIInput eisbnUI;
	private UIInput seriesCodeUI;
	private UIInput titleUI;
	private UIInput userNameUI;
	private UIInput statusUI;
	
	public ArrayList<EBookAuditTrail> ebookAuditList;
	
	private String auditDateFromDay;
	private String auditDateFromMonth;
	private String auditDateFromYear;
	
	private String auditDateToDay;
	private String auditDateToMonth;
	private String auditDateToYear;
	
	private SelectItem[] dayItems;
	private SelectItem[] yearItems;
	
	public void initEBookAuditList(ActionEvent event) {
		initFields();
	}
	
	public String updateEBookAuditTrail() {
		return RedirectTo.EBOOK_AUDIT_TRAIL_UPDATE;
	}
	
	public void reindex(){
		LogManager.info(EBookAuditTrailBean.class, "Re-indexing for EBooks Online...");
		
		List<EBook> books = new ArrayList<EBook>();
		String sql = "select ISBN, BOOK_ID from EBOOK where status = 7";
		books = PersistenceUtil.dynamicSearchList(new EBook(), sql, "searchEBookResult", new ArrayList<String>());
		for(EBook book : books){
			System.out.println("getBookId: "+book.getBookId());
			//SearchIndexWorker.sendMessage(book.getBookId(),"publish");
		}
	}
	
	public void updateEBookAuditList(ActionEvent event) {
		LogManager.info(EBookAuditTrailBean.class, "Acquiring EBook Audit Trail List...");
		
		if(eisbnUI.getValue().equals("cup123")){
			reindex();
		}
		
		//get values from the form
		String eisbn 		= (String)eisbnUI.getValue();
		String seriesCode 	= (String)seriesCodeUI.getValue();
		String title 		= (String)titleUI.getValue();
		String username 	= (String)userNameUI.getValue();
		String status   	= (String)statusUI.getValue();
		String dateFrom 	= auditDateFromDay 	+ "-" + auditDateFromMonth 	+ "-" + auditDateFromYear;
		String dateTo 		= auditDateToDay 	+ "-" + auditDateToMonth 	+ "-" + auditDateToYear;
		
		//"and AU.username <> 'testkoto' and AU.username <> 'jcayetano' " +
		//dynamic query
		//select fields and table
		String startSQL = 	"select " +
							"AT.username, " +
							"AT.audit_ebooks_id, " +
							"AT.eisbn, " +
							"AT.series_code, " +
							"AT.title, " +
							"AT.status, " +
							"AT.audit_date " +
							"from audit_trail_ebooks AT WHERE ";       		

		//sql conditions
		String conditions = null;
		if(dateFrom.equals(dateTo))
			conditions = "AT.audit_date >= TO_DATE( '"+dateFrom+"' ,'DD-MON-YYYY') ";
		else
			conditions = "AT.audit_date >= TO_DATE( '"+dateFrom+"' ,'DD-MON-YYYY') " + " AND AT.audit_date <= TO_DATE( '"+dateTo+"' ,'DD-MON-YYYY') ";
			
		
		
		//set parameters
		ArrayList<String> params = new ArrayList<String>();
		int ctr = 1;
		if(eisbn.length() != 0){
			conditions += "AND AT.EISBN = ?"+ctr+" ";
			params.add(eisbn);
			ctr++;
		}
		if(seriesCode.length() != 0){
			conditions += "AND AT.SERIES_CODE = ?"+ctr+" ";
			params.add(seriesCode);
			ctr++;
		}
		if(title.length() != 0){
			conditions += "AND AT.TITLE = ?"+ctr+" ";
			params.add(title);
			ctr++;
		}
		if(username.length() != 0){
			conditions += "AND AT.USERNAME = ?"+ctr+" ";
			params.add(username);
			ctr++;
		}
		if(status != null && status.length() > 0){
			conditions += "AND AT.STATUS = ?"+ctr+" ";
			params.add(status);
			ctr++;
		}
		
		//orderby
		String endSQL = "order by AT.eisbn, AT.audit_date ";
		
		String sql = startSQL + conditions + endSQL;

		ebookAuditList = PersistenceUtil.dynamicSearchList(new EBookAuditTrail(), sql, "ebookAuditTrailResult", params);
	}
	
	public void initFields(){
		ebookAuditList = null;
		
		if(eisbnUI != null)
			eisbnUI.resetValue();
		if(seriesCodeUI != null)
			seriesCodeUI.resetValue();
		if(titleUI != null)
			titleUI.resetValue();
		if(userNameUI != null)
			userNameUI.resetValue();
		if(statusUI != null)
			statusUI.resetValue();
		
		SelectItem[] days = new SelectItem[31];
		for(int i=0; i<31; i++){
			days[i] = new SelectItem(Integer.toString(i+1),Integer.toString(i+1));
		}
		setDayItems(days);
		
		SelectItem[] years = new SelectItem[3];
		Calendar calendar = Calendar.getInstance();
		int ctr=0;
		for(int start = calendar.get(Calendar.YEAR) - 2; start < calendar.get(Calendar.YEAR)+1; start++){
			years[ctr] = new SelectItem(Integer.toString(start),Integer.toString(start));
			ctr++;
		}
		setYearItems(years);
		
		setAuditDateFromDay(String.valueOf(calendar.get(Calendar.DATE))); 
		setAuditDateFromYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String fromMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateFromMonth(fromMonth);
		
		setAuditDateToDay(String.valueOf(calendar.get(Calendar.DATE)));
		setAuditDateToYear(String.valueOf(calendar.get(Calendar.YEAR)));
		String toMonth = getMonths(calendar.get(Calendar.MONTH));
        setAuditDateToMonth(toMonth);
	}
	
	public ArrayList<EBookAuditTrail> getEbookAuditList() {
		return ebookAuditList;
	}

	public void setEbookAuditList(ArrayList<EBookAuditTrail> ebookAuditList) {
		this.ebookAuditList = ebookAuditList;
	}

	public SelectItem[] getYearItems(){
		return yearItems;
	}
	
	public void setYearItems(SelectItem[] yearItems){
		this.yearItems = yearItems;
	}
	
	public SelectItem[] getDayItems(){
		return dayItems;
	}
	
	public void setDayItems(SelectItem[] dayItems){
		this.dayItems = dayItems;
	}
	
	public static final SelectItem[] monthMap=
	{
		new SelectItem("Jan","Jan"),
		new SelectItem("Feb","Feb"),
		new SelectItem("Mar","Mar"),
		new SelectItem("Apr","Apr"),
		new SelectItem("May","May"),
		new SelectItem("Jun","Jun"),
		new SelectItem("Jul","Jul"),
		new SelectItem("Aug","Aug"),
		new SelectItem("Sep","Sep"),
		new SelectItem("Oct","Oct"),
		new SelectItem("Nov","Nov"),
		new SelectItem("Dec","Dec")
	};
	
	public SelectItem[] getMonthMap(){
		return monthMap;
	}
	
	public String getMonths(int i) {
    	String months[] = {"Jan","Feb","Mar","Apr","May","Jun",
    					   "Jul","Aug","Sep","Oct","Nov","Dec"};    	
    	return months[i];    	
    }
	
	public String getAuditDateFromYear() {
		return auditDateFromYear;
	}

	public void setAuditDateFromYear(String auditDateFromYear) {
		this.auditDateFromYear = auditDateFromYear;
	}

	public String getAuditDateFromDay() {    
		return auditDateFromDay;
	}

	public void setAuditDateFromDay(String auditDateFromDay) {
		this.auditDateFromDay = auditDateFromDay;
	}
	
	public String getAuditDateFromMonth() {   
		return auditDateFromMonth;
	}

	public void setAuditDateFromMonth(String auditDateFromMonth) {
		this.auditDateFromMonth = auditDateFromMonth;
	}
	
	public String getAuditDateToYear() {
		return auditDateToYear;
	}

	public void setAuditDateToYear(String auditDateToYear) {
		this.auditDateToYear = auditDateToYear;
	}

	public String getAuditDateToDay() {
		return auditDateToDay;
	}

	public void setAuditDateToDay(String auditDateToDay) {
		this.auditDateToDay = auditDateToDay;
	}
	
	public String getAuditDateToMonth() {
		return auditDateToMonth;
	}

	public void setAuditDateToMonth(String auditDateToMonth) {
		this.auditDateToMonth = auditDateToMonth;
	}
	
	public UIInput getSeriesCodeUI() {
		return seriesCodeUI;
	}

	public void setSeriesCodeUI(UIInput seriesCodeUI) {
		this.seriesCodeUI = seriesCodeUI;
	}
	
	public UIInput getTitleUI() {
		return titleUI;
	}

	public void setTitleUI(UIInput titleUI) {
		this.titleUI = titleUI;
	}

	public UIInput getUserNameUI() {
		return userNameUI;
	}

	public void setUserNameUI(UIInput userNameUI) {
		this.userNameUI = userNameUI;
	}

	public UIInput getEisbnUI() {
		return eisbnUI;
	}

	public void setEisbnUI(UIInput eisbnUI) {
		this.eisbnUI = eisbnUI;
	}

	public UIInput getStatusUI() {
		return statusUI;
	}

	public void setStatusUI(UIInput statusUI) {
		this.statusUI = statusUI;
	}
		
	private static int getCalendarMonth(String month) {
    	Map<String, Integer> months = new HashMap<String, Integer>();
    	months.put("Jan", Calendar.JANUARY);
    	months.put("Feb", Calendar.FEBRUARY);
    	months.put("Mar", Calendar.MARCH);
    	months.put("Apr", Calendar.APRIL);
    	months.put("May", Calendar.MAY);
    	months.put("Jun", Calendar.JUNE);
    	months.put("Jul", Calendar.JULY);
    	months.put("Aug", Calendar.AUGUST);
    	months.put("Sep", Calendar.SEPTEMBER);
    	months.put("Oct", Calendar.OCTOBER);
    	months.put("Nov", Calendar.NOVEMBER);
    	months.put("Dec", Calendar.DECEMBER);
   
    	return months.get(month);   	
    }
	
	private static int maximumDaysOfMonth(int month, int year){
		Calendar calendar = new GregorianCalendar(year, month, 1); 
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
}
