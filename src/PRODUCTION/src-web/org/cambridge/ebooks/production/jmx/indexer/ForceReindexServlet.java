package org.cambridge.ebooks.production.jmx.indexer;

import java.io.IOException;
import java.io.Writer;

import javax.management.AttributeChangeNotification;
import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.jmx.util.JMXConnectorManager;
import org.cambridge.ebooks.production.util.Log4JLogger;

public class ForceReindexServlet extends HttpServlet {
	
	private static final long serialVersionUID = -5910230138565404510L;
	
	private static final Log4JLogger LogManager = new Log4JLogger(ForceReindexServlet.class);
	private static final String FORCE_REINDEX_SUCCESS = "Force Re-indexing success!";
	private static final String FORCE_REINDEX_FAIL = "Force Re-indexing fail!";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String isbn = request.getParameter("ISBN");
		if(StringUtils.isNotEmpty(isbn)){
			
			try {
				JMXConnector jmxc = JMXConnectorManager.getJMXConnector();
			
				MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
				 
				ClientListener listener = new ClientListener();
				 
				ObjectName mbeanName = new ObjectName("org.cambridge.ebooks.service.mbean:service=Indexer");
				IndexerMBean mbeanProxy = JMX.newMBeanProxy(mbsc, mbeanName, IndexerMBean.class, true);
		        
		        //Add notification listener
				mbsc.addNotificationListener(mbeanName, listener, null, null);
		        
		        LogManager.info("Invoking reindexBooks in Indexer MBean...");
		        mbeanProxy.reindexBooks(isbn, "");
		        jmxc.close();
		        
		        Writer writer = response.getWriter();
				writer.write(FORCE_REINDEX_SUCCESS);
				writer.close();
				
		        
	        
			} catch (Exception e) {
				e.printStackTrace();
				
				Writer writer = response.getWriter();
				writer.write(FORCE_REINDEX_FAIL);
				writer.close();
			}
		}
	}
	
	/**
     * Inner class that will handle the notifications.
     */
    public static class ClientListener implements NotificationListener {
        public void handleNotification(Notification notification,
                                       Object handback) {
            LogManager.info("\nReceived notification:");
            if (notification instanceof AttributeChangeNotification) {
                AttributeChangeNotification acn =
                    (AttributeChangeNotification) notification;
                LogManager.info("\tAttributeName: " + acn.getAttributeName());
                LogManager.info("\tAttributeType: " + acn.getAttributeType());
                LogManager.info("\tNewValue: " + acn.getNewValue());
                LogManager.info("\tOldValue: " + acn.getOldValue());
            }
        }
    }
	
}
