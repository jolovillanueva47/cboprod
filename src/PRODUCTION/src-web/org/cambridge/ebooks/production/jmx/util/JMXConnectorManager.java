package org.cambridge.ebooks.production.jmx.util;

import javax.management.AttributeChangeNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.cambridge.ebooks.production.util.Log4JLogger;

public class JMXConnectorManager {
	
	private static final Log4JLogger LogManager = new Log4JLogger(JMXConnectorManager.class);
	
	public final static String JMX_IP_ADDRESS = System.getProperty("jmx.ip.address");
	public final static String JMX_PORT = System.getProperty("jmx.port");
	public final static String RMI_REGISTRY_PORT = System.getProperty("rmi.registry.port");
	public final static String JNDI_PATH = "/jmxconnector";
	
	public static JMXConnector getJMXConnector() throws Exception{
		
		String jmxAddress = "service:jmx:rmi://" + JMX_IP_ADDRESS + ":" + JMX_PORT + "/";
		String jndiAddress = "jndi/rmi://" + JMX_IP_ADDRESS + ":" + RMI_REGISTRY_PORT + JNDI_PATH;
		
		// Create an RMI connector client and
        // connect it to the RMI connector server
		String serviceUrl = jmxAddress + jndiAddress;
		 JMXServiceURL url =  new JMXServiceURL(serviceUrl);
		 
		 JMXConnector jmxc = JMXConnectorFactory.connect(url,null);
		 
		 return jmxc;
	}
	
	/**
     * Inner class that will handle the notifications.
     */
    public static class ClientListener implements NotificationListener {
        public void handleNotification(Notification notification,
                                       Object handback) {
            LogManager.info("\nReceived notification:");
            if (notification instanceof AttributeChangeNotification) {
                AttributeChangeNotification acn =
                    (AttributeChangeNotification) notification;
                LogManager.info("\tAttributeName: " + acn.getAttributeName());
                LogManager.info("\tAttributeType: " + acn.getAttributeType());
                LogManager.info("\tNewValue: " + acn.getNewValue());
                LogManager.info("\tOldValue: " + acn.getOldValue());
            }
        }
    }
}
