package org.cambridge.ebooks.production.jmx.indexer;

public interface IndexerMBean {
	//-----------
    // operations
    //-----------
	
	public void reindexBooks(String isbns, String cores);
	public void replicateFromMaster(String slaveServerUrl, String cores);
	public void optimizeMasterIndex(String cores);
}