package org.cambridge.ebooks.production.document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.util.Misc;

/**
 * @author Karlson A. Mulingtapang
 * EbookDetailsBean.java - Ebook Details Beans
 */
public class EbookDetailsBeanOrig {
	private static final Log4JLogger LogManager = new Log4JLogger(EbookDetailsBeanOrig.class);
	private final EBookSearchWorker solrWorker = new EBookSearchWorker();
	
	private final static String ID = "bookId";
//	private final static String MAIN_TITLE = "MAIN_TITLE";
//	private final static String ALPHASORT = "ALPHASORT";
//	private final static String SUB_TITLE = "SUB_TITLE";
//	private final static String EDITION = "EDITION";
//	private final static String NUMBER = "NUMBER";
//	private final static String AUTHOR = "AUTHOR";
//	private final static String EDITOR = "EDITOR";
//	private final static String AUTHOR_POSITION = "AUTHOR_POSITION";
//	private final static String EDITOR_POSITION = "EDITOR_POSITION";
//	private final static String AFFILIATION = "AFFILIATION";
//	private final static String DOI = "DOI";
//	private final static String ISBN = "ISBN";
//	private final static String ALT_ISBN = "ALT_ISBN";
//	private final static String TYPE = "TYPE";
//	private final static String VOLUME = "VOLUME";
//	private final static String OTHER_VOLUME = "OTHER_VOLUME";
//	private final static String SERIES = "SERIES";
//	private final static String SERIES_POSITION = "POSITION";
//	private final static String PUBLISHER = "PUBLISHER";
//	private final static String PUBLISHER_LOC = "PUBLISHER_LOC";
//	private final static String PRINT_DATE = "PRINT_DATE";
//	private final static String ONLINE_DATE = "ONLINE_DATE";
//	private final static String COPYRIGHT_STATEMENT = "COPYRIGHT_STATEMENT";
//	private final static String SUBJECT = "SUBJECT";
//	private final static String BLURB = "BLURB";
//	private final static String PAGES = "PAGES";
//	private final static String COVER_IMAGE = "FILENAME";
//	private final static String CODE = "CODE";
	private final static String HTML_BREAK = "<br />";
//	private final static String BOOK_GROUP = "GROUP";
//	private final static String ROLE = "ROLE";
//	private final static String TRANS_TITLE = "TRANS_TITLE";
//	private final static String PART = "PART";
		
	private final static String IMAGE_DIR_PATH = System.getProperty("content.url").trim();
	private final static String NO_COVER_IMAGE = System.getProperty("no.cover.standard.dir").trim();
	private final static String NO_COVER_THUMBNAIL = System.getProperty("no.cover.thumbnail.dir").trim(); 

	private String bookId;
	private String mainTitle;
	private String subTitle;
	private String edition;
	private String editionNumber;
	private String seriesNumber;
	private String seriesPosition;
	private String author;
	private String editor;
	private String affiliation;
	private String doi;
	private String eisbn;
	private String paperback;
	private String hardback;
	private String other;
	private String volume;
	private String otherVolume;
	private String series;
	private String publisherName;
	private String publisherLoc;
	private String printDate;
	private String onlineDate;
	private String copyrightStatement;
	private String subject;
	private String blurb;
	private String pages;
	private String standardImage;
	private String thumbImage;
	private String bookGroup;
	private String mainTitleAlphaSort;
	private String seriesAlphaSort;	
	private String seriesPart;
	
	private TreeMap<String, String> authorMap;
	private TreeMap<String, String> editorMap;
	
	private ArrayList<RoleAffBean> authorAffList;
	private ArrayList<RoleAffBean> editorAffList;
	
	private boolean displayEdition = false;
	private boolean displaySubtitle = false;
	private boolean displayAuthor = false;
	private boolean displayEditor = false;
	private boolean displayAffiliation = false;
	private boolean displayAltIsbn = false;
	private boolean displayAltIsbnType = false;
	private boolean displayPaperback = false;
	private boolean displayHardback = false;
	private boolean displayOther = false;
	private boolean displayVolume = false;
	private boolean displayOtherVolume = false;
	private boolean displaySeries = false;
	private boolean displayStandardImage = false;
	private boolean displayThumbImage = false;
	private boolean displayTransTitle = false;	
	private boolean displayPart = false;
	
	public EbookDetailsBeanOrig() {}
	public EbookDetailsBeanOrig(String bookId) {
		this.bookId = bookId;
		try {			
			//populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryString()));
			populateBean(solrWorker.searchBookCore(getQueryString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String show() {			
		resetFields();
		resetFlags();
		
		try 
		{  
			//populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryString()));
			populateBean(solrWorker.searchBookCore(getQueryString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return RedirectTo.EBOOK_DETAILS;
	}
	
	public void showListener(ActionEvent event) {
		setBookId(""); // clear
		
		FacesContext context = FacesContext.getCurrentInstance();  
		Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap(); 
		String bookId = (String)(event.getComponent().getAttributes().get(ID) != null ?  event.getComponent().getAttributes().get(ID) : requestMap.get(ID));
		
		setBookId(bookId);
	}
	
	private String getQueryString() {		
		StringBuffer queryString = new StringBuffer("book_id:" + getBookId());			
		//queryString.append(" AND (PARENT:metadata OR PARENT:subject-group OR PARENT:pub-dates OR ELEMENT:book)");
		return queryString.toString();
	}
	
//	private void populateBean(List<Document> docs) throws IOException {
//		
//		StringBuffer author = new StringBuffer();
//		StringBuffer editor = new StringBuffer();
//		StringBuffer affiliation = new StringBuffer();
//		StringBuffer copyrightStatement = new StringBuffer();
//		StringBuffer subject = new StringBuffer();
//		
//		authorMap = new TreeMap<String, String>();
//		editorMap = new TreeMap<String, String>();
//		authorAffList = new ArrayList<RoleAffBean>();
//		editorAffList = new ArrayList<RoleAffBean>();
//		
//		for(Document document : docs) {
//			
//			if(!Misc.isEmpty(document.get(MAIN_TITLE))) {
//				setMainTitle(document.get(MAIN_TITLE));
//				setMainTitleAlphaSort(document.get(ALPHASORT));
//			}
//			
//			if(!Misc.isEmpty(document.get(SUB_TITLE))) {
//				if(!isDisplaySubtitle()) {
//					setDisplaySubtitle(true);
//				}				
//				setSubTitle(document.get(SUB_TITLE));
//			}
//			
//			if(!Misc.isEmpty(document.get(EDITION))) {
//				if(!isDisplayEdition()) {
//					setDisplayEdition(true);
//				}
//				setEdition(document.get(EDITION));
//				setEditionNumber(document.get(NUMBER));
//			}
//			
//			//Karl's
//			/*
//			if(!Misc.isEmpty(document.get(AUTHOR))){
//				if(!isDisplayAuthor()) {
//					setDisplayAuthor(true);
//				}
//				author.append(document.get(AUTHOR));				
//				author.append(HTML_BREAK);
//				
//				if(!Misc.isEmpty(document.get(AFFILIATION))) {
//					if(!isDisplayAffiliation()) {
//						setDisplayAffiliation(true);
//					}
//					affiliation.append(document.get(AFFILIATION));
//					affiliation.append(HTML_BREAK);
//				}
//				
//				authorMap.put(document.get(AUTHOR_POSITION), document.get(AUTHOR));
//			} 			
//			
//			if(!Misc.isEmpty(document.get(EDITOR))) {
//				if(!isDisplayEditor()) {
//					setDisplayEditor(true);
//				}
//				
//				editor.append(document.get(EDITOR));
//				editor.append(HTML_BREAK);
//				
//				if(!Misc.isEmpty(document.get(AFFILIATION))) {
//					if(!isDisplayAffiliation()) {
//						setDisplayAffiliation(true);
//					}
//					affiliation.append(document.get(AFFILIATION));
//					affiliation.append(HTML_BREAK);
//				}
//				
//				editorMap.put(document.get(EDITOR_POSITION), document.get(EDITOR));
//			} 
//			*/
//			
//			//modified for displaying new author/editor format
//			if(!Misc.isEmpty(document.get(AUTHOR))){
//				if(!isDisplayAuthor()) {
//					setDisplayAuthor(true);
//				}
//				
//				RoleAffBean authorBean = new RoleAffBean();
//				authorBean.setAuthor(document.get(AUTHOR));
//				
//				authorBean.setRole(document.get(ROLE));
//				
//				if(!Misc.isEmpty(document.get(AFFILIATION))) {
//					if(!isDisplayAffiliation()) {
//						setDisplayAffiliation(true);
//					}
//					authorBean.setAffiliation(document.get(AFFILIATION));
//				}
//				
//				authorAffList.add(authorBean);				
//				authorMap.put(document.get(AUTHOR_POSITION), document.get(AUTHOR));				
//			} 			
//			
//			if(!Misc.isEmpty(document.get(EDITOR))) {
//				if(!isDisplayEditor()) {
//					setDisplayEditor(true);
//				}
//				
//				editor.append(document.get(EDITOR));
//				editor.append(HTML_BREAK);
//				
//				if(!Misc.isEmpty(document.get(AFFILIATION))) {
//					if(!isDisplayAffiliation()) {
//						setDisplayAffiliation(true);
//					}
//					affiliation.append(document.get(AFFILIATION));
//					affiliation.append(HTML_BREAK);
//				}
//				
//				editorMap.put(document.get(EDITOR_POSITION), document.get(EDITOR));
//			}
//			
//			
//			if(!Misc.isEmpty(document.get(DOI))) {
//				setDoi(document.get(DOI));
//			}
//			
//			if(!Misc.isEmpty(document.get(ISBN))) {
//				setEisbn(document.get(ISBN));
//			}			
//			
//			if(!Misc.isEmpty(document.get(ALT_ISBN))) {
//				if(!isDisplayAltIsbn()) {
//					setDisplayAltIsbn(true);
//				}
//				
//				if(!Misc.isEmpty(document.get(TYPE))) {
//					if(!isDisplayAltIsbnType()) {
//						setDisplayAltIsbnType(true);
//					}
//					
//					if(document.get(TYPE).equals("paperback")) {
//						if(!isDisplayPaperback()) {
//							setDisplayPaperback(true);
//						}
//						setPaperback(document.get(ALT_ISBN));
//					}
//					
//					if(document.get(TYPE).equals("hardback")) {
//						if(!isDisplayHardback()) {
//							setDisplayHardback(true);
//						}						
//						setHardback(document.get(ALT_ISBN));
//					}
//					
//					if(document.get(TYPE).equals("other")) {
//						if(!isDisplayOther()) {
//							setDisplayOther(true);
//						}						
//						setOther(document.get(ALT_ISBN));
//					}					
//				}
//			}
//			
//			if(!Misc.isEmpty(document.get(VOLUME))) {
//				if(!isDisplayVolume()) {
//					setDisplayVolume(true);
//				}
//				setVolume(document.get(VOLUME));
//			}
//			
//			if(!Misc.isEmpty(document.get(OTHER_VOLUME))) {
//				if(!isDisplayOtherVolume()) {
//					setDisplayOtherVolume(true);
//				}
//				setOtherVolume(document.get(OTHER_VOLUME));
//			}
//			
//			if(!Misc.isEmpty(document.get(SERIES))) {
//				if(!isDisplaySeries()) {
//					setDisplaySeries(true);
//				}
//				setSeries(document.get(SERIES));
//				setSeriesNumber(document.get(NUMBER));
//				setSeriesPosition(document.get(SERIES_POSITION));
//			}
//			
//			if(!Misc.isEmpty(document.get(PUBLISHER))) {
//				setPublisherName(document.get(PUBLISHER));
//			}
//			
//			if(!Misc.isEmpty(document.get(PUBLISHER_LOC))) {
//				setPublisherLoc(document.get(PUBLISHER_LOC));
//			}
//			
//			if(!Misc.isEmpty(document.get(PRINT_DATE))) {
//				setPrintDate(document.get(PRINT_DATE));
//			}
//			
//			if(!Misc.isEmpty(document.get(ONLINE_DATE))) {
//				setOnlineDate(document.get(ONLINE_DATE));
//			}
//			
//			if(!Misc.isEmpty(document.get(COPYRIGHT_STATEMENT))) {
//				copyrightStatement.append(document.get(COPYRIGHT_STATEMENT));
//				copyrightStatement.append(HTML_BREAK);
//			}
//			
//			if(!Misc.isEmpty(document.get(SUBJECT))) {
//				if (!Misc.isEmpty(document.get(CODE))) {
//					subject.append(document.get(CODE) + ", "+ document.get(SUBJECT));
//					subject.append(HTML_BREAK);
//				} else {
//					subject.append(document.get(SUBJECT));
//					subject.append(HTML_BREAK);	
//				}
//			}
//			
//			if(!Misc.isEmpty(document.get(BLURB))) {				
//				setBlurb(document.get(BLURB));
//			}
//			
//			if(!Misc.isEmpty(document.get(BOOK_GROUP))) {				
//				setBookGroup(document.get(BOOK_GROUP));
//			}
//			
//			if(!Misc.isEmpty(document.get(PAGES))) {
//				setPages(document.get(PAGES));					
//			}
//			
//			if(!Misc.isEmpty(document.get(COVER_IMAGE))) {
//				if(document.get(COVER_IMAGE).indexOf(getEisbn() + "t") > -1) {
//					if(!isDisplayThumbImage()) {
//						setDisplayThumbImage(true);
//					}					
//					setThumbImage(document.get(COVER_IMAGE));
//				} else {
//					if(!isDisplayStandardImage()) {
//						setDisplayStandardImage(true);
//					}
//					setStandardImage(document.get(COVER_IMAGE));
//				}				
//			}
//			
//			
//		}
//		
//		setAuthor(author.toString());
//		setEditor(editor.toString());
//		setAffiliation(affiliation.toString());
//		setCopyrightStatement(copyrightStatement.toString());
//		setSubject(subject.toString());
//	}
	
	
	private void populateBean(List<BookMetadataDocument> docs) throws IOException {
		
//		StringBuffer author = new StringBuffer();
//		StringBuffer editor = new StringBuffer();
//		StringBuffer affiliation = new StringBuffer();
		StringBuffer copyrightStatement = new StringBuffer();
		StringBuffer subject = new StringBuffer();
		
		authorMap = new TreeMap<String, String>();
		editorMap = new TreeMap<String, String>();
		authorAffList = new ArrayList<RoleAffBean>();
		editorAffList = new ArrayList<RoleAffBean>();
		
		for(BookMetadataDocument document : docs) 
		{
			
			if(!Misc.isEmpty(document.getTitle())) 
			{
				setMainTitle(document.getTitle());
				setMainTitleAlphaSort(document.getTitleAlphasort());
			}
			
			if(!Misc.isEmpty(document.getSubtitle())) 
			{
				if(!isDisplaySubtitle()) 
					setDisplaySubtitle(true);
							
				setSubTitle(document.getSubtitle());
			}
			
			if(!Misc.isEmpty(document.getEdition())) 
			{
				if(!isDisplayEdition()) 
					setDisplayEdition(true);
				
				setEdition(document.getEdition());
				setEditionNumber(document.getEditionNumber());
			}
			
			
			List<String> authorNames = document.getAuthorNameList();
			List<String> authorAffiliations = document.getAuthorAffiliationList();
			List<String> authorPositions = document.getAuthorPositionList();
			List<String> authorRoles = document.getAuthorRoleList();
			if(authorNames != null && !authorNames.isEmpty())
			{
				for(int index=0; index<authorNames.size(); index++)
				{
					if(!isDisplayAuthor()) 
						setDisplayAuthor(true);
					
					RoleAffBean authorBean = new RoleAffBean();
					authorBean.setAuthor(authorNames.get(index));
					
					authorBean.setRole(authorRoles.get(index));
					
					if(authorAffiliations != null && !authorAffiliations.isEmpty()) 
					{
						if(!isDisplayAffiliation()) 
							setDisplayAffiliation(true);
						
						authorBean.setAffiliation(authorAffiliations.get(index));
					}
					
					authorAffList.add(authorBean);				
					authorMap.put(authorPositions.get(index), authorNames.get(index));	
				}
			} 			

			
			
			if(!Misc.isEmpty(document.getDoi())) 
				setDoi(document.getDoi());
			
			
			if(!Misc.isEmpty(document.getIsbn()))
				setEisbn(document.getIsbn());
						
			
			String eisbn = document.getAltIsbnEisbn();
			String hardBack = document.getAltIsbnHardback();
			String other = document.getAltIsbnOther();
			String paperBack = document.getAltIsbnPaperback();
			
			if(Misc.isNotEmpty(eisbn) || Misc.isNotEmpty(hardBack) || Misc.isNotEmpty(other) || Misc.isNotEmpty(paperBack))
				setDisplayAltIsbnType(true);
			
					
			if(Misc.isNotEmpty(paperBack)) 
			{
				if(!isDisplayPaperback()) 
					setDisplayPaperback(true);
						
				setPaperback(paperBack);
			}
					
			if(Misc.isNotEmpty(hardBack)) 
			{
				if(!isDisplayHardback()) 
					setDisplayHardback(true);
												
				setHardback(hardBack);
			}
					
			if(Misc.isNotEmpty(other)) 
			{
				if(!isDisplayOther()) 
					setDisplayOther(true);
										
				setOther(other);
			}
			
			
			if(!Misc.isEmpty(document.getVolumeNumber())) {
				if(!isDisplayVolume()) 
					setDisplayVolume(true);
				
				setVolume(document.getVolumeNumber());
			}
			

			if(!Misc.isEmpty(document.getSeries())) {
				if(!isDisplaySeries()) 
					setDisplaySeries(true);
				
				setSeries(document.getSeries());
				setSeriesNumber(document.getSeriesNumber());
				setSeriesPosition(document.getSeriesPosition());
			}
			
			if(!Misc.isEmpty(document.getPublisherName())) 
				setPublisherName(document.getPublisherName());
			
			if(!Misc.isEmpty(document.getPublisherLoc())) 
				setPublisherLoc(document.getPublisherLoc());
			
			if(!Misc.isEmpty(document.getPrintDate())) 
				setPrintDate(document.getPrintDate());
			
			if(!Misc.isEmpty(document.getOnlineDate())) 
				setOnlineDate(document.getOnlineDate());
			
			List<String> copyrights = document.getCopyrightStatementList();
			if(copyrights != null && !copyrights.isEmpty()) 
			{
				for(String copyright : copyrights)
				{
					copyrightStatement.append(copyright);
					copyrightStatement.append(HTML_BREAK);
				}
			}
			
			List<String> subjects = document.getSubjectList();
			List<String> subjectCodes = document.getSubjectCodeList();
			if(subjects != null && !subjects.isEmpty())
			{
				for(int index=0; index<subjects.size(); index++)
				{
					subject.append(subjectCodes.get(index) + ", "+ subjects.get(index));
					subject.append(HTML_BREAK);
					
					subject.append(subjects.get(index));
					subject.append(HTML_BREAK);	
				}
			}

			String blurb = getBlurb(document.getIsbn());
			if(!Misc.isEmpty(blurb))				
				setBlurb(blurb);
			
			if(!Misc.isEmpty(document.getBookGroup())) {				
				setBookGroup(document.getBookGroup());
			}
			
			if(!Misc.isEmpty(document.getPages())) 
				setPages(document.getPages());					
			
			List<String> images = document.getCoverImageFilenameList();
			List<String> types = document.getCoverImageTypeList();
			if(images != null && types != null)
			{
				for(int index=0; index<images.size(); index++)
				{
					if("standard".equals(types.get(index)))
					{
						setDisplayStandardImage(true);
						setStandardImage(images.get(index));
					}
					else
					{
						setDisplayThumbImage(true);
						setThumbImage(images.get(index));
					}
				}
			}
				
		}
			
		setCopyrightStatement(copyrightStatement.toString());
		setSubject(subject.toString());
	}
	
	private void resetFlags() {
		setDisplayAltIsbn(false);
		setDisplayAltIsbnType(false);
		setDisplayPaperback(false);
		setDisplayHardback(false);
		setDisplayOther(false);
		setDisplayAffiliation(false);
		setDisplayAuthor(false);
		setDisplayEditor(false);
		setDisplayOtherVolume(false);
		setDisplaySeries(false);
		setDisplaySubtitle(false);
		setDisplayVolume(false);
		setDisplayStandardImage(false);
		setDisplayThumbImage(false);
		setDisplayTransTitle(false);
		setDisplayPart(false);
	}
	
	private void resetFields() {
		setMainTitle("");
		setSubTitle("");
		setEdition("");
		setAuthor("");
		setEditor("");
		setAffiliation("");
		setDoi("");
		setPaperback("");
		setHardback("");
		setEisbn("");
		setOther("");
		setVolume("");
		setOtherVolume("");
		setSeries("");
		setPublisherName("");
		setPublisherLoc("");
		setPrintDate("");
		setOnlineDate("");
		setCopyrightStatement("");
		setSubject("");
		setBlurb("");
		setPages("");
		setStandardImage("");
		setThumbImage("");
		setBookGroup("");
		setSeriesNumber("");
		setSeriesPosition("");
		
	}

	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the mainTitle
	 */
	public String getMainTitle() {
		return mainTitle;
	}

	/**
	 * @param mainTitle the mainTitle to set
	 */
	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	/**
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * @param subTitle the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * @return the edition
	 */
	public String getEdition() {
		return edition;
	}

	/**
	 * @param edition the edition to set
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the editor
	 */
	public String getEditor() {
		return editor;
	}

	/**
	 * @param editor the editor to set
	 */
	public void setEditor(String editor) {
		this.editor = editor;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the volume
	 */
	public String getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}

	/**
	 * @return the otherVolume
	 */
	public String getOtherVolume() {
		return otherVolume;
	}

	/**
	 * @param otherVolume the otherVolume to set
	 */
	public void setOtherVolume(String otherVolume) {
		this.otherVolume = otherVolume;
	}

	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}

	/**
	 * @param series the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}

	/**
	 * @return the publisherName
	 */
	public String getPublisherName() {
		return publisherName;
	}

	/**
	 * @param publisherName the publisherName to set
	 */
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	/**
	 * @return the publisherLoc
	 */
	public String getPublisherLoc() {
		return publisherLoc;
	}

	/**
	 * @param publisherLoc the publisherLoc to set
	 */
	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}

	/**
	 * @return the printDate
	 */
	public String getPrintDate() {
		return printDate;
	}

	/**
	 * @param printDate the printDate to set
	 */
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	/**
	 * @return the copyrightStatement
	 */
	public String getCopyrightStatement() {
		return copyrightStatement;
	}

	/**
	 * @param copyrightStatement the copyrightStatement to set
	 */
	public void setCopyrightStatement(String copyrightStatement) {
		this.copyrightStatement = copyrightStatement;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the blurb
	 */
	public String getBlurb() {
		return blurb;
	}

	/**
	 * @param blurb the blurb to set
	 */
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}

	/**
	 * @return the bookGroup
	 */
	public String getBookGroup() {
		return bookGroup;
	}

	/**
	 * @param bookGroup the bookGroup to set
	 */
	public void setBookGroup(String bookGroup) {		
		this.bookGroup = bookGroup;//.substring(0,1).toUpperCase()
				//+ bookGroup.substring(1);
	}
	
	/**
	 * @return the pages
	 */
	public String getPages() {
		return pages;
	}

	/**
	 * @param pages the pages to set
	 */
	public void setPages(String pages) {
		this.pages = pages;
	}

	/**
	 * @return the displayAuthor
	 */
	public boolean isDisplayAuthor() {
		return displayAuthor;
	}

	/**
	 * @param displayAuthor the displayAuthor to set
	 */
	public void setDisplayAuthor(boolean displayAuthor) {
		this.displayAuthor = displayAuthor;
	}

	/**
	 * @return the displayEditor
	 */
	public boolean isDisplayEditor() {
		return displayEditor;
	}

	/**
	 * @param displayEditor the displayEditor to set
	 */
	public void setDisplayEditor(boolean displayEditor) {
		this.displayEditor = displayEditor;
	}

	/**
	 * @return the onlineDate
	 */
	public String getOnlineDate() {
		return onlineDate;
	}

	/**
	 * @param onlineDate the onlineDate to set
	 */
	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	/**
	 * @return the displaySubtitle
	 */
	public boolean isDisplaySubtitle() {
		return displaySubtitle;
	}

	/**
	 * @param displaySubtitle the displaySubtitle to set
	 */
	public void setDisplaySubtitle(boolean displaySubtitle) {
		this.displaySubtitle = displaySubtitle;
	}

	/**
	 * @return the displayAltIsbn
	 */
	public boolean isDisplayAltIsbn() {
		return displayAltIsbn;
	}

	/**
	 * @param displayAltIsbn the displayAltIsbn to set
	 */
	public void setDisplayAltIsbn(boolean displayAltIsbn) {
		this.displayAltIsbn = displayAltIsbn;
	}

	/**
	 * @return the displayVolume
	 */
	public boolean isDisplayVolume() {
		return displayVolume;
	}

	/**
	 * @param displayVolume the displayVolume to set
	 */
	public void setDisplayVolume(boolean displayVolume) {
		this.displayVolume = displayVolume;
	}

	/**
	 * @return the displayOtherVolume
	 */
	public boolean isDisplayOtherVolume() {
		return displayOtherVolume;
	}

	/**
	 * @param displayOtherVolume the displayOtherVolume to set
	 */
	public void setDisplayOtherVolume(boolean displayOtherVolume) {
		this.displayOtherVolume = displayOtherVolume;
	}

	/**
	 * @return the displaySeries
	 */
	public boolean isDisplaySeries() {
		return displaySeries;
	}

	/**
	 * @param displaySeries the displaySeries to set
	 */
	public void setDisplaySeries(boolean displaySeries) {
		this.displaySeries = displaySeries;
	}

	/**
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * @return the displayAffiliation
	 */
	public boolean isDisplayAffiliation() {
		return displayAffiliation;
	}

	/**
	 * @param displayAffiliation the displayAffiliation to set
	 */
	public void setDisplayAffiliation(boolean displayAffiliation) {
		this.displayAffiliation = displayAffiliation;
	}

	/**
	 * @return the displayAltIsbnType
	 */
	public boolean isDisplayAltIsbnType() {
		return displayAltIsbnType;
	}

	/**
	 * @param displayAltIsbnType the displayAltIsbnType to set
	 */
	public void setDisplayAltIsbnType(boolean displayAltIsbnType) {
		this.displayAltIsbnType = displayAltIsbnType;
	}

	/**
	 * @return the paperback
	 */
	public String getPaperback() {
		return paperback;
	}

	/**
	 * @param paperback the paperback to set
	 */
	public void setPaperback(String paperback) {
		this.paperback = paperback;
	}

	/**
	 * @return the hardback
	 */
	public String getHardback() {
		return hardback;
	}

	/**
	 * @param hardback the hardback to set
	 */
	public void setHardback(String hardback) {
		this.hardback = hardback;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * @return the displayPaperback
	 */
	public boolean isDisplayPaperback() {
		return displayPaperback;
	}

	/**
	 * @param displayPaperback the displayPaperback to set
	 */
	public void setDisplayPaperback(boolean displayPaperback) {
		this.displayPaperback = displayPaperback;
	}

	/**
	 * @return the displayHardback
	 */
	public boolean isDisplayHardback() {
		return displayHardback;
	}

	/**
	 * @param displayHardback the displayHardback to set
	 */
	public void setDisplayHardback(boolean displayHardback) {
		this.displayHardback = displayHardback;
	}

	/**
	 * @return the displayOther
	 */
	public boolean isDisplayOther() {
		return displayOther;
	}

	/**
	 * @param displayOther the displayOther to set
	 */
	public void setDisplayOther(boolean displayOther) {
		this.displayOther = displayOther;
	}	
	
	/**
	 * @return the eisbn
	 */
	public String getEisbn() {
		return eisbn;
	}

	/**
	 * @param eisbn the eisbn to set
	 */
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}	
	
	/**
	 * @return the standardImage
	 */
	public String getStandardImage() {
		return standardImage;
	}

	/**
	 * @param standardImage the standardImage to set
	 */
	public void setStandardImage(String standardImage) {		
		this.standardImage = setImagePath(standardImage, NO_COVER_IMAGE);
	}

	/**
	 * @return the thumbImage
	 */
	public String getThumbImage() {
		return thumbImage;
	}

	/**
	 * @param thumbImage the thumbImage to set
	 */
	public void setThumbImage(String thumbImage) {		
		this.thumbImage = setImagePath(thumbImage, NO_COVER_THUMBNAIL);
	}
	
	/**
	 * @return the displayStandardImage
	 */
	public boolean isDisplayStandardImage() {
		return displayStandardImage;
	}

	/**
	 * @param displayStandardImage the displayStandardImage to set
	 */
	public void setDisplayStandardImage(boolean displayStandardImage) {
		this.displayStandardImage = displayStandardImage;
	}

	/**
	 * @return the displayThumbImage
	 */
	public boolean isDisplayThumbImage() {
		return displayThumbImage;
	}

	/**
	 * @param displayThumbImage the displayThumbImage to set
	 */
	public void setDisplayThumbImage(boolean displayThumbImage) {
		this.displayThumbImage = displayThumbImage;
	}

	/**
	 * @return the displayEdition
	 */
	public boolean isDisplayEdition() {
		return displayEdition;
	}

	/**
	 * @param displayEdition the displayEdition to set
	 */
	public void setDisplayEdition(boolean displayEdition) {
		this.displayEdition = displayEdition;
	}
	
	public TreeMap<String, String> getAuthorMap() {
		return authorMap;
	}
	
	public TreeMap<String, String> getEditorMap() {
		return editorMap;
	}
	
	public String getEditionNumber() {
		return editionNumber;
	}
	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}
	
	public String getSeriesNumber() {
		return seriesNumber;
	}
	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
	
	public String getSeriesPosition() {
		return seriesPosition;
	}
	public void setSeriesPosition(String seriesPosition) {
		this.seriesPosition = seriesPosition;
	}
	
	/**
	 * validates if the image exists, replaces with a default 
	 * no cover when it does not exists 
	 * @param image
	 * @return
	 */
	public String setImagePath(String image, String noImageFile) {
//		String imagePath = CONTENT_DIR + getEisbn() + "/" +	image;
		String imagePath = IsbnContentDirUtil.getPath(getEisbn()) + image;
		File file = new File(imagePath);		
		LogManager.info(this.getClass(), "Finding file in : " + imagePath);
		LogManager.info(this.getClass(), "Can I read the file? : " + file.canRead());
		if(file.canRead()){
			return IMAGE_DIR_PATH + getEisbn() + "/" +
					image;
		}else{
			return noImageFile;
		}
		
	}
	/**
	 * @return the mainTitleAlphaSort
	 */
	public String getMainTitleAlphaSort() {
		return mainTitleAlphaSort;
	}
	
	/**
	 * @param mainTitleAlphaSort the mainTitleAlphaSort to set
	 */
	public void setMainTitleAlphaSort(String mainTitleAlphaSort) {
		this.mainTitleAlphaSort = mainTitleAlphaSort;
	}	
	
	/**	 
	 * @return AuthorAffList
	 */
	public ArrayList<RoleAffBean> getAuthorAffList() {
		return authorAffList;
	}
	public void setAuthorAffList(ArrayList<RoleAffBean> authorAffList) {
		this.authorAffList = authorAffList;
	}
	public ArrayList<RoleAffBean> getEditorAffList() {
		return editorAffList;
	}
	public void setEditorAffList(ArrayList<RoleAffBean> editorAffList) {
		this.editorAffList = editorAffList;
	}
	public String getSeriesAlphaSort() {
		return seriesAlphaSort;
	}
	public void setSeriesAlphaSort(String seriesAlphaSort) {
		this.seriesAlphaSort = seriesAlphaSort;
	}
	public String getSeriesPart() {
		return seriesPart;
	}
	public void setSeriesPart(String seriesPart) {
		this.seriesPart = seriesPart;
	}
	public boolean isDisplayTransTitle() {
		return displayTransTitle;
	}
	public void setDisplayTransTitle(boolean displayTransTitle) {
		this.displayTransTitle = displayTransTitle;
	}
	public boolean isDisplayPart() {
		return displayPart;
	}
	public void setDisplayPart(boolean displayPart) {
		this.displayPart = displayPart;
	}
	
	
	private String getBlurb(String isbn){
		String result = "";
		final File blurbDir = new File(IsbnContentDirUtil.getPath(isbn) + isbn + "_blurb.txt" );
		
		try
		{
			StringBuilder sb = new StringBuilder();
			if(blurbDir != null && blurbDir.exists())
			{
				String temp = null;
				FileReader reader = new FileReader(blurbDir);
				BufferedReader breader = new BufferedReader(reader);
				while((temp = breader.readLine()) != null)
					sb.append(temp);
		
				result = sb.toString();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return result;
	}
}
