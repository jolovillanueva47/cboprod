package org.cambridge.ebooks.production.document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.event.ActionEvent;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.ebook.content.EBookInsertBean;
import org.cambridge.ebooks.production.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.solr.bean.KeywordsContentItemDocument;
import org.cambridge.ebooks.production.solr.bean.WordsContentItemDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.ImageUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.StringUtil;
import org.cambridge.util.Misc;

/**
 * @author Karlson A. Mulingtapang EbookChapterDetailsBean.java - Ebook Chapter
 *         Details bean
 *         
 * REVISIONS:
 * 20130624 jmendiola - adding keywords from core to the code
 * 20131205 jmendiola - removed because stahl 2 content it didn't push through
 * 20131207 alacerna - added words from core to the code
 */
public class EbookChapterDetailsBean {
	
//	private static final Log4JLogger LogManager = new Log4JLogger(EbookChapterDetailsBean.class);
	
	private final EBookSearchWorker solrWorker = new EBookSearchWorker();
	
	private final static String EBOOK_CONTENT_BEAN_ATTR = "ebookContentBean";
	private final static String PROBLEM_NONE = "none";
	private final static String PROBLEM_DISPLAY = "display";
	private final static String PROBLEM_UNICODE = "unicode";
	private final static String PROBLEM_NOTE_NONE = " (HTML abstract will be displayed)";
	private final static String PROBLEM_NOTE_DISPLAY = " (JPEG abstract will be displayed)";
	private final static String PROBLEM_NOTE_UNICODE = " (JPEG abstract will be displayed)";
	private final static String BR = "<br[\\s]*[/]?>";
	private final static String HTML_BREAK = "<br />";
	
	private static final String CONTENT_DIR = System.getProperty("content.dir").trim();
	
	private final static int KEYWORD_DISP_LENGTH = 5;

	private String contentId;
	private String contentType;
	private String pdfFilename;
	private String doi;
	private String headingLabel;
	private String headingTitle;
	private String headingSubtitle;
	private String contributorName;
	private String contributorCollab;
	private String contributorAffiliation;
	private String contributorPosition;
	private String subjectGroup;
	private String themeGroup;
	private String keyword;
	private String theme;
	private String toc;
	private String abstractContent;
	private String object;
	private String contentItem;
	private String references;
	private String abstractImage;
	private String pageEnd;
	private String pageStart;
	private String problem;
	private String titleAlphasort;
	
	private String isbnBreakdown;
	
	private List<String> citations;
	// title with part or chapter
	private String title;
	private String contentTitle;
	private EBookContentBean ebookContentBean;
	
	private List<KeywordsContentItemDocument> keywordsFromCore;
	private List<WordsContentItemDocument> wordsFromCore; //20131206 arvin
	
	// PID 81717
	private List<EBookInsertBean> ebookInsertBeansList;
	private boolean displayEBookInsertBeansList;

	private boolean displayPdfFilename;
	private boolean displayHeadingLabel;
	private boolean displayHeadingSubtitle;
	private boolean displayContributorName;
	private boolean displayContributorCollab;
	private boolean displayContributorAffiliation;
	private boolean displaySubjectGroup;
	private boolean displayThemeGroup;
	private boolean displayKeyword;
	private boolean displayTheme;
	private boolean displayKeywordsFromCore; //20130624 jubs
	private boolean displayWordsFromCore; //20131206 arvin
	private boolean displayToc;
	private boolean displayAbstractContent;
	private boolean displayObject;
	private boolean displayContentItem;
	private boolean displayReferences;
	private boolean displayProblem;
	private boolean displayCitations;
	private boolean displayPages;
	private boolean displayDOI;
	private boolean displayIndented;
	private boolean displayAlphasort;
	
	public EbookChapterDetailsBean() {
	}
	
	/**
	 * resets all bean properties by creating a new bean 
	 * and copying all the properties; thereby resetting all values 
	 */
	private void resetBeanPropAll(){
		EbookChapterDetailsBean bean = new EbookChapterDetailsBean();
		bean.setContentId(getContentId());
		bean.setContentType(getContentType());
		bean.setContentTitle(getContentTitle());
		try {
			BeanUtils.copyProperties(this, bean);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		setCitations(new ArrayList<String>());
	}

	//TODO:SOLR-ify this
	public String show() {
		//resetFields();
		//resetFlags();
		resetBeanPropAll();

		try {	
			
				populate(solrWorker.searchContentCore(getQueryStringContent()));
//				if(getContentType().equals("part-title")) 
//				{
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContent()));
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringAbstract()));
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringForPartTitleContributor()));
//				} 
//				else 
//				{
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContent()));
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContentDetails()));
//					populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringTocItems()));
//				
//				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return RedirectTo.EBOOK_CHAPTER_DETAILS;
	}

	public void showListener(ActionEvent event) {
		setContentId(""); // clear
		setContentType(""); // clear
		setContentTitle(""); // clear
		setEbookContentBean(null); // clear
		
		setEbookContentBean((EBookContentBean)event.getComponent().getAttributes()
				.get(EBOOK_CONTENT_BEAN_ATTR));
		setContentId(getEbookContentBean().getContentId());
		setContentType(getEbookContentBean().getType());
		setContentTitle(getEbookContentBean().getTitle());
	}

	
	@SuppressWarnings("unused")
	private void resetFlags() {
		setDisplayPdfFilename(false);
		setDisplayHeadingLabel(false);
		setDisplayHeadingSubtitle(false);
		setDisplayContributorName(false);
		setDisplayContributorAffiliation(false);
		setDisplaySubjectGroup(false);
		setDisplayThemeGroup(false);
		setDisplayKeyword(false);
		setDisplayTheme(false);
		setDisplayToc(false);
		setDisplayAbstractContent(false);
		setDisplayObject(false);
		setDisplayContentItem(false);
		setDisplayReferences(false);
		setDisplayProblem(false);
		setDisplayCitations(false);
		setDisplayPages(false);
		setDisplayAlphasort(false);
	}
	@SuppressWarnings("unused")
	private void resetFields() {
		setPdfFilename("");
		setDoi("");
		setHeadingLabel("");
		setHeadingTitle("");
		setHeadingSubtitle("");
		setContributorName("");
		setContributorAffiliation("");
		setSubjectGroup("");
		setThemeGroup("");
		setKeyword("");
		setTheme("");
		setToc("");
		setAbstractContent("");
		setAbstractImage("");
		setObject("");
		setContentItem("");
		setReferences("");
		setPageEnd("");
		setPageStart("");
		setProblem("");
	}

//	//TODO:SOLR-ify this
//	@Deprecated
//	private void populateBean(List<Document> docs) throws IOException {
//		boolean hasPageStart = false;
//		boolean hasPageEnd = false;
//		StringBuffer contributor = new StringBuffer();
//		StringBuffer contributorCollab = new StringBuffer();
//		StringBuffer affiliation = new StringBuffer();
//		StringBuffer tocItem = new StringBuffer();
//		for (Document document : docs) {
//
////			if (getContentType().equals("part-title")
////					&& !Misc.isEmpty(document.get("TYPE"))
////					&& !document.get("TYPE").equals("part-title")) {
////				continue;
////			}
//
//			if (!Misc.isEmpty(document.get(PDF))) {
//				if (!isDisplayPdfFilename()) {
//					setDisplayPdfFilename(true);
//				}
//				
//				setPdfFilename(document.get(PDF));
//			}
//
//			if (!Misc.isEmpty(document.get(DOI))) {
//				if (!isDisplayDOI()) {
//					setDisplayDOI(true);
//				}
//				setDoi(document.get(DOI));
//			}
//
//			if (!Misc.isEmpty(document.get(HEADING_TITLE))) {
//				setHeadingTitle(document.get(HEADING_TITLE));
//			}
//			
//			if (!Misc.isEmpty(document.get(HEADING_LABEL))) {
//				if (!isDisplayHeadingLabel()) {
//					setDisplayHeadingLabel(true);
//				}
//				setHeadingLabel(document.get(HEADING_LABEL));
//			}
//
//			if (!Misc.isEmpty(document.get(HEADING_SUB_TITLE))) {
//				if (!isDisplayHeadingSubtitle()) {
//					setDisplayHeadingSubtitle(true);
//				}
//				setHeadingSubtitle(document.get(HEADING_SUB_TITLE));
//			}
//
//			if (!Misc.isEmpty(document.get(PAGE_END))
//					&& Misc.isEmpty(getPageEnd())) {
//				hasPageEnd = true;
//				setPageEnd(document.get(PAGE_END));
//			}
//
//			if (!Misc.isEmpty(document.get(PAGE_START))
//					&& Misc.isEmpty(getPageStart())) {
//				hasPageStart = true;
//				setPageStart(document.get(PAGE_START));
//			}
//
//			if (!Misc.isEmpty(document.get(CONTRIBUTOR_NAME))) {
//				if (!isDisplayContributorName()) {
//					setDisplayContributorName(true);
//				}
//				contributor.append(document.get(CONTRIBUTOR_NAME) + " <em>("
//						+ document.get(ALPHASORT) + ")</em>" + HTML_BREAK);				
////				setContributorName(document.get(CONTRIBUTOR_NAME));
//			}
//			
//			if (!Misc.isEmpty(document.get(CONTRIBUTOR_COLLAB))) {
//				if (!isDisplayContributorCollab()) {
//					setDisplayContributorCollab(true);
//				}
//				contributorCollab.append(document.get(CONTRIBUTOR_COLLAB) + HTML_BREAK);				
//			}
//			
//			if (!Misc.isEmpty(document.get(CONTRIBUTOR_AFFILIATION))) {
//				if (!isDisplayContributorAffiliation()) {
//					setDisplayContributorAffiliation(true);
//				}
//				affiliation.append(document.get(CONTRIBUTOR_AFFILIATION) + HTML_BREAK);
////				setContributorAffiliation(document.get(CONTRIBUTOR_AFFILIATION));
//			}
//
//			if (!Misc.isEmpty(document.get(KEYWORD))) {
//				if (!isDisplayKeyword()) {
//					setDisplayKeyword(true);
//				}
//				// System.out.println(document.get(KEYWORD));
//				setKeyword(document.get(KEYWORD));
//			}
//
//			if (!Misc.isEmpty(document.get(ABSTRACT))) {
//				if (!isDisplayAbstractContent()) {
//					setDisplayAbstractContent(true);
//				}
//
////				String newAbstract = ""document.get(ABSTRACT));				
//
//				setAbstractContent(document.get(ABSTRACT));
//				setAbstractImage(document.get(ABSTRACT_IMAGE));
//				
////				if (!isDisplayPdfFilename()) {
////					setDisplayPdfFilename(true);
////					setPdfFilename(document.get(ABSTRACT_IMAGE));
////				}				
//			}
//
//			if (!Misc.isEmpty(document.get(PROBLEM))) {
//				if (!isDisplayProblem()) {
//					setDisplayProblem(true);
//				}
//				String problem = document.get(PROBLEM);
//				if (problem.equals(PROBLEM_NONE)) {
//					problem += PROBLEM_NOTE_NONE;
//				} else if (problem.equals(PROBLEM_DISPLAY)) {
//					problem += PROBLEM_NOTE_DISPLAY;
//				} else if (problem.equals(PROBLEM_UNICODE)) {
//					problem += PROBLEM_NOTE_UNICODE;
//				}
//				setProblem(problem);
//			}
//
//			if (!Misc.isEmpty(document.get(CITATION))) {
//				if (!isDisplayCitations()) {
//					setDisplayCitations(true);
//				}
//
//				addCitation(document.get(CITATION));
//			}
//
//			// get alphasort
//			if (CONTENT_ITEM.equals(document.get(ELEMENT))) {
//				setTitleAlphasort(document.get(ALPHASORT));
//			}
//			
//			if(!Misc.isEmpty(document.get(TOC_ITEM))) {
//				if(!isDisplayToc()) {
//					setDisplayToc(true);
//				}
//				
//				tocItem.append(document.get(TOC_ITEM) + " <em>(start page: " + document.get(TOC_START_PAGE) + ")</em>" + HTML_BREAK);
//			}
//		} // end for
//
//		// this method "isDisplayAbstractContent" will return false if the image
//		// does not
//		// exist, the following would be executed when in error.
//		// setAbstract implements a logic for finding the error
//		if (!isDisplayAbstractContent()) {
//			setAbstractImage(EbookDetailsBean.NO_IMAGE_IN_XML);
//		}
//		if (hasPageEnd && hasPageStart) {
//			setDisplayPages(true);
//		}
//		
//		
//		//arrange title - heading label + title
////		if( this.headingLabel != null && this.headingLabel.trim().length() > 0 ){
////			setTitle(this.headingLabel + COLON + SPACE + this.headingTitle  );
////		}else{
////			setTitle(this.headingTitle);
////		}		
//		// use title generated in ebook_chapter page
//		if(getContentTitle() != null && getContentTitle().trim().length() > 0){
//			setTitle(getContentTitle());
//		}			
//		
//		if(!Misc.isEmpty(contributor.toString())) {
//			setContributorName(contributor.toString());
//		}
//		
//		if(!Misc.isEmpty(contributorCollab.toString())) {
//			setContributorCollab(contributorCollab.toString());
//		}
//		
//		if(!Misc.isEmpty(affiliation.toString())) {
//			setContributorAffiliation(affiliation.toString());
//		}
//		
////		if(!Misc.isEmpty(getContentTitle())) {
////			setHeadingTitle(getContentTitle().replaceAll(getHeadingLabel() + COLON + SPACE, ""));
////		}
//		
//		if(!Misc.isEmpty(tocItem.toString())) {
//			setToc(tocItem.toString());
//		}
//	}	



	private String getQueryStringContent() {
		//StringBuffer queryString = new StringBuffer("ID:" + getContentId() + " AND ELEMENT:content-item");
		StringBuffer queryString = new StringBuffer("id:" + getContentId());
		return queryString.toString();
	}
	
	
	private void populate(List<BookContentItemDocument> contentItems) throws IOException {
		boolean hasPageStart = false;
		boolean hasPageEnd = false;
		StringBuffer contributor = new StringBuffer();
		StringBuffer contributorCollab = new StringBuffer();
		StringBuffer affiliation = new StringBuffer();
		
		for (BookContentItemDocument contentItem : contentItems)
		{
			if (!Misc.isEmpty(contentItem.getPdfFilename())) {
				setPdfFilename(contentItem.getPdfFilename());
				
				// PID 83258 2012-04-25
				setIsbnBreakdown(IsbnContentDirUtil.getFilePathIsbnBreakdown(contentItem.getIsbn(), getPdfFilename()));
				
				if (!isDisplayPdfFilename() && (new File(CONTENT_DIR + getIsbnBreakdown() + getPdfFilename()).exists())) { // 2012-11-21
					setDisplayPdfFilename(true);
				}
			}

			if (!Misc.isEmpty(contentItem.getDoi())) {
				if (!isDisplayDOI()) {
					setDisplayDOI(true);
				}
				setDoi(contentItem.getDoi());
			}

			if (!Misc.isEmpty(contentItem.getTitle())) {
				setHeadingTitle(StringUtil.mdashUnicodeToHtml(contentItem.getTitle()));
			}
			
			if (!Misc.isEmpty(contentItem.getLabel())) {
				if (!isDisplayHeadingLabel()) {
					setDisplayHeadingLabel(true);
				}
				setHeadingLabel(contentItem.getLabel());
			}

			if (!Misc.isEmpty(contentItem.getSubtitle())) {
				if (!isDisplayHeadingSubtitle()) {
					setDisplayHeadingSubtitle(true);
				}
				setHeadingSubtitle(contentItem.getSubtitle());
			}

			if (!Misc.isEmpty(contentItem.getPageEnd())
					&& Misc.isEmpty(getPageEnd())) {
				hasPageEnd = true;
				setPageEnd(contentItem.getPageEnd());
			}

			if (!Misc.isEmpty(contentItem.getPageStart())
					&& Misc.isEmpty(getPageStart())) {
				hasPageStart = true;
				setPageStart(contentItem.getPageStart());
			}

			List<String> contributors = contentItem.getAuthorNameList();
			List<String> alphasort = contentItem.getAuthorNameAlphasortList();
			List<String> position = contentItem.getAuthorPositionList();
			if (contributors != null && !contributors.isEmpty()) 
			{
				if (!isDisplayContributorName()) 
					setDisplayContributorName(true);
				
				for(int index=0; index<contributors.size(); index++)
				{
					contributor.append(contributors.get(index) 
							+ (( alphasort != null && !"none".equals(alphasort)) ? " <em>("+ alphasort.get(index) + ")</em>" : "") 
							+ ( position != null && !"none".equals(position) && !(position.hashCode()==31) ? " [Author Position: " + position.get(index) +"]" : " <font color=\"red\">[AUTHOR POSITION EMPTY!] </font>" ) 
							+ HTML_BREAK
							);
				}					
			}
			
			List<String> affiliations = contentItem.getAuthorAffiliationList();
			if (affiliations != null && !affiliations.isEmpty()) 
			{
				if (!isDisplayContributorAffiliation())
					setDisplayContributorAffiliation(true);
				
				for(String affiliationName : affiliations)
					affiliation.append(affiliationName + HTML_BREAK);
			}

			List<String> keywords = contentItem.getKeywordTextList();
			if (keywords != null && !keywords.isEmpty()) 
			{
				if (!isDisplayKeyword()) 
					setDisplayKeyword(true);
				
				StringBuilder sb = new StringBuilder();
				for(String keyword : keywords)
					sb.append(keyword);
				
				setKeyword(sb.toString());
			}
			
			List<String> themes = contentItem.getKeywordThemeList();
			if (themes != null && !themes.isEmpty()) 
			{
				if (!isDisplayTheme()) 
					setDisplayTheme(true);
				
				StringBuilder sb = new StringBuilder();
//				sb.append("keyword-group source=\"theme\"");
				for(String theme : themes)
					sb.append(theme);
				
				setTheme(sb.toString());
			}
			
			//20131205 removed because stahl 2 content didn't push through
			//20130624 jubs - adding search from keywordsCore
//			List<KeywordsContentItemDocument> keywordsFromCore = solrWorker.searchKeywordCore("content_id:"+contentItem.getContentId());
//			if (keywordsFromCore != null && !keywordsFromCore.isEmpty()) 
//			{
//				if (!isDisplayKeywordsFromCore()) 
//					setDisplayKeywordsFromCore(true);	
//				
//				setKeywordsFromCore(keywordsFromCore);
//			}
			
			//20131206 arvin - adding search from wordCore
			List<WordsContentItemDocument> wordsFromCore = solrWorker.searchWordCore("book_id:"+contentItem.getBookId());
			if(wordsFromCore != null && !wordsFromCore.isEmpty())
			{
				if(!isDisplayWordsFromCore());
					setDisplayWordsFromCore(true);
					
				setWordsFromCore(wordsFromCore);
					
					
			}

			String contentAbstract = getAbstract(contentItem.getIsbn(), contentItem.getId());
			if (!Misc.isEmpty(contentAbstract)) 
			{
				if (!isDisplayAbstractContent()) 
					setDisplayAbstractContent(true);
				
				setAbstractContent(contentAbstract);
				setAbstractImage(contentItem.getAbstractFilename());
			}

			if (!Misc.isEmpty(contentItem.getAbstractProblem())) 
			{
				if (!isDisplayProblem()) 
					setDisplayProblem(true);
				
				String problem = contentItem.getAbstractProblem();
				if (problem.equals(PROBLEM_NONE)) 
					problem += PROBLEM_NOTE_NONE;
				else if (problem.equals(PROBLEM_DISPLAY)) 
					problem += PROBLEM_NOTE_DISPLAY;
				else if (problem.equals(PROBLEM_UNICODE))
					problem += PROBLEM_NOTE_UNICODE;
				
				setProblem(problem);
			}
		
			
			// get alphasort
			//setTitleAlphasort(StringEscapeUtils.escapeXml(contentItem.getTitleAlphasort()));
			String alphasortString = contentItem.getTitleAlphasort();
			setTitleAlphasort(alphasortString);
			if(containsSpecialChar(alphasortString))
				setDisplayAlphasort(true);

			
			if(!Misc.isEmpty(contentItem.getTocText())) 
			{
				if(!isDisplayToc())
					setDisplayToc(true);
				// fixed UnmappableCharacter exception
				setToc(StringUtil.mdashUnicodeToHtml(contentItem.getTocText()));
			}
			
			// --> PID 81717
			if(contentItem.getInsertIds() != null && !contentItem.getInsertIds().isEmpty()) {
				
				if (!isDisplayEBookInsertBeansList())
					setDisplayEBookInsertBeansList(true);
				
				List<EBookInsertBean> insertBeansList = new ArrayList<EBookInsertBean>();
				for(int i = 0; i < contentItem.getInsertIds().size(); i++) {
					
					EBookInsertBean insertBean = new EBookInsertBean();
					insertBean.setInsertTitle(contentItem.getInsertTitles().get(i));
					insertBean.setInsertAlphasortTitle(contentItem.getInsertTitlesAlphasort().get(i));
					insertBean.setInsertFilename(contentItem.getInsertFilenames().get(i));
					
					insertBeansList.add(insertBean);
					
				}
				
				setEbookInsertBeansList(insertBeansList);
				
			}
			// <-- PID 81717
			
			
		} // end for


		if (!isDisplayAbstractContent()) {
			setAbstractImage(EbookDetailsBean.NO_IMAGE_IN_XML);
		}
		if (hasPageEnd && hasPageStart) {
			setDisplayPages(true);
		}
		
			
		// use title generated in ebook_chapter page
		if(getContentTitle() != null && getContentTitle().trim().length() > 0){
			setTitle(getContentTitle());
		}			
		
		if(!Misc.isEmpty(contributor.toString())) {
			setContributorName(contributor.toString());
		}
		
		if(!Misc.isEmpty(contributorCollab.toString())) {
			setContributorCollab(contributorCollab.toString());
		}
		
		if(!Misc.isEmpty(affiliation.toString())) {
			setContributorAffiliation(affiliation.toString());
		}
		
		
	}	
	
	private boolean containsSpecialChar(String alphasort){
		boolean found = false;
		Pattern p = Pattern.compile("\\W*+");
		Matcher m = p.matcher(alphasort);
		if(m.find())
			found = true; 
		return found;
	}
	

	/**
	 * @return the pdfFilename
	 */
	public String getPdfFilename() {
		return pdfFilename;
	}

	/**
	 * @param pdfFilename
	 *            the pdfFilename to set
	 */
	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi
	 *            the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the headingLabel
	 */
	public String getHeadingLabel() {
		return headingLabel;
	}

	/**
	 * @param headingLabel
	 *            the headingLabel to set
	 */
	public void setHeadingLabel(String headingLabel) {
		this.headingLabel = headingLabel;
	}

	/**
	 * @return the headingTitle
	 */
	public String getHeadingTitle() {
		return headingTitle;
	}

	/**
	 * @param headingTitle
	 *            the headingTitle to set
	 */
	public void setHeadingTitle(String headingTitle) {
		this.headingTitle = headingTitle;
	}

	/**
	 * @return the headingSubtitle
	 */
	public String getHeadingSubtitle() {
		return headingSubtitle;
	}

	/**
	 * @param headingSubtitle
	 *            the headingSubtitle to set
	 */
	public void setHeadingSubtitle(String headingSubtitle) {
		this.headingSubtitle = headingSubtitle;
	}

	/**
	 * @return the contributorName
	 */
	public String getContributorName() {
		return contributorName;
	}

	/**
	 * @param contributorName
	 *            the contributorName to set
	 */
	public void setContributorName(String contributorName) {
		this.contributorName = contributorName;
	}

	/**
	 * @return the contributorCollab
	 */
	public String getContributorCollab() {
		return contributorCollab;
	}

	/**
	 * @param contributorCollab
	 *            the contributorCollab to set
	 */
	public void setContributorCollab(String contributorCollab) {
		this.contributorCollab = contributorCollab;
	}
	
	/**
	 * @return the contributorAffiliation
	 */
	public String getContributorAffiliation() {
		return contributorAffiliation;
	}

	/**
	 * @param contributorAffiliation
	 *            the contributorAffiliation to set
	 */
	public void setContributorAffiliation(String contributorAffiliation) {
		this.contributorAffiliation = contributorAffiliation;
	}

	public String getContributorPosition() {
		return contributorPosition;
	}

	public void setContributorPosition(String contributorPosition) {
		this.contributorPosition = contributorPosition;
	}

	/**
	 * @return the subjectGroup
	 */
	public String getSubjectGroup() {
		return subjectGroup;
	}

	/**
	 * @param subjectGroup
	 *            the subjectGroup to set
	 */
	public void setSubjectGroup(String subjectGroup) {
		this.subjectGroup = subjectGroup;
	}

	/**
	 * @return the themeGroup
	 */
	public String getThemeGroup() {
		return themeGroup;
	}

	/**
	 * @param themeGroup
	 *            the themeGroup to set
	 */
	public void setThemeGroup(String themeGroup) {
		this.themeGroup = themeGroup;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;		
	}
	
	
	
	/**
	 * get keyword for display
	 * @return
	 */
	public ArrayList<ArrayList<String>> getKeywordDisplay() {
		String[] keys;
		 
		keys = keyword.split(BR);
		
		ArrayList<ArrayList<String>> strArr = new ArrayList<ArrayList<String>>();
		for( int i = 0; i < KEYWORD_DISP_LENGTH; i++ ){
			strArr.add(new ArrayList<String>());
		}		
		int rowsPerCol = (int) Math.ceil(keys.length*1.0/KEYWORD_DISP_LENGTH);		
		int i=0;
		int listNum=0;
		for(String str : keys){
			strArr.get(listNum).add(str.trim());
			i++;
			if( i == rowsPerCol ){
				i = 0;
				listNum++;
			}
			
			
		}
		return strArr;
	}

	/**
	 * @param keyword
	 *            the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * get keyword for display
	 * @return
	 */
	public ArrayList<ArrayList<String>> getThemeDisplay() {
		String[] keys;
		 
		keys = theme.split(BR);
		
		ArrayList<ArrayList<String>> strArr = new ArrayList<ArrayList<String>>();
		for( int i = 0; i < KEYWORD_DISP_LENGTH; i++ ){
			strArr.add(new ArrayList<String>());
		}		
		int rowsPerCol = (int) Math.ceil(keys.length*1.0/KEYWORD_DISP_LENGTH);		
		int i=0;
		int listNum=0;
		for(String str : keys){
			strArr.get(listNum).add(str.trim());
			i++;
			if( i == rowsPerCol ){
				i = 0;
				listNum++;
			}
			
			
		}
		return strArr;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public boolean isDisplayTheme() {
		return displayTheme;
	}

	public void setDisplayTheme(boolean displayTheme) {
		this.displayTheme = displayTheme;
	}
	
	public boolean isDisplayKeywordsFromCore() {
		return displayKeywordsFromCore;
	}

	public void setDisplayKeywordsFromCore(boolean displayKeywordsFromCore) {
		this.displayKeywordsFromCore = displayKeywordsFromCore;
	}
	
	//20131206 arvin - added for WordCore
	public boolean isDisplayWordsFromCore() {
		return displayWordsFromCore;
	}
	
	public void setDisplayWordsFromCore(boolean displayWordsFromCore) {
		this.displayWordsFromCore = displayWordsFromCore;	
	}
	
	


	/**
	 * @return the toc
	 */
	public String getToc() {
		return toc;
	}

	/**
	 * @param toc
	 *            the toc to set
	 */
	public void setToc(String toc) {
		this.toc = toc;
	}

	/**
	 * @return the abstractContent
	 */
	public String getAbstractContent() {
		return abstractContent;
	}

	/**
	 * @param abstractContent
	 *            the abstractContent to set
	 */
	public void setAbstractContent(String abstractContent) {
		this.abstractContent = abstractContent;
	}

	/**
	 * @return the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * @param object
	 *            the object to set
	 */
	public void setObject(String object) {
		this.object = object;
	}

	/**
	 * @return the contentItem
	 */
	public String getContentItem() {
		return contentItem;
	}

	/**
	 * @param contentItem
	 *            the contentItem to set
	 */
	public void setContentItem(String contentItem) {
		this.contentItem = contentItem;
	}

	/**
	 * @return the references
	 */
	public String getReferences() {
		return references;
	}

	/**
	 * @param references
	 *            the references to set
	 */
	public void setReferences(String references) {
		this.references = references;
	}

	/**
	 * @return the displayPdfFilename
	 */
	public boolean isDisplayPdfFilename() {
		return displayPdfFilename;
	}

	/**
	 * @param displayPdfFilename
	 *            the displayPdfFilename to set
	 */
	public void setDisplayPdfFilename(boolean displayPdfFilename) {
		this.displayPdfFilename = displayPdfFilename;
	}

	/**
	 * @return the displayHeadingLabel
	 */
	public boolean isDisplayHeadingLabel() {
		return displayHeadingLabel;
	}

	/**
	 * @param displayHeadingLabel
	 *            the displayHeadingLabel to set
	 */
	public void setDisplayHeadingLabel(boolean displayHeadingLabel) {
		this.displayHeadingLabel = displayHeadingLabel;
	}

	/**
	 * @return the displayHeadingSubtitle
	 */
	public boolean isDisplayHeadingSubtitle() {
		return displayHeadingSubtitle;
	}

	/**
	 * @param displayHeadingSubtitle
	 *            the displayHeadingSubtitle to set
	 */
	public void setDisplayHeadingSubtitle(boolean displayHeadingSubtitle) {
		this.displayHeadingSubtitle = displayHeadingSubtitle;
	}

	/**
	 * @return the displayContributorAffiliation
	 */
	public boolean isDisplayContributorAffiliation() {
		return displayContributorAffiliation;
	}

	/**
	 * @param displayContributorAffiliation
	 *            the displayContributorAffiliation to set
	 */
	public void setDisplayContributorAffiliation(
			boolean displayContributorAffiliation) {
		this.displayContributorAffiliation = displayContributorAffiliation;
	}

	/**
	 * @return the displayContributorCollab
	 */
	public boolean isDisplayContributorCollab() {
		return displayContributorCollab;
	}

	/**
	 * @param displayContributorCollab
	 *            the displayContributorCollab to set
	 */
	public void setDisplayContributorCollab(
			boolean displayContributorCollab) {
		this.displayContributorCollab = displayContributorCollab;
	}
	
	/**
	 * @return the displaySubjectGroup
	 */
	public boolean isDisplaySubjectGroup() {
		return displaySubjectGroup;
	}

	/**
	 * @param displaySubjectGroup
	 *            the displaySubjectGroup to set
	 */
	public void setDisplaySubjectGroup(boolean displaySubjectGroup) {
		this.displaySubjectGroup = displaySubjectGroup;
	}

	/**
	 * @return the displayThemeGroup
	 */
	public boolean isDisplayThemeGroup() {
		return displayThemeGroup;
	}

	/**
	 * @param displayThemeGroup
	 *            the displayThemeGroup to set
	 */
	public void setDisplayThemeGroup(boolean displayThemeGroup) {
		this.displayThemeGroup = displayThemeGroup;
	}

	/**
	 * @return the displayKeyword
	 */
	public boolean isDisplayKeyword() {
		return displayKeyword;
	}

	/**
	 * @param displayKeyword
	 *            the displayKeyword to set
	 */
	public void setDisplayKeyword(boolean displayKeyword) {
		this.displayKeyword = displayKeyword;
	}

	/**
	 * @return the displayToc
	 */
	public boolean isDisplayToc() {
		return displayToc;
	}

	/**
	 * @param displayToc
	 *            the displayToc to set
	 */
	public void setDisplayToc(boolean displayToc) {
		this.displayToc = displayToc;
	}

	/**
	 * @return the displayAbstractContent
	 */
	public boolean isDisplayAbstractContent() {
		return displayAbstractContent;
	}

	/**
	 * @param displayAbstractContent
	 *            the displayAbstractContent to set
	 */
	public void setDisplayAbstractContent(boolean displayAbstractContent) {
		this.displayAbstractContent = displayAbstractContent;
	}

	/**
	 * @return the displayObject
	 */
	public boolean isDisplayObject() {
		return displayObject;
	}

	/**
	 * @param displayObject
	 *            the displayObject to set
	 */
	public void setDisplayObject(boolean displayObject) {
		this.displayObject = displayObject;
	}

	/**
	 * @return the displayContentItem
	 */
	public boolean isDisplayContentItem() {
		return displayContentItem;
	}

	/**
	 * @param displayContentItem
	 *            the displayContentItem to set
	 */
	public void setDisplayContentItem(boolean displayContentItem) {
		this.displayContentItem = displayContentItem;
	}

	/**
	 * @return the displayReferences
	 */
	public boolean isDisplayReferences() {
		return displayReferences;
	}

	/**
	 * @param displayReferences
	 *            the displayReferences to set
	 */
	public void setDisplayReferences(boolean displayReferences) {
		this.displayReferences = displayReferences;
	}

	/**
	 * @return the contentId
	 */
	public String getContentId() {
		return contentId;
	}

	/**
	 * @param contentId
	 *            the contentId to set
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the displayContributorName
	 */
	public boolean isDisplayContributorName() {
		return displayContributorName;
	}

	/**
	 * @param displayContributorName
	 *            the displayContributorName to set
	 */
	public void setDisplayContributorName(boolean displayContributorName) {
		this.displayContributorName = displayContributorName;
	}

	public boolean isDisplayCitations() {
		return displayCitations;
	}

	public void setDisplayCitations(boolean displayCitations) {
		this.displayCitations = displayCitations;
	}

	public List<String> getCitations() {
		return citations;
	}

	public void setCitations(List<String> citations) {
		this.citations = citations;
	}

	public void addCitation(String citation) {
		citations.add(citation);
	}

	/**
	 * @return the abstractImage
	 */
	public String getAbstractImage() {
		return abstractImage;
	}

	/**
	 * uses ImageUtil to check if the image exists or not will supply default
	 * path in errors are encountered
	 * 
	 * @param abstractImage
	 *            the abstractImage to set
	 */
	public void setAbstractImage(String abstractImage) {
		if (abstractImage != EbookDetailsBean.NO_IMAGE_IN_XML) {
			this.abstractImage = ImageUtil.setImagePath(abstractImage,
					EbookDetailsBean.MISSING_COVER_IMAGE,
					EbookDetailsBean.IMAGE_DIR_PATH, this.getClass());
		} else {
			this.abstractImage = EbookDetailsBean.NO_COVER_IMAGE;
		}

	}

	/**
	 * @return the pageEnd
	 */
	public String getPageEnd() {
		return pageEnd;
	}

	/**
	 * @param pageEnd
	 *            the pageEnd to set
	 */
	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	/**
	 * @return the pageStart
	 */
	public String getPageStart() {
		return pageStart;
	}

	/**
	 * @param pageStart
	 *            the pageStart to set
	 */
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	/**
	 * @return the displayPages
	 */
	public boolean isDisplayPages() {
		return displayPages;
	}

	/**
	 * @param displayPages
	 *            the displayPages to set
	 */
	public void setDisplayPages(boolean displayPages) {
		this.displayPages = displayPages;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public boolean isDisplayProblem() {
		return displayProblem;
	}

	public void setDisplayProblem(boolean displayProblem) {
		this.displayProblem = displayProblem;
	}

	public String getTitleAlphasort() {
		return titleAlphasort;
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public boolean isDisplayDOI() {
		return displayDOI;
	}

	public void setDisplayDOI(boolean displayDOI) {
		this.displayDOI = displayDOI;
	}

	public boolean isDisplayIndented() {
		return displayIndented;
	}

	public void setDisplayIndented(boolean displayIndented) {
		this.displayIndented = displayIndented;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the contentTitle
	 */
	public String getContentTitle() {
		return  contentTitle;
	}

	/**
	 * @param contentTitle the contentTitle to set
	 */
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	/**
	 * @return the ebookContentBean
	 */
	public EBookContentBean getEbookContentBean() {
		return ebookContentBean;
	}

	/**
	 * @param ebookContentBean the ebookContentBean to set
	 */
	public void setEbookContentBean(EBookContentBean ebookContentBean) {
		this.ebookContentBean = ebookContentBean;
	}
	
	
	private String getAbstract(String isbn, String contentId){
		String result = "";
		// PID 83258
		String filename = contentId + "_abstract.txt";
		final File abstractDir = new File(IsbnContentDirUtil.getFilePath(isbn, filename) + File.separator + filename);
		
		try
		{
			StringBuilder sb = new StringBuilder();
			if(abstractDir != null && abstractDir.exists())
			{
				String temp = null;
				FileReader reader = new FileReader(abstractDir);
				BufferedReader breader = new BufferedReader(reader);
				while((temp = breader.readLine()) != null)
					sb.append(temp);
		
				result = sb.toString();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return result;
	}

	public String getIsbnBreakdown() {
		return isbnBreakdown;
	}

	public void setIsbnBreakdown(String isbnBreakdown) {
		this.isbnBreakdown = isbnBreakdown;
	}

	// --> PID 81717
	public List<EBookInsertBean> getEbookInsertBeansList() {
		return ebookInsertBeansList;
	}

	public void setEbookInsertBeansList(List<EBookInsertBean> ebookInsertBeansList) {
		this.ebookInsertBeansList = ebookInsertBeansList;
	}

	public boolean isDisplayEBookInsertBeansList() {
		return displayEBookInsertBeansList;
	}

	public void setDisplayEBookInsertBeansList(boolean displayEBookInsertBeansList) {
		this.displayEBookInsertBeansList = displayEBookInsertBeansList;
	}
	// <-- PID 81717

	public boolean isDisplayAlphasort() {
		return displayAlphasort;
	}

	public void setDisplayAlphasort(boolean displayAlphasort) {
		this.displayAlphasort = displayAlphasort;
	}

	public List<KeywordsContentItemDocument> getKeywordsFromCore() {
		return keywordsFromCore;
	}

	public void setKeywordsFromCore(
			List<KeywordsContentItemDocument> keywordsFromCore) {
		this.keywordsFromCore = keywordsFromCore;
	}
	//20131206 arvin -  added for WordCore
	public List<WordsContentItemDocument> getWordsFromCore() {
		return wordsFromCore;
	}

	public void setWordsFromCore(
			List<WordsContentItemDocument> wordsFromCore) {
		this.wordsFromCore = wordsFromCore;
	}

	
}
