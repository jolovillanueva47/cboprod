package org.cambridge.ebooks.production.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.common.LogManager;

/**
 * 
 * @author jgalang
 * 
 */

public class AuthorizationFilter implements Filter {
	
	private static final String LOAD_DATA = "/load_data.jsf";
	private static final String VIEW_DTD = "/view_dtd.jsf";
	private static final String SYSTEM_USERS = "/system_users.jsf";
	private static final String AUDIT_TRAIL = "/audit_trail.jsf";
	private static final String REDIRECT_TO = "home.jsf";
	private FilterConfig filterConfig;
	
	public void init ( FilterConfig config ) throws ServletException {
		filterConfig = config;
	}
	
	public void doFilter( ServletRequest httpRequest, ServletResponse httpResponse, FilterChain chain ) throws IOException, ServletException { 
		HttpServletRequest 	request  = (HttpServletRequest)  httpRequest;
		HttpServletResponse response = (HttpServletResponse) httpResponse;
		
		String servletPath = request.getServletPath();

		User user = HttpUtil.getUserFromSession(request.getSession());
		//TODO: -C2- Additional authorization for unimplemented features.
		boolean authorized = true; 
		if (servletPath.equals(LOAD_DATA)) {
			authorized = user.getAccessRights().getCanLoadData();
		} else if (servletPath.equals(VIEW_DTD)) {
			authorized = user.getAccessRights().getCanViewDtd();
		} else if (servletPath.equals(SYSTEM_USERS)) {
			authorized = user.getAccessRights().getCanManageUsers();
		} else if (servletPath.equals(AUDIT_TRAIL)) {
			authorized = user.getAccessRights().getCanViewAuditTrail();
		}
		if (!authorized) {
			LogManager.info(AuthorizationFilter.class, "Unauthorized user redirected to home page: " + servletPath);
			response.sendRedirect(REDIRECT_TO);
			return;
		}

		
		chain.doFilter(request, response);
	}
		
	public void destroy() {
		filterConfig = null;
	}
}
