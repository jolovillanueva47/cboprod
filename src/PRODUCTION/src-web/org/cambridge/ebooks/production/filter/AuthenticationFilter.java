package org.cambridge.ebooks.production.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.common.LogManager;
import org.cambridge.util.Misc;

/**
 * 
 * @author jgalang
 * 
 */

public class AuthenticationFilter implements Filter {

	private static final String LOGIN_JSF = "/login.jsf";
	private static final String REDIRECT_TO = "login.jsf";
	private static final String EBOOK_CONTENTS_JSF = "/ebook_contents.jsf";
	
	private FilterConfig filterConfig;

	public void init(FilterConfig config) throws ServletException {
		filterConfig = config;
	}

	public void doFilter(ServletRequest httpRequest, ServletResponse httpResponse, FilterChain chain) throws IOException, ServletException {
			
		HttpServletRequest request = (HttpServletRequest) httpRequest;
		HttpServletResponse response = (HttpServletResponse) httpResponse;

		String servletPath = request.getServletPath();

		setClientIp(request);

		if (!servletPath.equals(LOGIN_JSF) && !servletPath.equals(EBOOK_CONTENTS_JSF)) {

			try
			{
				User user = HttpUtil.getUserFromSession(request.getSession());
				if (user != null && user.getUserId().length() > 0) 
				{
					LogManager.info(AuthenticationFilter.class,	"Validated user at: " + servletPath);
				} 
				else 
				{
					LogManager.info(AuthenticationFilter.class,	"Guest user redirected to login page(2): " + servletPath);
					response.sendRedirect(REDIRECT_TO);
					return;
				}
			} 
			catch (Exception e) 
			{
				LogManager.error(AuthenticationFilter.class, "Guest user redirected to login page(1): "	+ servletPath);
			}
		}
		
		chain.doFilter(request, response);
		
	}

	public void setClientIp(HttpServletRequest request) {		
		HttpSession session = request.getSession();

		boolean ipChecked = null == session.getAttribute("checkedIp");
		String testIp = request.getParameter("ip");

		if (Misc.isNotEmpty(testIp) || ipChecked) 
        {
            String ipAddress = request.getHeader("True-Client-IP");
            LogManager.info(AuthenticationFilter.class, "True-Client-IP: " + ipAddress);
            
            if (Misc.isEmpty(ipAddress)) {  
                ipAddress = request.getHeader("X-Cluster-Client-Ip");
                LogManager.info(AuthenticationFilter.class, "X-Cluster-Client-Ip: " + ipAddress);
            }
            
            if (Misc.isEmpty(ipAddress)) {  
                ipAddress = request.getRemoteAddr();
                LogManager.info(AuthenticationFilter.class, "Remote address: "  + ipAddress);
            }
            
            String ip = Misc.isNotEmpty(testIp) ? testIp : ipAddress;

            session.setAttribute("checkedIp", ip);
            LogManager.info(AuthenticationFilter.class, "Setting IP to session: " + ip);
        }
	}

	public void destroy() {
		filterConfig = null;
	}
}
