package org.cambridge.ebooks.production.references;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.HttpUtil;
//import org.cambridge.ebooks.production.util.Log4JLogger;

public class ViewReferencesBean {
	
//	private static final Log4JLogger LogManager = new Log4JLogger(ViewReferencesBean.class);
	
//	private static String ELEMENT_ISBN = "ISBN";
//	private static String ELEMENT_ID = "ID";
//	private static String ELEMENT_TYPE = "TYPE";
//	private static String ELEMENT_POSITION = "POSITION";
//	private static String ELEMENT_TITLE = "TITLE";
//	private static String ELEMENT_REFERENCE_TITLE = "REFERENCE_TITLE";
	private static String REFERENCE_FILE_SUFFIX = "ref.html";
	
	// PID 83758
	private static final String CONTENT_DIR_ABSOLUTE = "/app/ebooks/content/";

	private String eisbn;
	private ArrayList<ContentReferences> referenceList = new ArrayList<ContentReferences>();
	private static final EBookSearchWorker worker = new EBookSearchWorker();

	public ArrayList<ContentReferences> getReferenceList() {
		String bookId = HttpUtil.getHttpServletRequestParameter("bid");
		eisbn = getEisbn(bookId);
		referenceList = getReferences(bookId);
		return referenceList;
	}
	
	private static String getEisbn(String bookId) {
		//List<Document> doclist = IndexSearchUtil.searchIndex(IndexSearchUtil.INDEX_DIR, "ELEMENT:isbn AND BOOK_ID:" + bookId);
		List<BookMetadataDocument> doclist = worker.searchBookCore("book_id:" + bookId);
		if(doclist != null && !doclist.isEmpty())
			return doclist.get(0).getIsbn();
		else
			return "";
	}
	
	private static ArrayList<ContentReferences> getReferences(String bookId) {
		ArrayList<ContentReferences> result = new ArrayList<ContentReferences>();
		//List<Document> docs = IndexSearchUtil.searchIndex(IndexSearchUtil.INDEX_DIR, "ELEMENT:content-item AND BOOK_ID:" + bookId);
		List<BookContentItemDocument> docs = worker.searchContentCore("book_id:" + bookId + " AND content_type:content-item");
		for (BookContentItemDocument doc : docs) 
		{
			ContentReferences cr = new ContentReferences();
			String contentId = doc.getContentId();
			cr.setContentId(contentId);
			cr.setContentType(doc.getContentType());
			cr.setContentTitle(doc.getTitle());
			cr.setContentPosition(doc.getPosition());
			cr.setReferenceType(doc.getReferenceType());
			boolean hasReferenceType = cr.getReferenceType() != null && cr.getReferenceType().length()>0;
			cr.setReferenceTitle(hasReferenceType ? doc.getReferenceTitle() : null);
			
			// PID 83758 - this solution needs no adding of new boolean element in the indexer therefore, no re-index-all task...
			//cr.setReferencesFile(hasReferences ? contentId + REFERENCE_FILE_SUFFIX : null);
			cr.setReferencesFile(contentId + REFERENCE_FILE_SUFFIX);
			if (cr.getIsbnBreakdown() != null) {
				File file = new File(CONTENT_DIR_ABSOLUTE + cr.getIsbnBreakdown() + cr.getReferencesFile());
				cr.setReferenceFileExists( (file.canRead() ? true : false) );
			} else {
				cr.setReferenceFileExists(false);
			}

			result.add(cr);
		}
		return result;
	}
	
//	private static String getReferenceTitle(String contentId) {
//		return IndexSearchUtil.searchIndex(IndexSearchUtil.INDEX_DIR, "ELEMENT:citation AND PARENT_ID:" + contentId).get(0).get(ELEMENT_REFERENCE_TITLE);
//	}
	
//	private static String getReferenceType(String contentId) {
//		String result = null;
//		List<Document> docs = IndexSearchUtil.searchIndex(IndexSearchUtil.INDEX_DIR, "ELEMENT:references AND PARENT_ID:" + contentId);
//		if (docs != null && docs.size()>0) {
//			result = docs.get(0).get(ELEMENT_TYPE);
//		}
//		return result;
//	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public void setReferenceList(ArrayList<ContentReferences> referenceList) {
		this.referenceList = referenceList;
	}
}
