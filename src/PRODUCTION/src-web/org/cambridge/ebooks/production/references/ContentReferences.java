package org.cambridge.ebooks.production.references;


public class ContentReferences {

	private String contentId;
	private String contentType;
	private String contentTitle;
	private String contentPosition;
	private String referenceType;
	private String referenceTitle;
	private String referencesFile;
	
	// PID 83758
	private boolean referenceFileExists;
	
	// PID 83258 2012-04-26
	private ContentReferencesWorker contentRefsWorker = new ContentReferencesWorker();
	
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContentTitle() {
		return contentTitle;
	}
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}
	public String getContentPosition() {
		return contentPosition;
	}
	public void setContentPosition(String contentPosition) {
		this.contentPosition = contentPosition;
	}
	public String getReferenceType() {
		return referenceType;
	}
	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}
	public String getReferenceTitle() {
		return referenceTitle;
	}
	public void setReferenceTitle(String referenceTitle) {
		this.referenceTitle = referenceTitle;
	}
	public String getReferencesFile() {
		return referencesFile;
	}
	public void setReferencesFile(String referencesFile) {
		this.referencesFile = referencesFile;
	}
	
	//PID 83258 2012-04-26
	// referencesFile should be set first before calling this method
	public String getIsbnBreakdown() {
		return contentRefsWorker.createIsbnBreakdown(getReferencesFile());
	}
	
	// --> PID 83758
	public boolean isReferenceFileExists() {
		return referenceFileExists;
	}
	public void setReferenceFileExists(boolean referenceFileExists) {
		this.referenceFileExists = referenceFileExists;
	}
	// <-- PID 83758
	
}
