package org.cambridge.ebooks.production.references;

import java.io.File;

import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;

public class ContentReferencesWorker {
	
	private final static String BOOK_ID = "bookId";
	
	public String createIsbnBreakdown(String filename) {
		
		String bookId = (String) HttpUtil.getAttributeFromCurrentSession(BOOK_ID);
		String isbn = IsbnContentDirUtil.bookIdToIsbn(bookId);
		String isbnFilePath = IsbnContentDirUtil.getFilePath(isbn, filename);
		String isbnBreakdownPath = IsbnContentDirUtil.getFilePathIsbnBreakdown(isbn, filename);
		File contentRefFile = new File(isbnFilePath + filename);
		
		return contentRefFile.canRead() ? isbnBreakdownPath : null; 
		
	}

}
