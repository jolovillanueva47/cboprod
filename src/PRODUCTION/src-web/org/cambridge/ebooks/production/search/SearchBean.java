package org.cambridge.ebooks.production.search;

import javax.faces.event.ActionEvent;

/**
 * 
 * @author jgalang
 * 
 */

public class SearchBean {

	private String author;
	private String eisbn;
	private String bookTitle;
	private String chapterTitle;
	private String doi;
	
	
	public void initNewSearch(ActionEvent event) {
		System.out.println("init");
	}
	
	public void search(ActionEvent event) {
		System.out.println("searchiiing");
		System.out.println(author);
		System.out.println(eisbn);
		System.out.println(bookTitle);
		System.out.println(chapterTitle);
		System.out.println(doi);
		
		
	}
	
	public void reset(ActionEvent event) {
		System.out.println("reseeet");
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getChapterTitle() {
		return chapterTitle;
	}

	public void setChapterTitle(String chapterTitle) {
		this.chapterTitle = chapterTitle;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}
}
