package org.cambridge.ebooks.production.publisher;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.user.SystemUserBean;
import org.cambridge.ebooks.production.util.LogManager;
import org.cambridge.ebooks.production.util.PersistencePager;

public class PublisherBean {
	private PersistencePager<Publisher> pager;
	private List<Publisher> publisherList = new ArrayList<Publisher>();
	
	private String pagingText;
	private int goToPage;
	private int resultsPerPage;
	private ArrayList<SelectItem> pages;
	private String sortBy;
	private String tmpSortBy;
	
	public String updatePublisher() {
		return RedirectTo.PUBLISHER_UPDATE;
	}
	
	public void initNewPublisherSearch(ActionEvent event) {
		LogManager.info(PublisherBean.class, "Init search users.");
		sortBy = Publisher.SORT_BY_PUBLISHER;
		initPaging();
	}
	
	public void initPaging() {
		goToPage = 0;
		resultsPerPage = 10;
		String reportQuery = null;
		
		if (sortBy.equals(Publisher.SORT_BY_PUBLISHER)) 
			reportQuery = Publisher.SEARCH_PUB_ORDERBY_NAME_ASC;
		else if (sortBy.equals(Publisher.SORT_BY_EMAIL)) 
			reportQuery = Publisher.SEARCH_PUB_ORDERBY_EMAIL_ASC;
		 
		pager = new PersistencePager<Publisher>(
				Publisher.COUNT_PUBLISHERS, 
				resultsPerPage, 
				reportQuery
		);
		
		initGoToPage();
		LogManager.info(PublisherBean.class, pager.getMaxResults() + " results");
	}
	
	private void initGoToPage() {
		pages = new ArrayList<SelectItem>();
		for (int p=0 ; p<pager.getMaxPages() ; p++) {
			pages.add(new SelectItem(p, String.valueOf(p+1)));
		}
	}
	
	public List<Publisher> getPublisherList() {
		LogManager.info(PublisherBean.class, "Acquiring Publishers...");
		publisherList = pager.getCurrentResults();
		return publisherList;
	}
	
	public void setPagerValues(ActionEvent event) {
		LogManager.info(SystemUserBean.class, "Set pager values.");
		resultsPerPage = pager.getPageSize();
		goToPage = pager.getCurrentPage();
		sortBy = tmpSortBy;
	}
	
	public void setSortBy(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Sort by " + (String) event.getNewValue());
		tmpSortBy = sortBy = (String) event.getNewValue();
		initPaging();
	}
	
	public void setCurrentPage(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Go to page " + (Integer) event.getNewValue());
		pager.setCurrentPage((Integer) event.getNewValue());
		goToPage = pager.getCurrentPage();
	}
	
	public void setPageSize(ValueChangeEvent event) {
		LogManager.info(SystemUserBean.class, "Set size " + (Integer) event.getNewValue());
		pager.setPageSize((Integer) event.getNewValue());
		initGoToPage();
	}
	
	public void nextPage(ActionEvent event) {
		pager.next();
		goToPage = pager.getCurrentPage();
	}
	
	public void previousPage(ActionEvent event) {
		pager.previous();
		goToPage = pager.getCurrentPage();
	}
	
	public void firstPage(ActionEvent event) {
		pager.first();
		goToPage = pager.getCurrentPage();
	}
	
	public void lastPage(ActionEvent event) {
		pager.last();
		goToPage = pager.getCurrentPage();
	}

	public void setPublisherList(ArrayList<Publisher> publisherList) {
		this.publisherList = publisherList;
	}

	public String getPagingText() {
		pagingText = "Page " + (pager.getCurrentPage()+1) + " of " + pager.getMaxPages();
		return pagingText;
	}

	public void setPagingText(String pagingText) {
		this.pagingText = pagingText;
	}

	public int getGoToPage() {
		return goToPage;
	}

	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public ArrayList<SelectItem> getPages() {
		return pages;
	}

	public void setPages(ArrayList<SelectItem> pages) {
		this.pages = pages;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
}
