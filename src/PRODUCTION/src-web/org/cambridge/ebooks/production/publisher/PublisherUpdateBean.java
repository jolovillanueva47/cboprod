package org.cambridge.ebooks.production.publisher;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.publisher.PublisherIsbn;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.LogManager;
import org.cambridge.ebooks.production.util.MailManager;
import org.cambridge.ebooks.production.util.PersistencePager;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.util.Misc;


public class PublisherUpdateBean {
	
	private static final int STANDARD_ISBN_LENGTH = 13;
	private static final int MINIMUM_ISBN_LENGTH = 2;
	private static final String WILDCARD_CHAR = "*";
	
	class PublisherUpdateException extends ValidatorException {

		private static final long serialVersionUID = 1L;

		PublisherUpdateException(FacesMessage facesMessage) {
			super(facesMessage);
		}
	}
	
	private PersistencePager<PublisherIsbn> pager;
	
	private String pagingText;
	private int goToPage;
	private int resultsPerPage;
	private ArrayList<SelectItem> pages;
	private String sortBy;
	private String tmpSortBy;
	
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static final String DELETE_SUCCESSFUL = "Publisher has been successfully deleted.";
	public static final String DELETE_ISBN_SUCCESSFUL = "The following ISBNs have been successfully deleted.";
	public static final String DELETE_FLAG = "N";
	public static final String ACTIVE_FLAG = "Y";
	public static final String EXCLUDE_FLAG = "Y";
	public static final String INCLUDE_FLAG = "N";
	
	private static final String NO_PUBLISHER_NAME = "Please enter Publisher Name.";
	private static final String NO_PUBLISHER_ISBN = "Please enter an ISBN.";
	private static final String NO_EMAIL = "Please enter Email Address.";
	
	private static final String INVALID_ISBN = "Invalid ISBN.";
	
	public static final String PUBLISHER_CONSTRAINT = "IDX_EBOOK_PUB_DETAILS_NAME";
	public static final String EMAIL_CONSTRAINT = "IDX_EBOOK_PUB_DETAILS_EMAIL";
	public static final String ISBN_CONSTRAINT = "IDX_EBOOK_PUB_ID_ISBN";
	
	private static final String PUBLISHER_EXISTS = "Publisher name already exists.";
	private static final String INVALID_EMAIL = "Email is invalid.";
	private static final String EMAIL_EXISTS = "Email already exists.";
	private static final String ISBN_EXISTS = "ISBN already exists.";
	
	private static final String CREATE_SUCCESSFUL = "Publisher has been successfully created.";
	private static final String UPDATE_SUCCESSFUL = "Publisher details have been successfully updated.";
	private static final String UPDATE = "Update";
	private static final String CREATE = "Save";
	
	private String updateType;
	
	private UIInput uiPublisherName;
	private UIInput uiEmail;
	private UIInput uiPubIsbn;
	private UIInput uiPubIsbns;
	private UIInput isbnUi;
	private UIInput isbnsUi;
	
	private Publisher publisher = new Publisher();
	private List<PublisherIsbn> publisherIsbnList = new ArrayList<PublisherIsbn>();
	
	private boolean hasErrors;
	private boolean excludeAllIsbns;
	
	private String publisherId;
	
	private String errorMessage;
	
	public List<PublisherIsbn> deleteIsbnList = new ArrayList<PublisherIsbn>();
	public Set<String> deleteSetIsbns = new HashSet<String>();
	
	public List<PublisherIsbn> excludeIsbnList = new ArrayList<PublisherIsbn>();
	public Set<String> excludeSetIsbns = new HashSet<String>();

	public void initNewPublisher(ActionEvent event) {
		initFormValues();
		publisher = new Publisher();
		publisherIsbnList = new ArrayList<PublisherIsbn>();
		updateType = CREATE;
	}
	
	public void resetPager(){
	
	}
	
	public void initPublisher(ActionEvent event) {
		initFormValues();
		updateType = UPDATE;
		publisherId = (String) event.getComponent().getAttributes().get("publisherId");
		publisher = PersistenceUtil.searchEntity(new Publisher(), Publisher.SEARCH_PUBLISHER_BY_ID, publisherId);
		initPaging();
	}
	
	public void initFormValues() {
		
		if (isbnUi != null) 
			isbnUi.resetValue();
		
		if (isbnsUi != null)
			isbnsUi.resetValue();
		
		if (uiPubIsbn != null) 
			uiPubIsbn.resetValue();
		
		if (uiPubIsbns != null)
			uiPubIsbns.resetValue();
		
		if (uiPublisherName != null) 
			uiPublisherName.resetValue();
		
		if (uiEmail != null)
			uiEmail.resetValue();

	}
	
	public void validateDetails(FacesContext context, UIComponent component, Object value) {
		LogManager.info(PublisherUpdateBean.class, "Publisher detail validating...");
		
		
		String publisherName = (String) uiPublisherName.getValue();
		String email = (String) uiEmail.getValue();
		
		boolean emptyPublisherName = Misc.isEmpty(publisherName);
		boolean emptyEmail = Misc.isEmpty(email);
		boolean validEmail = MailManager.isValidEmailAddress(email);
		
		Publisher pub = null;
		if(!emptyPublisherName && !emptyEmail && validEmail)
		{
			pub = PersistenceUtil.searchEntity(new Publisher(), Publisher.SEARCH_PUBLISHER_BY_NAME_OR_EMAIL, publisherName, email);
			System.out.println(pub);
		}
			
		if(pub != null)
		{
			pub.setName(publisherName);
			pub.setEmail(email);
			publisher = pub;
		}
		else
		{
			if (emptyPublisherName) 
			{
				
				FacesMessage message = new FacesMessage(NO_PUBLISHER_NAME);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage("alertMessage", message);
				throw new PublisherUpdateException(message);	
			}
					
			if (emptyEmail) 
			{
				FacesMessage message = new FacesMessage(NO_EMAIL);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage("alertMessage", message);
				throw new PublisherUpdateException(message);	
			}
		
			if (!validEmail) 
			{
				FacesMessage message = new FacesMessage(INVALID_EMAIL);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage("alertMessage", message);
				throw new PublisherUpdateException(message);
			}
		}
			
		String isbn = (String)((isbnUi.getValue() == null || "".equals(isbnUi.getValue())) ? uiPubIsbn.getValue() : isbnUi.getValue());
		String isbns = (String)((isbnsUi.getValue() == null || "".equals(isbnsUi.getValue())) ? uiPubIsbns.getValue() : isbnsUi.getValue());
		boolean isCreate = updateType.equals(CREATE);
		boolean emptyIsbn = isbn == null || "".equals(isbn);
		boolean emptyIsbns = isbns == null || "".equals(isbns);
		
		String[] isbnArr = emptyIsbns ? null : isbns.split(",");
		emptyIsbns = Misc.isEmpty(isbnArr);
		
		if(isCreate)
		{	
			if (emptyIsbn && emptyIsbns) 
			{
				FacesMessage message = new FacesMessage(NO_PUBLISHER_ISBN);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage("alertMessage", message);
				throw new PublisherUpdateException(message);	
			}
		}
		
		if(!emptyIsbn)
		{
			String errorMessage = isISBNValid(isbn);
			if(!"".equals(errorMessage))
			{
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage("alertMessage", message);
				throw new PublisherUpdateException(message);	
			}
		}

		if(isbnArr != null && isbnArr.length > 0)
		{
			String errorMessage = "";
			for(String isbnToValidate : isbnArr)
			{
				errorMessage = isISBNValid(Misc.ifNullOrBlank(isbnToValidate, "").trim());
				if(!"".equals(errorMessage))
				{
					FacesMessage message = new FacesMessage(errorMessage);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					throw new PublisherUpdateException(message);	
				}
			}
		}
		
		
	}
	
	public String updatePublisherNav() {
		return hasErrors ? RedirectTo.PUBLISHER_UPDATE : (
			   updateType.equals(UPDATE) ? RedirectTo.PUBLISHER_UPDATE : RedirectTo.PUBLISHERS);
		//return RedirectTo.PUBLISHER_UPDATE;
	}
	
	
	public void addIsbnToList(ValueChangeEvent event) {
		String isbnCtr = (String)event.getComponent().getAttributes().get("isbnCounter");
		String isbn = (String)event.getComponent().getAttributes().get("isbn");
		
		if ((Boolean)event.getNewValue()) {
			if (deleteSetIsbns.add(isbnCtr)) {
				PublisherIsbn pubIsbn = new PublisherIsbn();
				pubIsbn.setIsbnCounter(isbnCtr);
				pubIsbn.setIsbn(isbn);
				pubIsbn.setActive(PublisherDeleteBean.DELETE_FLAG);
				deleteIsbnList.add(pubIsbn);
			}
		}
	}
	
	public void excludeIsbn(ValueChangeEvent event) {
		String isbnCtr = (String)event.getComponent().getAttributes().get("isbnCounter");
		String isbn = (String)event.getComponent().getAttributes().get("isbn");
		boolean newValue = (Boolean)event.getNewValue();
		
		if (excludeSetIsbns.add(isbnCtr)) 
		{
			PublisherIsbn pubIsbn = new PublisherIsbn();
			pubIsbn.setIsbnCounter(isbnCtr);
			pubIsbn.setIsbn(isbn);
			pubIsbn.setExclude(newValue ? "Y" : "N");
			excludeIsbnList.add(pubIsbn);
		}
		
	}
	
	public String cancelUpdate(){
		clearDeleteList();
		clearExcludeListList();
		return RedirectTo.PUBLISHERS;
	}
	
	public void deletePublisherIsbn() {
		EntityManager em = null;
		try {
			Set<String> idList = deleteSetIsbns;
			if(idList != null && !idList.isEmpty())
			{
				LogManager.info(PublisherUpdateBean.class, "Removing " + idList.size() + " publisher isbn/s...");
				
				em = emf.createEntityManager();
				em.getTransaction().begin();		
	
				//User currentUser = HttpUtil.getUserFromCurrentSession();
				for (String s : idList) 
				{		
					PublisherIsbn pubIsbn = em.find(PublisherIsbn.class, s);
					pubIsbn.setActive(PublisherUpdateBean.DELETE_FLAG);
					
					//TODO: create user audit trail
					//EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Delete publisher - ".concat(publisher.getName()));
					
					em.merge(pubIsbn);
				}
				em.getTransaction().commit();
				
				
				clearDeleteList();
	
				LogManager.info(PublisherDeleteBean.class, "Publisher Isbn/s deleted.");
			}
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public void excludePublisherIsbn() {
		EntityManager em = null;
		try {
			List<PublisherIsbn> list = excludeIsbnList;
			if(list != null && !list.isEmpty())
			{
				LogManager.info(PublisherUpdateBean.class, "Excluding/Including " + list.size() + " publisher isbn/s...");
				
				em = emf.createEntityManager();
				em.getTransaction().begin();		
	
				//User currentUser = HttpUtil.getUserFromCurrentSession();
				for (PublisherIsbn pubIsbns : list) 
				{		
					PublisherIsbn pubIsbn = em.find(PublisherIsbn.class, pubIsbns.getIsbnCounter());
					pubIsbn.setExclude(pubIsbns.getExclude());
					
					//TODO: create user audit trail
					//EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Delete publisher - ".concat(publisher.getName()));
					
					em.merge(pubIsbn);
				}
				em.getTransaction().commit();
				
				clearExcludeListList();
	
				LogManager.info(PublisherUpdateBean.class, "Publisher Isbn/s excluded.");
			}
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public void updatePublisher(ActionEvent event) {
		LogManager.info(PublisherUpdateBean.class, updateType + " publisher...");
		hasErrors = false;
		
		EntityManager em = emf.createEntityManager();
		
		Publisher publisherUpdate = null;
		String pubId = publisher.getPublisherId();
		if (updateType.equals(CREATE) && pubId == null)
		{
			System.out.println("pubname: " + uiPublisherName.getValue());
			publisherUpdate = publisher;
		} 
		else if (updateType.equals(UPDATE) || pubId != null) 
		{
			publisherUpdate = em.find(Publisher.class, publisher.getPublisherId());
			publisherUpdate.setName(publisher.getName());
			publisherUpdate.setEmail(publisher.getEmail());
		}
		
		User currentUser = HttpUtil.getUserFromCurrentSession();
		Date today = new Date();
		
		publisherUpdate.setModifiedDate(today);
		publisherUpdate.setModifiedBy(currentUser.getUsername());
		publisherUpdate.setActive(PublisherUpdateBean.ACTIVE_FLAG);
		
		try {
			
			em.getTransaction().begin();
			publisherUpdate = em.merge(publisherUpdate);
			em.getTransaction().commit();
	
			em.getTransaction().begin();
			String publisherId = publisherUpdate.getPublisherId();
			
			Set<String> isbnSet = new HashSet<String>();
			
			if(Misc.isNotEmpty(publisher.getIsbn()))
				isbnSet.add(publisher.getIsbn());
			
			if(Misc.isNotEmpty(publisher.getIsbns()))
				for(String isbn : publisher.getIsbns().split(","))
					isbnSet.add(isbn);
			
			for (String isbn : isbnSet) 
			{
				isbn = Misc.ifNullOrBlank("", isbn).trim();
				if(!Misc.isEmpty(isbn))
				{
					//create user audit trail
					//EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Delete user - ".concat(user.getUsername()));
								
					PublisherIsbn pubIsbn = PersistenceUtil.searchEntity(new PublisherIsbn(), PublisherIsbn.SEARCH_BY_ISBN_PUBLISHER_ID, isbn, publisherId);
					if(pubIsbn != null)
					{
						pubIsbn.setPublisherId(publisherId);
						pubIsbn.setIsbn(isbn);
						pubIsbn.setActive(PublisherUpdateBean.ACTIVE_FLAG);
						pubIsbn.setExclude(excludeAllIsbns ? PublisherUpdateBean.EXCLUDE_FLAG : PublisherUpdateBean.INCLUDE_FLAG);
						
						em.merge(pubIsbn);
					}
					else
					{
						pubIsbn = new PublisherIsbn();
						pubIsbn.setPublisherId(publisherId);
						pubIsbn.setIsbn(isbn);
						pubIsbn.setActive(PublisherUpdateBean.ACTIVE_FLAG);
						pubIsbn.setExclude(excludeAllIsbns ? PublisherUpdateBean.EXCLUDE_FLAG : PublisherUpdateBean.INCLUDE_FLAG);
						
						em.merge(pubIsbn);
					}

				}
			}	
		
			excludeAllIsbns = false;	
			em.getTransaction().commit();
			
			LogManager.info(PublisherUpdateBean.class, " Publisher persisted.");
			

			deletePublisherIsbn();
			excludePublisherIsbn();
			
			//create user audit trail
//			if(updateType.equals(CREATE)){
//				EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Add new user - ".concat((String) userUpdate.getUsername()));
//			}else{
//				EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Update user - ".concat(userUpdate.getUsername()));
//			}
			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = null;
			if (updateType.equals(CREATE)) 
			{
				message = new FacesMessage(CREATE_SUCCESSFUL);
				//TODO: SystemUserMailer.sendMail(userUpdate);
			} 
			else if (updateType.equals(UPDATE)) 
			{
				clearFields();
				initPaging();
				message = new FacesMessage(UPDATE_SUCCESSFUL);
			}
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			LogManager.info(PublisherUpdateBean.class, updateType + "_Publisher complete: " + publisherUpdate.getName());
			
		} 
		catch (PersistenceException pe) 
		{
			hasErrors = true;
			String errorMessage = pe.getCause().getMessage();
			LogManager.error(PublisherUpdateBean.class, "****************\n" + errorMessage + "\n****************");
			if (errorMessage.contains("ORA-00001")) 
			{
				
				if (errorMessage.contains(PublisherUpdateBean.PUBLISHER_CONSTRAINT)) 
				{
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage message = new FacesMessage(PUBLISHER_EXISTS);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					LogManager.error(PublisherUpdateBean.class, "Caught SQLException: " + PUBLISHER_EXISTS);
				} 
				else if (errorMessage.contains(PublisherUpdateBean.EMAIL_CONSTRAINT)) 
				{
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage message = new FacesMessage(EMAIL_EXISTS);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					LogManager.error(PublisherUpdateBean.class, "Caught SQLException: " + EMAIL_EXISTS);
				}
				else if (errorMessage.contains(PublisherUpdateBean.ISBN_CONSTRAINT)) 
				{
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage message = new FacesMessage(ISBN_EXISTS);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("alertMessage", message);
					LogManager.error(PublisherUpdateBean.class, "Caught SQLException: " + EMAIL_EXISTS);
				}
			}

		} 
		finally 
		{
			JPAUtil.close(em);
		}
	}
	
	
	private void clearDeleteList() {
		deleteIsbnList = new ArrayList<PublisherIsbn>();
		deleteSetIsbns = new HashSet<String>();
	}
	
	private void clearExcludeListList() {
		excludeIsbnList = new ArrayList<PublisherIsbn>();
		excludeSetIsbns = new HashSet<String>();
	}
	
	private void initGoToPage() {
		pages = new ArrayList<SelectItem>();
		for (int p=0 ; p<pager.getMaxPages() ; p++) {
			pages.add(new SelectItem(p, String.valueOf(p+1)));
		}
	}
	
	public void initPaging() {
		goToPage = 0;
		resultsPerPage = 10;
		//pager = new ListPager<PublisherIsbn>(resultsPerPage, publisher.getPublisherIsbn());
		pager = new PersistencePager<PublisherIsbn>(
				PublisherIsbn.COUNT_ISBN, 
				resultsPerPage, 
				PublisherIsbn.SELECT_ACTIVE_ISBN_BY_PUB_ID, 
				publisherId
		);
		initGoToPage();
		LogManager.info(PublisherBean.class, pager.getMaxResults() + " results");
	}
		
	public void setPagerValues(ActionEvent event) {
		LogManager.info(PublisherUpdateBean.class, "Set pager values.");
		resultsPerPage = pager.getPageSize();
		goToPage = pager.getCurrentPage();
		sortBy = tmpSortBy;
	}
	
	public void setSortBy(ValueChangeEvent event) {
		LogManager.info(PublisherUpdateBean.class, "Sort by " + (String) event.getNewValue());
		tmpSortBy = sortBy = (String) event.getNewValue();
		initPaging();
	}
	
	public void setCurrentPage(ValueChangeEvent event) {
		LogManager.info(PublisherUpdateBean.class, "Go to page " + (Integer) event.getNewValue());
		pager.setCurrentPage((Integer) event.getNewValue());
		goToPage = pager.getCurrentPage();
	}
	
	public void setPageSize(ValueChangeEvent event) {
		LogManager.info(PublisherUpdateBean.class, "Set size " + (Integer) event.getNewValue());
		pager.setPageSize((Integer) event.getNewValue());
		initGoToPage();
	}
	
	public void nextPage(ActionEvent event) {
		pager.next();
		goToPage = pager.getCurrentPage();
	}
	
	public void previousPage(ActionEvent event) {
		pager.previous();
		goToPage = pager.getCurrentPage();
	}
	
	public void firstPage(ActionEvent event) {
		pager.first();
		goToPage = pager.getCurrentPage();
	}
	
	public void lastPage(ActionEvent event) {
		pager.last();
		goToPage = pager.getCurrentPage();
	}
	
	public String getPagingText() {
		pagingText = "Page " + (pager.getCurrentPage()+1) + " of " + pager.getMaxPages();
		return pagingText;
	}

	public void setPagingText(String pagingText) {
		this.pagingText = pagingText;
	}

	public int getGoToPage() {
		return goToPage;
	}

	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public ArrayList<SelectItem> getPages() {
		return pages;
	}

	public void setPages(ArrayList<SelectItem> pages) {
		this.pages = pages;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public UIInput getUiPublisherName() {
		return uiPublisherName;
	}

	public void setUiPublisherName(UIInput uiPublisherName) {
		this.uiPublisherName = uiPublisherName;
	}

	public UIInput getUiEmail() {
		return uiEmail;
	}

	public void setUiEmail(UIInput uiEmail) {
		this.uiEmail = uiEmail;
	}


	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}



	public List<PublisherIsbn> getPublisherIsbnList() {
		publisherIsbnList = updateType.equals(CREATE) ? new ArrayList<PublisherIsbn>() : pager.getCurrentResults();
		return publisherIsbnList;
	}

	public void setPublisherIsbnList(List<PublisherIsbn> publisherIsbnList) {
		this.publisherIsbnList = publisherIsbnList;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public UIInput getUiPubIsbn() {
		return uiPubIsbn;
	}

	public void setUiPubIsbn(UIInput uiPubIsbn) {
		this.uiPubIsbn = uiPubIsbn;
	}

	public UIInput getUiPubIsbns() {
		return uiPubIsbns;
	}

	public void setUiPubIsbns(UIInput uiPubIsbns) {
		this.uiPubIsbns = uiPubIsbns;
	}

	public UIInput getIsbnUi() {
		return isbnUi;
	}

	public void setIsbnUi(UIInput isbnUi) {
		this.isbnUi = isbnUi;
	}

	public UIInput getIsbnsUi() {
		return isbnsUi;
	}

	public void setIsbnsUi(UIInput isbnsUi) {
		this.isbnsUi = isbnsUi;
	}
	
	public boolean isExcludeAllIsbns() {
		return excludeAllIsbns;
	}

	public void setExcludeAllIsbns(boolean excludeAllIsbns) {
		this.excludeAllIsbns = excludeAllIsbns;
	}
	
	private void clearFields(){
		publisher.setIsbn("");
		publisher.setIsbns("");
	}
	
	private static String isISBNValid(String isbn){
		
		String message = "";
		final String INVALID_ISBN_FORMAT = "Invalid ISBN format.";
		final String INVALID_ISBN_LENGTH = "Invalid ISBN length. Standard ISBN length is 13.";
		
		if(isbn != null && isbn.length() >= MINIMUM_ISBN_LENGTH)
		{
			int isbnLength = isbn.length();
			Pattern p = Pattern.compile("[^a-zA-Z0-9*]");
			Matcher m = p.matcher(isbn);
			boolean containsSpecialChar = m.find();
			
			if(containsSpecialChar)
			{
				message = INVALID_ISBN_FORMAT + " " + isbn;
			}
			else
			{
				if(isbn.contains(WILDCARD_CHAR))
				{
					if(isbn.indexOf(WILDCARD_CHAR) == (isbnLength-1))
					{
						if(isbn.length() > STANDARD_ISBN_LENGTH)
							message = INVALID_ISBN_LENGTH + " " + isbn;
					}
					else
						message = INVALID_ISBN_FORMAT + " " + isbn;	
				}
				else
				{
					if(isbn.length() != STANDARD_ISBN_LENGTH)
						message = INVALID_ISBN_LENGTH + " " + isbn;
				}
			}
		}
		
		return message;
	}


}
