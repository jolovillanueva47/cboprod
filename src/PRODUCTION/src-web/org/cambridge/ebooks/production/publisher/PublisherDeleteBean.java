package org.cambridge.ebooks.production.publisher;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

public class PublisherDeleteBean {
	private static final Log4JLogger LogManager = new Log4JLogger(PublisherDeleteBean.class);
	public static final String DELETE_SUCCESSFUL = "Publisher has been successfully deleted.";
	public static final String DELETE_FLAG = "N";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );
	
	public List<Publisher> deleteList = new ArrayList<Publisher>();
	public Set<String> deleteSetIds = new HashSet<String>();
		
	public String deletePublishersNav() {
		return RedirectTo.PUBLISHER_DELETE;
	}
	
	public void addToList(ValueChangeEvent event) {
		String publisherId = (String) event.getComponent().getAttributes().get("publisherId");
		String name = (String) event.getComponent().getAttributes().get("name");
		String email = (String) event.getComponent().getAttributes().get("email");
		if ((Boolean)event.getNewValue()) {
			if (deleteSetIds.add(publisherId)) {
				Publisher publisher = new Publisher();
				publisher.setPublisherId(publisherId);
				publisher.setName(name);
				publisher.setEmail(email);
				publisher.setActive(PublisherDeleteBean.DELETE_FLAG);
				deleteList.add(publisher);
			}
		}
	}
	
	public void confirmDelete(ActionEvent event) {
		LogManager.info(PublisherDeleteBean.class, "Attempting to delete " + deleteList.size() + " publisher/s...");
	}
	
	@SuppressWarnings("unchecked")
	public void deletePublishers(ActionEvent event) {
		Set<String> idList = (HashSet<String>) event.getComponent().getAttributes().get("deleteSetIds");
		LogManager.info(PublisherDeleteBean.class, "Removing " + idList.size() + " publisher/s...");
		
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();		
	
			for (String s : idList) 
			{		
				Publisher publisher = em.find(Publisher.class, s);
				publisher.setActive(PublisherDeleteBean.DELETE_FLAG);
				
				//TODO: create user audit trail
				//EventTracker.sendEvent( AuditTrail.MANAGE_SYSTEM_USERS, currentUser.getUserId(), "Delete publisher - ".concat(publisher.getName()));
				
				String publisherId = publisher.getPublisherId();
				StringBuilder update = new StringBuilder();
				update.append("BEGIN ");
				update.append("UPDATE ebook_publisher_isbn SET active = 'N' WHERE publisher_id = '" + publisherId + "'; ");
				update.append("DELETE FROM cbo_cupadmin_ebook_access WHERE user_id IN (SELECT user_id FROM ebook_publisher_member WHERE publisher_id = '" + publisherId + "'); ");
				update.append("DELETE FROM ebook_publisher_member WHERE publisher_id = '" + publisherId + "'; ");
				update.append("END; ");
				
				Query query = em.createNativeQuery(update.toString());
				query.executeUpdate();
					
				em.merge(publisher);
			}
			
			em.getTransaction().commit();
			
			clearDeleteList();
			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(DELETE_SUCCESSFUL);
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			LogManager.info(PublisherDeleteBean.class, "Publisher deleted.");
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
			
	public void cancelDelete(ActionEvent event) {
		clearDeleteList();
	}
	
	private void clearDeleteList() {
		deleteList = new ArrayList<Publisher>();
		deleteSetIds = new HashSet<String>();
	}
	
	public String redirectToPublishers() {
		return RedirectTo.PUBLISHERS;
	}

	public boolean getDeleteListEmpty() {
		return deleteList == null || deleteList.size() == 0;
	}

	public List<Publisher> getDeleteList() {
		return deleteList;
	}

	public void setDeleteList(ArrayList<Publisher> deleteList) {
		this.deleteList = deleteList;
	}

	public Set<String> getDeleteSetIds() {
		return deleteSetIds;
	}

	public void setDeleteSetIds(HashSet<String> deleteSetIds) {
		this.deleteSetIds = deleteSetIds;
	}
}
