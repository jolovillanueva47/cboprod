package org.cambridge.ebooks.production.constant;

/**
 * 
 * @author jgalang
 * @modified rvillamor 03/04/10
 */

public class SessionAttribute {
	public static final String USER = "userInfo";
	public static final String LOGIN_TIME = "loginTime";
	public static final String EBOOK_ACCESS = "ebookAccess";
	public static final String PUBLISHER = "publisherInfo";
	public static final String SRPUBLISHER = "srPublisherInfo";
}
