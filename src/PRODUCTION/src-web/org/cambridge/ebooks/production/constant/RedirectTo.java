package org.cambridge.ebooks.production.constant;

/**
 * 
 * @author jgalang
 * 
 */

public class RedirectTo {
	public static final String LOGIN_SUCCESS = "login_success";
	public static final String LOGIN_FAIL = "login_fail";
	public static final String LOGOUT = "logout";
	
	public static final String SYSTEM_USERS = "system_users";
	public static final String SYSTEM_USERS_UPDATE = "system_users_update";
	public static final String SYSTEM_USERS_DELETE = "system_users_delete";
	
	public static final String THIRD_PARTY = "third_party";
	public static final String THIRD_PARTY_UPDATE = "third_party_update";
	public static final String THIRD_PARTY_DELETE = "third_party_delete";
	
	public static final String PASSWORD_UPDATE_SUCCESS = "password_update_success";
	public static final String PASSWORD_UPDATE_FAIL = "password_update_fail";
	
	public static final String EBOOK_DETAILS = "ebook_details";
	public static final String EBOOK_CHAPTER = "ebook_chapter";
	public static final String EBOOK_CHAPTER_DETAILS = "ebook_chapter_details";
	
	public static final String ARTICLE_AUDIT_TRAIL_UPDATE = "article_audittrail";
	public static final String USER_AUDIT_TRAIL_UPDATE = "user_audittrail";
	
	public static final String BOOK_AUDIT_TRAIL_UPDATE = "book_audittrail";
	public static final String EBOOK_AUDIT_TRAIL_UPDATE = "ebook_audittrail";
	
	public static final String PUBLISHERS = "publishers";
	public static final String PUBLISHER_UPDATE = "publisher_update";
	public static final String PUBLISHER_DELETE = "publisher_delete";
	
	public static final String VALID_IP = "valid_ip";
}