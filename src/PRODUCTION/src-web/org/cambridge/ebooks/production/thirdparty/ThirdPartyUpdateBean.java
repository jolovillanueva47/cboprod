package org.cambridge.ebooks.production.thirdparty;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectBoolean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdParty;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdPartyContent;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.user.SystemUserUpdateBean;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.Password;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.util.Misc;


/**
 * 
 * @author jgalang
 * 
 */

public class ThirdPartyUpdateBean {
	private static final Log4JLogger LogManager = new Log4JLogger(ThirdPartyUpdateBean.class);
	private static final String NO_NAME = "Please enter Name.";
	private static final String NO_USERNAME = "Please enter Username.";
	private static final String NO_PASSWORD = "Please enter Password.";
	private static final String NO_FTP_ADDRESS = "Please enter FTP Address.";
	private static final String NO_CONTENT_TYPE = "Please select at least one content type.";
	private static final String INVALID_DATE_FORMAT = "Invalid Inactive date (dd/MM/yyyy)";
	private static final String INVALID_DATE = "Inactive date should be greater than the current date.";
	private static final String CREATE_SUCCESSFUL = "Third party has been successfully created.";
	private static final String UPDATE_SUCCESSFUL = "Third party has been successfully updated.";
	private static final String CREATE = "Save";
	private static final String UPDATE = "Update";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());

	private UIInput uiName;
	private UIInput uiUsername;
	private UIInput uiPassword;
	private UIInput uiFTPAddress;
	private UIInput uiInactiveDate;
	private UISelectBoolean uiPDF;
	private UISelectBoolean uiSGML_H;
	private UISelectBoolean uiSGML_S;
	private UISelectBoolean uiXML_H;
	private UISelectBoolean uiXML_W;
	private UISelectBoolean uiHTML_W;
	private UISelectBoolean uiXML_R;
	
	private String displayPassword;//TODO: -C2- Remove obfuscating/deobfuscating procedures on new DB 
	private String updateType;

	private ThirdParty thirdParty = new ThirdParty();
	
	private boolean hasErrors;
	
	public void initNewThirdParty(ActionEvent event) {
		updateType = CREATE;
		thirdParty = new ThirdParty();
	}
	
	public void initThirdParty(ActionEvent event) {
		updateType = UPDATE;
		thirdParty = (ThirdParty) event.getComponent().getAttributes().get("thirdParty");
		thirdParty.setContent(new ThirdPartyContent(thirdParty.getThirdPartyContent()));
	}
	
	public void validateDetails(FacesContext context, UIComponent component, Object value) {
		LogManager.info(ThirdPartyUpdateBean.class, "Third party validating...");
		
		String name = (String) uiName.getValue();
		boolean emptyName = Misc.isEmpty(name);
		if (emptyName) {
			FacesMessage message = new FacesMessage(NO_NAME);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}

		String username = (String) uiUsername.getValue();
		boolean emptyUsername = Misc.isEmpty(username);
		if (emptyUsername) {
			FacesMessage message = new FacesMessage(NO_USERNAME);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
		
		String password = (String) uiPassword.getValue();
		boolean emptyPassword = Misc.isEmpty(password);
		if (emptyPassword) {
			FacesMessage message = new FacesMessage(NO_PASSWORD);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
		
		String ftpAddress = (String) uiFTPAddress.getValue();
		boolean emptyFTPAddress = Misc.isEmpty(ftpAddress);
		if (emptyFTPAddress) {
			FacesMessage message = new FacesMessage(NO_FTP_ADDRESS);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);	
		}
		
		String inactiveDate = (String) uiInactiveDate.getValue();
		if (inactiveDate.length() > 0) {
			boolean validDate = isValidDateFormat(inactiveDate);
			if (!validDate) {
				FacesMessage message = new FacesMessage(INVALID_DATE_FORMAT);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(message);
			}
			
			boolean futureDate = isFutureDate(inactiveDate);
			if (!futureDate) {
				FacesMessage message = new FacesMessage(INVALID_DATE);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(message);
			}
		}
		
		boolean noContentType = !hasContentType();
		if (noContentType) {
			FacesMessage message = new FacesMessage(NO_CONTENT_TYPE);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
		
		
		LogManager.info(ThirdPartyUpdateBean.class, "Validation complete.");
	}
	
	private boolean hasContentType() {
		if ((Boolean) uiPDF.getValue()) return true;
		if ((Boolean) uiSGML_H.getValue()) return true;
		if ((Boolean) uiSGML_S.getValue()) return true;
		if ((Boolean) uiXML_H.getValue()) return true;
		if ((Boolean) uiXML_W.getValue()) return true;
		if ((Boolean) uiHTML_W.getValue()) return true;
		if ((Boolean) uiXML_R.getValue()) return true;
		return false;
	}
	
	private boolean isValidDateFormat(String date) {
		DateFormat df = new SimpleDateFormat(ThirdParty.DATE_FORMAT);
		try {
			df.parse(date);
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}
	
	private boolean isFutureDate(String dateString) {
		boolean future = false;
		DateFormat df = new SimpleDateFormat(ThirdParty.DATE_FORMAT);
		try {
			Date date = df.parse(dateString);
			if (date.compareTo(new Date()) > 0) future = true;
		} catch (ParseException pe) {
			LogManager.error(ThirdPartyUpdateBean.class, "The format should have been validated by now!");
		}
		return future;
	}
	
	public void updateThirdParty(ActionEvent event) {
		LogManager.info(SystemUserUpdateBean.class, updateType + " third party...");
		hasErrors = false;
		
		EntityManager em = emf.createEntityManager();
		ThirdParty updateThirdParty = null;
		if (updateType.equals(CREATE)) {
			updateThirdParty = thirdParty;
		} else if (updateType.equals(UPDATE)) {
			updateThirdParty = em.find(ThirdParty.class, thirdParty.getThirdPartyId());
			updateThirdParty.setThirdPartyActive(thirdParty.getThirdPartyActive());
			updateThirdParty.setThirdPartyCompression(thirdParty.getThirdPartyCompression());
			updateThirdParty.setThirdPartyDeliveryType(thirdParty.getThirdPartyDeliveryType());
			updateThirdParty.setThirdPartyEmail(thirdParty.getThirdPartyEmail());
			updateThirdParty.setThirdPartyFolder(thirdParty.getThirdPartyFolder());
			updateThirdParty.setDisplayInactiveDate(thirdParty.getDisplayInactiveDate());
			updateThirdParty.setThirdPartyName(thirdParty.getThirdPartyName());
			updateThirdParty.setThirdPartyUsername(thirdParty.getThirdPartyUsername());
			updateThirdParty.setThirdPartyWebsite(thirdParty.getThirdPartyWebsite());
		}
		
		User currentUser = HttpUtil.getUserFromCurrentSession();
		Date today = new Date();
		
		updateThirdParty.setThirdPartyPassword(Password.obfuscate(displayPassword));
		updateThirdParty.setThirdPartyModifiedDate(today);
		updateThirdParty.setThirdPartyModifiedBy(currentUser.getUsername());
		updateThirdParty.setThirdPartyContent(thirdParty.getContent().getContentAsString());
		
		try {
			
			em.getTransaction().begin();
			em.merge(updateThirdParty);
			em.getTransaction().commit();

			LogManager.info(ThirdPartyUpdateBean.class, "User persisted. Persisting access...");

			//TODO: -C2- ebooks for delivery
			
//			em.getTransaction().begin();
//			em.merge(ca);
//			em.getTransaction().commit();


			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = null;
			if (updateType.equals(CREATE)) {
				message = new FacesMessage(CREATE_SUCCESSFUL);
			} else if (updateType.equals(UPDATE)) {
				message = new FacesMessage(UPDATE_SUCCESSFUL);
			}
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			LogManager.info(ThirdPartyUpdateBean.class, updateType + " Third Party complete: " + thirdParty.getThirdPartyUsername());
			
		} catch (PersistenceException pe) {
			hasErrors = true;
			pe.printStackTrace();
		} finally {
			em.close();
		}
	}
	
	public String updateThirdPartyNav() {
		return hasErrors ? RedirectTo.THIRD_PARTY_UPDATE: RedirectTo.THIRD_PARTY;
	}
	
	public String getDisplayPassword() {
		displayPassword = Misc.isEmpty(thirdParty.getThirdPartyPassword()) ? "" : Password.deobfuscate(thirdParty.getThirdPartyPassword());
		return displayPassword;
	}

	public void setDisplayPassword(String displayPassword) {
		this.displayPassword = displayPassword;
	}

	public ThirdParty getThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(ThirdParty thirdParty) {
		this.thirdParty = thirdParty;
	}

	public String getUpdateType() {
		return updateType;
	}

	public UIInput getUiName() {
		return uiName;
	}

	public void setUiName(UIInput uiName) {
		this.uiName = uiName;
	}

	public UIInput getUiUsername() {
		return uiUsername;
	}

	public void setUiUsername(UIInput uiUsername) {
		this.uiUsername = uiUsername;
	}

	public UIInput getUiPassword() {
		return uiPassword;
	}

	public void setUiPassword(UIInput uiPassword) {
		this.uiPassword = uiPassword;
	}

	public UIInput getUiFTPAddress() {
		return uiFTPAddress;
	}

	public void setUiFTPAddress(UIInput uiFTPAddress) {
		this.uiFTPAddress = uiFTPAddress;
	}

	public UIInput getUiInactiveDate() {
		return uiInactiveDate;
	}

	public void setUiInactiveDate(UIInput uiInactiveDate) {
		this.uiInactiveDate = uiInactiveDate;
	}

	public UISelectBoolean getUiPDF() {
		return uiPDF;
	}

	public void setUiPDF(UISelectBoolean uiPDF) {
		this.uiPDF = uiPDF;
	}

	public UISelectBoolean getUiSGML_H() {
		return uiSGML_H;
	}

	public void setUiSGML_H(UISelectBoolean uiSGML_H) {
		this.uiSGML_H = uiSGML_H;
	}

	public UISelectBoolean getUiSGML_S() {
		return uiSGML_S;
	}

	public void setUiSGML_S(UISelectBoolean uiSGML_S) {
		this.uiSGML_S = uiSGML_S;
	}

	public UISelectBoolean getUiXML_H() {
		return uiXML_H;
	}

	public void setUiXML_H(UISelectBoolean uiXML_H) {
		this.uiXML_H = uiXML_H;
	}

	public UISelectBoolean getUiXML_W() {
		return uiXML_W;
	}

	public void setUiXML_W(UISelectBoolean uiXML_W) {
		this.uiXML_W = uiXML_W;
	}

	public UISelectBoolean getUiHTML_W() {
		return uiHTML_W;
	}

	public void setUiHTML_W(UISelectBoolean uiHTML_W) {
		this.uiHTML_W = uiHTML_W;
	}

	public UISelectBoolean getUiXML_R() {
		return uiXML_R;
	}

	public void setUiXML_R(UISelectBoolean uiXML_R) {
		this.uiXML_R = uiXML_R;
	}
	
	
}
