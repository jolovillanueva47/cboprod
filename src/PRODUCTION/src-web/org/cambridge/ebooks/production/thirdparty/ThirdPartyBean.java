package org.cambridge.ebooks.production.thirdparty;

import java.util.ArrayList;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdParty;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;

/**
 * 
 * @author jgalang
 * 
 */

public class ThirdPartyBean {
	private static final Log4JLogger LogManager = new Log4JLogger(ThirdPartyBean.class);
	private ArrayList<ThirdParty> thirdPartyList;
	
	public String updateThirdParty() {
		return RedirectTo.THIRD_PARTY_UPDATE;
	}

	public ArrayList<ThirdParty> getThirdPartyList() {
		LogManager.info(ThirdPartyBean.class, "Acquiring Third Parties...");
		thirdPartyList = PersistenceUtil.searchList(new ThirdParty(), ThirdParty.SEARCH_ALL_THIRD_PARTIES, new String[]{});
		return thirdPartyList;
	}

	public void setThirdPartyList(ArrayList<ThirdParty> thirdPartyList) {
		this.thirdPartyList = thirdPartyList;
	}
}
