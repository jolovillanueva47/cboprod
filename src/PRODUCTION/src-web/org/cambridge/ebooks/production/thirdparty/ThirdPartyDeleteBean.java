package org.cambridge.ebooks.production.thirdparty;

import java.util.ArrayList;
import java.util.HashSet;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.jpa.thirdparty.ThirdParty;
import org.cambridge.ebooks.production.user.SystemUserDeleteBean;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * 
 * @author jgalang
 * 
 */

public class ThirdPartyDeleteBean {
	private static final Log4JLogger LogManager = new Log4JLogger(ThirdPartyDeleteBean.class);
	public static final String DELETE_SUCCESSFUL = "Third party has been successfully deleted.";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	private ArrayList<ThirdParty> deleteList = new ArrayList<ThirdParty>();
	
	public HashSet<Integer> deleteSetIds = new HashSet<Integer>();
	
	public String deleteThirdPartyNav() {
		return RedirectTo.THIRD_PARTY_DELETE;
	}
	
	public void addToList(ValueChangeEvent event) {
		ThirdParty tp = (ThirdParty) event.getComponent().getAttributes().get("thirdParty");
		if ((Boolean)event.getNewValue()) {
			if (deleteSetIds.add(tp.getThirdPartyId())) {
				deleteList.add(tp);
			}
		}
	}
	
	public void confirmDelete(ActionEvent event) {
		LogManager.info(SystemUserDeleteBean.class, "Attempting to delete " + deleteList.size() + " third party/ies...");
	}
	
	public void deleteThirdParty(ActionEvent event) {
		HashSet<Integer> idList = (HashSet<Integer>) event.getComponent().getAttributes().get("deleteSetIds");
		LogManager.info(SystemUserDeleteBean.class, "Removing " + idList.size() + " third party/ies...");
		
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			for (Integer s : idList) {
	//			CupadminAccess ca = em.find(CupadminAccess.class, s);
	//			em.remove(ca);//TODO: -C2- ebooks for delivery
				
				ThirdParty tp = em.find(ThirdParty.class, s);
				em.remove(tp);
			}
			em.getTransaction().commit();
			
			
			
			clearDeleteList();
			
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(DELETE_SUCCESSFUL);
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("alertMessage", message);
			LogManager.info(SystemUserDeleteBean.class, "User deleted.");
		}
		finally {
			JPAUtil.close(em);
		}
	}
	
	public void cancelDelete(ActionEvent event) {
		clearDeleteList();
	}
	
	private void clearDeleteList() {
		deleteList = new ArrayList<ThirdParty>();
		deleteSetIds = new HashSet<Integer>();
	}
	
	public String deleteThirdParty() {
		return RedirectTo.THIRD_PARTY;
	}

	public boolean getDeleteListEmpty() {
		return deleteList == null || deleteList.size() == 0;
	}

	public ArrayList<ThirdParty> getDeleteList() {
		return deleteList;
	}

	public void setDeleteList(ArrayList<ThirdParty> deleteList) {
		this.deleteList = deleteList;
	}

	public HashSet<Integer> getDeleteSetIds() {
		return deleteSetIds;
	}

	public void setDeleteSetIds(HashSet<Integer> deleteSetIds) {
		this.deleteSetIds = deleteSetIds;
	}
	
	
	
}
