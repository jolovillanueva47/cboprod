package org.cambridge.ebooks.production.counter;

public enum CounterLogType {
	R1 ("1", 1),
	R2 ("2", 2),
	R3 ("3", 3),
	R4 ("4", 4),
	R5 ("5", 5),
	R6 ("6", 6);	
	
	private final String stringQ;
	private final int numQ;
	
	CounterLogType(String str, int num){
		this.stringQ = str;
		this.numQ = num;
	}
	
	public String getStringQ(){
		return stringQ;
	}
	
	public int getNumQ(){
		return numQ;
	}
	
	public static void main(String[] args) {
		
	}
}
