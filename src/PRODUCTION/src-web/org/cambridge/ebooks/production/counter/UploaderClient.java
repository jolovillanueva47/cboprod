package org.cambridge.ebooks.production.counter;



import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.cambridge.ebooks.production.counter.ejb.jpa.CounterBean;
import org.cambridge.ebooks.production.counter.util.DateUtil;

public class UploaderClient {

	UploaderClient() throws Exception {

		Properties properties = new Properties();
		properties.put(	Context.INITIAL_CONTEXT_FACTORY,
						"org.jnp.interfaces.NamingContextFactory");
		properties.put(	Context.URL_PKG_PREFIXES,
						"org.jboss.naming:org.jnp.interfaces");
		properties.put(Context.PROVIDER_URL, "localhost:1099");
		InitialContext ctx = new InitialContext(properties);

		Queue queue = (Queue) ctx.lookup("queue/counter/logger");
		QueueConnectionFactory qcf = (QueueConnectionFactory) ctx
				.lookup("ConnectionFactory");

		sendMessage(qcf, queue, "uploader");
	}

	public static void main(String[] args) throws Exception {
		System.out.println("YIYOYO");
		new UploaderClient();
	}
	
	

	public void sendMessage(QueueConnectionFactory qcf, Queue queue, String remarks)
			throws Exception {
		QueueConnection qc = qcf.createQueueConnection();
		System.out.println("YESYES");
		try {
			QueueSession qs = qc.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			QueueSender sender = qs.createSender(queue);
			Message payload = qs.createMapMessage();

			//insert your code here
			CounterBean cb  = new CounterBean();
			cb.setComponentId("component");
			cb.setIsbn("ISBN1239*978");		
			cb.setIssn("ISSN");			
			cb.setLogType(6);
			cb.setSessionId("sessionxxxx");
			cb.setTimestamp(new Timestamp(new Date().getTime()));
			cb.setTitle("title2");
			cb.setService("service2");
			cb.setIsSearch("Y");
			cb.setBodyId(132);
			System.out.println("adffasdf");
			
			payload.setStringProperty(CounterBean.LOG_TYPE, String.valueOf(cb.getLogType()));
        	payload.setStringProperty(CounterBean.SERVICE, cb.getService());
        	payload.setStringProperty(CounterBean.ISBN, cb.getIsbn());
        	payload.setStringProperty(CounterBean.ISSN, cb.getIssn());
        	payload.setStringProperty(CounterBean.TITLE, cb.getTitle());
        	payload.setStringProperty(CounterBean.PUBLISHER, cb.getPublisher());
        	payload.setStringProperty(CounterBean.PLATFORM, cb.getPlatform());
        	payload.setStringProperty(CounterBean.SESSION_ID, cb.getSessionId());
        	payload.setStringProperty(CounterBean.COMPONENT_ID, cb.getComponentId());
        	payload.setStringProperty(CounterBean.IS_SEARCH, cb.getIsSearch());        	
        	payload.setStringProperty(CounterBean.TIMESTAMP, 
        			DateUtil.convTsToStr(cb.getTimestamp()));   
        	payload.setStringProperty(CounterBean.BODY_ID, 
        			DateUtil.convTsToStr(cb.getTimestamp()));
        	payload.setStringProperty(CounterBean.BODY_ID, String.valueOf(cb.getBodyId()));
        	System.out.println(cb.getTimestamp());
			sender.send(payload);
			sender.close();
			qs.close();
			System.out.println("adffasdf");

		} finally {
			qc.close();
		}
		System.out.println("SYSOU");
	}
}