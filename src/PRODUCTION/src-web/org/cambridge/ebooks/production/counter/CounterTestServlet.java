package org.cambridge.ebooks.production.counter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.production.counter.ejb.jpa.CounterBean;


public class CounterTestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4593749126886770125L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {		
		String submit = req.getParameter("SUBMIT");
		
		if("Submit1".equals(submit)){
			CounterBean cb = new CounterBean();
			getTitleValues(req, cb);
			cb.setTimestamp(getTimestamp("TIMESTAMP1", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(1);
			CounterTracker.logEvent(cb);
		}else if("Submit2".equals(submit)){
			CounterBean cb = new CounterBean();
			getTitleValues(req, cb);
			cb.setTimestamp(getTimestamp("TIMESTAMP2", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(2);
			CounterTracker.logEvent(cb);			
		}else if("Submit3".equals(submit)){
			CounterBean cb = new CounterBean();
			getTitleValues(req, cb);
			cb.setTimestamp(getTimestamp("TIMESTAMP3", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(3);
			CounterTracker.logEvent(cb);			
		}else if("Submit4".equals(submit)){
			CounterBean cb = new CounterBean();
			cb.setService(getService(req));
			cb.setTimestamp(getTimestamp("TIMESTAMP4", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(4);
			CounterTracker.logEvent(cb);			
		}else if("Submit5".equals(submit)){
			CounterBean cb = new CounterBean();
			getTitleValues(req, cb);
			cb.setTimestamp(getTimestamp("TIMESTAMP5", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(5);
			cb.setIsSearch(getIsSearch(req));
			CounterTracker.logEvent(cb);			
		}else if("Submit6".equals(submit)){
			CounterBean cb = new CounterBean();
			cb.setService(getService(req));
			cb.setTimestamp(getTimestamp("TIMESTAMP6", req));
			cb.setSessionId(getSessionId(req));
			cb.setBodyId(getBodyId(req));
			cb.setLogType(6);
			cb.setIsSearch(getIsSearch(req));
			CounterTracker.logEvent(cb);			
		}
		
		
		resp.sendRedirect("counter_test.jsp");
	}
	
	private void getTitleValues(HttpServletRequest req, CounterBean cb) {
		String book = req.getParameter("BOOK");
		String[] bd = book.split(":");
		cb.setIsbn(bd[0]);
		cb.setTitle(bd[1]);
	}
	
	private Timestamp getTimestamp(String name, HttpServletRequest req) 
			throws ServletException{
		String sdfFormat = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(sdfFormat);
		String dateStr = req.getParameter(name);
		Date date;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			System.out.println(new Timestamp(new Date().getTime()));
			return new Timestamp(new Date().getTime());
		}
		return new Timestamp(date.getTime());
	}
	
	private String getSessionId(HttpServletRequest req){
		HttpSession session = req.getSession();
		return session.getId();
	}
	
	private int getBodyId(HttpServletRequest req){
		String bodyId = req.getParameter("BODY_ID");
		return Integer.parseInt(bodyId);
	}
	
	private String getService(HttpServletRequest req) {
		String service = req.getParameter("SERVICE");
		return service;
	}
	
	private String getIsSearch(HttpServletRequest req){
		String isSearch = req.getParameter("IS_SEARCH");
		return isSearch;
	}

}
