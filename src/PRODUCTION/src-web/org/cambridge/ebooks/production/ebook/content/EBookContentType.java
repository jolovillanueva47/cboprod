package org.cambridge.ebooks.production.ebook.content;

public enum EBookContentType {
	
//	CHAPTER("chapter"), FRONTMATTER("frontmatter"), PRELUDE("prelude"), 
//	PROLOGUE("prologue"), ACTS("acts");
	NONE("NONE");
	
	private String type;

	
	private EBookContentType(String type){
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	

	public static boolean isIncluded(String type) {
		boolean included = false;
		
		if(type != null && type.length() > 0)
		{
			for(EBookContentType contentType : EBookContentType.values())
			{
				if(contentType.type.equalsIgnoreCase(type))
				{
					included = true;
					break;
				}
			}
		}
	
		return included;
	}
	
}
