package org.cambridge.ebooks.production.ebook.content;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EBookCrossrefBean {
	private String filename;
	private String doiBatchId;
	private String submitter;
	private String timestamp;
	private String status;
	
	public EBookCrossrefBean(String path, String filename){
		this.filename = filename;
		extractData(path, filename);		
	}

	private void extractData(String path, String filename){
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (new File(path + filename));
			
			Node node;
			
			NodeList timestamp = doc.getElementsByTagName("timestamp");
			node = timestamp.item(0);
			this.timestamp = formatDate(node.getTextContent());
			System.out.println("timestamp = "+this.timestamp);

			NodeList name = doc.getElementsByTagName("name");
			node = name.item(0);
			this.submitter = node.getTextContent();
			System.out.println("name = "+this.submitter);
			
			NodeList doiBatchId = doc.getElementsByTagName("doi_batch_id");
			node = doiBatchId.item(0);
			this.doiBatchId = node.getTextContent();
			System.out.println("doiBatchId = "+this.doiBatchId);
			
			this.status = extractStatus(filename);
			
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static String extractStatus(String file){
		String status = "";
		String ext = ".crossref.xml";
		String statusCode = file.substring(file.indexOf(ext)-1, file.indexOf(ext));
		
		if(statusCode.equals("n")){
			status = "Not Sent";
		} else if(statusCode.equals("f")){
			status = "Failed";
		} else if(statusCode.equals("s") || file.length() < 30){
			status = "Sent Successfully";
		} else {
			status = "Status Unknown";
		}
		return status;
	}
	
	private String formatDate(String date){
		int year =  Integer.parseInt(date.substring(0, 4));
		int month =  Integer.parseInt(date.substring(4, 6));
		int day =  Integer.parseInt(date.substring(6, 8));
		int hourOfDay = Integer.parseInt(date.substring(8, 10));
		int minute = Integer.parseInt(date.substring(10, 12));
		int second = Integer.parseInt(date.substring(12, 14));
		
		@SuppressWarnings("deprecation")
		Date realDate = new Date(year - 1900, month - 1, day, hourOfDay, minute, second);		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE d MMM yyyy HH:mm:ss");
		
		return sdf.format(realDate);
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDoiBatchId() {
		return doiBatchId;
	}
	public void setDoiBatchId(String doiBatchId) {
		this.doiBatchId = doiBatchId;
	}
	public String getSubmitter() {
		return submitter;
	}
	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
