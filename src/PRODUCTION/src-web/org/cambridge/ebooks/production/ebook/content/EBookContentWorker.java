package org.cambridge.ebooks.production.ebook.content;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.document.ContributorBean;
import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.ebook.EBookDAO;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.bean.WordsContentItemDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.ListUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.StringUtil;

/**
 * @author rvillamor
 * 
 * REVISIONS:
 * 20131205 - jmendiola - removed because stahl 2 content it didn't push through
 * 20131208 - alacerna - added method for display of words in JSP
 * 20140213 - alacerna  - for HTML handling
 *
 */
public class EBookContentWorker {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookContentWorker.class);
	public final static String UPDATE = "update";
	public final static String CHAPTER = "chapter";
	public final static String OTHER = "other";
	public final static String DISPLAY_BY_STATUS = "displayByStatus";
	private static final String CONTENT_FOLDER = System.getProperty("content.dir");
	
	// PID 83258 2012-04-25
	private final static String BOOK_ID = "bookId";
	
	private List<Integer> status = new ArrayList<Integer>();
	private final EBookSearchWorker worker = new EBookSearchWorker();
	
	private ArrayList<LevelBean> levelBean = new ArrayList<LevelBean>();
	
	/**
	 * 
	 * Method to fetch content items from index and database by bookId
	 * list of content items from the index is merged with the content items from the database,
	 * if an item from the index is not in the database, it will not be included.
	 * 
	 * @param bookId
	 * 
	 * @return EBookContentBean List
	 * @throws SQLException 
	 */
	public List<EBookContentBean> ebookContents(String bookId) throws SQLException{	
		System.out.println(EBookContentWorker.class + "---BOOK ID:" + bookId);
		
		List<EBookContentBean> ebookContentFromIndex = null;
		List<EBookContent> ebookContentFromDB = null;		
		
		ebookContentFromIndex = getContentsFromIndex(bookId);
		ebookContentFromDB = EBookContentDAO.searchContentsByBookId(bookId);
		
		System.out.println("ebookContentFromIndex: " + ebookContentFromIndex);
		System.out.println("ebookContentFromDB: " + ebookContentFromDB);
		System.out.println(EBookContentWorker.class + ".ebookContents(String, boolean) [INDEX] " + ebookContentFromIndex.size() + " [DB] " + ebookContentFromDB.size());

		setContentProperties(ebookContentFromDB, ebookContentFromIndex);
		
		return ebookContentFromIndex;
	}
	

	public EBookContentBean ebookContents(String bookId, int page){	

		EBookContentBean ebookContentFromIndex = null;
		EBookContent ebookContentFromDB = null;
		
		ebookContentFromIndex =  getContentFromIndex(bookId, page); 
		if(ebookContentFromIndex != null)
		{
			ebookContentFromDB = PersistenceUtil.searchEntity(new EBookContent(), EBookContent.SEARCH_BY_CONTENT_ID, ebookContentFromIndex.getContentId());		
			setContentProperties(ebookContentFromIndex, ebookContentFromDB);
		}

		return ebookContentFromIndex;
	}
	
	public List<WordsContentItemDocument> ebookDictionary(String bookId) {
		
		List<WordsContentItemDocument> ebookContentFromIndex = null;
			
		ebookContentFromIndex =  getDictionaryFromIndex(bookId); 
		

		return ebookContentFromIndex;
	}
	
	private List<WordsContentItemDocument> getDictionaryFromIndex(String bookId) {
		List<WordsContentItemDocument> contentItem = worker.searchWordCore("book_id:" + bookId + " AND type:dictionary");
		
		if (contentItem != null){
			
			String isbnBreakdown = IsbnContentDirUtil.getPath(bookId.substring(3)).replace(CONTENT_FOLDER, "");
			
			for (WordsContentItemDocument word : contentItem)
			
				word.setIsbnBreakdown(isbnBreakdown);
			
			return contentItem;
			
		}
		else
			return null;
		
	}


	/**
	 * 
	 * Method to fetch content items from index and database by bookId
	 * list of content items from the index is merged with the content items from the database,
	 * if an item from the index is not in the database, it will not be included.
	 * 
	 * @param bookId
	 * 
	 * @return EBookContentBean List
	 */
	public List<EBookContentBean> ebookContents(String bookId, String status){ 		
//		LogManager.info(EBookContentWorker.class, "---BOOK ID:" + bookId);
//		LogManager.info(EBookContentWorker.class, "---STATUS:" + status);
		
		List<EBookContentBean> ebookContentFromIndex = null;
		List<EBookContent> ebookContentFromDB = null;	
		
		ebookContentFromIndex = getContentsFromIndex(bookId); 
		ebookContentFromDB = EBookContentDAO.searchContentsByBookIdAndStatus(bookId, status);		
		
		System.out.println("BEFORE setContentProperties(List, List) !!!!!!!!!!!!!!!!");
		
		setContentProperties(ebookContentFromDB, ebookContentFromIndex);
		
		if(ebookContentFromDB.size() == 0)
			return null;
		else
			return ebookContentFromIndex;
	}
	

	private void setContentProperties(EBookContentBean ebookContentBean, EBookContent ebookContent) {
		
		try 
		{
			if(ebookContentBean != null && ebookContent != null)
			{
				String contentIdIdx = ebookContentBean.getContentId();
				String contentIdDb = ebookContent.getContentId();
				
				if(contentIdIdx.equals(contentIdDb))
				{
					//String partId = "";
					ebookContentBean.setEbookContent(ebookContent);
								
					String label = ebookContentBean.getLabel();							
					String title = ebookContentBean.getTitle();							
					String ebookType = ebookContentBean.getType();
					String newTitle = "";
								
					title = StringUtil.correctHtmlTags(title);
					
					int level = getLevel(ebookContentBean);
					ebookContentBean.setIndentLevel(getLevelString(level));
//					System.out.println("****INDENT LEVEL: "+ebookContentBean.getIndentLevel());
								
					if(level > 0){
						ebookContentBean.setIndented(true);
					}
					
					
//					if(!ebookType.equals("part-title") && partId.equals(ebookContentBean.getParentId())){ 
//						ebookContentBean.setIndented(true);
//					}
							
					if(ebookType.equals("part-title")) 
					{										
					
						if(ebookContent.getStatus() == -1) 
							ebookContentBean.setNotLoadedPartTitle(true); 						
							
					}
								
					if(label == null) 
						newTitle = title;
					else 
						newTitle = label + ": " + title;
					
					ebookContentBean.setTitle(newTitle);
				}
				
			}
		}
		catch(Exception e) 
		{		
			LogManager.error(EBookContentWorker.class, " setContentProperties(EBookContentBean, EBookContent): " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setContentProperties(List ebookContentList, List ebookContentBeanList) {
		
		try {
			
			if(ListUtil.isNotNullAndEmpty(ebookContentList) && ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
				
				LogManager.info(EBookContentWorker.class, "[Number of EBook Contents] DB Size: "	+ ebookContentList.size() + " Index Size: " + ebookContentBeanList.size());

				ebookContentBeanList.retainAll(ebookContentList);
				if(ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
					
					String partId = "";
					for(Object ebookContentBean : ebookContentBeanList) {
						Object ebookContent = ListUtil.searchObject(ebookContentList, ebookContentBean);
						if(ebookContent != null) {
							EBookContent ebookContent_ = (EBookContent)ebookContent;	
							EBookContentBean ebookContentBean_ = ((EBookContentBean)ebookContentBean);
							ebookContentBean_.setEbookContent(ebookContent_);
							
							String label = ebookContentBean_.getLabel();							
							String title = ebookContentBean_.getTitle();							
							String ebookType = ebookContentBean_.getType();
							String newTitle = "";
							
							title = StringUtil.correctHtmlTags(title);
							
							int level = getLevel(ebookContentBean_);
							ebookContentBean_.setIndentLevel(getLevelString(level));
//							System.out.println("****INDENT LEVEL: "+ebookContentBean_.getIndentLevel());
							
							if(level > 0){
								ebookContentBean_.setIndented(true);
							}
//							if(!ebookType.equals("part-title") 
//									&& partId.equals(ebookContentBean_.getParentId())) {
////								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
////								LogManager.info(EBookContentWorker.class, "---PARENT ID:" + ebookContentBean_.getParentId());
////								LogManager.info(EBookContentWorker.class, "---INDENTED: true");
//								ebookContentBean_.setIndented(true);
//								
//							}
							
							LogManager.info(EBookContentWorker.class, "---EBOOK TYPE:" + ebookType);
							
							if(ebookType.equals("part-title")) {										
								partId = ebookContentBean_.getContentId();
//								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
								
								EBookContent ec = EBookContentDAO.searchContentByContentIdAndType(partId, ebookType);
								if(ec == null || ec.getStatus() == -1) 
									ebookContentBean_.setNotLoadedPartTitle(true);
											
							}
							
							if(label == null) {
//								newTitle = ebookContentBean_.getPosition() + ": " + title;
								newTitle = title;
							} else {
								newTitle = label + ": " + title;
							}
							ebookContentBean_.setTitle(newTitle);
						}
					}					
				}				
			}
		}
		catch(Exception e) {		
			LogManager.error(EBookContentWorker.class, " setContentProperties(List, List): " + e.getMessage());
			e.printStackTrace();
		}
	}
	

	public EBookBean searchEBook(String bookId){
		return setBookTitle(bookId);
	}
	
	public SelectItem[] resolveAllSelectItems(String currentStatus, LinkedHashMap<String, String> status) {
		ArrayList<SelectItem> options = new ArrayList<SelectItem>();
		for (String desc : status.keySet()) {
			options.add(new SelectItem(status.get(desc), desc, desc, false));
		}
		return options.toArray(new SelectItem[]{});
	}
	public SelectItem[] resolveSelectItems(String currentStatus, AccessRights rights, LinkedHashMap<String, String> status, boolean removeSignedOff) {
		ArrayList<SelectItem> options = new ArrayList<SelectItem>();
		for (String desc : status.keySet()) {
			String statusCode = status.get(desc);
			boolean isChangeStatusAllowed = rights.getCanRevertStatus() || rights.isChangeStatusAllowed(Status.getStatus(Integer.valueOf(statusCode)));
			if ((isChangeStatusAllowed || statusCode.equals(currentStatus)) ){
			    if (! (removeSignedOff && desc.equals("Content Signed Off"))){
			        options.add(new SelectItem(statusCode, desc, desc, false));
			    }
			}
		}
		return options.toArray(new SelectItem[]{});
	}
	
	
	public void updateStatus(final String bookId){
		if(ListUtil.isNotNullAndEmpty(status) && StringUtil.isNotEmpty(bookId)) {
			int ctr = 0;
			int[] stat = new int[status.size()];
			
			for(Integer s : status) {
				stat[ctr++] = s;
			}
			
			Status bookStatus = Status.getBookStatus(stat);
			EBookDAO.setEBookStatus(bookId, Integer.valueOf(bookStatus.getStatus()));
			status.clear();
			
			/*
			if( Status.isApprove(stat) ) {
				AssetWorker.sendAssetEvent(eIsbn(bookId));
			}
			*/
		}
	}
	
	public void updateEbookContents(List<EBookContentBean> ebookContentBeanList) {
		
		if(ListUtil.isNotNullAndEmpty(ebookContentBeanList))
		{	
			if(isEbookContentsValid(ebookContentBeanList))
				for(EBookContentBean ebookContentBean : ebookContentBeanList)
				{
					if(ebookContentBean.isUpdate())
					{
						EBookContentDAO.updateContentEntity(ebookContentBean.getEbookContent());
						ebookContentBean.setUpdate(false);
					}
					
					status.add(ebookContentBean.getEbookContent().getStatus());
				}
		}
			
	}
	
	public void updateEBookOthers(Map<String, EBookContentBean> ebookOthers){
		if(ebookOthers != null && !ebookOthers.isEmpty())
		{	
			if(isEbookContentsValid(ebookOthers.values()))
			{
				EBookContentDAO.updateEbookOthers(ebookOthers);
				status.add(ebookOthers.get(EBookContent.XML).getEbookContent().getStatus());
			}
		}	
	}
	
	public void updateOtherContents(List<EBookContentBean> ebookOthersList){
		EBookContentDAO.updateOtherContents(ebookOthersList);	
	}
	
	//overload method for calls from classes other than EBookContentManagedBean
	public Map<String, EBookContentBean> initEbookOthers(Map<String, EBookContentBean> ebookOthers, final String bookId) {
	    return initEbookOthers(ebookOthers, bookId, false);
	}
	
	public Map<String, EBookContentBean> initEbookOthers(Map<String, EBookContentBean> ebookOthers, final String bookId, boolean removeSignedOff) {
		AccessRights rights = HttpUtil.getUserFromCurrentSession().getAccessRights();
		List<EBookContent> ebookOthersList = EBookContentDAO.searchEBookHeaderAndImagesByBookId(bookId); //PersistenceUtil.searchList(new EBookOthers(), EBookOthers.SEARCH_BY_BOOK_ID, bookId);
		ebookOthers = new HashMap<String, EBookContentBean>();
		for (EBookContent ebo : ebookOthersList) {
			EBookContentBean othersBean = new EBookContentBean(ebo);
			Status status = Status.getStatus(Integer.valueOf(ebo.getStatus()));
			othersBean.setOptions(resolveSelectItems(String.valueOf(ebo.getStatus()), rights, Status.getOptions(status, rights.getCanRevertStatus()), removeSignedOff));
			ebookOthers.put(ebo.getType(), othersBean);			
		}
		
		return ebookOthers;
	}
	
	public void setContentStatus(final List<EBookContentBean> contentList, boolean removeSignedOff){
		if(ListUtil.isNotNullAndEmpty(contentList))
		{
			AccessRights rights = HttpUtil.getUserFromCurrentSession().getAccessRights();
			for(EBookContentBean ebookContentBean : contentList)
			{				
				Status status = Status.getStatus(Integer.valueOf(ebookContentBean.getEbookContent().getStatus()));
				ebookContentBean.setOptions(resolveSelectItems(String.valueOf(ebookContentBean.getEbookContent().getStatus()), rights, Status.getOptions(status, rights.getCanRevertStatus()),removeSignedOff));
			}
		}
	}
	
	public void setContentStatus(EBookContentBean content, boolean removeSignedOff){
		if(content != null)
		{
			AccessRights rights = HttpUtil.getUserFromCurrentSession().getAccessRights();
			
			Status status = Status.getStatus(Integer.valueOf(content.getEbookContent().getStatus()));
			content.setOptions(resolveSelectItems(String.valueOf(content.getEbookContent().getStatus()), rights, Status.getOptions(status, rights.getCanRevertStatus()), removeSignedOff));

		}
	}

	// Moved to org.cambridge.ebooks.production.util.IsbnContentDirUtil
//	public String eIsbn(String bookId){
//		String eIsbn = null;
//		if(StringUtil.isNotEmpty(bookId))
//		{
//			Pattern p = Pattern.compile("\\d+");
//			Matcher m = p.matcher(bookId);
//			
//			if(m.find())
//			{
//				eIsbn = m.group();
//			}
//		}
//		
//		return eIsbn;
//	}
	
	
	public EBookContentBean ebookContent(List<EBookContentBean> chapterList){
		String contentId =  HttpUtil.getHttpServletRequestParameter("cid");	
		if(ListUtil.isNotNullAndEmpty(chapterList) && StringUtil.isNotEmpty(contentId))
		{
			for(EBookContentBean ebookContentBean : chapterList)
			{
				if(contentId.equals(ebookContentBean.getContentId()))
					return ebookContentBean;
			}
		}
		
		return null;
	}
	
	private EBookBean setBookTitle(String bookId){

		EBookBean ebookBean = new EBookBean();
		if(StringUtil.isNotEmpty(bookId))
		{		
			List<BookMetadataDocument> books = worker.searchBookCore("book_id:" + bookId);
			if(books != null && !books.isEmpty())
			{
				BookMetadataDocument doc = books.get(0);
				ebookBean.setTitle(doc.getTitle());
				ebookBean.setIsbn(doc.getIsbn());
			}
		}
		
		return ebookBean;
	}
	
	private boolean isEbookContentsValid(Collection<EBookContentBean> ebookContentBeanList){
		boolean result = true;
		
		try
		{
			for(EBookContentBean bean : ebookContentBeanList)
			{
				int status = bean.getEbookContent().getStatus();
				if(status == Integer.valueOf(Status.EMBARGO.getStatus()))
				{
					Date embargoDate = bean.getEbookContent().getEmbargoDate();
					if(embargoDate == null || "".equals(embargoDate))
					{
						result = false;
						
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage message = new FacesMessage("Embargo dates must not be empty.");
						message.setSeverity(FacesMessage.SEVERITY_INFO);
						context.addMessage("alertMessage", message);
						
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			System.err.println(EBookContentWorker.class +  " isEbookContentsValid() " + e.getMessage());
			result = false;
		}
	
		return result;
	}
	
	private  List<EBookContentBean> getContentsFromIndex(String bookId){
		List<BookContentItemDocument> contentItems = worker.searchContentCore("book_id:" + bookId + " AND content_type:content-item");
		return populateContentBeans(contentItems);
	}
	
	private  EBookContentBean getContentFromIndex(String bookId, int page){
		BookContentItemDocument contentItem = worker.searchContentCoreSinglePager("book_id:" + bookId + " AND content_type:content-item", page);
		//return contentItem != null ? populateContentBean(contentItem) : null;
		// replaced code above; fixed NullPointerException issue when last content is viewed in the View Metadata/View Keywords tab
		if ((contentItem != null) && (contentItem.getContentId() != null) && (contentItem.getIsbn() != null))
			return populateContentBean(contentItem);
		else
			return null;
	}
	
	private List<EBookContentBean> populateContentBeans(List<BookContentItemDocument> contentItems){
		List<EBookContentBean> result = new ArrayList<EBookContentBean>();
		for(BookContentItemDocument contentItem : contentItems)
			result.add(populateContentBean(contentItem));
	
		return result;
	}
	
	private EBookContentBean populateContentBean(BookContentItemDocument contentItem){
		
		EBookContentBean contentBean = new EBookContentBean();
		contentBean.setContentId(contentItem.getContentId());
		if (contentItem.getPdfFilename()!=null){
		contentBean.setFileName(contentItem.getPdfFilename());
		}
		if(contentItem.getNavFile()!=null){
		contentBean.setNavFileName("html_chunk/"+contentItem.getNavFile().get(0));
		}
		contentBean.setTitle(contentItem.getTitle());
		contentBean.setDoi(contentItem.getDoi());
		contentBean.setAbstractText(getAbstract(contentItem.getIsbn(), contentItem.getContentId()));
		contentBean.setAbstractImage(contentItem.getAbstractFilename());
		contentBean.setType(contentItem.getType());
		contentBean.setPosition(contentItem.getPosition());
		contentBean.setPageStart(contentItem.getPageStart());
		contentBean.setPageEnd(contentItem.getPageEnd());
		contentBean.setAlphasort(contentItem.getTitleAlphasort());
		contentBean.setLabel(contentItem.getLabel());
		contentBean.setParentId(contentItem.getParentId());
		
		contentBean.setChapterContributors(getChapterContributors(contentItem));
		
		return contentBean;
	}
	 //20140213 - alacerna  - for HTML handling
//	private String checkHtml(String pdfFilename ,EBookContentBean contentBean) {
//		if(pdfFilename!=null && pdfFilename.endsWith(".html"))
//		{
//			pdfFilename = "html_chunk/" + pdfFilename;
//			contentBean.setHtml(true);
//		}
//		
//		//System.out.println(pdfFilename);
//			
//		return pdfFilename;
//	}


	private ArrayList<ContributorBean> getChapterContributors(BookContentItemDocument contentItem){
		ArrayList<ContributorBean> contributors = new ArrayList<ContributorBean>();
		
		List<String> authorNames = contentItem.getAuthorNameLfList();		
		List<String> authorPostions = contentItem.getAuthorPositionList();
		ContributorBean cb;
		
		if(authorNames != null && !authorNames.isEmpty())
		{
			for (int ctr=0; ctr<authorNames.size(); ctr++) 
			{
				cb = new ContributorBean();
				cb.setContributor(authorNames.get(ctr));
				cb.setPosition(authorPostions != null && !"none".equals(authorPostions.get(ctr)) ? authorPostions.get(ctr) : "0");
				contributors.add(cb);
			}
		}
	
		return contributors;
	}
	
	
	private String getAbstract(String isbn, String contentId){
		String result = "";
		
		// placed 'try' here for View Metadata and View Keywords fix
		try
		{
			
			// PID 83258
			String filename = contentId + "_abstract.txt";
			final File abstractDir = new File(IsbnContentDirUtil.getFilePath(isbn, filename) + File.separator + filename);
		
			StringBuilder sb = new StringBuilder();
			if(abstractDir != null && abstractDir.exists())
			{
				String temp = null;
				FileReader reader = new FileReader(abstractDir);
				BufferedReader breader = new BufferedReader(reader);
				while((temp = breader.readLine()) != null)
					sb.append(temp);
		
				result = sb.toString();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return result;
	}
	
	private int getLevel(EBookContentBean ebcb) {
		int value = 0;
		if(ebcb.getParentId().equals("root")){
			levelBean.add(new LevelBean(ebcb.getParentId(), 0, ebcb.getContentId()));
			value = 0;
		} else {
			for(LevelBean level: levelBean){
				if(level.getValue().equals(ebcb.getParentId())){
					value = level.getLevel()+1;
					levelBean.add(new LevelBean(ebcb.getParentId(), value, ebcb.getContentId()));
					break;
				}
			}
		}
		return value;
	}
	
	private String getLevelString(int level){
		String levelString = "part_chapter";
		switch (level){
		case 0:
			//do nothing, child of root
			levelString = "";
			break;
		case 1:
			//use the default "part_chapter"
			break;
		default:
			levelString = levelString + level;
			break;
		}
		
		return levelString;
	}
	
	// PID 83258 2012-04-25
	public String createIsbnBreakdown(String filename) {
		
		String bookId = (String) HttpUtil.getAttributeFromCurrentSession(BOOK_ID);
		String isbn = IsbnContentDirUtil.bookIdToIsbn(bookId);
		
		return IsbnContentDirUtil.getFilePathIsbnBreakdown(isbn, filename);
		
	}
	
	public String createPath(String filename) {
		
		String bookId = (String) HttpUtil.getAttributeFromCurrentSession(BOOK_ID);
		String isbn = IsbnContentDirUtil.bookIdToIsbn(bookId);
		
		return IsbnContentDirUtil.getPath(isbn).replace(CONTENT_FOLDER, "");
		
	}
	
	public String createIsbn(String filename) {
		
		String bookId = (String) HttpUtil.getAttributeFromCurrentSession(BOOK_ID);
		String isbn = IsbnContentDirUtil.bookIdToIsbn(bookId);
		
		return isbn;
		
	}
	
//	//20131205 removed because stahl 2 content didn't push through
//	public void setCBML(List<EBookContentBean> ebookContentList, Map<String, KeywordsContentItemDocument> keywords){
//		
//		for(EBookContentBean contentItem:ebookContentList){
//			if(keywords.get(contentItem.getContentId()) != null)
//				contentItem.setXmlFileName(keywords.get(contentItem.getContentId()).getXmlFilename());
//			
//		}
//	}
//	//20131205 removed because stahl 2 content didn't push through
//	public Map<String, KeywordsContentItemDocument> getKeywordList(String bookId){
//		Map <String, KeywordsContentItemDocument> keywordMap = new HashMap <String, KeywordsContentItemDocument>();
//		List<KeywordsContentItemDocument> keyword = solrWorker.searchCore(new KeywordsContentItemDocument(), "book_id:"+bookId, SOLRServer.getInstance().getKeywordCore());
//		
//		for(KeywordsContentItemDocument key:keyword){
//			if(! keywordMap.containsKey(key.getContentId()))
//				keywordMap.put(key.getContentId(), key);
//		}
//		return keywordMap;
//	}
}
