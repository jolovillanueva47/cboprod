///**
// * 
// */
//package org.cambridge.ebooks.production.ebook;
//
////TODO:SOLR-ify this
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.WhitespaceAnalyzer;
//import org.apache.lucene.document.Document;
//import org.apache.lucene.queryParser.QueryParser;
//import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.search.Query;
//import org.apache.lucene.search.ScoreDoc;
//import org.apache.lucene.search.TopDocCollector;
//import org.cambridge.ebooks.production.jpa.user.EBookAccess;
//import org.cambridge.ebooks.production.util.IndexSearchUtil;
//import org.cambridge.ebooks.production.util.ListUtil;
//import org.cambridge.ebooks.production.util.LogManager;
//import org.cambridge.ebooks.production.util.StringUtil;
//
///**
// * @author rvillamor
// *
// */
//public class EBookIndexSearch {
//	
//	private static final String EBOOK_TITLES_QUERY = "((ELEMENT:main-title AND PARENT:metadata) OR (ELEMENT:isbn AND PARENT:metadata))";
//	
//	public static List<EBookBean> ebookTitles() {
//		return ebookTitles(EBookIndexSearch.EBOOK_TITLES_QUERY);
//	}
//	  
//	public static List<EBookBean> ebookTitles(List<EBookAccess> accessibleEbooks) {
//		List<EBookBean> result;
//		StringBuilder filter = new StringBuilder();
//		if (accessibleEbooks != null && accessibleEbooks.size() > 0) 
//		{
//			
//			boolean allEbooksAccess = accessibleEbooks != null 
//				&& accessibleEbooks.size() == 1 
//				&& accessibleEbooks.get(0).equals(new EBookAccess(EBookAccess.ALL_EBOOKS)); 
//			
//			boolean allOrgEbooksAccess = accessibleEbooks != null 
//			&& accessibleEbooks.size() == 1 
//			&& accessibleEbooks.get(0).equals(new EBookAccess(EBookAccess.ALL_PUBLISHER_EBOOKS)); 
//			
//			if (allEbooksAccess) 
//			{
//				result = new ArrayList<EBookBean>();
//				result.add(new EBookBean(EBookAccess.ALL_EBOOKS));
//			} 
//			else if (allOrgEbooksAccess) 
//			{
//				result = new ArrayList<EBookBean>();
//				result.add(new EBookBean(EBookAccess.ALL_PUBLISHER_EBOOKS));
//			} 
//			else 
//			{
//				filter.append(" AND (");
//
//				for (Iterator<EBookAccess> it = accessibleEbooks.iterator(); it.hasNext();) 
//				{
//					EBookAccess eba = it.next();
//					filter.append("BOOK_ID:" + eba.getBookId());
//					if (it.hasNext()) 
//					{
//						filter.append(" OR ");
//					}
//				}
//
//				filter.append(")");
//				result = ebookTitles(EBOOK_TITLES_QUERY + filter.toString());
//			}
//		} 
//		else 
//		{
//			result = new ArrayList<EBookBean>();
//		}
//		return result;
//	}
//	
//	public static List<EBookBean> ebookTitles(String queryStr){
//		  ArrayList<EBookBean> result = new ArrayList<EBookBean>();
//		  List<Document> ebookListFromIndex = IndexSearchUtil.searchIndex( IndexSearchUtil.INDEX_DIR, queryStr );
//		 
//		  try
//		  {
//			  if(ListUtil.isNotNullAndEmpty(ebookListFromIndex))
//			  {
//				 for(Document doc : ebookListFromIndex)
//				 {
//					EBookBean ebook = new EBookBean();
//					if( StringUtil.isNotEmpty(doc.get("BOOK_ID")) )
//						ebook.setBookId(doc.get("BOOK_ID"));
//				
//					if( StringUtil.isNotEmpty(doc.get("MAIN_TITLE")) )
//						ebook.setTitle(doc.get("MAIN_TITLE"));
//					
//					if( StringUtil.isNotEmpty(doc.get("ISBN")) )
//						ebook.setIsbn(doc.get("ISBN"));
//					
//					if(StringUtil.isNotEmpty(doc.get("ALPHASORT"))){
//						ebook.setAlphasort(doc.get("ALPHASORT"));
//					}					
//				
//					EBookBean ebookObjResult = ListUtil.searchObject(result, ebook);
//					if(ebookObjResult != null)
//					{
//						if(StringUtil.isNotEmpty(ebookObjResult.getTitle()))
//							ebookObjResult.setIsbn(ebook.getIsbn());
//						else	
//							ebookObjResult.setTitle(ebook.getTitle());
//					}
//					else
//					{
//						result.add(ebook);
//					}
//					
//				 }
//			  }
//		  }
//		  catch (Exception e) 
//		  {
//			// TODO Auto-generated catch block
//			  System.err.println(EBookManagedBean.class + " ebookTitles(String queryStr) [Exception]: " + e.getMessage());
//		  }
//		  
//		  return result;
//	  }
//	
//	public static ArrayList<EBookBean> ebookTitlesByEisbn(String[] eisbnList) {
//		ArrayList<EBookBean> result = new ArrayList<EBookBean>();
//		StringBuilder searchString = new StringBuilder();
//		int listSize = eisbnList == null ? 0 : eisbnList.length;
//
//		if (listSize > 0) {
//			String isbnSearch = "ISBN:";
//			for (int c = 0; c < listSize; c++) {
//				searchString.append(isbnSearch.concat(eisbnList[c]));
//				if (c < listSize - 1) {
//					searchString.append(" OR ");
//				}
//			}
//
//			try {
//				IndexSearcher searcher = new IndexSearcher(
//						IndexSearchUtil.INDEX_DIR);
//				ScoreDoc[] docs = search(searcher, "ISBN", searchString
//						.toString(), listSize);
//
//				for (ScoreDoc hit : docs) {
//					EBookBean ebb = new EBookBean();
//					int docId = hit.doc;
//					Document d = searcher.doc(docId);
//					ebb.setBookId(d.get("BOOK_ID"));
//					ebb.setIsbn(d.get("ISBN"));
//					ebb.setTitle(searchDocument(searcher, "BOOK_ID", "ELEMENT:main-title AND BOOK_ID:" + ebb.getBookId()).get("MAIN_TITLE"));
//					result.add(ebb);
//				}
//
//				searcher.close();
//			} catch (Exception e) {
//				LogManager.error(EBookIndexSearch.class, e.getMessage());
//			}
//		}
//
//		return result;
//	}
//
//	public static ScoreDoc[] search(IndexSearcher searcher,
//			String defaultField, String queryString, int pageSize) {
//		ScoreDoc[] result = null;
//		try {
//			Analyzer analyzer = new WhitespaceAnalyzer();
//			Query query = new QueryParser(defaultField, analyzer).parse(queryString);
//
//			TopDocCollector collector = new TopDocCollector(pageSize);
//			searcher.search(query, collector);
//			result = collector.topDocs().scoreDocs;
//
//		} catch (Exception e) {
//			LogManager.error(EBookIndexSearch.class, e.getMessage());
//		}
//
//		return result;
//	}
//
//	public static Document searchDocument(IndexSearcher searcher,
//			String defaultField, String queryString) {
//		Document result = null;
//		try {
//			Analyzer analyzer = new WhitespaceAnalyzer();
//			Query query = new QueryParser(defaultField, analyzer).parse(queryString);
//
//			TopDocCollector collector = new TopDocCollector(1);
//			searcher.search(query, collector);
//
//			ScoreDoc[] docs = collector.topDocs().scoreDocs;
//
//			if (docs != null && docs.length == 1) {
//				result = searcher.doc(docs[0].doc);
//			} else {
//				throw new Exception("scoreDocs returned null or more than 1 hit.");
//			}
//
//		} catch (Exception e) {
//			LogManager.error(EBookIndexSearch.class, e.getMessage());
//		}
//
//		return result;
//	}
//}
