/**
 * 
 */
package org.cambridge.ebooks.production.ebook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.delivery.EBookDeliveryWorker;
import org.cambridge.ebooks.production.ebook.content.EBookContentBean;
import org.cambridge.ebooks.production.ebook.content.EBookContentDAO;
import org.cambridge.ebooks.production.ebook.content.EBookContentWorker;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.AssetWorker;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.ListUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.StringUtil;
import org.cambridge.util.Misc;

/**
 * 
 * @author rvillamor
 * @recode: jgalang - 28 Aug 2009 
 *
 */

public class EBookWorker {
    private static final Log4JLogger LogManager = new Log4JLogger(EBookWorker.class);
    private static final EBookSearchWorker worker = new EBookSearchWorker();
    
    private static final String CAMBRIDGE_ISBN_PREFIX = "978";
    private static final int STANDARD_ISBN_LENGTH = 13;
        
    public static Set<String> getQuickSearchResult(String searchTerm) {
        Set<String> bookIds = new HashSet<String>();
        
        StringBuilder formatted = new StringBuilder();
        if (searchTerm.length() < STANDARD_ISBN_LENGTH-3) 
        {
            formatted.append("9*");
            formatted.append(searchTerm);
            formatted.append("*");
        }
        else if (STANDARD_ISBN_LENGTH-searchTerm.length() > 0) 
        {
            formatted.append(CAMBRIDGE_ISBN_PREFIX.substring(0, STANDARD_ISBN_LENGTH-searchTerm.length()));
            formatted.append(searchTerm);
        } 
        else 
        {
            formatted.append(searchTerm);
        }
        
        StringBuilder luceneQuery = new StringBuilder();
        luceneQuery.append("isbn:");
        luceneQuery.append(formatted);
        luceneQuery.append(" OR title:");
        luceneQuery.append(searchTerm);
        
        luceneQuery.append(" OR alt_isbn_hardback:");
        luceneQuery.append(searchTerm);
        luceneQuery.append(" OR alt_isbn_paperback:");
        luceneQuery.append(searchTerm);
    
        LogManager.info(EBookWorker.class, "SOLR Query: " + luceneQuery);
        
        for (BookMetadataDocument doc : worker.searchBookCore(luceneQuery.toString())) 
            bookIds.add(doc.getBookId());
        
        return bookIds;
    }
    
    public static List<EBookBean> wrapToEBookBean(List<EBook> ebooks) {
        List<EBookBean> result = new ArrayList<EBookBean>();
        
        long start = System.currentTimeMillis();
        
        if (ebooks != null && !ebooks.isEmpty()) {
            for(EBook ebook : ebooks) {
                EBookBean bean = new EBookBean(ebook);
                bean.setTitle(StringUtil.formatTitle(bean.getTitle())); 
                result.add(bean);
            }
        }
        
        long end = System.currentTimeMillis();
        double done = (end - start) * 0.001;
        System.out.println("Done wrapping: [" + done + "] seconds.");
        return result;
    }
    
    public static List<EBookBean> setBookTitle(List ebookListFromDB, List eBookListFromIndex) {
        List<EBookBean> result = new ArrayList<EBookBean>();
        if(ListUtil.isNotNullAndEmpty(ebookListFromDB) && ListUtil.isNotNullAndEmpty(eBookListFromIndex)) {
            LogManager.info(EBookWorker.class, "[Number of EBooks]  DB Size: "    + ebookListFromDB.size() + " Index Size: " + eBookListFromIndex.size());
            
            eBookListFromIndex.retainAll(ebookListFromDB);
            if(ListUtil.isNotNullAndEmpty(eBookListFromIndex)) {
                for(int index=0; index<eBookListFromIndex.size(); index++) {
                    Object returnedObjFromIndex = eBookListFromIndex.get(index);
                    Object returnedObjFromDB = ListUtil.searchObject(ebookListFromDB, returnedObjFromIndex);
                    if(returnedObjFromDB != null) {
                        ((EBookBean)returnedObjFromIndex).setEbook((EBook)returnedObjFromDB);
                    }
                } 
                result = resolveBrowseEbooks(eBookListFromIndex);
            }
        }
        
        return result;
    }
    
    private static List<EBookBean> resolveBrowseEbooks(List<EBookBean> data) {
            List<EBookBean> results = new ArrayList<EBookBean>();
            String title = null;
            
            EBookBean book = null;
                   
            if(ListUtil.isNotNullAndEmpty(data)) {
                int size = data.size();
                int status = 0;
                
                for(int i=0;i<size;i++){
                    EBookBean vo = data.get(i);
                    
                    status = vo.getEbook().getStatus();
                    title = vo.getTitle();           
                    
                    book = new EBookBean();
                    book.setIsbn(vo.getIsbn());           
                    book.setTitle(StringUtil.correctHtmlTags(title));
                    book.setAlphasort(vo.getAlphasort());
                    book.setBookId(vo.getBookId());
                    book.setEbook(vo.getEbook());
                    book.setStatusCode(status);
                    book.setStatusDescription(Status.getStatus(status).getDescription());                
                    results.add(book);
                    
                }
            }
            return results;
        }
    
    public static Map<String, List<EBookBean>> formatTitleSortedList(List<EBookBean> list) {
        Map<String, List<EBookBean>> result = new LinkedHashMap<String, List<EBookBean>>();
        ArrayList<String> firstLetters = new ArrayList<String>();
        for (EBookBean ebb : list) {
            String alphasort = Misc.isNotEmpty(ebb.getAlphasort()) ? ebb.getAlphasort() : "";
            String firstLetter = alphasort.length()>0 ? alphasort.substring(0, 1).toUpperCase() : "";
                                   
            if(!firstLetters.contains(firstLetter)){
                firstLetter = alphasort.substring(0,1).toUpperCase();
                firstLetters.add(firstLetter);
                
                List<EBookBean> letterGroup = new ArrayList<EBookBean>();
                letterGroup.add(ebb);
                result.put(firstLetter, letterGroup);                
            } else {
                result.get(firstLetter).add(ebb);
            }
        }
        return result;
    }   
    
    public void updateEbook(String bookId) {        
        List<EBookContent> ebookContents = EBookContentDAO.searchContentsByBookId(bookId);

        try {
            String status = String.valueOf(Status.getBookStatus(ebookContents));            
            
            manageEvent(bookId, status);
            
            EBookDAO.updateEBookStatus(status, bookId);
        } catch (Exception e) {
            LogManager.error(EBookWorker.class, e.getMessage());
            e.printStackTrace();
        }
    }
    
    public String updateEbook(String bookId, List<EBookContentBean> ebookContents) {    
        String crossRefXmlFileName = null;
        try {
            String status = String.valueOf(Status.getBookStatusFromContentListPage(ebookContents));            
            
            crossRefXmlFileName = manageEvent(bookId, status);
            
            EBookDAO.updateEBookStatus(status, bookId);
        } catch (Exception e) {
            LogManager.error(EBookWorker.class, e.getMessage());
            e.printStackTrace();
        }
        return crossRefXmlFileName;
    }
    
    /**
     * Manages the event. Event triggers the Audit Trail, Asset Store
     * and Indexer. 
     * 
     * @param bookId - Book id
     * @param status - New Status of the book 
     */
    private String manageEvent(String bookId, String status){
        String crossRefXmlFileName = null;
        try{
            // Get EBook status based from DB
            List<EBook> ebookListFromDB = EBookDAO.searchAllEBooksByBookId(bookId);
            
            if(ebookListFromDB.size() > 0){
                // Get EBook details from DB
                EBook ebook = ebookListFromDB.get(0);
                
                //Log the status
                LogManager.info(getClass(), "Audit: "+ bookId+" current status: "+status);
                
                if(ebook.getStatus() < Integer.parseInt(status)){
                    // Audit Ebook status
                    EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    
                    // If status is published:
                    // 1. Send to Asset Store
                    // 2. Create Indexer for EBooks Online
                    if(Integer.parseInt(status) == 7 && ebook.getBookData().get(0).getOnlineFlag().equals("Y")){
                        AssetWorker.sendAssetEvent(ebook.getIsbn());
                        
                        //Deliver to crossref
                        List<EBookContentBean> ebookContentList = (new EBookContentWorker()).ebookContents(ebook.getBookId());
                        
                        EBookDeliveryWorker worker = new EBookDeliveryWorker();
                        crossRefXmlFileName = worker.deliverEBook(ebook.getBookId(), ebook.getIsbn(), ebookContentList, true, EBookDeliveryWorker.XML_AND_SEND);
                    }else if(Integer.parseInt(status) == 8){
                        // If status is Unpublished then delete the ebook details in the indexer
                        //SearchIndexWorker.sendMessage(ebook.getBookId(),"unpublish");
                    }
                } else if(ebook.getStatus() == 2){ // Status changed from PROOFED to LOADED
                    if(Integer.parseInt(status) == 0 ){
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    }
                } else if(ebook.getStatus() == 4){ // Status changed from APPROVE to PROOFED
                    if(Integer.parseInt(status) == 2 ){
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    }
                } else if(ebook.getStatus() == 7){ // Status changed from PUBLISHED to APPROVE
                    if(Integer.parseInt(status) == 4 ){
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    }
                } else if(ebook.getStatus() == 8){ // Status changed from UNPUBLISHED to PUBLISHED
                    if(Integer.parseInt(status) == 7 && ebook.getBookData().get(0).getOnlineFlag().equals("Y")){
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                        AssetWorker.sendAssetEvent(ebook.getIsbn());
                        
                        //Deliver to crossref
                        List<EBookContentBean> ebookContentList = (new EBookContentWorker()).ebookContents(ebook.getBookId());
                        
                        EBookDeliveryWorker worker = new EBookDeliveryWorker();
                        crossRefXmlFileName = worker.deliverEBook(ebook.getBookId(), ebook.getIsbn(), ebookContentList, true, EBookDeliveryWorker.XML_AND_SEND);
                    }
                } else if(ebook.getStatus() == 9){
                    if(Integer.parseInt(status) == 4){ // Status changed from EMBARGO to APPROVE
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    }else if(Integer.parseInt(status) == 7){ // Status changed from EMBARGO to PUBLISHED
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                        AssetWorker.sendAssetEvent(ebook.getIsbn());//                        
                        
                        //Deliver to crossref
                        List<EBookContentBean> ebookContentList = (new EBookContentWorker()).ebookContents(ebook.getBookId());
                        
                        EBookDeliveryWorker worker = new EBookDeliveryWorker();
                        crossRefXmlFileName = worker.deliverEBook(ebook.getBookId(), ebook.getIsbn(), ebookContentList, true, EBookDeliveryWorker.XML_AND_SEND);
                    }
                } else if(ebook.getStatus() == 100){
                    if(Integer.parseInt(status) == 8){ // Status changed from DELETED to UNPUBLISHED
                        EventTracker.sendEBookEvent(bookId, status, HttpUtil.getUserFromCurrentSession().getUsername());
                    }
                }
            }
        }catch(Exception e){
            LogManager.error(EBookWorker.class, e.getMessage());
            e.printStackTrace();
        }
        
        LogManager.info(getClass(), bookId + " Logging Event Sent to EJB");
        
        return crossRefXmlFileName;
    }
    
    private static String generateInClauseStatement(int inLength, int startValue){
        StringBuilder sb = new StringBuilder();        
        for(int i=(startValue-1); i < (startValue -1 + inLength); i++){
            sb.append(" ?" + (i+1) + "," );
        }
        String out = sb.toString();
        //remove the last ,
        return out = out.substring(0, out.length()-1);
    }
    
    public static String generateSrTableFilterQuery(
            String srProductFilter,
            String srPublisherFilter,
            int bookIdsLength,
            boolean hasLimitedAccess,
            boolean hasLetterFilter,
            boolean hasPublisherFilter,
            boolean hasBookIds,
            String sortBy,
            boolean ascending,
            boolean isFirstRun) {
        
        StringBuilder query = new StringBuilder();

        int param = 0;
        int publisherIdparam = 0;
        int alphasortParam = 0;
        String sortOrder = ascending ? "ASC" : "DESC";
        
        query.append(    "SELECT e.book_id, e.isbn, e.series_code, e.modified_date, "                 );
        query.append(                   " e.modified_by, e.title, e.title_alphasort, "                );
        query.append(                   " ei.pub_date_print, ei.pub_date_online, "                    );
        query.append(                   " DECODE(ec.book_id, null, 200, e.status) status "            );
        query.append(    "FROM ebook e "                                                              );
        query.append(    "RIGHT JOIN ebook_content ec on ec.book_id = e.book_id "                     );
        query.append(    "RIGHT JOIN ebook_isbn_data_load ei on ei.isbn = e.isbn "                    );
        query.append(    "WHERE (ec.type='xml' "                                                      );
        
        if (hasLimitedAccess) {
            query.append(    "AND e.book_id IN(SELECT book_id FROM cbo_cupadmin_ebook_access ccea "   );
            query.append(                   " WHERE ccea.user_id = ?" + (++param) + ")"               );
        }

        if (hasLetterFilter) {
            alphasortParam = ++param;
            query.append( "AND REGEXP_INSTR(UPPER(e.title_alphasort), ?" + (alphasortParam) + ") = 1 ");
        }
        
        if (hasBookIds) {
            String bookIds = generateInClauseStatement(bookIdsLength, ++param);
            param += (bookIdsLength-1);
            query.append(    "AND e.book_id IN (" + bookIds + ") "                                     );
        }

        publisherIdparam = ++param;
        StringBuilder queryWrapper = new StringBuilder(query.toString());        
        queryWrapper.append("AND e.isbn IN ( " );
        queryWrapper.append(    "SELECT ean_number as isbn "                 );
        queryWrapper.append(    "FROM core_isbn_cbo "                        );
        if(isFirstRun){
            queryWrapper.append("where cbo_production_status not in (7)");
        }
        if(!srPublisherFilter.equals("ALL")){    
            queryWrapper.append(    "WHERE cbo_product_group "                );
            if (! srProductFilter.equals("ALL")){
                queryWrapper.append(    "= ?"    + publisherIdparam            );
            } else {
                queryWrapper.append("   IN ( SELECT cbo_product_group "                );
                queryWrapper.append("   FROM sr_cbo_product_group_ora "            );
                queryWrapper.append("   WHERE publisher_code = ?" + (publisherIdparam)    );
                queryWrapper.append("   ) ");
            }
        }
        queryWrapper.append(") ");
        
        if(isFirstRun){
            queryWrapper.append("AND e.status not in (7)");
        }
        
        queryWrapper.append(" )");
        
        if (sortBy.equals(EBookManagedBean.SORT_BY_STATUS)) {
            queryWrapper.append(" ORDER BY "                                                            );
            queryWrapper.append(" status " + sortOrder + ", ");
            queryWrapper.append(    " UPPER(title_alphasort)"                                            );
            queryWrapper.append(    " " + sortOrder                                                        );

        } 
        else if (sortBy.equals(EBookManagedBean.SORT_BY_ISBN)) {
        
        queryWrapper.append(" ORDER BY isbn"                                                        );
        queryWrapper.append(    " " + sortOrder                                                        );

        } 
        else if (sortBy.equals(EBookManagedBean.SORT_BY_PUBLISH_ONLINE))
        {
            
        queryWrapper.append(" ORDER BY pub_date_online"                                                );
        queryWrapper.append(    " " + sortOrder                                                        );
            
        } 
        else if (sortBy.equals(EBookManagedBean.SORT_BY_PUBLISH_PRINT))
        {
            
        queryWrapper.append(" ORDER BY pub_date_print"                                                );
        queryWrapper.append(    " " + sortOrder                                                        );

        }

        return queryWrapper.toString();
    }
    
    public static String generateSrStatusFilterQuery(
            String srProductFilter,
            String srPublisherFilter,
            int statusFilterLength,
            int bookIdsLength,
            boolean hasLimitedAccess,
            boolean hasLetterFilter,
            boolean hasPublisherFilter,
            boolean hasBookIds,
            String sortBy,
            boolean ascending,
            boolean isFirstRun) {
        
        int param = 0;
        int publisherIdparam = 0;
        int alphasortParam = 0;
        
        String sortOrder = ascending ? "ASC" : "DESC";
        
        StringBuilder query = new StringBuilder(); 

        query.append("SELECT e.book_id, e.isbn, e.series_code, e.modified_date, e.modified_by, " );
        query.append(                "e.title, e.title_alphasort, ei.pub_date_print, ei.pub_date_online, ");
        query.append(                "DECODE(ec.book_id, null, 200, e.status) status "                    );
        query.append(    "FROM ebook e "                                                            );
        query.append(    "RIGHT JOIN ebook_content ec on ec.book_id = e.book_id "                );
        query.append(    "RIGHT JOIN ebook_isbn_data_load ei on ei.isbn = e.isbn "                );
        query.append("WHERE (ec.type='xml' "                                                              );
        
        if (hasLimitedAccess) 
        {
            query.append("AND e.book_id ");
            query.append("IN(SELECT book_id FROM cbo_cupadmin_ebook_access ccea WHERE ccea.user_id = ?"+ (++param) + ") ");
        }
        
        if (hasLetterFilter) 
        {
            alphasortParam = (++param);
            query.append("AND REGEXP_INSTR(UPPER(e.title_alphasort), ?" + alphasortParam + ") = 1 ");
        }

        if (hasBookIds) 
        {
            String bookIds = generateInClauseStatement(bookIdsLength, ++param);
            param += (bookIdsLength-1);
            query.append("AND e.book_id IN (" + bookIds + ") ");
        }
            
        publisherIdparam = (++param);
        String filter = generateInClauseStatement(statusFilterLength, ++param);
        
        query.append("AND e.status in (" + filter + ") ");
        
        StringBuilder queryWrapper = new StringBuilder(query.toString());
        
        
        queryWrapper.append("AND e.isbn IN ( " );

        queryWrapper.append(    "SELECT ean_number as isbn "                 );
        queryWrapper.append(    "FROM core_isbn_cbo "                        );
        if(isFirstRun){
            queryWrapper.append("where cbo_production_status not in (7)");
        }

        if(!srPublisherFilter.equals("ALL")){
            queryWrapper.append(    "WHERE cbo_product_group "                );
            
            if (! srProductFilter.equals("ALL")){
                queryWrapper.append(    "= ?"    + publisherIdparam            );
            } else {
                queryWrapper.append("   IN ( SELECT cbo_product_group "                );
                queryWrapper.append("   FROM sr_cbo_product_group_ora "            );
                queryWrapper.append("   WHERE publisher_code = ?" + (publisherIdparam)    );
                queryWrapper.append("   ) ");
            }
        }
        queryWrapper.append(") ");
        
        if(isFirstRun){
            queryWrapper.append("AND e.status not in (7)");
        }
        queryWrapper.append(" )");
        
        if (sortBy.equals(EBookManagedBean.SORT_BY_STATUS)) 
        {
            queryWrapper.append(" ORDER BY"                                                            );
            queryWrapper.append(" status " + sortOrder + ", "                                       );
            queryWrapper.append(    " UPPER(title_alphasort)"                                        );
            queryWrapper.append(    " " + sortOrder                                                    );
            
        } else if (sortBy.equals(EBookManagedBean.SORT_BY_ISBN)) {
            queryWrapper.append(" ORDER BY isbn"                                                    );
            queryWrapper.append(    " " + sortOrder                                                    );
        
        } else if (sortBy.equals(EBookManagedBean.SORT_BY_PUBLISH_ONLINE)){
            queryWrapper.append(" ORDER BY pub_date_online"                                            );
            queryWrapper.append(    " " + sortOrder                                                    );
        
        } else if (sortBy.equals(EBookManagedBean.SORT_BY_PUBLISH_PRINT)){
            queryWrapper.append(" ORDER BY pub_date_print"                                            );
            queryWrapper.append(    " " + sortOrder                                                    );
        }
                
        return queryWrapper.toString();
    }
    
    
}
