package org.cambridge.ebooks.production.ebook.content;

//a marker interface to compare list of EBookContent class to list of EBookContentBean
public interface EBookContentComponent {
	
	String getContentId();
	
}
