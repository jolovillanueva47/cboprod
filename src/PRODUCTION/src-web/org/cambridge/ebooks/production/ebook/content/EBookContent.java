package org.cambridge.ebooks.production.ebook.content;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.cambridge.ebooks.production.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.production.solr.bean.WordsContentItemDocument;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.util.Misc;

/**
 * Revision
 * 20131207 alacerna - added method to display words in JSP
 * 
 */

public class EBookContent {
	
	private EBookContentWorker ebookContentWorker = new EBookContentWorker();
	private boolean lastContent;
	
	public List<WordsContentItemDocument>getDictionaryDetails(){
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		String bookId = request.getParameter("bookId");
			
		List<WordsContentItemDocument> content = ebookContentWorker.ebookDictionary(bookId);
			
		return content;
		
		
	}
	
	public List<EbookChapterDetailsBean> getChapterDetails(){
		
		List<EbookChapterDetailsBean> result = new ArrayList<EbookChapterDetailsBean>();
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String bookId = request.getParameter("bookId");
		String page = request.getParameter("page");
		
		if((bookId != null && bookId.length() > 0) && (page != null && page.length() > 0))
		{
			EBookContentBean content = ebookContentWorker.ebookContents(bookId, Integer.parseInt(page));
			
			if(content != null)
			{
				EbookChapterDetailsBean chBean = new EbookChapterDetailsBean();
				chBean.setContentId(content.getContentId());
				chBean.setContentType(content.getType());	
				chBean.setContentTitle(content.getTitle());
				chBean.show();
				
				//take additional information from content
				//chBean.setTitle(content.getTitle());
				chBean.setDisplayIndented(content.isIndented());
				
				result.add(chBean);
				
			}
			
			EBookContentBean bean = ebookContentWorker.ebookContents(bookId, (Integer.parseInt(page) + 1)); //EBookContentIndexSearch.ebookContents(bookId, false, (Integer.parseInt(page) + 1));
			if(bean == null || Misc.isEmpty(bean.getContentId()))
				lastContent = true;
			
		}
		return result;
	}

	public boolean isLastContent() {
		return lastContent;
	}
}
