package org.cambridge.ebooks.production.ebook.content;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.jpa.common.EBookContent;

public class EBookInsertBean implements EBookContentComponent {
	
	// insertId renamed to contentId to implement contentId of EBookContentComponent interface
	private String contentId;
	
	private String insertTitle;
	private String insertAlphasortTitle;
	private String insertFilename;
	
	private EBookContent ebookContent;
	private EBookInsertWorker ebookInsertWorker = new EBookInsertWorker();
	private SelectItem[] options;
	private boolean update;
	
	
	public EBookInsertBean() {}
	
	public boolean isUpdate() {
		return update;
	}
	
	public void setUpdate(boolean update) {
		this.update = update;
	}
	
	public void valueChanged(ValueChangeEvent event){
		this.update = true;
	}
	
	public String getContentId() {
		return contentId;
	}

	public void setContentId(String insertId) {
		this.contentId = insertId;
	}


	public String getInsertTitle() {
		return insertTitle;
	}

	public void setInsertTitle(String insertTitle) {
		this.insertTitle = insertTitle;
	}

	public String getInsertAlphasortTitle() {
		return insertAlphasortTitle;
	}

	public void setInsertAlphasortTitle(String insertAlphasortTitle) {
		this.insertAlphasortTitle = insertAlphasortTitle;
	}

	public String getInsertFilename() {
		return insertFilename;
	}

	public void setInsertFilename(String insertFilename) {
		this.insertFilename = insertFilename;
	}

	public EBookContent getEbookContent() {
		return ebookContent;
	}

	public void setEbookContent(EBookContent ebookContent) {
		this.ebookContent = ebookContent;
	}
	
	public SelectItem[] getOptions() {
		return options;
	}

	public void setOptions(SelectItem[] options) {
		this.options = options;
	}
	
	public boolean equals(Object o) { 
		boolean result = false;
		
		if (o instanceof EBookContentComponent) {
			EBookContentComponent b = (EBookContentComponent) o;
			if( b.getContentId().equals(this.getContentId()) )
			{
				result = true;
			}
		}
		
		return result;
	}
	
	public String getIsbnBreakdown() {
		//return ebookInsertWorker.createIsbnBreakdown(getEbookContent().getFileName());
		return ebookInsertWorker.createIsbnBreakdown(getInsertFilename());
	}

}
