package org.cambridge.ebooks.production.ebook;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectBoolean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.constant.SessionAttribute;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.delivery.BatchDelivery;
import org.cambridge.ebooks.production.ebook.content.EBookContentDAO;
import org.cambridge.ebooks.production.ebook.content.EBookContentWorker;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.jpa.publisher.Publisher;
import org.cambridge.ebooks.production.jpa.publisher.SrProduct;
import org.cambridge.ebooks.production.jpa.publisher.SrPublisher;
import org.cambridge.ebooks.production.jpa.user.EBookAccess;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistencePager;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
import org.cambridge.util.Misc;

/**
 * @author rvillamor
 * @modified by C2 14 May 2009
 * 
 */
public class EBookManagedBean {
    private static final Log4JLogger LOGGER = new Log4JLogger(
            EBookManagedBean.class);

    public static final String SORT_BY_ISBN = "isbn";
    public static final String SORT_BY_STATUS = "status";
    public static final String SORT_BY_PUBLISH_ONLINE = "online";
    public static final String SORT_BY_PUBLISH_PRINT = "print";

    private static final String ACTION_TYPE_ATTRIBUTE = "actionType";
    private static final String ACTION_VALUE = "actionValue";
    private static final String ACTION_PAGER = "PAGER";
    private static final String ACTION_FILTER = "FILTER";
    private static final String ACTION_SORT = "SORT";
    private static final String ACTION_ATOZ = "ATOZ";
    private static final String ACTION_PUBLISHERS = "PUBLISHERS";
    private static final String ACTION_SR_PUBLISHERS = "SRPUBLISHERS";
    private static final String ACTION_SHOW_ALL = "ALL";
    private static final String ACTION_QUICK_SEARCH = "SEARCH";

    private static final String NO_FILTERS = "-2";
    private static final String APPROVED_STR = "4";
    private static final int APPROVED = 4;
    private static final String PUBLISH_SUCCESSFUL1 = "total content.";
    private static final String PUBLISH_SUCCESSFUL2 = "eBook(s) successfully published.";
    private static final String NO_EBOOK_TO_PUBLISH = "No eBooks selected.";
    private static final String INVALID_ISBN_SEARCH = "Invalid search.";
    private static final String DEFAULT_SORT = SORT_BY_STATUS;
    private static final String FILTER = "_filter";
    public static final String NO_PUBLISHER_FILTER = "-1";

    private String sortBy = DEFAULT_SORT;
    private String filterBy = NO_FILTERS;
    private String[] filterByArr;

    private boolean reverseSort = false;
    private SelectItem[] filterByOptions;

    private List<EBookBean> ebookList = new ArrayList<EBookBean>();
    private Map<String, List<EBookBean>> alphabeticalList = new LinkedHashMap<String, List<EBookBean>>();
    private List<String> alphaKeyList = new ArrayList<String>();
    private List<String> publishList = new ArrayList<String>();
    private UISelectBoolean uiSelectAllApproved = new UISelectBoolean();

    private int resultsPerPage;
    private int goToPage;
    private String letterFilter = "A";
    private String publisherFilter;
    private String srPublisherFilter;
    private String srProductFilter;
    private String action = ACTION_FILTER;
    private List<SelectItem> pages;
    private PersistencePager<EBook> pager;
    private String downloadSummarySize;
    private int summarySize;
    private UIInput uiSearchTerm;

    private EBookBean toDelete = new EBookBean();
    private int ebookListSize;
    private boolean displayPublish;

    private List<Publisher> publisherList = new ArrayList<Publisher>();
    private List<SrPublisher> srPublisherList = new ArrayList<SrPublisher>();
    private List<SrProduct> srProductList = new ArrayList<SrProduct>();
    private boolean hasPublisherFilter = true; // Misc.isNotEmpty(publisherFilter);

    private static EntityManagerFactory emf = Persistence
            .createEntityManagerFactory(PersistentUnits.EBOOKS.toString());

    private String initBooks;
    private boolean useDefaultLetterFilter = true;
    private boolean isFirstRun = true;

    public String getInitBooks() {
        LOGGER.info("getInitBooks Started");

        LOGGER.info("action = " + action);

        srPublisherList();
        initSrPublisherFilter();

        if (action.equals(ACTION_ATOZ)) {
            doSearch();
        } else if (action.equals(ACTION_PUBLISHERS)) {
            doSearch();
        } else if (action.equals(ACTION_SR_PUBLISHERS)) {
            doSearch();
        } else if (action.equals(ACTION_PAGER)) {
            ebookList = EBookWorker.wrapToEBookBean(pager
                    .getCurrentResults(letterFilter));
        } else if (action.equals(ACTION_SORT)) {
            doSearch();
        } else if (action.equals(ACTION_SHOW_ALL)) {
            doSearch();
        } else if (action.equals(ACTION_FILTER)) {
            doSearch();
        } else if (action.equals(ACTION_QUICK_SEARCH)) {
            filterByArr = new String[] { NO_FILTERS };
            letterFilter = null;
            doSearch();
        } else {
            LOGGER.info("NONE");
        }

        // check if the an item in the ebooklist contains a status code of 4
        checkBooksForPub();

        long start = System.currentTimeMillis();

        summarySize = EBookDAO.summaryReportSize(srPublisherFilter);
        downloadSummarySize = "Download Summary (" + summarySize + " eBooks)";

        long end = System.currentTimeMillis();
        double done = (end - start) * 0.001;
        LOGGER.info("Book summary done: [" + done + "] seconds.");
        return initBooks;
    }

    public void doAction(ActionEvent event) {
        action = (String) event.getComponent().getAttributes()
                .get(ACTION_TYPE_ATTRIBUTE);
        letterFilter = (String) event.getComponent().getAttributes()
                .get(ACTION_VALUE);
        useDefaultLetterFilter = false;
        isFirstRun = false;
        if (action.equals(ACTION_QUICK_SEARCH)
                && Misc.isEmpty((String) uiSearchTerm.getValue())) {
            action = ACTION_FILTER;
            letterFilter = "A";

            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(INVALID_ISBN_SEARCH);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("alertMessage", message);
        }
    }

    public void doPublisherAction(ActionEvent event) {
        action = ACTION_PUBLISHERS;
        isFirstRun = false;
        uiSearchTerm.resetValue();
    }

    public void doSrPublisherAction(ActionEvent event) {
        action = ACTION_SR_PUBLISHERS;

        LOGGER.info("srPublisherFilter = " + srPublisherFilter);
        LOGGER.info("srProductFilter = " + srProductFilter);
        isFirstRun = false;
        uiSearchTerm.resetValue();
    }

    public void doSrProductAction(ActionEvent event) {
        action = ACTION_SR_PUBLISHERS;
        isFirstRun = false;

        LOGGER.info("srPublisherFilter = " + srPublisherFilter);
        LOGGER.info("srProductFilter = " + srProductFilter);

    }

    @SuppressWarnings("unchecked")
    public void doSearch() {

        resultsPerPage = 100;
        goToPage = 0;
        letterFilter = useDefaultLetterFilter ? "A" : letterFilter;
        List<EBookAccess> accessList = (ArrayList<EBookAccess>) HttpUtil
                .getAttributeFromCurrentSession(SessionAttribute.EBOOK_ACCESS);

        boolean hasNoFilters = checkIfNoFilter(filterByArr);
        boolean hasFullAccess = accessList.contains(EBookAccess.ALL_EBOOKS_ACCESS);
        boolean hasOrgAccess = accessList.contains(EBookAccess.ALL_PUBLISHER_EBOOKS_ACCESS);
        boolean hasLimitedAccess = (hasFullAccess || hasOrgAccess) ? false : true;
        boolean hasLetterFilter = Misc.isNotEmpty(letterFilter);
        boolean hasBookIds = action.equals(ACTION_QUICK_SEARCH);

        Set<String> bookIds = new HashSet<String>();
        if (hasBookIds) {
            bookIds = EBookWorker.getQuickSearchResult(((String) uiSearchTerm.getValue()).trim());
            hasLetterFilter = false;
        }

        if (hasBookIds && bookIds.size() == 0) {
            ebookList = new ArrayList<EBookBean>();
        } else {
            String query = "";
            if (hasNoFilters) {
                query = EBookWorker.generateSrTableFilterQuery(
                        srProductFilter,
                        srPublisherFilter, 
                        bookIds.size(), 
                        hasLimitedAccess,
                        hasLetterFilter, 
                        hasPublisherFilter, 
                        hasBookIds,
                        sortBy, 
                        !reverseSort,
                        isFirstRun);
            } else if (!hasNoFilters) {
                query = EBookWorker.generateSrStatusFilterQuery(
                        srProductFilter, 
                        srPublisherFilter, 
                        filterByArr.length,
                        bookIds.size(), 
                        hasLimitedAccess, 
                        hasLetterFilter,
                        hasPublisherFilter, 
                        hasBookIds, 
                        sortBy, 
                        !reverseSort,
                        isFirstRun);
            }
            
            List<String> params = new ArrayList<String>();
            if (hasLimitedAccess) {
                params.add(HttpUtil.getUserFromCurrentSession().getUserId());
            }
            if (hasLetterFilter) {
                params.add("^" + letterFilter);
            }
            if (hasBookIds) {
                params.addAll(bookIds);
            }
            params.add(srProductFilter.equals("ALL") ? srPublisherFilter : srProductFilter);
            if (!hasNoFilters) {
                params.addAll(Arrays.asList(filterByArr));
            }

            long start = System.currentTimeMillis();

            pager = new PersistencePager<EBook>(resultsPerPage, query,
                    EBook.EBOOK_RESULT_SET_MAPPING,
                    params.toArray(new String[0]));

            long end = System.currentTimeMillis();
            double done = (end - start) * 0.001;

            // LOGGER.info("Filtered Query: " + query);
            // LOGGER.info("params: " + Arrays.asList(params.toArray(new
            // String[0])));
            LOGGER.info("Query done: [" + done + "] seconds.");

            start = System.currentTimeMillis();
            ebookList = EBookWorker.wrapToEBookBean(pager.getCurrentResults());
            end = System.currentTimeMillis();
            done = ((end - start) * 0.001);
            LOGGER.info("Processing done: [" + done + "] seconds.");
        }

        // -C2- Manually add this request parameter to run the update script.
        String requestUpdateStatus = HttpUtil
                .getHttpServletRequestParameter("updateStatus");
        if (requestUpdateStatus != null && requestUpdateStatus.equals("1")) {
            updateEBookTable();
        }

        // -C2- Manually add this request parameter to run the batch delivery
        // script.
        String batchDeliver = HttpUtil
                .getHttpServletRequestParameter("batchDeliver");
        if (batchDeliver != null && batchDeliver.equals("go")) {
            try {
                BatchDelivery.run();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        // sortList();
        publishList = new ArrayList<String>();
        uiSelectAllApproved.resetValue();
        
        isFirstRun = false;
        initGoToPage();
    }

    public void setCurrentPage(ValueChangeEvent event) {
        LOGGER.info("Go to page " + (Integer) event.getNewValue());
        pager.setCurrentPage((Integer) event.getNewValue());
        goToPage = pager.getCurrentPage();
    }

    public void setPageSize(ValueChangeEvent event) {
        LOGGER.info("Set size " + (Integer) event.getNewValue());
        pager.setPageSize((Integer) event.getNewValue());
        initGoToPage();
    }

    public void nextPage(ActionEvent event) {
        pager.next();
        goToPage = pager.getCurrentPage();
        action = ACTION_PAGER;
    }

    public void previousPage(ActionEvent event) {
        pager.previous();
        goToPage = pager.getCurrentPage();
        action = ACTION_PAGER;
    }

    public void firstPage(ActionEvent event) {
        pager.first();
        goToPage = pager.getCurrentPage();
        action = ACTION_PAGER;
    }

    public void lastPage(ActionEvent event) {
        pager.last();
        goToPage = pager.getCurrentPage();
        action = ACTION_PAGER;
    }

    public void setPagerValues(ActionEvent event) {
        LOGGER.info("Set pager values.");
        resultsPerPage = pager.getPageSize();
        goToPage = pager.getCurrentPage();
        action = ACTION_PAGER;
    }

    private void initGoToPage() {
        pages = new ArrayList<SelectItem>();
        for (int p = 0; p < pager.getMaxPages(); p++) {
            pages.add(new SelectItem(p, String.valueOf(p + 1)));
        }
    }

    public void sortList(ActionEvent event) {
        String newSort = (String) event.getComponent().getAttributes()
                .get("sortBy");
        if (sortBy.equals(newSort)) {
            reverseSort = !reverseSort;
        } else {
            reverseSort = false;
            sortBy = newSort;
        }
        action = ACTION_SORT;
    }

    public String doFilter() {

        return null;
    }

    /**
     * method to display tooltip options
     * 
     * @return
     */
    public String getFilterUsed() {
        StringBuffer sb = new StringBuffer();
        sb.append("Current Filter: - ");

        if (checkIfNoFilter(filterByArr)) {
            String[] filtersFromCookie = getFiltersFromCookie(filterByArr);
            if (filtersFromCookie == null)
                return sb.toString() + "No Filter";
            else {
                getFilterByOptions();
                filterByArr = filtersFromCookie;
            }
        }

        // " - " is important, this is the parser used by jquery

        for (String filter : filterByArr) {
            int filterNum = Integer.parseInt(filter);
            for (int i = 0; i < filterByOptions.length; i++) {
                int filterValue = Integer.parseInt((String) filterByOptions[i]
                        .getValue());
                if (filterValue == filterNum) {
                    sb.append(filterByOptions[i].getLabel());
                    break;
                }
            }
            sb.append("<br/>");
        }
        return sb.toString();
    }

    /**
     * checks whether filtering was selected
     * 
     * @param filterArr
     * @return
     */
    private boolean checkIfNoFilter(String[] filterArr) {
        if (filterArr == null) {
            return true;
        }

        if (filterArr.length == 0) {
            return true;
        }
        for (String str : filterArr) {
            if (NO_FILTERS.equals(str)) {
                return true;
            }
        }

        return false;
    }

    /**
     * sets setDisplayPub if a book has an Approved status code. will display in
     * the table headers the publish checkbox
     */
    private void checkBooksForPub() {
        setDisplayPublish(false);
        boolean check = false;
        // there are 15k books, better check first if the filter used is either
        // NO_FILTER or APPROVED, or nothing is selected
        if (filterByArr != null) {
            // if nothing is selected, forced to check
            if (filterByArr.length == 0) {
                check = true;
            } else {
                // else check all selected filters
                for (String str : filterByArr) {
                    if (APPROVED_STR.equalsIgnoreCase(str)
                            || NO_FILTERS.equalsIgnoreCase(str)) {
                        check = true;
                        break;
                    }
                }
            }
        } else {
            // initially when session is new this is null, therefore check
            check = true;
        }
        // check if at least one ebook is for publishing
        if (check) {
            for (EBookBean eb : ebookList) {
                if (APPROVED == eb.getStatusCode()) {
                    setDisplayPublish(true);
                    break;
                }
            }
        }
    }

    public void resetBrowseOptions(ActionEvent event) {
        sortBy = DEFAULT_SORT;
        filterBy = NO_FILTERS;
        reverseSort = false;
    }

    public List<String> getAlphaKeyList() {
        alphaKeyList = new ArrayList<String>(getAlphabeticalList().keySet());
        return alphaKeyList;
    }

    public SelectItem[] getFilterByOptions() {
        EBookContentWorker ebookContentWorker = new EBookContentWorker();
        filterByOptions = ebookContentWorker.resolveAllSelectItems(filterBy,
                Status.ALL_OPTIONS);

        List<SelectItem> tmp = new ArrayList<SelectItem>();
        tmp.add(new SelectItem(NO_FILTERS, "-- No Filter --"));
        for (SelectItem si : filterByOptions) {
            tmp.add(si);
        }
        filterByOptions = tmp.toArray(new SelectItem[] {});

        return filterByOptions;
    }

    public void filterEBooks(ActionEvent event) {
        LOGGER.info("Filtering by status code: " + filterBy);
    }

    public int getEbookListSize() {
        ebookListSize = ebookList.size();
        return ebookListSize;
    }

    public void addToPublishList(ValueChangeEvent event) {
        if ((Boolean) event.getNewValue()) {
            FacesContext context = FacesContext.getCurrentInstance();
            Map<String, String> requestMap = context.getExternalContext()
                    .getRequestParameterMap();
            String bookId = (String) (event.getComponent().getAttributes()
                    .get("bookId") != null ? event.getComponent()
                    .getAttributes().get("bookId") : requestMap.get("bookId"));

            publishList.add(bookId);
            LOGGER.info("On publish list: " + bookId);
        }
    }

    public void publishCheckedBooks(ActionEvent event) {

        String msg = NO_EBOOK_TO_PUBLISH;
        EntityManager em = null;
        try {
            if (publishList.size() > 0) {
                filterBy = NO_FILTERS;
                LOGGER.info("Publishing ebooks: " + publishList);

                em = emf.createEntityManager();
                Query q1 = em
                        .createNativeQuery(getBatchPublishContentSql(publishList));
                Query q2 = em
                        .createNativeQuery(getBatchPublishEBookSql(publishList));
                em.getTransaction().begin();
                int status1 = q1.executeUpdate();
                int status2 = q2.executeUpdate();
                em.getTransaction().commit();

                msg = status2 + " " + PUBLISH_SUCCESSFUL2;
                LOGGER.info(msg + " " + status1 + " " + PUBLISH_SUCCESSFUL1);
            }

            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(msg);
            message.setSeverity(FacesMessage.SEVERITY_INFO);
            context.addMessage("alertMessage", message);
        } finally {
            JPAUtil.close(em);
        }
    }

    private static String getBatchPublishEBookSql(List<String> publishList) {
        StringBuilder sql = new StringBuilder();
        if (publishList != null && publishList.size() > 0) {
            String userName = HttpUtil.getUserFromCurrentSession()
                    .getUsername();

            sql.append("UPDATE ebook SET status =");
            sql.append(" '" + Status.PUBLISHED.getStatus() + "',");
            sql.append(" modified_date = SYSDATE,");
            sql.append(" modified_by = '" + userName + "'");
            sql.append(" WHERE book_id IN (");

            StringBuilder bookId;

            for (Iterator<String> i = publishList.iterator(); i.hasNext();) {
                bookId = new StringBuilder(i.next());

                // create audit trail
                EventTracker.sendEBookEvent(bookId.toString(),
                        Status.PUBLISHED.getStatus(), userName);

                // send to asset store
                // AssetWorker.sendAssetEvent(bookId.substring(3,
                // bookId.length()).toString());

                // Deliver to crossref
                // CrossRefDeliveryMessage.sendDeliveryEvent(bookId.toString(),
                // HttpUtil.getUserFromCurrentSession().getUsername());

                sql.append("'" + bookId + "'");
                if (i.hasNext()){
                    sql.append(",");
                }
            }

            sql.append(")");
        }
        return sql.toString();
    }

    private static String getBatchPublishContentSql(
            List<String> publishList) {
        StringBuilder sql = new StringBuilder();
        if (publishList != null && publishList.size() > 0) {
            sql.append("UPDATE ebook_content SET status =");
            sql.append(" '" + Status.PUBLISHED.getStatus() + "'");
            sql.append(", modified_date = SYSDATE ");
            sql.append(", modified_by = '"
                    + HttpUtil.getUserFromCurrentSession().getUsername() + "'");
            sql.append(", embargo_date = NULL ");
            sql.append(" WHERE book_id IN (");

            StringBuilder bookId;
            for (Iterator<String> i = publishList.iterator(); i.hasNext();) {
                bookId = new StringBuilder(i.next());

                List<EBookContent> ebookContentList = EBookContentDAO
                        .searchContentsByBookId(bookId.toString());
                for (EBookContent ebookContent : ebookContentList) {
                    // create audit trail
                    EventTracker.sendBookEvent(ebookContent.getContentId(),
                            ebookContent.getIsbn(), ebookContent.getBook()
                                    .getSeriesId(), Status.PUBLISHED
                                    .getStatus(), "Batch Publishing", HttpUtil
                                    .getUserFromCurrentSession().getUsername(),
                            ebookContent.getFileName(), ebookContent.getType());

                    EventTracker.sendEvent(
                            AuditTrail.MAKE_FILE_AVAILABLE_FOR_USERS,
                            HttpUtil.getUserFromCurrentSession().getUserId(),
                            ebookContent.getType() + " - "
                                    + ebookContent.getContentId() + " "
                                    + Status.PUBLISHED.getDescription());

                }

                sql.append("'" + bookId + "'");
                if (i.hasNext()){
                    sql.append(",");
                }
            }

            sql.append(")");
        }
        return sql.toString();
    }

    public void confirmDelete(ActionEvent event) {
        this.toDelete = (EBookBean) event.getComponent().getAttributes()
                .get("book");
        LOGGER.info("Confirming delete eBook: " + this.toDelete.getBookId());
    }

    public void deleteEbook(ActionEvent event) {
        String bookId = this.toDelete.getBookId();
        EntityManager em = emf.createEntityManager();
        EBook book = null;
        try {

            em.getTransaction().begin();
            book = em.find(EBook.class, bookId);
            em.remove(book);
            em.getTransaction().commit();
        } finally {
            JPAUtil.close(em);
        }

        LOGGER.info("Book: " + bookId + " deleted.");
    }

    public static void updateEBookTable() {
        try {
            LOGGER.info("Updating statuses...");

            List<EBook> ebookListFromDB = EBookDAO.searchAllEBooks();
            List<EBookBean> eBookListFromIndex = getBooksFromIndex();
            List<EBookBean> ebookList = EBookWorker.setBookTitle(
                    ebookListFromDB, eBookListFromIndex);

            for (EBookBean eb : ebookList) {
                List<EBookContent> ebookContents = EBookContentDAO
                        .searchContentsByBookId(eb.getBookId());
                String status = String.valueOf(Status
                        .getBookStatus(ebookContents));
                // EBookDAO.updateEBookStatus(status, eb.getBookId());
                EBookDAO.updateEBookDetails(status, eb.getTitle(),
                        eb.getAlphasort(), eb.getBookId());
            }
            LOGGER.info("Done.");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public List<EBookBean> getEbookList() {
        return ebookList;
    }

    public Map<String, List<EBookBean>> getAlphabeticalList() {
        return alphabeticalList;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public EBookBean getToDelete() {
        return toDelete;
    }

    public void setToDelete(EBookBean toDelete) {
        this.toDelete = toDelete;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    public void setInitBooks(String initBooks) {
        this.initBooks = initBooks;
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }

    public UISelectBoolean getUiSelectAllApproved() {
        return uiSelectAllApproved;
    }

    public void setUiSelectAllApproved(UISelectBoolean uiSelectAllApproved) {
        this.uiSelectAllApproved = uiSelectAllApproved;
    }

    public String[] getFilterByArr() {
        return filterByArr;
    }

    public void setFilterByArr(String[] filterByArr) {
        storeFiltersToCookie(filterByArr);
        this.filterByArr = filterByArr;
    }

    public boolean isDisplayPublish() {
        return displayPublish;
    }

    public void setDisplayPublish(boolean displayPublish) {
        this.displayPublish = displayPublish;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public int getGoToPage() {
        return goToPage;
    }

    public void setGoToPage(int goToPage) {
        this.goToPage = goToPage;
    }

    public List<SelectItem> getPages() {
        return pages;
    }

    public void setPages(List<SelectItem> pages) {
        this.pages = pages;
    }

    public PersistencePager<EBook> getPager() {
        return pager;
    }

    public void setPager(PersistencePager<EBook> pager) {
        this.pager = pager;
    }

    public String getLetterFilter() {
        return letterFilter;
    }

    public void setLetterFilter(String letterFilter) {
        this.letterFilter = letterFilter;
    }

    public UIInput getUiSearchTerm() {
        return uiSearchTerm;
    }

    public void setUiSearchTerm(UIInput uiSearchTerm) {
        this.uiSearchTerm = uiSearchTerm;
    }

    public String getDownloadSummarySize() {
        return downloadSummarySize;
    }

    public List<Publisher> getPublisherList() {
        return publisherList;
    }

    public void setPublisherList(List<Publisher> publisherList) {
        this.publisherList = publisherList;
    }

    public int getPublisherListSize() {
        if (this.publisherList != null){
            return this.publisherList.size();
        } else {
            return 0;
        }
    }

    public int getSrPublisherListSize() {
        if (this.srPublisherList != null) {
            return this.srPublisherList.size();
        }
        else {
            return 0;
        }
    }

    public SelectItem[] getPublisherItems() {
        List<SelectItem> result = new ArrayList<SelectItem>();
        if (this.publisherList != null) {
            for (Publisher publisher : publisherList){
                result.add(new SelectItem(publisher.getPublisherId(), publisher.getName()));
            }
        }

        return result.toArray(new SelectItem[0]);
    }

    public SelectItem[] getSrPublisherItems() {
        List<SelectItem> result = new ArrayList<SelectItem>();
        if (this.srPublisherList != null) {
            for (SrPublisher publisher : srPublisherList){
                result.add(new SelectItem(publisher.getPublisher_code(),publisher.getPublisher_code()));
            }
        }

        return result.toArray(new SelectItem[0]);
    }

    public SelectItem[] getSrProductItems() {
        List<SelectItem> result = new ArrayList<SelectItem>();
        if (this.srProductList != null) {
            for (SrProduct product : srProductList){
                result.add(new SelectItem(product.getProduct_code(), product.getProduct_code()));
            }
        }
        return result.toArray(new SelectItem[0]);
    }

    public String getPublisherFilter() {
        return publisherFilter;
    }

    public int getSummarySize() {
        return summarySize;
    }

    public void setSummarySize(int summarySize) {
        this.summarySize = summarySize;
    }

    public void setPublisherFilter(String publisherFilter) {
        this.publisherFilter = publisherFilter;
    }

    private void storeFiltersToCookie(String[] filters) {
        if (filters != null && filters.length > 0) {
            HttpServletResponse response = HttpUtil.getHttpServletResponse();
            StringBuilder sb = new StringBuilder();
            for (int count = 0; count < filters.length; count++) {
                if ((filters.length - 1) == count){
                    sb.append(filters[count]);
                } else { 
                    sb.append(filters[count]).append(",");
                }
            }

            User user = HttpUtil.getUserFromCurrentSession();
            if (user != null) {
                Cookie cookieFilter = new Cookie(user.getUsername()
                        + EBookManagedBean.FILTER, sb.toString());
                cookieFilter.setMaxAge(60 * 60 * 24 * 365); // store cookie for 1 year
                response.addCookie(cookieFilter);
            }
        }
    }

    private String[] getFiltersFromCookie(String[] filterByArr) {
        String[] result = null;

        if (filterByArr != null && filterByArr.length > 0) {
            for (String str : filterByArr) {
                if (NO_FILTERS.equals(str)) {
                    return result;
                }
            }
        } else {
            HttpServletRequest request = HttpUtil.getHttpServletRequest();
            Cookie[] cookies = request.getCookies();
            User user = HttpUtil.getUserFromCurrentSession();
            if (cookies != null && user != null) {
                for (Cookie cookie : cookies) {
                    if ((user.getUsername() + EBookManagedBean.FILTER).equals(cookie.getName())) {
                        result = cookie.getValue().split(",");
                    }
                }
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private void srPublisherList() {
        List<EBookAccess> accessList = (List<EBookAccess>) HttpUtil
                .getAttributeFromCurrentSession(SessionAttribute.EBOOK_ACCESS);
        boolean hasFullAccess = accessList
                .contains(EBookAccess.ALL_EBOOKS_ACCESS);
        SrPublisher publisher = HttpUtil.getSrPublisherFromCurrentSession();

        if (hasFullAccess || publisher == null){
            srPublisherList = PersistenceUtil.searchList(new SrPublisher(),SrPublisher.SELECT_PUBLISHERS);
        } else {
            srPublisherList = new ArrayList<SrPublisher>();
            srPublisherList.add(publisher);
        }
        srPublisherList = cupFirst(srPublisherList);
        srPublisherList.add(0, new SrPublisher("ALL", "ALL"));
    }

    private List<SrPublisher> cupFirst(List<SrPublisher> list) {
        List<SrPublisher> updated = new ArrayList<SrPublisher>();

        for (SrPublisher str : list) {
            if (str.getPublisher_code().equals("CUP")) {
                updated.add(0, str);
            } else {
                updated.add(str);
            }
        }

        return updated;
    }

    @SuppressWarnings("unchecked")
    private void initSrPublisherFilter() {
        if (srPublisherFilter == null) {
            List<EBookAccess> accessList = (List<EBookAccess>) HttpUtil
                    .getAttributeFromCurrentSession(SessionAttribute.EBOOK_ACCESS);
            boolean hasFullAccess = accessList
                    .contains(EBookAccess.ALL_EBOOKS_ACCESS);

            SrPublisher srPublisher = HttpUtil
                    .getSrPublisherFromCurrentSession();

            if ((hasFullAccess || srPublisher == null) && (srPublisherList != null && !srPublisherList.isEmpty())) {
                    srPublisherFilter = srPublisherList.get(0).getPublisher_code();
            } else if (srPublisher != null) {
                srPublisherFilter = srPublisher.getPublisher_code();
            }
        }

        srProductList = PersistenceUtil.searchList(new SrProduct(),
                SrProduct.FILTER_BY_PUBLISHER, srPublisherFilter);

        if (srProductList.size() > 1 || srPublisherFilter.equalsIgnoreCase("ALL")) {
            srProductList.add(0, new SrProduct("ALL", "ALL", srPublisherFilter));
        }

        // CRUDE fix, please optimize -jubs
        // case 1 - product filter is null
        if (srProductFilter == null && srProductList != null
                && !srProductList.isEmpty()) {
            srProductFilter = srProductList.get(0).getProduct_code();
        }

        // case 2 - product filter exists but not in product list
        else if (srProductFilter != null && srProductList.size() == 1) {
            srProductFilter = srProductList.get(0).getProduct_code();
        }

        // case 3 - product filter exists in current list
        else if (srProductFilter != null && srProductList.size() > 1) {
            boolean isInList = false;
            for (int x = 0; x < srProductList.size(); x++) {
                if (srProductList.get(x).getProduct_code()
                        .equals(srProductFilter)) {
                    isInList = true;
                }
            }
            if (!isInList) {
                srProductFilter = srProductList.get(0).getProduct_code();
            }
        }

        // case 4 - current product filter does not exist in current list
        else if (!srProductList.isEmpty()) {
            srProductFilter = srProductList.get(0).getProduct_code();
        }

        // case 5 - the publisher has no product
        else {
            srProductFilter = "Publisher Has No Products";
        }
    }

    private static List<EBookBean> getBooksFromIndex() {
        List<EBookBean> result = new ArrayList<EBookBean>();
        EBookSearchWorker worker = new EBookSearchWorker();

        List<BookMetadataDocument> ebookListFromIndex = worker
                .searchBookCore("content_type:book");

        try {
            if (ebookListFromIndex != null && !ebookListFromIndex.isEmpty()) {
                for (BookMetadataDocument doc : ebookListFromIndex) {
                    EBookBean ebook = new EBookBean();
                    ebook.setBookId(doc.getBookId());
                    ebook.setTitle(doc.getTitle());
                    ebook.setIsbn(doc.getIsbn());
                    ebook.setAlphasort(doc.getTitleAlphasort());

                    result.add(ebook);
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.err.println(EBookManagedBean.class
                    + " ebookTitles() [Exception]: " + e.getMessage());
        }

        return result;
    }

    public String getSrPublisherFilter() {
        return srPublisherFilter;
    }

    public void setSrPublisherFilter(String srPublisherFilter) {
        this.srPublisherFilter = srPublisherFilter;
    }

    public List<SrPublisher> getSrPublisherList() {
        return srPublisherList;
    }

    public void setSrPublisherList(List<SrPublisher> srPublisherList) {
        this.srPublisherList = srPublisherList;
    }

    public String getSrProductFilter() {
        return srProductFilter;
    }

    public void setSrProductFilter(String srProductFilter) {
        this.srProductFilter = srProductFilter;
    }

    public List<SrProduct> getSrProductList() {
        return srProductList;
    }

    public void setSrProductList(List<SrProduct> srProductList) {
        this.srProductList = srProductList;
    }

}
