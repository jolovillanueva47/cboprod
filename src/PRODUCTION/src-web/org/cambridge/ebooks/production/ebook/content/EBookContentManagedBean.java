package org.cambridge.ebooks.production.ebook.content;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.constant.RedirectTo;
import org.cambridge.ebooks.production.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.ebook.EBookDAO;
import org.cambridge.ebooks.production.ebook.EBookWorker;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.util.DateUtil;
import org.cambridge.ebooks.production.util.FileFilter;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.StringUtil;

/**
 * @author rvillamor
 */
public class EBookContentManagedBean {

    private static final Log4JLogger LogManager = new Log4JLogger(
            EBookContentManagedBean.class);

    private final static String ID = "bookId";
    private final int urlExpireLength = 5;

    private EBookContentWorker ebookContentWorker = new EBookContentWorker();
    private EBookInsertWorker ebookInsertWorker = new EBookInsertWorker();
    private String ebookModelType;
    private EBookWorker ebookWorker = new EBookWorker();

    private List<EBookContentBean> ebookContentList = new ArrayList<EBookContentBean>();
    private List<EBookInsertBean> ebookInsertList = new ArrayList<EBookInsertBean>();
    private List<EBookCrossrefBean> crossrefFiles = new ArrayList<EBookCrossrefBean>();
    private Map<String, EBookContentBean> ebookOthers = new HashMap<String, EBookContentBean>();

    private String contentDir = System.getProperty("content.url.absolute").trim();
    private String solrHomeUrl = System.getProperty("proofing.target.url").trim();
    private String frontendUrl = System.getProperty("frontend.url").trim();
    private final String reindexString = System.getProperty("reindex.url").trim();
    private final String USER_AGENT = "Mozilla/5.0";

    private EBookBean ebook;

    private String bookId;
    private String eisbn;
    private String chapterStatus;
    private SelectItem[] statusOptions;
    private String displayStatus;
    private String crossRefXmlFileName;
    private String thirdPartyFlag;

    private int startPage = 1;
    private int endPage;
    private int totalPage;

    private String initBookContents;
    
    private String proofingUrl;
    private boolean removeSignedOff;
    private boolean allowGenerateUrl;
    
    private boolean hasXsltLog;
    
    public String getInitBookContents() throws SQLException {
        String bookId = getBookId();
        EBook ebook = EBookDAO.searchAllEBooksByBookIdModel(bookId);
        crossrefFiles = new ArrayList<EBookCrossrefBean>();

        if (StringUtils.isNotEmpty(displayStatus)) {
            ebookContentList = ebookContentWorker.ebookContents(bookId, getDisplayStatus());
            ebookInsertList = ebookInsertWorker.ebookInserts(bookId, getDisplayStatus());
        } else {
            ebookContentList = ebookContentWorker.ebookContents(bookId);
            ebookInsertList = ebookInsertWorker.ebookInserts(bookId);
        }
        
        // determine if book is Signed Off to allow generating URL
        checkBookStatus(bookId);
        
        //check if book has Proofing Code
        this.proofingUrl = ebook.getProofingCode();
        
        // set remove signed off flag to true when proofing code exists
        removeSignedOff = proofingUrl != null;
        
        // initialize proofing code URL to be displayed in text area
        if (removeSignedOff){
            this.proofingUrl = frontendUrl + getProofingUrl();
        }
        
        hasXsltLog = hasXsltLog(bookId);

        ebookContentWorker.setContentStatus(ebookContentList, removeSignedOff);
        ebookInsertWorker.setInsertStatus(ebookInsertList);
        crossrefFiles = getCrossrefFiles(bookId);

        this.ebookModelType = ebook.getModelType();
        this.thirdPartyFlag = "true";
        
        LogManager.info(getClass(), "Audit: " + bookId + " Status = " + ebook.getStatus());
        
        return initBookContents;
    }
    
    private void checkBookStatus(String bookId){
        String isbn = bookId.substring(3); //trim the prefix from the bookId
        EBook ebook = PersistenceUtil.searchEntity(new EBook(), EBook.SEARCH_EBOOK_BY_ISBN, isbn);
        
        if(ebook != null && ebook.getStatus() == 7){
            setAllowGenerateUrl(false);
        } else if (ebook != null){
            setAllowGenerateUrl(true);
        }
    }

    public String showChapter() {
        return RedirectTo.EBOOK_CHAPTER;
    }

    public void showChapterListener(ActionEvent event) {
        setBookId("");
        setCrossRefXmlFileName("");

        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap();
        String bookId = (String) (event.getComponent().getAttributes().get(ID) != null ? event.getComponent().getAttributes().get(ID): requestMap.get(ID));

        setBookId(bookId);

        HttpUtil.getHttpSession(false).setAttribute("bookId", getBookId());
        setDisplayStatus(null);
    }

    public EBookContentBean getEbookContent() {
        return ebookContentWorker.ebookContent(ebookContentList);
    }

    public String getEisbn() {
        eisbn = IsbnContentDirUtil.bookIdToIsbn(StringUtil.isNotEmpty(bookId) ? bookId: getBookId());
        return eisbn;
    }

    public Map<String, EBookContentBean> getEbookOthers() {
        return ebookOthers;
    }

    public SelectItem[] getStatusOptions() {

        List<EBookContentBean> temp = new ArrayList<EBookContentBean>();

        AccessRights rights = HttpUtil.getUserFromCurrentSession().getAccessRights();
        statusOptions = ebookContentWorker.resolveSelectItems(Status.RESET.getStatus(), rights, Status.ALL_OPTIONS, removeSignedOff);

        temp.addAll(ebookContentList);
        temp.addAll(EBookInsertWorker.wrapToEBookContentBean(ebookInsertList));

        return Status.mergeContentUserStatus(statusOptions, temp);
    }

    public String getChapterStatus() {
        chapterStatus = Status.RESET.getStatus();
        return chapterStatus;
    }

    public void setChapterStatus(String chapterStatus) {
        this.chapterStatus = chapterStatus;
    }

    public EBookBean getEbook() {
        String bookId = getBookId();
        
        removeSignedOff = (proofingUrl != null) ? true : false;
        this.ebookOthers = ebookContentWorker.initEbookOthers(ebookOthers, bookId, removeSignedOff);

        if (ebook != null && ebook.getBookId().equals(bookId)) {
            return ebook;
        } else {
            ebook = ebookContentWorker.searchEBook(bookId);
            return ebook;
        }

    }

    public String updateEbookContents() throws Exception {

        List<EBookContentBean> ebookFullContentList = new ArrayList<EBookContentBean>();
        ebookFullContentList.addAll(ebookContentList);
        for (String type : ebookOthers.keySet()) {
            ebookFullContentList.add(ebookOthers.get(type));
        }

        ebookContentWorker.updateEbookContents(ebookContentList);
        ebookContentWorker.updateEBookOthers(ebookOthers);

        ebookInsertWorker.updateEbookInserts(ebookInsertList);
        ebookFullContentList.addAll(EBookInsertWorker.wrapToEBookContentBean(ebookInsertList));

        ebookContentWorker.updateStatus(StringUtil.isNotEmpty(bookId) ? bookId : getBookId());

        crossRefXmlFileName = ebookWorker.updateEbook(bookId, ebookFullContentList);

        EBookDAO.updateEBookModel(ebookModelType, bookId);

        return EBookContentWorker.UPDATE;
    }

    public String displayEbookContentsByStatus() {
        return EBookContentWorker.DISPLAY_BY_STATUS;
    }

    public List<EBookContentBean> getEbookContentList() {
        return ebookContentList;
    }

    public List<EBookContentBean> getEbookContentList(
            boolean includeChapterContributors) throws SQLException {

        if (StringUtils.isNotEmpty(displayStatus)){
            ebookContentList = ebookContentWorker.ebookContents(getBookId(),getDisplayStatus());
        } else {
            ebookContentList = ebookContentWorker.ebookContents(getBookId());
        }
        removeSignedOff = (proofingUrl != null) ? true : false;
        ebookContentWorker.setContentStatus(ebookContentList, removeSignedOff);

        return ebookContentList;
    }

    /**
     * This method is used for view all chapters in metadata
     * 
     * @throws SQLException
     */
    public List<EbookChapterDetailsBean> getEbookChapterDetails()
            throws SQLException {
        List<EBookContentBean> alContent = getEbookContentList(false);
        List<EbookChapterDetailsBean> alChapterBean = new ArrayList<EbookChapterDetailsBean>();
        for (EBookContentBean content : alContent) {
            EbookChapterDetailsBean chBean = new EbookChapterDetailsBean();
            chBean.setContentId(content.getContentId());
            chBean.setContentType(content.getType());
            chBean.setContentTitle(content.getTitle());
            chBean.show();
            chBean.setDisplayIndented(content.isIndented());
            alChapterBean.add(chBean);
        }

        return alChapterBean;
    }
    
    public void generateUrl(ActionEvent event) throws Exception {
        setProofingUrl(UUID.randomUUID().toString().replaceAll("-", "").toString().substring(0, 8));
        Calendar dateNow = Calendar.getInstance();
        String dateXDaysAhead = DateUtil.dateTimePlusXdays(urlExpireLength, dateNow.get(Calendar.YEAR), dateNow.get(Calendar.MONTH), dateNow.get(Calendar.DAY_OF_MONTH));
        EBookDAO.updateEBookProofingUrl(getProofingUrl(), dateXDaysAhead, bookId);
    }
    
    public void deleteUrl(ActionEvent event) throws Exception {
        setProofingUrl("");
        String dateXDaysAhead = "";
        EBookDAO.updateEBookProofingUrl(getProofingUrl(), dateXDaysAhead, bookId);
        reindexBook(event);
    }
    
    public void reindexBook(ActionEvent event) throws Exception {
        StringBuilder urlString = new StringBuilder();
        urlString.append(solrHomeUrl.replace(":8080", ""));
        urlString.append(reindexString);
        urlString.append(bookId.subSequence(3, bookId.length()));
        
        URL reindexURL = new URL(urlString.toString());
        HttpURLConnection con = (HttpURLConnection) reindexURL.openConnection();
        
        try{
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + reindexURL);
            System.out.println("Response Code : " + responseCode);
        } catch (IOException e) {   
            LogManager.error(getClass(), e.getMessage());
        }
        
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public String getCrossRefXmlFileName() {
        return crossRefXmlFileName;
    }

    public void setCrossRefXmlFileName(String crossRefXmlFileName) {
        this.crossRefXmlFileName = crossRefXmlFileName;
    }

    public List<EBookCrossrefBean> getCrossrefFiles() {
        return crossrefFiles;
    }

    public void setCrossrefFiles(List<EBookCrossrefBean> crossrefFiles) {
        this.crossrefFiles = crossrefFiles;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void setInitBookContents(String initBookContents) {
        this.initBookContents = initBookContents;
    }

    public String getContentDir() {
        return contentDir;
    }

    public List<EBookInsertBean> getEbookInsertList() {
        return ebookInsertList;
    }

    private List<EBookCrossrefBean> getCrossrefFiles(String isbn) {
        List<EBookCrossrefBean> crossrefFiles = new ArrayList<EBookCrossrefBean>();
        String path = IsbnContentDirUtil.getPath(isbn.substring(3));
        try {
            File fileDir = new File(path);

            FilenameFilter filter = new FileFilter(".crossref.xml");

            String s[] = fileDir.list(filter);
            for (String str:s) {
                EBookCrossrefBean crossrefData = new EBookCrossrefBean(path, str);

                crossrefFiles.add(crossrefData);
            }
        } catch (NullPointerException e) {
            LogManager.error(getClass(), e.getMessage() + " " + isbn + " does not have crossref files");
        }

        return crossrefFiles;
    }
    
    private boolean hasXsltLog(String isbn) {
        String path = IsbnContentDirUtil.getPath(isbn.substring(3));
        boolean output = false;
        try {
            File fileDir = new File(path);

            FilenameFilter filter = new FileFilter("xsltTransformLog.html");

            String s[] = fileDir.list(filter);
            
            if(s.length > 0){
                output = true;
            }
        } catch (NullPointerException e) {
            LogManager.error(getClass(), e.getMessage() + " " + isbn + " does not have xsltTransformLog.html");
        }

        return output;
    }

    public String getEbookModelType() {
        return ebookModelType;
    }

    public void setEbookModelType(String ebookModelType) {
        this.ebookModelType = ebookModelType;
    }

    public String isThirdParty() {
        return thirdPartyFlag;
    }

    public void setThirdParty(String isThirdParty) {
        this.thirdPartyFlag = isThirdParty;
    }

    public String getProofingUrl() {
        return proofingUrl;
    }

    public void setProofingUrl(String string) {
        this.proofingUrl = string;
    }

    public boolean isAllowGenerateUrl() {
        return allowGenerateUrl;
    }

    public void setAllowGenerateUrl(boolean allowGenerateUrl) {
        this.allowGenerateUrl = allowGenerateUrl;
    }

    public boolean isHasXsltLog() {
        return hasXsltLog;
    }

    public void setHasXsltLog(boolean hasXsltLog) {
        this.hasXsltLog = hasXsltLog;
    }
}
