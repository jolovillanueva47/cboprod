/*package org.cambridge.ebooks.production.ebook;

import java.util.Comparator;

import org.cambridge.util.Misc;

public class EBookComparator implements Comparator<EBookBean> {
	private String sortBy;
	public EBookComparator(String sortBy) {
		this.sortBy = sortBy;
	}
	public int compare(EBookBean o1, EBookBean o2) {
		int result;
		if (sortBy.equals(EBookManagedBean.SORT_BY_TITLE)) {
			String a1 = Misc.isNotEmpty(o1.getAlphasort()) ? o1.getAlphasort() : "";
			String a2 = Misc.isNotEmpty(o2.getAlphasort()) ? o2.getAlphasort() : ""; 
			result = a1.toLowerCase().compareTo(a2.toLowerCase());
		} else if (sortBy.equals(EBookManagedBean.SORT_BY_ISBN)) {
			result = o1.getIsbn().compareTo(o2.getIsbn());
		} else if (sortBy.equals(EBookManagedBean.SORT_BY_STATUS)) {
			result = o1.getStatusCode().compareTo(o2.getStatusCode());
		} else {
			result = o1.getTitle().compareTo(o2.getTitle());
		}
		return result;
	}
}
*/