/**
 * 
 */
package org.cambridge.ebooks.production.ebook;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.jpa.common.SrEBookIsbn;
import org.cambridge.ebooks.production.jpa.publisher.PublisherIsbn;
import org.cambridge.ebooks.production.jpa.publisher.SrProduct;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;

/**
 * @author rvillamor
 *
 */
public class EBookDAO {
    private static final Log4JLogger LogManager = new Log4JLogger(EBookDAO.class);
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
    public static int getAllUploadedEbooks() {    
        EntityManager em = emf.createEntityManager();
        Query query = null;
        BigDecimal v  = null;
        try {
            query = em.createNamedQuery(EBook.COUNT_SEARCH_ALL);
            v = (BigDecimal)query.getSingleResult();
            return v.intValue(); 
        } finally {
            JPAUtil.close(em);
        }
    }
    
    public static ArrayList<EBook> searchAllEBooks(){
        return PersistenceUtil.searchList(new EBook(), EBook.SEARCH_ALL);
    }
    
    public static ArrayList<EBook> searchAllEBooksByBookId(String bookId) {
        return PersistenceUtil.searchList(new EBook(), EBook.SEARCH_ALL_BY_BOOK_ID, bookId);
    }
    
    public static EBook searchAllEBooksByBookIdModel(String bookId) {
        return PersistenceUtil.searchEntity(new EBook(), EBook.SEARCH_ALL_BY_BOOK_ID_WITH_MODEL, bookId);
    }
    
    public static void updateEBookModel(String model, String bookId) throws Exception {
        PersistenceUtil.updateEntity(EBook.UPDATE_EBOOK_MODEL_BY_BOOK_ID, model, bookId);
    }
    
    public static void updateEBookStatus(String status, String bookId) throws Exception {
        PersistenceUtil.updateEntity(EBook.UPDATE_EBOOK_STATUS_BY_BOOK_ID, status, bookId);
    }
    
    public static void updateEBookDetails(String status, String title, String alphaSort, String bookId) throws Exception {
        PersistenceUtil.updateEntity(EBook.UPDATE_EBOOK_DETAILS_BY_BOOK_ID, status, title, alphaSort, bookId);
    }
    
    public static void updateEBookProofingUrl(String proofingUrl, String dateXDaysAhead, String bookId) throws Exception {
        PersistenceUtil.updateEntity(EBook.UPDATE_EBOOK_PROOFING_CODE, proofingUrl, dateXDaysAhead, bookId);
    }
    
    public static List<EBook> getAccessibleEbooks(String userId, String[] statusCodes, String query){
        ArrayList<String> alParams = new ArrayList<String>(Arrays.asList(statusCodes));
        alParams.add(0, userId);
        return PersistenceUtil.dynamicSearchList(new EBook(), query, "searchEBookResult", alParams);    
    }
    
    public static void setEBookStatus(final String bookId, int status){
        EntityManager em = emf.createEntityManager();
        EBook result = null;
        try  { 
            em.getTransaction().begin();
            result = em.find(EBook.class, bookId);
            result.setStatus(status);
            em.getTransaction().commit();
            LogManager.info(EBookDAO.class, "Audit: "+bookId + "New EBookStatus: " + status ); //60801 added lgging -jubs
        } 
        catch (Exception e) { 
            LogManager.error(EBookDAO.class, " setEBookStatus(): " + e.getMessage() );
        } 
        finally {
            JPAUtil.close(em);
        }
    }
    
    public static String getPublisherIdByBookId(String bookId) {        
        EntityManager em = emf.createEntityManager();
        Query query = null;
        String v = null;
        try {
            query = em.createNamedQuery(PublisherIsbn.SEARCH_PUBLISHERID_BY_BOOKID);
            query.setParameter(1, bookId);
            v  = (String)query.getSingleResult();
            return v;
        }
        finally {
            JPAUtil.close(em);
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map statusSummary(String publisherId){
        Map<Integer, Integer> summary = new LinkedHashMap<Integer, Integer>();
         
        EntityManager em = emf.createEntityManager();
        Query query = null;
        
        try {
            query = em.createNamedQuery(publisherId.equals("ALL")?EBook.EBOOK_STATUS_SUMMARY_ALL:EBook.EBOOK_STATUS_SUMMARY);
            query.setParameter(1, publisherId);
            
            List<Object[]> list = (List<Object[]>)query.getResultList();
            for(Object[] result : list) {
                summary.put(stringToStatus(objectToString(result[0]), -1), stringToStatus(objectToString(result[1]), 1));
            }
            
            return summary;
        }
        finally {
            JPAUtil.close(em);
        }
    }

        
    @SuppressWarnings("unchecked")
    public static List<EBookBean> summaryReport(String publisherId){
        List<EBookBean> summaryReport = new ArrayList<EBookBean>();
         
        EntityManager em = emf.createEntityManager();
        Query query = null;
        try {
            query = em.createNamedQuery(publisherId.equals("ALL")?EBook.EBOOK_REPORT_ALL:EBook.EBOOK_REPORT);
            query.setParameter(1, publisherId);
            
            List<Object[]> list = (List<Object[]>)query.getResultList();
            for(Object[] result : list) {
                EBookBean ebookBean = new EBookBean();
                ebookBean.setBookId(objectToString(result[0]));
                ebookBean.setIsbn(objectToString(result[1]));
                
                ebookBean.setTitle(objectToString(result[2]));
                ebookBean.setAlphasort(objectToString(result[3]));
                ebookBean.setStatusDescription(Status.getStatus(stringToStatus(objectToString(result[4]), -1)).getDescription());
                
                ebookBean.setPubPrintDate(objectToString(result[5]));  
                ebookBean.setPubOnlineDate(objectToString(result[6]));
                ebookBean.setLoadedOnlineDate(objectToString(result[7]));
                
                if(! (result[8] == null || objectToString(result[8]).equals(null))){
                    ebookBean.setPublisher(objectToString(result[8]));
                    ebookBean.setProduct(objectToString(result[9]));
                } else {
                    SrEBookIsbn srEbookIsbn = PersistenceUtil.searchEntity(new SrEBookIsbn(), SrEBookIsbn.GET_PRODUCT_CODE, ebookBean.getIsbn());
                    ebookBean.setProduct(srEbookIsbn.getProductGroup());
                    SrProduct srProduct = PersistenceUtil.searchEntity(new SrProduct(), SrProduct.GET_PUBLISHER, srEbookIsbn.getProductGroup());
                    ebookBean.setPublisher(srProduct.getPublisher_code());
                }
                summaryReport.add(ebookBean);
            }
            return summaryReport;
        }
        finally {
            JPAUtil.close(em);
        }
    }
    
    public static int summaryReportSize(String publisherId){            
        EntityManager em = emf.createEntityManager();
        Query query = null;
        try {
            query = em.createNamedQuery(publisherId.equals("ALL")?EBook.EBOOK_REPORT_COUNT_ALL:EBook.EBOOK_REPORT_COUNT);
            query.setParameter(1, publisherId);
            
            BigDecimal v = (BigDecimal) query.getSingleResult();
            return  v.intValue();
        } finally {
            JPAUtil.close(em);
        }
    }
    
    private static String objectToString(Object obj){
        String result = "";
        result = (obj == null ? "" : obj.toString());
        
        return result;
    }
    
    private static int stringToStatus(String status, int stat){
        try{
            return Integer.parseInt(status);
        } catch(Exception ex){
            return stat;
        }
    }
}
