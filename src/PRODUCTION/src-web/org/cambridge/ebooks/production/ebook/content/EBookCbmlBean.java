package org.cambridge.ebooks.production.ebook.content;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.cambridge.ebooks.production.util.FileCbmlFilter;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;

/**
 * 
 * @author jmendiola
 * 
 * The purpose of this Bean is to process the list of files to be displayed in the HTML pop up for the ebook_chapter.jsp page
 */


public class EBookCbmlBean {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookCbmlBean.class);
	private static final String CONTENT_FOLDER = System.getProperty("content.dir");
	List<String> htmlList;
	private String isbn;
	private String initPage;
	private String filename;
	private String css;
	
	public EBookCbmlBean(){
		this.isbn = "";
		this.filename = "";
		this.css = "";
		getInitPage();
	}
	
	public String getInitPage(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		this.isbn = request.getParameter("isbn");
		this.filename = request.getParameter("filename");
		if(filename.contains("drg")){
			this.css = "@import url(css/users_stahlchunks.css);";
		} else {
			this.css = "@import url(css/users_contentchunks.css);";
		}
		return initPage;
	}
	
	private List<String> getFilenames(String isbn, String filename){
		
		String path = IsbnContentDirUtil.getPath(isbn) + "html_chunk/";
		
		
		File fileDir = new File(path);
		List<String> fileList = new ArrayList<String>();
		FilenameFilter filenameFilter = new FileCbmlFilter(filename);
		String s[] = fileDir.list(filenameFilter);
		String url;
		try{
			
		for (int i=0; i < s.length; i++) {
			url = (path + s[i]).replace(CONTENT_FOLDER, "");
			fileList.add(url);
			System.out.println(url);
			LogManager.info(getClass(), "url: "+url);
		} 
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return fileList;
	}

	public List<String> getHtmlList() {
		getInitPage();
		return getFilenames(isbn, filename);
	}

	public void setHtmlList(List<String> htmlList) {
		this.htmlList = htmlList;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

		

}
