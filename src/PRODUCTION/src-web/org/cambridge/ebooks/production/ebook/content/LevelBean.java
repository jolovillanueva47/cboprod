package org.cambridge.ebooks.production.ebook.content;

public class LevelBean {

	private String parent;
	private int level;
	private String value;
	
	public LevelBean (String parent, int level, String value){
		this.parent = parent;
		this.level = level;
		this.value = value;
	}
	
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
