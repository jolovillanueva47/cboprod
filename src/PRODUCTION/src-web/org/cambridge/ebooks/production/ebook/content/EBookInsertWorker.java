package org.cambridge.ebooks.production.ebook.content;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.jpa.user.AccessRights;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.util.EBookSearchWorker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.util.ListUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.StringUtil;

public class EBookInsertWorker {
	
	private static final Log4JLogger LogManager = new Log4JLogger(EBookInsertWorker.class);
	
	private final EBookSearchWorker worker = new EBookSearchWorker();
	
	private List<Integer> status = new ArrayList<Integer>();
	
	private final static String BOOK_ID = "bookId";
	
	
	public List<EBookInsertBean> ebookInserts(String bookId) throws SQLException{	
		
		System.out.println(EBookInsertWorker.class + "---BOOK ID:" + bookId);
		
		List<EBookContent> ebookContentFromDB = null;
		List<EBookInsertBean> ebookInsertsFromIndex = null;
		
		ebookInsertsFromIndex = getInsertsFromIndex(bookId);
		ebookContentFromDB = EBookContentDAO.searchContentsByBookId(bookId);

		setInsertProperties(ebookContentFromDB, ebookInsertsFromIndex);
		
		return ebookInsertsFromIndex;
	}
	
	public List<EBookInsertBean> ebookInserts(String bookId, String status){ 		
		
		List<EBookContent> ebookContentFromDB = null;
		List<EBookInsertBean> ebookInsertsFromIndex = null;
		
		ebookInsertsFromIndex = getInsertsFromIndex(bookId); 
		ebookContentFromDB = EBookContentDAO.searchContentsByBookIdAndStatus(bookId, status);		
		
		//LogManager.info("ebookinserts... before setInsertProperties()...");
		
		setInsertProperties(ebookContentFromDB, ebookInsertsFromIndex);
		
		if(ebookContentFromDB.size() == 0)
			return null;
		else
			return ebookInsertsFromIndex;
	}
	
	
	private List<EBookInsertBean> getInsertsFromIndex(String bookId){

		try {
			
			BookMetadataDocument bookMetadata = worker.searchBookCore("book_id:" + bookId).get(0);
			return populateInsertBean(bookMetadata);
			
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
		
	}
	
	
	private List<EBookInsertBean> populateInsertBean(BookMetadataDocument insertItem){
		
		List<EBookInsertBean> result = new ArrayList<EBookInsertBean>();
		
		if (ListUtil.isNotNullAndEmpty(insertItem.getInsertIds())) {
			
			for(int i = 0; i < insertItem.getInsertIds().size(); i++) {
				
				EBookInsertBean insertBean = new EBookInsertBean();
				insertBean.setContentId(insertItem.getInsertIds().get(i));
				insertBean.setInsertTitle(insertItem.getInsertTitles().get(i));
				insertBean.setInsertAlphasortTitle(insertItem.getInsertTitlesAlphasort().get(i));
				insertBean.setInsertFilename(insertItem.getInsertFilenames().get(i));
				
				result.add(insertBean);
				
			}
		
		}
		
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	private void setInsertProperties(List ebookContentList, List ebookInsertBeanList) {
		
		try {
			
			if(ListUtil.isNotNullAndEmpty(ebookContentList) && ListUtil.isNotNullAndEmpty(ebookInsertBeanList)) {
				
				ebookInsertBeanList.retainAll(ebookContentList);
				if (ListUtil.isNotNullAndEmpty(ebookInsertBeanList)) {
					
					for (Object ebookInsertBean : ebookInsertBeanList) {
						Object ebookContent = ListUtil.searchObject(ebookContentList, ebookInsertBean);
						if (ebookContent != null) {
							EBookContent ebookContent_ = (EBookContent)ebookContent;
							EBookInsertBean ebookInsertBean_ = (EBookInsertBean)ebookInsertBean;
							ebookInsertBean_.setEbookContent(ebookContent_);
						}
					}
					
				}
			
			}
		
		} catch (Exception e) {		
			LogManager.error(EBookInsertWorker.class, " setContentProperties(List, List): " + e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	
	public void setInsertStatus(final List<EBookInsertBean> insertList){
		if(ListUtil.isNotNullAndEmpty(insertList))
		{
			AccessRights rights = HttpUtil.getUserFromCurrentSession().getAccessRights();
			for(EBookInsertBean ebookInsertBean : insertList)
			{				
				Status status = Status.getStatus(Integer.valueOf(ebookInsertBean.getEbookContent().getStatus()));
				ebookInsertBean.setOptions(resolveSelectItems(String.valueOf(ebookInsertBean.getEbookContent().getStatus()), rights, Status.getOptions(status, rights.getCanRevertStatus())));
			}
		}
	}
	
	
	// can be reused
	public SelectItem[] resolveSelectItems(String currentStatus, AccessRights rights, LinkedHashMap<String, String> status) {
		/*SelectItem[] options = new SelectItem[status.size()];
		int i=0;
		for (String desc : status.keySet()) {
			String statusCode = status.get(desc);
			boolean isChangeStatusAllowed = rights.isChangeStatusAllowed(Status.getStatus(Integer.valueOf(statusCode)));
			options[i++] = new SelectItem(statusCode, desc, desc , statusCode.equals(currentStatus) ? false : !isChangeStatusAllowed);
		}*///Disabling won't work in IE 7 and below.
		ArrayList<SelectItem> options = new ArrayList<SelectItem>();
		for (String desc : status.keySet()) {
			String statusCode = status.get(desc);
			boolean isChangeStatusAllowed = rights.getCanRevertStatus() || rights.isChangeStatusAllowed(Status.getStatus(Integer.valueOf(statusCode)));
			if (isChangeStatusAllowed || statusCode.equals(currentStatus))
				options.add(new SelectItem(statusCode, desc, desc, false));
		}
		return options.toArray(new SelectItem[]{});
	}
	
	
	public void updateEbookInserts(List<EBookInsertBean> ebookInsertBeanList) {
		
		if(ListUtil.isNotNullAndEmpty(ebookInsertBeanList))
		{	
			if(isEbookInsertsValid(ebookInsertBeanList))
				for(EBookInsertBean ebookInsertBean : ebookInsertBeanList)
				{
					if(ebookInsertBean.isUpdate())
					{
						EBookContentDAO.updateContentEntity(ebookInsertBean.getEbookContent());
						ebookInsertBean.setUpdate(false);
					}
					
					status.add(ebookInsertBean.getEbookContent().getStatus());
				}
		}
			
	}
	
	
	// can be reused
	private boolean isEbookInsertsValid(Collection<EBookInsertBean> ebookInsertBeanList){
		boolean result = true;
		
		try
		{
			for(EBookInsertBean bean : ebookInsertBeanList)
			{
				int status = bean.getEbookContent().getStatus();
				if(status == Integer.valueOf(Status.EMBARGO.getStatus()))
				{
					Date embargoDate = bean.getEbookContent().getEmbargoDate();
					if(embargoDate == null || "".equals(embargoDate))
					{
						result = false;
						
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage message = new FacesMessage("Embargo dates must not be empty.");
						message.setSeverity(FacesMessage.SEVERITY_INFO);
						context.addMessage("alertMessage", message);
						
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			System.err.println(EBookInsertWorker.class +  " isEbookContentsValid() " + e.getMessage());
			result = false;
		}
	
		return result;
	}
	
	
	public static List<EBookContentBean> wrapToEBookContentBean(List<EBookInsertBean> ebookInsertList) {
		
		List<EBookContentBean> result = new ArrayList<EBookContentBean>();
		
		if (ebookInsertList != null && !ebookInsertList.isEmpty()) 
		{
			
			for(EBookInsertBean insertBean : ebookInsertList) {
				
				EBookContentBean contentBean = new EBookContentBean(insertBean);
				contentBean.setTitle(StringUtil.formatTitle(contentBean.getTitle())); //ampersand is escaped in db, this is a crude fix -jubs
				result.add(contentBean);
				
			}
			
		}
		
		return result;
		
	}
	
	
	// can be reused
	public String createIsbnBreakdown(String filename) {
		
		String bookId = (String) HttpUtil.getAttributeFromCurrentSession(BOOK_ID);
		String isbn = IsbnContentDirUtil.bookIdToIsbn(bookId);
		
		return IsbnContentDirUtil.getFilePathIsbnBreakdown(isbn, filename);
		
	}

}