package org.cambridge.ebooks.production.ebook;

public interface EBookComponent {
	String getBookId();
}
