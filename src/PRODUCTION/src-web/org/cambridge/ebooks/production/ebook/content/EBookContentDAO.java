/**
 * 
 */
package org.cambridge.ebooks.production.ebook.content;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.content.AuditTrail;
import org.cambridge.ebooks.production.jpa.common.EBookContent;
import org.cambridge.ebooks.production.jpa.user.User;
import org.cambridge.ebooks.production.util.EventTracker;
import org.cambridge.ebooks.production.util.HttpUtil;
import org.cambridge.ebooks.production.util.JPAUtil;
import org.cambridge.ebooks.production.util.ListUtil;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.ebooks.production.util.PersistenceUtil;
import org.cambridge.ebooks.production.util.PersistenceUtil.PersistentUnits;
/**
 * @author rvillamor
 *
 */
public class EBookContentDAO {
	private static final Log4JLogger LogManager = new Log4JLogger(EBookContentDAO.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static List<EBookContent> searchChaptersByBookId(String bookId){
		return PersistenceUtil.searchList(new EBookContent(), EBookContent.SEARCH_CHAPTERS_BY_BOOK_ID, bookId);
	}
	
	public static List<EBookContent> searchContentsByBookId(String bookId){
		return PersistenceUtil.searchList(new EBookContent(), EBookContent.SEARCH_BY_BOOK_ID, bookId);
	}
	
	public static EBookContent searchContentByContentIdAndType(String contentId, String type) {
		return PersistenceUtil.searchEntity(new EBookContent(), EBookContent.SEARCH_BY_CONTENT_ID_AND_TYPE, contentId, type);
	}
	
	public static List<EBookContent> searchContentsByBookIdAndStatus(String bookId, String status){
		return PersistenceUtil.searchList(new EBookContent(), EBookContent.SEARCH_BY_BOOK_ID_AND_STATUS, bookId, status);
	}
	
	public static List<EBookContent> searchOtherContentsByBookId(String bookId){
		return PersistenceUtil.searchList(new EBookContent(), EBookContent.SEARCH_OTHER_CONTENT_BY_BOOK_ID, bookId);
	}
	
	public static List<EBookContent> searchEBookHeaderAndImagesByBookId(String bookId){
		return PersistenceUtil.searchList(new EBookContent(), EBookContent.SEARCH_HEADER_AND_IMAGES_BY_BOOK_ID, bookId);
	}	
	
	public static void updateEbookOthers(Map<String, EBookContentBean> ebookOthers) {
		EntityManager em = emf.createEntityManager();
		
		try {
			for (String type : ebookOthers.keySet()) {
				EBookContentBean toUpdate = ebookOthers.get(type);
				if (toUpdate.isUpdate()) 
				{
					em.getTransaction().begin();
					
					EBookContent ebo = em.find(EBookContent.class, toUpdate.getEbookContent().getFileName());
					ebo.setStatus(toUpdate.getEbookContent().getStatus());
					ebo.setEmbargoDate(toUpdate.getEbookContent().getStatus() != Integer.valueOf(Status.EMBARGO.getStatus()) ? null : toUpdate.getEbookContent().getEmbargoDate());
					//ebo.setEmbargoDate(toUpdate.getEbookContent().getEmbargoDate());
					ebo.setRemarks(toUpdate.getEbookContent().getRemarks());
					ebo.setModifiedBy(HttpUtil.getUserFromCurrentSession().getUsername());
					ebo.setModifiedDate(new Date());
					
					em.merge(ebo);
					em.getTransaction().commit();
					
					//create audit trail
					User user = HttpUtil.getUserFromCurrentSession();
					EventTracker.sendBookEvent(ebo.getContentId(), ebo.getIsbn(), 
							ebo.getBook().getSeriesId(), String.valueOf(ebo.getStatus()),
							ebo.getRemarks(), user.getUsername(), 
							toUpdate.getEbookContent().getFileName(), toUpdate.getEbookContent().getType());
					
					if(ebo.getStatus() == 2 || ebo.getStatus() == 3){
		    			EventTracker.sendEvent( AuditTrail.PROOF_CONTENTS, user.getUserId(), toUpdate.getEbookContent().getType() + " - " + ebo.getIsbn());
		    		}
		    		else if(ebo.getStatus() == 4 || ebo.getStatus() == 5) {
		    			EventTracker.sendEvent( AuditTrail.APPROVE_CONTENTS, user.getUserId(), toUpdate.getEbookContent().getType() + " - " + ebo.getIsbn());
		    		}
		    		else if(ebo.getStatus() == 7 || ebo.getStatus() == 8 || ebo.getStatus() == 9) {
		    			EventTracker.sendEvent( AuditTrail.MAKE_FILE_AVAILABLE_FOR_USERS, user.getUserId(), toUpdate.getEbookContent().getType() + " - " + ebo.getIsbn()+" " + Status.getStatus(ebo.getStatus()));
		    		}
		    		else if(ebo.getStatus() == 100 ){
		    			EventTracker.sendEvent( AuditTrail.DELETE_CONTENTS, user.getUserId(), toUpdate.getEbookContent().getType() + " - " + ebo.getIsbn()+" " + Status.getStatus(ebo.getStatus()));
		    		}
					
					toUpdate.setUpdate(false);
				}
				
			}
			
		} catch (Exception e) {
			LogManager.error(EBookContentDAO.class, "Error updating EbookOthers: " + e.getMessage());
		} finally {
			JPAUtil.close(em);
		}
	}
	
	public static void updateOtherContents(List<EBookContentBean> ebookOtherContentsList) {
		EntityManager em = emf.createEntityManager();
		
		try {
			
				if(ListUtil.isNotNullAndEmpty(ebookOtherContentsList))
				{
					for(int index=0; index<ebookOtherContentsList.size(); index++)
					{
						EBookContentBean ebookOthersBean = ebookOtherContentsList.get(index);
						EBookContent ebookContent = ebookOthersBean.getEbookContent();
						if (ebookOthersBean.isUpdate()) {
							
							em.getTransaction().begin();
							
							EBookContent ebo = em.find(EBookContent.class, ebookContent.getFileName());
							ebo.setStatus(ebookContent.getStatus());
							ebo.setEmbargoDate(ebookContent.getEmbargoDate());
							ebo.setRemarks(ebookContent.getRemarks());
							ebo.setModifiedBy(HttpUtil.getUserFromCurrentSession().getUsername());
							ebo.setModifiedDate(new Date());
							
							em.merge(ebo);
							em.getTransaction().commit();
							
							ebookOthersBean.setUpdate(false);
							
							//create audit trail
							User user = HttpUtil.getUserFromCurrentSession();
							EventTracker.sendBookEvent(ebo.getContentId(), ebo.getIsbn(), 
									ebo.getBook().getSeriesId(), String.valueOf(ebo.getStatus()),
									ebo.getRemarks(), user.getUsername(), 
									ebookContent.getFileName(), ebookContent.getType());
							
							if(ebo.getStatus() == 2 || ebo.getStatus() == 3){
				    			EventTracker.sendEvent( AuditTrail.PROOF_CONTENTS, user.getUserId(), ebookContent.getType() + " - " + ebo.getContentId());
				    		}
				    		else if(ebo.getStatus() == 4 || ebo.getStatus() == 5) {
				    			EventTracker.sendEvent( AuditTrail.APPROVE_CONTENTS, user.getUserId(), ebookContent.getType() + " - " + ebo.getContentId());
				    		}
				    		else if(ebo.getStatus() == 7 || ebo.getStatus() == 8 || ebo.getStatus() == 9) {
				    			EventTracker.sendEvent( AuditTrail.MAKE_FILE_AVAILABLE_FOR_USERS, user.getUserId(), ebookContent.getType() + " - " + ebo.getContentId()+" " + Status.getStatus(ebo.getStatus()));
				    		}
				    		else if(ebo.getStatus() == 100 ){
				    			EventTracker.sendEvent( AuditTrail.DELETE_CONTENTS, user.getUserId(), ebookContent.getType() + " - " + ebo.getContentId()+" " + Status.getStatus(ebo.getStatus()));
				    		}
						}
					}
				}
				
		} catch (Exception e) {
			LogManager.error(EBookContentDAO.class, "Error updating EbookOthers: " + e.getMessage());
		} finally {
			JPAUtil.close(em);
		}
	}
	
	public static void updateContentEntity(EBookContent ebookContent) { 
		
		EntityManager em = emf.createEntityManager();
		
        try { 
        	em.getTransaction().begin();
        	EBookContent result = em.find(EBookContent.class, ebookContent.getContentId());
        		
        	result.setStatus(ebookContent.getStatus());
        	result.setEmbargoDate(ebookContent.getStatus() != Integer.valueOf(Status.EMBARGO.getStatus()) ? null : ebookContent.getEmbargoDate());
        	result.setRemarks(ebookContent.getRemarks());
        	result.setModifiedDate(new Date());
        	result.setModifiedBy(HttpUtil.getUserFromCurrentSession().getUsername());
        	em.merge(result);
        	em.getTransaction().commit();
        	
        	//create audit trail
        	User user = HttpUtil.getUserFromCurrentSession();
        	EventTracker.sendBookEvent(result.getContentId(), result.getIsbn(),
        							   result.getBook().getSeriesId(), String.valueOf(ebookContent.getStatus()),
        							   ebookContent.getRemarks(), user.getUsername(), result.getFileName(), result.getType());
    
    		if(ebookContent.getStatus() == 2 || ebookContent.getStatus() == 3){
    			EventTracker.sendEvent( AuditTrail.PROOF_CONTENTS, user.getUserId(), ebookContent.getContentId());
    		}
    		else if(ebookContent.getStatus() == 4 || ebookContent.getStatus() == 5) {
    			EventTracker.sendEvent( AuditTrail.APPROVE_CONTENTS, user.getUserId(), ebookContent.getContentId());
    		}
    		else if(ebookContent.getStatus() == 7 || ebookContent.getStatus() == 8 || ebookContent.getStatus() == 9) {
    			EventTracker.sendEvent( AuditTrail.MAKE_FILE_AVAILABLE_FOR_USERS, user.getUserId(), ebookContent.getContentId()+" " + Status.getStatus(ebookContent.getStatus()));
    		}
    		else if(ebookContent.getStatus() == 100 ){
    			EventTracker.sendEvent( AuditTrail.DELETE_CONTENTS, user.getUserId(), ebookContent.getContentId()+" " + Status.getStatus(ebookContent.getStatus()));
    		}
        } catch (Exception e) { 
        	LogManager.error(EntityManagerFactory.class, " updateChapterEntity " + e.getMessage() );
        } finally {
        	JPAUtil.close(em);
        }
		
	}
}
