/*
 * Created on Jan 07, 2004
 *
 */
package org.cambridge.ebooks.production.exception;

/**
 * @author jcg
 *
 */
public class InvalidDataException extends LoadException {

	/**
	 * 
	 */
	public InvalidDataException() {
		super();
	}

	/**
	 * @param message
	 */
	public InvalidDataException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public InvalidDataException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
	}

}
