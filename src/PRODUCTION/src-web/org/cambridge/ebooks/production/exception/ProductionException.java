/*
 * Created on Jan 07, 2004
 *
 */
package org.cambridge.ebooks.production.exception;

/**
 * @author jcg
 *
 */
public class ProductionException extends Exception {

	/**
	 * 
	 */
	public ProductionException() {
		super();
	}

	/**
	 * @param message
	 */
	public ProductionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ProductionException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ProductionException(String message, Throwable cause) {
		super(message, cause);
	}

}
