/*
 * Created on Jan 07, 2004
 *
 */
package org.cambridge.ebooks.production.exception;

/**
 * @author jcg
 *
 */
public class LoadException extends ProductionException {

	/**
	 * 
	 */
	public LoadException() {
		super();
	}

	/**
	 * @param message
	 */
	public LoadException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public LoadException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public LoadException(String message, Throwable cause) {
		super(message, cause);
	}

}
