package org.cambridge.ebooks.production.solr.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cambridge.ebooks.production.ebook.EBookBean;
import org.cambridge.ebooks.production.jpa.user.EBookAccess;
import org.cambridge.ebooks.production.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.solr.bean.WordsContentItemDocument;
import org.cambridge.ebooks.production.util.ListUtil;
import org.cambridge.ebooks.production.util.StringUtil;

/**
 * 
 * @author kmulingtapang
 * 
 * REVISIONS:
 * 20130624 jmendiola - added searchKeywordCore
 * 20131205 jmendiola - removed searchKeywordCore because stahl 2 content didn't push through
 * 20131206 alacerna -  added  searchWordCore for eDictionaries 
 *
 */

public class EBookSearchWorker {
	
	private static final String EBOOKS_QUERY = "(content_type:book)";
	private static final SOLRSearchWorker solrWorker = new SOLRSearchWorker();
	
	public List<EBookBean> ebookTitles() {
		return ebookTitles(EBOOKS_QUERY);
	}
	  
	public List<EBookBean> ebookTitles(List<EBookAccess> accessibleEbooks) {
		List<EBookBean> result;
		StringBuilder filter = new StringBuilder();
		if (accessibleEbooks != null && accessibleEbooks.size() > 0) 
		{
			
			boolean allEbooksAccess = accessibleEbooks != null 
				&& accessibleEbooks.size() == 1 
				&& accessibleEbooks.get(0).equals(new EBookAccess(EBookAccess.ALL_EBOOKS)); 
			
			boolean allOrgEbooksAccess = accessibleEbooks != null 
			&& accessibleEbooks.size() == 1 
			&& accessibleEbooks.get(0).equals(new EBookAccess(EBookAccess.ALL_PUBLISHER_EBOOKS)); 
			
			if (allEbooksAccess) 
			{
				result = new ArrayList<EBookBean>();
				result.add(new EBookBean(EBookAccess.ALL_EBOOKS));
			} 
			else if (allOrgEbooksAccess) 
			{
				result = new ArrayList<EBookBean>();
				result.add(new EBookBean(EBookAccess.ALL_PUBLISHER_EBOOKS));
			} 
			else 
			{
				filter.append(" AND (");

				for (Iterator<EBookAccess> it = accessibleEbooks.iterator(); it.hasNext();) 
				{
					EBookAccess eba = it.next();
					filter.append("book_id:" + eba.getBookId());
					if (it.hasNext()) 
					{
						filter.append(" OR ");
					}
				}

				filter.append(")");
				result = ebookTitles(EBOOKS_QUERY + filter.toString());
			}
		} 
		else 
		{
			result = new ArrayList<EBookBean>();
		}
		return result;
	}
	
	public List<EBookBean> ebookTitles(String queryStr){
		  List<EBookBean> result = new ArrayList<EBookBean>();
		  List<BookMetadataDocument> ebookListFromIndex =  searchBookCore(queryStr);		 
		  
		  try
		  {
			  if(ListUtil.isNotNullAndEmpty(ebookListFromIndex))
			  {
				 for(BookMetadataDocument doc : ebookListFromIndex)
				 {
					EBookBean ebook = new EBookBean();
					if(StringUtil.isNotEmpty(doc.getBookId()))
						ebook.setBookId(doc.getBookId());
				
					if(StringUtil.isNotEmpty(doc.getTitle()))
						ebook.setTitle(doc.getTitle());
					
					if(StringUtil.isNotEmpty(doc.getIsbn()))
						ebook.setIsbn(doc.getIsbn());
					
					if(StringUtil.isNotEmpty(doc.getTitleAlphasort()))
						ebook.setAlphasort(doc.getTitleAlphasort());
										
				
					result.add(ebook);
	
				 }
			  }
		  }
		  catch (Exception e) 
		  {
			// TODO Auto-generated catch block
			  System.err.println(EBookSearchWorker.class + " ebookTitles(String queryStr) [Exception]: " + e.getMessage());
		  }
		  
		  return result;
	}
	
	
	public ArrayList<EBookBean> ebookTitlesByEisbn(String[] eisbnList) {
		ArrayList<EBookBean> result = new ArrayList<EBookBean>();
		StringBuilder searchString = new StringBuilder();
		int listSize = eisbnList == null ? 0 : eisbnList.length;

		if (listSize > 0) 
		{
			String isbnSearch = "isbn:";
			for (int c = 0; c < listSize; c++) 
			{
				searchString.append(isbnSearch.concat(eisbnList[c]));
				if (c < listSize - 1) 
					searchString.append(" OR ");
			}

			try 
			{
				List<BookMetadataDocument> docs = solrWorker.searchCore(new BookMetadataDocument(), searchString.toString(), SOLRServer.getInstance().getBookCore());	

				for (BookMetadataDocument doc : docs) 
				{
					EBookBean ebb = new EBookBean();
					ebb.setBookId(doc.getBookId());
					ebb.setIsbn(doc.getIsbn());
					ebb.setTitle(doc.getTitle());
					result.add(ebb);
				}

			} catch (Exception e) {
				 System.err.println(EBookSearchWorker.class + " ebookTitlesByEisbn(String[]) [Exception]: " + e.getMessage());
			}
		}

		return result;
	}
	
	
	public List<BookMetadataDocument> searchBookCore(String queryStr){
		 return solrWorker.searchCore(new BookMetadataDocument(), queryStr, SOLRServer.getInstance().getBookCore());	
	}
	
	public List<BookContentItemDocument> searchContentCore(String queryStr){
		 return solrWorker.searchCore(new BookContentItemDocument(), queryStr, SOLRServer.getInstance().getContentCore());	
	}
	
	public BookContentItemDocument searchContentCoreSinglePager(String queryStr, int page){
		return solrWorker.searchContentCoreSinglePager(new BookContentItemDocument(), queryStr, SOLRServer.getInstance().getContentCore(), page);	
	}
	public List<WordsContentItemDocument> searchWordCore(String queryStr){
		 return solrWorker.searchCoreDictionary(new WordsContentItemDocument(), queryStr, SOLRServer.getInstance().getWordCore());	
	}

	//20131205 removed because stahl 2 content didn't push through
//	public List<KeywordsContentItemDocument> searchKeywordCore(String queryStr){
//		 return solrWorker.searchCore(new KeywordsContentItemDocument(), queryStr, SOLRServer.getInstance().getKeywordCore());	
//	}

}
