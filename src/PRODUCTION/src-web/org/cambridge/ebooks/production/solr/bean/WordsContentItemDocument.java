package org.cambridge.ebooks.production.solr.bean;

import org.apache.solr.client.solrj.beans.Field;

/**
 * @author alacerna
 * 
 * 20131207 - added bean for WordCore
 * 
 */

public class WordsContentItemDocument {

	@Field("id")
	private String id;
	
	@Field("book_id")
	private String bookId;
	
	@Field("content_id")
	private String contentId;

	@Field("word_text")
	private String wordText;
	
	@Field("word_url")
	private String wordUrl;
	
	@Field("type")
	private String type;
	
	@Field("toc_id")
	private String tocId;
	
	
	private String isbnBreakdown;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getWordText() {
		return wordText;
	}

	public void setWordText(String wordText) {
		this.wordText = wordText;
	}

	public String getWordUrl() {
		return wordUrl;
	}

	public void setWordUrl(String wordUrl) {
		this.wordUrl = wordUrl;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTocId() {
		return tocId;
	}

	public void setTocId(String tocId) {
		this.tocId = tocId;
	}

	public String getIsbnBreakdown() {
		return isbnBreakdown;
	}

	public void setIsbnBreakdown(String isbnBreakdown) {
		this.isbnBreakdown = isbnBreakdown;
	}

}
