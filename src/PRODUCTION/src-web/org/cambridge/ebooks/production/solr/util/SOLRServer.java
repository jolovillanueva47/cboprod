package org.cambridge.ebooks.production.solr.util;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;

/**
 * 
 * @author kmulingtapang
 * 
 * REVISIONS:
 * 20130624 jmendiola - added searchKeywordCore
 * 20131206 alacerna  - added searchWordCore
 *
 */

public class SOLRServer {
	
	private SolrServer bookCore;
	private SolrServer contentCore;
	private SolrServer wordCore;
	
	private static final SOLRServer SOLRSERVER = new SOLRServer();
	
	public static final String KEY_SOCKET_TIMEOUT = "KEY_SOCKET_TIMEOUT";
	public static final String KEY_CONN_TIMEOUT = "KEY_CONN_TIMEOUT";
	public static final String KEY_DEF_MAX_CONN_PER_HOST = "KEY_DEF_MAX_CONN_PER_HOST";
	public static final String KEY_MAX_TOTAL_CONN = "KEY_MAX_TOTAL_CONN";
	public static final String KEY_FOLLOW_REDIRECTS = "KEY_FOLLOW_REDIRECTS";
	public static final String KEY_ALLOW_COMPRESSION = "KEY_ALLOW_COMPRESSION";
	public static final String KEY_MAX_RETRIES = "KEY_MAX_RETRIES";
	
	private SOLRServer(){
		try 
		{
			Map<String, String> settings = new HashMap<String, String>();
			settings.put(SOLRServer.KEY_SOCKET_TIMEOUT, System.getProperty("socket.timeout"));
			settings.put(SOLRServer.KEY_CONN_TIMEOUT, System.getProperty("connection.timeout"));
			settings.put(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST, System.getProperty("default.max.connection.per.host"));
			settings.put(SOLRServer.KEY_MAX_TOTAL_CONN, System.getProperty("max.total.connections"));
			settings.put(SOLRServer.KEY_FOLLOW_REDIRECTS, System.getProperty("follow.redirects"));
			settings.put(SOLRServer.KEY_ALLOW_COMPRESSION, System.getProperty("allow.compression"));
			settings.put(SOLRServer.KEY_MAX_RETRIES, System.getProperty("max.retries"));
			
			bookCore = initServer(new CommonsHttpSolrServer(System.getProperty("solr.home.url") + System.getProperty("core.book")), settings);;	
			contentCore = initServer(new CommonsHttpSolrServer(System.getProperty("solr.home.url") + System.getProperty("core.content")), settings);		
			wordCore=initServer(new CommonsHttpSolrServer(System.getProperty("solr.home.url") + System.getProperty("core.word")), settings);
		} 
		catch(MalformedURLException e)
		{
			System.err.println("[MalformedURLException] SOLRServer() message: " + e.getMessage());
		}
		catch (Exception e) 
		{
			System.err.println("[Exception] SOLRServer() message: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
		
	public SolrServer getBookCore() {
		return bookCore;
	}
	
	public SolrServer getContentCore() {
		return contentCore;
	}
		
	public SolrServer getWordCore() {
		return wordCore;
	}


	public static SOLRServer getInstance(){
		return SOLRSERVER;
	}
	
	private SolrServer initServer(CommonsHttpSolrServer server, Map<String, String> settings){

		server.setSoTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_SOCKET_TIMEOUT)));
		server.setConnectionTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_CONN_TIMEOUT)));
		server.setDefaultMaxConnectionsPerHost(Integer.parseInt(settings.get(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST)));
		server.setMaxTotalConnections(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_TOTAL_CONN)));
		server.setFollowRedirects(Boolean.parseBoolean(settings.get(SOLRServer.KEY_FOLLOW_REDIRECTS)));
		server.setAllowCompression(Boolean.parseBoolean(settings.get(SOLRServer.KEY_ALLOW_COMPRESSION)));
		server.setMaxRetries(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_RETRIES)));

		return server;
	}
}
