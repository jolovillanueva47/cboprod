package org.cambridge.ebooks.production.solr.util;


import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;

/**
 * Revisions
 * 20131208 - alacerna added Query for Word Core
 */


public class SOLRSearchWorker{
	
	private static final String URL_SOLR_HOME = System.getProperty("solr.home.url");
	private static final String CORE_BOOK = System.getProperty("core.book");
	private static final String CORE_CHAPTER = System.getProperty("core.content");
	
	private static final SolrServer bookCore = SOLRServer.getInstance().getBookCore();
	
	public SOLRSearchWorker(){}	

		
	@SuppressWarnings("unchecked")
	public <T>List<T> searchCoreDictionary(T t, String searchText,SolrServer solrServer) {
		List<T> result = new ArrayList<T>();
		
		SolrQuery q = generateQueryDictionary(searchText);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] List<T> SOLRSearchWorker.searchCore(T, String, SolrServer)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer) {
		List<T> result = new ArrayList<T>();
		
		SolrQuery q = generateQuery(searchText);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] List<T> SOLRSearchWorker.searchCore(T, String, SolrServer)");
			ex.printStackTrace();
		}
			
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T searchContentCoreSinglePager(T t, String searchText, SolrServer solrServer, int page) {
		T result = t;
		
		SolrQuery q = generateQuery(searchText);
		onePagerPagination(q, page);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (T)response.getBeans(t.getClass()).get(0);
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] T SOLRSearchWorker.searchContentCoreSinglePager(T, String, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer, int rowcount) {
		List<T> result = new ArrayList<T>();
		
		SolrQuery q = generateQuery(searchText);
		setQueryRowCount(q, rowcount);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] SOLRSearchWorker.searchContentCoreSinglePager(T, String, SolrServer, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCores(T t, String searchText) {
		List<T> result = new ArrayList<T>();;
		String urlShardsSolrHome = URL_SOLR_HOME.replaceAll("http://", "");
		
		SolrQuery query = generateQuery(searchText);
		query.setRows(1000);
		query.setParam("shards", urlShardsSolrHome + CORE_BOOK + "," + urlShardsSolrHome + CORE_CHAPTER);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(query, bookCore,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());	
		}
		catch(Exception ex)
		{
			System.err.println("[Exception] List<T> SOLRSearchWorker.searchCores(T, String)" + ex.getMessage());
		}
			
		return result;
	}
	

	public SolrQuery generateQuery(String searchText){
		SolrQuery query = new SolrQuery();
		query.setQuery(searchText);
		query.setRows(1000);
		return query;
	}
	
	private SolrQuery generateQueryDictionary(String searchText) {
		SolrQuery query = new SolrQuery();
		query.setQuery(searchText);
		query.setRows(10000);
		return query;
	}
	
	
	public QueryResponse generateResponse(SolrQuery q, SolrServer server, SolrRequest.METHOD m){
		QueryResponse response = null;
		try 
		{
			response = server.query(q, m);
			System.err.println(server.query(q, m));
		} 
		catch (SolrServerException e) {
			// TODO Auto-generated catch block
			System.err.println("[SolrServerException] QueryResponse SOLRSearchWorker. generateResponse(SolrQuery, SolrServer, SolrRequest.METHOD) message: ");
			e.printStackTrace();
		}
		
		return response;
	}

	
	private void onePagerPagination(SolrQuery q, int pageNum) {
		q.setRows(1);
		q.setStart(pageNum);
	}
	
	private void setQueryRowCount(SolrQuery q, int rowcount) {
		q.setRows(rowcount);
	}
}
