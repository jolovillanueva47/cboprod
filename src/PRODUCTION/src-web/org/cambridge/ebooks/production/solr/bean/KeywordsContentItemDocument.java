package org.cambridge.ebooks.production.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

/**
 * 
 * @author jmendiola
 *
 * This class is a POJO for handling keywords from the keyword core
 * Created 20130624
 * 
 * 20130702 - jmendiola - added xmlFilename and bookId * 
 * 20131205 - jmendiola POSSIBILITY OF DELETION BECAUSE STAHL2 CHANGES DIDN'T PUSH THROUGH
 * 
 */


public class KeywordsContentItemDocument {

	@Field("id")
	private String id;
	
	@Field("content_id")
	private String contentId;
	
	@Field("xml_filename")
	private String xmlFilename;
	
	@Field("keyword_group_src")
	private String keywordName;
	
	@Field("keyword_text")
	private List<String> keywordText;
	
	@Field("book_id")
	private String bookId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getXmlFilename() {
		return xmlFilename;
	}

	public void setXmlFilename(String xmlFilename) {
		this.xmlFilename = xmlFilename;
	}

	public String getKeywordName() {
		return keywordName;
	}

	public void setKeywordName(String keywordName) {
		this.keywordName = keywordName;
	}

	public List<String> getKeywordText() {
		return keywordText;
	}

	public void setKeywordText(List<String> keywordText) {
		this.keywordText = keywordText;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	
	
}
