package org.cambridge.ebooks.production.solr.util;

import java.net.MalformedURLException;

import org.apache.solr.client.solrj.impl.LBHttpSolrServer;

public enum SolrCores {
	CBO_PROD_BOOK(System.getProperty("core.prod.book")),
	CBO_PROD_REFERENCES(System.getProperty("core.prod.references")),
	CBO_PROD_CONTENT(System.getProperty("core.prod.content")),
	CBO_PROD_WORD(System.getProperty("core.prod.word"));
	
	private LBHttpSolrServer solrServer;
	private String coreName;
	
	private SolrCores(String coreName) {		
		String slaveServer1 = System.getProperty("solr.server1.url");
		String slaveServer2 = System.getProperty("solr.server2.url");
		
		StringBuilder slaveCoreUrl1 = new StringBuilder(slaveServer1).append("/").append(coreName);
		StringBuilder slaveCoreUrl2 = new StringBuilder(slaveServer2).append("/").append(coreName);
		try {
			solrServer = new LBHttpSolrServer(slaveCoreUrl1.toString(), slaveCoreUrl2.toString());
			initServerSettings(solrServer);
		} catch (MalformedURLException e) {
			System.err.println("[Exception] SOLRServer() message: " + e.getMessage());
			e.printStackTrace();
		}
		
		this.coreName = coreName;
	}
	
	private void initServerSettings(LBHttpSolrServer server) {			
		server.setAliveCheckInterval(Integer.parseInt(System.getProperty("lb.alive.check.interval")));
		server.setConnectionManagerTimeout(Integer.parseInt(System.getProperty("lb.connection.manager.timeout")));
		server.setConnectionTimeout(Integer.parseInt(System.getProperty("lb.connection.timeout")));
		server.setSoTimeout(Integer.parseInt(System.getProperty("lb.socket.timeout")));
	}
	
	public LBHttpSolrServer solrServer() {
		return solrServer;
	}
	
	public String coreName() {
		return this.coreName;
	}
}
