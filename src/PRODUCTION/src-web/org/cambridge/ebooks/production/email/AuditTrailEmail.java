package org.cambridge.ebooks.production.email;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.util.MailManager;

public class AuditTrailEmail {
	
	private static String TEMPLATE_DIR = "/app/ebooks/templates";
	
	//live
	private static String OUTPUT_DIR = "/app/ebooks/outbox/";
	
	//staging
	//private static String OUTPUT_DIR = "/app/jboss-4.2.3.GA/server/default/outbox/";
	
//	private static String TEMPLATE_DIR = "C:/DRU/PRODUCTION/audit_trail_email/";
//	private static String OUTPUT_DIR = "C:/DRU/PRODUCTION/audit_trail_email/";
	
	private static String TEMPLATE_FILE = "mail_user_info.ftl";
	
	private static String FROM = "do-not-reply@cambridge.org";
	
	private static String[] TO = {"rmacdonald@cambridge.org"};
	
	private static String[] CC = {"amustill@cambridge.org","ebooks.cambridge@gmail.com"};
	
	private static String SUBJECT = "EBooks Production - Incorrect Loading";
	
	public static void main(String[] args) {
		Connection con = AuditTrailEmailDataSource.getConnection();
		try 
		{
			if(con != null)
			{
				Statement stmt = con.createStatement();
				String query = 	"select audit_id, eisbn, content_id, filename, " +
								"series_code, load_type, username, status, " +
								"audit_date, remarks  from audit_trail_books " +
								"where remarks = 'Warning: Content should not have been reloaded.' " +
								"and audit_date >= SYSDATE - 1 and audit_date <= SYSDATE";
				
				ResultSet rs = stmt.executeQuery(query);
				StringBuffer body = new StringBuffer();

				body.append("<html><body><table border='1' width='100%'>" +
								"<tr>" +
								"<td><b>EISBN</b></td>" +
								"<td><b>CONTENT ID</b></td>" +
								"<td><b>FILENAME</b></td>" +
								"<td><b>TYPE</b></td>" +
								"<td><b>USERNAME</b></td>" +
								"<td><b>STATUS</b></td>" +
								"<td><b>AUDIT DATE</b></td>" +
								"<td><b>REMARKS</b></td>" +
								"</tr>");
				
				boolean sendMail = false;
				while (rs.next()){
					sendMail = true;
					body.append("<tr>");
					body.append("<td>"+		rs.getString("EISBN") 		+"</td>");
					body.append("<td>"+		rs.getString("CONTENT_ID")	+"</td>");
					body.append("<td>"+		rs.getString("FILENAME")	+"</td>");
					body.append("<td>"+		rs.getString("LOAD_TYPE")	+"</td>");
					body.append("<td>"+		rs.getString("USERNAME")	+"</td>");
					body.append("<td>"+		Status.getStatus(Integer.parseInt(rs.getString("STATUS"))).getDescription()	+"</td>");
					body.append("<td>"+		rs.getString("AUDIT_DATE")	+"</td>");
					body.append("<td>"+		rs.getString("REMARKS")		+"</td>");
					body.append("</tr>");
				}
				body.append("</table></body></html>");
				
				if(sendMail){
					MailManager.storeMail(TEMPLATE_DIR, TEMPLATE_FILE, OUTPUT_DIR, FROM, TO,
							CC, null, SUBJECT, null, body.toString());
				}
			}
		} 
		catch(SQLException sqle)
		{
			System.out.println(AuditTrailEmail.class + " [SQLException] " + sqle);
			try {
				con.rollback();
			} catch (SQLException e) {
				System.out.println(AuditTrailEmail.class + " [SQLException] " + "error in rollback." + e.getMessage());
			}
		}
		catch (Exception e) 
		{
			System.out.println(AuditTrailEmail.class + " [Exception] " + e.getMessage());
		}
		finally
		{
			AuditTrailEmailDataSource.closeConnection(con);
		}
	}
}
