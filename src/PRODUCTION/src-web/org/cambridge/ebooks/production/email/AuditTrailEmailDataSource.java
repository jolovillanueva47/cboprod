package org.cambridge.ebooks.production.email;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author regin
 *
// * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuditTrailEmailDataSource {

	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			System.err.println("OracleDriver error"+e);
		}
	}
	
	public static Connection getConnection() {
		Connection con = null;
		try {
			String driverType = System.getProperty(AuditTrailEmailProperties.DRIVERTYPE);
			String serverName = System.getProperty(AuditTrailEmailProperties.SERVERNAME);
			String portNumber = System.getProperty(AuditTrailEmailProperties.PORTNUMBER);
			String databaseName = System.getProperty(AuditTrailEmailProperties.DATABASENAME);
			String user = System.getProperty(AuditTrailEmailProperties.USER);
			String password = System.getProperty(AuditTrailEmailProperties.PASSWORD);
			
			con = DriverManager.getConnection(driverType + ":@" + serverName + ":" + portNumber + ":" + databaseName, user, password);
		} catch (Exception e) {
			System.err.println("Error while getting connection"+e);
		}
		return con;
	}
	
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
