/*
 * Created on Jan 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.production.email;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuditTrailEmailProperties {
	
	public static String DRIVERTYPE = "db.drivertype";
	public static String SERVERNAME = "db.servername";
	public static String DATABASENAME = "db.databasename";
	public static String PORTNUMBER = "db.portnumber";
	public static String USER = "db.user";
	public static String PASSWORD = "db.password";	

	//private static final String indexerProperties = "C:/eclipse-jee-ganymede-SR1-win32/eclipse/workspace/cbo/PRODUCTION/src-web/org/cambridge/ebooks/production/email/ebooksdb.properties";
	
	//staging or live
	private static final String indexerProperties = "/app/jboss-4.2.3.GA/server/default/deploy/production.war/WEB-INF/classes/org/cambridge/ebooks/production/email/ebooksdb.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
