<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	
	<title>View Abstract</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>	
</head>

<body>	
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />			
	</div>
	<table id="content" cellspacing="0"> 
		<tbody>
			<tr>
				<td id="centre">
					<h1>[<h:outputText value="#{bookContentBean.ebook.isbn}"/>]&nbsp;<h:outputText value="#{bookContentBean.ebookOtherContent.title}"/></h1>
					
					<div id="registered">		
						<form >
							<table>
								<tr>
									<td>											
										<ul>
											<li>
												<span style="font-size: 12px">
												
												<c:choose>
													<c:when test="${empty sessionScope.bookContentBean.ebookOtherContent.abstractText}">
														<br/>&nbsp;No abstract.
													</c:when>
													<c:otherwise>
														${sessionScope.bookContentBean.ebookOtherContent.abstractText}
													</c:otherwise>
												</c:choose>
												
												</span>
											</li>										
										</ul>
									</td>
								</tr>
								
								<c:if test="${not empty sessionScope.bookContentBean.ebookOtherContent.abstractImage}">
									
									<tr>
										<td>											
											<ul>
												<li>
													<img border="1" src="../content/${sessionScope.bookContentBean.eisbn}/${sessionScope.bookContentBean.ebookOtherContent.abstractImage}" />
												</li>										
											</ul>
										</td>
									
									</tr>
								</c:if>
							</table>
							
						</form>		
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>
</body>

</f:view>

</html>