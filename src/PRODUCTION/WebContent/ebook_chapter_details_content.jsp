
<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

		<style type="text/css">
			
			table.inserts
			{
			position:relative;
			left:75px;
			width:90%;
			}
						
		</style>

		<h3 id="chapter_title">
			<strong>${chapterBeanDetails.title}</strong>
			(<em>${chapterBeanDetails.titleAlphasort}</em>)
		</h3>

				<table id="ebooks_metadata" cellspacing="1" cellpadding="3">				 
				<!-- 
					Note to Karl
					I used jstl in this page in order for the view meta data to work
					(display all contents of all chapters) 
				-->
					
					
					<tr>
						<th align="right" width="20%" valign="top">type:</th>
						<td width="80%"><c:out value="${chapterBeanDetails.contentType}" /></td>
					</tr>	
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th align="right" width="20%" valign="top">ID:</th>
						<td width="80%"><c:out value="${chapterBeanDetails.contentId}" /></td>
					</tr>
					<c:if test="${chapterBeanDetails.displayDOI}" >
						<tr>
							<th align="right" valign="top">DOI:</th>
							<td><c:out value="${chapterBeanDetails.doi}" /></td>						
						</tr>				
					</c:if>
					<c:if test="${chapterBeanDetails.displayPdfFilename}" >
						<tr>
							<th align="right" valign="top">PDF Filename:</th>
							<td>							
								<%-- <a href="#" onclick="openWindow('../content/${bookContentBean.eisbn}/${chapterBeanDetails.pdfFilename}');return false;"> --%>
								<a target='_blank' href='../content/${chapterBeanDetails.isbnBreakdown}/${chapterBeanDetails.pdfFilename}'>
									<c:out value="${chapterBeanDetails.pdfFilename}" />
								</a>							
							</td>
						</tr>
					</c:if>				
										
					<c:if test="${chapterBeanDetails.displayHeadingLabel}" >						
						<tr>
							<th align="right" valign="top">Label:</th>
							<!-- PID 86837 -->
							<td><c:out value="${chapterBeanDetails.headingLabel}" escapeXml="false" /></td> 
						</tr>		
					</c:if>					
					<tr>
						<th align="right" valign="top">Title:</th>
						<td><h:outputText value="#{chapterBeanDetails.headingTitle}" escape="false" /></td>
						<!-- 
						<td><c:out value="${chapterBeanDetails.headingTitle}" escapeXml="false" /></td>
						 -->
					</tr>						
					<c:if test="${chapterBeanDetails.displayHeadingSubtitle}" >						
						<tr>
							<th align="right" valign="top">Subtitle:</th>
							<td><c:out value="${chapterBeanDetails.headingSubtitle}" escapeXml="false" /></td> 
						</tr>		
					</c:if>
										
					<c:if test="${chapterBeanDetails.displayContributorName}" >
						<tr>
							<th align="right" valign="top">Contributor Name:</th>
							<td><c:out value="${chapterBeanDetails.contributorName}" escapeXml="false" /></td>
						</tr>					
					</c:if>
					<c:if test="${chapterBeanDetails.displayContributorCollab}" >
						<tr>
							<th align="right" valign="top">Contributor Collab:</th>
							<td><c:out value="${chapterBeanDetails.contributorCollab}" escapeXml="false" /></td>
						</tr>					
					</c:if>			
					<c:if test="${chapterBeanDetails.displayContributorAffiliation}" >
						<tr>
							<th align="right" valign="top">Contributor Affiliation:</th>
							<td><c:out value="${chapterBeanDetails.contributorAffiliation}" escapeXml="false" /></td>
						</tr>			
					</c:if>	
					<c:if test="${chapterBeanDetails.displaySubjectGroup}" >
						<tr>
							<th align="right" valign="top">Subject Group:</th>
							<td><c:out value="${chapterBeanDetails.subjectGroup}" /></td>
						</tr>
					</c:if>
					<c:if test="${chapterBeanDetails.displayThemeGroup}" >
						<tr>
							<th align="right" valign="top">Theme Group:</th>
							<td><c:out value="${chapterBeanDetails.themeGroup}" /></td>
						</tr>
					</c:if>	
					<%-- 				
					<c:if test="${chapterBeanDetails.displayKeyword}" >
						<tr>
							<th align="right" valign="top">Keyword(s):</th>
							<td><c:out value="${chapterBeanDetails.keyword}" /></td>
						</tr>
					</c:if>
					 --%>					
					<c:if test="${chapterBeanDetails.displayToc}">
						<tr>
							<th align="right" valign="top">TOC:</th>
							<td><c:out value="${chapterBeanDetails.toc}" escapeXml="false"/></td>
						</tr>
					</c:if>			
					<tr><td>&nbsp;</td></tr>
					<c:if test="${chapterBeanDetails.displayPages}" >						
						<tr>
							<th align="right" valign="top">Pages:</th>
							<td>
								<c:out value="${chapterBeanDetails.pageStart}" />&nbsp;-
								<c:out value="${chapterBeanDetails.pageEnd}" />
							</td>
						</tr>	
					</c:if>
					
					
					<!-- MAPS AND PLATES -->
					<tr><td>&nbsp;</td></tr>
					<tr>
					<td colspan="2">
					<table class="inserts" cellspacing="1" cellpadding="3" >
					<c:if test="${chapterBeanDetails.displayEBookInsertBeansList}" >
						<tr>
							<td></td>
							<td colspan="2">
								<p>
								<b>Additional Material:</b><br/><br/>
								<c:forEach var="mapsandplates" items="${chapterBeanDetails.ebookInsertBeansList}" >
									<b>Title:</b> <c:out value="${mapsandplates.insertTitle}" escapeXml="false" />&nbsp;<i>(<c:out value="${mapsandplates.insertAlphasortTitle}" /></i>)<br/>
									<b>PDF Filename:</b>
										<a style="text-decoration: none" target='_blank' href='../content/${mapsandplates.isbnBreakdown}/${mapsandplates.insertFilename}'>
											<b><c:out value="${mapsandplates.insertFilename}" /></b>
										</a>
									<br/><br/>
								</c:forEach>
								</p>
							</td>
						</tr>
					</c:if>
					</table>
					</td>
					</tr>
					<!-- MAPS AND PLATES END -->
					
					
					<!-- abstract content -->
					<c:if test="${chapterBeanDetails.displayAbstractContent}">
						<tr><td>&nbsp;</td></tr>
						<tr>
							<th colspan="2" align="left" valign="top">
							Abstract: 
							<c:if test="${chapterBeanDetails.displayProblem}">
								Problem = <c:out value="${chapterBeanDetails.problem}" />
							</c:if>
							</th>
						</tr>						
						<tr>							
							<td colspan="2"><c:out value="${chapterBeanDetails.abstractContent}" escapeXml="false" /></td>
						</tr>
						<tr><td>&nbsp;</td></tr>	
						<tr>							
							<td colspan="2" align="center">
								<img src="${chapterBeanDetails.abstractImage}" alt="${chapterBeanDetails.abstractImage}"		onerror="imageError(this, '<%=EbookDetailsBean.CORRUPT_COVER_IMAGE %>')" 
									border="1" />
							</td>
						</tr>					
					</c:if>
					

					
				</table>
