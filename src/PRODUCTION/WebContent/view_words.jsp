<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page	import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.util.HttpUtil"%>
<%@page	import="org.cambridge.ebooks.production.document.EbookChapterDetailsBean"%>
<%@page	import="org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean"%>
<%@page	import="org.cambridge.ebooks.production.ebook.content.EBookContentBean"%>

<f:view>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>List of Dictionary Words</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
		@import url(css/cbo.css);
		
	-->
	</style>
</head>

<body>
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />	
	</div>
	<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre"><div id="browse_ebooks">
		<!-- Body -->
		<h1>List of Words</h1>	
		</div></td>
	</tr></tbody>	
	</table>
	<div id="ebooks_list">
	<h:dataTable cellpadding="3" cellspacing="1" value="#{contentBean.dictionaryDetails}" var="dictionary" >
	<h:column>
		<h:outputLink target="_blank" value="../content/#{dictionary.isbnBreakdown}html_chunk/#{dictionary.wordUrl}" >
		<h:outputText value="#{dictionary.wordText}"/>
		</h:outputLink>
	</h:column>
	</h:dataTable>
	</div>


</body>
</f:view>

</html>