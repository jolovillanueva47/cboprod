<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Manage System Users</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcomepanel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<h:form>
			<h:commandLink value="Home" action="home" />
	
			&gt; <h:outputText value="Manage Publishers" escape="false"/>
		</h:form>
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menupanel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Manage Publishers</h1>
			<div id="searchresults">
			
			<h:inputHidden id="alertMessage"/>
			<h:panelGroup rendered="#{!empty facesContext.maximumSeverity}">
				<p class="alert"><h:message for="alertMessage"/><br/></p>
			</h:panelGroup>
			
				<h:form id="publishersForm" prependId="false">

					<h:commandButton actionListener="#{publisherUpdateBean.initNewPublisher}" action="#{publisherBean.updatePublisher}" styleClass="button" value="Add new publisher"/>
										 
					<h:commandLink id="jsf_submitter" actionListener="#{publisherBean.setPagerValues}"></h:commandLink>
					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">Sort by</td>
							<td>
    							<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherBean.setSortBy}" value="#{publisherBean.sortBy}">
    								<f:selectItem itemValue="Publisher" />
    								<f:selectItem itemValue="Email" />
    							</h:selectOneMenu>			
							</td>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{publisherBean.setPageSize}" value="#{publisherBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{publisherBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherBean.setCurrentPage}" value="#{publisherBean.goToPage}">
									<f:selectItems value="#{publisherBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{publisherBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{publisherBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{publisherBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{publisherBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>

					<h:dataTable id="publishers" var="publisher" value="#{publisherBean.publisherList}" cellspacing="0">
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Publisher Id"/>
        					</f:facet>
        					
        					<h:commandLink actionListener="#{publisherUpdateBean.initPublisher}" action="publisher_update"> 
								<h:outputText value="#{publisher.code}"/>
        						<f:attribute name="code" value="#{publisher.code}"/>
							</h:commandLink>
        				</h:column>
						
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Publisher Name"/>
        					</f:facet> 
							<h:outputText value="#{publisher.name}"/>
						</h:column>
						
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Email Address"/>
        					</f:facet> 
							<h:outputText value="#{publisher.email}"/>
						</h:column>
						
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Template"/>
        					</f:facet> 
							<h:outputText value="#{publisher.template}"/>
						</h:column>
						
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Modified By"/>
        					</f:facet> 
							<h:outputText value="#{publisher.modifiedBy}"/>
						</h:column>
						
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Modified Date"/>
        					</f:facet> 
							<h:outputText value="#{publisher.modifiedDate}">
								<f:convertDateTime pattern="dd-MMM-yyyy" />
							</h:outputText>
							
						</h:column>
												 			
						<h:column id="delete_column">
							<f:facet name="header">
        						<h:outputText value="Delete"/>
        					</f:facet> 
							<h:selectBooleanCheckbox valueChangeListener="#{publisherDeleteBean.addToList}">
								<f:attribute name="code" value="#{publisher.code}"/>
								<f:attribute name="name" value="#{publisher.name}"/>
								<f:attribute name="email" value="#{publisher.email}"/>
							</h:selectBooleanCheckbox>
						</h:column>
					</h:dataTable>


					<table cellspacing="0" class="basket_buttons">
						<tr>
							<td><h:commandButton actionListener="#{publisherDeleteBean.confirmDelete}" action="#{publisherDeleteBean.deletePublishersNav}" styleClass="button" value="Delete publishers" /></td>
						</tr>
					</table>

					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">Sort by</td>
							<td>
    							<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherBean.setSortBy}" value="#{publisherBean.sortBy}">
    								<f:selectItem itemValue="Publisher" />
    								<f:selectItem itemValue="Email" />
    							</h:selectOneMenu>			
							</td>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{publisherBean.setPageSize}" value="#{publisherBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{publisherBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherBean.setCurrentPage}" value="#{publisherBean.goToPage}">
									<f:selectItems value="#{publisherBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{publisherBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{publisherBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{publisherBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{publisherBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>
				</h:form>
			</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>