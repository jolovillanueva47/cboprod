<html>
	<head>
	</head>
	<body>
		<div id="log">
		</div>
		
		<script type="text/javascript" src="js/prototype-1.6.0.3.js"></script>
		<script type="text/javascript">
			var indexStartChecker = new Ajax.PeriodicalUpdater("", "php/index_process_checker.php?type=start", {
				method: "post",
				insertion: Insertion.Bottom,
				frequency: 1,
				onSuccess: function(transport) {	
					var json = transport.responseText.evalJSON(true);
					if(json["status"] == "start") {
						$("log").update($("log").innerHTML + json["response"] + "<br/>");
						indexStartChecker.stop();
						indexProcessChecker.start();
					}
				}
			});

			var indexProcessChecker = new Ajax.PeriodicalUpdater("", "php/index_process_checker.php?type=process", {
				method: "post",
				insertion: Insertion.Bottom,
				frequency: 1, 
				onSuccess: function(transport) {	
					var json = transport.responseText.evalJSON(true);
					if(json["status"] == "stop") {
						$("log").update($("log").innerHTML + json["response"] + "<br/>");
						indexProcessChecker.stop();
					}
				},
				onCreate: function() {
					indexProcessChecker.stop();
				}
			});
			
		</script>
	</body>
</html>