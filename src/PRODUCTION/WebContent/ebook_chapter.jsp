<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<f:view>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />    
    <title>eBook Details</title>
    <style type="text/css">
    <!--
        @import url(css/users_general.css);
        @import url(css/users_general_content.css);
        @import url(codebase/dhtmlxcalendar.css);
    -->
    </style>
    <!--[if IE 8]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 6]>
        <link href="css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->    
</head>
    
<body onload="updateAllEmbargoInputText();">

<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

    <f:subview id="welcome_panel">
        <c:import url="components/welcome.jsp"></c:import>
    </f:subview>
    
    <!-- Crumbtrail -->
    <div id="location"> 
        <h:form>
            <h:commandLink value="Home" action="home" />&gt; 
            <h:outputText value="#{bookContentBean.ebook.title}" escape="false"/>
        </h:form>
    </div>
    
</div>

    
<!-- Menu -->

    <f:subview id="menu_panel">
        <c:import url="components/navigation.jsp"></c:import>
    </f:subview>

<!-- main content -->

<table id="content" cellspacing="0"> 
    <tbody>

    <tr>
        <td id="centre">
        <!-- Body -->

            <h1>eBook Details</h1>
            <h:form id="chapter_form" prependId="false">
            <h:inputHidden id="loader" value="#{bookContentBean.initBookContents}" />
            <div id="browse_tabs">
                <ul>
                    <li><a class="selected"><h:outputText value="#{bookContentBean.ebook.isbn}" /></a></li>
                    <li><h:commandLink action="#{ebookBean.show}" actionListener="#{ebookBean.showListener}" >
                            <f:attribute name="bookId" value="#{bookContentBean.bookId}" />
                            <f:param name="bookId" value="#{bookContentBean.bookId}" />
                            <h:outputText value="#{bookContentBean.ebook.title}" escape="false"/>
                        </h:commandLink>
                    </li>
                </ul>
            </div>
            <div id="browse_ebooks">

                <h:inputHidden id="alertMessage"/>
                <h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
                    <p class="alert"><h:message for="alertMessage"/><br/></p>
                </h:panelGroup>

                <%--added feedback that CCO, CHO and SSO files cannot send crossref files -jubs --%>
                <h:panelGroup rendered="#{!empty eBookDeliveryBean.crossRefXmlFileName and eBookDeliveryBean.crossRefXmlFileName eq 'noFile'}" > 
                    <p class="alert">CCO, CHO and SSO files cannot send DOI files.</p>
                </h:panelGroup>
                <h:panelGroup rendered="#{!empty eBookDeliveryBean.crossRefXmlFileName and !( eBookDeliveryBean.crossRefXmlFileName eq 'noFile')}">
                    <p class="alert"><h:outputLink value="downloads/crossref/#{eBookDeliveryBean.crossRefXmlFileName}">Click this link to view the CrossRef XML file.</h:outputLink><br/><br/></p>
                </h:panelGroup>
                <h:panelGroup rendered="#{!empty bookContentBean.crossRefXmlFileName and !(eBookDeliveryBean.crossRefXmlFileName eq 'noFile')}">
                    <p class="alert"><h:outputLink value="downloads/crossref/#{bookContentBean.crossRefXmlFileName}">Click this link to view the CrossRef XML file.</h:outputLink><br/><br/></p>
                </h:panelGroup>
                
                <div>
                    <h:outputText value="All chapters: "/>
                    <h:selectOneMenu value="#{bookContentBean.chapterStatus}" onchange="javascript:selectEbooks(this.value);"  style="width:175px;">
                        <f:selectItems value="#{bookContentBean.statusOptions}"/>
                    </h:selectOneMenu> &nbsp;
                    <h:outputText value="Embargo: "/> <h:inputText id="embargoTextAll" style="left:50px; width: 100px; " />
                    &nbsp;&nbsp;<h:commandButton styleClass="button" action="#{bookContentBean.updateEbookContents}" value="Update Status All"/>
                     &nbsp;<h:message for="embargoTextAll" />
                    
                    <h:outputText value="Chapters: "/>
                    <h:selectOneMenu id="viewByStatusSelect" value="#{bookContentBean.displayStatus}" style="width:175px;">
                        <f:selectItem itemLabel="All" itemValue="" />
                        <f:selectItem itemLabel="Loaded for Proofreading" itemValue="0" />
                        <f:selectItem itemLabel="Reloaded with Corrections" itemValue="1" />
                        <f:selectItem itemLabel="Proofread Accepted" itemValue="2" />
                        <f:selectItem itemLabel="Proofread with Error" itemValue="3" />
                        <f:selectItem itemLabel="Approved" itemValue="4" />
                        <f:selectItem itemLabel="Reviewed with Error" itemValue="5" />
                        <f:selectItem itemLabel="Reloaded for Approver" itemValue="6" />
                        <f:selectItem itemLabel="Content Signed Off" itemValue="7" />
                        <f:selectItem itemLabel="Correction Required" itemValue="8" />
                        <f:selectItem itemLabel="Embargo" itemValue="9" />
                        <f:selectItem itemLabel="Deleted" itemValue="100" />
                    </h:selectOneMenu> 
                    &nbsp;
                    <h:commandButton styleClass="button" action="#{bookContentBean.displayEbookContentsByStatus}" value="Filter By Chapter"/>
                    &nbsp;
                    <h:inputHidden id="viewByStatusTemp" value=""/>
                </div>

                <div id="button_divider" align="right">
                    <h:panelGroup rendered="#{bookContentBean.ebookOthers['cbml'].ebookContent.fileName ne null || bookContentBean.ebookOthers['bits'].ebookContent.fileName ne null }">
                        <h:inputText id="proofingURL" value="#{bookContentBean.proofingUrl }" readonly="true" size="35"/>
                        <h:panelGroup rendered="#{! bookContentBean.allowGenerateUrl}">
                            <h:commandButton disabled="true" styleClass="disabled" value="Generate URL" />&nbsp;
                            <h:commandButton disabled="true" styleClass="disabled" value="Reindex Book" />&nbsp;
                        </h:panelGroup>
                        <h:panelGroup rendered="#{empty bookContentBean.proofingUrl && bookContentBean.allowGenerateUrl}">
                            <h:commandButton styleClass="button" value="Generate URL" actionListener="#{bookContentBean.generateUrl }" />&nbsp;
                            <h:commandButton disabled="true" styleClass="disabled" value="Reindex Book" />&nbsp;
                        </h:panelGroup>
                        <h:panelGroup rendered="#{! empty bookContentBean.proofingUrl && bookContentBean.allowGenerateUrl}">
                            <h:commandButton styleClass="button" value="Delete URL" actionListener="#{bookContentBean.deleteUrl }" />&nbsp;
                            <h:commandButton styleClass="button" value="Reindex Book" actionListener="#{bookContentBean.reindexBook }" />&nbsp;
                        </h:panelGroup>
                    </h:panelGroup> 
                    <h:commandButton styleClass="button" value="View metadata" onclick="viewMetadata(); return false;" />
                    <input type="button" value="View references" class="button" onclick="showPopupWindow('view_references.jsf?bid=<h:outputText value="#{bookContentBean.bookId}"/>'); return false;"/>
                    <input type="button" value="View keywords" class="button" onclick="viewKeywords(); return false;"/>
                </div>
<!-- 
                <div id="button_divider" align="left">
                    <input type="button" class="button" value="Force Re-index" id="forceReindexButton" onclick="forceReindex(); return false;" />
                    <label id="forceReindexMessage"> </label>            
                </div>
 -->
                <!-- XML -->
                <div id="ebooks_list">
                <table border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <th width="40%"><h:outputText styleClass="chapterDescription" value="XML"/></th>
                        <th width="20%"><h:outputText value="Status"/></th>
                        <th width="20%"><h:outputText value="Embargo"/></th>
                        <th width="20%"><h:outputText value="Remarks"/></th>
                       </tr>
                    <tr>
                        <td>                
                            <a style="text-decoration: none" href="../content/<h:outputText value="#{bookContentBean.ebookOthers['xml'].isbnBreakdown}/#{bookContentBean.ebookOthers['xml'].ebookContent.fileName}" />"><h:outputText value="header.xml" /></a>
                            <br/><br/>
                            <ul><li>
                                <%-- <a href="#" onclick="javascript:openWindow('<h:outputText value="../content/#{bookContentBean.isbnBreakdown}" />/<h:outputText value="#{bookContentBean.ebookOthers['xml'].ebookContent.fileName}" />');return false;"> --%>
                                <a href="#" onclick="javascript:openWindow('<h:outputText value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}" />/<h:outputText value="#{bookContentBean.ebookOthers['xml'].ebookContent.fileName}" />');return false;">
                                    <h:outputText value="#{bookContentBean.ebookOthers['xml'].ebookContent.displayFileType}"/>
                                </a>
                            </li></ul>
                        </td>
                        <td>
                            <h:selectOneMenu value="#{bookContentBean.ebookOthers['xml'].ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{bookContentBean.ebookOthers['xml'].valueChanged}" style="width:175px;" id="xmlStatusSelect">
                                <f:selectItems value="#{bookContentBean.ebookOthers['xml'].options}"/>
                            </h:selectOneMenu>
                        </td>
                        <td>
                            <h:inputText id="xmlEmbargoText" title="embargo" value="#{bookContentBean.ebookOthers['xml'].ebookContent.embargoDate}" valueChangeListener="#{bookContentBean.ebookOthers['xml'].valueChanged}" style="left:50px; width: 100px;" >
                                <f:convertDateTime pattern="dd-MMM-yyyy" />
                            </h:inputText>
                            <br />
                             <h:message style="color: red;" for="xmlEmbargoText" />
                        </td>
                        <td>
                            <h:inputTextarea cols="30" value="#{bookContentBean.ebookOthers['xml'].ebookContent.remarks}" valueChangeListener="#{bookContentBean.ebookOthers['xml'].valueChanged}" styleClass="expand10-80"/>
                        </td>
                    </tr>
                </table>
                </div>
                <br/>
                
                <%-- jubs 20130205 for eDictionaries CBML --%>
                <!-- CBML -->
                <div id="ebooks_list">
                <h:panelGroup rendered="#{bookContentBean.ebookOthers['cbml'].ebookContent.fileName ne null}">
                <table border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <th width="40%"><h:outputText styleClass="chapterDescription" value="CBML"/></th>
                        <th width="20%"><h:outputText value="Status"/></th>
                        <th width="20%"><h:outputText value="Embargo"/></th>
                        <th width="20%"><h:outputText value="Remarks"/></th>
                       </tr>
                    <tr>
                        <td>                
                            <a style="text-decoration: none" href="downloads/xml/<h:outputText value="#{bookContentBean.ebookOthers['cbml'].ebookContent.fileName}" />"><h:outputText value="#{bookContentBean.ebookOthers['cbml'].ebookContent.fileName}" /></a>
                            <br/><br/>
                            <ul><li>
                                <h:panelGroup>
                                    <a href="#" onclick="javascript:openWindow('<h:outputText value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}" />/<h:outputText value="#{bookContentBean.ebookOthers['cbml'].ebookContent.fileName}" />');return false;">
                                        <h:outputText value="#{bookContentBean.ebookOthers['cbml'].ebookContent.displayFileType}"/>
                                    </a>
                                </h:panelGroup>
                                &nbsp;
                                <h:panelGroup rendered="#{bookContentBean.ebookOthers['cbml'].ebookContent.navFileName ne null}">
                                    <h:outputLink target="_blank" value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}html_chunk/#{bookContentBean.ebookOthers['cbml'].ebookContent.navFileName}" >
                                        <h:outputText value="View NavFile" />
                                    </h:outputLink>
                                </h:panelGroup>
                                &nbsp;
                                <h:panelGroup rendered="#{bookContentBean.hasXsltLog}">
                                    <h:outputLink styleClass="button" target="_blank" value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}xsltTransformLog.html" >
                                        <h:outputText value=" View XSLT log" />
                                    </h:outputLink>
                                </h:panelGroup>
                            </li></ul>
                        </td>
                        <td>
                            <h:selectOneMenu value="#{bookContentBean.ebookOthers['cbml'].ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{bookContentBean.ebookOthers['cbml'].valueChanged}" style="width:175px;" id="cbmlStatusSelect">
                                <f:selectItems value="#{bookContentBean.ebookOthers['cbml'].options}"/>
                            </h:selectOneMenu>
                        </td>
                        <td>
                            <h:inputText id="cbmlEmbargoText" title="embargo" value="#{bookContentBean.ebookOthers['cbml'].ebookContent.embargoDate}" valueChangeListener="#{bookContentBean.ebookOthers['cbml'].valueChanged}" style="left:50px; width: 100px;" >
                                <f:convertDateTime pattern="dd-MMM-yyyy" />
                            </h:inputText>
                            <br />
                             <h:message style="color: red;" for="cbmlEmbargoText" />
                        </td>
                        <td>
                            <h:inputTextarea cols="30" value="#{bookContentBean.ebookOthers['cbml'].ebookContent.remarks}" valueChangeListener="#{bookContentBean.ebookOthers['cbml'].valueChanged}" styleClass="expand10-80"/>
                        </td>
                    </tr>
                </table>
                </h:panelGroup>
                </div>
                <br/>
                
                <%-- lng 20140911 for ILR BITS --%>
                <!-- BITS -->
                <div id="ebooks_list">
                <h:panelGroup rendered="#{bookContentBean.ebookOthers['bits'].ebookContent.fileName ne null}">
                <table border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <th width="40%"><h:outputText styleClass="chapterDescription" value="BITS"/></th>
                        <th width="20%"><h:outputText value="Status"/></th>
                        <th width="20%"><h:outputText value="Embargo"/></th>
                        <th width="20%"><h:outputText value="Remarks"/></th>
                       </tr>
                    <tr>
                        <td>                
                            <a style="text-decoration: none" href="../content/<h:outputText value="#{bookContentBean.ebookOthers['xml'].isbnBreakdown}/#{bookContentBean.ebookOthers['bits'].ebookContent.fileName}" />"><h:outputText value="#{bookContentBean.ebookOthers['bits'].ebookContent.fileName}" /></a>
                            <br/><br/>
                            <ul><li>
                                <%-- <a href="#" onclick="javascript:openWindow('<h:outputText value="../content/#{bookContentBean.isbnBreakdown}" />/<h:outputText value="#{bookContentBean.ebookOthers['xml'].ebookContent.fileName}" />');return false;"> --%>
                                <h:panelGroup>
                                    <a href="#" onclick="javascript:openWindow('<h:outputText value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}" />/<h:outputText value="#{bookContentBean.ebookOthers['bits'].ebookContent.fileName}" />');return false;">
                                        <h:outputText value="#{bookContentBean.ebookOthers['bits'].ebookContent.displayFileType}"/>
                                    </a>
                                </h:panelGroup>
                                &nbsp;
                                <h:panelGroup rendered="#{bookContentBean.ebookOthers['bits'].ebookContent.navFileName ne null}">
                                    <h:outputLink target="_blank" value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}html_chunk/#{bookContentBean.ebookOthers['bits'].ebookContent.navFileName}" >
                                        <h:outputText value="View NavFile" />
                                    </h:outputLink>
                                </h:panelGroup>
                                &nbsp;
                                <h:panelGroup rendered="#{bookContentBean.hasXsltLog}">
                                    <h:outputLink styleClass="button" target="_blank" value="../content/#{bookContentBean.ebookOthers['xml'].isbnBreakdown}xsltTransformLog.html" >
                                        <h:outputText value=" View XSLT log" />
                                    </h:outputLink>
                                </h:panelGroup>
                            </li></ul>
                        </td>
                        <td>
                            <h:selectOneMenu value="#{bookContentBean.ebookOthers['bits'].ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{bookContentBean.ebookOthers['bits'].valueChanged}" style="width:175px;" id="bitsStatusSelect">
                                <f:selectItems value="#{bookContentBean.ebookOthers['bits'].options}"/>
                            </h:selectOneMenu>
                        </td>
                        <td>
                            <h:inputText id="bitsEmbargoText" title="embargo" value="#{bookContentBean.ebookOthers['bits'].ebookContent.embargoDate}" valueChangeListener="#{bookContentBean.ebookOthers['bits'].valueChanged}" style="left:50px; width: 100px;" >
                                <f:convertDateTime pattern="dd-MMM-yyyy" />
                            </h:inputText>
                            <br />
                             <h:message style="color: red;" for="bitsEmbargoText" />
                        </td>
                        <td>
                            <h:inputTextarea cols="30" value="#{bookContentBean.ebookOthers['bits'].ebookContent.remarks}" valueChangeListener="#{bookContentBean.ebookOthers['bits'].valueChanged}" styleClass="expand10-80"/>
                        </td>
                    </tr>
                </table>
                </h:panelGroup>
                </div>
                <br/>
                
                
                <!-- CONTENTS -->
                <h:panelGroup rendered="#{bookContentBean.ebookContentList != null}">
                <div id="ebooks_list">
                <h:dataTable columnClasses="firstColumn,nextColumn,nextColumn,nextColumn"  border="0" cellpadding="3" cellspacing="1" value="#{bookContentBean.ebookContentList}" var="content" id="chapterTable">
                                    
                    <h:column>
                        <f:facet name="header">
                               <h:outputText styleClass="chapterDescription" value="Contents Description"/>
                           </f:facet>
                        <h:panelGroup rendered="#{content.indented}">
                        <!-- div id="part_chapter" -->
                        <h:outputText value='<div id="#{content.indentLevel}">' escape="false"/>
                            
                            <h3><h:commandLink action="#{chapterBean.show}" 
                                actionListener="#{chapterBean.showListener}" style="text-decoration: none" >
                                <f:actionListener type="org.cambridge.ebooks.production.delivery.EBookDeliveryBean"/>
                                <h:outputText value="#{content.title}" escape="false" />
                                <f:attribute name="ebookContentBean" value="#{content}" />
                                <f:attribute name="bookContentBean" value="#{bookContentBean}" />
                                <f:attribute name="chapterBean" value="#{chapterBean}" />
                            </h:commandLink></h3>
                            
                            <p> 
                            
                            <h5><b>Alphasort:</b> <h:outputText value="#{content.alphasort}" escape="false" /></h5> <!-- PID 86837 -->
                            <h5><b>Type:</b> <h:outputText value="#{content.type}" /></h5>
                            <h5><b>Chapter Id:</b> <h:outputText value="#{content.contentId}" /></h5>    
                            <h:panelGroup rendered="#{content.ebookContent.status > -1&& content.ebookContent.fileName ne null}">
                                <h5><b>PDF:</b> <h:outputText value="#{content.fileName}" /></h5>    
                            </h:panelGroup>
                            <h:panelGroup rendered="#{content.ebookContent.navFileName ne null}">
                                <h5><b>Nav Html:</b> <h:outputText value="#{content.navFileName}" /></h5>    
                            </h:panelGroup>                
                            </p>
                            <ul>                            
                                <li>
                                    <%-- added '|| content.ebookContent.fileName eq null' to the PDF check jubs 20140311 --%>
                                    <h:panelGroup rendered="#{content.ebookContent.status eq -1 || content.ebookContent.fileName eq null}">
                                        <h:outputText value="PDF" />
                                    </h:panelGroup>
                                    &nbsp;
                                    <h:panelGroup rendered="#{content.type eq 'dictionary'}" >
                                        <h:outputLink target="_blank" value="#" 
                                        onclick="javascript:openWindow('view_words.jsf?isbn=#{content.isbn}&bookId=#{bookContentBean.bookId}'); return false;">
                                        <h:outputText value="View Html" />
                                    </h:outputLink>
                                    </h:panelGroup>
                                    &nbsp;    
                                <h:panelGroup rendered="#{content.ebookContent.status > -1&& content.ebookContent.fileName ne null}">
                                        <%-- <h:outputLink value="#" onclick="javascript:openWindow('../content/#{bookContentBean.eisbn}/#{content.fileName}');return false;" > --%>
                                        <%-- <h:outputLink target="_blank" value="../content/#{bookContentBean.isbnBreakdown}/#{content.fileName}" > --%>
                                        <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.fileName}" >
                                            <h:outputText value="PDF" />
                                        </h:outputLink>
                                        &nbsp;
                                        <%-- <h:outputLink value="#" onclick="javascript:openWindow('popups/applet_pdf_viewer.jsp?cid=#{bookContentBean.isbnBreakdown}/#{content.fileName}'); return false;"> --%>
                                        <h:outputLink value="#" onclick="javascript:openWindow('popups/applet_pdf_viewer.jsp?cid=#{content.isbnPath}/#{content.fileName}'); return false;">    
                                            <h:outputText value="IcePDF" />
                                        </h:outputLink>
                                        &nbsp;
                                        <%-- jmendiola 20130709 for Stahl 2 
                                        <h:panelGroup rendered="#{content.xmlFileName ne null}">
                                            &nbsp;
                                            <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.xmlFileName}" >
                                                <h:outputText value="CBMLc" />
                                            </h:outputLink>
                                            &nbsp; 
                                            <h:outputLink target="_blank" value="#" 
                                                onclick="javascript:openWindow('ebook_details_cbml.jsf?isbn=#{content.isbn}&filename=#{content.cbmlFileName}'); return false;">
                                                <h:outputText value="HTML" />
                                            </h:outputLink>
                                        </h:panelGroup>--%>
                                    </h:panelGroup>
                                    <h:panelGroup rendered="#{content.ebookContent.navFileName ne null}">
                                         <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.navFileName}" >
                                            <h:outputText value="View NavFile" />
                                        </h:outputLink>
                                    </h:panelGroup>
                                    
                                </li>
                            </ul>
                        <h:outputText value="</div>" escape="false"/>
                        </h:panelGroup>
                        <h:panelGroup rendered="#{not content.indented}">
                            <h3><h:commandLink action="#{chapterBean.show}" 
                                actionListener="#{chapterBean.showListener}" style="text-decoration: none">
                                <f:actionListener type="org.cambridge.ebooks.production.delivery.EBookDeliveryBean"/>
                                <h:outputText value="#{content.title}" escape="false" />
                                <f:attribute name="ebookContentBean" value="#{content}" />
                                <f:attribute name="bookContentBean" value="#{bookContentBean}" />
                                <f:attribute name="chapterBean" value="#{chapterBean}" />
                            </h:commandLink></h3>                            
                            <p>
                            <h5><b>Alphasort:</b> <h:outputText value="#{content.alphasort}" escape="false" /></h5> <!-- PID 86837 -->
                            <h5><b>Type:</b> <h:outputText value="#{content.type}" /></h5>
                            <h5><b>Chapter Id:</b> <h:outputText value="#{content.contentId}" /></h5>                                                    
                            <h:panelGroup rendered="#{content.ebookContent.status > -1 && content.ebookContent.fileName ne null}">
                                <h5><b>PDF:</b> <h:outputText value="#{content.fileName}" /></h5>    
                            </h:panelGroup>
                            <h:panelGroup rendered="#{content.ebookContent.navFileName ne null}">
                                <h5><b>Nav Html:</b> <h:outputText value="#{content.navFileName}" /></h5>    
                            </h:panelGroup>                        
                            </p>                            
                            <ul>
                                <li>
                                    <h:panelGroup rendered="#{content.ebookContent.status eq -1 || content.ebookContent.fileName eq null && content.type ne 'dictionary' }">
                                        <h:outputText value="PDF" />
                                    </h:panelGroup>
                                    &nbsp;
                                    <h:panelGroup rendered="#{content.type eq 'dictionary'}" >
                                    <h:outputLink target="_blank" value="#" 
                                        onclick="javascript:openWindow('view_words.jsf?isbn=#{content.isbn}&bookId=#{bookContentBean.bookId}'); return false;">
                                        <h:outputText value="View Html" />
                                    </h:outputLink>
                                    </h:panelGroup>
                                    &nbsp;                                
                                    <h:panelGroup rendered="#{content.ebookContent.status > -1 && content.ebookContent.fileName ne null}">
                                        <%-- <h:outputLink value="#" onclick="javascript:openWindow('../content/#{bookContentBean.eisbn}/#{content.fileName}');return false;" > --%>
                                        <%-- <h:outputLink target="_blank" value="../content/#{bookContentBean.isbnBreakdown}/#{content.fileName}" > --%>
                                        <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.fileName}" >
                                            <h:outputText value="PDF" />
                                        </h:outputLink>
                                        &nbsp;
                                        <%-- <h:outputLink value="#" onclick="javascript:openWindow('popups/applet_pdf_viewer.jsp?cid=#{bookContentBean.isbnBreakdown}/#{content.fileName}'); return false;"> --%>
                                        <h:outputLink value="#" onclick="javascript:openWindow('popups/applet_pdf_viewer.jsp?cid=#{content.isbnPath}/#{content.fileName}'); return false;">    
                                            <h:outputText value="IcePDF" />
                                        </h:outputLink>
                                        &nbsp;
                                        <%-- jmendiola 20130709 for Stahl 2 
                                        <h:panelGroup rendered="#{content.xmlFileName ne null}">
                                            &nbsp;
                                            <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.xmlFileName}" >
                                                <h:outputText value="CBMLc" />
                                            </h:outputLink>
                                            &nbsp; 
                                            <h:outputLink target="_blank" value="#" 
                                                onclick="javascript:openWindow('ebook_details_cbml.jsf?isbn=#{content.isbn}&filename=#{content.cbmlFileName}'); return false;">
                                                <h:outputText value="HTML" />
                                            </h:outputLink>
                                        </h:panelGroup>--%>
                                    </h:panelGroup>
                                    <h:panelGroup rendered="#{content.ebookContent.navFileName ne null}">
                                        <h:outputLink target="_blank" value="../content/#{content.isbnPath}#{content.navFileName}" >
                                            <h:outputText value="View NavFile" />
                                        </h:outputLink>
                                    </h:panelGroup>                        
                                </li>
                            </ul>
                        </h:panelGroup>
                    </h:column>
                     
                    <h:column>
                        <f:facet name="header">
                               <h:outputText value="Status"/>
                           </f:facet>     
                        <h:selectOneMenu rendered="#{not content.notLoadedPartTitle && content.ebookContent.fileName ne null}" value="#{content.ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{content.valueChanged}" style="width:175px;" id="chaptersSelect">
                            <f:selectItems value="#{content.options}"/>
                        </h:selectOneMenu>
                    </h:column>

                     <h:column>
                        <f:facet name="header">
                               <h:outputText value="Embargo"/>
                           </f:facet>

                         <h:inputText rendered="#{not content.notLoadedPartTitle && content.ebookContent.fileName ne null}" id="embargoText" title="embargo" value="#{content.ebookContent.embargoDate}" valueChangeListener="#{content.valueChanged}" style="left:50px; width: 100px;" >
                             <f:convertDateTime pattern="dd-MMM-yyyy" />
                         </h:inputText>
                         
                         <br />
                         <h:message for="embargoText" />
                         </h:column>
                     
                    <h:column>
                        <f:facet name="header">
                               <h:outputText value="Remarks"/>
                           </f:facet>

                        <h:inputTextarea cols="30" rendered="#{not content.notLoadedPartTitle && content.ebookContent.fileName ne null}" value="#{content.ebookContent.remarks}" valueChangeListener="#{content.valueChanged}" styleClass="expand10-80"/>
                    </h:column>
                     
             
                </h:dataTable>
                </div>
                </h:panelGroup>

                <h:panelGroup rendered="#{empty bookContentBean.ebookContentList}">
                    <table cellspacing="0">        
                        <tr class="border">
                            <td colspan="2"><strong>No Contents generated.</strong></td>
                        </tr>
                    </table>
                </h:panelGroup>
                <br/>
                
                <!-- IMAGES -->
                <div id="ebooks_list">
                <table border="0" cellpadding="3" cellspacing="1" id="ebooks_list">
                    <tr>
                        <th width="40%"><h:outputText styleClass="chapterDescription" value="Images"/></th>
                        <th width="20%"><h:outputText value="Status"/></th>
                        <th width="20%"><h:outputText value="Embargo"/></th>
                        <th width="20%"><h:outputText value="Remarks"/></th>                            
                       </tr>
                    <tr>
                        <h:panelGroup rendered="#{!empty bookContentBean.ebookOthers['image_standard']}">
                            <h:panelGroup rendered="#{bookContentBean.ebookOthers['image_standard'].ebookContent.status > -1}">
                                <td>
                                    <a style="text-decoration: none" href="downloads/images/<h:outputText value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.fileName}" />"><h:outputText value="Cover Image" /></a>
                                    <br/><br/>
                                    <ul><li>
                                        <%-- <a href="#" onclick="javascript:openWindow('ebook_show_image.jsf?f=<h:outputText value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.fileName}" />&id=<h:outputText value="#{bookContentBean.bookId}" />');return false;"> --%>
                                        <a href="#" onclick="javascript:openWindow('ebook_show_image.jsf?imagetype=image_standard&f=<h:outputText value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.fileName}" />&id=<h:outputText value="#{bookContentBean.bookId}" />');return false;">
                                            <h:outputText value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.displayFileType}"/>
                                        </a>
                                    </li></ul>
                                </td>
                            </h:panelGroup>
                            <h:panelGroup rendered="#{bookContentBean.ebookOthers['image_standard'].ebookContent.status eq -1}">
                                <td>
                                    <h:outputText value="Cover Image" />
                                    <br/><br/>
                                    <ul><li>
                                        <h:outputText value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.displayFileType}"/>
                                    </li></ul>
                                </td>
                            </h:panelGroup>
                            <td>
                                <h:selectOneMenu value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{bookContentBean.ebookOthers['image_standard'].valueChanged}" style="width:175px;" id="imageStandardStatusSelect">
                                    <f:selectItems value="#{bookContentBean.ebookOthers['image_standard'].options}"/>
                                </h:selectOneMenu>
                            </td>
                            <td>
                                <h:inputText id="imageStandardEmbargoText" title="embargo" value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.embargoDate}" valueChangeListener="#{bookContentBean.ebookOthers['image_standard'].valueChanged}" style="left:50px; width: 100px;" >
                                    <f:convertDateTime pattern="dd-MMM-yyyy" />
                                </h:inputText>
                                <br />
                                 <h:message style="color: red;" for="imageStandardEmbargoText" />
                            </td>
                            <td>
                                <h:inputTextarea cols="30" value="#{bookContentBean.ebookOthers['image_standard'].ebookContent.remarks}" valueChangeListener="#{bookContentBean.ebookOthers['image_standard'].valueChanged}" styleClass="expand10-80"/>
                            </td>
                        </h:panelGroup>
                        <h:panelGroup rendered="#{empty bookContentBean.ebookOthers['image_standard']}">
                            <td>No cover image defined.</td>
                            <td></td>
                            <td></td>
                        </h:panelGroup>
                    </tr>
                    <tr>
                        <h:panelGroup rendered="#{!empty bookContentBean.ebookOthers['image_thumb']}">
                            <h:panelGroup rendered="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.status > -1}">
                                <td>
                                    <a style="text-decoration: none" href="downloads/images/<h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.fileName}" />"><h:outputText value="Cover Image Thumbnail" /></a>
                                    <br/><br/>
                                    <ul><li>
                                        <%-- <a href="#" onclick="javascript:openWindow('ebook_show_image.jsf?f=<h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.fileName}" />&id=<h:outputText value="#{bookContentBean.bookId}" />');return false;"> --%>
                                        <a href="#" onclick="javascript:openWindow('ebook_show_image.jsf?imagetype=image_thumb&f=<h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.fileName}" />&id=<h:outputText value="#{bookContentBean.bookId}" />');return false;">
                                            <h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.displayFileType}"/>
                                        </a>
                                    </li></ul>
                                </td>
                            </h:panelGroup>
                            <h:panelGroup rendered="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.status eq -1}">
                                <td>
                                    <h:outputText value="Cover Image Thumbnail" />
                                    <br/><br/>
                                    <ul><li>
                                        <h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.displayFileType}"/>
                                    </li></ul>
                                </td>
                            </h:panelGroup>
                            <td>
                                <h:selectOneMenu value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.status}" onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{bookContentBean.ebookOthers['image_thumb'].valueChanged}" style="width:175px;" id="imageThumbStatusSelect">
                                    <f:selectItems value="#{bookContentBean.ebookOthers['image_thumb'].options}"/>
                                </h:selectOneMenu>
                            </td>
                            <td>
                                <h:inputText id="imageThumbEmbargoText" title="embargo" value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.embargoDate}" valueChangeListener="#{bookContentBean.ebookOthers['image_thumb'].valueChanged}" style="left:50px; width: 100px;" >
                                    <f:convertDateTime pattern="dd-MMM-yyyy" />
                                </h:inputText>
                                <br />
                                 <h:message style="color: red;" for="imageThumbEmbargoText" />
                            </td>
                            <td>
                                <h:inputTextarea cols="30" value="#{bookContentBean.ebookOthers['image_thumb'].ebookContent.remarks}" valueChangeListener="#{bookContentBean.ebookOthers['image_thumb'].valueChanged}" styleClass="expand10-80"/>                                
                            </td>
                        </h:panelGroup>
                        <h:panelGroup rendered="#{empty bookContentBean.ebookOthers['image_thumb']}">
                            <td>No cover image thumbnail defined.</td>
                            <td></td>
                            <td></td>
                        </h:panelGroup>
                    </tr>
                </table>
                </div>
                <br/>
                
                
                <!-- ADDITIONAL MATERIALS: MAPS AND PLATES PID 81717 2012-05-07  latest version-->
                <h:panelGroup rendered="#{bookContentBean.ebookInsertList ne null && !empty bookContentBean.ebookInsertList}">
                <div id="ebooks_list">
                <h:dataTable columnClasses="firstColumn,nextColumn,nextColumn,nextColumn" border="0" cellpadding="3" cellspacing="1" value="#{bookContentBean.ebookInsertList}" var="inserts" id="mapsAndPlatesTable" >
                    <h:column>
                        <f:facet name="header">
                            <h:outputText value="Additional Materials" />
                        </f:facet>
                        <h3>
                            <!-- PID 86036 2012-11-26 -->
                            <!-- <a style="text-decoration: none" href="downloads/mapsandplates/<h:outputText value="#{inserts.insertFilename}" />">  -->
                                <h:outputText value="#{inserts.insertTitle}" escape="false" />
                            <!-- </a> -->
                        </h3>
                        <p>
                        <h5><b>Content ID:</b>
                            <h:outputText value="#{inserts.contentId}" />
                        </h5>
                        <h:panelGroup rendered="#{inserts.ebookContent.status > -1}" >
                            <h5><b>PDF:</b>
                                <h:outputText value="#{inserts.insertFilename}" />
                            </h5>
                        </h:panelGroup>
                        </p>
                        
                            <ul>                            
                                <li>
                                    <h:panelGroup rendered="#{inserts.ebookContent.status eq -1 || inserts.ebookContent.fileName eq null}">
                                        <h:outputText value="PDF" />
                                    </h:panelGroup>
                                    <h:panelGroup rendered="#{inserts.ebookContent.status > -1 && inserts.ebookContent.fileName ne null}">
                                        <h:outputLink target="_blank" value="../content/#{inserts.isbnBreakdown}/#{inserts.insertFilename}" >
                                            <h:outputText value="PDF" />
                                        </h:outputLink>
                                        &nbsp;
                                        <h:outputLink value="#" onclick="javascript:openWindow('popups/applet_pdf_viewer.jsp?cid=#{inserts.isbnBreakdown}/#{inserts.insertFilename}'); return false;">
                                            <h:outputText value="IcePDF" />
                                        </h:outputLink>
                                    </h:panelGroup>
                                </li>
                            </ul>
                    </h:column>
                    <h:column>
                        <f:facet name="header">
                               <h:outputText value="Status"/>
                           </f:facet>
                            <h:selectOneMenu rendered="#{inserts.ebookContent.fileName ne null}" value="#{inserts.ebookContent.status}" 
                                             onchange="javascript:updateEmbargoInputText(this);" valueChangeListener="#{inserts.valueChanged}" 
                                             style="width:175px;" id="mapsAndPlatesStatusSelect" >
                                <f:selectItems value="#{inserts.options}"/>
                            </h:selectOneMenu>
                    </h:column>
                    <h:column>
                        <f:facet name="header">
                                  <h:outputText value="Embargo"/>
                              </f:facet>
                         <h:inputText rendered="#{inserts.ebookContent.fileName ne null}" id="mapsAndPlatesEmbargoText" title="embargo" value="#{inserts.ebookContent.embargoDate}" 
                                      valueChangeListener="#{inserts.valueChanged}" style="left:50px; width: 100px;" >
                             <f:convertDateTime pattern="dd-MMM-yyyy" />
                         </h:inputText>
                         
                         <br />
                         <h:message for="embargoText" />
                       </h:column>
                    <h:column>
                        <f:facet name="header">
                               <h:outputText value="Remarks"/>
                           </f:facet>

                        <h:inputTextarea cols="30" rendered="#{inserts.ebookContent.fileName ne null}" value="#{inserts.ebookContent.remarks}" valueChangeListener="#{inserts.valueChanged}" 
                                         styleClass="expand10-80"/>
                    </h:column>
                </h:dataTable>
                </div>
                </h:panelGroup>
                <br/>
                <!-- END HERE FOR MAPS AND PLATES -->
                
                <div id="ebooks_list">
                <table border="0" cellpadding="3" cellspacing="1" id="reference">
                    <tr>
                        <th>Reference Type:</th>
                    </tr>
                    <tr>
                        <td>
                        <h:selectOneRadio id="model" value="#{bookContentBean.ebookModelType }">
                            <f:selectItem itemValue="CBO" itemLabel="CBO Model" />
                            <f:selectItem itemValue="CHO" itemLabel="CHO Model" />
                        </h:selectOneRadio>
                        </td>
                    </tr>
                </table>
                </div>
                
                
                <table cellspacing="0" class="basket_buttons">
                    <tr>
                        <td>
                            <h:commandButton styleClass="button" value="Update" id="publish" action="#{bookContentBean.updateEbookContents}" />
                        </td>
                    </tr>
                </table>
                
                <h:inputHidden id="id" value="#{bookContentBean.bookId}"/>
                
            </div>

            <h:panelGroup rendered="#{userInfo.accessRights.canDeliverData}">
            <div id="registered">
                <h:inputHidden id="deliveryBookId" value="#{eBookDeliveryBean.bookId}"/>
                <h:inputHidden id="deliveryLoader" value="#{eBookDeliveryBean.ebookDeliveryStatus}"/>
                <h2>eBook Delivery</h2>
                <table cellspacing="0">
                    <tr>
                        <td>Depositor name </td>
                        <td><h:outputText value="#{eBookDeliveryBean.depositorName}" /></td>
                    </tr>
                    <tr class="border">
                        <td>CrossRef date delivered</td>
                        <td><h:outputText value="#{eBookDeliveryBean.crossRefDateDelivered}"/></td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">CrossRef Batch ID</td>
                        <td><h:outputText value="#{eBookDeliveryBean.crossRefBatchId}"/></td>
                    </tr>
                    <tr class="border">
                        <td>Deliver to</td>
                        <td>
                            <h:selectBooleanCheckbox id="crossRefCheckBox" onchange="onlyCrossRefAvailable(this);" value="#{eBookDeliveryBean.deliverToCrossRef}"/>CrossRef
                            <script type="text/javascript">
                                document.getElementById('crossRefCheckBox').checked = true;
                                function onlyCrossRefAvailable(elem) {
                                    if (elem.checked == false) {
                                        elem.checked = true;
                                        alert('CrossRef is the only deliverable at the moment.');
                                    }
                                }
                            </script>
                        </td>
                    </tr>
                    <tr class="border">
                        <td>Delivery Option</td>
                        <td>
                            <h:selectOneRadio value="#{eBookDeliveryBean.deliveryOption}" styleClass="jsf_table">
                                <f:selectItem itemValue="xmlOnly" itemLabel="Create XML file only" />
                                <f:selectItem itemValue="xmlSend" itemLabel="Create and send XML file" />
                            </h:selectOneRadio>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" class="registered_buttons">
                    <tr>
                        <td>
                            <h:commandButton actionListener="#{eBookDeliveryBean.deliverEBook}" value="Deliver eBook" styleClass="button">
                                <f:attribute name="bookContentBean" value="#{bookContentBean}"/>
                            </h:commandButton>
                        </td>
                    </tr>
                </table>
            </div>
            
            
            <h:panelGroup rendered="#{!empty bookContentBean.crossrefFiles}">
            <div id="browse_ebooks">
            <div id="ebooks_list">
            <h2>Created Crossref Files</h2>
            <h:dataTable columnClasses="firstColumn,nextColumn,nextColumn,nextColumn,nextColumn" border="0" cellpadding="3" cellspacing="1" value="#{bookContentBean.crossrefFiles}" var="crossrefFilejsf" id="crossrefData">
                <h:column>
                    <f:facet name="header">
                        <h:outputText styleClass="chapterDescription" value="File Name"/>
                    </f:facet>
                    <h:outputLink value="downloads/crossref/#{crossrefFilejsf.filename }">
                        <h:outputText value="#{crossrefFilejsf.filename }" />
                    </h:outputLink>
                </h:column>
                
                <h:column>
                    <f:facet name="header">
                        <h:outputText  value="Submitted By"/>
                    </f:facet>
                    <h:outputText value="#{crossrefFilejsf.submitter }" />
                </h:column>
                
                <h:column>
                    <f:facet name="header">
                        <h:outputText  value="Date Created"/>
                    </f:facet>
                    <h:outputText value="#{crossrefFilejsf.timestamp }" />
                </h:column>
                
                <h:column>
                    <f:facet name="header">
                        <h:outputText  value="DOI Batch Id"/>
                    </f:facet>
                    <h:outputText value="#{crossrefFilejsf.doiBatchId }" />
                </h:column>
                
                <h:column>
                    <f:facet name="header">
                        <h:outputText  value="Send Status"/>
                    </f:facet>
                    <h:outputText value="#{crossrefFilejsf.status }" />
                </h:column>
            </h:dataTable>
            </div>
            </div>
            </h:panelGroup>
            
            
            </h:panelGroup>
            </h:form>
        
        </td>
    </tr>

    </tbody>
</table>


<f:subview id="footer_panel">
    <c:import url="components/footer.jsp"></c:import>
</f:subview>




    <script>window.dhx_globalImgPath="codebase/imgs/";</script>
    
    <script src="codebase/dhtmlxcommon.js"></script>
    <script src="codebase/dhtmlxcalendar.js"></script>
    
    <script type="text/javascript" src="js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="js/jquery.textarea-expander.js"></script>



<script type="text/javascript">
    
    var cal1, cal2, mDCal, newStyleSheet;
    var from = new Date();
    from.setDate(from.getDate() + 1);

    var to = new Date();
    to.setFullYear(to.getFullYear() + 5);
    
    $(function() {
        changeTRcolorByStatus();
    });
    
    function changeTRcolorByStatus(){
         $('select').each(function(){
                //8 for Correction Required
                //7 for Content Signed Off
                 if('8' == $(this).val()){
                     $(this).parent('td').parent('tr').css('background-color', '#ffcd85'); // #EEEE00
                 } else if('7' == $(this).val()){
                     $(this).parent('td').parent('tr').css('background-color', '#d2f5b0'); // #66CD00
                 }
            });
    }
    
    function openWindow(url) { //v2.0        
          var h = window.open(url,'','location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes,scrollbars=yes,width=700,height=440');
          h.focus();
    }
    
    function selectEbooks(val){
        var count;
        var optionCount;

        if(val == '300') //reset status in Status enum
        {
            document.chapter_form.reset();
        }
        else
        {
            
            updateEmbargoTextAll();
            
            for(x=2; x<(document.chapter_form.getElementsByTagName('select').length); x++)
            {
                for(y=0; y<(document.chapter_form.getElementsByTagName('select')[x].length); y++)
                {
                    if(document.chapter_form.getElementsByTagName('select')[x].options[y].value == val)
                    {
                         document.chapter_form.getElementsByTagName('select')[x].options[y].selected=true;
                         updateEmbargoInputText(document.chapter_form.getElementsByTagName('select')[x]);
                     }
                }
            }
        } // else
    }

    function updateAllEmbargoInputTextValues() {
        if (document.chapter_form.getElementsByTagName('select')[0].value == 9) {
            var count;
            var embargoVal;
            for(x=0; x<(document.chapter_form.getElementsByTagName('input').length); x++){
                if(document.chapter_form.getElementsByTagName('input')[x].getAttribute('title') == 'embargo'
                        && document.chapter_form.getElementsByTagName('input')[x].disabled==false){
                    embargoVal = document.getElementById('embargoTextAll').value;
                    
                    document.chapter_form.getElementsByTagName('input')[x].value = embargoVal;
                }
            }
        }
    }
    
    
    function updateAllSelectStatusFromEmbargoText() {
        var count;
        var optionCount;
        
        if (document.chapter_form.getElementsByTagName('select')[0].value == 9) {
            
            for(x=2; x<(document.chapter_form.getElementsByTagName('select').length); x++){
                for(y=0; y<(document.chapter_form.getElementsByTagName('select')[x].length); y++){
                     if(document.chapter_form.getElementsByTagName('select')[x].options[y].value == 9){
                         document.chapter_form.getElementsByTagName('select')[x].options[y].selected=true;
                         updateEmbargoInputText(document.chapter_form.getElementsByTagName('select')[x]);
                     }
                }
            }
            
        }

    }
    
    
    function updateEmbargoTextAll() {
        if (document.chapter_form.getElementsByTagName('select')[0].value != 9) {
            document.chapter_form.embargoTextAll.disabled = true;
        } else {
        
            document.chapter_form.embargoTextAll.disabled = false;
            
            cal1 = new dhtmlxCalendarObject('embargoTextAll');
            cal1.setDateFormat('%d-%b-%Y');
            cal1.setSensitive(from, to);
            cal1.attachEvent("onClick",updateAllSelectStatusFromEmbargoText);
            cal1.attachEvent("onClick",updateAllEmbargoInputTextValues);
            
            document.chapter_form.embargoTextAll.readOnly = true;
            
        }
    }
    
    function updateEmbargoInputText(elem) {
        var objectInput;
        
        if (elem.id == 'xmlStatusSelect') 
        {
            objectInput = document.getElementById('xmlEmbargoText')
        }
        else if (elem.id == 'cbmlStatusSelect') 
        {
            objectInput = document.getElementById('cbmlEmbargoText')
        } 
        else if (elem.id == 'bitsStatusSelect') 
        {
            objectInput = document.getElementById('bitsEmbargoText')
        } 
        else if (elem.id == 'imageStandardStatusSelect') 
        {
            objectInput = document.getElementById('imageStandardEmbargoText');
        } 
        else if (elem.id == 'imageThumbStatusSelect') 
        {
            objectInput = document.getElementById('imageThumbEmbargoText');
        } 
        else if (elem.id.split(':')[0] == 'chapterTable')
        {
            objectInput = document.getElementById('chapterTable:' + elem.id.split(':')[1] + ':embargoText');
        }
        // PID 81717
        else if (elem.id.split(':')[2] == 'mapsAndPlatesStatusSelect')
        {
            objectInput = document.getElementById('mapsAndPlatesTable:' + elem.id.split(':')[1] + ':mapsAndPlatesEmbargoText');
        }
        
        if(elem.value == 9) {
            
            objectInput.disabled = false;
            
            if (elem.id == 'xmlStatusSelect') 
            {
                cal1 = new dhtmlxCalendarObject('xmlEmbargoText');
            }
            else if (elem.id == 'cbmlStatusSelect') 
            {
                cal1 = new dhtmlxCalendarObject('cbmlEmbargoText');
            }
            else if (elem.id == 'bitsStatusSelect') 
            {
                cal1 = new dhtmlxCalendarObject('bitsEmbargoText');
            }
            else if (elem.id == 'imageStandardStatusSelect') 
            {
                cal1 = new dhtmlxCalendarObject('imageStandardEmbargoText');
            }
            else if (elem.id == 'imageThumbStatusSelect') 
            {
                cal1 = new dhtmlxCalendarObject('imageThumbEmbargoText');
            }
            else if (elem.id.split(':')[0] == 'chapterTable')
            {
                cal1 = new dhtmlxCalendarObject('chapterTable:' + elem.id.split(':')[1] + ':embargoText');
            }
            // PID 81717
            else if (elem.id.split(':')[2] == 'mapsAndPlatesStatusSelect')
            {
                cal1 = new dhtmlxCalendarObject('mapsAndPlatesTable:' + elem.id.split(':')[1] + ':mapsAndPlatesEmbargoText');
            }
            
            cal1.setDateFormat('%d-%b-%Y');            
            cal1.setSensitive(from, to);
            objectInput.readOnly = true; 
        
        }
        else 
        {
            objectInput.disabled = true; 
        }
    }
    
    function updateAllEmbargoInputText() {
        updateEmbargoTextAll();

        for(x=2; x<(document.chapter_form.getElementsByTagName('select').length); x++)
        {
            updateEmbargoInputText(document.chapter_form.getElementsByTagName('select')[x]);    
        }

    }
    
    
    function selectAllStatus(val){
        var count;
        var optionCount;
        
        for(x=0; x<(document.chapter_form.getElementsByTagName('select')[0].length); x++){
            
            if(document.chapter_form.getElementsByTagName('select')[0].options[x].selected == true){
                 document.chapter_form.getElementsByTagName('select')[0].options[x].value;
            }
        }
        
    }
    
    function forceReindex(isbn){
        $("#forceReindexButton").attr("disabled", true);
        $("#forceReindexButton").val("Re-indexing...");
        $("#forceReindexMessage").text("Re-indexing. Please wait...");
        $.post("/production/forceReindex", {ISBN: "${bookContentBean.ebook.isbn}"},
                function(data) {
                    $("#forceReindexMessage").text(data);
                    $("#forceReindexButton").val("Force Re-index");
                    $("#forceReindexButton").attr("disabled", false);
                }
            );
    }
    
    function viewMetadata() {    
        var url = "ebook_details_popup.jsf?<%=EbookDetailsBean.SHOW%>=<%=EbookDetailsBean.META%>&bookId=${bookContentBean.bookId}&page=0";
        openNewWindow(url,"metadata");                        
    }
    
    function viewKeywords() {        
        var url = "ebook_details_popup.jsf?<%=EbookDetailsBean.SHOW%>=<%=EbookDetailsBean.KEYWORD%>&bookId=${bookContentBean.bookId}&page=0";         
        openNewWindow(url, "keyword");            
    }

    function openNewWindow(url,name){
        $.get("ajax", 
                {     CLASS_NAME: "org.cambridge.ebooks.production.document.EbookDetailsBean",
                    VAR_NAME: "ebookBean",
                    CONTEXT: "SESSION",
                    METHOD: "SET,initPopup",
                    POPUP: "Y",
                    BOOK_ID: "${bookContentBean.bookId}"
                },
                function (msg) {
                    var width = 800;
                    var height = 600;
                    var left = (screen.width - width) / 2;
                    var top = (screen.height - height) / 2;        
                    var properties = "location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes," 
                            + "scrollbars=yes,width="+width+",height="+height+",top="+top+",left="+left;

                    window.open(url, name, properties);    

  
                   }
    
        );
        
                            
        }
    
    //-->
    </script>
        <script type="text/javascript">
        function showPopupWindow(url) {
            var width = 800;
            var height = 600;
            var left = (screen.width - width) / 2;
            var top = (screen.height - height) / 2;
            var name = "viewReferences";
            var properties = "location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes," 
                + "scrollbars=yes,width="+width+",height="+height+",top="+top+",left="+left;

            window.open(url, name, properties);
        }
    </script>    
</body>
</f:view>
</html>    