<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Password Maintenance</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Password Maintenance
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Password Maintenance</h1>
			<div id="registered">

				<h:form id="updatePasswordForm">
				<table cellspacing="0">
				  <tr>
				    <td>Old Password</td>
				    <td class="fields">
				    	<h:inputSecret id="oldPassword" binding="#{passwordMaintenanceBean.oldPassword}" styleClass="register_field"/>
				    </td>
				  </tr>
				  <tr>
				    <td>New Password</td>
				
				    <td class="fields">
				    	<h:inputSecret binding="#{passwordMaintenanceBean.newPassword}" styleClass="register_field"/>
						<p class="note">Password must be between 6 and 9 characters long.</p>
					</td>
				  </tr>
				  <tr>
				    <td nowrap="nowrap">Confirm New Password</td>
				    <td>
				    	<h:inputSecret binding="#{passwordMaintenanceBean.confirmPassword}" styleClass="register_field"/>
				    </td>
				  </tr>
				</table>
				
				<!-- end registration fields -->
				<table cellspacing="0" class="registered_buttons">
					<tr>
						<td>
							<h:inputHidden id="passwordValidator" value="C2" validator="#{passwordMaintenanceBean.validatePassword}"/>
							<h:commandButton styleClass="button" action="home" immediate="true" value="Cancel"/>
							&nbsp;<h:commandButton id="submit_button" styleClass="button" actionListener="#{passwordMaintenanceBean.updatePassword}" action="#{passwordMaintenanceBean.updatePasswordNav}" value="Save"/>
							<script>
								document.forms['updatePasswordForm'].onkeypress=new Function("{var keycode;if (window.event) keycode = window.event.keyCode;else if (event) keycode = event.which;else return true;if (keycode == 13) { document.getElementById('updatePasswordForm:submit_button').click();return false; } else  return true; }");
							</script>
						</td>
					</tr>
				</table>
				<br/>
				
				<p class="alert"><h:message for="passwordValidator"/><h:message for="oldPassword" /></p>
				
				</h:form>
			</div>
	
		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>