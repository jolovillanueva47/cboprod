<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

	<head>
		<title>Login</title>
		<style type="text/css">
		<!--
			@import url(css/users_general.css);
			@import url(css/users_general_content.css);
		-->
		</style>
	</head>

	<body onload="init();">
	<div id="loading">Please wait, page is loading...<br /><img src="images/loading.gif"></div>	 
	<div id="main" style="display: none">
	<!-- Header page information -->
	
	<a name="top"></a>
	
	<div id="strip">
		<h1>CJO Production</h1>
		<img src="images/logo_6699CC_small.gif" />
	
		<!-- Crumbtrail -->


		<div id="location">

			<a>Home</a>
			&gt; Login
		</div>
  	
  	</div>
  	
	<table id="content" cellspacing="0"> 
		<tr>
			<td id="left">	
				<!-- Left bar -->

				<div id="welcome">
					<h2>Welcome</h2>
					<ul>
					<li>User: Guest</li>
					<li>
					<h:form id="loginForm" prependId="false">
						Username
						<h:inputText required="true" binding="#{loginBean.inputUsername}" id="username" styleClass="login_field" >
							<f:attribute name="requiredMessage" value="User is required."/>
						</h:inputText>
						Password
						<h:inputSecret maxlength="9" required="true" binding="#{loginBean.inputPassword}" id="password" validator="#{loginBean.validateDetails}" styleClass="login_field" >
							<f:attribute name="requiredMessage" value="Password is required."/>
						</h:inputSecret>
						<h:commandButton action="#{loginBean.login}" styleClass="button" value="Log in" />
					</h:form>
					</li>
					<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
						<li><p class="alert">
							<h:message for="username" />
							<h:panelGroup rendered="#{loginBean.messageSize eq 2}"><br/></h:panelGroup>
							<h:message for="password" />
							<h:message for="email" />
						</p></li>
					</h:panelGroup>
					</ul>
				</div>
			</td>

			<td id="centre">
				<!-- Body -->
				<h1>Cambridge eBooks Online - Production Module</h1>
				<p>Please log in the welcome panel on your left side. If you have forgotten your username or password, please enter your email below and your username and password will be sent to you.</p>
				<p>Please contact Rachael Kendall at <a href="mailto:rkendall@cambridge.org">rkendall@cambridge.org</a> if you still have any difficulties logging in.</p>
				<div id="registered">
					<h:form id="forgotPasswordForm" prependId="false">
						<table cellspacing="0">
	
							<tr class="border">
								<td>Email</td>
								<td class="fields">
									<h:inputText required="true" validator="#{loginBean.validateEmail}" binding="#{loginBean.inputEmail}" id="email" size="25">
										<f:attribute name="requiredMessage" value="Please enter Email Address."/>
									</h:inputText>
								</td>
							</tr>
						</table>
						<table cellspacing="0" class="registered_buttons">
							<tr>
							<td>
								<h:commandButton actionListener="#{loginBean.resetEmail}" immediate="true" value="Reset" styleClass="button"></h:commandButton>
								&nbsp;<h:commandButton actionListener="#{loginBean.emailLoginDetails}" value="Submit" styleClass="button"></h:commandButton>
							</td>
							</tr>
						</table>
					</h:form>
				</div>

			</td>
		</tr>

	</table>

	<f:subview id="footer_panel">
		<c:import url="components/footer.jsp"></c:import>
	</f:subview>
	
	</div>	<%-- end main --%>

		<script type="text/javascript">
		function init() {
			var loading = document.getElementById('loading');
			if ( loading != null ) { 
				loading.style.display = 'none';
			}
			var main = document.getElementById('main');
			if ( main != null ) {
				main.style.display = 'block';
			}
		}
		</script>
		


	</body>

</f:view>	

</html>
