<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
</head>
<body>
	<form action="testCounter">
		<h1>Counter Test</h1>
		<h2>Select Book</h2>	
		<select name="BOOK">
			<option value="9780511485985:Dictionaries in Early Modern Europe">Dictionaries in Early Modern Europe</option>
			<option value="9780511486142:Theatre, Society and the Nation">Theatre, Society and the Nation</option>
			<option value="9780511481413:The Ancient Messenians">The Ancient Messenians</option>
		</select>
		<h2>Select Service</h2>	
		<select name="SERVICE">
			<option value="Service 1">Service 1</option>
			<option value="Service 2">Service 2</option>
			<option value="Service 3">Service 3</option>
		</select>
		<h2>Body Id</h2>
		<input type="text" name="BODY_ID" />
		<h2>Is Search</h2>
		<select name="IS_SEARCH">
			<option value="Y">YES</option>			
			<option value="N">NO</option>
		</select>
		<hr />
		<h2>Report 1 - Book Request Month and Title</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP1" />
		<input type="submit" value="Submit1" name="SUBMIT"/>
		
		<h2>Report 2 - Book Section Request Month and Title</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP2" />
		<input type="submit" value="Submit2" name="SUBMIT"/>
		
		<h2>Report 3 - Turnaways by Month and Title</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP3" />
		<input type="submit" value="Submit3" name="SUBMIT"/>
		
		<h2>Report 4 - Turnaways by Month and Service</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP4" />
		<input type="submit" value="Submit4" name="SUBMIT"/>
		
		<h2>Report 5 - Search Sessions by Month and Title</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP5" />				
		<input type="submit" value="Submit5" name="SUBMIT"/>		
		
		<h2>Report 6 - Search Sessions by Month and Service</h2>
		<h3>Timestamp</h3>
		<input type="text" name="TIMESTAMP6" />
		<input type="submit" value="Submit6" name="SUBMIT"/>
	</form>	
</body>
</html>

