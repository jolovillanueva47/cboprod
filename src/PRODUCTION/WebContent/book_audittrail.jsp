<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>


<f:view>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Content Trail</title>
<style type="text/css">
	<!--
	@import url(css/users_general.css);
	@import url(css/users_general_content.css);
	-->
</style>
</head>

<body>

<!-- Left header page information -->

<a name="top"></a>

<div id="strip">
	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview> <!-- Crumbtrail -->

	<div id="location">
		<a href="home.jsf">Home</a> 
		&gt; Content Audit Trail
	</div>

</div>


<!-- Menu -->
<f:subview id="menu_panel">
	<c:import url="components/navigation.jsp"></c:import>
</f:subview>

<h:form id="bookAuditTrailForm">
	<!-- main content -->

	<table id="content" cellspacing="0">
	<tbody>
		<tr>
			<td id="centre"><div id="browse_ebooks">
			<!-- Body -->
			<h1>Content Audit Trail</h1>
			<div id="browse_tabs">
			<ul>
				<li>
					<h:commandLink action="ebook_audittrail" actionListener="#{ebookAuditTrailBean.initEBookAuditList}">
						<h:outputText value="By Book"></h:outputText>
					</h:commandLink>
				</li>
				<li><a href="#" class="selected">By Content</a></li>
				<li>
					<h:commandLink action="user_audittrail" actionListener="#{userAuditTrailBean.initUserAuditList}">
						<h:outputText value="By User"></h:outputText>
					</h:commandLink>
				</li>
			</ul>
			</div>
			<table border="0" cellspacing="1" cellpadding="3" id="ebooks_metadata">
				<tr>
					<th width="20%" align="right"><b>EISBN</b></th>
					<td width="80%">
						<h:inputText binding="#{bookAuditTrailBean.eisbnUI}" id="eisbnUI" styleClass="login_field" />
					</td>
				</tr>
				<tr>
					<th width="20%" align="right"><b>Content Id</b></th>
					<td width="80%">
						<h:inputText binding="#{bookAuditTrailBean.contentIdUI}" id="contentIdUI" styleClass="login_field" />
					</td>
				</tr>
				<tr>
					<th width="20%" align="right"><b>Series Code</b></th>
					<td width="80%">
						<h:inputText binding="#{bookAuditTrailBean.seriesCodeUI}" id="seriesCodeUI" styleClass="login_field" />
					</td>
				</tr>
				<tr>
					<th width="20%" align="right" ><b>User Name</b></th>
					<td>
						<h:inputText binding="#{bookAuditTrailBean.userNameUI}" id="userNameUI" styleClass="login_field" />
					</td>
				</tr>
				<tr>
					<th width="20%" align="right"><b>Status</b></th>
					<td>
						<h:selectOneMenu binding="#{bookAuditTrailBean.statusUI}">
							<f:selectItem itemLabel="" itemValue="" />
							<f:selectItem itemLabel="Not yet Loaded" itemValue="-1" />
							<f:selectItem itemLabel="Loaded for Proofreading" itemValue="0" />
							<f:selectItem itemLabel="Reloaded with Corrections" itemValue="1" />
							<f:selectItem itemLabel="Proofread Accepted" itemValue="2" />
							<f:selectItem itemLabel="Proofread with Error" itemValue="3" />
							<f:selectItem itemLabel="Approved" itemValue="4" />
							<f:selectItem itemLabel="Reviewed with Error" itemValue="5" />
							<f:selectItem itemLabel="Reloaded for Approver" itemValue="6" />
							<f:selectItem itemLabel="Content Signed Off" itemValue="7" />
							<f:selectItem itemLabel="Correction Required" itemValue="8" />
							<f:selectItem itemLabel="Embargo" itemValue="9" />
							<f:selectItem itemLabel="Return to Approver" itemValue="10" />
							<f:selectItem itemLabel="Return to Uploader" itemValue="11" />							
							<f:selectItem itemLabel="Deleted" itemValue="100" />
						</h:selectOneMenu>
					</td>
				</tr>
				<tr>
					<th width="20%" align="right"><b>Audit Date From</b></th>
					<td>
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateFromDay}">
							<f:selectItems value="#{bookAuditTrailBean.dayItems}"/>
						</h:selectOneMenu>
						&nbsp;
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateFromMonth}">
							<f:selectItems value="#{bookAuditTrailBean.monthMap}"/>
						</h:selectOneMenu>
						&nbsp;
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateFromYear}">
							<f:selectItems value="#{bookAuditTrailBean.yearItems}"/>
						</h:selectOneMenu>
					</td>
				</tr>
				<tr>
					<th width="20%" align="right"><b>Audit Date To</b></th>
					<td>
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateToDay}">
							<f:selectItems value="#{bookAuditTrailBean.dayItems}"/>
						</h:selectOneMenu>
						&nbsp;
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateToMonth}">
							<f:selectItems value="#{bookAuditTrailBean.monthMap}"/>
						</h:selectOneMenu>
						&nbsp;
						<h:selectOneMenu value="#{bookAuditTrailBean.auditDateToYear}">
							<f:selectItems value="#{bookAuditTrailBean.yearItems}"/>
						</h:selectOneMenu>				
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h:commandButton id="iconquicksearchbutton" actionListener="#{bookAuditTrailBean.updateBookAuditList}" action="#{bookAuditTrailBean.updateBookAuditTrail}" styleClass="button" value="Search"/>
						<script>document.forms['bookAuditTrailForm'].onkeypress =new Function("{var keycode;if (window.event) keycode = window.event.keyCode;else if (event) keycode = event.which;else return true;if (keycode == 13) { document.getElementById('bookAuditTrailForm:iconquicksearchbutton').click();return false; } else  return true; }");</script> 
					</td>
				</tr>
			</table>
			</div>
		<c:if test="${bookAuditTrailBean.bookAuditList != null}">
			<c:if test="${not empty bookAuditTrailBean.bookAuditList}">
			<div id="registered">
			<table cellspacing="0">
				<tr class="border">
					<td><strong>EISBN</strong></td>
					<td><strong>Content Id</strong></td>
					<td><strong>Series Code</strong></td>
					<td><strong>Type</strong></td>
					<td><strong>User Name</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Audit Date</strong></td>
					<td><strong>Remarks</strong></td>
				</tr>	
				
				<c:set var="warning" value="Warning: Content should not have been reloaded."/>
				<c:forEach var="auditTrail" items="${bookAuditTrailBean.bookAuditList}">
					<tr class="border">
						<td>${auditTrail.eisbn}&nbsp;</td>
						<td>${auditTrail.contentId}&nbsp;</td>
						<td>${auditTrail.seriesCode}&nbsp;</td>
						<td>${auditTrail.loadType}&nbsp;</td>
						<td>${auditTrail.username}&nbsp;</td>
						<td>
							${auditTrail.status}&nbsp;
						</td>
						<td>
							${auditTrail.auditDate}&nbsp;
						</td>
						<td>
						<c:if test="${warning == auditTrail.remarks}">
							<img src="images/warning.gif" alt="Warning" />
						</c:if>
						${auditTrail.remarks}&nbsp;
						</td>					
					</tr>
				</c:forEach>
			</table>
			</div>				
		</c:if>
		<c:if test="${empty bookAuditTrailBean.bookAuditList}">
			<table cellspacing="0">		
				<tr class="border">
					<td colspan="2"><strong>No audit trail was generated.</strong></td>
				</tr>
			</table>
		</c:if>
	</c:if>
			
			</td>
		</tr>
	</tbody>
	</table>
</h:form>

	<f:subview id="footer_panel">
		<c:import url="components/footer.jsp"></c:import>
	</f:subview>
	</body>
</f:view>
</html>