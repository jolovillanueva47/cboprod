<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.util.HttpUtil"%>
<%@page import="org.cambridge.ebooks.production.document.EbookChapterDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean"%>
<%@page import="org.cambridge.ebooks.production.ebook.content.EBookContentBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />		

	<title>View ${param.SHOW}</title>

	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
	<!--[if IE 8]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 6]>
        <link href="css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->

</head>

<body >

	<a name="top"></a>
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a> </div>		
		<h1 >eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />			
	</div>	
	<h:form id="details_popup_form" prependId="false">
	<table id="content" cellspacing="0"> 
		<tbody>
			<tr>
				<td id="centre">
					<h1>${param.SHOW}</h1>
					<div id="browse_ebooks">
							<!-- metadata  -->	
							<%if(EbookDetailsBean.META.equals(request.getParameter(EbookDetailsBean.SHOW))){%>
							<h3 id="chapter_title">
							<strong><c:out value="${ebookBean.mainTitle}" escapeXml="false"/></strong>
							</h3>					
							<f:subview id="metadata">	
								<jsp:include  page="ebook_details_content.jsp" flush="true"></jsp:include>								
							</f:subview>
							<div id="backtotop">
								<a href="#top" >
									back to top 
									<img border="" src="images/browse_ebooks_backtotop.gif" />
								</a>
							</div>
							<%}%>	
					
							
								<f:subview id="content_items" >
									<jsp:include page="ebook_contents.jsp" flush="true"></jsp:include>
								</f:subview>
								
								<div align="left" id="book_content"></div>
								
								
								<div id="submit_link">
									<div id="backtotop"><a href="javascript:void(0);" id="scroll_below" >more</a></div>
								</div>
								
												
						
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<input type="hidden" id="current_page" value="1" />
	</h:form>
	
	
	<script type="text/javascript" src="js/imageError.js" ></script>
	<script type="text/javascript" src="js/jquery-1.3.2.js"></script>	
	
	<script type="text/javascript">
   // we will add our javascript code here          
 
	//$(document).ready(function(){

		var alreadyClicked = false;
		
		$("#scroll_below").click(function(){

			if(alreadyClicked) 
				return false;

			alreadyClicked = true;
			
			$("#details_popup_form").submit();
		});
		
		$("#details_popup_form").submit(function(){
			
			$.ajax({
				type: "GET",
				url: "ebook_contents.jsf",
				data: "SHOW=${param.SHOW}&bookId=${param.bookId}&page=" + $("#current_page").val(),
				success: function(msg){
					
					if(msg.indexOf('LAST_CONTENT') != -1)
						$("#submit_link").html('');
					
					var newVal = $("#book_content").html() + msg; 
					$("#book_content").html(newVal);

					var current = parseInt($("#current_page").val()) + 1; 
					$("#current_page").val(current);

					alreadyClicked = false;

				}
			});

			return false;
 
		});
 
	//});

 </script>  
	
	
	
	<script type="text/javascript">
		function openWindow(url) { //v2.0		
			var h = window.open(url,'','location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes,scrollbars=yes,width=700,height=440');
			h.focus();
		}
	</script>
		
	
</body>
</f:view>
</html>

