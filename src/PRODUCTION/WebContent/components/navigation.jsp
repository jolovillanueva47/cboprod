<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
	
<h:form prependId="false" id="navigation_form">
	<div id="navigation">
		<ul>
			<li><h:commandLink action="home" actionListener="#{bookBean.resetBrowseOptions}" value="Browse eBooks"></h:commandLink></li>
			

			<c:if test="${userInfo.accessRights.canLoadData}">
				<li>| <a href="load_data.jsf">Load Data</a></li>
			</c:if>
			

			<%-->li>| <a href="citation_reports.jsf">Citation Reports</a></li--%>
			

			<c:if test="${userInfo.accessRights.canViewDtd}">
				<li>| <a href="view_dtd.jsf">View DTD</a></li>
			</c:if>
			

			<c:if test="${userInfo.accessRights.canManageUsers}">
				<li>| <h:commandLink actionListener="#{systemUserBean.initNewUserSearch}" action="system_users">System Users</h:commandLink></li>
			</c:if>
			

			<%-->li>| <a href="third_parties.jsf">Third Parties</a></li --%>
			
			<%-->li>| <a href="manual_delivery.jsf">Manual Delivery</a></li --%>
			

			<c:if test="${userInfo.accessRights.canViewAuditTrail}">
				<li>| <h:commandLink action="ebook_audittrail" actionListener="#{ebookAuditTrailBean.initEBookAuditList}"><h:outputText value="Audit Trail"></h:outputText></h:commandLink></li>
			</c:if>
			
			<%--
			<c:if test="${userInfo.accessRights.canManagePublishers}">
				<li>| <h:commandLink action="publishers" actionListener="#{publisherBean.initNewPublisherSearch}"><h:outputText value="Publishers"></h:outputText></h:commandLink></li>
			</c:if>

			c:if test="${userInfo.accessRights.canSearch}">
				<li>| <h:commandLink actionListener="#{searchBean.initNewSearch}" action="search_ebooks"><h:outputText value="Search"></h:outputText></h:commandLink></li>
			</c:if--%>
			
		</ul>
	</div>
</h:form>