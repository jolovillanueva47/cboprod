<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<h:form>
	<div id="welcome">
		<h2>Welcome</h2>
		<ul>
			<li>User: ${userInfo.lastName}</li>
			<li><a href="password_maintenance.jsf">Change password</a> | <h:commandLink action="#{logoutBean.logout}">Log out</h:commandLink></li>
		</ul>
	</div>
	
	<h1 id="cboh1">Cambridge Books Online</h1>
	<img src="images/logo_6699CC_small.gif" alt="Cambridge Books Online" border="0">
</h:form>