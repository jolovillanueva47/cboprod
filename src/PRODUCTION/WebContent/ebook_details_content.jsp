

<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.util.HttpUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

			<div id="jacket_covers">
				<p>
					<strong>Standard Cover Image:</strong>
					<br />
					<img alt="${ebookBean.standardImage}" src="${ebookBean.standardImage}" onerror="imageError(this, '<%=EbookDetailsBean.CORRUPT_COVER_IMAGE %>')" border="1" />
					<br />
					<br />					
					<strong>Thumbnail Cover Image:</strong>
					<br />
					<img alt="${ebookBean.thumbImage}" src="${ebookBean.thumbImage}" onerror="imageError(this, '<%=EbookDetailsBean.CORRUPT_COVER_THUMBNAIL %>')" border="1" />
				</p>
			</div>
			<div id="browse_contents" >
				<table cellspacing="1" cellpadding="3">
					<tr>
						<th width="20%" align="right" valign="top">Book ID:</th>
						<td width="80%"><h:outputText value="#{ebookBean.bookId}" /></td>
					</tr>
					<tr>
						<th width="20%" align="right" valign="top">Main title:</th>
						<td width="80%"><h:outputText value="#{ebookBean.mainTitle}" escape="false" />&nbsp;(<i><h:outputText value="#{ebookBean.mainTitleAlphaSort}" escape="false" /></i>)</td>
					</tr>					
					<tr>
						<th align="right" valign="top">Group:</th>
						<td><h:outputText value="#{ebookBean.bookGroup}" /></td>
					</tr>
					<h:panelGroup rendered="#{ebookBean.displaySubtitle}">
						<tr>
							<th align="right" valign="top">Subtitle:</th>
							<td><h:outputText value="#{ebookBean.subTitle}" escape="false"/></td>
						</tr>
					</h:panelGroup>					
					<h:panelGroup rendered="#{ebookBean.displayTransTitle}">
						<tr>
							<th align="right" valign="top">Trans-title:</th>
							<td><h:outputText value="#{ebookBean.transTitle}" /></td>
						</tr>
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayEdition}">
						<tr>
							<th align="right" valign="top">Edition:</th>
							<td>
								<h:outputText value="#{ebookBean.edition}" escape="false" />
							</td> 
						</tr>		
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayVolumeTitle}">
						<tr>
							<th align="right" valign="top">Volume title:</th>
							<td>
								<h:outputText value="#{ebookBean.volumeTitle}" escape="false" />
							</td> 
						</tr>		
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayVolumeNumber}">
						<tr>
							<th align="right" valign="top">Volume number:</th>
							<td>
								<h:outputText value="#{ebookBean.volumeNumber}" escape="false" />
							</td> 
						</tr>		
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayPartTitle}">
						<tr>
							<th align="right" valign="top">Part title:</th>
							<td>
								<h:outputText value="#{ebookBean.partTitle}" escape="false" />
							</td> 
						</tr>		
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayPartNumber}">
						<tr>
							<th align="right" valign="top">Part number:</th>
							<td>
								<h:outputText value="#{ebookBean.partNumber}" escape="false" />
							</td> 
						</tr>		
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayAuthor}">
						<tr>
							<th align="right" valign="top">Author(s):</td>
						</tr>			
						<c:set scope="request" var="i" value="1" />		
						<c:forEach items="${ebookBean.authorAffList}" var="author" >				 
							<tr>
								<td align="right" valign="top"><h:panelGroup rendered="#{ebookBean.displayAuthorRole}"><c:out value="${author.role}" escapeXml="false"/>:</h:panelGroup>											
								</th>
								<td><c:out value="${author.author}" escapeXml="false" /><c:if test="${author.displayAuthorAlphasort}"> (<i><c:out value="${author.authorAlphasort}" escapeXml="false" /></i>)</c:if><c:if test="${author.displayAffiliation}">, <c:out value="${author.affiliation}" escapeXml="false" /></c:if> 
								</td> 
							</tr>	
							<c:set scope="request" var="i" value="${i+1}" />																									 						
						</c:forEach>																									
					</h:panelGroup>	
					<tr>
						<th align="right" valign="top">DOI:</th>
						<td><h:outputText value="#{ebookBean.doi}" /></td>
					</tr>	
					<tr>
						<th align="right" valign="top">E-ISBN:</th>
						<td><h:outputText value="#{ebookBean.eisbn}" /></td>
					</tr>				
					<h:panelGroup rendered="#{ebookBean.displayAltIsbn}">	
						<h:panelGroup rendered="#{ebookBean.displayPaperback}">
							<tr>
								<th align="right" valign="top">Paperback:</th>
								<td><h:outputText value="#{ebookBean.paperback}" escape="false" /></td>
							</tr>
						</h:panelGroup>
						<h:panelGroup rendered="#{ebookBean.displayHardback}">
							<tr>
								<th align="right" valign="top">Hardback:</th>
								<td><h:outputText value="#{ebookBean.hardback}" escape="false" /></td>
							</tr>
						</h:panelGroup>
						<h:panelGroup rendered="#{ebookBean.displayOther}">
							<tr>
								<th align="right" valign="top">Other:</th>
								<td><h:outputText value="#{ebookBean.other}" escape="false" /></td>
							</tr>
						</h:panelGroup>
					</h:panelGroup>					
					<h:panelGroup rendered="#{ebookBean.displayVolume}">
						<tr>
							<th align="right" valign="top">Volume:</th>
							<td><h:outputText value="#{ebookBean.volume}" /></td>
						</tr>
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displayOtherVolume}">
						<tr>
							<th align="right" valign="top">Other Volume:</th>
							<td><h:outputText value="#{ebookBean.otherVolume}" /></td>
						</tr>
					</h:panelGroup>
					<h:panelGroup rendered="#{ebookBean.displaySeries}">
						<tr>
							<th align="right" valign="top">Series Code, Series<h:panelGroup rendered="#{ebookBean.displaySeriesPart}">, SeriesPart</h:panelGroup>:</th>
							<td><c:out value="${ebookBean.seriesCode}" escapeXml="false"/>, <c:out value="${ebookBean.series}" escapeXml="false"/> (<i><c:out value="${ebookBean.seriesAlphasort}" escapeXml="false" /></i>)
								<h:panelGroup rendered="#{ebookBean.displaySeriesPart}">
									, <c:out value="${ebookBean.seriesPart}" escapeXml="false"/>
								</h:panelGroup>  
							</td>
						</tr>
						<h:panelGroup rendered="#{ebookBean.displaySeriesNumber}">
							<tr>
								<th align="right" valign="top">Series Number:</th>
								<td><h:outputText value="#{ebookBean.seriesNumber}" /></td>
							</tr>
						</h:panelGroup>
						<h:panelGroup rendered="#{ebookBean.displaySeriesPosition}">
							<tr>
								<th align="right" valign="top">Series Position:</th>
								<td><h:outputText value="#{ebookBean.seriesPosition}" /></td>
							</tr>
						</h:panelGroup>
						
						
					</h:panelGroup>
					<c:choose>
						<c:when test="${fn:length(ebookBean.pubNameList) == 1}"> 
							<tr>
								<th align="right" valign="top">Publisher Name:</th>
								<td><c:out value="${ebookBean.pubNameList[0].name}" escapeXml="false"/></td>
							</tr>
							<tr>
								<th align="right" valign="top">Publisher Location:</th>
								<td><c:out value="${ebookBean.pubLocList[0].location}" escapeXml="false"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<th align="right" valign="top">Publisher(s):</th>
							</tr>
							<c:set scope="request" var="i" value="0"></c:set>
							<c:forEach items="${ebookBean.pubNameList}" var="pubName">
								<tr>
									<td align="right" valign="top">Publisher <c:out value="${i+1}" escapeXml="false"/>:</th>
									<td><c:out value="${pubName.name}" escapeXml="false"/> - <c:out value="${ebookBean.pubLocList[i].location}" escapeXml="false"/></td>									
								</tr>
								<c:set scope="request" var="i" value="${i+1}"></c:set>
							</c:forEach>							
						</c:otherwise>
					</c:choose>
					<tr>
						<th align="right" valign="top">Print Date:</th>
						<td><h:outputText value="#{ebookBean.printDate}" escape="false"/></td>
					</tr>
					<tr>
						<th align="right" valign="top">Online Date:</th>
						<td><h:outputText value="#{ebookBean.onlineDate}" escape="false"/></td>
					</tr>
					<tr>
						<th align="right" valign="top">Copyright Statement:</th>
						<td><h:outputText value="#{ebookBean.copyrightStatement}" escape="false" /></td>
					</tr>
					
					<tr>
						<th align="right" valign="top">Publisher Code:</th>
						<td><h:outputText value="#{ebookBean.publisherCode}" escape="false" /></td>
					</tr>
					<tr>
						<th align="right" valign="top">Product Code:</th>
						<td><h:outputText value="#{ebookBean.productCode}" escape="false" /></td>
					</tr>
					
					
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th align="left" colspan="2">Subject(s):</th>
					</tr>
					<tr>						
						<td colspan="2">
							<c:forEach items="${ebookBean.subjectList}" var="subject">
								<c:out value="${subject.code}" escapeXml="false"/>, <c:out value="${subject.subject}" escapeXml="false"/><br />
							</c:forEach>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th align="left" colspan="2">Book Blurb:</th>
					</tr>
					<tr>
						<td colspan="2"><p><h:outputText escape="false" value="#{ebookBean.blurb}" /></p></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th align="right" valign="top">Pages:</th>
						<td><h:outputText value="#{ebookBean.pages}" /></td>
					</tr>					
				</table>				
			</div>