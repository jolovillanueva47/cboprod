<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>


		<style type="text/css">
			span.padding
			{
			padding-left:200px;
			}
		</style>


		<h3 id="chapter_title">
			<strong>${chapterBean.title}</strong>
			(<em>${chapterBean.titleAlphasort}</em>)
		</h3>

				<table id="ebooks_metadata" cellspacing="1" cellpadding="3" >				 
				<!-- 
					Note to Karl
					I used jstl in this page in order for the view meta data to work
					(display all contents of all chapters) 
				-->
					
					
					<tr>
						<th align="right" width="20%" valign="top">type:</th>
						<td width="80%"><c:out value="${chapterBean.contentType}" /></td>
					</tr>	
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th align="right" width="20%" valign="top">ID:</th>
						<td width="80%"><c:out value="${chapterBean.contentId}" /></td>
					</tr>
					<c:if test="${chapterBean.displayDOI}" >
						<tr>
							<th align="right" valign="top">DOI:</th>
							<td><c:out value="${chapterBean.doi}" /></td>						
						</tr>				
					</c:if>
					<c:choose>
						<tr>
							<th align="right" valign="top">PDF Filename:</th>
							<td>
							<c:when test="${chapterBean.displayPdfFilename}" >							
								<%-- <a href="#" onclick="openWindow('../content/${bookContentBean.eisbn}/${chapterBean.pdfFilename}');return false;"> --%>			
								<a target='_blank' href='../content/${chapterBean.isbnBreakdown}/${chapterBean.pdfFilename}'>
									<c:out value="${chapterBean.pdfFilename}" />
								</a>
							</c:when>
							<c:when	test="${!chapterBean.displayPdfFilename}" >
								<c:out value="${chapterBean.pdfFilename}" />
							</c:when>					
							</td>
						</tr>
					</c:choose>			
										
					<c:if test="${chapterBean.displayHeadingLabel}" >						
						<tr>
							<th align="right" valign="top">Label:</th>
							<!-- PID 86837 -->
							<td><c:out value="${chapterBean.headingLabel}" escapeXml="false" /></td> 
						</tr>		
					</c:if>					
					<tr>
						<th align="right" valign="top">Title:</th>
						<td><h:outputText value="#{chapterBean.headingTitle}" escape="false" /></td>
						<!-- 
						<td><c:out value="${chapterBean.headingTitle}" escapeXml="false" /></td>
						 -->
					</tr>						
					<c:if test="${chapterBean.displayHeadingSubtitle}" >						
						<tr>
							<th align="right" valign="top">Subtitle:</th>
							<td><c:out value="${chapterBean.headingSubtitle}" escapeXml="false" /></td> 
						</tr>		
					</c:if>
										
					<c:if test="${chapterBean.displayContributorName}" >
						<tr>
							<th align="right" valign="top">Contributor Name:</th>
							<td><c:out value="${chapterBean.contributorName}" escapeXml="false" /></td>
						</tr>					
					</c:if>
					<c:if test="${chapterBean.displayContributorCollab}" >
						<tr>
							<th align="right" valign="top">Contributor Collab:</th>
							<td><c:out value="${chapterBean.contributorCollab}" escapeXml="false" /></td>
						</tr>					
					</c:if>			
					<c:if test="${chapterBean.displayContributorAffiliation}" >
						<tr>
							<th align="right" valign="top">Contributor Affiliation:</th>
							<td><c:out value="${chapterBean.contributorAffiliation}" escapeXml="false" /></td>
						</tr>			
					</c:if>	
					<c:if test="${chapterBean.displaySubjectGroup}" >
						<tr>
							<th align="right" valign="top">Subject Group:</th>
							<td><c:out value="${chapterBean.subjectGroup}" /></td>
						</tr>
					</c:if>
					<c:if test="${chapterBean.displayThemeGroup}" >
						<tr>
							<th align="right" valign="top">Theme Group:</th>
							<td><c:out value="${chapterBean.themeGroup}" /></td>
						</tr>
					</c:if>	
					<%-- 				
					<c:if test="${chapterBean.displayKeyword}" >
						<tr>
							<th align="right" valign="top">Keyword(s):</th>
							<td><c:out value="${chapterBean.keyword}" /></td>
						</tr>
					</c:if>
					 --%>					
					<c:if test="${chapterBean.displayToc}">
						<tr>
							<th align="right" valign="top">TOC:</th>
							<td><c:out value="${chapterBean.toc}" escapeXml="false"/></td>
						</tr>
					</c:if>			
					<tr><td>&nbsp;</td></tr>
					<c:if test="${chapterBean.displayPages}" >						
						<tr>
							<th align="right" valign="top">Pages:</th>
							<td>
								<c:out value="${chapterBean.pageStart}" />&nbsp;-
								<c:out value="${chapterBean.pageEnd}" />
							</td>
						</tr>	
					</c:if>							
					
					<!-- MAPS AND PLATES -->
					<c:if test="${chapterBean.displayEBookInsertBeansList}" >
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td colspan="2">
								<p>
								<b><span class="padding">Additional Material:</span></b><br/><br/>
								<c:forEach var="mapsandplates" items="${chapterBean.ebookInsertBeansList}" >
									<span class="padding"><b>Title:</b> <c:out value="${mapsandplates.insertTitle}" escapeXml="false" />&nbsp;<i>(<c:out value="${mapsandplates.insertAlphasortTitle}" /></i>)</span><br/>
									<span class="padding"><b>PDF Filename:</b>
										<a style="text-decoration: none" target='_blank' href='../content/${mapsandplates.isbnBreakdown}/${mapsandplates.insertFilename}'>
											<b><c:out value="${mapsandplates.insertFilename}" /></b>
										</a>
									</span><br/><br/>
								</c:forEach>
								</p>
							</td>
						</tr>
					</c:if>
					<!-- MAPS AND PLATES END -->
					
					
					<!-- abstract content -->
					<c:if test="${chapterBean.displayAbstractContent}">
						<tr><td>&nbsp;</td></tr>
						<tr>
							<th colspan="2" align="left" valign="top">
							Abstract: 
							<c:if test="${chapterBean.displayProblem}">
								Problem = <c:out value="${chapterBean.problem}" />
							</c:if>
							</th>
						</tr>						
						<tr>							
							<td colspan="2"><c:out value="${chapterBean.abstractContent}" escapeXml="false" /></td>
						</tr>
						<tr><td>&nbsp;</td></tr>	
						<tr>							
							<td colspan="2" align="center">
								<img src="${chapterBean.abstractImage}" alt="${chapterBean.abstractImage}"		onerror="imageError(this, '<%=EbookDetailsBean.CORRUPT_COVER_IMAGE %>')" 
									border="1" />
							</td>
						</tr>					
					</c:if>
					

					
				</table>