<?php		
	require_once("settings.php");	
	$result = array();		
	$output = array();
	$error = 0;
	set_time_limit(0);
	
	try {
		$parts = pathinfo($_FILES['Filedata']['name']);
		$filename = basename($parts['basename'], '.'.$parts['extension']).'_'.date('YmdHis').".".$parts['extension'];
		$uploadPath = $config->path->upload;
		$unzipPath = $config->path->unzip;
		
		$logger->info("upload path:" . $uploadPath);
		$logger->info("unzip path:" . $unzipPath);
		$logger->info("file:" . $_FILES['Filedata']['name']);
		$logger->info("filename:" . $filename);
		$logger->info("temp file:" . $_FILES['Filedata']['tmp_name']);
		
		if(move_uploaded_file($_FILES['Filedata']['tmp_name'],$uploadPath.$filename)) {				
			$result["result"] = "success";
			$result["response"] = $parts['basename']." has been loaded to server.<br/>";	
			
			$command = "unzip -o ".$uploadPath.$filename." -d ".$unzipPath." 1>/dev/null 2>&1 &";
			$logger->info("command:" . $command);
			unset($output);
			exec($command, $output, $error);	
			
			if($error) {
				$result["result"] = "failed";
				$result["response"] = "An error occured while unzipping file. Zip file must be corrupted.<br/>";	
			}			
			
			$result["dummyFile"] = $uploadPath.$filename;	
		} else {	
			$result["result"] = "failed";
			$result["response"] = "There is an error uploading ".$parts['basename']."<br/>";		
		}
	} catch(Exception $e) {
		$result["result"] = "failed";
		$result["response"] = $e->getMessage();
	}
	// run indexer here	
	echo Zend_Json::encode($result);
?>