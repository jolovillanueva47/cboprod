<?php
	require_once("settings.php");
	$filename = basename($_REQUEST["filename"], ".zip");
	$validationTextPath = $config->path->unzip.$filename."/VALIDATION.txt";
	$logger->info("validationTextPath:" . $validationTextPath);
	if(file_exists("$validationTextPath")) {			
		echo (file_get_contents($validationTextPath));
		$fileBase = basename("$validationTextPath", ".txt");
		$filePath = dirname("$validationTextPath");
		$renamedFile = $filePath."/".$fileBase."_".date("YmdHis").".txt";
		$logger->info("renamedFile:" . $renamedFile);
		//echo "mv $validationTextPath $renamedFile";
		exec("mv $validationTextPath $renamedFile");
	}
?>