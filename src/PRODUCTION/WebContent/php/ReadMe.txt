Configuration with regards to the uploader (Involves php.ini).

post_max_size - max total amount size of files to be uploaded (maximum of 1999 Megabytes or 1.99 Gigabytes).
Any value greater than the said value will result to an error when uploading such files.
Example: 1999M or 1.9G

upload_max_filesize - max amount of a single file to be uploaded (maximum of 1999 Megabytes or 1.99 Gigabytes).
Any value greater than the said value will result to an error when uploading such files.
Example: 1999M or 1.9G

upload_tmp_dir - Need to set this to the path where the files will be uploaded for faster processing of files.
Example: /app/jboss014-4.2.3.JL/server/default/uploads/

// added .htaccess on /app/ebooks/uploads and /app/jboss-4.2.3.GA/server/default/deploy/production.war/php to prevent security error on uploader

--- Reference: http://digitarald.de/project/fancyupload/
Flash-request forgets cookies and session ID
 - Flash FileReference is not an intelligent upload class, 
   the request will not have the browser cookies, Flash saves his own cookies. 
   When you have sessions, append them as get-data to the the URL 
   (e.g. �upload.php?PHPSESSID=123456789abcdef�). Of course your session-name can be different.
   
   in upload.php: session_start(); 
				  session_name($_POST[session_name()]); 
				  // $_POST['param'] for post method; $_GET['param'] for get method				  

Are cross-domain uploads possible?
 - Forum solution, and FileReference docs: http://forum.mootools.net/viewtopic.php?id=8312
 - For uploading and downloading operations, 
   a SWF file can access files only within its own domain, 
   including any domains that are specified by a cross-domain policy file. 
   If the SWF that is initiating the upload or download doesn�t come 
   from the same domain as the file server, you must put a policy file on the file server. 
   
Uploads fail with 404 error code
 - Check your URL and better use an absolute upload URL.
   IE takes the upload url relative to the swf, all other browsers relative to the 
   html/current file. So the best solution is an absolute path for the option url 
   or rather the form action. If you have problems with failed upload and 404 error codes, 
   try an absolute url, in your form-action or url option when creating your FancyUpload instance.
   
Uploads fail with 406/403 error
 - From the swfupload documentation:
   If you are using Apache with mod_security this will not work, you need to put the following in your 
   .htaccess file to disable mod_security:
        SecFilterEngine Off 
        SecFilterScanPOST Off
   Disabling mod_security isn�t allowed on some shared hosts, and only do this if you know what 
   you are doing. This is due to a bug in the way that flash sends the headers back to the server 
   according to the Flash 8 documentation.
 
				  

   
 