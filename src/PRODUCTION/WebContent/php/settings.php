<?php 
// set include_path to library/ directory only
set_include_path(dirname(__FILE__).DIRECTORY_SEPARATOR.'library');

require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Stream.php';
require_once 'Zend/Config/Ini.php'; 
require_once 'Zend/Json.php';

$writer = new Zend_Log_Writer_Stream(dirname(__FILE__).DIRECTORY_SEPARATOR.'server_'.date("Ymd").'.log');
$logger = new Zend_Log();
$logger->addWriter($writer);

$server = isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : 'localhost';
$config = new Zend_Config_Ini(dirname(__FILE__).DIRECTORY_SEPARATOR.'config.ini', $server);

?>