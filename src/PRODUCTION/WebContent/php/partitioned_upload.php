<?php
require_once("settings.php");
$result = array();
//----------------------------------------------
//    partitioned upload file handler script
//----------------------------------------------

//
//    specify upload directory - storage 
//    for reconstructed uploaded files and for partitioned files
$upload_dir = $config->path->upload;

//
//    retrieve request parameters
$file_param_name = "file";
$file_name = $_FILES[$file_param_name]["name"];
$logger->info("file name:" . $file_name);
$file_id = $_POST["fileId"];
$logger->info("file id:" . $file_id);
$partition_index = $_POST["partitionIndex"];
$logger->info("partition index:" . $partition_index);
$partition_count = $_POST["partitionCount"];
$logger->info("partition count:" . $partition_count);
$file_length = $_POST["fileLength"];
$logger->info("file length:" . $file_length);

//
//    the $client_id is an essential variable, 
//    this is used to generate uploaded partitions file prefix, 
//    because we can not rely on 'fileId' uniqueness in a 
//    concurrent environment - 2 different clients (applets) 
//    may submit duplicate fileId. thus, this is responsibility 
//    of a server to distribute unique clientId values
//    (or other variable, for example this could be session id) 
//    for instantiated applets.
$client_id = $_GET["clientId"];

//
//    move uploaded partition to the staging folder 
//    using following name pattern:
//    ${clientId}.${fileId}.${partitionIndex}
$source_file_path = $_FILES[$file_param_name]["tmp_name"];
$logger->info("tmp path:" . $source_file_path);
$target_file_path = $upload_dir . $client_id . "." . $file_id . "." . $partition_index;
$logger->info("target file path:" . $target_file_path);
if(!move_uploaded_file($source_file_path, $target_file_path)) {
    echo "Error: Can't move uploaded file";
    return;
}

//
//    check if we have collected all partitions properly
$all_in_place = true;
$partitions_length = 0;
for($i = 0; $all_in_place && $i < $partition_count; $i++) {
    $partition_file = $upload_dir . $client_id . "." . $file_id . "." . $i;
    if(file_exists($partition_file)) {
        $partitions_length += filesize($partition_file);
    } else {
        $all_in_place = false;
    }
}

//
//    issue error if last partition uploaded, but partitions validation failed
if($partition_index == $partition_count - 1 && (!$all_in_place || $partitions_length != intval($file_length))) {
    echo "Error: Upload validation error";
    return;
}

//
//    reconstruct original file if all ok
if($all_in_place) {
	//$newFilename = basename($file_name,".zip") . "_" . date('YmdHis') . ".zip"; // org.cambridge.ebooks.production.load.ZipContentValidationWorker now manages transfer of file...
	$newFilename = basename($file_name, ".zip") . "_" . $file_id . ".zip"; // attached file id to prevent zip validation corruption...
    $file = $upload_dir . $newFilename;
    $file_handle = fopen($file, 'a');
    for($i = 0; $all_in_place && $i < $partition_count; $i++) {
        //
        //    read partition file
        $partition_file = $upload_dir . $client_id . "." . $file_id . "." . $i;
        $partition_file_handle = fopen($partition_file, "rb");
        $contents = fread($partition_file_handle, filesize($partition_file));
        fclose($partition_file_handle);
        //
        //    write to reconstruct file
        fwrite($file_handle, $contents);
        //
        //    remove partition file
        unlink($partition_file);
    }
    fclose($file_handle);
	
	$result["dummyFile"] = $upload_dir . $newFilename;
	echo Zend_Json::encode($result);	
}
?>