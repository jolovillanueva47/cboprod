<?php	
	require_once("settings.php");
	
	$WRITE_LOCK_FILE = "/app/ebooks/bookindex/write.lock";
	$MANIFEST_FILE = "/app/ebooks/content/MANIFEST.txt";
	$LOCK_FILE = "/app/ebooks/lockfile";
	$jsonArr = array();
	
	try {
		$successWriteLockDelete = unlink($WRITE_LOCK_FILE);
		$successManifestFileDelete = unlink($MANIFEST_FILE);
		$successLockFile = unlink($LOCK_FILE);
		
		if($successWriteLockDelete) {
			$jsonArr["writelock_status"] = "deleted";
			$jsonArr["writelock_response"] = "write.lock has been deleted.\n";
		} else {
			$jsonArr["writelock_status"] = "failed";
			$jsonArr["writelock_response"] = "write.lock deletion failed.\n";
		}
		
		if($successManifestFileDelete) {
			$jsonArr["manifest_status"] = "deleted";
			$jsonArr["manifest_response"] = "MANIFEST.txt has been deleted.\n";
		} else {
			$jsonArr["manifest_status"] = "failed";
			$jsonArr["manifest_response"] = "MANIFEST.txt deletion failed.\n";
		}
		
		if($successLockFile) {
			$jsonArr["lockfile_status"] = "deleted";
			$jsonArr["lockfile_response"] = "lockfile has been deleted.\n";
		} else {
			$jsonArr["lockfile_status"] = "failed";
			$jsonArr["lockfile_response"] = "lockfile deletion failed.\n";
		}
		
	} catch(Exception $e) {
		$jsonArr["status"] = "exception";
		$jsonArr["response"] = "[full_indexer.php - Exception] " . $e->getMessage();
	}
	
	echo (Zend_Json::encode($jsonArr));
?>