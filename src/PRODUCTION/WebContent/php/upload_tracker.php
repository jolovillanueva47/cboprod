<?php
	// VARIABLES
	$htmlNewLine = "<br/>";	
	$uploadsDir = "/app/ebooks/uploads";
	$dirContents = scandir($uploadsDir);
	$htmlTab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	$dateToday = strtotime("today"); // current date with 0 time
	$currentDate = strtotime("now"); // current date with current time
	$currentContentCount = 0;
	$displayText = "";
	$specifiedStartDate = "";
	$specifiedEndDate = "";
	
	if($_REQUEST["start_date"] != null || $_REQUEST["start_date"] != "") {
		$specifiedStartDate = strtotime($_REQUEST["start_date"]);
	}
	
	if($_REQUEST["end_date"] != null || $_REQUEST["end_date"] != "") {
		$specifiedEndDate = strtotime($_REQUEST["end_date"]);
	}
	
	echo("=== UPLOAD TRACKER ===$htmlNewLine");
	echo("Current Date: " . date("F d Y H:i:s",strtotime("now")) . $htmlNewLine);	
	if(($specifiedStartDate != null || $specifiedStartDate != "") && ($specifiedEndDate != null || $specifiedEndDate != "")) {
		echo("Specified Start Date:" . date("F d Y H:i:s", $specifiedStartDate) . $htmlNewLine);
		echo("Specified End Date:" . date("F d Y H:i:s", $specifiedEndDate) . $htmlNewLine);
	}
	
	foreach($dirContents as $content) {
		$contentModTime = filemtime("$uploadsDir/$content");
		if($content == "." || $content == "..") {
			continue;
		}
		if(($specifiedStartDate != null || $specifiedStartDate != "") && ($specifiedEndDate != null || $specifiedEndDate != "")) {
			if($contentModTime >= $specifiedStartDate && $contentModTime <= $specifiedEndDate) {
				$currentContentCount++;
				if(strrpos($content, ".txt") > -1) {
					$displayText = $displayText . "uploaded file: $content | modification time: " . 
						date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
				} else if(strrpos($content, ".zip") > -1) {
					$displayText = $displayText . "to be extracted: $content | modification time: " . 
						date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
				} else {
					$displayText = $displayText . "chunk file: $content | modification time: " . 
						date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
				}
			}
		}
		if($contentModTime >= $dateToday && $contentModTime <= $currentDate) {
			$currentContentCount++;
			if(strrpos($content, ".txt") > -1) {
				$displayText = $displayText . "uploaded file: $content | modification time: " . 
					date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
			} else if(strrpos($content, ".zip") > -1) {
				$displayText = $displayText . "to be extracted: $content | modification time: " . 
					date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
			} else {
				$displayText = $displayText . "chunk file: $content | modification time: " . 
					date("F d Y H:i:s", filemtime("$uploadsDir/$content")) . $htmlNewLine;
			}
		}
	}
	
	echo("Content count: " . $currentContentCount . "$htmlNewLine$htmlNewLine");
	echo($displayText);
?>