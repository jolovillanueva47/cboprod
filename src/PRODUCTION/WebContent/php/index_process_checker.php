<?php
	require_once("settings.php");
	
	$LOCK_FILE = "/app/ebooks/bookindex/write.lock";
	$type = $_REQUEST["type"];
	$jsonArr = array();
	
	try {	
		if($type == "start") {
			if(file_exists($LOCK_FILE)){
				$jsonArr["status"] = "start";
				$jsonArr["response"] = "Start indexing...\n";
			} else {
				$jsonArr["status"] = "waiting";
				$jsonArr["response"] = "Waiting to start indexing...\n";
			}	
		} else if($type == "process") {	
			if(file_exists($LOCK_FILE)){
				$jsonArr["status"] = "processing";
				$jsonArr["response"] = "Still indexing...\n";
			} else {
				$jsonArr["status"] = "stop";
				$jsonArr["response"] = "Stop indexing...\n";
			}
		}
	} catch (Exception $e) {
		$jsonArr["status"] = "exception";
		$jsonArr["response"] = "[index_process_checker.php - Exception] " . $e->getMessage();
	}
	
	echo (Zend_Json::encode($jsonArr));
?>