<?php 
	require_once("settings.php");
	$jsonArr = array();
	
	try {	
		if (isset($_REQUEST["isbn"])) {
			$ISBNERROR_DIR = "/app/ebooks/isbnerrors/";
			$isbnList = split(",", $_REQUEST["isbn"]);
			foreach($isbnList as $isbn) {
				$success = mkdir($ISBNERROR_DIR . trim($isbn));
				if($success) {
					$jsonArr[$isbn] = "deleted";
				} else {
					$jsonArr[$isbn] = "failed";
				}
			}	
		}
	} catch (Exception $e) {
		$jsonArr["exception"] = "[per_book_indexer.php - Exception] " . $e->getMessage();
	}
	
	echo (Zend_Json::encode($jsonArr));
?>