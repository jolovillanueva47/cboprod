<?php
	require_once("settings.php");	
	
	$logger->info("------ Process checker ----");
	$isStillUnzipping = true;	
	$output = array();
	while($isStillUnzipping) {			
		$logger->info("start loop ---");	
		$command = "ps -efo args | grep unzip";
		unset($output);
		exec($command, $output);
		$logger->info("command:" . $command);	
		
		$matchString = "unzip -o ".dirname($_POST["dummyFile"]) . "/" . basename($_POST["dummyFile"], ".zip");
		if(in_array(substr($matchString, 0, 79), $output) === false) {
			$isStillUnzipping = false;				
		} else {
			$logger->info("still unzipping ---");	
		}
		
		$logger->info("end loop ---");	
	}
	
	echo ("Done unzipping. <br/>");
?>