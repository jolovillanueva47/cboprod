<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	
	<title>View Abstract</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>	
</head>

<body>	
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />			
	</div>
	<table id="content" cellspacing="0"> 
		<tbody>
			<tr>
				<td id="centre">
					<h1>[<h:outputText value="#{bookContentBean.ebook.isbn}"/>]&nbsp;<h:outputText value="#{bookContentBean.ebookChapter.title}"/></h1>
					
					<div id="registered">		
						<h:form>
							<table>
								<tr>
									<td>											
										<ul>
											<li>
												<span style="font-size: 12px">
												<h:panelGroup rendered="#{not empty sessionScope.bookContentBean.ebookChapter.abstractText}">
													<h:outputText value="#{sessionScope.bookContentBean.ebookChapter.abstractText}" />
												</h:panelGroup>
												<h:panelGroup rendered="#{empty sessionScope.bookContentBean.ebookChapter.abstractText}">
													<br/>&nbsp;No abstract.
												</h:panelGroup>												
												</span>
											</li>										
										</ul>
									</td>

								</tr>
								
								<h:panelGroup rendered="#{not empty sessionScope.bookContentBean.ebookChapter.abstractImage}">
									<tr>
										<td>											
											<ul>
												<li>	
													<h:graphicImage value="../content/#{sessionScope.bookContentBean.eisbn}/#{sessionScope.bookContentBean.ebookChapter.abstractImage}" alt="The image could not be found"/>													
												</li>										
											</ul>
										</td>									
									</tr>									
								</h:panelGroup>
							</table>
							
						</h:form>		
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>
</body>

</f:view>

</html>