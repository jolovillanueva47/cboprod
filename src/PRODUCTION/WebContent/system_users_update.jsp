
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
 
<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Update User Details</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; <a href="system_users.jsf">Manage System Users</a>
		&gt; Update User Details

	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Update User Details</h1>
			<div id="registered">
			
			<h:inputHidden id="alertMessage"/>
			<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
			<p class="alert">
				<h:message for="alertMessage"/>
				<h:message for="updateValidator"/><br/></p>
			</h:panelGroup>

			<h:form id="updateForm" prependId="false">
			<table cellspacing="0">
					<tr>
						<td>Username*</td>
						<td><h:inputText maxlength="24" binding="#{systemUserUpdateBean.uiUsername}" value="#{systemUserUpdateBean.user.username}" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Password*</td>
						<td><h:inputText maxlength="9" binding="#{systemUserUpdateBean.uiPassword}" value="#{systemUserUpdateBean.displayPassword}" styleClass="register_field" /> 6-9 alphanumeric characters only </td>
					</tr>
					<tr>
						<td>Confirm Password*</td>
						<td><h:inputText maxlength="9" binding="#{systemUserUpdateBean.uiConfirmPassword}" value="#{systemUserUpdateBean.displayPassword}" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Email Address*</td>
						<td><h:inputText maxlength="80" binding="#{systemUserUpdateBean.uiEmail}" value="#{systemUserUpdateBean.user.email}" styleClass="register_field" /></td>
					</tr>
					<tr>
			
						<td>First Name*</td>
						<td class="fields"><h:inputText maxlength="48" binding="#{systemUserUpdateBean.uiFirstName}" value="#{systemUserUpdateBean.user.firstName}" styleClass="register_field" /></td>
					</tr>
					<tr>
			  			<td>Last Name*</td>
			  			<td><h:inputText maxlength="48" binding="#{systemUserUpdateBean.uiLastName}" value="#{systemUserUpdateBean.user.lastName}" styleClass="register_field" /></td>
					</tr>
					<tr>
			
						<td>Office location / Department</td>
						<td><h:inputText maxlength="80" value="#{systemUserUpdateBean.user.department}" styleClass="register_field" /></td>
					</tr>
					
					<tr class="border">
						<td colspan="2"><h4>Default User Access Rights</h4></td>
					</tr>
					
					
					<%-->tr>
						<td>Load SPD</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canSPD}"/></td>
					</tr--%>
					
					<%-- 
					
					<tr>
						<td>Load Data</td>
						<td><h:selectBooleanCheckbox id="load_data" value="#{systemUserUpdateBean.user.accessRights.canLoadData}"/></td>
					</tr>
					
					<tr>
						<td>Proofread Content</td>
						<td><h:selectBooleanCheckbox id="proofread_content" value="#{systemUserUpdateBean.user.accessRights.canProofread}"/></td>
					</tr>
					
					--%>
					
					<tr>
						<td>Load Data</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canLoadData}"/></td>
					</tr>
					
					<tr>
						<td>Proofread Content</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canProofread}"/></td>
					</tr>
					
					
					<tr>
						<td>Approve Content</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canApprove}"/></td>
					</tr>
					
					<tr>
						<td>Publish Data</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canPublish}"/></td>
					</tr>
					
					<tr>
						<td>Deliver Data</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canDeliverData}"/></td>
					</tr>
					
					<%-->tr>
						<td>View Archive Data</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canViewArchive}"/></td>
					</tr--%>
					
					<%-->tr>
						<td>Run Reports</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canRunReports}"/></td>
					</tr--%>
					
					<%-->tr>
						<td>Citation Reports</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canCitationReports}"/></td>
					</tr--%>
					
					<tr>
						<td>Delete Data</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canDelete}"/></td>
					</tr>
					
					<%--tr>
						<td>Search</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canSearch}"/></td>
					</tr--%>
					
					<tr>
						<td nowrap="nowrap">Revert Status</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canRevertStatus}"/></td>
					</tr>
					
					
					<tr class="border">
						<td colspan="2"><h4>Production Administrator Access Rights</h4></td>
					</tr>
					
					
					<tr>
						<td nowrap="nowrap">View/Download DTD</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canViewDtd}"/></td>
					</tr>
					
					<tr>
						<td nowrap="nowrap">Manage System Users</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canManageUsers}"/></td>
					</tr>
					
					<%--tr>
						<td nowrap="nowrap">Manage Third Parties</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canManageThirdParties}"/></td>
			
					</tr--%>
					
					<%-->tr>
						<td nowrap="nowrap">Manage Proprietary Third Party</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canDeliveryChanges}"/></td>
					</tr--%>
					
					<tr>
						<td nowrap="nowrap">View Audit Trail</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canViewAuditTrail}"/></td>
					</tr>
					
					
					<%-- <tr>
						<td nowrap="nowrap">Manage Publishers</td>
						<td>
							<h:selectBooleanCheckbox id="manage_publisher" value="#{systemUserUpdateBean.user.accessRights.canManagePublishers}" binding="#{systemUserUpdateBean.uiCanManagePublishers}"/>
						</td>
					</tr>--%>
			
					<%--tr>
						<td nowrap="nowrap">Register DOI</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canRegisterDOI}"/></td>
					</tr--%>
					
					<%--tr>
						<td nowrap="nowrap">Publish Script</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canPublishScript}"/></td>
			
					</tr--%>
					
					<%--tr>
						<td nowrap="nowrap">Site Reload</td>
						<td><h:selectBooleanCheckbox value="#{systemUserUpdateBean.user.accessRights.canSiteReload}"/></td>
					</tr--%>
					
					<%-- 
					<c:if test="${empty systemUserUpdateBean.publisherBean.publisherList_Name}">
						<tr class="border">
							<td><h4>Publisher</h4></td>
							<td>${systemUserUpdateBean.publisherBean.publisher.name}</td>
						</tr>
					</c:if>
					
					
					<c:if test="${not empty systemUserUpdateBean.publisherBean.publisherList_Name}">
						<tr class="border">
							<td><h4>Publisher</h4></td>
							<td>
							<h:selectOneMenu id="select_publisher" binding="#{systemUserUpdateBean.uiPublisherId}" 
									value="#{systemUserUpdateBean.publisherId}" 
									onchange="document.getElementById('clearBookResultAndAccessList').onclick();">	
									<f:selectItems id="publisherNameJsf" value="#{systemUserUpdateBean.publisherBean.publisherList_Name}"/>
							</h:selectOneMenu>
							</td>
						</tr>
					</c:if>
					--%>

					<tr class="border">
						<td colspan="2"><h4>eBooks Access</h4></td>
					</tr>
					
					<tr>
						<td></td>
						<td>
							<table cellspacing="0">
								<tr>
									<td>E-ISBN</td>
									<td width="80%">
										<h:inputText binding="#{systemUserUpdateBean.uiAuditEisbn}" id="uiAuditEisbn" styleClass="login_field" />
									</td>
								</tr>
								<tr>
									<td>Series Id</td>
									<td width="80%">
										<h:inputText binding="#{systemUserUpdateBean.uiAuditSeriesId}" id="uiAuditSeriesId" styleClass="login_field" />
									</td>
								</tr>
								<tr>
									<td>User Name</td>
									<td>
										<h:inputText binding="#{systemUserUpdateBean.uiAuditUsername}" id="uiAuditUsername" styleClass="login_field" />
									</td>
								</tr>
								<tr>
									<td>Status</td>
									<td>
										<h:selectOneMenu id="chapterStatus" binding="#{systemUserUpdateBean.uiAuditStatus}">
											<f:selectItem itemLabel="" itemValue="" />
											<f:selectItem itemLabel="Not yet Loaded" itemValue="-1" />
											<f:selectItem itemLabel="Loaded for Proofreading" itemValue="0" />
											<f:selectItem itemLabel="Reloaded with Corrections" itemValue="1" />
											<f:selectItem itemLabel="Proofread Accepted" itemValue="2" />
											<f:selectItem itemLabel="Proofread with Error" itemValue="3" />
											<f:selectItem itemLabel="Approved" itemValue="4" />
											<f:selectItem itemLabel="Reviewed with Error" itemValue="5" />
											<f:selectItem itemLabel="Reloaded for Approver" itemValue="6" />
											<f:selectItem itemLabel="Content Signed Off" itemValue="7" />
											<f:selectItem itemLabel="Correction Required" itemValue="8" />
											<f:selectItem itemLabel="Embargo" itemValue="9" />
											<f:selectItem itemLabel="Deleted" itemValue="100" />
										</h:selectOneMenu>
									</td>
								</tr>
								<tr>
									<td>Audit Date From</td>
									<td>
										<h:selectOneMenu id="fromDayItems" binding="#{systemUserUpdateBean.uiAuditDateFromDay}" value="#{systemUserUpdateBean.auditDateFromDay}">
											<f:selectItems value="#{systemUserUpdateBean.dayItems}"/>
										</h:selectOneMenu>
										&nbsp;
										<h:selectOneMenu id="fromMonthItems" binding="#{systemUserUpdateBean.uiAuditDateFromMonth}" value="#{systemUserUpdateBean.auditDateFromMonth}">
											<f:selectItems value="#{systemUserUpdateBean.monthMap}"/>
										</h:selectOneMenu>
										&nbsp;
										<h:selectOneMenu id="fromYearItems" binding="#{systemUserUpdateBean.uiAuditDateFromYear}" value="#{systemUserUpdateBean.auditDateFromYear}">
											<f:selectItems value="#{systemUserUpdateBean.yearItems}"/>
										</h:selectOneMenu>
									</td>
								</tr>
								<tr>
									<td>Audit Date To</td>
									<td>
										<h:selectOneMenu id="toDayItems" binding="#{systemUserUpdateBean.uiAuditDateToDay}" value="#{systemUserUpdateBean.auditDateToDay}">
											<f:selectItems value="#{systemUserUpdateBean.dayItems}"/>
										</h:selectOneMenu>
										&nbsp;
										<h:selectOneMenu id="toMonthItems" binding="#{systemUserUpdateBean.uiAuditDateToMonth}" value="#{systemUserUpdateBean.auditDateToMonth}">
											<f:selectItems value="#{systemUserUpdateBean.monthMap}"/>
										</h:selectOneMenu>
										&nbsp;
										<h:selectOneMenu id="toYearItems" binding="#{systemUserUpdateBean.uiAuditDateToYear}" value="#{systemUserUpdateBean.auditDateToYear}">
											<f:selectItems value="#{systemUserUpdateBean.yearItems}"/>
										</h:selectOneMenu>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<h:commandButton actionListener="#{systemUserUpdateBean.updateBookAuditList}" immediate="true" styleClass="button" value="Search" id="search_submit"/> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<h:panelGroup id="emptyResultsGroup" rendered="#{empty systemUserUpdateBean.bookResultList}">
						<tr>
							<h:panelGroup id="zeroResultsGroup" rendered="#{systemUserUpdateBean.bookResultListSize == 0}">
								<td><h4>Search Results</h4></td>
								<td>
									No books found.
								</td>
							</h:panelGroup>
						</tr>
					</h:panelGroup>
					
					
					<h:panelGroup id="resultsGroup" rendered="#{! empty systemUserUpdateBean.bookResultList}">
						<tr>
							<td><h4>Search Results</h4></td>
							<td>
									
									<h:commandLink immediate="true" id="jsf_submitter" actionListener="#{systemUserUpdateBean.setPagerValues}"></h:commandLink>
									<%--Sort by
									<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserBean.setSortBy}" value="#{systemUserBean.sortBy}">
				    					<f:selectItem itemValue="Username" />
		    							<f:selectItem itemValue="Email" />
		    							<f:selectItem itemValue="First Name" />
		    							<f:selectItem itemValue="Last Name" />
			    					</h:selectOneMenu --%>
			    					<table cellspacing="0">
			    						<tr>
			    							<td class="twentyfive">
			    								Results per page
												&nbsp;<h:selectOneMenu immediate="true" binding="#{systemUserUpdateBean.uiResultsPerPage}" onchange="document.getElementById('jsf_submitter').onclick();" 
							    					valueChangeListener="#{systemUserUpdateBean.setPageSize}" value="#{systemUserUpdateBean.resultsPerPage}">
							    					<f:selectItem itemValue="10" />
							    					<f:selectItem itemValue="20" />
							    					<f:selectItem itemValue="30" />
							    					<f:selectItem itemValue="40" />
							    					<f:selectItem itemValue="50" />
							    					<f:selectItem itemValue="100" />
							    					<f:selectItem itemValue="500" />
							    					<f:selectItem itemValue="1000" />
							    				</h:selectOneMenu>
			    							</td>
			    							<td class="fifty">
			    								<h:outputText id="pagingText" value="#{systemUserUpdateBean.pagingText}"/>
												| Go to page
												&nbsp;<h:selectOneMenu immediate="true" binding="#{systemUserUpdateBean.uiGoToPage}"
													onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserUpdateBean.setCurrentPage}" value="#{systemUserUpdateBean.goToPage}">
													<f:selectItems id="goToPage" value="#{systemUserUpdateBean.pages}"/>
												</h:selectOneMenu>
			    							</td>
			    							<td class="twentyfive">
				    							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Go to:
												&nbsp;<h:commandLink immediate="true" actionListener="#{systemUserUpdateBean.firstPage}">First</h:commandLink>
												&nbsp;| <h:commandLink immediate="true" actionListener="#{systemUserUpdateBean.previousPage}">Previous</h:commandLink>
												&nbsp;| <h:commandLink immediate="true" actionListener="#{systemUserUpdateBean.nextPage}">Next</h:commandLink>
												&nbsp;| <h:commandLink immediate="true" actionListener="#{systemUserUpdateBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
											</td>
			    						</tr>
			    					</table>
									
									<table cellspacing="0">
										<tr class="shaded">
											<td class="twentyfive">E-ISBN</td>
											<td class="fifty">Book Title</td>
											<td class="twentyfive">Add</td>
										</tr>
										<tr class="shaded">
											<td class="twentyfive">&nbsp;</td>
											<td class="fifty">&nbsp;</td>
											<td class="twentyfive"><input id="selectAllAuditResults" type="checkbox" onclick="selectAllCheckBox(this.checked, 'auditResults');" /></td>
										</tr>
									</table>
									<h:dataTable columnClasses="twentyfive,fifty,twentyfive" cellspacing="0" id="auditResults" var="book" value="#{systemUserUpdateBean.bookResultList}">
										<h:column>
	        								<h:outputText id="resultEisbn" value="#{book.isbn}"/>
										</h:column>
										<h:column>
	        								<h:outputText id="resultMinTitle" value="#{book.title}" escape="false"/>
										</h:column>
										<h:column>
	        								<h:selectBooleanCheckbox onclick="selectCheckBox(this.checked, 'selectAllAuditResults', 'auditResults');" immediate="true" valueChangeListener="#{systemUserUpdateBean.addToInsertToAccessList}" id="auditAddBook">
	        									<f:attribute name="eBookBean" value="#{book}" />
	        								</h:selectBooleanCheckbox>
        								</h:column>
									</h:dataTable>
							</td>
						</tr>
					
						<tr>
							<td>&nbsp;</td>
							<td>
								<h:panelGroup rendered="#{systemUserUpdateBean.uiPublisherId.submittedValue eq -1}">
									<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllEbooksToAccessList}" value="Add All Loaded eBooks" />								
								</h:panelGroup>
								<h:panelGroup rendered="#{systemUserUpdateBean.uiPublisherId.submittedValue ne -1}">
									<h:commandButton id="add_all_publisher_ebooks1" styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllOrgEbooksToAccessList}" value="Add All Publisher eBooks" />								
								</h:panelGroup>
								&nbsp;<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.insertEbooksToAccessList}" value="Add Selected eBooks" />
							</td>
						</tr>
					</h:panelGroup>
					
					<tr>
						<td><h4>Accessible eBooks</h4></td>
						<td>
							<h:panelGroup id="emptyAccessibleGroup" rendered="#{empty systemUserUpdateBean.ebookAccessList}">
								<h:panelGroup id="zeroAccessibleGroup" rendered="#{systemUserUpdateBean.ebookAccessListSize == 0}">
									No eBooks assigned.
								</h:panelGroup>
							</h:panelGroup>
							<h:panelGroup id="accessibleGroup" rendered="#{! empty systemUserUpdateBean.ebookAccessList}">
																
								<table cellspacing="0">
									<tr class="shaded">
										<td class="twentyfive">E-ISBN</td>
										<td class="fifty">Book Title</td>
										<td class="twentyfive">Remove</td>
									</tr>
									<tr class="shaded">
										<td class="twentyfive"></td>
										<td class="fifty"></td>
										<td class="twentyfive">
											<input id="selectAllAccessible" onclick="selectAllCheckBox(this.checked, 'accessibleEbooksTable');" type="checkbox" />
										</td>
									</tr>
								</table>
								<h:dataTable columnClasses="twentyfive,fifty,twentyfive" cellspacing="0" id="accessibleEbooksTable" var="book" value="#{systemUserUpdateBean.ebookAccessList}">
									<h:column>
        								<h:outputText id="accessibleEisbn" value="#{book.isbn}"/>
										<h:panelGroup id="allEbooksAdded" rendered="#{empty book.isbn}">
											<%-- only "ALL EBOOKS" should be expected here --%>
        									<h:outputText id="accessibleBookId" value="#{book.bookId}"/>
										</h:panelGroup>
									</h:column>
									<h:column>
        								<h:outputText id="accessibleMainTitle" value="#{book.title}" escape="false"/>
									</h:column>
									<h:column>
        								<h:selectBooleanCheckbox onclick="selectCheckBox(this.checked, 'selectAllAccessible', 'accessibleEbooksTable');" immediate="true" valueChangeListener="#{systemUserUpdateBean.addToRemoveFromAccessList}" id="accessibleRemoveBook">
        									<f:attribute name="bookId" value="#{book.bookId}" />
        									<f:param name="bookId" value="#{book.bookId}" />
        								</h:selectBooleanCheckbox>
									</h:column>
								</h:dataTable>
								
							</h:panelGroup>
						</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td>
							<%--
						    <h:commandButton id="add_all_loaded_ebooks" styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllEbooksToAccessList}" value="Add All Loaded eBooks" />
							&nbsp;<h:commandButton id="add_all_publisher_ebooks2" styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllOrgEbooksToAccessList}" value="Add All Publisher eBooks" />
							&nbsp;
							 --%>
							 
							<c:if test="${empty systemUserUpdateBean.uiPublisherId.submittedValue}">
								<h:panelGroup rendered="#{systemUserUpdateBean.publisherId eq -1}">
									<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllEbooksToAccessList}" value="Add All Loaded eBooks" />								
								</h:panelGroup>
								<h:panelGroup rendered="#{systemUserUpdateBean.publisherId ne -1}">
									<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllOrgEbooksToAccessList}" value="Add All Publisher eBooks" />								
								</h:panelGroup>
							</c:if>
							
							<c:if test="${not empty systemUserUpdateBean.uiPublisherId.submittedValue}">
								<h:panelGroup rendered="#{systemUserUpdateBean.uiPublisherId.submittedValue eq -1}">
									<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllEbooksToAccessList}" value="Add All Loaded eBooks" />								
								</h:panelGroup>
								<h:panelGroup rendered="#{systemUserUpdateBean.uiPublisherId.submittedValue ne -1}">
									<h:commandButton styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.setAllOrgEbooksToAccessList}" value="Add All Publisher eBooks" />								
								</h:panelGroup>
							</c:if>
							
							<h:commandButton id="remove_selected_ebooks" styleClass="button" immediate="true" actionListener="#{systemUserUpdateBean.removeEbooksFromAccessList}" value="Remove Selected eBooks" />
						</td>
					</tr>
			
					<tr class="border">
			  			<td>User Activation</td>
			  			<td>
							<h:selectOneMenu value="#{systemUserUpdateBean.user.displayActive}">
								<f:selectItem itemLabel="Yes" itemValue="Yes" />
								<f:selectItem itemLabel="No" itemValue="No" />  
							</h:selectOneMenu>
			   			</td>
					</tr>
					
				</table>
			
				<table cellspacing="0" class="registered_buttons">
					<tr>
						<td>
							<p class="alert">* Required fields</p>
							<h:commandButton immediate="true" action="system_users" styleClass="button" value="Cancel" />
							<h:inputHidden id="updateValidator" value="C2" validator="#{systemUserUpdateBean.validateDetails}"/>
							&nbsp;<h:commandButton id="updateButton" actionListener="#{systemUserUpdateBean.updateSystemUser}" action="#{systemUserUpdateBean.updateSystemUserNav}" styleClass="button" value="#{systemUserUpdateBean.updateType}" />
						</td>
					</tr>
				</table>
				
				
				<h:commandLink id="setAllEbookToAccessList" immediate="true" actionListener="#{systemUserUpdateBean.setAllEbooksToAccessList}" ></h:commandLink>
				<h:commandLink id="setPublisherEbooksToAccessList" immediate="true" actionListener="#{systemUserUpdateBean.setAllOrgEbooksToAccessList}" />	
				<h:commandLink id="removeAllEbookToAccessList" immediate="true" actionListener="#{systemUserUpdateBean.removeAllEbooksToAccessList}" ></h:commandLink>
				<h:commandLink id="clearBookResultAndAccessList" immediate="true" actionListener="#{systemUserUpdateBean.clearBookResultAndAccessList}" ></h:commandLink>
					
			</h:form>
			</div>
		
		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>


<script type="text/javascript" src="js/jquery-1.3.2.js"></script>

<script type="text/javascript">
	//jquery

		//$(document).ready(function(){
			

			/*
			
			$("#select_publisher").change(function(){

				var publisherId = document.getElementById('select_publisher').value; 
				if(publisherId == '-1')
					$("#setAllEbookToAccessList").click();
				else
					$("#setPublisherEbooksToAccessList").click();
				
			});
			*/
			

		//});
	
	</script>

	<script type="text/javascript">
	function selectAllCheckBox(checked, docId) {
		var checkGroup = document.getElementById(docId).getElementsByTagName('input');
		for (i=0;i<checkGroup.length;i++) {
			checkGroup[i].checked = checked;
		}
	}
	function selectCheckBox(checked, selectAllDocId, checkGroupDocId) {
		if (checked == false) {
			document.getElementById(selectAllDocId).checked = false;
		} else {
			var accessible = document.getElementById(checkGroupDocId).getElementsByTagName('input');
			var allChecked = true;
			for (i=0;i<accessible.length;i++) {
				if (accessible[i].checked == false) {
					allChecked = false;
					break;
				}
			}
			if (allChecked) {
				document.getElementById(selectAllDocId).checked = true;
			}
		}
	}

	function enableDisablePublisher(flag){
		
		document.getElementById('select_publisher').disabled = flag;

		var obj = document.getElementById('add_all_publisher_ebooks1');
		if(obj != null)
			obj.disabled = flag;
		
		document.getElementById('add_all_publisher_ebooks2').disabled = flag;
	}
	
	function enableDisableEbookAccess(flag){
		document.getElementById('uiAuditEisbn').disabled = flag;
		document.getElementById('uiAuditSeriesId').disabled = flag;
		document.getElementById('uiAuditUsername').disabled = flag;
		document.getElementById('chapterStatus').disabled = flag;
		document.getElementById('fromDayItems').disabled = flag;
		document.getElementById('fromMonthItems').disabled = flag;
		document.getElementById('fromYearItems').disabled = flag;
		document.getElementById('toDayItems').disabled = flag;
		document.getElementById('toMonthItems').disabled = flag;
		document.getElementById('toYearItems').disabled = flag;
		document.getElementById('search_submit').disabled = flag;
	}

	function clearSearchAndAccessList2(){
		var isPublisherDisabled = document.getElementById('select_publisher').disabled

		if(isPublisherDisabled)
			return false;
		else
			return true;
	}
	
	function clearSearchAndAccessList(){
		var isManagePublisherChecked = document.getElementById('manage_publisher').checked; 
	
		if(isManagePublisherChecked)
			return false;
		else 
			return true;
	}
	
	function setPublisher(){
		var isManagePublisherChecked = document.getElementById('manage_publisher').checked; 

		return isManagePublisherChecked;
	}
	
	function setPublisherAndAccessList(){
		var manage_publisher = document.getElementById('manage_publisher');
		if(manage_publisher != null)
		{
			var isManagePublisherChecked = document.getElementById('manage_publisher').checked; 

			var isPublisherDisabled = setPublisher();
			
			enableDisablePublisher(isPublisherDisabled);
			enableDisableEbookAccess(isManagePublisherChecked);
		}
	}
	</script>



</body>

</f:view>

</html>