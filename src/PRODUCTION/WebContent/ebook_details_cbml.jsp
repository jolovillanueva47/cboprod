<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.util.HttpUtil"%>
<%@page import="org.cambridge.ebooks.production.document.EbookChapterDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean"%>
<%@page import="org.cambridge.ebooks.production.ebook.content.EBookContentBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />		
	<h:inputHidden id="loader" value="#{cbmlBean.initPage}" />
	<title>View ${cbmlBean.isbn }</title>

	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
		<c:out value="${cbmlBean.css}" />
		
	-->
	</style>
	<!--[if IE 8]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 6]>
        <link href="css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->

</head>
<body >
	<div class="legacy_content_container">
	
	<%-- <h:outputText value="#{cbmlBean.htmlList }" /> <br>--%>
		<f:subview id="cbmlView">
		
			<c:forEach items="${cbmlBean.htmlList }" var="cbml" >
				<c:set var="pageUrl" value="${cbml }" />
				<c:set var="content_url" scope="page" value='<%=System.getProperty("content.url.absolute")%>'/>
				<%-- <c:out value="${pageScope.content_url}${cbml}" /> --%>
				<c:import url="${pageScope.content_url}${cbml}"></c:import>
			</c:forEach> 
			
		</f:subview>
		
	</div>	
	<br />
</body>
</f:view>
</html>

