<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE package SYSTEM "../manifest.dtd">
<package id="${referenceID}">
<#list items as item>
<file filename="${item.fileName}" datatype="${item.dataType}"/>
</#list>
</package>