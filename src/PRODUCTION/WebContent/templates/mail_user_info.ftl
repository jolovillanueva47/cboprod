from:${from}
to:${to}<#if (cc)??>
cc:${cc}</#if><#if (bcc)??>
bcc:${bcc}</#if>
subject:${subject}
body:<p>
Dear ${body.name}, 
</p>

<p>
The following is your eBooks Production user account:
</p>

<p>
Username: ${body.username}
<br/>
Password: ${body.password}
</p>

<p>
Yours sincerely,
<br/>
eBooks Production Administrator
</p>