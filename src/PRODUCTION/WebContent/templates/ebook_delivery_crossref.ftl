<?xml version="${root.xml.version}" encoding="${root.xml.encoding}"?>
<doi_batch version="${root.doiBatch.version}" xmlns="${root.doiBatch.xmlns}" xmlns:xsi="${root.doiBatch.xmlnsXsi}" xsi:schemaLocation="${root.doiBatch.xsiSchemaLocation}">
	<#assign head=root.doiBatch.head>
    <head>
        <doi_batch_id>${head.doiBatchId}</doi_batch_id>
        <timestamp>${head.timestamp}</timestamp>
        <depositor>
            <name>${head.depositor.depositorName}</name>
            <email_address>${head.depositor.depositorEmail}</email_address>
        </depositor>
        <registrant>${head.registrant}</registrant>
    </head>
	<#assign body=root.doiBatch.body>
	<body>
		<book book_type="${body.book.bookType}">
			<book_metadata>
				<#assign bookMetadata=body.book.bookMetadata>
				<#if bookMetadata.contributors?size &gt; 0>
				<contributors>
				<#list bookMetadata.contributors as personName>
					<person_name sequence="${personName.sequence}" contributor_role="${personName.contributorRole}">
						<#if (personName.givenName)??>
						<given_name>${personName.givenName}</given_name>
						</#if>
						<surname>${personName.surname}</surname>
					</person_name>
				</#list>
				</contributors>
				</#if>
				<#if bookMetadata.titles?size &gt; 0>
				<#list bookMetadata.titles as title>
				<titles>
					<title>${title}</title>
					<#if bookMetadata.subTitle??>
					<subtitle>${bookMetadata.subTitle}</subtitle>
					</#if>
				</titles>
				</#list>
				</#if>
				<#if (body.book.bookMetadata.seriesMetadata)??>
				<#assign seriesMetadata=body.book.bookMetadata.seriesMetadata>
				<series_metadata>
					<#if seriesMetadata.titles?size &gt; 0>
					<#list seriesMetadata.titles as title>
					<titles>
						<title>${title}</title>
					</titles>
					</#list>
					</#if>
					<isbn>${seriesMetadata.isbn}</isbn>
					<doi_data>
						<doi>${seriesMetadata.doiData.doi}</doi>
						<resource>${seriesMetadata.doiData.resource}</resource>
					</doi_data>
				</series_metadata>
				</#if>
				<#if (bookMetadata.editionNumber)??>
				<edition_number>${bookMetadata.editionNumber}</edition_number>
				</#if>
				<#if (bookMetadata.printPublicationDate)??>
				<publication_date media_type="${bookMetadata.printPublicationDate.mediaType}">
					<#if (bookMetadata.printPublicationDate.month)??>
					<month>${bookMetadata.printPublicationDate.month}</month>
					</#if>
					<#if (bookMetadata.printPublicationDate.day)??>
					<day>${bookMetadata.printPublicationDate.day}</day>
					</#if>
					<#if (bookMetadata.printPublicationDate.year)??>
					<year>${bookMetadata.printPublicationDate.year}</year>
					</#if>
				</publication_date>
				</#if>
				<#if (bookMetadata.onlinePublicationDate)??>
				<publication_date media_type="${bookMetadata.onlinePublicationDate.mediaType}">
					<#if (bookMetadata.onlinePublicationDate.month)??>
					<month>${bookMetadata.onlinePublicationDate.month}</month>
					</#if>
					<#if (bookMetadata.onlinePublicationDate.day)??>
					<day>${bookMetadata.onlinePublicationDate.day}</day>
					</#if>
					<#if (bookMetadata.onlinePublicationDate.year)??>
					<year>${bookMetadata.onlinePublicationDate.year}</year>
					</#if>
				</publication_date>
				</#if>
				<isbn>${bookMetadata.isbn}</isbn>
				<publisher>
					<publisher_name>${bookMetadata.publisher.publisherName}</publisher_name>
					<publisher_place>${bookMetadata.publisher.publisherPlace}</publisher_place>
				</publisher>
				<doi_data>
					<doi>${bookMetadata.doiData.doi}</doi>
					<resource>${bookMetadata.doiData.resource}</resource>
				</doi_data>
			</book_metadata>
			<#if (body.book.contentItems)?? && body.book.contentItems?size &gt; 0>
			<#list body.book.contentItems as contentItem>
			<#if (contentItem.doiData.doi)??>
			<content_item component_type="${contentItem.componentType}"
				<#if (contentItem.levelSequenceNumber)??>
				level_sequence_number="${contentItem.levelSequenceNumber}"
				</#if>
				publication_type="${contentItem.publicationType}">
				<#if contentItem.contributors?size &gt; 0>
				<contributors>
				<#list contentItem.contributors as personName>
					<person_name sequence="${personName.sequence}" contributor_role="${personName.contributorRole}">
						<#if (personName.givenName)??>
						<given_name>${personName.givenName}</given_name>
						</#if>
						<surname>${personName.surname}</surname>
					</person_name>
				</#list>
				</contributors>
				</#if>
				<#if contentItem.titles?size &gt; 0>
				<#list contentItem.titles as title>
				<titles>
					<title>${title}</title>
				</titles>
				</#list>
				</#if>
				<#if (contentItem.componentNumber)??>
				<component_number>${contentItem.componentNumber}</component_number>
				</#if>
				<#if (contentItem.pages.firstPage)?? && (contentItem.pages.lastPage)??>
				<pages>
					<first_page>${contentItem.pages.firstPage}</first_page>
					<last_page>${contentItem.pages.lastPage}</last_page>
				</pages>
				</#if>
				<#if (contentItem.publisherItem.itemNumber)??>
				<publisher_item>
					<item_number>${contentItem.publisherItem.itemNumber}</item_number>
				</publisher_item>
				</#if>
				<doi_data>
					<doi>${contentItem.doiData.doi}</doi>
					<resource>${contentItem.doiData.resource}</resource>
				</doi_data>
			</content_item>
			</#if>
			</#list>
			</#if>
		</book>
	</body>
</doi_batch>