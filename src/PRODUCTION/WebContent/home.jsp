<html>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	
	<title>Browse eBooks</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
		@import url(css/jquery.tooltip.css);
	-->
	</style>	
</head>
	
<body onload="init();">

<div id="loading">Please wait, page is loading...<br /><img src="images/loading.gif" /></div>	
 
<div id="main" style="display: none">

<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Browse eBooks
	</div>
	
</div>


<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre"><div id="browse_ebooks">
		<!-- Body -->
		<h1>Browse eBooks</h1>
		<h:inputHidden id="alertMessage"/>
		<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
			<p class="alert"><h:message for="alertMessage"/><br/></p>
		</h:panelGroup>		
		</div></td>
	</tr></tbody>
	
</table>

<table id="content" cellspacing="0"> 
<tbody>
	<tr>
	<td id="centre"><div id="browse_ebooks">
		<h:form prependId="false">
		
		<h:inputHidden id="loader" value="#{bookBean.initBooks}" />
		<h:inputHidden id="ltrFilter" value="#{bookBean.letterFilter}" />
			<div id="ebooks_list">

			<h:commandLink id="jsf_pagination_submitter" actionListener="#{bookBean.setPagerValues}"></h:commandLink>
			
			
						<table border="0" cellpadding="3" cellspacing="1" id="browse_header">
			<tbody><tr>
			
			<h:panelGroup rendered="#{bookBean.sortBy eq 'status' or bookBean.sortBy eq 'isbn'}"><th class="header_sort checkall">&nbsp;</th></h:panelGroup>
			<th class="header_sort" id="ten">
				<h:outputText value="Publisher" />
			</th>			
			
			<th class="header_sort">
				<div id="atoz">
					<ul>		
						<h:panelGroup rendered="false">
						<h:commandLink id="publisher_submit" actionListener="#{bookBean.doPublisherAction}" />
							<li>       						
       						<h:selectOneMenu id="select_publisher" value="#{bookBean.publisherFilter}" onchange="document.getElementById('publisher_submit').onclick();" >
								<f:selectItems value="#{bookBean.publisherItems}"/>
							</h:selectOneMenu>
       						</li>
       					</h:panelGroup>	
       					
       					<h:panelGroup rendered="#{bookBean.publisherListSize == 1}">

       						<li>
       							<h:outputText value="#{bookBean.publisherList[0].name}" />
       							<!-- 
								<h:commandLink actionListener="#{bookBean.doPublisherAction}">
									<h:outputText value="#{publisher.name}" />
									<f:attribute name="actionType" value="PUBLISHERS"/>
									<f:attribute name="actionPublisherId" value="#{publisher.publisherId}"/>
									<f:attribute name="actionValue" value="A"/>
								</h:commandLink>
								 -->
       						</li>

       					</h:panelGroup>
       					
       					<h:panelGroup rendered="#{bookBean.srPublisherListSize gt 1}">
						<h:commandLink id="publisher_submit2" actionListener="#{bookBean.doSrPublisherAction}" />
							<li>       						
       						<h:selectOneMenu id="select_publisher2" value="#{bookBean.srPublisherFilter}" onchange="document.getElementById('publisher_submit2').onclick();" >
								<f:selectItems value="#{bookBean.srPublisherItems}"/>
							</h:selectOneMenu>
       						</li>
       					</h:panelGroup>			
					</ul>
				</div>
			</th>
			
			<th class="header_sort" id="ten">
				<h:outputText value="Product" />
			</th>
			
			<th class="header_sort">
				<div id="product_atoz">
					<ul>		
						      					
       					
       					<h:panelGroup rendered="true">
						<h:commandLink id="product_submit2" actionListener="#{bookBean.doSrPublisherAction}" />
							<li>       						
       						<h:selectOneMenu id="select_product2" value="#{bookBean.srProductFilter}" onchange="document.getElementById('product_submit2').onclick();" >
								<f:selectItems value="#{bookBean.srProductItems}"/>
							</h:selectOneMenu>
       						</li>
       					</h:panelGroup>			
					</ul>
				</div>
			</th>

			</tr></tbody>
			</table>
			
			
			
			<ul id="status_pagination">
				<h:panelGroup rendered="#{not empty bookBean.ebookList}">
				<li class="totalcount">
					<h:outputText id="numberFilter" value="#{bookBean.letterFilter}" rendered="#{not empty bookBean.letterFilter and bookBean.letterFilter ne '\\\d'}"/>
					<h:outputText id="letterFilter" value="#" rendered="#{not empty bookBean.letterFilter and bookBean.letterFilter eq '\\\d'}"/>
					<h:outputText id="allBooksTotal" value="A to Z" rendered="#{empty bookBean.letterFilter}" />
					 - 
					<h:outputText id="maxResults" value="#{bookBean.pager.maxResults}" /> eBooks
				</li>
				<li>
					Results per page
					<h:selectOneMenu id="resultsPerPage"
						onchange="document.getElementById('jsf_pagination_submitter').onclick();"
						valueChangeListener="#{bookBean.setPageSize}" 
						value="#{bookBean.resultsPerPage}">
						
						<f:selectItem itemValue="100"/>
						<f:selectItem itemValue="500"/>
						<f:selectItem itemValue="1000"/>
					</h:selectOneMenu>
	            </li>
				<h:panelGroup rendered="#{bookBean.pager.maxResults > 0}">
	            <li>
					Page
					<h:selectOneMenu id="goToPage"
						onchange="document.getElementById('jsf_pagination_submitter').onclick();"
						valueChangeListener="#{bookBean.setCurrentPage}"
						value="#{bookBean.goToPage}">

						<f:selectItems value="#{bookBean.pages}"/>
					</h:selectOneMenu>
					of <h:outputText value="#{bookBean.pager.maxPages}" />
				</li>
	            <li class="nextprev">
					<h:commandLink actionListener="#{bookBean.firstPage}">&laquo; First</h:commandLink> |
					<h:commandLink actionListener="#{bookBean.previousPage}">Previous</h:commandLink> |
					<h:commandLink actionListener="#{bookBean.nextPage}">Next</h:commandLink> |
					<h:commandLink actionListener="#{bookBean.lastPage}">Last &raquo;</h:commandLink>
				</li>
				</h:panelGroup>
				</h:panelGroup>
	            <li class="download_summary">
	            	<h:commandButton disabled="#{bookBean.summarySize lt 1}" style="margin: 0px; padding: 0px;" value="#{bookBean.downloadSummarySize}" actionListener="#{summaryReportBean.downloadEBookSummary}" styleClass="button">
						<f:attribute name="eBookManagedBean" value="#{bookBean}" />
					</h:commandButton>	
				</li>
	            <li class="download_summary">
					<h:outputText value="Book Title/ISBN: " />
					<h:inputText immediate="true" id="quickSearch" binding="#{bookBean.uiSearchTerm}" />
					<h:commandButton style="display:none;" id="jsfQuickSearchSubmit" actionListener="#{bookBean.doAction}"><f:attribute name="actionType" value="SEARCH"/></h:commandButton> 
					<input type="button" id="quickSearchButton" value="Search" class="button"></input>&nbsp;&nbsp;
				</li>
			</ul>

			<table border="0" cellpadding="3" cellspacing="1" id="browse_header">
			<tbody><tr>
				<h:panelGroup rendered="#{bookBean.sortBy eq 'status' or bookBean.sortBy eq 'isbn'}"><th class="header_sort checkall">&nbsp;</th></h:panelGroup>
				<th class="header_sort" id="ten">
					<h:commandLink actionListener="#{bookBean.sortList}" value="ISBN ">
						<h:graphicImage rendered="#{bookBean.sortBy eq 'isbn' and not bookBean.reverseSort}" value="images/browse_ebooks_arrowup.gif" style="border: 0"/>
						<h:graphicImage rendered="#{bookBean.sortBy eq 'isbn' and bookBean.reverseSort}" value="images/browse_ebooks_arrowdown.gif" style="border: 0"/>
						<f:attribute name="sortBy" value="isbn"/>
					</h:commandLink>
				</th>
				<th class="header_sort">
					<div id="atoz">
						<ul>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="#" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="\d"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="A" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="A"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="B" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="B"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="C" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="C"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="D" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="D"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="E" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="E"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="F" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="F"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="G" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="G"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="H" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="H"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="I" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="I"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="J" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="J"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="K" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="K"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="L" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="L"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="M" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="M"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="N" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="N"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="O" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="O"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="P" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="P"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="Q" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="Q"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="R" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="R"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="S" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="S"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="T" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="T"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="U" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="U"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="V" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="V"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="W" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="W"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="X" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="X"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="Y" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="Y"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value="Z" />
									<f:attribute name="actionType" value="ATOZ"/>
									<f:attribute name="actionValue" value="Z"/>
								</h:commandLink>
							</li>
							<li>
								<h:commandLink actionListener="#{bookBean.doAction}" styleClass="letter_filter">
									<h:outputText value=" - Show All" />
									<f:attribute name="actionType" value="ALL"/>
									<f:attribute name="actionPublisherId" value=""/>
									<f:attribute name="actionValue" value=""/>
								</h:commandLink>
							</li>
							
						</ul>
					</div>
				</th>
				<th class="header_sort" id="twentyfive">
					<h:commandLink actionListener="#{bookBean.sortList}" value="Publish Date ">
						<h:graphicImage rendered="#{bookBean.sortBy eq 'print' and not bookBean.reverseSort}" value="images/browse_ebooks_arrowup.gif" style="border: 0"/>
						<h:graphicImage rendered="#{bookBean.sortBy eq 'print' and bookBean.reverseSort}" value="images/browse_ebooks_arrowdown.gif" style="border: 0"/>
						<f:attribute name="sortBy" value="print"/>
					</h:commandLink>
					
				</th>
				<th class="header_sort" id="twentyfive">
					<h:commandLink actionListener="#{bookBean.sortList}" value="Product Group ">
						<h:graphicImage rendered="#{bookBean.sortBy eq 'online' and not bookBean.reverseSort}" value="images/browse_ebooks_arrowup.gif" style="border: 0"/>
						<h:graphicImage rendered="#{bookBean.sortBy eq 'online' and bookBean.reverseSort}" value="images/browse_ebooks_arrowdown.gif" style="border: 0"/>
						<f:attribute name="sortBy" value="online"/>
					</h:commandLink>
					
				</th>
				<th class="header_sort" id="twentyfive">
					<h:commandLink actionListener="#{bookBean.sortList}" value="Status ">
						<h:graphicImage rendered="#{bookBean.sortBy eq 'status' and not bookBean.reverseSort}" value="images/browse_ebooks_arrowup.gif" style="border: 0"/>
						<h:graphicImage rendered="#{bookBean.sortBy eq 'status' and bookBean.reverseSort}" value="images/browse_ebooks_arrowdown.gif" style="border: 0"/>
						<f:attribute name="sortBy" value="status"/>
					</h:commandLink>
				</th>
			</tr></tbody>
			</table>
			</div>

			<%------ $$ISBN ------%>
			<%--h:panelGroup rendered="#{bookBean.sortBy eq 'isbn'}"--%>
			<h:panelGroup rendered="false">
				<div id="ebooks_list">
				<h:dataTable value="#{bookBean.ebookList}" var="book" border="0" cellpadding="3" cellspacing="1" id="table_isbn" rowClasses="odd,even" columnClasses="checkall,ten,sixtyfive,twentyfive">
					<!-- CHECK BOX -->
					<h:column>
						<f:facet name="header"><h:selectBooleanCheckbox id="selectAllApproved" rendered="#{userInfo.accessRights.canPublish and bookBean.displayPublish}" onclick="selectAllCheckBox(this.checked, 'table_isbn');"/></f:facet>
						<h:selectBooleanCheckbox rendered="#{userInfo.accessRights.canPublish and book.ebook.status eq '4'}" valueChangeListener="#{bookBean.addToPublishList}" onclick="selectCheckBox(this.checked, 'table_isbn:selectAllApproved', 'table_isbn');">
							<f:attribute name="bookId" value="#{book.bookId}" />
							<f:param name="bookId" value="#{book.bookId}" />
						</h:selectBooleanCheckbox>
					</h:column>
					<!-- ISBN -->
					<h:column>
						<f:facet name="header"><h:commandButton actionListener="#{bookBean.publishCheckedBooks}" rendered="#{userInfo.accessRights.canPublish and bookBean.displayPublish}" styleClass="button" value="Sign off"/></f:facet>						
						<h:panelGroup rendered="#{book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
							<h:commandLink value="#{book.isbn}" action="#{bookContentBean.showChapter}" 
								actionListener="#{bookContentBean.showChapterListener}" >
								<f:attribute name="bookId" value="#{book.bookId}" />
								<f:param name="bookId" value="#{book.bookId}" />
							</h:commandLink>
						</h:panelGroup>
						
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							 <h:outputText value="#{book.isbn}" />
						</h:panelGroup>						
					</h:column>
					
					<!-- TITLE -->
					<h:column>
						<h:panelGroup rendered="#{not empty book.isbn and book.isbn ne -2 and book.isbn ne -1 and book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
							 <h:commandLink action="#{ebookBean.show}" 
								actionListener="#{ebookBean.showListener}" >
								<f:attribute name="bookId" value="#{book.bookId}" />
								<f:param name="bookId" value="#{book.bookId}" />
								<h:outputText value="#{book.title}" escape="false"/>
							 </h:commandLink>
						</h:panelGroup>
						
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							 <h:outputText value="#{book.title}" escape="false"/>
						</h:panelGroup>	
					</h:column>
					<!-- STATUS -->
					<h:column>		
						<!-- status description -->				
						<h:panelGroup rendered="#{book.ebook.status ne 200}">
							<h:commandLink actionListener="#{summaryReportBean.downloadEBookContentSummary}">								
								<h:outputText value="#{book.statusDescription}" />
								<f:attribute name="bookId" value="#{book.bookId}"/>
								<f:param name="bookId" value="#{book.bookId}"/>
								<f:attribute name="isbn" value="#{book.isbn}" />
								<f:attribute name="bookTitle" value="#{book.title}" />
							</h:commandLink>	
						</h:panelGroup>	
						
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							<h:outputText style="color: red;" value="#{book.statusDescription}" />
							<h:panelGroup rendered="#{userInfo.accessRights.canDelete}">
								&nbsp;-&nbsp;
								<h:commandLink action="deleteEbook" actionListener="#{bookBean.confirmDelete}" value="Remove from list">
									<f:attribute name="book" value="#{book}" />
								</h:commandLink>
							</h:panelGroup>
						</h:panelGroup>
					</h:column>
				</h:dataTable>
				<h:panelGroup rendered="#{empty bookBean.ebookList}">
					<table>
						<tr>
							<td id="centre">
								<br></br>&nbsp;No eBooks available.
							</td>
						</tr>
					</table>
				</h:panelGroup>
				</div>
				<div id="backtotop"><a href="#top">back to top <img src="images/browse_ebooks_backtotop.gif" border="" /></a></div>
			</h:panelGroup>

			<%------ $$TITLE ------%>
			<h:panelGroup rendered="#{bookBean.sortBy eq 'title'}">
				<h:dataTable value="#{bookBean.alphaKeyList}" id="table_title" var="key" width="100%" border="0" cellpadding="0" cellspacing="0">
					<h:column>
						<div id="ebooks_list">
						<h:dataTable value="#{bookBean.alphabeticalList[key]}" width="100%" var="book" border="0" cellpadding="3" cellspacing="1" rowClasses="odd,even" columnClasses="checkall,ten,sixtyfive,twentyfive">
							<!-- CHECK BOX -->
							<h:column>
								<f:facet name="header"> 
									&nbsp;
								</f:facet>
								<h:selectBooleanCheckbox rendered="#{userInfo.accessRights.canPublish and book.ebook.status eq '4'}" valueChangeListener="#{bookBean.addToPublishList}" onclick="selectTitleCheckBox(this.checked, 'table_title:selectAllApproved', 'table_title');">
									<f:attribute name="bookId" value="#{book.bookId}" />
									<f:param name="bookId" value="#{book.bookId}" />
								</h:selectBooleanCheckbox>
							</h:column>
							<!-- ISBN -->
							<h:column>
								<f:facet name="header">
									<h:commandButton actionListener="#{bookBean.publishCheckedBooks}" rendered="#{userInfo.accessRights.canPublish and bookBean.displayPublish}" styleClass="button" value="Sign off"/>
								</f:facet>
								<h:panelGroup rendered="#{book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
									<h:commandLink value="#{book.isbn}" action="#{bookContentBean.showChapter}" actionListener="#{bookContentBean.showChapterListener}" >
										<f:attribute name="bookId" value="#{book.bookId}" />
										<f:param name="bookId" value="#{book.bookId}" />
									</h:commandLink>
								</h:panelGroup>
								
								<h:panelGroup rendered="#{book.ebook.status eq 200}">
									 <h:outputText value="#{book.isbn}" />
								</h:panelGroup>
							</h:column>
							<!-- TITLE -->
							<h:column>
								<f:facet name="header">
									<h:outputText escape="false" value="<a id=\"#{key}\"></a>eBooks - #{key}" />
									
								</f:facet>
								<h:panelGroup rendered="#{not empty book.isbn and book.isbn ne -2 and book.isbn ne -1 and book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
									 <h:commandLink action="#{ebookBean.show}" actionListener="#{ebookBean.showListener}" >
										<f:attribute name="bookId" value="#{book.bookId}" />
										<f:param name="bookId" value="#{book.bookId}" />
										<h:outputText value="#{book.title}" escape="false"/>
									 </h:commandLink>
								</h:panelGroup>
								
								<h:panelGroup rendered="#{book.ebook.status eq 200}">
									 <h:outputText value="#{book.title}" escape="false"/>
								</h:panelGroup>
							</h:column>
							<h:column>
							</h:column>
							<h:column>
							</h:column>
							<!-- STATUS -->
							<h:column>
								<f:facet name="header">
									&nbsp;
								</f:facet>
								
								<!-- status description -->
								<h:panelGroup rendered="#{book.ebook.status ne 200}">
									<h:commandLink actionListener="#{summaryReportBean.downloadEBookContentSummary}">								
										<h:outputText value="#{book.statusDescription}" />
										<f:attribute name="bookId" value="#{book.bookId}"/>
										<f:param name="bookId" value="#{book.bookId}"/>
										<f:attribute name="isbn" value="#{book.isbn}" />
										<f:attribute name="bookTitle" value="#{book.title}" />
									</h:commandLink>	
								</h:panelGroup>	
								
								<h:panelGroup rendered="#{book.ebook.status eq 200}">
									<h:outputText style="color: red;" value="#{book.statusDescription}" />
									<h:panelGroup rendered="#{userInfo.accessRights.canDelete}">
										&nbsp;-&nbsp;
										<h:commandLink action="deleteEbook" actionListener="#{bookBean.confirmDelete}" value="Remove from list">
											<f:attribute name="book" value="#{book}" />
										</h:commandLink>
									</h:panelGroup>
								</h:panelGroup>
							</h:column>
						</h:dataTable>
						</div>
						<div id="backtotop"><a href="#top">back to top <img src="images/browse_ebooks_backtotop.gif" border="" /></a></div>
					</h:column>
				</h:dataTable>
				<h:panelGroup rendered="#{empty bookBean.alphabeticalList}">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td id="centre">
								<br></br>&nbsp;No eBooks available.
							</td>
						</tr>
					</table>
				</h:panelGroup>
			</h:panelGroup>

			<%------ $$STATUS ------%>
			<h:commandLink id="jsf_submitter" actionListener="#{bookBean.filterEBooks}"/>
			<h:panelGroup rendered="#{bookBean.sortBy eq 'status' or bookBean.sortBy eq 'isbn' or bookBean.sortBy eq 'online' or bookBean.sortBy eq 'print'}">
				<div id="ebooks_list">
				<h:dataTable rendered="#{not empty bookBean.ebookList}" id="table_status" value="#{bookBean.ebookList}" var="book" border="0" cellpadding="3" cellspacing="1" rowClasses="odd,even" columnClasses="checkall,ten,sixtyfive,twentyfive,twentyfive,twentyfive ">
					<!-- CHECK BOX -->
					<h:column>
						<f:facet name="header"><h:selectBooleanCheckbox id="selectAllApproved" binding="#{bookBean.uiSelectAllApproved}" rendered="#{userInfo.accessRights.canPublish and bookBean.displayPublish}" onclick="selectAllCheckBox(this.checked, 'table_status');"/></f:facet>
						<h:selectBooleanCheckbox rendered="#{userInfo.accessRights.canPublish and book.ebook.status eq '4'}" valueChangeListener="#{bookBean.addToPublishList}" onclick="selectCheckBox(this.checked, 'table_status:selectAllApproved', 'table_status');">
							<f:attribute name="bookId" value="#{book.bookId}" />
							<f:param name="bookId" value="#{book.bookId}"/>
						</h:selectBooleanCheckbox>
					</h:column>
					<!-- ISBN -->
					<h:column>
						<f:facet name="header"><h:commandButton actionListener="#{bookBean.publishCheckedBooks}" rendered="#{userInfo.accessRights.canPublish and bookBean.displayPublish}" styleClass="button" value="Sign off"/></f:facet>				
						<h:panelGroup rendered="#{book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
							<h:commandLink value="#{book.isbn}" action="#{bookContentBean.showChapter}" actionListener="#{bookContentBean.showChapterListener}" >
								<f:attribute name="bookId" value="#{book.bookId}" />
								<f:param name="bookId" value="#{book.bookId}"/>
							</h:commandLink>
						</h:panelGroup>
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							 <h:outputText value="#{book.isbn}" />
						</h:panelGroup>					
					</h:column>
					<!-- TITLE -->
					<h:column>
						<f:facet name="header">
							<h:panelGroup>
								
								<%--- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ---%>
								
							</h:panelGroup>
						</f:facet>
						<h:panelGroup rendered="#{not empty book.isbn and book.isbn ne -2 and book.isbn ne -1 and book.ebook.status ne 200}"> <%-- Status 200 is hard coded on EBook entity class --%>
							<h:commandLink action="#{ebookBean.show}" 
								actionListener="#{ebookBean.showListener}" >
								<f:attribute name="bookId" value="#{book.bookId}" />
								<f:param name="bookId" value="#{book.bookId}"/>
								<h:outputText value="#{book.title}" escape="false"/>
																					
							</h:commandLink>
						</h:panelGroup>
						
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							 <h:outputText value="#{book.title}" escape="false"/>
						</h:panelGroup>
					</h:column>
					<!-- PUB DATE PRINT -->
					<h:column>
						<f:facet name="header">
							<h:panelGroup>
								
								<%--- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ---%>
								
							</h:panelGroup>
						</f:facet>
												
						<h:panelGroup>
							 <h:outputText value="#{book.pubPrint}" escape="false">
							 	<f:convertDateTime pattern="dd-MMM-yyyy" />
							 </h:outputText>
						</h:panelGroup>
					</h:column>
					<!-- PUB DATE ONLINE -->
					<h:column>
						<f:facet name="header">
							<h:panelGroup>
								
								<%--- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ---%>
								
							</h:panelGroup>
						</f:facet>	
						
						<h:panelGroup>
							 	<h:outputText value="#{book.publisher}/#{book.product}" escape="false">
							 </h:outputText>
						</h:panelGroup>
					</h:column>
					<!-- STATUS -->
					<h:column>
						<f:facet name="header">
							<h:panelGroup>
								<a href="javascript:toggleFilter();" id="filter">Show Filter</a><br/>
								<div id="listBox">
									<h:selectManyListbox value="#{bookBean.filterByArr}" onclick="checkFilter(this);">
										<f:selectItems value="#{bookBean.filterByOptions}"/>
									</h:selectManyListbox><br/>
									<h:commandButton action="#{bookBean.doFilter}" styleClass="button" value="Filter"/>
								</div>
							</h:panelGroup>
                		</f:facet>
                		
                		<!-- status description -->
						<h:panelGroup rendered="#{book.ebook.status ne 200}">
							<h:commandLink actionListener="#{summaryReportBean.downloadEBookContentSummary}">								
								<h:outputText value="#{book.statusDescription}" />
								<f:attribute name="bookId" value="#{book.bookId}"/>
								<f:param name="bookId" value="#{book.bookId}"/>
								<f:attribute name="isbn" value="#{book.isbn}" />
								<f:attribute name="bookTitle" value="#{book.title}" />
							</h:commandLink>	
						</h:panelGroup>	
						
						<h:panelGroup rendered="#{book.ebook.status eq 200}">
							<h:outputText style="color: red;" value="#{book.statusDescription}" />
							<h:panelGroup rendered="#{userInfo.accessRights.canDelete}">
								&nbsp;-&nbsp;
								<h:commandLink action="deleteEbook" actionListener="#{bookBean.confirmDelete}" value="Remove from list">
									<f:attribute name="book" value="#{book}" />
								</h:commandLink>
							</h:panelGroup>
						</h:panelGroup>
					</h:column>
				</h:dataTable>
				<h:panelGroup rendered="#{empty bookBean.ebookList}">
					<table border="0" cellpadding="3" cellspacing="1">
						<tr>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>
								<a href="javascript:toggleFilter();" id="filter">Show Filter</a><br/>
								<div id="listBox">	
									<h:selectManyListbox value="#{bookBean.filterByArr}"  onclick="checkFilter(this);">
										<f:selectItems value="#{bookBean.filterByOptions}"/>
									</h:selectManyListbox><br/>
									<h:commandButton action="#{bookBean.doFilter}" styleClass="button" value="Filter"/>																		
								</div>
							</th>		
						</tr>
						<tr>
							<td class="checkall">&nbsp;</td>
							<td class="ten">&nbsp;</td>
							<td class="sixtyfive">No eBooks available.</td>
							<td class="twentyfive">&nbsp;</td>
						</tr>
					</table>
				</h:panelGroup>
				</div>
				<div id="backtotop"><a href="#top">back to top <img src="images/browse_ebooks_backtotop.gif" border="" /></a></div>
			</h:panelGroup>

			<div style="text-align: right; padding: 0px 0px 5px 0px;">
				<h:commandButton disabled="#{bookBean.summarySize lt 1}" value="#{bookBean.downloadSummarySize}" actionListener="#{summaryReportBean.downloadEBookSummary}" styleClass="button">
					<f:attribute name="eBookManagedBean" value="#{bookBean}" />
				</h:commandButton>
			</div>

		</h:form>
	</tr>

</tbody>
</table>

<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>

</div> <%-- end main --%>

<%-- 
<script type="text/javascript" src="http://konami-js.googlecode.com/svn/trunk/konami.js"></script>
<script type="text/javascript">
	konami.code = function() {
		alert('ok...');
		window.location = 'home.jsf?updateStatus=1';
		}

	konami.load()
</script>
 --%>
 
 	<script type="text/javascript" src="js/jquery-1.3.2.js"></script>	
	<script type="text/javascript" src="js/ui.core.js"></script>
	<script type="text/javascript" src="js/ui.draggable.js"></script>
	<script type="text/javascript" src="js/jquery.dimensions.js"></script>
	<script type="text/javascript" src="js/jquery.tooltip.js"></script>

	<script type="text/javascript">


	//$(document).ready(function(){
	var link = $("#filter");
	var listbox = $("#listBox");			
	listbox.draggable();			
	listbox.css("position","absolute");
	listbox.css("z-index","1000");			
	listbox.css("display","block");
	listbox.css("visibility","visible");
	listbox.css("float","left");
	listbox.css("background","#f1f1f1");	
	listbox.css("border","5px solid #cccccc");
	listbox.css("padding","10px");
	
	//link.click(toggleFilter);
	link.attr("title","<h:outputText value='#{bookBean.filterUsed}' escape='false'/> ");			
	listbox.css("visibility","hidden");				
	link.tooltip({
		track: true,
		delay: 0,
		showURL: false,
		showBody: " - ",
		fade: 250			
	});

	// for underlining selected letter/number filter
	$(".letter_filter").each(function(){
		var filterVal = $("#ltrFilter").val();
		var thisText = $(this).text(); 
		if(thisText == filterVal || (filterVal == '\\d' && thisText == '#') || (filterVal == '' && thisText.toLowerCase().indexOf("show all") >= 0)){
			$(this).html("<u>"+$(this).text()+"</u>");
		}
	});	

	$("#quickSearch").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$("#quickSearchButton").click();
        	return false;
        }
	});

	$("#quickSearchButton").click(function(){
		if ($("#quickSearch").val().replace(/^\s+|\s+$/g,"").length > 0) {
			$("#jsfQuickSearchSubmit").click();
		} else {
			alert("Invalid search.");
		}
	});

				
//});


	

	function toggleFilter(){
		var link = $("#filter");
		var listbox = $("#listBox");
		if( link.html() == "Show Filter" ){
			link.html("Hide Filter");
			listbox.css("visibility","visible");
			//listbox.css("display","block");												
		}else{
			link.html("Show Filter");						
			listbox.css("visibility","hidden");
			//listbox.css("display","none");						
		}	
	}

	//deselects all filters if one filter is selected
	function checkFilter(elem){
		if(elem.options[0].selected == true){
			for( var i=1; i < elem.length ; i++ ){
				elem.options[i].selected = false;
			}
		}
	}

	function init() {
		var loading = document.getElementById('loading');
		if ( loading != null ) { loading.style.display = 'none'; }
		var main = document.getElementById('main');
		if ( main != null ) { main.style.display = 'block';	}
		focusTo();
	}

	
	function focusTo() { 
		var loc =  window.location.href;
		var idx = loc.indexOf('#');
		if ( idx > -1 ) { 
			window.location = loc;
		}
	}

	
	function selectAllCheckBox(checked, docId) {
		var checkGroup = document.getElementById(docId).getElementsByTagName('input');
		for (i=0;i<checkGroup.length;i++) {
			checkGroup[i].checked = checked;
		}
	}

	
	function selectCheckBox(checked, selectAllDocId, checkGroupDocId) {
		if (checked == false) {
			document.getElementById(selectAllDocId).checked = false;
		} else {
			var accessible = document.getElementById(checkGroupDocId).getElementsByTagName('input');
			var allChecked = true;
			for (i=0;i<accessible.length;i++) {
				if (accessible[i].checked == false) {
					allChecked = false;
					break;
				}
			}
			if (allChecked) {
				document.getElementById(selectAllDocId).checked = true;
			}
		}
	}

	function selectTitleCheckBox(checked, selectAllDocId, checkGroupDocId) {
		if (checked == true) {
			var accessible = document.getElementById(checkGroupDocId).getElementsByTagName('input');
			var allChecked = true;
			for (i=0;i<accessible.length;i++) {
				if (accessible[i].checked == false) {
					allChecked = false;
					break;
				}
			}
		}
	}
	</script>
 
</body>

</f:view>

</html>