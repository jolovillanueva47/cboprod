<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Third Party Manual Delivery</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Third Party Manual Delivery
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Manual Delivery</h1>

			<h:form>
			<div id="alphalist">
				<table cellspacing="0">
					<tr>
						<td class="ebook"><h3><a href="manual_delivery_all_ebooks.jsf">Select all Issues from all eBooks</a></h3></td>
					</tr> 
					<tr>
						<td class="ebook"><h3><a href="#" onclick="redirectToIssuesByJournal()">Browse and select Issues by eBooks</a></h3></td>
					</tr> 
					<tr>
						<td class="ebook">&nbsp;</td>
					</tr> 
				</table>	
			</div>	
				<table cellpsacing="0">	
					<tr>
						<td>From : 
							<select name="dayFrom" size="1">
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>

								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>

								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
							</select>
							<select name="monthFrom" size="1">
								<option value="01" selected="selected">Jan</option>
								<option value="02">Feb</option>
								<option value="03">Mar</option>
								<option value="04">Apr</option>
								<option value="05">May</option>
									
								<option value="06">Jun</option>
								<option value="07">Jul</option>
								<option value="08">Aug</option>
								<option value="09">Sep</option>
								<option value="10">Oct</option>
								<option value="11">Nov</option>
								<option value="12">Dec</option>
							</select>
      						<select name="yearFrom" size="1">
      							<option value="1887">1887</option>
								<option value="1888">1888</option>
								<option value="1889">1889</option>
								<option value="1890">1890</option>
								<option value="1891">1891</option>
								<option value="1892">1892</option>
								<option value="1893">1893</option>
								<option value="1894">1894</option>
								<option value="1895">1895</option>
								<option value="1896">1896</option>
									
								<option value="1897">1897</option>
								<option value="1898">1898</option>
								<option value="1899">1899</option>
								<option value="1900">1900</option>
								<option value="1901">1901</option>
								<option value="1902">1902</option>
								<option value="1903">1903</option>
								<option value="1904">1904</option>
								<option value="1905">1905</option>
									
								<option value="1906">1906</option>
								<option value="1907">1907</option>
								<option value="1908">1908</option>
								<option value="1909">1909</option>
								<option value="1910">1910</option>
								<option value="1911">1911</option>
								<option value="1912">1912</option>
								<option value="1913">1913</option>
								<option value="1914">1914</option>
									
								<option value="1915">1915</option>
								<option value="1916">1916</option>
								<option value="1917">1917</option>
								<option value="1918">1918</option>
								<option value="1919">1919</option>
								<option value="1920">1920</option>
								<option value="1921">1921</option>
								<option value="1922">1922</option>
								<option value="1923">1923</option>
								
								<option value="1924">1924</option>
								<option value="1925">1925</option>
								<option value="1926">1926</option>
								<option value="1927">1927</option>
								<option value="1928">1928</option>
								<option value="1929">1929</option>
								<option value="1930">1930</option>
								<option value="1931">1931</option>
								<option value="1932">1932</option>
									
								<option value="1933">1933</option>
								<option value="1934">1934</option>
								<option value="1935">1935</option>
								<option value="1936">1936</option>
								<option value="1937">1937</option>
								<option value="1938">1938</option>
								<option value="1939">1939</option>
								<option value="1940">1940</option>
								<option value="1941">1941</option>
									
								<option value="1942">1942</option>
								<option value="1943">1943</option>
								<option value="1944">1944</option>
								<option value="1945">1945</option>
								<option value="1946">1946</option>
								<option value="1947">1947</option>
								<option value="1948">1948</option>
								<option value="1949">1949</option>
								<option value="1950">1950</option>
								
								<option value="1951">1951</option>
								<option value="1952">1952</option>
								<option value="1953">1953</option>
								<option value="1954">1954</option>
								<option value="1955">1955</option>
								<option value="1956">1956</option>
								<option value="1957">1957</option>
								<option value="1958">1958</option>
								<option value="1959">1959</option>
									
								<option value="1960">1960</option>
								<option value="1961">1961</option>
								<option value="1962">1962</option>
								<option value="1963">1963</option>
								<option value="1964">1964</option>
								<option value="1965">1965</option>
								<option value="1966">1966</option>
								<option value="1967">1967</option>
								<option value="1968">1968</option>
									
								<option value="1969">1969</option>
								<option value="1970">1970</option>
								<option value="1971">1971</option>
								<option value="1972">1972</option>
								<option value="1973">1973</option>
								<option value="1974">1974</option>
								<option value="1975">1975</option>
								<option value="1976">1976</option>
								<option value="1977">1977</option>
									
								<option value="1978">1978</option>
								<option value="1979">1979</option>
								<option value="1980">1980</option>
								<option value="1981">1981</option>
								<option value="1982">1982</option>
								<option value="1983">1983</option>
								<option value="1984">1984</option>
								<option value="1985">1985</option>
								<option value="1986">1986</option>
									
								<option value="1987">1987</option>
								<option value="1988">1988</option>
								<option value="1989">1989</option>
								<option value="1990">1990</option>
								<option value="1991">1991</option>
								<option value="1992">1992</option>
								<option value="1993">1993</option>
								<option value="1994">1994</option>
								<option value="1995">1995</option>
									
								<option value="1996">1996</option>
								<option value="1997">1997</option>
								<option value="1998">1998</option>
								<option value="1999">1999</option>
								<option value="2000">2000</option>
								<option value="2001">2001</option>
								<option value="2002">2002</option>
								<option value="2003">2003</option>
								<option value="2004">2004</option>
									
								<option value="2005">2005</option>
								<option value="2006">2006</option>
								<option value="2007">2007</option>
								<option value="2008">2008</option>
								<option value="2009" selected="selected">2009</option>
							</select>
							To : 
							<select name="dayTo" size="1">
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
									
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>

								<option value="31">31</option>
							</select>
							<select name="monthTo" size="1">
								<option value="01" selected="selected">Jan</option>
								<option value="02">Feb</option>
								<option value="03">Mar</option>
								<option value="04">Apr</option>
								<option value="05">May</option>
								<option value="06">Jun</option>
								<option value="07">Jul</option>
								
								<option value="08">Aug</option>
								<option value="09">Sep</option>
								<option value="10">Oct</option>
								<option value="11">Nov</option>
								<option value="12">Dec</option>
							</select>
							<select name="yearTo" size="1">
								<option value="1887">1887</option>
								<option value="1888">1888</option>
								<option value="1889">1889</option>
								
								<option value="1890">1890</option>
								<option value="1891">1891</option>
								<option value="1892">1892</option>
								<option value="1893">1893</option>
								<option value="1894">1894</option>
								<option value="1895">1895</option>
								<option value="1896">1896</option>
								<option value="1897">1897</option>
								<option value="1898">1898</option>
								
								<option value="1899">1899</option>
								<option value="1900">1900</option>
								<option value="1901">1901</option>
								<option value="1902">1902</option>
								<option value="1903">1903</option>
								<option value="1904">1904</option>
								<option value="1905">1905</option>
								<option value="1906">1906</option>
								<option value="1907">1907</option>
								
								<option value="1908">1908</option>
								<option value="1909">1909</option>
								<option value="1910">1910</option>
								<option value="1911">1911</option>
								<option value="1912">1912</option>
								<option value="1913">1913</option>
								<option value="1914">1914</option>
								<option value="1915">1915</option>
								<option value="1916">1916</option>
								
								<option value="1917">1917</option>
								<option value="1918">1918</option>
								<option value="1919">1919</option>
								<option value="1920">1920</option>
								<option value="1921">1921</option>
								<option value="1922">1922</option>
								<option value="1923">1923</option>
								<option value="1924">1924</option>
								<option value="1925">1925</option>
								
								<option value="1926">1926</option>
								<option value="1927">1927</option>
								<option value="1928">1928</option>
								<option value="1929">1929</option>
								<option value="1930">1930</option>
								<option value="1931">1931</option>
								<option value="1932">1932</option>
								<option value="1933">1933</option>
								<option value="1934">1934</option>
								
								<option value="1935">1935</option>
								<option value="1936">1936</option>
								<option value="1937">1937</option>
								<option value="1938">1938</option>
								<option value="1939">1939</option>
								<option value="1940">1940</option>
								<option value="1941">1941</option>
								<option value="1942">1942</option>
								<option value="1943">1943</option>
								
								<option value="1944">1944</option>
								<option value="1945">1945</option>
								<option value="1946">1946</option>
								<option value="1947">1947</option>
								<option value="1948">1948</option>
								<option value="1949">1949</option>
								<option value="1950">1950</option>
								<option value="1951">1951</option>
								<option value="1952">1952</option>
								
								<option value="1953">1953</option>
								<option value="1954">1954</option>
								<option value="1955">1955</option>
								<option value="1956">1956</option>
								<option value="1957">1957</option>
								<option value="1958">1958</option>
								<option value="1959">1959</option>
								<option value="1960">1960</option>
								<option value="1961">1961</option>
								
								<option value="1962">1962</option>
								<option value="1963">1963</option>
								<option value="1964">1964</option>
								<option value="1965">1965</option>
								<option value="1966">1966</option>
								<option value="1967">1967</option>
								<option value="1968">1968</option>
								<option value="1969">1969</option>
								<option value="1970">1970</option>
								
								<option value="1971">1971</option>
								<option value="1972">1972</option>
								<option value="1973">1973</option>
								<option value="1974">1974</option>
								<option value="1975">1975</option>
								<option value="1976">1976</option>
								<option value="1977">1977</option>
								<option value="1978">1978</option>
								<option value="1979">1979</option>
								<option value="1980">1980</option>
								
								<option value="1981">1981</option>
								<option value="1982">1982</option>
								<option value="1983">1983</option>
								<option value="1984">1984</option>
								<option value="1985">1985</option>
								<option value="1986">1986</option>
								<option value="1987">1987</option>
								<option value="1988">1988</option>
								
								<option value="1989">1989</option>
								<option value="1990">1990</option>
								<option value="1991">1991</option>
								<option value="1992">1992</option>
								<option value="1993">1993</option>
								<option value="1994">1994</option>
								<option value="1995">1995</option>
								<option value="1996">1996</option>
								<option value="1997">1997</option>
									
								<option value="1998">1998</option>
								<option value="1999">1999</option>
								<option value="2000">2000</option>
								<option value="2001">2001</option>
								<option value="2002">2002</option>
								<option value="2003">2003</option>
								<option value="2004">2004</option>
								<option value="2005">2005</option>
								<option value="2006">2006</option>
									
								<option value="2007">2007</option>
								<option value="2008">2008</option>
								<option value="2009" selected="selected">2009</option>
							</select>
						</td>
				  	</tr>
					<tr>
						<td colspan="2">Date : <input type="radio" name="dateType" value="all_date" checked="checked">All Date <input type="radio" name="dateType" value="pub_online_date">Publish Online Date <input type="radio" name="dateType" value="circulation_date">Circulation Date</td>
					</tr>
					<tr>
						<td colspan="2">Type : <input type="radio" name="selectType" value="issues_by_ebook" checked="checked">Issues by eBook <input type="radio" name="selectType" value="first_view_articles">First View Articles </td>
					</tr>
					<tr>
						<td class="ebook" colspan="2">&nbsp;</td>
					</tr> 		
					<tr>
						<td class="ebook" colspan="2"><h3>Find eBook by mnemonic</h3>&nbsp;
							<select name="allEBookId" multiple="multiple" size="10" style="width:500px">
								<option value="EPS">.</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
							</select>&nbsp;
						</td>
					</tr> 
					<tr>
						<td class="ebook" colspan="2">
							<input name="addall" type="button" class="button" value="Add All" onclick="mover('addall');">
							<input name="add" type="button" class="button" value="Add" onclick="mover('add');">
							<input name="remove" type="button" class="button" value="Remove" onclick="mover('remove');">
			
							<input name="removeall" type="button" class="button" value="Remove All" onclick="mover('removeall');">
						</td>
					</tr>		
					<tr>
						<td class="ebook" colspan="2">&nbsp;
							<select name="ebookId" multiple="multiple" size="10" style="width:500px"></select>&nbsp;
						</td>
					</tr> 
					<tr>
						<td colspan="2">			
							<input name="cmd" type="submit" class="button" value="Search" onclick="selectJournals();"  />
						</td>
					</tr>
				</table>
			</h:form>
		
		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>