<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Manage System Users</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcomepanel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Manage System Users
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menupanel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Manage System Users</h1>
			<div id="searchresults">
			
			<h:inputHidden id="alertMessage"/>
			<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
				<p class="alert"><h:message for="alertMessage"/><br/></p>
			</h:panelGroup>

				<h:form id="systemUserForm" prependId="false">
					<h:commandButton actionListener="#{systemUserUpdateBean.initNewSystemUser}" action="#{systemUserBean.updateSystemUser}" styleClass="button" value="Add new user"/>
					<h:commandLink id="jsf_submitter" actionListener="#{systemUserBean.setPagerValues}"></h:commandLink>
					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">Sort by</td>
							<td>
    							<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserBean.setSortBy}" value="#{systemUserBean.sortBy}">
    								<f:selectItem itemValue="Username" />
    								<f:selectItem itemValue="Email" />
    								<f:selectItem itemValue="First Name" />
    								<f:selectItem itemValue="Last Name" />
    							</h:selectOneMenu>			
							</td>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{systemUserBean.setPageSize}" value="#{systemUserBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{systemUserBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserBean.setCurrentPage}" value="#{systemUserBean.goToPage}">
									<f:selectItems value="#{systemUserBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{systemUserBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>

					<h:dataTable id="system_users" var="user" value="#{systemUserBean.userList}" cellspacing="0">
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Username"/>
        					</f:facet>
        					<h:commandLink actionListener="#{systemUserUpdateBean.initSystemUser}" action="#{systemUserBean.updateSystemUser}"> 
								<h:outputText value="#{user.username}"/>
        						<f:attribute name="userId" value="#{user.userId}"/>
							</h:commandLink>
						</h:column>
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Email Address"/>
        					</f:facet> 
							<h:outputText value="#{user.email}"/>
						</h:column>
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Last Name"/>
        					</f:facet> 
							<h:outputText value="#{user.lastName}"/>
						</h:column>
						<h:column>
							<f:facet name="header">
        						<h:outputText value="First Name"/>
        					</f:facet> 
							<h:outputText value="#{user.firstName}"/>
						</h:column>
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Active"/>
        					</f:facet> 
							<h:outputText value="#{user.displayActive}"/>
						</h:column>
						<h:column id="delete_column">
							<f:facet name="header">
        						<h:outputText value="Delete"/>
        					</f:facet> 
							<h:selectBooleanCheckbox valueChangeListener="#{systemUserDeleteBean.addToList}">
								<f:attribute name="userId" value="#{user.userId}"/>
								<f:attribute name="username" value="#{user.username}"/>
								<f:attribute name="email" value="#{user.email}"/>
							</h:selectBooleanCheckbox>
						</h:column>
					</h:dataTable>

					<%--table cellspacing="0">
						<tr class="shaded">
							<td class="shaded">Username</td>
							<td class="shaded">Email Address</td>
							<td class="shaded">Last Name</td>

							<td class="shaded">First Name</td>
							<td class="shaded">Active</td>
							<td class="inputs">Delete</td>
						</tr>
						
						<c:forEach var="user" items="${systemUserBean.userList}">
							<tr>
								<td><h:commandLink action="system_users_update">
									${user.username}
								</h:commandLink></td>
								<td>${user.email}</td>
								<td>${user.lastName}</td>
								<td>${user.firstName}</td>
								<td>Yes</td>
								<td class="inputs"><input type="checkbox"/></td>
							</tr>
						</c:forEach>
					</table--%>

					<table cellspacing="0" class="basket_buttons">
						<tr>
							<td><h:commandButton actionListener="#{systemUserDeleteBean.confirmDelete}" action="#{systemUserDeleteBean.deleteSystemUsersNav}" styleClass="button" value="Delete users" /></td>
						</tr>
					</table>

					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">Sort by</td>
							<td>
    							<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserBean.setSortBy}" value="#{systemUserBean.sortBy}">
    								<f:selectItem itemValue="Username" />
    								<f:selectItem itemValue="Email" />
    								<f:selectItem itemValue="First Name" />
    								<f:selectItem itemValue="Last Name" />
    							</h:selectOneMenu>			
							</td>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{systemUserBean.setPageSize}" value="#{systemUserBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{systemUserBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{systemUserBean.setCurrentPage}" value="#{systemUserBean.goToPage}">
									<f:selectItems value="#{systemUserBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{systemUserBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{systemUserBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>
				</h:form>
			</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>