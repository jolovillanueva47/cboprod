<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Search</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Search
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Search</h1>
			<div id="advanced_search">

				<h:form id="search">
				
					<table cellspacing="0">
						<tr>
							<th width="10%" align="left">Author</th>
							<td><h:inputText value="#{searchBean.author}"/></td>
						</tr>
						<tr>
							<th width="10%" align="left">E-ISBN</th>
							<td><h:inputText value="#{searchBean.eisbn}"/></td>
						</tr>
						<tr>
							<th width="10%" align="left">Book Title</th>
							<td><h:inputText value="#{searchBean.bookTitle}"/></td>
						</tr>
						<tr>
							<th width="10%" align="left">Chapter Title</th>
							<td><h:inputText value="#{searchBean.chapterTitle}"/></td>
						</tr>
						<tr>
							<th width="10%" align="left">DOI</th>
							<td><h:inputText value="#{searchBean.doi}"/></td>
						</tr>
						<tr>
							<th width="10%" align="left">Status</th>
							<td>
								<select>
									<option value="">All Status</option>
								</select>
							</td>
						</tr>
					</table>
	
					<table cellspacing="0" class="search_buttons">
						<tr>
							<td>
								<h:commandButton styleClass="button" actionListener="#{searchBean.reset}" value="Reset" />
								<h:commandButton styleClass="button" actionListener="#{searchBean.search}" value="Search" />
							</td>
						</tr>
					</table>
				</h:form>
			</div>
			<br/>
			
			<div id="searchresults">
				<table class="search_navigation" cellspacing="0">
					<tr>
						<td nowrap="nowrap">Sort by</td>
						<td>
				    		<select name="sortBy">
								<option value="BOOK_ID">Book ID</option>
								
								<option value="TITLE">Title</option>
								<option value="AUTHOR">Author</option>
								<option value="FILE_ID">PII</option>
								<option value="DOI">DOI</option>
								<option value="PUBMED_ID">Pubmed ID</option>
								<option value="DOI_BATCH_ID">Crossref Batch ID</option>
							</select>
						</td>

						<td nowrap="nowrap">
							Results per page
						</td>
				
						<td>
				    		<select name="pageSize">
								<option value="10" selected="selected">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>
							</select>
						</td>

						<td class="goto1" nowrap="nowrap">Page 1 of 13 | Go to page</td>

						<td>
				    		<select name="currentPage">
								<option value="1" selected="selected">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
							</select>
						</td>
				
						<td class="goto2" nowrap="nowrap">
							Go to:&nbsp;&nbsp;<a href="#">First</a> | <a href="#">Previous</a> | <a href="#">Next</a> | <a href="#">Last</a>&nbsp;&nbsp;
						</td>
				
					</tr>
				</table>

				<table cellspacing="0">
					<tr class="shaded">
						<td class="shaded" width="15%"><a>Chapter ID</a></td>
						<td class="shaded" width="40%"><a>Title</a></td>
						<td class="shaded" width="15%"><a>Author</a></td>
						<td class="shaded" width="10%"><a>Status</a></td>
						<td class="shaded" width="10%"><a>DOI</a></td>
					</tr>
					<tr>
						<td>88462521</td>
						<td>Ang Alamat ng Kamote</td>
						<td>Galang, Cito</td>
						<td>LIVE</td>
						<td>1133997762541</td>
					</tr>
				</table>
			</div>
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>