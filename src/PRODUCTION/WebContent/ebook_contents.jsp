<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>

<%@page	import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@page import="org.cambridge.ebooks.production.util.HttpUtil"%>
<%@page	import="org.cambridge.ebooks.production.document.EbookChapterDetailsBean"%>
<%@page	import="org.cambridge.ebooks.production.ebook.content.EBookContentManagedBean"%>
<%@page	import="org.cambridge.ebooks.production.ebook.content.EBookContentBean"%>


<c:forEach items="${contentBean.chapterDetails}" var="chapterBeanDetails" varStatus="status">

	<c:if test="${chapterBeanDetails.displayIndented}">
		<div id="part_chapter">
	</c:if>

	<% 	if(EbookDetailsBean.META.equals(request.getParameter(EbookDetailsBean.SHOW))){ %>
	<!-- Metadata -->
	<c:set scope="session" var="chapterBeanDetails" value="${chapterBeanDetails}" />
	
	<f:subview id="content_items">
		<jsp:include page="ebook_chapter_details_content.jsp" flush="true"></jsp:include>
	</f:subview>

	<%}else{ %>
	
	<h3 id="chapter_title">
		<strong>${chapterBeanDetails.title}</strong>
		(<em>${chapterBeanDetails.titleAlphasort}</em>)
	</h3>

	<!-- Keywords -->
	<table id="ebooks_metadata" cellspacing="1" cellpadding="3">
		<tr>
			<th align="right" width="20%" valign="top">type:</th>
			<td width="80%"><c:out value="${chapterBeanDetails.contentType}" /></td>
		</tr>
		<tr>
			<th align="right" width="20%" valign="top">Pages:</th>
			<td><c:out value="${chapterBeanDetails.pageStart}" />&nbsp;- <c:out
				value="${chapterBeanDetails.pageEnd}" /></td>
		</tr>
		<c:if test="${chapterBeanDetails.displayTheme}">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><b>keyword-group source="theme"</b></td>
			</tr>
			<tr>
				<td align="left" valign="top" colspan="2">
				<c:forEach items="${chapterBeanDetails.themeDisplay}" var="themeCol">
					<ul id="theme_list_column">
						<c:forEach items="${themeCol}" var="theme">
							<li><c:out value="${theme}" escapeXml="false" /></li>
						</c:forEach>
					</ul>
				</c:forEach></td>
			</tr>
		</c:if>
		<c:if test="${(chapterBeanDetails.displayKeyword) && (! chapterBeanDetails.displayKeywordsFromCore)}">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><b>keyword-group source="index"</b></td>
			</tr>
			<tr>
				<td align="left" valign="top" colspan="2">
				<c:forEach items="${chapterBeanDetails.keywordDisplay}" var="keyCol">
					<ul id="keywords_list_column">
						
						<c:forEach items="${keyCol}" var="key">
							<li><c:out value="${key}" escapeXml="false" /></li>
						</c:forEach>
					</ul>
				</c:forEach></td>
			</tr>
		</c:if>
		<c:if test="${chapterBeanDetails.displayKeywordsFromCore}">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<c:forEach items="${chapterBeanDetails.keywordsFromCore}" var="keyFromCore">
				<tr>
					<td colspan="2"><b>keyword-group source="<c:out value="${keyFromCore.keywordName}" escapeXml="false"/>"</b></td>
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">
						<ul id = "keywordsFromCore_list_column">
						<c:forEach items="${keyFromCore.keywordText}" var="keyFromCoreText">
							<li><c:out value="${keyFromCoreText }" escapeXml="false" /></li>							
						</c:forEach>
						</ul>
					</td>
				</tr>					
			</c:forEach>
		</c:if>
	</table>
	
	<%}%>

	<div id="backtotop"><a href="#top"> back to top <img border="" src="images/browse_ebooks_backtotop.gif" /> </a></div>

	<c:if test="${contentBean.lastContent}">
		<input type="hidden" id="last_content" value="LAST_CONTENT" />
	</c:if>

	<c:if test="${chapterBeanDetails.displayIndented}">
		</div>
	</c:if>
	
</c:forEach>


