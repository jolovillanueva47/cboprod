<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/production.tld" prefix="gui" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Manage System Users</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
		@import "css/demo_page.css";
		@import "css/demo_table.css";
	-->
	</style>
	
	<script src="<%=request.getContextPath() %>/js/jquery/jquery.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/jquery/jquery.dataTables.js" type="text/javascript"></script>
	
</head>
	
<body id="dt_example">

	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcomepanel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Manage System Users
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menupanel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->

<gui:grid className="org.cambridge.ebooks.production.grid.UserListGridImplementation" 
	dataSource="grid" 
	tableHeaders="Username,Email Address,Last Name,First Name,Active" 
	tableHeaderSizes="20,25,25,15,15">
</gui:grid>

<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>