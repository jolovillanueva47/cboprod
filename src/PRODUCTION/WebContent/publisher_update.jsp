<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Publisher Details</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>	
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcomepanel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<h:form id="publisherUpdateForm" prependId="false">
			<h:commandLink value="Home" action="home" />
			&gt; <h:outputText value="Manage Publishers" escape="false"/>&gt; <h:outputText value="Publisher Details" escape="false"/>
		</h:form>
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menupanel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Publisher Details</h1>
			<div id="searchresults">
			
			<h:inputHidden id="alertMessage"/>
			<h:panelGroup rendered="#{!empty facesContext.maximumSeverity}">
				<p class="alert"><h:message for="alertMessage"/><br/></p>
			</h:panelGroup>
			
				<h:form id="publisherDetailsForm" prependId="false">
					<table cellspacing="0">
					
					<h:panelGroup rendered="#{!empty publisherUpdateBean.publisher.publisherId}">
					<tr>
						<td width="100">Publisher Id</td>
							<td><h:outputText value="#{publisherUpdateBean.publisher.publisherId}" /></td>
					</tr>
					</h:panelGroup>
					
					<tr>
						<td width="100">Publisher Name*</td>
						<td><h:inputText maxlength="50" binding="#{publisherUpdateBean.uiPublisherName}" value="#{publisherUpdateBean.publisher.name}" styleClass="register_field" /></td>

					</tr>
				
					<tr>
						<td width="100">Email*</td>
						<td ><h:inputText maxlength="50" binding="#{publisherUpdateBean.uiEmail}" value="#{publisherUpdateBean.publisher.email}" styleClass="register_field" /> </td>
					</tr>	
 				
					</table>
					
					<h:panelGroup rendered="#{(publisherUpdateBean.updateType eq 'Save') || (empty publisherUpdateBean.publisherIsbnList)}">
					

						<table cellspacing="0">
							<tr>
								<td width="100">ISBN*</td>
								<td width="100"><h:inputText binding="#{publisherUpdateBean.isbnUi}" value="#{publisherUpdateBean.publisher.isbn}" styleClass="register_field" /></td>
								<td>Exclude ISBNs <h:selectBooleanCheckbox value="#{publisherUpdateBean.excludeAllIsbns}"></h:selectBooleanCheckbox></td>
							</tr>
						</table>
	
					
					<table cellspacing="0">
						<tr>
							<td>	
								<h:outputText value="You can also put all the ISBNs at once delimited by comma (,) in the text area below:"/><br/>
								<h:inputTextarea  binding="#{publisherUpdateBean.isbnsUi}" value="#{publisherUpdateBean.publisher.isbns}" cols="80" rows="5" />
							</td>
						</tr>
					</table>
					</h:panelGroup>
					 
				<h:panelGroup rendered="#{!empty publisherUpdateBean.publisherIsbnList}">
					<br />
					<h1>Assigned ISBN</h1>
					<h:commandLink id="jsf_submitter" actionListener="#{publisherUpdateBean.setPagerValues}"></h:commandLink>
					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{publisherUpdateBean.setPageSize}" value="#{publisherUpdateBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{publisherUpdateBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu immediate="true" onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherUpdateBean.setCurrentPage}" value="#{publisherUpdateBean.goToPage}">
									<f:selectItems value="#{publisherUpdateBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{publisherUpdateBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>
					<br/>
					
					
						<table cellspacing="0">
									<tr class="shaded">
										<td class="twentysix">ISBN</td>
										<td class="fiftyone">Delete</td>
										<td class="twentysix">Exclude</td>
									</tr>
									<tr class="shaded">
										<td class="twentysix"></td>
										<td class="fiftyone">
											<input id="selectAllDeletes" onclick="selectAllCheckBox(this.checked, 'isbns', 'delete');" type="checkbox" />
										</td>
										<td class="twentysix">
											<input id="selectAllExcludes" onclick="selectAllCheckBox(this.checked, 'isbns', 'exclude');" type="checkbox" />
										</td>
									</tr>
								</table>
								
					<h:dataTable columnClasses="twentysix,fiftyone,twentysix" cellspacing="0" id="isbns" var="isbn" value="#{publisherUpdateBean.publisherIsbnList}">
						
						<h:column>

							<h:outputText value="#{isbn.isbn}"/>
						</h:column>
						
					
						<h:column id="delete_column">

							<h:selectBooleanCheckbox title="delete" valueChangeListener="#{publisherUpdateBean.addIsbnToList}">
								<f:attribute name="isbnCounter" value="#{isbn.isbnCounter}"/>
								<f:attribute name="isbn" value="#{isbn.isbn}"/>
							</h:selectBooleanCheckbox>
						</h:column>
						
						<h:column id="exclude_column">
	
							<h:selectBooleanCheckbox title="exclude" valueChangeListener="#{publisherUpdateBean.excludeIsbn}" value="#{isbn.excludeBoolean}">
								<f:attribute name="isbnCounter" value="#{isbn.isbnCounter}"/>
								<f:attribute name="isbn" value="#{isbn.isbn}"/>
							</h:selectBooleanCheckbox>
						</h:column>
					</h:dataTable>
		
					
					<table>
							<tr>
								<td class="twentysix"><h:inputText binding="#{publisherUpdateBean.uiPubIsbn}" value="#{publisherUpdateBean.publisher.isbn}" styleClass="register_field"/></td>
								<td class="fiftyone">Exclude ISBNs <h:selectBooleanCheckbox value="#{publisherUpdateBean.excludeAllIsbns}"></h:selectBooleanCheckbox></td>
								<td class="twentysix"></td>
				
							</tr>
						</table>
	
					
					<table>
						<tr>
							<td>	
								<h:outputText value="You can also put all the ISBNs at once delimited by comma (,) in the text area below:"/><br/>
								<h:inputTextarea binding="#{publisherUpdateBean.uiPubIsbns}" value="#{publisherUpdateBean.publisher.isbns}" cols="80" rows="5" />
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
					
					<br/>
					<table class="search_navigation" cellspacing="0">
						<tr>
							<td nowrap="nowrap">
								Results per page
							</td>
							<td>
	    						<h:selectOneMenu onchange="document.getElementById('jsf_submitter').onclick();" 
	    							valueChangeListener="#{publisherUpdateBean.setPageSize}" value="#{publisherUpdateBean.resultsPerPage}">
	    							<f:selectItem itemValue="10" />
	    							<f:selectItem itemValue="20" />
	    							<f:selectItem itemValue="30" />
	    							<f:selectItem itemValue="40" />
	    							<f:selectItem itemValue="50" />
	    						</h:selectOneMenu>
							</td>
							<td class="goto1" nowrap="nowrap">
								<h:outputText value="#{publisherUpdateBean.pagingText}"/>
							 | Go to page</td>
							<td>
								<h:selectOneMenu immediate="true" onchange="document.getElementById('jsf_submitter').onclick();" valueChangeListener="#{publisherUpdateBean.setCurrentPage}" value="#{publisherUpdateBean.goToPage}">
									<f:selectItems value="#{publisherUpdateBean.pages}"/>
								</h:selectOneMenu>
							</td>
							<td class="goto2" nowrap="nowrap">
								Go to:&nbsp;&nbsp;<h:commandLink actionListener="#{publisherUpdateBean.firstPage}">First</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.previousPage}">Previous</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.nextPage}">Next</h:commandLink> | <h:commandLink actionListener="#{publisherUpdateBean.lastPage}">Last</h:commandLink>&nbsp;&nbsp;
							</td>	
						</tr>
					</table>
				</h:panelGroup>
				
				<br/>
				<table cellspacing="0" class="registered_buttons">
					<tr>
						<td>
							<p class="alert">* Required fields</p>
							<h:commandButton immediate="true" action="#{publisherUpdateBean.cancelUpdate}" styleClass="button" value="Cancel" />
							<h:inputHidden id="updateValidator" value="validatePublisherDetails" validator="#{publisherUpdateBean.validateDetails}"/>
							&nbsp;<h:commandButton id="updateButton" actionListener="#{publisherUpdateBean.updatePublisher}" action="#{publisherUpdateBean.updatePublisherNav}" styleClass="button" value="#{publisherUpdateBean.updateType}" />
						</td>
					</tr>
				</table>
				
				</h:form>
			</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>

<script type="text/javascript" src="js/jquery-1.3.2.js"></script>

<script type="text/javascript">
	function selectAllCheckBox(checked, docId, attribute) {
		var checkGroup = document.getElementById(docId).getElementsByTagName('input');
		
		for (i=0;i<checkGroup.length;i++)
		{
			if(checkGroup[i].getAttribute('title') == attribute)
			{
				checkGroup[i].checked = checked;
			}
		}
	}

	
	
	
</script>


</body>

</f:view>

</html>