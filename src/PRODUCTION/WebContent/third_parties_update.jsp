<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Update Third Party</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; <a href="third_parties.jsf">Manage Third Parties</a>
		&gt; Update Third Party Details

	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Update Third Party Details</h1>
			<div id="registered">
				<h:inputHidden id="alertMessage"/>
				<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
				<p class="alert">
					<h:message for="alertMessage"/>
					<h:message for="updateValidator"/><br/></p>
				</h:panelGroup>

				<h:form>
				<table cellspacing="0">
					<tr>
						<td>Name*</td>				
						<td><h:inputText binding="#{thirdPartyUpdateBean.uiName}" value="#{thirdPartyUpdateBean.thirdParty.thirdPartyName}" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Username*</td>
						<td><h:inputText binding="#{thirdPartyUpdateBean.uiUsername}" value="#{thirdPartyUpdateBean.thirdParty.thirdPartyUsername}" maxlength="50" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Password*</td>
						<td><h:inputText binding="#{thirdPartyUpdateBean.uiPassword}" value="#{thirdPartyUpdateBean.displayPassword}" maxlength="50" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>FTP Address*</td>
						<td><h:inputText binding="#{thirdPartyUpdateBean.uiFTPAddress}" value="#{thirdPartyUpdateBean.thirdParty.thirdPartyWebsite}" maxlength="120" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Folder</td>
						<td><h:inputText value="#{thirdPartyUpdateBean.thirdParty.thirdPartyFolder}" maxlength="100" styleClass="register_field" /></td>
					</tr>
					<tr>
						<td>Delivery Type</td>
						<td>
							<h:selectOneMenu value="#{thirdPartyUpdateBean.thirdParty.thirdPartyDeliveryType}">
								<f:selectItem itemLabel="FTP" itemValue="FTP"/>
								<f:selectItem itemLabel="SFTP" itemValue="SFTP"/>
							</h:selectOneMenu>				
						</td>
				
					</tr>	
					<tr>
						<td>Contact Email Address</td>
						<td><h:inputText value="#{thirdPartyUpdateBean.thirdParty.thirdPartyEmail}" maxlength="100" styleClass="register_field" /></td>
					</tr>	
					<tr>
						<td>Active</td>
						<td><h:selectBooleanCheckbox value="#{thirdPartyUpdateBean.thirdParty.active}" /></td>
					</tr>
				
					<tr>
						<td>Inactive Date</td>
						<td><h:inputText binding="#{thirdPartyUpdateBean.uiInactiveDate}" value="#{thirdPartyUpdateBean.thirdParty.displayInactiveDate}" maxlength="10" /></td>
					</tr>
					
					<tr class="border">
						<td colspan="2"><h4>Content*</h4></td>
					</tr>
					
					<tr>
						<td>PDF</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiPDF}" value="#{thirdPartyUpdateBean.thirdParty.content.hasPDF}"/></td>
					</tr>
				
					<tr>
						<td>SGML Header</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiSGML_H}" value="#{thirdPartyUpdateBean.thirdParty.content.hasSGML_H}" /></td>
					</tr>
				
					<tr>
						<td>SGML Fulltext</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiSGML_S}" value="#{thirdPartyUpdateBean.thirdParty.content.hasSGML_S}" /></td>
					</tr>
					
					<tr>
						<td>XML Header</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiXML_H}" value="#{thirdPartyUpdateBean.thirdParty.content.hasXML_H}" /></td>
					</tr>
					
					<tr>
						<td>XML Fulltext</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiXML_W}" value="#{thirdPartyUpdateBean.thirdParty.content.hasXML_W}" /></td>
					</tr>
					
					<tr>
						<td>HTML Fulltext</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiHTML_W}" value="#{thirdPartyUpdateBean.thirdParty.content.hasHTML_W}" /></td>
					</tr>
					
					<tr>
						<td>Reference</td>
						<td><h:selectBooleanCheckbox binding="#{thirdPartyUpdateBean.uiXML_R}" value="#{thirdPartyUpdateBean.thirdParty.content.hasXML_R}" /></td>
					</tr>
					
					<tr class="border">
						<td colspan="2"><h4>Compression</h4></td>
					</tr>
	
					<tr>
						<td>
							<h:selectOneMenu value="#{thirdPartyUpdateBean.thirdParty.thirdPartyCompression}">
								<f:selectItem itemLabel="ZIP" itemValue="ZIP"/>
								<f:selectItem itemLabel="TAR" itemValue="TAR"/>
							</h:selectOneMenu>				
						</td>
					</tr>
		
					<tr class="border">
						<td colspan="2">
							<h:commandButton immediate="true" action="third_party" styleClass="button" value="Cancel" />
							<h:inputHidden id="updateValidator" value="C2" validator="#{thirdPartyUpdateBean.validateDetails}"/>
							&nbsp;<h:commandButton actionListener="#{thirdPartyUpdateBean.updateThirdParty}" action="#{thirdPartyUpdateBean.updateThirdPartyNav}" styleClass="button" value="#{thirdPartyUpdateBean.updateType}" />
						</td>
					</tr>
		
					<tr class="border">
						<td><h4>eBooks For Delivery*</h4></td>
						<td>&nbsp;</td>
					</tr>
		
					<tr>
						<td>&nbsp;</td>
						<td>
							<table cellspacing="0">
				
								<tr class="border">
									<td><a href="javascript:orderBy('mnemonic');">By mnemonic</a></td>
									<td><a href="javascript:orderBy('title');">By title</a></td>
									<td><input type="checkbox" name="art_select_all" onclick="javascript: toggle('thirdPartiesForm', this, 'art')"/>&nbsp;Issue</td>
									<td><input type="checkbox" name="inc_select_all" onclick="javascript: toggle('thirdPartiesForm', this, 'inc')"/>&nbsp;Forthcoming</td>
									<td>Embargo</td>
								</tr>	
								
								<tr class="border">
									<td>[AFH]</td>
									<td>Triumph Forsaken</td>
									<td><input type="checkbox" name="art_AFH" value=""  /></td>
									<td><input type="checkbox" name="inc_AFH" value=""  /></td>
									<td>
										<select name="embargo_AFH" size="1">
				
												<option value="0" selected="selected"></option>
												<option value="1" >1</option>
												<option value="2" >2</option>
												<option value="3" >3</option>
												<option value="4" >4</option>
												<option value="5" >5</option>
				
												<option value="6" >6</option>
												<option value="7" >7</option>
												<option value="8" >8</option>
												<option value="9" >9</option>
												<option value="10" >10</option>
												<option value="11" >11</option>
				
												<option value="12" >12</option>
											</select>
										</td>
									</tr>
								
									<tr class="border">
										<td>[ATM]</td>
										<td>The Global Cold War</td>
				
										<td><input type="checkbox" name="art_ATM" value=""  /></td>
										<td><input type="checkbox" name="inc_ATM" value=""  /></td>
										<td>
											<select name="embargo_ATM" size="1">
												<option value="0" selected="selected"></option>
												<option value="1" >1</option>
												<option value="2" >2</option>
												<option value="3" >3</option>
				
												<option value="4" >4</option>
												<option value="5" >5</option>
												<option value="6" >6</option>
												<option value="7" >7</option>
												<option value="8" >8</option>
												<option value="9" >9</option>
				
												<option value="10" >10</option>
												<option value="11" >11</option>
												<option value="12" >12</option>
											</select>
										</td>
									</tr>
								
									<tr class="border">
				
										<td>[ABA]</td>
										<td>The Study of Language</td>
										<td><input type="checkbox" name="art_ABA" value="Y" checked="checked" /></td>
										<td><input type="checkbox" name="inc_ABA" value=""  /></td>
										<td>
											<select name="embargo_ABA" size="1">
												<option value="0" selected="selected"></option>
												<option value="1" >1</option>
				
												<option value="2" >2</option>
												<option value="3" >3</option>
												<option value="4" >4</option>
												<option value="5" >5</option>
												<option value="6" >6</option>
												<option value="7" >7</option>
				
												<option value="8" >8</option>
												<option value="9" >9</option>
												<option value="10" >10</option>
												<option value="11" >11</option>
												<option value="12" >12</option>
											</select>
				
										</td>
									</tr>
								
									<tr class="border">
										<td>[ASO]</td>
										<td>Exiles and Pioneers</td>
				
										<td><input type="checkbox" name="art_ASO" value=""  /></td>
										<td><input type="checkbox" name="inc_ASO" value=""  /></td>
										<td>
											<select name="embargo_ASO" size="1">
												<option value="0" selected="selected"></option>
												<option value="1" >1</option>
												<option value="2" >2</option>
												<option value="3" >3</option>
				
												<option value="4" >4</option>
												<option value="5" >5</option>
												<option value="6" >6</option>
												<option value="7" >7</option>
												<option value="8" >8</option>
												<option value="9" >9</option>
				
												<option value="10" >10</option>
												<option value="11" >11</option>
												<option value="12" >12</option>
											</select>
										</td>
									</tr>
								
									<tr class="border">
				
										<td>[AKO]</td>
										<td>Applied Multilevel Analysis</td>
										<td><input type="checkbox" name="art_AKO" value="Y" checked="checked" /></td>
										<td><input type="checkbox" name="inc_AKO" value=""  /></td>
										<td>
											<select name="embargo_AKO" size="1">
												<option value="0" selected="selected"></option>
												<option value="1" >1</option>
				
												<option value="2" >2</option>
												<option value="3" >3</option>
												<option value="4" >4</option>
												<option value="5" >5</option>
												<option value="6" >6</option>
												<option value="7" >7</option>
				
												<option value="8" >8</option>
												<option value="9" >9</option>
												<option value="10" >10</option>
												<option value="11" >11</option>
												<option value="12" >12</option>
											</select>
				
										</td>
									</tr>
								
								<tr class="border">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
				
									<td><input type="checkbox" name="art_select_all" onclick="javascript: toggle('thirdPartiesForm', this, 'art')"/>&nbsp;Select All</td>
									<td><input type="checkbox" name="inc_select_all" onclick="javascript: toggle('thirdPartiesForm', this, 'inc')"/>&nbsp;Select All</td>
								</tr>	
							</table>
						</td>
					</tr>		
					
				</table>
				
				<table cellspacing="0" class="registered_buttons">
					<tr>
						<td>
							<p class="alert">* Required fields</p>
							<input name="command" type="submit" class="button" value="Cancel" />
							<input name="command" type="submit" class="button" value="Update"/>
						</td>
					</tr>
				</table>
				
				</h:form>
			</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>