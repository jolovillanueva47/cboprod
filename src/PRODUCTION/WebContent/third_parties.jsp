<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Manage Third Parties</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Manage Third Parties
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Manage Third Parties</h1>
			<div id="searchresults">

				<h:inputHidden id="alertMessage"/>
				<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
					<p class="alert"><h:message for="alertMessage"/><br/></p>
				</h:panelGroup>

				<h:form>

					<h:commandButton styleClass="button" actionListener="#{thirdPartyUpdateBean.initNewThirdParty}" action="#{thirdPartyBean.updateThirdParty}" value="Add new third party" ></h:commandButton>
					<br/><br/>

					<h:dataTable var="thirdParty" value="#{thirdPartyBean.thirdPartyList}" cellspacing="0">
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Name"/>
        					</f:facet>
        					<h:commandLink actionListener="#{thirdPartyUpdateBean.initThirdParty}" action="#{thirdPartyBean.updateThirdParty}">
        						<h:outputText value="#{thirdParty.thirdPartyName}"/>
        						<f:attribute name="thirdParty" value="#{thirdParty}"/>
							</h:commandLink>
						</h:column>
						<h:column>
							<f:facet name="header">
        						<h:outputText value="Delete"/>
        					</f:facet>
        					<h:selectBooleanCheckbox valueChangeListener="#{thirdPartyDeleteBean.addToList}">
								<f:attribute name="thirdParty" value="#{thirdParty}"/>
							</h:selectBooleanCheckbox>
						</h:column>
					</h:dataTable>		
					
					<!-- end basket -->
	
					<table cellspacing="0" class="basket_buttons">
						<tr>
							<td>
								<h:commandButton actionListener="#{thirdPartyDeleteBean.confirmDelete}" action="#{thirdPartyDeleteBean.deleteThirdPartyNav}" styleClass="button" value="Delete third parties" />
							</td>
						</tr>
					</table>
					<!-- end basket_buttons -->
	
				</h:form>
			</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>