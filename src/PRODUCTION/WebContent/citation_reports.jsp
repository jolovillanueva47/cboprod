<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Run Citation Reports</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Run Citation Reports
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Run Citation Reports</h1>
			<div id="advanced_search">

			<h:form>
				<table cellspacing="0">
					<tr>
						<td>Report Type</td>
						<td class="fields">
						<select name="reportType" size="1"><option value="Deliveries" selected="selected">Deliveries</option></select>
						</td>
					</tr>

					<tr>
						<td>From</td>
						<td>
							<select name="monthFrom" size="1"><option value="01" selected="selected">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>

      						<select name="yearFrom" size="1"><option value="2005">2005</option>
								<option value="2006">2006</option>
								<option value="2007">2007</option>
								<option value="2008">2008</option>
								<option value="2009" selected="selected">2009</option>
								<option value="2010">2010</option>
							</select>
						</td>
  					</tr>
  					
  					<tr>
						<td>To</td>
						<td>
							<select name="monthTo" size="1">
								<option value="01" selected="selected">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>

							<select name="yearTo" size="1"><option value="2005">2005</option>
								<option value="2006">2006</option>
								<option value="2007">2007</option>
								<option value="2008">2008</option>
								<option value="2009" selected="selected">2009</option>
								<option value="2010">2010</option>
							</select>
						</td>
					</tr>
	
					<tr>
						<td>Organisation</td>
						<td>
							<select name="organisation" size="1">
								<option value="3rdparty" selected="selected">3rd Parties</option>
								<option value="crossref">CrossRef</option>
								<option value="pubmed">PubMed</option>
							</select>
						</td>
					</tr>
	
					<tr>
						<td>eBooks</td>
						<td>
							<select name="ebooks" multiple="multiple" size="10">
								<option value="ALL_EBOOKS" selected="selected">All eBooks</option>
								<option value="EPS">.</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
								<option value="AAA">Triumph Forsaken</option>
								<option value="AAA">The Global Cold War</option>
								<option value="AAA">The Study of Language</option>
								<option value="AAA">Exiles and Pioneers</option>
								<option value="AAA">Applied Multilevel Analysis</option>
							</select>
						</td>
  					</tr>
  					
  					<tr>
						<td>Status</td>
						<td>
							<select name="status" multiple="multiple" size="3">
								<option value="2" selected="selected">Pending</option>
								<option value="1">Successful</option>
								<option value="0">Failed</option>
							</select>
						</td>
  					</tr>
  					
				</table>
				
				<!-- end advanced_search fields -->
				<table cellspacing="0" class="search_buttons">
					<tr>
						<td>
							<input name="reset" type="reset" class="button" value="Reset" />
							<input name="command" type="submit" class="button" value="Run report" />
						</td>
					</tr>
				</table>
				
				<!-- end basket_buttons -->

			</h:form>
			</div>
		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>