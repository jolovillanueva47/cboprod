var fileCount = 0;
var completedFileCount = 0;
var errorFileCount = 0;

// www.sean.co.uk
function pausecomp(millis){
	var date = new Date();
	var curDate = null;

	do { curDate = new Date(); }
	while(curDate-date < millis);
} 

function setTotalFilesOutput(count) {
	$("uploadStatus").getElement(".total-files").set("html", "Total files for upload: " + count);
}

function setCompletedFilesOutput(count) {
	$("uploadStatus").getElement(".completed-files").set("html", "Completed files: " + count);
}

function setErrorFilesOutput(count) {
	$("uploadStatus").getElement(".error-files").set("html", "Error files: " + count);
}

var Uploader = new Class({	
	Extends: FancyUpload2,
	allowScriptAccess: 'always',
	typeFilter: {"Archive files (*.zip)": "*.zip"},
	onSelect: function(file, index, length) {		
		var fileSizeMb = file.size / 1048576;
		var maxFileUploadSize = 1999;
		
		// Check file size
		if(fileSizeMb.round(1) > maxFileUploadSize) {
			alert("Upload error: " + file.name 
					+ "\nFile upload size must not exceed " + maxFileUploadSize + " MB");
			
			return false;
		} 		
		this.parent(file, index, length);
		fileCount++;
	}
});

window.addEvent('load', function() {
	var swiffy = new Uploader($('uploadStatus'), $('uploadList'), {
		'url': $('uploadForm').action,
		'fieldName': 'Filedata',
		'path': 'swf/Swiff.Uploader.swf',
		'limitFiles': 0,		
		
		// methods		
		fileComplete: function(file, response) {
			this.options.processResponse || this
			var json = $H(JSON.decode(response, true));
			if (json.get("result") == "success") {
				file.element.addClass("file-success");
				file.info.set("html", json.get("response"));					
				
				var previousMessage = file.info.get("html");
				
				// unzip checker request
				new Request({  
					method: "post",  
					url: "php/process_checker.php",	
					onRequest: function() { 
						file.info.set("html", previousMessage + "Unzipping file. Please wait..."); 
					},
					onSuccess: function(responseText, responseXML) {
						file.info.set("html", responseText);											
						
						var isTimeout = false;
						
						// validate contents checker request				
						var validator = new Request({  
							method: "post",  
							url: "contents_validator",
							//timeout: 15 * 1000,
							//onTimeout: function() {
							//	file.info.set("html", previousMessage + "Request timeout");							
							//	isTimeout = true;						
								//file.element.removeClass('file-success');
								//file.element.addClass('file-failed');								
							//	validator.send("filename=" + file.name);
							//},
							onRequest: function() { 								
								//if(!isTimeout) {
									previousMessage = previousMessage + file.info.get("html");
									file.info.set("html", previousMessage + "Validating contents. Please wait...");
								//} else {
								//	file.info.set("html", previousMessage + "Validating contents. Please wait...");
								//}
							},
							onSuccess: function(responseText, responseXML) {
								file.info.set("html", previousMessage + responseText);		
								//if(isTimeout) {
								//	file.element.removeClass('file-failed');
								//	file.element.addClass('file-success');
								//}
								completedFileCount++;
								setCompletedFilesOutput(completedFileCount);
							},
							onFailure: function(xhr) {
								file.info.set("html", previousMessage + xhr.status + " " + xhr.statusText);
								if(xhr.status > 0) {								
									errorFileCount++;									
									setErrorFilesOutput(errorFileCount);
								} else {
								//	file.element.removeClass('file-failed');
								//	file.element.addClass('file-success');
								}
								//if(!isTimeout) {
									file.element.addClass('file-failed');
								//}
							}
						}).send("filename=" + file.name);  
					},
					onFailure: function(xhr) {
						file.info.set("html", previousMessage + xhr.status + " " + xhr.statusText);	
						file.element.addClass('file-failed');			
						errorFileCount++;
						setErrorFilesOutput(errorFileCount);
					}
				}).send("dummyFile=" + json.get("dummyFile")); 
				
				//pausecomp(5 * 1000); // 5 secs process freeze
			} else {				
				file.element.addClass("file-failed");
				file.info.set("html", json.get("response") || response);
				errorFileCount++;
				setErrorFilesOutput(errorFileCount);
			}						
		},
		fileRemove: function(file) {
			file.element.fade('out').retrieve('tween').chain(Element.destroy.bind(Element, file.element));
			fileCount--;
			setTotalFilesOutput(fileCount);
		},
		
		// events
		onAllSelect: function(files, current, overall) {
			setTotalFilesOutput(fileCount);
		},
		onError: function(file, error, info) {
			errorFileCount++;
			setErrorFilesOutput(errorFileCount);
		},
		
		// The changed parts!
		'debug': true, // enable logs, uses console.log
		'target': 'browse' // the element for the overlay (Flash 10 only)
	});
	
	/**
	 * Various interactions
	 */
	$('browse').addEvent('click', function() {		
		/**
		 * Doesn't work anymore with Flash 10: swiffy.browse();
		 * FancyUpload moves the Flash movie as overlay over the link.
		 * (see option "target" above)
		 */
		swiffy.browse({"Archive files (*.zip)": "*.zip"});
		return false;
	});

	$('clear').addEvent('click', function() {
		swiffy.removeFile();
		swiffy.currentTitle.set('html', 'File Progress');
		swiffy.currentProgress.set(0);
		swiffy.overallTitle.set('html', 'Overall Progress');
		swiffy.overallProgress.set(0);
		swiffy.currentText.set('html','');
		setTotalFilesOutput(0);
		setCompletedFilesOutput(0);
		setErrorFilesOutput(0);
		fileCount = 0;
		completedFileCount = 0;
		errorFileCount = 0;
		return false;
	});

	$('upload').addEvent('click', function() {
		swiffy.upload();
		return false;
	});
});