/**
 * GLOBALS
 */

/**
 * APPLET EVENTS
 * -----------------------------------------------------------------------
 */

/**
 * file added notification
 */
function uploaderFileAdded(uploader, file) {
	var error = getUploader().startUpload();
	if(error != null) {
		if(error != "Can't start upload") {
			alert(error);
		}
	}
}

/**
 * file status changed notification
 */
function uploaderFileStatusChanged(uploader, file) {
	if(file.getStatus() == 2) { // finish
		// do validation
		//var json = $H(JSON.decode(file.getResponseContent(), true));
		var json = file.getResponseContent().evalJSON();
		// unzip checker request
		new Ajax.Request("php/process_checker.php?dummyFile=" + json.dummyFile, {
			method: "post",
			onSuccess: function(transport) {
				new Ajax.Request("contents_validator?filename=" + file.getName(), {
					method: "post",
					onSuccess: function(transport) {	
						getUploaderLog().value = getUploaderLog().value + transport.responseText.replace(/<br>/g, "\n");
						getUploaderLogHidden().value = getUploaderLogHidden().value + transport.responseText;
					},
					onFailure: function(oXHR, oJson) {
						getUploaderLog().value = getUploaderLog().value 
						+ "Error: " + file.getName() + "\n" 
						+ oXHR.status + ": " + oXHR.statusText.replace(/<br>/g, "\n") + "\n";
						
						getUploaderLogHidden().value = getUploaderLogHidden().value
						+ "Error: " + file.getName() + "<br>" 
						+ oXHR.status + ": " + oXHR.statusText + "<br>";
					}
				});
			},
			onFailure: function(oXHR, oJson) {
				getUploaderLog().value = getUploaderLog().value 
				+ "Error: " + file.getName() + "\n" 
				+ oXHR.status + ": " + oXHR.statusText.replace(/<br>/g, "\n") + "\n";
				
				getUploaderLogHidden().value = getUploaderLogHidden().value
				+ "Error: " + file.getName() + "<br>" 
				+ oXHR.status + ": " + oXHR.statusText + "<br>";
			}
		});

//		new Request({  
//			method: "post",  
//			url: "php/process_checker.php",	
//			onRequest: function() { 
//				 
//			},
//			onSuccess: function(responseText, responseXML) {					
//				// validate contents checker request				
//				var validator = new Request({  
//					method: "post",  
//					url: "contents_validator",
//					timeout: 15 * 1000,
//					onTimeout: function() {													
//						validator.send("filename="+file.name+"&timeoutFlag=-1");
//					},
//					onRequest: function() { 
//					},
//					onSuccess: function(responseText, responseXML) {						
//						$("uploader-log").update($("uploader-log").innerHTML + responseText);
//						getUploaderLog().value = $("uploader-log").innerHTML;
//						//getUploaderLog().value = getUploaderLog().value + responseText;
//					},
//					onFailure: function(xhr) {
//						if(xhr.status > 0) {
//							$("uploader-log").update($("uploader-log").innerHTML
//									+ "Error: " + file.getName() + "<br />" 
//									+ xhr.status + ": " + xhr.statusText + "<br />");
//							
//							getUploaderLog().value = $("uploader-log").innerHTML;
//							
//							//getUploaderLog().value = getUploaderLog().value 
//							//+ "Error: " + file.getName() + "\n" 
//							//+ xhr.status + ": " + xhr.statusText + "\n"
//						}						
//					}
//				}).send("filename=" + file.getName());  
//			},
//			onFailure: function(xhr) {
//				$("uploader-log").update($("uploader-log").innerHTML
//						+ "Error: " + file.getName() + "<br />" 
//						+ xhr.status + ": " + xhr.statusText + "<br />");
//				
//				getUploaderLog().value = $("uploader-log").innerHTML;
//				//getUploaderLog().value = getUploaderLog().value 
//				//+ "Error: " + file.getName() + "\n" 
//				//+ xhr.status + ": " + xhr.statusText + "\n"
//			}
//		}).send("dummyFile=" + json.get("dummyFile")); 
	}
}

function appletInitialized(applet) {
	getViewConfig().setUploadViewStartActionVisible(false);
	getUploadView().updateView()
}

/**
 * HTML
 * ------------------------------------------------------------------------
 */

function getApplet() {
	return document.getElementById("jumpLoaderApplet");
}

function getUploader() {	
	return getApplet().getUploader();
}

function getViewConfig() {
	return getApplet().getViewConfig();
}

function getMainView() {
	return getApplet().getMainView();
}

function getUploadView() {
	return getMainView().getUploadView();
}

function getUploaderLog() {
	return document.getElementById("uploader-log");
}

function getUploaderLogHidden() {
	return document.getElementById("uploader-log-hidden");
}
