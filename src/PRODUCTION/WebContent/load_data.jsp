<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Load Data</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
		
		#printLink {
			display: block !important; 
			text-align: right !important;
		}
		
	</style>		
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; Load Data
	</div>
	
</div>
	
<!-- Menu -->

<f:subview id="menu_panel">
	<c:import url="components/navigation.jsp"></c:import>
</f:subview>
	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Load Data Files</h1>
			<form name="uploaderLogForm" id="uploaderLogForm" action="view_uploader_logs.jsp" method="post" target="_print">
				<div id="advanced_search">
					<table cellspacing="0" width="100%">						
						<tr>
							<td style="width: 630px !important; height: 400px;">
								<applet id="jumpLoaderApplet"
										code="jmaster.jumploader.app.JumpLoaderApplet.class"
										archive="jumploader_z.jar"
										width="100%"
										height="100%" 
										mayscript="">
										
									<param name="uc_uploadUrl" value="php/partitioned_upload.php" />
									<param name="uc_autoRetryCount" value="20" />
									<param name="uc_partitionLength" value="2097152" />
									<!-- param name="uc_uploadThreadCount" value="3" / -->
									<param name="uc_maxTransferRate" value="17179869184" />
									<param name="uc_fileNamePattern" value="^.+\.(?i)((zip))$" />
									<param name="ac_fireAppletInitialized" value="true"/>								
									<param name="ac_fireUploaderFileAdded" value="true" />
									<param name="ac_fireMainViewMessageShown" value="true" />
									<param name="ac_fireUploaderFileStatusChanged" value="true"/>
									<param name="ac_fireUploaderFileRemoved" value="true"/>												
									<param name="vc_lookAndFeel" value="system" />	
									<param name="vc_uploadListViewName" value="_compact" />
	       							<param name="vc_useThumbs" value="false" />												
									<param name="vc_fileNamePattern" value="^.+\.(?i)((zip))$" />											
								</applet>
							</td>
							<td>								
								<table cellspacing="0" width="100%">	
									<tr>	
										<div style="border: 2px solid #cccccc; height: 400px; overflow: auto;">
											<a id="printLink" href="#" onclick="javascript:document.uploaderLogForm.submit();">
												<img src="images/button/btn-print-page.gif" />
											</a>
											<div id="uploader-log">
											</div>
										</div>
										<!-- div id="uploader-log" style="border: 2px solid #cccccc; height: 400px; overflow: auto;">
										</div -->
										<!-- textarea name="uploader-log" id="uploader-log" 
										rows="30" cols="150" wrap="hard" 
										readonly="true" style="width: 100% !important; height: 100% !important;"></textarea-->								
									</tr>									
								</table>																									
							</td>
						</tr>						
					</table>
					<textarea name="uploader-log-hidden" id="uploader-log-hidden" style="visibility: hidden;"></textarea>
				</div>		
			</form>			
		</td>
	</tr></tbody>
</table>
<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>


<script type="text/javascript" src="js/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>

<script type="text/javascript">

/**
 * file added notification
 */
function uploaderFileAdded(uploader, file) {	
	//if(uploader.isReady()) alert('READY');
	//1.check if book is assigned to user 
	//2.check status of book if NOTLOADED, REJECTED, DISAPPROVED or RELOAD
	
	var ajaxresponse;
	$.ajax({
		url:"uploadFileValidator?isbn=" + file.getName().replace('.zip', '') + 
				"&userid=<%=org.cambridge.ebooks.production.util.HttpUtil.getUserFromSession(session).getUserId()%>"
				,
		async:false,
		success:function(msg){	
					ajaxresponse = msg.split(':');			
					if(ajaxresponse[0] == 'CANNOT_BE_UPLOADED')
					{
						getUploader().removeFile(file);
						var message = document.getElementById('uploader-log').innerHTML + '<br />' + file.getName() + ' Removed: ' + ajaxresponse[1];
						document.getElementById('uploader-log').innerHTML = message;
						return false;
					}
				},
		error:function(xhr, ajaxOptions, thrownError){
	          	alert(xhr.status + " : " + thrownError);
	             error = uploader.stopUpload();
					if(error != null)
					{
						alert(error);
					}
	      		}
	});
	
}


/**
 * file status changed notification
 */
function uploaderFileStatusChanged(uploader, file) {
	//alert("b file.getStatus() == "+file.getStatus());
	if(file.getStatus() == 2) { // finish
		//alert('pasok rawr');
		var ajaxresponse;
		$.ajax({
			url:"uploadPostCheck?isbn=" + file.getName() + "&fileid=" + file.getId() + 
					"&userid=<%=org.cambridge.ebooks.production.util.HttpUtil.getUserFromSession(session).getUserId()%>"
					,
			success:function(msg){
					
				ajaxresponse = msg;
					
				var message = document.getElementById('uploader-log').innerHTML + '<br />' + file.getName() + ' : ' + ajaxresponse;
				document.getElementById('uploader-log').innerHTML = message;
				return false;
					
				},
			error:function(xhr, ajaxOptions, thrownError){
		          	alert(xhr.status + " : " + thrownError);
		             error = uploader.stopUpload();
						if(error != null)
						{
							alert(error);
						}
		      		}
		});
		
		
		// do validation
		var json = file.getResponseContent().evalJSON();	
		
		new Ajax.PeriodicalUpdater("uploader-log", "php/output_checker.php?filename=" + file.getName(), {
			method: "post",
			insertion: Insertion.Bottom,
			frequency: 1,
			decay: 2, 
			onSuccess: function(transport) {	
				var message = document.getElementById('uploader-log').innerHTML + transport.responseText;
				document.getElementById('uploader-log').innerHTML = message;
				getUploaderLogHidden().value = document.getElementById('uploader-log').innerHTML;
				
				//alert('Here: ' + document.getElementById('uploader-log').innerHTML);
			}
		});


//	$.ajax({
//		  url:"php/upload_details.php?filename=" + file.getName() + "&userid=<%=org.cambridge.ebooks.production.util.HttpUtil.getUserFromSession(session).getUserId()%>" +
//	      "&username=<%=org.cambridge.ebooks.production.util.HttpUtil.getUserFromSession(session).getUsername()%>",
//	  type:"post",
//	  async:false,
//	  success:function(msg){
//	  		  },
//	  error:function(xhr, ajaxOptions, thrownError){
          	
 //             error = uploader.stopUpload();
//				if(error != null)
//				{
//					alert(error);
//				}
 //   		}    
//	});
		
	

	
	//new Ajax.Request("php/output_checker.php?filename=" + file.getName(), {
	//	method: "post",
	//	onSuccess: function(transport) {	
	//		getUploaderLog().value = getUploaderLog().value + transport.responseText.replace(/<br>/g, "\n");
	//		getUploaderLogHidden().value = getUploaderLogHidden().value + transport.responseText;
	//	},
	//	onFailure: function(oXHR, oJson) {
	//		getUploaderLog().value = getUploaderLog().value 
	//		+ "Error: " + file.getName() + "\n" 
	//		+ oXHR.status + ": " + oXHR.statusText.replace(/<br>/g, "\n") + "\n";
	//		
	//		getUploaderLogHidden().value = getUploaderLogHidden().value
	//		+ "Error: " + file.getName() + "<br>" 
	//		+ oXHR.status + ": " + oXHR.statusText + "<br>";
	//	}
	//});
	// unzip checker request
	//new Ajax.Request("php/process_checker.php?dummyFile=" + json.dummyFile, {
	//	method: "post",
	//	onSuccess: function(transport) {			
	//		new Ajax.Request("contents_validator?filename=" + file.getName(), {
	//			method: "post",
	//			onSuccess: function(transport) {	
	//				getUploaderLog().value = getUploaderLog().value + transport.responseText.replace(/<br>/g, "\n");
	//				getUploaderLogHidden().value = getUploaderLogHidden().value + transport.responseText;
	//			},
	//			onFailure: function(oXHR, oJson) {
	//				getUploaderLog().value = getUploaderLog().value 
	//				+ "Error: " + file.getName() + "\n" 
	//				+ oXHR.status + ": " + oXHR.statusText.replace(/<br>/g, "\n") + "\n";
	//				
	//				getUploaderLogHidden().value = getUploaderLogHidden().value
	//				+ "Error: " + file.getName() + "<br>" 
	//				+ oXHR.status + ": " + oXHR.statusText + "<br>";
	//			}
	//		});
	//	},
	//	onFailure: function(oXHR, oJson) {
	//		getUploaderLog().value = getUploaderLog().value 
	//		+ "Error: " + file.getName() + "\n" 
	//		+ oXHR.status + ": " + oXHR.statusText.replace(/<br>/g, "\n") + "\n";
	//		
	//		getUploaderLogHidden().value = getUploaderLogHidden().value
	//		+ "Error: " + file.getName() + "<br>" 
	//		+ oXHR.status + ": " + oXHR.statusText + "<br>";
	//	}
	//});
	}
}

function appletInitialized(applet) {
	//getViewConfig().setUploadViewStartActionVisible(false);
	//getUploadView().updateView();	
}

function getApplet() {
	return document.getElementById("jumpLoaderApplet");
}

function getUploader() {	
	return getApplet().getUploader();
}

function getViewConfig() {
	return getApplet().getViewConfig();
}

function getMainView() {
	return getApplet().getMainView();
}

function getUploadView() {
	return getMainView().getUploadView();
}

function getUploaderLog() {
	return document.getElementById("uploader-log");
}

function getUploaderLogHidden() {
	return document.getElementById("uploader-log-hidden");
}
function uploaderFileRemoved(uploader, uf){
//	alert('file: ' + uf.getName());
}
</script>
</body>
</f:view>
</html>