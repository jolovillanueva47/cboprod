<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>View References</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
<body>	
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />	
	</div>
	<table id="content" cellspacing="0"><tbody>
		<tr><td id="centre">
			<a id="top"></a>
			<h1>References</h1>
			
			<div id="browse_ebooks">
				<c:forEach items="${viewReferencesBean.referenceList}" var="reference">
					<h3 id="chapter_title"><strong>${reference.contentTitle}</strong></h3>
					<p><b>Type:</b>&nbsp;${reference.contentType}</p>
					<c:choose>
						<c:when test="${reference.referenceFileExists}" >
								<!-- 
									<p>
										<c:if test="${not empty reference.referenceTitle}"><b>Reference Title:</b>&nbsp;${reference.referenceTitle}<br/></c:if>
										<b>Reference Type:</b>&nbsp;${reference.referenceType}
									</p>
								 -->
							<f:subview id="reference">
								<c:set var="content_url" scope="page" value='<%=System.getProperty("content.url.absolute")%>'/>
								<c:import url="${pageScope.content_url}${reference.isbnBreakdown}/${reference.referencesFile}"></c:import>
							</f:subview>
						</c:when>
						<c:otherwise>
							No references.
						</c:otherwise>
					</c:choose>
					<div id="backtotop"><a href="#top">back to top <img src="images/browse_ebooks_backtotop.gif" border="" /></a></div>
				</c:forEach>
			</div>
		</td></tr>
	</tbody></table>
</body>
</f:view>

</html>
