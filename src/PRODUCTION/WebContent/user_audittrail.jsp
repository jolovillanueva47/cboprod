<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>


<f:view>

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>User Audit Trail</title>
	<style type="text/css">
		<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
		-->
	</style>
	</head>

	<body>

	<!-- Left header page information -->

	<a name="top"></a>

	<div id="strip">
		<f:subview id="welcome_panel">
			<c:import url="components/welcome.jsp"></c:import>
		</f:subview> <!-- Crumbtrail -->

		<div id="location">
			<a href="home.jsf">Home</a> 
			&gt; User Audit Trail
		</div>

	</div>


	<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	<h:form id="articleAuditTrailForm">
		<!-- main content -->
		<table id="content" cellspacing="0">
			<tbody>
				<tr>
					<td id="centre"><div id="browse_ebooks">
					<!-- Body -->
					<h1>User Audit Trail</h1>
					<div id="browse_tabs">
					<ul>
						<li>
							<h:commandLink action="ebook_audittrail" actionListener="#{ebookAuditTrailBean.initEBookAuditList}">
								<h:outputText value="By Book"></h:outputText>
							</h:commandLink>
						</li>
						<li>
							<h:commandLink action="book_audittrail" actionListener="#{bookAuditTrailBean.initBookAuditList}">
								<h:outputText value="By Content"></h:outputText>
							</h:commandLink>
						</li>
						<li><a href="#" class="selected">By User</a></li>	
					</ul>
					</div>
					<table border="0" cellspacing="1" cellpadding="3" id="ebooks_metadata">
						<tr>
							<th width="20%" align="right">User Name</th>
							<td width="80%"><h:inputText binding="#{userAuditTrailBean.userName}" id="userName" styleClass="login_field" /></td>
						</tr>
						<tr>
							<th width="20%" align="right">Audit Date From</th>
							<td>
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateFromDay}">
									<f:selectItems value="#{userAuditTrailBean.dayItems}"/>
								</h:selectOneMenu>
								&nbsp;
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateFromMonth}">
									<f:selectItems value="#{userAuditTrailBean.monthMap}"/>
								</h:selectOneMenu>
								&nbsp;
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateFromYear}">
									<f:selectItems value="#{userAuditTrailBean.yearItems}"/>
								</h:selectOneMenu>				
							</td>
						</tr>
						<tr>
							<th width="20%" align="right">Audit Date To</th>
							<td>
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateToDay}">
									<f:selectItems value="#{userAuditTrailBean.dayItems}"/>
								</h:selectOneMenu>
								&nbsp;
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateToMonth}">
									<f:selectItems value="#{userAuditTrailBean.monthMap}"/>
								</h:selectOneMenu>
								&nbsp;
								<h:selectOneMenu value="#{userAuditTrailBean.auditDateToYear}">
									<f:selectItems value="#{userAuditTrailBean.yearItems}"/>
								</h:selectOneMenu>				
							</td>
						</tr>
						<tr>
							<td colspan="2">								
								<h:commandButton id="iconquicksearchbutton" actionListener="#{userAuditTrailBean.updateUserList}" action="#{userAuditTrailBean.updateUserAuditTrail}" styleClass="button" value="Search"/>
								<script>document.forms['articleAuditTrailForm'].onkeypress =new Function("{var keycode;if (window.event) keycode = window.event.keyCode;else if (event) keycode = event.which;else return true;if (keycode == 13) { document.getElementById('articleAuditTrailForm:iconquicksearchbutton').click();return false; } else  return true; }");</script>
							</td>
						</tr>
					</table>
					</div>
				<c:if test="${userAuditTrailBean.userAuditList != null}">
					<c:if test="${not empty userAuditTrailBean.userAuditList}">
					<div id="registered">
					<h:dataTable var="userAudit" value="#{userAuditTrailBean.userAuditList}" cellspacing="0">
						<h:column>
							<f:facet name="header">
			     				<h:outputText value="User Name"/>
			     			</f:facet>
			     			<h:outputText value="#{userAudit.user.username}"/>
						</h:column>
						<h:column>
							<f:facet name="header">
			     						<h:outputText value="Action"/>
			     					</f:facet> 
							<h:outputText value="#{userAudit.action}"/>
						</h:column>
						<h:column>
							<f:facet name="header">
			     						<h:outputText value="Audit Date"/>
			     					</f:facet> 
							<h:outputText value="#{userAudit.auditDate}">
								<f:convertDateTime pattern="MMM d, yyyy HH:mm" />
							</h:outputText>
						</h:column>
						<h:column>
							<f:facet name="header">
			     						<h:outputText value="Remarks"/>
			     					</f:facet> 
							<h:outputText value="#{userAudit.remarks}"/>
						</h:column>
					</h:dataTable>
					</div>
					</c:if>
					<c:if test="${empty userAuditTrailBean.userAuditList}">
						<table cellspacing="0">		
							<tr class="border">
								<td colspan="2"><strong>No audit trail was generated.</strong></td>
							</tr>
						</table>
					</c:if>
				</c:if>
					
					</td>
				</tr>
			</tbody>
		</table>

	</h:form>

	<f:subview id="footer_panel">
		<c:import url="components/footer.jsp"></c:import>
	</f:subview>

	</body>

</f:view>

</html>