<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>eBook Details</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
	<!--[if IE 8]>
        <link href="ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 7]>
        <link href="ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 6]>
        <link href="ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
</head>
	

<body>
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; <h:outputText value="#{ebookBean.mainTitle}" escape="false"/>
	</div>
	
</div>

	
<!-- Menu -->
<f:subview id="menu_panel">
	<c:import url="components/navigation.jsp"></c:import>
</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody>
	<tr>
		<td id="centre">
		<!-- Body -->
		<h:form>			
			<h1>eBook Details</h1>
			<div id="browse_tabs" >
				<ul>
					<li>
						<h:commandLink value="#{ebookBean.eisbn}" action="#{bookContentBean.showChapter}" actionListener="#{bookContentBean.showChapterListener}" >
							<f:attribute name="bookId" value="#{ebookBean.bookId}" />
							<f:param name="bookId" value="#{ebookBean.bookId}" />
					 	</h:commandLink>					 	
					</li>
					<li>
						<a href="#" class="selected"><c:out value="${ebookBean.mainTitle}" escapeXml="false"/></a>
					</li>
				</ul>
			</div>
			<div id="browse_ebooks">	
				<f:subview id="ebook_details_content">			
					<jsp:include page="ebook_details_content.jsp" flush="true" ></jsp:include>
				</f:subview>								
			</div>
		</h:form>
		</td>
	</tr>
	</tbody>
</table>

<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>


<script type="text/javascript" src="js/imageError.js" ></script>


</body>

</f:view>

</html>	