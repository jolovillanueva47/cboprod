<html>
<head>
 <script type="text/javascript" src="../js/deployJava.js"></script>
</head>
<body>
 	   <script> 
        var attributes = {code: 'org.cambridge.ebooks.online.esampling.pdfviewer.ViewerApplet.class', archive: 'icepdf-viewer.jar, icepdf-core.jar, viewerapplet.jar, icepdf-pro.jar, icepdf-pro-intl.jar' , width:800, height:800} ;
        <%-- 
        var parameters = {url: 'http://dib.cambridge.org/pdf/dib.prelims_v.1.0.pdf', code: 'org.cambridge.ebooks.online.esampling.pdfviewer.ViewerApplet.class'} ;
        --%>
        var parameters = {url: '<%= request.getParameter("cid") %>', code: 'org.cambridge.ebooks.online.esampling.pdfviewer.ViewerApplet.class'} ;  
        deployJava.runApplet(attributes, parameters, '1.5'); 
    	</script>
</body>
</html>