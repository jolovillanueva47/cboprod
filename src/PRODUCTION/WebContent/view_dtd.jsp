<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>View DTD Files</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
	
	<script type="text/javascript">
		function showPopupWindow(url) {
			var width = 800;
			var height = 600;
			var left = (screen.width - width) / 2;
			var top = (screen.height - height) / 2;
			var name = "dtdFileContents";
			var properties = "location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes," 
				+ "scrollbars=yes,width="+width+",height="+height+",top="+top+",left="+left;

			window.open(url, name, properties);
		}
	</script>
	
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; View DTD Files
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>View DTD Files</h1>
			<div id="registered">
			
			<form>
				<table cellspacing="0">

				<tr class="shaded">
					<td>Type</td>
					<td>Files</td>
				</tr>
	
				<tr class="border">
					<td>DTD</td>
					<td class="fields">
						<a href="#" onclick="showPopupWindow('html/view_dtd_file_contents.html'); return false;">header.dtd</a>									
					</td>
				</tr>
		
				<!-- tr class="border">
					<td>DTD</td>
					<td class="fields"><a href="viewDTDFileContents?file=cupjnl2_1.dtd">cupjnl2_1.dtd</a></td>
				</tr>
		
				<tr class="border">
					<td>DTD</td>
					<td class="fields"><a href="viewDTDFileContents?file=cupjnl2_2.dtd">cupjnl2_2.dtd</a></td>
				</tr>
		
				<tr class="border">
					<td>DTD</td>
					<td class="fields"><a href="viewDTDFileContents?file=cupjnlhd.dtd">cupjnlhd.dtd</a></td>
				</tr>
		
				<tr class="border">
					<td>DTD</td>
					<td class="fields"><a href="viewDTDFileContents?file=cupsssh.dtd">cupsssh.dtd</a></td>
				</tr>
		
				<tr class="border">
					<td>DTD</td>	
					<td class="fields"><a href="viewDTDFileContents?file=cupjnlhd1_2.dtd">cupjnlhd1_2.dtd</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=cupjnls.ent">cupjnls.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isoamsa.ent">isoamsa.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isoamsb.ent">isoamsb.ent</a></td>
				</tr>
	
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isoamsc.ent">isoamsc.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
				<td class="fields"><a href="viewDTDFileContents?file=isoamsn.ent">isoamsn.ent</a></td>

				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isoamso.ent">isoamso.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isoamsr.ent">isoamsr.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isobox.ent">isobox.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isocyr1.ent">isocyr1.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isocyr2.ent">isocyr2.ent</a></td>
				</tr>

		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isodia.ent">isodia.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isogrk1.ent">isogrk1.ent</a></td>
				</tr>
			
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isogrk2.ent">isogrk2.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isogrk3.ent">isogrk3.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isolat1.ent">isolat1.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isolat2.ent">isolat2.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isomfrk.ent">isomfrk.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isomopf.ent">isomopf.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isomscr.ent">isomscr.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isonum.ent">isonum.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isopub.ent">isopub.ent</a></td>
				</tr>
		
				<tr class="border">
					<td>entity</td>
					<td class="fields"><a href="viewDTDFileContents?file=isotech.ent">isotech.ent</a></td>
				</tr-->
			</table>

			<!-- table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
						<input name="Download all" type="submit" class="button" value="Download all" />
					</td>
				</tr>
			</table-->

			</form>

		</div>

		
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>