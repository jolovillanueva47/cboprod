<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>View Uploader Logs</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>

<body>	
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />			
	</div>
	<table id="content" cellspacing="0"> 
		<tbody>
			<tr>
				<td id="centre">
					<h1>Uploader Logs</h1>
					
					<div id="registered">		
						<form method="post">							
							<table>
								<tr>
									<td>
										<%
											String logs = request.getParameter("uploader-log-hidden");
											out.println(logs);
										%>
									</td>
								</tr>
							</table>	
							<table cellspacing="0" class="registered_buttons">
								<tr>
									<td>
										<input type="button" value="Print" class="button" onclick="javascript:window.print();" />
									</td>
								</tr>
							</table>						
						</form>		
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
