<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>Delete Third Party</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; <a href="third_parties.jsf">Manage Third Parties</a>
		&gt; Delete Third Party
	</div>
	
</div>

	
<!-- Menu -->

	<f:subview id="menu_panel">
		<c:import url="components/navigation.jsp"></c:import>
	</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody><tr>
		<td id="centre">
		<!-- Body -->
			<h1>Delete Third Party</h1>
			<div id="registered">

				<h:form>
				<h:panelGroup rendered="#{thirdPartyDeleteBean.deleteListEmpty}"><p>Please select at least one third party to delete.</p></h:panelGroup>
				<h:panelGroup rendered="#{!thirdPartyDeleteBean.deleteListEmpty}"><p>Are you sure you want to delete the following third parties?</p>
					<h:dataTable cellspacing="0" var="thirdParty" value="#{thirdPartyDeleteBean.deleteList}">
						<h:column>
							<f:facet name="header">
	        					<h:outputText value="Name"/>
	        				</f:facet> 
							<h:commandLink actionListener="#{thirdPartyUpdateBean.initThirdParty}" action="#{thirdPartyBean.updateThirdParty}">
        						<h:outputText value="#{thirdParty.thirdPartyName}"/>
        						<f:attribute name="thirdParty" value="#{thirdParty}"/>
							</h:commandLink>
						</h:column>
					</h:dataTable>
				</h:panelGroup>
				
				<table cellspacing="0" class="registered_buttons">
					<tr>
						<td>
							<h:commandButton actionListener="#{thirdPartyDeleteBean.cancelDelete}" action="third_party" styleClass="button" value="Cancel" />
							<c:if test="${!thirdPartyDeleteBean.deleteListEmpty}">
								&nbsp;<h:commandButton actionListener="#{thirdPartyDeleteBean.deleteThirdParty}" action="#{thirdPartyDeleteBean.deleteThirdParty}" styleClass="button" value="Delete">
									<f:attribute name="deleteSetIds" value="#{thirdPartyDeleteBean.deleteSetIds}"/>
								</h:commandButton>
							</c:if>
						</td>
					</tr>
				</table>
				
				</h:form>
			</div>
		</td>
	</tr></tbody>
</table>


<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>



</body>

</f:view>

</html>