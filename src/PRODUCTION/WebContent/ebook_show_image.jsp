<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<f:view>

<head>
	
	<title>View Cover Image</title>
	<style type="text/css">
	<!--
		@import url(css/users_abstract.css);
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>	
</head>

<body>	
	<div id="strip">	
		<div id="close"><a href="#" onclick="javascript:self.close();">close</a></div>		
		<h1>eBooks Production</h1>
		<img src="images/logo_6699CC_small.gif" />			
	</div>
	<table id="content" cellspacing="0"> 
		<tbody>
			<tr>
				<td id="centre">
					<%-- <h1>[${bookDetails.ebookNoLoad.isbn}]&nbsp;${bookDetails.ebookNoLoad.mainTitle}</h1> --%>
					<h1>[<h:outputText value="#{bookContentBean.ebook.isbn}"/>]&nbsp;<h:outputText value="#{bookContentBean.ebook.title}"/></h1>
					<div id="registered">		
						<form>
							<table>
								<tr>
									<td>
										<c:set var="content_url" scope="page" value='<%=System.getProperty("content.url")%>'/>
										<c:if test="${param.imagetype == 'image_standard'}" >
											<img src="<c:out value="${pageScope.content_url}" /><h:outputText value="#{bookContentBean.ebookOthers['image_standard'].isbnBreakdown}" />/${param.f}" />
										</c:if>	
										<c:if test="${param.imagetype == 'image_thumb'}" >
											<img src="<c:out value="${pageScope.content_url}" /><h:outputText value="#{bookContentBean.ebookOthers['image_thumb'].isbnBreakdown}" />/${param.f}" />
										</c:if>											
									</td>
								</tr>
							</table>
						</form>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>
</body>

</f:view>

</html>