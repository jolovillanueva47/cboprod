<%@page import="org.cambridge.ebooks.production.document.EbookDetailsBean"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<title>eBook Chapter Details</title>
	<style type="text/css">
	<!--
		@import url(css/users_general.css);
		@import url(css/users_general_content.css);
	-->
	</style>
	<!--[if IE 8] -->
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <!-- <![endif]-->
	
    <!--[if IE 7] -->
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <!-- <![endif]-->
	
    <!--[if IE 6] -->
        <link href="css/ie6.css" rel="stylesheet" type="text/css" />
    <!-- <![endif]-->
	
	<!--[if !IE]> -->
		<link href="css/non-ie.css" rel="stylesheet" type="text/css" />
	<!-- <![endif]-->
</head>
	
<body>
	
<!-- Left header page information -->

<a name="top"></a>

<div id="strip">

	<f:subview id="welcome_panel">
		<c:import url="components/welcome.jsp"></c:import>
	</f:subview>
	
	<!-- Crumbtrail -->
	
	<div id="location">
		<a href="home.jsf">Home</a>
		&gt; <h:outputLink value="ebook_chapter.jsf?id=#{bookContentBean.ebook.bookId}"><h:outputText value="#{bookContentBean.ebook.title}" escape="false"/></h:outputLink>
		&gt; <h:outputText value="#{chapterBean.headingTitle}" escape="false"/>
	</div>
	
</div>

	
<!-- Menu -->
<f:subview id="menu_panel">
	<c:import url="components/navigation.jsp"></c:import>
</f:subview>

	
<!-- main content -->
<table id="content" cellspacing="0"> 
	<tbody>
	<tr>
		<td id="centre">
		<!-- Body -->
		<h:form>
			<h1>eBook Details</h1>
			<h:inputHidden id="alertMessage"/>
			<div id="browse_tabs" >
				<ul>
					<li>
						<h:outputLink value="ebook_chapter.jsf" >${bookContentBean.ebook.isbn}</h:outputLink>					 	
					</li>
					<li>
						<a href="#" class="selected"><c:out value="${bookContentBean.ebook.title}" escapeXml="false"/></a>
					</li>
				</ul>
			</div>
			<h:panelGroup rendered="#{! empty facesContext.maximumSeverity}">
				<p class="alert"><h:message for="alertMessage"/><br/></p>
			</h:panelGroup>
			<h:panelGroup rendered="#{!empty eBookDeliveryBean.crossRefXmlFileName}">
				<p/>
				<p class="alert"><h:outputLink value="downloads/crossref/#{eBookDeliveryBean.crossRefXmlFileName}">Click this link to view the CrossRef XML file.</h:outputLink><br/><br/></p>
			</h:panelGroup>
			<div id="browse_ebooks">	
				 		
				<f:subview id="chapter_details_content">
					<jsp:include page="ebook_content_details.jsp"></jsp:include>
				</f:subview>				
			</div>

	
		</h:form>
		</td>
	</tr>
	</tbody>
</table>

<f:subview id="footer_panel">
	<c:import url="components/footer.jsp"></c:import>
</f:subview>

<script type="text/javascript" src="js/imageError.js" ></script>	

<script type="text/javascript">
	function openWindow(url) { //v2.0		
		var h = window.open(url,'','location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes,scrollbars=yes,width=700,height=440');
		h.focus();
	}
</script>
</body>

</f:view>

</html>	