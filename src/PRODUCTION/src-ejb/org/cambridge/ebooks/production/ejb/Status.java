package org.cambridge.ebooks.production.ejb;

import java.util.LinkedHashMap;

public enum Status {
	NOTLOADED(-1, "Not yet Loaded"),
	LOADED(0, "Loaded for Proofreading"), 
	RELOADED(1, "Reloaded with Corrections"), 
	PROOFED(2, "Proofread Accepted"), 
	REJECTED(3, "Proofread with Error"), 
	APPROVE(4, "Approved"), 
	DISAPPROVED(5, "Reviewed with Error"), 
	REAPPROVE(6, "Reloaded for Approver"), 
	PUBLISHED(7, "Published"), 
	UNPUBLISHED(8, "Unpublished"), 
	EMBARGO(9, "Embargo"), 
	DELETED(100, "Deleted");
	
	private int status;
	private String description;
	
	Status(int status, String description) {
		this.status = status;
		this.description = description;
	}
	public String getStatus() { return String.valueOf(status); }
	public static Status getStatus(int status) {
		Status s = NOTLOADED;
		if (status == Integer.parseInt(LOADED.getStatus())) s = LOADED;
		else if (status == Integer.parseInt(RELOADED.getStatus())) s = RELOADED;
		else if (status == Integer.parseInt(PROOFED.getStatus())) s = PROOFED;
		else if (status == Integer.parseInt(REJECTED.getStatus())) s = REJECTED;
		else if (status == Integer.parseInt(APPROVE.getStatus())) s = APPROVE;
		else if (status == Integer.parseInt(DISAPPROVED.getStatus())) s = DISAPPROVED;
		else if (status == Integer.parseInt(REAPPROVE.getStatus())) s = REAPPROVE;
		else if (status == Integer.parseInt(PUBLISHED.getStatus())) s = PUBLISHED;
		else if (status == Integer.parseInt(UNPUBLISHED.getStatus())) s = UNPUBLISHED;
		else if (status == Integer.parseInt(EMBARGO.getStatus())) s = EMBARGO;
		else if (status == Integer.parseInt(DELETED.getStatus())) s = DELETED;
		return s;
	}
	public String getDescription() { return description; }
	
	public static final LinkedHashMap<String, String> ALL_OPTIONS = new LinkedHashMap<String, String>();
	static {
		ALL_OPTIONS.put(LOADED.getDescription(), LOADED.getStatus());
		ALL_OPTIONS.put(RELOADED.getDescription(), RELOADED.getStatus());
		ALL_OPTIONS.put(PROOFED.getDescription(), PROOFED.getStatus());
		ALL_OPTIONS.put(REJECTED.getDescription(), REJECTED.getStatus());
		ALL_OPTIONS.put(APPROVE.getDescription(), APPROVE.getStatus());
		ALL_OPTIONS.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());
		ALL_OPTIONS.put(REAPPROVE.getDescription(), REAPPROVE.getStatus());
		ALL_OPTIONS.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
		ALL_OPTIONS.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
		ALL_OPTIONS.put(EMBARGO.getDescription(), EMBARGO.getStatus());
		ALL_OPTIONS.put(DELETED.getDescription(), DELETED.getStatus());
	}
	
	public static LinkedHashMap<String, String> getOptions(Status status) {
		LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
		switch (status) {
//		case NOTLOADED:
//			options.put(NOTLOADED.getDescription(), NOTLOADED.getStatus());
//			break;
		case LOADED:
			options.put(LOADED.getDescription(), LOADED.getStatus());
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			break;
		case RELOADED:
			options.put(RELOADED.getDescription(), RELOADED.getStatus());
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			break;
		case PROOFED:
			options.put(PROOFED.getDescription(), PROOFED.getStatus());
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());			
			break;
		case REJECTED:
			// user needs to reload and system set to RELOADED
			options.put(REJECTED.getDescription(), REJECTED.getStatus());
			break;
		case APPROVE:
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(EMBARGO.getDescription(), EMBARGO.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case DISAPPROVED:
			// user needs to reload and system set to REAPPROVE
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());
			break;
		case REAPPROVE:
			options.put(REAPPROVE.getDescription(), REAPPROVE.getStatus());
			options.put(APPROVE.getDescription(), APPROVE.getStatus());
			options.put(DISAPPROVED.getDescription(), DISAPPROVED.getStatus());		
			break;
		case PUBLISHED:
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
			break;
		case UNPUBLISHED:
			options.put(UNPUBLISHED.getDescription(), UNPUBLISHED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus());
			break;
		case EMBARGO:
			options.put(EMBARGO.getDescription(), EMBARGO.getStatus());
			options.put(PUBLISHED.getDescription(), PUBLISHED.getStatus());
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		case DELETED:
			options.put(DELETED.getDescription(), DELETED.getStatus()); // for checking
			break;
		default:
			options.put(NOTLOADED.getDescription(), NOTLOADED.getStatus());
			
		}
		return options;
	}
	
	public static Status getBookStatus(int status[]) {
		Status result = PUBLISHED;
		for (int i: status) {
			if (i != Integer.parseInt(PUBLISHED.getStatus())) result = UNPUBLISHED;
		}
		return result;
	}
	
	public static int getNextStatus(int status) {
		String result = String.valueOf(status);
		if (status == Integer.parseInt(REJECTED.getStatus())) {
			result = RELOADED.getStatus();
		} else if (status == Integer.parseInt(DISAPPROVED.getStatus())) {
			result = REAPPROVE.getStatus();
		}
		return Integer.parseInt(result);
	}
}
