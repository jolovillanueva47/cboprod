package org.cambridge.ebooks.production.ejb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table ( name = "USER_AUDIT_TRAIL_BOOKS" )
public class UserAudit {

	public static final String SEARCH_USER_AUDIT = "searchUserAudit";
	
	@Id
	@GeneratedValue(generator="USER_AUDIT_TRAIL_SEQ")
    @SequenceGenerator(name="USER_AUDIT_TRAIL_SEQ", sequenceName="USER_AUDIT_TRAIL_SEQ", allocationSize=1)
	@Column( name = "AUDIT_ID")
	private int auditId;
	
	@Column( name = "ACTION")
	private String action;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column( name="AUDIT_DATE")
    private Date auditDate;
	
	@Column( name = "REMARKS")
	private String remarks;

	@Column( name = "USER_ID")
	private String userId;
	
	public String getUserId(){
		return userId;
	}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	
	public int getAuditId() {
		return auditId;
	}

	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
