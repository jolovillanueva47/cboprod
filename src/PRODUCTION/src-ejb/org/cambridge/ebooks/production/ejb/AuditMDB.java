package org.cambridge.ebooks.production.ejb;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

@MessageDriven(name = "AuditMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/ebooks/logger") })
@TransactionManagement(TransactionManagementType.BEAN)	
public class AuditMDB implements MessageListener {
	
	private static final long serialVersionUID = 1L;

	@Resource
	private MessageDrivenContext mdc;
	
    @Resource
    SessionContext sc;
    
	@PersistenceUnit( unitName = "CounterService" )	
	private EntityManagerFactory emf;
	
	private EntityManager em;
	
	public void onMessage(Message recvMsg) {
		UserTransaction ut = sc.getUserTransaction();
		try {
			if ( recvMsg instanceof Message ) {
				
				Message payload = (Message) recvMsg;
				Date today = new Date();
				
				if(payload.getStringProperty("TYPE").equals("Book")){
					BookAudit event = new BookAudit();
					event.setChapterId(payload.getStringProperty("CHAPTER_ID"));
					event.setContentId(payload.getStringProperty("CONTENT_ID"));
					event.setEisbn(payload.getStringProperty("EISBN"));
					event.setSeriesCode(payload.getStringProperty("SERIES_CODE"));
					event.setStatus(payload.getStringProperty("STATUS"));
					event.setRemarks(payload.getStringProperty("REMARKS"));
					event.setFilename(payload.getStringProperty("FILENAME"));
					event.setLoadType(payload.getStringProperty("LOAD_TYPE"));
					event.setUsername(payload.getStringProperty("USERNAME"));
					event.setAuditDate(today);
										
					try {			
						ut.begin();
						em.joinTransaction();
					    em.persist(event);
					    ut.commit();
					} catch ( Exception exc ) { 
						String err = "STATUS[" + event.getStatus() + "]";
						Logger.getLogger( AuditMDB.class).error( "Failed to log event :"+err);
						Logger.getLogger( AuditMDB.class).error( "Message: "+exc.getMessage() );
						Logger.getLogger( AuditMDB.class).error( exc );
					}
				}else if(payload.getStringProperty("TYPE").equals("EBook")){
					Logger.getLogger( AuditMDB.class ).info( " Audit trail EBook " );
					EBookAudit event = new EBookAudit();
					event.setEisbn(payload.getStringProperty("EISBN"));
					event.setSeriesCode(payload.getStringProperty("SERIES_CODE"));
					event.setStatus(payload.getStringProperty("STATUS"));
					event.setUsername(payload.getStringProperty("USERNAME"));
					event.setTitle(payload.getStringProperty("TITLE"));
					event.setAuditDate(today);
					
					try {			
						ut.begin();
						em.joinTransaction();
					    em.persist(event);
					    ut.commit();
					} catch ( Exception exc ) { 
						String err = "STATUS[" + event.getStatus() + "]";
						Logger.getLogger( AuditMDB.class).error( "Failed to log event :"+err);
						Logger.getLogger( AuditMDB.class).error( "Message: "+exc.getMessage() );
						Logger.getLogger( AuditMDB.class).error( exc );
					}
				}else if(payload.getStringProperty("TYPE").equals("User")) {
					UserAudit event = new UserAudit();
					event.setAction( payload.getStringProperty("ACTION"));
					event.setUserId( payload.getStringProperty("USER_ID"));
					event.setRemarks(payload.getStringProperty("REMARKS"));
					event.setAuditDate(today);
					
					try {
						ut.begin();
						em.joinTransaction();
					    em.persist(event);
					    ut.commit();
					} catch ( Exception exc ) { 
						String err = "ACTION[" + event.getAction() + "]";
						Logger.getLogger( AuditMDB.class).error( "Failed to log event :"+err);
						Logger.getLogger( AuditMDB.class).error( "Message: "+exc.getMessage() );
						Logger.getLogger( AuditMDB.class).error( exc );
					}
				}
			}
		} catch (JMSException exc) {
			Logger.getLogger( AuditMDB.class ).error( "An exception occured while processing message " + exc.getMessage() );
			mdc.setRollbackOnly();
		}
	}
	
	@PostConstruct
    void init() {
		Logger.getLogger( AuditMDB.class ).info( " Initialize EJB " );		
		em = emf.createEntityManager();
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger( AuditMDB.class ).info( " Destroy EJB " );
    	em.close();
    }
}