package org.cambridge.ebooks.production.searchejb.document;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.cambridge.ebooks.production.searchejb.util.IndexSearchUtil;
import org.cambridge.ebooks.production.searchejb.util.StringUtil;
import org.cambridge.util.Misc;

import org.apache.commons.beanutils.BeanUtils;

/**
 * @author Karlson A. Mulingtapang, ahcollado
 *         EbookChapterDetailsBean.java - Ebook Chapter Details bean
 */
public class EbookChapterDetailsBean {
	private final static String EBOOK_CONTENT_BEAN_ATTR = "ebookContentBean";
	private final static String PDF = "PDF";
	private final static String DOI = "DOI";
	private final static String HEADING_TITLE = "TITLE";
	private final static String HEADING_LABEL = "LABEL";
	private final static String HEADING_SUB_TITLE = "SUB_TITLE";
	private final static String CONTRIBUTOR_NAME = "CONTRIBUTOR";
	private final static String CONTRIBUTOR_AFFILIATION = "AFFILIATION";
	private final static String KEYWORD = "KEYWORD";
	private final static String ABSTRACT = "ABSTRACT";
	private final static String CITATION = "CITATION";
	private final static String ABSTRACT_IMAGE = "ALT_FILENAME";
	private final static String PAGE_END = "PAGE_END";
	private final static String PAGE_START = "PAGE_START";
	private final static String PROBLEM = "PROBLEM";
	private final static String INDEX_DIRECTORY = System.getProperty("index.dir").trim();
	private final static String ELEMENT = "ELEMENT";
	private final static String ALPHASORT = "ALPHASORT";
	private final static String CONTENT_ITEM = "content-item";
	private final static String TOC_ITEM = "TOC_ITEM";
	private final static String TOC_START_PAGE = "START_PAGE";
	private final static String TYPE = "TYPE";

	private final static String PROBLEM_NONE = "none";
	private final static String PROBLEM_DISPLAY = "display";
	private final static String PROBLEM_UNICODE = "unicode";
	private final static String PROBLEM_NOTE_NONE = " (HTML abstract will be displayed)";
	private final static String PROBLEM_NOTE_DISPLAY = " (JPEG abstract will be displayed)";
	private final static String PROBLEM_NOTE_UNICODE = " (JPEG abstract will be displayed)";
	private final static String COLON = ":";
	private final static String SPACE = " ";
	private final static String BR = "<br[\\s]*[/]?>";
	private final static String HTML_BREAK = "<br />";
	
	private final static String PARENT_ID = "PARENT_ID";
	
	private final static int KEYWORD_DISP_LENGTH = 5;

	private String contentId;
	private String contentType;
	private String pdfFilename;
	private String doi;
	private String headingLabel;
	private String headingTitle;
	private String headingSubtitle;
	private String contributorName;
	private String contributorNameAlphasort;
	private String contributorAffiliation;
	private String subjectGroup;
	private String themeGroup;
	private String keyword;
	private String toc;
	private String abstractContent;
	private String object;
	private String contentItem;
	private String references;
	private String abstractImage;
	private String pageEnd;
	private String pageStart;
	private String problem;
	private String titleAlphasort;
	private List<String> citations;
	// title with part or chapter
	private String title;
	private String contentTitle;
	private String parentId;
	private List<String> contributors;
//	private EBookContentBean ebookContentBean;
	
	public EbookChapterDetailsBean() {
	}
	
	/**
	 * resets all bean properties by creating a new bean 
	 * and copying all the properties; thereby resetting all values 
	 */
	private void resetBeanPropAll(){
		EbookChapterDetailsBean bean = new EbookChapterDetailsBean();
		bean.setContentId(getContentId());
		bean.setContentType(getContentType());
//		bean.setContentTitle(getContentTitle());
		try {
			BeanUtils.copyProperties(this, bean);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		setCitations(new ArrayList<String>());
	}

	public void show(String contentId) {
		setContentId(contentId);
		setCitations(new ArrayList<String>());

		try {			
			// content type will have a value after this method call
			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContent()));
			if(getContentType().equals("part-title")) {
				populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringAbstract()));
			} else {
				populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContentDetails()));
				populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringTocItems()));
			}
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContent()));
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContentDetails()));
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringTocItems()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void show(IndexSearcher searcher, String contentId) {
		setContentId(contentId);
		setCitations(new ArrayList<String>());

		try {			
			// content type will have a value after this method call
			populateBean(IndexSearchUtil.searchIndex(searcher, getQueryStringContent()));
			if(getContentType().equals("part-title")) {
				populateBean(IndexSearchUtil.searchIndex(searcher, getQueryStringAbstract()));
			} else {
				populateBean(IndexSearchUtil.searchIndex(searcher, getQueryStringContentDetails()));
				populateBean(IndexSearchUtil.searchIndex(searcher, getQueryStringTocItems()));
			}
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContent()));
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringContentDetails()));
//			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY, getQueryStringTocItems()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void populateBean(List<Document> docs) throws IOException {
		boolean hasPageStart = false;
		boolean hasPageEnd = false;
		StringBuffer contributor = new StringBuffer();
		StringBuffer affiliation = new StringBuffer();
		StringBuffer tocItem = new StringBuffer();
		contributors = new ArrayList<String>();
		for (Document document : docs) {

//			if (getContentType().equals("part-title")
//					&& !Misc.isEmpty(document.get("TYPE"))
//					&& !document.get("TYPE").equals("part-title")) {
//				continue;
//			}
			
			if(Misc.isNotEmpty(document.get(TYPE))) {
				if(Misc.isEmpty(getContentType())) {
					setContentType(document.get(TYPE));	
				}				
			}
			
			if (Misc.isNotEmpty(document.get(PDF))) {
				if(Misc.isEmpty(getPdfFilename())) {
					setPdfFilename(document.get(PDF));
				}
			}

			if (Misc.isNotEmpty(document.get(DOI))) {
				if(Misc.isEmpty(getDoi())) {
					setDoi(document.get(DOI));
				}
			}

			if (Misc.isNotEmpty(document.get(HEADING_TITLE))) {
				if(Misc.isEmpty(getHeadingTitle())) {
					setHeadingTitle(document.get(HEADING_TITLE));
				}
			}
			
			if (Misc.isNotEmpty(document.get(HEADING_LABEL))) {
				if(Misc.isEmpty(getHeadingLabel())) {
					setHeadingLabel(document.get(HEADING_LABEL));
				}
			}

			if (Misc.isNotEmpty(document.get(HEADING_SUB_TITLE))) {
				if(Misc.isEmpty(getHeadingSubtitle())) {
					setHeadingSubtitle(document.get(HEADING_SUB_TITLE));
				}
			}

			if (Misc.isNotEmpty(document.get(PAGE_END))
					&& Misc.isEmpty(getPageEnd())) {
				hasPageEnd = true;
				if(Misc.isEmpty(getPageEnd())) {
					setPageEnd(document.get(PAGE_END));
				}
			}

			if (Misc.isNotEmpty(document.get(PAGE_START))
					&& Misc.isEmpty(getPageStart())) {
				hasPageStart = true;
				if(Misc.isEmpty(getPageStart())) {
					setPageStart(document.get(PAGE_START));
				}
			}

			if (Misc.isNotEmpty(document.get(CONTRIBUTOR_NAME))) {
				contributor.append(document.get(CONTRIBUTOR_NAME) + " <em>("
						+ document.get(ALPHASORT) + ")</em>" + HTML_BREAK);	
				
				
				contributors.add(document.get(CONTRIBUTOR_NAME));
				
				setContributorName(document.get(CONTRIBUTOR_NAME));
				setContributorNameAlphasort(document.get(ALPHASORT));
			}

			if (Misc.isNotEmpty(document.get(CONTRIBUTOR_AFFILIATION))) {
				affiliation.append(document.get(CONTRIBUTOR_AFFILIATION) + HTML_BREAK);
				setContributorAffiliation(document.get(CONTRIBUTOR_AFFILIATION));
			}

			if (Misc.isNotEmpty(document.get(KEYWORD))) {
				if(Misc.isEmpty(getKeyword())) {
					setKeyword(document.get(KEYWORD));
				}
			}

			if (Misc.isNotEmpty(document.get(ABSTRACT))) {	
				if(Misc.isEmpty(getAbstractContent())) {
					setAbstractContent(document.get(ABSTRACT));
				}	
			}
			
			if(Misc.isNotEmpty(document.get(ABSTRACT_IMAGE))) {
				if(Misc.isEmpty(getAbstractImage())) {
					setAbstractImage(document.get(ABSTRACT_IMAGE));
				}
			}

			if (Misc.isNotEmpty(document.get(PROBLEM))) {
				String problem = document.get(PROBLEM);
				if (problem.equals(PROBLEM_NONE)) {
					problem += PROBLEM_NOTE_NONE;
				} else if (problem.equals(PROBLEM_DISPLAY)) {
					problem += PROBLEM_NOTE_DISPLAY;
				} else if (problem.equals(PROBLEM_UNICODE)) {
					problem += PROBLEM_NOTE_UNICODE;
				}
				if(Misc.isEmpty(getProblem())) {
					setProblem(problem);
				}
			}

			if (Misc.isNotEmpty(document.get(CITATION))) {
				addCitation(document.get(CITATION));
			}

			// get alphasort
			if (CONTENT_ITEM.equals(document.get(ELEMENT))) {
				if(Misc.isEmpty(getTitleAlphasort())) {
					setTitleAlphasort(document.get(ALPHASORT));
				}
			}
			
			if(Misc.isNotEmpty(document.get(TOC_ITEM))) {
				tocItem.append(document.get(TOC_ITEM) + " <em>(start page: " + document.get(TOC_START_PAGE) + ")</em>" + HTML_BREAK);
			}
			
			//for parent id
			if(Misc.isNotEmpty(document.get(PARENT_ID))){
				if(Misc.isEmpty(getParentId())) {
					setParentId(document.get(PARENT_ID));
				}
			}
			
		} // end for

		// this method "isDisplayAbstractContent" will return false if the image
		// does not
		// exist, the following would be executed when in error.
		// setAbstract implements a logic for finding the error
		
		//	setAbstractImage(EbookDetailsBean.NO_IMAGE_IN_XML);
		
		//arrange title - heading label + title
//		if( this.headingLabel != null && this.headingLabel.trim().length() > 0 ){
//			setTitle(this.headingLabel + COLON + SPACE + this.headingTitle  );
//		}else{
//			setTitle(this.headingTitle);
//		}		
		// use title generated in ebook_chapter page
//		if(getContentTitle() != null && getContentTitle().trim().length() > 0){
//			setTitle(getContentTitle());
//		}			
		
		if(!Misc.isEmpty(contributor.toString())) {
			setContributorName(contributor.toString());
		}
		
		if(!Misc.isEmpty(affiliation.toString())) {
			setContributorAffiliation(affiliation.toString());
		}
		
//		if(!Misc.isEmpty(getContentTitle())) {
//			setHeadingTitle(getContentTitle().replaceAll(getHeadingLabel() + COLON + SPACE, ""));
//		}
		
		if(!Misc.isEmpty(tocItem.toString())) {
			setToc(tocItem.toString());
		}
	}	

	private String getQueryStringContentDetails() {
		StringBuffer queryString = new StringBuffer("PARENT_ID:" + getContentId());
		queryString
		.append(" AND (PARENT:content-item OR PARENT:heading OR PARENT:contributor-group)");

		return queryString.toString();
	}
	
	private String getQueryStringTocItems() {
		StringBuffer queryString = new StringBuffer("CONTENT_ITEM_ID:" + getContentId());
		return queryString.toString();
	}

	private String getQueryStringContent() {
		StringBuffer queryString = new StringBuffer("ID:" + getContentId()
				+ " AND ELEMENT:content-item");
		return queryString.toString();
	}
	
	private String getQueryStringAbstract() {
		StringBuffer queryString = new StringBuffer("(PARENT_ID:");
		queryString.append(getContentId());
		queryString.append(" AND ELEMENT:abstract)");
		return queryString.toString();
	}

	/**
	 * @return the pdfFilename
	 */
	public String getPdfFilename() {
		return pdfFilename;
	}

	/**
	 * @param pdfFilename
	 *            the pdfFilename to set
	 */
	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi
	 *            the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the headingLabel
	 */
	public String getHeadingLabel() {
		return headingLabel;
	}

	/**
	 * @param headingLabel
	 *            the headingLabel to set
	 */
	public void setHeadingLabel(String headingLabel) {
		this.headingLabel = headingLabel;
	}

	/**
	 * @return the headingTitle
	 */
	public String getHeadingTitle() {
		return StringUtil.correctHtmlTags(headingTitle);
	}

	/**
	 * @param headingTitle
	 *            the headingTitle to set
	 */
	public void setHeadingTitle(String headingTitle) {
		this.headingTitle = headingTitle;
	}

	/**
	 * @return the headingSubtitle
	 */
	public String getHeadingSubtitle() {
		return StringUtil.correctHtmlTags(headingSubtitle);
	}

	/**
	 * @param headingSubtitle
	 *            the headingSubtitle to set
	 */
	public void setHeadingSubtitle(String headingSubtitle) {
		this.headingSubtitle = headingSubtitle;
	}

	/**
	 * @return the contributorName
	 */
	public String getContributorName() {
		return StringUtil.correctHtmlTags(contributorName);
	}
	
	public String getContributorNameModified() {
		String contributor = getContributorName();
		try {
			contributor = contributor.replaceAll("<em>(", "");
			contributor = contributor.replaceAll(")</em>", "");
			contributor = contributor.replaceAll(HTML_BREAK, ",");
		} catch (Exception e) {
			return null;
		}		
		return contributor;
	}
	

	/**
	 * @param contributorName
	 *            the contributorName to set
	 */
	public void setContributorName(String contributorName) {
		this.contributorName = contributorName;
	}

	/**
	 * @return the contributorAffiliation
	 */
	public String getContributorAffiliation() {
		return StringUtil.correctHtmlTags(contributorAffiliation);
	}

	/**
	 * @param contributorAffiliation
	 *            the contributorAffiliation to set
	 */
	public void setContributorAffiliation(String contributorAffiliation) {
		this.contributorAffiliation = contributorAffiliation;
	}

	/**
	 * @return the subjectGroup
	 */
	public String getSubjectGroup() {
		return StringUtil.correctHtmlTags(subjectGroup);
	}

	/**
	 * @param subjectGroup
	 *            the subjectGroup to set
	 */
	public void setSubjectGroup(String subjectGroup) {
		this.subjectGroup = subjectGroup;
	}

	/**
	 * @return the themeGroup
	 */
	public String getThemeGroup() {
		return themeGroup;
	}

	/**
	 * @param themeGroup
	 *            the themeGroup to set
	 */
	public void setThemeGroup(String themeGroup) {
		this.themeGroup = themeGroup;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return StringUtil.correctHtmlTags(keyword);		
	}
	
	
	
	/**
	 * get keyword for display
	 * @return
	 */
	public ArrayList<ArrayList<String>> getKeywordDisplay() {
		String[] keys;
		 
		keys = keyword.split(BR);
		
		ArrayList<ArrayList<String>> strArr = new ArrayList<ArrayList<String>>();
		for( int i = 0; i < KEYWORD_DISP_LENGTH; i++ ){
			strArr.add(new ArrayList<String>());
		}		
		int rowsPerCol = (int) Math.ceil(keys.length*1.0/KEYWORD_DISP_LENGTH);		
		int i=0;
		int listNum=0;
		for(String str : keys){
			strArr.get(listNum).add(str.trim());
			i++;
			if( i == rowsPerCol ){
				i = 0;
				listNum++;
			}
			
			
		}
		return strArr;
	}

	/**
	 * @param keyword
	 *            the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * @return the toc
	 */
	public String getToc() {
		return StringUtil.correctHtmlTags(toc);
	}

	/**
	 * @param toc
	 *            the toc to set
	 */
	public void setToc(String toc) {
		this.toc = toc;
	}

	/**
	 * @return the abstractContent
	 */
	public String getAbstractContent() {
		return StringUtil.correctHtmlTags(abstractContent);
	}

	/**
	 * @param abstractContent
	 *            the abstractContent to set
	 */
	public void setAbstractContent(String abstractContent) {
		this.abstractContent = abstractContent;
	}

	/**
	 * @return the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * @param object
	 *            the object to set
	 */
	public void setObject(String object) {
		this.object = object;
	}

	/**
	 * @return the contentItem
	 */
	public String getContentItem() {
		return contentItem;
	}

	/**
	 * @param contentItem
	 *            the contentItem to set
	 */
	public void setContentItem(String contentItem) {
		this.contentItem = contentItem;
	}

	/**
	 * @return the references
	 */
	public String getReferences() {
		return StringUtil.correctHtmlTags(references);
	}

	/**
	 * @param references
	 *            the references to set
	 */
	public void setReferences(String references) {
		this.references = references;
	}

	/**
	 * @return the contentId
	 */
	public String getContentId() {
		return contentId;
	}

	/**
	 * @param contentId
	 *            the contentId to set
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public List<String> getCitations() {
		return citations;
	}

	public void setCitations(List<String> citations) {
		this.citations = citations;
	}

	public void addCitation(String citation) {
		citations.add(citation);
	}

	/**
	 * @return the abstractImage
	 */
	public String getAbstractImage() {
		return abstractImage;
	}
	
	public void setAbstractImage(String abstractImage) {
		this.abstractImage = abstractImage;
	}

	/**
	 * uses ImageUtil to check if the image exists or not will supply default
	 * path in errors are encountered
	 * 
	 * @param abstractImage
	 *            the abstractImage to set
	 */
//	public void setAbstractImage(String abstractImage) {
//		if (abstractImage != EbookDetailsBean.NO_IMAGE_IN_XML) {
//			this.abstractImage = ImageUtil.setImagePath(abstractImage,
//					EbookDetailsBean.MISSING_COVER_IMAGE,
//					EbookDetailsBean.CONTENT_DIR,
//					EbookDetailsBean.IMAGE_DIR_PATH, this.getClass());
//		} else {
//			this.abstractImage = EbookDetailsBean.NO_COVER_IMAGE;
//		}
//
//	}

	/**
	 * @return the pageEnd
	 */
	public String getPageEnd() {
		return pageEnd;
	}

	/**
	 * @param pageEnd
	 *            the pageEnd to set
	 */
	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	/**
	 * @return the pageStart
	 */
	public String getPageStart() {
		return pageStart;
	}

	/**
	 * @param pageStart
	 *            the pageStart to set
	 */
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getProblem() {
		return StringUtil.correctHtmlTags(problem);
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getTitleAlphasort() {
		return StringUtil.correctHtmlTags(titleAlphasort);
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public String getTitle() {
		return StringUtil.correctHtmlTags(title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the contentTitle
	 */
//	public String getContentTitle() {
//		return  StringUtil.correctHtmlTags(contentTitle);
//	}

	/**
	 * @param contentTitle the contentTitle to set
	 */
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the contributorNameAlphasort
	 */
	public String getContributorNameAlphasort() {
		return contributorNameAlphasort;
	}

	/**
	 * @param contributorNameAlphasort the contributorNameAlphasort to set
	 */
	public void setContributorNameAlphasort(String contributorNameAlphasort) {
		this.contributorNameAlphasort = contributorNameAlphasort;
	}

	/**
	 * @return the contributors
	 */
	public List<String> getContributors() {
		return contributors;
	}

	
	/**
	 * @return the ebookContentBean
	 */
//	public EBookContentBean getEbookContentBean() {
//		return ebookContentBean;
//	}

	/**
	 * @param ebookContentBean the ebookContentBean to set
	 */
//	public void setEbookContentBean(EBookContentBean ebookContentBean) {
//		this.ebookContentBean = ebookContentBean;
//	}
	
}
