/**
 * 
 */
package org.cambridge.ebooks.production.searchejb.util;

/**
 * @author rvillamor
 *
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.HitCollector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.cambridge.ebooks.production.searchejb.indexer.IndexerProperties;


public class IndexSearchUtil {
	
	public static final String INDEX_DIR = System.getProperty(IndexerProperties.INDEX);
	
	public static List<Document> searchIndex(final String indexDir, final String querystr) {
		List<Document> result = new ArrayList<Document>();

		try {
			Analyzer analyzer = new WhitespaceAnalyzer();
			FSDirectory index = FSDirectory.getDirectory(indexDir);
			Query q = new QueryParser("BOOK_ID", analyzer).parse(querystr);

			IndexSearcher searcher = new IndexSearcher(index);
			AllDocCollector collector = new IndexSearchUtil.AllDocCollector();
			searcher.search(q, collector);
			List<ScoreDoc> hits = collector.getHits();

			for (ScoreDoc score : hits) {
				result.add(searcher.doc(score.doc));
			}

			searcher.close();
		}

		catch (IOException e) {
			e.printStackTrace();
			System.out.println( "IndexSearchUtil.class, " +"[IOException]: searchIndex() "+e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println( "IndexSearchUtil.class, " +"[ParseException]: searchIndex() "+e.getMessage());
		} catch (Exception e) {	
			e.printStackTrace();
			System.out.println( "IndexSearchUtil.class, " +"[Exception]: searchIndex() "+e.getMessage());
		}

		return result;
	}

	public static List<Document> searchIndex(IndexSearcher searcher, String querystr) {
		List<Document> result = new ArrayList<Document>();
		
		try {
//			System.out.println("searchIndex start");
			
			Analyzer analyzer = new WhitespaceAnalyzer();
			Query q = new QueryParser("BOOK_ID", analyzer).parse(querystr);

			//IndexSearcher searcher = new IndexSearcher(index);
			AllDocCollector collector = new IndexSearchUtil.AllDocCollector();
			searcher.search(q, collector);
			List<ScoreDoc> hits = collector.getHits();

			for (ScoreDoc score : hits) {
				result.add(searcher.doc(score.doc));
			}

			//searcher.close();
//			System.out.println("searchIndex end");
		}
		catch (IOException e) {
			System.out.println( "IndexSearchUtil.class, " +"[IOException]: searchIndex() "+e.getMessage());
		} catch (ParseException e) {
			System.out.println( "IndexSearchUtil.class, " +"[ParseException]: searchIndex() "+e.getMessage());
		} catch (Exception e) {			
			System.out.println( "IndexSearchUtil.class, " +"[Exception]: searchIndex() "+e.getMessage());
		}

		return result;
	}
	
	private static class  AllDocCollector extends HitCollector {
		List<ScoreDoc> docs = new ArrayList<ScoreDoc>();

		public void collect(int doc, float score) {
			if (score > 0.0f) {
				docs.add(new ScoreDoc(doc, score));
			}
		}

		@SuppressWarnings("unused")
		public void reset() {
			docs.clear();
		}

		public List<ScoreDoc> getHits() {
			return docs;
		}
	}
	
	
	public static void main(String[] arg){
		  String search = "BOOK_ID:CBO9780511470936";
		  System.out.println("hits: " + searchIndex("C:/indexer/books/bookindex", search).size());
	}
}


