/*
 * Created on Oct 2, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.production.searchejb.util;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
@Deprecated public class LogManager implements Serializable {
	
	static {
		DOMConfigurator.configure("log4j.xml");
	}
	
	private LogManager() {}	

	@Deprecated public static void debug(Object c, String s) {
		Logger.getLogger(c.getClass()).debug(s);
	}

	@Deprecated public static void info(Object c, String s) {
		Logger.getLogger(c.getClass()).info(s);
	} 
	
	@Deprecated public static void info(Class c, String s) {
		Logger.getLogger(c).info(s);
	} 

	@Deprecated public static void warn(Object c, String s) {
		Logger.getLogger(c.getClass()).warn(s);
	} 

	@Deprecated public static void error(Object c, String s) {
		try {
			Logger.getLogger(c.getClass()).
			error("["+ java.net.InetAddress.getLocalHost().getHostName() + "]" +
				  "["+ System.getProperty("jboss.server.name")+"]" + 
				  s);
		} catch(Exception e) { System.out.println("Error"); }	
	} 
	
	@Deprecated public static void error(Class c, String s) {
		try {
			Logger.getLogger(c).
			error("["+ java.net.InetAddress.getLocalHost().getHostName() + "]" +
				  "["+ System.getProperty("jboss.server.name")+"]" + 
				  s);
		} catch(Exception e) { System.out.println("Error"); }	
	}
	

	@Deprecated public static void success() { 
//		LogManager util = new LogManager();
//		info(util, "            _");
//		info(util, "           / )");
//		info(util, "        ,-(,' ,---. ");
//		info(util, "       (,-.\\,' `  _)-._ ");
//		info(util, "          ,' `(_)_)  ,-`--. ");
//		info(util, "         /          (      )  ");
//		info(util, "        /            `-.,-'|   IT IS DONE SIR! ");
//		info(util, "       /                |  /");
//		info(util, "       |               ,^ /");
//		info(util, "      /                   |");
//		info(util, "      |                   /");
//		info(util, "     /                   /");
//		info(util, "     |                   |");
//		info(util, "     |                   |");
//		info(util, "     /                    \\ ");
//		info(util, "   ,.|                    |");
//		info(util, " (`\\ |                    |");
//		info(util, " (\\  |   --.      /  \\_   |");
//		info(util, " (__(   ___)-.   | '' )  /)");
//		info(util, "     `---...\\\\\\--(__))/-'-'		");
	}
	
	@Deprecated public static void loggedOutWithNoerror() { 
		LogManager util = new LogManager();
		info(util, "    |.--------_--_------------_--__--.|");
		info(util, "    ||    /\\ |_)|_)|   /\\ | |(_ |_   ||");
		info(util, "    ;;`,_/``\\|__|__|__/``\\|_| _)|__ ,:|");
		info(util, "   ((_(-,-----------.-.----------.-.)`)");
		info(util, "    \\__ )        ,'     `.        \\ _/");
		info(util, "    :  :        |_________|       :  :");
		info(util, "    |-'|       ,'-.-.--.-.`.      |`-|");
		info(util, "    |_.|      (( (*  )(*  )))     |._|");
		info(util, "    |  |       `.-`-'--`-'.'      |  |");
		info(util, "    |-'|        | ,-.-.-. |       |._|");
		info(util, "    |  |        |(|-|-|-|)|       |  |");
		info(util, "    :,':        |_`-'-'-'_|       ;`.;");
		info(util, "     \\  \\     ,'           `.    /._/");
		info(util, "      \\/ `._ /_______________\\_,'  /");
		info(util, "       \\  / :   ___________   : \\,'");
		info(util, "        `.| |  |           |  |,'");
		info(util, "          `.|  |           |  |");
		info(util, "            |  | SSt       |  |");
	}

 
	@Deprecated public static void fatal(Object c, String s) {
		Logger.getLogger(c.getClass()).fatal(s);
	} 
}