package org.cambridge.ebooks.production.searchejb.document;

public class PublisherNameBean implements Comparable<PublisherNameBean>{
	private String name;
	private String order;
	
	private static final String COMMA = ",";
	
	private static final String SPACE = " ";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public int compareTo(PublisherNameBean o) {
		try {
			return this.order.compareTo(o.getOrder());
		} catch (Exception e) {
			return 0;
		}		
		
	}
	
	public static String fixPublisherName(String publisher){
		if(publisher.contains(COMMA)){
			String[] name = publisher.split(COMMA);
			return name[1].trim() + SPACE + name[0].trim();
		}else{
			return publisher;
		}
	}
	
}
