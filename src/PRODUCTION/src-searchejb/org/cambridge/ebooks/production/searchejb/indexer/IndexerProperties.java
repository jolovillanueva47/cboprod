/*
 * Created on Jan 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.production.searchejb.indexer;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IndexerProperties {
	
	public static String CHAPTER_LANDING_INDEX 	= "chapter.landing.index.dir";
	public static String BOOK_LANDING_INDEX 	= "book.landing.index.dir";
	public static String SEARCH_INDEX 			= "search.index.dir";
	public static String INDEX 					= "index.dir";
	public static String STOPWORDS 				= "file.stopwords";
	public static String SUBJECT_LANDING_INDEX	= "subject.index.dir";
	public static String SERIES_LANDING_INDEX	= "series.index.dir";
	
	//private static final String indexerProperties = "C:/dru/eclipse J2EE/workspace/cbo/PRODUCTION/src-searchejb/indexer.properties";
	//private static final String indexerProperties = "D:/data/work/ws/cbo/PRODUCTION/src-searchejb/indexer.properties";
	private static final String indexerProperties = "/app/ebooks/indexer/properties/indexer.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			e.getMessage();
		}
	}
}
