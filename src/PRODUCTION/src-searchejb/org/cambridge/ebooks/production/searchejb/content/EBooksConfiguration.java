package org.cambridge.ebooks.production.searchejb.content;

import org.cambridge.common.LogManager;

/**
 * 
 * @author rvillamor
 *
 */

public class EBooksConfiguration {

	public static String getProperty(String propertyName){
		String result = "";
		
		if ( StringUtil.isEmpty( System.getProperty(propertyName)) )
		{			
			LogManager.error(EBooksConfiguration.class, "Property name: " + propertyName + " has not been set on property-service.xml");
		}
		else
		{
			result = System.getProperty(propertyName).trim();
		}
		
		return result;
		
	}
	
}
