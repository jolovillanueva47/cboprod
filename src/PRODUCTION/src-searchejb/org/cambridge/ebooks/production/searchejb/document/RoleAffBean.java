package org.cambridge.ebooks.production.searchejb.document;

import org.cambridge.ebooks.production.searchejb.util.RoleLookupUtil;

/**
 * An object for storing role and affiliation
 * @author mmanalo
 *
 */
public class RoleAffBean implements Comparable<RoleAffBean>{
	
	private static final String COMMA = ",";
	private static final String SPACE = " ";
	private String author;	
	private String fixAuthor;
	private String role;
	private String affiliation;
	private String authorAlphasort;
	private String position;
	
	private boolean displayAuthor;
	private boolean displayAffiliation;
	
	private boolean displayAuthorAlphasort;
	
	public boolean isDisplayAuthor() {
		return displayAuthor;
	}
	public void setDisplayAuthor(boolean displayAuthor) {
		this.displayAuthor = displayAuthor;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getAuthorAlphasort() {
		return authorAlphasort;
	}
	public void setAuthorAlphaSort(String authorAlphasort) {		
		this.authorAlphasort = authorAlphasort;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = RoleLookupUtil.getRole(role);
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	
	public String toString() {
		return getAuthor() + "-" + getRole() + "-" + getAffiliation();
	}
	
	public int compareTo(RoleAffBean o) {
		return this.position.compareTo(o.getPosition());		
	}
	
	public static String fixAuthorName(String author){
		if(author.contains(COMMA)){
			String[] name = author.split(COMMA);
			return name[1].trim() + SPACE + name[0].trim();
		}else{
			return author;
		}
	}
	
	public boolean isDisplayAffiliation() {
		return displayAffiliation;
	}
	public void setDisplayAffliation(boolean displayAffiliation) {
		this.displayAffiliation = displayAffiliation;
	}
	public boolean isDisplayAuthorAlphasort() {
		return displayAuthorAlphasort;
	}
	public void setDisplayAuthorAlphasort(boolean displayAuthorAlphasort) {
		this.displayAuthorAlphasort = displayAuthorAlphasort;
	}
	/**
	 * @return the fixAuthor
	 */
	public String getFixAuthor() {
		return fixAuthor;
	}
	/**
	 * @param fixAuthor the fixAuthor to set
	 */
	public void setFixAuthor(String fixAuthor) {
		this.fixAuthor = fixAuthorName(fixAuthor);
	}

}
