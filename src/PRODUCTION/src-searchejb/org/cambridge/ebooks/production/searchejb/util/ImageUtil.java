package org.cambridge.ebooks.production.searchejb.util;

import java.io.File;

import org.cambridge.ebooks.production.searchejb.util.IsbnContentDirUtil;


public class ImageUtil {
	
	private static final String BOOKBEAN_NAME = "bookContentBean";

	/**
	 * validates if the image exists, replaces with a default no cover when it
	 * does not exists
	 * 
	 * @param image
	 * @return
	 */
	public static String setImagePath(String image, String noImageFile, String isbn, String imageDirPath, Class<?> className) {
//		String imagePath = contentDir + isbn + "/" + image;
		String imagePath = IsbnContentDirUtil.getPath(isbn) + "/" + image;
		File file = new File(imagePath);
		
		//LogManager.info(className, "Finding file in : " + imagePath);
		//LogManager.info(className, "Can I read the file? : "+ file.canRead());
		
		if (file.canRead()) {
			return imageDirPath + isbn + "/" + image;
		} else {
			return noImageFile;
		}

	}
	
	/**
	 * same as setImagepath except that this gets the isbn from request attribute for the bean
	 * @param image
	 * @param noImageFile
	 * @param contentDir
	 * @param imageDirPath
	 * @param className
	 * @return
	 */
//	public static String setImagePath(String image, String noImageFile, 
//			String contentDir, String imageDirPath, Class<?> className) {
//		HttpServletRequest request = HttpUtil.getHttpServletRequest();
//		EBookContentManagedBean bean = (EBookContentManagedBean) request
//				.getAttribute(BOOKBEAN_NAME);
//		String eisbn = "";
//		if(bean != null){
//			eisbn = bean.getEisbn();
//		}else{
//			HttpSession session = HttpUtil.getHttpSession(false);
//			bean = (EBookContentManagedBean) session.getAttribute(BOOKBEAN_NAME);
//			if(bean != null){
//				eisbn = bean.getEisbn();
//			}
//		}
//		
//		return setImagePath(image, noImageFile, contentDir, eisbn, 
//				imageDirPath, className);
//		
//	}
}
