package org.cambridge.ebooks.production.searchejb.document;

public class OtherEditionBean implements Comparable<OtherEditionBean>{
	private String number;
	private String isbn;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public int compareTo(OtherEditionBean o) {
		return this.number.compareTo(o.getNumber());		
	}
	
	
}
