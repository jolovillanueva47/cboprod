package org.cambridge.ebooks.production.searchejb;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.cambridge.ebooks.production.searchejb.content.IndexSearchUtil;
import org.cambridge.ebooks.production.searchejb.content.SearchForIndented;
import org.cambridge.ebooks.production.searchejb.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.production.searchejb.document.EbookDetailsBean;
import org.cambridge.ebooks.production.searchejb.document.PublisherNameBean;
import org.cambridge.ebooks.production.searchejb.document.RoleAffBean;
import org.cambridge.ebooks.production.searchejb.document.SubjectBean;
import org.cambridge.ebooks.production.searchejb.ebook.EBookIndexSearch;
import org.cambridge.ebooks.production.searchejb.indexer.IndexerProperties;
import org.cambridge.ebooks.production.searchejb.jpa.EBook;
import org.cambridge.ebooks.production.searchejb.util.EncodingUtil;
import org.cambridge.ebooks.production.searchejb.util.StringUtil;
import org.cambridge.util.Misc;

@MessageDriven(name = "SearchMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/search/logger"),
		@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1") })	
		
@TransactionManagement(TransactionManagementType.BEAN)	
public class SearchMDB implements MessageListener {
	
	private static final long serialVersionUID = 1L;
	
	private static final String PART_TITLE = "part_title";
	
	private static final String YES = "Y";
	
	private static final String NO = "N";
	
	private IndexWriter writer; // new index being built
	
	private static String[] ENGLISH_STOP_WORDS;
	
	private static final String BOOK_ID = "BOOK_ID";
	
	private static final String CONTRIBUTOR = "CONTRIBUTOR";
	
	private static final String CONTRIBUTOR_COLLAB = "CONTRIBUTOR_COLLAB";
	
	private static final String AFFILIATION = "AFFILIATION";	
	
	private static final String CONTENT_ID = "CONTENT_ID";
	
	private static final String DOC_TYPE = "DOC_TYPE";
	
	private static final String PARENT_ID = "PARENT_ID";
	
	private static final String BOOK_CONTRIBUTOR = "book_contributor";
	
	private static final String CONTENT_DETAILS = "content_details";
	
	private static final String[] STRIP_CHARS = {"&#8211;", "&#8208;", ",", "\\."};
	
	private static final String DELIMITER = "=DEL=";
	
//	public static final String[] STOP_WORDS = {
//		"a", "about", "after", "all", "also", "an", "and",
//		"any", "are", "as", "at", "be", "because",
//		"been", "but", "by", "can", "co", "corp",
//		"could", "for", "from", "had", "has", "have",
//		"he", "her", "his", "if", "in", "inc", "into",
//		"is", "it", "its", "last", "more", "most", "mr",
//		"mrs", "ms", "mz", "no", "not", "of", "on", "one",
//		"only", "or", "other", "out", "over", "says",
//		"she", "so", "some", "such", "than", "that", "the",
//		"their", "there", "they", "this", "to", "up",
//		"was", "we", "were", "when", "which", "who", 
//		"will", "with", "would"
//	};
		
	@Resource
	private MessageDrivenContext mdc;
	
    @Resource
    SessionContext sc;
    
    private UserTransaction ut;
    
	//@PersistenceUnit(unitName = "OnlineService")	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("OnlineService");
	
	private EntityManager em;
	
	private String liveDate;
	
	private String liveDateNum;
		
	/**
	 * main ejb method
	 */
	public void onMessage(Message recvMsg) {
		Logger.getLogger(SearchMDB.class).info("Search EJB called");

		UserTransaction ut = sc.getUserTransaction();
		
		if ( recvMsg instanceof Message ) {
			Message payload = (Message) recvMsg;
			
			try {
				String bookId = payload.getStringProperty("BOOK_ID");
				
				if(payload.getStringProperty("TYPE").equals("publish")){
					System.out.println("--- BOOK_ID:" + bookId);
					liveDate = getLiveDate(bookId);
					liveDateNum = getLiveDateNum(bookId);
					
					// search index for ebook details
					EbookDetailsBean ebook = new EbookDetailsBean(bookId);
						
					// search index for chapter details
					List<String> contentIds = new ArrayList<String>();
					contentIds = EBookIndexSearch.getContentIds(bookId);				
									 
					HashMap<String, String> eContentMap = SearchForIndented.setIndentedHashmap(bookId);
					HashMap<String, String> tlMap = SearchForIndented.setTopLevelHashmap(bookId);					
					
					List<EbookChapterDetailsBean> chapters = new ArrayList<EbookChapterDetailsBean>();
					EbookChapterDetailsBean ebookChapter;
					
					for(String id : contentIds){
						ebookChapter = new EbookChapterDetailsBean();
						ebookChapter.show(id);					
						chapters.add(ebookChapter);
					
					}
					
					// search index for chapter contents
					Hashtable<String, String> contents = new Hashtable<String,String>();
					contents = getContents(chapters);
					
					try {
						createAdvanceSearchIndex(ebook, chapters, contents, eContentMap);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					try {
						createBookLandingIndex(ebook, chapters, eContentMap, tlMap);
					} catch (Exception e) {					
						e.printStackTrace();
					}
					try {
						createChapterLandingIndex(ebook, chapters, eContentMap, tlMap);
					} catch (Exception e) {					
						e.printStackTrace();
					}
					try {
						createSubjectLandingIndex(ebook, chapters, eContentMap);
					} catch (Exception e) {					
						e.printStackTrace();
					}
					
					// series indexer
					if(ebook.getSeries() != null && ebook.getSeries().length() > 0){
						try {
							createSeriesLandingIndex(ebook, chapters, eContentMap);
						} catch (Exception e) {					
							e.printStackTrace();
						}
					}
				}else if(payload.getStringProperty("TYPE").equals("unpublish")){					
					deleteLuceneDocuments(bookId, IndexerProperties.SEARCH_INDEX);
					deleteLuceneDocuments(bookId, IndexerProperties.BOOK_LANDING_INDEX);
					deleteLuceneDocuments(bookId, IndexerProperties.CHAPTER_LANDING_INDEX);
					deleteLuceneDocuments(bookId, IndexerProperties.SUBJECT_LANDING_INDEX);
					deleteLuceneDocuments(bookId, IndexerProperties.SERIES_LANDING_INDEX);
				}
			} catch (JMSException e) {
				e.printStackTrace();
				mdc.setRollbackOnly();
			}
		}
	}
	
	/**
	 * get contents of chapters
	 * @param chapters
	 * @return
	 */
	public Hashtable<String, String> getContents(List<EbookChapterDetailsBean> chapters){
		Hashtable<String, String> contentsMap = new Hashtable<String, String>();
		String content;
		for(EbookChapterDetailsBean chapter : chapters){
			content = EBookIndexSearch.getContent(chapter.getContentId());

			//Remove Copyright so it wont be indexed
			content.replaceAll("Cambridge Books Online &#169; Cambridge University Press, 2009", "");
								
			contentsMap.put(chapter.getContentId(), content);
		}
		return contentsMap;
	} 
	
	
	/**
	 * checks if index is locked
	 * @param indexDir
	 * @throws IOException
	 */
	private void checkIfIndexIsLocked(String indexDir) throws IOException{
		if(IndexWriter.isLocked(FSDirectory.getDirectory(System.getProperty(indexDir))))
		{
			System.out.println("LOCKED! [" + System.getProperty(indexDir) + "]");
			//add custom method here andreu
			//SYSTEM.EXIT
		}
	}
	
	/**
	 * gets a new index writer
	 * @param indexDir
	 * @return
	 * @throws Exception
	 */
	private IndexWriter getIndexWriter(String indexDir) throws Exception{
		IndexWriter writer;
		writer = new IndexWriter(System.getProperty(indexDir), 
				new WhitespaceAnalyzer(), 
				checkIfCreateIndex(indexDir), 
				IndexWriter.MaxFieldLength.LIMITED);

		writer.setMaxFieldLength(Integer.MAX_VALUE);
		writer.setMergeFactor(20);
		writer.setUseCompoundFile(false);
		return writer;
	}
	
	private IndexWriter getIndexWriter(String indexDir, Analyzer analyzer) throws Exception{
		IndexWriter writer;
		writer = new IndexWriter(System.getProperty(indexDir), 
				analyzer, 
				checkIfCreateIndex(indexDir), 
				IndexWriter.MaxFieldLength.LIMITED);

		writer.setMaxFieldLength(Integer.MAX_VALUE);
		writer.setMergeFactor(20);
		writer.setUseCompoundFile(false);
		return writer;
	}
	
	/**
	 * main method for advanced search index
	 * @param ebook
	 * @param chapters
	 * @param contents
	 * @throws Exception 
	 */
	public void createAdvanceSearchIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters, 
			Hashtable<String, String> contents, HashMap<String,String> eContents) 
			throws Exception{
		System.out.println("--- SEARCH INDEX START ---");
		// Advance Search Indexer
		try {
			checkIfIndexIsLocked(IndexerProperties.SEARCH_INDEX);
			
			writer = getIndexWriter(IndexerProperties.SEARCH_INDEX, new SnowballAnalyzer("English"));
	        
			indexDocs(ebook, chapters, contents, eContents);
			
			writer.setUseCompoundFile(true);
			writer.commit();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--- SEARCH INDEX END ---");
	}
	
	/**
	 * checks if will create a new index or incremental indexing only
	 * returns true to create
	 * false to append
	 * @return
	 */
	private boolean checkIfCreateIndex(String indexDir){
		File file = new File(System.getProperty(indexDir));
		String list[] = file.list(); 
		boolean create = true; 

		if (list != null && list.length > 0) create = false;
		
		return create;
	}
	
	/**
	 * main method for creating book landing Index
	 * @param ebook
	 * @param chapters
	 * @param contents
	 * @throws Exception 
	 */
	public void createBookLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
			HashMap<String, String> contents, HashMap<String,String> tlMap) throws Exception{
		System.out.println("--- BOOK LANDING INDEX START ---");
		// Book Landing Page Indexer
		try {	        
			checkIfIndexIsLocked(IndexerProperties.BOOK_LANDING_INDEX);
			
			writer = getIndexWriter(IndexerProperties.BOOK_LANDING_INDEX);			
	        
	        //Per book
			indexBookLandingPerBook(ebook);			
			
			// Per author	        
			indexBookLandingPerAuthor(ebook, false, null);		
			
			// Per subject	        
			indexBookPerSubject(ebook, false, null);			
						
			// Per content-item	        
	        indexBookPerContent(ebook, chapters, contents, tlMap);
	        
	        // Per contributor
	        indexChapterPerContributor(ebook.getBookId());
			
	        writer.setUseCompoundFile(true);
	        writer.commit();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--- BOOK LANDING INDEX END ---");
	}
	
	/**
	 * main method for creating subject landing Index
	 * @param ebook
	 * @param chapters
	 * @param contents
	 * @throws Exception 
	 */
	public void createSubjectLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
			HashMap<String, String> contents) throws Exception{
		System.out.println("--- SUBJECT INDEX START ---");
		// Book Landing Page Indexer
		try {	        
			checkIfIndexIsLocked(IndexerProperties.SUBJECT_LANDING_INDEX);
			
			writer = getIndexWriter(IndexerProperties.SUBJECT_LANDING_INDEX);			
	        
	        //Per book
			indexSubjectLandingPerBook(ebook);			
			
			writer.setUseCompoundFile(true);
			writer.commit();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--- SUBJECT INDEX END ---");
	}
	
	/**
	 * main method for creating series landing Index
	 * @param ebook
	 * @param chapters
	 * @param contents
	 * @throws Exception 
	 */
	public void createSeriesLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
			HashMap<String, String> contents) throws Exception{
		System.out.println("--- SERIES INDEX START ---");
		// Book Landing Page Indexer
		try {	        
			checkIfIndexIsLocked(IndexerProperties.SERIES_LANDING_INDEX);
			
			writer = getIndexWriter(IndexerProperties.SERIES_LANDING_INDEX);			
	        
	        //Per book
			indexSeriesLandingPerBook(ebook);			
			
			writer.setUseCompoundFile(true);
			writer.commit();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--- SERIES INDEX END ---");
	}
	
	/**
	 * indexes contributor per contentItem
	 * @param bookId
	 */
	private void indexChapterPerContributor(String bookId) {
		try {			
			checkIfIndexIsLocked(IndexerProperties.INDEX);			
			
			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(IndexerProperties.INDEX), 
					"ELEMENT: contributor AND BOOK_ID: " + bookId);		
			for(Document doc : docs ){						
				String name = doc.get(CONTRIBUTOR);
				String aff = doc.get(AFFILIATION);
				String collab = doc.get(CONTRIBUTOR_COLLAB);
				Document newDoc = new Document();
				if(Misc.isNotEmpty(name)) { 				
					newDoc.add(new Field(CONTRIBUTOR, name, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				}
				if(Misc.isNotEmpty(aff)) { 				
					newDoc.add(new Field(AFFILIATION, aff, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				}
				if(Misc.isNotEmpty(collab)) {
					newDoc.add(new Field(CONTRIBUTOR_COLLAB, collab, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				}
				
				//DOC_TYPE
				newDoc.add(new Field(DOC_TYPE, BOOK_CONTRIBUTOR, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				//BOOK_ID
				newDoc.add(new Field(BOOK_ID, bookId, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				newDoc.add(new Field(CONTENT_ID, doc.get(PARENT_ID), Field.Store.YES, Field.Index.ANALYZED_NO_NORMS));
				
				writer.addDocument(newDoc);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void indexContentContributor(String contentId, Document document) {
		try {			
			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(IndexerProperties.INDEX), 
					"ELEMENT: contributor AND PARENT_ID: " + contentId);	
			
			List<String> contributors = new ArrayList<String>();
			List<String> contribCollabs = new ArrayList<String>();
			ArrayList<String> allContribs = new ArrayList<String>();
			
			for(Document doc : docs){						
				String name = doc.get(CONTRIBUTOR);
				String collab = doc.get(CONTRIBUTOR_COLLAB); 
				if(Misc.isNotEmpty(name)) { 
					contributors.add(name);
				}
				if(Misc.isNotEmpty(collab)) {
					contribCollabs.add(collab);
				}
			}
			allContribs.addAll(contributors);
			allContribs.addAll(contribCollabs);
			allContribs.trimToSize();
			
			String contributorDisplay = StringUtil.getContributorDisplay(allContribs);
			String contributor = StringUtil.getContributors(allContribs);
			String contributorAlphasort = StringUtil.getContributorsAlphasort(contributors);
			String contributorCollab = StringUtil.getContributors(contribCollabs);
			if(Misc.isNotEmpty(contributor)) {
				document.add(new Field("CONTRIBUTOR", contributor.trim() + " " + contributorAlphasort.trim(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));				
				document.add(new Field("CONTENT_CONTRIBUTORS", contributor.trim(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				document.add(new Field("CONTENT_CONTRIBUTORS_DISPLAY", contributorDisplay.trim(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
				document.add(new Field("CONTENT_CONTRIBUTOR_COLLAB", contributorCollab.trim(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * create chapter landing index
	 * @param ebook
	 * @param chapters
	 * @param contents
	 * @throws Exception 
	 */
	public void createChapterLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
			HashMap<String,String> contents, HashMap<String,String> tlMap) throws Exception{
		System.out.println("--- CHAPTER LANDING INDEX START ---");
		// Book Landing Page Indexer
		try {
			checkIfIndexIsLocked(IndexerProperties.CHAPTER_LANDING_INDEX);
			
			
			writer = getIndexWriter(IndexerProperties.CHAPTER_LANDING_INDEX);
	        
			// per content
			indexChapterLandingPerContent(ebook, chapters, contents, tlMap);			
			
			// Per author	  
			//indexChapterBookLandingPerAuthor(ebook, chapters);				        
			indexBookLandingPerAuthor(ebook, false, null);		
						
			// per subject			
			//indexChapterBookPerSubject(ebook, chapters);
			indexBookPerSubject(ebook, false, null);
			
			// per content-item
			//indexBookPerContent(ebook, chapters);			

			// Per book contributor
			indexChapterPerContributor(ebook.getBookId());

			writer.setUseCompoundFile(true);
			writer.commit();
			writer.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--- CHAPTER LANDING INDEX END ---");
	}
	
	
	/**
	 * create per content chapter landing
	 * @param ebook
	 * @param chaptersList
	 * @param contents
	 */
	private void indexChapterLandingPerContent(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList,
			HashMap<String,String> contents, HashMap<String,String> tlMap){		
		if(chaptersList != null && chaptersList.size() > 0)	
		{
			Document doc;
			for(EbookChapterDetailsBean chapter : chaptersList) 
			{
				doc = new Document();
				try 
				{					
					doc.add(new Field(DOC_TYPE, CONTENT_DETAILS, Field.Store.YES, Field.Index.NOT_ANALYZED));
					// ebook
					if(Misc.isNotEmpty(ebook.getBookId()))			
						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(ebook.getMainTitle())) 			
						doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
						doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(), Field.Store.YES, Field.Index.NO));
					}
					if(Misc.isNotEmpty(ebook.getSubTitle())) 			
						doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getSeriesCode())) 			
						doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getSeries())) 				
						doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getSeriesNumber())) 				
						doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getCopyrightStatement())) 	
						doc.add(new Field("COPYRIGHT", removeBr(ebook.getCopyrightStatement()),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getPrintDate())) 			
						doc.add(new Field("PRINT_PUBLICATION_DATE", removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getOnlineDate())) 			
						doc.add(new Field("ONLINE_PUBLICATION_DATE", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getEisbn())) 				
						doc.add(new Field("ONLINE_ISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getHardback()))			
						doc.add(new Field("HARDBACK_ISBN", ebook.getHardback(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getPaperback())) 			
						doc.add(new Field("PAPERBACK_ISBN", ebook.getPaperback(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getDoi())) 				
						doc.add(new Field("BOOK_DOI", ebook.getDoi(),Field.Store.YES, Field.Index.NO));
					
					if(Misc.isNotEmpty(ebook.getStandardImage())) 		
						doc.add(new Field("COVER_IMAGE", ebook.getStandardImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(ebook.getThumbImage())) 			
						doc.add(new Field("COVER_IMAGE_THUMBNAIL", ebook.getThumbImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
					
					
					if(Misc.isNotEmpty(ebook.getVolume())) { 		
						doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
					} 
					if(Misc.isNotEmpty(ebook.getOtherVolume())) { 		
						doc.add(new Field("OTHER_VOLUME", ebook.getOtherVolume(), Field.Store.YES, Field.Index.ANALYZED));
					} 
					if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
						doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
						doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}  
					if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
						doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}  
					if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
						doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					//additional
					if(Misc.isNotEmpty(ebook.getEditionNumber())) { 	
						doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					if(Misc.isNotEmpty(ebook.getEdition())) {
						doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED));
					}
//					String liveDate = getLiveDate(ebook.getBookId());
					if(Misc.isNotEmpty(liveDate)) {
						doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					
					// publisher 
					if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
						List<PublisherNameBean> publisherList = ebook.getPubNameList();
						StringBuffer publisherDisplay = new StringBuffer();
						for (Iterator<PublisherNameBean> it = publisherList.iterator();it.hasNext();) {
							PublisherNameBean bean = it.next();
							publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
							if (it.hasNext()) {
								publisherDisplay.append(", ");
							}
						}
						doc.add(new Field("PUBLISHER", publisherDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					
					// chapter
					if(Misc.isNotEmpty(chapter.getContentId())) 		
						doc.add(new Field("CONTENT_ID", chapter.getContentId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(chapter.getHeadingTitle()))		
						doc.add(new Field("CONTENT_TITLE", chapter.getHeadingTitle(), Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
						doc.add(new Field("CONTENT_LABEL", chapter.getHeadingLabel(), Field.Store.YES, Field.Index.NO));
					}
					if(Misc.isNotEmpty(chapter.getHeadingSubtitle()))	
						doc.add(new Field("CONTENT_SUB_TITLE", chapter.getHeadingSubtitle(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getPageStart())) 		
						doc.add(new Field("FIRST_PAGE", chapter.getPageStart(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getPageEnd())) 		
						doc.add(new Field("PAGE", "pp. " + chapter.getPageStart() + "-" + chapter.getPageEnd(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getPdfFilename()))		
						doc.add(new Field("PDF", chapter.getPdfFilename(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getDoi())) 				
						doc.add(new Field("CHAPTER_DOI", chapter.getDoi(),Field.Store.YES, Field.Index.NO));
					
					indexContentContributor(chapter.getContentId(), doc);
					
//					if(chapter.getContributors().size() > 0) {
//						doc.add(new Field("CONTENT_CONTRIBUTORS", 
//								StringUtil.getContributorDisplay(chapter.getContributors()), Field.Store.YES, Field.Index.NO));
//					}
//					if(Misc.isNotEmpty(chapter.getContributorNameModified()))	
//						doc.add(new Field("CHAPTER_CONTRIBUTORS", chapter.getContributorNameModified(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getAbstractContent()))	
						doc.add(new Field("CONTENT_EXTRACT", chapter.getAbstractContent(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getAbstractImage()))	
						doc.add(new Field("CONTENT_IMAGE_EXTRACT", chapter.getAbstractImage(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getParentId())) {	
						doc.add(new Field("PARENT_ID", chapter.getParentId(),Field.Store.YES, Field.Index.ANALYZED));
					}
					if(Misc.isNotEmpty(chapter.getProblem())) {						
						doc.add(new Field("PROBLEM", chapter.getProblem(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					
					if(Misc.isNotEmpty(chapter.getContentType())) {
						doc.add(new Field("CONTENT_TYPE", chapter.getContentType(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					
					//getIfContains Subchapter
					doc.add(new Field("CONTENT_SUB_CHAPTERS", hasSubchapters(contents, chapter),Field.Store.YES, Field.Index.NO));
					
					//if it is top level
					doc.add(new Field("BOOK_LEVEL", isTopLevel(tlMap, chapter), Field.Store.YES, Field.Index.ANALYZED));
					
					// TOC_ITEM
					if(Misc.isNotEmpty(chapter.getToc())) {
						doc.add(new Field("TOC_ITEM", 
								chapter.getToc().replaceAll("<em>", "")
								.replaceAll("</em>", "")
								.replaceAll("<br />", "=DEL="), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS)
						);
					}
					
					writer.addDocument(doc);
				}
				catch (Exception e) 
				{
					Logger.getLogger(SearchMDB.class).error("ChapterLanding: Error while adding Document per content: "+ e.getMessage());					
			 	}
			}
		}
	}
	
	private void indexDocs(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList, 
			Hashtable<String, String> contents, HashMap<String,String> eContent){		
		if(chaptersList != null && chaptersList.size() > 0) {
			Document doc;
			for(EbookChapterDetailsBean chapter : chaptersList) {
				doc = new Document();
				try {
					// ebook - INDEXED/TOKENIZED
					// AUTHOR AND ROLE
					StringBuffer authorEditorContributor = new StringBuffer();
					if(!Misc.isEmpty(ebook.getAuthorAffList())) {
						StringBuilder authors = new StringBuilder();
						StringBuilder authorAlphasorts = new StringBuilder();
						StringBuilder authorAffStrBldr = new StringBuilder();
						
						List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
						for(RoleAffBean roleAff : authorAffList) {							
							authors.append(RoleAffBean.fixAuthorName(roleAff.getAuthor()));
							authorAlphasorts.append(roleAff.getAuthorAlphasort());
							authorAffStrBldr.append(roleAff.getAffiliation());							
							authors.append(" ");
							authorAlphasorts.append(" ");
							authorAffStrBldr.append(" ");
						}
						
						if(Misc.isNotEmpty(authors.toString())) {
							authorEditorContributor.append(stripHTMLTags(authors.toString()) + " ");
						}
						if(Misc.isNotEmpty(authorAlphasorts.toString())) {
							authorEditorContributor.append(authorAlphasorts.toString() + " ");
						}						
						
						// AUTHOR_AFFILIATION
						if(Misc.isNotEmpty(authorAffStrBldr.toString())) {
							doc.add(new Field("AUTHOR_AFFILIATION", authorAffStrBldr.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));			
						}
					}					

					// AUTHOR_EDITOR_CONTIBUTOR
					if(Misc.isNotEmpty(ebook.getEditor())) {
						authorEditorContributor.append(stripHTMLTags(ebook.getEditor()) + " ");
						authorEditorContributor.append(ebook.getEditorAlphasort() + " ");
					}
					if(Misc.isNotEmpty(authorEditorContributor.toString())) {
						doc.add(new Field("AUTHOR_EDITOR", authorEditorContributor.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));			
					}				
										
					// SUBJECT
					if(!Misc.isEmpty(ebook.getSubjectList())) {
						List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
						subjectList = ebook.getSubjectList();
						StringBuffer subjectSb = new StringBuffer();
						for(SubjectBean subject : subjectList){
							subjectSb.append(subject.getSubject() + " ");
						}
						if(Misc.isNotEmpty(subjectSb.toString())) {
							doc.add(new Field("SUBJECT", subjectSb.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
						}
					}
					
					// PUBLICATION DATE
					StringBuffer publicationDate = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getOnlineDate()))
						publicationDate.append(ebook.getOnlineDate() + " ");
					if(Misc.isNotEmpty(ebook.getPrintDate()))
						publicationDate.append(ebook.getPrintDate() + " ");
					
//					String liveDate = getLiveDate(ebook.getBookId());
					if(Misc.isNotEmpty(liveDate)) {
						publicationDate.append(liveDate + " ");
						doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}	
					
					if(Misc.isNotEmpty(liveDateNum)) {
						doc.add(new Field("PUBLICATION_DATE_NUM", liveDateNum, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					if(Misc.isNotEmpty(publicationDate.toString())) { 
						doc.add(new Field("PUBLICATION_DATE", publicationDate.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					// TITLE_VOLUME
					StringBuffer titleVolume = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getMainTitle())) {
						titleVolume.append(ebook.getMainTitle()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getSubTitle())) {
						titleVolume.append(ebook.getSubTitle()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
						titleVolume.append(ebook.getMainTitleAlphaSort()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getVolume())) {
						titleVolume.append(ebook.getVolume() + " ");
					}
					if(Misc.isNotEmpty(ebook.getEdition())) {
						titleVolume.append(ebook.getEdition() + " ");
					}
					if(Misc.isNotEmpty(titleVolume.toString())) {
						doc.add(new Field("TITLE_VOLUME", 
								stripUnicodes(stripHTMLTags(titleVolume.toString())), 
								Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					// ISBN
					StringBuffer isbn = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getEisbn()))
						isbn.append(ebook.getEisbn());
					if(Misc.isNotEmpty(ebook.getHardback()))
						isbn.append(" "+ebook.getHardback());
					if(Misc.isNotEmpty(ebook.getPaperback()))
						isbn.append(" "+ebook.getPaperback());
					if(Misc.isNotEmpty(isbn.toString())) 		
						doc.add(new Field("ISBN", isbn.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					
					
					// SERIES
					if(Misc.isNotEmpty(ebook.getSeries())) {
						String series = ebook.getSeries() + " ";
						if(Misc.isNotEmpty(ebook.getSeriesNumber())) {
							series = series + ebook.getSeriesNumber();
						}
						doc.add(new Field("SERIES_NAME", series, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					if(Misc.isNotEmpty(ebook.getSeriesNumber())) {		
						doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					// SERIES - INDEXED/STORED
					if(Misc.isNotEmpty(ebook.getSeries())) {
						doc.add(new Field("SERIES_NAME_DISPLAY", ebook.getSeries(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					// DOI
					StringBuffer doi = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getDoi())) {
						doi.append(ebook.getDoi());
					}
					if(Misc.isNotEmpty(chapter.getDoi())) {
						doi.append(" "+chapter.getDoi());	
					}
					if(Misc.isNotEmpty(doi.toString())) {			
						doc.add(new Field("DOI", doi.toString(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					// KEYWORDS
					if(Misc.isNotEmpty(chapter.getKeyword())) {
						doc.add(new Field("KEYWORDS", stripHTMLTags(stripUnicodes(chapter.getKeyword())), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					// ebook - STORED
					if(Misc.isNotEmpty(ebook.getBookId())) 		
						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getEisbn())) 		
						doc.add(new Field("EISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					if(Misc.isNotEmpty(ebook.getHardback())) {
						doc.add(new Field("PISBN", ebook.getHardback(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));	
					} else if(Misc.isNotEmpty(ebook.getPaperback())) {
						doc.add(new Field("PISBN", ebook.getPaperback(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));	
					}
					
					if(Misc.isNotEmpty(ebook.getMainTitle())) 	
						doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) 	
						doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getSubTitle())) 	
						doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getSeriesCode()))
						doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getOnlineDate()))
						doc.add(new Field("ONLINE_PUBLICATION_DATE", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(ebook.getPrintDate()))
						doc.add(new Field("PRINT_DATE", removeBr(ebook.getPrintDate()), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					if(Misc.isNotEmpty(ebook.getCopyrightStatement()))
						doc.add(new Field("COPYRIGHT", removeBr(ebook.getCopyrightStatement()),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					if(Misc.isNotEmpty(ebook.getVolume())) { 		
						doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED_NO_NORMS));
					} 
					if(Misc.isNotEmpty(ebook.getOtherVolume())) { 		
						doc.add(new Field("OTHER_VOLUME", ebook.getOtherVolume(), Field.Store.YES, Field.Index.ANALYZED_NO_NORMS));
					} 
					if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
						doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
						doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}  
					if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
						doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}  
					if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
						doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					} 
					
					//additional
					if(Misc.isNotEmpty(ebook.getEditionNumber())) { 	
						doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					if(Misc.isNotEmpty(ebook.getEdition())) {
						doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED_NO_NORMS));
					}
					
					// AUTHOR DISPLAY
					if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
						List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
						StringBuilder authorsDisplay = new StringBuilder();
						StringBuilder authorsSort = new StringBuilder();
						StringBuilder bookAuthorAffStrBldr = new StringBuilder();
						StringBuilder foreword = new StringBuilder();
						
						int ctr = 0;
						for(RoleAffBean roleAffBean : authorAffList) {
							ctr++;
							authorsDisplay.append(RoleAffBean.fixAuthorName(roleAffBean.getAuthor()));
							String authorAlphaSort = Misc.isNotEmpty(roleAffBean.getAuthorAlphasort()) ? 
									roleAffBean.getAuthorAlphasort() : roleAffBean.getAuthor();
							authorsSort.append(authorAlphaSort);
							bookAuthorAffStrBldr.append(roleAffBean.getAffiliation());							
							foreword.append(roleAffBean.getRole());
							
							if(ctr < authorAffList.size()) {
								authorsDisplay.append(DELIMITER);	
								bookAuthorAffStrBldr.append(DELIMITER);
								foreword.append(DELIMITER);
							}
							authorsSort.append(" ");
						}
						if(Misc.isNotEmpty(foreword.toString())) {	
							doc.add(new Field("FOREWORD", foreword.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
						}
						doc.add(new Field("BOOK_AUTHORS", authorsDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
						doc.add(new Field("BOOK_AUTHOR_AFFILIATION_DISPLAY", bookAuthorAffStrBldr.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
						doc.add(new Field("BOOK_AUTHORS_ALPHASORT", authorsSort.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					// chapter - STORED
					if(Misc.isNotEmpty(chapter.getContentId())) 	
						doc.add(new Field("CONTENT_ID", chapter.getContentId(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(chapter.getHeadingTitle()))	
						doc.add(new Field("CONTENT_TITLE", chapter.getHeadingTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
						doc.add(new Field("CONTENT_LABEL", chapter.getHeadingLabel(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					if(Misc.isNotEmpty(chapter.getHeadingSubtitle()))	
						doc.add(new Field("CONTENT_SUB_TITLE", chapter.getHeadingSubtitle(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(chapter.getPdfFilename()))	
						doc.add(new Field("PDF", chapter.getPdfFilename(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					if(Misc.isNotEmpty(chapter.getDoi())) { 			
						doc.add(new Field("CONTENT_DOI", chapter.getDoi(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					} else {
						doc.add(new Field("CONTENT_DOI", "null", Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					if(Misc.isNotEmpty(chapter.getPageStart()) 
					&& Misc.isNotEmpty(chapter.getPageEnd())) 		
						doc.add(new Field("PAGE", "pp "+chapter.getPageStart()+"-"+chapter.getPageEnd(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					indexContentContributor(chapter.getContentId(), doc);
					
					if(Misc.isNotEmpty(chapter.getAbstractContent()))
						doc.add(new Field("CONTENT_EXTRACT", chapter.getAbstractContent(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					if(Misc.isNotEmpty(chapter.getAbstractImage()))
						doc.add(new Field("CONTENT_IMAGE_EXTRACT", chapter.getAbstractImage(),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					// chapter - INDEXED/TOKENIZED
					if(Misc.isNotEmpty(chapter.getContentId())
							&& Misc.isNotEmpty(contents.get(chapter.getContentId()))) {	

						//Remove Copyright so it wont be indexed
						doc.add(new Field("CONTENT", stripUnicodes(stripHTMLTags(
								contents.get(chapter.getContentId()).replaceAll("(?i)(cambridge books online &#169; cambridge university press, 2009)", ""))), 
								Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
					}
					
					if(Misc.isNotEmpty(chapter.getContentType())) {
						doc.add(new Field("CONTENT_TYPE", chapter.getContentType(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					if(Misc.isNotEmpty(chapter.getContentId())
							&& Misc.isNotEmpty(contents.get(chapter.getContentId()))){
						//Remove Copyright so it wont be indexed
						doc.add(new Field("CONTENT_DISPLAY", contents.get(chapter.getContentId()).replaceAll("(?i)(Cambridge Books Online &#169; Cambridge University Press, 2009)", ""),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}
					
					//System.out.println("test " + contents.get(chapter.getContentId()));
					//getIfContains Subchapter
					doc.add(new Field("CONTENT_SUB_CHAPTERS", hasSubchapters(eContent, chapter),Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					
					// publisher 
					if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
						//List<PublisherNameBean> pnBean = ebook.getPublisherName<>();
						List<PublisherNameBean> publisherList = ebook.getPubNameList();
						StringBuffer publisherDisplay = new StringBuffer();						
						int ctr = 0;
						for(PublisherNameBean bean : publisherList) {
							ctr++;
							publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
							if(ctr < publisherList.size()) {
								publisherDisplay.append(", ");
							}							
						}
						doc.add(new Field("PUBLISHER", publisherDisplay.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
					}					
					writer.addDocument(doc);
				}
				catch (Exception e) 
				{
					Logger.getLogger( SearchMDB.class).error("AdvanceSearchIndex Error while adding Document: "+ e);
					
			 	}
			}
		}
	}
	
	private void indexBookLandingPerBook(EbookDetailsBean ebook){
		Document doc;
			
		doc = new Document();
		try 
		{
			// ebook
			doc.add(new Field("DOC_TYPE", "book_details",Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			if(Misc.isNotEmpty(ebook.getBookId())) 		
				doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
				doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
				doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(), Field.Store.YES, Field.Index.NO));
			}
			if(Misc.isNotEmpty(ebook.getSubTitle())) 	
				doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getSeriesCode())) 	
				doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getSeries())) 		
				doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getSeriesNumber())) 		
				doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getCopyrightStatement())) 	
				doc.add(new Field("COPYRIGHT", removeBr(ebook.getCopyrightStatement()),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getPrintDate())) 	
				doc.add(new Field("PRINT_PUBLICATION_DATE",removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getOnlineDate())) 	
				doc.add(new Field("ONLINE_PUBLICATION_DATE", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getDoi())) 		
				doc.add(new Field("BOOK_DOI", ebook.getDoi(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getEisbn())) 		
				doc.add(new Field("ONLINE_ISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getHardback())) 	
				doc.add(new Field("HARDBACK_ISBN", ebook.getHardback(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getPaperback())) 	
				doc.add(new Field("PAPERBACK_ISBN", ebook.getPaperback(),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getVolume())) { 		
				doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
			} 			
			if(Misc.isNotEmpty(ebook.getOtherVolume())) { 		
				doc.add(new Field("OTHER_VOLUME", ebook.getOtherVolume(), Field.Store.YES, Field.Index.ANALYZED));
			} 
			if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
				doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
				doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
				doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
				doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			
			if(Misc.isNotEmpty(ebook.getEdition())) {
				doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED));
			}
			
			//additional
			if(Misc.isNotEmpty(ebook.getEditionNumber())) 	
				doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			if(Misc.isNotEmpty(ebook.getStandardImage()))
				doc.add(new Field("COVER_IMAGE", ebook.getStandardImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getThumbImage())) 	
				doc.add(new Field("COVER_IMAGE_THUMBNAIL", ebook.getThumbImage().replace("../", ""),Field.Store.YES, Field.Index.NO));
			if(Misc.isNotEmpty(ebook.getBlurb())) 	
				doc.add(new Field("BOOK_BLURB", EncodingUtil.convertToUTF8(ebook.getBlurb()),Field.Store.YES, Field.Index.NO));
			
			// publisher 
			if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
				List<PublisherNameBean> publisherList = ebook.getPubNameList();
				StringBuffer publisherDisplay = new StringBuffer();
				for (Iterator<PublisherNameBean> it = publisherList.iterator();it.hasNext();) {
					PublisherNameBean bean = it.next();
					publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
					if (it.hasNext()) {
						publisherDisplay.append(", ");
					}
				}
				doc.add(new Field("PUBLISHER", publisherDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
//			String liveDate = getLiveDate(ebook.getBookId());
			if(Misc.isNotEmpty(liveDate)) {
				doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			writer.addDocument(doc);
		}
		catch (Exception e) 
		{
			Logger.getLogger( SearchMDB.class).error("Book Landing - Error while adding Document per book: "+ e.getMessage());
	 	}
	}
	
	private void indexSubjectLandingPerBook(EbookDetailsBean ebook){
		Document doc;
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHMMss");
		String timestamp = sdf.format(today);
			
		doc = new Document();
		try 
		{
			// ebook
			doc.add(new Field("TIMESTAMP", timestamp, Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			// ebook
			if(Misc.isNotEmpty(ebook.getBookId())) 		
				doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
				doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSubTitle())) 	
				doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getEisbn())) 		
				doc.add(new Field("EISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getBlurb())) 	
				doc.add(new Field("BOOK_BLURB", EncodingUtil.convertToUTF8(ebook.getBlurb()),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSeries())) 		
				doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSeriesNumber())) 		
				doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSeriesAlphasort())) {
				doc.add(new Field("SERIES_NAME_ALPHASORT", ebook.getSeriesAlphasort().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
						
			if(Misc.isNotEmpty(ebook.getSeriesCode())) 		
				doc.add(new Field("SERIES_CODE", ebook.getSeriesCode().toLowerCase(), Field.Store.YES, Field.Index.ANALYZED));
						
			if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
				List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
//				Map<String, RoleAffBean> authorMap = new LinkedHashMap<String, RoleAffBean>();
//				Map<String, RoleAffBean> authorAlphaMap = new LinkedHashMap<String, RoleAffBean>();
//				for(RoleAffBean roleAffBean : authorAffList) {
//					authorMap.put(roleAffBean.getPosition().trim(), roleAffBean);
//					authorAlphaMap.put(roleAffBean.getPosition().trim(), roleAffBean);
//				}

				StringBuffer authorsDisplay = new StringBuffer();
				StringBuilder authorAlphaSort = new StringBuilder();
//				for(int i = 1; i <= authorAffList.size(); i++) {
//					authorsDisplay.append(authorMap.get(i).getRole() + " - " + authorMap.get(i).getFixAuthor());
//					authorAlphaSort.append(authorAlphaMap.get(i).getAuthorAlphasort());
//					if(i < authorAffList.size()) {
//						authorsDisplay.append(", ");
//						authorAlphaSort.append(";");
//					}
//				}
				
				for (Iterator<RoleAffBean> it = authorAffList.iterator();it.hasNext();) {
					RoleAffBean bean = it.next();
					authorsDisplay.append(bean.getRole() + " - " + bean.getFixAuthor());					
					authorAlphaSort.append(Misc.isNotEmpty(bean.getAuthorAlphasort()) 
							? bean.getAuthorAlphasort() : bean.getAuthor());
					if (it.hasNext()) {
						authorsDisplay.append(", ");
						authorAlphaSort.append(" ");
					}
				}
				doc.add(new Field("BOOK_AUTHORS", authorsDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("BOOK_AUTHORS_ALPHASORT", authorAlphaSort.toString().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			if(!Misc.isEmpty(ebook.getSubjectList())) {
				List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
				subjectList = ebook.getSubjectList();
				
				int ctr=1;
				for(SubjectBean subject : subjectList){
					if(Misc.isNotEmpty(subject.getSubject())) 	
						doc.add(new Field("SUBJECT_NAME_"+ctr, subject.getSubject(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(subject.getCode())) 		
						doc.add(new Field("SUBJECT_CODE_"+ctr, subject.getCode().toLowerCase(), Field.Store.YES, Field.Index.ANALYZED));
					
					ctr++;
				}
			}
			
			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
				doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort().toLowerCase(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getVolume())) { 		
				doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("VOLUME_ALPHASORT", ebook.getVolume().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
				doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
				doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
				doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
				doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
						
			//additional
			if(Misc.isNotEmpty(ebook.getEditionNumber())) { 	
				doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			if(Misc.isNotEmpty(ebook.getEdition())) {
				doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED));
			}
			
			if(Misc.isNotEmpty(ebook.getOnlineDate())) { 		
				doc.add(new Field("PUBLICATION_DATE", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("PUBLICATION_DATE_ALPHASORT", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getPrintDate())) {
				doc.add(new Field("PRINT_DATE", removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.ANALYZED));				
				doc.add(new Field("PRINT_DATE_ALPHASORT", removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getHardback()) || Misc.isNotEmpty(ebook.getPaperback())) {
				String pisbn = Misc.isNotEmpty(ebook.getHardback()) ? ebook.getHardback() : ebook.getPaperback();
				doc.add(new Field("PRINT_ISBN", pisbn, Field.Store.YES, Field.Index.NOT_ANALYZED));	
			}
			
			if(Misc.isNotEmpty(ebook.getDoi())) {
				doc.add(new Field("DOI", ebook.getDoi(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
//			String liveDate = getLiveDate(ebook.getBookId());
			if(Misc.isNotEmpty(liveDate)) {
				doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
						
			writer.addDocument(doc);
		}
		catch (Exception e) 
		{
			Logger.getLogger( SearchMDB.class).error("Book Landing - Error while adding Document per book: "+ e.getMessage());
	 	}
		
	}
	
	private void indexSeriesLandingPerBook(EbookDetailsBean ebook){
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHMMss");
		String timestamp = sdf.format(today);
		
		Document doc = new Document();
		try 
		{
			// ebook
			doc.add(new Field("TIMESTAMP", timestamp, Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			if(Misc.isNotEmpty(ebook.getMainTitle())) { 	
				doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(),Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			} else {
				doc.add(new Field("BOOK_TITLE", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("BOOK_TITLE_ALPHASORT", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			if(Misc.isNotEmpty(ebook.getEisbn())) 		
				doc.add(new Field("EISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			else
				doc.add(new Field("EISBN", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			if(Misc.isNotEmpty(ebook.getSeriesCode())) 		
				doc.add(new Field("SERIES_CODE", ebook.getSeriesCode(),Field.Store.YES, Field.Index.ANALYZED));
			else
				doc.add(new Field("SERIES_CODE", "null", Field.Store.YES, Field.Index.ANALYZED));
			
			if(Misc.isNotEmpty(ebook.getSeries())) 		
				doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			else
				doc.add(new Field("SERIES_NAME", "null",Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			if(Misc.isNotEmpty(ebook.getVolume())) { 		
				doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("VOLUME_ALPHASORT", ebook.getVolume(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			} else {
				doc.add(new Field("VOLUME", "null", Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("VOLUME_ALPHASORT", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
				doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
				doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
				doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
				doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
						
			if(Misc.isNotEmpty(ebook.getOnlineDate())) { 		
				doc.add(new Field("PUBLICATION_DATE", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("PUBLICATION_DATE_ALPHASORT", removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
			} else {
				doc.add(new Field("PUBLICATION_DATE", "null", Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("PUBLICATION_DATE_ALPHASORT", "null",Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			if(Misc.isNotEmpty(ebook.getBlurb())) 	
				doc.add(new Field("BOOK_BLURB", ebook.getBlurb(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			else
				doc.add(new Field("BOOK_BLURB", "null", Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
				List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
				StringBuffer authorsDisplay = new StringBuffer();
				for (Iterator<RoleAffBean> it = authorAffList.iterator();it.hasNext();) {
					RoleAffBean bean = it.next();
					authorsDisplay.append(RoleAffBean.fixAuthorName(bean.getAuthor()));
					if (it.hasNext()) {
						authorsDisplay.append(", ");
					}
				}
				doc.add(new Field("BOOK_AUTHORS", authorsDisplay.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("BOOK_AUTHORS_ALPHASORT", authorsDisplay.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			writer.addDocument(doc);			
		}
		catch (Exception e) 
		{
			Logger.getLogger( SearchMDB.class).error("Book Landing - Error while adding Document per book: "+ e.getMessage());
	 	}
	}
	
	private void indexChapterBookLandingPerAuthor(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters){
		for(EbookChapterDetailsBean chBean: chapters){
			indexBookLandingPerAuthor(ebook, true, chBean.getContentId());
		}
	}
	
	private void indexBookLandingPerAuthor(EbookDetailsBean ebook, boolean addContentId, String contentId){
		
		Document doc = null;		
		try 
		{
			if(!Misc.isEmpty(ebook.getAuthorAffList())) {
				List<RoleAffBean> authorAffList = new ArrayList<RoleAffBean>();
				authorAffList = ebook.getAuthorAffList();
				for(RoleAffBean roleAff : authorAffList){
					doc = new Document();
					doc.add(new Field("DOC_TYPE", "book_author",Field.Store.YES, Field.Index.NOT_ANALYZED));
					
					if(Misc.isNotEmpty(ebook.getBookId()))			
						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(roleAff.getAuthor())) 		
						doc.add(new Field("NAME", roleAff.getAuthor(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(roleAff.getFixAuthor())) {
						doc.add(new Field("FIX_NAME", roleAff.getFixAuthor(),Field.Store.YES, Field.Index.NO));
					}
					if(Misc.isNotEmpty(roleAff.getAffiliation())) 	
						doc.add(new Field("AFFILIATION", roleAff.getAffiliation(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(roleAff.getPosition())) 		
						doc.add(new Field("POSITION", roleAff.getPosition(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(roleAff.getRole())) 			
						doc.add(new Field("ROLE", roleAff.getRole(),Field.Store.YES, Field.Index.NO));
										
					//add contentId
					addContentId(doc, addContentId, contentId);
					
					writer.addDocument(doc);
				}
			}
			
			
		}
		catch (Exception e) 
		{
			Logger.getLogger( SearchMDB.class).error("Book Landing - Error while adding Document per author: "+ e.getMessage());
	 	}
	}
	
	private void indexBookPerContent(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList,
			HashMap<String,String> content, HashMap<String,String> tlMap){		
		if(chaptersList != null && chaptersList.size() > 0)	
		{
			Document doc;
			for(EbookChapterDetailsBean chapter : chaptersList) 
			{
				doc = new Document();
				try 
				{
					doc.add(new Field("DOC_TYPE", "content_item",Field.Store.YES, Field.Index.NOT_ANALYZED));
					
					// ebook
					if(Misc.isNotEmpty(ebook.getBookId()))
						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(ebook.getEisbn())) 		
						doc.add(new Field("ONLINE_ISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NO));
					
					// chapter
					if(Misc.isNotEmpty(chapter.getContentId())) 		
						doc.add(new Field("CONTENT_ID", chapter.getContentId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(chapter.getDoi())) {
						doc.add(new Field("CHAPTER_DOI", chapter.getDoi(), Field.Store.YES, Field.Index.NO));
					}
					if(Misc.isNotEmpty(chapter.getHeadingTitle()))		
						doc.add(new Field("CONTENT_TITLE", chapter.getHeadingTitle(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
						doc.add(new Field("CONTENT_LABEL", chapter.getHeadingLabel(), Field.Store.YES, Field.Index.NO));
					}
					if(Misc.isNotEmpty(chapter.getContentType())) {
						doc.add(new Field("CONTENT_TYPE", chapter.getContentType(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					}
					if(Misc.isNotEmpty(chapter.getHeadingSubtitle()))	
						doc.add(new Field("CONTENT_SUB_TITLE", chapter.getHeadingSubtitle(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getPdfFilename()))		
						doc.add(new Field("PDF", chapter.getPdfFilename(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getPageStart()))			
						doc.add(new Field("PAGE", chapter.getPageStart(),Field.Store.YES, Field.Index.NO));
					
					indexContentContributor(chapter.getContentId(), doc);
					
//					if(chapter.getContributors().size() > 0) {
//						doc.add(new Field("CONTENT_CONTRIBUTORS", 
//								StringUtil.getContributorDisplay(chapter.getContributors()), Field.Store.YES, Field.Index.NO));
//					}
					
//					if(Misc.isNotEmpty(chapter.getContributorNameModified()))				
//						doc.add(new Field("CONTRIBUTOR", chapter.getContributorNameModified(),Field.Store.YES, Field.Index.NO));
					
					//added for view abstract text and image
					if(Misc.isNotEmpty(chapter.getAbstractContent()))				
						doc.add(new Field("CONTENT_EXTRACT", chapter.getAbstractContent(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(chapter.getAbstractImage()))				
						doc.add(new Field("CONTENT_IMAGE_EXTRACT", chapter.getAbstractImage(),Field.Store.YES, Field.Index.NO));
					
					
					//getIfContains Subchapter
					doc.add(new Field("CONTENT_SUB_CHAPTERS", hasSubchapters(content, chapter),Field.Store.YES, Field.Index.NO));
					
					//if it is top level
					doc.add(new Field("BOOK_LEVEL", isTopLevel(tlMap, chapter), Field.Store.YES, Field.Index.ANALYZED));
					
					// TOC_ITEM
					if(Misc.isNotEmpty(chapter.getToc())) {
						doc.add(new Field("TOC_ITEM", 
								chapter.getToc().replaceAll("<em>", "")
								.replaceAll("</em>", "")
								.replaceAll("<br />", "=DEL="), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS)
						);
					}
					
					writer.addDocument(doc);
				}
				catch (Exception e) 
				{
					Logger.getLogger( SearchMDB.class).error("Book - Error while adding Document per content: "+ e.getMessage());
					e.printStackTrace();
			 	}
			}
		}
	}
	
	/**
	 * checks if contains subchapter
	 * @param content
	 * @param chapter
	 * @return
	 */
	private String hasSubchapters(HashMap<String,String> content, EbookChapterDetailsBean chapter) {		
		return content.get(chapter.getContentId());
	}
	
	private String isTopLevel(HashMap<String,String> tlMap, EbookChapterDetailsBean chapter) { 
		return tlMap.get(chapter.getContentId());
	}
	
	/**
	 * per subject editing for chapter /// must add content id
	 * @param ebook
	 * @param chapters
	 */
	private void indexChapterBookPerSubject(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters ){
		for(EbookChapterDetailsBean chBean : chapters){
			indexBookPerSubject(ebook, true, chBean.getContentId());			
		}		
	}
	
	/**
	 * Index book Per Subject
	 * @param ebook
	 */
	private void indexBookPerSubject(EbookDetailsBean ebook, boolean addContentId, String contentId){
	
		
		try 
		{
			if(!Misc.isEmpty(ebook.getSubjectList())) {
				List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
				subjectList = ebook.getSubjectList();
				
				for(SubjectBean subject : subjectList){
					Document doc = new Document();
					doc.add(new Field("DOC_TYPE", "subject",Field.Store.YES, Field.Index.NOT_ANALYZED));
					if(Misc.isNotEmpty(ebook.getBookId()))		
						doc.add(new Field("BOOK_ID", ebook.getBookId(),Field.Store.YES, Field.Index.ANALYZED));
					if(Misc.isNotEmpty(subject.getSubject())) 	
						doc.add(new Field("SUBJECT", subject.getSubject(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(subject.getCode())) 		
						doc.add(new Field("SUBJECT_CODE", subject.getCode(),Field.Store.YES, Field.Index.NO));
					
					//add contentId
					addContentId(doc, addContentId, contentId);
					
					writer.addDocument(doc);
				}				
			}
		}
		catch (Exception e) 
		{
			Logger.getLogger( SearchMDB.class).error("Book - Error while adding Document per subject: "+ e.getMessage());
	 	}
	}
		
	/**
	 * adds content id to doc
	 * @param doc
	 * @param addContentId
	 * @param contentId
	 */
	private void addContentId(Document doc, boolean addContentId, String contentId) {
		if(addContentId){
			if(Misc.isNotEmpty(contentId)){
				doc.add(new Field("CONTENT_ID", contentId, Field.Store.YES, Field.Index.ANALYZED));
			}
		}
	}
	
	public void deleteLuceneDocuments(final String bookId, String indexer){
		try
		{
			System.out.println("delete Documents bookId:" + bookId + " indexer:" + indexer); 
			//FSDirectory dirIndex = FSDirectory.getDirectory(System.getProperty(IndexerProperties.SEARCH_INDEX));
			FSDirectory dirIndex = FSDirectory.getDirectory(System.getProperty(indexer));
			if(IndexWriter.isLocked(dirIndex))
			{
				System.out.println("LOCKED! [" + System.getProperty(indexer) + "]");
			}
			
			IndexWriter writer = new IndexWriter(dirIndex, new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
			System.out.println("Deleting BOOK_ID in "+indexer+": " + bookId);
			writer.deleteDocuments(new Term("BOOK_ID", bookId));
			writer.commit();
			writer.close();
		}
		catch(IOException e)
		{
			System.out.println("[IOException] deleteLuceneDocuments() " + e.getMessage());
			//send back to queue
		}
	}
	
	private EBook getEBook(String bookId) {	
		em = emf.createEntityManager();
		try { 						
			EBook ebook = em.find(EBook.class, bookId);
			if(ebook != null) {				
				return ebook;
			}
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        	em.close();
        }
        
        return null;
	}
	
	private String getFormatedDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	private Date getLiveDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("ddMMMyyyy");
		try {
			Date specifiedDate = dateFormat.parse("16Nov2009"); // 16 NOV 2009
			if(date.before(specifiedDate)) { 
				return specifiedDate;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	private String getLiveDate(String bookId) {
		EBook eBookJpa = getEBook(bookId);
		if(eBookJpa != null) {
			if(eBookJpa.getModifiedDate() != null) {
				return getFormatedDate(getLiveDate(eBookJpa.getModifiedDate()), "MMMMM yyyy");
			}
		}
		
		return "";
	}
	
	private String getLiveDateNum(String bookId) {
		EBook eBookJpa = getEBook(bookId);
		if(eBookJpa != null) {
			if(eBookJpa.getModifiedDate() != null) {
				return getFormatedDate(getLiveDate(eBookJpa.getModifiedDate()), "yyyyMMdd");
			}
		}
		
		return "";
	}
	
	private static String removeBr(String input){
		return input.replaceAll("<br />", " ");
	}
	
	private static String removeHTMLTags(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("<i>", "").
		replaceAll("</i>", "").
		replaceAll("<b>", "").
		replaceAll("</b>", "").
		replaceAll("<blockquote>", "").
		replaceAll("</blockquote>", "").
		replaceAll("<u>", "").
		replaceAll("</u>", "").
		replaceAll("<span style='font-variant:small-caps'>", "").
		replaceAll("</span>", "").
		replaceAll("<sup>", "").
		replaceAll("</sup>", "").
		replaceAll("<sub>", "").
		replaceAll("</sub>", "");
	}
	
	private static String stripSpecialCharacters(String source) {
		String result = source;
		for(String s : STRIP_CHARS) {
			result = result.replaceAll(s, " ");
		}
		
		return result;
	}
	
	private static String removeComma(String input) {		
		return input.replaceAll(",", " ");
	}
	
	private final static String stripHTMLTags(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("(?i)(<[\\s]*[^>]*(" +
				"a|abbr|acronym|address|applet|area|b|base|basefont|bdo|big|blockquote|body|" +
				"br|button|caption|center|cite|code|col|colgroup|dd|del|dfn|" +
				"dir|div|dl|dt|em|fieldset|font|form|frame|frameset|" +
				"h1|h2|h3|h4|h5|h6|head|html|i|iframe|img|input|ins|isindex|kbd|label|" +
				"legend|li|link|map|menu|meta|noframes|noscript|object|ol|optgroup|option|" +
				"p|param|pre|q|s|samp|script|select|small|span|strike|strong|style|sub|sup|" +
				"table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var|xmp)[^>]*>)", " ");
	}
	
	private final static String stripUnicodes(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("&#[^;]+;", " ");
	}
	
	
	@PostConstruct
    void init() {
		Logger.getLogger( SearchMDB.class ).info( " Initialize Search EJB " );
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger( SearchMDB.class ).info( " Destroy Search EJB " );
    }
}