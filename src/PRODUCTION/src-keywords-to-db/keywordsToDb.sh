#!/bin/sh
jar_path="/app/service/keywords-to-db/"

NOW=`date '+keywordsToDB.log.%Y-%d-%m'`
LOGFILE="/app/service/keywords-to-db/log/$NOW.txt"

/app/java6/bin/java -Xms128m -Xmx256m -jar -Dproperty.file=`hostname` "$jar_path"keywordsToDb.jar  >> $LOGFILE &
echo "Finished Adding Keywords to DB..."
