package org.cambridge.ebooks.production.keywords;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.List;

public class KeywordsDBInfo {
    
//    private static final boolean DEBUG = KeywordsApplicationProperties.DEBUG_MODE;
    private static final String CONTROL_FOLDER = KeywordsApplicationProperties.CONTROL_FOLDER;
    
    public static void insertDB(List<String> ListBookIsbn, List<String> ListChapterIds, List<String> ListCleanKeywords, Connection conn) throws SQLException, IOException{
        if(ListBookIsbn.size() > 0){
            deleteIfExists(conn, ListBookIsbn.get(0));
        }
        for(int x=0; x< ListBookIsbn.size(); x++){
            List<String> listString = KeywordsToDBUtil.breakIntoChunks(ListCleanKeywords.get(x));
            
            System.out.println("Inserting new values for "+ListChapterIds.get(x)+"...");
            for(String keywordChunk: listString){
                insertToDB(conn, ListBookIsbn.get(x), ListChapterIds.get(x), keywordChunk);
            }
        }
    }
    
    public static boolean deleteIfExists(Connection conn, String id) throws SQLException {
        PreparedStatement st = null;
        boolean doesExist = false;
        
        try {
            System.out.println("DELETE FROM EBOOK_KEYWORD_FINAL WHERE ISBN = '" + id + "'");
            st = conn.prepareStatement("DELETE FROM EBOOK_KEYWORD_FINAL WHERE ISBN = ?");
            st.setString(1, id);
            st.executeUpdate();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            st.close();
        }

        return doesExist;
    }
    
    public static boolean insertToDB(Connection conn, String ListBookIsbn, String ListChapterIds, String ListCleanKeywords) throws SQLException, IOException{
        boolean success = true;
        
        PreparedStatement st = null;
        String sql = null;
        try{
            sql = "INSERT INTO EBOOK_KEYWORD_FINAL VALUES (KEYWORD_SEQ.nextval, '" + ListBookIsbn + "', '" + ListChapterIds + "', '" +ListCleanKeywords+ "')";
            System.out.println(sql);
            st = conn.prepareStatement("INSERT INTO EBOOK_KEYWORD_FINAL VALUES (KEYWORD_SEQ.nextval, ?, ?, ?)");
            st.setString(1, ListBookIsbn);
            st.setString(2, ListChapterIds);
            st.setString(3, ListCleanKeywords);
            st.executeUpdate();
        
        
        } catch (SQLSyntaxErrorException e){
            System.out.println("ERROR ENCOUNTERED ON sql: " +sql);
            File file = new File(CONTROL_FOLDER+"isbnlist_failedKeywordToDB-"+ KeywordsToDBUtil.dateStampNow()+".txt");
            Writer out = new BufferedWriter(new FileWriter(file, true));
            
            out.append("[SQLSyntaxErrorException] sql: " +sql +"\n");
            out.close();
        } catch (SQLException e){
            System.out.println("ERROR ENCOUNTERED ON sql: " +sql);
            File file = new File(CONTROL_FOLDER+"isbnlist_failedKeywordToDB-"+ KeywordsToDBUtil.dateStampNow()+".txt");
            Writer out = new BufferedWriter(new FileWriter(file, true));
            
            out.append("[SQLException] sql: " +sql +"\n");
            out.close();
        } finally {
            st.close();
        }
        return success;
    }
}
