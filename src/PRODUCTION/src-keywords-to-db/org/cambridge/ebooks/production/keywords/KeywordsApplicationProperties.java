package org.cambridge.ebooks.production.keywords;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;


public class KeywordsApplicationProperties {
	
	private Configuration config;
	
	private static final String propertyFile = System.getProperty("property.file"); 
	private static final KeywordsApplicationProperties APP_PROPERTIES = new KeywordsApplicationProperties();
	
	private KeywordsApplicationProperties(){
		try 
		{
			config = new PropertiesConfiguration(propertyFile + ".properties");
		} 
		catch (ConfigurationException e) 
		{
			System.err.println("[ConfigurationException] ApplicationProperties() message: " + e.getMessage());
		}
	}

	public static final String SOLR_HOME_URL = APP_PROPERTIES.config.getString("solr.home.url");
	
	public static final int SOCKET_TIMEOUT = APP_PROPERTIES.config.getInt("socket.timeout");
	public static final int CONNECTION_TIMEOUT = APP_PROPERTIES.config.getInt("connection.timeout");
	public static final int DEFAULT_MAX_CONNECTION_PER_HOST = APP_PROPERTIES.config.getInt("default.max.connection.per.host");
	public static final int MAX_TOTAL_CONNECTIONS = APP_PROPERTIES.config.getInt("max.total.connections");
	public static final boolean FOLLOW_REDIRECTS = APP_PROPERTIES.config.getBoolean("follow.redirects");
	public static final boolean ALLOW_COMPRESSION = APP_PROPERTIES.config.getBoolean("allow.compression");
	public static final int MAX_RETRIES = APP_PROPERTIES.config.getInt("max.retries");
	
	public static final String CORE_BOOK = APP_PROPERTIES.config.getString("core.book");
	public static final String CORE_CONTENT = APP_PROPERTIES.config.getString("core.content");
	
	public static final String DIR_EBOOK_CONTENT = APP_PROPERTIES.config.getString("dir.ebook.content");
	public static final String DIR_EBOOK_FILE_DUMP = APP_PROPERTIES.config.getString("dir.ebook.file.dump");
	public static final String DIR_HEADER_FILES = APP_PROPERTIES.config.getString("dir.header.files");
	public static final String DIR_APP_EBOOKS = APP_PROPERTIES.config.getString("dir.app.ebooks");
		
	public static final int MAX_DOC_COMMIT = APP_PROPERTIES.config.getInt("max.doc.commit");	
	
	public static final String JDBC_DRIVER = APP_PROPERTIES.config.getString("jdbc.driver");
	public static final String JDBC_URL = APP_PROPERTIES.config.getString("jdbc.url");
	public static final String JDBC_USER = APP_PROPERTIES.config.getString("jdbc.user");
	public static final String JDBC_PASS = APP_PROPERTIES.config.getString("jdbc.pass");
	
	public static final String CONTROL_FOLDER = APP_PROPERTIES.config.getString("control.folder");
	public static final String CONTROL_FILENAME = APP_PROPERTIES.config.getString("control.filename");
	public static final String CONTROL_ARCHIVE = APP_PROPERTIES.config.getString("control.archive");
	
	public static final boolean DEBUG_MODE = APP_PROPERTIES.config.getBoolean("debug");

}
