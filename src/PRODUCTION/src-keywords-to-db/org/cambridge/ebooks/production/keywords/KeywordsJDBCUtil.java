package org.cambridge.ebooks.production.keywords;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class KeywordsJDBCUtil {
    public static Connection getConnection() {
        Connection connection = null; 
        try { 
            // Load the JDBC driver 
            String driverName = String.valueOf(KeywordsApplicationProperties.JDBC_DRIVER);//"oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);
            
            // Create a connection to the database            
            String url=String.valueOf(KeywordsApplicationProperties.JDBC_URL);
            String username = String.valueOf(KeywordsApplicationProperties.JDBC_USER);//"username"; 
            String password = String.valueOf(KeywordsApplicationProperties.JDBC_PASS);//"password"; 
            if(url == null){
                
            }
            System.out.println("connecting to url=" + url);
            connection = DriverManager.getConnection(url, username, password); 
        } catch (ClassNotFoundException e) { 
            // Could not find the database driver
            e.printStackTrace();
        } catch (SQLException e) { 
            // Could not connect to the database
            e.printStackTrace();
        }        
        return connection;
    }
        
    public static void closeConnection(Connection con) {
        try {
            con.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }    
}
