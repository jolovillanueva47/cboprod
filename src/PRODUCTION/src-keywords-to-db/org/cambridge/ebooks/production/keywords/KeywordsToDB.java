package org.cambridge.ebooks.production.keywords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.production.indexer.solr.server.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.production.indexer.solr.server.CupSolrServer;
import org.cambridge.ebooks.production.properties.ApplicationProperties;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;

public class KeywordsToDB {
    private static final String CONTROL_FOLDER = KeywordsApplicationProperties.CONTROL_FOLDER;
    private static final String CONTROL_FILENAME = KeywordsApplicationProperties.CONTROL_FILENAME;
    private static final String CONTROL_ARCHIVE = KeywordsApplicationProperties.CONTROL_ARCHIVE;
    private static final String LOCK_FILE = KeywordsApplicationProperties.DIR_APP_EBOOKS+"lockfile";
    private static final String CONTENT_DIR = KeywordsApplicationProperties.DIR_EBOOK_CONTENT;

    private static SolrServer chapterCore;
    private static CupSolrServer server = new CupCommonsHttpSolrServer();
    
    private static List<String> ListCleanKeywords;
    private static List<String> ListIsbn;
    private static List<String> ListChapterIds;
    
    public static void main(String[]args){
        initSolrServer();
        
        long startTime = 0;
        long endTime = 0;
        try {
            startTime = System.currentTimeMillis();
            List<String> isbnList = getIsbnList(args.length > 0 ? args[0] : "");
            processIsbnList(isbnList); //bulk of the code
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            endTime = System.currentTimeMillis();
        }
        computeTotalTime(startTime, endTime);
    }
    
    public static List<String> getIsbnList(String args){
        List<String> isbnList = new ArrayList<String>();
        String isbn = "";
        
        if(args.equalsIgnoreCase("ALL")){
            System.out.println("ALL option detected, retrieving ISBN numbers from content folder...");
            File[] rawr = getHeaderFileDirectoryList(new File(CONTENT_DIR));
            for(File f: rawr){
                isbn = f.toString().replace("\\", "/").replace(CONTENT_DIR, "").replace("/", "");
                if ((isbn.startsWith("978") || isbn.startsWith("555")) && isbn.length() == 13){
                    isbnList.add(isbn);
                    System.out.println("read: "+f.toString().replace("\\", "/").replace(CONTENT_DIR, "").replace("/", ""));
                }
            }
            System.out.println("Done. Proceeding with Keyword processing...");
        } else {
            //if lockfile exists, wait for 5 minutes
            System.out.println("No ALL option detected, proceeding with file processing");
            while(lockFileExists()){
                try {
                    System.out.println(KeywordsToDBUtil.dateTimeNow()+": "+ LOCK_FILE+" detected. Waiting for 1 minute.");
                    Thread.sleep(100000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(LOCK_FILE+" not detected. Proceeding with Keyword processing...");
            String inputFile = CONTROL_FOLDER + CONTROL_FILENAME;
            isbnList = readContent(inputFile);
            KeywordsToDBUtil.moveFile(CONTROL_FOLDER + CONTROL_FILENAME , CONTROL_ARCHIVE + CONTROL_FILENAME + "_" +KeywordsToDBUtil.dateTimeNow());
        }
        
        return isbnList;
    }
    
    private static File[] getHeaderFileDirectoryList(File indexDir){
        File[] headerFiles = indexDir.listFiles(
                new FileFilter() {
                        public boolean accept(File f) {        
                            return f.isDirectory();
                        }
                });
        
        return headerFiles;
        
    }
    
    public static void computeTotalTime(long start, long end){
        final long minute = 60;
        final long milliseconds = 1000;
        System.out.println("Time to complete: " + (((end - start)/milliseconds)/minute) +" minutes");
    }
    
    public static void processIsbnList(List<String> isbnList) throws SQLException, IOException{
        Connection conn = KeywordsJDBCUtil.getConnection();
        conn.setAutoCommit(false);
        try {
            for(String str: isbnList){
                System.out.println(getKeywordsFromSolr(str));
                KeywordsDBInfo.insertDB(ListIsbn, ListChapterIds, ListCleanKeywords, conn);
                conn.commit();
                System.out.println("Commit Successful for "+str);
            }
        } finally {
            conn.close();
            System.out.println("DB Connection Closed");
        }
    }
    
    public static boolean lockFileExists(){
        File f = new File(LOCK_FILE);
        return f.exists();
    }
    
    public static List<String> readContent(String sourceFile){
        List<String> list = new ArrayList<String>();
        
        try {
            BufferedReader in = new BufferedReader(new FileReader(sourceFile));
            String str;
            
            while ((str = in.readLine()) != null) {
                System.out.println("read: "+str);
                str = str.substring(str.indexOf("_")-13, str.indexOf("_"));
                list.add(str);
            }
            
            in.close();
        } catch (FileNotFoundException e){
            System.out.println("File "+sourceFile+" does not exist.");
            e.getStackTrace();
        } catch (Exception e) { 
            e.printStackTrace();
        }
        return list;
    }
    
    public static void initSolrServer(){
        Map<String, String> serverSettings = new HashMap<String, String>();
        serverSettings.put(CupSolrServer.KEY_SOCKET_TIMEOUT, String.valueOf(ApplicationProperties.SOCKET_TIMEOUT));
        serverSettings.put(CupSolrServer.KEY_CONN_TIMEOUT, String.valueOf(ApplicationProperties.CONNECTION_TIMEOUT));
        serverSettings.put(CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST, String.valueOf(ApplicationProperties.DEFAULT_MAX_CONNECTION_PER_HOST));
        serverSettings.put(CupSolrServer.KEY_MAX_TOTAL_CONN, String.valueOf(ApplicationProperties.MAX_TOTAL_CONNECTIONS));
        serverSettings.put(CupSolrServer.KEY_FOLLOW_REDIRECTS, String.valueOf(ApplicationProperties.FOLLOW_REDIRECTS));
        serverSettings.put(CupSolrServer.KEY_ALLOW_COMPRESSION, String.valueOf(ApplicationProperties.ALLOW_COMPRESSION));
        serverSettings.put(CupSolrServer.KEY_MAX_RETRIES, String.valueOf(ApplicationProperties.MAX_RETRIES));
        
        chapterCore = server.getMultiCore(ApplicationProperties.SOLR_HOME_URL, ApplicationProperties.CORE_CONTENT, serverSettings);
    }
    
    public static String getKeywordsFromSolr(String id) {
        System.out.println("[getKeywordsFromSolr]book_id:" + id);
        
        String isbn = "";
        String chapterId = "";
        List<String> keywords = null;
        ListCleanKeywords = new ArrayList<String>();
        ListIsbn = new ArrayList<String>();
        ListChapterIds = new ArrayList<String>();
        SolrQuery q = new SolrQuery();
        q.setRows(Integer.MAX_VALUE);
        q.setQuery("book_id:" +"CBO"+ id);
        
        System.out.println(q.toString());
        
        QueryResponse response = null;
        try {
            
            response = chapterCore.query(q);
            List<BookSolrDocument> docs = response.getBeans(BookSolrDocument.class);
            for(BookSolrDocument bsd:docs) {
                isbn = bsd.getIsbn();
                chapterId = bsd.getId();
                keywords = KeywordsToDBUtil.cleanKeywords(bsd.getKeywordTextList());
                
                if(! (keywords == null || keywords.isEmpty())){
                    ListIsbn.add(isbn);
                    ListChapterIds.add(chapterId);
                    ListCleanKeywords.add(KeywordsToDBUtil.oneLongString(keywords));
                }
            }
        } catch (SolrServerException e) {
            System.out.println(ExceptionPrinter.getStackTraceAsString(e));
        }
        
        return isbn;
    }
    
    
}
