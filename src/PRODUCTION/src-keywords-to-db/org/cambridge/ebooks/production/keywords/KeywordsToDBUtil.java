package org.cambridge.ebooks.production.keywords;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

public class KeywordsToDBUtil {
    public static String oneLongString(List<String> list){
        String oneString = "";
        
        if(! list.isEmpty()){
            for(String str:list){
                if(StringUtils.isEmpty(oneString)){
                    oneString+=" ";
                }
                oneString = oneString + str;
            }
            oneString.trim();
        }
        return oneString;
    }
    
    public static List<String> cleanKeywords(List<String> list){
        if (list == null || list.isEmpty()){
            return list;
        }
        List<String> cleanList = new ArrayList<String>();
        
        for(String str:list){
            cleanList.add(removeUnicode(stripTags(str)));
        }
        
        return cleanList;
    }
    
    public static String removeUnicode(String str){
        String unencoded = StringEscapeUtils.unescapeXml(str);
        String cleanString = "";
        for(int x = 0; x < unencoded.length(); x++){
            if(Character.isDigit(unencoded.charAt(x)) || 
                    Character.isLetter(unencoded.charAt(x)) || 
                    Character.isWhitespace(unencoded.charAt(x)) ||
                    "-".equals(unencoded.charAt(x)))
                cleanString = cleanString + unencoded.charAt(x);
            else
                cleanString = cleanString + " ";
        }
        return cleanString;
    }
    
    /**
     * Strip html tags
     * @param text String source text
     * @return String
     */
    public static String stripTags(String text) {
        if(text == null || text.equals("")){
            return text;
        }
        
        return text.replaceAll("<i>", "").
        replaceAll("</i>", "").
        replaceAll("<b>", "").
        replaceAll("</b>", "").
        replaceAll("<blockquote>", "").
        replaceAll("</blockquote>", "").
        replaceAll("<u>", "").
        replaceAll("</u>", "").
        replaceAll("<div>", "").
        replaceAll("</div>", "").
        replaceAll("<span style='font-variant:small-caps'>", "").
        replaceAll("</span>", "").
        replaceAll("<sup>", "").
        replaceAll("</sup>", "").
        replaceAll("<br>", "").
        replaceAll("</br>", "").
        replaceAll("<br/>", "").
        replaceAll("<sub>", "").
        replaceAll("</sub>", "");
    }
    
    public static boolean moveFile(String sourceFile, String destinationFile){
        boolean success = true;
        try{
          File f1 = new File(sourceFile);
          File f2 = new File(destinationFile);
          InputStream in = new FileInputStream(f1);
          
          //For Append the file.
//          OutputStream out = new FileOutputStream(f2,true);

          //For Overwrite the file.
          OutputStream out = new FileOutputStream(f2);

          byte[] buf = new byte[1024];
          int len;
          while ((len = in.read(buf)) > 0){
            out.write(buf, 0, len);
          }
          in.close();
          out.close();
          System.out.println("Target File: "+destinationFile+" created.");
          if(f1.delete()){
              System.out.println("Source file: "+destinationFile+" deleted.");
          }
        }
        catch(FileNotFoundException ex){
          System.out.println(ex.getMessage());
          success = false;
        }
        catch(IOException e){
          System.out.println(e.getMessage());
          success = false;
        }
        
        return success;
    }
    
    public static String dateTimeNow(){
        Calendar dateNow = Calendar.getInstance();
        String str = String.valueOf(dateNow.get(Calendar.YEAR)) +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)), 2, "0") +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0") +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + 
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0");
        
        return str;
    }
    
    public static String dateStampNow(){
        Calendar dateNow = Calendar.getInstance();
        String str = String.valueOf(dateNow.get(Calendar.YEAR)) +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)), 2, "0") +
            StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0");
        return str;
    }
    
    public static List<String> breakIntoChunks(String vls){
        List<String> listKeywords = new ArrayList<String>();
        int inc = 1000;
        for (int x=0; x < vls.length(); ){
            
            //reached end, set limit to last character
            if(x+inc+1 > vls.length()){
                listKeywords.add(vls.substring(x, vls.length()));
                x=vls.length()+1;
            }
            
            //not yet end, find last instance of space and cut there
            else {
                //slice found a space, proceed with chunk
                if(Character.isWhitespace(vls.charAt(x+inc))){
                    listKeywords.add(vls.substring(x, x+inc));
                    x+=inc;
                //subtract from inc until whitespace is found
                } else {
                    int dec;
                    for (dec = 0; !Character.isWhitespace(vls.charAt(x+inc-dec)) ; dec++){}
                    listKeywords.add(vls.substring(x, x+inc-dec));
                    x = x+inc-dec;
                }
            }
        }
        return listKeywords;
    }
}
