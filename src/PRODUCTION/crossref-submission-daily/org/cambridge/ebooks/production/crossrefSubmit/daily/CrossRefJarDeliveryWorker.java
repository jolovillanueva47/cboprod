package org.cambridge.ebooks.production.crossrefSubmit.daily;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentDAO;
import org.cambridge.ebooks.production.crossrefSubmit.util.CrossRefDeliveryWorker;
import org.cambridge.ebooks.production.crossrefSubmit.util.EBookContentWorker;
import org.cambridge.ebooks.production.crossrefSubmit.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.crossrefSubmit.util.XMLParser;
import org.cambridge.ebooks.production.jpa.delivery.DoiBatch;
import org.cambridge.util.Misc;

public class CrossRefJarDeliveryWorker {
    
    public enum PersistentUnits {
        EBOOKS("crossRefService");
        
        private PersistentUnits(String name ) { 
            this.name = name;
        }
        
        private String name;
        
        public String toString() { 
            return name;
        }
    }
    
    private static Map<String, String> props = new HashMap<String, String>();
    
    static
    {    
        props.put("javax.persistence.jdbc.url", System.getProperty("jdbc.url"));
    }
    
    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString(), props);
    
    private EBookContentDAO dao;

    public static final String XML_ONLY = "xmlOnly";
    public static final String XML_AND_SEND = "xmlSend";
    
    private boolean isSuccess = false;

    public String deliverEBook(
            String bookId,
            String eisbn,
            boolean deliverToCrossRef,
            String deliveryOption) throws SQLException {
        
        String crossRefXmlFileName = null;
        String output = "";
        
        dao = new EBookContentDAO(); 
        dao.setEmf(emf);
        
        EBookContentWorker eworker = new EBookContentWorker();
        eworker.setDao(dao);
        List<EBookContentBean> ebookContentList = eworker.ebookContents(bookId, true);
        
        String username = "CambridgeBooksOnline";
        Date date = new Date();
        
        EntityManager em = emf.createEntityManager();
        DoiBatch db = new DoiBatch(new Date(), username);
        em.getTransaction().begin();
        em.persist(db);
        em.getTransaction().commit();

        boolean withError = false;
        String errorMessage = "";
        if (deliverToCrossRef) {
            System.out.println("DELIVER TO CROSSREF STEP");
            crossRefXmlFileName = CrossRefDeliveryWorker.processEBookXml(bookId, eisbn, ebookContentList, username, db.getDoiBatchId(), date);
            
            if(!crossRefXmlFileName.contains("fail")){
                XMLParser parser = new XMLParser();
                errorMessage = parser.validatateCrossRef(eisbn, crossRefXmlFileName);
                withError = Misc.isNotEmpty(errorMessage);
    
                if (deliveryOption.equals(CrossRefJarDeliveryWorker.XML_AND_SEND)) {
                    
    //                TODO: COMMENT OUT TEMPORARILY FOR TESTING
                    if (!withError) {
                        int responseCode = CrossRefDeliveryWorker.sendDeposit(eisbn, crossRefXmlFileName);
                        withError = !(responseCode==200);
                    }
                }
            } else {
                //enter code here to create file
                errorMessage = "ALERT: " + bookId + " encountered an error while processing.";
            }
            
        }

        if (withError && errorMessage.length()>0) {
            output = eisbn + " - FAIL: Error writing XML: " + errorMessage;
        } else if (errorMessage.contains("ALERT:")){
            output = eisbn + " - FAIL: " + crossRefXmlFileName.replace("fail ", "");
        } else if (deliveryOption.equals(CrossRefJarDeliveryWorker.XML_AND_SEND)) {
            output = eisbn + " - SUCCESS: Sending Successful for bookId: "+bookId;
            renameFileToSuccess(eisbn, crossRefXmlFileName);
            this.isSuccess = true;
        }
        System.out.println(output);
        return output;
    }
    
    private void renameFileToSuccess(String isbn, String crossRefXmlFileName){
        File oldname = new File(IsbnContentDirUtil.getPath(isbn) + crossRefXmlFileName);
        File newname = new File(oldname.toString().replace("af", "as"));
        
        if(oldname.renameTo(newname)){
            System.out.println("crossref file renamed to: "+newname.getName());
        } else {
            System.out.println("***failed to rename crossref file!");
        }
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    
    
}
