package org.cambridge.ebooks.production.crossrefSubmit.solr;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;


public class CfSOLRServer {
	private SolrServer bookCore;
	private SolrServer contentCore;
	private static final CfSOLRServer CFSOLRSERVER = new CfSOLRServer();
	
	public static final String KEY_SOCKET_TIMEOUT = "KEY_SOCKET_TIMEOUT";
	public static final String KEY_CONN_TIMEOUT = "KEY_CONN_TIMEOUT";
	public static final String KEY_DEF_MAX_CONN_PER_HOST = "KEY_DEF_MAX_CONN_PER_HOST";
	public static final String KEY_MAX_TOTAL_CONN = "KEY_MAX_TOTAL_CONN";
	public static final String KEY_FOLLOW_REDIRECTS = "KEY_FOLLOW_REDIRECTS";
	public static final String KEY_ALLOW_COMPRESSION = "KEY_ALLOW_COMPRESSION";
	public static final String KEY_MAX_RETRIES = "KEY_MAX_RETRIES";
	
	private CfSOLRServer(){
		try 
		{
			Map<String, String> settings = new HashMap<String, String>();
			settings.put(CfSOLRServer.KEY_SOCKET_TIMEOUT, System.getProperty("socket.timeout"));
			settings.put(CfSOLRServer.KEY_CONN_TIMEOUT, System.getProperty("connection.timeout"));
			settings.put(CfSOLRServer.KEY_DEF_MAX_CONN_PER_HOST, System.getProperty("default.max.connection.per.host"));
			settings.put(CfSOLRServer.KEY_MAX_TOTAL_CONN, System.getProperty("max.total.connections"));
			settings.put(CfSOLRServer.KEY_FOLLOW_REDIRECTS, System.getProperty("follow.redirects"));
			settings.put(CfSOLRServer.KEY_ALLOW_COMPRESSION, System.getProperty("allow.compression"));
			settings.put(CfSOLRServer.KEY_MAX_RETRIES, System.getProperty("max.retries"));
			
			bookCore = initServer(new CommonsHttpSolrServer(System.getProperty("solr.home.url") + System.getProperty("core.book")), settings);;	
			contentCore = initServer(new CommonsHttpSolrServer(System.getProperty("solr.home.url") + System.getProperty("core.content")), settings);		

		} 
		catch(MalformedURLException e)
		{
			System.err.println("[MalformedURLException] SOLRServer() message: " + e.getMessage());
		}
		catch (Exception e) 
		{
			System.err.println("[Exception] SOLRServer() message: " + e.getMessage());
		}
	}
	
		
	public SolrServer getBookCore() {
		return bookCore;
	}
	
	public SolrServer getContentCore() {
		return contentCore;
	}
	
	public static CfSOLRServer getInstance(){
		return CFSOLRSERVER;
	}
	
	private SolrServer initServer(CommonsHttpSolrServer server, Map<String, String> settings){

		server.setSoTimeout(Integer.parseInt(settings.get(CfSOLRServer.KEY_SOCKET_TIMEOUT)));
		server.setConnectionTimeout(Integer.parseInt(settings.get(CfSOLRServer.KEY_CONN_TIMEOUT)));
		server.setDefaultMaxConnectionsPerHost(Integer.parseInt(settings.get(CfSOLRServer.KEY_DEF_MAX_CONN_PER_HOST)));
		server.setMaxTotalConnections(Integer.parseInt(settings.get(CfSOLRServer.KEY_MAX_TOTAL_CONN)));
		server.setFollowRedirects(Boolean.parseBoolean(settings.get(CfSOLRServer.KEY_FOLLOW_REDIRECTS)));
		server.setAllowCompression(Boolean.parseBoolean(settings.get(CfSOLRServer.KEY_ALLOW_COMPRESSION)));
		server.setMaxRetries(Integer.parseInt(settings.get(CfSOLRServer.KEY_MAX_RETRIES)));

		return server;
	}
}
