package org.cambridge.ebooks.production.crossrefSubmit.daily.bean;

import java.util.Date;

import org.cambridge.ebooks.production.common.Status;
import org.cambridge.ebooks.production.ebook.EBookComponent;
import org.cambridge.ebooks.production.jpa.common.EBook;
import org.cambridge.ebooks.production.util.StringUtil;

public class EBookBean {

	
	
	
	private String bookId="";
	private String isbn;
	private String title="";
	private String alphasort;
	private EBook ebook;
	private Integer statusCode;
	private String statusDescription;
	private Date pubPrint;
	private Date pubOnline;
	
	//start
	private String product;
	private String publisher;
	//end
	
	private String pubPrintDate;
	private String pubOnlineDate;
	private String loadedOnlineDate;
		
	public EBookBean(){}
	
	public EBookBean(EBook ebook){
		
		this.ebook = ebook;
		this.bookId = ebook.getBookId();
		this.isbn = ebook.getIsbn();
		this.title = ebook.getTitle();
		this.alphasort = ebook.getTitleAlphasort();
		this.statusCode = ebook.getStatus();
		
		this.statusDescription = Status.getStatus(ebook.getStatus()).getDescription();
		if(! ebook.getBookData().isEmpty()){
			this.pubPrint = ebook.getBookData().get(0).getPubPrint();
			this.pubOnline = ebook.getBookData().get(0).getPubOnline();
		} else {
			this.pubPrint = null;
			this.pubOnline = null;
		}
		
		
	}
		
//	private String formatPubPrint(List<EBookIsbnDataLoad> bookData){
//		String returnVal="";
//		
//		if(bookData.isEmpty()){
//			returnVal = "no info";
//		} else {
//			//Calendar cal = Calendar.getInstance();
//			//cal.setTime(pubPrint);
//			//returnVal = String.valueOf(cal.get(Calendar.YEAR));
//			returnVal = bookData.get(0).getPubPrint().toString();
//		} 
//		return returnVal;
//	}
//	
//	private String formatPubOnline(List<EBookIsbnDataLoad> bookData){
//		String returnVal="";
//		
//		if(bookData.isEmpty()){
//			returnVal = "no info";
//		} else {
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(pubOnline);
//			returnVal = String.valueOf(cal.get(Calendar.YEAR)) + "." +
//						String.valueOf(cal.get(Calendar.MONTH)) + "." +
//						String.valueOf(cal.get(Calendar.DATE));
//			returnVal = bookData.get(0).getPubOnline().toString();
//		} 
//		return returnVal;
//	}
	
	//start
	public String getProduct(){
		return product;
	}
	public void setProduct(String product){
		this.product = product;
	}
	public String getPublisher(){
		return publisher;
	}
	public void setPublisher(String publisher){
		this.publisher = publisher;
	}
	//end
		
	
	public EBookBean(String bookId){
		this.bookId = bookId;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public String getBookId() {
		return bookId;
	}
	
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
		
	public String toString(){
		return "bookId: " + bookId +
		        " isbn: " + isbn +
		        " title: " + title+
		        " pubPrint: " + pubPrint+
		        " pubOnline: " + pubOnline
		        ;
	}
	
	public boolean equals(Object o) { 
		boolean result = false;
		if (o instanceof EBookComponent) {
			EBookComponent b = (EBookComponent) o;
			if ( b.getBookId().equals(this.getBookId()) )  { 
				result = true;
			}
		}
		return result;
	}
	
	public int compareTo(EBookBean ebb) {
	    int result = title.compareTo(ebb.title);
	    return result;
	} 
	
	public static boolean canBeUploaded(Status status){
		switch(status)
		{
			case NOTLOADED: 
			case REJECTED:
			case DISAPPROVED:
			case RELOAD:
			case UNPUBLISHED:
			case RETURNTOUPLOADER: return true;
			default: return false;
		}
	}
	
	public String getTitle() {
		return StringUtil.correctHtmlTags(title);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public EBook getEbook() {
		return ebook;
	}
	
	public void setEbook(EBook ebook) {
		this.ebook = ebook;
	}
	
	public String getStatusDescription() {
		return statusDescription;
	}
	
	public void setStatusDescription(String statusDescription) {		
		this.statusDescription = statusDescription;
	}
	
	/**
	 * @return the alphasort
	 */
	public String getAlphasort() {
		return StringUtil.correctHtmlTags(alphasort);
	}

	/**
	 * @param alphasort the alphasort to set
	 */
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getPubPrintDate() {
		return pubPrintDate;
	}

	public void setPubPrintDate(String pubPrintDate) {
		this.pubPrintDate = pubPrintDate;
	}

	public String getPubOnlineDate() {
		return pubOnlineDate;
	}

	public void setPubOnlineDate(String pubOnlineDate) {
		this.pubOnlineDate = pubOnlineDate;
	}

	public String getLoadedOnlineDate() {
		return loadedOnlineDate;
	}

	public void setLoadedOnlineDate(String loadedOnlineDate) {
		this.loadedOnlineDate = loadedOnlineDate;
	}

	public Date getPubPrint() {
		return pubPrint;
	}

	public void setPubPrint(Date pubPrint) {
		this.pubPrint = pubPrint;
	}

	public Date getPubOnline() {
		return pubOnline;
	}

	public void setPubOnline(Date pubOnline) {
		this.pubOnline = pubOnline;
	}
	
	

}
