package org.cambridge.ebooks.production.crossrefSubmit.daily.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;



public class CrossrefInfoBean {
	private String isbn;
	private String onlineFlag;
	private Date lastModified;
	private String bookId;
	private String productGroup; //86267 added product group -jubs
	private String subProductGroup;
		
	public CrossrefInfoBean(String isbn, String onlineFlag, Date lastModified) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
		this.lastModified = lastModified;
	}
	
	public CrossrefInfoBean(String isbn, String onlineFlag, Date lastModified, String bookId, String product, String subProduct) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
		this.lastModified = lastModified;
		this.bookId = bookId;
		this.productGroup = product; //86267 added product group -jubs
		this.subProductGroup = subProduct;
	}
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getOnlineFlag() {
		return onlineFlag;
	}
	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getBookId() {
		return bookId;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	
	public String getSubProductGroup() {
		return subProductGroup;
	}
	
	public void setSubProductGroup(String subProductGroup) {
		this.subProductGroup = subProductGroup;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getDate_MMMMM_yyyy(){
		return getDateFormat("MMMMM yyyy");
	}
	public String getDate_yyyyMMdd(){
		return getDateFormat("yyyyMMdd");
	}
	public String getDate_ddMMMyyyy(){
		return getDateFormat("ddMMMyyyy");
	}	
	public String getDateFormat(String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(this.lastModified);
	}
	
	@Override
	public String toString() {
		String sql = 
			"Sending to CrossRef book id: " + this.bookId + " onlineFlag: " + this.onlineFlag + " lastModified: " + this.getDate_yyyyMMdd() 
			+" productGroup: "+this.productGroup +" subProductGroup: "+this.subProductGroup;
		return sql;
	}
	
	
}
