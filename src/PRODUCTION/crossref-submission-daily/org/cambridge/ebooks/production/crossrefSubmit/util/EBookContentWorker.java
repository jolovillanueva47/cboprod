package org.cambridge.ebooks.production.crossrefSubmit.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentDAO;
import org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean;
import org.cambridge.ebooks.production.crossref.ejb.jpa.EBookContent;
import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefStringUtil;
import org.cambridge.ebooks.production.crossref.ejb.util.ListUtil;
import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc.DBInfo;
import org.cambridge.ebooks.production.crossrefSubmit.solr.CfSOLRSearchWorker;
import org.cambridge.ebooks.production.crossrefSubmit.solr.CfSOLRServer;
import org.cambridge.ebooks.production.solr.bean.CrossrefContentItemDocument;
import org.cambridge.ebooks.production.util.StringUtil;


/**
 * @author rvillamor
 *
 */
public class EBookContentWorker {
//	private static SolrServer bookCore = EbookSolrServer.getBookCore();
	private static final String URL_SOLR_HOME = System.getProperty("solr.home.url");
	private static final String CORE_BOOK = System.getProperty("core.book");
	private static final String CORE_CHAPTER = System.getProperty("core.chapter");	
	
	public final static String UPDATE = "update";
	public final static String CHAPTER = "chapter";
	public final static String OTHER = "other";
	public final static String DISPLAY_BY_STATUS = "displayByStatus";
	
//	private List<Integer> status = new ArrayList<Integer>();
	private final CfSOLRSearchWorker worker = new CfSOLRSearchWorker();
	
	private EBookContentDAO dao;
	
	/**
	 * 
	 * Method to fetch content items from index and database by bookId
	 * list of content items from the index is merged with the content items from the database,
	 * if an item from the index is not in the database, it will not be included.
	 * 
	 * @param bookId
	 * 
	 * @return EBookContentBean List
	 * @throws SQLException 
	 */
	public List<EBookContentBean> ebookContents(String bookId, boolean includeChapterContributors) throws SQLException{	
		System.out.println(EBookContentWorker.class + "---BOOK ID:" + bookId);
		
		List<EBookContentBean> ebookContentFromIndex = null;
		List<EBookContent> ebookContentFromDB = null;	
		
		ebookContentFromDB = DBInfo.searchContentsByBookId(bookId);
//		System.out.println("FROM DB*****************************************");
//		outEBookContentDB(ebookContentFromDB);
		
//		System.out.println("FROM ORIGINAL INDEX*****************************");
//		ebookContentFromIndex = EBookContentIndexSearch.ebookContents(bookId,includeChapterContributors);
//		outEBookContentBean(ebookContentFromIndex);
		
		ebookContentFromIndex = getContentsFromIndex(bookId);
//		System.out.println("FROM SOLR***************************************");
//		outEBookContentBean(ebookContentFromIndex);
		
		
		
		setContentProperties(ebookContentFromDB, ebookContentFromIndex);
		
		return ebookContentFromIndex;
	}
	
	@SuppressWarnings("unused")
	private void outEBookContentDB(List<EBookContent> ebookContentFromDB){
		int count = 0;
		for (EBookContent ebc : ebookContentFromDB){
			System.out.println("*************************************"+ ++count);
			if (! (ebc.getBook() == null )){
				System.out.println("getBook().getTitle() ="+ (ebc.getBook().getTitle() == null || ebc.getBook().getTitle() == "" ? "" : ebc.getBook().getTitle()) );
				System.out.println("getBook().getBookId() ="+ (ebc.getBook().getBookId() == null || ebc.getBook().getBookId() == "" ? "" : ebc.getBook().getBookId()));
				System.out.println("getBook().getIsbn() ="+ (ebc.getBook().getIsbn() == null || ebc.getBook().getIsbn() == "" ? "" : ebc.getBook().getIsbn()));
				System.out.println("getBook().getModifiedBy() ="+ (ebc.getBook().getModifiedBy() == null || ebc.getBook().getModifiedBy() == "" ? "" : ebc.getBook().getModifiedBy()));
				System.out.println("getBook().getModifiedBy() ="+ (ebc.getBook().getSeriesId() == null || ebc.getBook().getSeriesId() == "" ? "" : ebc.getBook().getSeriesId()));
				System.out.println("getBook().getStatus() ="+ ebc.getBook().getStatus());
				System.out.println("getBook().getModifiedDate() ="+ (ebc.getBook().getModifiedDate() == null  ? "" : ebc.getBook().getModifiedDate()));
			}
			System.out.println("getContentId ="+ ebc.getContentId());
			System.out.println("getDisplayFileType ="+ ebc.getDisplayFileType());
			System.out.println("getFileName ="+ ebc.getFileName());
			System.out.println("getIsbn ="+ ebc.getIsbn());
			System.out.println("getModifiedBy ="+ ebc.getModifiedBy());
			System.out.println("getRemarks ="+ ebc.getRemarks());
			System.out.println("getStatus ="+ ebc.getStatus());
			System.out.println("getType ="+ ebc.getType());
			System.out.println("getEmbargoDate ="+ ebc.getEmbargoDate());
			System.out.println("getModifiedDate ="+ ebc.getModifiedDate());
		}
	}
	
	@SuppressWarnings("unused")
	private void outEBookContentBean(List<EBookContentBean> ebookContentFromIndex){
		int count = 0;
		for (EBookContentBean cb : ebookContentFromIndex){
			System.out.println("*************************************"+ ++count);
			System.out.println("getAbstractImage = "+cb.getAbstractImage());
			System.out.println("getAbstractText = "+cb.getAbstractText());
			System.out.println("getAlphasort = "+cb.getAlphasort());
			System.out.println("getBookId = "+cb.getBookId());
			System.out.println("getChapterContributors = "+cb.getChapterContributors());
			System.out.println("getContentId = "+cb.getContentId());
			System.out.println("getDoi = "+cb.getDoi());
			System.out.println("getEbookContent = "+cb.getEbookContent());
			System.out.println("getFileName = "+cb.getFileName());
			System.out.println("getLabel = "+cb.getLabel());
			System.out.println("getPageEnd = "+cb.getPageEnd());
			System.out.println("getPageStart = "+cb.getPageStart());
			System.out.println("getParentId = "+cb.getParentId());
			System.out.println("getPosition = "+cb.getPosition());
			System.out.println("getTitle = "+cb.getTitle());
			System.out.println("getType = "+cb.getType());
		}
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setContentProperties(List ebookContentList, List ebookContentBeanList) {
		
		try {
			if(ListUtil.isNotNullAndEmpty(ebookContentList) && ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
//				LogManager.info(EBookContentWorker.class, "[Number of EBook Contents] DB Size: "	+ ebookContentList.size() + " Index Size: " + ebookContentBeanList.size());
				
				ebookContentBeanList.retainAll(ebookContentList);
				if(ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
					
					String partId = "";
					for(Object ebookContentBean : ebookContentBeanList) {
						Object ebookContent = ListUtil.searchObject(ebookContentList, ebookContentBean);
						if(ebookContent != null) {
							EBookContent ebookContent_ = (EBookContent)ebookContent;	
							EBookContentBean ebookContentBean_ = ((EBookContentBean)ebookContentBean);
							ebookContentBean_.setEbookContent(ebookContent_);
							
							String label = ebookContentBean_.getLabel();							
							String title = ebookContentBean_.getTitle();							
							String ebookType = ebookContentBean_.getType();
							String newTitle = "";
							
							title = CrossRefStringUtil.correctHtmlTags(title);	
							
							if(!ebookType.equals("part-title") 
									&& partId.equals(ebookContentBean_.getParentId())) {
//								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
//								LogManager.info(EBookContentWorker.class, "---PARENT ID:" + ebookContentBean_.getParentId());
//								LogManager.info(EBookContentWorker.class, "---INDENTED: true");
								ebookContentBean_.setIndented(true);
							}
							
//							LogManager.info(EBookContentWorker.class, "---EBOOK TYPE:" + ebookType);
							
							if(ebookType.equals("part-title")) {										
								partId = ebookContentBean_.getContentId();
//								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
								
								EBookContent ec = EBookContentDAO.searchContentByContentIdAndType(partId, ebookType);
								if(ec.getStatus() == -1) {
									ebookContentBean_.setNotLoadedPartTitle(true);
								} 						
							}
							
							if(label == null) {
//								newTitle = ebookContentBean_.getPosition() + ": " + title;
								newTitle = title;
							} else {
								newTitle = label + ": " + title;
							}
							ebookContentBean_.setTitle(newTitle);
						}
					}					
				}				
			}
		}
		catch(Exception e) {		
			System.out.println(EBookContentWorker.class + " setContentProperties(List, List): " + e.getMessage());
			e.printStackTrace();
		}
	}


	public EBookContentDAO getDao() {
		return dao;
	}


	public void setDao(EBookContentDAO dao) {
		this.dao = dao;
	}
	
	private  List<EBookContentBean> getContentsFromIndex(String bookId){
		
//		SolrQuery query = generateQuery(bookId, true);
//		
//		QueryResponse response = null;
//		List<CrossrefContentItemDocument> bookDocList = null;
//		try {
//			response = bookCore.query(query, METHOD.GET);
//			bookDocList = response.getBeans(CrossrefContentItemDocument.class);
//		} catch (SolrServerException e) {
//			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
//		}
		String query = "book_id:" + bookId + " AND content_type:content-item";
		System.out.println(query);
		List<BookContentItemDocument> contentItems = worker.searchCore(new BookContentItemDocument(), query, CfSOLRServer.getInstance().getContentCore());
//		List<BookContentItemDocument> contentItems = worker.searchContentCore("book_id:" + bookId + " AND content_type:content-item");
		
		return populateContentItemBeans(contentItems);
	}
	
	private List<EBookContentBean> populateContentItemBeans(List<BookContentItemDocument> contentItems){
		List<EBookContentBean> result = new ArrayList<EBookContentBean>();
		for(BookContentItemDocument contentItem : contentItems)
			result.add(populateContentBean(contentItem));
	
		return result;
	}
	
	private EBookContentBean populateContentBean(BookContentItemDocument contentItem){
		
		EBookContentBean contentBean = new EBookContentBean();
		contentBean.setContentId(contentItem.getContentId());
		contentBean.setFileName(contentItem.getPdfFilename());
		contentBean.setTitle(contentItem.getTitle());
		contentBean.setDoi(contentItem.getDoi());
		contentBean.setAbstractText(getAbstract(contentItem.getIsbn(), contentItem.getContentId()));
		contentBean.setAbstractImage(contentItem.getAbstractFilename());
		contentBean.setType(contentItem.getType());
		contentBean.setPosition(contentItem.getPosition());
		contentBean.setPageStart(contentItem.getPageStart());
		contentBean.setPageEnd(contentItem.getPageEnd());
		contentBean.setAlphasort(contentItem.getTitleAlphasort());
		contentBean.setLabel(contentItem.getLabel());
		contentBean.setParentId(contentItem.getParentId());
		
		contentBean.setChapterContributors(getChapterContributors(contentItem));
		
		return contentBean;
	}

	private ArrayList<ContributorBean> getChapterContributors(BookContentItemDocument contentItem){
		ArrayList<ContributorBean> contributors = new ArrayList<ContributorBean>();
		
		List<String> authorNames = contentItem.getAuthorNameLfList();		
		List<String> authorPostions = contentItem.getAuthorPositionList();
		ContributorBean cb;
		
		if(authorNames != null && !authorNames.isEmpty())
		{
			for (int ctr=0; ctr<authorNames.size(); ctr++) 
			{
				cb = new ContributorBean();
				cb.setContributor(authorNames.get(ctr));
				cb.setPosition(authorPostions != null && !"none".equals(authorPostions.get(ctr)) ? authorPostions.get(ctr) : "0");
				contributors.add(cb);
			}
		}

		return contributors;
	}
	
	protected static SolrQuery generateQuery(String bookId, boolean addShards){
		String urlShardsSolrHome = URL_SOLR_HOME.replaceAll("http://", "");
		StringBuilder queryBuilder = new StringBuilder();
		if(StringUtils.isNotEmpty(bookId)) {
			queryBuilder.append("id:")
			.append(bookId)
			.append(" OR ")
			.append("book_id:")
			.append(bookId);
		} else {
			queryBuilder.append("id:")
			.append(bookId)
			.append(" OR ")
			.append("book_id:")
			.append(bookId);
		}
		
		SolrQuery query = new SolrQuery();		
		query.setQuery(queryBuilder.toString());
		query.setRows(1000);
		if(addShards)
			query.setParam("shards", urlShardsSolrHome + CORE_BOOK + "," + urlShardsSolrHome + CORE_CHAPTER);
		
		System.out.println("query:" + query.toString());
		return query;
	}
	
	@SuppressWarnings("unused")
	private String getQueryString(String bookId) {
		StringBuffer queryString = new StringBuffer("book_id:" + bookId);
		//queryString.append(" AND (PARENT:metadata OR PARENT:subject-group OR PARENT:pub-dates OR ELEMENT:book)");
		return queryString.toString();
	}
	
	@SuppressWarnings("unused")
	private List<EBookContentBean> populateContentBeans(List<CrossrefContentItemDocument> contentItems){
		List<EBookContentBean> result = new ArrayList<EBookContentBean>();
		for(CrossrefContentItemDocument contentItem : contentItems)
			if(! contentItem.getContentType().equals("toc")){
			result.add(populateContentBean(contentItem));
			}
	
		return result;
	}
	
	private EBookContentBean populateContentBean(CrossrefContentItemDocument contentItem){
		
		EBookContentBean contentBean = new EBookContentBean();
		contentBean.setContentId(contentItem.getId());
		contentBean.setFileName(contentItem.getPdfFilename());
		contentBean.setTitle(contentItem.getTitle());
		contentBean.setDoi(contentItem.getDoi());
		contentBean.setAbstractText(getAbstract(contentItem.getIsbn(), contentItem.getContentId()));
		contentBean.setAbstractImage(contentItem.getAbstractFilename());
		contentBean.setType(contentItem.getType());
		contentBean.setPosition(contentItem.getPosition());
		contentBean.setPageStart(contentItem.getPageStart());
		contentBean.setPageEnd(contentItem.getPageEnd());
		contentBean.setAlphasort(contentItem.getTitle());
		contentBean.setLabel(contentItem.getLabel());
		
		contentBean.setChapterContributors(convertContributor(getChapterContributors(contentItem)));
		
		return contentBean;
	}

	private String getAbstract(String isbn, String contentId){
		String result = "";
		final File abstractDir = new File(IsbnContentDirUtil.getPath(isbn) + contentId + "_abstract.txt" );
		
		try
		{
			StringBuilder sb = new StringBuilder();
			if(abstractDir != null && abstractDir.exists())
			{
				String temp = null;
				FileReader reader = new FileReader(abstractDir);
				BufferedReader breader = new BufferedReader(reader);
				while((temp = breader.readLine()) != null)
					sb.append(temp);
		
				result = sb.toString();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return result;
	}
	
	private ArrayList<ContributorBean> getChapterContributors(CrossrefContentItemDocument contentItem){
		ArrayList<ContributorBean> contributors = new ArrayList<ContributorBean>();
		
		List<String> authorNames = contentItem.getAuthorNameAlphasortList();
		List<String> authorPostions = contentItem.getAuthorPositionList();
		
		if(StringUtil.isEmpty(authorNames)){
			authorNames = contentItem.getAuthorNameList();
		}
		ContributorBean cb;
		
		if(authorNames != null && !authorNames.isEmpty())
		{
			for (int ctr=0; ctr<authorNames.size(); ctr++) 
			{
				cb = new ContributorBean();
				cb.setContributor(StringEscapeUtils.escapeXml(authorNames.get(ctr)));
				cb.setPosition(authorPostions != null && !"none".equals(authorPostions.get(ctr)) ? authorPostions.get(ctr) : String.valueOf(ctr+1));
				contributors.add(cb);
			}
		}
	
		return contributors;
	}
	
	private ArrayList<org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean> convertContributor(ArrayList<org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean> prodCB){
		ArrayList<org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean> crossrefCB = new ArrayList<org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean>();
		for(ContributorBean cb : prodCB){
			org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean toAdd = new org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean();
			
			toAdd.setContributor(cb.getContributor());
			toAdd.setPosition(cb.getPosition());
			
			crossrefCB.add(toAdd);
		}
		return crossrefCB;
		
	}
	
}

