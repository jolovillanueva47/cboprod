package org.cambridge.ebooks.production.crossrefSubmit.daily;

import java.sql.Timestamp;

import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.CrossrefInfoBean;
import org.cambridge.ebooks.production.util.DateUtil;

public class DailyLogger {
	public static void createLogHeader(StringBuffer sb){
		sb.append("***************************************** \n");
		sb.append("online reindex from DB" + new Timestamp(new java.util.Date().getTime()) + "\n");
	}
	
	public static void append(StringBuffer sb, String str){
		sb.append(str);
	}
	
	
	
	public static void createLogBody(StringBuffer sb, CrossrefInfoBean b){
		sb.append(b.toString() + "\n");
	}
	
	public static void createLogEnd(StringBuffer sb){
		sb.append("********************** FIN \n");
	}	
	
	public static String generateFilename(String... args){
		int size = args.length;
		String loc = args[size-1];		
			
		/*generate date format*/
		String currDate = DateUtil.getCurrentDate("yyyy-MMMMM-dd");
		
		String filename = "reindex.daily." + currDate;
		
		return loc + filename;
	}
}
