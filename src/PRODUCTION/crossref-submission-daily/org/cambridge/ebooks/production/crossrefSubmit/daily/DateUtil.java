package org.cambridge.ebooks.production.crossrefSubmit.daily;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static final String getCurrentDate(String pattern){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);		
		return sdf.format(date);
	}
}
