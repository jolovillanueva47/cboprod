package org.cambridge.ebooks.production.crossrefSubmit.daily.bean;

public class HeadBean {
	
	private String email;
	private String publisherName;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
}
