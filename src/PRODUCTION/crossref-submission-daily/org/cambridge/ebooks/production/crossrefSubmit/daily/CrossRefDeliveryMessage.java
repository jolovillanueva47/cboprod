package org.cambridge.ebooks.production.crossrefSubmit.daily;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.common.ServiceLocatorException;

/**
 * @author rvillamor
 *
 */

public class CrossRefDeliveryMessage {

	public static void sendDeliveryEvent(String bookId, String username)  {
		
		//if(isNotEsampling(bookId)){    
			try 
	        {	
	            ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");
	
	            Connection connection = connectionFactory.createConnection();
	            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	            
	            Destination destination = getJmsDestination("queue/crossref/logger");            
	            MessageProducer messageProducer = session.createProducer(destination);
	            Message payload = session.createMapMessage();
	            
	        	payload.setStringProperty("BOOK_ID", bookId);
	        	payload.setStringProperty("USERNAME", username);
	
	            try 
	            { 
		            messageProducer.send(payload);
		            System.out.println( bookId +" has been delivered");
	            } 
	            finally 
	            { 
	            	messageProducer.close();
	                session.close();
	                connection.close();	
	            }
	            
	        } 
	        catch (JMSException je) 
	        {
	        	//TODO: fix loggers
	        	//LogManager.error(CrossRefDeliveryMessage.class, "[JMSException] Message not sent to asset-ejb: " + je.getMessage());
	        	System.out.println("[JMSException]: " + je);
	        } 
	        catch (ServiceLocatorException sle) 
	        {
	        	//LogManager.error(CrossRefDeliveryMessage.class, "[ServiceLocatorException] Message not sent to asset-ejb:" + sle.getMessage());
	        	System.out.println("****************ServiceLocatorException found");
	        	sle.printStackTrace();
	        }
		//}
    }
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws ServiceLocatorException {
		ConnectionFactory jmsConnectionFactory = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory)ctx.lookup(jmsConnectionFactoryJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws ServiceLocatorException {
		Destination jmsDestination = null;
		try 
		{
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} 
		catch (ClassCastException cce) 
		{
		   throw new ServiceLocatorException(cce);
		} 
		catch (NamingException ne) 
		{
		   throw new ServiceLocatorException(ne);
		}
		return jmsDestination;
	}
	
//	private static boolean isNotEsampling(String bookId) {
//		String result = "";
//		boolean esamplingFlag = false;
//		
//		bookId = bookId.substring(3); //remove "CBO" from bookId
//		
//		List<ProductIsbnTitle> list = PersistenceUtil.searchList(new ProductIsbnTitle(), ProductIsbnTitle.GET_ESAMPLING_FLAG, bookId);
//		result = list.get(0).getEsamplingFlag();
//		
//		if(result == "N"){
//			esamplingFlag = true;
//		}
//		
//		System.out.println("esamplingFlag = " +esamplingFlag);
//		return esamplingFlag;
//	}
}
