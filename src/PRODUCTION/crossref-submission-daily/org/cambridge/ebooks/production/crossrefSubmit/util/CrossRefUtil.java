package org.cambridge.ebooks.production.crossrefSubmit.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;

public class CrossRefUtil {
	
	public static String readFile() throws IOException{
		String filePath = System.getProperty("crossref.app.dir") +"lastSuccessful.txt";
		String out = "";
		
		BufferedReader in = new BufferedReader(new FileReader(filePath));
		out = in.readLine();
		
		
		return out;
	}
	
	public static void printFile(String path, String text) throws IOException {
		Writer output = null;	    
	    File file = new File(path);
	    output = new BufferedWriter(new FileWriter(file));
	    output.write(text);
	    output.close();
	    System.out.println("File "+ path +" has been written");  
	}
	
	public static String currentDate(){
		Calendar dateNow = Calendar.getInstance();
		
		return StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0")+ 
		threeLetterMonth(dateNow.get(Calendar.MONTH)) +
		String.valueOf(dateNow.get(Calendar.YEAR)).substring(2);
		
	}
	
	private static String threeLetterMonth(int iMonth){
		String val = "";
		switch(iMonth){
			case 0: val = "-JAN-"; 	break;
			case 1: val = "-FEB-"; 	break;
			case 2: val = "-MAR-"; 	break;
			case 3: val = "-APR-"; 	break;
			case 4: val = "-MAY-"; 	break;
			case 5: val = "-JUN-"; 	break;
			case 6: val = "-JUL-"; 	break;
			case 7: val = "-AUG-"; 	break;
			case 8: val = "-SEP-"; 	break;
			case 9: val = "-OCT-";	break;
			case 10: val = "-NOV-";	break;
			case 11: val = "-DEC-";	break;
				
		}
		return val;
	}
	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
}
