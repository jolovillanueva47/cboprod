package org.cambridge.ebooks.production.crossrefSubmit.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.production.crossref.ejb.constant.RedirectTo;
import org.cambridge.ebooks.production.crossref.ejb.document.OtherEditionBean;
import org.cambridge.ebooks.production.crossref.ejb.document.PublisherLocBean;
import org.cambridge.ebooks.production.crossref.ejb.document.PublisherNameBean;
import org.cambridge.ebooks.production.crossref.ejb.document.RoleAffBean;
import org.cambridge.ebooks.production.crossref.ejb.document.SubjectBean;
import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc.DBInfo;
import org.cambridge.ebooks.production.crossrefSubmit.util.ImageUtil;
//import org.cambridge.ebooks.production.util.StringUtil;
import org.cambridge.ebooks.service.solr.bean.BookSolrDocument;
import org.cambridge.util.Misc;

/**
 * @author rvillamor
 */
public class EbookDetailsBean {
	private final EBookSearchWorker solrWorker = new EBookSearchWorker();
	
	private final static String ID = "bookId";
	private final static String MAIN_TITLE = "MAIN_TITLE";
	private final static String ALPHASORT = "ALPHASORT";
	private final static String SUB_TITLE = "SUBTITLE";
	private final static String EDITION = "EDITION";
	private final static String NUMBER = "NUMBER";
	private final static String AUTHOR = "AUTHOR";
	private final static String EDITOR = "EDITOR";
	private final static String AUTHOR_POSITION = "AUTHOR_POSITION";
	private final static String EDITOR_POSITION = "EDITOR_POSITION";
	private final static String AFFILIATION = "AFFILIATION";
	private final static String DOI = "DOI";
	private final static String ISBN = "ISBN";
	private final static String ALT_ISBN = "ALT_ISBN";
	private final static String VOLUME_NUMBER = "VOLUME_NUMBER";
	private final static String VOLUME_TITLE = "VOLUME_TITLE";
	private final static String PART_NUMBER = "PART_NUMBER";
	private final static String PART_TITLE = "PART_TITLE";
	private final static String TYPE = "TYPE";
	private final static String VOLUME = "VOLUME";
	private final static String OTHER_VOLUME = "OTHER_VOLUME";
	private final static String SERIES = "SERIES";
	private final static String SERIES_POSITION = "POSITION";
	private final static String PUBLISHER = "PUBLISHER";
	private final static String PUBLISHER_LOC = "PUBLISHER_LOC";
	private final static String PRINT_DATE = "PRINT_DATE";
	private final static String ONLINE_DATE = "ONLINE_DATE";
	private final static String COPYRIGHT_STATEMENT = "COPYRIGHT_STATEMENT";
	private final static String SUBJECT = "SUBJECT";
	private final static String BLURB = "BLURB";
	private final static String PAGES = "PAGES";
	private final static String COVER_IMAGE = "FILENAME";
	private final static String CODE = "CODE";
	private final static String BOOK_GROUP = "GROUP";
	private final static String ROLE = "ROLE";
	private final static String TRANS_TITLE = "TRANS_TITLE";
	private final static String PART = "PART";
	private final static String LEVEL = "LEVEL";
	private final static String ELEMENT = "ELEMENT";
	private final static String POSITION = "POSITION";
	
	private final static String ELEMENT_OTHER_EDITION = "other-edition";
	private final static String ELEMENT_PRINT_DATE = "print-date";
	private final static String ELEMENT_ONLINE_DATE = "online-date";

	private final static String INDEX_DIRECTORY = System.getProperty("index.dir").trim();
	public final static String IMAGE_DIR_PATH = System.getProperty("content.url").trim();
	public final static String MISSING_COVER_IMAGE = System.getProperty("missing.cover.standard").trim();
	public final static String MISSING_COVER_THUMBNAIL = System.getProperty("missing.cover.thumbnail").trim();
	public final static String NO_COVER_IMAGE = System.getProperty("no.cover.standard").trim();
	public final static String NO_COVER_THUMBNAIL = System.getProperty("no.cover.thumbnail").trim();
	public final static String CORRUPT_COVER_IMAGE = System.getProperty("corrupt.cover.standard").trim();
	public final static String CORRUPT_COVER_THUMBNAIL = System.getProperty("corrupt.cover.thumbnail").trim();
	public final static String NO_IMAGE_IN_XML = "NO_IMAGE_IN_XML";
	public final static String IMAGE_CORRUPTED = "Corrupted image.";
	
	public final static String HTML_BREAK = "<br />";
	
	public final static String POPUP = "POPUP";
	public final static String YES = "Y";
	public final static String BOOK_ID = "BOOK_ID";
	public final static String SHOW = "SHOW";
	public final static String META = "Metadata";
	public final static String KEYWORD = "Keywords";

	private String bookId;
	private String mainTitle;
	private String subTitle;
	private String edition;
	private String editionNumber;
	private String seriesNumber;
	private String seriesPosition;
	private String author;
	private String editor;
	private String affiliation;
	private String doi;
	private String eisbn;
	private String paperback;
	private String hardback;
	private String other;
	private String volume;
	private String volumeNumber;
	private String volumeTitle;
	private String partNumber;
	private String partTitle;
	private String otherVolume;
	private String series;
	private String publisherName;
	private String publisherLoc;
	private String printDate;
	private String onlineDate;
	private String copyrightStatement;
	private String subject;
	private String blurb;
	private String pages;
	private String standardImage;
	private String thumbImage;
	private String bookGroup;
	private String mainTitleAlphaSort;
	private String seriesAlphasort;
	private String seriesPart;
	private String seriesCode;	
	private String transTitle;
	private String subjectLevel;	
	
	private String publisherCode;
	private String productCode;
	private String subProductCode;
	
	private TreeMap<String, String> authorMap;
	private TreeMap<String, String> editorMap;

	private ArrayList<RoleAffBean> authorAffList;
	private ArrayList<RoleAffBean> editorAffList;
	private ArrayList<SubjectBean> subjectList;
	private ArrayList<OtherEditionBean> otherEdList;
	private ArrayList<PublisherNameBean> pubNameList;	
	private ArrayList<PublisherLocBean> pubLocList;


	private boolean displayEdition;
	private boolean displaySubtitle;
	private boolean displayAuthor;
	private boolean displayEditor;
	private boolean displayAffiliation;
	private boolean displayAltIsbn;
	private boolean displayAltIsbnType;
	private boolean displayPaperback;
	private boolean displayHardback;
	private boolean displayOther;
	private boolean displayVolume;
	private boolean displayVolumeNumber;
	private boolean displayVolumeTitle;
	private boolean displayPartNumber;
	private boolean displayPartTitle;
	private boolean displayOtherVolume;
	private boolean displaySeries;
	private boolean displayStandardImage;
	private boolean displayThumbImage;
	private boolean displayTransTitle;
	private boolean displayPart;
	private boolean displayAuthorAlphasort;
	private boolean displaySeriesPart;
	private boolean displaySeriesCode;	
	private boolean displaySeriesPosition;
	private boolean displaySeriesNumber;
	private boolean displayAuthorRole;
	
	@SuppressWarnings("unused")
	private static final String[] DOCUMENT_FIELDS_SEARCH = new String[] {
			MAIN_TITLE, SUB_TITLE, EDITION, AUTHOR, DOI, ISBN, ALT_ISBN,
			VOLUME_NUMBER, VOLUME_TITLE, PART_NUMBER, PART_TITLE, VOLUME, 
			OTHER_VOLUME, SERIES, PUBLISHER, PUBLISHER_LOC, PRINT_DATE,
			ONLINE_DATE, COPYRIGHT_STATEMENT, SUBJECT, BLURB, BOOK_GROUP,
			PAGES, COVER_IMAGE, TRANS_TITLE };

	/**
	 * will be used for indentifying the common field props (dynamically assigning bean prop)
	 */
	private static final HashMap<String, String[]> FIELD_PROP_MAP = new HashMap<String, String[]>();

	static {
		FIELD_PROP_MAP.put(SUB_TITLE, new String[] { "subTitle",
				"displaySubtitle" });
		FIELD_PROP_MAP.put(DOI, new String[] { "doi", "" });
		FIELD_PROP_MAP.put(ISBN, new String[] { "eisbn", "" });
		FIELD_PROP_MAP.put(VOLUME, new String[] { "volume", "displayVolume" });
		FIELD_PROP_MAP.put(OTHER_VOLUME, new String[] { "otherVolume",
				"displayOtherVolume" });
		FIELD_PROP_MAP.put(PUBLISHER, new String[] { "publisherName", "" });
		FIELD_PROP_MAP.put(PUBLISHER_LOC, new String[] { "publisherLoc", "" });
		FIELD_PROP_MAP.put(BLURB, new String[] { "blurb", "" });
		FIELD_PROP_MAP.put(BOOK_GROUP, new String[] { "bookGroup", "" });
		FIELD_PROP_MAP.put(PAGES, new String[] { "pages", "" });
		FIELD_PROP_MAP.put(TRANS_TITLE, new String[] { "transTitle",
				"displayTransTitle" });
	}

	private static final String FALSE = "false";

	public EbookDetailsBean() {
	}

	public EbookDetailsBean(String bookId) {
		this.bookId = bookId;
		try {
			show();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * resets all bean properties by creating a new bean 
	 * and copying all the properties; thereby resetting all values 
	 */
	private void resetBeanPropAll(){
		EbookDetailsBean bean = new EbookDetailsBean();
		bean.setBookId(getBookId());
		
		try {
			BeanUtils.copyProperties(this, bean);
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		} catch (InvocationTargetException e) {			
			e.printStackTrace();
		}
	}

	public String show() throws IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, InterruptedException {
		resetBeanPropAll();

		System.out.println("*******************before searchBookCore");
		populateBean(solrWorker.searchBookCore(getQueryString()));
		System.out.println("*******************after searchBookCore");
		return RedirectTo.EBOOK_DETAILS;
	}

	
	private String getQueryString() {
		StringBuffer queryString = new StringBuffer("book_id:" + getBookId());
		return queryString.toString();
	}

	/**
	 * set common fields, dynamically assign bean getters and setters
	 * 
	 * @param key
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("unused")
	private void setCommonFields(String key, Document document)
			throws IllegalAccessException, InvocationTargetException {
		String[] propVal = FIELD_PROP_MAP.get(key);
		if (propVal != null) {
			setCommonFields(key, propVal[0], propVal[1], document);
		}
	}
	
	/**
	 * set common fields, dynamically assign bean getters and setters,
	 * performs additional checking. checks first is the element is empty
	 * @param key
	 * @param prop
	 * @param check
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unused")
	private void setCommonFieldsWithCheck(String key, String prop, String check,
			Document document) throws IllegalAccessException, InvocationTargetException{
		if(document.get(key) != null && !"".equals(document.get(key))){
			setCommonFields(key, prop, check, document);
		}
	}
	
	/**
	 * set common fields, dynamically assign bean getters and setters
	 * @param key
	 * @param prop
	 * @param check
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private void setCommonFields(String key, String prop, String check,
			Document document) throws IllegalAccessException, InvocationTargetException{
		try {
			if (BeanUtils.getProperty(this, check).equals(FALSE)) {
				BeanUtils.setProperty(this, check, true);
			}
		} catch (NoSuchMethodException e) {
			// do nothing
			// some items do not have getter setter to check if should display
		}
		BeanUtils.setProperty(this, prop, document.get(key));
		
	}
	
	private String removeChars(String number){
		String cleanNumber = "";
		
		for (int x =0; x< number.length(); x++){
			if(Character.isDigit(number.charAt(x))){
				cleanNumber+=number.charAt(x);
			}
		}
		
		return cleanNumber;
	}
	
	@SuppressWarnings("unused")
	private void populateSolrBean(List<BookSolrDocument> bookDocList){
		authorMap = new TreeMap<String, String>();
		editorMap = new TreeMap<String, String>();
		authorAffList = new ArrayList<RoleAffBean>();
		editorAffList = new ArrayList<RoleAffBean>();
		subjectList = new ArrayList<SubjectBean>();
		otherEdList = new ArrayList<OtherEditionBean>();
		pubNameList = new ArrayList<PublisherNameBean>();
		pubLocList = new ArrayList<PublisherLocBean>();
		
		for (BookSolrDocument bsd : bookDocList){
			
			this.setBlurb(bsd.getBlurb());
			this.setBookId(bsd.getBookId());
			
			this.setDoi(bsd.getDoi());
			this.setEisbn(bsd.getIsbn());
			
			
			if(CrossRefUtil.isNotEmpty(bsd.getPrintDateDisplay())){
				this.setPrintDate(removeChars(bsd.getPrintDateDisplay()) + HTML_BREAK);
			}
			if(CrossRefUtil.isNotEmpty(bsd.getOnlineDateDisplay())){
				this.setOnlineDate(removeChars(bsd.getOnlineDateDisplay()) + HTML_BREAK);
			}
			this.setMainTitle(CrossRefXMLWorker.convertHtmlNameToHtmlNumber(StringEscapeUtils.escapeXml(bsd.getTitle())));
			this.setMainTitleAlphaSort(bsd.getTitleAlphasort());
			this.setSubTitle(bsd.getSubtitle());
			
			if(CrossRefUtil.isNotEmpty(bsd.getEdition())){
				if(!isDisplayEdition()) {
					setDisplayEdition(true);
				}
				this.setEdition(bsd.getEdition() + "&nbsp;- " + bsd.getEditionNumber());
			} else {
				this.setEdition(bsd.getEditionNumber());
			}
			
			for (int ctr = 0; ctr < bsd.getAuthorNameList().size(); ctr++){
				if (!isDisplayAuthor()) {
					setDisplayAuthor(true);
				}
				
				RoleAffBean authorBean = new RoleAffBean();
				authorBean.setAuthor(bsd.getAuthorNameLfList().get(ctr));
				
				if(CrossRefUtil.isNotEmpty(bsd.getAuthorRoleList().get(ctr))){
					if (!isDisplayAuthorRole()) {
						setDisplayAuthorRole(true);
					}
					authorBean.setRole(bsd.getAuthorRoleList().get(ctr));
				}
				
				if(CrossRefUtil.isNotEmpty(bsd.getAuthorAffiliationList().get(ctr))){
					authorBean.setDisplayAffliation(true);
					authorBean.setAffiliation(bsd.getAuthorAffiliationList().get(ctr));
				}
				
				if (CrossRefUtil.isNotEmpty(bsd.getAuthorNameLfList().get(ctr))) {
					if (!isDisplayAuthorAlphasort()) {
						setDisplayAuthorAlphasort(true);
					}
					authorBean.setDisplayAuthorAlphasort(true);
					authorBean.setAuthorAlphaSort(bsd.getAuthorNameLfList().get(ctr));
				}
				
				authorBean.setPosition(bsd.getAuthorPositionList().get(ctr));
				
				authorAffList.add(authorBean);
				authorMap.put(bsd.getAuthorPositionList().get(ctr), bsd.getAuthorNameList().get(ctr));
			}
			
			if (CrossRefUtil.isNotEmpty(bsd.getAltIsbnHardback())){
				if (!isDisplayHardback()) {
					setDisplayHardback(true);
				}
				this.setHardback(bsd.getAltIsbnHardback());
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getAltIsbnPaperback())){
				if (!isDisplayPaperback()) {
					setDisplayPaperback(true);
				}
				this.setPaperback(bsd.getAltIsbnPaperback());
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getAltIsbnOther())){
				if (!isDisplayOther()) {
					setDisplayOther(true);
				}
				this.setOther(bsd.getAltIsbnOther());
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getSeries())){
				
				this.setSeries(bsd.getSeries());
			}
			if(CrossRefUtil.isNotEmpty(""+bsd.getSeriesNumber())){
				setDisplaySeriesNumber(true);
				this.setSeriesNumber(""+bsd.getSeriesNumber());
			}
			if(CrossRefUtil.isNotEmpty(bsd.getSeriesAlphasort())){
				this.setSeriesAlphasort(bsd.getSeriesAlphasort());
			}
			if(CrossRefUtil.isNotEmpty(bsd.getSeriesCode())){
				setDisplaySeriesCode(true);
				this.setSeriesCode(bsd.getSeriesCode());
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getCopyrightStatementList())){
				this.setCopyrightStatement(bsd.getCopyrightStatementList().get(0) +HTML_BREAK);
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getSubjectList())){
				for(int ctr =0 ; ctr < bsd.getSubjectList().size(); ctr ++){
					SubjectBean sb = new SubjectBean();
					sb.setSubject(bsd.getSubjectList().get(ctr));
					sb.setLevel(bsd.getSubjectCodeList().get(ctr));
					sb.setCode(bsd.getSubjectCodeList().get(ctr));
					
					subjectList.add(sb);
				}
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getCoverImageFilenameList())){
				for (int ctr =0; ctr < bsd.getCoverImageFilenameList().size(); ctr ++){
					if(bsd.getCoverImageTypeList().get(ctr).equals("standard")){
						if (!isDisplayStandardImage()) {
							setDisplayStandardImage(true);
						}
					} else if (bsd.getCoverImageTypeList().get(ctr).equals("thumb")){
						if (!isDisplayThumbImage()) {
							setDisplayThumbImage(true);
						}
					}
				}
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getPublisherName())){
				PublisherNameBean pnBean = new PublisherNameBean();
				pnBean.setName(bsd.getPublisherName());
				pnBean.setOrder("0");
				pubNameList.add(pnBean);
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getPublisherLoc())){
				PublisherLocBean plBean = new PublisherLocBean();
				plBean.setLocation(bsd.getPublisherLoc());
				plBean.setOrder("0");
				pubLocList.add(plBean);
			}
			
			if(CrossRefUtil.isNotEmpty(bsd.getVolumeNumber())){
				setVolumeNumber(bsd.getVolumeNumber());
				setDisplayVolumeNumber(true);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void outSolrBean (List<BookSolrDocument> docs) {
		int counter = 0;
		for (BookSolrDocument document : docs) {
			System.out.println("***************************  "+ ++counter);
			System.out.println("getAbstractFilename = " + document.getAbstractFilename());
			System.out.println("getAbstractProblem = " + document.getAbstractProblem());
			System.out.println("getAbstractText = " + document.getAbstractText());
			System.out.println("getAltDoi = " + document.getAltDoi());
			System.out.println("getAltIsbnEisbn = " + document.getAltIsbnEisbn());
			System.out.println("getAltIsbnHardback = " + document.getAltIsbnHardback());
			System.out.println("getAltIsbnOther = " + document.getAltIsbnOther());
			System.out.println("getAltIsbnPaperback = " + document.getAltIsbnPaperback());
			System.out.println("getAuthorNameAlphasort = " + document.getAuthorNameAlphasort());
			System.out.println("getAuthorSingleline = " + document.getAuthorSingleline());
			System.out.println("getBlurb = " + document.getBlurb());
			System.out.println("getBookId = " + document.getBookId());
			System.out.println("getChapterId = " + document.getChapterId());
			System.out.println("getContentType = " + document.getContentType());
			System.out.println("getCopyrightStatementList = " + document.getCopyrightStatementList());
			System.out.println("getCoverImageFilenameList = " + document.getCoverImageFilenameList());
			System.out.println("getCoverImageTypeList = " + document.getCoverImageTypeList());
			System.out.println("getDoi = " + document.getDoi());
			System.out.println("getEdition = " + document.getEdition());
			System.out.println("getEditionNumber = " + document.getEditionNumber());
			System.out.println("getFlag = " + document.getFlag());
			System.out.println("getId = " + document.getId());
			System.out.println("getIsbn = " + document.getIsbn());
			System.out.println("getLabel = " + document.getLabel());
			System.out.println("getOnlineDate = " + document.getOnlineDate());
			System.out.println("getOnlineDateDisplay = " + document.getOnlineDateDisplay());
			System.out.println("getPageEnd = " + document.getPageEnd());
			System.out.println("getPageStart = " + document.getPageStart());
			System.out.println("getParentId = " + document.getParentId());
			System.out.println("getPartNumber = " + document.getPartNumber());
			System.out.println("getPartTitle = " + document.getPartTitle());
			System.out.println("getPdfFilename = " + document.getPdfFilename());
			System.out.println("getPosition = " + document.getPosition());
			System.out.println("getPrintDate = " + document.getPrintDate());
			System.out.println("getPrintDateDisplay = " + document.getPrintDateDisplay());
			System.out.println("getPublisherId = " + document.getPublisherId());
			System.out.println("getPublisherLoc = " + document.getPublisherLoc());
			System.out.println("getPublisherName = " + document.getPublisherName());
			System.out.println("getSeries = " + document.getSeries());
			System.out.println("getSeriesAlphasort = " + document.getSeriesAlphasort());
			System.out.println("getSeriesCode = " + document.getSeriesCode());
			System.out.println("getSeriesNumber = " + document.getSeriesNumber());
			System.out.println("getSubtitle = " + document.getSubtitle());
			System.out.println("getSubjectCodeList = " + document.getSubjectCodeList());
			System.out.println("getSubjectLevelList = " + document.getSubjectLevelList());
			System.out.println("getSubjectList = " + document.getSubjectList());
			System.out.println("getTitle = " + document.getTitle());
			System.out.println("getTitleAlphasort = " + document.getTitleAlphasort());
			System.out.println("getTocText = " + document.getTocText());
			System.out.println("getType = " + document.getType());
			System.out.println("getVolumeNumber = " + document.getVolumeNumber());
			System.out.println("getVolumeTitle = " + document.getVolumeTitle());
			System.out.println("getAuthorAffiliationList = " + document.getAuthorAffiliationList());
			System.out.println("getAuthorNameLfList = " + document.getAuthorNameLfList());
			System.out.println("getAuthorPositionList = " + document.getAuthorPositionList());
			System.out.println("getAuthorRoleList = " + document.getAuthorRoleList());
			System.out.println("getCopyrightStatementList = " + document.getCopyrightStatementList());
			System.out.println("getCoverImageFilenameList = " + document.getCoverImageFilenameList());
			System.out.println("getCoverImageTypeList = " + document.getCoverImageTypeList());
			System.out.println("getKeywordTextList = " + document.getKeywordTextList());
			System.out.println("getSubjectCodeList = " + document.getSubjectCodeList());
			System.out.println("getSubjectLevelList = " + document.getSubjectLevelList());
			System.out.println("getSubjectList = " + document.getSubjectList());
		}
	}
	
	@SuppressWarnings("unused")
	private List<Document> convertBookSolrDocumentToDocument (List<BookSolrDocument> solrDocs){
		List<Document> docs = new ArrayList<Document>();
		
		for (BookSolrDocument bsd : solrDocs){
			Document doc = new Document();
			
			if(CrossRefUtil.isNotEmpty(bsd.getEdition())){
				Field ELEMENT = new Field("ELEMENT", bsd.getEdition(), Field.Store.NO, Field.Index.NO);
			}
		}
		
		return docs;
	}

	private void populateBean(List<BookMetadataDocument> docs) throws IOException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {

		StringBuffer editor = new StringBuffer();
		StringBuffer affiliation = new StringBuffer();
		StringBuffer copyrightStatement = new StringBuffer();
		StringBuffer printDate = new StringBuffer();
		StringBuffer onlineDate = new StringBuffer();

		authorMap = new TreeMap<String, String>();
		editorMap = new TreeMap<String, String>();
		authorAffList = new ArrayList<RoleAffBean>();
		editorAffList = new ArrayList<RoleAffBean>();
		subjectList = new ArrayList<SubjectBean>();
		otherEdList = new ArrayList<OtherEditionBean>();
		pubNameList = new ArrayList<PublisherNameBean>();
		pubLocList = new ArrayList<PublisherLocBean>();
		

		for (BookMetadataDocument document : docs) {
			//search for online date
			printDate.append(document.getPrintDate());
			printDate.append(HTML_BREAK);
								
			onlineDate.append(document.getOnlineDate());
			onlineDate.append(HTML_BREAK);
			//TODO JUBS MARKER escapeXml
			setMainTitle(CrossRefXMLWorker.convertHtmlNameToHtmlNumber(document.getTitle()));
			setMainTitleAlphaSort(document.getTitleAlphasort());
			
			if(!Misc.isEmpty(document.getSubtitle())) 
			{
				setDisplaySubtitle(true);
				setSubTitle(document.getSubtitle());
			}		
			
			if(document.getEdition() != null)
			{
				setDisplayEdition(true);
				setEdition(document.getEdition() + "&nbsp;- " + document.getEditionNumber());

				if (!Misc.isEmpty(document.getEditionNumber())) 
					setEditionNumber(document.getEditionNumber());
			}
			
			List<String> authorNames = document.getAuthorNameList();
			List<String> authorAffiliations = document.getAuthorAffiliationList();
			List<String> authorAlphasorts = document.getAuthorNameAlphasort();
			List<String> authorPositions = document.getAuthorPositionList();
			List<String> authorRoles = document.getAuthorRoleList();
			if(authorNames != null)
			{
				setDisplayAuthor(true);
				for(int index=0; index<authorNames.size(); index++)
				{
					RoleAffBean authorBean = new RoleAffBean();
					authorBean.setAuthor(authorAlphasorts.get(index));
					
					if (authorRoles != null && !"none".equals(authorRoles.get(index)))
					{
						setDisplayAuthorRole(true);
						authorBean.setRole(authorRoles.get(index));
					}
					
					if (authorAffiliations != null && !"none".equals(authorAffiliations.get(index)))
					{
						authorBean.setDisplayAffliation(true);
						authorBean.setAffiliation(authorAffiliations.get(index));
					}
					
					if (authorAlphasorts != null && !"none".equals(authorAlphasorts.get(index)))
					{
						setDisplayAuthorAlphasort(true);
						authorBean.setDisplayAuthorAlphasort(true);
						authorBean.setAuthorAlphaSort(authorAlphasorts.get(index));
					}
					
					authorBean.setPosition(authorPositions.get(index));

					authorAffList.add(authorBean);
					authorMap.put(authorPositions.get(index), authorNames.get(index));
				}	
			}
			
			String eisbn = document.getIsbn();
			String hardBack = document.getAltIsbnHardback();
			String other = document.getAltIsbnOther();
			String paperBack = document.getAltIsbnPaperback();
			
			if(Misc.isNotEmpty(hardBack) || Misc.isNotEmpty(other) || Misc.isNotEmpty(paperBack))
				setDisplayAltIsbn(true);

			if(!Misc.isEmpty(paperBack)) 
			{
				setDisplayPaperback(true);
				setPaperback(paperBack);
			}
			
			if(!Misc.isEmpty(hardBack)) 
			{
				setDisplayHardback(true);
				setHardback(hardBack);
			}
			
			if(!Misc.isEmpty(other)) 
			{
				setDisplayOther(true);
				setOther(other);
			}
			
			if(!Misc.isEmpty(eisbn)) 
			{
				setEisbn(eisbn);
			}

			if(Misc.isNotEmpty(document.getSeries()))
			{
				setDisplaySeries(true);	
				setSeries(document.getSeries());
			}
			
			if(Misc.isNotEmpty(document.getSeriesNumber()))
			{
				setDisplaySeriesNumber(true);		
				setSeriesNumber(document.getSeriesNumber());
			}
			
			if(Misc.isNotEmpty(document.getSeriesPosition()))
			{
				setDisplaySeriesPosition(true);		
				setSeriesPosition(document.getSeriesPosition());
			}
			
			if(Misc.isNotEmpty(document.getSeriesAlphasort()))
			{
				setSeriesAlphasort(document.getSeriesAlphasort());
			}
			
			if(Misc.isNotEmpty(document.getSeriesCode()))
			{
				setDisplaySeriesCode(true);		
				setSeriesCode(document.getSeriesCode());
			}
			
			List<String> copyrights = document.getCopyrightStatementList();
			for(String copyright : copyrights)
			{
				copyrightStatement.append(copyright);
				copyrightStatement.append(HTML_BREAK);
			}
				

			List<String> subjects = document.getSubjectList();
			List<String> subjectCodes = document.getSubjectCodeList();
			List<String> subjectLevels = document.getSubjectLevelList();
			if(subjects != null && !subjects.isEmpty())
			{
				for(int index=0; index<subjects.size(); index++)
				{
					SubjectBean subjectBean = new SubjectBean();
					subjectBean.setSubject(subjects.get(index));
					subjectBean.setCode(subjectCodes.get(index));
					subjectBean.setLevel(subjectLevels.get(index));
					
					subjectList.add(subjectBean);
				}
			}
			
			List<String> images = document.getCoverImageFilenameList();
			List<String> types = document.getCoverImageTypeList();
			if(images != null && types != null)
			{
				for(int index=0; index<images.size(); index++)
				{
					if("standard".equals(types.get(index)))
					{
						setDisplayStandardImage(true);
						setStandardImage(images.get(index));
					}
					else
					{
						setDisplayThumbImage(true);
						setThumbImage(images.get(index));
					}
				}
			}

			PublisherNameBean pnBean = new PublisherNameBean();
			pnBean.setName(document.getPublisherName());
			pnBean.setOrder("1");
			pubNameList.add(pnBean);
		
			PublisherLocBean plBean = new PublisherLocBean();
			plBean.setLocation(document.getPublisherLoc());
			plBean.setOrder("1");
			pubLocList.add(plBean);

			if(Misc.isNotEmpty(document.getVolumeNumber()))
			{
				setVolumeNumber(document.getVolumeNumber());
				setDisplayVolumeNumber(true);
			}

			if(Misc.isNotEmpty(document.getVolumeTitle()))
			{
				setVolumeTitle(document.getVolumeTitle());
				setDisplayVolumeTitle(true);
			}
			
			if(Misc.isNotEmpty(document.getPartNumber()))
			{
				setPartNumber(document.getPartNumber());
				setDisplayPartNumber(true);
			}
			
			if(Misc.isNotEmpty(document.getPartTitle()))
			{
				setPartTitle(document.getPartTitle());
				setDisplayPartTitle(true);
			}
			
			String blurb = getBlurb(document.getIsbn());
			if(Misc.isNotEmpty(blurb))
				setBlurb(blurb);
			
			if(Misc.isNotEmpty(document.getBookGroup()))
				setBookGroup(document.getBookGroup());
			
			if(Misc.isNotEmpty(document.getDoi()))
				setDoi(document.getDoi());
			
			if(Misc.isNotEmpty(document.getPages()))
				setPages(document.getPages());
			
			
		}

		//sort collections
		Collections.sort(authorAffList);
		Collections.sort(subjectList);
		Collections.sort(otherEdList);
		Collections.sort(pubNameList);
		Collections.sort(pubLocList);
		
		setOnlineDate(onlineDate.toString());
		setPrintDate(printDate.toString());
		setEditor(editor.toString());
		setAffiliation(affiliation.toString());
		setCopyrightStatement(copyrightStatement.toString());
		
		try {
			setProductCode(DBInfo.getProductCode(eisbn));
			setSubProductCode(DBInfo.getSubProductCode(eisbn));
			setPublisherCode (DBInfo.getPublisherCode(getProductCode()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	public void setStandardImage(String standardImage) {
		if(standardImage != NO_IMAGE_IN_XML){
			this.standardImage = ImageUtil.setImagePath(standardImage, MISSING_COVER_IMAGE, getEisbn(), IMAGE_DIR_PATH, this.getClass());
		}else{
			this.standardImage = NO_COVER_IMAGE;
		}
	}
	
	
	@SuppressWarnings("unused")
	private void resetFlags() {
		setDisplayAltIsbn(false);
		setDisplayAltIsbnType(false);
		setDisplayPaperback(false);
		setDisplayHardback(false);
		setDisplayOther(false);
		setDisplayAffiliation(false);
		setDisplayAuthor(false);
		setDisplayEditor(false);
		setDisplayOtherVolume(false);
		setDisplaySeries(false);
		setDisplaySubtitle(false);
		setDisplayVolume(false);
		setDisplayVolumeNumber(false);
		setDisplayVolumeTitle(false);
		setDisplayPartNumber(false);
		setDisplayPartTitle(false);
		setDisplayStandardImage(false);
		setDisplayThumbImage(false);
		setDisplayTransTitle(false);
		setDisplayPart(false);
		setDisplayAuthorAlphasort(false);
		setDisplayEdition(false);
	}

	@SuppressWarnings("unused")
	private void resetFields() {
		setMainTitle("");
		setSubTitle("");
		setEdition("");
		setAuthor("");
		setEditor("");
		setAffiliation("");
		setDoi("");
		setPaperback("");
		setHardback("");
		setEisbn("");
		setOther("");
		setVolume("");
		setVolumeNumber("");
		setVolumeTitle("");
		setPartNumber("");
		setPartTitle("");
		setOtherVolume("");
		setSeries("");
		setPublisherName("");
		setPublisherLoc("");
		setPrintDate("");
		setOnlineDate("");
		setCopyrightStatement("");
		setSubject("");
		setBlurb("");
		setPages("");
		setBookGroup("");
		setSeriesNumber("");
		setSeriesPosition("");
		setEditionNumber("");
	}
	
	

	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId
	 *            the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the mainTitle
	 */
	public String getMainTitle() {
		return mainTitle;
	}

	/**
	 * @param mainTitle
	 *            the mainTitle to set
	 */
	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	/**
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * @param subTitle
	 *            the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * @return the edition
	 */
	public String getEdition() {
		return edition;
	}

	/**
	 * @param edition
	 *            the edition to set
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the editor
	 */
	public String getEditor() {
		return editor;
	}

	/**
	 * @param editor
	 *            the editor to set
	 */
	public void setEditor(String editor) {
		this.editor = editor;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi
	 *            the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the volume
	 */
	public String getVolume() {
		return volume;
	}

	/**
	 * @param volumeNumber
	 *            the volumeNumber to set
	 */
	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}

	/**
	 * @return the volumeNummber
	 */
	public String getVolumeNumber() {
		return volumeNumber;
	}

	/**
	 * @param volumeTitle
	 *            the volumeTitle to set
	 */
	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	/**
	 * @return the volumeTitle
	 */
	public String getVolumeTitle() {
		return volumeTitle;
	}
	
	/**
	 * @param partNumber
	 *            the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partTitle
	 *            the partTitle to set
	 */
	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}

	/**
	 * @return the partTitle
	 */
	public String getPartTitle() {
		return partTitle;
	}
	
	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	/**
	 * @return the otherVolume
	 */
	public String getOtherVolume() {
		return otherVolume;
	}

	/**
	 * @param otherVolume
	 *            the otherVolume to set
	 */
	public void setOtherVolume(String otherVolume) {
		this.otherVolume = otherVolume;
	}

	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}

	/**
	 * @param series
	 *            the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}

	/**
	 * @return the publisherName
	 */
	public String getPublisherName() {
		return publisherName;
	}

	/**
	 * @param publisherName
	 *            the publisherName to set
	 */
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	/**
	 * @return the publisherLoc
	 */
	public String getPublisherLoc() {
		return publisherLoc;
	}

	/**
	 * @param publisherLoc
	 *            the publisherLoc to set
	 */
	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}

	/**
	 * @return the printDate
	 */
	public String getPrintDate() {
		return printDate;
	}

	/**
	 * @param printDate
	 *            the printDate to set
	 */
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	/**
	 * @return the copyrightStatement
	 */
	public String getCopyrightStatement() {
		return copyrightStatement;
	}

	/**
	 * @param copyrightStatement
	 *            the copyrightStatement to set
	 */
	public void setCopyrightStatement(String copyrightStatement) {
		this.copyrightStatement = copyrightStatement;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the blurb
	 */
	public String getBlurb() {
		return blurb;
	}

	/**
	 * @param blurb
	 *            the blurb to set
	 */
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}

	/**
	 * @return the bookGroup
	 */
	public String getBookGroup() {
		return bookGroup;
	}

	/**
	 * @param bookGroup
	 *            the bookGroup to set
	 */
	public void setBookGroup(String bookGroup) {
		if(bookGroup != null){
			if(bookGroup.length() > 2 ){
				this.bookGroup = bookGroup.substring(0, 1).toUpperCase()
					+ bookGroup.substring(1);
			}else if(bookGroup.length() == 1){
				this.bookGroup = bookGroup.toUpperCase();
			}else{
				this.bookGroup = bookGroup;
			}
		}else{
			this.bookGroup = bookGroup;
		}
	}

	/**
	 * @return the pages
	 */
	public String getPages() {
		return pages;
	}

	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setPages(String pages) {
		this.pages = pages;
	}

	/**
	 * @return the displayAuthor
	 */
	public boolean isDisplayAuthor() {
		return displayAuthor;
	}

	/**
	 * @param displayAuthor
	 *            the displayAuthor to set
	 */
	public void setDisplayAuthor(boolean displayAuthor) {
		this.displayAuthor = displayAuthor;
	}

	/**
	 * @return the displayEditor
	 */
	public boolean isDisplayEditor() {
		return displayEditor;
	}

	/**
	 * @param displayEditor
	 *            the displayEditor to set
	 */
	public void setDisplayEditor(boolean displayEditor) {
		this.displayEditor = displayEditor;
	}

	/**
	 * @return the onlineDate
	 */
	public String getOnlineDate() {
		return onlineDate;
	}

	/**
	 * @param onlineDate
	 *            the onlineDate to set
	 */
	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	/**
	 * @return the displaySubtitle
	 */
	public boolean isDisplaySubtitle() {
		return displaySubtitle;
	}

	/**
	 * @param displaySubtitle
	 *            the displaySubtitle to set
	 */
	public void setDisplaySubtitle(boolean displaySubtitle) {
		this.displaySubtitle = displaySubtitle;
	}

	/**
	 * @return the displayAltIsbn
	 */
	public boolean isDisplayAltIsbn() {
		return displayAltIsbn;
	}

	/**
	 * @param displayAltIsbn
	 *            the displayAltIsbn to set
	 */
	public void setDisplayAltIsbn(boolean displayAltIsbn) {
		this.displayAltIsbn = displayAltIsbn;
	}

	/**
	 * @return the displayVolume
	 */
	public boolean isDisplayVolume() {
		return displayVolume;
	}

	/**
	 * @param displayVolume
	 *            the displayVolume to set
	 */
	public void setDisplayVolume(boolean displayVolume) {
		this.displayVolume = displayVolume;
	}
	
	/**
	 * @return the displayVolumeNumber
	 */
	public boolean isDisplayVolumeNumber() {
		return displayVolumeNumber;
	}

	/**
	 * @param displayVolumeNumber
	 *            the displayVolumeNumber to set
	 */
	public void setDisplayVolumeNumber(boolean displayVolumeNumber) {
		this.displayVolumeNumber = displayVolumeNumber;
	}
	
	/**
	 * @return the displayVolumeTitle
	 */
	public boolean isDisplayVolumeTitle() {
		return displayVolumeTitle;
	}

	/**
	 * @param displayVolumeTitle
	 *            the displayVolumeTitle to set
	 */
	public void setDisplayVolumeTitle(boolean displayVolumeTitle) {
		this.displayVolumeTitle = displayVolumeTitle;
	}

	/**
	 * @return the displayPartNumber
	 */
	public boolean isDisplayPartNumber() {
		return displayPartNumber;
	}

	/**
	 * @param displayPartNumber
	 *            the displayPartNumber to set
	 */
	public void setDisplayPartNumber(boolean displayPartNumber) {
		this.displayPartNumber = displayPartNumber;
	}
	
	/**
	 * @return the displayPartTitle
	 */
	public boolean isDisplayPartTitle() {
		return displayPartTitle;
	}

	/**
	 * @param displayPartTitle
	 *            the displayPartTitle to set
	 */
	public void setDisplayPartTitle(boolean displayPartTitle) {
		this.displayPartTitle = displayPartTitle;
	}
	
	/**
	 * @return the displayOtherVolume
	 */
	public boolean isDisplayOtherVolume() {
		return displayOtherVolume;
	}

	/**
	 * @param displayOtherVolume
	 *            the displayOtherVolume to set
	 */
	public void setDisplayOtherVolume(boolean displayOtherVolume) {
		this.displayOtherVolume = displayOtherVolume;
	}

	/**
	 * @return the displaySeries
	 */
	public boolean isDisplaySeries() {
		return displaySeries;
	}

	/**
	 * @param displaySeries
	 *            the displaySeries to set
	 */
	public void setDisplaySeries(boolean displaySeries) {
		this.displaySeries = displaySeries;
	}

	/**
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation
	 *            the affiliation to set
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * @return the displayAffiliation
	 */
	public boolean isDisplayAffiliation() {
		return displayAffiliation;
	}

	/**
	 * @param displayAffiliation
	 *            the displayAffiliation to set
	 */
	public void setDisplayAffiliation(boolean displayAffiliation) {
		this.displayAffiliation = displayAffiliation;
	}

	/**
	 * @return the displayAltIsbnType
	 */
	public boolean isDisplayAltIsbnType() {
		return displayAltIsbnType;
	}

	/**
	 * @param displayAltIsbnType
	 *            the displayAltIsbnType to set
	 */
	public void setDisplayAltIsbnType(boolean displayAltIsbnType) {
		this.displayAltIsbnType = displayAltIsbnType;
	}

	/**
	 * @return the paperback
	 */
	public String getPaperback() {
		return paperback;
	}

	/**
	 * @param paperback
	 *            the paperback to set
	 */
	public void setPaperback(String paperback) {
		this.paperback = paperback;
	}

	/**
	 * @return the hardback
	 */
	public String getHardback() {
		return hardback;
	}

	/**
	 * @param hardback
	 *            the hardback to set
	 */
	public void setHardback(String hardback) {
		this.hardback = hardback;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other
	 *            the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * @return the displayPaperback
	 */
	public boolean isDisplayPaperback() {
		return displayPaperback;
	}

	/**
	 * @param displayPaperback
	 *            the displayPaperback to set
	 */
	public void setDisplayPaperback(boolean displayPaperback) {
		this.displayPaperback = displayPaperback;
	}

	/**
	 * @return the displayHardback
	 */
	public boolean isDisplayHardback() {
		return displayHardback;
	}

	/**
	 * @param displayHardback
	 *            the displayHardback to set
	 */
	public void setDisplayHardback(boolean displayHardback) {
		this.displayHardback = displayHardback;
	}

	/**
	 * @return the displayOther
	 */
	public boolean isDisplayOther() {
		return displayOther;
	}

	/**
	 * @param displayOther
	 *            the displayOther to set
	 */
	public void setDisplayOther(boolean displayOther) {
		this.displayOther = displayOther;
	}

	/**
	 * @return the eisbn
	 */
	public String getEisbn() {
		return eisbn;
	}

	/**
	 * @param eisbn
	 *            the eisbn to set
	 */
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	/**
	 * @return the standardImage
	 */
	public String getStandardImage() {
		return standardImage;
	}

	
	/**
	 * @return the thumbImage
	 */
	public String getThumbImage() {
		return thumbImage;
	}

	
	/**
	 * @return the displayStandardImage
	 */
	public boolean isDisplayStandardImage() {
		return displayStandardImage;
	}

	/**
	 * @param displayStandardImage
	 *            the displayStandardImage to set
	 */
	public void setDisplayStandardImage(boolean displayStandardImage) {
		this.displayStandardImage = displayStandardImage;
	}

	/**
	 * @return the displayThumbImage
	 */
	public boolean isDisplayThumbImage() {
		return displayThumbImage;
	}

	/**
	 * @param displayThumbImage
	 *            the displayThumbImage to set
	 */
	public void setDisplayThumbImage(boolean displayThumbImage) {
		this.displayThumbImage = displayThumbImage;
	}

	/**
	 * @return the displayEdition
	 */
	public boolean isDisplayEdition() {
		return displayEdition;
	}

	/**
	 * @param displayEdition
	 *            the displayEdition to set
	 */
	public void setDisplayEdition(boolean displayEdition) {
		this.displayEdition = displayEdition;
	}

	public TreeMap<String, String> getAuthorMap() {
		return authorMap;
	}

	public TreeMap<String, String> getEditorMap() {
		return editorMap;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getSeriesPosition() {
		return seriesPosition;
	}

	public void setSeriesPosition(String seriesPosition) {
		this.seriesPosition = seriesPosition;
	}


	/**
	 * @return the mainTitleAlphaSort
	 */
	public String getMainTitleAlphaSort() {
		return mainTitleAlphaSort;
	}

	/**
	 * @param mainTitleAlphaSort
	 *            the mainTitleAlphaSort to set
	 */
	public void setMainTitleAlphaSort(String mainTitleAlphaSort) {
		this.mainTitleAlphaSort = mainTitleAlphaSort;
	}

	/**
	 * @return AuthorAffList
	 */
	public ArrayList<RoleAffBean> getAuthorAffList() {
		return authorAffList;
	}

	public void setAuthorAffList(ArrayList<RoleAffBean> authorAffList) {
		this.authorAffList = authorAffList;
	}

	public ArrayList<RoleAffBean> getEditorAffList() {
		return editorAffList;
	}

	public void setEditorAffList(ArrayList<RoleAffBean> editorAffList) {
		this.editorAffList = editorAffList;
	}

	public String getSeriesAlphasort() {
		return seriesAlphasort;
	}

	public void setSeriesAlphasort(String seriesAlphasort) {
		this.seriesAlphasort = seriesAlphasort;
	}

	public String getSeriesPart() {
		return seriesPart;
	}

	public void setSeriesPart(String seriesPart) {
		this.seriesPart = seriesPart;
	}

	public boolean isDisplayTransTitle() {
		return displayTransTitle;
	}

	public void setDisplayTransTitle(boolean displayTransTitle) {
		this.displayTransTitle = displayTransTitle;
	}

	public boolean isDisplayPart() {
		return displayPart;
	}

	public void setDisplayPart(boolean displayPart) {
		this.displayPart = displayPart;
	}

	public String getTransTitle() {
		return transTitle;
	}

	public void setTransTitle(String transTitle) {
		this.transTitle = transTitle;
	}
	

	public boolean isDisplayAuthorAlphasort() {
		return displayAuthorAlphasort;
	}

	public void setDisplayAuthorAlphasort(boolean displayAuthorAlphasort) {
		this.displayAuthorAlphasort = displayAuthorAlphasort;
	}
	public boolean isDisplaySeriesPart() {
		return displaySeriesPart;
	}

	public void setDisplaySeriesPart(boolean displaySeriesPart) {
		this.displaySeriesPart = displaySeriesPart;
	}

	public boolean isDisplaySeriesPosition() {
		return displaySeriesPosition;
	}

	public void setDisplaySeriesPosition(boolean displaySeriesPosition) {
		this.displaySeriesPosition = displaySeriesPosition;
	}

	public boolean isDisplaySeriesNumber() {
		return displaySeriesNumber;
	}

	public void setDisplaySeriesNumber(boolean displaySeriesNumber) {
		this.displaySeriesNumber = displaySeriesNumber;
	}
	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	public boolean isDisplaySeriesCode() {
		return displaySeriesCode;
	}

	public void setDisplaySeriesCode(boolean displaySeriesCode) {
		this.displaySeriesCode = displaySeriesCode;
	}
	
	public String getSubjectLevel() {
		return subjectLevel;
	}

	public void setSubjectLevel(String subjectLevel) {
		this.subjectLevel = subjectLevel;
	}

	public boolean isDisplayAuthorRole() {
		return displayAuthorRole;
	}

	public void setDisplayAuthorRole(boolean displayAuthorRole) {
		this.displayAuthorRole = displayAuthorRole;
	}
	
	public ArrayList<SubjectBean> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(ArrayList<SubjectBean> subjectList) {
		this.subjectList = subjectList;
	}
	
	public ArrayList<OtherEditionBean> getOtherEdList() {
		return otherEdList;
	}

	public void setOtherEdList(ArrayList<OtherEditionBean> otherEdList) {
		this.otherEdList = otherEdList;
	}
	
	public ArrayList<PublisherNameBean> getPubNameList() {
		return pubNameList;
	}

	public void setPubNameList(ArrayList<PublisherNameBean> pubNameList) {
		this.pubNameList = pubNameList;
	}

	public ArrayList<PublisherLocBean> getPubLocList() {
		return pubLocList;
	}

	public void setPubLocList(ArrayList<PublisherLocBean> pubLocList) {
		this.pubLocList = pubLocList;
	}
	
	public void setThumbImage(String thumbImage) {
		if(thumbImage != NO_IMAGE_IN_XML){
			this.thumbImage = ImageUtil.setImagePath(thumbImage, MISSING_COVER_THUMBNAIL, getEisbn(), IMAGE_DIR_PATH, this.getClass());
		}else{
			this.thumbImage = NO_COVER_THUMBNAIL;
		}
		
	}
	
	private String getBlurb(String isbn){
		String result = "";
		final File blurbDir = new File(IsbnContentDirUtil.getPath(isbn) + isbn + "_blurb.txt" );
		
		try
		{
			StringBuilder sb = new StringBuilder();
			if(blurbDir != null && blurbDir.exists())
			{
				String temp = null;
				FileReader reader = new FileReader(blurbDir);
				BufferedReader breader = new BufferedReader(reader);
				while((temp = breader.readLine()) != null)
					sb.append(temp);
		
				result = sb.toString();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return result;
	}

	public String getPublisherCode() {
		return publisherCode;
	}

	public void setPublisherCode(String publisherCode) {
		this.publisherCode = publisherCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public String getSubProductCode() {
		return subProductCode;
	}

	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}	
}

