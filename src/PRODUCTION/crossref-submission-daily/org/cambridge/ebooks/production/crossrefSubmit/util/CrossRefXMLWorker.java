package org.cambridge.ebooks.production.crossrefSubmit.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean;
import org.cambridge.ebooks.production.crossref.ejb.document.RoleAffBean;
import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefProperties;
import org.cambridge.ebooks.production.crossref.ejb.util.EBooksConfiguration;
import org.cambridge.ebooks.production.crossref.ejb.util.EBooksProperties;
import org.cambridge.ebooks.production.crossref.ejb.xml.Body;
import org.cambridge.ebooks.production.crossref.ejb.xml.Book;
import org.cambridge.ebooks.production.crossref.ejb.xml.BookMetadata;
import org.cambridge.ebooks.production.crossref.ejb.xml.ContentItem;
import org.cambridge.ebooks.production.crossref.ejb.xml.Depositor;
import org.cambridge.ebooks.production.crossref.ejb.xml.DoiBatch;
import org.cambridge.ebooks.production.crossref.ejb.xml.DoiData;
import org.cambridge.ebooks.production.crossref.ejb.xml.Head;
import org.cambridge.ebooks.production.crossref.ejb.xml.Pages;
import org.cambridge.ebooks.production.crossref.ejb.xml.PersonName;
import org.cambridge.ebooks.production.crossref.ejb.xml.PublicationDate;
import org.cambridge.ebooks.production.crossref.ejb.xml.Publisher;
import org.cambridge.ebooks.production.crossref.ejb.xml.Root;
import org.cambridge.ebooks.production.crossref.ejb.xml.Xml;
import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.HeadBean;
import org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc.DBInfo;
import org.cambridge.ebooks.production.util.StringUtil;
import org.cambridge.util.Misc;


/**
 * 
 * @author rvillamor
 * 
 */

public class CrossRefXMLWorker implements CrossRefProperties{
	
	private class NameParser {
		private String firstName;
		private String lastName;
		public NameParser(String fullName) {
			String names[] = fullName.split(",\\s");
			lastName = names[0];
			firstName = names.length>1 ? names[1] : null;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
	}
	
	public static final String CCO_RESOURCE_URL = "cco.resource.url";
	public static final String CHO_RESOURCE_URL = "cho.resource.url";
	public static final String SSO_RESOURCE_URL = "sso.resource.url";
	public static final String CLR_RESOURCE_URL = "clr.resource.url";
	
		
	private static final String CAMBRIDGE_REGISTRANT = "Cambridge University Press";
	private static final String LANGUAGE_DEFAULT = "en";
	
	private static final String BOOK_TYPE_MONOGRAPH = "monograph";
	
	private static final String SEQUENCE_FIRST = "first";
	private static final String SEQUENCE_ADDITIONAL = "additional";
	
	private static final String MEDIA_TYPE_PRINT = "print";
	private static final String MEDIA_TYPE_ONLINE = "online";
	
	private static final String CONTRIBUTOR_ROLE_AUTHOR = "author";
	private static final String CONTRIBUTOR_ROLE_EDITOR = "editor";
	
	private static final String COMPONENT_TYPE_CHAPTER = "chapter";
	private static final String PUBLICATION_TYPE_FULL_TEXT = "full_text";
	
	public static Root getRoot(
			String username,
			int doiBatchId,
			Date date,
			List<EBookContentBean> ebookContentList,
			EbookDetailsBean ebookDetails,
			String contentId) {
		return new Root(new Xml(XML_VERSION, XML_ENCODING), getDoiBatch(username, doiBatchId, date, ebookContentList, ebookDetails, null));
	}
	
	public static DoiBatch getDoiBatch(
			String username,
			int doiBatchId,
			Date date,
			List<EBookContentBean> ebookContentList,
			EbookDetailsBean ebookDetails,
			String contentId) {
		return new DoiBatch(
				DOIBATCH_VERSION,
				DOIBATCH_XMLNS,
				DOIBATCH_XMLNSXSI,
				DOIBATCH_XSISCHEMALOC,
				getHead(username, doiBatchId, date, ebookDetails.getEisbn()),
				getBody(ebookContentList, ebookDetails, contentId)
				);
	}
	
	public static Head getHead(String username, int doiBatchId, Date date, String isbn) {
		HeadBean hb = new HeadBean();
		
		try {
			hb = DBInfo.getPublisherEmail(isbn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Head head = new Head(
				String.valueOf(doiBatchId),
				date,
				new Depositor(username, DEPOSITOR_EMAIL),
				convertAmp(hb.getPublisherName()) 
				);
		return head;
	}
	
	public static String convertAmp(String publisher){
		return publisher.replace("&", "&amp;");
	}
	
	public static Body getBody(List<EBookContentBean> ebookContentList, EbookDetailsBean ebookDetails, String contentId) {
		ArrayList<ContentItem> contentItems = null;
		contentItems = getContentItems(ebookContentList, ebookDetails);
		Body body = new Body(
			new Book(
					getBookType(ebookDetails),
					getBookMetadata(ebookDetails), 
					contentItems
					)
			);
		return body;
	}
	
	public static String getBookType(EbookDetailsBean ebookDetails){
		System.out.println(CrossRefXMLWorker.class +  " getBookType() start");
		String bookType=null;
		String role = null;
		
		if(ebookDetails.getAuthorAffList() != null && ebookDetails.getAuthorAffList().size() > 0){
			role = ebookDetails.getAuthorAffList().get(0).getRole();
			System.out.println(CrossRefXMLWorker.class +  " role: "+role);
			if(role != null){
				if(role.equals("GE") || role.equals("AE") || role.equals("ED") || 
					role.equalsIgnoreCase("Editor") || role.equalsIgnoreCase("General Editor") || 
					role.equalsIgnoreCase("Associate Editor") || role.equalsIgnoreCase("Edited by")){
					bookType = "edited_book";
				}else{
					bookType = "monograph";
				}
			}
		}
		
		System.out.println(CrossRefXMLWorker.class + " getBookType() end: "+bookType);
		return bookType;
	}
	
	public static BookMetadata getBookMetadata(EbookDetailsBean ebookDetails) {
		String resourceUrl="";
		String pubName="";
		String pubLoc="";
		
		if (ebookDetails.getProductCode().equals("CCO") || ebookDetails.getProductCode().equals("CHO") || ebookDetails.getProductCode().equals("SSO")){
			resourceUrl = getResourceUrlMigration(ebookDetails.getProductCode(), ebookDetails.getBookId());
		} else if ("CLR".equals(ebookDetails.getSubProductCode())) {
			resourceUrl = getResourceUrlMigration(ebookDetails.getSubProductCode(), ebookDetails.getBookId());
		} else if (ebookDetails.getPublisherCode().equals("CUP")){
			resourceUrl = getResourceUrlCup(ebookDetails.getBookId());
		} else {
			resourceUrl = getResourceUrlUpo(ebookDetails.getPublisherCode(), ebookDetails.getProductCode(), ebookDetails.getBookId());
		}
		
		try{
			pubName = ebookDetails.getPubNameList().get(0).getName();
			pubLoc = ebookDetails.getPubLocList().get(0).getLocation();
		} catch (IndexOutOfBoundsException e ){
			pubName = "";
			pubLoc = "";
		}
		
		BookMetadata bookMetadata = new BookMetadata(
				LANGUAGE_DEFAULT,
				getContributors(ebookDetails),
				getTitleAsList(ebookDetails.getMainTitle()),
				ebookDetails.getSubTitle(),
				null,//TODO: -C2- Series Metadata
				ebookDetails.getVolumeNumber(),
				ebookDetails.getEditionNumber(),
				new PublicationDate(MEDIA_TYPE_PRINT, null, null, ebookDetails.getPrintDate().replaceAll(EbookDetailsBean.HTML_BREAK, "")),
				new PublicationDate(MEDIA_TYPE_ONLINE, null, null, ebookDetails.getOnlineDate().replaceAll(EbookDetailsBean.HTML_BREAK, "")),
				ebookDetails.getEisbn(),
				ebookDetails.getHardback(),
				ebookDetails.getPaperback(),
				new Publisher(pubName, pubLoc),
				new DoiData(ebookDetails.getDoi(), resourceUrl) // resource URL
				);
		return bookMetadata;
	}
	
	public static ArrayList<PersonName> getContributors(EbookDetailsBean ebookDetails) {
		ArrayList<PersonName> contributors = new ArrayList<PersonName>();
		ArrayList<RoleAffBean> roleAff = new ArrayList<RoleAffBean>();
		roleAff = ebookDetails.getAuthorAffList();
		
		for (RoleAffBean author : roleAff) {
			NameParser np = new CrossRefXMLWorker().new NameParser(author.getAuthor());
			contributors.add(
				new PersonName(
					Integer.valueOf(author.getPosition())==1 ? SEQUENCE_FIRST : SEQUENCE_ADDITIONAL,
					getContributorRole(author.getRole()),
					np.getFirstName() == null ? null : np.getFirstName().trim(),
					np.getLastName() == null ? null : np.getLastName().trim()
					)
			);
		}
		
		for (String position : ebookDetails.getEditorMap().keySet()) {
			NameParser np = new CrossRefXMLWorker().new NameParser(ebookDetails.getEditorMap().get(position));
			contributors.add(
				new PersonName(
					Integer.valueOf(position)==1 ? SEQUENCE_FIRST : SEQUENCE_ADDITIONAL,
					CONTRIBUTOR_ROLE_EDITOR,
					np.getFirstName() == null ? null : np.getFirstName().trim(),
					np.getLastName() == null ? null : np.getLastName().trim()
					)
			);
		}
		
		return contributors;
	}
	
	public static ArrayList<PersonName> getChapterContributors(
									ArrayList<ContributorBean> chapterContributors,
									EbookDetailsBean ebookDetails) {
		
		boolean hasChapterContributors = false;
		
		ArrayList<PersonName> contributors = new ArrayList<PersonName>();
		for(ContributorBean cb : chapterContributors){
			hasChapterContributors = true;
			
			NameParser np = new CrossRefXMLWorker().new NameParser(cb.getContributor());
			contributors.add(
					new PersonName(
						Integer.valueOf(cb.getPosition())==1 ? SEQUENCE_FIRST : SEQUENCE_ADDITIONAL,
						CONTRIBUTOR_ROLE_AUTHOR,
						np.getFirstName() == null ? null : np.getFirstName().trim(),
						np.getLastName() == null ? null : np.getLastName().trim()
						)
				);
		}
		
		ArrayList<RoleAffBean> roleAff = new ArrayList<RoleAffBean>();
		roleAff = ebookDetails.getAuthorAffList();
		
		boolean hasAuthors = false;
		
		for (RoleAffBean author : roleAff) {
			hasAuthors  = true;
			NameParser np = new CrossRefXMLWorker().new NameParser(author.getAuthor());
			contributors.add(
				new PersonName(
					hasChapterContributors ? SEQUENCE_ADDITIONAL : 
						Integer.valueOf(author.getPosition())==1 ? SEQUENCE_FIRST : SEQUENCE_ADDITIONAL,
					getContributorRole(author.getRole()),
					np.getFirstName() == null ? null : np.getFirstName().trim(),
					np.getLastName() == null ? null : np.getLastName().trim() 
					)
			);
		}
		
		for (String position : ebookDetails.getEditorMap().keySet()) {
			NameParser np = new CrossRefXMLWorker().new NameParser(ebookDetails.getEditorMap().get(position));
			contributors.add(
				new PersonName(
					hasChapterContributors || hasAuthors ? SEQUENCE_ADDITIONAL : Integer.valueOf(position)==1 ? SEQUENCE_FIRST : SEQUENCE_ADDITIONAL,
					CONTRIBUTOR_ROLE_EDITOR,
					np.getFirstName() == null ? null : np.getFirstName().trim(),
					np.getLastName() == null ? null : np.getLastName().trim()
					)
			);
		}
		
		return contributors;
	}
	
	public static String getPosition(){
		
		return null;
	}
	
	public static String getContributorRole(String role){		
		if(role != null){
			if(role.equals("GE") || role.equals("AE") || role.equals("ED") ||
				role.equalsIgnoreCase("Editor") || role.equalsIgnoreCase("General Editor") || 
				role.equalsIgnoreCase("Associate Editor") || role.equalsIgnoreCase("Edited By")){
				role = "editor";
			}else{
				role = "author";
			}
		}
		
		return role;
	}
	
	@Deprecated //not used anymore in favor of getContentItems() (not included in 20130131 emergency release yet)
	public static ArrayList<ContentItem> getContentItem(List<EBookContentBean> ebookContentList, EbookDetailsBean ebookDetails, String contentId) {
		ArrayList<ContentItem> contentItem = new ArrayList<ContentItem>();
		
		EBookContentBean contentToInsert = new EBookContentBean(contentId);
		
		List<EBookContentBean> findList = ebookContentList;
		for (EBookContentBean content : findList) {
			String resourceUrl="";
			if (ebookDetails.getProductCode().equals("CCO") || ebookDetails.getProductCode().equals("CHO") || ebookDetails.getProductCode().equals("SSO")){
				resourceUrl = getResourceUrlMigration(ebookDetails.getProductCode(), content.getContentId());
			} else if (ebookDetails.getPublisherCode().equals("CUP")){
				resourceUrl = getResourceUrlCup(content.getContentId());
			} else  {
				resourceUrl = getResourceUrlUpo(ebookDetails.getPublisherCode(), ebookDetails.getProductCode(), content.getContentId());
			}
			if (content.equals(contentToInsert)) {
				contentItem.add(
						new ContentItem(
							COMPONENT_TYPE_CHAPTER,
							null,
							PUBLICATION_TYPE_FULL_TEXT,
							getChapterContributors(content.getChapterContributors(), ebookDetails),
							getTitleAsList(content.getTitle()),
							null,
							new Pages(content.getPageStart(), content.getPageEnd()),
							null,
							new DoiData(content.getDoi(), resourceUrl) //resource URL
							)
					);
				break;
			}
		} 
		return contentItem;
	}
	
	public static ArrayList<ContentItem> getContentItems(List<EBookContentBean> ebookContentList, EbookDetailsBean ebookDetails) {
		ArrayList<ContentItem> contentItems = new ArrayList<ContentItem>();
		
		for (EBookContentBean content : ebookContentList) {
			String resourceUrl="";
			if (ebookDetails.getProductCode().equals("CCO") || ebookDetails.getProductCode().equals("CHO") || ebookDetails.getProductCode().equals("SSO")){
				resourceUrl = getResourceUrlMigration(ebookDetails.getProductCode(), content.getContentId());
			} else if ("CLR".equals(ebookDetails.getSubProductCode())) {
				resourceUrl = getResourceUrlMigration(ebookDetails.getSubProductCode(), content.getContentId());
			} else if (ebookDetails.getPublisherCode().equals("CUP")){
				resourceUrl = getResourceUrlCup(content.getContentId());
			} else  {
				resourceUrl = getResourceUrlUpo(ebookDetails.getPublisherCode(), ebookDetails.getProductCode(), content.getContentId());
			}
			contentItems.add(
			new ContentItem(
				COMPONENT_TYPE_CHAPTER,
				null,
				PUBLICATION_TYPE_FULL_TEXT,
				getChapterContributors(content.getChapterContributors(), ebookDetails),
				getTitleAsList(formatTitle(content.getTitle(), content.getLabel())),
				content.getLabel(),
				new Pages(content.getPageStart(), content.getPageEnd()),
				null,
				new DoiData(content.getDoi(), resourceUrl) //resource URL
				)
			);
		}
		return contentItems;
	}
	
	private static String formatTitle(String title, String label){
		title = removeLabelFromTitle(title, label); // remove label
		title = StringUtil.stripHTMLTags(title);	// strip html tags
		title = convertHtmlNameToHtmlNumber(title);
		return title;
	}
	
	public static String convertHtmlNameToHtmlNumber(String str){
		str = str.replaceAll("&quot;", "&#34;");
		str = str.replaceAll("&amp;", "&#38;");
		str = str.replaceAll("&apos;", "&#39;");
		str = str.replaceAll("&lt;", "&#60;");
		str = str.replaceAll("&gt;", "&#62;");
		
		return str;
	}
	
	private static String getQueryStringContentDetails(String contentId) {
		StringBuffer queryString = new StringBuffer("PARENT_ID:" + contentId);
		queryString
		.append(" AND (PARENT:content-item OR PARENT:heading OR PARENT:contributor-group)");

		return queryString.toString();
	}
	
	public static String removeLabelFromTitle(String title, String label){
		return title.replaceFirst(label+": ", "");
	}
	
	public static ArrayList<String> getTitleAsList(String title) {
		ArrayList<String> titles = new ArrayList<String>();
		titles.add(title);
		return titles;
	}
	
	public static String getResourceUrlCup(String doi) {
		return Misc.isNotEmpty(doi) ? EBooksConfiguration.getProperty(EBooksProperties.EBOOKS_RESOURCE_URL).concat(doi) : null;
	}
	
	public static String getResourceUrlMigration(String productCode, String doi) {
		String cupUrl = "";
		if (Misc.isNotEmpty(doi)){
			String url = "";
			if(productCode.equals("CCO")) {
				url = EBooksConfiguration.getProperty(CCO_RESOURCE_URL);
			} else if(productCode.equals("CHO")) {
				url = EBooksConfiguration.getProperty(CHO_RESOURCE_URL);
			} else if(productCode.equals("SSO")) {
				url = EBooksConfiguration.getProperty(SSO_RESOURCE_URL);
			} else if(productCode.equals("CLR")) {
				url = EBooksConfiguration.getProperty(CLR_RESOURCE_URL);
			}
			
			cupUrl = url + doi;
		}
		
		
		return cupUrl;
	}
	
	public static String getResourceUrlUpo(String pubCode, String prodCode, String doi) {
		String pubPrefix ="";
		try {
			pubPrefix = DBInfo.getPublisherPrefix(pubCode.toUpperCase());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Misc.isNotEmpty(doi) ? EBooksConfiguration.getProperty(EBooksProperties.UPO_RESOURCE_URL).
				concat(pubPrefix.toLowerCase()).concat("/").concat(doi) : null;
	}
}
