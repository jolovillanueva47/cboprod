package org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.crossref.ejb.jpa.EBookContent;
import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.CrossrefInfoBean;
import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.HeadBean;
import org.cambridge.ebooks.production.crossrefSubmit.util.CrossRefUtil;
import org.cambridge.ebooks.production.util.StringUtil;

public class DBInfo {
	
	public static Connection conn;
	
	public static void startConnection(){
		System.out.println("Opening DB connection..");
		conn = JDBCUtil.getConnection();
	}
	
	public static void endConnection() throws SQLException {
		if (conn != null && !conn.isClosed()) {
			conn.close();
			System.out.println("Successfully closed connection.");
		}
	}
	
	public static List<CrossrefInfoBean> getObjectList() throws SQLException, IOException{
		PreparedStatement st = null;
		ResultSet rs = null;
		List<CrossrefInfoBean> liRein = new ArrayList<CrossrefInfoBean>();
		String lastProcessDate = CrossRefUtil.readFile();

		try{
			//NOTE: lastProcessDate takes the date from System.getProperty("crossref.app.dir") +"lastSuccessful.txt"
			StringBuilder queryBuilder =  new StringBuilder();
			queryBuilder.append("select eidl.isbn, eidl.online_flag,  ateb.audit_date as modified_date, eb.book_id ");
			queryBuilder.append(" ,eidl.cbo_product_group , eidl.sub_product_group_code ");
			queryBuilder.append("FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, ");
			queryBuilder.append("	(select null book_id, eisbn isbn, status, max(audit_date) audit_date ");
			queryBuilder.append("		from oraebooks.audit_trail_ebooks ");
			queryBuilder.append("		where status = 7 GROUP BY null, eisbn, status) ateb  ");
			queryBuilder.append("WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7'   ");
			queryBuilder.append("	and (trunc(online_flag_change_date) > trunc(to_date( ? ))");
			queryBuilder.append("		or trunc(esample_change_date) > trunc(to_date( ? ))");
			queryBuilder.append("		or trunc(ateb.audit_date) > trunc(to_date( ? ))");
			queryBuilder.append("		or (trunc(eidl.PUB_DATE_ONLINE) > CURRENT_DATE -2 and (trunc(eidl.PUB_DATE_ONLINE) < CURRENT_DATE -1))");
			queryBuilder.append("	) ");
			queryBuilder.append("ORDER BY eidl.isbn");
			
			st = conn.prepareStatement(queryBuilder.toString());
			st.setString(1, lastProcessDate);
			st.setString(2, lastProcessDate);
			st.setString(3, lastProcessDate);
			
			rs = st.executeQuery();
			while(rs.next()){
				String isbn = rs.getString("isbn");
				String onlineFlag = rs.getString("online_flag");
				Date lastModified = rs.getDate("modified_date");
				String bookId = rs.getString("book_id");
				String product = rs.getString("cbo_product_group");
				String subProduct = rs.getString("sub_product_group_code");
				java.sql.Date _lastModfied = null;
				if(lastModified != null){
					_lastModfied = new java.sql.Date(lastModified.getTime());
				}
				CrossrefInfoBean rein = new CrossrefInfoBean(isbn, onlineFlag, _lastModfied, bookId, product, subProduct); 
				liRein.add(rein);		
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			st.close();
		}

		return liRein;		
	}
	
	public static List<CrossrefInfoBean> getObjectListManual(ArrayList<String> isbnList) throws SQLException{		
		Statement st = null;
		ResultSet rs = null;
		List<CrossrefInfoBean> liRein = new ArrayList<CrossrefInfoBean>();
			st = conn.createStatement();
			try{
				String sql = generateManualQuery(isbnList);					
				rs = st.executeQuery(sql);
				try{
					while(rs.next()){
						String isbn = rs.getString("isbn");
						String onlineFlag = rs.getString("online_flag");
						Date lastModified = rs.getDate("modified_date");
						String bookId = rs.getString("book_id");
						String product = rs.getString("cbo_product_group");
						String subProduct = rs.getString("sub_product_group_code");
						java.sql.Date _lastModfied = null;
						if(lastModified != null){
							_lastModfied = new java.sql.Date(lastModified.getTime());
						}						
						CrossrefInfoBean rein = new CrossrefInfoBean(isbn, onlineFlag, _lastModfied, bookId, product, subProduct); 
						liRein.add(rein);		
					}
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					rs.close();
				}
			}finally{
				st.close();
			}
		return liRein;		
	}
	
	/*
	 * private methods
	 */
	private static String generateManualQuery(ArrayList<String> isbnList){
		StringBuilder sb = new StringBuilder();
		sb.append("select dl.isbn, dl.online_flag, eb.modified_date, eb.book_id ");
		sb.append(" ,dl.cbo_product_group ");
		sb.append(" ,dl.sub_product_group_code ");
		sb.append("from oraebooks.ebook_isbn_data_load dl, oraebooks.ebook eb ");
		sb.append("where dl.isbn = eb.isbn ");
		
		if(isbnList.size() > 0){
			sb.append(" and dl.isbn in (");
			sb.append(StringUtil.join(isbnList, "'", ","));
			sb.append(") ");
		}
		
		return sb.toString();
	}
	
	
	public static HeadBean getPublisherEmail (String isbn) throws SQLException{
		PreparedStatement st = null;
		ResultSet rs = null;
		String emailString = "";
		String publisherName = "";
		HeadBean hb = new HeadBean();
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT PUBLISHER_EMAIL, PUBLISHER_LEGEND ");
		queryBuilder.append("FROM oraebooks.SR_PUBLISHER ");
		queryBuilder.append("WHERE PUBLISHER_CODE = "); 
		queryBuilder.append("   (SELECT publisher_code from oraebooks.SR_CBO_PRODUCT_GROUP_ORA where CBO_PRODUCT_GROUP = ");
		queryBuilder.append("      (select CBO_PRODUCT_GROUP from oraebooks.core_isbn_cbo where ean_number = ?))");
		queryBuilder.append("AND PUBLISHER_ACTIVE_FLAG = 'Y'");
		st = conn.prepareStatement(queryBuilder.toString());
		st.setString(1, isbn);
		try{
			rs = st.executeQuery();
			while(rs.next())
			{
				emailString = objectToString(rs.getObject("PUBLISHER_EMAIL"));
				publisherName = objectToString(rs.getObject("PUBLISHER_LEGEND"));
			}
			hb.setEmail(emailString);
			hb.setPublisherName(publisherName);
			
		} finally {
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return hb;
	}
	
	public static String getProductCode (String isbn) throws SQLException{	
		PreparedStatement st = null;
		ResultSet rs = null;
		String product_code = "";
		st = conn.prepareStatement("SELECT CBO_PRODUCT_GROUP FROM oraebooks.CORE_ISBN_CBO WHERE EAN_NUMBER = ?");
		st.setString(1, isbn);
		try{
			rs = st.executeQuery();
			while(rs.next())
			{
				product_code = objectToString(rs.getObject("CBO_PRODUCT_GROUP"));
			}
		}finally{
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return product_code;
	}
	
	public static String getSubProductCode(String isbn) throws SQLException {
		PreparedStatement st = null;
		ResultSet rs = null;
		String subProductCode = "";
		st = conn.prepareStatement("SELECT SUB_PRODUCT_GROUP_CODE FROM oraebooks.EBOOK_ISBN_DATA_LOAD WHERE ISBN = ?");
		st.setString(1, isbn);
		try {
			rs = st.executeQuery();
			while (rs.next()) {
				subProductCode = objectToString(rs
						.getObject("SUB_PRODUCT_GROUP_CODE"));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return subProductCode;
	}
	
	public static String getPublisherPrefix (String pubCode) throws SQLException{
		PreparedStatement st = null;
		ResultSet rs = null;
		String publisherPrefix = "";
		st = conn.prepareStatement("SELECT URL_PREFIX FROM oraebooks.SR_PUBLISHER WHERE PUBLISHER_CODE = ?");
		st.setString(1, pubCode);
		try{
			rs = st.executeQuery();
			while(rs.next())
			{
				publisherPrefix = objectToString(rs.getObject("URL_PREFIX"));
			}
		}finally{
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return publisherPrefix;
	}
	
	public static String getPublisherCode (String isbn) throws SQLException{
		PreparedStatement st = null;
		ResultSet rs = null;
		String product_code = "";
		st = conn.prepareStatement("SELECT PUBLISHER_CODE FROM oraebooks.SR_CBO_PRODUCT_GROUP_ORA WHERE CBO_PRODUCT_GROUP = ?");
		st.setString(1, isbn);
		try{
			rs = st.executeQuery();
			while(rs.next())
			{
				product_code = objectToString(rs.getObject("PUBLISHER_CODE"));
			}
		}finally{
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return product_code;
	}
	
	
	public static List<EBookContent> searchContentsByBookId(String bookId) throws SQLException{
		PreparedStatement st = null;
		ResultSet rs = null;
		List<EBookContent> ebookContents = new ArrayList<EBookContent>();
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT filename, content_id, isbn, status, embargo_date, remarks, modified_date, modified_by, type ");
		queryBuilder.append("FROM oraebooks.ebook_content ");
		queryBuilder.append("WHERE book_id = ?");
		st = conn.prepareStatement(queryBuilder.toString());
		st.setString(1, bookId);
		try{
			rs = st.executeQuery();
			while(rs.next())
			{
				EBookContent ebookContent = new EBookContent();

				ebookContent.setFileName(objectToString(rs.getObject("filename")));
				ebookContent.setContentId(objectToString(rs.getObject("content_id")));
				ebookContent.setIsbn(objectToString(rs.getObject("isbn")));
				ebookContent.setStatus(Integer.parseInt(objectToString(rs.getObject("status"))));
				ebookContent.setEmbargoDate(objectToDate((Timestamp)rs.getObject("embargo_date")));
				ebookContent.setRemarks(objectToString(rs.getObject("remarks")));
				ebookContent.setModifiedDate(objectToDate((Timestamp)rs.getObject("modified_date")));
				ebookContent.setModifiedBy(objectToString(rs.getObject("modified_by")));
				ebookContent.setType(objectToString(rs.getObject("type")));

				ebookContents.add(ebookContent);
			}
		}finally{
			if (rs != null) {
				rs.close();
			}
			st.close();
		}
		return ebookContents;
	}
	
	private static String objectToString(Object obj){
		String result = "";
		result = (obj == null ? "" : obj.toString());
		
		return result;
	}
	
	private static Date objectToDate(Timestamp ts){
		Date date = null; 
		date = (ts == null ? null : new Date(ts.getTime()));
		
		return date; 
		
	}
	
}
