package org.cambridge.ebooks.production.crossrefSubmit.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

/**
 * @author jmendiola
 * 
 * This class was created to handle the automated email reports.
 * It reads fill up the required slots from the config file and
 * it accepts the body from parameters sent by the program.
 *
 */

public class CrossRefEmailReport {
	
	private String emailTo = System.getProperty("email.to");
	private String emailFrom = System.getProperty("email.from");
	private String emailPath = System.getProperty("email.path");
	
	public String sendEmailReport(String body){
		String response = "success";
		try {
			printFile(emailPath+generateTimeStamp(true), generateEmail(body));
		} catch (IOException e) {
			e.printStackTrace();
			response = "fail";
		}
		
		return response;
	}
	
	private String generateEmail(String body){
		return "from:"+emailFrom+ "\n"+
			"to:"+emailTo+"\n"+
			"subject:CrossRef Report " + generateTimeStamp(false)+"\n"+
			"body:<p>Hi,<br />"+"\n"+
			"This is the result of the Automated Crossref Sender that finished on "+generateTimeStamp(false).replace("_", " ") 
			+".<br /><br />\n"+
			body.replace("\n", "<br />")+"\n"+
			"</p>";
	}
	
	private String generateTimeStamp(boolean trigger){
		Calendar dateNow = Calendar.getInstance();
		String str = 
			dateNow.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.UK) + "_" +
			dateNow.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.UK) + "_" +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0") + "_" +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + "_" +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") + "_" +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0") + "_" +
			dateNow.getTimeZone().getDisplayName(dateNow.getTimeZone().useDaylightTime(), TimeZone.SHORT);
		
		//true = file name
		if(trigger){
			str = str.concat("_" + dateNow.get(Calendar.YEAR) + ".mail");
		}
		
		return str;
	}
	
	private static void printFile(String path, String text) throws IOException {
		Writer output = null;	    
	    File file = new File(path);
	    output = new BufferedWriter(new FileWriter(file));
	    output.write(text);
	    output.close();
	    System.out.println("File "+ path +" has been written");  
	}
}
