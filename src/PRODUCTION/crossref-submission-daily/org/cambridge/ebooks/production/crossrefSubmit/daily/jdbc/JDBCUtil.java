package org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {
	public static Connection getConnection() {
		Connection connection = null; 
		try { 
			// Load the JDBC driver 
			String driverName = System.getProperty("jdbc.driver");//"oracle.jdbc.driver.OracleDriver";
//			System.out.println("driverName=" + driverName);
			Class.forName(driverName);
			
			// Create a connection to the database			
			String url=System.getProperty("jdbc.url");
			String username = System.getProperty("jdbc.user");//"username"; 
			String password = System.getProperty("jdbc.pass");//"password"; 
			if(url == null){
				
			}
//			System.out.println("connecting to url=" + url);
			connection = DriverManager.getConnection(url, username, password); 
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			e.printStackTrace();
		} catch (SQLException e) { 
			// Could not connect to the database
			e.printStackTrace();
		}		
		return connection;
	}
		
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
