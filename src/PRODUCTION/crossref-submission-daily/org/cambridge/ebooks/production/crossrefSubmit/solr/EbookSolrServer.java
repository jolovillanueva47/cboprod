package org.cambridge.ebooks.production.crossrefSubmit.solr;

import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.cambridge.ebooks.production.crossrefSubmit.solr.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.production.crossrefSubmit.solr.CupSolrServer;

public class EbookSolrServer {
	private static CupSolrServer server;
	private static SolrServer searchCore;
	private static SolrServer bookCore;
	private static SolrServer chapterCore;
	
	static {
		try {
			server = new CupCommonsHttpSolrServer();
			
			Map<String, String> settings = new HashMap<String, String>();
			settings.put(CupSolrServer.KEY_SOCKET_TIMEOUT, System.getProperty("socket.timeout"));
			settings.put(CupSolrServer.KEY_CONN_TIMEOUT, System.getProperty("connection.timeout"));
			settings.put(CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST, System.getProperty("default.max.connection.per.host"));
			settings.put(CupSolrServer.KEY_MAX_TOTAL_CONN, System.getProperty("max.total.connections"));
			settings.put(CupSolrServer.KEY_FOLLOW_REDIRECTS, System.getProperty("follow.redirects"));
			settings.put(CupSolrServer.KEY_ALLOW_COMPRESSION, System.getProperty("allow.compression"));
			settings.put(CupSolrServer.KEY_MAX_RETRIES, System.getProperty("max.retries"));
			
			bookCore = server.getMultiCore(System.getProperty("solr.home.url"), System.getProperty("core.book"), settings);	
			chapterCore = server.getMultiCore(System.getProperty("solr.home.url"), System.getProperty("core.chapter"), settings);
			searchCore = server.getMultiCore(System.getProperty("solr.home.url"), System.getProperty("core.search"), settings); 			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public static SolrServer getSearchCore() {			
		return searchCore;
	}
	
	public static SolrServer getBookCore() {
		return bookCore;
	}
	
	public static SolrServer getChapterCore() {
		return chapterCore;
	}
}

