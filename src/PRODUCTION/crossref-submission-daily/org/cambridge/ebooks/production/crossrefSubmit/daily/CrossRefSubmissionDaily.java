package org.cambridge.ebooks.production.crossrefSubmit.daily;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.cambridge.ebooks.production.crossrefSubmit.daily.bean.CrossrefInfoBean;
import org.cambridge.ebooks.production.crossrefSubmit.daily.jdbc.DBInfo;
import org.cambridge.ebooks.production.crossrefSubmit.util.CrossRefEmailReport;
import org.cambridge.ebooks.production.crossrefSubmit.util.CrossRefUtil;
import org.cambridge.ebooks.production.references.ReferenceResolver;
import org.cambridge.ebooks.production.util.DateUtil;
import org.cambridge.ebooks.production.util.FileUtil;

/**
 * @author bryan manalo/modified by joseph mendiola
 * 
 * CHANGELOG:
 * 20130823 - DPB-7027 - jmendiola - removed CCO/CHO/SSO note on skip processing
 *                                   added stripTagFromIsbn(String) to handle tag stripping from book_id
 *
 */

public class CrossRefSubmissionDaily {
    
    /* how to use */
    /* 
    arg[0] = set to null otherwise location of text file with isbn list
    arg[1-x] = all property files you want to load
    arg[last] = location of the log file    
    */
    public static void main(String... args) throws Exception {
        //Note: insure that when user forcefully shuts down this task, DBInfo is closed - jlonceras.
        Runtime.getRuntime().addShutdownHook(new Thread("Shutdown Cleanup") {
            public void run() {
                System.out.println("Shutting down...");
                try { DBInfo.endConnection(); } 
                catch (SQLException e) { e.printStackTrace(); }
            }
        });
        mainProper(args);
    }
    
    private static void mainProper(String... args) throws Exception {        
                
        areAllDirFiles(args); //checks and throws exception dirs
        initProperties(args); //init props    

        StringBuffer sb = new StringBuffer();                        
        try{
            DBInfo.startConnection();
            if(args[0].equals("null")){
                dbReindex(sb);
            }else{
                manualReindex(args[0], sb);
            }
        }catch(Exception e){            
            e.printStackTrace();
            sb.append("Error encountered: Printing message: " + e.getMessage());
            throw new Exception(e.getMessage());
        }finally{
            DBInfo.endConnection();
            String path = DailyLogger.generateFilename(args);
            FileUtil.generateFileWithRename(sb, path);
        }
    }
    
    private static  void dbReindex(StringBuffer sb) throws SQLException, IOException{      
        System.out.println("Start Sending to Crossref from db "+ DateUtil.getCurrentDate("yyyyMMdd hh:mm:ss aaa"));
        DailyLogger.append(sb, "start reindex from db\n");      
        DailyLogger.createLogHeader(sb);
                    
        List<CrossrefInfoBean> liRein = DBInfo.getObjectList();     
        doReindex(liRein, sb);
        resolveCrossReferences(liRein, sb);
        
        System.out.println("Finished at "+ DateUtil.getCurrentDate("yyyyMMdd hh:mm:ss aaa"));
        DailyLogger.append(sb, "Finished at "+ DateUtil.getCurrentDate("yyyyMMdd hh:mm:ss aaa"));
    }
    
    private static  void manualReindex(String loc, StringBuffer sb) throws SQLException, IOException{
        System.out.println("start reindex from file");
        DailyLogger.append(sb, "start reindex from file\n");
        
        List<CrossrefInfoBean> liRein = readFromFile(loc);
        doReindex(liRein, sb);
        resolveCrossReferences(liRein, sb);
        
        System.out.println("Finished at "+ DateUtil.getCurrentDate("yyyyMMdd hh:mm:ss aaa"));
        DailyLogger.append(sb, "Finished at "+ DateUtil.getCurrentDate("yyyyMMdd hh:mm:ss aaa"));
    }
    
    private static void doReindex(List<CrossrefInfoBean> liRein, StringBuffer sb) throws SQLException, IOException{
        DailyLogger.append(sb, "size of db = " +liRein.size() + "\n");
        System.out.println("size of db = " +liRein.size());
        boolean shouldPrint = false;
        StringBuffer resultString = new StringBuffer();
        for(CrossrefInfoBean rein : liRein){
            System.out.println(rein);
            
            //send to crossref if book is visible online
            if(rein.getOnlineFlag().equals("Y")){
                System.out.println(rein.getIsbn() + " passed requirements. Processing now...");
                CrossRefJarDeliveryWorker deliver = new CrossRefJarDeliveryWorker();
                
                resultString.append( deliver.deliverEBook(rein.getBookId(), stripTagFromIsbn(rein.getBookId()), true, "xmlSend") + "\n");
                
                if (deliver.isSuccess()){
                    shouldPrint = true;
                }
            } else {
                System.out.println(rein.getIsbn() + " is onlineFlag = N. Skip processing."); 
            }
            //create log body
            DailyLogger.createLogBody(sb, rein);
        }
        
        //Send Email Report
        CrossRefEmailReport emailReport = new CrossRefEmailReport();
        emailReport.sendEmailReport(resultString.toString());
        DailyLogger.append(sb, "Email Report Generated!");
        System.out.println("Email Report Generated!");
        
        if(shouldPrint){
            CrossRefUtil.printFile(System.getProperty("crossref.app.dir")+"lastSuccessful.txt",CrossRefUtil.currentDate());
        }
        
        //create log end
        DailyLogger.createLogEnd(sb);
    }
    
    /*
     * 20130823 - DPB-7027 - added to strip the tag from the ISBN to make sure it processes correctly.
     */
    private static String stripTagFromIsbn(String eisbn){
        if(eisbn.length()>13){
            eisbn = eisbn.substring(3);
        }
        return eisbn;
    }
    
    
    @SuppressWarnings("unused") // no longer used since DPB-5804 was deployed. - jubs
    private static boolean isNotThirdParty(String productGroup){
        boolean isNotThirdParty = true;
        if(productGroup.equals("CCO") || productGroup.equals("CHO") || productGroup.equals("SSO")){
            isNotThirdParty = false;
        }
        return isNotThirdParty;
    }
    
    
    /*
     * This method will take all the configurations in the config file then store 
     * them in the System for quick access during the execution of the program. 
     */
    private static void initProperties(String... args) throws FileNotFoundException, IOException {        
        Properties properties = new Properties(System.getProperties());        
        for(int i = 1; i < args.length - 1; i++){            
            properties.load(new FileInputStream(args[i]));
        }     
        System.setProperties(properties);        
        
        //required for log4j
        System.setProperty("jboss.server.home.dir", "/app/jboss-5.1.0.GA/server/production/log");
        
    }
        
    
    @SuppressWarnings("unused")
    private static List<CrossrefInfoBean> test(){
        List<CrossrefInfoBean> liRein = new ArrayList<CrossrefInfoBean>();
        return liRein;
    }
    
    /*
     * This method will check if the file locations given by the config file are valid.
     */
    
    private static void areAllDirFiles(String... args) throws Exception{
        for(String loc : args){
            File file = new File(loc);
            if(!(file.isDirectory() || file.isFile() || "null".equals(loc))){
                throw new Exception(loc + " is not a directory!");
            }            
        }        
    }    
    
    private static List<CrossrefInfoBean> readFromFile(String loc) throws SQLException{
        ArrayList<CrossrefInfoBean> liRein = new ArrayList<CrossrefInfoBean>();
        try {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input =  new BufferedReader(new FileReader(loc));
            try {
                String line = null; //not declared within while loop
                /*
                * readLine is a bit quirky :
                * it returns the content of a line MINUS the newline.
                * it returns null only for the END of the stream.
                * it returns an empty String if two newlines appear in a row.
                */
                int cnt = 0;
                ArrayList<String> isbnList = new ArrayList<String>();
                                
                //limits to 900 because sql has a limit for in (,,,,,) up to 999 according to jhorelle -jubs
                while (( line = input.readLine()) != null){
                    isbnList.add(line.trim());
                    if(cnt==900){                    
                        addToReindexList(isbnList, liRein);
                        isbnList = new ArrayList<String>();
                        cnt=0;
                    }                    
                    cnt++;                    
                }
                
                //if item is not in 900 mulitples make sure to add also, which is highly likely
                addToReindexList(isbnList, liRein);
            } finally {
                input.close();
            }
        } catch (IOException ex){
            ex.printStackTrace();
        }
        return liRein;

    }
    
    private static void resolveCrossReferences(final List<CrossrefInfoBean> liRein, StringBuffer sb) {
        System.out.println("start resolving crossreferences");
        DailyLogger.append(sb, "start resolving crossreferences\n");
        try {
            final String SPACE = " ";
            StringBuffer reinSb = new StringBuffer("query");
            if (liRein != null) {
                for (CrossrefInfoBean rein : liRein) {
                    String  isbn = rein.getIsbn();
                    boolean isOnline = rein.getOnlineFlag().equals("Y"); //References should only be queried with Cross-ref when the book has online_flag=Y.
                    if (isbn != null && isbn.length() > 0 && isOnline) {
                        reinSb.append(SPACE+rein.getIsbn());
                    }
                }
            }
            ReferenceResolver.main(reinSb.toString().split(SPACE));
        }
        catch (Exception e) {
            DailyLogger.append(sb, "Error encountered but will go on: Printing message: " + e.getMessage()+"\n");
        }
        System.out.println("finished");
        
    }
    
    private static void addToReindexList(ArrayList<String> isbnList, ArrayList<CrossrefInfoBean> liRein) throws SQLException{
        List<CrossrefInfoBean> _liRein = DBInfo.getObjectListManual(isbnList);
        liRein.addAll(_liRein);
    }
    
    
}
