package org.cambridge.ebooks.production.reindex.daily.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.reindex.daily.bean.ReindexInfoBean;
import org.cambridge.ebooks.production.util.StringUtil;

public class DBInfo {
	public static List<ReindexInfoBean> getObjectList() throws SQLException{
		Connection conn = JDBCUtil.getConnection();		
		Statement st = null;
		ResultSet rs = null;
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try{
			st = conn.createStatement();
			try{
				String sql = 
					"select dl.isbn, dl.online_flag, eb.modified_date, eb.book_id " +
					"from ebook_isbn_data_load dl, ebook eb " +
					"where trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) " + 
					"  and dl.isbn = eb.isbn ";
				rs = st.executeQuery(sql);
				try{
					//int cnt = 0;
					while(rs.next()){
						String isbn = rs.getString("isbn");
						String onlineFlag = rs.getString("online_flag");
						Date lastModified = rs.getDate("modified_date");
						String bookId = rs.getString("book_id");
						java.sql.Date _lastModfied = null;
						if(lastModified != null){
							_lastModfied = new java.sql.Date(lastModified.getTime());
						}						
						ReindexInfoBean rein = new ReindexInfoBean(isbn, onlineFlag, _lastModfied, bookId);
						liRein.add(rein);		
						//System.out.println(cnt++);
					}
				}finally{
					rs.close();
				}
			}finally{
				st.close();
			}
		}finally{			
			conn.close();
		}
		return liRein;		
	}
	
	public static List<ReindexInfoBean> getObjectListManual(ArrayList<String> isbnList) throws SQLException{
		Connection conn = JDBCUtil.getConnection();		
		Statement st = null;
		ResultSet rs = null;
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try{
			st = conn.createStatement();
			try{
				String sql = generateManualQuery(isbnList);					
				rs = st.executeQuery(sql);
				try{
					//int cnt = 0;
					while(rs.next()){
						String isbn = rs.getString("isbn");
						String onlineFlag = rs.getString("online_flag");
						Date lastModified = rs.getDate("modified_date");
						String bookId = rs.getString("book_id");
						java.sql.Date _lastModfied = null;
						if(lastModified != null){
							_lastModfied = new java.sql.Date(lastModified.getTime());
						}						
						ReindexInfoBean rein = new ReindexInfoBean(isbn, onlineFlag, _lastModfied, bookId);
						liRein.add(rein);		
						//System.out.println(cnt++);
					}
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					rs.close();
				}
			}finally{
				st.close();
			}
		}finally{			
			conn.close();
		}
		return liRein;		
	}
	
	/*
	 * private methods
	 */
	private static String generateManualQuery(ArrayList<String> isbnList){
		String sql = 
			"select dl.isbn, dl.online_flag, eb.modified_date, eb.book_id " +
			"from ebook_isbn_data_load dl, ebook eb " +
			"where dl.isbn = eb.isbn "; 
			
		
		StringBuffer sb = new StringBuffer();
		
		if(isbnList.size() > 0){
			sb.append(" and dl.isbn in (");
			sb.append(StringUtil.join(isbnList, "'", ","));
			sb.append(") ");
		}
		
		return sql + sb.toString();
	}
	
	
}
