package org.cambridge.ebooks.production.reindex.daily;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.cambridge.ebooks.production.reindex.SearchMDB;
import org.cambridge.ebooks.production.reindex.daily.bean.ReindexInfoBean;
import org.cambridge.ebooks.production.reindex.daily.jdbc.DBInfo;
import org.cambridge.ebooks.production.util.FileUtil;

public class OnlineReindexDaily {
	
	/* how to use */
	/*
	arg[0] = set to null otherwise location of text file with isbn list
	arg[1-x] = all property files you want to load
	arg[last] = location of the log file	
	*/
	public static void main(String... args) throws Exception {
		mainProper(args);
	}
	
	private static void mainProper(String... args) throws Exception {		
		//checks and throws exception dirs		
		areAllDirFiles(args);
			
		//init props		
		initProperties(args);	

		StringBuffer sb = new StringBuffer();						
		try{
			if(args[0].equals("null")){
				dbReindex(sb);
			}else{				
				manualReindex(args[0], sb);
			}
		}catch(Exception e){			
			e.printStackTrace();
			sb.append("Error encountered: Printing message: " + e.getMessage());
			throw new Exception(e.getMessage());
		}finally{
			String path = DailyLogger.generateFilename(args);
			FileUtil.generateFileWithRename(sb, path);
		}

	}
	
	private static void doReindex(List<ReindexInfoBean> liRein, StringBuffer sb){
		//logs
		DailyLogger.append(sb, "size of db = " +liRein.size() + "\n");
		System.out.println("size of db = " +liRein.size());
		SearchMDB mdb = new SearchMDB();		
		for(ReindexInfoBean rein : liRein){
			System.out.println(rein);
			mdb.onMessage(rein);
			
			
			//create log body
			DailyLogger.createLogBody(sb, rein);				
		}		
		
		try {
			mdb.optimizeIndexAll(sb);
		} catch (Exception e) {			
			System.out.println("index not optimized");
			sb.append("index not optimized\n");
			sb.append(e.getMessage());
		}
		
		//create log end
		DailyLogger.createLogEnd(sb);
	}
	
	private static void initProperties(String... args) throws FileNotFoundException, IOException {		
		Properties properties = new Properties(System.getProperties());		
		for(int i = 1; i < args.length - 1; i++){			
			properties.load(new FileInputStream(args[i]));
		}	 
		System.setProperties(properties);		
		
		//required for log4j
		System.setProperty("jboss.server.home.dir", "/app/jboss-4.2.3.GA/log");
		
	}
		
	
	@SuppressWarnings("unused")
	private static List<ReindexInfoBean> test(){
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
//		Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
//		ReindexInfoBean b1 = new ReindexInfoBean("9780511485183", "Y", sqlDate, "CBO9780511485183");
//		ReindexInfoBean b2 = new ReindexInfoBean("9780511483066", "Y", sqlDate, "CBO9780511483066");
//		
//		liRein.add(b1);
//		liRein.add(b2);		
		return liRein;
	}
		
	private static void areAllDirFiles(String... args) throws Exception{
		for(String loc : args){
			File file = new File(loc);
			if(!(file.isDirectory() || file.isFile() || "null".equals(loc))){
				throw new Exception(loc + " is not a directory!");
			}			
		}		
	}	
	
	private static  void dbReindex(StringBuffer sb) throws SQLException{		
		//logs
		System.out.println("start reindex from db");
		DailyLogger.append(sb, "start reindex from db\n");		
		DailyLogger.createLogHeader(sb);
					
		List<ReindexInfoBean> liRein = //test(); 
			DBInfo.getObjectList();		
		
		doReindex(liRein, sb);		
		//doyourthing();
		System.out.println("finished");		
	}
	
	private static  void manualReindex(String loc, StringBuffer sb) throws SQLException{
		System.out.println("start reindex from file");
		DailyLogger.append(sb, "start reindex from file\n");	
		List<ReindexInfoBean> liRein = readFromFile(loc);
		doReindex(liRein, sb);
		System.out.println("finished");
	}
	
	private static List<ReindexInfoBean> readFromFile(String loc) throws SQLException{
		ArrayList<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try {
			//use buffering, reading one line at a time
		    //FileReader always assumes default encoding is OK!
		    BufferedReader input =  new BufferedReader(new FileReader(loc));
		    try {
		    	String line = null; //not declared within while loop
		        /*
		        * readLine is a bit quirky :
		        * it returns the content of a line MINUS the newline.
		        * it returns null only for the END of the stream.
		        * it returns an empty String if two newlines appear in a row.
		        */
		    	int cnt = 0;
		    	ArrayList<String> isbnList = new ArrayList<String>();
		    			    	
		    	//limits to 900 because sql has a limit for in (,,,,,) up to 999 according to jhorelle
		    	while (( line = input.readLine()) != null){
		    		isbnList.add(line.trim());
		    		if(cnt==900){		    		
		    			addToReindexList(isbnList, liRein);
		    			isbnList = new ArrayList<String>();
		    			cnt=0;
		    		}		    		
		    		cnt++;		    		
		    	}
		    	
		    	//if item is not in 900 mulitples make sure to add also, which is highly likely
				addToReindexList(isbnList, liRein);
		    } finally {
		        input.close();
		    }
		} catch (IOException ex){
			ex.printStackTrace();
		}
		return liRein;

	}
	
	
	private static void addToReindexList(ArrayList<String> isbnList, ArrayList<ReindexInfoBean> liRein) throws SQLException{
		List<ReindexInfoBean> _liRein = DBInfo.getObjectListManual(isbnList);
		liRein.addAll(_liRein);
	}
	

	
	
	
}
