package org.cambridge.ebooks.production.crossref.ejb.xml;


public class PublisherItem {

	private String itemNumber;
	
	public PublisherItem(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public String getItemNumber() {
		return itemNumber;
	}
}
