package org.cambridge.ebooks.production.crossref.ejb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentDAO;
import org.cambridge.ebooks.production.crossref.ejb.util.EBookContentWorker;
import org.cambridge.ebooks.production.crossref.ejb.util.EBookDeliveryWorker;


@MessageDriven(name = "CrossRefDeliveryMDB", activationConfig = {@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
																 @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
																 @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/crossref/logger") })

@TransactionManagement(TransactionManagementType.BEAN)	

public class CrossRefDeliveryMDB implements MessageListener {
	
	private static final long serialVersionUID = 1L;

	@Resource
	private MessageDrivenContext mdc;
	
    @Resource
    SessionContext sc;
    
	//@PersistenceUnit(unitName = "CrossrefService")	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("CrossrefService");
	
	private EntityManager em;
	
	private EBookContentDAO dao;
	
	public void onMessage(Message recvMsg){
	
		try 
		{
			if (recvMsg instanceof Message) 
			{		
				dao = new EBookContentDAO(); 
				dao.setEmf(emf);
				Message message = (Message) recvMsg;
				
				Logger.getLogger(CrossRefDeliveryMDB.class).info("CrossRefDeliveryMDB onMessage called." );
				
				//insert your code here
				String bookId = message.getStringProperty("BOOK_ID");
				String username = message.getStringProperty("USERNAME");
	
				Logger.getLogger(CrossRefDeliveryMDB.class).info("[BOOK_ID]: " + bookId);
				Logger.getLogger(CrossRefDeliveryMDB.class).info("[USERNAME]: " + username);
				
				//Deliver to crossref
				deliverEBook(bookId, username);
			}
		} 
		catch (JMSException exc) 
		{
			System.out.println(CrossRefDeliveryMDB.class + " [JMSException]:Uploader EJB: " + exc.getMessage());
			exc.printStackTrace();
			mdc.setRollbackOnly();
		}
	}
	
	private void deliverEBook(String bookId, String username){
		EBookContentWorker eworker = new EBookContentWorker();
		eworker.setDao(dao);
		List<EBookContentBean> ebookContentList = eworker.ebookContents(bookId, true);
	
		EBookDeliveryWorker worker = new EBookDeliveryWorker();
		worker.setEmf(emf);
		worker.setUserTransaction(sc.getUserTransaction());
		worker.deliverEBook(bookId, bookId.replace("CBO", ""), username, ebookContentList, true, EBookDeliveryWorker.XML_AND_SEND);
	}
	

	@PostConstruct
    void init() {
		Logger.getLogger(CrossRefDeliveryMDB.class ).info("Initialize Crossref EJB");		
		em = emf.createEntityManager();
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger(CrossRefDeliveryMDB.class ).info(" Destroy Crossref EJB ");
    	em.close();
    }
}