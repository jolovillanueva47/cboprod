package org.cambridge.ebooks.production.crossref.ejb.xml;

public class Root {

	private Xml xml;
	private DoiBatch doiBatch;
	
	public Root(Xml xml, DoiBatch doiBatch) {
		this.xml = xml;
		this.doiBatch = doiBatch;
	}
	
	public Xml getXml() {
		return xml;
	}
	public DoiBatch getDoiBatch() {
		return doiBatch;
	}
	
	
}
