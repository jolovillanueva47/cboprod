package org.cambridge.ebooks.production.crossref.ejb.xml;

import java.util.ArrayList;


public class ContentItem {

	private String componentType;
	private String levelSequenceNumber;
	private String publicationType;
	private ArrayList<PersonName> contributors;
	private ArrayList<String> titles;
	private String componentNumber;
	private Pages pages;
	private PublisherItem publisherItem;
	private DoiData doiData;
	
	public ContentItem(
			String componentType,
			String levelSequenceNumber,
			String publicationType,
			ArrayList<PersonName> contributors,
			ArrayList<String> titles,
			String componentNumber,
			Pages pages,
			PublisherItem publisherItem,
			DoiData doiData
		) {
		
		this.componentType = componentType;
		this.levelSequenceNumber = levelSequenceNumber;
		this.publicationType = publicationType;
		this.contributors = contributors;
		this.titles = titles;
		this.componentNumber = componentNumber;
		this.pages = pages;
		this.publisherItem = publisherItem;
		this.doiData = doiData;
	}
	
	public String getComponentType() {
		return componentType;
	}
	public String getLevelSequenceNumber() {
		return levelSequenceNumber;
	}
	public String getPublicationType() {
		return publicationType;
	}
	public ArrayList<PersonName> getContributors() {
		return contributors;
	}
	public ArrayList<String> getTitles() {
		return titles;
	}
	public String getComponentNumber() {
		return componentNumber;
	}
	public Pages getPages() {
		return pages;
	}
	public PublisherItem getPublisherItem() {
		return publisherItem;
	}
	public DoiData getDoiData() {
		return doiData;
	}
	
	
}
