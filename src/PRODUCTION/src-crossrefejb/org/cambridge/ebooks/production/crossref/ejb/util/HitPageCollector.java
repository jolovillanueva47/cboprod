package org.cambridge.ebooks.production.crossref.ejb.util;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document; 
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.HitCollector; 
import org.apache.lucene.search.IndexSearcher; 
import org.apache.lucene.search.Query; 
import org.apache.lucene.search.ScoreDoc; 
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.PriorityQueue;


public class HitPageCollector extends HitCollector { //Demo code showing pagination 
	int nDocs; 
	PriorityQueue hq; 
	float minScore = 0.0f; 
	int totalHits = 0; 
	int start; 
	int maxNumHits; 
	int totalInThisPage;

	public HitPageCollector(int start, int maxNumHits) { 
		this.nDocs = start + maxNumHits; 
		this.start = start; 
		this.maxNumHits = maxNumHits; 
		hq = new HitQueue(nDocs); 
	}

	public void collect(int doc, float score) { 
		totalHits++; 
		if((hq.size()<nDocs)||(score >= minScore)) 
		{ 
			ScoreDoc scoreDoc = new ScoreDoc(doc,score); 
			hq.insert(scoreDoc); 
			
			// update hit queue 
			minScore = ((ScoreDoc)hq.top()).score; 
			
			// reset minScore 
		} 
		totalInThisPage=hq.size(); 
	}

	public ScoreDoc[] getScores() { 
		//just returns the number of hits required from the required start point 
		/* So, given hits: 1234567890 and a start of 2 + maxNumHits of 3 should return: 234 or, given hits 12 should return 2 and so, on. */ 
		if (start <= 0) 
		{ 
			throw new IllegalArgumentException("Invalid start :" + start+" - start should be >=1"); 
		} 
	
		int numReturned = Math.min(maxNumHits, (hq.size() - (start - 1))); 
		if (numReturned <= 0) 
		{ 
			return new ScoreDoc[0]; 
		} 
	
		ScoreDoc[] scoreDocs = new ScoreDoc[numReturned]; 
		ScoreDoc scoreDoc; 
	
		for (int i = hq.size() - 1; i >= 0; i--) // put docs in array, working backwards from lowest count 
		{ 
			scoreDoc = (ScoreDoc) hq.pop(); 
			if (i < (start - 1)) 
			{ 
				break; //off the beginning of the results array 
			}

			if (i < (scoreDocs.length + (start - 1))) 
			{ 
				scoreDocs[i - (start - 1)] = scoreDoc; //within scope of results array 
			} 
		} 
	
		return scoreDocs; 
	}

	public int getTotalAvailable() { return totalHits; }

	public int getStart() { return start; }

	//public int getEnd() { return start+totalInThisPage-1; }
	public int getEnd() { return totalInThisPage != totalHits ? totalInThisPage -1 : totalInThisPage; }

	public class HitQueue extends PriorityQueue	{ 
		
		public HitQueue(int size) { initialize(size); } 
		
		public final boolean lessThan(Object a, Object b) { 
			ScoreDoc hitA = (ScoreDoc)a; 
			ScoreDoc hitB = (ScoreDoc)b;

			if (hitA.score == hitB.score) 
				return hitA.doc > hitB.doc; 
			else 
				return hitA.score < hitB.score; 
		} 
	} 
	
	
	public static void main(String[] args) throws Exception { 
		
		Analyzer analyzer = new WhitespaceAnalyzer();
		FSDirectory index = FSDirectory.getDirectory("D:/cbo/bookindex");
		
		//String search = "BOOK_ID:CBO9780511470936";
		String search = "BOOK_ID:CBO9780511493966 AND ELEMENT:content-item";
		 
		IndexSearcher s=new IndexSearcher(index); 
		Query q = new QueryParser("BOOK_ID", analyzer).parse(search);
		//Query q=new TermQuery(new Term("contents","sea"));

		//Retrieve page 1 (hits 1-10) 
		HitPageCollector hpc = new HitPageCollector(23,1); 
		s.search(q,hpc); 
		ScoreDoc[] sd = hpc.getScores(); 
		
		System.out.println("Hits "+ hpc.getStart()+" - "+ hpc.getEnd()+" of "+hpc.getTotalAvailable()); 
	
		for (int i = 0; i < sd.length; i++) 
		{ 
			Document doc=s.doc(sd[i].doc); 
			System.out.println(sd[i].score +" "+doc.get("TITLE")); 
		}


		
		s.close(); 
	}
} 