package org.cambridge.ebooks.production.crossref.ejb.document;

public class PublisherNameBean implements Comparable<PublisherNameBean>{
	private String name;
	private String order;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public int compareTo(PublisherNameBean o) {
		try {
			return this.order.compareTo(o.getOrder());
		} catch (Exception e) {
			return 0;
		}		
		
	}
	
	
}
