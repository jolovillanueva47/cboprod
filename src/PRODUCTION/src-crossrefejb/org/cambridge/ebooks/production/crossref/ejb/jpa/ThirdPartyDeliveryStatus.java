package org.cambridge.ebooks.production.crossref.ejb.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//@NamedNativeQueries({
//	@NamedNativeQuery(
//	        name = ThirdPartyDeliveryStatus.SEARCH_EBOOK_DELIVERY_STATUS, 
//	        query = "SELECT * FROM third_party_delivery_status" +
//	        		" WHERE content_id = ?1 AND" +
//	        		" deliver_date = (SELECT MAX(deliver_date) FROM third_party_delivery_status " +
//	        		" WHERE content_id = ?1 AND status = 1)", 
//	        resultSetMapping = "searchThirdPartyDeliveryStatusResult"
//	),
//	@NamedNativeQuery(
//	        name = ThirdPartyDeliveryStatus.SEARCH_CHAPTER_DELIVERY_STATUS, 
//	        query = "SELECT * FROM third_party_delivery_status" +
//	        		" WHERE (content_id = ?1 OR content_id = ?2) AND" +
//	        		" deliver_date = (SELECT MAX(deliver_date) FROM third_party_delivery_status " +
//	        		" WHERE (content_id = ?1 OR content_id = ?2) AND status = 1)",
//	        resultSetMapping = "searchThirdPartyDeliveryStatusResult"
//	)
//})

@SqlResultSetMapping(
        name = "searchThirdPartyDeliveryStatusResult", 
        entities = { 
                @EntityResult(
                        entityClass = ThirdPartyDeliveryStatus.class, 
                        fields = {
                            @FieldResult(name = "reportId", column = "REPORT_ID"),
                            @FieldResult(name = "thirdPartyId", column = "THIRD_PARTY_ID"),
                            @FieldResult(name = "contentId", column = "CONTENT_ID"),
                            @FieldResult(name = "fileType", column = "FILE_TYPE"),
                            @FieldResult(name = "depositor", column = "DEPOSITOR"),
                            @FieldResult(name = "status", column = "STATUS"),
                            @FieldResult(name = "deliverDate", column = "DELIVER_DATE"),
                            @FieldResult(name = "queueDate", column = "QUEUE_DATE"),
                            @FieldResult(name = "reason", column = "REASON"),
                            @FieldResult(name = "doiBatchId", column = "DOI_BATCH_ID"),
                            @FieldResult(name = "counter", column = "COUNTER")
                        }
                ) 
        }
)

@Entity
@Table(name="THIRD_PARTY_DELIVERY_STATUS")
public class ThirdPartyDeliveryStatus {
	
	public static final String SEARCH_EBOOK_DELIVERY_STATUS = "eBookDeliveryStatusSearch";
	public static final String SEARCH_CHAPTER_DELIVERY_STATUS = "chapterDeliveryStatusSearch";
	public static final int THIRD_PARTY_ID_CROSSREF = 1;
	public static final String FILE_TYPE_XML = "XML";

	@Id
    @GeneratedValue(generator="THIRD_PARTY_DELIVERY_SEQ")
    @SequenceGenerator(name="THIRD_PARTY_DELIVERY_SEQ", sequenceName="THIRD_PARTY_DELIVERY_SEQ", allocationSize=1)
	@Column( name = "REPORT_ID")
	private int reportId;
	
    @Column(name="THIRD_PARTY_ID")
	private int thirdPartyId;
	
    @Column(name="CONTENT_ID")
	private String contentId;
	
    @Column(name="FILE_TYPE")
	private String fileType;
	
    @Column(name="DEPOSITOR")
	private String depositor;
	
    @Column(name="STATUS")
	private String status;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DELIVER_DATE")
	private Date deliverDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="QUEUE_DATE")
	private Date queueDate;
	
    @Column(name="REASON")
	private String reason;
	
    @Column(name="DOI_BATCH_ID")
	private int doiBatchId;
	
    @Column(name="COUNTER")
	private int counter = 0;

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public int getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(int thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setStatus(boolean withError) {
		this.status = withError ? "0" : "1";
	}

	public Date getDeliverDate() {
		return deliverDate;
	}

	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}

	public Date getQueueDate() {
		return queueDate;
	}

	public void setQueueDate(Date queueDate) {
		this.queueDate = queueDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getDoiBatchId() {
		return doiBatchId;
	}

	public void setDoiBatchId(int doiBatchId) {
		this.doiBatchId = doiBatchId;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
}
