package org.cambridge.ebooks.production.crossref.ejb.xml;


public class Publisher {

	private String publisherName;
	private String publisherPlace;
	
	public Publisher(String publisherName, String publisherPlace) {
		this.publisherName = publisherName;
		this.publisherPlace = publisherPlace;
	}
	
	public String getPublisherName() {
		return publisherName;
	}
	public String getPublisherPlace() {
		return publisherPlace;
	}
	
	
}
