package org.cambridge.ebooks.production.crossref.ejb.constant;

/**
 * 
 * @author rvillamor
 * 
 */

public class SessionAttribute {
	public static final String USER = "userInfo";
	public static final String LOGIN_TIME = "loginTime";
	public static final String EBOOK_ACCESS = "ebookAccess";
	public static final String PUBLISHER = "publisherInfo";
}
