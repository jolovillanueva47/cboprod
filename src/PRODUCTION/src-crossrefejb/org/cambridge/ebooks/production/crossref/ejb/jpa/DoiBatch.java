package org.cambridge.ebooks.production.crossref.ejb.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="DOI_BATCH")
public class DoiBatch {

	@Id
    @GeneratedValue(generator="DOI_SEQ")
    @SequenceGenerator(name="DOI_SEQ", sequenceName="DOI_SEQ", allocationSize=1)
	@Column( name = "DOI_BATCH_ID")
	private int doiBatchId;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED_DATE")
    private Date createdDate;
    
    @Column(name="CREATED_BY")
    private String createdBy;
    
    //public DoiBatch() {}
//    public DoiBatch(Date createdDate, String createdBy) {
//    	this.createdDate = createdDate;
//    	this.createdBy = createdBy;
//    }

	public int getDoiBatchId() {
		return doiBatchId;
	}

	public void setDoiBatchId(int doiBatchId) {
		this.doiBatchId = doiBatchId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
