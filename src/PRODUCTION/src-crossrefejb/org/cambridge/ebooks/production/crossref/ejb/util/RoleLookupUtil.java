package org.cambridge.ebooks.production.crossref.ejb.util;

public class RoleLookupUtil {
	private static final String L_COURSE_CONSULTANT = "Course Consultant"; 
	private static final String L_AUTHOR = "Author";
	private static final String L_EDITOR_AND_TRANSLATOR = "Editor and Translator";
	private static final String L_FOREWORD = "Foreword";
	private static final String L_AFTERWORD = "Afterword";
	private static final String L_INTRODUCTION_BY = "Introduction by";
	private static final String L_WITH = "With";
	private static final String L_PREFACE_BY = "Preface by";
	private static final String L_PHOTOGRAPHS_BY = "Photographs by";
	private static final String L_ASSISTED_BY = "Assisted by";
	private static final String L_ILLUSTRATIONS_BY = "Illustrations by";
	private static final String L_ADAPTATION_BY = "Adaptation by";
	private static final String L_NARRATED_BY = "Narrated by"; 
	private static final String L_APPENDIX_BY = "Appendix by";
	private static final String L_CONSULTANT_EDITOR = "Consultant Editor";
	private static final String L_PREPARED_FOR_PUBLICATION_BY = "Prepared for publication by";
	private static final String L_GENERAL_EDITOR = "General Editor";
	private static final String L_CORPORATION = "Corporation";
	private static final String L_EDITOR = "Editor";
	private static final String L_TRANSLATOR = "Translator";
	private static final String L_REAL_AUTHOR = "Real Author";
	private static final String L_ASSOCIATE_EDITOR = "Associate Editor";
	private static final String L_WITH_CONTRIBUTIONS_BY = "With contributions by";
	private static final String L_PROLOGUE_BY = "Prologue by";
	private static final String L_PHOTOGRAPHED_AND_RECORDED_BY = "Photographed and recorded by";
	private static final String L_TEXT_BY = "Text by";
	private static final String L_SOFTWARE_DEVELOPER = "Software developer";
	private static final String L_COMPILED_BY = "Compiled by";
	private static final String L_GENERAL_RAPPORTEUR = "General Rapporteur";
	private static final String L_IN_COLLABORATION_WITH = "In collaboration with";
	private static final String L_SERIES_EDITOR_CAM_LEARNING_USE_ONLY = "Series editor Cam Learning use ONLY";
	
	private static final String C_COURSE_CONSULTANT = "CS";
	private static final String C_AUTHOR = "AU";
	private static final String C_EDITOR_AND_TRANSLATOR = "EDT";
	private static final String C_FOREWORD = "FW";
	private static final String C_AFTERWORD = "AW";
	private static final String C_INTRODUCTION_BY = "IN";
	private static final String C_WITH = "WI";
	private static final String C_PREFACE_BY = "PR";
	private static final String C_PHOTOGRAPHS_BY = "PH";
	private static final String C_ASSISTED_BY = "AS";
	private static final String C_ILLUSTRATIONS_BY = "IL";
	private static final String C_ADAPTATION_BY = "AD";
	private static final String C_NARRATED_BY = "NA";
	private static final String C_APPENDIX_BY = "AP";
	private static final String C_CONSULTANT_EDITOR = "CO";
	private static final String C_PREPARED_FOR_PUBLICATION_BY = "PP";
	private static final String C_GENERAL_EDITOR = "GE";
	private static final String C_CORPORATION = "COR";
	private static final String C_EDITOR = "ED";
	private static final String C_TRANSLATOR = "TR";
	private static final String C_REAL_AUTHOR = "RA";
	private static final String C_ASSOCIATE_EDITOR = "AE";
	private static final String C_WITH_CONTRIBUTIONS_BY = "WC";
	private static final String C_PROLOGUE_BY = "PL";
	private static final String C_PHOTOGRAPHED_AND_RECORDED_BY = "PB";
	private static final String C_TEXT_BY = "TX";
	private static final String C_SOFTWARE_DEVELOPER = "SD";
	private static final String C_COMPILED_BY = "CB";
	private static final String C_GENERAL_RAPPORTEUR = "GR";
	private static final String C_IN_COLLABORATION_WITH = "IC";
	private static final String C_SERIES_EDITOR_CAM_LEARNING_USE_ONLY = "SE";


	public static final String getRole(String code){
		if(C_COURSE_CONSULTANT.equalsIgnoreCase(code)){
			return L_COURSE_CONSULTANT;
		}else if(C_AUTHOR.equalsIgnoreCase(code)){
			return L_AUTHOR;
		}else if(C_EDITOR_AND_TRANSLATOR.equalsIgnoreCase(code)){
			return L_EDITOR_AND_TRANSLATOR;
		}else if(C_FOREWORD.equalsIgnoreCase(code)){
			return L_FOREWORD;
		}else if(C_AFTERWORD.equalsIgnoreCase(code)){
			return L_AFTERWORD;	
		}else if(C_INTRODUCTION_BY.equalsIgnoreCase(code)){
			return L_INTRODUCTION_BY;
		}else if(C_WITH.equalsIgnoreCase(code)){
			return L_WITH;
		}else if(C_PREFACE_BY.equalsIgnoreCase(code)){
			return L_PREFACE_BY;
		}else if(C_PHOTOGRAPHS_BY.equalsIgnoreCase(code)){
			return L_PHOTOGRAPHS_BY;
		}else if(C_ASSISTED_BY.equalsIgnoreCase(code)){
			return L_ASSISTED_BY;
		}else if(C_ILLUSTRATIONS_BY.equalsIgnoreCase(code)){
			return L_ILLUSTRATIONS_BY;
		}else if(C_ADAPTATION_BY.equalsIgnoreCase(code)){
			return L_ADAPTATION_BY;
		}else if(C_NARRATED_BY.equalsIgnoreCase(code)){
			return L_NARRATED_BY;
		}else if(C_APPENDIX_BY.equalsIgnoreCase(code)){
			return L_APPENDIX_BY;
		}else if(C_CONSULTANT_EDITOR.equalsIgnoreCase(code)){
			return L_CONSULTANT_EDITOR;
		}else if(C_PREPARED_FOR_PUBLICATION_BY.equalsIgnoreCase(code)){
			return L_PREPARED_FOR_PUBLICATION_BY;
		}else if(C_GENERAL_EDITOR.equalsIgnoreCase(code)){
			return L_GENERAL_EDITOR;
		}else if(C_CORPORATION.equalsIgnoreCase(code)){
			return L_CORPORATION;
		}else if(C_EDITOR.equalsIgnoreCase(code)){
			return L_EDITOR;
		}else if(C_TRANSLATOR.equalsIgnoreCase(code)){
			return L_TRANSLATOR;
		}else if(C_REAL_AUTHOR.equalsIgnoreCase(code)){
			return L_REAL_AUTHOR;
		}else if(C_ASSOCIATE_EDITOR.equalsIgnoreCase(code)){
			return L_ASSOCIATE_EDITOR;
		}else if(C_WITH_CONTRIBUTIONS_BY.equalsIgnoreCase(code)){
			return L_WITH_CONTRIBUTIONS_BY;
		}else if(C_PROLOGUE_BY.equalsIgnoreCase(code)){
			return L_PROLOGUE_BY;
		}else if(C_PHOTOGRAPHED_AND_RECORDED_BY.equalsIgnoreCase(code)){
			return L_PHOTOGRAPHED_AND_RECORDED_BY;
		}else if(C_TEXT_BY.equalsIgnoreCase(code)){
			return L_TEXT_BY;
		}else if(C_SOFTWARE_DEVELOPER.equalsIgnoreCase(code)){
			return L_SOFTWARE_DEVELOPER;
		}else if(C_COMPILED_BY.equalsIgnoreCase(code)){
			return L_COMPILED_BY;
		}else if(C_GENERAL_RAPPORTEUR.equalsIgnoreCase(code)){
			return L_GENERAL_RAPPORTEUR;
		}else if(C_IN_COLLABORATION_WITH.equalsIgnoreCase(code)){
			return L_IN_COLLABORATION_WITH;
		}else if(C_SERIES_EDITOR_CAM_LEARNING_USE_ONLY.equalsIgnoreCase(code)){
			return L_SERIES_EDITOR_CAM_LEARNING_USE_ONLY;			
		}
		return code;		
	}

}
