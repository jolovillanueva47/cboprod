package org.cambridge.ebooks.production.crossref.ejb.xml;

public class Body {
	private Book book;
	
	public Body(Book book) {
		this.book = book;
	}

	public Book getBook() {
		return book;
	}
}
