package org.cambridge.ebooks.production.crossref.ejb.document;

public class ContributorBean {

	private String contributor;
	private String position;
	public String getContributor() {
		return contributor;
	}
	public void setContributor(String contributor) {
		this.contributor = contributor;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	
}
