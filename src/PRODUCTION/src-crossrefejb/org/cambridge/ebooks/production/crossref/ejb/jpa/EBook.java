package org.cambridge.ebooks.production.crossref.ejb.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author rvillamor
 */

@SqlResultSetMapping( 
	name = "searchEBookResult",
	entities = {
		@EntityResult (
			entityClass = EBook.class,
			fields = {
				@FieldResult(name = "bookId",    column = "BOOK_ID"),
				@FieldResult(name = "isbn",      column = "ISBN"),
				@FieldResult(name = "status",    column = "STATUS"),
				@FieldResult(name = "seriesId",  column = "SERIES_CODE"),
				@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
				@FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
				@FieldResult(name = "title", column = "TITLE")
			}
		)
	}
 )
 
@Entity
@Table(name = "EBOOK")
public class EBook {
	
	public static final String SEARCH_ALL = "searchAllEBookSQL";
	public static final String SEARCH_ACCESSIBLE_EBOOKS_BY_USER_ID = "accessibleEbooksSearchByUserId";
	public static final String SEARCH_BY_BOOK_ID = "searchByBookId";
	public static final String INSERT_EBOOK = "insertEBook";
	public static final String UPDATE_EBOOK_BY_BOOK_ID = "updateEbookByBookId";
	
	@Id
	@Column(name = "BOOK_ID")
	private String bookId;
	
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "STATUS")
	private int status;
	
	@Column(name = "SERIES_CODE")
	private String seriesId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "TITLE")
	private String title;	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getSeriesId() {
		return seriesId;
	}
	
	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public String getBookId() {
		return bookId;
	}
	
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}	
}
