package org.cambridge.ebooks.production.crossref.ejb.common;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.cambridge.ebooks.production.crossref.ejb.jpa.EBookContent;


public class EBookContentDAO {
	
	private static EntityManagerFactory emf; //= Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	
	@SuppressWarnings("unchecked")
	public static List<EBookContent> searchContentsByBookId(String bookId){
		List<EBookContent> ebookContents = new ArrayList<EBookContent>();
		
		System.out.println("HERE: 1");
		
		String sql = "SELECT filename, content_id, isbn, status, embargo_date, remarks, modified_date, modified_by, type " +
				     "FROM ebook_content " +
				     "WHERE book_id = ?1";

		 
		EntityManager em = emf.createEntityManager();
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, bookId);
		System.out.println("QUERY: " + query.toString());
		
		List<Object[]> result = query.getResultList();
		
		for(Object[] objArr : result)
		{
			EBookContent ebookContent = new EBookContent();
			
			ebookContent.setFileName(objectToString(objArr[0]));
			ebookContent.setContentId(objectToString(objArr[1]));
			ebookContent.setIsbn(objectToString(objArr[2]));
			ebookContent.setStatus(Integer.parseInt(objectToString(objArr[3])));
			ebookContent.setEmbargoDate((Date)objArr[4]);
			ebookContent.setRemarks(objectToString(objArr[5]));
			ebookContent.setModifiedDate((Date)objArr[6]);
			ebookContent.setModifiedBy(objectToString(objArr[7]));
			ebookContent.setType(objectToString(objArr[8]));
			
			ebookContents.add(ebookContent);
		}
		
		
		return ebookContents;
	}
	
	public static EBookContent searchContentByContentIdAndType(String contentId, String type){
		EBookContent ebookContent = new EBookContent();
		
		String sql = "SELECT filename, content_id, isbn, status, embargo_date, remarks, modified_date, modified_by, type " +
				     "FROM ebook_content " +
				     "WHERE content_id = ?1 " +
				     "AND TYPE = ?2";
		 
		EntityManager em = emf.createEntityManager();
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, contentId);
		query.setParameter(2, type);
		System.out.println("QUERY: " + query.toString());
		try{
			Object[] result = (Object[])query.getSingleResult();
			
			ebookContent.setFileName(objectToString(result[0]));
			ebookContent.setContentId(objectToString(result[1]));
			ebookContent.setIsbn(objectToString(result[2]));
			ebookContent.setStatus(Integer.parseInt(objectToString(result[3])));
			ebookContent.setEmbargoDate((Date)result[4]);
			ebookContent.setRemarks(objectToString(result[5]));
			ebookContent.setModifiedDate((Date)result[6]);
			ebookContent.setModifiedBy(objectToString(result[7]));
			ebookContent.setType(objectToString(result[8]));
		} catch (NoResultException e){
			e.printStackTrace();
		}
		return ebookContent;
	}
	
	
	private static String objectToString(Object obj){
		String result = "";
		result = (obj == null ? "" : obj.toString());
		
		return result;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		EBookContentDAO.emf = emf;
	}
	
		     

}
