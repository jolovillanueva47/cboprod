package org.cambridge.ebooks.production.crossref.ejb.common;

//a marker interface to compare list of EBookContent class to list of EBookContentBean
public interface EBookContentComponent {
	
	String getContentId();
	
}
