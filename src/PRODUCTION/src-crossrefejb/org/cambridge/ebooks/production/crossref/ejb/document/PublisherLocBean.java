package org.cambridge.ebooks.production.crossref.ejb.document;

public class PublisherLocBean implements Comparable<PublisherLocBean>{
	private String location;
	private String order;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String name) {
		this.location = name;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public int compareTo(PublisherLocBean o) {
		try {
			return this.order.compareTo(o.getOrder());
		} catch (Exception e) {
			return 0;
		}		
	}
}
