package org.cambridge.ebooks.production.crossref.ejb.xml;


public class Pages {

	private String firstPage;
	private String lastPage;
	
	public Pages(String firstPage, String lastPage) {
		this.firstPage = firstPage;
		this.lastPage = lastPage;
	}
	
	public String getFirstPage() {
		return firstPage;
	}
	public String getLastPage() {
		return lastPage;
	}
}
