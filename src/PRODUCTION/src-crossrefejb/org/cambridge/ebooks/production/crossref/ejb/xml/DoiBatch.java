package org.cambridge.ebooks.production.crossref.ejb.xml;


public class DoiBatch {

	private String version;
	private String xmlns;
	private String xmlnsXsi;
	private String xsiSchemaLocation;
	private Head head;
	private Body body;
	
	public DoiBatch(String version, String xmlns, String xmlnsXsi, String xsiSchemaLocation, Head head, Body body) {
		this.version = version;
		this.xmlns = xmlns;
		this.xmlnsXsi = xmlnsXsi;
		this.xsiSchemaLocation = xsiSchemaLocation;
		this.head = head;
		this.body = body;
	}
	
	public String getVersion() {
		return version;
	}
	public String getXmlns() {
		return xmlns;
	}
	public String getXmlnsXsi() {
		return xmlnsXsi;
	}
	public String getXsiSchemaLocation() {
		return xsiSchemaLocation;
	}
	public Head getHead() {
		return head;
	}
	public Body getBody() {
		return body;
	}
}
