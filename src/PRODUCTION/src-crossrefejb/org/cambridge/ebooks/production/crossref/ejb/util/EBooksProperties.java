
package org.cambridge.ebooks.production.crossref.ejb.util;


public class EBooksProperties {
	
	public static final String INDEX = "index.dir";
	public static final String TEMPLATES_DIR = "templates.dir";
	public static final String EBOOKS_RESOURCE_URL = "ebooks.resource.url";
	public static final String UPO_RESOURCE_URL = "upo.resource.url";
	
	public static final String VALID_IPS_PATH = "valid.ips.path";
	public static final String SUBNET_IPS_PATH = "subnet.ips.path";
	
}
