/**
 * 
 */
package org.cambridge.ebooks.production.crossref.ejb.util;

import java.util.List;



/**
 * @author rvillamor
 *
 */
public class ListUtil {
	
	public static <T> T searchObject(final List<T> list, final T obj ) {
		T result = null;
		int index = list.indexOf(obj);
		if ( index > -1 ) { 
			result = list.get(index);
		}
		return result;
	}
	
	public static <T> boolean isNotNullAndEmpty(List<T> list) { 
		return list != null && !list.isEmpty();
	} 
	
	
}
