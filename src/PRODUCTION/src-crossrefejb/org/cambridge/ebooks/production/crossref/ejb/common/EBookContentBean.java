package org.cambridge.ebooks.production.crossref.ejb.common;

import java.util.ArrayList;

import org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean;
import org.cambridge.ebooks.production.crossref.ejb.jpa.EBookContent;
import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefStringUtil;

public class EBookContentBean implements EBookContentComponent, Comparable<EBookContentBean> {
	
	private String fileName;
	private String bookId;
	private String contentId;
	private String doi;
	private String title;
	private String abstractText;
	private String abstractImage;
	private String position;
	private String type;
	private String pageStart;
	private String pageEnd;
	private String alphasort;
	private String label;
	private String parentId;
	private boolean indented;
	private boolean notLoadedPartTitle;
	private ArrayList<ContributorBean> chapterContributors;
	
	private EBookContent ebookContent;
	private boolean update;
	
	
	public EBookContentBean(){}
	public EBookContentBean(EBookContent ebookContent){
		this.ebookContent = ebookContent;
	}
	public EBookContentBean(String contentId) {
		this.contentId = contentId;
	}
	
	public boolean isUpdate() {
		return update;
	}
	public void setUpdate(boolean update) {
		this.update = update;
	}
	
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	
	public String getDoi() {
		return doi;
	} 
	
	public void setDoi(String doi) {
		this.doi = doi;
	}
	
	public String getTitle() {
		return CrossRefStringUtil.correctHtmlTags(title);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAbstractText() {
		return CrossRefStringUtil.correctHtmlTags(abstractText);
	}
	
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}
	
	public String getAbstractImage() {
		return abstractImage;
	}
	
	public void setAbstractImage(String abstractImage) {
		this.abstractImage = abstractImage;
	}
	
	public boolean equals(Object o) { 
		boolean result = false;
		
		if (o instanceof EBookContentComponent) {
			EBookContentComponent b = (EBookContentComponent) o;
			if( b.getContentId().equals(this.getContentId()) )
			{
				result = true;
			}
		} else if (o instanceof EBookContentBean) {
			EBookContentBean b = (EBookContentBean) o;
			if (b.getContentId().equals(this.getContentId())) {
				result = true;
			}
		}
		
		return result;
	}
	
	public int hashCode(){
		return this.getContentId().length();
	}

	
	public String getContentId() {
		return contentId;
	}
	
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public EBookContent getEbookContent() {
		return ebookContent;
	}
	
	public void setEbookContent(EBookContent ebookContent) {
		this.ebookContent = ebookContent;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPageStart() {
		return pageStart;
	}
	
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}
	
	public String getPageEnd() {
		return pageEnd;
	}
	
	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getAlphasort() {
		return CrossRefStringUtil.correctHtmlTags(alphasort);
	}
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}
	
	/**
	 * @return the indented
	 */
	public boolean isIndented() {
		return indented;
	}
	
	/**
	 * @param indented the indented to set
	 */
	public void setIndented(boolean indented) {
		this.indented = indented;
	}
	
	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}
	
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public int compareTo(EBookContentBean o) {
		return Integer.valueOf(this.position).compareTo(Integer.valueOf(o.getPosition()));		
	}
	
	/**
	 * @return the notLoadedPartTitle
	 */
	public boolean isNotLoadedPartTitle() {
		return notLoadedPartTitle;
	}
	
	/**
	 * @param notLoadedPartTitle the notLoadedPartTitle to set
	 */
	public void setNotLoadedPartTitle(boolean notLoadedPartTitle) {
		this.notLoadedPartTitle = notLoadedPartTitle;
	}
	
	public ArrayList<ContributorBean> getChapterContributors() {
		return chapterContributors;
	}
	public void setChapterContributors(ArrayList<ContributorBean> chapterContributors) {
		this.chapterContributors = chapterContributors;
	}
}
