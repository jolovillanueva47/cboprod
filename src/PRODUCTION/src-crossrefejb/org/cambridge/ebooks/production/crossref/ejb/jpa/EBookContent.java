package org.cambridge.ebooks.production.crossref.ejb.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentComponent;
import org.cambridge.ebooks.production.crossref.ejb.jpa.EBook;
import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefStringUtil;


/**
 * 
 * @author rvillamor
 *			   
 *
 */

//@NamedNativeQueries( {
//	
//	@NamedNativeQuery (
//			name = EBookContent.SEARCH_HEADER_AND_IMAGES_BY_BOOK_ID,
//			query = " SELECT * FROM ebook_content " +		 
//				 	" WHERE book_id = ?1 " +
//				 	" AND type IN('xml', 'image_thumb', 'image_standard')",
//			resultSetMapping = "searchContentResult"
//	),
//	
//	@NamedNativeQuery(
//			name  = EBookContent.SEARCH_CHAPTERS_BY_BOOK_ID,
//			query = " SELECT * FROM ebook_content  " + 
//					" WHERE book_id = ?1 " +
//					" AND type = 'chapter' ",
//			resultSetMapping =  "searchContentResult"
//	
//	),
//	
//	@NamedNativeQuery(
//			name  = EBookContent.SEARCH_BY_BOOK_ID,
//			query = " SELECT * FROM ebook_content  " + 
//					" WHERE book_id = ?1",
//			resultSetMapping =  "searchContentResult"
//	
//	),
//	
//	@NamedNativeQuery(
//			name  = "searchContentByBookIdSQL2",
//			query = "SELECT  ec.FILENAME, ec.CONTENT_ID, ec.BOOK_ID, ec.ISBN, ec.STATUS, ec.EMBARGO_DATE, ec.REMARKS, " +
//			        "ec.MODIFIED_DATE, ec.MODIFIED_BY, ec.TYPE, eb.PUB_DATE_ONLINE, eb.PUB_DATE_PRINT " +
//					"FROM ebook_content ec " +
//					"JOIN ebook eb " +
//					"ON (ec.book_id = eb.book_id) " +
//					"WHERE ec.book_id = ?1",
//			resultSetMapping =  "searchContentResult"
//	
//	),
//	
//	
//	@NamedNativeQuery(
//			name  = EBookContent.SEARCH_BY_BOOK_ID_AND_STATUS,
//			query = " SELECT * FROM ebook_content  " + 
//					" WHERE book_id = ?1 " +
//					" AND STATUS = ?2",
//			resultSetMapping =  "searchContentResult"
//	
//	),
//	
//	@NamedNativeQuery(
//			name  = EBookContent.SEARCH_BY_CONTENT_ID_AND_TYPE,
//			query = " SELECT * FROM ebook_content " + 
//					" WHERE content_id = ?1 " +
//					" AND TYPE = ?2",
//			resultSetMapping =  "searchContentResult"
//	
//	),
//	
//	@NamedNativeQuery(
//			name  = EBookContent.SEARCH_BY_CONTENT_ID,
//			query = " SELECT * FROM ebook_content " + 
//					" WHERE content_id = ?1",
//			resultSetMapping =  "searchContentResult"
//	),
//	
//	@NamedNativeQuery (
//			name = EBookContent.INSERT_CONTENT,
//			query = "INSERT INTO ebook_content" 
//					+ " VALUES(?1, ?2, ?3, ?4, NULL, NULL, ?5, ?6, ?7, ?8)"			
//	),
//	
//	@NamedNativeQuery(
//			name  = EBookContent.UPDATE_STATUS_BY_FILENAME,
//			query = "UPDATE ebook_content" 
//					+ " SET status = ?1, modified_date = ?2, modified_by = ?3" 
//					+ " WHERE filename = ?4"
//	
//	),
//	
//	@NamedNativeQuery (
//			name = EBookContent.SEARCH_OTHER_CONTENT_BY_BOOK_ID,
//			query = " SELECT * FROM ebook_content " +		 
//				 	" WHERE book_id = ?1 " +
//				 	" AND type NOT IN('xml', 'image_thumb', 'image_standard', 'chapter')",
//			resultSetMapping = "searchContentResult"
//	),
//	
//	@NamedNativeQuery (
//			name = EBookContent.UPDATE_BY_FILENAME,
//			query = "UPDATE ebook_content" 
//					+ " SET content_id = ?1, isbn = ?2, book_id = ?3, status = ?4, type = ?5, modified_date = ?6, modified_by = ?7" 
//					+ " WHERE filename = ?8"
//	),
//	
//	@NamedNativeQuery (
//			name = EBookContent.SEARCH_BY_FILENAME,
//			query = "SELECT * FROM ebook_content"	
//					+" WHERE filename = ?1",
//			resultSetMapping = "searchContentResult"
//	)
//	
//} )

@SqlResultSetMapping( 
	name = "searchContentResult",
	entities = {
		@EntityResult (
			entityClass = EBookContent.class,
			fields = {
				@FieldResult(name = "filename", column = "FILENAME"),
				@FieldResult( name = "contentId", column = "CONTENT_ID"),
				@FieldResult( name = "isbn",      column = "ISBN"),
				@FieldResult( name = "status",    column = "STATUS"),
				@FieldResult( name = "embargoDate",    column = "EMBARGO_DATE"),
				@FieldResult( name = "remarks", column = "REMARKS"),
				@FieldResult( name = "modifiedDate", column = "MODIFIED_DATE"),
				@FieldResult( name = "modifiedBy", column = "MODIFIED_BY"),
				@FieldResult(name = "type", column = "TYPE")
			}
		)
	}
 )

@Entity
@Table(name = "EBOOK_CONTENT")
public class EBookContent implements EBookContentComponent {
	
	public static final String SEARCH_CHAPTERS_BY_BOOK_ID = "searchChaptersByBookIdSQL";
	public static final String SEARCH_BY_BOOK_ID = "searchContentByBookIdSQL";
	public static final String SEARCH_BY_BOOK_ID_AND_STATUS = "searchContentByBookIdStatusSQL";
	public static final String SEARCH_BY_FILENAME = "searchByFilename";
	public static final String SEARCH_BY_CONTENT_ID_AND_TYPE = "searchByContentIdAndType";
	public static final String SEARCH_BY_CONTENT_ID = "searchByContentId";
	public static final String SEARCH_OTHER_CONTENT_BY_BOOK_ID = "searchOtherContentByBookId";
	public static final String SEARCH_HEADER_AND_IMAGES_BY_BOOK_ID = "searchHeaderAndImagesByBookIdSQL";
	
	public static final String UPDATE_BY_FILENAME = "updateByFilename";
	public static final String UPDATE_STATUS_BY_FILENAME = "updateStatusByFilename";
	
	public static final String INSERT_CONTENT = "insertContent";
	
	public static final String IMAGE_STANDARD = "image_standard";
	public static final String IMAGE_THUMB = "image_thumb";
	public static final String XML = "xml";
	
	@Column(name = "FILENAME")
	private String fileName;
	
	@Id
	@Column(name = "CONTENT_ID")
	private String contentId;
	
	@Column(name = "ISBN")
	private String isbn;
		
	@Column(name = "STATUS")
	private int status;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="EMBARGO_DATE")
    private Date embargoDate;
	
	@Column(name = "REMARKS")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "TYPE")
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "BOOK_ID", columnDefinition="VARCHAR2(20)", referencedColumnName="BOOK_ID")
	private EBook book;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getEmbargoDate() {
		return embargoDate;
	}

	public void setEmbargoDate(Date embargoDate) {
		this.embargoDate = embargoDate;
	}

	public String getRemarks() {
		if(CrossRefStringUtil.isEmpty(remarks))
			remarks = "";

		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public EBook getBook() {
		return book;
	}

	public void setBook(EBook book) {
		this.book = book;
	}
	
	public String getDisplayFileType() {
		String display;
		try {
			display = (fileName.substring( fileName.lastIndexOf(".")+1, fileName.length() )).toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();
			display = "unknown filetype";
		}
		return display;
	}

	public boolean equals(Object o) { 
		boolean result = false;
		
		if (o instanceof EBookContentComponent) {
			EBookContentComponent b = (EBookContentComponent) o;
			if( b.getContentId().equals(this.getContentId()) )
			{
				result = true;
			}
		}
		
		return result;
	}
	
	public int hashCode(){
		return this.getContentId().length();
	}

}
