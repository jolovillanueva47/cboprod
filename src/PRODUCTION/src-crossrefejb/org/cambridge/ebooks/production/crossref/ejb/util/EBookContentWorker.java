package org.cambridge.ebooks.production.crossref.ejb.util;

import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentDAO;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentIndexSearch;
import org.cambridge.ebooks.production.crossref.ejb.jpa.EBookContent;
import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefStringUtil;
import org.cambridge.ebooks.production.crossref.ejb.util.ListUtil;


/**
 * @author rvillamor
 *
 */
public class EBookContentWorker {
	
	public final static String UPDATE = "update";
	public final static String CHAPTER = "chapter";
	public final static String OTHER = "other";
	public final static String DISPLAY_BY_STATUS = "displayByStatus";
	
	private List<Integer> status = new ArrayList<Integer>();
	
	private EBookContentDAO dao;
	
	/**
	 * 
	 * Method to fetch content items from index and database by bookId
	 * list of content items from the index is merged with the content items from the database,
	 * if an item from the index is not in the database, it will not be included.
	 * 
	 * @param bookId
	 * 
	 * @return EBookContentBean List
	 */
	public List<EBookContentBean> ebookContents(String bookId, boolean includeChapterContributors){	
		System.out.println(EBookContentWorker.class + "---BOOK ID:" + bookId);
		
		List<EBookContentBean> ebookContentFromIndex = null;
		List<EBookContent> ebookContentFromDB = null;		
		
		ebookContentFromIndex = EBookContentIndexSearch.ebookContents(bookId,includeChapterContributors);
		ebookContentFromDB = getDao().searchContentsByBookId(bookId);		
			
		setContentProperties(ebookContentFromDB, ebookContentFromIndex);
		
		return ebookContentFromIndex;
	}
	

	@SuppressWarnings("unchecked")
	private void setContentProperties(List ebookContentList, List ebookContentBeanList) {
		
		try {
			if(ListUtil.isNotNullAndEmpty(ebookContentList) && ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
//				LogManager.info(EBookContentWorker.class, "[Number of EBook Contents] DB Size: "	+ ebookContentList.size() + " Index Size: " + ebookContentBeanList.size());
				
				ebookContentBeanList.retainAll(ebookContentList);
				if(ListUtil.isNotNullAndEmpty(ebookContentBeanList)) {
					
					String partId = "";
					for(Object ebookContentBean : ebookContentBeanList) {
						Object ebookContent = ListUtil.searchObject(ebookContentList, ebookContentBean);
						if(ebookContent != null) {
							EBookContent ebookContent_ = (EBookContent)ebookContent;	
							EBookContentBean ebookContentBean_ = ((EBookContentBean)ebookContentBean);
							ebookContentBean_.setEbookContent(ebookContent_);
							
							String label = ebookContentBean_.getLabel();							
							String title = ebookContentBean_.getTitle();							
							String ebookType = ebookContentBean_.getType();
							String newTitle = "";
							
							title = CrossRefStringUtil.correctHtmlTags(title);	
							
							if(!ebookType.equals("part-title") 
									&& partId.equals(ebookContentBean_.getParentId())) {
//								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
//								LogManager.info(EBookContentWorker.class, "---PARENT ID:" + ebookContentBean_.getParentId());
//								LogManager.info(EBookContentWorker.class, "---INDENTED: true");
								ebookContentBean_.setIndented(true);
							}
							
//							LogManager.info(EBookContentWorker.class, "---EBOOK TYPE:" + ebookType);
							
							if(ebookType.equals("part-title")) {										
								partId = ebookContentBean_.getContentId();
//								LogManager.info(EBookContentWorker.class, "---PART ID:" + partId);
								
								EBookContent ec = EBookContentDAO.searchContentByContentIdAndType(partId, ebookType);
								if(ec.getStatus() == -1) {
									ebookContentBean_.setNotLoadedPartTitle(true);
								} 						
							}
							
							if(label == null) {
//								newTitle = ebookContentBean_.getPosition() + ": " + title;
								newTitle = title;
							} else {
								newTitle = label + ": " + title;
							}
							ebookContentBean_.setTitle(newTitle);
						}
					}					
				}				
			}
		}
		catch(Exception e) {		
			System.out.println(EBookContentWorker.class + " setContentProperties(List, List): " + e.getMessage());
			e.printStackTrace();
		}
	}


	public EBookContentDAO getDao() {
		return dao;
	}


	public void setDao(EBookContentDAO dao) {
		this.dao = dao;
	}
	
	
	
}
