package org.cambridge.ebooks.production.crossref.ejb.xml;


public class Xml {

	private String version;
	private String encoding;
	
	public Xml(String version, String encoding) {
		this.version = version;
		this.encoding = encoding;
	}
	
	public String getVersion() {
		return version;
	}
	public String getEncoding() {
		return encoding;
	}
	
	
}
