package org.cambridge.ebooks.production.crossref.ejb.xml;


public class PersonName {

	private String sequence;
	private String contributorRole;
	private String givenName;
	private String surname;
	
	public PersonName(String sequence, String contributorRole, String givenName, String surname) {
		this.sequence = sequence;
		this.contributorRole = contributorRole;
		this.givenName = givenName;
		this.surname = surname;		
	}
	
	public String getSequence() {
		return sequence;
	}
	public String getContributorRole() {
		return contributorRole;
	}
	public String getGivenName() {
		return givenName;
	}
	public String getSurname() {
		return surname;
	}
}
