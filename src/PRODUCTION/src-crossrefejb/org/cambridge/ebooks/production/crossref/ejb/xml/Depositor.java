package org.cambridge.ebooks.production.crossref.ejb.xml;

public class Depositor {

	private String depositorName;
	private String depositorEmail;
	
	public Depositor(String depositorName, String depositorEmail) {
		this.depositorName = depositorName;
		this.depositorEmail = depositorEmail;
	}
	
	public String getDepositorName() {
		return depositorName;
	}
	public String getDepositorEmail() {
		return depositorEmail;
	}
}
