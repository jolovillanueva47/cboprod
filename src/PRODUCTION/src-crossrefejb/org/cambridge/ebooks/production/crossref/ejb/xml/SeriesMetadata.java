package org.cambridge.ebooks.production.crossref.ejb.xml;

import java.util.ArrayList;


public class SeriesMetadata {

	private ArrayList<String> titles;
	private String isbn;
	private DoiData doiData;
	
	public SeriesMetadata(ArrayList<String> titles, String isbn, DoiData doiData) {
		this.titles = titles;
		this.isbn = isbn;
		this.doiData = doiData;
	}

	public ArrayList<String> getTitles() {
		return titles;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public DoiData getDoiData() {
		return doiData;
	}
}
