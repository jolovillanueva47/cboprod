package org.cambridge.ebooks.production.crossref.ejb.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.lucene.document.Document;
import org.cambridge.ebooks.production.crossref.ejb.document.ContributorBean;
import org.cambridge.ebooks.production.crossref.ejb.util.EBooksConfiguration;
import org.cambridge.ebooks.production.crossref.ejb.util.EBooksProperties;
import org.cambridge.ebooks.production.crossref.ejb.util.IndexSearchUtil;


/**
 * @author rvillamor
 *
 */
public class EBookContentIndexSearch {
	
	public static EBookContentBean ebookContents(final String bookId, boolean includeChapterContributors, int page) {
		
		EBookContentBean ebookContentBean = null;
		
		try 
		{
			String indexDir = EBooksConfiguration.getProperty(EBooksProperties.INDEX);
			StringBuilder queryStr = new StringBuilder("BOOK_ID:" + bookId + " AND ELEMENT:content-item");
			
			Document doc = IndexSearchUtil.searchIndex(indexDir, queryStr.toString(), page);

			if(doc != null)
			{
				ebookContentBean = new EBookContentBean();
				
				ebookContentBean.setContentId(doc.get("ID"));
				ebookContentBean.setFileName(doc.get("PDF"));
				ebookContentBean.setTitle(doc.get("TITLE"));
				ebookContentBean.setDoi(doc.get("DOI"));
				ebookContentBean.setAbstractText(doc.get("ABSTRACT"));
				ebookContentBean.setAbstractImage(doc.get("ALT_FILENAME"));
				ebookContentBean.setType(doc.get("TYPE"));
				ebookContentBean.setPosition(doc.get("POSITION"));
				ebookContentBean.setPageStart(doc.get("PAGE_START"));
				ebookContentBean.setPageEnd(doc.get("PAGE_END"));
				ebookContentBean.setPageEnd(doc.get("PAGE_END"));
				ebookContentBean.setAlphasort(doc.get("ALPHASORT"));
				ebookContentBean.setLabel(doc.get("LABEL"));
				ebookContentBean.setParentId(doc.get("PARENT_ID"));
				if(includeChapterContributors){
					ebookContentBean.setChapterContributors(getChapterContributors(doc.get("ID")));
				}
			}
			
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			System.out.println(EBookContentIndexSearch.class + " [Exception]: ebookContents(String, boolean, int) "	+ e.getMessage());
		}

		return ebookContentBean;
	}
	
	public static List<EBookContentBean> ebookContents(final String bookId, boolean includeChapterContributors) {
		List<EBookContentBean> result = new ArrayList<EBookContentBean>();

		try 
		{
			String indexDir = EBooksConfiguration.getProperty(EBooksProperties.INDEX);
			StringBuilder queryStr = new StringBuilder("BOOK_ID:" + bookId + " AND ELEMENT:content-item");
			
			List<Document> docList = IndexSearchUtil.searchIndex(indexDir, queryStr.toString());
			for (Document doc : docList) 
			{
				EBookContentBean ebookContentBean = new EBookContentBean();
				
				ebookContentBean.setContentId(doc.get("ID"));
				ebookContentBean.setFileName(doc.get("PDF"));
				ebookContentBean.setTitle(doc.get("TITLE"));
				ebookContentBean.setDoi(doc.get("DOI"));
				ebookContentBean.setAbstractText(doc.get("ABSTRACT"));
				ebookContentBean.setAbstractImage(doc.get("ALT_FILENAME"));
				ebookContentBean.setType(doc.get("TYPE"));
				ebookContentBean.setPosition(doc.get("POSITION"));
				ebookContentBean.setPageStart(doc.get("PAGE_START"));
				ebookContentBean.setPageEnd(doc.get("PAGE_END"));
				ebookContentBean.setPageEnd(doc.get("PAGE_END"));
				ebookContentBean.setAlphasort(doc.get("ALPHASORT"));
				ebookContentBean.setLabel(doc.get("LABEL"));
				ebookContentBean.setParentId(doc.get("PARENT_ID"));
				if(includeChapterContributors){
					ebookContentBean.setChapterContributors(getChapterContributors(doc.get("ID")));
				}
				
				result.add(ebookContentBean);	
				// Sort list base on position: Ascending
				Collections.sort(result);
			}

		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			System.out.println(EBookContentIndexSearch.class + " [Exception]: ebookContents() "	+ e.getMessage());
		}

		return result;
	}
	
	
	public static ArrayList<ContributorBean> getChapterContributors(String contentId){
		ArrayList<ContributorBean> contributors = new ArrayList<ContributorBean>();
		
		String indexDir = EBooksConfiguration.getProperty(EBooksProperties.INDEX);
		StringBuilder queryStr = new StringBuilder("PARENT_ID:" + contentId);
		queryStr
		.append(" AND (PARENT:content-item OR PARENT:heading OR PARENT:contributor-group)");
		
		List<Document> docList = IndexSearchUtil.searchIndex(indexDir, queryStr.toString());
		ContributorBean cb;
		for (Document doc : docList) 
		{
			if(doc.get("CONTRIBUTOR") != null){
				cb = new ContributorBean();
				cb.setContributor(doc.get("CONTRIBUTOR"));
				cb.setPosition(doc.get("POSITION"));
				contributors.add(cb);
			}
		}
		
		return contributors;
	}
}
