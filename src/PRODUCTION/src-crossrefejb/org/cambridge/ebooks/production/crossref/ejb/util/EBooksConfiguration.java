package org.cambridge.ebooks.production.crossref.ejb.util;


/**
 * 
 * @author rvillamor
 *
 */

public class EBooksConfiguration {

	public static String getProperty(String propertyName){
		String result = "";
		
		if ( CrossRefStringUtil.isEmpty( System.getProperty(propertyName)) )
		{			
			System.out.println(EBooksConfiguration.class + " Property name: " + propertyName + " has not been set on property-service.xml");
		}
		else
		{
			result = System.getProperty(propertyName).trim();
		}
		
		return result;
		
	}
	
}
