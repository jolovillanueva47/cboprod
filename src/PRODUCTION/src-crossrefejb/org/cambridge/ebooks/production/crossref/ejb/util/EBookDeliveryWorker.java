package org.cambridge.ebooks.production.crossref.ejb.util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

import org.cambridge.ebooks.production.crossref.ejb.util.CrossRefDeliveryWorker;
import org.cambridge.ebooks.production.crossref.ejb.common.EBookContentBean;
import org.cambridge.ebooks.production.crossref.ejb.jpa.DoiBatch;
import org.cambridge.ebooks.production.crossref.ejb.jpa.ThirdPartyDeliveryStatus;
import org.cambridge.ebooks.production.crossref.ejb.util.XMLParser;


public class EBookDeliveryWorker {
	
	private static EntityManagerFactory emf; //= Persistence.createEntityManagerFactory( PersistentUnits.EBOOKS.toString() );
	private UserTransaction userTransaction;
	
	public static final String XML_ONLY = "xmlOnly";
	public static final String XML_AND_SEND = "xmlSend";
	
	public static final String DELIVER_SUCCESSFUL = " The contents of the e-book will be delivered to the recipients in a few moments.";

	public String deliverEBook(
			String bookId,
			String eisbn,
			String username,
			List<EBookContentBean> ebookContentList,
			boolean deliverToCrossRef,
			String deliveryOption) {
		
		String crossRefXmlFileName = null;
		Date date = new Date();
		
		EntityManager em = emf.createEntityManager();
		try
		{
			DoiBatch db = new DoiBatch();
			db.setCreatedDate(new Date());
			db.setCreatedBy(username);
			
			getUserTransaction().begin();
			em.persist(db);
			getUserTransaction().commit();

			boolean withError = false;
			String errorMessage = "";
			if (deliverToCrossRef) {  
				crossRefXmlFileName = CrossRefDeliveryWorker.processEBookXml(bookId, eisbn, ebookContentList, username, db.getDoiBatchId(), date);
				XMLParser parser = new XMLParser();
				errorMessage = parser.validatateCrossRef(eisbn, crossRefXmlFileName);
				withError = errorMessage != null && !"".equals(errorMessage);

				if (deliveryOption.equals(EBookDeliveryWorker.XML_AND_SEND)) {
					
					if (!withError) {
						int responseCode = CrossRefDeliveryWorker.sendDeposit(eisbn, crossRefXmlFileName);
						withError = !(responseCode==200);
					}
					
					ThirdPartyDeliveryStatus deliveryStatus = new ThirdPartyDeliveryStatus();
					deliveryStatus.setThirdPartyId(ThirdPartyDeliveryStatus.THIRD_PARTY_ID_CROSSREF);
					deliveryStatus.setContentId(bookId);
					deliveryStatus.setFileType(ThirdPartyDeliveryStatus.FILE_TYPE_XML);
					deliveryStatus.setDepositor(username);
					deliveryStatus.setStatus(withError);
					deliveryStatus.setDeliverDate(date);
					deliveryStatus.setQueueDate(null);
					deliveryStatus.setReason(withError ? null : errorMessage);
					deliveryStatus.setDoiBatchId(db.getDoiBatchId());

					getUserTransaction().begin();
					em.persist(deliveryStatus);
					getUserTransaction().commit();
				}
			}

			if (withError && errorMessage.length()>0) {
				System.out.println("[EBookDeliveryWorker] deliverEBook(): Error writing XML: " + errorMessage);
			} else if (deliveryOption.equals(EBookDeliveryWorker.XML_AND_SEND)) {		
				System.out.println("[EBookDeliveryWorker]: " + EBookDeliveryWorker.DELIVER_SUCCESSFUL);
			}
		}
		catch(Exception ex)
		{
			System.out.println("[ERROR]: " + ex.getMessage());
		}
		finally
		{
			em.close();
		}

		return crossRefXmlFileName;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		EBookDeliveryWorker.emf = emf;
	}

	public UserTransaction getUserTransaction() {
		return userTransaction;
	}

	public void setUserTransaction(UserTransaction userTransaction) {
		this.userTransaction = userTransaction;
	}
}
