package org.cambridge.ebooks.production.crossref.ejb.xml;

import java.util.ArrayList;


public class Book {
	private String bookType;
	private BookMetadata bookMetadata;
	private ArrayList<ContentItem> contentItems;
	
	public Book(String bookType, BookMetadata bookMetadata, ArrayList<ContentItem> contentItems) {
		this.bookType = bookType;
		this.bookMetadata = bookMetadata;
		this.contentItems = contentItems;
	}

	public String getBookType() {
		return bookType;
	}

	public BookMetadata getBookMetadata() {
		return bookMetadata;
	}

	public ArrayList<ContentItem> getContentItems() {
		return contentItems;
	}
}
