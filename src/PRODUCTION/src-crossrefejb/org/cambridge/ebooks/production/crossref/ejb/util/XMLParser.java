package org.cambridge.ebooks.production.crossref.ejb.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class XMLParser extends DefaultHandler implements ErrorHandler {

	private StringBuilder message = new StringBuilder("");
	
	public void error(SAXParseException arg) throws SAXException {
		message.append(arg.getMessage()).append(" (Line: ").append(arg.getLineNumber()).append(")").append("\n");
	}

	public String validatatePubMed(String file) {
		String error = null;
		try {
			System.getProperties().setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(this);
			reader.parse(file);
			
		} catch (Exception e) {
			message.append(e.getMessage());
		} finally {
			error = message.toString();
		}
		return error;
	}

	public String validatateCrossRef(String eisbnFolder, String file) {
		String error = null;
		try {

	        SchemaFactory factory = SchemaFactory.newInstance(CrossRefProperties.SCHEMA_FACTORY);
	        factory.setErrorHandler(this);
	        
	        URL schemaLocation = new URL(CrossRefProperties.SCHEMA_URL);
	        Schema schema = factory.newSchema(schemaLocation);
	    
	        Validator validator = schema.newValidator();
	        InputStream stream = new FileInputStream(new File(
	        		CrossRefProperties.XML_DIR
	        		+ eisbnFolder + "/"
	        		+ file));
	        Source source = new StreamSource(stream);
	        
            validator.validate(source);
		    
		} catch(SAXParseException e) {
			StringBuilder errorMessage = new StringBuilder(
					e.getMessage())
						.append(" (Line: ")
						.append(e.getLineNumber())
						.append(", Column: ")
						.append(e.getColumnNumber())
						.append(")");
			System.out.println(XMLParser.class + " " + errorMessage.toString());
			message.append(errorMessage.append("\n"));
		} catch (Exception e) {
			message.append(e.getMessage()).append("\n");
			System.out.println(XMLParser.class + " " + e.toString());
		} finally {
			error = message.toString();
		}
		return error;
	}
}
