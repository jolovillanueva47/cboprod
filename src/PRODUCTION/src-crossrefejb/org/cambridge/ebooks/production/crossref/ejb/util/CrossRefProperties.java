package org.cambridge.ebooks.production.crossref.ejb.util;

import static org.cambridge.ebooks.production.crossref.ejb.util.EBooksConfiguration.getProperty;

public interface CrossRefProperties {
	
	public static final String XML_VERSION = getProperty("crossref.xml.version");
	public static final String XML_ENCODING = getProperty("crossref.xml.encoding");
	public static final String DOIBATCH_VERSION = getProperty("crossref.doibatch.version");
	public static final String DOIBATCH_XMLNS = getProperty("crossref.doibatch.xmlns");
	public static final String DOIBATCH_XMLNSXSI = getProperty("crossref.doibatch.xmlnsxsi");
	public static final String DOIBATCH_XSISCHEMALOC = getProperty("crossref.doibatch.xsischemaloc");
	
	public static final String DEPOSITOR_EMAIL = getProperty("crossref.depositor.email");
	public static final String XML_DIR = getProperty("crossref.xml.dir");
	public static final String DELIVERY_TEMPLATE = getProperty("crossref.delivery.template");
	public static final String SCHEMA_FACTORY = getProperty("crossref.schema.factory");
	public static final String SCHEMA_URL = getProperty("crossref.schema.url");
	public static final String USERNAME = getProperty("crossref.username");
	public static final String PASSWORD = getProperty("crossref.password");
	public static final String DEFAULT_REGISTRANT = getProperty("crossref.default.registrant");
	public static final String ORG_SITE = getProperty("crossref.org.site");
	public static final String ORG_PORT = getProperty("crossref.org.port");
	public static final String ORG_TEST_AREA = getProperty("crossref.org.test.area");
	public static final String ORG_FUZZY = getProperty("crossref.org.fuzzy");
	public static final String UPLOAD_SITE = getProperty("crossref.upload.site");
	
	public static final String WEB_DIR = getProperty("crossref.web.dir");
}
