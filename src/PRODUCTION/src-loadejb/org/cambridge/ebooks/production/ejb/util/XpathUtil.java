package org.cambridge.ebooks.production.ejb.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.ejb.ContentsDbUploader;
import org.cambridge.ebooks.production.ejb.vo.InsertItem;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Karlson A. Mulingtapang
 * XpathUtil.java - XPATH related functionalities
 */
public class XpathUtil {
	
	public static void processMainTitleNodeList(NodeList nList, ContentsDbUploader contentsDbUploader) {
		String mainTitle = "";
		String alphaSort = "";
		for(int index=0; index<nList.getLength(); index++){
			Node node = nList.item(index);
			
			if(node != null && node.hasAttributes()){
				if("main-title".equalsIgnoreCase(node.getNodeName())){
					
//					mainTitle = StringEscapeUtils.escapeSql(cleanTextNode(getChildNodeTree("main-title", node))); //.replaceAll("'", "''");
					mainTitle = cleanTextNode(getChildNodeTree("main-title", node));
					Logger.getLogger(XpathUtil.class).info("**+++mainTitle = "+mainTitle);
					contentsDbUploader.setBookTitle(mainTitle);
					
					if(node.hasAttributes()){
						NamedNodeMap nmap = node.getAttributes();
//						alphaSort = StringEscapeUtils.escapeSql(getNodeTextContent(nmap.getNamedItem("alphasort"))); //.replaceAll("'", "''");
						alphaSort = getNodeTextContent(nmap.getNamedItem("alphasort"));
						contentsDbUploader.setBookTitleAlphasort(alphaSort);
					}
				}
				
			}
		}
	}
	
	public static ArrayList<InsertItem> processInsertNodeList(NodeList nList, ContentsDbUploader contentsDbUploader) {
		ArrayList<InsertItem> InsertItemList = new ArrayList<InsertItem>();
		String id = "";
		String filename = "";
		Logger.getLogger(XpathUtil.class).info("***BEGINNING INSERT NODE PROCESSING***");
		
		for(int index=0; index<nList.getLength(); index++){
			Node node = nList.item(index);
			InsertItem insert = new InsertItem();
			if(node != null && node.hasAttributes()){
				if("insert".equalsIgnoreCase(node.getNodeName())){
					
					if(node.hasAttributes()){
						NamedNodeMap nmap = node.getAttributes();
//						alphaSort = StringEscapeUtils.escapeSql(getNodeTextContent(nmap.getNamedItem("alphasort"))); //.replaceAll("'", "''");
						filename = getNodeTextContent(nmap.getNamedItem("filename"));
						Logger.getLogger(XpathUtil.class).info("**** insertFilename: "+filename);
						id = getNodeTextContent(nmap.getNamedItem("id"));
						Logger.getLogger(XpathUtil.class).info("**** insertId: "+id);
						
						insert.setFilename(filename);
						insert.setId(id);
						
					}
				}
				
			}
			InsertItemList.add(insert);
		}
		Logger.getLogger(XpathUtil.class).info("***COMPLETED INSERT NODE PROCESSING***");
		return InsertItemList;
	}
	
	public static void displayInsertNodeData(ArrayList<InsertItem> list){
		for(InsertItem item:list){
			Logger.getLogger(XpathUtil.class).info("insertFilename: "+item.getFilename());
			Logger.getLogger(XpathUtil.class).info("insertId: "+item.getId());
			Logger.getLogger(XpathUtil.class).info("insertMainTitle: "+item.getTitle());
			Logger.getLogger(XpathUtil.class).info("insertAlphaSort: "+item.getAlphasort());
		}
	}
	
	public static void processInsertTitleNodeList(NodeList nList, ContentsDbUploader contentsDbUploader, ArrayList<InsertItem> insertItemList) {
		String mainTitle = "";
		String alphasort = "";
		Logger.getLogger(XpathUtil.class).info("***BEGINNING INSERT TITLE NODE PROCESSING***");
		
		for(int index=0; index<nList.getLength(); index++){
			Node node = nList.item(index);
			
			if(node != null && node.hasAttributes()){
				if("title".equalsIgnoreCase(node.getNodeName())){
					
//					mainTitle = StringEscapeUtils.escapeSql(cleanTextNode(getChildNodeTree("main-title", node))); //.replaceAll("'", "''");
					mainTitle = cleanTextNode(getChildNodeTree("title", node));
					Logger.getLogger(XpathUtil.class).info("**** insertMainTitle: "+mainTitle);
					
//					if(node.hasChildNodes()){
//						NodeList child = node.getChildNodes();
//						Node titleNode = child.item(0);
//						mainTitle = cleanTextNode(getChildNodeTree("title", titleNode));
//						alphaSort = getNodeTextContent(titleNode.getAttributes().getNamedItem("alphasort"));
//						Logger.getLogger(XpathUtil.class).info("**** insertMainTitle: "+mainTitle);
//						Logger.getLogger(XpathUtil.class).info("**** insertAlphaSort: "+alphaSort);
//					}
					
					if(node.hasAttributes()){
						NamedNodeMap nmap = node.getAttributes();
//						alphaSort = StringEscapeUtils.escapeSql(getNodeTextContent(nmap.getNamedItem("alphasort"))); //.replaceAll("'", "''");
						alphasort = getNodeTextContent(nmap.getNamedItem("alphasort"));
//						contentsDbUploader.setBookTitleAlphasort(alphaSort);
						Logger.getLogger(XpathUtil.class).info("**** insertAlphaSort: "+alphasort);
						
					}
					insertItemList.get(index).setTitle(mainTitle);
					insertItemList.get(index).setAlphasort(alphasort);
				}
				
			}
		}
		contentsDbUploader.setInsertItemList(insertItemList);
		Logger.getLogger(XpathUtil.class).info("***COMPLETED INSERT TITLE NODE PROCESSING***");
	}

	
	public static String processContentTitle(NodeList nList) {
		String contentTitle = "";
		for(int index=0; index<nList.getLength(); index++){
			Node node = nList.item(index);
			
			if(node != null && node.hasAttributes()){
				if("title".equalsIgnoreCase(node.getNodeName())){
					contentTitle = cleanTextNode(getChildNodeTree("title", node));
					contentTitle = StringEscapeUtils.escapeSql(contentTitle);//.replaceAll("'", "''");
//					if(node.hasAttributes()){
//						NamedNodeMap nmap = node.getAttributes();
//						contentsDbUploader.setBookTitleAlphasort((getNodeTextContent(nmap.getNamedItem("alphasort"))));
//					}
				}
				
			}
		}		
		return contentTitle;
	}
	
	private static String getNodeTextContent(Node node){
		try {
			String content = node.getTextContent();
			return content;
		}catch(Exception ex) {
			return "";
		}
	}
	
	private static String cleanTextNode(String val) {
		if (Misc.isNotEmpty(val)) {
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll("\\s+", " ");  //.trim()
		}
		return val;
	}
	
	private static String getChildNodeTree(final String parentName, Node node) {
		
		StringBuilder result = new StringBuilder();
		StringBuilder attributes = new StringBuilder();
		String root = null;
		
		if(node != null && node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			if(node.hasAttributes() && !excludeTags().contains(node.getNodeName())) {
				NamedNodeMap nmap = node.getAttributes();
				for(int index=0; index<nmap.getLength(); index++) {
					Node attributeNode = nmap.item(index);
					if(attributeNode != null && !attributeNode.getNodeName().equals("xmlns:xml")) {
						attributes.append(" ").append(attributeNode.getNodeName()).append("=\"").append(attributeNode.getTextContent()).append("\"");						
					}
				}
			}
			
			root = node.getNodeName();
			
			for(int index=0; index<nodeList.getLength(); index++)
			{
				Node nodeFromList = nodeList.item(index);	
				if(nodeFromList != null )
				{
					
					if(!nodeFromList.hasChildNodes())
					{
						result.append(nodeFromList.getTextContent());
						Logger.getLogger(XpathUtil.class).info("result from extraction 1 = " + result);
					}
					else
					{
						result.append(getChildNodeTree(parentName, nodeFromList));
						Logger.getLogger(XpathUtil.class).info("result from extraction 2 = " + result);
					}
				}
			}
			
			if(root != null && !root.equalsIgnoreCase(parentName) && !excludeTags().contains(root)) {
				result.insert(0, "[LTHAN]" + root + attributes.toString() + "[GTHAN]").append("[LTHAN]/" + root + "[GTHAN]");	
			}
			
		}
		
		Logger.getLogger(XpathUtil.class).info("result before if = " + result);
		
		if(node != null && node.getNodeName().equals(parentName)) {
			Logger.getLogger(XpathUtil.class).info("in if");
			result = new StringBuilder(result.toString().replace("[LTHAN]", "<").replace("[GTHAN]", ">"));
			result = new StringBuilder(StringUtil.convertToUTF8(result.toString()));
			result = new StringBuilder(StringUtil.correctHtmlTags(result.toString()));
		}
		
		Logger.getLogger(XpathUtil.class).info("result after if = " + result);
		
		return result.toString();		
	}
	
	private static List<String> excludeTags() {
		String[] tags = {"book-title", "journal-title", "chapter-title",
				         "volume", "issue", "article-title", "startpage",
				         "author", "name", "forenames", "surname", "name-link",
				         "suffix", "collab", "affiliation",	"publisher-name", 
				         "publisher-loc", "year" };
		
		return Arrays.asList(tags);
	}
}
