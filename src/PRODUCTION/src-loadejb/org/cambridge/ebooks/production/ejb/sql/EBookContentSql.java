package org.cambridge.ebooks.production.ejb.sql;

public enum EBookContentSql {	
	INSERT_CONTENT("INSERT INTO ebook_content VALUES(?1, ?2, ?3, ?4, NULL, NULL, ?5, ?6, ?7, ?8)"),
	UPDATE_STATUS_BY_CONTENT_ID("UPDATE ebook_content" 
			+ " SET status = ?1, modified_date = ?2, modified_by = ?3" 
			+ " WHERE content_id = ?4"),
	UPDATE_BY_CONTENT_ID("UPDATE ebook_content" 
			+ " SET filename = ?1, isbn = ?2, book_id = ?3, status = ?4, type = ?5, modified_date = ?6, modified_by = ?7" 
			+ " WHERE content_id = ?8"),
	SEARCH_BY_CONTENT_ID("SELECT * FROM ebook_content"	
			+ " WHERE content_id = ?1"),
	SEARCH_BY_BOOK_ID("SELECT * FROM ebook_content WHERE book_id = ?1");
	
	private String sql;
	
	private EBookContentSql(String sql) {
		this.sql = sql;
	}
	
	public String toString() {
		return this.sql;
	}	
}
