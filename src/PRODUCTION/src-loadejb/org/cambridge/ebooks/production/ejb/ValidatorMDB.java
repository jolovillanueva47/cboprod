package org.cambridge.ebooks.production.ejb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.production.ejb.audittrail.AuditTrail;
import org.cambridge.ebooks.production.ejb.audittrail.EventTracker;
import org.cambridge.ebooks.production.ejb.common.LoadEJBStatus;
import org.cambridge.ebooks.production.ejb.constant.Loader;
import org.cambridge.ebooks.production.ejb.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.ejb.util.XmlValidator;
import org.cambridge.ebooks.production.ejb.util.XpathUtil;
import org.cambridge.ebooks.production.ejb.vo.ContentItem;
import org.cambridge.ebooks.production.ejb.vo.ImageFile;
import org.cambridge.ebooks.production.ejb.vo.InsertItem;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.cambridge.util.Misc;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REVISIONS:
 * 20131202 - jmendiola - Added CBML handling for eDictionaries
 * 20140205 - jmendiola - added handling for content-item/html filename (for eDictionaries)
 * 20140214 - alacerna -  HTML Handling
 * 20140310 - jmendiola - removed document parsing from getNodeList and moved it to the parent method. 
 */
@MessageDriven(name = "ValidatorMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "12000"), // fix for heap out of memory issue...
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/uploader/logger") })


public class ValidatorMDB implements MessageListener {
	
	private static final Log4JLogger LogManager = new Log4JLogger(ValidatorMDB.class); // 2012-11-16 Jubs
	
	private static final long serialVersionUID = 1L;
	
	public static final String CHAPTER = "chapter";
	

	@PersistenceContext(unitName="UploaderService")
	private EntityManager em;
	
	private ContentsDbUploader contentsDbUploader;
	private XmlValidator xmlValidator;
	
	public void onMessage(Message recvMsg) {
		System.out.println("[INFO] onMessage called...");
		try {
			if (recvMsg instanceof Message) {		
				contentsDbUploader = new ContentsDbUploader();
				contentsDbUploader.setEm(em);
				Message message = (Message) recvMsg;
				
				LogManager.info(" Uploader EJB onMessage called " );
				
				String filename = message.getStringProperty("FILENAME");					
				String userid = message.getStringProperty("USERID");				
				String username = message.getStringProperty("USERNAME");	
				LogManager.info("----FILENAME:" + filename);
				LogManager.info("----USERID:" + userid);
				LogManager.info("----USERNAME:" + username);
							
				validate(filename, userid, username);
			}
		} catch (JMSException exc) {
			LogManager.info("[JMSException]:Uploader EJB:" + exc.getMessage());
			exc.printStackTrace();
		}
	}
	
	private void validate(String filename, String userid, String username) {		
		String manifest = "";
		
		String dummyDTD = IsbnContentDirUtil.getPath(getHeaderFilename(filename, false)).substring(0, IsbnContentDirUtil.getPath(getHeaderFilename(filename, false)).length() - 2)+"header.dtd";
		copyfile(Loader.BASE_CONTENT_PATH.trim()+"header.dtd", dummyDTD);
		LogManager.info("Creating dummy DTD from "+ Loader.BASE_CONTENT_PATH.trim()+"header.dtd");
		File toDelete = new File (dummyDTD);
		
		try {	
			String headerFilePath = IsbnContentDirUtil.getPath(filename) + getHeaderFilename(filename, false);
			xmlValidator = XmlValidator.newInstance();
			xmlValidator.setXmlFilePath(headerFilePath);
			xmlValidator.setDtdFilePath(Loader.DTD_FILE_PATH);
			xmlValidator.validate();
			
			//Parse document once
			DocumentBuilderFactory domFactory = null;		
			DocumentBuilder builder = null;		
			Document doc = null;

			domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true);
			
			builder = domFactory.newDocumentBuilder();
			doc = builder.parse(getHeaderFilePath(filename));			
					
			// insert item badly coded data extraction
			NodeList insertNode = getNodeList(Loader.XPATH_BOOK_INSERT, doc);
			NodeList insertTitleNode = getNodeList(Loader.XPATH_BOOK_INSERT_TITLE, doc);
			ArrayList<InsertItem> InsertItemList = new ArrayList<InsertItem>();
			InsertItemList = XpathUtil.processInsertNodeList(insertNode, contentsDbUploader);
			XpathUtil.processInsertTitleNodeList(insertTitleNode, contentsDbUploader, InsertItemList);
			XpathUtil.displayInsertNodeData(InsertItemList);
			
			// print to file
			StringBuffer respOut = new StringBuffer();
			respOut.append(filename + " has been loaded to server." + Loader.NEW_LINE);
			respOut.append(checkContentItemNodes(contentsDbUploader, filename, manifest, doc));
			respOut.append(Loader.NEW_LINE);
			respOut.append(checkCoverImageNodes(contentsDbUploader, filename, manifest, doc));
			respOut.append(Loader.NEW_LINE);
			respOut.append(checkInsertItemNodes(contentsDbUploader, filename, manifest, InsertItemList));
			respOut.append(Loader.SEPARATOR);
			respOut.append(Loader.NEW_LINE);
			putManifest(filename);
			putFileOutput(respOut.toString(), getValidationOutputPath(filename));		
			putFileOutput(getContentItemsList(filename), getContentItemsOutputPath(filename));
			
			
			//Create Audit Trail
			EventTracker.sendEvent(AuditTrail.LOAD_DATA_FILES, userid, filename+".zip" );
			
			// database process
			NodeList bookIdNode = getNodeList(Loader.XPATH_BOOK_ID, doc);
			NodeList bookTitleNode = getNodeList(Loader.XPATH_BOOK_TITLE, doc);
			NodeList eIsbnNode = getNodeList(Loader.XPATH_EISBN, doc);
			NodeList seriesNode = getNodeList(Loader.XPATH_SERIES_CODE, doc);
			
			
			// book id
			contentsDbUploader.setBookId(bookIdNode.item(0).getNodeValue());
			XpathUtil.processMainTitleNodeList(bookTitleNode, contentsDbUploader);
			
			// isbn
			contentsDbUploader.setEIsbn(eIsbnNode.item(0).getNodeValue());
			// series code
			if(seriesNode != null && seriesNode.getLength() > 0) {
				contentsDbUploader.setSeriesId(seriesNode.item(0).getNodeValue());
			} else {
				contentsDbUploader.setSeriesId(null);
			}			
			
			NodeList bookDoi = getNodeList(Loader.XPATH_BOOK_DOI, doc);
			LogManager.info("bookDoi.getLength() = "+ bookDoi.getLength());
			// doi
			if(bookDoi.getLength() > 0)
				contentsDbUploader.setBookDoi(bookDoi.item(0).getNodeValue());
			
			//check if CBML exists 
			LogManager.info("Check if CBML exists..");
			String cbmlFilename = getCbmlFilename(filename, false);
			File cbmlFile = new File(IsbnContentDirUtil.getPath(getHeaderFilename(filename, false))+cbmlFilename);
			if(cbmlFile.isFile()){	
				contentsDbUploader.setCbmlFilename(cbmlFilename);
				LogManager.info("CBML Found!");
				
				String cbmlNavFilename = getBookNavHtmlFilename(doc);
				if (cbmlNavFilename != null && cbmlNavFilename.length() > 0) {
					LogManager.info("Found book HTML nav-file");
					contentsDbUploader.setNavHtmlFilename(cbmlNavFilename);
				} else {
					LogManager.info("No HTML nav-file found.");
				}
			} else {
				contentsDbUploader.setCbmlFilename("none");
				LogManager.info("No CBML Content!");
			}
			//check if BITS exist
			LogManager.info("Check if BITS exists..");
			String bitsFilename = getBitsFilename(filename, false);
			File bitsFile = new File(IsbnContentDirUtil.getPath(getHeaderFilename(filename, false))+bitsFilename);
			if(bitsFile.isFile()){	
				contentsDbUploader.setBitsFilename(bitsFilename);
				LogManager.info("BITS Found!");
								
				String bitsNavFilename = getBookNavHtmlFilename(doc);
				if (bitsNavFilename != null && bitsNavFilename.length() > 0) {
					LogManager.info("Found book HTML nav-file");
					contentsDbUploader.setNavHtmlFilename(bitsNavFilename);
				} else {
					LogManager.info("No HTML nav-file found.");
				}				
			} else {
				contentsDbUploader.setBitsFilename("none");
				LogManager.info("No BITS Content!");
			}
			
			
			contentsDbUploader.setFilename(filename);
			contentsDbUploader.setUserid(userid);
			contentsDbUploader.setUsername(username);
			contentsDbUploader.loadData();
		} catch (XPathExpressionException e) {
			putFileOutput(getFormatedExceptionMessage(e, filename), getValidationOutputPath(filename));
			LogManager.info(e.getMessage());
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			putFileOutput(getFormatedExceptionMessage(e, filename), getValidationOutputPath(filename));
			LogManager.info("[ParserConfigurationException]:" + e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			putFileOutput(getFormatedExceptionMessage(e, filename), getValidationOutputPath(filename));
			LogManager.info("[SAXException]:" + e.getMessage());
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			putFileOutput(getFormatedExceptionMessage(e, filename), getValidationOutputPath(filename));
			LogManager.info("[FileNotFoundException]:" + e.getMessage());
			e.printStackTrace();
			
			//Create Audit Trail
			EventTracker.sendEvent(AuditTrail.LOAD_DATA_FILES, userid, "Cannot find header file " + getHeaderFilename(filename, false));
		} catch (Exception e) {
			putFileOutput(getFormatedExceptionMessage(e, filename), getValidationOutputPath(filename));
			LogManager.info("[Exception]:" + e.getMessage());
			e.printStackTrace();
			
			//Create Audit Trail
			EventTracker.sendEvent(AuditTrail.LOAD_DATA_FILES, userid, e.getMessage());
		} finally {
			toDelete.delete();
			LogManager.info("dummy dtd deleted. "+toDelete.toString());
		}
	}
	
	private String getContentItemsList(String filename) {
		StringBuilder result = new StringBuilder();
		
		result.append(getHeaderFilename(filename, false) + "\n");
		
		for(ContentItem contentItem : contentsDbUploader.getContentChapterList()) {				
			result.append(contentItem.getId() + "\n");
		}
		
		for(ContentItem contentItem : contentsDbUploader.getContentOtherList()) {
			result.append(contentItem.getId() + "\n");
		}
		
		for(ImageFile imageFile : contentsDbUploader.getImageFileList()) {					
			result.append(imageFile.getId() + "\n");
		}
		
		return result.toString();
	}
	
	private String getFormatedExceptionMessage(Object object, String filename) {
		String error = "";
		if(object instanceof XPathExpressionException) {
			XPathExpressionException e = (XPathExpressionException)object;
			
			error = "[ISBN]:" + filename + Loader.NEW_LINE + "[XPathExpressionException]" +
				Loader.NEW_LINE + e.getLocalizedMessage() +
				Loader.NEW_LINE + Loader.SEPARATOR;
		} else if(object instanceof ParserConfigurationException) {
			ParserConfigurationException e = (ParserConfigurationException)object;
			
			error = "[ISBN]:" + filename + Loader.NEW_LINE + "[ParserConfigurationException]" + 
				Loader.NEW_LINE + e.getLocalizedMessage() +
				Loader.NEW_LINE + Loader.SEPARATOR;
		} else if(object instanceof SAXException) {
			SAXException e = (SAXException)object;
			
			error = "[ISBN]:" + filename + Loader.NEW_LINE + "[SAXException]" + 
				Loader.NEW_LINE + e.getLocalizedMessage() + 
				Loader.NEW_LINE + Loader.SEPARATOR;
		} else if(object instanceof FileNotFoundException) {
			FileNotFoundException e = (FileNotFoundException)object;
			
			error = "[ISBN]:" + filename + Loader.NEW_LINE + "[FileNotFoundException]" + 
				Loader.NEW_LINE + e.getLocalizedMessage() + 
				Loader.NEW_LINE + Loader.SEPARATOR ;
		} else {
			Exception e = (Exception)object;
			
			error = "[ISBN]:" + filename + Loader.NEW_LINE + "[Exception]" + 
				Loader.NEW_LINE + e.getLocalizedMessage() + 
				Loader.NEW_LINE + Loader.SEPARATOR;
		}
		
		return error;		
	}
	
	private String checkContentItemNodes(ContentsDbUploader contentsDbUploader, 
			String filename, String manifest, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {	
		
		// local variables
		NodeList pdfFiles = getNodeList(Loader.XPATH_PDF_ALL, doc);
		NodeList chapterIdList = getNodeList(Loader.XPATH_CHAPTER_IDS, doc);		
		NodeList contentItems = getNodeList(Loader.XPATH_CONTENT_ITEMS, doc);
		
		StringBuffer result = new StringBuffer();
		String directory = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true));
		String manifestXML = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + "manifest.xml";
		String fileType = "pdf";
		
		BufferedReader br = null;
		
		int matchCount = 0;
		
		result.append(pdfFiles.getLength() + " pdf file(s) defined in " 
				+ getHeaderFilename(filename, false) + "." + Loader.NEW_LINE);				
		
		List<ContentItem> contentOtherList = new ArrayList<ContentItem>();
		List<ContentItem> contentChapterList = new ArrayList<ContentItem>();
		
		for(int i = 0; contentItems != null && i < contentItems.getLength(); i++) {
			
			ContentItem contentItem = new ContentItem();
			String contentId = contentItems.item(i).getNodeValue();
			String pdfFilename = getPdfFilename(contentId, doc);
			String htmlFilename = getHtmlFilename(contentId, doc);
			String navHtmlFilename = getNavHtmlFilename(contentId,doc);
			String doi = getContentDoi(contentId, doc);
			String type = getPdfType(contentId, doc);
			
			StringBuilder manifestContent = new StringBuilder("");
			
			LogManager.info("---CONTENT ID:" + contentId);
			LogManager.info("---HTMLFILENAME:" + htmlFilename);
			LogManager.info("---NAVFILES:" + navHtmlFilename);
			LogManager.info("---PDFFILENAME:" + pdfFilename);
			LogManager.info("---TYPE:" + type);
			
			contentItem.setId(contentId);
			contentItem.setName(pdfFilename);
			contentItem.setHtmlName(htmlFilename);
			contentItem.setNavHtmlName(navHtmlFilename);
			contentItem.setType(type);
			contentItem.setDoi(doi);
			contentItem.setTitle(getContentTitle(contentId, doc));
			contentItem.setPageStart(getContentPageStart(contentId, doc));
			contentItem.setPageEnd(getContentPageEnd(contentId, doc));
			
			try {
			
				br = new BufferedReader(new FileReader(new File(manifestXML)));
				String line;
				while((line = br.readLine()) != null){
					manifestContent.append(line);
				}
				
			} catch (IOException ioe) {
				LogManager.info("ValidatorMDB.checkContentItemNodes IO Exception: " + ioe.getMessage());
				ioe.printStackTrace();
			} catch (Exception e) {
				LogManager.info("ValidatorMDB.checkContentItemNodes Exception: " + e.getMessage());
				e.printStackTrace();
			} finally {
				try {
					if (br != null) {
						br.close();
					}
				} catch (IOException ioe) {
					LogManager.info(ioe.getMessage());
					ioe.printStackTrace();
				}
			}
			
			LogManager.info("---manifestXML:" + manifestXML);
			LogManager.info("---contains:" + manifestContent.toString().contains(pdfFilename));
			if(Misc.isNotEmpty(pdfFilename) && 
					(manifestContent.toString().contains(pdfFilename) 
							&& new File(directory + "/" + pdfFilename).exists())) {
				matchCount++;		
				contentItem.setStatus(LoadEJBStatus.LOADED.getStatus());			
			} else {
				contentItem.setStatus(LoadEJBStatus.NOTLOADED.getStatus());			
			}	
			if(type.equalsIgnoreCase(CHAPTER)) {		
				contentChapterList.add(contentItem);
			} else {					
				contentOtherList.add(contentItem);
			}			
		}
	
		result.append("- " + matchCount + " out of " + pdfFiles.getLength() 
				+ " match with the definition." + Loader.NEW_LINE);
		
		result.append(getCurrentFilesMessage(directory, fileType));		
		result.append(getNewFilesMessage(directory, filename, fileType, manifest));
		
		contentsDbUploader.setContentOtherList(contentOtherList);
		contentsDbUploader.setContentChapterList(contentChapterList);		
		contentsDbUploader.setChapterIdList(chapterIdList);
		contentsDbUploader.setHeaderFilename(getHeaderFilename(filename, false));
		return result.toString();
		
	}
	
	private String checkCoverImageNodes(ContentsDbUploader contentsDbUploader, 
			String filename, String manifest, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {	
		
		// local variables
		NodeList coverImages = getNodeList(Loader.XPATH_COVER_IMAGES, doc);
		NodeList abstractImages = getNodeList(Loader.XPATH_ABSTRACT_IMAGES, doc);
		NodeList standardImage = getNodeList(Loader.XPATH_COVER_STANDARD, doc);
		NodeList thumbImage = getNodeList(Loader.XPATH_COVER_THUMB, doc);
		String manifestXML = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + "manifest.xml";
		
		StringBuffer result = new StringBuffer();
		String directory = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true));
		String fileType = "image";
		
		BufferedReader br = null;
		StringBuffer manifestContent = new StringBuffer("");
		
		int matchCount = 0;
		int totalImages = coverImages.getLength() + abstractImages.getLength();
		
		ImageFile imageFile = new ImageFile();
		List<ImageFile> imageFileList = new ArrayList<ImageFile>();
		
		result.append(totalImages + " image file(s) defined in " 
				+ getHeaderFilename(filename, false) + "." + Loader.NEW_LINE);			
		
		try {
			br = new BufferedReader(new FileReader(new File(manifestXML)));
			String line;
			while((line = br.readLine()) != null){
				manifestContent.append(line);
			}
		} catch (IOException ioe) {
			LogManager.info("ValidatorMDB.checkCoverImageNodes IO Exception: " + ioe.getMessage());
			ioe.printStackTrace();	
		} catch (Exception e) {
			LogManager.info("ValidatorMDB.checkContentItemNodes Exception: " + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ioe) {
				LogManager.info(ioe.getMessage());
				ioe.printStackTrace();
			}
		}
		
		
		// Match count				
		if(standardImage != null && standardImage.getLength() > 0) {	
			String sImage = standardImage.item(0).getNodeValue();
			
			imageFile = new ImageFile();
			imageFile.setName(sImage);
			imageFile.setId(sImage);
			imageFile.setType("image_standard");
			
			LogManager.info("---sImage:" + sImage);
			LogManager.info("---contains:" + manifestContent.toString().contains(sImage));
			if(manifestContent.toString().contains(sImage) && new File(directory + "/" + sImage).exists()){
				matchCount++;
				imageFile.setStatus(LoadEJBStatus.LOADED.getStatus());
			} else {
				imageFile.setStatus(LoadEJBStatus.NOTLOADED.getStatus());
			}			
			imageFileList.add(imageFile);
		} 		
		
		if(thumbImage != null && thumbImage.getLength() > 0) {
			String tImage = thumbImage.item(0).getNodeValue();
			
			imageFile = new ImageFile();
			imageFile.setName(tImage);
			imageFile.setId(tImage);
			imageFile.setType("image_thumb");
			
			LogManager.info("---tImage:" + tImage);
			LogManager.info("---contains:" + manifestContent.toString().contains(tImage));
			if(manifestContent.toString().contains(tImage) && new File(directory + "/" + tImage).exists()) {
				matchCount++;
				imageFile.setStatus(LoadEJBStatus.LOADED.getStatus());
			} else {
				imageFile.setStatus(LoadEJBStatus.NOTLOADED.getStatus());
			}	
			imageFileList.add(imageFile);
		}
		
		for(int i = 0; abstractImages != null && i < abstractImages.getLength(); i++) {
			String abstractImage = abstractImages.item(i).getNodeValue();
	
			LogManager.info("---tImage:" + abstractImage);
			LogManager.info("---contains:" + manifestContent.toString().contains(abstractImage));

			if(manifestContent.toString().contains(abstractImage)
					&& new File(directory + "/" + abstractImage).exists()) {
				matchCount++;				
			} 

		}		
		
		contentsDbUploader.setImageFileList(imageFileList);
		
		result.append("- " + matchCount + " out of " + totalImages + " match with the definition." + Loader.NEW_LINE);		
		result.append(getCurrentFilesMessage(directory, fileType));		
		result.append(getNewFilesMessage(directory, filename, fileType, manifest));
				
		return result.toString();
	}
	
	private String checkInsertItemNodes(ContentsDbUploader contentsDbUploader, 
			String filename, String manifest, ArrayList<InsertItem> insertItemList) 
	throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {	
				
		StringBuffer result = new StringBuffer();
		String directory = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true));
		String manifestXML = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + "manifest.xml";
		String fileType = "pdf";
		
		BufferedReader br = null;
		
		int matchCount = 0;
		
		result.append(insertItemList.size() + " insert file(s) defined in " 
				+ getHeaderFilename(filename, false) + "." + Loader.NEW_LINE);				
		
		for(int i = 0; insertItemList != null && i < insertItemList.size(); i++) {
			InsertItem insertItem = insertItemList.get(i);
			String insertId = insertItem.getId();
			String pdfFilename = insertItem.getFilename();
			String title = insertItem.getTitle();
			
			StringBuffer manifestContent = new StringBuffer("");
			
			LogManager.info("---INSERT ID:" + insertId);
			LogManager.info("---FILENAME:" + pdfFilename);
			LogManager.info("---TITLE:" + title);
			
			insertItem.setType("insert");
			
			try {
				br = new BufferedReader(new FileReader(new File(manifestXML)));
				String line;
				while((line = br.readLine()) != null){
					manifestContent.append(line);
				}
			} catch (IOException ioe) {
				LogManager.info("ValidatorMDB.checkInsertItemNodes IO Exception: " + ioe.getMessage());
				ioe.printStackTrace();	
			} catch (Exception e) {
				LogManager.info("ValidatorMDB.checkInsertItemNodes Exception: " + e.getMessage());
				e.printStackTrace();
			} finally {
				try {
					if (br != null) {
						br.close();
					}
				} catch (IOException ioe) {
					LogManager.info(ioe.getMessage());
					ioe.printStackTrace();
				}
			}
			
			LogManager.info("---manifestXML:" + manifestXML);
			LogManager.info("---contains:" + manifestContent.toString().contains(pdfFilename));
			if(Misc.isNotEmpty(pdfFilename) && (manifestContent.toString().contains(pdfFilename) 
							&& new File(directory + "/" + pdfFilename).exists())) {
				matchCount++;
				insertItem.setStatus(LoadEJBStatus.LOADED.getStatus());
			} else {
				insertItem.setStatus(LoadEJBStatus.NOTLOADED.getStatus());
			}
			
		} 
	
		result.append("- " + matchCount + " out of " + insertItemList.size() 
				+ " match with the definition." + Loader.NEW_LINE);
		
		result.append(getCurrentFilesMessage(directory, fileType));		
		result.append(getNewFilesMessage(directory, filename, fileType, manifest));
		
		contentsDbUploader.setInsertItemList(insertItemList);
		
		return result.toString();
	}
	
	private String getNewFilesMessage(String directory, String filename,
			String fileType, String manifest) {
		String message = "";
		manifest = getManifest(filename);
		File[] files = getRecentFiles(new File(directory), manifest, fileType);				
		if(files != null && files.length > 0)	{
			message = "- " + files.length + " new " + fileType + " file(s) loaded in server." + Loader.NEW_LINE;			
		}		
		return message;
	}
	
	private String getCurrentFilesMessage(String directory, String fileType) {
		String message = "";
		String[] filenames = getFileNamesByFileType(new File(directory), fileType);
		if(filenames != null && filenames.length > 0) {
			message = "- " + filenames.length + " " + fileType + " file(s) currently loaded in server." + Loader.NEW_LINE;
		} else {
			message = "- There are no " + fileType + " file(s) currently loaded in server." + Loader.NEW_LINE;
		}
		
		return message;
	}
	
	// Removed Document parsing to reduce memory costs. Moved it to parent method and changed all methods accessing it - jubs 20140310
	private NodeList getNodeList(String xpathExpression, Document doc) throws ParserConfigurationException, 
	IOException, SAXException, XPathExpressionException {
		

		XPathFactory xpFactory = null;
		XPath xPath = null;
		XPathExpression xExpression = null;
		Object result = null;
				
		xpFactory = XPathFactory.newInstance();
		xPath = xpFactory.newXPath();
		
		xExpression = xPath.compile(xpathExpression);
		
		result = xExpression.evaluate(doc, XPathConstants.NODESET);
		
		return (NodeList) result;
	}
	
	private String getHeaderFilename(String filename, boolean excludeExtension) {
		String result = "";
		int index = filename.indexOf(".zip");
		if(index > -1) {
			result = filename.substring(0, index);
			if(!excludeExtension) {			
				result = result + ".xml";
			} 
		} else {
			result = filename;
			if(!excludeExtension) {			
				result = filename + ".xml";
			} 
		}
		
		return result;
	}

	private String getCbmlFilename(String filename, boolean excludeExtension) {
		String result = "";
		int index = filename.indexOf(".zip");
		if(index > -1) {
			result = filename.substring(0, index);
			if(!excludeExtension) {			
				result = result + "_CBML.xml";
			} 
		} else {
			result = filename + "_CBML";
			if(!excludeExtension) {			
				result = filename + "_CBML.xml";
			} 
		}
		
		return result;
	}
	
	private String getBitsFilename(String filename, boolean excludeExtension) {
		String result = "";
		int index = filename.indexOf(".zip");
		if(index > -1) {
			result = filename.substring(0, index);
			if(!excludeExtension) {			
				result = result + "_BITS.xml";
			} 
		} else {
			result = filename + "_BITS";
			if(!excludeExtension) {			
				result = filename + "_BITS.xml";
			} 
		}
		
		return result;
	}
	
	private String getHeaderFilePath(String filename) {
		String filePath = IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) 
		+ getHeaderFilename(filename, false);
		if(!new File(filePath).exists()) {
			return "";
		}
		
		return filePath;
	}
	
	private String getContentItemManifestPath(String filename) {
		return IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + Loader.MANIFEST_FILENAME;
	}
	
	private String getValidationOutputPath(String filename) {		
		return IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + Loader.VALIDATION_OUTPUT_FILENAME;
	}
	
	private String getContentItemsOutputPath(String filename) {
		return IsbnContentDirUtil.getPath(getHeaderFilename(filename, true)) + Loader.CONTENT_ITEMS_LIST_OUTPUT_FILENAME;
	}
	
	private boolean isDirectoryAndExists(File dir){
		return (dir != null && dir.exists() && dir.isDirectory());
	}
	
	private String[] getFileNamesByFileType(File directory, final String fileType){
		if(isDirectoryAndExists(directory) && (fileType != null && !"".equals(fileType.trim()))){			
			return directory.list(
				new FilenameFilter(){
					public boolean accept (File file, String n){
						String f = new File(n).getName().toLowerCase();		
						if(fileType.equals("image")) {
							return f.endsWith(".jpg") || f.endsWith(".png") || f.endsWith(".gif");
						}
						return f.endsWith(fileType);
					}
				}
			); 	
		}		
		return null;
	}
	
	private File[] getRecentFiles(File dir, final String manifest, final String fileType) {
		return dir.listFiles(new FileFilter() {
			public boolean accept(File f) {
				long timestamp = 0;
				try { 
					timestamp = Long.parseLong(manifest); 
				} catch (Exception e) { 
					e.printStackTrace();
				}				
				if(fileType.equals("image")) {
					return (f.lastModified() > timestamp) 
					&& (f.getName().endsWith(".jpg") 
							|| f.getName().endsWith(".png") || f.getName().endsWith(".gif")); 
				}
				
				return (f.lastModified() > timestamp) && f.getName().endsWith(fileType); 
			}
		});
	}	
	
	private String getManifest(String filename) {
		String result = "0";
		BufferedReader br = null;
		try {
			if(new File(getContentItemManifestPath(filename)).exists()) {
				br = new BufferedReader(new FileReader(getContentItemManifestPath(filename)));
				String s = null;
				if ((s = br.readLine()) != null) {
					result = s;
				}
			} 
		} catch (Exception e) {
			LogManager.info(e.getMessage());
		} finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				LogManager.info(e.getMessage());
			}
		}
		return result;
	}
	
	private void putManifest(String filename) {
		FileWriter fw = null;
		try {
			long l = Calendar.getInstance().getTimeInMillis();
			fw = new FileWriter(getContentItemManifestPath(filename));
			fw.write(String.valueOf(l));			
		} catch (Exception e) {
			LogManager.info(e.getMessage());
		} finally {
			try {
				if(fw != null) {
					fw.flush();
					fw.close();
				}
			} catch (IOException e) {
				LogManager.info(e.getMessage());
			}
		}
	}
	
	private void putFileOutput(String output, String filename) {
		FileWriter fw = null;
		try {			
			fw = new FileWriter(filename);
			fw.write(output);			
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.info(e.getMessage());
		} finally {
			try {
				if(fw != null) {
					fw.flush();
					fw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LogManager.info(e.getMessage());
			}
		}
	}
	
	private String getPdfType(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException  {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/attribute::type";
		
		return getData(contentId, doc, xpathExpression);
	}
	
	private String getPdfFilename(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/pdf/attribute::filename";
		
		return getData(contentId, doc, xpathExpression);
	}
	
	private String getBookNavHtmlFilename(Document doc) throws XPathExpressionException,
	SAXException, IOException, ParserConfigurationException {
		 String xpathExpression = "book/content-items/child::nav-file/attribute::filename";
		 return getData(null, doc, xpathExpression);
	}
	
	private String getNavHtmlFilename(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException{
		
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/nav-file/attribute::filename";
		return getData(contentId, doc,xpathExpression);
	}
	
	private String getHtmlFilename(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/html/attribute::filename";
		
		return getData(contentId, doc,xpathExpression);
	}
	
	private String getData(String contentId, Document doc,String xpathExpression) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException{
		
		NodeList nodeList = getNodeList(xpathExpression, doc);
		String data = "";
		if(nodeList != null && nodeList.getLength() > 0) {
			data = nodeList.item(0).getNodeValue();
		}
		return data;
	}
	
	private String getContentDoi(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/doi/text()";
		
		return getData(contentId, doc, xpathExpression);
	}
	
	private String getContentTitle(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/heading/title";
		
		return XpathUtil.processContentTitle(getNodeList(xpathExpression, doc));
	}
	
	private String getContentPageStart(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/attribute::page-start";
		
		return getData(contentId, doc, xpathExpression);
	}
	
	private String getContentPageEnd(String contentId, Document doc) throws XPathExpressionException, 
	SAXException, IOException, ParserConfigurationException {
		String xpathExpression = "book/content-items/descendant::content-item[attribute::id=\"" 
			+ contentId + "\"]/attribute::page-end";
		
		return getData(contentId, doc, xpathExpression);
	}
		
	@PostConstruct
    void init() {
		Logger.getLogger( ValidatorMDB.class ).info("Initialize Uploader EJB");		
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger( ValidatorMDB.class ).info(" Destroy Uploader EJB ");
    }
    
    public static void copyfile(String srFile, String dtFile){
    	
    	InputStream in = null;
    	OutputStream out = null;
    	
	    try{
	      File f1 = new File(srFile);
	      File f2 = new File(dtFile);
	      in = new FileInputStream(f1);
	      
	      //For Append the file.
	      //For Overwrite the file.
	      out = new FileOutputStream(f2);

	      byte[] buf = new byte[1024];
	      int len;
	      while ((len = in.read(buf)) > 0){
	        out.write(buf, 0, len);
	      }

	      LogManager.info("dummy dtd created at "+dtFile);
	      System.out.println("dummy dtd created..");
	    }
	    catch(FileNotFoundException ex){
	      System.out.println(ex.getMessage() + " in the specified directory.");
	      System.exit(0);
	    }
	    catch(IOException e){
	      System.out.println(e.getMessage());      
	    } finally {
	    	try {
		    	if (in != null)
		    		in.close();
		    	if (out != null) {
		    		out.flush();
		    		out.close();
		    	}
	    	} catch (IOException ioe) {
	    		LogManager.info("ValidatorMDB.copyfile() ERROR: " + ioe.getMessage());
	    		ioe.printStackTrace();
	    	} 
	    }
	    
	  }
}