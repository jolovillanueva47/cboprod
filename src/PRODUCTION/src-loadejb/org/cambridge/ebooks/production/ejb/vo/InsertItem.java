package org.cambridge.ebooks.production.ejb.vo;

public class InsertItem {
	
	private String filename;
	private String id;
	private String title;
	private String alphasort;
	private String type;
	private String status;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAlphasort() {
		return alphasort;
	}
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
