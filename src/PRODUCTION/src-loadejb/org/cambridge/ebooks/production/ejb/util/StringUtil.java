package org.cambridge.ebooks.production.ejb.util;

/**
 * 
 * @author cmcastro
 *
 */

public class StringUtil {

	private static  final String REAL_NUMBER = "^[-+]?\\d+(\\.\\d+)?$";

	public static boolean isNumeric(String val) {
	   return isEmpty( val ) ? false : val.matches(REAL_NUMBER);
	}

	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
	
	public static String nvl(String value, String replacement) { 
		return isEmpty(value) ? replacement : value;
	}
	
	public static String nvl(String value ) { 
		return isEmpty(value) ? "" : value;
	}
	
	public static <T> String nvl(T value) { 
		return ((T) (isEmpty(value) ? "" : value)).toString();
	}
	
	public static <T> String nvl(T value, T replacement) { 
		return ((T) (isEmpty(value) ? replacement : value)).toString();
	}
	
	public static boolean validateLength(Object obj, int minLength, int maxLength) { 
		boolean result = true;
		if ( obj  instanceof String ) { 
			String value = ( String ) obj;
			if ( isNotEmpty(value) ) { 
				result = value.length() >= minLength && value.length() <= maxLength;
			}
		}
		return result;
	}	
	
	public static String addPadding(final String value, final String padding ) {
		boolean ifValueIsLongerThanThePadding = nvl(value).length() > nvl(padding).length();
		if ( ifValueIsLongerThanThePadding ) { 
			return value;
		}
		String result = nvl(padding) + nvl(value);
		return result.substring( 0 + nvl(value).length(), result.length() );
	}
	
	public static String  join(String [] array, char separator) {
		if (array == null) {
            return null;
        }
        int arraySize = array.length;
        StringBuilder  result = new StringBuilder ();
        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                result.append(separator);
            }
            if (array[i] != null) {
                result.append(array[i]);
            }
        }
        return result.toString();
    }
	
	public static String[] split(String value){
		if(isEmpty(value)) return null;
		value = value.replace("[", "").replace("]", "");
		return value.split(",");
	}

	public static String convertToUTF8(String toUTF8){
		StringBuilder utf8 = new StringBuilder();
		if(toUTF8 != null && toUTF8.length() > 0)
		{
			utf8 = new StringBuilder(toUTF8);
			try 
			{
				//if( !isUTF8(utf8.toString()) ) //removed because of UTF-8 checking issue on solaris
				//{
					boolean isDone = false;
					while(!isDone)
					{
						int index = 0;
						char[] c = utf8.toString().toCharArray();
						for(index=0; index<c.length; index++)
						{
							int intValue = (int)c[index];
							if(intValue == 38 || intValue == 60 || intValue == 62 || intValue > 127) //0 -127 common, no need to check
							{
								String stringValue = Character.toString(c[index]);
								String temp = new String(stringValue.getBytes("ISO-8859-1"), "UTF-8");
								if(temp != null)
								{
									if(temp.equals("?") || stringValue.equals("<") || stringValue.equals(">") || intValue > 160) //160 up are special characters 
									{
										utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
										break;
									}
									else if(temp.equals("&"))
									{
										if((c.length == 1) || (index != c.length - 1 && c[index + 1] != '#') || 
										   (index == c.length - 1)) 
										   
										{
											utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
											break;
										}
										
									}
								}
							}
						}
						
						if(index == utf8.toString().length()) isDone = true;
					}
				//}
				
			} 
			catch (Exception e) 
			{
				System.err.println("[Exception] convertToUTF8() " + e.getMessage());
			}
			
		}
		return utf8.toString();
	}
	
	public static String correctHtmlTags(String text) {		
		String output = "";
		try {
			output = text.replaceAll("<italic>", "<i>")
			.replaceAll("</italic>", "</i>")
			.replaceAll("<block>", "<blockquote id=\"indent\">")
			.replaceAll("</block>", "</blockquote>")
			.replaceAll("<underline>", "<u>")
			.replaceAll("</underline>", "</u>")
			.replaceAll("<bold>", "<b>")
			.replaceAll("</bold>", "</b>")
			.replaceAll("<list-item>", "<li>")
			.replaceAll("</list-item>", "</li>")
			.replaceAll("<list", "<ul")
			.replaceAll("style=\"", "style=\"list-style:")
			.replaceAll("style=\"list-style:bullet", "style=\"padding: 20px 20px 20px 30px; list-style:disc !important;")
			.replaceAll("style=\"list-style:number", "style=\"padding: 20px 20px 20px 30px; list-style:decimal !important;")
			.replaceAll("style=\"list-style:none", "style=\"padding: 20px 20px 20px 30px; list-style:none !important;")
			.replaceAll("</list>", "</ul>")
			.replaceAll("<small-caps>", "<span style=\"font-variant:small-caps\">")
			.replaceAll("</small-caps>", "</span>")
			.replaceAll("<source>", "<p align=\"right\">") /* Fix for SQLException issue 2012-11-22 */
			.replaceAll("</source>", "</p>");
		} catch (Exception e) {
			output = "";
		}
		
		return output;
	}	
}
