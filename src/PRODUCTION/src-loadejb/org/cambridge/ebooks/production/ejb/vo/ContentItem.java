package org.cambridge.ebooks.production.ejb.vo;

/**
 * @author Karlson A. Mulingtapang
 * PdfFile.java - Pdf File Bean
 */
public class ContentItem {
	private String name;
	private String htmlName;
	private String navHtmlName;
	private String type;
	private String status;
	private String id;
	private String doi;
	private String title;
	private String pageStart;
	private String pageEnd;
		
	public ContentItem() {}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the pageStart
	 */
	public String getPageStart() {
		return pageStart;
	}

	/**
	 * @param pageStart the pageStart to set
	 */
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	/**
	 * @return the pageEnd
	 */
	public String getPageEnd() {
		return pageEnd;
	}

	/**
	 * @param pageEnd the pageEnd to set
	 */
	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public void setHtmlName(String htmlName) {
		this.htmlName = htmlName;
	}

	public String getHtmlName() {
		return htmlName;
	}

	public void setNavHtmlName(String navHtmlName) {
		this.navHtmlName = navHtmlName;
	}

	public String getNavHtmlName() {
		return navHtmlName;
	}	
}
