package org.cambridge.ebooks.production.ejb.util;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Karlson A. Mulingtapang
 * XmlValidator.java - Validates a given XML file against a given DTD file.
 */
public class XmlValidator {
	private DocumentBuilderFactory documentBuilderFactory;
	private DocumentBuilder documentBuilder;
	private String errorText;
	private String warningText;
	private String fatalErrorText;
	private String xmlFilePath;
	private String dtdFilePath;
	
	private XmlValidator() throws ParserConfigurationException {
		documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setValidating(true);
		documentBuilder = documentBuilderFactory.newDocumentBuilder();
		documentBuilder.setErrorHandler(new ErrorHandler() {
			public void error(SAXParseException exception) throws SAXException {
				errorText = "[Error@line:" + exception.getLineNumber() + "]: " + 
					exception.getMessage();
				throw new SAXException(errorText);
			}
			
			public void fatalError(SAXParseException exception) throws SAXException {
				fatalErrorText = "[Fatal Error@line:" + exception.getLineNumber() + "]: " + 
					exception.getMessage();
				throw new SAXException(fatalErrorText);
			}
			
			public void warning(SAXParseException exception) throws SAXException {
				warningText = "[Warning@line:" + exception.getLineNumber() + "]: " + 
					exception.getMessage();
				throw new SAXException(warningText);
			}
		});
	}
	
	/**
	 * Creates a new instance of XmlValidator
	 * @return XmlValidator
	 * @throws ParserConfigurationException
	 */
	public static XmlValidator newInstance() throws ParserConfigurationException {
		return new XmlValidator();
	}
	
	/**
	 * Validate an xml file against a dtd file
	 * @throws IOException
	 * @throws SAXException
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 */
	public void validate() throws IOException, SAXException, 
		TransformerConfigurationException, TransformerException, NullPointerException {
		
		if(this.xmlFilePath == null || this.xmlFilePath.equals("")) {
			throw new NullPointerException("xmlFilePath is null");
		}
		
		if(this.dtdFilePath == null || this.dtdFilePath.equals("")) {
			throw new NullPointerException("dtdFilePath is null");
		}
		
		Document xmlDocument = this.documentBuilder.parse(new File(this.xmlFilePath));
		
		DOMSource domSource = new DOMSource(xmlDocument);
		
		StreamResult streamResult = new StreamResult(System.out);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, this.dtdFilePath);
		transformer.transform(domSource, streamResult);
	}

	/**
	 * @return the xmlFilePath
	 */
	public String getXmlFilePath() {
		return xmlFilePath;
	}

	/**
	 * @param xmlFilePath the xmlFilePath to set
	 */
	public void setXmlFilePath(String xmlFilePath) {
		this.xmlFilePath = xmlFilePath;
	}

	/**
	 * @return the dtdFilePath
	 */
	public String getDtdFilePath() {
		return dtdFilePath;
	}

	/**
	 * @param dtdFilePath the dtdFilePath to set
	 */
	public void setDtdFilePath(String dtdFilePath) {
		this.dtdFilePath = dtdFilePath;
	}	
}
