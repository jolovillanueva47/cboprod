package org.cambridge.ebooks.production.ejb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.cambridge.ebooks.production.ejb.audittrail.EventTracker;
import org.cambridge.ebooks.production.ejb.common.LoadEJBStatus;
import org.cambridge.ebooks.production.ejb.jpa.EBook;
import org.cambridge.ebooks.production.ejb.jpa.EBookContent;
import org.cambridge.ebooks.production.ejb.sql.EBookSql;
import org.cambridge.ebooks.production.ejb.vo.ContentItem;
import org.cambridge.ebooks.production.ejb.vo.ImageFile;
import org.cambridge.ebooks.production.ejb.vo.InsertItem;
import org.cambridge.ebooks.production.util.Log4JLogger;
import org.w3c.dom.NodeList;

/**
 * @author Karlson A. Mulingtapang
 * ContentsDbUploader.java - Database process after upload
 */
public class ContentsDbUploader {
	
	private static final Log4JLogger LogManager = new Log4JLogger(ContentsDbUploader.class); // 2012-11-16 Jubs
	
	private String bookId;
	private String bookTitle;
	private String bookTitleAlphasort;
	private String eIsbn;
	private String bookDoi;
	private String seriesId;
	private String chapterId;	
	private String headerFilename;
	private String bitsFilename;
	private String cbmlFilename;
	private String filename;
	private String htmlFilename;
	private String navHtmlFilename;
	private String username;
	private String userid;
	private EBook ebook;
	private NodeList chapterIdList;
	private List<ContentItem> contentOtherList;
	private List<ContentItem> contentChapterList;
	private List<ImageFile> imageFileList;
	private List<InsertItem> insertItemList;
	private EntityManager em;
	private UserTransaction userTransaction;
	private ArrayList<String> ebookContentQueries;
	
	public static final String INDEX = "index.dir";
	public static final String CONTENT_ID_LOG = "/app/ebooks/log/deleteContentIdLog";
	
	private static final String INSERT_TEMPLATE = "INSERT INTO EBOOK_CONTENT (FILENAME,HTML_FILENAME,NAVHTML_FILENAME,CONTENT_ID, ISBN, BOOK_ID, EMBARGO_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, STATUS, TYPE, TITLE, PAGE_START, PAGE_END, DOI)";
	
	public ContentsDbUploader() {
		
		ebookContentQueries = new ArrayList<String>();
	}
	
	public void loadData() throws Exception {		
		// Update EBOOK
		if(isUpdateEbook()) {			
			updateEbookInDb();
			
			// Update EBOOK_CHAPTER
			for(ContentItem contentItem : getContentChapterList()) {				
				if(contentItem.getStatus().equalsIgnoreCase(LoadEJBStatus.LOADED.getStatus()) || 
				   contentItem.getStatus().equalsIgnoreCase(LoadEJBStatus.NOTLOADED.getStatus())) {
					updateContentStatusInDb(contentItem, true);
				} else {
					updateContentStatusInDb(contentItem, false);
				}			
			}
			
			// Update EBOOK_OTHERS						
			for(ContentItem contentItem : getContentOtherList()) {
				
				if(contentItem.getStatus().equalsIgnoreCase(LoadEJBStatus.LOADED.getStatus()) ||
				   contentItem.getStatus().equalsIgnoreCase(LoadEJBStatus.NOTLOADED.getStatus())) {
					updateContentToDb(contentItem, true);						
				} else {
					updateContentToDb(contentItem, false);		
				}	
			}
			
			//DeleteImages();
			
			for(ImageFile imageFile : getImageFileList()) {					
				if(imageFile.getStatus().equalsIgnoreCase(LoadEJBStatus.LOADED.getStatus()) ||
				   imageFile.getStatus().equalsIgnoreCase(LoadEJBStatus.NOTLOADED.getStatus())) {
					updateContentToDb(imageFile, true);					
				} else {
					updateContentToDb(imageFile, false);	
				}
			}
			
			for(InsertItem insertItem : getInsertItemList()){
				if(insertItem.getStatus().equalsIgnoreCase(LoadEJBStatus.LOADED.getStatus()) ||
				   insertItem.getStatus().equalsIgnoreCase(LoadEJBStatus.NOTLOADED.getStatus())) {
					updateContentToDb(insertItem, true);					
				} else {
					updateContentToDb(insertItem, false);	
				}
			}
			
			updateContentToDb(getHeaderFilename(), getHeaderFilename(), "xml", true);
			
			//CBML Insert
			if(! getCbmlFilename().equals("none")){
				updateContentToDb(getCbmlFilename(), getCbmlFilename(), "cbml", true);
			}
			//BITS Insert
			if(! getBitsFilename().equals("none")){
				updateContentToDb(getBitsFilename(), getBitsFilename(), "bits", true);
			}
			
		} else {
			LogManager.info(ContentsDbUploader.class, "---INSERT EBOOK");
			insertEbookToDb();
			
			// INSERT EBOOK_CHAPTER
			
			for(ContentItem contentItem : getContentChapterList()) {					
				ebookContentQueryBuilder(contentItem);	   
				
				//Create Audit Trail
		    	EventTracker.sendBookEvent(contentItem.getId(), getEIsbn(), 
		    			getSeriesId(), contentItem.getStatus(), "Content Loading", 
		    			getUsername(), contentItem.getName(), contentItem.getType());
			}		
			
			for(ContentItem contentItem : getContentOtherList()) {
				ebookContentQueryBuilder(contentItem);	
				
				//Create Audit Trail
		    	EventTracker.sendBookEvent(contentItem.getId(), getEIsbn(), 
		    			getSeriesId(), contentItem.getStatus(), "Content Loading", 
		    			getUsername(), contentItem.getName(), contentItem.getType());
			}
			
			for(ImageFile imageFile : getImageFileList()) {		
				ebookContentQueryBuilder(imageFile);	
				
				//Create Audit Trail
		    	EventTracker.sendBookEvent(null, getEIsbn(), 
		    			getSeriesId(), imageFile.getStatus(), "Content Loading", 
		    			getUsername(), imageFile.getName(), imageFile.getType());
			}
			
			for(InsertItem insertItem : getInsertItemList()){
				ebookContentQueryBuilder(insertItem);

				//Create Audit Trail
		    	EventTracker.sendBookEvent(null, getEIsbn(), 
		    			getSeriesId(), insertItem.getStatus(), "Content Loading", 
		    			getUsername(), insertItem.getFilename(), insertItem.getType());
			}
			
			StringBuilder query = new StringBuilder();
			
			query.append(INSERT_TEMPLATE);
			query.append("VALUES(");
			query.append(format(getHeaderFilename()));
			query.append(",NULL,");
			query.append("NULL,");
			query.append(format(getHeaderFilename()));
			query.append(",");
			query.append(format(getEIsbn()));
			query.append(",");
			query.append(format(getBookId()));
			query.append(",NULL,NULL,SYSDATE,");
			query.append(format(getUsername()));
			query.append(",");
			query.append(format(LoadEJBStatus.LOADED.getStatus()));
			query.append(",'xml',NULL, NULL, NULL, NULL");
			query.append(")");
			
			ebookContentQueryBuilder(query.toString());
				
			if(! getCbmlFilename().equals("none")){
				
				query = new StringBuilder();
				
				query.append(INSERT_TEMPLATE);
				query.append("VALUES(");
				query.append(format(getCbmlFilename()));
				query.append(",NULL,");
				query.append(hasBookNavHtmlFile("cbml") ? format(getNavHtmlFilename()) : "NULL");
				query.append(",");				
				query.append(format(getCbmlFilename()));
				query.append(",");
				query.append(format(getEIsbn()));
				query.append(",");
				query.append(format(getBookId()));
				query.append(",NULL,NULL,SYSDATE,");
				query.append(format(getUsername()));
				query.append(",");
				query.append(format(LoadEJBStatus.LOADED.getStatus()));
				query.append(",'cbml',NULL, NULL, NULL, NULL");
				query.append(")");
				
				ebookContentQueryBuilder(query.toString());
				
			}
			
			if(! getBitsFilename().equals("none")){
				
				query = new StringBuilder();
				
				query.append(INSERT_TEMPLATE);
				query.append("VALUES(");
				query.append(format(getBitsFilename()));
				query.append(",NULL,");
				query.append(hasBookNavHtmlFile("bits") ? format(getNavHtmlFilename()) : "NULL");
				query.append(",");
				query.append(format(getBitsFilename()));
				query.append(",");
				query.append(format(getEIsbn()));
				query.append(",");
				query.append(format(getBookId()));
				query.append(",NULL,NULL,SYSDATE,");
				query.append(format(getUsername()));
				query.append(",");
				query.append(format(LoadEJBStatus.LOADED.getStatus()));
				query.append(",'bits',NULL, NULL, NULL, NULL");
				query.append(")");
				
				ebookContentQueryBuilder(query.toString());
				
			}
			
			insertContentToDb();
			
			EventTracker.sendBookEvent(null, getEIsbn(), 
	    			getSeriesId(), LoadEJBStatus.LOADED.getStatus(), "Content Loading", 
	    			getUsername(), getHeaderFilename(), "xml");
		}		
		
		
		updateEBookStatus();
	}
	


	private String format(String string) {
		return ("'" + string + "'");
	}
	
	private boolean hasBookNavHtmlFile(String type) {
		return (("bits".equals(type) || "cbml".equals(type)) && navHtmlFilename != null && navHtmlFilename.length() > 0);
	}

	private void ebookContentQueryBuilder(String query) {
		
		ebookContentQueries.add(query);
	}
	
	private void ebookContentQueryBuilder(ContentItem pdfFile) {
		String pdfTitle = pdfFile.getTitle() != null ? pdfFile.getTitle() : "";
		
		StringBuilder query = new StringBuilder();
		
		query.append(INSERT_TEMPLATE);
		query.append("VALUES(");
		query.append(format(pdfFile.getName()));
		query.append(",");
		query.append(format(pdfFile.getHtmlName()));
		query.append(",");
		query.append(format(pdfFile.getNavHtmlName()));
		query.append(",");
		query.append(format(pdfFile.getId()));
		query.append(",");
		query.append(format(getEIsbn()));
		query.append(",");
		query.append(format(getBookId()));
		query.append(",NULL,NULL,SYSDATE,");
		query.append(format(getUsername()));
		query.append(",");
		query.append(format(pdfFile.getStatus()));
		query.append(",");
		query.append(format(pdfFile.getType()));
		query.append(",");
		query.append(format((pdfTitle.contains("?") ? pdfTitle.replaceAll("\\?", "&#63;") : pdfTitle)));
		query.append(",");
		query.append(format(pdfFile.getPageStart()));
		query.append(",");
		query.append(format(pdfFile.getPageEnd()));
		query.append(",");
		query.append(format(pdfFile.getDoi()));
		query.append(")");
		
		ebookContentQueryBuilder(query.toString());
		
	}
	
	private void ebookContentQueryBuilder(ImageFile imageFile) {
		
		StringBuilder query = new StringBuilder();
		
		query.append(INSERT_TEMPLATE);
		query.append("VALUES(");
		query.append(format(imageFile.getName()));
		query.append(",NULL,");
		query.append("NULL,");
		query.append(format(imageFile.getId()));
		query.append(",");
		query.append(format(getEIsbn()));
		query.append(",");
		query.append(format(getBookId()));
		query.append(",NULL,NULL,SYSDATE,");
		query.append(format(getUsername()));
		query.append(",");
		query.append(format(imageFile.getStatus()));
		query.append(",");
		query.append(format(imageFile.getType()));
		query.append(",NULL, NULL, NULL, NULL");
		query.append(")");

		ebookContentQueryBuilder(query.toString());
	}

	private void ebookContentQueryBuilder(InsertItem insertItem) {
		
		String title = insertItem.getTitle();
		title = title != null ? (title.contains("?") ? title.replaceAll("\\?", "&#63;") : title).replaceAll("'", "&#x27;") : "";
		String contentId = insertItem.getId();
		String filename = insertItem.getFilename();
		String status = insertItem.getStatus();
		String type = insertItem.getType();
				
		StringBuilder query = new StringBuilder();
		
		query.append(INSERT_TEMPLATE);
		query.append("VALUES(");
		query.append(format(filename));
		query.append(",NULL,");
		query.append("NULL,");
		query.append(format(contentId));
		query.append(",");
		query.append(format(getEIsbn()));
		query.append(",");
		query.append(format(getBookId()));
		query.append(",NULL,NULL,SYSDATE,");
		query.append(format(getUsername()));
		query.append(",");
		query.append(format(status));
		query.append(",");
		query.append(format(type));
		query.append(",");
		query.append(format(title));
		query.append(",NULL,NULL,NULL");
		query.append(")");
		
		ebookContentQueryBuilder(query.toString());

	}
	
	private boolean isUpdateEbook() {
		EntityManager em = getEm();
		
		try { 			
			ebook = em.find(EBook.class, getBookId());
			if(ebook != null) {
				LogManager.info(ContentsDbUploader.class, "---IS EBOOK UPDATE");
				return true;
			}
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        }
        LogManager.info(ContentsDbUploader.class, "---IS NOT EBOOK UPDATE");
    	return false;
	}
	
	@SuppressWarnings("unchecked")
	private List<EBookContent> getEBookContents(String bookId) throws Exception {
		LogManager.info(ContentsDbUploader.class, "---getEBookContents():" + bookId);
		List<EBookContent> result = null;
		EntityManager em = getEm();				
		try {			
			StringBuilder sql = new StringBuilder("SELECT * FROM ebook_content WHERE content_id in (");
			
			for(ContentItem contentItem : getContentChapterList()) {				
				sql.append("'" + contentItem.getId() + "',");
			}
			
			for(ContentItem contentItem : getContentOtherList()) {
				sql.append("'" + contentItem.getId() + "',");
			}
			
			for(ImageFile imageFile : getImageFileList()) {					
				sql.append("'" + imageFile.getId() + "',");
			}
			
			sql.append("'" + getHeaderFilename() + "')");
			
			Query query = em.createNativeQuery(sql.toString(), EBookContent.class);
			
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
		}
		
		return result;
	}
	
	@SuppressWarnings("deprecation")
	private void updateEBookStatus() throws Exception {
		LogManager.info(ContentsDbUploader.class, "---UPDATE EBOOK STATUS");
		int status = 0;
		EntityManager em = null;
		try {
			status = LoadEJBStatus.getNextStatus(LoadEJBStatus.getBookStatus(getEBookContents(getBookId())));
			em = getEm();		
			Query query = em.createNativeQuery(EBookSql.UPDATE_EBOOK_STATUS_BY_BOOK_ID.toString());
			query.setParameter(1, status);
			query.setParameter(2, getUsername());
			query.setParameter(3, getBookId());
			query.executeUpdate();
			EventTracker.sendEBookEvent(getEIsbn(), getSeriesId(), getBookTitle(), ""+status, getUsername());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
		}
	}

	private void insertEbookToDb() throws Exception {	
		LogManager.info(ContentsDbUploader.class, "---Book Id:" + getBookId());
		LogManager.info(ContentsDbUploader.class, "---ISBN:" + getEIsbn());
		LogManager.info(ContentsDbUploader.class, "---Status:" + LoadEJBStatus.UNPUBLISHED.getStatus());
		LogManager.info(ContentsDbUploader.class, "---Series Id:" + getSeriesId());
		LogManager.info(ContentsDbUploader.class, "---Date:" + new Date());
		LogManager.info(ContentsDbUploader.class, "---User name:" + getUsername());
		LogManager.info(ContentsDbUploader.class, "---Book title:" + getBookTitle());
		LogManager.info(ContentsDbUploader.class, "---Book title alphasort:" + getBookTitleAlphasort());
		
		EntityManager em = getEm();
		Query query = null;
		
		if(getSeriesId() == null) {
			query = em.createNativeQuery(EBookSql.INSERT_EBOOK_WO_SERIES.toString());
			
			query.setParameter(1, getBookId());
			query.setParameter(2, getEIsbn());
			query.setParameter(3, LoadEJBStatus.NOTLOADED.getStatus());
			query.setParameter(4, new Date());	
			query.setParameter(5, getUsername());	
			query.setParameter(6, getBookTitle());
			query.setParameter(7, getBookTitleAlphasort());
			query.setParameter(8, getBookDoi());
			query.executeUpdate();			
		} else {
			query = em.createNativeQuery(EBookSql.INSERT_EBOOK.toString());
			
			query.setParameter(1, getBookId());
			query.setParameter(2, getEIsbn());
			query.setParameter(3, LoadEJBStatus.NOTLOADED.getStatus());
			query.setParameter(4, getSeriesId());		
			query.setParameter(5, new Date());	
			query.setParameter(6, getUsername());	
			query.setParameter(7, getBookTitle());
			query.setParameter(8, getBookTitleAlphasort());
			query.setParameter(9, getBookDoi());
			query.executeUpdate();
		}	
	}
	private void insertContentToDb() throws Exception {				
		EntityManager em = getEm();
		for(String oneQuery : ebookContentQueries){
			Query query = em.createNativeQuery(oneQuery);		
			query.executeUpdate();

		}			
	}	
	
	private void updateEbookInDb() throws Exception {
		EntityManager em = getEm();
		
		Query query = em.createNativeQuery(EBookSql.UPDATE_EBOOK_BY_BOOK_ID.toString());
		
		query.setParameter(1, getEIsbn());
		query.setParameter(2, new Date());
		query.setParameter(3, getUsername());
		query.setParameter(4, getBookTitle());
		query.setParameter(5, getBookTitleAlphasort());		
		query.setParameter(6, getBookDoi());
		query.setParameter(7, getBookId());	
		
		query.executeUpdate();				
	}
	
	private void updateContentStatusInDb(ContentItem contentItem, boolean isMatch) throws Exception {	
		String status = "";
		
		EBookContent ebookContent = null;		
		EntityManager em = getEm();	
        
        try { 
        	ebookContent = em.find(EBookContent.class, contentItem.getId());
        } catch (Exception e) {
        	throw e;
        } finally {
        }
        	
		if(isMatch) {
			if(ebookContent == null || ebookContent.getStatus() == Integer.parseInt(LoadEJBStatus.NOTLOADED.getStatus())) {
				status = LoadEJBStatus.LOADED.getStatus();
			} else {
				status = String.valueOf(LoadEJBStatus.getNextStatus(ebookContent.getStatus()));
			}
		} else {
			if(ebookContent == null) {
				status = LoadEJBStatus.NOTLOADED.getStatus();
			} else {
				status = String.valueOf(ebookContent.getStatus());
			}
		}	
		
		String contentItemTitle = contentItem.getTitle();
		contentItemTitle = contentItemTitle != null ? (contentItemTitle.contains("?") ? contentItemTitle.replaceAll("\\?", "&#63;") : contentItemTitle ) : "";
		String nativeQueryEbookContent = "MERGE INTO EBOOK_CONTENT d USING DUAL ON (d.CONTENT_ID = '" 
		    + contentItem.getId() + "')"
		    + " WHEN MATCHED THEN UPDATE SET d.status = '" + status 
		    + "', d.modified_date = SYSDATE, d.modified_by = '" + getUsername() + "', d.filename = '"
		    + contentItem.getName() + "', d.type = '" + contentItem.getType() 
		    + "', d.title = '" + contentItemTitle 
		    + "', d.page_start = '" + contentItem.getPageStart()
		    + "', d.page_end = '" + contentItem.getPageEnd() 
		    + "', d.doi = '" + contentItem.getDoi() + "'"
		    + " WHEN NOT MATCHED THEN INSERT (FILENAME, CONTENT_ID, ISBN, BOOK_ID, " +
		    		"EMBARGO_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, " +
		    		"STATUS, TYPE, TITLE, PAGE_START, PAGE_END, DOI) VALUES ('" + contentItem.getName() + "', '"
		    + contentItem.getId() + "', '" + getEIsbn() + "', '" + getBookId() + "', NULL, NULL, SYSDATE, '"
		    + getUsername() + "', '" + status + "', '" + contentItem.getType() + "', '" 
		    + contentItemTitle + "', '" + contentItem.getPageStart() + "', '" 
		    + contentItem.getPageEnd() + "', '" + contentItem.getDoi() + "')";	  
		
		if(isMatch){  
		    if(ebookContent != null) {
				//Create Audit trail
				if(ebookContent.getStatus() == 3 || ebookContent.getStatus() == 5 || ebookContent.getType().equals("xml")){
					
			    	EventTracker.sendBookEvent(ebookContent.getContentId(), 
			    			ebookContent.getIsbn(), getSeriesId(), 
			    			status, "Content Loading", getUsername(), ebookContent.getFileName(), ebookContent.getType());
				} else {
			    	EventTracker.sendBookEvent(ebookContent.getContentId(),  
			    			ebookContent.getIsbn(), getSeriesId(), status,
			    			"Warning: Content should not have been reloaded.", 
			    			getUsername(), ebookContent.getFileName(), ebookContent.getType());
				}
		    } else {
		    	EventTracker.sendBookEvent(contentItem.getId(), getEIsbn(), getSeriesId(), 
		    			status, "Content Loading", getUsername(), contentItem.getName(), contentItem.getType());
		    }
		} else if(ebookContent == null) { // new content item		   
	    	EventTracker.sendBookEvent(contentItem.getId(), getEIsbn(), getSeriesId(), 
	    			status, "Content Loading", getUsername(), contentItem.getName(), contentItem.getType());
		   
		} else {
			nativeQueryEbookContent = "UPDATE EBOOK_CONTENT d SET d.filename = '"
			    + contentItem.getName() + "', d.type = '" + contentItem.getType() 
			    + "', d.title = '" + contentItemTitle 
			    + "', d.page_start = '" + contentItem.getPageStart() 
			    + "', d.page_end = '" + contentItem.getPageEnd()
			    + "', d.doi = '" + contentItem.getDoi()
			    + "' WHERE d.CONTENT_ID = '" + contentItem.getId() + "'";
		}
		
		LogManager.info(ContentsDbUploader.class, "---Native query: " + nativeQueryEbookContent);
	    
	    try {
		    em = getEm();
		    
			Query query = em.createNativeQuery(nativeQueryEbookContent);	
			
			query.executeUpdate();
	    } catch (Exception e) {
	    	throw e;
	    } finally {				
	    }
	}	
		
	public void deleteImages()throws Exception{
		ArrayList<EBookContent> resultList = new ArrayList<EBookContent>();
			
		String nativeQuery = "DELETE from oraebooks.ebook_content " +
				"WHERE book_id ='" + bookId + "' " +
				"AND type in ('image_standard', 'image_thumb') ";
		
		LogManager.info(ContentsDbUploader.class, "---Native query: " + nativeQuery);
		
		EntityManager em = null;
		try {
		    em = getEm();
			Query query = em.createNativeQuery(nativeQuery);
			
			System.out.println(query.executeUpdate() + " image entries deleted");
	    } catch (Exception e) {
	    	throw e;
	    } finally {
	    	if(em != null){
	    	}
	    }
	    
		for(EBookContent content : resultList){
	    	EventTracker.sendBookEvent(content.getContentId(), getEIsbn(), 
	    			getSeriesId(), String.valueOf(content.getStatus()), "Content Deleted", 
	    			getUsername(), content.getFileName(), content.getType());
		}
	}
	
	private void updateContentToDb(ImageFile imageFile, boolean isMatch) throws Exception {
		LogManager.info(ContentsDbUploader.class, " imageFile.getId() = "+imageFile.getId());
		updateContentToDb(imageFile.getName(), imageFile.getId(), imageFile.getType(), isMatch);
	}
		
	private void updateContentToDb(String filename, String contentId, String type, boolean isMatch) throws Exception {
		String status = "";
		LogManager.info(ContentsDbUploader.class, "---UPDATE CONTENT TO DB");

		EBookContent ebookContent = null;		
		EntityManager em = getEm();	
        
        try { 
        	ebookContent = em.find(EBookContent.class, contentId);
        } catch (Exception e) {
        	throw e;
        } finally {
        }
               
		if(isMatch) {
			if(ebookContent == null || ebookContent.getStatus() == Integer.parseInt(LoadEJBStatus.NOTLOADED.getStatus())) {
				status = LoadEJBStatus.LOADED.getStatus();
			} else {
				status = String.valueOf(LoadEJBStatus.getNextStatus(ebookContent.getStatus()));
			}
		} else {
			if(ebookContent == null) {
				status = LoadEJBStatus.NOTLOADED.getStatus();
			} else {
				status = String.valueOf(ebookContent.getStatus());
			}
		}	
		
		StringBuilder nativeQuery = new StringBuilder();
		nativeQuery.append("MERGE INTO EBOOK_CONTENT d USING DUAL ON (d.CONTENT_ID = ");
		nativeQuery.append(format(contentId));
		nativeQuery.append(")");
		nativeQuery.append(" WHEN MATCHED THEN UPDATE SET d.status = ");
		nativeQuery.append(format(status));
		nativeQuery.append(", d.modified_date = SYSDATE, d.modified_by = ");
		nativeQuery.append(format(getUsername()));
		nativeQuery.append(", d.filename = ");
		nativeQuery.append(format(filename));
		if (hasBookNavHtmlFile(type)) {
			nativeQuery.append(", d.navhtml_filename = ");
			nativeQuery.append(format(navHtmlFilename));
		}
		nativeQuery.append(", d.type = ");
		nativeQuery.append(format(type));
		nativeQuery.append(" WHEN NOT MATCHED THEN INSERT (FILENAME, NAVHTML_FILENAME, CONTENT_ID, ISBN, BOOK_ID,");
		nativeQuery.append(" EMBARGO_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY,");
		nativeQuery.append(" STATUS, TYPE, TITLE, PAGE_START, PAGE_END, DOI) VALUES (");
		nativeQuery.append(format(filename));
		nativeQuery.append(",");
		if (hasBookNavHtmlFile(type)) {
			nativeQuery.append(format(navHtmlFilename));
			nativeQuery.append(",");
		} else {
			nativeQuery.append("NULL, ");
		}
		nativeQuery.append(format(contentId));
		nativeQuery.append(",");
		nativeQuery.append(format(getEIsbn()));
		nativeQuery.append(",");
		nativeQuery.append(format(getBookId()));
		nativeQuery.append(", NULL, NULL, SYSDATE, ");
		nativeQuery.append(format(getUsername()));
		nativeQuery.append(",");
		nativeQuery.append(format(status));
		nativeQuery.append(",");
		nativeQuery.append(format(type));
		nativeQuery.append(", NULL, NULL, NULL, NULL)");
		
		if(isMatch) {				
			//Create Audit trail
		    if(ebookContent != null) 
		    {
				if(ebookContent.getStatus() == 3 || ebookContent.getStatus() == 5 || ebookContent.getType().equals("xml"))
				{			
			    	EventTracker.sendBookEvent(ebookContent.getContentId(), 
			    			ebookContent.getIsbn(), getSeriesId(), 
			    			status, "Content Loading", getUsername(), ebookContent.getFileName(), ebookContent.getType());
				}
				else 
				{
			    	EventTracker.sendBookEvent(ebookContent.getContentId(),  
			    			ebookContent.getIsbn(), getSeriesId(), status,
			    			"Warning: Content should not have been reloaded.", 
			    			getUsername(), ebookContent.getFileName(), ebookContent.getType());
				}
		    } 
		    else 
		    { // new Loaded
		    	EventTracker.sendBookEvent(contentId, getEIsbn(), getSeriesId(), status, "Content Loading",	getUsername(), filename, type);
		    }
		} 
		else if (ebookContent == null) 
		{ // new content item			
		    // new Loaded
	    	EventTracker.sendBookEvent(contentId, getEIsbn(), getSeriesId(), status, "Content Loading",	getUsername(), filename, type);
		    
		}
		else 
		{
			nativeQuery = new StringBuilder();
			nativeQuery.append("UPDATE EBOOK_CONTENT d SET d.filename = ");
			nativeQuery.append(format(filename));
			nativeQuery.append(", d.type = ");
			nativeQuery.append(format(type));
			nativeQuery.append(", d.modified_by = ");
			nativeQuery.append(format(getUsername()));
			nativeQuery.append(", d.modified_date = SYSDATE WHERE d.CONTENT_ID = ");
			nativeQuery.append(format(contentId));
		}	    
		try 
		{
		    em = getEm();
		    
			Query query = em.createNativeQuery(nativeQuery.toString());	
			
			query.executeUpdate();
	    } 
		catch (Exception e)
		{
	    	throw e;
	    } 
		finally 
		{				
	    }			
	}	
	
	private void updateContentToDb(ContentItem contentItem, boolean isMatch) throws Exception {
		String status = "";
		LogManager.info(ContentsDbUploader.class, "---UPDATE CONTENT TO DB");
		EBookContent ebookContent = null;		
		EntityManager em = getEm();	
        
		String contentId = contentItem.getId();
		String filename = contentItem.getName();
		String htmlFilename = contentItem.getHtmlName();
		String navHtmlFilename = contentItem.getNavHtmlName();
		String type = contentItem.getType();
		String title = contentItem.getTitle();
		title = title != null ? (title.contains("?") ? title.replaceAll("\\?", "&#63;") : title ) : "";
		String pageStart = contentItem.getPageStart();
		String pageEnd = contentItem.getPageEnd();
		String doi = contentItem.getDoi();
		
        try { 
        	ebookContent = em.find(EBookContent.class, contentId);
        } catch (Exception e) {
        	throw e;
        } finally {
        }
        
		if(isMatch) {
			if(ebookContent == null || ebookContent.getStatus() == Integer.parseInt(LoadEJBStatus.NOTLOADED.getStatus())) {
				status = LoadEJBStatus.LOADED.getStatus();
			} else {
				status = String.valueOf(LoadEJBStatus.getNextStatus(ebookContent.getStatus()));
			}
		} else {
			if(ebookContent == null) {
				status = LoadEJBStatus.NOTLOADED.getStatus();
			} else {
				status = String.valueOf(ebookContent.getStatus());
			}
		}	
				
		StringBuilder nativeQuery = new StringBuilder();
		
		nativeQuery.append("MERGE INTO EBOOK_CONTENT d USING DUAL ON (d.CONTENT_ID = ");
		nativeQuery.append(format(contentId));
		nativeQuery.append(")");
		nativeQuery.append("WHEN MATCHED THEN UPDATE SET d.status = ");
		nativeQuery.append(format(status));
		nativeQuery.append(", d.modified_date = SYSDATE, d.modified_by = ");
		nativeQuery.append(format(getUsername()));
		nativeQuery.append(", d.filename = ");
		nativeQuery.append(format(filename));
		nativeQuery.append(", d.html_filename = ");
		nativeQuery.append(format(htmlFilename));
		nativeQuery.append(", d.navhtml_filename = ");
		nativeQuery.append(format(navHtmlFilename));
		nativeQuery.append(", d.type = ");
		nativeQuery.append(format(type));
		nativeQuery.append(", d.title = ");
		nativeQuery.append(format(title));
		nativeQuery.append(", d.page_start = ");
		nativeQuery.append(format(pageStart));
		nativeQuery.append(", d.page_end = ");
		nativeQuery.append(format(pageEnd));
		nativeQuery.append(", d.doi = ");
		nativeQuery.append(format(doi));
		nativeQuery.append("WHEN NOT MATCHED THEN INSERT (FILENAME, HTML_FILENAME,NAVHTML_FILENAME,CONTENT_ID, ISBN, BOOK_ID,EMBARGO_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY,STATUS, TYPE, TITLE, PAGE_START, PAGE_END, DOI) VALUES (");
		nativeQuery.append(format(filename));
		nativeQuery.append(",");
		nativeQuery.append(format(htmlFilename));
		nativeQuery.append(",");
		nativeQuery.append(format(navHtmlFilename));
		nativeQuery.append(",");
		nativeQuery.append(format(contentId));
		nativeQuery.append(",");
		nativeQuery.append(format(getEIsbn()));
		nativeQuery.append(",");
		nativeQuery.append(format(getBookId()));
		nativeQuery.append(",NULL, NULL, SYSDATE,");
		nativeQuery.append(format(getUsername()));
		nativeQuery.append(",");
		nativeQuery.append(format(status));
		nativeQuery.append(",");
		nativeQuery.append(format(type));
		nativeQuery.append(",");
		nativeQuery.append(format(title));
		nativeQuery.append(",");
		nativeQuery.append(format(pageStart));
		nativeQuery.append(",");
		nativeQuery.append(format(pageEnd));
		nativeQuery.append(",");
		nativeQuery.append(format(doi));
		nativeQuery.append(")");
		
		
		if(isMatch) {				
			//Create Audit trail
		    if(ebookContent != null) {
				if(ebookContent.getStatus() == 3 || ebookContent.getStatus() == 5 || ebookContent.getType().equals("xml")){			
			    	EventTracker.sendBookEvent(ebookContent.getContentId(), 
			    			ebookContent.getIsbn(), getSeriesId(), 
			    			status, "Content Loading", getUsername(), ebookContent.getFileName(), ebookContent.getType());
				} else {
			    	EventTracker.sendBookEvent(ebookContent.getContentId(),  
			    			ebookContent.getIsbn(), getSeriesId(), status,
			    			"Warning: Content should not have been reloaded.", 
			    			getUsername(), ebookContent.getFileName(), ebookContent.getType());
				}
		    } else { // new Loaded
		    	EventTracker.sendBookEvent(contentId, getEIsbn(), 
		    			getSeriesId(), status, "Content Loading", 
		    			getUsername(), filename, type);
		    }
		} else if (ebookContent == null) { // new content item			
		    // new Loaded
	    	EventTracker.sendBookEvent(contentId, getEIsbn(), 
	    			getSeriesId(), status, "Content Loading", 
	    			getUsername(), filename, type);
		    
		} else {
			nativeQuery = new StringBuilder();
		
			nativeQuery.append("UPDATE EBOOK_CONTENT d SET d.filename = ");
			nativeQuery.append(format(filename));
			nativeQuery.append(", d.type = ");
			nativeQuery.append(format(type));
			nativeQuery.append(", d.modified_by = ");
			nativeQuery.append(format(getUsername()));
			nativeQuery.append(", d.modified_date = SYSDATE, d.title =");
			nativeQuery.append(format(title));
			nativeQuery.append(", d.page_start = ");
			nativeQuery.append(format(pageStart));
			nativeQuery.append(", d.page_end = ");
			nativeQuery.append(format(pageEnd));
			nativeQuery.append(", d.doi = ");
			nativeQuery.append(format(doi));
			nativeQuery.append("WHERE d.CONTENT_ID = ");
			nativeQuery.append(format(contentId));
			
		}    
		try {
		    em = getEm();

			Query query = em.createNativeQuery(nativeQuery.toString());	
			
			query.executeUpdate();

	    } catch (Exception e) {
	    	throw e;
	    } finally {				
	    }			
	}
	
	private void updateContentToDb(InsertItem insertItem, boolean isMatch) throws Exception {
		String status = "";
		LogManager.info(ContentsDbUploader.class, "---UPDATE CONTENT TO DB");
		EBookContent ebookContent = null;		
		EntityManager em = getEm();	
        
		String contentId = insertItem.getId();
		String filename = insertItem.getFilename();
		String type = insertItem.getType();
		String title = insertItem.getTitle();
		title = title != null ? (title.contains("?") ? title.replaceAll("\\?", "&#63;") : title).replaceAll("'", "&#x27;") : "";
		String pageStart = "NULL";
		String pageEnd = "NULL";
		String doi = "NULL";
		
        try { 
        	ebookContent = em.find(EBookContent.class, contentId);
        } catch (Exception e) {
        	throw e;
        } finally {
        }
        
		if(isMatch) {
			if(ebookContent == null || ebookContent.getStatus() == Integer.parseInt(LoadEJBStatus.NOTLOADED.getStatus())) {
				status = LoadEJBStatus.LOADED.getStatus();
			} else {
				status = String.valueOf(LoadEJBStatus.getNextStatus(ebookContent.getStatus()));
			}
		} else {
			if(ebookContent == null) {
				status = LoadEJBStatus.NOTLOADED.getStatus();
			} else {
				status = String.valueOf(ebookContent.getStatus());
			}
		}	
		

		String nativeQuery = "MERGE INTO EBOOK_CONTENT d USING DUAL ON (d.CONTENT_ID = '" 
		    + contentId + "')"
		    + " WHEN MATCHED THEN UPDATE SET d.status = '" + status 
		    + "', d.modified_date = SYSDATE, d.modified_by = '" + getUsername() + "', d.filename = '"
		    + filename + "', d.type = '" + type + "', d.title = '" + title 
		    + "', d.page_start = '" + pageStart + "', d.page_end = '" + pageEnd 
		    + "', d.doi = '" + doi + "'"
		    + " WHEN NOT MATCHED THEN INSERT (FILENAME, CONTENT_ID, ISBN, BOOK_ID, " +
		    		"EMBARGO_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, " +
		    		"STATUS, TYPE, TITLE, PAGE_START, PAGE_END, DOI) VALUES ('" + filename + "', '"
		    + contentId + "', '" + getEIsbn() + "', '" + getBookId() + "', NULL, NULL, SYSDATE, '"
		    + getUsername() + "', '" + status + "', '" + type + "', '" + title 
		    + "', '" + pageStart + "', '" + pageEnd + "', '" + doi + "')";	   
		
		if(isMatch) {				
			//Create Audit trail
		    if(ebookContent != null) {
				if(ebookContent.getStatus() == 3 || ebookContent.getStatus() == 5 || ebookContent.getType().equals("xml")){			
			    	EventTracker.sendBookEvent(ebookContent.getContentId(), 
			    			ebookContent.getIsbn(), getSeriesId(), 
			    			status, "Content Loading", getUsername(), ebookContent.getFileName(), ebookContent.getType());
				} else {
			    	EventTracker.sendBookEvent(ebookContent.getContentId(),  
			    			ebookContent.getIsbn(), getSeriesId(), status,
			    			"Warning: Content should not have been reloaded.", 
			    			getUsername(), ebookContent.getFileName(), ebookContent.getType());
				}
		    } else { // new Loaded
		    	EventTracker.sendBookEvent(contentId, getEIsbn(), 
		    			getSeriesId(), status, "Content Loading", 
		    			getUsername(), filename, type);
		    }
		} else if (ebookContent == null) { // new content item			
		    // new Loaded
	    	EventTracker.sendBookEvent(contentId, getEIsbn(), 
	    			getSeriesId(), status, "Content Loading", 
	    			getUsername(), filename, type);
		    
		} else {
			nativeQuery = "UPDATE EBOOK_CONTENT d SET d.filename = '"
			    + filename + "', d.type = '" + type 
			    + "', d.modified_by = '" + getUsername() 
			    + "', d.modified_date = SYSDATE, d.title = '" + title 
			    + "', d.page_start = '" + pageStart + "', d.page_end = '" + pageEnd 
			    + "', d.doi = '" + doi
			    + "' WHERE d.CONTENT_ID = '" + contentId + "'";
		}
	    
		try {
		    em = getEm();

			Query query = em.createNativeQuery(nativeQuery);	
			
			query.executeUpdate();

	    } catch (Exception e) {
	    	throw e;
	    } finally {	
		
	    }			
	}	
	
	public void deleteNotIncluded()throws Exception{
		deleteContentIdsInDb();
	}
		
	@SuppressWarnings("unchecked")
	public void auditDeletion()throws Exception{
		StringBuffer contentIds = new StringBuffer();
		contentIds = getContentIds();
		
		ArrayList<EBookContent> resultList = new ArrayList<EBookContent>();
			
		String nativeQuery = 	"select * from EBOOK_CONTENT where isbn = "+getEIsbn()+
								" and CONTENT_ID not in ("+contentIds+")";
		
		LogManager.info(ContentsDbUploader.class, "---Native query: " + nativeQuery);
		
		EntityManager em = null;
		try {
		    em = getEm();
			Query query = em.createNativeQuery(nativeQuery, EBookContent.class);
			
			resultList = (ArrayList<EBookContent>) query.getResultList();
	    } catch (Exception e) {
	    	throw e;
	    } finally {
	    	if(em != null){

	    	}
	    }
	    
		for(EBookContent content : resultList){
			//Create Audit Trail
	    	EventTracker.sendBookEvent(content.getContentId(), getEIsbn(), 
	    			getSeriesId(), String.valueOf(content.getStatus()), "Content Deleted", 
	    			getUsername(), content.getFileName(), content.getType());
		}
	}
	
	/**
	 * Create Log file to save the Content Ids that were not deleted during the process
	 * 
	 * @param resultList
	 */
	public static void deleteContentIdLog(ArrayList<EBookContent> resultList){
		FileWriter fw;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(CONTENT_ID_LOG,true);
			bw = new BufferedWriter(fw);
			
			for(EBookContent content : resultList){
				bw.newLine();
				bw.write(content.getContentId());
			}
			
			bw.flush();
			bw.close();
		} catch(IOException ioe){
			LogManager.error(ContentsDbUploader.class, "[IOException] Error while trying to create logfile:" 
							+ CONTENT_ID_LOG + ":"+ ioe.getMessage());
		}
	}
	
	/**
	 * Deletes the database records were content ids are not in header
	 * @throws Exception
	 */
	public void deleteContentIdsInDb()throws Exception{
		LogManager.info(ContentsDbUploader.class, "---Delete Content Ids in DB where Content Ids are not included in the header");
		
		// Audit deletion of content id
		auditDeletion();
		
		StringBuffer contentIds = new StringBuffer();
		contentIds = getContentIds();
		
		if(contentIds != null && contentIds.length() > 0){
			String nativeQuery = 	"delete from EBOOK_CONTENT where isbn = "+getEIsbn()+
									" and CONTENT_ID not in ("+contentIds+")";
			
			LogManager.info(ContentsDbUploader.class, "---Delete query: " + nativeQuery);
			    
			EntityManager em = null;
			try {
			    em = getEm();

				Query query = em.createNativeQuery(nativeQuery);	
				
				query.executeUpdate();

		    } catch (Exception e) {
		    	throw e;
		    } finally {
		    	if(em != null){

		    	}
		    }
		}
	}
	
	public StringBuffer getContentIds(){
		StringBuffer contentIds = new StringBuffer();
		
		
		for(ContentItem contentItem : getContentChapterList()) {		
			contentIds.append("'"+contentItem.getId()+"',");
		}		
		
		for(ContentItem contentItem : getContentOtherList()) {
			contentIds.append("'"+contentItem.getId()+"',");
		}
		
		for(ImageFile imageFile : getImageFileList()) {		
			contentIds.append("'"+imageFile.getId()+"',");
		}
		
		contentIds.append("'"+getHeaderFilename()+"'");
		
		return contentIds;
	}
	
	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the chapterIdList
	 */
	public NodeList getChapterIdList() {
		return chapterIdList;
	}

	/**
	 * @param chapterIdList the chapterIdList to set
	 */
	public void setChapterIdList(NodeList chapterIdList) {
		this.chapterIdList = chapterIdList;
	}

	/**
	 * @return the headerFilename
	 */
	public String getHeaderFilename() {
		return headerFilename;
	}

	/**
	 * @param headerFilename the headerFilename to set
	 */
	public void setHeaderFilename(String headerFilename) {
		this.headerFilename = headerFilename;
	}  

	public String getCbmlFilename() {
		return cbmlFilename;
	}


	public void setCbmlFilename(String cbmlFilename) {
		this.cbmlFilename = cbmlFilename;
	}

	/**
	 * @return the chapterId
	 */
	public String getChapterId() {
		return chapterId;
	}

	/**
	 * @param chapterId the chapterId to set
	 */
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}	

	/**
	 * @return the eIsbn
	 */
	public String getEIsbn() {
		return eIsbn;
	}

	/**
	 * @param isbn the eIsbn to set
	 */
	public void setEIsbn(String isbn) {
		eIsbn = isbn;
	}

	/**
	 * @return the seriesId
	 */
	public String getSeriesId() {
		return seriesId;
	}

	/**
	 * @param seriesId the seriesId to set
	 */
	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the contentOtherList
	 */
	public List<ContentItem> getContentOtherList() {
		return contentOtherList;
	}

	/**
	 * @param contentOtherList the contentOtherList to set
	 */
	public void setContentOtherList(List<ContentItem> contentOtherList) {
		this.contentOtherList = contentOtherList;
	}

	/**
	 * @return the contentChapterList
	 */
	public List<ContentItem> getContentChapterList() {
		return contentChapterList;
	}

	/**
	 * @param contentChapterList the contentChapterList to set
	 */
	public void setContentChapterList(List<ContentItem> contentChapterList) {
		this.contentChapterList = contentChapterList;
	}

	/**
	 * @return the imageFileList
	 */
	public List<ImageFile> getImageFileList() {
		return imageFileList;
	}

	/**
	 * @param imageFileList the imageFileList to set
	 */
	public void setImageFileList(List<ImageFile> imageFileList) {
		this.imageFileList = imageFileList;
	}

	public List<InsertItem> getInsertItemList() {
		return insertItemList;
	}

	public void setInsertItemList(List<InsertItem> insertItemList) {
		this.insertItemList = insertItemList;
	}

	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * @return the userTransaction
	 */
	public UserTransaction getUserTransaction() {
		return userTransaction;
	}

	/**
	 * @param userTransaction the userTransaction to set
	 */
	public void setUserTransaction(UserTransaction userTransaction) {
		this.userTransaction = userTransaction;
	}

	/**
	 * @return the bookTitle
	 */
	public String getBookTitle() {
		return bookTitle;
	}

	/**
	 * @param bookTitle the bookTitle to set
	 */
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	/**
	 * @return the bookTitleAlphasort
	 */
	public String getBookTitleAlphasort() {
		return bookTitleAlphasort;
	}

	/**
	 * @param bookTitleAlphasort the bookTitleAlphasort to set
	 */
	public void setBookTitleAlphasort(String bookTitleAlphasort) {
		this.bookTitleAlphasort = bookTitleAlphasort;
	}

	public String getBookDoi() {
		return bookDoi;
	}

	public void setBookDoi(String bookDoi) {
		this.bookDoi = bookDoi;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public void setHtmlFilename(String htmlFilename) {
		this.htmlFilename = htmlFilename;
	}

	public String getHtmlFilename() {
		return htmlFilename;
	}

	public void setBitsFilename(String bitsFilename) {
		this.bitsFilename = bitsFilename;
	}

	public String getBitsFilename() {
		return bitsFilename;
	}

	public void setNavHtmlFilename(String navHtmlFilename) {
		this.navHtmlFilename = navHtmlFilename;
	}

	public String getNavHtmlFilename() {
		return navHtmlFilename;
	}	
	
}
