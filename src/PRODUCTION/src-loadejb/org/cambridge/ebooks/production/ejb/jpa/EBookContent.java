package org.cambridge.ebooks.production.ejb.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.cambridge.ebooks.production.ejb.util.StringUtil;


/**
 * @author rvillamor
 */

@SqlResultSetMapping( 
	name = "searchContentResult",
	entities = {
		@EntityResult (
			entityClass = EBookContent.class,
			fields = {
				@FieldResult(name = "filename", column = "FILENAME"),
				@FieldResult(name = "contentId", column = "CONTENT_ID"),
				@FieldResult(name = "isbn", column = "ISBN"),
				@FieldResult(name = "bookId", column = "BOOK_ID"),
				@FieldResult(name = "status", column = "STATUS"),
				@FieldResult(name = "embargoDate", column = "EMBARGO_DATE"),
				@FieldResult(name = "remarks", column = "REMARKS"),
				@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
				@FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
				@FieldResult(name = "type", column = "TYPE")
			}
		)
	}
 )

@Entity
@Table(name = "EBOOK_CONTENT")
public class EBookContent {
	
	public static final String SEARCH_CHAPTERS_BY_BOOK_ID = "searchChaptersByBookIdSQL";
	public static final String SEARCH_BY_BOOK_ID = "searchContentByBookIdSQL";
	public static final String SEARCH_BY_FILENAME = "searchByFilename";
	public static final String SEARCH_BY_CONTENT_ID_AND_TYPE = "searchByContentIdAndType";
	public static final String SEARCH_OTHER_CONTENT_BY_BOOK_ID = "searchOtherContentByBookId";
	public static final String SEARCH_HEADER_AND_IMAGES_BY_BOOK_ID = "searchHeaderAndImagesByBookIdSQL";
	
	public static final String UPDATE_BY_FILENAME = "updateByFilename";
	public static final String UPDATE_STATUS_BY_FILENAME = "updateStatusByFilename";
	
	public static final String INSERT_CONTENT = "insertContent";
	
	public static final String IMAGE_STANDARD = "image_standard";
	public static final String IMAGE_THUMB = "image_thumb";
	public static final String XML = "xml";
	
	
	@Column(name = "FILENAME")
	private String fileName;
	
	@Id
	@Column(name = "CONTENT_ID")
	private String contentId;
	
	@Column(name = "ISBN")
	private String isbn;
		
	@Column(name = "STATUS")
	private int status;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="EMBARGO_DATE")
    private Date embargoDate;
	
	@Column(name = "REMARKS")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "BOOK_ID")
	private String bookId;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getEmbargoDate() {
		return embargoDate;
	}

	public void setEmbargoDate(Date embargoDate) {
		this.embargoDate = embargoDate;
	}

	public String getRemarks() {
		if(StringUtil.isEmpty(remarks))
		{
			remarks = "";
		}
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getDisplayFileType() {
		String display;
		try {
			display = (fileName.substring( fileName.lastIndexOf(".")+1, fileName.length() )).toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();
			display = "unknown filetype";
		}
		return display;
	}

	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
}
