package org.cambridge.ebooks.production.ejb.sql;

public enum EBookSql {
	INSERT_EBOOK("INSERT INTO EBOOK (BOOK_ID, ISBN, STATUS, SERIES_CODE, MODIFIED_DATE, MODIFIED_BY, TITLE, TITLE_ALPHASORT, DOI) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)"),
	INSERT_EBOOK_WO_SERIES("INSERT INTO EBOOK (BOOK_ID, ISBN, STATUS, MODIFIED_DATE, MODIFIED_BY, TITLE, TITLE_ALPHASORT, DOI) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)"),
	UPDATE_EBOOK_BY_BOOK_ID("UPDATE EBOOK"
			+ " SET ISBN = ?1, MODIFIED_DATE = ?2, MODIFIED_BY = ?3, TITLE = ?4, TITLE_ALPHASORT = ?5, DOI = ?6, PROOFING_CODE=null, PROOFING_EXPIRY_DATE=null"
			+ " WHERE BOOK_ID = ?7"),
	SEARCH_BY_BOOK_ID("SELECT * FROM EBOOK" 
			+ " WHERE BOOK_ID = ?1"),
	UPDATE_EBOOK_STATUS_BY_BOOK_ID("UPDATE EBOOK"
			+ " SET STATUS = ?1, MODIFIED_BY = ?2, MODIFIED_DATE = SYSDATE"
			+ " WHERE BOOK_ID = ?3");
	
	private String sql;
	
	private EBookSql(String sql) {
		this.sql = sql;
	}
	
	public String toString() {
		return this.sql;
	}
}
