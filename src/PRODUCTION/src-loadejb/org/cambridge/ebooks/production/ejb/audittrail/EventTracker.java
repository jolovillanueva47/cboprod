package org.cambridge.ebooks.production.ejb.audittrail;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.cambridge.ebooks.production.ejb.util.LogManager;

/**
 * @author andreau
 * @modified by rvillamor
 *
 */
public class EventTracker {

	public static void sendEvent( String action, String userId, String remarks)  {
        try {
        	

        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");  
            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
        	payload.setStringProperty("ACTION", 	action 	);
            payload.setStringProperty("USER_ID", 	userId 	);
            payload.setStringProperty("REMARKS",	remarks );
            payload.setStringProperty("TYPE",		"User" 	);
            
            try { 
	            messageProducer.send( payload );	            
            } finally { 
            	messageProducer.close();
                session.close();
                connection.stop();
                connection.close();	
            }
        } 
        catch (JMSException je) 
        {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
        	System.out.println("je: "+je);
        } 
        catch (Exception ex) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + ex.getMessage());
			System.out.println("ex: " + ex);
		} 
    }
	
	public static void sendBookEvent( String contentId, String isbn, String seriesCode,
									  String status, String remarks, String username,
									  String filename, String loadType)  {
        try {
        	
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
            
            MessageProducer messageProducer = session.createProducer(destination);
            Message payload = session.createMapMessage();
            
            payload.setStringProperty("CONTENT_ID",	contentId 	);
            payload.setStringProperty("EISBN",		isbn 		);
            payload.setStringProperty("SERIES_CODE",seriesCode	);
        	payload.setStringProperty("STATUS", 	status 		);            
            payload.setStringProperty("REMARKS",	remarks 	);
            payload.setStringProperty("FILENAME", 	filename 	);
            payload.setStringProperty("LOAD_TYPE", 	loadType 	);
            payload.setStringProperty("USERNAME", 	username 	);
            payload.setStringProperty("TYPE",		"Book" 		);
            
            try 
            { 
	            messageProducer.send( payload );
            } 
            finally 
            { 
            	messageProducer.close();
                session.close();
                connection.stop();
                connection.close();	
            }
        } 
        catch (JMSException je) 
        {
        	LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
        	System.out.println("je: "+je);
        } 
        catch (Exception ex) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + ex.getMessage());
			System.out.println("ex: " + ex);
		} 
    }
	
	@Deprecated
	public static void sendEBookEvent( 	String isbn, String seriesCode, String title,
			  							String status, String username)  {
		try {			
			
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
			
			MessageProducer messageProducer = session.createProducer(destination);
			Message payload = session.createMapMessage();
			
			payload.setStringProperty("EISBN",		isbn 		);
			payload.setStringProperty("SERIES_CODE",seriesCode	);
			payload.setStringProperty("TITLE",		title		);
			payload.setStringProperty("STATUS", 	status 		);            
			payload.setStringProperty("USERNAME", 	username 	);
			payload.setStringProperty("TYPE",		"EBook_D" 	); //EBook changed to EBook_D because of the new method sendEBookEvent(String bookId, String status, String username)
		
			try { 
				messageProducer.send( payload );
			} finally { 
				messageProducer.close();
				session.close();
				connection.close();	
			}
		} 
		catch (JMSException je) 
		{
			LogManager.error( EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage() );
			System.out.println("je: "+je);
		} 
		catch (Exception ex) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + ex.getMessage());
			System.out.println("ex: " + ex);
		} 
	}
	
	
	public static void sendEBookEvent(String bookId, String status, String username) {
		try 
		{
        	ConnectionFactory connectionFactory = getJmsConnectionFactory("ConnectionFactory");

            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Destination destination = getJmsDestination("queue/AuditTrail");
            
			MessageProducer messageProducer = session.createProducer(destination);
			Message payload = session.createMapMessage();

			payload.setStringProperty("BOOKID", bookId);
			payload.setStringProperty("STATUS", status);
			payload.setStringProperty("USERNAME", username);
			payload.setStringProperty("TYPE", "EBook");

			try 
			{
				messageProducer.send(payload);
			} 
			finally 
			{
				messageProducer.close();
				session.close();
				connection.stop();
				connection.close();
			}
		} 
		catch (JMSException je) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + je.getMessage());
			System.out.println("je: " + je);
		} 
		catch (Exception ex) 
		{
			LogManager.error(EventTracker.class, " Message not sent to audit-trail-ejb: " + ex.getMessage());
			System.out.println("ex: " + ex);
		} 
		
	}
	
	private static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws Exception {
		ConnectionFactory jmsConnectionFactory = null;
		try {
		   Context ctx = new InitialContext();
		   jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		} catch (ClassCastException cce) {
		   throw cce;
		} catch (NamingException ne) {
		   throw ne;
		}
		return jmsConnectionFactory;
	}
	
	private static Destination getJmsDestination(String jmsDestinationJndiName) throws Exception {
		Destination jmsDestination = null;
		try {
		   Context ctx = new InitialContext();
		   jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		} catch (ClassCastException cce) {
		   throw cce;
		} catch (NamingException ne) {
		   throw ne;
		}
		return jmsDestination;
	}
}