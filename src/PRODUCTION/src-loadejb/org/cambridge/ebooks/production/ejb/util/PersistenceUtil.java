package org.cambridge.ebooks.production.ejb.util;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.cambridge.ebooks.production.ejb.jpa.EBook;
import org.cambridge.ebooks.production.ejb.jpa.EBookContent;

import org.cambridge.ebooks.production.util.Log4JLogger;
/**
 * 
 * @author cmcastro
 * @author C2
 *
 */

public class PersistenceUtil {
	private static final Log4JLogger LogManager = new Log4JLogger(PersistenceUtil.class);
	public enum PersistentUnits {
		UPLOADER("UploaderService");
		
		private PersistentUnits(String name) { 
			this.name = name;
		}
		
		private String name;
		
		public String toString() { 
			return name;
		}
	}

	private static PersistentUnits UploaderService = PersistentUnits.UPLOADER;
	
	private static EntityManagerFactory emf;
//	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.UPLOADER.toString());
	
	//private static UserTransaction ut;
	
	@SuppressWarnings("unchecked")
	public static <T> void updateEntity(Object obj) {
		EntityManager em = emf.createEntityManager();
		em.merge((T) obj);
		try {
			em.getTransaction().commit();
		} catch (Exception e) {
			LogManager.error(PersistenceUtil.class, "Error in updating entity. " + e.getMessage());
		} finally {
			em.close();
		}
	}
	
	public static void updateEntity(String namedQuery, String... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		//ut.begin();
		//em.joinTransaction();
		Query query = em.createNativeQuery( namedQuery );
		
		if ( params != null && params.length > 0 ) { 
			int ctr = 1;
		    for (String param : params) { 
		    	query.setParameter( ctr++, param); 
		    }
		}
		
		query.executeUpdate();
		//ut.commit();
		em.getTransaction().commit();
		em.close();				
	}
	
	public static void updateEntity(String namedQuery, Object... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		//ut.begin();
		//em.joinTransaction();
		Query query = em.createNativeQuery( namedQuery );
		
		if (params != null && params.length > 0) { 
			int ctr = 1;
		    for (Object param : params) { 
		    	query.setParameter( ctr++, param); 
		    }
		}
		
		query.executeUpdate();
		//ut.commit();
		em.getTransaction().commit();
		em.close();				
	}
	
	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> searchList( T t, String namedQuery, String... params ) { 
		ArrayList<T> result = new ArrayList<T>();
		
		EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery( namedQuery );
        
        if (params != null && params.length > 0) { 
        	int ctr = 1;
	        for (String param : params) { 
	        	query.setParameter( ctr++, param); 
	        }
        }
        
        try { 
        	result = (ArrayList<T>) query.getResultList();
        } catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString() + " named query is: " + namedQuery );
        } finally {
        	em.close();
        }
		return  result;
	}
	
	
	public static EBook findEBook(String param) { 
		EBook result = null;		
		EntityManager em = emf.createEntityManager();	
        
//        if (params != null && params.length > 0) { 
//        	int ctr = 1;
//	        for (String param : params) { 
//	        	query.setParameter(ctr++, param); 
//	        }
//        }
//        
//        try { 
//        	LogManager.info(PersistenceUtil.class, "-----FIND EBOOK: " + query.getSingleResult().toString());
//        	result = (EBook) query.getSingleResult();
//        } catch (NoResultException nre) { 
//        	LogManager.info(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString() + " named query is: " + nativeQuery );
//        } finally {
//        	em.close();
//        }		
//		
        try { 
        	result = em.find(EBook.class, param);
        } catch (NoResultException nre) { 
        	result = null;
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString());
        } finally {
        	em.close();
        }
		return result;
	}
	
	public static EBookContent findEBookContent(String param) { 
		EBookContent result = null;		
		EntityManager em = emf.createEntityManager();			
//        Query query = em.createNativeQuery(nativeQuery);
//        
//        if (params != null && params.length > 0) { 
//        	int ctr = 1;
//	        for (String param : params) { 
//	        	query.setParameter(ctr++, param); 
//	        }
//        }
//        
//        try { 
//        	LogManager.info(PersistenceUtil.class, "-----FIND EBOOK CONTENT: " + query.getSingleResult().toString());
//        	result = (EBookContent) query.getSingleResult();
//        } catch (NoResultException nre) { 
//        	LogManager.info(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString() + " named query is: " + nativeQuery );
//        } finally {
//        	em.close();
//        }        
        
        try { 
        	result = em.find(EBookContent.class, param);
        } catch (NoResultException nre) { 
        	result = null;
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString());
        } finally {
        	em.close();
        }
        
		return result;
	}	
	
	public static <T> void updateEntities( T t, String namedQuery, String... params ) { 			
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
        Query query = em.createNamedQuery( namedQuery );
        
        
        if (params != null && params.length > 0) { 
        	int ctr = 1;
	        for (String param : params) { 
	        	query.setParameter(ctr++, param); 
	        }
        }
        
        try { 
        	query.executeUpdate();
        	em.getTransaction().commit();
        } catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, "Update failed.. " + " named query is: " + namedQuery );
        } finally {
        	em.close();
        }		
	}
	
	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> dynamicSearchList( T t, String queryStr, String resultSetMapping, ArrayList<String> params ) { 
		ArrayList<T> result = new ArrayList<T>();		
		
		EntityManager em = emf.createEntityManager();        
		Query query = em.createNativeQuery(queryStr, resultSetMapping);
		
		if ( params != null && params.size() > 0 ) { 
			int ctr = 1;
			for(String param : params){
				query.setParameter(ctr++, param);
			}
		}
		
        try { 
        	result = (ArrayList<T>) query.getResultList();
        } catch (NoResultException nre ) { 
        	LogManager.error(PersistenceUtil.class, " I did not get any result from: " + UploaderService.toString() + " dynamic query is: " + queryStr );
        } finally {
        	em.close();
        }
		return  result;
	}
	
	public static void setEntityManagerFactory(EntityManagerFactory emf) {
		PersistenceUtil.emf = emf;
	}
	
	public static void setUserTransaction(UserTransaction ut) {
		//PersistenceUtil.ut = ut;
	}
}
