package org.cambridge.ebooks.production.content.cleaner.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.cambridge.ebooks.production.content.cleaner.ApplicationProperties;

public class JDBCUtil {
	public static Connection getConnection() {
		Connection connection = null; 
		try { 
			// Load the JDBC driver 
			String driverName = ApplicationProperties.DB_DRIVER;//"oracle.jdbc.driver.OracleDriver";
//			System.out.println("driverName=" + driverName);
			Class.forName(driverName);
			
			// Create a connection to the database			
			//String serverName = System.getProperty("jdbc.server");//"127.0.0.1"; 
			//String portNumber = System.getProperty("jdbc.port");//"1521"; 
			//String sid = System.getProperty("jdbc.sid");//"mydatabase"; 
			//String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
			String url=ApplicationProperties.DB_URL;
			String username = ApplicationProperties.DB_USER;//"username"; 
			String password = ApplicationProperties.DB_PASS;//"password"; 
			if(url == null){
				
			}
//			System.out.println("connecting to url=" + url);
			connection = DriverManager.getConnection(url, username, password); 
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			e.printStackTrace();
		} catch (SQLException e) { 
			// Could not connect to the database
			e.printStackTrace();
		}		
		return connection;
	}
		
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
