package org.cambridge.ebooks.production.content.cleaner.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.cambridge.ebooks.production.content.cleaner.jdbc.JDBCUtil;

public class DBInfo {
	
	public static Connection conn;
	
	public static void startConnection(){
		System.out.println("Opening DB connection..");
		conn = JDBCUtil.getConnection();
	}
	
	public static void endConnection() throws SQLException {
		if (conn != null && !conn.isClosed()) {
			conn.close();
			System.out.println("Successfully closed connection.");
		}
	}
	
	public static String deleteImages (String bookId) throws SQLException{
		Statement st = null;
		String publisherPrefix = "";
		startConnection();
		st = conn.createStatement();
		try{
			String sql = "DELETE from oraebooks.ebook_content " +
				"WHERE content_id ='" + bookId + "' " +
				"AND type in ('image_standard', 'image_thumb')";
			
			
			System.out.println("sql:" + sql+ " = " + st.execute(sql));
			conn.commit();
		}catch (Exception e){
			e.printStackTrace();
			
		}finally{
			st.close();
			endConnection();
		}

		return publisherPrefix;
	}
		
}
