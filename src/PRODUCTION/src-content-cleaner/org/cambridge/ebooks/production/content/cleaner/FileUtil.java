package org.cambridge.ebooks.production.content.cleaner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.content.cleaner.jdbc.DBInfo;

public class FileUtil {
	
	private static String path;
	private static String filename;
	
	public boolean deleteAbstractText(List<String> list){
		boolean success = true;
		
		if(StringUtils.isEmpty(path)){
			path = "/app/ebooks/content/";
		};
		
		if(StringUtils.isEmpty(filename)){
			filename = "/delete.txt";
		}
		
		try {
			List<IsbnData> isbnData = new ArrayList<IsbnData>();
			
			//get the filenames across all delete.txt
			System.out.println("filenames to be deleted: ");
			for(String str:list){
			    IsbnData data = new IsbnData();
				if(str.contains("_")){
				    data.setIsbn(str.substring(str.indexOf("_")-13, str.indexOf("_")));
					data.setToDelete(readContent(IsbnContentDirUtil.getPath(data.getIsbn(), filename)));
				}
				isbnData.add(data);
			}
			System.out.println("begin deleting: ");
			//delete the files
			for(IsbnData toBeDeleted:isbnData){
			    for(String fileToBeDeleted:toBeDeleted.getToDelete()){
    				if (fileToBeDeleted.endsWith(".txt")){
    					deleteFile(toBeDeleted.getIsbn(), fileToBeDeleted);
    				} else if (fileToBeDeleted.endsWith(".jpg")){
    					DBInfo.deleteImages(fileToBeDeleted);
    					deleteFile(toBeDeleted.getIsbn(), fileToBeDeleted);
    				}
			    }
			}
		} catch (Exception e){
			success = false;
			e.printStackTrace();
		}
		return success;
	}
	
	public List<String> readContent(String sourceFile){
        List<String> list = new ArrayList<String>();
        
        try {
            BufferedReader in = new BufferedReader(new FileReader(sourceFile));
            String str;
            
            while (! StringUtils.isEmpty(str = in.readLine())) {
                System.out.println("read: "+str);
                list.add(str);
            }
            
            in.close();
        } catch (FileNotFoundException e){
            System.out.println("File "+sourceFile+" does not exist.");
            e.getStackTrace();
        } catch (Exception e) { 
            e.printStackTrace();
        }
        return list;
    }
	
	//***IMPORTANT NOTE!! CHANGE "/" to "\\" when testing in local for deleteTxtFile	
	public void deleteFile(String isbn, String fileToBeDeleted){
		System.out.println("fileToBeDeleted = "+fileToBeDeleted);
		List<String> completeFileName = new ArrayList<String>();
		
		String chunked = IsbnContentDirUtil.getPath(isbn,fileToBeDeleted);
		if (! StringUtils.isEmpty(chunked)){
            completeFileName.add(chunked);
        }
		String legacy = IsbnContentDirUtil.checkLegacyFile(isbn, fileToBeDeleted);
		if (! StringUtils.isEmpty(legacy)){
		    completeFileName.add(legacy);
		}
		
		for(String fileName:completeFileName){
    		File file = new File(fileName);
    		if(file.delete()){
    			System.out.println("successfully deleted "+file.toString());
    		} else {
    			System.out.println("failed to delete "+file.toString());
    		}
		}
	}
	
	public String extractIsbn(String fileName){
		String isbn = "";
		if(fileName.contains("abstract")){
			isbn = fileName.subSequence(fileName.indexOf("A")-13, fileName.indexOf("A")).toString();
		} else if (fileName.contains("jpg")){
			isbn = fileName.substring(0, 13);
		}
		
		return isbn;
	}

	public boolean moveFile(String sourceFile, String destinationFile) {
		boolean success = true;
		try {
			File f1 = new File(sourceFile);
			File f2 = new File(destinationFile + dateTimeNow());
			InputStream in = new FileInputStream(f1);
			
			//proceed only when file is not empty
			if(in.available() > 0){
				// For Append the file.
				// OutputStream out = new FileOutputStream(f2,true);
	
				// For Overwrite the file.
				OutputStream out = new FileOutputStream(f2);
	
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
				// System.out.println("Target File: "+destinationFile+" created.");
				if (f1.delete()) {
					// System.out.println("Source file: "+destinationFile+" deleted.");
					System.out.println("isbnlist.txt archived as "
							+ f2.getAbsolutePath());
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
			success = false;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			success = false;
		}

		return success;
	}
	
	public String dateTimeNow(){
		Calendar dateNow = Calendar.getInstance();
		String str = String.valueOf(dateNow.get(Calendar.YEAR)) +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + 
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0");
		
		return str;
	}
	    

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		FileUtil.path = path;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		FileUtil.filename = filename;
	}
}
