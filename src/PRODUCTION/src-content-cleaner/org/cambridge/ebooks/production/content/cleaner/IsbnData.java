package org.cambridge.ebooks.production.content.cleaner;

import java.util.List;

public class IsbnData {

    private String isbn;
    private List<String> toDelete;
    
    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    public List<String> getToDelete() {
        return toDelete;
    }
    public void setToDelete(List<String> toDelete) {
        this.toDelete = toDelete;
    }
    
}
