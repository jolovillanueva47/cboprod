package org.cambridge.ebooks.production.content.cleaner;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ApplicationProperties
{
  private Configuration config;
  private static final String propertyFile = System.getProperty("property.file");
  private static final ApplicationProperties APP_PROPERTIES = new ApplicationProperties();

  public static final String CONTROL_FOLDER = APP_PROPERTIES.config.getString("control.folder");
  public static final String CONTROL_ARCHIVE = APP_PROPERTIES.config.getString("control.archive");
  public static final String CONTROL_FILENAME = APP_PROPERTIES.config.getString("control.filename");
  public static final String CONTENT_FOLDER = APP_PROPERTIES.config.getString("content.folder");
  public static final String CONTENT_FILENAME = APP_PROPERTIES.config.getString("content.filename");
  
  public static final String DB_DRIVER = APP_PROPERTIES.config.getString("jdbc.driver");
  public static final String DB_USER = APP_PROPERTIES.config.getString("jdbc.user");
  public static final String DB_PASS = APP_PROPERTIES.config.getString("jdbc.pass");
  public static final String DB_URL = APP_PROPERTIES.config.getString("jdbc.url");

  private ApplicationProperties()
  {
    try
    {
      this.config = new PropertiesConfiguration(propertyFile + ".properties");
    }
    catch (ConfigurationException e)
    {
      System.err.println("[ConfigurationException] ApplicationProperties() message: " + e.getMessage());
    }
  }
}