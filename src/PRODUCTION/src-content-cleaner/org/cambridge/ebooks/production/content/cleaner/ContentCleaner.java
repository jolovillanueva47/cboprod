package org.cambridge.ebooks.production.content.cleaner;

import java.util.List;

public class ContentCleaner
{
  private static final String CONTROL_FOLDER = ApplicationProperties.CONTROL_FOLDER;
  private static final String CONTROL_ARCHIVE = ApplicationProperties.CONTROL_ARCHIVE;
  private static final String CONTROL_FILENAME = ApplicationProperties.CONTROL_FILENAME;
  private static final String CONTENT_FOLDER = ApplicationProperties.CONTENT_FOLDER;
  private static final String CONTENT_FILENAME = ApplicationProperties.CONTENT_FILENAME;

  public static void main(String[] args) {
    FileUtil fu = new FileUtil();

    List<String> listOfIsbn = fu.readContent(CONTROL_FOLDER + CONTROL_FILENAME);

    fu.setFilename(CONTENT_FILENAME);
    fu.setPath(CONTENT_FOLDER);

    if (fu.deleteAbstractText(listOfIsbn)) //bulk of the code
      System.out.println("Delete Operation completed successfully");
    else {
      System.out.println("[ERROR] Delete Operation encountered an error");
    }

    if (fu.moveFile(CONTROL_FOLDER + CONTROL_FILENAME, CONTROL_ARCHIVE + CONTROL_FILENAME + "_" + fu.dateTimeNow()))
      System.out.println("Successfully moved " + 
        CONTROL_FOLDER + CONTROL_FILENAME + " to archive as " + 
        CONTROL_ARCHIVE + CONTROL_FILENAME + "_" + fu.dateTimeNow());
    else
      System.out.println("[ERROR] Move Operation failed.");
  }
}