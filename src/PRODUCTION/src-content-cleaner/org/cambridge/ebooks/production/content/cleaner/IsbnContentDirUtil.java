package org.cambridge.ebooks.production.content.cleaner;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.content.cleaner.ApplicationProperties;

public class IsbnContentDirUtil {
	
	/**
	 * http://en.wikipedia.org/wiki/International_Standard_Book_Number
 	 * http://en.wikipedia.org/wiki/File:ISBN_Details.svg
	 */
	
	private static final String CONTENT_DIR = ApplicationProperties.CONTENT_FOLDER;
	private static final String SEPARATOR = File.separator;
	
	public static String getPath(String isbn, String filename) {
		
		String ean = isbn.substring(0, 3);
		String group = isbn.substring(3, 5);
		String publisher = isbn.substring(5, 9);
		String title = isbn.substring(9, 12);
		String checkDigit = isbn.substring(12, 13);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(getContentDir());
		sb.append(ean);
		sb.append(SEPARATOR); 
		sb.append(group);
		sb.append(SEPARATOR); 
		sb.append(publisher);
		sb.append(SEPARATOR); 
		sb.append(title);
		sb.append(SEPARATOR); 
		sb.append(checkDigit);
		sb.append(SEPARATOR);
		sb.append(filename);
		
		String isbnPath = sb.toString();
		
		File f = new File(isbnPath);
		return f.isFile() ? isbnPath : null;
	}
	
public static String checkLegacyFile(String isbn, String filename) {
        String fileCompletePath = getContentDir() + isbn + SEPARATOR + filename;
        
        File f = new File(fileCompletePath);
        return f.isFile() ? fileCompletePath : null;
    }
	
	private static String getContentDir() {
		String contentDir = CONTENT_DIR;		
		if(!StringUtils.endsWith(CONTENT_DIR, SEPARATOR)) {
			contentDir = CONTENT_DIR.concat(SEPARATOR);
		}		
		return contentDir;
	}
}