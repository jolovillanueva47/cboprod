package org.cambridge.ebooks.production.batch;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

public class ImageManip implements RenderListener {
	private static boolean CONSIDER_R = false; 
	private String filename;
	
	public static void main(String[] args) {
//		doToFiles("E:\\workspace\\ebooks\\",
//				new ActionToFile() { public void doToFile(String s) {
//					if (!s.toLowerCase().endsWith(".pdf")) {
//						new File(s).delete();
//					}
//		}});		
		doToFiles("E:\\workspace\\ebooks\\9780511676123\\",
				new ActionToFile() { public void doToFile(String s) {
					if (s.toLowerCase().endsWith(".pdf")) {
						System.out.println(s);
						ImageManip im = new ImageManip();
//						im.extract(s);
						im.replace(s);
					}
		}});
	}
	
    private void extract(String filename) {
    	try {
    		PdfReader reader = new PdfReader(current(filename));
	        PdfReaderContentParser parser = new PdfReaderContentParser(reader);
	        for (int i = 1; i <= reader.getNumberOfPages(); i++)
	            parser.processContent(i, this);
    	} catch (Exception e) { System.err.println(e.getMessage()); }
    }
    
	private void replace(String filename) {
		try {
			PdfReader reader = new PdfReader(filename);
			int count = reader.getXrefSize();
			for (int i = 0; i < count; i++) {
				PdfObject object = reader.getPdfObject(i);
				if (object != null && object.isStream()) {
					PRStream stream = (PRStream) object;
					String filter = String.valueOf(stream.get(PdfName.FILTER));
					if (filter.indexOf(String.valueOf(PdfName.JPXDECODE)) > -1) {
						String resource = filename.substring(0, filename.lastIndexOf("."))+"_"+i;
						File file = new File(resource+".jpg");
						if (file.exists()) {
							Properties p = getP(resource);
							java.awt.Image image = new ImageIcon(file.getAbsolutePath()).getImage();
							BufferedImage img = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
							Graphics2D g = img.createGraphics();
							g.drawImage(image, 0, 0, null);
							int width = (int) (img.getWidth());
							int height = (int) (img.getHeight());
							ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
							ImageIO.write(img, "JPG", imgBytes);
							stream.clear();
							stream.setData(imgBytes.toByteArray(), false, PRStream.NO_COMPRESSION);
							stream.put(PdfName.TYPE, PdfName.XOBJECT);
							stream.put(PdfName.SUBTYPE, PdfName.IMAGE);
							stream.put(PdfName.FILTER, PdfName.DCTDECODE);
							stream.put(PdfName.WIDTH, new PdfNumber(width));
							stream.put(PdfName.HEIGHT, new PdfNumber(height));
							stream.put(PdfName.BITSPERCOMPONENT, new PdfNumber(Integer.valueOf(p.getProperty("BITSPERCOMPONENT"))));
							stream.put(PdfName.COLORSPACE, PdfName.DEVICERGB);
						}
					}
				}
			}
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(filename.replaceAll("ebooks", "ebooks_out")));
			stamper.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private static void doToFiles(String s, ActionToFile atf) {
		File fo = new File(s);
		if (fo.isDirectory()) {
			String internalNames[] = fo.list();
			for (int i = 0; i < internalNames.length; i++)
				doToFiles(fo.getAbsolutePath() + "\\" + internalNames[i], atf);
		} else atf.doToFile(fo.getAbsolutePath());
	}
	
	private static void spawn(String filename, boolean invert) {
		Process p = null;
	    try { 
	      p=Runtime.getRuntime().exec("C:\\Program Files\\IrfanView\\i_view32.exe "+filename+".jp2 "+(invert?"/invert ":"")+"/convert="+filename+".jpg");
	    } catch(Exception e) { System.err.println(e.getMessage()); } 
	    finally { try { p.getErrorStream().close(); p.getInputStream().close(); p.getOutputStream().close(); } catch (Exception e) {} }
	}
	
	private static void setP(String resource, Properties pr) {
		Properties p = new Properties();
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			in = new FileInputStream(resource+".txt");
			p.load(in);
		} catch (Exception e) { System.err.println(e.getMessage()); } 
		finally { try { in.close(); } catch (Exception e) {} }
		
		try {
			p.putAll(pr);
			out = new FileOutputStream(resource+".txt");
			p.store(out, "");
			out.close();
		} catch (Exception e) { System.err.println(e.getMessage()); } 
		finally { try { out.close(); } catch (Exception e) {} }
	}
	
	private static Properties getP(String resource) {
		Properties p = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream(resource+".txt");
			p.load(in);
		} catch (Exception e) { System.err.println(e.getMessage()); } 
		finally { try { in.close(); } catch (Exception e) {} }		
		return p;
	}
	
	public void renderImage(ImageRenderInfo arg0) {
		try {
			PdfImageObject image = arg0.getImage();
			String filter = String.valueOf(image.get(PdfName.FILTER));
			if (filter.indexOf(String.valueOf(PdfName.JPXDECODE)) > -1) {
				String resource = filename.substring(0, filename.lastIndexOf("."))+"_"+arg0.getRef().getNumber();
				String cs = String.valueOf(image.get(PdfName.COLORSPACE));
				OutputStream os = new FileOutputStream(resource+".jp2");
		        os.write(image.getStreamBytes());
		        os.close();
		        spawn(resource, CONSIDER_R && cs.indexOf('R') > -1);
				Properties p = new Properties();
				p.setProperty("FILTER", filter);
				p.setProperty("COLORSPACE", cs);
				p.setProperty("BITSPERCOMPONENT", String.valueOf(image.get(PdfName.BITSPERCOMPONENT)));
				setP(resource, p);
			} 
		} catch (Exception e) { System.err.println(e.getMessage()); }
	}
	public void beginTextBlock() {}
	public void endTextBlock() {}
	public void renderText(TextRenderInfo arg0) {}
	private String current(String filename) { return this.filename = filename; }
}

interface ActionToFile { void doToFile(String s); }
