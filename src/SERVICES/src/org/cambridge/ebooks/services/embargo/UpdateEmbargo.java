package org.cambridge.ebooks.services.embargo;

import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;


public class UpdateEmbargo {
	private static String cron = "0 0 0 * * ?";

	public static void main(String args[]) throws SchedulerException {
		JobDetail job = new JobDetail("update_embargo_job", "update_embargo_group", UpdateEmbargoJob.class);
		CronTrigger trigger = null;
		
		try 
		{
			trigger = new CronTrigger("update_embargo_trigger", "update_embargo_group", cron);
		
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.scheduleJob(job, trigger);
			scheduler.start();
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
