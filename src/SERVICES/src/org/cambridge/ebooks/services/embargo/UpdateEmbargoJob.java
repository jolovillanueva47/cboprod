package org.cambridge.ebooks.services.embargo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class UpdateEmbargoJob implements StatefulJob {
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		Connection con = DataSource.getConnection();
		
		try 
		{
			if(con != null)
			{
				con.setAutoCommit(false);
				
				Statement stmt = con.createStatement();
				stmt.executeUpdate("UPDATE ebook_content SET status = 7 WHERE (status = 9 AND embargo_date < SYSDATE)");
				
				stmt.executeUpdate("UPDATE ebook " +
								   "SET status = 7 " +
								   "WHERE book_id IN ( SELECT ec.book_id " +
								                      "FROM ( SELECT book_id, DECODE( COUNT(status)- SUM(DECODE(status, 7, 1, 0)), 0, 'Y', 'N' ) toupdate " +
								                      		 "FROM ebook_content " +
								                      		 "WHERE type IN ('xml', 'chapter') " +
								                      		 "GROUP BY book_id ) ec " +
								                      "WHERE ec.toupdate = 'Y' )");
				con.commit();
			}

		} 
		catch(SQLException sqle)
		{
			System.err.println(UpdateEmbargoJob.class + " [SQLException] " + sqle.getMessage());
			try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println(UpdateEmbargoJob.class + " [SQLException] " + "error in rollback.");
			}
		}
		catch (Exception e) 
		{
			System.err.println(UpdateEmbargoJob.class + " [Exception] " + e.getMessage());
		}
		finally
		{
			DataSource.closeConnection(con);
		}
	}
	
	
}
