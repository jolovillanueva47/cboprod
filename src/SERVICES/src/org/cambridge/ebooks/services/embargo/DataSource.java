package org.cambridge.ebooks.services.embargo;

import java.sql.Connection;
import java.sql.DriverManager;

import org.cambridge.ebooks.services.properties.ServicesProperties;

/**
 * @author regin
 *
// * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataSource {

	static {
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public static Connection getConnection() {
		Connection con = null;
		try {
			String driverType = System.getProperty(ServicesProperties.DRIVERTYPE);
			String serverName = System.getProperty(ServicesProperties.SERVERNAME);
			String portNumber = System.getProperty(ServicesProperties.PORTNUMBER);
			String databaseName = System.getProperty(ServicesProperties.DATABASENAME);
			String user = System.getProperty(ServicesProperties.USER);
			String password = System.getProperty(ServicesProperties.PASSWORD);
			
			con = DriverManager.getConnection(driverType + ":@" + serverName + ":" + portNumber + ":" + databaseName, user, password);
		} catch (Exception e) {
			System.err.println(e);
		}
		return con;
	}
	
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
