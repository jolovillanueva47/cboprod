/*
 * Created on Jan 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.services.properties;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ServicesProperties {
	
	private static final Logger LOG = Logger.getLogger(ServicesProperties.class);
	
	public static String DRIVERTYPE = "db.drivertype";
	public static String SERVERNAME = "db.servername";
	public static String DATABASENAME = "db.databasename";
	public static String PORTNUMBER = "db.portnumber";
	public static String USER = "db.user";
	public static String PASSWORD = "db.password";	

	public static String DIR_SCRIPT = "dir.shell.script";
	
	private static final String indexerProperties = "C:/ebooks/workspace/cbo/SERVICES/service.properties";
	
	//private static final String indexerProperties = "/app/ebooks/contents/properties/indexer.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
}
