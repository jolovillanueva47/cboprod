package org.cambridge.ebooks.services.upload;

import java.io.IOException;
import java.io.InputStream;

import org.cambridge.ebooks.services.properties.ServicesProperties;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class PostUploadJob implements StatefulJob {
	
	private static long TWENTY_SECONDS = 20 * 1000;
	private static String SHELLSCRIPT = System.getProperty(ServicesProperties.DIR_SCRIPT);
	
	public PostUploadJob() {}

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		try {
	
			System.out.println("Started.");
			
			Thread t = new Thread();
			t.start();
			Thread.sleep(PostUploadJob.TWENTY_SECONDS);
	
			System.out.println("Running..");
			
			ProcessBuilder pb = new ProcessBuilder(PostUploadJob.SHELLSCRIPT);
			pb.redirectErrorStream(true); // use this to capture messages sent to stderr
			
			Process shell = pb.start();
			InputStream shellIn = shell.getInputStream(); // this captures the output from the command
			int shellExitStatus = shell.waitFor(); // wait for the shell to finish and get the return code
			System.out.println("shellExitStatus: " + shellExitStatus);
			
			// at this point you can process the output issued by the command
			// for instance, this reads the output and writes it to System.out:
			int c;
			while ((c = shellIn.read()) != -1) 
			{
				System.out.write(c);
			}
			
			// close the stream
			try 
			{
				shellIn.close();
			} 
			catch (IOException ignoreMe) 
			{
				System.err.println(PostUploadJob.class + " [ERROR] " + ignoreMe.getMessage());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
