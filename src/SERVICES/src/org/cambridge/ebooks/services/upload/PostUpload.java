package org.cambridge.ebooks.services.upload;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;


public class PostUpload {
	private static int SECONDS = 1 * 1000;

	public static void main(String args[]) throws SchedulerException {
		JobDetail job = new JobDetail("cron_job", "post_upload_group", PostUploadJob.class);
		
		SimpleTrigger trigger = new SimpleTrigger("cronjob_trigger", "post_upload_group", SimpleTrigger.REPEAT_INDEFINITELY, PostUpload.SECONDS);
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			
		scheduler.scheduleJob(job, trigger);
		scheduler.start();
		
	}
}
