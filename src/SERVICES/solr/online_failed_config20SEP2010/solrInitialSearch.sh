#!/bin/sh
DUMP_DIR="/app/jboss-5.1.0.GA/bin/dump"
SOLR_SEARCH="http://loghost:8280/solr/search/select?qt=quick"
WGET_OPTIONS="-q -P"
PARAM=""
COUNTER=1

SEARCH_TERM="cambridge"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="university"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="s.a"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the cia fbi and origins of 9/11"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="color"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="externalising problems and inhibition at age 3 years"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="medicine for cancer"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="publication manual of the american psychological association"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="medicine"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="evidence for the nature of age effects in second language"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="chris alford"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="increasing returns path dependence and the study of politics"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="psychology"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the regional dimensions of state failure"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="field"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="marine biology"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="china crisis"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="trawl"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cathedral chapter"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="relationship"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="sentence processing"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="motherese"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="maze"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="money"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="democracy"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="evidence from hungary romania russia and ukraine"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="st. andrews"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the n-town plays"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="political stability"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="block technique for rule acquisition in incomplete information systems"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="sexuals"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="nolan spectrum opinion"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="application of an aesthetic evaluation model to data"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="meneveau"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="oil spill and chemical pollution mapping using gis"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="economic growth"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="club"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="scaris"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="ed. richard beadle"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="power"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="abkhazia"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="oxford"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="marketing"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="tenochtitlan"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="wireless"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="democracy"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="international"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="analysis"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="theory"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="project management"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="social worker and health care"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="yearbook of international network on chip"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="social worker and health care"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="book"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="john loughlin"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="how english songs can help primary students to improve general english listening skill in esl classroom"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the cultural context of youth suicide in australia"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="correlations between bed occupancy rates"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="prediction of difficult tracheal intubation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cambridge archaeological journal 14 71-77."
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="managing threats to welfare"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="property rights"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="universe"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="self-efficacy in teachers in measurements"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="penn state cage free"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="congo china"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cross-linguistic evidence for the nature of age effects in second language"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="compare"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="life"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="islam"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="economic development"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="shopping for value  insurance distribution in the information age"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="series"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="panel data econometric"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="social media meets insurance regulation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="when buying auto insurance online"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="prevalence and management in a united kingdom"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="yeti not a snowman"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="major depression and cbt"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="neural correlates of trait anxiety in fear extinction"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="an introduction"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="comparative sociology"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the cambridge companion to medieval english theatre"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="puberty"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="anic"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="ran"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="stefan wolff"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="diversification's effect on firm value"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="innovation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="commercial law of turk"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="rood"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="formal methods for software architectures"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="osteoarthritis"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="alan j."
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="ardnamurchan"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="save thousands on insurance"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="houses"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="bone conduction auditory brainstem response in"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cross-linguistic"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="pronounciation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="forensic investigation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="j.a"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="willy brandt series of working papers in international migration and ethnic relations"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cambridge press"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="employment and wage effects of privatisation"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="equality impact assessment"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="frances women and the images of woman in the spanish civil war"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="cerebral palsy physiotherapy management"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="rock in space"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the influence of emotions on inhibitory functioning"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="composition mathematica"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="ideas policy"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="evaluation performance"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="origin of columnar basalt"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="british tertiary"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="education"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="stomach"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="pilates"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="loughlin"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

echo "Finish warming searcher"