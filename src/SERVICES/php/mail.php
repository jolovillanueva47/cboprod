<?php
include('settings.php');
require_once "lib/Swift.php";
require_once("lib/Swift/Connection/SMTP.php");
$logfilename = dirname(__FILE__).'/mail_'.date("Ymd").'.log';
$tmpfilename = dirname(__FILE__).'/mail.pid';
if (!($tmpfile = @fopen($tmpfilename,"w")))
	exit;
if (!@flock( $tmpfile, LOCK_EX | LOCK_NB, &$wouldblock) || $wouldblock) {
	@fclose($tmpfile);
	exit;
}
try {
	$g = glob (OUTBOX.'*.mail');
	if (count($g) <= 0) exit;	

	$smtp = new Swift_Connection_SMTP(SMTP);
	$smtp->setTimeout(180);
	$mailer = new Swift($smtp); 
	
	foreach ($g as $v) {
		$file = pathinfo($v);
		if ($file['extension'] == 'mail') {
			$lines = file(OUTBOX.$file['basename']);
			foreach ($lines as $line_num => $line) {
				$segment = explode(':', $line, 2);
				if ($segment[0] == 'from') $from = $segment[1];
				elseif ($segment[0] == 'to') $tos = explode(',', $segment[1]);
				elseif ($segment[0] == 'cc') $ccs = explode(',', $segment[1]);
				elseif ($segment[0] == 'bcc') $bccs = explode(',', $segment[1]);
				elseif ($segment[0] == 'subject') $subject = '*** TEST *** '.$segment[1];
				elseif ($segment[0] == 'body') {
					$body = $segment[1];
					while (++$line_num < count($lines)) 
						$body .= $lines[$line_num];
					break;
				}
			}
		   $message = new Swift_Message($subject, $body, "text/html");
		   $recipients = new Swift_RecipientList();
		   if (count($tos) > 0) foreach ($tos as $to) if (strlen($to) > 0) $recipients->addTo($to); 
		   if (count($ccs) > 0) foreach ($ccs as $cc) if (strlen($cc) > 0) $recipients->addCc($cc);
		   if (count($bccs) > 0) foreach ($bccs as $bcc) if (strlen($bcc) > 0) $recipients->addBcc($bcc); 
		   error_log('Processing '.OUTBOX.$file['basename'].'...', 3, $logfilename);
		   if ($mailer->send($message, $recipients, new Swift_Address($from))) {
				rename(OUTBOX.$file['basename'], ARCHIVE.$file['basename']);
				error_log("OK\n", 3, $logfilename);
			} else 
				error_log("ERROR\n", 3, $logfilename);
		}
   }
   $mailer->disconnect();   
} catch (Swift_ConnectionException $e) {
	error_log("Swift_ConnectionException:".$e->getMessage()."\n", 3, $logfilename);
} catch (Swift_Message_MimeException $e) {
	error_log("Swift_Message_MimeException:".$e->getMessage()."\n", 3, $logfilename);
}
?>