#!/bin/sh
DUMP_DIR="/app/jboss-5.1.0.GA/bin/dump"
SOLR_SEARCH="http://loghost:8180/search/select?qt=quick"
WGET_OPTIONS="-q -P"
PARAM=""
COUNTER=1

SEARCH_TERM="cambridge"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="university"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="s.a"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="the cia fbi and origins of 9/11"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="color"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="externalising problems and inhibition at age 3 years"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="medicine for cancer"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="publication manual of the american psychological association"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="medicine"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

SEARCH_TERM="evidence for the nature of age effects in second language"
SEARCH_QUERY="&q=chapter_fulltext:($SEARCH_TERM) OR article_fulltext:($SEARCH_TERM) \
OR title:($SEARCH_TERM) OR author_name:($SEARCH_TERM) \
OR author_affiliation:($SEARCH_TERM) OR abstract:($SEARCH_TERM) \
OR keyword_norms:($SEARCH_TERM) OR contributor_name:($SEARCH_TERM) \
OR main_title:($SEARCH_TERM) OR main_subtitle:($SEARCH_TERM) \
OR volume_title:($SEARCH_TERM) OR subject:($SEARCH_TERM) \
OR series:($SEARCH_TERM) OR subtitle:($SEARCH_TERM)"
wget $WGET_OPTIONS$DUMP_DIR "$SOLR_SEARCH$SEARCH_QUERY$PARAM"
echo $COUNTER
COUNTER=`expr $COUNTER + 1`

echo "Finish warming searcher"