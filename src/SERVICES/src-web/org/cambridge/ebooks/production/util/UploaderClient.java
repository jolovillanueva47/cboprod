package org.cambridge.ebooks.production.util;

import java.util.Properties;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

public class UploaderClient {

	UploaderClient() throws Exception {

		Properties properties = new Properties();
		properties.put(	Context.INITIAL_CONTEXT_FACTORY,
						"org.jnp.interfaces.NamingContextFactory");
		properties.put(	Context.URL_PKG_PREFIXES,
						"org.jboss.naming:org.jnp.interfaces");
		properties.put(Context.PROVIDER_URL, "localhost:1099");
		InitialContext ctx = new InitialContext(properties);

		Queue queue = (Queue) ctx.lookup("queue/uploader/logger");
		QueueConnectionFactory qcf = (QueueConnectionFactory) ctx
				.lookup("ConnectionFactory");

		sendMessage(qcf, queue, "uploader");
	}

	public static void main(String[] args) throws Exception {
		new UploaderClient();
	}

	public void sendMessage(QueueConnectionFactory qcf, Queue queue, String remarks)
			throws Exception {
		QueueConnection qc = qcf.createQueueConnection();
		try {
			QueueSession qs = qc.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			QueueSender sender = qs.createSender(queue);
			Message payload = qs.createMapMessage();

			//insert your code here
			payload.setStringProperty("CONTENT_ID", "123456");
			payload.setStringProperty("EISBN", "1234567");
			payload.setStringProperty("SERIES_ID", "12345678");
			payload.setStringProperty("STATUS", "14");
			payload.setStringProperty("REMARKS", remarks);
			payload.setStringProperty("FILENAME", "filename");
			payload.setStringProperty("LOAD_TYPE", "loadtype");
			payload.setStringProperty("USERNAME", "testkoto");
			payload.setStringProperty("TYPE", "Book");

			sender.send(payload);
			sender.close();
			qs.close();

		} finally {
			qc.close();
		}
	}
}