package org.cambridge.ebooks.online.indexer;

import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;

public class BookLandingIndexerMain {
	
	public static void main(String[] args) {
		OnlineIndexer oi = new OnlineIndexer();
		System.out.println("[OnlineIndexer] main");		
		oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.BOOK_LANDING_INDEX), "book");
	}
}
