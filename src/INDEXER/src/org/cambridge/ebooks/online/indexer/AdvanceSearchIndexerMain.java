package org.cambridge.ebooks.online.indexer;

import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;

public class AdvanceSearchIndexerMain {
	public static void main(String[] args) {
		OnlineIndexer oi = new OnlineIndexer();
		System.out.println("[OnlineIndexer] main");		
		oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.SEARCH_INDEX), "search");
	}
}
