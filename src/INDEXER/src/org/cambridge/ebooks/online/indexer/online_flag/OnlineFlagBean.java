package org.cambridge.ebooks.online.indexer.online_flag;

public class OnlineFlagBean {
	private String isbn;
	private String onlineFlag;
	
	
	public OnlineFlagBean(String isbn, String onlineFlag) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getOnlineFlag() {
		return onlineFlag;
	}
	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}	
	public String getFlag(){
		String flag = "0";
		if("N".equals(onlineFlag)){
			flag = "0";
		}else if("Y".equals(onlineFlag)){
			flag = "1";
		}
		
		return flag;
	}
}
