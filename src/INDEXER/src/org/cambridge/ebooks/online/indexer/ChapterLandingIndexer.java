package org.cambridge.ebooks.online.indexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.cambridge.ebooks.online.indexer.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.online.indexer.document.EbookDetailsBean;
import org.cambridge.ebooks.online.indexer.document.PublisherNameBean;
import org.cambridge.ebooks.online.indexer.document.RoleAffBean;
import org.cambridge.ebooks.online.indexer.document.SubjectBean;
import org.cambridge.ebooks.online.indexer.online_flag.OnlineFlagList;
import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;
import org.cambridge.ebooks.online.indexer.util.IndexSearchUtil;
import org.cambridge.ebooks.online.indexer.util.IndexerUtil;
import org.cambridge.ebooks.online.indexer.util.StringUtil;
import org.cambridge.util.Misc;

public class ChapterLandingIndexer {
	
//	private String currentBookId;
	
//	public ChapterLandingIndexer(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
//			HashMap<String,String> contents, HashMap<String,String> tlMap, IndexWriter writer) throws Exception{
//		createChapterLandingIndex(ebook, chapters, contents, tlMap, writer);
//	}
	
	/**
	 * create chapter landing index
	 * @param ebook
	 * @param chapters
	 * @param contentMap
	 * @throws Exception 
	 */
	public static void createChapterLandingIndex(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chapters,
			HashMap<String, String> contentMap, HashMap<String,String> tlMap, IndexWriter writer, OnlineFlagList oflist) throws Exception{
		// Book Landing Page Indexer
		//System.out.println("--- CHAPTER LANDING INDEX START ---");
		// per content
		indexChapterLandingPerContent(ebook, chapters, contentMap, tlMap, writer, oflist);			
		
		// Per author	  
		//indexChapterBookLandingPerAuthor(ebook, chapters);				        
		indexBookLandingPerAuthor(ebook, false, null, writer);		
					
		// per subject			
		//indexChapterBookPerSubject(ebook, chapters);
		indexBookPerSubject(ebook, false, null, writer);
		
		// per content-item
		//indexBookPerContent(ebook, chapters);			

		// Per book contributor
		indexChapterPerContributor(ebook.getBookId(), writer);
		//System.out.println("--- CHAPTER LANDING INDEX END ---");
	}
	
	/**
	 * indexes contributor per contentItem
	 * @param bookId
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	private static void indexChapterPerContributor(String bookId, IndexWriter writer) throws CorruptIndexException, IOException {
		
			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(OnlineIndexerProperties.INDEX), 
					"ELEMENT: contributor AND BOOK_ID: " + bookId);		
			
			Field contributorField = new Field(IndexerUtil.CONTRIBUTOR, "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field affField = new Field(IndexerUtil.AFFILIATION, "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field docTypeField = new Field(IndexerUtil.DOC_TYPE, IndexerUtil.BOOK_CONTRIBUTOR, Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookIdField = new Field(IndexerUtil.BOOK_ID, "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentIdField = new Field(IndexerUtil.CONTENT_ID, "", Field.Store.YES, Field.Index.ANALYZED);
			Field contributorCollabField = new Field(IndexerUtil.CONTRIBUTOR_COLLAB, "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			for(Document doc : docs ){						
				String name = doc.get(IndexerUtil.CONTRIBUTOR);
				String aff = doc.get(IndexerUtil.AFFILIATION);
				String contribCollab = doc.get(IndexerUtil.CONTRIBUTOR_COLLAB);
				Document newDoc = new Document();
				if(Misc.isNotEmpty(name)) {
					contributorField.setValue(name);
					newDoc.add(contributorField);
				}
				if(Misc.isNotEmpty(aff)) {
					affField.setValue(aff);
					newDoc.add(affField);
				}
				if(Misc.isNotEmpty(contribCollab)) {
					contributorCollabField.setValue(contribCollab);
					newDoc.add(contributorCollabField);
				}
				
				//DOC_TYPE
				newDoc.add(docTypeField);
				
				//BOOK_ID
				bookIdField.setValue(bookId);
				newDoc.add(bookIdField);
				
				// CONTENT_ID
				contentIdField.setValue(doc.get(IndexerUtil.PARENT_ID));
				newDoc.add(contentIdField);
				
				writer.addDocument(newDoc);
			}
		
	}
	
	private static void indexBookLandingPerAuthor(EbookDetailsBean ebook, boolean addContentId, 
			String contentId, IndexWriter writer) throws CorruptIndexException, IOException{
		
		Document doc = null;		
		
		if(!Misc.isEmpty(ebook.getAuthorAffList())) {
			List<RoleAffBean> authorAffList = new ArrayList<RoleAffBean>();
			authorAffList = ebook.getAuthorAffList();
			
			Field docTypeField = new Field("DOC_TYPE", "book_author", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookIdField = new Field("BOOK_ID", "", Field.Store.YES, Field.Index.ANALYZED);
			Field nameField = new Field("NAME", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field fixNameField = new Field("FIX_NAME", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field affField = new Field("AFFILIATION", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field positionField = new Field("POSITION", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field roleField = new Field("ROLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentIdField = new Field("CONTENT_ID", "", Field.Store.YES, Field.Index.ANALYZED);
			
			for(RoleAffBean roleAff : authorAffList){
				doc = new Document();
				doc.add(docTypeField);
				
				if(Misc.isNotEmpty(ebook.getBookId())) {
					bookIdField.setValue(ebook.getBookId());
					doc.add(bookIdField);
				}
				
				if(Misc.isNotEmpty(roleAff.getAuthor())) { 	
					nameField.setValue(roleAff.getAuthor());
					doc.add(nameField);
				}
				if(Misc.isNotEmpty(roleAff.getFixAuthor())) {
					fixNameField.setValue(roleAff.getFixAuthor());
					doc.add(fixNameField);
				}
				if(Misc.isNotEmpty(roleAff.getAffiliation())) {
					affField.setValue(roleAff.getAffiliation());
					doc.add(affField);
				}
				if(Misc.isNotEmpty(roleAff.getPosition())) {
					positionField.setValue(roleAff.getPosition());
					doc.add(positionField);
				}
				if(Misc.isNotEmpty(roleAff.getRole())) { 	
					roleField.setValue(roleAff.getRole());
					doc.add(roleField);
				}									
				
				if(addContentId){
					if(Misc.isNotEmpty(contentId)){
						contentIdField.setValue(contentId);
						doc.add(contentIdField);
					}
				}
				
				writer.addDocument(doc);
			}
		}		
	}
	
	/**
	 * Index book Per Subject
	 * @param ebook
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	private static void indexBookPerSubject(EbookDetailsBean ebook, boolean addContentId, 
			String contentId, IndexWriter writer) throws CorruptIndexException, IOException{
		
			if(!Misc.isEmpty(ebook.getSubjectList())) {
				List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
				subjectList = ebook.getSubjectList();
				
				Field docTypeField = new Field("DOC_TYPE", "subject", Field.Store.YES, Field.Index.NOT_ANALYZED);
				Field bookIdField = new Field("BOOK_ID", "", Field.Store.YES, Field.Index.ANALYZED);
				Field subjectField = new Field("SUBJECT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
				Field subjectCodeField = new Field("SUBJECT_CODE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
				Field contentIdField = new Field("CONTENT_ID", "", Field.Store.YES, Field.Index.ANALYZED);
				
				for(SubjectBean subject : subjectList){
					Document doc = new Document();
					
					doc.add(docTypeField);
					
					if(Misc.isNotEmpty(ebook.getBookId())) {		
						bookIdField.setValue(ebook.getBookId());
						doc.add(bookIdField);
					}
					if(Misc.isNotEmpty(subject.getSubject())) { 
						subjectField.setValue(subject.getSubject());
						doc.add(subjectField);
					}
					if(Misc.isNotEmpty(subject.getCode())) { 
						subjectCodeField.setValue(subject.getCode());
						doc.add(subjectCodeField);
					}
					
					//add contentId
					if(addContentId){
						if(Misc.isNotEmpty(contentId)){
							contentIdField.setValue(contentId);
							doc.add(contentIdField);
						}
					}
					
					writer.addDocument(doc);
				}				
			}
	}
	
	/**
	 * adds content id to doc
	 * @param doc
	 * @param addContentId
	 * @param contentId
	 */
	private void addContentId(Document doc, boolean addContentId, String contentId) {
		if(addContentId){
			if(Misc.isNotEmpty(contentId)){
				doc.add(new Field("CONTENT_ID", contentId, Field.Store.YES, Field.Index.ANALYZED));
			}
		}
	}
	
	/**
	 * create per content chapter landing
	 * @param ebook
	 * @param chaptersList
	 * @param contentMap
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	private static void indexChapterLandingPerContent(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList,
			HashMap<String, String> contentMap, HashMap<String,String> tlMap, IndexWriter writer, OnlineFlagList oflist) throws CorruptIndexException, IOException{		
		
		if(chaptersList != null && chaptersList.size() > 0)	{			
			Document doc;
			
			Field docTypeField = new Field(IndexerUtil.DOC_TYPE, IndexerUtil.CONTENT_DETAILS, Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookIdField = new Field("BOOK_ID", "", Field.Store.YES, Field.Index.ANALYZED);
			Field bookTitleField = new Field("BOOK_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookTitleAlphaField = new Field("BOOK_TITLE_ALPHASORT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookSubTitleField = new Field("BOOK_SUB_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field seriesCodeField = new Field("SERIES_CODE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field seriesNameField = new Field("SERIES_NAME", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field seriesNumberField = new Field("SERIES_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field copyrightField = new Field("COPYRIGHT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field printDateField = new Field("PRINT_PUBLICATION_DATE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field onlineDateField = new Field("ONLINE_PUBLICATION_DATE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field onlineIsbnField = new Field("ONLINE_ISBN", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field hardbackIsbnField = new Field("HARDBACK_ISBN", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field paperbackIsbnField = new Field("PAPERBACK_ISBN", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookDoiField = new Field("BOOK_DOI", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field editionNumberField = new Field("EDITION_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field editionField = new Field("EDITION", "", Field.Store.YES, Field.Index.ANALYZED);
			Field coverImageField = new Field("COVER_IMAGE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field coverImageThumbField = new Field("COVER_IMAGE_THUMBNAIL", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeField = new Field("VOLUME", "", Field.Store.YES, Field.Index.ANALYZED);
			Field otherVolumeField = new Field("OTHER_VOLUME", "", Field.Store.YES, Field.Index.ANALYZED);
			Field liveDateField = new Field("LIVE_DATE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field publisherField = new Field("PUBLISHER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentId = new Field("CONTENT_ID", "", Field.Store.YES, Field.Index.ANALYZED);			
			Field contentTitleField = new Field("CONTENT_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentLabelField = new Field("CONTENT_LABEL", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentSubTitleField = new Field("CONTENT_SUB_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field firstPageField = new Field("FIRST_PAGE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field pageField = new Field("PAGE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);			
			Field pdfField = new Field("PDF", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field chapterDoiField = new Field("CHAPTER_DOI", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contributorsField = new Field("CONTENT_CONTRIBUTORS", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contributorsDispField = new Field("CONTENT_CONTRIBUTORS_DISPLAY", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contributorsCollabField = new Field("CONTENT_CONTRIBUTOR_COLLAB", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentExtractField = new Field("CONTENT_EXTRACT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentImageExtractField = new Field("CONTENT_IMAGE_EXTRACT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field parentIdField = new Field("PARENT_ID", "", Field.Store.YES, Field.Index.ANALYZED);
			Field problemField = new Field("PROBLEM", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentTypeField = new Field("CONTENT_TYPE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentSubChaptersField = new Field("CONTENT_SUB_CHAPTERS", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookLevelField = new Field("BOOK_LEVEL", "", Field.Store.YES, Field.Index.ANALYZED);
			Field tocItem = new Field("TOC_ITEM", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field partTitleField = new Field("PART_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field partNumberField = new Field("PART_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeTitleField = new Field("VOLUME_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeNumberField = new Field("VOLUME_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			 
			for(EbookChapterDetailsBean chapter : chaptersList) {
				doc = new Document();
				
				doc.add(new Field(OnlineIndexer.FLAG, oflist.getOnlineFlagValue(ebook.getEisbn()), Field.Store.YES, Field.Index.ANALYZED));
								
				// DOC_TYPE
				doc.add(docTypeField);
				
				// BOOK_ID
				if(Misc.isNotEmpty(ebook.getBookId())) {	
					bookIdField.setValue(ebook.getBookId());
					doc.add(bookIdField);
				}
				
				// BOOK_TITLE
				if(Misc.isNotEmpty(ebook.getMainTitle())) {	
					bookTitleField.setValue(ebook.getMainTitle());
					doc.add(bookTitleField);
				}
				
				// BOOK_TITLE_ALPHASORT
				if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
					bookTitleAlphaField.setValue(ebook.getMainTitleAlphaSort());
					doc.add(bookTitleAlphaField);
				}
				
				// BOOK_SUB_TITLE
				if(Misc.isNotEmpty(ebook.getSubTitle())) {
					bookSubTitleField.setValue(ebook.getSubTitle());
					doc.add(bookSubTitleField);
				}
				
				// SERIES_CODE
				if(Misc.isNotEmpty(ebook.getSeriesCode())) { 	
					seriesCodeField.setValue(ebook.getSeriesCode());
					doc.add(seriesCodeField);
				}
				
				// SERIES_NAME
				if(Misc.isNotEmpty(ebook.getSeries())) {		
					seriesNameField.setValue(ebook.getSeries());
					doc.add(seriesNameField);
				}
				
				// SERIES_NUMBER
				if(Misc.isNotEmpty(ebook.getSeriesNumber())) {	
					seriesNumberField.setValue(ebook.getSeriesNumber());
					doc.add(seriesNumberField);
				}
				
				// COPYRIGHT
				if(Misc.isNotEmpty(ebook.getCopyrightStatement())) {
					copyrightField.setValue(IndexerUtil.removeBr(ebook.getCopyrightStatement()));
					doc.add(copyrightField);
				}
				
				// PRINT_PUBLICATION_DATE
				if(Misc.isNotEmpty(ebook.getPrintDate())) { 			
					printDateField.setValue(IndexerUtil.removeBr(ebook.getPrintDate()));
					doc.add(printDateField);
				}
				
				// ONLINE_PUBLICATION_DATE
				if(Misc.isNotEmpty(ebook.getOnlineDate())) {
					onlineDateField.setValue(IndexerUtil.removeBr(ebook.getOnlineDate()));
					doc.add(onlineDateField);
				}
				
				// ONLINE_ISBN
				if(Misc.isNotEmpty(ebook.getEisbn())) {		
					onlineIsbnField.setValue(ebook.getEisbn());
					doc.add(onlineIsbnField);
				}
				
				// HARDBACK_ISBN
				if(Misc.isNotEmpty(ebook.getHardback())) {			
					hardbackIsbnField.setValue(ebook.getHardback());
					doc.add(hardbackIsbnField);
				}
				
				// PAPERBACK_ISBN
				if(Misc.isNotEmpty(ebook.getPaperback())) {	
					paperbackIsbnField.setValue(ebook.getPaperback());
					doc.add(paperbackIsbnField);
				}
				
				// BOOK_DOI
				if(Misc.isNotEmpty(ebook.getDoi())) {		
					bookDoiField.setValue(ebook.getDoi());
					doc.add(bookDoiField);
				}
				
				// EDITION_NUMBER
				if(Misc.isNotEmpty(ebook.getEditionNumber())) {
					editionNumberField.setValue(ebook.getEditionNumber());
					doc.add(editionNumberField);
				}
				
				// EDITION
				if(Misc.isNotEmpty(ebook.getEdition())) {
					editionField.setValue(ebook.getEdition());
					doc.add(editionField);
				}
				
				// COVER_IMAGE
				if(Misc.isNotEmpty(ebook.getStandardImage())) {	
					coverImageField.setValue(ebook.getStandardImage().replace("../", ""));
					doc.add(coverImageField);
				}
				
				// COVER_IMAGE_THUMBNAIL
				if(Misc.isNotEmpty(ebook.getThumbImage())) {	
					coverImageThumbField.setValue(ebook.getThumbImage().replace("../", ""));
					doc.add(coverImageThumbField);
				}
				
				// VOLUME
				if(Misc.isNotEmpty(ebook.getVolume())) { 		
					doc.add(volumeField);
				}
				
				// OTHER_VOLUME
				if(Misc.isNotEmpty(ebook.getOtherVolume())) { 
					otherVolumeField.setValue(ebook.getOtherVolume());
					doc.add(otherVolumeField);
				} 					
				
				if(Misc.isNotEmpty(ebook.getPartTitle())) { 
					partTitleField.setValue(ebook.getPartTitle());
					doc.add(partTitleField);
				}
				if(Misc.isNotEmpty(ebook.getPartNumber())) { 	
					partNumberField.setValue(ebook.getPartNumber());
					doc.add(partNumberField);
				}  
				if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
					volumeTitleField.setValue(ebook.getVolumeTitle());
					doc.add(volumeTitleField);
				}  
				if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 	
					volumeNumberField.setValue(ebook.getVolumeNumber());
					doc.add(volumeNumberField);
				}  
				
				// LIVE_DATE
				String liveDate = OnlineIndexer.getEBookLiveDate(ebook.getBookId());
				if(Misc.isNotEmpty(liveDate)) {
					liveDateField.setValue(liveDate);
					doc.add(liveDateField);
				}
				
				// PUBLISHER
				if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
					List<PublisherNameBean> publisherList = ebook.getPubNameList();
					StringBuffer publisherDisplay = new StringBuffer();
					for (Iterator<PublisherNameBean> it = publisherList.iterator();it.hasNext();) {
						PublisherNameBean bean = it.next();
						publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
						if (it.hasNext()) {
							publisherDisplay.append(", ");
						}
					}
					publisherField.setValue(publisherDisplay.toString());
					doc.add(publisherField);
				}
				
				// CONTENT_ID
				if(Misc.isNotEmpty(chapter.getContentId())) {
					contentId.setValue(chapter.getContentId());
					doc.add(contentId);
				}
				
				// CONTENT_TITLE
				if(Misc.isNotEmpty(chapter.getHeadingTitle())) {
					contentTitleField.setValue(chapter.getHeadingTitle());
					doc.add(contentTitleField);
				}
				
				// CONTENT_LABEL
				if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
					contentLabelField.setValue(chapter.getHeadingLabel());
					doc.add(contentLabelField);
				}
				
				// CONTENT_SUB_TITLE
				if(Misc.isNotEmpty(chapter.getHeadingSubtitle())) {
					contentSubTitleField.setValue(chapter.getHeadingSubtitle());
					doc.add(contentSubTitleField);
				}
				
				// FIRST_PAGE
				if(Misc.isNotEmpty(chapter.getPageStart())) {	
					firstPageField.setValue(chapter.getPageStart());
					doc.add(firstPageField);
				}
				
				// PAGE
				if(Misc.isNotEmpty(chapter.getPageEnd())) { 
					pageField.setValue("pp. " + chapter.getPageStart() + "-" + chapter.getPageEnd());
					doc.add(pageField);
				}
				
				// PDF
				if(Misc.isNotEmpty(chapter.getPdfFilename())) {	
					pdfField.setValue(chapter.getPdfFilename());
					doc.add(pdfField);
				}
				
				// CHAPTER_DOI
				if(Misc.isNotEmpty(chapter.getDoi())) {	
					chapterDoiField.setValue(chapter.getDoi());
					doc.add(chapterDoiField);
				}
				
				// CONTRIBUTOR
				indexContentContributor(chapter.getContentId(), doc, 
						contributorsField, contributorsDispField, contributorsCollabField);
				
				// CONTENT_EXTRACT
				if(Misc.isNotEmpty(chapter.getAbstractContent())) {
					contentExtractField.setValue(chapter.getAbstractContent());
					doc.add(contentExtractField);
				}
				
				// CONTENT_IMAGE_EXTRACT
				if(Misc.isNotEmpty(chapter.getAbstractImage()))	{
					contentImageExtractField.setValue(chapter.getAbstractImage());
					doc.add(contentImageExtractField);
				}
				
				// PARENT_ID
				if(Misc.isNotEmpty(chapter.getParentId())) {	
					parentIdField.setValue(chapter.getParentId());
					doc.add(parentIdField);
				}
				
				// PROBLEM
				if(Misc.isNotEmpty(chapter.getProblem())) {	
					problemField.setValue(chapter.getProblem());
					doc.add(problemField);
				}
				
				// CONTENT_TYPE
				if(Misc.isNotEmpty(chapter.getContentType())) {
					contentTypeField.setValue(chapter.getContentType());
					doc.add(contentTypeField);
				}
				
				// CONTENT_SUB_CHAPTERS
				contentSubChaptersField.setValue(IndexerUtil.hasSubchapters(contentMap, chapter));
				doc.add(contentSubChaptersField);
				
				// BOOK_LEVEL
				bookLevelField.setValue(IndexerUtil.isTopLevel(tlMap, chapter));
				doc.add(bookLevelField);
				
				// TOC_ITEM
				if(Misc.isNotEmpty(chapter.getToc())) {
					tocItem.setValue(
							chapter.getToc().replaceAll("<em>", "")
							.replaceAll("</em>", "")
							.replaceAll("<br />", "=DEL=")
					);
					doc.add(tocItem);
				}
				
				writer.addDocument(doc);
			}
		}
	}
	
	private static void indexContentContributor(String contentId, Document document, 
			Field contributorsField, Field contributorsDispField, Field contribCollabField) {
		
		try {			
			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(OnlineIndexerProperties.INDEX), 
					"ELEMENT: contributor AND PARENT_ID: " + contentId);	
			
			List<String> contributors = new ArrayList<String>();
			List<String> contribCollabs = new ArrayList<String>();
			ArrayList<String> allContribs = new ArrayList<String>();
			
			for(Document doc : docs){						
				String name = doc.get(IndexerUtil.CONTRIBUTOR);
				String collab = doc.get(IndexerUtil.CONTRIBUTOR_COLLAB); 
				if(Misc.isNotEmpty(name)) { 
					contributors.add(name);
				}
				if(Misc.isNotEmpty(collab)) {
					contribCollabs.add(collab);
				}
			}
			allContribs.addAll(contributors);
			allContribs.addAll(contribCollabs);
			allContribs.trimToSize();
			
			String contributorDisplay = StringUtil.getContributorDisplay(allContribs);
			String contributor = StringUtil.getContributors(contributors);
			String contributorCollab = StringUtil.getContributors(contribCollabs);
			if(Misc.isNotEmpty(contributor)) {
				contributorsField.setValue(contributor.trim());
				contributorsDispField.setValue(contributorDisplay.trim());
				contribCollabField.setValue(contributorCollab.trim());
				document.add(contributorsField);
				document.add(contributorsDispField);
				document.add(contribCollabField);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	public String getCurrentBookId() {
//		return currentBookId;
//	}
//
//	public void setCurrentBookId(String currentBookId) {
//		this.currentBookId = currentBookId;
//	}
}
