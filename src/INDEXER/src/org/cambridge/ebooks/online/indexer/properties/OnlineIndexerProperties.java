package org.cambridge.ebooks.online.indexer.properties;

import java.io.FileInputStream;
import java.util.Properties;

public class OnlineIndexerProperties {
	public static String DRIVERTYPE = "db.drivertype";
	public static String SERVERNAME = "db.servername";
	public static String DATABASENAME = "db.databasename";
	public static String PORTNUMBER = "db.portnumber";
	public static String USER = "db.user";
	public static String PASSWORD = "db.password";	

	public static String CHAPTER_LANDING_INDEX 	= "chapter.landing.index.dir";
	public static String BOOK_LANDING_INDEX 	= "book.landing.index.dir";
	public static String SEARCH_INDEX 			= "search.index.dir";
	public static String INDEX 					= "index.dir";
	public static String STOPWORDS 				= "file.stopwords";
	public static String SUBJECT_LANDING_INDEX	= "subject.index.dir";
	public static String SERIES_LANDING_INDEX	= "series.index.dir";
	
	public static String DIR_SCRIPT = "dir.shell.script";
	
	public static String ISBN_LIST = "isbn.list";
	
	//private static final String indexerProperties = "C:/eclipse-jee-ganymede-SR1-win32/eclipse/workspace/cbo-2-prod/PRODUCTION/src-web/org/cambridge/ebooks/production/util/application.properties";
	private static final String indexerProperties = "/app/ebooks_old/online_indexer/properties/online.indexer.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
