package org.cambridge.ebooks.online.indexer.document;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.cambridge.ebooks.online.indexer.util.ImageUtil;
import org.cambridge.ebooks.online.indexer.util.IndexSearchUtil;
import org.cambridge.ebooks.online.indexer.util.StringUtil;
import org.cambridge.util.Misc;

/**
 * @author Karlson A. Mulingtapang, ahcollado
 * 
 *  EbookDetailsBean.java - Ebook Details Beans
 */
public class EbookDetailsBean {
	private final static String ID = "bookId";
	private final static String MAIN_TITLE = "MAIN_TITLE";
	private final static String ALPHASORT = "ALPHASORT";
	private final static String SUB_TITLE = "SUB_TITLE";
	private final static String EDITION = "EDITION";
	private final static String NUMBER = "NUMBER";
	private final static String AUTHOR = "AUTHOR";
	private final static String EDITOR = "EDITOR";
	private final static String AUTHOR_POSITION = "AUTHOR_POSITION";
	private final static String EDITOR_POSITION = "EDITOR_POSITION";
	private final static String AFFILIATION = "AFFILIATION";
	private final static String DOI = "DOI";
	private final static String ISBN = "ISBN";
	private final static String ALT_ISBN = "ALT_ISBN";
	private final static String TYPE = "TYPE";
	private final static String VOLUME = "VOLUME";
	private final static String OTHER_VOLUME = "OTHER_VOLUME";
	private final static String SERIES = "SERIES";
	private final static String SERIES_POSITION = "POSITION";
	private final static String PUBLISHER = "PUBLISHER";
	private final static String PUBLISHER_LOC = "PUBLISHER_LOC";
	private final static String PRINT_DATE = "PRINT_DATE";
	private final static String ONLINE_DATE = "ONLINE_DATE";
	private final static String COPYRIGHT_STATEMENT = "COPYRIGHT_STATEMENT";
	private final static String SUBJECT = "SUBJECT";
	private final static String BLURB = "BLURB";
	private final static String PAGES = "PAGES";
	private final static String COVER_IMAGE = "FILENAME";
	private final static String CODE = "CODE";
	private final static String BOOK_GROUP = "GROUP";
	private final static String ROLE = "ROLE";
	private final static String TRANS_TITLE = "TRANS_TITLE";
	private final static String PART = "PART";
	private final static String LEVEL = "LEVEL";
	private final static String ELEMENT = "ELEMENT";
	private final static String POSITION = "POSITION";
	private final static String PART_TITLE = "PART_TITLE";
	private final static String PART_NUMBER = "PART_NUMBER";
	private final static String VOLUME_TITLE = "VOLUME_TITLE";
	private final static String VOLUME_NUMBER = "VOLUME_NUMBER";
	
	private final static String ELEMENT_OTHER_EDITION = "other-edition";
	private final static String ELEMENT_PRINT_DATE = "print-date";
	private final static String ELEMENT_ONLINE_DATE = "online-date";

	private final static String INDEX_DIRECTORY = System.getProperty("index.dir").trim();
	public final static String IMAGE_DIR_PATH = System.getProperty("content.url").trim();;
	public final static String CONTENT_DIR = System.getProperty("content.dir").trim();
	public final static String MISSING_COVER_IMAGE = System.getProperty("missing.cover.standard").trim();
	public final static String MISSING_COVER_THUMBNAIL = System.getProperty("missing.cover.thumbnail").trim();
	public final static String NO_COVER_IMAGE = System.getProperty("no.cover.standard").trim();
	public final static String NO_COVER_THUMBNAIL = System.getProperty("no.cover.thumbnail").trim();
	public final static String CORRUPT_COVER_IMAGE = System.getProperty("corrupt.cover.standard").trim();
	public final static String CORRUPT_COVER_THUMBNAIL = System.getProperty("corrupt.cover.thumbnail").trim();
	
//  local test only
//	private final static String INDEX_DIRECTORY = "C:/CBO/bookindex/".trim();
//	public final static String IMAGE_DIR_PATH = "C:/DRU/dump/test.txt".trim();
//	public final static String CONTENT_DIR = "C:/jboss-4.2.3.GA/server/default/deploy/ebooks.war/content/".trim();
//	public final static String MISSING_COVER_IMAGE = "C:/DRU/dump/test.txt".trim();
//	public final static String MISSING_COVER_THUMBNAIL = "C:/DRU/dump/test.txt".trim();
//	public final static String NO_COVER_IMAGE = "C:/DRU/dump/test.txt".trim();
//	public final static String NO_COVER_THUMBNAIL = "C:/DRU/dump/test.txt".trim();
//	public final static String CORRUPT_COVER_IMAGE = "C:/DRU/dump/test.txt".trim();
//	public final static String CORRUPT_COVER_THUMBNAIL = "C:/DRU/dump/test.txt".trim();
	
	public final static String NO_IMAGE_IN_XML = "NO_IMAGE_IN_XML";
	public final static String IMAGE_CORRUPTED = "Corrupted image.";
	
	public final static String HTML_BREAK = "<br />";
	
	//private final static String COMMA = "COMMA";	
	public final static String POPUP = "POPUP";
	public final static String YES = "Y";
	public final static String BOOK_ID = "BOOK_ID";
	public final static String SHOW = "SHOW";
	public final static String META = "Metadata";
	public final static String KEYWORD = "Keywords";

	private String bookId;
	private String mainTitle;
	private String subTitle;
	private String edition;
	private String editionNumber;
	private String seriesNumber;
	private String seriesPosition;
	private String author;
	private String editor;
	private String editorAlphasort;
	private String affiliation;
	private String doi;
	private String eisbn;
	private String paperback;
	private String hardback;
	private String other;
	private String volume;
	private String otherVolume;
	private String series;
	private String publisherName;
	private String publisherLoc;
	private String printDate;
	private String onlineDate;
	private String copyrightStatement;
	private String subject;
	private String blurb;
	private String pages;
	private String standardImage;
	private String thumbImage;
	private String bookGroup;
	private String mainTitleAlphaSort;
	private String seriesAlphasort;
	private String seriesPart;
	private String seriesCode;	
	private String transTitle;
	private String subjectLevel;	
	private String partTitle;
	private String partNumber;
	private String volumeTitle;
	private String volumeNumber;
	

	private TreeMap<String, String> authorMap;
	private TreeMap<String, String> editorMap;

	private ArrayList<RoleAffBean> authorAffList;
	private ArrayList<RoleAffBean> editorAffList;
	private ArrayList<SubjectBean> subjectList;
	private ArrayList<OtherEditionBean> otherEdList;
	private ArrayList<PublisherNameBean> pubNameList;	
	private ArrayList<PublisherLocBean> pubLocList;
	
	private static final String[] DOCUMENT_FIELDS_SEARCH = new String[] {
			MAIN_TITLE, SUB_TITLE, EDITION, AUTHOR, DOI, ISBN, ALT_ISBN,
			VOLUME, OTHER_VOLUME, SERIES, PUBLISHER, PUBLISHER_LOC, PRINT_DATE,
			ONLINE_DATE, COPYRIGHT_STATEMENT, SUBJECT, BLURB, BOOK_GROUP,
			PAGES, COVER_IMAGE, TRANS_TITLE, PART_TITLE, PART_NUMBER, VOLUME_TITLE, VOLUME_NUMBER };

	/**
	 * will be used for indentifying the common field props (dynamically assigning bean prop)
	 */
	private static final HashMap<String, String[]> FIELD_PROP_MAP = new HashMap<String, String[]>();

	static {
		FIELD_PROP_MAP.put(SUB_TITLE, new String[] { "subTitle",
				"displaySubtitle" });
		FIELD_PROP_MAP.put(DOI, new String[] { "doi", "" });
		FIELD_PROP_MAP.put(ISBN, new String[] { "eisbn", "" });
		FIELD_PROP_MAP.put(VOLUME, new String[] { "volume", "displayVolume" });
		FIELD_PROP_MAP.put(OTHER_VOLUME, new String[] { "otherVolume",
				"displayOtherVolume" });
		//FIELD_PROP_MAP.put(PUBLISHER, new String[] { "publisherName", "" });
		//FIELD_PROP_MAP.put(PUBLISHER_LOC, new String[] { "publisherLoc", "" });
		//FIELD_PROP_MAP.put(PRINT_DATE, new String[] { "printDate", "" });
		//FIELD_PROP_MAP.put(ONLINE_DATE, new String[] { "onlineDate", "" });
		FIELD_PROP_MAP.put(BLURB, new String[] { "blurb", "" });
		FIELD_PROP_MAP.put(BOOK_GROUP, new String[] { "bookGroup", "" });
		FIELD_PROP_MAP.put(PAGES, new String[] { "pages", "" });
		FIELD_PROP_MAP.put(TRANS_TITLE, new String[] { "transTitle",
				"displayTransTitle" });
	}

	private static final String FALSE = "false";

	public EbookDetailsBean() {
	}

	public EbookDetailsBean(String bookId) {
		this.bookId = bookId.toUpperCase();
		show();
	}
	
	public EbookDetailsBean(IndexSearcher searcher, String bookId) {
		this.bookId = bookId.toUpperCase();
		show(searcher);
	}
		
	/**
	 * Used to initialize the init popup
	 */
	
	@SuppressWarnings("unused")
	private HttpServletRequest initPopup;
	public void setInitPopup(HttpServletRequest request){		
		if(YES.equals(request.getParameter(POPUP))){			
			this.setBookId(request.getParameter(EbookDetailsBean.BOOK_ID));			
			this.show();			
		}		
	}
	
	public void show() {
		try {
			populateBean(IndexSearchUtil.searchIndex(INDEX_DIRECTORY,
					getQueryString()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void show(IndexSearcher searcher) {
		try {
			populateBean(IndexSearchUtil.searchIndex(searcher, getQueryString()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public void showListener(ActionEvent event) {
		setBookId(""); // clear
		setBookId((String) event.getComponent().getAttributes().get(ID));
	}

	private String getQueryString() {
		StringBuffer queryString = new StringBuffer("BOOK_ID:" + getBookId());
		queryString
				.append(" AND (PARENT:metadata OR PARENT:subject-group OR PARENT:pub-dates OR ELEMENT:book)");

		return queryString.toString();
	}

	/**
	 * set common fields, dynamically assign bean getters and setters
	 * 
	 * @param key
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private void setCommonFields(String key, Document document)
			throws IllegalAccessException, InvocationTargetException {
		String[] propVal = FIELD_PROP_MAP.get(key);
		if (propVal != null) {
			setCommonFields(key, propVal[0], propVal[1], document);
		}
	}
	
	/**
	 * set common fields, dynamically assign bean getters and setters,
	 * performs additional checking. checks first is the element is empty
	 * @param key
	 * @param prop
	 * @param check
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private void setCommonFieldsWithCheck(String key, String prop, String check,
			Document document) throws IllegalAccessException, InvocationTargetException{
		if(!Misc.isEmpty(document.get(key))){
			setCommonFields(key, prop, check, document);
		}
	}
	
	/**
	 * set common fields, dynamically assign bean getters and setters
	 * @param key
	 * @param prop
	 * @param check
	 * @param document
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private void setCommonFields(String key, String prop, String check,
			Document document) throws IllegalAccessException, InvocationTargetException{
		try {
			if (BeanUtils.getProperty(this, check).equals(FALSE)) {
				BeanUtils.setProperty(this, check, true);
			}
		} catch (NoSuchMethodException e) {
			// do nothing
			// some items do not have getter setter to check if should display
		}
		BeanUtils.setProperty(this, prop, document.get(key));
		
	}

	private void populateBean(List<Document> docs) throws IOException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {

		//StringBuffer author = new StringBuffer();
		StringBuffer editor = new StringBuffer();
		StringBuilder editorAlphasort = new StringBuilder();
		StringBuffer affiliation = new StringBuffer();
		StringBuffer copyrightStatement = new StringBuffer();
		StringBuffer printDate = new StringBuffer();
		StringBuffer onlineDate = new StringBuffer();
		//StringBuffer subject = new StringBuffer();

		authorMap = new TreeMap<String, String>();
		editorMap = new TreeMap<String, String>();
		authorAffList = new ArrayList<RoleAffBean>();
		editorAffList = new ArrayList<RoleAffBean>();
		subjectList = new ArrayList<SubjectBean>();
		otherEdList = new ArrayList<OtherEditionBean>();
		pubNameList = new ArrayList<PublisherNameBean>();
		pubLocList = new ArrayList<PublisherLocBean>();
		

		for (Document document : docs) {
			
			
			//search for other editions
			if(ELEMENT_OTHER_EDITION.equals(document.get(ELEMENT))){
				OtherEditionBean oedBean = new OtherEditionBean();
				oedBean.setIsbn(document.get(NUMBER));
				oedBean.setNumber(document.get(NUMBER));
				otherEdList.add(oedBean);
			}	
			//search for online date
			if(ELEMENT_PRINT_DATE.equals(document.get(ELEMENT))){
				printDate.append(document.get(PRINT_DATE));
				printDate.append(HTML_BREAK);
			}					
			if(ELEMENT_ONLINE_DATE.equals(document.get(ELEMENT))){
				onlineDate.append(document.get(ONLINE_DATE));
				onlineDate.append(HTML_BREAK);
			}
			for (String field : DOCUMENT_FIELDS_SEARCH) {
				if (!Misc.isEmpty(document.get(field))) {
					if (MAIN_TITLE.equals(field)) {
						setMainTitle(document.get(MAIN_TITLE));
						setMainTitleAlphaSort(document.get(ALPHASORT));
					} else if (ISBN.equals(field)) {
						setEisbn(document.get(ISBN));
						
					} else if (EDITION.equals(field)) {
						if(!Misc.isEmpty(getEdition())) {
							continue;
						}
//						if(!Misc.isEmpty(document.get(EDITION)) && !Misc.isEmpty(document.get(NUMBER))) {
//							setEdition(document.get(EDITION) + "&nbsp;- " + document.get(NUMBER));
//						} else if(!Misc.isEmpty(document.get(EDITION)) && Misc.isEmpty(document.get(NUMBER))) {
//							setEdition(document.get(EDITION));
//						} else if(Misc.isEmpty(document.get(EDITION)) && !Misc.isEmpty(document.get(NUMBER))) {
//							setEdition(document.get(NUMBER));
//						}
						setEdition(document.get(EDITION) + "&nbsp;- " + document.get(NUMBER));
//						setEdition(document.get(EDITION));
//						setEditionNumber(document.get(NUMBER));
					} else if (AUTHOR.equals(field)) {
						
						RoleAffBean authorBean = new RoleAffBean();
						authorBean.setAuthor(document.get(AUTHOR));
						authorBean.setFixAuthor(authorBean.getAuthor());
						
						if (!Misc.isEmpty(document.get(ROLE))) {
							authorBean.setRole(document.get(ROLE));
						}
							

						if (!Misc.isEmpty(document.get(AFFILIATION))) {
							authorBean.setDisplayAffliation(true);
							authorBean.setAffiliation(document.get(AFFILIATION));
						}
						
						if (!Misc.isEmpty(document.get(ALPHASORT))) {
							authorBean.setDisplayAuthorAlphasort(true);
							authorBean.setAuthorAlphaSort(document.get(ALPHASORT));
						}
						
						authorBean.setPosition(document.get(AUTHOR_POSITION));

						authorAffList.add(authorBean);
						authorMap.put(document.get(AUTHOR_POSITION), document.get(AUTHOR));
						
						
					} else if (EDITOR.equals(field)) {
						editor.append(document.get(EDITOR));
						editor.append(HTML_BREAK);
						editorAlphasort.append(document.get(ALPHASORT));
						editorAlphasort.append(" ");

						if (!Misc.isEmpty(document.get(AFFILIATION))) {
							affiliation.append(document.get(AFFILIATION));
							affiliation.append(HTML_BREAK);
						}

						editorMap.put(document.get(EDITOR_POSITION), document
								.get(EDITOR));
					} else if (ALT_ISBN.equals(field)) {
						if (!Misc.isEmpty(document.get(TYPE))) {
							if (document.get(TYPE).equals("paperback")) {
								setPaperback(document.get(ALT_ISBN));
							}

							if (document.get(TYPE).equals("hardback")) {
								setHardback(document.get(ALT_ISBN));
							}

							if (document.get(TYPE).equals("other")) {
								setOther(document.get(ALT_ISBN));
							}
						}
					} else if (SERIES.equals(field)) {
						//set series
						setSeries(document.get(SERIES));
						//setSeriesNumber
						setCommonFieldsWithCheck(NUMBER, "seriesNumber", "displaySeriesNumber", document);
						//setSeriesPosition
						setCommonFieldsWithCheck(SERIES_POSITION, "seriesPosition", "displaySeriesPosition", document);						
						//setSeriesAlphasort
						setCommonFieldsWithCheck(ALPHASORT, "seriesAlphasort", "displaySeriesAlphasort", document);
						//setSeriesPart
						setCommonFieldsWithCheck(PART, "seriesPart", "displaySeriesPart", document);
						//setSeriesCode
						setCommonFieldsWithCheck(CODE, "seriesCode", "displaySeriesCode", document);						
					} else if (COPYRIGHT_STATEMENT.equals(field)) {
						copyrightStatement.append(document
								.get(COPYRIGHT_STATEMENT));
						copyrightStatement.append(HTML_BREAK);
					} else if (SUBJECT.equals(field)) {
						//all three are required in the dtd
						SubjectBean subjectBean = new SubjectBean();
						//System.out.println(document.get(SUBJECT));
						subjectBean.setSubject(document.get(SUBJECT));
						subjectBean.setCode(document.get(CODE));
						subjectBean.setLevel(document.get(LEVEL));
						
						subjectList.add(subjectBean);						
						
					} else if (COVER_IMAGE.equals(field)) {
						if (document.get(COVER_IMAGE).indexOf(getEisbn() + "t") > -1) {
							setThumbImage(document.get(COVER_IMAGE));
						} else {
							setStandardImage(document.get(COVER_IMAGE));
						}
					} else if(PUBLISHER.equals(field)){
						PublisherNameBean pnBean = new PublisherNameBean();
						pnBean.setName(document.get(PUBLISHER));
						pnBean.setOrder(document.get(POSITION));
						pubNameList.add(pnBean);
						
					} else if(PUBLISHER_LOC.equals(field)){
						PublisherLocBean plBean = new PublisherLocBean();
						plBean.setLocation(document.get(PUBLISHER_LOC));
						plBean.setOrder(document.get(POSITION));
						pubLocList.add(plBean);
						
					} else if(BLURB.equals(field)){
						setBlurb(document.get(BLURB));
						
					} else if(DOI.equals(field)){
						setDoi(document.get(DOI));
						
					} else if(PART_TITLE.equals(field)){
						setPartTitle(document.get(PART_TITLE));
						
					} else if(PART_NUMBER.equals(field)){
						setPartNumber(document.get(PART_NUMBER));
						
					} else if(VOLUME_NUMBER.equals(field)){
						setVolumeNumber(document.get(VOLUME_NUMBER));
						
					} else if(VOLUME_TITLE.equals(field)){
						setVolumeTitle(document.get(VOLUME_TITLE));
						
					} else {
						// set common fields
						setCommonFields(field, document);
					}
				}
			}
		}
		
		//check if no xml date for thumbnail or standard image was found, isDisplay.. would be false 
		//the following would then be executed
		
		//sort collections
		Collections.sort(authorAffList);
		Collections.sort(subjectList);
		Collections.sort(otherEdList);
		Collections.sort(pubNameList);
		Collections.sort(pubLocList);
		
		//setAuthor(author.toString());
		setOnlineDate(onlineDate.toString());
		setPrintDate(printDate.toString());
		setEditor(editor.toString());
		setEditorAlphasort(editorAlphasort.toString());
		setAffiliation(affiliation.toString());
		setCopyrightStatement(copyrightStatement.toString());
		//setSubject(subject.toString());
	}	
	
	@SuppressWarnings("unused")
	private void resetFields() {
		setMainTitle("");
		setSubTitle("");
		setEdition("");
		setAuthor("");
		setEditor("");
		setAffiliation("");
		setDoi("");
		setPaperback("");
		setHardback("");
		setEisbn("");
		setOther("");
		setVolume("");
		setOtherVolume("");
		setSeries("");
		setPublisherName("");
		setPublisherLoc("");
		setPrintDate("");
		setOnlineDate("");
		setCopyrightStatement("");
		setSubject("");
		setBlurb("");
		setPages("");
//		setStandardImage("");
//		setThumbImage("");
		setBookGroup("");
		setSeriesNumber("");
		setSeriesPosition("");
		setEditionNumber("");
	}

	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId.toUpperCase();
	}

	/**
	 * @param bookId
	 *            the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the mainTitle
	 */
	public String getMainTitle() {
		return StringUtil.correctHtmlTags(mainTitle);
	}

	/**
	 * @param mainTitle
	 *            the mainTitle to set
	 */
	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	/**
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return StringUtil.correctHtmlTags(subTitle);
	}

	/**
	 * @param subTitle
	 *            the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * @return the edition
	 */
	public String getEdition() {
		return StringUtil.correctHtmlTags(edition);
	}

	/**
	 * @param edition
	 *            the edition to set
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return StringUtil.correctHtmlTags(author);
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the editor
	 */
	public String getEditor() {
		return StringUtil.correctHtmlTags(editor);
	}

	/**
	 * @param editor
	 *            the editor to set
	 */
	public void setEditor(String editor) {
		this.editor = editor;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi
	 *            the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the volume
	 */
	public String getVolume() {
		return volume;
	}

	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}

	/**
	 * @return the otherVolume
	 */
	public String getOtherVolume() {
		return otherVolume;
	}

	/**
	 * @param otherVolume
	 *            the otherVolume to set
	 */
	public void setOtherVolume(String otherVolume) {
		this.otherVolume = otherVolume;
	}

	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}

	/**
	 * @param series
	 *            the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}

	/**
	 * @return the publisherName
	 */
	public String getPublisherName() {
		return StringUtil.correctHtmlTags(publisherName);
	}

	/**
	 * @param publisherName
	 *            the publisherName to set
	 */
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	/**
	 * @return the publisherLoc
	 */
	public String getPublisherLoc() {
		return publisherLoc;
	}

	/**
	 * @param publisherLoc
	 *            the publisherLoc to set
	 */
	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}

	/**
	 * @return the printDate
	 */
	public String getPrintDate() {
		return printDate;
	}

	/**
	 * @param printDate
	 *            the printDate to set
	 */
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	/**
	 * @return the copyrightStatement
	 */
	public String getCopyrightStatement() {
		return copyrightStatement;
	}

	/**
	 * @param copyrightStatement
	 *            the copyrightStatement to set
	 */
	public void setCopyrightStatement(String copyrightStatement) {
		this.copyrightStatement = copyrightStatement;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return StringUtil.correctHtmlTags(subject);
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the blurb
	 */
	public String getBlurb() {
		return StringUtil.correctHtmlTags(blurb);
	}

	/**
	 * @param blurb
	 *            the blurb to set
	 */
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}

	/**
	 * @return the bookGroup
	 */
	public String getBookGroup() {
		return bookGroup;
	}

	/**
	 * @param bookGroup
	 *            the bookGroup to set
	 */
	public void setBookGroup(String bookGroup) {
		if(bookGroup != null){
			if(bookGroup.length() > 2 ){
				this.bookGroup = bookGroup.substring(0, 1).toUpperCase()
					+ bookGroup.substring(1);
			}else if(bookGroup.length() == 1){
				this.bookGroup = bookGroup.toUpperCase();
			}else{
				this.bookGroup = bookGroup;
			}
		}else{
			this.bookGroup = bookGroup;
		}
	}

	/**
	 * @return the pages
	 */
	public String getPages() {
		return pages;
	}

	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setPages(String pages) {
		this.pages = pages;
	}

	/**
	 * @return the onlineDate
	 */
	public String getOnlineDate() {
		return onlineDate;
	}

	/**
	 * @param onlineDate
	 *            the onlineDate to set
	 */
	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	/**
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return StringUtil.correctHtmlTags(affiliation);
	}

	/**
	 * @param affiliation
	 *            the affiliation to set
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * @return the paperback
	 */
	public String getPaperback() {
		return paperback;
	}

	/**
	 * @param paperback
	 *            the paperback to set
	 */
	public void setPaperback(String paperback) {
		this.paperback = paperback;
	}

	/**
	 * @return the hardback
	 */
	public String getHardback() {
		return hardback;
	}

	/**
	 * @param hardback
	 *            the hardback to set
	 */
	public void setHardback(String hardback) {
		this.hardback = hardback;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other
	 *            the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}
 	
	/**
	 * @return the eisbn
	 */
	public String getEisbn() {
		return eisbn;
	}

	/**
	 * @param eisbn
	 *            the eisbn to set
	 */
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	/**
	 * @return the standardImage
	 */
	public String getStandardImage() {
		return standardImage;
	}

	/**
	 * this method also checks whether the said image exists in the server.
	 * if not, would display the appropriate image
	 * Uses ImageUtil class to set the proper path
	 * @param standardImage
	 *            the standardImage to set
	 */
	public void setStandardImage(String standardImage) {
		if(standardImage != NO_IMAGE_IN_XML){
			this.standardImage = ImageUtil.setImagePath(standardImage, MISSING_COVER_IMAGE,
					CONTENT_DIR, getEisbn(), IMAGE_DIR_PATH, this.getClass());
		}else{
			this.standardImage = NO_COVER_IMAGE;
		}
	}

	/**
	 * @return the thumbImage
	 */
	public String getThumbImage() {
		return thumbImage;
	}

	/**
	 * this method also checks whether the said image exists in the server.
	 * if not, would display the appropriate image
	 * Uses ImageUtil class to set the proper path
	 * @param thumbImage
	 *            the thumbImage to set
	 */
	public void setThumbImage(String thumbImage) {
		if(thumbImage != NO_IMAGE_IN_XML){
			this.thumbImage = ImageUtil.setImagePath(thumbImage, MISSING_COVER_THUMBNAIL,
					CONTENT_DIR, getEisbn(), IMAGE_DIR_PATH, this.getClass());
		}else{
			this.thumbImage = NO_COVER_THUMBNAIL;
		}
		
	}

	public TreeMap<String, String> getAuthorMap() {
		return authorMap;
	}

	public TreeMap<String, String> getEditorMap() {
		return editorMap;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getSeriesPosition() {
		return seriesPosition;
	}

	public void setSeriesPosition(String seriesPosition) {
		this.seriesPosition = seriesPosition;
	}


	/**
	 * @return the mainTitleAlphaSort
	 */
	public String getMainTitleAlphaSort() {
		return StringUtil.correctHtmlTags(mainTitleAlphaSort);
	}

	/**
	 * @param mainTitleAlphaSort
	 *            the mainTitleAlphaSort to set
	 */
	public void setMainTitleAlphaSort(String mainTitleAlphaSort) {
		this.mainTitleAlphaSort = mainTitleAlphaSort;
	}

	/**
	 * @return AuthorAffList
	 */
	public ArrayList<RoleAffBean> getAuthorAffList() {
		return authorAffList;
	}

	public void setAuthorAffList(ArrayList<RoleAffBean> authorAffList) {
		this.authorAffList = authorAffList;
	}

	public ArrayList<RoleAffBean> getEditorAffList() {
		return editorAffList;
	}

	public void setEditorAffList(ArrayList<RoleAffBean> editorAffList) {
		this.editorAffList = editorAffList;
	}

	public String getSeriesAlphasort() {
		return StringUtil.correctHtmlTags(seriesAlphasort);
	}

	public void setSeriesAlphasort(String seriesAlphasort) {
		this.seriesAlphasort = seriesAlphasort;
	}

	public String getSeriesPart() {
		return StringUtil.correctHtmlTags(seriesPart);
	}

	public void setSeriesPart(String seriesPart) {
		this.seriesPart = seriesPart;
	}

	public String getTransTitle() {
		return StringUtil.correctHtmlTags(transTitle);
	}

	public void setTransTitle(String transTitle) {
		this.transTitle = transTitle;
	}
	
	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	
	public String getSubjectLevel() {
		return subjectLevel;
	}

	public void setSubjectLevel(String subjectLevel) {
		this.subjectLevel = subjectLevel;
	}

	public ArrayList<SubjectBean> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(ArrayList<SubjectBean> subjectList) {
		this.subjectList = subjectList;
	}
	
	public ArrayList<OtherEditionBean> getOtherEdList() {
		return otherEdList;
	}

	public void setOtherEdList(ArrayList<OtherEditionBean> otherEdList) {
		this.otherEdList = otherEdList;
	}
	
	public ArrayList<PublisherNameBean> getPubNameList() {
		return pubNameList;
	}

	public void setPubNameList(ArrayList<PublisherNameBean> pubNameList) {
		this.pubNameList = pubNameList;
	}

	public ArrayList<PublisherLocBean> getPubLocList() {
		return pubLocList;
	}

	public void setPubLocList(ArrayList<PublisherLocBean> pubLocList) {
		this.pubLocList = pubLocList;
	}

	/**
	 * @return the editorAlphasort
	 */
	public String getEditorAlphasort() {
		return editorAlphasort;
	}

	/**
	 * @param editorAlphasort the editorAlphasort to set
	 */
	public void setEditorAlphasort(String editorAlphasort) {
		this.editorAlphasort = editorAlphasort;
	}

	/**
	 * @return the partTitle
	 */
	public String getPartTitle() {
		return partTitle;
	}

	/**
	 * @param partTitle the partTitle to set
	 */
	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}

	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return the volumeTitle
	 */
	public String getVolumeTitle() {
		return volumeTitle;
	}

	/**
	 * @param volumeTitle the volumeTitle to set
	 */
	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	/**
	 * @return the volumeNumber
	 */
	public String getVolumeNumber() {
		return volumeNumber;
	}

	/**
	 * @param volumeNumber the volumeNumber to set
	 */
	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}	
}
