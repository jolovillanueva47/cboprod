package org.cambridge.ebooks.online.indexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.cambridge.ebooks.online.indexer.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.online.indexer.document.EbookDetailsBean;
import org.cambridge.ebooks.online.indexer.document.PublisherNameBean;
import org.cambridge.ebooks.online.indexer.document.RoleAffBean;
import org.cambridge.ebooks.online.indexer.document.SubjectBean;
import org.cambridge.ebooks.online.indexer.online_flag.OnlineFlagList;
import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;
import org.cambridge.ebooks.online.indexer.util.IndexSearchUtil;
import org.cambridge.ebooks.online.indexer.util.IndexerUtil;
import org.cambridge.ebooks.online.indexer.util.StringUtil;
import org.cambridge.util.Misc;

public class AdvanceSearchIndexer {

	private final static String CONTRIBUTOR = "CONTRIBUTOR";
	private final static String CONTRIBUTOR_COLLAB = "CONTRIBUTOR_COLLAB";
	private final static String DELIMITER = "=DEL=";
	
	private AdvanceSearchIndexer(){	}
	
	public static void indexDocs(EbookDetailsBean ebook, List<EbookChapterDetailsBean> chaptersList, 
			HashMap<String, String> contents, HashMap<String,String> eContent, IndexWriter searchWriter, OnlineFlagList oflist) throws CorruptIndexException, IOException{
		//System.out.println("--- ADVANCE SEARCH INDEX START ---");
		if(chaptersList != null && chaptersList.size() > 0)	{			
			Document doc;
			
			// Search
			Field authorEditorField = new Field("AUTHOR_EDITOR", "", Field.Store.NO, Field.Index.ANALYZED);
			Field contributorField = new Field("CONTRIBUTOR", "", Field.Store.NO, Field.Index.ANALYZED);
			Field authorAffField = new Field("AUTHOR_AFFILIATION", "", Field.Store.NO, Field.Index.ANALYZED);
			Field titleVolumeField = new Field("TITLE_VOLUME", "", Field.Store.NO, Field.Index.ANALYZED);
			Field subjectField = new Field("SUBJECT", "", Field.Store.NO, Field.Index.ANALYZED);
			Field publicationDateField = new Field("PUBLICATION_DATE", "", Field.Store.NO, Field.Index.ANALYZED);
			Field publicationDateNumField = new Field("PUBLICATION_DATE_NUM", "", Field.Store.NO, Field.Index.ANALYZED);
			Field isbnField = new Field("ISBN", "", Field.Store.NO, Field.Index.ANALYZED);			
			Field seriesNameField = new Field("SERIES_NAME", "", Field.Store.NO, Field.Index.ANALYZED);
			Field contentTitleSearchField = new Field("CONTENT_TITLE_SEARCH", "",Field.Store.NO, Field.Index.ANALYZED);
			Field doiField = new Field("DOI", "", Field.Store.NO, Field.Index.ANALYZED);
			Field keywordsField = new Field("KEYWORDS", "", Field.Store.NO, Field.Index.ANALYZED);
			Field contentField = new Field("CONTENT", "", Field.Store.NO, Field.Index.ANALYZED);
			
			// Display
			Field liveDateField = new Field("LIVE_DATE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field liveDateYearField = new Field("LIVE_DATE_YEAR", "", Field.Store.YES, Field.Index.ANALYZED);
			Field seriesNumberField = new Field("SERIES_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field seriesNameDisplayField = new Field("SERIES_NAME_DISPLAY", "", Field.Store.YES, Field.Index.NOT_ANALYZED);			
			Field bookIdField = new Field("BOOK_ID", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field eisbnField = new Field("EISBN", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field pisbnField = new Field("PISBN", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookTitleField = new Field("BOOK_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookTitleAlphaField = new Field("BOOK_TITLE_ALPHASORT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookSubTitleField = new Field("BOOK_SUB_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field seriesCodeField = new Field("SERIES_CODE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field onlineDateField = new Field("ONLINE_PUBLICATION_DATE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field printDateField = new Field("PRINT_DATE", "", Field.Store.YES, Field.Index.ANALYZED);
			Field copyrightField = new Field("COPYRIGHT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookAuthorsField = new Field("BOOK_AUTHORS", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookAuthorsAlphaField = new Field("BOOK_AUTHORS_ALPHASORT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field bookAuthorAffDispField = new Field("BOOK_AUTHOR_AFFILIATION_DISPLAY", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field forewordField = new Field("FOREWORD", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentIdField = new Field("CONTENT_ID", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentTitleField = new Field("CONTENT_TITLE", "",Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentSubTitleField = new Field("CONTENT_SUB_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field pdfField = new Field("PDF", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentDoiField = new Field("CONTENT_DOI", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field pageField = new Field("PAGE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentCtrbField = new Field("CONTENT_CONTRIBUTORS", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentCtrbDispField = new Field("CONTENT_CONTRIBUTORS_DISPLAY", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contributorCollabField = new Field("CONTENT_CONTRIBUTOR_COLLAB", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentExtrField = new Field("CONTENT_EXTRACT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);		
			Field contentImgExtrField = new Field("CONTENT_IMAGE_EXTRACT", "", Field.Store.YES, Field.Index.NOT_ANALYZED);			
			Field contentTypeField = new Field("CONTENT_TYPE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentDispField = new Field("CONTENT_DISPLAY", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentSubChapterField = new Field("CONTENT_SUB_CHAPTERS", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field publisherField = new Field("PUBLISHER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeField = new Field("VOLUME", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field otherVolumeField = new Field("OTHER_VOLUME", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field editionNumberField = new Field("EDITION_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field editionField = new Field("EDITION", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field contentLabelField = new Field("CONTENT_LABEL", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field partTitleField = new Field("PART_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field partNumberField = new Field("PART_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeTitleField = new Field("VOLUME_TITLE", "", Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field volumeNumberField = new Field("VOLUME_NUMBER", "", Field.Store.YES, Field.Index.NOT_ANALYZED);			
			
			for(EbookChapterDetailsBean chapter : chaptersList) {
				doc = new Document();
				
					doc.add(new Field(OnlineIndexer.FLAG, oflist.getOnlineFlagValue(ebook.getEisbn()) , Field.Store.YES, Field.Index.ANALYZED));
				
					// ebook - INDEXED/TOKENIZED
					// AUTHOR AND ROLE
					StringBuffer authorEditorContributor = new StringBuffer();
					if(!Misc.isEmpty(ebook.getAuthorAffList())) {
						StringBuilder authors = new StringBuilder();
						StringBuilder authorAlphasorts = new StringBuilder();
						StringBuilder authorAffStrBldr = new StringBuilder();
						
						List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
						for(RoleAffBean roleAff : authorAffList) {							
							authors.append(RoleAffBean.fixAuthorName(roleAff.getAuthor()));
							authorAlphasorts.append(roleAff.getAuthorAlphasort());
							authorAffStrBldr.append(roleAff.getAffiliation());							
							authors.append(" ");
							authorAlphasorts.append(" ");
							authorAffStrBldr.append(" ");
						}
						
						if(Misc.isNotEmpty(authors.toString())) {
							authorEditorContributor.append(StringUtil.stripHTMLTags(authors.toString()) + " ");
						}
						if(Misc.isNotEmpty(authorAlphasorts.toString())) {
							authorEditorContributor.append(authorAlphasorts.toString() + " ");
						}						
						
						// AUTHOR_AFFILIATION
						if(Misc.isNotEmpty(authorAffStrBldr.toString())) {
							authorAffField.setValue(authorAffStrBldr.toString());
							doc.add(authorAffField);			
						}
					}					

					// AUTHOR_EDITOR
					if(Misc.isNotEmpty(ebook.getEditor())) {
						authorEditorContributor.append(StringUtil.stripHTMLTags(ebook.getEditor()) + " ");
						authorEditorContributor.append(ebook.getEditorAlphasort() + " ");
					}			
					if(Misc.isNotEmpty(authorEditorContributor.toString())) {
						authorEditorField.setValue(authorEditorContributor.toString());
						doc.add(authorEditorField);				
					}
					
					// CONTRIBUTOR
//					if(Misc.isNotEmpty(chapter.getContributorNameModified())) {
//						contributorField.setValue(chapter.getContributorNameModified() + " "
//								+ chapter.getContributorNameAlphasort());
//						doc.add(contributorField);	
//					}		
					
					// SUBJECT
					if(!Misc.isEmpty(ebook.getSubjectList())) {
						List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
						subjectList = ebook.getSubjectList();
						StringBuffer subjectSb = new StringBuffer();
						for(SubjectBean subject : subjectList){
							subjectSb.append(subject.getSubject() + " ");
						}
						if(Misc.isNotEmpty(subjectSb.toString())) {
							subjectField.setValue(subjectSb.toString());
							doc.add(subjectField);
						}
					}
					
					// PUBLICATION DATE
					StringBuffer publicationDate = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getOnlineDate()))
						//publicationDate.append(ebook.getOnlineDate() + " ");
					if(Misc.isNotEmpty(ebook.getPrintDate()))
						publicationDate.append(ebook.getPrintDate() + " ");
					
					// LIVE_DATE
					String liveDate = OnlineIndexer.getEBookLiveDate(ebook.getBookId());
					String liveDateNum = OnlineIndexer.getEBookLiveDateNum(ebook.getBookId());
					if(Misc.isNotEmpty(liveDate)) {
						publicationDate.append(liveDate + " ");
						liveDateField.setValue(liveDate);
						liveDateYearField.setValue(liveDate.split(" ")[1]);
						doc.add(liveDateField);
						doc.add(liveDateYearField);
					}	
					if(Misc.isNotEmpty(liveDateNum)) {
						publicationDateNumField.setValue(liveDateNum);
						doc.add(publicationDateNumField);
					}
					
					//PUBLICATION_DATE
					if(Misc.isNotEmpty(publicationDate.toString())) {
						publicationDateField.setValue(publicationDate.toString());
						doc.add(publicationDateField);
					}
										
					// TITLE_VOLUME
					StringBuffer titleVolume = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getMainTitle())) {
						titleVolume.append(ebook.getMainTitle()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getSubTitle())) {
						titleVolume.append(ebook.getSubTitle()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) {
						titleVolume.append(ebook.getMainTitleAlphaSort()+ " ");
					}
					if(Misc.isNotEmpty(ebook.getVolume())) {
						titleVolume.append(ebook.getVolume() + " ");
					}
					if(Misc.isNotEmpty(ebook.getEdition())) {
						titleVolume.append(ebook.getEdition() + " ");
					}
					if(Misc.isNotEmpty(titleVolume.toString())) {
						titleVolumeField.setValue(StringUtil.stripUnicodes(
								StringUtil.stripHTMLTags(titleVolume.toString())));
						doc.add(titleVolumeField);
					}
					
					// ISBN
					StringBuffer isbn = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getEisbn()))
						isbn.append(ebook.getEisbn());
					if(Misc.isNotEmpty(ebook.getHardback()))
						isbn.append(" "+ebook.getHardback());
					if(Misc.isNotEmpty(ebook.getPaperback()))
						isbn.append(" "+ebook.getPaperback());
					if(Misc.isNotEmpty(isbn.toString())) { 		
						isbnField.setValue(isbn.toString());
						doc.add(isbnField);
					}
					
					// SERIES
					if(Misc.isNotEmpty(ebook.getSeries())) {
						String series = ebook.getSeries() + " ";
						if(Misc.isNotEmpty(ebook.getSeriesNumber())) {
							series = series + ebook.getSeriesNumber();
						}
						seriesNameField.setValue(series);
						doc.add(seriesNameField);
					}
					
					// SERIES_NUMBER
					if(Misc.isNotEmpty(ebook.getSeriesNumber())) {
						seriesNumberField.setValue(ebook.getSeriesNumber());
						doc.add(seriesNumberField);
					}
					
					// SERIES - INDEXED/STORED
					if(Misc.isNotEmpty(ebook.getSeries())) {
						seriesNameDisplayField.setValue(ebook.getSeries());
						doc.add(seriesNameDisplayField);
					}
					
					// DOI
					StringBuffer doi = new StringBuffer();
					if(Misc.isNotEmpty(ebook.getDoi())) {
						doi.append(ebook.getDoi());
					}
					if(Misc.isNotEmpty(chapter.getDoi())) {
						doi.append(" "+chapter.getDoi());	
					}
					if(Misc.isNotEmpty(doi.toString())) {	
						doiField.setValue(doi.toString());
						doc.add(doiField);
					}
					
					// KEYWORDS
					if(Misc.isNotEmpty(chapter.getKeyword())) {
						keywordsField.setValue(StringUtil.stripHTMLTags(
								StringUtil.stripUnicodes(chapter.getKeyword())));
						doc.add(keywordsField);
					}
					
					// ebook - STORED
					if(Misc.isNotEmpty(ebook.getBookId())) {						
						bookIdField.setValue(ebook.getBookId());
						doc.add(bookIdField);
					}
						
					// EISBN
					if(Misc.isNotEmpty(ebook.getEisbn())) { 
						eisbnField.setValue(ebook.getEisbn());
						doc.add(eisbnField);
					}
					
					// PISBN
					if(Misc.isNotEmpty(ebook.getHardback())) {
						pisbnField.setValue(ebook.getHardback());
						doc.add(pisbnField);	
					} else if(Misc.isNotEmpty(ebook.getPaperback())) {
						pisbnField.setValue(ebook.getPaperback());
						doc.add(pisbnField);	
					}
					
					if(Misc.isNotEmpty(ebook.getMainTitle())) { 	
						bookTitleField.setValue(ebook.getMainTitle());
						doc.add(bookTitleField);
					}
									
					if(Misc.isNotEmpty(ebook.getMainTitleAlphaSort())) { 	
						bookTitleAlphaField.setValue(ebook.getMainTitleAlphaSort());
						doc.add(bookTitleAlphaField);
					}
					if(Misc.isNotEmpty(ebook.getSubTitle())) { 	
						bookSubTitleField.setValue(ebook.getSubTitle());
						doc.add(bookSubTitleField);
					}
					if(Misc.isNotEmpty(ebook.getSeriesCode())) {
						seriesCodeField.setValue(ebook.getSeriesCode());
						doc.add(seriesCodeField);
					}
					if(Misc.isNotEmpty(ebook.getOnlineDate())) {
						onlineDateField.setValue(IndexerUtil.removeBr(ebook.getOnlineDate()));
						doc.add(onlineDateField);
					}
					if(Misc.isNotEmpty(ebook.getPrintDate())) {
						printDateField.setValue(IndexerUtil.removeBr(ebook.getPrintDate()));
						doc.add(printDateField);
					}					
					if(Misc.isNotEmpty(ebook.getCopyrightStatement())) {
						copyrightField.setValue(IndexerUtil.removeBr(ebook.getCopyrightStatement()));
						doc.add(copyrightField);
					}
					
					// VOLUME
					if(Misc.isNotEmpty(ebook.getVolume())) { 		
						volumeField.setValue(ebook.getVolume());
						doc.add(volumeField);
					} 				
					
					// OTHER_VOLUME
					if(Misc.isNotEmpty(ebook.getOtherVolume())) { 	
						otherVolumeField.setValue(ebook.getOtherVolume());
						doc.add(otherVolumeField);
					} 
					
					if(Misc.isNotEmpty(ebook.getPartTitle())) { 
						partTitleField.setValue(ebook.getPartTitle());
						doc.add(partTitleField);
					}
					if(Misc.isNotEmpty(ebook.getPartNumber())) { 	
						partNumberField.setValue(ebook.getPartNumber());
						doc.add(partNumberField);
					}  
					if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
						volumeTitleField.setValue(ebook.getVolumeTitle());
						doc.add(volumeTitleField);
					}  
					if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 	
						volumeNumberField.setValue(ebook.getVolumeNumber());
						doc.add(volumeNumberField);
					}  
					
					// EDITION_NUMBER
					if(Misc.isNotEmpty(ebook.getEditionNumber())) { 
						editionNumberField.setValue(ebook.getEditionNumber());
						doc.add(editionNumberField);
					}
					
					// EDITION
					if(Misc.isNotEmpty(ebook.getEdition())) {
						editionField.setValue(ebook.getEdition());
						doc.add(editionField);
					}
					
					// AUTHOR DISPLAY
					if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
						List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
						StringBuilder authorsDisplay = new StringBuilder();
						StringBuilder authorsSort = new StringBuilder();
						StringBuilder bookAuthorAffStrBldr = new StringBuilder();
						StringBuilder foreword = new StringBuilder();
						
						int ctr = 0;
						for(RoleAffBean roleAffBean : authorAffList) {
							ctr++;
							authorsDisplay.append(RoleAffBean.fixAuthorName(roleAffBean.getAuthor()));
							String authorAlphaSort = Misc.isNotEmpty(roleAffBean.getAuthorAlphasort()) ? 
									roleAffBean.getAuthorAlphasort() : roleAffBean.getAuthor();
							authorsSort.append(authorAlphaSort);
							bookAuthorAffStrBldr.append(roleAffBean.getAffiliation());							
							foreword.append(roleAffBean.getRole());
							
							if(ctr < authorAffList.size()) {
								authorsDisplay.append(DELIMITER);	
								bookAuthorAffStrBldr.append(DELIMITER);
								foreword.append(DELIMITER);
							}
							authorsSort.append(" ");
						}
						bookAuthorsField.setValue(authorsDisplay.toString());
						bookAuthorAffDispField.setValue(bookAuthorAffStrBldr.toString());
						bookAuthorsAlphaField.setValue(authorsSort.toString());
						if(Misc.isNotEmpty(foreword.toString())) {	
							forewordField.setValue(foreword.toString());
							doc.add(forewordField);
						}
						doc.add(bookAuthorsField);
						doc.add(bookAuthorAffDispField);
						doc.add(bookAuthorsAlphaField);
					}
					
					// chapter - STORED
					if(Misc.isNotEmpty(chapter.getContentId())) { 	
						contentIdField.setValue(chapter.getContentId());
						doc.add(contentIdField);
					}
					if(Misc.isNotEmpty(chapter.getHeadingTitle())) {	
						contentTitleField.setValue(chapter.getHeadingTitle());						
						doc.add(contentTitleField);
						
						contentTitleSearchField.setValue(chapter.getHeadingTitle());
						doc.add(contentTitleSearchField);
					}
					// CONTENT LABEL
					if(Misc.isNotEmpty(chapter.getHeadingLabel())) {
						contentLabelField.setValue(chapter.getHeadingLabel());
						doc.add(contentLabelField);
					}
					if(Misc.isNotEmpty(chapter.getHeadingSubtitle())) {	
						contentSubTitleField.setValue(chapter.getHeadingSubtitle());
						doc.add(contentSubTitleField);
					}
					if(Misc.isNotEmpty(chapter.getPdfFilename())) {	
						pdfField.setValue(chapter.getPdfFilename());
						doc.add(pdfField);
					}
					if(Misc.isNotEmpty(chapter.getDoi())) { 	
						contentDoiField.setValue(chapter.getDoi());
						doc.add(contentDoiField);
					} else {
						contentDoiField.setValue("null");
						doc.add(contentDoiField);
					}
					if(Misc.isNotEmpty(chapter.getPageStart()) 
							&& Misc.isNotEmpty(chapter.getPageEnd())) { 		
						pageField.setValue("pp " + chapter.getPageStart() + "-" + chapter.getPageEnd());
						doc.add(pageField);
					}
					
					indexContentContributor(contentCtrbField, contentCtrbDispField, 
							contributorField, contributorCollabField, chapter.getContentId(), doc);
					
//					if(Misc.isNotEmpty(chapter
//							.getContributorNameModified()))			
//						doc.add(new Field("CONTRIBUTOR", chapter.getContributorNameModified(),Field.Store.YES, Field.Index.NOT_ANALYZED));
					if(Misc.isNotEmpty(chapter.getAbstractContent())) {
						contentExtrField.setValue(chapter.getAbstractContent());
						doc.add(contentExtrField);
					}
					if(Misc.isNotEmpty(chapter.getAbstractImage())) {
						contentImgExtrField.setValue(chapter.getAbstractImage());
						doc.add(contentImgExtrField);
					}
					
					// chapter - INDEXED/TOKENIZED
					if(Misc.isNotEmpty(chapter.getContentId())
							&& Misc.isNotEmpty(contents.get(chapter.getContentId()))) {	
						
						contentField.setValue(StringUtil.stripUnicodes(StringUtil.stripHTMLTags(
								contents.get(chapter.getContentId())
								.replaceAll("(?i)(Cambridge Books Online &#169; Cambridge University Press, 2009)", ""))
						));
						
						//Remove Copyright so it wont be indexed
						doc.add(contentField);
					}
					
					if(Misc.isNotEmpty(chapter.getContentType())) {
						contentTypeField.setValue(chapter.getContentType());
						doc.add(contentTypeField);
					}
					
					if(Misc.isNotEmpty(chapter.getContentId()) 
							&& Misc.isNotEmpty(contents.get(chapter.getContentId()))){
						contentDispField.setValue(contents.get(chapter.getContentId())
								.replaceAll("(?i)(Cambridge Books Online &#169; Cambridge University Press, 2009)", ""));
						
						//Remove Copyright so it wont be indexed
						doc.add(contentDispField);
					}
					
					//System.out.println("test " + contents.get(chapter.getContentId()));
					//getIfContains Subchapter
					contentSubChapterField.setValue(hasSubchapters(eContent, chapter));
					doc.add(contentSubChapterField);
					
					// publisher 
					if(ebook.getPubNameList() != null && ebook.getPubNameList().size() > 0) {
						//List<PublisherNameBean> pnBean = ebook.getPublisherName<>();
						List<PublisherNameBean> publisherList = ebook.getPubNameList();
						StringBuffer publisherDisplay = new StringBuffer();						
						int ctr = 0;
						for(PublisherNameBean bean : publisherList) {
							ctr++;
							publisherDisplay.append(PublisherNameBean.fixPublisherName(bean.getName()));
							if(ctr < publisherList.size()) {
								publisherDisplay.append(", ");
							}							
						}
						publisherField.setValue(publisherDisplay.toString());
						doc.add(publisherField);
					}
					
					searchWriter.addDocument(doc);
				
			}
		}
		//System.out.println("--- ADVANCE SEARCH INDEX END ---");
	}
	
	/**
	 * checks if contains subchapter
	 * @param content
	 * @param chapter
	 * @return
	 */
	private static String hasSubchapters(HashMap<String,String> content, EbookChapterDetailsBean chapter) {		
		return content.get(chapter.getContentId());
	}
	
	private static void indexContentContributor(Field contentCtrbField, Field contentCtrbDispField, 
			Field cntrbField, Field contribCollabField, String contentId, Document document) {
		try {			
			List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(OnlineIndexerProperties.INDEX), 
					"ELEMENT: contributor AND PARENT_ID: " + contentId);	
			
			List<String> contributors = new ArrayList<String>();
			List<String> contribCollabs = new ArrayList<String>();
			ArrayList<String> allContribs = new ArrayList<String>();
			
			for(Document doc : docs){						
				String name = doc.get(CONTRIBUTOR);
				String collab = doc.get(CONTRIBUTOR_COLLAB); 
				if(Misc.isNotEmpty(name)) { 
					contributors.add(name);
				}
				if(Misc.isNotEmpty(collab)) {
					contribCollabs.add(collab);
				}
			}
			allContribs.addAll(contributors);
			allContribs.addAll(contribCollabs);
			allContribs.trimToSize();
			
			String contributorDisplay = StringUtil.getContributorDisplay(allContribs);
			String contributor = StringUtil.getContributors(allContribs);
			String contributorAlphasort = StringUtil.getContributorsAlphasort(contributors);
			String contributorCollab = StringUtil.getContributors(contribCollabs);
			if(Misc.isNotEmpty(contributor)) {
				cntrbField.setValue(contributor.trim() + " " + contributorAlphasort.trim());
				contentCtrbField.setValue(contributor.trim());				
				contentCtrbDispField.setValue(contributorDisplay.trim());
				contribCollabField.setValue(contributorCollab);
				document.add(cntrbField);
				document.add(contentCtrbField);
				document.add(contentCtrbDispField);
				document.add(contribCollabField);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
}