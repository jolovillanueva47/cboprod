package org.cambridge.ebooks.online.indexer.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;

public class JDBCUtil {
	
	public static Connection getConnection() {
		Connection connection = null; 
		try { 
			// Load the JDBC driver 
			//String driverName = System.getProperty("oracle.jdbc.driver.OracleDriver");//"oracle.jdbc.driver.OracleDriver"; 
			Class.forName("oracle.jdbc.driver.OracleDriver");			
		
			// Create a connection to the database			
			String serverName = System.getProperty(OnlineIndexerProperties.SERVERNAME);//"127.0.0.1"; 
			String portNumber = System.getProperty(OnlineIndexerProperties.PORTNUMBER);//"1521"; 
			String sid = System.getProperty(OnlineIndexerProperties.DATABASENAME);//"mydatabase"; 
			String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid; 
			String username = System.getProperty(OnlineIndexerProperties.USER);//"username"; 
			String password = System.getProperty(OnlineIndexerProperties.PASSWORD);//"password"; 
			connection = DriverManager.getConnection(url, username, password); 
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			e.printStackTrace();
		} catch (SQLException e) { 
			// Could not connect to the database
			e.printStackTrace();
		}		
		return connection;
	}

	
	public static Connection getConnection2() {
		Connection connection = null; 
		try { 
			// Load the JDBC driver 
			String driverName = "oracle.jdbc.driver.OracleDriver"; 
			Class.forName(driverName);
			
			// Create a connection to the database			
			String serverName = "ora-east-1.cup.cam.ac.uk"; 
			String portNumber = "1521"; 
			String sid = "TIGER1T"; 
			String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid; 
			String username = "oraebooks" ;
			String password = "oraebooks"; 
			connection = DriverManager.getConnection(url, username, password); 
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			e.printStackTrace();
		} catch (SQLException e) { 
			// Could not connect to the database
			e.printStackTrace();
		}		
		return connection;
	}
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.err.println(e);
		}
	}	
}
