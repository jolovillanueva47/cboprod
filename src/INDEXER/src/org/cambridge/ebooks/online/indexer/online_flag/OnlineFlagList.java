package org.cambridge.ebooks.online.indexer.online_flag;

import java.sql.SQLException;
import java.util.LinkedList;

public class OnlineFlagList extends LinkedList<OnlineFlagBean> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6942969490591848137L;
	
	//default is 10
	private int threshold = 10;
	
	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	@Override
	public boolean add(OnlineFlagBean element) {
		if(element != null){
			if(this.size() >= threshold){
				this.remove();
			}
			
			super.add(element);
		}		
		return true;
	}	
	
	private OnlineFlagBean get(String isbn) {
		OnlineFlagBean bean = null;		
		for(OnlineFlagBean ol : this){			
			if(ol.getIsbn().equals(isbn)){
				bean = ol;
				break;
			}
		}
		
		if(bean == null){		
			OnlineFlagBean ol = null;
			try {		
				ol = OnlineFlagWorker.getOnlineFlagInfo(isbn);
			} catch (Exception e) {				
				ol = new OnlineFlagBean(isbn, null); 
			}
			if(ol == null || ol.getIsbn() == null){				
				ol = new OnlineFlagBean(isbn, null);				
			}		
			bean = ol;
			
			this.add(bean);
		}
			
		
		return bean;
	}
	
	public String getOnlineFlagValue(String isbn){
		System.out.println("ISBN ====== " + isbn);
		OnlineFlagBean bean = get(isbn);
		//return bean.getOnlineFlag();		
		return bean.getFlag();
	}
	
	
}
