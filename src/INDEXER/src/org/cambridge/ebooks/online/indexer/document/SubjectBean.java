package org.cambridge.ebooks.online.indexer.document;

public class SubjectBean implements Comparable<SubjectBean>{
	private String subject;
	private String code;
	private String level;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	public int compareTo(SubjectBean o) {
		return this.level.compareTo(o.getLevel());		
	}
	
	
	
}
