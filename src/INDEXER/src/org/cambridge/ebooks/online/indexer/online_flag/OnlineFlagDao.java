package org.cambridge.ebooks.online.indexer.online_flag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OnlineFlagDao {
	public static OnlineFlagBean getOnlineFlagInfo(String isbn, Connection conn) throws SQLException{
		String sql =
				"select isbn, online_flag " +
				"from oraebooks.ebook_isbn_data_load " +
				"where isbn = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		try{
			ps.setString(1, isbn);
			OnlineFlagBean ob = null;
			ResultSet rs = ps.executeQuery();
			try{			
				while(rs.next()){
					String _isbn = rs.getString("isbn");
					String onlineFlag = rs.getString("online_flag");
					
					ob = new OnlineFlagBean(_isbn, onlineFlag);
				}
				
				return ob;
			}finally{
				rs.close();
			}
		}finally{
			ps.close();
		}		
	}
}
