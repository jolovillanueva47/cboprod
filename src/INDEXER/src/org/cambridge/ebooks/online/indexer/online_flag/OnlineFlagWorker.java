package org.cambridge.ebooks.online.indexer.online_flag;

import java.sql.Connection;
import java.sql.SQLException;

import org.cambridge.ebooks.online.indexer.jdbc.JDBCUtil;



public class OnlineFlagWorker {
	public static OnlineFlagBean getOnlineFlagInfo(String isbn) throws SQLException{
		Connection conn = JDBCUtil.getConnection();
		try{
			return OnlineFlagDao.getOnlineFlagInfo(isbn, conn);
		}finally{
			conn.close();
		}
	}
}
