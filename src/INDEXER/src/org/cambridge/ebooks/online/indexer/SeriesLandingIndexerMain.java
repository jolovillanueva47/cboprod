package org.cambridge.ebooks.online.indexer;

import org.cambridge.ebooks.online.indexer.properties.OnlineIndexerProperties;

public class SeriesLandingIndexerMain {
	public static void main(String[] args) {
		OnlineIndexer oi = new OnlineIndexer();
		System.out.println("[OnlineIndexer] main");		
		oi.reindexFromFile(System.getProperty(OnlineIndexerProperties.SERIES_LANDING_INDEX), "series");
	}
}
