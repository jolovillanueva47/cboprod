package org.cambridge.ebooks.online.indexer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.cambridge.ebooks.online.indexer.document.EbookDetailsBean;
import org.cambridge.ebooks.online.indexer.document.RoleAffBean;
import org.cambridge.ebooks.online.indexer.document.SubjectBean;
import org.cambridge.ebooks.online.indexer.online_flag.OnlineFlagList;
import org.cambridge.ebooks.online.indexer.util.EncodingUtil;
import org.cambridge.ebooks.online.indexer.util.IndexerUtil;
import org.cambridge.util.Misc;

public class SubjectLandingIndexer {

//	private String currentBookId;
//	
//	public SubjectLandingIndexer(EbookDetailsBean ebook, IndexWriter writer) throws CorruptIndexException, IOException{
//		indexSubjectLandingPerBook(ebook, writer);
//	}
	
	public static void indexSubjectLandingPerBook(EbookDetailsBean ebook, IndexWriter writer, OnlineFlagList oflist) throws CorruptIndexException, IOException{
		//System.out.println("--- SUBJECT INDEX START ---");
		Document doc;
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHMMss");
		String timestamp = sdf.format(today);
			
		doc = new Document();
			// ebook
			doc.add(new Field("TIMESTAMP", timestamp, Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			doc.add(new Field(OnlineIndexer.FLAG, oflist.getOnlineFlagValue(ebook.getEisbn()), Field.Store.YES, Field.Index.ANALYZED));
			
			// ebook
			if(Misc.isNotEmpty(ebook.getBookId())) 		
				doc.add(new Field("BOOK_ID", ebook.getBookId(), Field.Store.YES, Field.Index.ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
				doc.add(new Field("BOOK_TITLE", ebook.getMainTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSubTitle())) 	
				doc.add(new Field("BOOK_SUB_TITLE", ebook.getSubTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getEisbn())) 		
				doc.add(new Field("EISBN", ebook.getEisbn(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getBlurb())) 	
				doc.add(new Field("BOOK_BLURB", EncodingUtil.convertToUTF8(ebook.getBlurb()),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSeries())) {
				doc.add(new Field("SERIES_NAME", ebook.getSeries(),Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
						
			if(Misc.isNotEmpty(ebook.getSeriesNumber())) 		
				doc.add(new Field("SERIES_NUMBER", ebook.getSeriesNumber(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getSeriesAlphasort())) {
				doc.add(new Field("SERIES_NAME_ALPHASORT", ebook.getSeriesAlphasort().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
						
			if(Misc.isNotEmpty(ebook.getSeriesCode())) 		
				doc.add(new Field("SERIES_CODE", ebook.getSeriesCode().toLowerCase(), Field.Store.YES, Field.Index.ANALYZED));
						
			if(ebook.getAuthorAffList() != null && ebook.getAuthorAffList().size() > 0) {
				List<RoleAffBean> authorAffList = ebook.getAuthorAffList();
				StringBuffer authorsDisplay = new StringBuffer();
				StringBuilder authorAlphaSort = new StringBuilder();
				for (Iterator<RoleAffBean> it = authorAffList.iterator();it.hasNext();) {
					RoleAffBean bean = it.next();
					authorsDisplay.append(bean.getRole() + " - " + RoleAffBean.fixAuthorName(bean.getAuthor()));
					authorAlphaSort.append(Misc.isNotEmpty(bean.getAuthorAlphasort()) 
							? bean.getAuthorAlphasort() : bean.getAuthor());
					if (it.hasNext()) {
						authorsDisplay.append(", ");
						authorAlphaSort.append(" ");
					}
				}
				doc.add(new Field("BOOK_AUTHORS", authorsDisplay.toString(),Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("BOOK_AUTHORS_ALPHASORT", authorAlphaSort.toString().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			if(!Misc.isEmpty(ebook.getSubjectList())) {
				List<SubjectBean> subjectList = new ArrayList<SubjectBean>();
				subjectList = ebook.getSubjectList();
				
				int ctr=1;
				for(SubjectBean subject : subjectList){
					if(Misc.isNotEmpty(subject.getSubject())) 	
						doc.add(new Field("SUBJECT_NAME_"+ctr, subject.getSubject(),Field.Store.YES, Field.Index.NO));
					if(Misc.isNotEmpty(subject.getCode())) 		
						doc.add(new Field("SUBJECT_CODE_"+ctr, subject.getCode().toLowerCase(), Field.Store.YES, Field.Index.ANALYZED));
					
					ctr++;
				}
			}
			
			if(Misc.isNotEmpty(ebook.getMainTitle())) 	
				doc.add(new Field("BOOK_TITLE_ALPHASORT", ebook.getMainTitleAlphaSort().toLowerCase(),Field.Store.YES, Field.Index.NOT_ANALYZED));
						
			if(Misc.isNotEmpty(ebook.getVolume())) { 		
				doc.add(new Field("VOLUME", ebook.getVolume(), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("VOLUME_ALPHASORT", ebook.getVolume().toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getPartTitle())) { 		
				doc.add(new Field("PART_TITLE", ebook.getPartTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			if(Misc.isNotEmpty(ebook.getPartNumber())) { 		
				doc.add(new Field("PART_NUMBER", ebook.getPartNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeTitle())) { 		
				doc.add(new Field("VOLUME_TITLE", ebook.getVolumeTitle(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}  
			if(Misc.isNotEmpty(ebook.getVolumeNumber())) { 		
				doc.add(new Field("VOLUME_NUMBER", ebook.getVolumeNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			}
			
			// EDITION_NUMBER
			if(Misc.isNotEmpty(ebook.getEditionNumber())) { 	
				doc.add(new Field("EDITION_NUMBER", ebook.getEditionNumber(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			// EDITION
			if(Misc.isNotEmpty(ebook.getEdition())) {
				doc.add(new Field("EDITION", ebook.getEdition(), Field.Store.YES, Field.Index.ANALYZED));
			}
			
			if(Misc.isNotEmpty(ebook.getOnlineDate())) { 		
				doc.add(new Field("PUBLICATION_DATE", IndexerUtil.removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("PUBLICATION_DATE_ALPHASORT", IndexerUtil.removeBr(ebook.getOnlineDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getPrintDate())) {
				doc.add(new Field("PRINT_DATE", IndexerUtil.removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.ANALYZED));				
				doc.add(new Field("PRINT_DATE_ALPHASORT", IndexerUtil.removeBr(ebook.getPrintDate()),Field.Store.YES, Field.Index.NOT_ANALYZED));
			} 
			
			if(Misc.isNotEmpty(ebook.getHardback()) || Misc.isNotEmpty(ebook.getPaperback())) {
				String pisbn = Misc.isNotEmpty(ebook.getHardback()) ? ebook.getHardback() : ebook.getPaperback();
				doc.add(new Field("PRINT_ISBN", pisbn, Field.Store.YES, Field.Index.NOT_ANALYZED));	
			}
			
			if(Misc.isNotEmpty(ebook.getDoi())) {
				doc.add(new Field("DOI", ebook.getDoi(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			
			String liveDate = OnlineIndexer.getEBookLiveDate(ebook.getBookId());
			if(Misc.isNotEmpty(liveDate)) {
				doc.add(new Field("LIVE_DATE", liveDate, Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
						
			writer.addDocument(doc);
		
		//System.out.println("--- SUBJECT INDEX END ---");
	}
	
//	public String getCurrentBookId() {
//		return currentBookId;
//	}
//
//	public void setCurrentBookId(String currentBookId) {
//		this.currentBookId = currentBookId;
//	}
}
