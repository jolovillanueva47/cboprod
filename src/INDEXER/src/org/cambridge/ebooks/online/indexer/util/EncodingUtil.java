package org.cambridge.ebooks.online.indexer.util;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class EncodingUtil {

	
	public static synchronized String convertToUTF8(String toUTF8){
		StringBuilder utf8 = new StringBuilder();
		if(toUTF8 != null && toUTF8.length() > 0)
		{
			utf8 = new StringBuilder(toUTF8);
			try 
			{
				//if( !isUTF8(utf8.toString()) ) //removed because of UTF-8 checking issue on solaris
				//{
					boolean isDone = false;
					while(!isDone)
					{
						int index = 0;
						char[] c = utf8.toString().toCharArray();
						for(index=0; index<c.length; index++)
						{
							if((int)c[index] == 38 || (int)c[index] == 60 || (int)c[index] == 62 || (int)c[index] > 127) //0 -127 common, no need to check
							{
								String temp = new String(Character.toString(c[index]).getBytes("ISO-8859-1"), "UTF-8");
								if(temp != null)
								{
									if(temp.equals("?") || temp.equals("<") || temp.equals(">"))
									{
										utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
										break;
									}
									else if(temp.equals("&"))
									{
										if((c.length == 1) || (index != c.length - 1 && c[index + 1] != '#') || 
										   (index == c.length - 1)) 
										   
										{
											utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
											break;
										}
										
									}
								}
							}
						}
						
						if(index == utf8.toString().length()) isDone = true;
					}
				//}
				
			} 
			catch (Exception e) 
			{
				System.err.println("[Exception] convertToUTF8() " + e.getMessage());
			}
			
		}
		return utf8.toString();
	}
	
	public static boolean isUTF8(final String toCheckForEncoding) {
		 
		boolean result = false;
		 CharsetDecoder cd = null;
		 
		 if(toCheckForEncoding != null && toCheckForEncoding.length() > 0)
		 {
			byte[] b = toCheckForEncoding.getBytes();
			 cd = Charset.availableCharsets().get("UTF-8").newDecoder();
			
			try 
			{
				cd.decode(ByteBuffer.wrap(b));
				result = true;
			} 
			catch (CharacterCodingException e) 
			{
				result = false;
			}
		 }	 
		 return result;
		 
	}
}
