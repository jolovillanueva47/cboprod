package org.cambridge.ebooks.online.indexer.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.cambridge.ebooks.online.indexer.document.EbookChapterDetailsBean;
import org.cambridge.ebooks.online.indexer.ebook.EBookIndexSearch;

public class IndexerUtil {
	
	public static final String[] STRIP_CHARS = {"&#8211;", "&#8208;", ",", "\\."};
	
	public static final String CONTRIBUTOR = "CONTRIBUTOR";
	
	public static final String CONTRIBUTOR_COLLAB = "CONTRIBUTOR_COLLAB";
	
	public static final String AFFILIATION = "AFFILIATION";	
	
	public static final String CONTENT_ID = "CONTENT_ID";
	
	public static final String DOC_TYPE = "DOC_TYPE";
	
	public static final String PARENT_ID = "PARENT_ID";
	
	public static final String BOOK_CONTRIBUTOR = "book_contributor";
	
	public static final String BOOK_ID = "BOOK_ID";
	
	public static final String CONTENT_DETAILS = "content_details";
	
	public static final int MERGE_FACTOR = 15;
	
	public static String removeHTMLTags(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("<i>", "").
		replaceAll("</i>", "").
		replaceAll("<b>", "").
		replaceAll("</b>", "").
		replaceAll("<blockquote>", "").
		replaceAll("</blockquote>", "").
		replaceAll("<u>", "").
		replaceAll("</u>", "").
		replaceAll("<span style='font-variant:small-caps'>", "").
		replaceAll("</span>", "").
		replaceAll("<sup>", "").
		replaceAll("</sup>", "").
		replaceAll("<sub>", "").
		replaceAll("</sub>", "");
	}
	
	public static String removeComma(String input) {		
		return input.replaceAll(",", " ");
	}
	
	public static String removeBr(String input){
		return input.replaceAll("<br />", " ");
	}
	
	public static String stripSpecialCharacters(String source) {
		String result = source;
		for(String s : STRIP_CHARS) {
			result = result.replaceAll(s, " ");
		}
		
		return result;
	}
	
	/**
	 * get contents of chapters
	 * @param chapters
	 * @return
	 */
	public static HashMap<String, String> getContents(List<EbookChapterDetailsBean> chapters){
		HashMap<String, String> contentsMap = new HashMap<String, String>();
		String content;
		for(EbookChapterDetailsBean chapter : chapters){
			content = EBookIndexSearch.getContent(chapter.getContentId());

			//Remove Copyright so it wont be indexed
			content.replaceAll("Cambridge Books Online &#169; Cambridge University Press, 2009", "");
								
			contentsMap.put(chapter.getContentId(), content);
		}
		return contentsMap;
	}
	
	/**
	 * get contents of chapters
	 * @param chapters
	 * @return
	 */
	public static HashMap<String, String> getContents(IndexSearcher searcher, List<EbookChapterDetailsBean> chapters){
		HashMap<String, String> contentsMap = new HashMap<String, String>();
		String content;
		for(EbookChapterDetailsBean chapter : chapters){
			content = EBookIndexSearch.getContent(searcher, chapter.getContentId());

			//Remove Copyright so it wont be indexed
			content.replaceAll("Cambridge Books Online &#169; Cambridge University Press, 2009", "");
								
			contentsMap.put(chapter.getContentId(), content);
		}
		return contentsMap;
	}
	
	/**
	 * gets a new index writer
	 * @param indexDir
	 * @return
	 * @throws Exception
	 */	
	public static IndexWriter getIndexWriter(String indexDir) throws Exception{
		return getIndexWriter(indexDir, MERGE_FACTOR);
	}
	
	public static IndexWriter getIndexWriter(String indexDir, int mergeFactor) throws Exception{
		return getIndexWriter(indexDir, mergeFactor, new WhitespaceAnalyzer());
	}
	
	public static IndexWriter getIndexWriter(String indexDir, int mergeFactor, Analyzer analyzer) throws Exception{
		IndexWriter writer = new IndexWriter(FSDirectory.getDirectory(indexDir), 
				analyzer, 
				checkIfCreateIndex(indexDir), 
				IndexWriter.MaxFieldLength.LIMITED);

		writer.setMaxFieldLength(Integer.MAX_VALUE);
		writer.setMaxBufferedDocs(100);
		writer.setMergeFactor(mergeFactor);
		//writer.setUseCompoundFile(false);
		return writer;
	}
	
	/**
	 * checks if will create a new index or incremental indexing only
	 * returns true to create
	 * false to append
	 * @return
	 */
	public static boolean checkIfCreateIndex(String indexDir){
		File file = new File(indexDir);
		String list[] = file.list(); 
		boolean create = true; 

		if (list != null && list.length > 0) create = false;
		
		return create;
	}
	
	/**
	 * checks if contains subchapter
	 * @param contentMap
	 * @param chapter
	 * @return
	 */
	public static String hasSubchapters(HashMap<String, String> contentMap, EbookChapterDetailsBean chapter) {		
		return contentMap.get(chapter.getContentId());
	}
	
	public static String isTopLevel(HashMap<String,String> tlMap, EbookChapterDetailsBean chapter) { 
		return tlMap.get(chapter.getContentId());
	}
	
	/**
	 * checks if index is locked
	 * @param indexDir
	 * @throws IOException
	 */
	public static void checkIfIndexIsLocked(String indexDir) throws IOException{
		if(IndexWriter.isLocked(FSDirectory.getDirectory(System.getProperty(indexDir))))
		{
			System.out.println("LOCKED! [" + System.getProperty(indexDir) + "]");
			//add custom method here andreu
			//SYSTEM.EXIT
		}
	}
	
	public static String getFormatedDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	public static Date getLiveDate(String date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateFromDb = null;
		try {
			dateFromDb = dateFormat.parse(date);
			Date specifiedDate = dateFormat.parse("2009-11-16 00:00:00"); // 16 NOV 2009
			if(dateFromDb.before(specifiedDate)) { 
				return specifiedDate;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateFromDb;
	}
}
