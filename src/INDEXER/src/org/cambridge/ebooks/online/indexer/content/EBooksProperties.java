
package org.cambridge.ebooks.online.indexer.content;


public class EBooksProperties {
	
	public static final String INDEX = "index.dir";
	
	public static final String TEMPLATES_DIR = "templates.dir";
	
	public static final String EBOOKS_RESOURCE_URL = "ebooks.resource.url";
	
}
