/**
 * 
 */
package org.cambridge.ebooks.online.indexer.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.ebooks.online.indexer.util.IndexSearchUtil;

/**
 * @author rvillamor
 *
 */
public class SearchForIndented {
	
	@SuppressWarnings("unused")
	private static final String PART_TITLE = "part-title";
	
	private static final String YES = "Y";
	
	private static final String NO = "N";
	
	@SuppressWarnings("unused")
	private static final String TYPE = "TYPE";
	
	private static final String ID = "ID";
	
	@SuppressWarnings("unused")
	private static final String CHAPTER = "chapter";
	
	private static final String PARENT_ID = "PARENT_ID";
	
	private static final String PARENT = "PARENT";
	
	private static final String CONTENT_ITEMS = "content-items";
	
	/**
	 * sets indented to true if type = "part-title"
	 * @param alContents
	 */
	public static HashMap<String,String> setIndentedHashmap(String bookId) {
		HashMap<String, String> hmIndent = new HashMap<String, String>();
		List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(IndexerProperties.INDEX), 
				"ELEMENT: content-item AND BOOK_ID: " + bookId);		
		
		//collect all content items
		ArrayList<String> allContentItems = new ArrayList<String>();
		for(Document doc : docs ){
			allContentItems.add(doc.get(ID));
		}
		
		//get all that has subchapters
		ArrayList<String> allHasSubchapters = new ArrayList<String>();
		for(Document doc : docs ){
			String parent = doc.get(PARENT_ID);
			if(allContentItems.contains(parent)){
				allHasSubchapters.add(parent);
			}
		}
		
		//return hashmap of subchapters		
		for(Document doc : docs ){
			String id = doc.get(ID);
			if(allHasSubchapters.contains(id)){
				hmIndent.put(id, YES);
			}else{
				hmIndent.put(id, NO);
			}
		}
		
		//manual clean up 
		allHasSubchapters = null;
		allContentItems = null;
		
		return hmIndent;
	}
	
	/**
	 * sets indented to true if type = "part-title"
	 * @param alContents
	 */
	public static HashMap<String,String> setIndentedHashmap(IndexSearcher searcher, String bookId) {
		HashMap<String, String> hmIndent = new HashMap<String, String>();
		List<Document> docs = IndexSearchUtil.searchIndex(searcher, 
				"ELEMENT: content-item AND BOOK_ID: " + bookId);		
		
		//collect all content items
		ArrayList<String> allContentItems = new ArrayList<String>();
		for(Document doc : docs ){
			allContentItems.add(doc.get(ID));
		}
		
		//get all that has subchapters
		ArrayList<String> allHasSubchapters = new ArrayList<String>();
		for(Document doc : docs ){
			String parent = doc.get(PARENT_ID);
			if(allContentItems.contains(parent)){
				allHasSubchapters.add(parent);
			}
		}
		
		//return hashmap of subchapters		
		for(Document doc : docs ){
			String id = doc.get(ID);
			if(allHasSubchapters.contains(id)){
				hmIndent.put(id, YES);
			}else{
				hmIndent.put(id, NO);
			}
		}
		
		//manual clean up 
		allHasSubchapters = null;
		allContentItems = null;
		
		return hmIndent;
	}
	
	public static HashMap<String,String> setTopLevelHashmap(String bookId) {
		HashMap<String, String> tlmap = new HashMap<String, String>();
		List<Document> docs = IndexSearchUtil.searchIndex(System.getProperty(IndexerProperties.INDEX), 
				"ELEMENT: content-item AND BOOK_ID: " + bookId);		
		for(Document doc : docs ){
			String parent = doc.get(PARENT);
			String id = doc.get(ID);
			if(CONTENT_ITEMS.equals(parent)){
				tlmap.put(id, YES);
			}else{
				tlmap.put(id, NO);
			}
		}
		return tlmap;
	}
	
	public static HashMap<String,String> setTopLevelHashmap(IndexSearcher searcher, String bookId) {
		HashMap<String, String> tlmap = new HashMap<String, String>();
		List<Document> docs = IndexSearchUtil.searchIndex(searcher, 
				"ELEMENT: content-item AND BOOK_ID: " + bookId);		
		for(Document doc : docs ){
			String parent = doc.get(PARENT);
			String id = doc.get(ID);
			if(CONTENT_ITEMS.equals(parent)){
				tlmap.put(id, YES);
			}else{
				tlmap.put(id, NO);
			}
		}
		return tlmap;
	}
	
}
