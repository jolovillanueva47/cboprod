/**
 * 
 */
package org.cambridge.ebooks.online.indexer.ebook;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocCollector;
import org.cambridge.ebooks.online.indexer.util.IndexSearchUtil;
import org.cambridge.ebooks.online.indexer.util.StringUtil;


/**
 * @author rvillamor, ahcollado
 *
 */
public class EBookIndexSearch {
	
	public static List<String> getContentIds(String bookId){
		List<String> result = new ArrayList<String>();
		List<Document> ebookListFromIndex = IndexSearchUtil.searchIndex( 
				IndexSearchUtil.INDEX_DIR, "BOOK_ID:"+bookId+ " AND ELEMENT: content-item" );
		
		for(Document doc : ebookListFromIndex){
			if( StringUtil.isNotEmpty(doc.get("ID")) )
				result.add(doc.get("ID"));
		}
			
		return result;
	}
	
	public static List<String> getContentIds(IndexSearcher searcher, String bookId){
		List<String> result = new ArrayList<String>();
		List<Document> ebookListFromIndex = IndexSearchUtil.searchIndex( 
				searcher, "BOOK_ID:"+bookId+ " AND ELEMENT: content-item" );
		
		for(Document doc : ebookListFromIndex){
			if( StringUtil.isNotEmpty(doc.get("ID")) )
				result.add(doc.get("ID"));
		}
			
		return result;
	}
	
	public static String getContent(String contentId){
		StringBuffer content = new StringBuffer();
		List<Document> ebookChapterListFromIndex = IndexSearchUtil.searchIndex( 
				IndexSearchUtil.INDEX_DIR, "PARENT_ID:"+contentId+ " AND ELEMENT: fulltext " +
						"AND (PARENT:content-item OR PARENT:heading OR PARENT:contributor-group) " );
		
		for(Document doc : ebookChapterListFromIndex){
			if( StringUtil.isNotEmpty(doc.get("CONTENTS")) )
				content.append(doc.get("CONTENTS")+" ");
		}
		
		return content.toString();
	}
	
	public static String getContent(IndexSearcher searcher, String contentId){
		StringBuffer content = new StringBuffer();
		List<Document> ebookChapterListFromIndex = IndexSearchUtil.searchIndex( 
				searcher, "PARENT_ID:"+contentId+ " AND ELEMENT: fulltext " +
						"AND (PARENT:content-item OR PARENT:heading OR PARENT:contributor-group) " );
		
		for(Document doc : ebookChapterListFromIndex){
			if( StringUtil.isNotEmpty(doc.get("CONTENTS")) )
				content.append(doc.get("CONTENTS")+" ");
		}
		
		return content.toString();
	}
	
	public static ScoreDoc[] search(IndexSearcher searcher,
			String defaultField, String queryString, int pageSize) {
		ScoreDoc[] result = null;
		try {
			Analyzer analyzer = new WhitespaceAnalyzer();
			Query query = new QueryParser(defaultField, analyzer).parse(queryString);

			TopDocCollector collector = new TopDocCollector(pageSize);
			searcher.search(query, collector);
			result = collector.topDocs().scoreDocs;

		} catch (Exception e) {
			Logger.getLogger( EBookIndexSearch.class).error(e.getMessage());
		}

		return result;
	}

	public static Document searchDocument(IndexSearcher searcher,
			String defaultField, String queryString) {
		Document result = null;
		try {
			Analyzer analyzer = new WhitespaceAnalyzer();
			Query query = new QueryParser(defaultField, analyzer).parse(queryString);

			TopDocCollector collector = new TopDocCollector(1);
			searcher.search(query, collector);

			ScoreDoc[] docs = collector.topDocs().scoreDocs;

			if (docs != null && docs.length == 1) {
				result = searcher.doc(docs[0].doc);
			} else {
				throw new Exception("scoreDocs returned null or more than 1 hit.");
			}

		} catch (Exception e) {
			Logger.getLogger( EBookIndexSearch.class).error(e.getMessage());
		}

		return result;
	}
}
