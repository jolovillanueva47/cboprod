package org.cambridge.ebooks.online.indexer.content;

import org.cambridge.ebooks.online.indexer.util.StringUtil;

/**
 * 
 * @author rvillamor
 *
 */

public class EBooksConfiguration {

	public static String getProperty(String propertyName){
		String result = "";
		
		if (StringUtil.isEmpty( System.getProperty(propertyName)))
		{			
			System.out.println("[EBooksConfiguration]Property name: " + propertyName + " has not been set on property-service.xml");
		}
		else
		{
			result = System.getProperty(propertyName).trim();
		}
		
		return result;
		
	}
	
}
