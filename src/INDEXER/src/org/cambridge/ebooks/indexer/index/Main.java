package org.cambridge.ebooks.indexer.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.ebooks.indexer.util.EBookUtil;

/**
 * This is a class that will index some files on a local filesystem.  This code
 * was modified from a demo that comes with the lucene search engine.
 */
public class Main {
	private static final Logger LOG = Logger.getLogger(Main.class);	
	
	private static String[] ENGLISH_STOP_WORDS;		
	private IndexWriter writer; // new index being built
	private static final String MANIFEST_FILENAME = System.getProperty(IndexerProperties.META_BASE)+ File.separator + "MANIFEST.txt";
	

	
	/**
	 * This is the main entry point for the indexer.
	 *
	 * @param argv The command line arguments.
	 */
	public static void main(String[] argv) {
		
		try
		{
			if(IndexWriter.isLocked(FSDirectory.getDirectory(System.getProperty(IndexerProperties.INDEX))))
			{
				System.out.println("LOCKED! [" + System.getProperty(IndexerProperties.INDEX) + "]");
				System.exit(0);
			}
			
			Main.loadStopWords();		
			 
			File file = new File(System.getProperty(IndexerProperties.INDEX));
			String list[] = file.list();		
			boolean create = true;            

			if (list != null && list.length > 0) create = false;
			
			//this three properties is found in the indexer.properties file
			//this will define the work to be run, anything true will be executed
			boolean isIndex = System.getProperty(IndexerProperties.MAINPROCINDEX) != null;
			boolean isOptimize = System.getProperty(IndexerProperties.MAINPROCOPTIMIZE) != null;
			
			if (isIndex) 
			{
				Set<File> booksToIndex = new HashSet<File>();
				Main indexer = new Main();
				File[] dirs = null;
				boolean argNotEmpty = (argv != null && argv.length > 0);
				
				if(argNotEmpty && argv[0] != null && argv[0].equalsIgnoreCase("all"))
				{
					dirs = EBookUtil.deleteLuceneDocuments(System.getProperty(IndexerProperties.INDEX), EBookUtil.getAllISBNsToIndex());	
				}
				else if(argNotEmpty)
				{
					dirs = EBookUtil.deleteLuceneDocuments(System.getProperty(IndexerProperties.INDEX), returnIsbnsDir(argv));	
				}
				else
				{
					dirs = EBookUtil.getRecentDirs(new File(System.getProperty(IndexerProperties.META_BASE)), getManifest());
					booksToIndex.addAll(Arrays.asList(dirs));
					booksToIndex.addAll(EBookUtil.getDirsWithError());
					
					dirs = EBookUtil.deleteLuceneDocuments(System.getProperty(IndexerProperties.INDEX), booksToIndex.toArray(new File[0]));				
				}
				
				indexer.index(create, dirs);
			}

			if (isOptimize) {		
			//for optimizing the index, removing the stales 
			// ideally run this once every two weeks.
				Main indexer = new Main();
				indexer.optimize();
			}
		}
		catch(Exception ex)
		{
			System.out.println("Exception in main.. " + ex.getMessage());
		}
		
		
	}

	public void optimize() {
		try {
			writer = new IndexWriter(System.getProperty(IndexerProperties.INDEX), new StandardAnalyzer(ENGLISH_STOP_WORDS), false, IndexWriter.MaxFieldLength.LIMITED);
			writer.setMaxFieldLength(Integer.MAX_VALUE);
			writer.setMergeFactor(50);
			writer.optimize();			
			writer.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	

	
	public void index(boolean create, File[] dirs) {
		try {
			writer = new IndexWriter(System.getProperty(IndexerProperties.INDEX), new StandardAnalyzer(ENGLISH_STOP_WORDS), create, IndexWriter.MaxFieldLength.LIMITED);
            writer.setMaxFieldLength(Integer.MAX_VALUE);
            writer.setMergeFactor(50);
            
             String[] bookIds = indexDocs(dirs);
            
            writer.commit();
			writer.close();
			
			//remove lock file
			//EBookUtil.removeLockFileIfExists();
			
			/*for (String bookId : bookIds) {
				ReferenceResolver.resolveCitations(bookId, false);
			}
			
			for (String bookId : bookIds) {
				ReferenceResolver.resolveCitations(bookId, true);
			}*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
//	public void indexAll(File[] dirs) {
//		try 
//		{
//			if(dirs != null && dirs.length > 0)
//			{
//				File dirToBackup = new File(System.getProperty(IndexerProperties.INDEX));
//				if(EBookUtil.isDirectoryAndExists(dirToBackup))
//				{
//					String filename = System.getProperty(IndexerProperties.INDEX) + "_tmp";
//					File backup = new File(filename);
//					if(backup.mkdir())
//					{
//						writer = new IndexWriter(filename, new StandardAnalyzer(ENGLISH_STOP_WORDS), true, IndexWriter.MaxFieldLength.LIMITED);
//			            writer.setMaxFieldLength(Integer.MAX_VALUE);
//			            writer.setMergeFactor(50);
//			            
//			            indexDocs(dirs);
//			            
//			            writer.commit();
//						writer.close();
//						
//						SimpleDateFormat sf = new SimpleDateFormat("MM-dd-yy[hh.mm.ss]");
//						String format = sf.format(new Date());
//						File fileName = new File(System.getProperty(IndexerProperties.INDEX) + "." + format);
//						if(fileName != null)
//						{
//							if(dirToBackup.renameTo( fileName ))
//							{
//								backup.renameTo( new File(System.getProperty(IndexerProperties.INDEX)) );
//							}
//						}
//						
//						
//					}
//				}
//				
//			}
//		} catch (Exception e) {
//			System.out.println("[ERROR] INDEX ALL FAILED! " + e.getMessage());
//		}
//	}	
	
	private String[] indexDocs(File[] dirs){
		Set<String> bookIDs = new HashSet<String>();
		String replacement = String.valueOf(Calendar.getInstance().getTimeInMillis());
		
		if(dirs != null && dirs.length > 0)	
		{
			System.out.println("[INDEX] START ---------");
			long before = Calendar.getInstance().getTimeInMillis();
			System.out.println("Start: " + before);
			
			for(File dir : dirs) 
			{
				try 
				{
					String[] filenames = EBookUtil.getHeaderFile(dir);
					if(filenames != null && filenames.length > 0) 
					{
						ArrayList<Document> metadataList = EbookDocument.metadata(filenames);
						for ( Document metadata : metadataList ) 
						{ 
							writer.addDocument(metadata);
							bookIDs.add(metadata.get("BOOK_ID"));		
						}
					}
				}
				catch (Exception e) 
				{
					System.out.println("[METADATA] Error in dir: " + dir + "error: "+ e.getMessage() );
					EBookUtil.isbnError(dir.getName(), e.getMessage());
			 	}
				
			}
			
			long after = Calendar.getInstance().getTimeInMillis();
			System.out.println("End: " + after);
			long result = after - before;
			System.out.println("Difference: " +  result / (1000 * 1.0));
			
			System.out.println("[INDEX] END ---------");
		}

		putManifest(replacement);
		
		return bookIDs.toArray(new String[0]);
		
	}
	
	private static void loadStopWords() {
		try {			 
			File file = new File(System.getProperty(IndexerProperties.STOPWORDS));
			byte buffer[] = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(buffer);
			fis.close();
			ENGLISH_STOP_WORDS = new String(buffer).split("\\s\\W");			
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
	
	private static String getManifest() {
		String result = "0";
		try {
			BufferedReader br = new BufferedReader(new FileReader(MANIFEST_FILENAME));
			String s = null;
			if ((s = br.readLine()) != null) result = s;
			br.close();
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return result;
	}

	private static void putManifest(String manifest) {
		try {
			FileWriter fw = new FileWriter(MANIFEST_FILENAME);
			fw.write(manifest); //			fw.write(String.valueOf(Calendar.getInstance().getTimeInMillis()));
			fw.close();
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
	
	private static File[] returnIsbnsDir(String[] args){
		Set<File> files = new HashSet<File>();
		if(args != null && args.length > 0)
		{
			for(String isbn : args)
				files.add(new File(System.getProperty(IndexerProperties.META_BASE) + File.separator + isbn));
		}
		
		return files.toArray(new File[0]);
	}
}


