package org.cambridge.ebooks.indexer.index;

import java.io.File;
import java.util.ArrayList;
import org.cambridge.ebooks.indexer.elements.*;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.ebooks.indexer.tools.XMLDOMParser;
import org.cambridge.ebooks.indexer.xpaths.Pattern;

public class EbookDocument
    implements Pattern
{

    public EbookDocument()
    {
    }

    public static ArrayList metadata(String fileNames[])
    {
        ArrayList result = new ArrayList();
        Element elements[] = {
            new Book("/book"), 
            new Author("book/metadata/descendant::author"), 
            new AltIsbn("book/metadata/descendant::alt-isbn"), 
            new AltIsbn("book/metadata/other-volume/alt-isbn"), 
            new Blurb("book/metadata/blurb"), 
            new Metadata("book/descendant::metadata"), 
            new MainTitle("book/metadata/main-title"), 
            new SubTitle("book/metadata/subtitle"), 
            new TransTitle("book/metadata/trans-title"), 
            new Edition("book/metadata/edition"), 
            new OtherEdition("book/metadata/other-edition"), 
            new Doi("book/metadata/doi"), 
            new Isbn("book/metadata/isbn"), 
            new Volume("book/metadata/volume"), 
            new OtherVolume("book/metadata/other-volume"), 
            new Isbn("book/metadata/other-volume/isbn"), 
            new Volume("book/metadata/other-volume/volume"), 
            new VolumeNumber(Pattern.METADATA_VOLUME_NUMBER_XPATH),
            new VolumeTitle(Pattern.METADATA_VOLUME_TITLE_XPATH),
            new PartNumber(Pattern.METADATA_PART_NUMBER_XPATH),
            new PartTitle(Pattern.METADATA_PART_TITLE_XPATH),
            new Series("book/metadata/series"), 
            new PublisherName("book/metadata/descendant::publisher-name"), 
            new PublisherLoc("book/metadata/descendant::publisher-loc"), 
            new PubDates("book/metadata/pub-dates"), 
            new PrintDate("book/metadata/pub-dates/descendant::print-date"), 
            new OnlineDate("book/metadata/pub-dates/descendant::online-date"), 
            new CopyrightStatement("book/metadata/descendant::copyright-statement"), 
            new CopyrightHolder("book/metadata/descendant::copyright-statement/descendant::copyright-holder"), 
            new CopyrightDate("book/metadata/descendant::copyright-statement/descendant::copyright-date"), 
            new SubjectGroup("book/metadata/subject-group"), 
            new Subject("book/metadata/subject-group/descendant::subject"), 
            new Pages("book/metadata/pages"), 
            new CoverImage("book/metadata/descendant::cover-image"), 
            new ContentItems("book/content-items"), 
            new ContentItem("book/content-items/descendant::content-item"), 
            new Title("book/content-items/descendant::content-item/heading/title"), 
            new SubTitle("book/content-items/descendant::content-item/heading/subtitle"), 
            new Pdf("book/content-items/descendant::content-item/pdf"), 
            new Doi("book/content-items/descendant::content-item/doi"), 
            new Contributor("book/content-items/descendant::content-item/contributor-group"), 
            new ContributorGroup("book/content-items/descendant::content-item/contributor-group"), 
            new ThemeGroup("book/content-items/descendant::content-item/theme-group"), 
            new Theme("book/content-items/descendant::content-item/theme-group/descendant::theme"), 
            new SubjectGroup("book/content-items/descendant::content-item/subject-group"), 
            new Subject("book/content-items/descendant::content-item/subject-group/descendant::subject"), 
            new KeywordGroup("book/content-items/descendant::content-item/descendant::keyword-group"), 
            new Keyword("book/content-items/descendant::content-item/descendant::keyword-group/descendant::keyword"), 
            new Toc("book/content-items/descendant::content-item/toc"), 
            new TocItem("book/content-items/descendant::content-item/toc/descendant::toc-item"), 
            new Abstract("book/content-items/descendant::content-item/abstract"), 
            new Block("book/content-items/descendant::content-item/abstract/descendant::block"), 
            new List("book/content-items/descendant::content-item/abstract/descendant::list"), 
            new org.cambridge.ebooks.indexer.elements.Object("book/content-items/descendant::content-item/object"), 
            new References("book/content-items/descendant::content-item/descendant::references"), 
            new Citation("book/content-items/descendant::content-item/descendant::references/descendant::citation"), 
            new BookTitle("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::book-title"), 
            new JournalTitle("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::journal-title"), 
            new ChapterTitle("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::chapter-title"), 
            new Volume("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::volume"), 
            new Issue("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::issue"), 
            new ArticleTitle("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::article-title"), 
            new StartPage("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::startpage"), 
            new Author("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::author"), 
            new Collab("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::collab"), new PublisherName("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::publisher-name"), new PublisherLoc("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::publisher-loc"), new Year("book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::year"), new Label("book/content-items/descendant::content-item/descendant::label"), new FullText("book/content-items/descendant::content-item/pdf")
        };
        String as[];
        int j = (as = fileNames).length;
        for(int i = 0; i < j; i++)
        {
            String fileName = as[i];
            String filePath = (new StringBuilder(String.valueOf(System.getProperty(IndexerProperties.META_BASE)))).append(File.separator).append(fileName.replaceAll(".xml", "")).append(File.separator).append(fileName).toString();
            fileName = fileName.replaceAll(".xml", "");
            org.w3c.dom.Document doc = XMLDOMParser.getXmlDOM(filePath);
            if(doc != null)
            {
                Element obj = elements[elements.length - 1];
                if(obj instanceof FullText)
                    ((FullText)obj).setDir(fileName);
                Element aelement[];
                int l = (aelement = elements).length;
                for(int k = 0; k < l; k++)
                {
                    Element element = aelement[k];
                    result.addAll(element.process(doc));
                }

            }
        }

        return result;
    }
}