///*
// * Created on Oct 3, 2003
// *
// * To change the template for this generated file go to
// * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
// */
//package org.cambridge.ebooks.indexer.properties;
//
//import java.sql.Connection;
//
//import oracle.jdbc.pool.OracleDataSource;
//
///**
// * @author Administrator
// *
//// * To change the template for this generated type comment go to
// * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
// */
//public class DataSource {
//
//	static private OracleDataSource ods;
//
//	static {
//		try {
//			ods = new OracleDataSource();
//			ods.setDriverType(System.getProperty(IndexerProperties.DRIVERTYPE));
//			ods.setServerName(System.getProperty(IndexerProperties.SERVERNAME));
//			ods.setNetworkProtocol(System.getProperty(IndexerProperties.NETWORKPROTOCOL));
//			ods.setDatabaseName(System.getProperty(IndexerProperties.DATABASENAME));
//			ods.setPortNumber(Integer.parseInt(System.getProperty(IndexerProperties.PORTNUMBER)));
//			ods.setUser(System.getProperty(IndexerProperties.USER));
//			ods.setPassword(System.getProperty(IndexerProperties.PASSWORD));
//		} catch (Exception e) {
//			System.err.println(e);
//		}
//	}
//	
//	public static Connection getConnection() {
//		Connection con = null;
//		try {
//			con = ods.getConnection();
//		} catch (Exception e) {
//			System.err.println(e);
//		}
//		return con;
//	}
//	
//	public static void closeConnection(Connection con) {
//		try {
//			con.close();
//		} catch (Exception e) {
//			System.err.println(e);
//		}
//	}	
//}
