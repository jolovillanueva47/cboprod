/*
 * Created on Jan 19, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.indexer.properties;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IndexerProperties {
	
	private static final Logger LOG = Logger.getLogger(IndexerProperties.class);
	
//	public static String DRIVERTYPE = "db.drivertype";
//	public static String SERVERNAME = "db.servername";
//	public static String NETWORKPROTOCOL = "db.networkprotocol";
//	public static String DATABASENAME = "db.databasename";
//	public static String PORTNUMBER = "db.portnumber";
//	public static String USER = "db.user";
//	public static String PASSWORD = "db.password";	
//	public static String ROWPREFETCH = "db.rowprefetch";
	
	public static String META_BASE = "dir.meta.base";
	public static String ABSTRACT_BASE = "dir.abstract.base";
	public static String INDEX = "dir.index";
	public static String PDF_TEXT_BASE = "dir.meta.pdf.text";
	public static String STOPWORDS = "file.stopwords";
	public static String MAINPROCINDEX = "main.proc.index";
	public static String MAINPROCOPTIMIZE = "main.proc.optimize";
	public static String XPDF_DIR = "dir.xpdf.dir";
	public static String ERROR_DIR = "error.isbns.dir";
	public static String LOCK_FILE = "lock.file";

	private static final String indexerProperties = "C:/dev/workspace/ebooks/cbo-release2-a/INDEXER/indexer.properties";
	
	//private static final String indexerProperties = "/app/ebooks/indexer/properties/indexer.properties";
	
	static {
		try {
			Properties props = new Properties(System.getProperties());
			FileInputStream propFile = new FileInputStream(indexerProperties);
			props.load(propFile);
			System.setProperties(props);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
}
