package org.cambridge.ebooks.indexer.elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FullText extends Element{
		
	//for searching
	public static final String PDF = "PDF";
	public static final String FILESIZE = "FILESIZE";
	public static final String CONTENTS = "CONTENTS";
	public static final String PAGE_NUMBER = "PAGE_NUMBER";
	
	private String dir;
	private String pdfFilename;
	private String pageContent;
	private String pageNumber;
	private String listItemXPath;
	
	public FullText(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getPageContent() {
		return pageContent;
	}

	public void setPageContent(String pageContent) {
		this.pageContent = pageContent;
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(pageContent)) document.add(new Field(FullText.CONTENTS, cleanTextNode(pageContent) ,Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(pdfFilename)) document.add(new Field(FullText.PDF, pdfFilename ,Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(pageNumber)) document.add(new Field(FullText.PAGE_NUMBER, pageNumber ,Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("pdf".equalsIgnoreCase(node.getNodeName())){
					
						String parentId = "";
						String pdfFilename = "";
						String parentName = "";
						
						Node parent = node.getParentNode();
						parentName = parent != null ? parent.getNodeName() : "";
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							parentId = getNodeTextContent(nmap.getNamedItem("id"));
						}
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							pdfFilename = getNodeTextContent(nmap.getNamedItem("filename"));
						}
								
						//String fullTextPath = System.getProperty(IndexerProperties.PDF_TEXT_BASE);
						String fullTextPath = System.getProperty(IndexerProperties.META_BASE);
						if(Misc.isNotEmpty(pdfFilename) && Misc.isNotEmpty(fullTextPath) && Misc.isNotEmpty(this.dir)){
							File pdfPath = new File(fullTextPath + File.separator + this.dir + File.separator + pdfFilename.replace("pdf", "txt"));
							if(pdfPath != null && pdfPath.exists() && pdfPath.isFile()){
								
						        StringBuilder pageContent = new StringBuilder();
						        int pageNumber		= 1;
						        
								
								try {
									Reader reader = new InputStreamReader(new FileInputStream(pdfPath), "ISO-8859-1");
																	
									
									int data;
									while ((data = reader.read()) > -1) {
										char dataChar = (char)data;
										
										if(data == 12) { // '\f'
											this.setBookId(bookId);
											this.setElement("fulltext");
											this.setParentId(parentId);
											this.setParent(parentName);
											this.setPdfFilename(pdfFilename);		
											this.setPageContent(pageContent.toString());
											this.setPageNumber("" + pageNumber);
											
											pageNumber++;
											pageContent = new StringBuilder();
											
											result.add(this.getDocument());
										} else if(data == 10) { // next line
											pageContent.append(" ");
										} else {
											// 0 - 127 common, no need to check
											if(data == 38 || data == 60 || data == 62 || data > 127) {
												String stringValue = Character.toString(dataChar);
												String stringValueTemp = new String(stringValue.getBytes("ISO-8859-1"), "UTF-8");
												if(stringValueTemp != null) {
													//160 up are special characters 
													if(data == 173) { // dash
														pageContent.append("&#" + 8208 + ";");
													} else if(stringValueTemp.equals("?") || dataChar == '<' || dataChar == '>' || data > 160) {
														pageContent.append("&#" + data + ";");
													} else if(stringValueTemp.equals("&")) {
														if(dataChar != '#') {
															pageContent.append("&#" + data + ";");
														}
													}
												} else {
													pageContent.append("&#" + data + ";");
												}
											} else {											
												pageContent.append(dataChar);
											}
										}
									}
									reader.close();
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} 								
							}
						}
					}
					
				}
					
			}
		
		}
		
		return result;
	}
	
	
	

}
