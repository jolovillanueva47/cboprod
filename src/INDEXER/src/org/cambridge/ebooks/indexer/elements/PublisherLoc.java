package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT publisher-loc (#PCDATA) >
 * 
 */

//book is the root element
public class PublisherLoc extends Element{
	
	//field name
	public static final String PUBLISHER_LOC = "PUBLISHER_LOC";
	public static final String POSITION = "POSITION";
	
	//attributes
	private String text;
	private String position;
	private String listItemXPath;
	
	public PublisherLoc(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(PublisherLoc.PUBLISHER_LOC, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(position)) document.add(new Field(PublisherLoc.POSITION, position, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					if("publisher-loc".equalsIgnoreCase(node.getNodeName())){

						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						this.setPosition(Integer.toString(index));
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

}
