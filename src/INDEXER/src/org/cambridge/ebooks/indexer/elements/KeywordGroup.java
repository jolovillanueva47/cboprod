package org.cambridge.ebooks.indexer.elements;



import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/*
 * <!ELEMENT keyword-group (keyword+) >
 * <!ATTLIST keyword-group source (index-general | index-other |index-author | other) #REQUIRED >
 */

public class KeywordGroup extends Element{
	
	//field name
	public static final String SOURCE = "SOURCE";
	public static final String KEYWORD = "KEYWORD";
	
	//attributes
	private String sourceAttribute;
	private String keyword;
	private String listItemXPath;
	
	public KeywordGroup(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getSourceAttribute() {
		return sourceAttribute;
	}

	public void setSourceAttribute(String sourceAttribute) {
		this.sourceAttribute = sourceAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(sourceAttribute)) document.add(new Field(KeywordGroup.SOURCE, sourceAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(keyword)) document.add(new Field(KeywordGroup.KEYWORD, keyword, Field.Store.YES, Field.Index.ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null && node.hasAttributes()){
					if("keyword-group".equalsIgnoreCase(node.getNodeName()))
					{
						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes())
						{
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes())
						{
							NamedNodeMap nmap = node.getAttributes();
							this.setSourceAttribute(getNodeTextContent(nmap.getNamedItem("source")));
						}
						
						this.setKeyword(cleanTextNode(getKeywords(node, "keyword-group")));
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

	
	
}
