package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT online-date (#PCDATA) >
 * <!ATTLIST online-date platform CDATA #IMPLIED>
 * 
 */

//book is the root element
public class OnlineDate extends Element{
	
	//field name
	public static final String ONLINE_DATE = "ONLINE_DATE";
	public static final String PLATFORM = "PLATFORM";
	
	//attributes
	private String text;
	private String platformAttribute;
	private String listItemXPath;
	
	public OnlineDate(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPlatformAttribute() {
		return platformAttribute;
	}

	public void setPlatformAttribute(String platformAttribute) {
		this.platformAttribute = platformAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(OnlineDate.ONLINE_DATE, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(platformAttribute)) document.add(new Field(OnlineDate.PLATFORM, platformAttribute, Field.Store.YES, Field.Index.ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("online-date".equalsIgnoreCase(node.getNodeName())){

						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setPlatformAttribute(getNodeTextContent(nmap.getNamedItem("platform")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
