package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT object (#PCDATA) >
 * <!ATTLIST object ref CDATA #REQUIRED
 *                              filename CDATA #REQUIRED>
 * 
 */

//book is the root element
public class Object extends Element{
	
	//field name
	public static final String OBJECT = "OBJECT";
	public static final String REF = "REF";
	public static final String FILENAME = "FILENAME";
	
	//attributes
	private String text;
	private String refAttribute;
	private String filenameAttribute;
	private String listItemXPath;
	
	public Object(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getRefAttribute() {
		return refAttribute;
	}

	public void setRefAttribute(String refAttribute) {
		this.refAttribute = refAttribute;
	}

	public String getFilenameAttribute() {
		return filenameAttribute;
	}

	public void setFilenameAttribute(String filenameAttribute) {
		this.filenameAttribute = filenameAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Object.OBJECT, text, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(refAttribute)) document.add(new Field(Object.REF, refAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(filenameAttribute)) document.add(new Field(Object.FILENAME, filenameAttribute, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("object".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setRefAttribute(getNodeTextContent(nmap.getNamedItem("ref")));
							this.setFilenameAttribute(getNodeTextContent(nmap.getNamedItem("filename")));
						}
						
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
