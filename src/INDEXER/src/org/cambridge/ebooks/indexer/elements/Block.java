package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT block (p+, source?)>
 * 
 */

public class Block extends Element{
	
	//field name
	public static final String BLOCK = "BLOCK";
	public static final String SOURCE = "SOURCE";
	
	//attributes
	private String text;
	private String source;
	private String listItemXPath;
	
	public Block(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Block.BLOCK, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(source)) document.add(new Field(Block.SOURCE, source, Field.Store.YES, Field.Index.ANALYZED));
		
		return document;
	}
	
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("block".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						this.setElement(node.getNodeName());
						
						if(node.hasChildNodes()){
							StringBuilder sb = new StringBuilder();
							NodeList childNodes = node.getChildNodes();
							for(int i=0; i<childNodes.getLength(); i++){
								Node cnode = childNodes.item(i);
								
								if(cnode != null){
									String nodeName = cnode.getNodeName();
									if("p".equalsIgnoreCase(nodeName)){
										
										sb.append(cleanTextNode(cnode.getTextContent())).append(" ");
										this.setText(sb.toString());
										
									}else if("source".equalsIgnoreCase(nodeName)){
										
										this.setSource(cleanTextNode(cnode.getTextContent()));
										
									}
							
								}
							}	
						}
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
