package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT list (list-item)+>
 * <!ATTLIST list style (number|bullet|none) #REQUIRED >
 * 
 */

public class List extends Element{
	
	//field name
	public static final String LIST = "LIST";
	public static final String STYLE = "STYLE";
	
	//attributes
	private String text;
	private String style;
	private String listItemXPath;
	
	public List(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(List.LIST, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(style)) document.add(new Field(List.STYLE, style, Field.Store.YES, Field.Index.ANALYZED));
		
		return document;
	}
	
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("list".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setStyle(getNodeTextContent(nmap.getNamedItem("style")));
						}
						
						if(node.hasChildNodes()){
							StringBuilder sb = new StringBuilder();
							NodeList childNodes = node.getChildNodes();
							for(int i=0; i<childNodes.getLength(); i++){
								Node cnode = childNodes.item(i);
								
								if(cnode != null){
									String nodeName = cnode.getNodeName();
									if("list-item".equalsIgnoreCase(nodeName)){
										sb.append(cleanTextNode(cnode.getTextContent())).append(" ");
										this.setText(sb.toString());
									}
							
								}
							}	
						}
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
