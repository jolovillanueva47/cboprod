package org.cambridge.ebooks.indexer.elements;



import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/*
 * <!ELEMENT theme-group ( theme+) >
 */

public class ThemeGroup extends Element{
	
	private String listItemXPath;
	
	public ThemeGroup(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public Document getDocument(){
		Document document = getCommonDocs();
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			//for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(0);
				
				if(node != null ){
					
					if("theme-group".equalsIgnoreCase(node.getNodeName())){
						
					
						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
				
						result.add(this.getDocument());
					}
					
				}
			//}
		}
		
		return result;
	}
	
	
}
