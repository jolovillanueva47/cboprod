package org.cambridge.ebooks.indexer.elements;

public interface CommonFields{
	
	//for internal use
	
	public static final String BOOK_ID = "BOOK_ID";
	public static final String PARENT = "PARENT";
	public static final String PARENT_ID = "PARENT_ID";
		
	public static final String POSITION = "POSITION";
	public static final String ELEMENT = "ELEMENT";
	
	
}
