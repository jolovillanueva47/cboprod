package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT title (#PCDATA | italic| bold| small-caps | underline | sup | sub)* >
 * <!ATTLIST title alphasort CDATA #IMPLIED >
 * 
 */

//book is the root element
public class Title extends Element{
	
	//field name
	public static final String TITLE = "TITLE";
	public static final String ALPHASORT = "ALPHASORT";
	public static final String TYPE = "TYPE";
	
	//attributes
	private String text;
	private String type;
	private String id;
	private String alphasortAttribute;
	private String listItemXPath;
	
	public Title(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}

	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Title.TITLE, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(Title.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(type)) document.add(new Field(Title.TYPE, type, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null && node.hasAttributes()){
					if("title".equalsIgnoreCase(node.getNodeName())){
						
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						Node grandParent = parent.getParentNode();
						if(grandParent != null && grandParent.hasAttributes()){
							NamedNodeMap nmap = grandParent.getAttributes();
							this.setType(getNodeTextContent(nmap.getNamedItem("type")));
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
						this.setText(convertToUTF8(cleanTextNode(node.getTextContent())));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}


}
