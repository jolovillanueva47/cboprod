package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT book ( (metadata, content-items, supplementary-materials?)|(basic-metadata,supplementary-materials)) >
 *	<!ATTLIST book id ID #REQUIRED group (academic |learning) #REQUIRED>
 */

//book is the root element
public class Book extends Element {

	//fields
	public static final String ID = "ID";
	public static final String GROUP = "GROUP";
	public static final String EISBN = "EISBN";
	
	private String idAttribute;
	private String groupAttribute;
	private String eisbn;
	private String listItemXPath;
	
	public Book(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	
	public String getEisbn() {
		return eisbn;
	}
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	public String getIdAttribute() {
		return idAttribute;
	}
	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}

	public String getGroupAttribute() {
		return groupAttribute;
	}
	public void setGroupAttribute(String groupAttribute) {
		this.groupAttribute = groupAttribute;
	}
	
	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(Book.ID, idAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(groupAttribute)) document.add(new Field(Book.GROUP, groupAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(eisbn)) document.add(new Field(Book.EISBN, eisbn, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		
		if(nList != null & nList.getLength() > 0)
		{
			for(int index=0; index<nList.getLength(); index++)
			{
				Node node = nList.item(index);
				
				if(node != null && node.hasAttributes())
				{
					this.setElement(node.getNodeName());
					
					NamedNodeMap nmap = node.getAttributes();
					this.setBookId(getNodeTextContent(nmap.getNamedItem("id")));
					this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));
					this.setGroupAttribute(getNodeTextContent(nmap.getNamedItem("group")));
					
					
				}
				
				if(node.hasChildNodes())
				{
					NodeList childNodes = node.getChildNodes();
					for(int i=0; i<childNodes.getLength(); i++)
					{
						Node childNode = childNodes.item(i);
						if(childNode != null)
						{
							String nodeName = childNode.getNodeName();
							if("metadata".equals(nodeName))
							{
								if(childNode.hasChildNodes())
								{
									NodeList cNodes = childNode.getChildNodes();
									for(int ctr=0; ctr<cNodes.getLength(); ctr++)
									{
										Node cNode = cNodes.item(ctr);
										if(cNode != null)
										{
											String nName = cNode.getNodeName();
											if("isbn".equals(nName))
											{
												this.setEisbn(cNode.getTextContent());
											}
										}
									}
								}
								
							}
						}
						
					}
				}
				
				result.add(this.getDocument());
			}
		}
		
		return result;
	}

	private String getExpandedGroupCode(String groupCode){
		String expandedGroupCode = null;
		if(groupCode.equals("A"))
			expandedGroupCode = "Academic";
		else if(groupCode.equals("X"))
			expandedGroupCode = "Other";
		else if(groupCode.equals("E"))
			expandedGroupCode = "ELT";
		else if(groupCode.equals("S"))
			expandedGroupCode = "Education";
		else
			expandedGroupCode = groupCode;
		
		return expandedGroupCode;
	}
}
