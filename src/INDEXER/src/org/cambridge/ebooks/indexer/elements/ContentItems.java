package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT content-items (content-item+)  >
 * <!ATTLIST content-items id ID #REQUIRED >
 */

public class ContentItems extends Element{
	
	//field name
	public static final String ID = "ID";
	
	//attributes
	private String idAttribute;
	private String listItemXPath;
	
	public ContentItems(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(ContentItems.ID, idAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("content-items".equalsIgnoreCase(node.getNodeName())){
										
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
