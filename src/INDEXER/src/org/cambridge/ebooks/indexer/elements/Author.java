package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *  <!ELEMENT author (name, affiliation?) >
 *	<!ATTLIST  author id  CDATA #IMPLIED>
 *  <!ATTLIST  author position  CDATA #IMPLIED>
 *  <!ATTLIST  author alphasort  CDATA #IMPLIED>
 *  <!ATTLIST author role (CS | AU | EDT | FW | AW | IN |
 *                         WI | PR | PH | AS | IL | AD |
 *                         NA | AP | CO | PP | GE | COR |
 *                         ED | TR | RA | AE | WC | PL | PB |
 *                         TX | SD | CB | GR | IC | SE ) #REQUIRED>
 *                         
 *  Course consultant 		CS
 *  Author					AU
 *  Editor and Translator	EDT
 *  Foreword				FW
 *  Afterword				AW
 *  Introduction by			IN
 *  With					WI
 *  Preface by				PR
 *  Photographs by			PH
 *  Assisted by				AS
 *  Illustrations by		IL
 *  Adaptation by 			AD
 *  Narrated by 			NA
 *  Appendix by				AP
 *  Consultant Editor		CO
 *  Prepared for publication by	PP
 *  General Editor			GE
 *  Corporation				COR
 *  Editor					ED
 *  Translator				TR
 *  Real Author				RA
 *  Associate Editor		AE
 *  With contributions by	WC
 *  Prologue by				PL
 *  Photographed and recorded by	PB
 *  Text by					TX
 *  Software developer		SD
 *  Compiled by				CB
 *  General Rapporteur		GR
 *  In collaboration with	IC
 *  Series editor Cam Learning use ONLY	SE
 */
		
public class Author extends Element{
		
	//for searching
	public static final String ID = "ID";
	public static final String AUTHOR_POSITION = "AUTHOR_POSITION";
	public static final String AUTHOR = "AUTHOR";
	public static final String AFFILIATION = "AFFILIATION";
	public static final String ALPHASORT = "ALPHASORT";
	public static final String ROLE = "ROLE";
		
	private String author;
	private String affiliation;
	
	private String idAttribute;
	private String positionAttribute;
	private String alphasortAttribute;
	private String roleAttribute; 
	
	private String listItemXPath;
	
	public Author(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}

	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}

	public String getPositionAttribute() {
		return positionAttribute;
	}

	public void setPositionAttribute(String positionAttribute) {
		this.positionAttribute = positionAttribute;
	}
	
	public String getRoleAttribute() {
		return roleAttribute;
	}
	public void setRoleAttribute(String roleAttribute) {
		this.roleAttribute = roleAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(author)) document.add(new Field(Author.AUTHOR, author, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(affiliation)) document.add(new Field(Author.AFFILIATION, affiliation, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(Author.ID, idAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(positionAttribute)) document.add(new Field(Author.AUTHOR_POSITION, positionAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(Author.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(roleAttribute)) document.add(new Field(Author.ROLE, roleAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null){
					affiliation = null; // PID: 0060209
					
					if("author".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));
							this.setPositionAttribute(getNodeTextContent(nmap.getNamedItem("position")));
							this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
							this.setRoleAttribute(getNodeTextContent(nmap.getNamedItem("role")));
						}
						
						if(node.hasChildNodes()){
							NodeList childNodes = node.getChildNodes();
							for(int i=0; i<childNodes.getLength(); i++){
								Node cnode = childNodes.item(i);
								
								if(cnode != null)
								{
									String nodeName = cnode.getNodeName();
									if("name".equalsIgnoreCase(nodeName))
									{
										StringBuilder author = new StringBuilder();
										
										List<Node> namelinks = getChildNodes(cnode, "name-link");	
										for(int ctr=0; ctr<namelinks.size(); ctr++)
										{
											Node namelink = namelinks.get(ctr);
											author.append(namelink.getTextContent()).append(" ");
										}
										
										List<Node> surnames = getChildNodes(cnode, "surname");	
										for(int ctr=0; ctr<surnames.size(); ctr++)
										{
											Node surname = surnames.get(ctr);
											author.append(surname.getTextContent()).append(" ");
										}
										
										List<Node> suffixes = getChildNodes(cnode, "suffix");	
										for(int ctr=0; ctr<suffixes.size(); ctr++)
										{
											Node suffix = suffixes.get(ctr);
											author.append(suffix.getTextContent()).append(" ");
										}
										
										List<Node> forenames = getChildNodes(cnode, "forenames");
										StringBuilder fn = new StringBuilder();
										for(int ctr=0; ctr<forenames.size(); ctr++)
										{
											Node forename = forenames.get(ctr);
											fn.append(forename.getTextContent()).append(" ");
										}
										
										if((!namelinks.isEmpty() || !surnames.isEmpty() || !suffixes.isEmpty()) && !forenames.isEmpty())
											author.insert(author.length() -1, fn.insert(0, ", "));
										
										this.setAuthor(convertToUTF8(cleanTextNode(author.toString())));
											
									}
									else if("affiliation".equalsIgnoreCase(nodeName))
									{
										this.setAffiliation(convertToUTF8(cleanTextNode(cnode.getTextContent())));
									}
								}
							}	
						}
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	
	
}
