package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT alt-isbn (#PCDATA) >
 * <!ATTLIST alt-isbn type (paperback |hardback |e-isbn | other) #REQUIRED >
 * <!ATTLIST alt-isbn digits (10|13) #REQUIRED >
 * 
 */


public class AltIsbn extends Element{
	
	//field name
	public static final String ALT_ISBN = "ALT_ISBN";
	public static final String TYPE = "TYPE";
	public static final String DIGITS = "DIGITS";
	
	private String text;
	private String typeAttribute;
	private String digitsAttribute;
	private String listItemXPath;
	
	public AltIsbn(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

	public String getDigitsAttribute() {
		return digitsAttribute;
	}

	public void setDigitsAttribute(String digitsAttribute) {
		this.digitsAttribute = digitsAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(text)) document.add(new Field(AltIsbn.ALT_ISBN, text, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(typeAttribute)) document.add(new Field(AltIsbn.TYPE, typeAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(digitsAttribute)) document.add(new Field(AltIsbn.DIGITS, digitsAttribute, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("alt-isbn".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setTypeAttribute(getNodeTextContent(nmap.getNamedItem("type")));
							this.setDigitsAttribute(getNodeTextContent(nmap.getNamedItem("digits")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}


}
