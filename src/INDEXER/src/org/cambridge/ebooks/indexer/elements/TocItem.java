package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT toc-item (toc-text, author*, toc-item*) >
 * <!ATTLIST toc-item startpage CDATA #REQUIRED
                            id                    ID     #REQUIRED>
 */

//book is the root element
public class TocItem extends Element{
	
	//field name
	public static final String TOC_ITEM = "TOC_ITEM";
	public static final String START_PAGE = "START_PAGE";
	public static final String ID = "ID";
	public static final String CONTENT_ITEM_ID = "CONTENT_ITEM_ID";
	public static final String AUTHOR = "AUTHOR";
	
	//attributes
	private String text;
	private String startPageAttribute;
	private String idAttribute;
	private String contentItemId;
	private String author;
	
	private String listItemXPath;
	
	public TocItem(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(TocItem.TOC_ITEM, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(TocItem.ID, idAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(contentItemId)) document.add(new Field(TocItem.CONTENT_ITEM_ID, contentItemId, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(startPageAttribute)) document.add(new Field(TocItem.START_PAGE, startPageAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(author)) document.add(new Field(TocItem.AUTHOR, author, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null){
					if("toc-item".equalsIgnoreCase(node.getNodeName())){
						
						
						this.setBookId(bookId);
						this.setElement(node.getNodeName());
						
						Node parent = node.getParentNode();
						if(parent != null)
						{
							this.setParent(parent.getNodeName());
							if(parent.hasAttributes())
							{
								NamedNodeMap nmap = parent.getAttributes();
								this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
							}
							
						}
						
						//from content-item
						Node contentItem = getParent(node, "content-item");
						if(contentItem != null)
						{
							if(contentItem.hasAttributes())
							{
								NamedNodeMap nmap = contentItem.getAttributes();
								this.setContentItemId(getNodeTextContent(nmap.getNamedItem("id")));
							}
						}
						//--
					
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setStartPageAttribute(getNodeTextContent(nmap.getNamedItem("startpage")));
							this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));
						}
							
						if(node.hasChildNodes()){
							NodeList nodeList = node.getChildNodes();
							for(int tocIndex=0; tocIndex < nodeList.getLength(); tocIndex++)
							{
								Node item = nodeList.item(tocIndex);
								if(item != null)
								{
									if("toc-text".equals(item.getNodeName()))
										this.setText(cleanTextNode(getChildNodeTree("toc-text", item)));
									else if("author".equals(item.getNodeName()))
									{
										NodeList nameList = item.getChildNodes();
										for(int nameIndex=0; nameIndex < nameList.getLength(); nameIndex++)
										{
											Node name = nodeList.item(nameIndex);
											if(name != null)
												if("name".equals(item.getNodeName()))
												{
													StringBuilder author = new StringBuilder();
													
													List<Node> namelinks = getChildNodes(item, "name-link");	
													for(int ctr=0; ctr<namelinks.size(); ctr++)
													{
														Node namelink = namelinks.get(ctr);
														author.append(namelink.getTextContent()).append(" ");
													}
													
													List<Node> surnames = getChildNodes(item, "surname");	
													for(int ctr=0; ctr<surnames.size(); ctr++)
													{
														Node surname = surnames.get(ctr);
														if(surnames.size() -1 == ctr)
															author.append(surname.getTextContent());
														else
															author.append(surname.getTextContent()).append(" ");
													}
													
													List<Node> forenames = getChildNodes(item, "forenames");	
													for(int ctr=0; ctr<forenames.size(); ctr++)
													{
														if(!surnames.isEmpty() && !author.toString().contains(","))
														{
															author.append(", ");
														}
														
														Node forename = forenames.get(ctr);
														author.append(forename.getTextContent()).append(" ");
													}
													
													List<Node> suffixes = getChildNodes(item, "suffix");	
													for(int ctr=0; ctr<suffixes.size(); ctr++)
													{
														Node suffix = suffixes.get(ctr);
														if(suffixes.size() -1 == ctr)
															author.append(suffix.getTextContent());
														else
															author.append(suffix.getTextContent()).append(" ");
													}
													
													this.setAuthor(convertToUTF8(cleanTextNode(author.toString())));
												}
											
										}
									}
										
								}
								
							}
						}
						
						
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStartPageAttribute() {
		return startPageAttribute;
	}

	public void setStartPageAttribute(String startPageAttribute) {
		this.startPageAttribute = startPageAttribute;
	}

	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}
	protected String getContentItemId() {
		return contentItemId;
	}
	protected void setContentItemId(String contentItemId) {
		this.contentItemId = contentItemId;
	}

	protected String getAuthor() {
		return author;
	}

	protected void setAuthor(String author) {
		this.author = author;
	}

}
