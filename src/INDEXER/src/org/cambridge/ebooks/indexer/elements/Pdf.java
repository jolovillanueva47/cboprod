package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *   <!ELEMENT pdf EMPTY >
 *   <!ATTLIST pdf filename CDATA #REQUIRED>
 */

public class Pdf extends Element{
	
	//field name
	public static final String FILENAME = "FILENAME";
	public static final String TYPE = "TYPE";
	
	//attributes
	private String filenameAttribute;
	private String type;
	private String listItemXPath;
	
	public Pdf(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilenameAttribute() {
		return filenameAttribute;
	}

	public void setFilenameAttribute(String filenameAttribute) {
		this.filenameAttribute = filenameAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(filenameAttribute)) document.add(new Field(Pdf.FILENAME, filenameAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(type)) document.add(new Field(Pdf.TYPE, type, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("pdf".equalsIgnoreCase(node.getNodeName())){
						

						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes()){
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
							this.setType(getNodeTextContent(nmap.getNamedItem("type")));
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setFilenameAttribute(getNodeTextContent(nmap.getNamedItem("filename")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
