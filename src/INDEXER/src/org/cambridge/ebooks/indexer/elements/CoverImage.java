package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
* <!ELEMENT cover-image EMPTY >
* <!ATTLIST cover-image filename CDATA #REQUIRED
*                       type (standard|thumb) #REQUIRED>
*/

public class CoverImage extends Element{
		
	//field names
	public static final String FILENAME = "FILENAME";
	public static final String TYPE = "TYPE";
	
	private String filenameAttribute;
	private String typeAttribute;
	private String listItemXPath;
	
	public CoverImage(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}	
	public String getFilenameAttribute() {
		return filenameAttribute;
	}

	public void setFilenameAttribute(String filenameAttribute) {
		this.filenameAttribute = filenameAttribute;
	}

	public String getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
				
		if(Misc.isNotEmpty(filenameAttribute)) document.add(new Field(CoverImage.FILENAME, filenameAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(typeAttribute)) document.add(new Field(CoverImage.TYPE, typeAttribute, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("cover-image".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");				
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setFilenameAttribute(getNodeTextContent(nmap.getNamedItem("filename")));
							this.setTypeAttribute(getNodeTextContent(nmap.getNamedItem("type")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	
	
}
