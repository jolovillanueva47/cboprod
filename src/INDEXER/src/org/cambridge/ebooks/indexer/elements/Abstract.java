package org.cambridge.ebooks.indexer.elements;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.ebooks.indexer.util.EBookUtil;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Abstract extends Element{
	
	//field name
	public static final String ABSTRACT = "ABSTRACT";
	public static final String ALT_FILENAME = "ALT_FILENAME";
	public static final String PROBLEM = "PROBLEM";

	
	//attributes
	private String text;
	private String altFileNameAttribute;
	private String problemAttribute;
	private String pdfFileName;
	private String listItemXPath;
	
	private HashMap<String, String> abstractMap = new HashMap<String, String>();
	
	public String getPdfFileName() {
		return pdfFileName;
	}

	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}

	public Abstract(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAltFileNameAttribute() {
		return altFileNameAttribute;
	}

	public void setAltFileNameAttribute(String altFileNameAttribute) {
		this.altFileNameAttribute = altFileNameAttribute;
	}
	
	public String getProblemAttribute() {
		return problemAttribute;
	}

	public void setProblemAttribute(String problemAttribute) {
		this.problemAttribute = problemAttribute;
	}
	
	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Abstract.ABSTRACT, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(altFileNameAttribute)) document.add(new Field(Abstract.ALT_FILENAME, altFileNameAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(problemAttribute)) document.add(new Field(Abstract.PROBLEM, problemAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public void createAbstractFile()throws Exception{
		Set<String> filenaNames = abstractMap.keySet();
		Iterator<String> it = filenaNames.iterator();
		File indexDir = new File(System.getProperty(IndexerProperties.ABSTRACT_BASE));
		String bookId = this.getBookId();
		
		File dir = new File(indexDir, bookId.substring(2, bookId.length()));
		if(!EBookUtil.isDirectoryAndExists(dir))
		{
			dir.mkdir();
		}
		
		while(it.hasNext())
		{
			String file = it.next();
			File abstractFile = new File(dir, file);
			if(!abstractFile.exists())
			{
				abstractFile.createNewFile();
			}
			
			PrintWriter writer = new PrintWriter(abstractFile);
			writer.write(abstractMap.get(file));
			writer.close();
		}
		
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++)
			{
				Node node = nList.item(index);
				if(node != null )
				{
					if("abstract".equalsIgnoreCase(node.getNodeName()))
					{
						this.setBookId(bookId);
						
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						if(parent.hasAttributes())
						{
							NamedNodeMap nmap = parent.getAttributes();
							this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));	
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes())
						{
							NamedNodeMap nmap = node.getAttributes();
							this.setAltFileNameAttribute(getNodeTextContent(nmap.getNamedItem("alt-filename")));
							this.setProblemAttribute(getNodeTextContent(nmap.getNamedItem("problem")));
						}
						
						if(node.hasChildNodes())
						{
							this.setText(cleanTextNode(getChildNodeTree("abstract", node)));							
						}
						result.add(this.getDocument());
					}
				}
			}
		}
		
		return result;
	}
	
}
