package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT series (#PCDATA|italic)* >
 * <!ATTLIST series number CDATA #IMPLIED
 *                              part CDATA #IMPLIED
 *                              position CDATA #IMPLIED
 *                              code    CDATA    #REQUIRED>
 * 
 */

//book is the root element
public class Series extends Element{
	
	//field name
	public static final String SERIES = "SERIES";
	public static final String NUMBER = "NUMBER";
	public static final String PART = "PART";
	public static final String POSITION = "POSITION";
	public static final String ALPHASORT = "ALPHASORT";
	public static final String CODE = "CODE";
	
	//attributes
	private String text;
	private String numberAttribute;
	private String partAttribute;
	private String positionAttribute;
	private String alphasortAttribute;
	private String codeAttribute;
	
	private String listItemXPath;
	
	public Series(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getNumberAttribute() {
		return numberAttribute;
	}

	public void setNumberAttribute(String numberAttribute) {
		this.numberAttribute = numberAttribute;
	}

	public String getPartAttribute() {
		return partAttribute;
	}

	public void setPartAttribute(String partAttribute) {
		this.partAttribute = partAttribute;
	}

	public String getPositionAttribute() {
		return positionAttribute;
	}

	public void setPositionAttribute(String positionAttribute) {
		this.positionAttribute = positionAttribute;
	}

	public String getCodeAttribute() {
		return codeAttribute;
	}

	public void setCodeAttribute(String codeAttribute) {
		this.codeAttribute = codeAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Series.SERIES, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(numberAttribute)) document.add(new Field(Series.NUMBER, numberAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(partAttribute)) document.add(new Field(Series.PART, partAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(positionAttribute)) document.add(new Field(Series.POSITION, positionAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(Series.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(codeAttribute)) document.add(new Field(Series.CODE, codeAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					if("series".equalsIgnoreCase(node.getNodeName())){
									
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setPartAttribute(getNodeTextContent(nmap.getNamedItem("part")));
							this.setPositionAttribute(getNodeTextContent(nmap.getNamedItem("position")));
							this.setNumberAttribute(getNodeTextContent(nmap.getNamedItem("number")));
							this.setCodeAttribute(getNodeTextContent(nmap.getNamedItem("code")));
							this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
							
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}
	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}

}
