package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT  copyright-date (#PCDATA) >
 * 
 */

//book is the root element
public class CopyrightDate extends Element{
	
	//field name
	public static final String COPYRIGHT_DATE = "COPYRIGHT_DATE";
	
	//attributes
	private String text;
	private String listItemXPath;
	
	public CopyrightDate(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(CopyrightDate.COPYRIGHT_DATE, text, Field.Store.YES, Field.Index.ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("copyright-date".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}


}
