package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT theme (#PCDATA) >
 * 
 */

//book is the root element
public class Theme extends Element{
	
	//field name
	public static final String THEME = "THEME";
	public static final String TYPE = "TYPE";
	
	//attributes
	private String text;
	private String listItemXPath;
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Theme(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Theme.THEME, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(type)) document.add(new Field(Theme.TYPE, type, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("theme".equalsIgnoreCase(node.getNodeName())){
					
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						if(parent != null)
						{
							this.setParent(parent.getNodeName());
							Node grandParent = parent.getParentNode();
							if(grandParent != null && grandParent.hasAttributes()){
								NamedNodeMap nmap = grandParent.getAttributes();
								this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
								this.setType(getNodeTextContent(nmap.getNamedItem("type")));
							}
						}
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(node.getTextContent()));
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}


}
