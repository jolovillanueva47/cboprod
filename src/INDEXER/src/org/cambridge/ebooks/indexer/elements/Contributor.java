package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.tools.XPathWorker;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *  <!ELEMENT contributor (name, affiliation?) >
 *  <!ATTLIST  contributor position CDATA #IMPLIED>
 */
		
public class Contributor extends Element{
		
	//for searching
	public static final String POSITION = "POSITION";
	public static final String CONTRIBUTOR = "CONTRIBUTOR";
	public static final String AFFILIATION = "AFFILIATION";
	public static final String ALPHASORT = "ALPHASORT";
	public static final String TYPE = "TYPE";
	public static final String CONTRIBUTOR_COLLAB = "CONTRIBUTOR_COLLAB";
		
	private String contributor;
	private String affiliation;
	private String type;
	private String collab;
	
	private String positionAttribute;
	private String alphasortAttribute;
	private String listItemXPath;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Contributor(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}

	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}

	public String getContributor() {
		return contributor;
	}

	public void setContributor(String contributor) {
		this.contributor = contributor;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getCollab() {
		return collab;
	}

	public void setCollab(String collab) {
		this.collab = collab;
	}
	
	public String getPositionAttribute() {
		return positionAttribute;
	}

	public void setPositionAttribute(String positionAttribute) {
		this.positionAttribute = positionAttribute;
	}
	

	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(contributor)) document.add(new Field(Contributor.CONTRIBUTOR, contributor, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(affiliation)) document.add(new Field(Contributor.AFFILIATION, affiliation, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(collab)) document.add(new Field(Contributor.CONTRIBUTOR_COLLAB, collab, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(positionAttribute)) document.add(new Field(Contributor.POSITION, positionAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(Contributor.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(type)) document.add(new Field(Contributor.TYPE, type, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				if(node != null){
					
					if("contributor-group".equalsIgnoreCase(node.getNodeName()))
					{
						
						if(node.hasChildNodes())
						{
							NodeList cNodes = node.getChildNodes();
							for(int ctr=0; ctr<cNodes.getLength(); ctr++)
							{
								Node contributor = cNodes.item(ctr);
								if("contributor".equalsIgnoreCase(contributor.getNodeName()))
								{
									resetFields();
									this.setBookId(bookId);

									Node parent = getParent(contributor, "content-item");
									if(parent != null)
									{
										this.setParent(parent.getNodeName());
										if(parent.hasAttributes()){
											NamedNodeMap nmap = parent.getAttributes();
											this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
											this.setType(getNodeTextContent(nmap.getNamedItem("type")));
										}
									}
									
									this.setElement(contributor.getNodeName());
									
									if(contributor.hasAttributes()){
										NamedNodeMap nmap = contributor.getAttributes();
										this.setPositionAttribute(getNodeTextContent(nmap.getNamedItem("position")));
										this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
									}
									
									if(contributor.hasChildNodes()){
										NodeList childNodes = contributor.getChildNodes();
										for(int i=0; i<childNodes.getLength(); i++){
											Node cnode = childNodes.item(i);
											
											if(cnode != null){
												String nodeName = cnode.getNodeName();
												if("name".equalsIgnoreCase(nodeName)){
													
													StringBuilder cbtr = new StringBuilder();
													
													List<Node> namelinks = getChildNodes(cnode, "name-link");	
													for(int x=0; x<namelinks.size(); x++)
													{
														Node namelink = namelinks.get(x);
														cbtr.append(namelink.getTextContent()).append(" ");
													}
													
													List<Node> surnames = getChildNodes(cnode, "surname");	
													for(int x=0; x<surnames.size(); x++)
													{
														Node surname = surnames.get(x);
														if(surnames.size() -1 == x)
															cbtr.append(surname.getTextContent());
														else
															cbtr.append(surname.getTextContent()).append(" ");
													}
													
													List<Node> forenames = getChildNodes(cnode, "forenames");	
													for(int x=0; x<forenames.size(); x++)
													{
														if(!surnames.isEmpty() && !cbtr.toString().contains(","))
														{
															cbtr.append(", ");
														}
														
														Node forename = forenames.get(x);
														cbtr.append(forename.getTextContent()).append(" ");
													}
													
													List<Node> suffixes = getChildNodes(cnode, "suffix");	
													for(int x=0; x<suffixes.size(); x++)
													{
														Node suffix = suffixes.get(x);
														if(suffixes.size() -1 == x)
															cbtr.append(suffix.getTextContent());
														else
															cbtr.append(suffix.getTextContent()).append(" ");
													}
																	
													this.setContributor(convertToUTF8(cleanTextNode(cbtr.toString())));
													
													// collab
													StringBuilder cbtrCollab = new StringBuilder();
													List<Node> collabs = getChildNodes(cnode, "collab");	
													for(int x=0; x<collabs.size(); x++)
													{
														Node collab = collabs.get(x);
														if(collabs.size() -1 == x)
															cbtrCollab.append(collab.getTextContent());
														else
															cbtrCollab.append(collab.getTextContent()).append(" ");
													}
													this.setCollab(cleanTextNode(cbtrCollab.toString()));
													
													/*if(contributor.hasAttributes()){
														NamedNodeMap nmap = cnode.getAttributes();
														this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort") != null ? getNodeTextContent(nmap.getNamedItem("alphasort").getTextContent() : "");
													}*/
														
												}else if("affiliation".equalsIgnoreCase(nodeName)){
													this.setAffiliation(cleanTextNode(cnode.getTextContent()));
												}
											}
											
										}	
									}
									
									result.add(this.getDocument());
								}
							}
						}
					}
					
					
				}
			}
		}
		
		return result;
	}
	
	public void resetFields(){
		this.contributor = "";
		this.affiliation = "";
		this.collab = "";
		this.type = "";
		this.positionAttribute = "";
		this.alphasortAttribute = "";
	}
	
}
