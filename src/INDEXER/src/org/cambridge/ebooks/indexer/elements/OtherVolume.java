package org.cambridge.ebooks.indexer.elements;



import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/*
 * <!ELEMENT other-volume (isbn, alt-isbn?,  volume) >
 */

public class OtherVolume extends Element{
	
	private String listItemXPath;
	
	public OtherVolume(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public Document getDocument(){
		Document document = getCommonDocs();
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			//for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(0);
				
				if(node != null ){
					
					if("other-volume".equalsIgnoreCase(node.getNodeName())){
						
					
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						this.setElement(node.getNodeName());
				
						result.add(this.getDocument());
					}
					
				}
			//}
		}
		
		return result;
	}
	
}
