package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT content-item (pdf?, doi, heading, contributor-group?, subject-group?, theme-group?, keyword-group?,toc?,abstract?, object?, (content-item+|references?) )>
 * <!ATTLIST content-item
 *                       id      ID     #REQUIRED
 *                       page-start CDATA #IMPLIED
 *                       page-end CDATA #IMPLIED
 *                       type CDATA #IMPLIED
 *                       position CDATA #REQUIRED>
 */

public class ContentItem extends Element{
	
	//field name
	public static final String ID = "ID";
	public static final String PAGE_START = "PAGE_START";
	public static final String PAGE_END = "PAGE_END";
	public static final String TYPE = "TYPE";
	public static final String POSITION = "POSITION";
	public static final String PDF = "PDF";
	public static final String TITLE = "TITLE";
	public static final String ALPHASORT = "ALPHASORT";
	public static final String DOI = "DOI";
	public static final String ABSTRACT = "ABSTRACT";
	public static final String ALT_FILENAME = "ALT_FILENAME";
	public static final String LABEL = "LABEL";
	
	//attributes
	private String idAttribute;
	private String pageStartAttribute;
	private String pageEndAttribute;
	private String typeAttribute;
	private String positionAttribute;
	private String alphasortAttribute;
	private String listItemXPath;
	private String pdf;
	private String doi;
	private String abstractText;
	private String title;
	private String altfilename;
	private String label;
	
	public ContentItem(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	
	private void resetFields(){
		this.idAttribute = null;
		this.pageStartAttribute = null;
		this.pageEndAttribute = null;
		this.typeAttribute = null;
		this.positionAttribute = null;
		this.alphasortAttribute = null;
		this.pdf = null;
		this.doi = null;
		this.abstractText = null;
		this.title = null;
		this.altfilename = null;
		this.label = null;
		this.bookId = null;
		this.parent = null;
		this.parentId = null;
		this.element = null;
	}
	
	public String getAltfilename() {
		return altfilename;
	}
	public void setAltfilename(String altfilename) {
		this.altfilename = altfilename;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getAbstractText() {
		return abstractText;
	}
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPdf() {
		return pdf;
	}
	public void setPdf(String pdf) {
		this.pdf = pdf;
	}
	
	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}

	public String getPageStartAttribute() {
		return pageStartAttribute;
	}

	public void setPageStartAttribute(String pageStartAttribute) {
		this.pageStartAttribute = pageStartAttribute;
	}

	public String getPageEndAttribute() {
		return pageEndAttribute;
	}

	public void setPageEndAttribute(String pageEndAttribute) {
		this.pageEndAttribute = pageEndAttribute;
	}

	public String getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

	public String getPositionAttribute() {
		return positionAttribute;
	}

	public void setPositionAttribute(String positionAttribute) {
		this.positionAttribute = positionAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(ContentItem.ID, idAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(pageStartAttribute)) document.add(new Field(ContentItem.PAGE_START, pageStartAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(pageEndAttribute)) document.add(new Field(ContentItem.PAGE_END, pageEndAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(typeAttribute)) document.add(new Field(ContentItem.TYPE, typeAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(positionAttribute)) document.add(new Field(ContentItem.POSITION, positionAttribute, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(ContentItem.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(pdf)) document.add(new Field(ContentItem.PDF, pdf, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(title)) document.add(new Field(ContentItem.TITLE, title, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(doi)) document.add(new Field(ContentItem.DOI, doi, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(abstractText)) document.add(new Field(ContentItem.ABSTRACT, abstractText, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(altfilename)) document.add(new Field(ContentItem.ALT_FILENAME, altfilename, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(label)) document.add(new Field(ContentItem.LABEL, label, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				if(node != null )
				{
					if("content-item".equalsIgnoreCase(node.getNodeName()))
					{
						resetFields();
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						if(parent != null )
						{	
							this.setParent(parent.getNodeName());
							if(parent.hasAttributes())
							{
								NamedNodeMap nmap = parent.getAttributes();
								this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
							}
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));	
							this.setPageStartAttribute(getNodeTextContent(nmap.getNamedItem("page-start")));
							this.setPageEndAttribute(getNodeTextContent(nmap.getNamedItem("page-end")));
							this.setTypeAttribute(getNodeTextContent(nmap.getNamedItem("type")));
							this.setPositionAttribute(getNodeTextContent(nmap.getNamedItem("position")));
						}
						
						if(node.hasChildNodes())
						{
							NodeList childNodes = node.getChildNodes();
							for(int i=0; i<childNodes.getLength(); i++)
							{
								Node childNode =  childNodes.item(i);
								if(childNode != null)
								{
									
									
									String nodeName = childNode.getNodeName();
									if("pdf".equalsIgnoreCase(nodeName))
									{
										if(childNode.hasAttributes())
										{
											NamedNodeMap nmap = childNode.getAttributes();
											this.setPdf(getNodeTextContent(nmap.getNamedItem("filename")));
										}
									}
									else if("doi".equalsIgnoreCase(nodeName))
									{
										this.setDoi(childNode.getTextContent());
									}
									else if("abstract".equalsIgnoreCase(nodeName))
									{
										if(childNode.hasAttributes())
										{
											NamedNodeMap nmap = childNode.getAttributes();
											this.setAltfilename(getNodeTextContent(nmap.getNamedItem("alt-filename")));
										}
										this.setAbstractText(cleanTextNode(getChildNodeTree("abstract", childNode)));
										
									}
									else if("heading".equalsIgnoreCase(nodeName))
									{
										if(childNode.hasChildNodes())
										{
											
											NodeList headingChildNodes = childNode.getChildNodes();
											for(int ii=0; ii<headingChildNodes.getLength(); ii++)
											{
												Node headingnode = headingChildNodes.item(ii);
												
												if(headingnode != null)
												{
													String nName = headingnode.getNodeName();
													if("title".equalsIgnoreCase(nName))
													{
														this.setTitle(getChildNodeTree("title", headingnode));
														if(headingnode.hasAttributes())
														{
															NamedNodeMap nmap = headingnode.getAttributes();
															this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
														}
													}
													else if("label".equalsIgnoreCase(nName))
													{
														this.setLabel(convertToUTF8(headingnode.getTextContent()));
													}
												}
											}
											
										}	
									}
									
									
									
								}
							}
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}
	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}


}
