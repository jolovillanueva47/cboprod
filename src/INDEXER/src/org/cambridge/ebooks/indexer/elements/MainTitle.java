package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT main-title (#PCDATA | italic | bold| small-caps | underline | sup | sub)* >
 * <!ATTLIST main-title alphasort CDATA #REQUIRED >
 * 
 */

//book is the root element
public class MainTitle extends Element{
	
	public static final String MAIN_TITLE = "MAIN_TITLE";
	public static final String ALPHASORT = "ALPHASORT";
	
	//attributes
	private String text;
	private String alphasortAttribute;
	private String listItemXPath;
	
	public MainTitle(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public String getAlphasortAttribute() {
		return alphasortAttribute;
	}
	public void setAlphasortAttribute(String alphasortAttribute) {
		this.alphasortAttribute = alphasortAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
			
		if(Misc.isNotEmpty(text)) document.add(new Field(MainTitle.MAIN_TITLE, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(alphasortAttribute)) document.add(new Field(MainTitle.ALPHASORT, convertToUTF8(alphasortAttribute), Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null && node.hasAttributes()){
					if("main-title".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(getChildNodeTree("main-title", node)));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setAlphasortAttribute(getNodeTextContent(nmap.getNamedItem("alphasort")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
}
