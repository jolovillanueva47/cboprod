package org.cambridge.ebooks.indexer.elements;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.tools.XPathWorker;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public abstract class Element implements CommonFields {
	
	//for internal use
	public String bookId;
	public String parent;
	public String parentId;
	public String element;

	protected static XPathWorker xpathWorker = new XPathWorker();
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	
	public abstract ArrayList<Document> process(org.w3c.dom.Document doc);
	
	protected Document getCommonDocs(){
		Document document = new Document();
		
		if(Misc.isNotEmpty(bookId)) document.add(new Field(CommonFields.BOOK_ID, bookId, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(parent))document.add(new Field(CommonFields.PARENT, parent, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(parentId)) document.add(new Field(CommonFields.PARENT_ID, parentId, Field.Store.YES, Field.Index.NOT_ANALYZED));
		if(Misc.isNotEmpty(element)) document.add(new Field(CommonFields.ELEMENT, element, Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		return document;
	}
	
	
	protected Node getParent(Node node, String parentName){
		if(node != null && node.getParentNode() != null )
		{
			if(parentName != null && parentName.length() > 0)
			{
				Node parent = node.getParentNode();
				if(parent.getNodeName().equals(parentName))
				{
					return parent;
				}
				else
				{
					return getParent(parent, parentName);
				}
			}
		}
		
		return null;
	}

		
	protected String getChildNodeTree(final String parentName, Node node){
		
		StringBuilder result = new StringBuilder();
		StringBuilder attributes = new StringBuilder();
		String root = null;
		
		if(node != null && node.hasChildNodes())
		{
			NodeList nodeList = node.getChildNodes();
			if( node.hasAttributes() && !excludeTags().contains(node.getNodeName()) )
			{
				NamedNodeMap nmap = node.getAttributes();
				for(int index=0; index<nmap.getLength(); index++)
				{
					Node attributeNode = nmap.item(index);
					if(attributeNode != null && !attributeNode.getNodeName().equals("xmlns:xml"))
					{
						attributes.append(" ").append(attributeNode.getNodeName()).append("=\"").append(attributeNode.getTextContent()).append("\"");						
					}

				}
			}
			
			root = node.getNodeName();
			
			for(int index=0; index<nodeList.getLength(); index++)
			{
				Node nodeFromList = nodeList.item(index);	
				if(nodeFromList != null )
				{
					
					if(!nodeFromList.hasChildNodes())
					{
						result.append(nodeFromList.getTextContent());
					}
					else
					{
						result.append(getChildNodeTree(parentName, nodeFromList));
					}
				}
			}
			
			if(root != null && !root.equalsIgnoreCase(parentName) && !excludeTags().contains(root))
			{
				result.insert(0, "[LTHAN]" + root + attributes.toString() + "[GTHAN]").append("[LTHAN]/" + root + "[GTHAN]");	
			}
			
		}
		
		if(node != null && node.getNodeName().equals(parentName))
		{
			result = new StringBuilder(convertToUTF8(result.toString()));
			result = new StringBuilder(result.toString().replace("[LTHAN]", "<").replace("[GTHAN]", ">"));
			result = new StringBuilder(correctHtmlTags(result.toString()));
		}
		
		return result.toString();
		
	}

	protected String correctHtmlTags(String text) {		
		String output = "";
		try {
			output = text.replaceAll("<italic>", "<i>")
			.replaceAll("</italic>", "</i>")
			.replaceAll("<block>", "<blockquote id=\"indent\">")
			.replaceAll("</block>", "</blockquote>")
			.replaceAll("<underline>", "<u>")
			.replaceAll("</underline>", "</u>")
			.replaceAll("<bold>", "<b>")
			.replaceAll("</bold>", "</b>")
			.replaceAll("<list-item>", "<li>")
			.replaceAll("</list-item>", "</li>")
			.replaceAll("<list", "<ul")
			.replaceAll("style=\"", "style=\"list-style:")
			.replaceAll("style=\"list-style:bullet", "style=\"padding: 20px 20px 20px 30px; list-style:disc !important;")
			.replaceAll("style=\"list-style:number", "style=\"padding: 20px 20px 20px 30px; list-style:decimal !important;")
			.replaceAll("style=\"list-style:none", "style=\"padding: 20px 20px 20px 30px; list-style:none !important;")
			.replaceAll("</list>", "</ul>")
			.replaceAll("<small-caps>", "<span style='font-variant:small-caps'>")
			.replaceAll("</small-caps>", "</span>")
			.replaceAll("<source>", "<p align=\"right\">")
			.replaceAll("</source>", "</p>");
		} catch (Exception e) {
			output = "";
		}
		
		return output;
	}
	
	protected List<Node> getChildNodes(Node node, final String nodeName){
		List<Node> nodes = new ArrayList<Node>();
		if(node != null && node.hasChildNodes())
		{
			if(nodeName != null && nodeName.length() > 0)
			{
				NodeList nodeList = node.getChildNodes();
				for(int index=0; index<nodeList.getLength(); index++)
				{
					Node returnedNode = nodeList.item(index);
					if(returnedNode != null && nodeName.equalsIgnoreCase(returnedNode.getNodeName()))
					{
						nodes.add(returnedNode);
					}
				}
			}	
		}
		return nodes;
	}
	
	//TODO: to be removed, use getNodeContents instead
	protected String getChildNodeTreeWithOutTags(final String parentName, Node node){
		StringBuilder result = new StringBuilder();
		
		try
		{
			if(node != null && node.hasChildNodes())
			{
				NodeList nodeList = node.getChildNodes();
				
				for(int index=0; index<nodeList.getLength(); index++)
				{
					Node nodeFromList = nodeList.item(index);	
					if(nodeFromList != null )
					{
						if(!nodeFromList.hasChildNodes())
						{
							result.append(cleanTextNode(nodeFromList.getTextContent())).append(" ");
						}
						else
						{
							result.append(getChildNodeTreeWithOutTags(parentName, nodeFromList));
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println("[" + Element.class +  "] getChildNodeTreeWithOutTags() " + ex.getMessage());
		}
		
		return result.toString();
	}
	
	protected String getNodeContents(Node node){
		StringBuilder result = new StringBuilder();
		
		try
		{
			if(node != null && node.hasChildNodes())
			{
				NodeList nodeList = node.getChildNodes();
				
				for(int index=0; index<nodeList.getLength(); index++)
				{
					Node nodeFromList = nodeList.item(index);	
					if(nodeFromList != null )
					{
						if(!nodeFromList.hasChildNodes())
						{
							result.append(nodeFromList.getTextContent()).append(" ");
						}
						else
						{
							result.append(getNodeContents(nodeFromList));
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println("[" + Element.class +  "] getNodeContents() " + ex.getMessage());
		}
		
		return result.toString();
	}
	
	protected String getKeywords(Node node, final String parent){
		StringBuilder result = new StringBuilder();
				
		try
		{
			if(node != null && node.hasChildNodes())
			{
				
				NodeList nodeList = node.getChildNodes();
				for(int index=0; index<nodeList.getLength(); index++)
				{
					Node nodeFromList = nodeList.item(index);	
					if(nodeFromList != null )
					{
						
						if(!nodeFromList.hasChildNodes() && !"".equals(nodeFromList.getTextContent()))
						{
							result.append(cleanTextNode(nodeFromList.getTextContent()));
						}
						else
						{
							if( "kwd-text".equalsIgnoreCase(nodeFromList.getNodeName()) )
							{
								if("keyword".equals(nodeFromList.getParentNode().getParentNode().getNodeName()))
								{
									int parentCount = getParentCount("keyword", nodeFromList);
									result.append(repeatString("[LTHAN]div[GTHAN]", parentCount) + getKeywords(nodeFromList, parent) + repeatString("[LTHAN]/div[GTHAN]", parentCount)  + "[LTHAN]br/[GTHAN]");
								}
								else
								{
									result.append(getKeywords(nodeFromList, parent)).append("[LTHAN]br/[GTHAN]");
								}	
							}
							else
							{
								String tagName = nodeFromList.getNodeName();
								if(!tagName.equals("keyword") && !tagName.equals(parent))
								{
									result.append("[LTHAN]" + tagName + "[GTHAN]").append(getKeywords(nodeFromList, parent)).append("[LTHAN]/" + tagName + "[GTHAN]");
								}
								else
								{
									result.append(getKeywords(nodeFromList, parent));
								}
							}						
						}
						
					}
				}
				
				if(node.getNodeName().equals(parent))
				{
					result = new StringBuilder(convertToUTF8(result.toString()));
					result = new StringBuilder(result.toString().replace("[LTHAN]", "<").replace("[GTHAN]", ">"));
					result = new StringBuilder(correctHtmlTags(result.toString()));
				}
				
			}
		}
		catch(Exception ex)
		{
			System.out.println("[" + Element.class +  "] getKeywords() " + ex.getMessage());
		}
		
		return result.toString();
	}
	
	private int getParentCount(String parentName, Node node){
		int result = 0;
		boolean done = false;
		
		while(!done)
		{
			node = node.getParentNode();
			if(node != null)
			{
				if(parentName.equals(node.getNodeName()))
					result++;
				else
					done = true;
			}
		}
	
		return result - 1;
	}
	
	private String repeatString(String toRepeat, int count){
		StringBuilder result = new StringBuilder();
		
		if(count > 0)
			while(count != 0)
			{
				count--;
				result.append(toRepeat);
			}
		
		return result.toString();
	}
	
	private List<String> excludeTags(){
		String[] tags = {"book-title", "journal-title", "chapter-title",
				         "volume", "issue", "article-title", "startpage",
				         "author", "name", "forenames", "surname", "name-link",
				         "suffix", "collab", "affiliation",	"publisher-name", 
				         "publisher-loc", "year" };
		
		return Arrays.asList(tags);
	}
	
	protected String cleanTextNode(String val) {
		if (Misc.isNotEmpty(val)) 
		{
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll("\\s+", " ");  //.trim()
		}
		return val;
	}
	
	protected String getXPath(org.w3c.dom.Document doc, String listItemXPath) {
		
		if(listItemXPath != null && !"".equals(listItemXPath)){
			NodeList nl = xpathWorker.searchForNode(doc, listItemXPath);

			if (nl.getLength() > 0) {
				return nl.item(0).getNodeValue();
				
			}
		}
			
		return null;
	}
	
	protected static boolean isEmpty(java.lang.Object str) { 
		return str == null || "".equals(str);
	}
	
	protected static boolean isNotEmpty(java.lang.Object str){ 
		return !isEmpty(str);
	}
	
	protected String bookId(NodeList nList){
		String bookId = "";
		try
		{
			bookId = getParent(nList.item(0), "book").getAttributes().getNamedItem("id").getTextContent();
		}
		catch(Exception ex){}
			
		return bookId;
	}
	
	protected String getNodeTextContent(Node node){
		try
		{
			String content = node.getTextContent();
			return content;
		}
		catch(Exception ex)
		{
			return "";
		}
	}
	
	public static synchronized String convertToUTF8(String toUTF8){
		StringBuilder utf8 = new StringBuilder();
		if(toUTF8 != null && toUTF8.length() > 0)
		{
			utf8 = new StringBuilder(toUTF8);
			try 
			{
				//if( !isUTF8(utf8.toString()) ) //removed because of UTF-8 checking issue on solaris
				//{
					boolean isDone = false;
					while(!isDone)
					{
						int index = 0;
						char[] c = utf8.toString().toCharArray();
						for(index=0; index<c.length; index++)
						{
							int intValue = (int)c[index];
							if(intValue == 38 || intValue == 60 || intValue == 62 || intValue > 127) //0 -127 common, no need to check
							{
								String stringValue = Character.toString(c[index]);
								String temp = new String(stringValue.getBytes("ISO-8859-1"), "UTF-8");
								if(temp != null)
								{
									if(temp.equals("?") || stringValue.equals("<") || stringValue.equals(">") || intValue > 160) //160 up are special characters 
									{
										utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
										break;
									}
									else if(temp.equals("&"))
									{
										if((c.length == 1) || (index != c.length - 1 && c[index + 1] != '#') || 
										   (index == c.length - 1)) 
										   
										{
											utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
											break;
										}
										
									}
								}
							}
						}
						
						if(index == utf8.toString().length()) isDone = true;
					}
				//}
				
			} 
			catch (Exception e) 
			{
				System.err.println("[Exception] convertToUTF8() " + e.getMessage());
			}
			
		}
		return utf8.toString();
	}
	
	public static boolean isUTF8(final String toCheckForEncoding) {
		 
		boolean result = false;
		 CharsetDecoder cd = null;
		 
		 if(toCheckForEncoding != null && toCheckForEncoding.length() > 0)
		 {
			byte[] b = toCheckForEncoding.getBytes();
			 cd = Charset.availableCharsets().get("UTF-8").newDecoder();
			
			try 
			{
				cd.decode(ByteBuffer.wrap(b));
				result = true;
			} 
			catch (CharacterCodingException e) 
			{
				result = false;
			}
		 }	 
		 return result;
		 
	}
	
	
	public static void main(String[] args){
		String str = "� �  �  >   <";
		
		System.out.println(convertToUTF8(str));
	}
	
}
