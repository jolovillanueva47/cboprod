package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT other-edition EMPTY >
 * <!ATTLIST other-edition number CDATA #REQUIRED
 *                           isbn CDATA #REQUIRED>
 * 
 */

public class OtherEdition extends Element{
	
	//field name
	public static final String NUMBER = "NUMBER";
	public static final String ISBN = "ISBN";
	
	//attributes
	private String isbnAttribute;
	private String numberAttribute;
	private String listItemXPath;
	
	public OtherEdition(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getIsbnAttribute() {
		return isbnAttribute;
	}

	public void setIsbnAttribute(String isbnAttribute) {
		this.isbnAttribute = isbnAttribute;
	}

	public String getNumberAttribute() {
		return numberAttribute;
	}

	public void setNumberAttribute(String numberAttribute) {
		this.numberAttribute = numberAttribute;
	}

	
	public Document getDocument(){
		Document document = getCommonDocs();
		
		if(Misc.isNotEmpty(isbnAttribute)) document.add(new Field(OtherEdition.ISBN, isbnAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(numberAttribute)) document.add(new Field(OtherEdition.NUMBER, numberAttribute, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("other-edition".equalsIgnoreCase(node.getNodeName())){

						this.setBookId(bookId);
						Node parent = node.getParentNode();
						this.setParent(parent != null ? parent.getNodeName() : "");
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setNumberAttribute(getNodeTextContent(nmap.getNamedItem("number")));
							this.setIsbnAttribute(getNodeTextContent(nmap.getNamedItem("isbn")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}

}
