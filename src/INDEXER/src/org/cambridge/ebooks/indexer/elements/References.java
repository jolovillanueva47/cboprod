package org.cambridge.ebooks.indexer.elements;



import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/*
 * <!ELEMENT references (citation+) >
 * <!ATTLIST references type (notes | further-reading|bibliography |reference-list) #IMPLIED>
 */

public class References extends Element{
	
	//field name
	public static final String TYPE = "TYPE";
	
	//attributes
	private String typeAttribute;
	private String listItemXPath;
	
	public References(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}

	public String getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

	public Document getDocument(){
		Document document = getCommonDocs();
		if(Misc.isNotEmpty(typeAttribute)) document.add(new Field(References.TYPE, typeAttribute, Field.Store.YES, Field.Index.NO));
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
			
				if(node != null ){
					if("references".equalsIgnoreCase(node.getNodeName())){
					
						this.setBookId(bookId);
						
						Node parent = getParent(node, "content-item");
						
						if(parent != null)
						{
							this.setParent(parent.getNodeName());
							if(parent.hasAttributes())
							{
								NamedNodeMap nmap = parent.getAttributes();
								this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
							}
						}
						
						this.setElement(node.getNodeName());
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setTypeAttribute(getNodeTextContent(nmap.getNamedItem("type")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
	
	
}
