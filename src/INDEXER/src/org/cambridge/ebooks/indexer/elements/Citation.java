package org.cambridge.ebooks.indexer.elements;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.cambridge.ebooks.indexer.xpaths.Pattern;
import org.cambridge.util.Misc;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * <!ELEMENT citation (#PCDATA | italic | bold| underline| uri | sup | sub | small-caps| book-title | journal-title| chapter-title | volume  | issue| article-title| startpage | author | publisher-name |publisher-loc | year )* >
 * <!ATTLIST citation
 *        type (book | journal | other) #IMPLIED
 *        id	 ID #IMPLIED>
 * 
 */

//book is the root element
public class Citation extends Element{
	
	//field name
	public static final String CITATION = "CITATION";
	public static final String REFERENCE_TITLE = "REFERENCE_TITLE";
	public static final String ID = "ID";
	public static final String TYPE = "TYPE";
	public static final String REFERENCES_TYPE = "REFERENCES_TYPE"; //this was added because the parent element of CITATION element does not have an id
	
	private String text;
	private String refTitle;
	
	//attributes
	private String idAttribute;
	private String typeAttribute;
	private String referenceType;
	private String listItemXPath;
	
	public Citation(String listItemXPath){
		this.listItemXPath = listItemXPath;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getReferenceType() {
		return referenceType;
	}
	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}
	
	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}

	public String getTypeAttribute() {
		return typeAttribute;
	}

	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}
	
	public String getRefTitle() {
		return refTitle;
	}
	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public Document getDocument(){
		Document document = getCommonDocs();

		if(Misc.isNotEmpty(text)) document.add(new Field(Citation.CITATION, text, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(refTitle)) document.add(new Field(Citation.REFERENCE_TITLE, convertToUTF8(refTitle), Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(idAttribute)) document.add(new Field(Citation.ID, idAttribute, Field.Store.YES, Field.Index.ANALYZED));
		if(Misc.isNotEmpty(typeAttribute)) document.add(new Field(Citation.TYPE, typeAttribute, Field.Store.YES, Field.Index.NO));
		if(Misc.isNotEmpty(referenceType)) document.add(new Field(Citation.REFERENCES_TYPE, referenceType, Field.Store.YES, Field.Index.NO));
		
		return document;
	}
	
	public ArrayList<Document> process(org.w3c.dom.Document doc){
		ArrayList<Document> result = new ArrayList<Document>();
		NodeList nList = xpathWorker.searchForNode(doc, listItemXPath);
		String bookId = null;
		
		if(nList != null & nList.getLength() > 0){
			bookId = bookId(nList);
			for(int index=0; index<nList.getLength(); index++){
				Node node = nList.item(index);
				
				if(node != null ){
					
					if("citation".equalsIgnoreCase(node.getNodeName())){
						
						this.setBookId(bookId);
						
						Node contentItemNode = getParent(node, "content-item");
						
						if(contentItemNode != null)
						{
							this.setParent(contentItemNode.getNodeName());
							if(contentItemNode.hasAttributes())
							{
								NamedNodeMap nmap = contentItemNode.getAttributes();
								this.setParentId(getNodeTextContent(nmap.getNamedItem("id")));
							}
						}
						
						Node referencesNode = getParent(node, "references");
						if(referencesNode != null)
						{
							if(referencesNode.hasAttributes())
							{
								NamedNodeMap nmap = referencesNode.getAttributes();
								this.setReferenceType(getNodeTextContent(nmap.getNamedItem("type")));
							}
						}
						
						//to get reference title node
						Node parent = node.getParentNode();
						NodeList nlist = parent.getChildNodes();
						for(int cindex=0; cindex < nlist.getLength(); cindex++)				
						{
							Node childNode = nlist.item(cindex);
							if(childNode != null && childNode.getNodeName().equalsIgnoreCase("title"))
							{
								this.setRefTitle(childNode.getTextContent());
							}
						}
							
						this.setElement(node.getNodeName());
						this.setText(cleanTextNode(getChildNodeTree("citation", node)));
						
						if(node.hasAttributes()){
							NamedNodeMap nmap = node.getAttributes();
							this.setIdAttribute(getNodeTextContent(nmap.getNamedItem("id")));
							this.setTypeAttribute(getNodeTextContent(nmap.getNamedItem("type")));
						}
						
						result.add(this.getDocument());
					}
					
				}
			}
		}
		
		return result;
	}
}
