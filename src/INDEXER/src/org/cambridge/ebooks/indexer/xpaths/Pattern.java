package org.cambridge.ebooks.indexer.xpaths;

public interface Pattern {
	
	public static final String BOOK_XPATH =  "/book";
	public static final String BOOK_ID =  "book/attribute::id";
	
	public static final String METADATA_XPATH= "book/descendant::metadata";
	
	public static final String METADATA_MAIN_TITLE_XPATH = "book/metadata/main-title";
	public static final String METADATA_SUB_TITLE_XPATH = "book/metadata/subtitle";
	public static final String METADATA_TRANS_TITLE_XPATH = "book/metadata/trans-title";
	
	public static final String METADATA_EDITION_XPATH = "book/metadata/edition";
	public static final String METADATA_OTHER_EDITION_XPATH = "book/metadata/other-edition";
	
	public static final String METADATA_AUTHOR_XPATH = "book/metadata/descendant::author";
	
	public static final String METADATA_EDITOR_XPATH = "book/metadata/descendant::editor";
	
	public static final String METADATA_DOI_XPATH = "book/metadata/doi";
	
	public static final String METADATA_ISBN_XPATH = "book/metadata/isbn";
	
	public static final String METADATA_ALT_ISBN_XPATH = "book/metadata/descendant::alt-isbn";
	
	public static final String METADATA_VOLUME_XPATH = "book/metadata/volume";
	
	public static final String METADATA_OTHER_VOLUME_XPATH = "book/metadata/other-volume";
	public static final String METADATA_OTHER_VOLUME_ISBN_XPATH = "book/metadata/other-volume/isbn";
	public static final String METADATA_OTHER_VOLUME_ALT_ISBN_XPATH = "book/metadata/other-volume/alt-isbn"; 
	public static final String METADATA_OTHER_VOLUME_VOLUME_XPATH = "book/metadata/other-volume/volume";
	
	public static final String METADATA_SERIES_XPATH = "book/metadata/series";
		
	public static final String METADATA_PUBLISHER_XPATH = "book/metadata/descendant::publisher-name";
	public static final String METADATA_PUBLISHER_LOC_XPATH = "book/metadata/descendant::publisher-loc";
	
	public static final String METADATA_PUB_DATES_XPATH = "book/metadata/pub-dates";
	public static final String METADATA_PRINT_DATE_XPATH = "book/metadata/pub-dates/descendant::print-date";
	public static final String METADATA_ONLINE_DATE_XPATH = "book/metadata/pub-dates/descendant::online-date";

	public static final String PRINT_DATE_XPATH = "book/metadata/pub-dates/descendant::print-date[1]";
	public static final String ONLINE_DATE_XPATH = "book/metadata/pub-dates/descendant::online-date[1]";
	
	
	public static final String METADATA_COPYRIGHT_STATEMENT_XPATH = "book/metadata/descendant::copyright-statement";
	public static final String METADATA_COPYRIGHT_HOLDER_XPATH = "book/metadata/descendant::copyright-statement/descendant::copyright-holder";
	public static final String METADATA_COPYRIGHT_DATE_XPATH = "book/metadata/descendant::copyright-statement/descendant::copyright-date";
	
	public static final String METADATA_SUBJECT_GROUP_XPATH = "book/metadata/subject-group";
	public static final String METADATA_SUBJECT_XPATH = "book/metadata/subject-group/descendant::subject";

	public static final String METADATA_BLURB_XPATH = "book/metadata/blurb"; //*
	
	public static final String METADATA_PAGES_XPATH = "book/metadata/pages";
	
	public static final String METADATA_COVERIMAGES_XPATH = "book/metadata/descendant::cover-image";
	

	public static final String CONTENT_ITEMS_XPATH = "book/content-items";
	public static final String CONTENT_ITEM_XPATH =  "book/content-items/descendant::content-item";
	
	public static final String CHAPTER_ID_XPATH =  "book/content-items/descendant::content-item";
	

	public static final String CONTENT_ITEM_PDF_XPATH = "book/content-items/descendant::content-item/pdf"; 
	
	public static final String CONTENT_ITEM_DOI_XPATH = "book/content-items/descendant::content-item/doi";
	
	public static final String CONTENT_ITEM_CONTRIBUTOR_GROUP_XPATH = "book/content-items/descendant::content-item/contributor-group";
	public static final String CONTENT_ITEM_CONTRIBUTOR_XPATH = "book/content-items/descendant::content-item/contributor-group/descendant::contributor";
	
	public static final String CONTENT_ITEM_SUBJECT_GROUP_XPATH = "book/content-items/descendant::content-item/subject-group";
	public static final String CONTENT_ITEM_SUBJECT_XPATH = "book/content-items/descendant::content-item/subject-group/descendant::subject";
	
	public static final String CONTENT_ITEM_THEMEGROUP_XPATH = "book/content-items/descendant::content-item/theme-group";
	public static final String CONTENT_ITEM_THEME_XPATH = "book/content-items/descendant::content-item/theme-group/descendant::theme";
	

	public static final String CONTENT_ITEM_KEYWORDGROUP_XPATH = "book/content-items/descendant::content-item/descendant::keyword-group";
	public static final String CONTENT_ITEM_KEYWORD_XPATH = "book/content-items/descendant::content-item/descendant::keyword-group/descendant::keyword";
	
	
	public static final String CONTENT_ITEM_TOC_XPATH = "book/content-items/descendant::content-item/toc";
	public static final String CONTENT_ITEM_TOC_ITEM_XPATH = "book/content-items/descendant::content-item/toc/descendant::toc-item";
	
	public static final String CONTENT_ITEM_ABSTRACT_XPATH = "book/content-items/descendant::content-item/abstract";
	public static final String CONTENT_ITEM_ABSTRACT_BLOCK_XPATH = "book/content-items/descendant::content-item/abstract/descendant::block";
	public static final String CONTENT_ITEM_ABSTRACT_LIST_XPATH = "book/content-items/descendant::content-item/abstract/descendant::list";
	
	public static final String CONTENT_ITEM_OBJECT_XPATH = "book/content-items/descendant::content-item/object";
	
	public static final String REFERENCES_XPATH =  "book/content-items/descendant::content-item/descendant::references";
	public static final String CITATION_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation";
	

	public static final String CITATION_BOOK_TITLE_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::book-title";	
	public static final String CITATION_JOURNAL_TITLE_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::journal-title";	
	public static final String CITATION_CHAPTER_TITLE_XPATH=  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::chapter-title";

	public static final String CITATION_VOLUME_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::volume";
	public static final String CITATION_ISSUE_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::issue";
	public static final String CITATION_ARTICLE_TITLE_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::article-title";
	public static final String CITATION_STARTPAGE_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::startpage";
	public static final String CITATION_COLLAB_XAPTH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::collab";
	public static final String CITATION_PUBLISHER_NAME_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::publisher-name";
	public static final String CITATION_PUBLISHER_LOC_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::publisher-loc";	
	public static final String CITATION_YEAR_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::year";

	public static final String CITATION_AUTHOR_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::author";
	public static final String CITATION_EDITOR_XPATH =  "book/content-items/descendant::content-item/descendant::references/descendant::citation/descendant::editor";

	public static final String CONTENT_ITEM_HEADING_TITLE_XPATH = "book/content-items/descendant::content-item/heading/title"; 
	public static final String CONTENT_ITEM_HEADING_SUBTITLE = "book/content-items/descendant::content-item/heading/subtitle"; 
	
	public static final String CONTENT_ITEM_LABEL = "book/content-items/descendant::content-item/descendant::label";
	
	public static final String CONTENT_ITEM_HEADING = "book/content-items/descendant::content-item[attribute::id=\"?\"]/heading";
	public static final String CONTENT_ITEM_HEADING_LABEL = "book/content-items/descendant::content-item[attribute::id=\"?\"]/heading/label/text()"; //*
	

	public static final String CONTENT_ITEM_HEADING_TITLE_APHASORT = "book/content-items/descendant::content-item[attribute::id=\"?\"]/heading/title/attribute::alphasort"; //*
	
	public static final String METADATA_VOLUME_NUMBER_XPATH = "book/metadata/volume-number";
	public static final String METADATA_VOLUME_TITLE_XPATH = "book/metadata/volume-title";
	public static final String METADATA_PART_NUMBER_XPATH = "book/metadata/part-number";
	public static final String METADATA_PART_TITLE_XPATH = "book/metadata/part-title";
}
