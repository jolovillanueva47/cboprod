package org.cambridge.ebooks.indexer.util;


import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.cambridge.ebooks.indexer.properties.IndexerProperties;
import org.cambridge.ebooks.indexer.tools.XMLDOMParser;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class EBookUtil {
	private static final Logger LOG = Logger.getLogger(EBookUtil.class);
	public static void convertPdfToText(File pdfFile, File outputDir) throws IOException{
		if((pdfFile != null && pdfFile.exists()) &&  isDirectoryAndExists(outputDir)){
			
			File textFile = new File(outputDir.getPath()+ File.separator + pdfFile.getName().replace("pdf", "txt"));
			if(textFile == null || ( !textFile.exists() || pdfFile.lastModified() >  textFile.lastModified() ))
			{
				List<String> command = new ArrayList<String>();
				ProcessBuilder builder = new ProcessBuilder(command);
				
				command.add(System.getProperty(IndexerProperties.XPDF_DIR) + File.separator + "pdftotext");
				command.add(pdfFile.getPath());
			    command.add(outputDir.getPath()+ File.separator + pdfFile.getName().replace("pdf", "txt"));
				//start converting
				builder.start();
			}
			
		}
	}

	public static boolean isDirectoryAndExists(File dir){
		return (dir != null && dir.exists() && dir.isDirectory());
	}
	
	
	public static String[] getFileNamesByFileType(File directory, final String fileType, final String... exclude){
		if( isDirectoryAndExists(directory) && (fileType != null && !"".equals(fileType.trim())) ){
			
			return directory.list(
					new FilenameFilter(){
						public boolean accept (File dir, String n){
							String f = new File(n).getName().toLowerCase();
							if(exclude != null && exclude.length > 0)
							{
								boolean contains = false;
								for(String str : exclude)
									if(str != null && !"".equals(str))
									{
										if(( contains = (f.contains(str) || f.contains(str.toUpperCase())) ))
											break;
									}
								
								return f.endsWith(fileType) && !contains;  
							}else{
								return f.endsWith(fileType);  //"pdf"
							}
							
						}
					}); 
	
		}
		
		return null;
	}
	
	public static String[] getHeaderFile(File directory){
		if( isDirectoryAndExists(directory) ){
			
			final String filename = directory.getName() + ".xml";
			return directory.list(
					new FilenameFilter(){
						public boolean accept (File dir, String n){
							String f = new File(n).getName().toLowerCase();
							return f.equals(filename);
						}
					}); 
	
		}
		
		return null;
	}
	
	public static File[] getFilesByType(File directory, final String fileType){
		if( isDirectoryAndExists(directory) && (fileType != null && !"".equals(fileType.trim())) ){
			
			return directory.listFiles(
					new FilenameFilter(){
						public boolean accept (File dir, String n){
							String f = new File(n).getName().toLowerCase();
							return f.endsWith(fileType);  //"pdf"
						}
					}); 
			
		}
		
		return null;
	}
	
	public static File[] getDirectoriesInADirectory(File dir){
		if( isDirectoryAndExists(dir)){
			return dir.listFiles(
					
					new FileFilter(){
						public boolean accept(File pathname) {
							return isDirectoryAndExists(pathname); 
						}
					}
					
			); 
		}
		
		return null;
	}
	
	public static File[] getRecentDirs(File dir, final String manifest) {
		return dir.listFiles(new FileFilter() {
			public boolean accept(File f) {
				long timestamp = 0;
				try { timestamp = Long.parseLong(manifest); } catch (Exception e) { }
				return f.isDirectory() && (f.lastModified() > timestamp); // return isDirectoryAndExists(pathname);
			}
		});
	}
	
	public static Set<File> getDirsWithError() {
		File dirWithErrors = new File(System.getProperty(IndexerProperties.ERROR_DIR));
		Set<File> isbns = new HashSet<File>();
		if(EBookUtil.isDirectoryAndExists(dirWithErrors))
		{
			File[] isbnsWithError = dirWithErrors.listFiles();
			if(isbnsWithError != null && isbnsWithError.length > 0)
			{
				for(File file : isbnsWithError)
				{
					try
					{
						isbns.add(new File(System.getProperty(IndexerProperties.META_BASE) +  File.separator + file.getName()));
						file.delete();
					}
					catch(Exception e)
					{
						System.out.println("[Exception] getDirsWithError().. File: " + file.getName() + " "  + e.getMessage());
					}
				}
			}
		}
		
		return isbns;
	}

	public static File[] extractTextFromPDF(String outputDir, File[] dirs){
		
		File outputdir = new File(outputDir);
		File newfile = null;
		Set<File> isbns = new HashSet<File>();
		
		if(dirs != null && dirs.length > 0)
		{
			
			for(File file : dirs)
			{	
				try
				{
					File[] pdfs = getFilesByType(file, "pdf");
					if(pdfs != null && pdfs.length > 0)
					{
						if(outputdir != null && !outputdir.exists())
							outputdir.mkdir();
							
						newfile = new File(outputdir, file.getName());
						if(newfile != null && !newfile.exists())
							newfile.mkdir();
						
						for(File pdf : pdfs)
						{
							convertPdfToText(pdf, newfile);
						}
						
					}
					isbns.add(file);
				}
				catch(IOException e)
				{
					System.out.println("[IOException] extractTextFromPDF().. File: " + file.getName() + " "  + e.getMessage());
					EBookUtil.isbnError(file.getName(), "extractTextFromPDF() " + e.getMessage());
				}
			}
		}
			
		return isbns.toArray(new File[0]);
		
	}
	
	
	public static File[] deleteLuceneDocuments(final String indexDir, File[] dirs){
		Set<File> isbns = new HashSet<File>();
	
		if(dirs != null && dirs.length > 0 && indexDir != null && !"".equals(indexDir)){
			try
			{
				FSDirectory dirIndex = FSDirectory.getDirectory( indexDir );
				IndexWriter writer = new IndexWriter(dirIndex, new StandardAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
				String bookId = "";
				
				for(File file : dirs)
				{
					try
					{
						String[] filenames = EBookUtil.getHeaderFile(file);
						String filePath = "";
						if(filenames != null && filenames.length > 0)
						{
							filePath = System.getProperty(IndexerProperties.META_BASE) + File.separator + filenames[0].replaceAll(".xml", "") + File.separator + filenames[0];
							
							org.w3c.dom.Document doc = XMLDOMParser.getXmlDOM(filePath);
							NodeList nl = searchForNode(doc, "book/attribute::id");
							if (nl != null && nl.getLength() > 0) 
							{
								bookId = nl.item(0).getNodeValue();
								System.out.println("Deleting BOOK_ID: " + bookId);
								
								writer.deleteDocuments(new Term("BOOK_ID", bookId));
								isbns.add(file);
							}
							
						}
					}
					catch(Exception ex)
					{
						System.out.println("[ERROR] deleteLuceneDocuments().. BOOK_ID: " + bookId + " "  + ex.getMessage());
						EBookUtil.isbnError(file.getName(), "deleteLuceneDocuments() " + ex.getMessage());
					}
					
				}
				
				writer.optimize();
				writer.commit();
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("[IOException] deleteLuceneDocuments() " + e.getMessage());
				EBookUtil.isbnError(dirs, "deleteLuceneDocuments() " + e.getMessage());
			}
			
		}
		return  isbns.toArray(new File[0]);
	}
	
	public static void isbnError(String filename, String errorMessage){
		try
		{
			File file = new File(System.getProperty(IndexerProperties.ERROR_DIR) + File.separator + filename);
			file.createNewFile();
			
			if(file.exists())
			{
				PrintWriter writer = new PrintWriter(new FileWriter(file, true));
				writer.println(errorMessage);
				writer.close();
			}
			
		}
		catch(Exception e)
		{
			LOG.error("[ERROR] isbnError().. filename: " + filename );
		}
	}
	
	public static void isbnError(File[] files, String errorMessage){
		if(files != null && files.length > 0)
		{
			for(File file : files)
			{
				if(file != null)
					EBookUtil.isbnError(file.getName(), errorMessage);
			}
		}
	}
	
	public static File[] getAllISBNsToIndex(){
		File[] result = null;
		File dirContainingBooks = null;
		
		dirContainingBooks = new File(System.getProperty(IndexerProperties.META_BASE));
		if(dirContainingBooks != null)
			result = dirContainingBooks.listFiles();
		
		return result;
	}
	
	public static NodeList searchForNode(final Node xmlDom,final String xpathExpression) { 
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodes = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);
			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODESET);
			
			
			nodes = (NodeList) result;
			
			
		} catch (XPathExpressionException ex) { 
			
			System.err.println("searchForNode() oopss! :" + ex.getMessage());
		}
		
		
		return nodes;
	}
	
	public static void removeLockFileIfExists(){
		File lockFile = new File(System.getProperty(IndexerProperties.LOCK_FILE));
		try
		{
			if(lockFile != null && lockFile.exists())
				lockFile.delete();
		}
		catch(Exception ex)
		{
			System.out.println("CAN'T REMOVE LOCK FILE: " + ex.getMessage());
		}
	}
	

}
