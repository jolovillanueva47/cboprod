package org.cambridge.ebooks.indexer.tools;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XPathWorker {
	
	public NodeList searchForNode(final Node xmlDom,final String xpathExpression) { 
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodes = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);
			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODESET);
			
			
			nodes = (NodeList) result;
			
			
		} catch (XPathExpressionException ex) { 
			
			System.err.println("oopss!" + ex.getMessage());
		}
		
		
		return nodes;
	}
	
	
	
	
//	public static void main (String args[] ) { 
//		 
//		String filePath = "C:\\BOOK_XML\\stahl\\ebook\\meta\\9780511471063\\9780511471063.xml";
////		String xpath =  "/drug/therapeutics/sec[title=\"Commonly Prescribed For\"]/descendant::text()";
////		String xpath =  "/drug/therapeutics/sec[title=\"Commonly Prescribed For\"]/descendant::list[2]/descendant::text()";
//		
//		String xpath = "book/descendant::contributor-group/descendant::contributor";
//		//String xpath = "book/metadata/descendant::author";
//			
//			
//		Document doc = XMLDOMParser.getXmlDOM(filePath);
//		XPathWorker worker = new XPathWorker();
//		
//		System.out.println(doc);
//		
//		NodeList nl = worker.searchForNode(doc, xpath);
//		System.out.println(nl);
//		for (int i = 0; i < nl.getLength(); i++ ) { 
//			
//			Node n  = nl.item(i);
//			System.out.println("[" + (i + 1) + "] node: " + n.getNodeName());
//			//System.out.println("node: " + n.getAttributes().getNamedItem("id").getTextContent());
//			
//			String val = n.getNodeValue();
//			if ( val != null && val.trim().length()> 0 ) 
//				System.out.println(val);
//			
//		}
//		
//	}

}
