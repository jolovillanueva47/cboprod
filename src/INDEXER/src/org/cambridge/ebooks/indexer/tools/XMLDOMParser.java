package org.cambridge.ebooks.indexer.tools;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLDOMParser {
	private static final Logger LOG = Logger.getLogger(XMLDOMParser.class);
	private static DocumentBuilderFactory factory = null;
	static {
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();
				//factory.setIgnoringComments(true);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	//TODO:FIXME!
	public static Document getXmlDOM(String filename) { 
		Document doc = null;
		File file = new File(filename);
		try { 
			
			//DTD location changed to publicly accessible URL. Quick fix is to ignore DTD in Header file.
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			builder.setEntityResolver(new EntityResolver() {
		        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
		            if (systemId.contains("header.dtd")) {
		                return new InputSource(new StringReader(""));
		            } else {
		                return null;
		            }
		        } 
		    });
			doc = builder.parse(file);
			
			//doc = factory.newDocumentBuilder().parse(file);	
					
		} catch (IOException ioe) { 
			LOG.error("Exception... ");
		} catch (Exception exc) { 
			LOG.error(exc.getMessage());
		}
		return doc;
	}	
	
}
