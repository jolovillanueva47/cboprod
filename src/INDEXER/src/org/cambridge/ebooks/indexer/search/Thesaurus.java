/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cambridge.ebooks.indexer.search;

import java.io.FileInputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Thesaurus {
	static Properties table = new Properties();
	
	public static void load(String filename) {
		try {
			FileInputStream propFile = new FileInputStream(filename);
			table.load(propFile);
		} catch (Exception e) {
		}
	}
	
	public static String get(String string) {
		String result = (String)table.get(string);
		if (result == null) {
			if (table.containsValue(string)) {
				Set set = table.entrySet();
				Iterator i = set.iterator();
				while (i.hasNext()) {
					Map.Entry entry = (Map.Entry)i.next();
					if (entry.getValue().equals(string)) 
						result = (String)entry.getKey(); 
				}				
			} else {
				result = "";
			}						
		} 				
		return result;
	}
	
//	public static void main(String args[]) {
//		Date start = new Date();
//		System.out.println("[" + Thesaurus.get("succour") + "]");
//		Date end = new Date();
//		System.out.println((end.getTime() - start.getTime()) + " ms.");		
//	}
}
