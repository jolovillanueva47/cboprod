*** Introduction
This application will create the index database that will be used by the Lucene search engine.


*** Software Requirement
Apache Ant. Download the latest version at http://ant.apache.org/. To install the application you can just
extract the archive to any folder. The website contains information on advanced options and other details.

HTML2Text. Download the latest version at http://userpage.fu-berlin.de/~mbayer/tools/html2text.html. 

XPDF. Download the latest version at http://www.foolabs.com/xpdf.


*** Configuration
The only file to be updated is indexer.properties. Below is the list of the properties together with a short
description and example value.

# Oracle Database driver type
db.drivertype=oci8

# Oracle Database IP address
db.servername=10.251.251.251

# Oracle Database protocol
db.networkprotocol=tcp

# Oracle Database name
db.databasename=Manilla

# Oracle Database port number
db.portnumber=1521

# Oracle Database user name
db.user=oracjo_live

# Oracle Database password
db.password=sirius

# Base folder of the PDF contents. (This folder must exist)
dir.base=Z:/content

# Folder of the Lucene index files. (This folder must exist)
dir.index=C:/data/cjo/index

# Folder where the TXT files will be transfered after the index is build. (This folder must exist)
dir.archive=C:/data/cjo/archive

# Fully qualified path of the Lucene stopwords. Words listed here will be ignored by the indexer.
file.stopwords=C:/eclipse/workspace/index/indexer.stopwords

# Configuration file of HTML2Text.
file.rc=/app/cjo/html2textrc

# Command line of XPDF
convert.pdf=/usr/local/bin/pdftotext

# Command line of HTML2Text
convert.htm=/usr/local/bin/html2text

# Command line of ..... (not implemented yet)
convert.ps=
	    
It is also a requirement that the index folder is existing.


*** Running the application
1. Change directory to the Indexer folder (or where build.xml is located)
2. Execute the ant script within the Indexer folder

	e.g. If ant is installed at /usr/java/apache-ant-1.5.4 execute it as
		 $ /usr/java/apache-ant-1.5.4/bin/ant


#mainproc params
#CONVERT, INDEX, OPTIMISE
#convert=use this if you wanted to run the fulltext convertion to lucene txt files only
#index = use this if you wanted to run index only
#optimise=use this if you wanted to optimise your index documents
#all the trhee param can be combine but in the sequnce listed above.

#Nov10 2007
#Edong Avena
#convert Procedure only uses the class 
#1. ConvertDocuments.java
#2.IndexDocumentsDAO.java

#Index Procedure
#1. FileDocument.java
#2. FiledocumentVO.java
#3.SerializerDAO.java


