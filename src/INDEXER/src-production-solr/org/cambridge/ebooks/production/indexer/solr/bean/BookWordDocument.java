package org.cambridge.ebooks.production.indexer.solr.bean;

import org.apache.solr.client.solrj.beans.Field;

/**
 * @author alacerna
 * 
 * 20131206 - added for word core
 * 
 */


public class BookWordDocument {

	@Field("id")
	private String id;
	
	@Field("book_id")
	private String bookId;
	
	@Field("content_id")
	private String contentId;

	@Field("word_text")
	private String wordText;
	
	@Field("word_url")
	private String wordUrl;
	
	@Field("type")
	private String type;
	
	@Field("toc_id")
	private String tocId;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getWordText() {
		return wordText;
	}

	public void setWordText(String wordText) {
		this.wordText = wordText;
	}

	public String getWordUrl() {
		return wordUrl;
	}

	public void setWordUrl(String wordUrl) {
		this.wordUrl = wordUrl;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setTocId(String tocId) {
		this.tocId = tocId;
	}

	public String getTocId() {
		return tocId;
	}

	
}
