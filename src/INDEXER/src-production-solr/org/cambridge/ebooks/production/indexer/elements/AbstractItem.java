/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class AbstractItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class AbstractItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _p.
     */
    private org.cambridge.ebooks.production.indexer.elements.P _p;

    /**
     * Field _block.
     */
    private org.cambridge.ebooks.production.indexer.elements.Block _block;

    /**
     * Field _list.
     */
    private org.cambridge.ebooks.production.indexer.elements.List _list;


      //----------------/
     //- Constructors -/
    //----------------/

    public AbstractItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'block'.
     * 
     * @return the value of field 'Block'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Block getBlock(
    ) {
        return this._block;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'list'.
     * 
     * @return the value of field 'List'.
     */
    public org.cambridge.ebooks.production.indexer.elements.List getList(
    ) {
        return this._list;
    }

    /**
     * Returns the value of field 'p'.
     * 
     * @return the value of field 'P'.
     */
    public org.cambridge.ebooks.production.indexer.elements.P getP(
    ) {
        return this._p;
    }

    /**
     * Sets the value of field 'block'.
     * 
     * @param block the value of field 'block'.
     */
    public void setBlock(
            final org.cambridge.ebooks.production.indexer.elements.Block block) {
        this._block = block;
        this._choiceValue = block;
    }

    /**
     * Sets the value of field 'list'.
     * 
     * @param list the value of field 'list'.
     */
    public void setList(
            final org.cambridge.ebooks.production.indexer.elements.List list) {
        this._list = list;
        this._choiceValue = list;
    }

    /**
     * Sets the value of field 'p'.
     * 
     * @param p the value of field 'p'.
     */
    public void setP(
            final org.cambridge.ebooks.production.indexer.elements.P p) {
        this._p = p;
        this._choiceValue = p;
    }

}
