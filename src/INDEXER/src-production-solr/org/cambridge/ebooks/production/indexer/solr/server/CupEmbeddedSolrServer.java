package org.cambridge.ebooks.production.indexer.solr.server;

import java.io.File;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.core.CoreContainer;

public class CupEmbeddedSolrServer implements CupSolrServer {
	
	public SolrServer getMultiCore(String solrHome, String coreName, Map<String, String> settings) {
		
		File home = new File(solrHome);
	    File configFile = new File(home, "solr.xml");
	    CoreContainer container = null;
		
	    try 
	    {
			container = new CoreContainer(solrHome, configFile);
		} 
	    catch (Exception e) 
	    {
			System.err.println("[Exception] CupEmbeddedSolrServer.getMultiCore(String, String, Map<String, String>) message: " + e.getMessage());
		} 

	    return new EmbeddedSolrServer(container, coreName);
	}

	public SolrServer getSingleCore(String solrHome, Map<String, String> settings) {
		
		System.setProperty("solr.solr.home", solrHome);
		CoreContainer.Initializer initializer = new CoreContainer.Initializer();
		CoreContainer coreContainer = new CoreContainer();
		
		try 
		{
			coreContainer = initializer.initialize();
		} 
		catch (Exception e) 
		{
			System.err.println("[Exception] CupEmbeddedSolrServer.getSingleCore(String, Map<String, String>) message: " + e.getMessage());
		} 
		
		return new EmbeddedSolrServer(coreContainer, "");
	}		
}
