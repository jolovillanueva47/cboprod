package org.cambridge.ebooks.production.indexer.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.indexer.util.IsbnContentDirUtil;

public class FileUtil {
	
	public static File[] getIsbnsFromFile(String sourceFile){
		ArrayList<String> list = readContent(sourceFile);
		File[] fileArray = new File[list.size()];
		
		for(int x=0; x < list.size(); x++){
			String str = list.get(x);
			if(str.contains("_")){
				try{
					str = str.substring(str.indexOf("_")-13, str.indexOf("_"));
					fileArray[x] = new File(IsbnContentDirUtil.getPath(str));
				} catch (StringIndexOutOfBoundsException ex){
					System.out.println("invalid file name for: "+str);
				}
			}
		}
		
		
		return fileArray;
	}
	
	public static ArrayList<String> readContent(String sourceFile){
		ArrayList<String> list = new ArrayList<String>();
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(sourceFile));
			String str;
			
			while ((str = in.readLine()) != null) {
				System.out.println("read: "+str);
				list.add(str);
			}
			
			in.close();
		} catch (FileNotFoundException e){
			System.out.println("File "+sourceFile+" does not exist.");
			e.getStackTrace();
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return list;
	}
	
	//***IMPORTANT NOTE!! CHANGE "/" to "\\" when testing in local for deleteTxtFile	
	public void deleteTxtFile(String fileToBeDeleted){
		String completeFileName = IsbnContentDirUtil.getPath(
			fileToBeDeleted.subSequence(fileToBeDeleted.indexOf("A")-13, fileToBeDeleted.indexOf("A")).toString())+
			fileToBeDeleted;
		
		File file = new File(completeFileName);
		if(file.delete()){
			System.out.println("successfully deleted "+file.toString());
		} else {
			System.out.println("failed to delete "+file.toString());
		}
	}
	
	public String extractIsbn(String fileName){
		return null;
	}

	public static boolean moveFile(String sourceFile, String destinationFile) {
		boolean success = true;
		try {
			File f1 = new File(sourceFile);
			File f2 = new File(destinationFile + dateTimeNow());
			InputStream in = new FileInputStream(f1);
			
			//proceed only when file is not empty
			if(in.available() > 0){
				// For Append the file.
				// OutputStream out = new FileOutputStream(f2,true);
	
				// For Overwrite the file.
				OutputStream out = new FileOutputStream(f2);
	
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
				// System.out.println("Target File: "+destinationFile+" created.");
				if (f1.delete()) {
					// System.out.println("Source file: "+destinationFile+" deleted.");
					System.out.println("isbnlist.txt archived as "
							+ f2.getAbsolutePath());
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
			success = false;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			success = false;
		}

		return success;
	}
	
	public static String dateTimeNow(){
		Calendar dateNow = Calendar.getInstance();
		String str = String.valueOf(dateNow.get(Calendar.YEAR)) +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MONTH)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.DAY_OF_MONTH)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.HOUR_OF_DAY)), 2, "0") + 
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.MINUTE)), 2, "0") +
			StringUtils.leftPad(String.valueOf(dateNow.get(Calendar.SECOND)), 2, "0");
		
		return str;
	}
	
	public static void copyfile(String srFile, String dtFile) {
		try {
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);

			// For Append the file.
			// OutputStream out = new FileOutputStream(f2,true);

			// For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			// System.out.println("dummy dtd created..");
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
