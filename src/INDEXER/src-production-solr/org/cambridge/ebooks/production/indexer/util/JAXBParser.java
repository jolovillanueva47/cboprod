package org.cambridge.ebooks.production.indexer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;



public class JAXBParser<T> {

	//private static final Logger LOGGER = Logger.getLogger(JAXBParser.class);
	private JAXBContext jaxbContext;
	private Unmarshaller unmarshaller;
	
	public JAXBParser() {
		
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "net.sf.saxon.dom.DocumentBuilderFactoryImpl");
		System.setProperty("javax.xml.xpath.XPathFactory", "net.sf.saxon.xpath.XPathFactoryImpl");
		System.setProperty("javax.xml.parsers.SAXParserFactory", "net.sf.saxon.aelfred.SAXParserFactoryImpl");		
		
		try 
		{
			jaxbContext = JAXBContext.newInstance("org.cambridge.ebooks.production.indexer.elements");
			unmarshaller = jaxbContext.createUnmarshaller();
		} 
		catch(JAXBException jaxbe)
		{
			System.err.println("[JAXBException] JAXBParser()" + jaxbe.getMessage());
		}
		catch(Exception ex)
		{
			System.err.println("[Exception] JAXBParser()" + ex.getMessage());
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public T parse(String xmlDir){
		T t = null;
		try 
		{
			InputStream fis = new FileInputStream(xmlDir);		
			t = (T)unmarshaller.unmarshal(fis);
		} 
		catch (Exception ex) 
		{
			System.err.println("[Exception] JAXBParser.parse(String)" + ex.getMessage());
		}		
		return t ;
	}
	
	@SuppressWarnings("unchecked")
	public T parse(File headerFile){
		T t = null;
		try 
		{
			InputStream fis = new FileInputStream(headerFile);		
			t = (T)unmarshaller.unmarshal(fis);
		} 
		catch (Exception ex) 
		{
			System.err.println("[Exception] JAXBParser.parse(File)" + ex.getMessage());
			//LOGGER.error("[Exception] parse(String) message: " + ex.getMessage());
		}		
		return t ;
	}
}