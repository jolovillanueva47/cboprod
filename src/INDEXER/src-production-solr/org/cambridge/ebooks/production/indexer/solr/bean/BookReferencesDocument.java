package org.cambridge.ebooks.production.indexer.solr.bean;

import org.apache.solr.client.solrj.beans.Field;

/**
 * @author alacerna
 * 
 * 20140303 - added for references core
 * 
 */


public class BookReferencesDocument {
	 
	@Field("id")
	private String id;
	
	@Field("book_id")
	private String book_id;
	
	@Field("chapter_id")
	private String chapter_id;
	
	@Field("order_number")
	private int order_number;
	
	@Field("position")
	private String position;
	
	@Field("data")
	private String data;
	
	@Field("crossRefLink")
	private String crossRefLink;
	
	@Field("crossRefStatus")
	private String crossRefStatus;
	
	@Field("crossRefDoi")
	private String crossRefDoi;
	
	@Field("openURLLink")
	private String openURLLink;
	
	@Field("googleScholarLink")
	private String googleScholarLink;
	
	@Field("googleBooksLink")
	private String googleBooksLink; 
	
	@Field("cambridgeLInk")
	private String cambridgeLInk;
	
	@Field("linkType")
	private String linkType;

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBook_id() {
		return book_id;
	}

	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}

	public String getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(String chapter_id) {
		this.chapter_id = chapter_id;
	}

	public int getOrder_number() {
		return order_number;
	}

	public void setOrder_number(int x) {
		this.order_number = x;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCrossRefLink() {
		return crossRefLink;
	}

	public void setCrossRefLink(String crossRefLink) {
		this.crossRefLink = crossRefLink;
	}

	public String getCrossRefStatus() {
		return crossRefStatus;
	}

	public void setCrossRefStatus(String crossRefStatus) {
		this.crossRefStatus = crossRefStatus;
	}

	public String getCrossRefDoi() {
		return crossRefDoi;
	}

	public void setCrossRefDoi(String crossRefDoi) {
		this.crossRefDoi = crossRefDoi;
	}

	public String getOpenURLLink() {
		return openURLLink;
	}

	public void setOpenURLLink(String openURLLink) {
		this.openURLLink = openURLLink;
	}

	public String getGoogleScholarLink() {
		return googleScholarLink;
	}

	public void setGoogleScholarLink(String googleScholarLink) {
		this.googleScholarLink = googleScholarLink;
	}

	public String getGoogleBooksLink() {
		return googleBooksLink;
	}

	public void setGoogleBooksLink(String googleBooksLink) {
		this.googleBooksLink = googleBooksLink;
	}

	public String getCambridgeLInk() {
		return cambridgeLInk;
	}

	public void setCambridgeLInk(String cambridgeLInk) {
		this.cambridgeLInk = cambridgeLInk;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	
	
	
	
	
	
	
	

}
