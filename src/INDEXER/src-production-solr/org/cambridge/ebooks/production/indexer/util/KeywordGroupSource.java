package org.cambridge.ebooks.production.indexer.util;

/**
 * 20130625 jalvarez
 */ 

public enum KeywordGroupSource {
	
	INDEX("index"),
	THEME("theme"),
	OTHER("other"),
	USE("use"),
	CLASS("class"),
	GENERIC_DRUG("generic-drug"),
	PROPRIETARY_DRUG("proprietary-drug");
	
	private String src;
	
	KeywordGroupSource(String src) {
		this.setSource(src);
	}

	public String getSource() {
		return src;
	}

	public void setSource(String src) {
		this.src = src;
	}

}
