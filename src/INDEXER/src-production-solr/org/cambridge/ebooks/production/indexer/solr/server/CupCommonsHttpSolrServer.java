package org.cambridge.ebooks.production.indexer.solr.server;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;

public class CupCommonsHttpSolrServer implements CupSolrServer {
	
	public SolrServer getMultiCore(String solrHome, String coreName, Map<String, String> settings) {		
		CommonsHttpSolrServer server = null;
		try {
			server = new CommonsHttpSolrServer(solrHome + coreName);
			
			if(null != settings) {			
				
				String socketTimeout = settings.get(CupSolrServer.KEY_SOCKET_TIMEOUT);
				String connTimeout = settings.get(CupSolrServer.KEY_CONN_TIMEOUT);
				String defMaxConnPerHost = settings.get(CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST);
				String maxTotalConn = settings.get(CupSolrServer.KEY_MAX_TOTAL_CONN);
				String followRedirects = settings.get(CupSolrServer.KEY_FOLLOW_REDIRECTS);
				String allowCompression = settings.get(CupSolrServer.KEY_ALLOW_COMPRESSION);
				String maxRetries = settings.get(CupSolrServer.KEY_MAX_RETRIES);
				
				if(StringUtils.isNotEmpty(socketTimeout)) {
					server.setSoTimeout(Integer.parseInt(socketTimeout));
				}
				if(StringUtils.isNotEmpty(connTimeout)) {
					server.setDefaultMaxConnectionsPerHost(Integer.parseInt(connTimeout));
				}
				if(StringUtils.isNotEmpty(defMaxConnPerHost)) {
					server.setDefaultMaxConnectionsPerHost(Integer.parseInt(defMaxConnPerHost));
				}
				if(StringUtils.isNotEmpty(maxTotalConn)) {
					server.setMaxTotalConnections(Integer.parseInt(maxTotalConn));
				}
				if(StringUtils.isNotEmpty(followRedirects)) {
					server.setFollowRedirects(Boolean.parseBoolean(followRedirects));
				}
				if(StringUtils.isNotEmpty(allowCompression)) {
					server.setAllowCompression(Boolean.parseBoolean(allowCompression));
				}
				if(StringUtils.isNotEmpty(maxRetries)) {
					server.setMaxRetries(Integer.parseInt(maxRetries));
				}
			}
			
		} catch (Exception e) {
			System.err.println("[Exception] CupCommonsHttpSolrServer.getMultiCore(String, String, Map<String, String>) message: " + e.getMessage());
		}
		
		return server;
	}

	public SolrServer getSingleCore(String solrHome, Map<String, String> settings) {
		CommonsHttpSolrServer server = null;
		try {
			server = new CommonsHttpSolrServer(solrHome);
			
			if(null != settings) {			
				
				String socketTimeout = settings.get(CupSolrServer.KEY_SOCKET_TIMEOUT);
				String connTimeout = settings.get(CupSolrServer.KEY_CONN_TIMEOUT);
				String defMaxConnPerHost = settings.get(CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST);
				String maxTotalConn = settings.get(CupSolrServer.KEY_MAX_TOTAL_CONN);
				String followRedirects = settings.get(CupSolrServer.KEY_FOLLOW_REDIRECTS);
				String allowCompression = settings.get(CupSolrServer.KEY_ALLOW_COMPRESSION);
				String maxRetries = settings.get(CupSolrServer.KEY_MAX_RETRIES);
				
				if(StringUtils.isNotEmpty(socketTimeout)) {
					server.setSoTimeout(Integer.parseInt(socketTimeout));
				}
				if(StringUtils.isNotEmpty(connTimeout)) {
					server.setDefaultMaxConnectionsPerHost(Integer.parseInt(connTimeout));
				}
				if(StringUtils.isNotEmpty(defMaxConnPerHost)) {
					server.setDefaultMaxConnectionsPerHost(Integer.parseInt(defMaxConnPerHost));
				}
				if(StringUtils.isNotEmpty(maxTotalConn)) {
					server.setMaxTotalConnections(Integer.parseInt(maxTotalConn));
				}
				if(StringUtils.isNotEmpty(followRedirects)) {
					server.setFollowRedirects(Boolean.parseBoolean(followRedirects));
				}
				if(StringUtils.isNotEmpty(allowCompression)) {
					server.setAllowCompression(Boolean.parseBoolean(allowCompression));
				}
				if(StringUtils.isNotEmpty(maxRetries)) {
					server.setMaxRetries(Integer.parseInt(maxRetries));
				}
			}
			
		} catch (Exception e) {
			System.err.println("[Exception] CupCommonsHttpSolrServer.getSingleCore(String, Map<String, String>) message: " + e.getMessage());
		}
		
		return server;
	}
	
}
