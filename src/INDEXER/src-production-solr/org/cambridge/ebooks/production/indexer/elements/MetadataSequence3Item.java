/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class MetadataSequence3Item.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MetadataSequence3Item implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _publisherName.
     */
    private java.lang.String _publisherName;

    /**
     * Field _publisherLoc.
     */
    private java.lang.String _publisherLoc;


      //----------------/
     //- Constructors -/
    //----------------/

    public MetadataSequence3Item() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'publisherLoc'.
     * 
     * @return the value of field 'PublisherLoc'.
     */
    public java.lang.String getPublisherLoc(
    ) {
        return this._publisherLoc;
    }

    /**
     * Returns the value of field 'publisherName'.
     * 
     * @return the value of field 'PublisherName'.
     */
    public java.lang.String getPublisherName(
    ) {
        return this._publisherName;
    }

    /**
     * Sets the value of field 'publisherLoc'.
     * 
     * @param publisherLoc the value of field 'publisherLoc'.
     */
    public void setPublisherLoc(
            final java.lang.String publisherLoc) {
        this._publisherLoc = publisherLoc;
    }

    /**
     * Sets the value of field 'publisherName'.
     * 
     * @param publisherName the value of field 'publisherName'.
     */
    public void setPublisherName(
            final java.lang.String publisherName) {
        this._publisherName = publisherName;
    }

}
