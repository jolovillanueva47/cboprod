package org.cambridge.ebooks.production.indexer;


import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrServer;
import org.cambridge.ebooks.production.indexer.elements.Book;
import org.cambridge.ebooks.production.indexer.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookReferencesDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookWordDocument;
import org.cambridge.ebooks.production.indexer.solr.server.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.production.indexer.solr.server.CupSolrServer;
import org.cambridge.ebooks.production.indexer.util.DocumentBuilder;
import org.cambridge.ebooks.production.indexer.util.IsbnContentDirUtil;
import org.cambridge.ebooks.production.indexer.util.JAXBParser;
import org.cambridge.ebooks.production.properties.ApplicationProperties;
import org.cambridge.ebooks.production.references.ReferenceResolver;
import org.cambridge.ebooks.production.references.elements.References;
import org.cambridge.ebooks.production.references.util.ReferenceProperties;
import org.cambridge.ebooks.production.references.util.StringUtil;

public class ProductionSOLRIndexer {

	private static SolrServer bookCore;
	private static SolrServer contentCore;
	private static SolrServer wordCore;
	private static SolrServer referencesCore;
	
	private CupSolrServer server = new CupCommonsHttpSolrServer();
	private JAXBParser<Book> xmlParser = new JAXBParser<Book>();
	
	public ProductionSOLRIndexer()  {
	
		Map<String, String> serverSettings = new HashMap<String, String>();
		serverSettings.put(CupSolrServer.KEY_SOCKET_TIMEOUT, String.valueOf(ApplicationProperties.SOCKET_TIMEOUT));
		serverSettings.put(CupSolrServer.KEY_CONN_TIMEOUT, String.valueOf(ApplicationProperties.CONNECTION_TIMEOUT));
		serverSettings.put(CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST, String.valueOf(ApplicationProperties.DEFAULT_MAX_CONNECTION_PER_HOST));
		serverSettings.put(CupSolrServer.KEY_MAX_TOTAL_CONN, String.valueOf(ApplicationProperties.MAX_TOTAL_CONNECTIONS));
		serverSettings.put(CupSolrServer.KEY_FOLLOW_REDIRECTS, String.valueOf(ApplicationProperties.FOLLOW_REDIRECTS));
		serverSettings.put(CupSolrServer.KEY_ALLOW_COMPRESSION, String.valueOf(ApplicationProperties.ALLOW_COMPRESSION));
		serverSettings.put(CupSolrServer.KEY_MAX_RETRIES, String.valueOf(ApplicationProperties.MAX_RETRIES));
		
		bookCore = server.getMultiCore(ApplicationProperties.SOLR_HOME_URL, ApplicationProperties.CORE_BOOK, serverSettings);
		contentCore = server.getMultiCore(ApplicationProperties.SOLR_HOME_URL, ApplicationProperties.CORE_CONTENT, serverSettings);
		wordCore = server.getMultiCore(ApplicationProperties.SOLR_HOME_URL, ApplicationProperties.CORE_WORD, serverSettings);
		referencesCore = server.getMultiCore(ApplicationProperties.SOLR_HOME_URL, ApplicationProperties.CORE_REFERENCES, serverSettings);
		
	}
	
	public void optimize(String indexName) {
		try 
		{
			if(ApplicationProperties.CORE_BOOK.equals(indexName)) 
				bookCore.optimize();
			else if(ApplicationProperties.CORE_CONTENT.equals(indexName)) 
				contentCore.optimize();
			else if(ApplicationProperties.CORE_WORD.equals(indexName))
				wordCore.optimize();
			else if(ApplicationProperties.CORE_REFERENCES.equals(indexName))
				referencesCore.optimize();
			else 
			{
				bookCore.optimize();
				contentCore.optimize();
				wordCore.optimize();
				referencesCore.optimize();
			}	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void add(boolean mode, File[] isbnsDir, String... toIndex){		
		Book book = null;
		String indexName = getIndexType(toIndex);
		int docCountToCommit = 0;
		int indexCount = 0;
		
		if(isbnsDir != null && isbnsDir.length > 0)
		{	
			System.out.println("=== ADD/UPDATE ===");
			
			long start = System.currentTimeMillis();
			String indexId = String.valueOf(start);
			int toIndexLength = isbnsDir.length;
			
			System.out.println("Production indexer start. [" + new Date() + "]");
			System.out.println("ID:[" + indexId + "] LENGTH:[" + toIndexLength + "]");
			System.out.println(Arrays.asList(isbnsDir).toString().replace("[", "").replace("]", "").replaceAll(",", "\n").replace(" ", ""));
			System.out.println("-----------------------------------------------------");
			for(File headerDir : isbnsDir)
			{
				File toDelete = null;
				try
				{
					String isbn = headerDir.getAbsolutePath().toString().substring(headerDir.getAbsolutePath().toString().length()-17, headerDir.getAbsolutePath().toString().length()).replaceAll("\\\\", "").replaceAll("/", "");
					System.out.println(isbn);
					
					String dummyDTD = IsbnContentDirUtil.getPath(isbn).substring(0, IsbnContentDirUtil.getPath(isbn).length() - 2)+"header.dtd";
					StringUtil.copyfile(ReferenceProperties.CONTENT_DIR+"header.dtd", dummyDTD);
					toDelete = new File (dummyDTD);
					
					File headerFile = getHeaderFileFromDirectory(headerDir);
					
					book = xmlParser.parse(headerFile);
					
					if(book != null)
					{
						System.out.println("Processing ["+book.getMetadata().getIsbn()+"]");
						delete(book, indexName);

						if(ApplicationProperties.CORE_BOOK.equals(indexName)) 
						{
							BookMetadataDocument metadataDocument = DocumentBuilder.generateBookMetadata(book);
							bookCore.addBean(metadataDocument);
							
							indexCount++;
							docCountToCommit++;
						}
						else if(ApplicationProperties.CORE_CONTENT.equals(indexName))
						{
							List<BookContentItemDocument> contentDocument = DocumentBuilder.generateContentItemData(book);
							contentCore.addBeans(contentDocument);
							
							indexCount++;
							docCountToCommit++;
						}
						else if(ApplicationProperties.CORE_WORD.equals(indexName))
						{
							List<BookWordDocument> wordDocument = DocumentBuilder.generateWordData(book);
								wordCore.addBeans(wordDocument);
								indexCount++;
								docCountToCommit++;
							
						}
						else
						{
							BookMetadataDocument metadataDocument = DocumentBuilder.generateBookMetadata(book);
							bookCore.addBean(metadataDocument);

							List<BookContentItemDocument> contentDocument = DocumentBuilder.generateContentItemData(book);
							contentCore.addBeans(contentDocument);
							
							List<BookWordDocument> wordDocument = DocumentBuilder.generateWordData(book);
							if(! wordDocument.isEmpty())
								wordCore.addBeans(wordDocument);
							
							indexCount++;
							docCountToCommit++;
						}
						
						
						if(docCountToCommit > (ApplicationProperties.MAX_DOC_COMMIT - 1))
						{
							docCountToCommit = 0;
							if(ApplicationProperties.CORE_BOOK.equals(indexName)) 
								bookCore.commit();		
							else if(ApplicationProperties.CORE_CONTENT.equals(indexName)) 
								contentCore.commit();
							else if(ApplicationProperties.CORE_WORD.equals(indexName))
								wordCore.commit();
							else 
							{
								bookCore.commit();
								contentCore.commit();
								wordCore.commit();
							}
						}
					}
					else
					{
						System.out.println("ID:["+ indexId + "] [PARSE ERROR] ProductionSOLRIndexer.add(File, String...) : [ " + headerFile.getName() + " ] ");
					}
					
					if(mode){
						String[] data = {"create", book.getMetadata().getIsbn()};
						ReferenceResolver.main(data);
					}
				}
				catch(Exception ex)
				{
					System.out.println("ID:["+ indexId + "] [Exception] ProductionSOLRIndexer.add(File, String...) " + ex.getMessage() + " File: [" + headerDir + " ]");
					ex.printStackTrace();
				} finally {
					toDelete.delete();
				}
			}
			
			
			
			try 
			{
				if(docCountToCommit > 0) 
				{
					if(ApplicationProperties.CORE_BOOK.equals(indexName))
						bookCore.commit();		
					else if(ApplicationProperties.CORE_CONTENT.equals(indexName)) 
						contentCore.commit();
					else if(ApplicationProperties.CORE_WORD.equals(indexName))
						wordCore.commit();
					else 
					{
						bookCore.commit();		
						contentCore.commit();
						wordCore.commit();
					}
				}
			} 
			catch (Exception e) 
			{
				System.out.println("ID:["+ indexId + "] [Exception]" + e.getMessage());
			}
			int totalIndexed = indexCount++;
			System.out.println("\nID:["+ indexId + "] Number of books to index: " + toIndexLength +  " | Number of books indexed: " + totalIndexed);
			
			long end = System.currentTimeMillis();
			double done = (end - start) * 0.001;
			System.out.println("\nID:["+ indexId + "] Production indexer done. [" + new Date() + "]");
			System.out.println("ID:["+ indexId + "] Total indexing time. [" + done/60 + "] mins.");		
		}		
	}
	private void deleteRef(String bookId, String indexName) {
		indexName = indexName != null ? indexName : "";
		int docCountToCommit = 0;
		if(bookId != null && bookId.length() > 0){
			try{	
					if(ApplicationProperties.CORE_REFERENCES.equals(indexName)) 
					{					
						referencesCore.deleteByQuery("book_id:" + bookId);
						docCountToCommit++;
					}	 
					else
					{
						referencesCore.deleteByQuery("book_id:" + bookId);	
						docCountToCommit++;
					}	
					if(docCountToCommit > 0) 
					{
						if(ApplicationProperties.CORE_REFERENCES.equals(indexName)) 
							referencesCore.commit();		
						else
						{
							referencesCore.commit();
						}
					}			
			}
			catch (Exception e) 
			{
				System.err.println("[Exception] ProductionSOLRIndexer.delete(String, String) : " + e.getMessage() + " ISBN: [ " + bookId + " ]");
			} 			
		}		
	}
	
	public void delete(Book book, String indexName){	
		indexName = indexName != null ? indexName : "";
		int docCountToCommit = 0;

			try 
			{								
					if(book != null)
					{
						
						String bookId = book.getId();
						
						if(ApplicationProperties.CORE_BOOK.equals(indexName)) 
						{					
							bookCore.deleteById(bookId);
							docCountToCommit++;
						}
						else if(ApplicationProperties.CORE_CONTENT.equals(indexName))
						{
							contentCore.deleteByQuery("book_id:" + bookId + " AND content_type:content-item");
							docCountToCommit++;
						}  
						else if(ApplicationProperties.CORE_WORD.equals(indexName))
						{
							wordCore.deleteByQuery("book_id:" + bookId);
							docCountToCommit++;
						}
						else
						{
							bookCore.deleteById(bookId);	
							contentCore.deleteByQuery("book_id:" + bookId + " AND content_type:content-item");
							wordCore.deleteByQuery("book_id:" + bookId);
							docCountToCommit++;
						}
						
						if(docCountToCommit > 0) 
						{
							if(ApplicationProperties.CORE_BOOK.equals(indexName)) 
								bookCore.commit();		
							else if(ApplicationProperties.CORE_CONTENT.equals(indexName))
								contentCore.commit();
							else if(ApplicationProperties.CORE_WORD.equals(indexName))
								wordCore.commit();
							else
							{
								bookCore.commit();
								contentCore.commit();
								wordCore.commit();
							}
						}
					}				
			} 
			catch (Exception e) 
			{
				System.err.println("[Exception] ProductionSOLRIndexer.delete(String, String) : " + e.getMessage() + " ISBN: [ " + book.getId() + " ]");

			} 		
	}
	
	private File getHeaderFileFromDirectory(File indexDir){
		File[] headerFile = indexDir.listFiles(
				new FileFilter() {
					Pattern p = Pattern.compile("^\\d\\d+\\.xml$");
					
					public boolean accept(File f) {		
						return f.isFile() && p.matcher(f.getName()).find();
					}
				});
		
		return (headerFile != null && headerFile.length > 0) ? headerFile[0] : null;
		
	}
	
	private String getIndexType(String[] indexType){
		String result = null;
		if(indexType != null && indexType.length > 0)
			result = indexType[0];
		
		return result;
	}	
	public void indexRef(File[] isbnsDir,List<References> allReferences, String... toIndex) {
		
		String indexName = getIndexType(toIndex);
		int docCountToCommit = 0;
		int indexCount = 0;
		
		
		if(isbnsDir != null && isbnsDir.length > 0)
		{	
			System.out.println("=== ADD/UPDATE ===");
			
			long start = System.currentTimeMillis();
			String indexId = String.valueOf(start);
			int toIndexLength = isbnsDir.length;
			
			System.out.println("Production indexer start. [" + new Date() + "]");
			System.out.println("ID:[" + indexId + "] LENGTH:[" + toIndexLength + "]");
			System.out.println(Arrays.asList(isbnsDir).toString().replace("[", "").replace("]", "").replaceAll(",", "\n").replace(" ", ""));
			System.out.println("-----------------------------------------------------");
			for(File headerDir : isbnsDir)
			{
				try
				{		
					if(allReferences != null)
					{
						
						for(References references : allReferences){
							
							deleteRef(references.getBookId(),indexName);
						
						if(ApplicationProperties.CORE_REFERENCES.equals(indexName)) 
						{
							List<BookReferencesDocument> referencesDocument = DocumentBuilder.generateRefdata(allReferences);
							referencesCore.addBeans(referencesDocument);
							
							indexCount++;
							docCountToCommit++;
						}
						
						else
						{	
							List<BookReferencesDocument>  referencesDocument = DocumentBuilder.generateRefdata(allReferences);
								referencesCore.addBeans(referencesDocument);
							
							indexCount++;
							docCountToCommit++;
						}
						
						
						if(docCountToCommit > (ApplicationProperties.MAX_DOC_COMMIT - 1))
						{
							docCountToCommit = 0;
							if(ApplicationProperties.CORE_REFERENCES.equals(indexName)) 
								referencesCore.commit();		
							else 
							{
								referencesCore.commit();
							}
						}
					}
					
				}
				}
				catch(Exception ex)
				{
					System.out.println("ID:["+ indexId + "] [Exception] ProductionSOLRIndexer.add(File, String...) " + ex.getMessage() + " File: [" + headerDir + " ]");
					ex.printStackTrace();
				} 
			}	
			try 
			{
				if(docCountToCommit > 0) 
				{
					if(ApplicationProperties.CORE_REFERENCES.equals(indexName))
						referencesCore.commit();		
					else 
					{
						referencesCore.commit();
					}
				}
			} 
			catch (Exception e) 
			{
				System.out.println("ID:["+ indexId + "] [Exception]" + e.getMessage());
			}
			
			
			
			int totalIndexed = indexCount++;
			System.out.println("\nID:["+ indexId + "] Number of books to index: " + toIndexLength +  " | Number of books indexed: " + totalIndexed);
			
			long end = System.currentTimeMillis();
			double done = (end - start) * 0.001;
			System.out.println("\nID:["+ indexId + "] Production indexer done. [" + new Date() + "]");
			System.out.println("ID:["+ indexId + "] Total indexing time. [" + done/60 + "] mins.");
			
			
		}
		
	}

}
