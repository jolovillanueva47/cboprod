/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class ForenamesItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class ForenamesItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _italic.
     */
    private org.cambridge.ebooks.production.indexer.elements.Italic _italic;

    /**
     * Field _bold.
     */
    private org.cambridge.ebooks.production.indexer.elements.Bold _bold;

    /**
     * Field _sup.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sup _sup;

    /**
     * Field _sub.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sub _sub;

    /**
     * Field _smallCaps.
     */
    private org.cambridge.ebooks.production.indexer.elements.SmallCaps _smallCaps;


      //----------------/
     //- Constructors -/
    //----------------/

    public ForenamesItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'bold'.
     * 
     * @return the value of field 'Bold'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Bold getBold(
    ) {
        return this._bold;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'italic'.
     * 
     * @return the value of field 'Italic'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Italic getItalic(
    ) {
        return this._italic;
    }

    /**
     * Returns the value of field 'smallCaps'.
     * 
     * @return the value of field 'SmallCaps'.
     */
    public org.cambridge.ebooks.production.indexer.elements.SmallCaps getSmallCaps(
    ) {
        return this._smallCaps;
    }

    /**
     * Returns the value of field 'sub'.
     * 
     * @return the value of field 'Sub'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sub getSub(
    ) {
        return this._sub;
    }

    /**
     * Returns the value of field 'sup'.
     * 
     * @return the value of field 'Sup'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sup getSup(
    ) {
        return this._sup;
    }

    /**
     * Sets the value of field 'bold'.
     * 
     * @param bold the value of field 'bold'.
     */
    public void setBold(
            final org.cambridge.ebooks.production.indexer.elements.Bold bold) {
        this._bold = bold;
        this._choiceValue = bold;
    }

    /**
     * Sets the value of field 'italic'.
     * 
     * @param italic the value of field 'italic'.
     */
    public void setItalic(
            final org.cambridge.ebooks.production.indexer.elements.Italic italic) {
        this._italic = italic;
        this._choiceValue = italic;
    }

    /**
     * Sets the value of field 'smallCaps'.
     * 
     * @param smallCaps the value of field 'smallCaps'.
     */
    public void setSmallCaps(
            final org.cambridge.ebooks.production.indexer.elements.SmallCaps smallCaps) {
        this._smallCaps = smallCaps;
        this._choiceValue = smallCaps;
    }

    /**
     * Sets the value of field 'sub'.
     * 
     * @param sub the value of field 'sub'.
     */
    public void setSub(
            final org.cambridge.ebooks.production.indexer.elements.Sub sub) {
        this._sub = sub;
        this._choiceValue = sub;
    }

    /**
     * Sets the value of field 'sup'.
     * 
     * @param sup the value of field 'sup'.
     */
    public void setSup(
            final org.cambridge.ebooks.production.indexer.elements.Sup sup) {
        this._sup = sup;
        this._choiceValue = sup;
    }

}
