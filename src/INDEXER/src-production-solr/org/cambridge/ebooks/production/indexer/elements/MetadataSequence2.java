/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class MetadataSequence2.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MetadataSequence2 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _doi.
     */
    private java.lang.String _doi;

    /**
     * Field _altDoi.
     */
    private org.cambridge.ebooks.production.indexer.elements.AltDoi _altDoi;


      //----------------/
     //- Constructors -/
    //----------------/

    public MetadataSequence2() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'altDoi'.
     * 
     * @return the value of field 'AltDoi'.
     */
    public org.cambridge.ebooks.production.indexer.elements.AltDoi getAltDoi(
    ) {
        return this._altDoi;
    }

    /**
     * Returns the value of field 'doi'.
     * 
     * @return the value of field 'Doi'.
     */
    public java.lang.String getDoi(
    ) {
        return this._doi;
    }

    /**
     * Sets the value of field 'altDoi'.
     * 
     * @param altDoi the value of field 'altDoi'.
     */
    public void setAltDoi(
            final org.cambridge.ebooks.production.indexer.elements.AltDoi altDoi) {
        this._altDoi = altDoi;
    }

    /**
     * Sets the value of field 'doi'.
     * 
     * @param doi the value of field 'doi'.
     */
    public void setDoi(
            final java.lang.String doi) {
        this._doi = doi;
    }

}
