package org.cambridge.ebooks.production.indexer.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

public class BookContentItemDocument {
	
	@Field("id")
	private String id;
	
	@Field("parent_id")
	private String parentId;
	
	@Field("book_id")
	private String bookId;
	
	@Field("content_type")
	private String contentType;
	
	@Field("isbn")
	private String isbn;
	
	
	@Field("content_id")
	private String contentId;
	
	@Field("page_start")
	private String pageStart;
	
	@Field("page_end")
	private String pageEnd;
	
	@Field("type")
	private String type;
	
	@Field("position")
	private String position;
	
	
	
	@Field("pdf_filename")
	private String pdfFilename;
	
	@Field("html_filename")
	private String htmlFilename;
	
	@Field("doi")
	private String doi;
	
	
	
	@Field("label")
	private String label;
	
	@Field("title")
	private String title;
	
	@Field("title_alphasort")
	private String titleAlphasort;
	
	@Field("subtitle")
	private String subtitle;	
	
	
	
	@Field("abstract_alt_filename")
	private String abstractFilename;
	
	@Field("abstract_problem")
	private String abstractProblem;
	
	@Field("keyword_theme")
	private List<String> keywordThemeList;
	
	@Field("keyword_text")
	private List<String> keywordTextList;
	
	
	
	@Field("toc_text")
	private String tocText;
	
	@Field("nav-file_filename")
	private List<String> navFileFilename;
	
	@Field("nav-file_filename_rid")
	private List<String> navFileFilenameRid;
	
	@Field("author_name")
	private List<String> authorNameList;
	
	@Field("author_name_lf")
	private List<String> authorNameLfList;
	
	@Field("author_affiliation")
	private List<String> authorAffiliationList;
	
	@Field("author_name_alphasort")
	private List<String> authorNameAlphasortList;
	
	@Field("author_role")
	private List<String> authorRoleList;
	
	@Field("author_position")
	private List<String> authorPositionList;

	@Field("reference_title")
	private String referenceTitle;
	
	@Field("reference_type")
	private String referenceType;
	
	@Field("insert_filename")
	private List<String> insertFilenameList;
	
	@Field("insert_id")
	private List<String> insertIdList;
	
	@Field("insert_title")
	private List<String> insertTitleList;
	
	@Field("insert_title_alphasort")
	private List<String> insertTitleAlphasortList;
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleAlphasort() {
		return titleAlphasort;
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getAbstractFilename() {
		return abstractFilename;
	}

	public void setAbstractFilename(String abstractFilename) {
		this.abstractFilename = abstractFilename;
	}

	public String getAbstractProblem() {
		return abstractProblem;
	}

	public void setAbstractProblem(String abstractProblem) {
		this.abstractProblem = abstractProblem;
	}

	public List<String> getKeywordTextList() {
		return keywordTextList;
	}

	public void setKeywordTextList(List<String> keywordTextList) {
		this.keywordTextList = keywordTextList;
	}

	public List<String> getKeywordThemeList() {
		return keywordThemeList;
	}

	public void setKeywordThemeList(List<String> keywordThemeList) {
		this.keywordThemeList = keywordThemeList;
	}

	public String getTocText() {
		return tocText;
	}

	public void setTocText(String tocText) {
		this.tocText = tocText;
	}

	public List<String> getAuthorNameList() {
		return authorNameList;
	}

	public void setAuthorNameList(List<String> authorNameList) {
		this.authorNameList = authorNameList;
	}

	public List<String> getAuthorAffiliationList() {
		return authorAffiliationList;
	}

	public void setAuthorAffiliationList(List<String> authorAffiliationList) {
		this.authorAffiliationList = authorAffiliationList;
	}

	public List<String> getAuthorRoleList() {
		return authorRoleList;
	}

	public void setAuthorRoleList(List<String> authorRoleList) {
		this.authorRoleList = authorRoleList;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public List<String> getAuthorNameAlphasortList() {
		return authorNameAlphasortList;
	}

	public void setAuthorNameAlphasortList(List<String> authorNameAlphasortList) {
		this.authorNameAlphasortList = authorNameAlphasortList;
	}

	public List<String> getAuthorPositionList() {
		return authorPositionList;
	}

	public void setAuthorPositionList(List<String> authorPositionList) {
		this.authorPositionList = authorPositionList;
	}

	public String getReferenceTitle() {
		return referenceTitle;
	}

	public void setReferenceTitle(String referenceTitle) {
		this.referenceTitle = referenceTitle;
	}

	public String getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	public List<String> getInsertFilenameList() {
		return insertFilenameList;
	}

	public void setInsertFilenameList(List<String> insertFilenameList) {
		this.insertFilenameList = insertFilenameList;
	}

	public List<String> getInsertIdList() {
		return insertIdList;
	}

	public void setInsertIdList(List<String> insertIdList) {
		this.insertIdList = insertIdList;
	}

	public List<String> getInsertTitleList() {
		return insertTitleList;
	}

	public void setInsertTitleList(List<String> insertTitleList) {
		this.insertTitleList = insertTitleList;
	}

	public List<String> getInsertTitleAlphasortList() {
		return insertTitleAlphasortList;
	}

	public void setInsertTitleAlphasortList(List<String> insertTitleAlphasortList) {
		this.insertTitleAlphasortList = insertTitleAlphasortList;
	}

	public void setAuthorNameLfList(List<String> authorNameLfList) {
		this.authorNameLfList = authorNameLfList;
	}

	public List<String> getAuthorNameLfList() {
		return authorNameLfList;
	}

	public void setHtmlFilename(String htmlFilename) {
		this.htmlFilename = htmlFilename;
	}

	public String getHtmlFilename() {
		return htmlFilename;
	}

	public void setNavFileFilename(List<String> navFileFilename) {
		this.navFileFilename = navFileFilename;
	}

	public List<String> getNavFileFilename() {
		return navFileFilename;
	}

	public void setNavFileFilenameRid(List<String> navFileFilenameRid) {
		this.navFileFilenameRid = navFileFilenameRid;
	}

	public List<String> getNavFileFilenameRid() {
		return navFileFilenameRid;
	}
	

}
