package org.cambridge.ebooks.production.indexer.main;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;

import org.cambridge.ebooks.production.indexer.ProductionSOLRIndexer;
import org.cambridge.ebooks.production.indexer.util.FileUtil;
import org.cambridge.ebooks.production.properties.ApplicationProperties;
import org.cambridge.ebooks.production.indexer.util.IsbnContentDirUtil;

public class Main {

	/**
	 * @param args
	 */
	private static final String MANIFEST_FILENAME_W = ApplicationProperties.DIR_HEADER_FILES  + "MANIFEST.txt";
	private static final String DIR_HEADER_FILES = ApplicationProperties.DIR_HEADER_FILES;
	private static final String LOCK_FILE = ApplicationProperties.DIR_APP_EBOOKS + "lockfile";
	private static String isbn;
	private static boolean mode;
	
	public static void main(String[] args) {
		String manifestContent = String.valueOf(System.currentTimeMillis());
		
		processArgs(args);
		
		index();
		putManifest(manifestContent);
		FileUtil.moveFile(ApplicationProperties.SOURCE_FOLDER + ApplicationProperties.INPUT_FILE, 
				ApplicationProperties.ARCHIVE_FOLDER + ApplicationProperties.INPUT_FILE);
		
		removeLockFile();
	}

	
	private static void index(){
		File[] isbnsDir = null;
		ProductionSOLRIndexer p = new ProductionSOLRIndexer();
		
		String type = isbn;
		
		if(type != null && "ALL".equals(type)) {
			isbnsDir = getHeaderFileDirectoryList(new File(DIR_HEADER_FILES));
		} else if(type != null && (type.startsWith("978") || type.startsWith("555"))) { 
			isbnsDir = new File[]{ new File(DIR_HEADER_FILES + type) };
		} else {
			isbnsDir = FileUtil.getIsbnsFromFile(ApplicationProperties.SOURCE_FOLDER + ApplicationProperties.INPUT_FILE);
		}
		
		try {
			//change isbn dirs to use new content folder structure
			for(int x=0 ; x < isbnsDir.length ; x++){
				File isbnFile = isbnsDir[x];
				if(! isbnFile.exists()){
					String isbnStr = isbnFile.toString();
					isbnsDir[x] = new File(IsbnContentDirUtil.getPath(isbnStr.substring(isbnStr.indexOf("978"), isbnStr.length())));
				}
			}
			p.add(mode, isbnsDir);
		} catch (Exception e){
			System.out.println("Caught Error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private static void processArgs (String[] args){
		mode = true;
		isbn = "";
		if(args.length > 0){
			for(String str:args){
				if (str.equalsIgnoreCase("createoff")){
					mode = false;
				} else if(str.startsWith("978") || str.startsWith("555")){
					isbn = str;
				} else if(str.equalsIgnoreCase("all")){
					isbn = str;
				}
			}
		}
	}
	
	private static void putManifest(String manifest) {
		try 
		{
			FileWriter fw = new FileWriter(MANIFEST_FILENAME_W);
			fw.write(manifest); 	
			fw.close();
		} 
		catch (Exception e) 
		{
			System.err.println("[Exception] Main.putManifest() : " + e.getMessage());
		}
	}
	
	private static File[] getHeaderFileDirectoryList(File indexDir){
		File[] headerFiles = indexDir.listFiles(
				new FileFilter() {
						public boolean accept(File f) {		
							return f.isDirectory();
						}
				});
		
		return headerFiles;
		
	}
	
	private static void removeLockFile(){
		final File lock = new File(LOCK_FILE);
		if(lock.exists())
		{
			try
			{
				if(!lock.delete())
					System.err.println(LOCK_FILE + " was not deleted!");
				
			}
			catch(Exception ex)
			{
				System.err.println(LOCK_FILE + " was not deleted! message: " + ex.getMessage());
			}
		}		
	}
}
