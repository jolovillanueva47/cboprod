/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class CitationItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CitationItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _italic.
     */
    private org.cambridge.ebooks.production.indexer.elements.Italic _italic;

    /**
     * Field _bold.
     */
    private org.cambridge.ebooks.production.indexer.elements.Bold _bold;

    /**
     * Field _underline.
     */
    private org.cambridge.ebooks.production.indexer.elements.Underline _underline;

    /**
     * Field _uri.
     */
    private org.cambridge.ebooks.production.indexer.elements.Uri _uri;

    /**
     * Field _sup.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sup _sup;

    /**
     * Field _sub.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sub _sub;

    /**
     * Field _smallCaps.
     */
    private org.cambridge.ebooks.production.indexer.elements.SmallCaps _smallCaps;

    /**
     * Field _bookTitle.
     */
    private org.cambridge.ebooks.production.indexer.elements.BookTitle _bookTitle;

    /**
     * Field _journalTitle.
     */
    private org.cambridge.ebooks.production.indexer.elements.JournalTitle _journalTitle;

    /**
     * Field _chapterTitle.
     */
    private org.cambridge.ebooks.production.indexer.elements.ChapterTitle _chapterTitle;

    /**
     * Field _volume.
     */
    private java.lang.String _volume;

    /**
     * Field _issue.
     */
    private java.lang.String _issue;

    /**
     * Field _articleTitle.
     */
    private org.cambridge.ebooks.production.indexer.elements.ArticleTitle _articleTitle;

    /**
     * Field _startpage.
     */
    private java.lang.String _startpage;

    /**
     * Field _author.
     */
    private org.cambridge.ebooks.production.indexer.elements.Author _author;

    /**
     * Field _publisherName.
     */
    private java.lang.String _publisherName;

    /**
     * Field _publisherLoc.
     */
    private java.lang.String _publisherLoc;

    /**
     * Field _year.
     */
    private java.lang.String _year;


      //----------------/
     //- Constructors -/
    //----------------/

    public CitationItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'articleTitle'.
     * 
     * @return the value of field 'ArticleTitle'.
     */
    public org.cambridge.ebooks.production.indexer.elements.ArticleTitle getArticleTitle(
    ) {
        return this._articleTitle;
    }

    /**
     * Returns the value of field 'author'.
     * 
     * @return the value of field 'Author'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Author getAuthor(
    ) {
        return this._author;
    }

    /**
     * Returns the value of field 'bold'.
     * 
     * @return the value of field 'Bold'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Bold getBold(
    ) {
        return this._bold;
    }

    /**
     * Returns the value of field 'bookTitle'.
     * 
     * @return the value of field 'BookTitle'.
     */
    public org.cambridge.ebooks.production.indexer.elements.BookTitle getBookTitle(
    ) {
        return this._bookTitle;
    }

    /**
     * Returns the value of field 'chapterTitle'.
     * 
     * @return the value of field 'ChapterTitle'.
     */
    public org.cambridge.ebooks.production.indexer.elements.ChapterTitle getChapterTitle(
    ) {
        return this._chapterTitle;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'issue'.
     * 
     * @return the value of field 'Issue'.
     */
    public java.lang.String getIssue(
    ) {
        return this._issue;
    }

    /**
     * Returns the value of field 'italic'.
     * 
     * @return the value of field 'Italic'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Italic getItalic(
    ) {
        return this._italic;
    }

    /**
     * Returns the value of field 'journalTitle'.
     * 
     * @return the value of field 'JournalTitle'.
     */
    public org.cambridge.ebooks.production.indexer.elements.JournalTitle getJournalTitle(
    ) {
        return this._journalTitle;
    }

    /**
     * Returns the value of field 'publisherLoc'.
     * 
     * @return the value of field 'PublisherLoc'.
     */
    public java.lang.String getPublisherLoc(
    ) {
        return this._publisherLoc;
    }

    /**
     * Returns the value of field 'publisherName'.
     * 
     * @return the value of field 'PublisherName'.
     */
    public java.lang.String getPublisherName(
    ) {
        return this._publisherName;
    }

    /**
     * Returns the value of field 'smallCaps'.
     * 
     * @return the value of field 'SmallCaps'.
     */
    public org.cambridge.ebooks.production.indexer.elements.SmallCaps getSmallCaps(
    ) {
        return this._smallCaps;
    }

    /**
     * Returns the value of field 'startpage'.
     * 
     * @return the value of field 'Startpage'.
     */
    public java.lang.String getStartpage(
    ) {
        return this._startpage;
    }

    /**
     * Returns the value of field 'sub'.
     * 
     * @return the value of field 'Sub'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sub getSub(
    ) {
        return this._sub;
    }

    /**
     * Returns the value of field 'sup'.
     * 
     * @return the value of field 'Sup'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sup getSup(
    ) {
        return this._sup;
    }

    /**
     * Returns the value of field 'underline'.
     * 
     * @return the value of field 'Underline'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Underline getUnderline(
    ) {
        return this._underline;
    }

    /**
     * Returns the value of field 'uri'.
     * 
     * @return the value of field 'Uri'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Uri getUri(
    ) {
        return this._uri;
    }

    /**
     * Returns the value of field 'volume'.
     * 
     * @return the value of field 'Volume'.
     */
    public java.lang.String getVolume(
    ) {
        return this._volume;
    }

    /**
     * Returns the value of field 'year'.
     * 
     * @return the value of field 'Year'.
     */
    public java.lang.String getYear(
    ) {
        return this._year;
    }

    /**
     * Sets the value of field 'articleTitle'.
     * 
     * @param articleTitle the value of field 'articleTitle'.
     */
    public void setArticleTitle(
            final org.cambridge.ebooks.production.indexer.elements.ArticleTitle articleTitle) {
        this._articleTitle = articleTitle;
        this._choiceValue = articleTitle;
    }

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(
            final org.cambridge.ebooks.production.indexer.elements.Author author) {
        this._author = author;
        this._choiceValue = author;
    }

    /**
     * Sets the value of field 'bold'.
     * 
     * @param bold the value of field 'bold'.
     */
    public void setBold(
            final org.cambridge.ebooks.production.indexer.elements.Bold bold) {
        this._bold = bold;
        this._choiceValue = bold;
    }

    /**
     * Sets the value of field 'bookTitle'.
     * 
     * @param bookTitle the value of field 'bookTitle'.
     */
    public void setBookTitle(
            final org.cambridge.ebooks.production.indexer.elements.BookTitle bookTitle) {
        this._bookTitle = bookTitle;
        this._choiceValue = bookTitle;
    }

    /**
     * Sets the value of field 'chapterTitle'.
     * 
     * @param chapterTitle the value of field 'chapterTitle'.
     */
    public void setChapterTitle(
            final org.cambridge.ebooks.production.indexer.elements.ChapterTitle chapterTitle) {
        this._chapterTitle = chapterTitle;
        this._choiceValue = chapterTitle;
    }

    /**
     * Sets the value of field 'issue'.
     * 
     * @param issue the value of field 'issue'.
     */
    public void setIssue(
            final java.lang.String issue) {
        this._issue = issue;
        this._choiceValue = issue;
    }

    /**
     * Sets the value of field 'italic'.
     * 
     * @param italic the value of field 'italic'.
     */
    public void setItalic(
            final org.cambridge.ebooks.production.indexer.elements.Italic italic) {
        this._italic = italic;
        this._choiceValue = italic;
    }

    /**
     * Sets the value of field 'journalTitle'.
     * 
     * @param journalTitle the value of field 'journalTitle'.
     */
    public void setJournalTitle(
            final org.cambridge.ebooks.production.indexer.elements.JournalTitle journalTitle) {
        this._journalTitle = journalTitle;
        this._choiceValue = journalTitle;
    }

    /**
     * Sets the value of field 'publisherLoc'.
     * 
     * @param publisherLoc the value of field 'publisherLoc'.
     */
    public void setPublisherLoc(
            final java.lang.String publisherLoc) {
        this._publisherLoc = publisherLoc;
        this._choiceValue = publisherLoc;
    }

    /**
     * Sets the value of field 'publisherName'.
     * 
     * @param publisherName the value of field 'publisherName'.
     */
    public void setPublisherName(
            final java.lang.String publisherName) {
        this._publisherName = publisherName;
        this._choiceValue = publisherName;
    }

    /**
     * Sets the value of field 'smallCaps'.
     * 
     * @param smallCaps the value of field 'smallCaps'.
     */
    public void setSmallCaps(
            final org.cambridge.ebooks.production.indexer.elements.SmallCaps smallCaps) {
        this._smallCaps = smallCaps;
        this._choiceValue = smallCaps;
    }

    /**
     * Sets the value of field 'startpage'.
     * 
     * @param startpage the value of field 'startpage'.
     */
    public void setStartpage(
            final java.lang.String startpage) {
        this._startpage = startpage;
        this._choiceValue = startpage;
    }

    /**
     * Sets the value of field 'sub'.
     * 
     * @param sub the value of field 'sub'.
     */
    public void setSub(
            final org.cambridge.ebooks.production.indexer.elements.Sub sub) {
        this._sub = sub;
        this._choiceValue = sub;
    }

    /**
     * Sets the value of field 'sup'.
     * 
     * @param sup the value of field 'sup'.
     */
    public void setSup(
            final org.cambridge.ebooks.production.indexer.elements.Sup sup) {
        this._sup = sup;
        this._choiceValue = sup;
    }

    /**
     * Sets the value of field 'underline'.
     * 
     * @param underline the value of field 'underline'.
     */
    public void setUnderline(
            final org.cambridge.ebooks.production.indexer.elements.Underline underline) {
        this._underline = underline;
        this._choiceValue = underline;
    }

    /**
     * Sets the value of field 'uri'.
     * 
     * @param uri the value of field 'uri'.
     */
    public void setUri(
            final org.cambridge.ebooks.production.indexer.elements.Uri uri) {
        this._uri = uri;
        this._choiceValue = uri;
    }

    /**
     * Sets the value of field 'volume'.
     * 
     * @param volume the value of field 'volume'.
     */
    public void setVolume(
            final java.lang.String volume) {
        this._volume = volume;
        this._choiceValue = volume;
    }

    /**
     * Sets the value of field 'year'.
     * 
     * @param year the value of field 'year'.
     */
    public void setYear(
            final java.lang.String year) {
        this._year = year;
        this._choiceValue = year;
    }

}
