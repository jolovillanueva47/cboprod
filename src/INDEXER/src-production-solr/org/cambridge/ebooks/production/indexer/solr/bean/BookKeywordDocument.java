package org.cambridge.ebooks.production.indexer.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

/**
 * 20130625 jalvarez
 */ 

public class BookKeywordDocument {
	
	// surrogate key
	@Field("id")
	private String id;
	
	@Field("book_id")
	private String bookId;
	
	@Field("content_id")
	private String contentId;
	
	@Field("xml_filename")
	private String xmlFilename;	
	
	@Field("keyword_group_src")
	private String keywordGroupSrc;
	
	@Field("keyword_text")
	private List<String> keywordTextList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getKeywordGroupSrc() {
		return keywordGroupSrc;
	}

	public void setKeywordGroupSrc(String keywordGroupSrc) {
		this.keywordGroupSrc = keywordGroupSrc;
	}

	public List<String> getKeywordTextList() {
		return keywordTextList;
	}

	public void setKeywordTextList(List<String> keywordTextList) {
		this.keywordTextList = keywordTextList;
	}

	public String getXmlFilename() {
		return xmlFilename;
	}

	public void setXmlFilename(String xmlFilename) {
		this.xmlFilename = xmlFilename;
	}

}
