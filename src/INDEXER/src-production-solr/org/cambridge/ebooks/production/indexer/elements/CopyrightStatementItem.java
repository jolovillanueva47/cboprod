/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class CopyrightStatementItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CopyrightStatementItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _italic.
     */
    private org.cambridge.ebooks.production.indexer.elements.Italic _italic;

    /**
     * Field _copyrightHolder.
     */
    private org.cambridge.ebooks.production.indexer.elements.CopyrightHolder _copyrightHolder;

    /**
     * Field _copyrightDate.
     */
    private java.lang.String _copyrightDate;


      //----------------/
     //- Constructors -/
    //----------------/

    public CopyrightStatementItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'copyrightDate'.
     * 
     * @return the value of field 'CopyrightDate'.
     */
    public java.lang.String getCopyrightDate(
    ) {
        return this._copyrightDate;
    }

    /**
     * Returns the value of field 'copyrightHolder'.
     * 
     * @return the value of field 'CopyrightHolder'.
     */
    public org.cambridge.ebooks.production.indexer.elements.CopyrightHolder getCopyrightHolder(
    ) {
        return this._copyrightHolder;
    }

    /**
     * Returns the value of field 'italic'.
     * 
     * @return the value of field 'Italic'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Italic getItalic(
    ) {
        return this._italic;
    }

    /**
     * Sets the value of field 'copyrightDate'.
     * 
     * @param copyrightDate the value of field 'copyrightDate'.
     */
    public void setCopyrightDate(
            final java.lang.String copyrightDate) {
        this._copyrightDate = copyrightDate;
        this._choiceValue = copyrightDate;
    }

    /**
     * Sets the value of field 'copyrightHolder'.
     * 
     * @param copyrightHolder the value of field 'copyrightHolder'.
     */
    public void setCopyrightHolder(
            final org.cambridge.ebooks.production.indexer.elements.CopyrightHolder copyrightHolder) {
        this._copyrightHolder = copyrightHolder;
        this._choiceValue = copyrightHolder;
    }

    /**
     * Sets the value of field 'italic'.
     * 
     * @param italic the value of field 'italic'.
     */
    public void setItalic(
            final org.cambridge.ebooks.production.indexer.elements.Italic italic) {
        this._italic = italic;
        this._choiceValue = italic;
    }

}
