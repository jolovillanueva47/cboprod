/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class MetadataSequence3.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MetadataSequence3 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _items.
     */
    private java.util.Vector<org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item> _items;


      //----------------/
     //- Constructors -/
    //----------------/

    public MetadataSequence3() {
        super();
        this._items = new java.util.Vector<org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vMetadataSequence3Item
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMetadataSequence3Item(
            final org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item vMetadataSequence3Item)
    throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vMetadataSequence3Item);
    }

    /**
     * 
     * 
     * @param index
     * @param vMetadataSequence3Item
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMetadataSequence3Item(
            final int index,
            final org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item vMetadataSequence3Item)
    throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vMetadataSequence3Item);
    }

    /**
     * Method enumerateMetadataSequence3Item.
     * 
     * @return an Enumeration over all
     * org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item
     * elements
     */
    public java.util.Enumeration<? extends org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item> enumerateMetadataSequence3Item(
    ) {
        return this._items.elements();
    }

    /**
     * Method getMetadataSequence3Item.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item
     * at the given index
     */
    public org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item getMetadataSequence3Item(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getMetadataSequence3Item: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return (org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item) _items.get(index);
    }

    /**
     * Method getMetadataSequence3Item.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item[] getMetadataSequence3Item(
    ) {
        org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item[] array = new org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item[0];
        return (org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item[]) this._items.toArray(array);
    }

    /**
     * Method getMetadataSequence3ItemCount.
     * 
     * @return the size of this collection
     */
    public int getMetadataSequence3ItemCount(
    ) {
        return this._items.size();
    }

    /**
     */
    public void removeAllMetadataSequence3Item(
    ) {
        this._items.clear();
    }

    /**
     * Method removeMetadataSequence3Item.
     * 
     * @param vMetadataSequence3Item
     * @return true if the object was removed from the collection.
     */
    public boolean removeMetadataSequence3Item(
            final org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item vMetadataSequence3Item) {
        boolean removed = _items.remove(vMetadataSequence3Item);
        return removed;
    }

    /**
     * Method removeMetadataSequence3ItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item removeMetadataSequence3ItemAt(
            final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vMetadataSequence3Item
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setMetadataSequence3Item(
            final int index,
            final org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item vMetadataSequence3Item)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setMetadataSequence3Item: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vMetadataSequence3Item);
    }

    /**
     * 
     * 
     * @param vMetadataSequence3ItemArray
     */
    public void setMetadataSequence3Item(
            final org.cambridge.ebooks.production.indexer.elements.MetadataSequence3Item[] vMetadataSequence3ItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vMetadataSequence3ItemArray.length; i++) {
                this._items.add(vMetadataSequence3ItemArray[i]);
        }
    }

}
