package org.cambridge.ebooks.production.indexer.util;

import java.io.File;
import java.io.FileWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.indexer.elements.AltIsbn;
import org.cambridge.ebooks.production.indexer.elements.Author;
import org.cambridge.ebooks.production.indexer.elements.Block;
import org.cambridge.ebooks.production.indexer.elements.Bold;
import org.cambridge.ebooks.production.indexer.elements.Book;
import org.cambridge.ebooks.production.references.elements.Citation;
import org.cambridge.ebooks.production.indexer.elements.Collab;
import org.cambridge.ebooks.production.indexer.elements.ContentItem;
import org.cambridge.ebooks.production.indexer.elements.Contributor;
import org.cambridge.ebooks.production.indexer.elements.CopyrightHolder;
import org.cambridge.ebooks.production.indexer.elements.CopyrightStatement;
import org.cambridge.ebooks.production.indexer.elements.CoverImage;
import org.cambridge.ebooks.production.indexer.elements.Forenames;
import org.cambridge.ebooks.production.indexer.elements.Include;
import org.cambridge.ebooks.production.indexer.elements.Insert;
import org.cambridge.ebooks.production.indexer.elements.Italic;
import org.cambridge.ebooks.production.indexer.elements.Keyword;
import org.cambridge.ebooks.production.indexer.elements.KeywordGroup;
import org.cambridge.ebooks.production.indexer.elements.ListItem;
import org.cambridge.ebooks.production.indexer.elements.NavFile;
import org.cambridge.ebooks.production.indexer.elements.OnlineDate;
import org.cambridge.ebooks.production.indexer.elements.P;
import org.cambridge.ebooks.production.references.elements.References;
import org.cambridge.ebooks.production.indexer.elements.SmallCaps;
import org.cambridge.ebooks.production.indexer.elements.Source;
import org.cambridge.ebooks.production.indexer.elements.Sub;
import org.cambridge.ebooks.production.indexer.elements.Subject;
import org.cambridge.ebooks.production.indexer.elements.Sup;
import org.cambridge.ebooks.production.indexer.elements.Surname;
import org.cambridge.ebooks.production.indexer.elements.Title;
import org.cambridge.ebooks.production.indexer.elements.TocItem;
import org.cambridge.ebooks.production.indexer.elements.Underline;
import org.cambridge.ebooks.production.indexer.solr.bean.BookContentItemDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookReferencesDocument;
import org.cambridge.ebooks.production.indexer.solr.bean.BookWordDocument;

public class DocumentBuilder {
	
	public static BookMetadataDocument generateBookMetadata(Book book) throws Exception {
		
		BookMetadataDocument result = new BookMetadataDocument();
		
		result.setId(book.getId());
		result.setBookId(book.getId());
		result.setContentType(ContentType.BOOK.getName());
		result.setTitle(getContent(book.getMetadata().getMainTitle().getContent()));
		result.setTitleAlphasort(book.getMetadata().getMainTitle().getAlphasort());
		result.setBookGroup(book.getGroup());
		result.setPages(book.getMetadata().getPages());
		
		if(book.getMetadata().getSubtitle()!= null) 
			result.setSubtitle(getContent(book.getMetadata().getSubtitle().getContent()));
		
		String onlineDate = getDate(book.getMetadata().getPubDates().getOnlineDate());
		String printDate = getDate(book.getMetadata().getPubDates().getPrintDate());
		
		result.setPrintDate(printDate);		
		result.setOnlineDate(onlineDate);			
		
		if(book.getMetadata().getEdition()!= null) 
		{
			result.setEdition(StringEscapeUtils.escapeXml(book.getMetadata().getEdition().getContent()));
			result.setEditionNumber(book.getMetadata().getEdition().getNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeNumber()))
			result.setVolumeNumber(book.getMetadata().getVolumeNumber());
				
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeTitle())) 
			result.setVolumeTitle(StringEscapeUtils.escapeXml(book.getMetadata().getVolumeTitle()));
		
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartNumber()))
			result.setPartNumber(book.getMetadata().getPartNumber());
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartTitle())) 
			result.setPartTitle(StringEscapeUtils.escapeXml(book.getMetadata().getPartTitle()));
		
		
		if(book.getMetadata().getSeries()!= null) 
		{
			result.setSeriesCode(book.getMetadata().getSeries().getCode());
			result.setSeries(getContent(book.getMetadata().getSeries().getContent()));
			result.setSeriesAlphasort(book.getMetadata().getSeries().getAlphasort());
			
			if(StringUtils.isNotEmpty(book.getMetadata().getSeries().getNumber())) 
				result.setSeriesNumber(Integer.parseInt(book.getMetadata().getSeries().getNumber()));
			
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getDoi())) 
			result.setDoi(book.getMetadata().getDoi());
		
		
		if(book.getMetadata().getAltDoi()!= null)
			result.setAltDoi(book.getMetadata().getAltDoi().getContent());
		
		
		result.setIsbn(book.getMetadata().getIsbn());
		
		for(AltIsbn altIsbn : book.getMetadata().getAltIsbn()) 
		{
			if(ContentType.PAPERBACK.getName().equalsIgnoreCase(altIsbn.getType())) 
				result.setAltIsbnPaperback(altIsbn.getContent());
			else if(ContentType.HARDBACK.getName().equalsIgnoreCase(altIsbn.getType())) 
				result.setAltIsbnHardback(altIsbn.getContent());
			else if(ContentType.EISBN.getName().equalsIgnoreCase(altIsbn.getType())) 
				result.setAltIsbnEisbn(altIsbn.getContent());
			else if(ContentType.OTHER.getName().equalsIgnoreCase(altIsbn.getType())) 
				result.setAltIsbnOther(altIsbn.getContent());
		}
		
		List<String> coverFilenameList = new ArrayList<String>();
		List<String> coverTypeList = new ArrayList<String>();
		for(CoverImage cover : book.getMetadata().getCoverImage()) 
		{
			coverFilenameList.add(cover.getFilename());
			coverTypeList.add(cover.getType());
		}
		result.setCoverImageFilenameList(coverFilenameList);
		result.setCoverImageTypeList(coverTypeList);
		
		List<String> insertFilenameList = new ArrayList<String>();
		List<String> insertIdList = new ArrayList<String>();
		List<String> insertTitleList = new ArrayList<String>();
		List<String> insertTitleAlphasortList = new ArrayList<String>();
		for(Insert insert :book.getMetadata().getInsert()){
			insertFilenameList.add(insert.getFilename());
			insertIdList.add(insert.getId());
			insertTitleList.add(getContent(insert.getTitle().getContent())); // PID 86532 - 2012-11-26
			insertTitleAlphasortList.add(insert.getTitle().getAlphasort());
		}
		result.setInsertFilenameList(insertFilenameList);
		result.setInsertIdList(insertIdList);
		result.setInsertTitleList(insertTitleList);
		result.setInsertTitleAlphasortList(insertTitleAlphasortList);
		
		List<String> authorNameList = new ArrayList<String>();	
		List<String> authorNameLfList = new ArrayList<String>();
		List<String> authorAffiliationList = new ArrayList<String>();		
		List<String> authorRoleList = new ArrayList<String>();		
		List<String> authorPositionList = new ArrayList<String>();
		List<String> authorNameAlphasort = new ArrayList<String>();
		
	
		for(Author author : book.getMetadata().getAuthor()) 
		{
			
			authorNameList.add(getFirstLastName(author.getName().getContent()));
			authorNameLfList.add(getLastFirstName(author.getName().getContent()));
			authorRoleList.add(author.getRole());
			authorPositionList.add(author.getPosition());
			if(author.getAffiliation()!= null) 
				authorAffiliationList.add(getContent(author.getAffiliation().getContent()));
			else 
				authorAffiliationList.add("none");
			
			if(author.getAlphasort()!= null) 
				authorNameAlphasort.add(author.getAlphasort());
			else
				authorNameAlphasort.add("none");			
		}				
		result.setAuthorNameList(authorNameList);
		result.setAuthorNameAlphasort(authorNameAlphasort);
		result.setAuthorAffiliationList(authorAffiliationList);
		result.setAuthorRoleList(authorRoleList);
		result.setAuthorPositionList(authorPositionList);
		
		List<String> subjectCodeList = new ArrayList<String>();
		List<String> subjectList = new ArrayList<String>();
		List<String> subjectLevelList = new ArrayList<String>();
		for(Subject subject : book.getMetadata().getSubjectGroup().getSubject()) 
		{
			subjectCodeList.add(subject.getCode());
			subjectList.add(subject.getContent());
			subjectLevelList.add(subject.getLevel());
		}
		result.setSubjectCodeList(subjectCodeList);
		result.setSubjectList(subjectList);
		result.setSubjectLevelList(subjectLevelList);	
		
		try
		{
			if(book.getMetadata().getBlurb()!= null) 
			{
				StringBuilder blurb = new StringBuilder();
				for(P p : book.getMetadata().getBlurb().getP()) 
					blurb.append(getContent(p.getContent()));			
				
				File dir = new File(IsbnContentDirUtil.getPath(book.getMetadata().getIsbn()));
				if(!dir.exists()) 
					dir.mkdir();	
				
				File file = new File(IsbnContentDirUtil.getPath(book.getMetadata().getIsbn()) + book.getMetadata().getIsbn() + "_blurb.txt");
				FileWriter fileWriter = new FileWriter(file);
				fileWriter.write(blurb.toString());
				fileWriter.close();
				
				System.out.println("successfully written: "+IsbnContentDirUtil.getPath(book.getMetadata().getIsbn()) + book.getMetadata().getIsbn() + "_blurb.txt");
			}
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] Cannot create blurb file for: " + book.getMetadata().getIsbn());
		}

		
		setPublisher(book.getMetadata().getPublisherNameAndPublisherLoc(), result);
		
		result.setCopyrightStatementList(getCopyrightStatement(book.getMetadata().getCopyrightStatement()));
		
		return result;
	}
	
	
	public static List<BookContentItemDocument> generateContentItemData(Book book) throws Exception {
		
		List<BookContentItemDocument> resultList = new ArrayList<BookContentItemDocument>();
		BookMetadataDocument doc = new BookMetadataDocument();
		
		doc.setIsbn(book.getMetadata().getIsbn());
		doc.setId(book.getId());
		
		resultList.addAll(generateContentItemData(book.getContentItems().getContentItem(), "root", doc));
		
		return resultList;
	}
	
	private static List<BookContentItemDocument> generateContentItemData(List<ContentItem> contentItemList, String parentId, BookMetadataDocument bookDoc) throws Exception {
		List<BookContentItemDocument> resultList = new ArrayList<BookContentItemDocument>();
		
		for(ContentItem contentItem : contentItemList) 
		{			
			BookContentItemDocument doc = new BookContentItemDocument();	
			
			doc.setContentType(ContentType.CONTENT_ITEM.getName());
			doc.setId(contentItem.getId());
			doc.setContentId(contentItem.getId());
			doc.setParentId(parentId);
			doc.setBookId(bookDoc.getId());
			doc.setIsbn(bookDoc.getIsbn());
			doc.setPageStart(contentItem.getPageStart());
			doc.setPageEnd(contentItem.getPageEnd());
			doc.setType(contentItem.getType());
			doc.setPosition(contentItem.getPosition());			
			doc.setTitle(getContent(contentItem.getHeading().getTitle().getContent()));		
			doc.setTitleAlphasort(StringEscapeUtils.escapeXml(contentItem.getHeading().getTitle().getAlphasort())); // PID 86837
			doc.setDoi(contentItem.getDoi());	
			
			if(contentItem.getHeading().getLabel()!= null) 				
				doc.setLabel(StringEscapeUtils.escapeXml(contentItem.getHeading().getLabel()));	// PID 86837
			
			if(contentItem.getHeading().getSubtitle()!= null) 				
				doc.setSubtitle(getContent(contentItem.getHeading().getSubtitle().getContent()));
			
			if(contentItem.getPdf() != null)			
				doc.setPdfFilename(contentItem.getPdf().getFilename());
			
			if(contentItem.getHtml()!=null)			
				doc.setHtmlFilename(contentItem.getHtml().getFilename());
			
			if(contentItem.getNavFile()!= null)
			{
				List<String> navFileFilename = new ArrayList<String>();
				List<String> navFileFilenameRid = new ArrayList<String>();
					
				for(NavFile navFile : contentItem.getNavFile()){
					
					navFileFilename.add(navFile.getFilename());
					navFileFilenameRid.add(navFile.getRid());
				}
				doc.setNavFileFilename(navFileFilename);
				doc.setNavFileFilenameRid(navFileFilenameRid);
				
			}
			if(contentItem.getContributorGroup()!= null)
			{
				List<String> contributorNameList = new ArrayList<String>();
				List<String> contributorNameLfList = new ArrayList<String>();
				List<String> contributorAffiliationList = new ArrayList<String>();
				List<String> contributorNameAlphasortList = new ArrayList<String>();
				List<String> contributorPosition = new ArrayList<String>();
				for(Contributor contributor : contentItem.getContributorGroup().getContributor())
				{						
					contributorNameList.add(getFirstLastName(contributor.getName().getContent()));
					contributorNameLfList.add(getLastFirstName(contributor.getName().getContent()));
					if(contributor.getAffiliation()!= null) 						
						contributorAffiliationList.add(getContent(contributor.getAffiliation().getContent()));
					else 
						contributorAffiliationList.add("none");
					
					if(contributor.getAlphasort()!= null) 						
						contributorNameAlphasortList.add(StringEscapeUtils.escapeXml(contributor.getAlphasort()));

					else 
						contributorNameAlphasortList.add("none");
					
					if(contributor.getPosition()!= null) 						
						contributorPosition.add(contributor.getPosition());
					else 
						contributorPosition.add("none");
					
				}
				doc.setAuthorNameList(contributorNameList);
				doc.setAuthorAffiliationList(contributorAffiliationList);
				doc.setAuthorPositionList(contributorPosition);
				doc.setAuthorNameAlphasortList(contributorNameAlphasortList);
				doc.setAuthorNameLfList(contributorNameLfList);
			}
			
			if(contentItem.getAbstract()!= null) 
			{	
				try
				{
					File dir = new File(IsbnContentDirUtil.getPath(bookDoc.getIsbn()));
					if(!dir.exists()) 
						dir.mkdir();
					
					File file = new File(IsbnContentDirUtil.getPath(bookDoc.getIsbn()) + doc.getId() + "_abstract.txt");
					FileWriter fileWriter = new FileWriter(file);		
					fileWriter.write(getContent(contentItem.getAbstract().getPOrBlockOrList()));
					fileWriter.close();
					
					System.out.println("successfully written: "+IsbnContentDirUtil.getPath(bookDoc.getIsbn()) + doc.getId() + "_abstract.txt");
				}
				catch(Exception ex)
				{
					System.out.println("[Exception] Cannot create abstract file for: " + bookDoc.getIsbn());
				}
				
				doc.setAbstractFilename(contentItem.getAbstract().getAltFilename());
				doc.setAbstractProblem(contentItem.getAbstract().getProblem());
			}
			
			if(contentItem.getToc()!= null) 
				doc.setTocText(getTocItems(contentItem.getToc().getTocItem()));
		
			
			if(contentItem.getKeywordGroup()!= null) 
			{
				List<String> keywordThemeList = new ArrayList<String>();
				List<String> keywordTextList = new ArrayList<String>();
				for(KeywordGroup keywordGroup : contentItem.getKeywordGroup()){
					if(KeywordGroupSource.THEME.getSource().equals(keywordGroup.getSource())){
						keywordThemeList.addAll(getKeywordTextList(keywordGroup.getKeyword(), 0));
					} else {
						keywordTextList.addAll(getKeywordTextList(keywordGroup.getKeyword(), 0));
					}
				}
				doc.setKeywordThemeList(keywordThemeList);
				doc.setKeywordTextList(keywordTextList);
			}
			
			if(contentItem.getInclude()!= null){
				List<String> insertFilenameList = new ArrayList<String>();
				List<String> insertIdList = new ArrayList<String>();
				List<String> insertTitleList = new ArrayList<String>();
				List<String> insertTitleAlphasortList = new ArrayList<String>();
				
				for(Include include : contentItem.getInclude()){

					Insert insert = (Insert) include.getHref();
					
					insertFilenameList.add(insert.getFilename());
					insertIdList.add(insert.getId());
					insertTitleList.add(getContent(insert.getTitle().getContent())); // PID 86532 - 2012-11-26
					insertTitleAlphasortList.add(insert.getTitle().getAlphasort());
				}
				doc.setInsertFilenameList(insertFilenameList);
				doc.setInsertIdList(insertIdList);
				doc.setInsertTitleList(insertTitleList);
				doc.setInsertTitleAlphasortList(insertTitleAlphasortList);
			}
			

			if(contentItem.getReferences() != null && !contentItem.getReferences().isEmpty())
			{
				Title refTtitle = contentItem.getReferences().get(0).getTitle();
				if(refTtitle != null)
					doc.setReferenceTitle(getContent(refTtitle.getContent()));
				
				doc.setReferenceType(contentItem.getReferences().get(0).getType());
			}

			
			resultList.add(doc);
			
			if(contentItem.getContentItem() != null && contentItem.getContentItem().size() > 0) 
				resultList.addAll(generateContentItemData(contentItem.getContentItem(), contentItem.getId(), bookDoc));
			
		}
		return resultList;
	}

	public static List<BookReferencesDocument> generateRefdata(List<References> allReferences) throws Exception {
		
		int x =1;
		
		List<BookReferencesDocument> resultList = new ArrayList<BookReferencesDocument>();
		
		for(References references : allReferences){
			
			List<Citation> citation =references.getAllCitations();
			 
			for(Citation citation2:citation){
			
				BookReferencesDocument refDoc = new BookReferencesDocument();
				
				refDoc.setBook_id(citation2.getBookId());
				refDoc.setId(citation2.getCitationId());
				refDoc.setPosition(citation2.getParentPosition());
				refDoc.setCrossRefDoi(citation2.getCrossRefDoi());
				refDoc.setCrossRefLink(citation2.getCrossRefLink());
				refDoc.setCrossRefStatus(citation2.getCrossRefStatus());
				refDoc.setData(citation2.getFullText());
				refDoc.setOpenURLLink(citation2.getOpenURLLink());
				refDoc.setGoogleBooksLink(citation2.getGoogleBooksLink());
				refDoc.setGoogleScholarLink(citation2.getGoogleScholarLink());
				refDoc.setChapter_id(citation2.getParentId());
				refDoc.setCambridgeLInk(citation2.getCambridgeLInk());
				refDoc.setLinkType(citation2.getLinkType());
				refDoc.setOrder_number(x);
			
				resultList.add(refDoc);

				x++;		 
			 }		
		}
		return resultList;
	}
	

	public static List<BookWordDocument> generateWordData(Book book) throws Exception {
		
		return generateWordData(book.getContentItems().getContentItem(), book.getId());
	}
	
	private static List<BookWordDocument> generateWordData(List<ContentItem> contentItemList, String bookId)throws Exception {
		
		List<BookWordDocument> resultList = new ArrayList<BookWordDocument>();
		
		String regex = "\\[|\\]" ;
		
		for(ContentItem contentItem : contentItemList){
			
			if(contentItem.getType().equalsIgnoreCase("dictionary")){
				
				for(TocItem item : contentItem.getToc().getTocItem()){
					
					BookWordDocument wordDoc = new BookWordDocument();
					
					wordDoc.setBookId(bookId);
					wordDoc.setContentId(contentItem.getId());
					wordDoc.setType(contentItem.getType());
					wordDoc.setTocId(item.getId());
					wordDoc.setWordText(stripAccents(item.getTocText().getContent().toString().replaceAll(regex, "")).toUpperCase());
					wordDoc.setWordUrl(item.getHtml().getFilename());
			
					resultList.add(wordDoc);
							
					if(contentItem.getContentItem() != null&& contentItem.getContentItem().size() > 0) 
						resultList.addAll(generateWordData(contentItem.getContentItem(), bookId));
				}
			}
		}
		return resultList;
	}
	
	public static String stripAccents(String input) 
	{
	    input = Normalizer.normalize(input, Normalizer.Form.NFKD);
	    input=  input.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	    return input;    
	}
	
	@SuppressWarnings("rawtypes")
	private static String getContent(List<Object> content) throws Exception {
		StringBuffer result = new StringBuffer();
		
		for(Object object : content) 
		{
			if(object instanceof Italic)
			{
				result.append("<i>");
				result.append(getContent(((Italic)object).getContent()));
				result.append("</i>");
			} 
			else if(object instanceof Bold) 
			{
				result.append("<b>");
				result.append(getContent(((Bold)object).getContent()));
				result.append("</b>");
			} 
			else if(object instanceof SmallCaps)
			{
				result.append("<span style='font-variant:small-caps'>");
				result.append(getContent(((SmallCaps)object).getContent()));
				result.append("</span>");
			} 
			else if(object instanceof Underline) 
			{
				result.append("<u>");
				result.append(getContent(((Underline)object).getContent()));
				result.append("</u>");
			} 
			else if(object instanceof Sup) 
			{
				result.append("<sup>");
				result.append(getContent(((Sup)object).getContent()));
				result.append("</sup>");
			} 
			else if(object instanceof Sub) 
			{
				result.append("<sub>");
				result.append(getContent(((Sub)object).getContent()));
				result.append("</sub>");
			} 
			else if(object instanceof P) 
			{
				result.append("<p>");
				result.append(getContent(((P)object).getContent()));
				result.append("</p>");
			} 
			else if(object instanceof Block) 
			{
				result.append(getBlock((Block)object));
			} 
			else if(object instanceof org.cambridge.ebooks.production.indexer.elements.List) 
			{
				result.append(getList((org.cambridge.ebooks.production.indexer.elements.List)object));
			} 
			else if(object instanceof Source)
			{
				result.append(getContent(((Source)object).getContent()));
			} 
			else if(object instanceof CopyrightHolder)
			{
				result.append(getContent(((CopyrightHolder)object).getContent()));
			}
			else if(object instanceof CopyrightStatement)
			{
				result.append(getContent(((CopyrightStatement)object).getContent()));
			} 
			else if(object instanceof JAXBElement) 
			{
				result.append(" ");
				result.append((String)((JAXBElement)object).getValue());
			} 
			else if(object instanceof Forenames) 
			{
				result.append(getContent(((Forenames)object).getContent()));
			}
			else if(object instanceof Surname)
			{
				result.append(" ");
				result.append(getContent(((Surname)object).getContent()));
			} 
			else if(object instanceof Collab)
			{
				result.append(getContent(((Collab)object).getContent()));
			} 
			else 
			{
				result.append(cleanTextNode(StringEscapeUtils.escapeXml((String)object)));
			}
		}
		
		return result.toString();
	}	
	
	
	private static String getBlock(Block block) throws Exception {
		StringBuffer result = new StringBuffer();
		List<P> pList = block.getP();
		Source source = block.getSource();

		result.append("<blockquote id='indent'>");
		for(P p : pList)
		{
			result.append("<p>");
			result.append(getContent(p.getContent()));
			result.append("</p>");
		}
		
		if(source!= null) 
			result.append(getContent(source.getContent()));
		
		result.append("</blockquote>");
		
		return result.toString();
	}
	
	private static String getList(org.cambridge.ebooks.production.indexer.elements.List ul) throws Exception {
		StringBuffer result = new StringBuffer();
		result.append("<ul ");
		if("bullet".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:disc !important;'>");
		} else if("number".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:decimal !important;'>");
		} else {
			result.append("style='padding: 20px 20px 20px 30px; list-style:none !important;'>");
		}
		for(ListItem li : ul.getListItem()) {
			result.append("<li>");		
			result.append(getContent(li.getContent()));
			result.append("</li>");		
		}		
		result.append("</ul>");
		
		return result.toString();
	}
	
	private static String getTocItems(List<TocItem> tocList) throws Exception {
		StringBuffer sb = new StringBuffer();
		
		for(TocItem tocItem : tocList) 
		{

			sb.append(getContent(tocItem.getTocText().getContent()));
			sb.append(" <em>(start page: " + tocItem.getStartpage() + ")</em><br/>");

			if(tocItem.getTocItem() != null && tocItem.getTocItem().size() > 0) 				
				sb.append(getTocItems(tocItem.getTocItem()));
			
		}
		
		return sb.toString();
	}	
	
	private static String cleanTextNode(String val) {
		if (val!= null) {
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "")
			.replaceAll("\\s+", " ")
			.replaceAll("\\\\", "&#92;")
			.replaceAll("&apos;", "&#39;"); 
		}
		return val;
	}
	
	private static List<String> getCopyrightStatement(List<CopyrightStatement> copyrightStatementList) throws Exception {
		List<String> result = new ArrayList<String>();
		
		for(CopyrightStatement cs : copyrightStatementList) {
			result.add(getContent(cs.getContent()));
		}
		
		return result;
	}
	
	private static <T> String getDate(List<T> dateList) {
		String result = "";
		
		if(dateList != null && dateList.size() > 0) 
			if(dateList.get(0) instanceof String)
				result = dateList.get(0).toString();
			else if(dateList.get(0) instanceof OnlineDate)
				result = ((OnlineDate)dateList.get(0)).getContent();
			
		return result;		
	}
	
	private static String getFirstLastName(List<Object> content) throws Exception {
		StringBuilder result = new StringBuilder();
		List<String> lastName = new ArrayList<String>();
		List<String> firstName = new ArrayList<String>();
		List<String> collab = new ArrayList<String>();
		List<String> suffix = new ArrayList<String>();
		List<String> nameLink = new ArrayList<String>();
		
		getName(content, lastName, firstName, collab, suffix, nameLink);
		
		for(String s : firstName) {
			result.append(s).append(" ");
		}
				
		for(String s : suffix) {
			result.append(s).append(" ");
		}		
		
		for(String s : nameLink) {
			result.append(s).append(" ");
		}
		
		for(String s : lastName) {
			result.append(s).append(" ");
		}
		
		for(String s : collab) {
			result.append(s).append(" ");
		}
		
		String trimed = result.toString();
		if(StringUtils.isNotEmpty(trimed)) {
			trimed = trimed.trim();
		}
		
		return trimed;
	}
	
	private static String getLastFirstName(List<Object> content) throws Exception {
		StringBuilder result = new StringBuilder();
		List<String> lastName = new ArrayList<String>();
		List<String> firstName = new ArrayList<String>();
		List<String> collab = new ArrayList<String>();
		List<String> suffix = new ArrayList<String>();
		List<String> nameLink = new ArrayList<String>();
		
		getName(content, lastName, firstName, collab, suffix, nameLink);
		
		for(String s : nameLink)
			result.append(s).append(" ");
		
		
		int ctr = 0;
		for(String s : lastName) 
		{
			ctr++;
			result.append(s);
			if(ctr < lastName.size()) 
				result.append(" ");
			else 
				result.append(", ");				
		}		
		
		for(String s : firstName) 
			result.append(s).append(" ");
		
		for(String s : suffix) 
			result.append(s).append(" ");
			
		for(String s : collab) 
			result.append(s).append(" ");
		
		String trimed = result.toString();
		if(StringUtils.isNotEmpty(trimed)) 
			trimed = trimed.trim();
		
		return trimed;
	}
	
	
	@SuppressWarnings("rawtypes")
	private static String getName(List<Object> content, List<String> lastName, 
			List<String> firstName, List<String> collab, List<String> suffix, List<String> nameLink) throws Exception {
		
		StringBuffer result = new StringBuffer();
		for(Object object : content) 
		{
			if(object instanceof Italic) 
			{
				result.append("<i>");
				result.append(getContent(((Italic)object).getContent()));
				result.append("</i>");
			} 
			else if(object instanceof Bold) 
			{
				result.append("<b>");
				result.append(getContent(((Bold)object).getContent()));
				result.append("</b>");
			} 
			else if(object instanceof SmallCaps) 
			{
				result.append("<span style='font-variant:small-caps'>");
				result.append(getContent(((SmallCaps)object).getContent()));
				result.append("</span>");
			} 
			else if(object instanceof Underline) 
			{
				result.append("<u>");
				result.append(getContent(((Underline)object).getContent()));
				result.append("</u>");
			} 
			else if(object instanceof Sup) 
			{
				result.append("<sup>");
				result.append(getContent(((Sup)object).getContent()));
				result.append("</sup>");
			} 
			else if(object instanceof Sub) 
			{
				result.append("<sub>");
				result.append(getContent(((Sub)object).getContent()));
				result.append("</sub>");
			} 
			else if(object instanceof P) 
			{
				result.append("<p>");
				result.append(getContent(((P)object).getContent()));
				result.append("</p>");
			} 
			else if(object instanceof Block) 
			{
				result.append(getBlock((Block)object));
			}
			else if(object instanceof org.cambridge.ebooks.production.indexer.elements.List) 
			{
				result.append(getList((org.cambridge.ebooks.production.indexer.elements.List)object));
			} 
			else if(object instanceof Source) 
			{
				result.append(getContent(((Source)object).getContent()));
			} 
			else if(object instanceof CopyrightHolder)
			{
				result.append(getContent(((CopyrightHolder)object).getContent()));
			} 
			else if(object instanceof CopyrightStatement) 
			{
				result.append(getContent(((CopyrightStatement)object).getContent()));
			} 
			else if(object instanceof JAXBElement) 
			{
				JAXBElement jaxb = (JAXBElement)object;
				if("suffix".equalsIgnoreCase(jaxb.getName().toString()))
					suffix.add((String)jaxb.getValue());
				else if("name-link".equalsIgnoreCase(jaxb.getName().toString()))
					nameLink.add((String)jaxb.getValue()); 
			} 
			else if(object instanceof Forenames) 
			{
				firstName.add(getName(((Forenames)object).getContent(), lastName, firstName, collab, suffix, nameLink));
			} 
			else if(object instanceof Surname) 
			{				
				lastName.add(getName(((Surname)object).getContent(), lastName, firstName, collab, suffix, nameLink));
			} 
			else if(object instanceof Collab) 
			{
				collab.add(getName(((Collab)object).getContent(), lastName, firstName, collab, suffix, nameLink));
			} 
			else 
			{
				result.append(cleanTextNode(StringEscapeUtils.escapeXml((String)object)));
			}
		}
		
		return result.toString();
	}
	
	private static void setPublisher(List<JAXBElement<String>> publisherList, BookMetadataDocument doc) {		
		int ctr = 0;
		for(JAXBElement<String> jaxb : publisherList) 
		{
			ctr++;		
			if("publisher-name".equalsIgnoreCase(jaxb.getName().toString()))			
				doc.setPublisherName(StringEscapeUtils.escapeXml(jaxb.getValue()));
			else 				
				doc.setPublisherLoc(StringEscapeUtils.escapeXml(jaxb.getValue()));			
		}
	}
	
	@SuppressWarnings("unused")
	private static List<BookContentItemDocument> getTocItem(List<TocItem> tocList, String parentId, String contentId, String bookId) throws Exception {
		List<BookContentItemDocument> resultList = new ArrayList<BookContentItemDocument>();
		
		for(TocItem tocItem : tocList) 
		{
			BookContentItemDocument doc = new BookContentItemDocument();
			doc.setContentType(ContentType.TOC_ITEM.getName());
			doc.setBookId(bookId);
			doc.setParentId(parentId);
			doc.setContentId(contentId);
			doc.setId(tocItem.getId());
			doc.setPageStart(tocItem.getStartpage());
			doc.setTocText(getContent(tocItem.getTocText().getContent()));
			if(tocItem.getAuthor() != null&& tocItem.getAuthor().size() > 0) 
			{
				List<String> authorNameList = new ArrayList<String>();
				List<String> authorRoleList = new ArrayList<String>();
				List<String> authorAffiliationList = new ArrayList<String>();
				List<String> authorNameAlphasortList = new ArrayList<String>();
				for(Author author : tocItem.getAuthor()) 
				{
					authorNameList.add(getContent(author.getName().getContent()));
					authorRoleList.add(author.getRole());
					if(author.getAffiliation()!= null) 
						authorAffiliationList.add(getContent(author.getAffiliation().getContent()));
					
					if(author.getAlphasort()!= null) 
						authorNameAlphasortList.add(StringEscapeUtils.escapeXml(author.getAlphasort()));
				}
				
				doc.setAuthorNameList(authorNameList);
				doc.setAuthorRoleList(authorRoleList);
				doc.setAuthorNameAlphasortList(authorNameAlphasortList);
				if(authorAffiliationList.size() > 0) 
					doc.setAuthorAffiliationList(authorAffiliationList);
				
			}	
			
			resultList.add(doc);
			if(tocItem.getTocItem() != null && tocItem.getTocItem().size() > 0) 				
				resultList.addAll(getTocItem(tocItem.getTocItem(), tocItem.getId(), contentId, bookId));
			
		}
		
		return resultList;
	}
	
	private static List<String> getKeywordTextList(List<Keyword> keywordList, int pad) throws Exception {
		List<String> result = new ArrayList<String>();
		for(Keyword keyword : keywordList) 
		{
			
			result.add(pad("<div>", getContent(keyword.getKwdText().getContent()), "</div>", pad));
			if(keyword.getKeyword() != null && keyword.getKeyword().size() > 0) 
				result.addAll(getKeywordTextList(keyword.getKeyword(), pad + 1));
		}
		return result;
	}
	
	private static String pad(String leftPad, String toPad, String rightPad, int times){
		String result = "";
		
		if(times > 0)
		{
			StringBuilder sb = new StringBuilder();
			for(int ctr=0; ctr<times; ctr++)
				sb.append(leftPad);
			
			sb.append(toPad);
			
			for(int ctr=0; ctr<times; ctr++)
				sb.append(rightPad);
			
			result = sb.toString();
		}
		else
			result = toPad;

		return result + "<br/>";
	}
	
}
