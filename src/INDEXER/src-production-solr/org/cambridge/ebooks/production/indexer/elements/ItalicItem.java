/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class ItalicItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class ItalicItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _sup.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sup _sup;

    /**
     * Field _sub.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sub _sub;

    /**
     * Field _bold.
     */
    private org.cambridge.ebooks.production.indexer.elements.Bold _bold;

    /**
     * Field _smallCaps.
     */
    private org.cambridge.ebooks.production.indexer.elements.SmallCaps _smallCaps;

    /**
     * Field _underline.
     */
    private org.cambridge.ebooks.production.indexer.elements.Underline _underline;


      //----------------/
     //- Constructors -/
    //----------------/

    public ItalicItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'bold'.
     * 
     * @return the value of field 'Bold'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Bold getBold(
    ) {
        return this._bold;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'smallCaps'.
     * 
     * @return the value of field 'SmallCaps'.
     */
    public org.cambridge.ebooks.production.indexer.elements.SmallCaps getSmallCaps(
    ) {
        return this._smallCaps;
    }

    /**
     * Returns the value of field 'sub'.
     * 
     * @return the value of field 'Sub'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sub getSub(
    ) {
        return this._sub;
    }

    /**
     * Returns the value of field 'sup'.
     * 
     * @return the value of field 'Sup'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sup getSup(
    ) {
        return this._sup;
    }

    /**
     * Returns the value of field 'underline'.
     * 
     * @return the value of field 'Underline'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Underline getUnderline(
    ) {
        return this._underline;
    }

    /**
     * Sets the value of field 'bold'.
     * 
     * @param bold the value of field 'bold'.
     */
    public void setBold(
            final org.cambridge.ebooks.production.indexer.elements.Bold bold) {
        this._bold = bold;
        this._choiceValue = bold;
    }

    /**
     * Sets the value of field 'smallCaps'.
     * 
     * @param smallCaps the value of field 'smallCaps'.
     */
    public void setSmallCaps(
            final org.cambridge.ebooks.production.indexer.elements.SmallCaps smallCaps) {
        this._smallCaps = smallCaps;
        this._choiceValue = smallCaps;
    }

    /**
     * Sets the value of field 'sub'.
     * 
     * @param sub the value of field 'sub'.
     */
    public void setSub(
            final org.cambridge.ebooks.production.indexer.elements.Sub sub) {
        this._sub = sub;
        this._choiceValue = sub;
    }

    /**
     * Sets the value of field 'sup'.
     * 
     * @param sup the value of field 'sup'.
     */
    public void setSup(
            final org.cambridge.ebooks.production.indexer.elements.Sup sup) {
        this._sup = sup;
        this._choiceValue = sup;
    }

    /**
     * Sets the value of field 'underline'.
     * 
     * @param underline the value of field 'underline'.
     */
    public void setUnderline(
            final org.cambridge.ebooks.production.indexer.elements.Underline underline) {
        this._underline = underline;
        this._choiceValue = underline;
    }

}
