package org.cambridge.ebooks.production.indexer.util;

public enum ContentType {
	BOOK("book"), CONTENT_ITEM("content-item"), REFERENCES("references"), CITATION("citation"), 
	TOC_ITEM("toc-item"), PAPERBACK("paperback"), HARDBACK("hardback"), EISBN("e-isbn"), OTHER("other");
	
	String name;
	private ContentType(String name){ this.name = name;}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
