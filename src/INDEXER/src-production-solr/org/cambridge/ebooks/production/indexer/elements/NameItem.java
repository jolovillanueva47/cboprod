/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class NameItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class NameItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _sup.
     */
    private org.cambridge.ebooks.production.indexer.elements.Sup _sup;

    /**
     * Field _forenames.
     */
    private org.cambridge.ebooks.production.indexer.elements.Forenames _forenames;

    /**
     * Field _surname.
     */
    private org.cambridge.ebooks.production.indexer.elements.Surname _surname;

    /**
     * Field _nameLink.
     */
    private java.lang.String _nameLink;

    /**
     * Field _suffix.
     */
    private java.lang.String _suffix;

    /**
     * Field _collab.
     */
    private org.cambridge.ebooks.production.indexer.elements.Collab _collab;


      //----------------/
     //- Constructors -/
    //----------------/

    public NameItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'collab'.
     * 
     * @return the value of field 'Collab'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Collab getCollab(
    ) {
        return this._collab;
    }

    /**
     * Returns the value of field 'forenames'.
     * 
     * @return the value of field 'Forenames'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Forenames getForenames(
    ) {
        return this._forenames;
    }

    /**
     * Returns the value of field 'nameLink'.
     * 
     * @return the value of field 'NameLink'.
     */
    public java.lang.String getNameLink(
    ) {
        return this._nameLink;
    }

    /**
     * Returns the value of field 'suffix'.
     * 
     * @return the value of field 'Suffix'.
     */
    public java.lang.String getSuffix(
    ) {
        return this._suffix;
    }

    /**
     * Returns the value of field 'sup'.
     * 
     * @return the value of field 'Sup'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Sup getSup(
    ) {
        return this._sup;
    }

    /**
     * Returns the value of field 'surname'.
     * 
     * @return the value of field 'Surname'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Surname getSurname(
    ) {
        return this._surname;
    }

    /**
     * Sets the value of field 'collab'.
     * 
     * @param collab the value of field 'collab'.
     */
    public void setCollab(
            final org.cambridge.ebooks.production.indexer.elements.Collab collab) {
        this._collab = collab;
        this._choiceValue = collab;
    }

    /**
     * Sets the value of field 'forenames'.
     * 
     * @param forenames the value of field 'forenames'.
     */
    public void setForenames(
            final org.cambridge.ebooks.production.indexer.elements.Forenames forenames) {
        this._forenames = forenames;
        this._choiceValue = forenames;
    }

    /**
     * Sets the value of field 'nameLink'.
     * 
     * @param nameLink the value of field 'nameLink'.
     */
    public void setNameLink(
            final java.lang.String nameLink) {
        this._nameLink = nameLink;
        this._choiceValue = nameLink;
    }

    /**
     * Sets the value of field 'suffix'.
     * 
     * @param suffix the value of field 'suffix'.
     */
    public void setSuffix(
            final java.lang.String suffix) {
        this._suffix = suffix;
        this._choiceValue = suffix;
    }

    /**
     * Sets the value of field 'sup'.
     * 
     * @param sup the value of field 'sup'.
     */
    public void setSup(
            final org.cambridge.ebooks.production.indexer.elements.Sup sup) {
        this._sup = sup;
        this._choiceValue = sup;
    }

    /**
     * Sets the value of field 'surname'.
     * 
     * @param surname the value of field 'surname'.
     */
    public void setSurname(
            final org.cambridge.ebooks.production.indexer.elements.Surname surname) {
        this._surname = surname;
        this._choiceValue = surname;
    }

}
