/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package org.cambridge.ebooks.production.indexer.elements;

/**
 * Class MetadataSequence.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MetadataSequence implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _edition.
     */
    private org.cambridge.ebooks.production.indexer.elements.Edition _edition;

    /**
     * Field _otherEditionList.
     */
    private java.util.Vector<org.cambridge.ebooks.production.indexer.elements.OtherEdition> _otherEditionList;


      //----------------/
     //- Constructors -/
    //----------------/

    public MetadataSequence() {
        super();
        this._otherEditionList = new java.util.Vector<org.cambridge.ebooks.production.indexer.elements.OtherEdition>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vOtherEdition
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addOtherEdition(
            final org.cambridge.ebooks.production.indexer.elements.OtherEdition vOtherEdition)
    throws java.lang.IndexOutOfBoundsException {
        this._otherEditionList.addElement(vOtherEdition);
    }

    /**
     * 
     * 
     * @param index
     * @param vOtherEdition
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addOtherEdition(
            final int index,
            final org.cambridge.ebooks.production.indexer.elements.OtherEdition vOtherEdition)
    throws java.lang.IndexOutOfBoundsException {
        this._otherEditionList.add(index, vOtherEdition);
    }

    /**
     * Method enumerateOtherEdition.
     * 
     * @return an Enumeration over all
     * org.cambridge.ebooks.production.indexer.elements.OtherEdition
     * elements
     */
    public java.util.Enumeration<? extends org.cambridge.ebooks.production.indexer.elements.OtherEdition> enumerateOtherEdition(
    ) {
        return this._otherEditionList.elements();
    }

    /**
     * Returns the value of field 'edition'.
     * 
     * @return the value of field 'Edition'.
     */
    public org.cambridge.ebooks.production.indexer.elements.Edition getEdition(
    ) {
        return this._edition;
    }

    /**
     * Method getOtherEdition.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * org.cambridge.ebooks.production.indexer.elements.OtherEdition
     * at the given index
     */
    public org.cambridge.ebooks.production.indexer.elements.OtherEdition getOtherEdition(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._otherEditionList.size()) {
            throw new IndexOutOfBoundsException("getOtherEdition: Index value '" + index + "' not in range [0.." + (this._otherEditionList.size() - 1) + "]");
        }

        return (org.cambridge.ebooks.production.indexer.elements.OtherEdition) _otherEditionList.get(index);
    }

    /**
     * Method getOtherEdition.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public org.cambridge.ebooks.production.indexer.elements.OtherEdition[] getOtherEdition(
    ) {
        org.cambridge.ebooks.production.indexer.elements.OtherEdition[] array = new org.cambridge.ebooks.production.indexer.elements.OtherEdition[0];
        return (org.cambridge.ebooks.production.indexer.elements.OtherEdition[]) this._otherEditionList.toArray(array);
    }

    /**
     * Method getOtherEditionCount.
     * 
     * @return the size of this collection
     */
    public int getOtherEditionCount(
    ) {
        return this._otherEditionList.size();
    }

    /**
     */
    public void removeAllOtherEdition(
    ) {
        this._otherEditionList.clear();
    }

    /**
     * Method removeOtherEdition.
     * 
     * @param vOtherEdition
     * @return true if the object was removed from the collection.
     */
    public boolean removeOtherEdition(
            final org.cambridge.ebooks.production.indexer.elements.OtherEdition vOtherEdition) {
        boolean removed = _otherEditionList.remove(vOtherEdition);
        return removed;
    }

    /**
     * Method removeOtherEditionAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public org.cambridge.ebooks.production.indexer.elements.OtherEdition removeOtherEditionAt(
            final int index) {
        java.lang.Object obj = this._otherEditionList.remove(index);
        return (org.cambridge.ebooks.production.indexer.elements.OtherEdition) obj;
    }

    /**
     * Sets the value of field 'edition'.
     * 
     * @param edition the value of field 'edition'.
     */
    public void setEdition(
            final org.cambridge.ebooks.production.indexer.elements.Edition edition) {
        this._edition = edition;
    }

    /**
     * 
     * 
     * @param index
     * @param vOtherEdition
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setOtherEdition(
            final int index,
            final org.cambridge.ebooks.production.indexer.elements.OtherEdition vOtherEdition)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._otherEditionList.size()) {
            throw new IndexOutOfBoundsException("setOtherEdition: Index value '" + index + "' not in range [0.." + (this._otherEditionList.size() - 1) + "]");
        }

        this._otherEditionList.set(index, vOtherEdition);
    }

    /**
     * 
     * 
     * @param vOtherEditionArray
     */
    public void setOtherEdition(
            final org.cambridge.ebooks.production.indexer.elements.OtherEdition[] vOtherEditionArray) {
        //-- copy array
        _otherEditionList.clear();

        for (int i = 0; i < vOtherEditionArray.length; i++) {
                this._otherEditionList.add(vOtherEditionArray[i]);
        }
    }

}
