#!/bin/sh
LIB="/app/ebooks/solr/online/solr_daily/lib/"
BIN="/app/ebooks/solr/online/solr_daily/bin/"
EXEC_JAR=$LIB"solr_daily_reindex.jar"
NOW=`date '+daily.solr.indexer.log.%Y-%d-%m'`
LOG_FILE="/app/ebooks/solr/online/solr_daily/log/$NOW"

cd $BIN
nohup java -jar -Xms128m -Xmx512m $EXEC_JAR $BIN"isbn.txt" $BIN"properties.txt" >> $LOG_FILE.out 2>> $LOG_FILE.err < /dev/null &