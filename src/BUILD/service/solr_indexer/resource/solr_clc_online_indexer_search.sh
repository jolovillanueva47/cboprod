#!/bin/sh
INDEXER_HOME="/app/ebooks/solr/online/indexer"
LIB="$INDEXER_HOME/lib/"
BIN="$INDEXER_HOME/bin/"
EXEC_JAR=$LIB"solr_cbo_indexer.jar"
NOW=`date '+solr.clc.search.index.log.%Y-%d-%m'`
LOG_FILE="$INDEXER_HOME/log/$NOW.txt"
JAVA="/app/jdk1.6.0_20/bin/java"

nohup $JAVA -jar -Xms128m -Xmx512m $EXEC_JAR clc search >> $LOG_FILE &