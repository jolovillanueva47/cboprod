#!/bin/sh

NOW=`date '+coreisbn.update.all.log.%Y-%d-%m'`

HOME="/app/ebooks/service/coreisbn_updater"
LIB="$HOME/lib"
BIN="$HOME/bin"
LOG_FILE="$HOME/log/$NOW"
EXEC_JAR=$LIB"/coreisbn_updater.jar"
JAVA="/app/jdk1.6.0_20/bin/java"

nohup $JAVA -jar -Xms128m -Xmx2048m $EXEC_JAR all >> $LOG_FILE.out 2>> $LOG_FILE.err < /dev/null &