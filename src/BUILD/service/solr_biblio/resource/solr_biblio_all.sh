#!/bin/sh

NOW=`date '+solr.biblio.all.log.%Y-%d-%m'`

SOLR_BIBLIO_HOME="/app/ebooks/solr/online/solrBiblio"
LIB="$SOLR_BIBLIO_HOME/lib"
BIN="$SOLR_BIBLIO_HOME/bin"
EXEC_JAR="$LIB/solrBiblioUpdate.jar"
LOG_FILE="$SOLR_BIBLIO_HOME/log/$NOW"
JAVA="/app/jdk1.6.0_20/bin/java"

cd $BIN

nohup $JAVA -jar -Xms128m -Xmx2048m $EXEC_JAR all >> $LOG_FILE.out 2>> $LOG_FILE.err < /dev/null &