package org.cambridge.ebooks.service.solr.daily;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.cambridge.ebooks.service.solr.EbookSolrIndexer;
import org.cambridge.ebooks.service.solr.Indexer;
import org.cambridge.ebooks.service.solr.bean.ReindexInfoBean;
import org.cambridge.ebooks.service.solr.daily.jdbc.DBInfo;
import org.cambridge.ebooks.service.solr.server.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.service.solr.server.CupSolrServer;
import org.cambridge.ebooks.service.solr.util.JaxbXmlParser;
import org.cambridge.ebooks.service.solr.util.XmlParser;

public class OnlineReindexDaily {
	
	// start == kmulingtapang: integrate to solr
//	private static SolrServer bookCore;
//	private static SolrServer chapterCore;
//	private static SolrServer searchCore;
	private static CupSolrServer server = new CupCommonsHttpSolrServer();
	private static XmlParser xmlParser = new JaxbXmlParser("org.cambridge.ebooks.service.solr.bean.jaxb");
	
	// start == need to be in property file
//	private static final String socketTimeout = "100000";
//	private static final String connTimeout = "10000";
//	private static final String defMaxConnPerHost = "10000";
//	private static final String maxTotalConn = "100000";
//	private static final String followRedirects = "false";
//	private static final String allowCompression = "false";
//	private static final String retries = "1";
//
//	private static final String solrHome = "http://192.168.60.240:8080/"; 
//	private static final String coreBook = "book";
//	private static final String coreChapter = "chapter";
//	private static final String coreSearch = "search";	
	// end == need to be in property file	
	// end == kmulingtapang: integrate to solr 
	
	
	/* how to use */
	/*
	arg[0] = set to "null" otherwise location of text file with isbn list
	arg[1-x] = all property files you want to load		
	*/
	public static void main(String... args) throws Exception {
		mainProper(args);
	}
	
	private static void mainProper(String... args) throws Exception {		
		//checks and throws exception dirs		
		areAllDirFiles(args);
			
		//init props
		System.out.println("init properties file before method");
		initProperties(args);
								
		try{
			if(args[0].equals("null")){
				dbReindex();
			}else{				
				manualReindex(args[0]);
			}
		}catch(Exception e){			
			e.printStackTrace();			
			throw new Exception(e.getMessage());
		}
	}
	
	private static void doReindex(List<ReindexInfoBean> liRein){
		//logs
		
		System.out.println("size of db = " +liRein.size());
		
		// start == kmulingtapang: integrate to solr		
//		Map<String, String> serverSettings = new HashMap<String, String>();
		//serverSettings.put(KEY_SOCKET_TIMEOUT, socketTimeout);
		//serverSettings.put(KEY_CONN_TIMEOUT, connTimeout);
		//serverSettings.put(KEY_DEF_MAX_CONN_PER_HOST, defMaxConnPerHost);
		//serverSettings.put(KEY_MAX_TOTAL_CONN, maxTotalConn);
		//serverSettings.put(KEY_FOLLOW_REDIRECTS, followRedirects);
		//serverSettings.put(KEY_ALLOW_COMPRESSION, allowCompression);
		//serverSettings.put(KEY_MAX_RETRIES, retries);				
		
		//bookCore = server.getMultiCore(solrHome, coreBook, serverSettings);
		//chapterCore = server.getMultiCore(solrHome, coreChapter, serverSettings);
		//searchCore = server.getMultiCore(solrHome, coreSearch, serverSettings);
		
		Indexer indexer = new EbookSolrIndexer(server, xmlParser);
		System.out.println("ADD/UPDATE... xoxox");
		indexer.add(liRein, "ALL");			
		// end == kmulingtapang: integrate to solr 
				
		try {
			if(liRein != null && liRein.size() > 0){
				ReindexInfoBean rein = liRein.get(0);
				System.out.println("singeLine :" + rein.getBookId() + " " + rein.getAuthorSingleline());
				
			}
			
			//System.out.println("optimizing...");
			// start == kmulingtapang: integrate to solr
			//indexer.optimize("ALL");
			// end == kmulingtapang: integrate to solr 
			
			//mdb.optimizeIndexAll(sb);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("testing");			
		}
		
		
		
	}
	
	private static void initProperties(String... args) throws FileNotFoundException, IOException {		
		Properties properties = new Properties(System.getProperties());
		System.out.println("init properties file");
		for(int i = 1; i < args.length; i++){	
			System.out.println("loading init files : " + args[i]);
			properties.load(new FileInputStream(args[i]));
		}	 
		System.setProperties(properties);		
		
	}
		
	
	@SuppressWarnings("unused")
	private static List<ReindexInfoBean> test(){
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
//		Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
//		ReindexInfoBean b1 = new ReindexInfoBean("9780511485183", "Y", sqlDate, "CBO9780511485183");
//		ReindexInfoBean b2 = new ReindexInfoBean("9780511483066", "Y", sqlDate, "CBO9780511483066");
//		
//		liRein.add(b1);
//		liRein.add(b2);		
		return liRein;
	}
		
	private static void areAllDirFiles(String... args) throws Exception{
		for(String loc : args){
			File file = new File(loc);
			if(!(file.isDirectory() || file.isFile() || "null".equals(loc))){
				throw new Exception(loc + " is not a directory!");
			}			
		}		
	}	
	
	private static  void dbReindex() throws SQLException{		
		//logs
		System.out.println("start reindex from db");		
					
		List<ReindexInfoBean> liRein = //test(); 
			DBInfo.getObjectList();		
		
		doReindex(liRein);		
		//doyourthing();
		System.out.println("finished");		
	}
	
	private static  void manualReindex(String loc) throws SQLException{
		System.out.println("start reindex from file yep");
			
		List<ReindexInfoBean> liRein = readFromFile(loc);
		doReindex(liRein);
		System.out.println("finished");
	}
	
	private static List<ReindexInfoBean> readFromFile(String loc) throws SQLException{
		System.out.println("reading from loc = " + loc);
		ArrayList<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try {
			//use buffering, reading one line at a time
		    //FileReader always assumes default encoding is OK!
		    BufferedReader input =  new BufferedReader(new FileReader(loc));
		    try {
		    	String line = null; //not declared within while loop
		        /*
		        * readLine is a bit quirky :
		        * it returns the content of a line MINUS the newline.
		        * it returns null only for the END of the stream.
		        * it returns an empty String if two newlines appear in a row.
		        */
		    	int cnt = 0;
		    	ArrayList<String> isbnList = new ArrayList<String>();
		    			    	
		    	//limits to 900 because sql has a limit for in (,,,,,) up to 999 according to jhorelle
		    	while (( line = input.readLine()) != null){
		    		isbnList.add(line.trim());
		    		if(cnt==900){		    		
		    			addToReindexList(isbnList, liRein);
		    			isbnList = new ArrayList<String>();
		    			cnt=0;
		    		}		    		
		    		cnt++;		    		
		    	}
		    	
		    	//if item is not in 900 mulitples make sure to add also, which is highly likely
				addToReindexList(isbnList, liRein);
		    } finally {
		        input.close();
		    }
		} catch (IOException ex){
			ex.printStackTrace();
		}
		return liRein;

	}
	
	
	private static void addToReindexList(ArrayList<String> isbnList, ArrayList<ReindexInfoBean> liRein) throws SQLException{
		List<ReindexInfoBean> _liRein = DBInfo.getObjectListManual(isbnList);
		liRein.addAll(_liRein);
	}
	

	
	
	
}
