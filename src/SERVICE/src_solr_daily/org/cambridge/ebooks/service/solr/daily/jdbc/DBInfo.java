package org.cambridge.ebooks.service.solr.daily.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.service.solr.bean.ReindexInfoBean;
import org.cambridge.ebooks.service.solr.daily.util.StringUtil;


public class DBInfo {
	public static List<ReindexInfoBean> getObjectList() throws SQLException{
		Connection conn = JDBCUtil.getConnection();		
		Statement st = null;
		ResultSet rs = null;
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try{
			st = conn.createStatement();
			try{
				String defaultpud = System.getProperty("publisher.id.default");
				String sql =
					/*
					"select dl.isbn, dl.online_flag, dl.pub_date_online modified_date, eb.book_id, decode(pub.publisher_id, null,'" + defaultpud +"', pub.publisher_id) publisher_id, dl.author_singleline " +
					"from ebook_isbn_data_load dl, ebook_publisher_id_isbn pub, " +
					      "(select null book_id,eisbn isbn,status, max(audit_date) audit_date " +
					      "from audit_trail_ebooks " +
					      "where status = 7 " +
					      "group by eisbn, status) eb " +      
					"where dl.isbn = eb.isbn " +					  
					  "and dl.isbn = pub.isbn (+) " +  
					  "and (trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(sysdate-1) = trunc(ESAMPLE_CHANGE_DATE) or trunc(sysdate-1) = trunc(eb.AUDIT_DATE))";
					  */
				"select dl.isbn, dl.online_flag, coalesce(dl.pub_date_online, b.MODIFIED_DATE) modified_date, eb.book_id, decode(pub.publisher_id, null,'" + defaultpud +"', pub.publisher_id) publisher_id, dl.author_singleline " +
				"from ebook_isbn_data_load dl, ebook_publisher_id_isbn pub, ebook b, " +
				      "(select null book_id,eisbn isbn,status, max(audit_date) audit_date " +
				      "from audit_trail_ebooks " +
				      "where status = 7 " +
				      "group by eisbn, status) eb " +
				"where dl.isbn = eb.isbn " +
				  "and dl.isbn = pub.isbn (+) " +                        
				  "and dl.isbn = b.isbn " +
				  "and (trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(sysdate-1) = trunc(ESAMPLE_CHANGE_DATE) or trunc(sysdate-1) = trunc(eb.AUDIT_DATE))";
				System.out.println(sql);
				rs = st.executeQuery(sql);
				try{
					//int cnt = 0;
					while(rs.next()){
						String isbn = rs.getString("isbn");
						String onlineFlag = rs.getString("online_flag");
						Date lastModified = rs.getDate("modified_date");
						String bookId = rs.getString("book_id");
						String publisherId = rs.getString("publisher_id");
						String singleLine = rs.getString("author_singleline");
						java.sql.Date _lastModfied = null;
						if(lastModified != null){
							_lastModfied = new java.sql.Date(lastModified.getTime());
						}						
						ReindexInfoBean rein = new ReindexInfoBean(isbn, onlineFlag, _lastModfied, publisherId, bookId);
						rein.setAuthorSingleline(singleLine);
						liRein.add(rein);		
						//System.out.println(cnt++);
					}
				}finally{
					rs.close();
				}
			}finally{
				st.close();
			}
		}finally{			
			conn.close();
		}
		return liRein;		
	}
	
	public static List<ReindexInfoBean> getObjectListManual(ArrayList<String> isbnList) throws SQLException{
		Connection conn = JDBCUtil.getConnection();		
		Statement st = null;
		ResultSet rs = null;
		List<ReindexInfoBean> liRein = new ArrayList<ReindexInfoBean>();
		try{
			st = conn.createStatement();
			try{
				String sql = generateManualQuery(isbnList);					
				rs = st.executeQuery(sql);
				try{
					//int cnt = 0;
					while(rs.next()){
						String isbn = rs.getString("isbn");
						String onlineFlag = rs.getString("online_flag");
						Date lastModified = rs.getDate("modified_date");
						String bookId = rs.getString("book_id");
						String publisherId = rs.getString("pub_id");
						String singleLine = rs.getString("author_singleline");
						java.sql.Date _lastModfied = null;
						if(lastModified != null){
							_lastModfied = new java.sql.Date(lastModified.getTime());
						}						
						ReindexInfoBean rein = new ReindexInfoBean(isbn, onlineFlag, _lastModfied, publisherId, bookId);
						rein.setAuthorSingleline(singleLine);
						liRein.add(rein);		
						//System.out.println(cnt++);
					}
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					rs.close();
				}
			}finally{
				st.close();
			}
		}finally{			
			conn.close();
		}
		return liRein;		
	}
	
	/*
	 * private methods
	 */
	private static String generateManualQuery(ArrayList<String> isbnList){
		String defaultpud = System.getProperty("publisher.id.default");
		String sql = 
			"select dl.isbn, dl.online_flag, eb.modified_date, eb.book_id, decode(pub.publisher_id, null, '" + defaultpud + "', pub.publisher_id) pub_id, dl.author_singleline " +
			"from ebook_isbn_data_load dl, ebook eb, ebook_publisher_id_isbn pub " +
			"where dl.isbn = eb.isbn " +
			"  and dl.isbn = pub.isbn (+) ";			
		
		StringBuffer sb = new StringBuffer();
		
		if(isbnList.size() > 0){
			sb.append(" and dl.isbn in (");
			sb.append(StringUtil.join(isbnList, "'", ","));
			sb.append(") ");
		}
		
		return sql + sb.toString();
	}
	
	
}
