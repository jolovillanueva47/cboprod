package org.cambridge.ebooks.service.solr.daily.util;

import java.util.List;

public class StringUtil {
	public static String join(List<String> list, String encapsulate, String delimiter){
		encapsulate = encapsulate == null ? "" : encapsulate;
		delimiter = delimiter == null ? "," : delimiter;
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<list.size(); i++){
			if(i < list.size()-1){
				sb.append(encapsulate + list.get(i) + encapsulate + delimiter);
			}else{
				sb.append(encapsulate + list.get(i) + encapsulate);
			}
		}
		
		return sb.toString();
	}
}
