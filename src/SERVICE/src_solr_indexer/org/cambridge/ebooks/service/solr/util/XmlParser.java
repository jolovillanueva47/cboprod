package org.cambridge.ebooks.service.solr.util;

public interface XmlParser {
	public <T> T parse (T objectType, String uri);
}
