package org.cambridge.ebooks.service.solr.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;



public class ReindexInfoBean {
	private String isbn;
	private String onlineFlag;
	private Date lastModified;
	private String publisherId;	
	private String bookId;
	private String authorSingleline;
		
	public ReindexInfoBean(String isbn, String onlineFlag, Date lastModified) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
		this.lastModified = lastModified;
	}
	
	public ReindexInfoBean(String isbn, String onlineFlag, Date lastModified,
			String bookId) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
		this.lastModified = lastModified;
		this.bookId = bookId;
	}	
	
	public ReindexInfoBean(String isbn, String onlineFlag, Date lastModified,
			String publisherId, String bookId) {
		super();
		this.isbn = isbn;
		this.onlineFlag = onlineFlag;
		this.lastModified = lastModified;
		this.publisherId = publisherId;
		this.bookId = bookId;
	}

	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getOnlineFlag() {
		return onlineFlag;
	}
	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getDate_MMMMM_yyyy(){
		return getDateFormat("MMMMM yyyy");
	}
	public String getDate_yyyyMMdd(){
		return getDateFormat("yyyyMMdd");
	}
	public String getDate_ddMMMyyyy(){
		return getDateFormat("ddMMMyyyy");
	}	
	public String getDateFormat(String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(this.lastModified);
	}	
	public String getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}	
	public String getAuthorSingleline() {
		return authorSingleline;
	}
	public void setAuthorSingleline(String authorSingleline) {
		this.authorSingleline = authorSingleline;
	}

	@Override
	public String toString() {
		String sql = 
			"Reindexing book id: " + this.bookId + " onlineFlag: " + this.onlineFlag + " lastModified: " + this.getDate_yyyyMMdd();
		return sql;
	}
	
	public String getFlag(){
//		String flag = "0";
//		if("N".equals(onlineFlag)){
//			flag = "0";
//		}else if("Y".equals(onlineFlag)){
//			flag = "1";
//		}
		
		return onlineFlag;
	}
	
	
}
