package org.cambridge.ebooks.service.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

public class SearchSolrDocument {

	public static final String DELIMITER = "=DEL=";
	
	// BOOK
	@Field("id")
	private String id;
	
	@Field("flag")
	private String flag;
	
	@Field("content_type")
	private String contentType;
		
	@Field("cluster_id")
	private String clusterId;
	
	@Field("book_id")
	private String bookId;
	
	@Field("publisher_id")
	private String publisherId;
	
	@Field("journal_id")
	private String journalId;
		
	@Field("main_title")
	private String mainTitle;
	
	@Field("main_title_alphasort")
	private String mainTitleAlphasort;
		
	@Field("main_subtitle")
	private String mainSubtitle;
		
	@Field("print_date_display")
	private String printDateDisplay;
	
	@Field("print_date")
	private String printDate;
	
	@Field("online_date_display")
	private String onlineDateDisplay;
	
	@Field("online_date")
	private String onlineDate;		
	
	@Field("edition")
	private String edition;
	
	@Field("edition_number")
	private String editionNumber;
	
	@Field("volume_number")
	private String volumeNumber;
	
	@Field("volume_title")
	private String volumeTitle;
	
	@Field("part_number")
	private String partNumber;
	
	@Field("part_title")
	private String partTitle;
	
	@Field("series_code")
	private String seriesCode;
	
	@Field("series")
	private String series;
	
	@Field("series_facet")
	private String seriesFacet;
	
	@Field("series_number")
	private String seriesNumber;
	
	@Field("book_doi")
	private String bookDoi;
	
	@Field("doi")
	private String doi;
	
	@Field("alt_doi")
	private String altDoi;
	
	@Field("isbn_issn")
	private String isbnIssn;
	
	@Field("alt_isbn_paperback")
	private String altIsbnPaperback;
	
	@Field("alt_isbn_hardback")
	private String altIsbnHardback;
	
	@Field("alt_eisbn_eissn")
	private String altEisbnEissn;
	
	@Field("alt_isbn_other")
	private String altIsbnOther;
	
	@Field("author_name")
	private String authorName;		
	
	@Field("author_alphasort")
	private String authorAlphasort;
	
	@Field("author_affiliation")
	private String authorAffiliation;
	
	@Field("author_role")
	private String authorRole;
	
	@Field("author_position")
	private String authorPosition;
	
	@Field("author_singleline")
	private String authorSingleline;
	
	@Field("subject_code")
	private List<String> subjectCodeList;
	
	@Field("subject_level")
	private List<String> subjectLevelList;
	
	@Field("subject")
	private List<String> subjectList;
	
	@Field("subject_facet")
	private List<String> subjectFacet;		
	
	// CONTENT_ITEMS	
	@Field("parent_id")
	private String parentId;
			
	@Field("title")
	private String title;
	
	@Field("title_alphasort")
	private String titleAlphasort;
	
	@Field("subtitle")
	private String subtitle;
		
	@Field("label")
	private String label;
	
	@Field("page_start")
	private String pageStart;
	
	@Field("page_end")
	private String pageEnd;
	
	@Field("type")
	private String type;
	
	@Field("position")
	private String position;
	
	@Field("contributor_name")
	private String contributorName;
	
	@Field("contributor_alphasort")
	private String contributorAlphasort;
	
	@Field("contributor_affiliation")
	private String contributorAffiliation;	
	
	@Field("pdf_filename")
	private String pdfFilename;
	
	@Field("chapter_fulltext")
	private String chapterFulltext;
	
	@Field("keyword_text")
	private List<String> keywordTextList;
	
	@Field("keyword_norms")
	private List<String> keywordNormsList;
	
	@Field("price")
	private List<String> price;
	
	@Field("article_fulltext")
	private String articleFulltext;
	
	@Field("issue_number")
	private String issueNumber;
	
	@Field("abstract")
	private String abstractText;
	
	// FACET
	@Field("book_title_facet")
	private String bookTitleFacet;
	
	@Field("journal_title_facet")
	private String journalTitleFacet;	

	@Field("author_name_facet")
	private List<String> authorNameFacetList;

	
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}

	public String getMainTitle() {
		return mainTitle;
	}

	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	public String getMainTitleAlphasort() {
		return mainTitleAlphasort;
	}

	public void setMainTitleAlphasort(String mainTitleAlphasort) {
		this.mainTitleAlphasort = mainTitleAlphasort;
	}

	public String getMainSubtitle() {
		return mainSubtitle;
	}

	public void setMainSubtitle(String mainSubtitle) {
		this.mainSubtitle = mainSubtitle;
	}

	public String getPrintDateDisplay() {
		return printDateDisplay;
	}

	public void setPrintDateDisplay(String printDateDisplay) {
		this.printDateDisplay = printDateDisplay;
	}

	public String getPrintDate() {
		return printDate;
	}

	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	public String getOnlineDateDisplay() {
		return onlineDateDisplay;
	}

	public void setOnlineDateDisplay(String onlineDateDisplay) {
		this.onlineDateDisplay = onlineDateDisplay;
	}

	public String getOnlineDate() {
		return onlineDate;
	}

	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getVolumeNumber() {
		return volumeNumber;
	}

	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}

	public String getVolumeTitle() {
		return volumeTitle;
	}

	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartTitle() {
		return partTitle;
	}

	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}

	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getBookDoi() {
		return bookDoi;
	}

	public void setBookDoi(String bookDoi) {
		this.bookDoi = bookDoi;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getAltDoi() {
		return altDoi;
	}

	public void setAltDoi(String altDoi) {
		this.altDoi = altDoi;
	}

	public String getIsbnIssn() {
		return isbnIssn;
	}

	public void setIsbnIssn(String isbnIssn) {
		this.isbnIssn = isbnIssn;
	}

	public String getAltIsbnPaperback() {
		return altIsbnPaperback;
	}

	public void setAltIsbnPaperback(String altIsbnPaperback) {
		this.altIsbnPaperback = altIsbnPaperback;
	}

	public String getAltIsbnHardback() {
		return altIsbnHardback;
	}

	public void setAltIsbnHardback(String altIsbnHardback) {
		this.altIsbnHardback = altIsbnHardback;
	}

	public String getAltEisbnEissn() {
		return altEisbnEissn;
	}

	public void setAltEisbnEissn(String altEisbnEissn) {
		this.altEisbnEissn = altEisbnEissn;
	}

	public String getAltIsbnOther() {
		return altIsbnOther;
	}

	public void setAltIsbnOther(String altIsbnOther) {
		this.altIsbnOther = altIsbnOther;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorAlphasort() {
		return authorAlphasort;
	}

	public void setAuthorAlphasort(String authorAlphasort) {
		this.authorAlphasort = authorAlphasort;
	}

	public String getAuthorAffiliation() {
		return authorAffiliation;
	}

	public void setAuthorAffiliation(String authorAffiliation) {
		this.authorAffiliation = authorAffiliation;
	}

	public String getAuthorRole() {
		return authorRole;
	}

	public void setAuthorRole(String authorRole) {
		this.authorRole = authorRole;
	}
	
	public String getAuthorPosition() {
		return authorPosition;
	}

	public void setAuthorPosition(String authorPosition) {
		this.authorPosition = authorPosition;
	}
	
	public String getAuthorSingleline() {
		return authorSingleline;
	}

	public void setAuthorSingleline(String authorSingleline) {
		this.authorSingleline = authorSingleline;
	}

	public List<String> getSubjectCodeList() {
		return subjectCodeList;
	}

	public void setSubjectCodeList(List<String> subjectCodeList) {
		this.subjectCodeList = subjectCodeList;
	}
	
	public List<String> getSubjectLevelList() {
		return subjectLevelList;
	}

	public void setSubjectLevelList(List<String> subjectLevelList) {
		this.subjectLevelList = subjectLevelList;
	}

	public List<String> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}

	public List<String> getSubjectFacet() {
		return subjectFacet;
	}

	public void setSubjectFacet(List<String> subjectFacet) {
		this.subjectFacet = subjectFacet;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleAlphasort() {
		return titleAlphasort;
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContributorName() {
		return contributorName;
	}

	public void setContributorName(String contributorName) {
		this.contributorName = contributorName;
	}

	public String getContributorAlphasort() {
		return contributorAlphasort;
	}

	public void setContributorAlphasort(String contributorAlphasort) {
		this.contributorAlphasort = contributorAlphasort;
	}

	public String getContributorAffiliation() {
		return contributorAffiliation;
	}

	public void setContributorAffiliation(String contributorAffiliation) {
		this.contributorAffiliation = contributorAffiliation;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getChapterFulltext() {
		return chapterFulltext;
	}

	public void setChapterFulltext(String chapterFulltext) {
		this.chapterFulltext = chapterFulltext;
	}

	public List<String> getKeywordTextList() {
		return keywordTextList;
	}

	public void setKeywordTextList(List<String> keywordTextList) {
		this.keywordTextList = keywordTextList;
	}
	
	public List<String> getKeywordNormsList() {
		return keywordNormsList;
	}

	public void setKeywordNormsList(List<String> keywordNormsList) {
		this.keywordNormsList = keywordNormsList;
	}

	public List<String> getPrice() {
		return price;
	}

	public void setPrice(List<String> price) {
		this.price = price;
	}

	public String getArticleFulltext() {
		return articleFulltext;
	}

	public void setArticleFulltext(String articleFulltext) {
		this.articleFulltext = articleFulltext;
	}

	public String getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(String issueNumber) {
		this.issueNumber = issueNumber;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getBookTitleFacet() {
		return bookTitleFacet;
	}

	public void setBookTitleFacet(String bookTitleFacet) {
		this.bookTitleFacet = bookTitleFacet;
	}

	public String getJournalTitleFacet() {
		return journalTitleFacet;
	}

	public void setJournalTitleFacet(String journalTitleFacet) {
		this.journalTitleFacet = journalTitleFacet;
	}

	public List<String> getAuthorNameFacetList() {
		return authorNameFacetList;
	}

	public void setAuthorNameFacetList(List<String> authorNameFacetList) {
		this.authorNameFacetList = authorNameFacetList;
	}

	public String getSeriesFacet() {
		return seriesFacet;
	}

	public void setSeriesFacet(String seriesFacet) {
		this.seriesFacet = seriesFacet;
	}	
}
