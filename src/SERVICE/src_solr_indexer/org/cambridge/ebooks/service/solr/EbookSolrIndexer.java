package org.cambridge.ebooks.service.solr;

import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.ALLOW_COMPRESSION;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.CONNECTION_TIMEOUT;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.CORE_BOOK;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.CORE_CHAPTER;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.CORE_SEARCH;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.DEFAULT_MAX_CONNECTION_PER_HOST;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.FOLLOW_REDIRECTS;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.MAX_RETRIES;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.MAX_TOTAL_CONNECTIONS;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.SOCKET_TIMEOUT;
import static org.cambridge.ebooks.service.solr.constant.ApplicationProperties.SOLR_HOME_URL;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_ALLOW_COMPRESSION;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_CONN_TIMEOUT;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_DEF_MAX_CONN_PER_HOST;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_FOLLOW_REDIRECTS;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_MAX_RETRIES;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_MAX_TOTAL_CONN;
import static org.cambridge.ebooks.service.solr.server.CupSolrServer.KEY_SOCKET_TIMEOUT;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.cambridge.ebooks.service.solr.bean.BookContentItemSolrDocument;
import org.cambridge.ebooks.service.solr.bean.BookMetaDataSolrDocument;
import org.cambridge.ebooks.service.solr.bean.ReindexInfoBean;
import org.cambridge.ebooks.service.solr.bean.SearchSolrDocument;
import org.cambridge.ebooks.service.solr.bean.jaxb.Book;
import org.cambridge.ebooks.service.solr.constant.ApplicationProperties;
import org.cambridge.ebooks.service.solr.server.CupSolrServer;
import org.cambridge.ebooks.service.solr.util.EbookContentBuilder;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;
import org.cambridge.ebooks.service.solr.util.XmlParser;

public class EbookSolrIndexer implements Indexer {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private static SolrServer bookCore;
	private static SolrServer chapterCore;
	private static SolrServer searchCore;
	private static SolrServer pageCore;
			
	private XmlParser xmlParser;
		
	public EbookSolrIndexer(CupSolrServer server, XmlParser xmlParser)  {
		this.xmlParser = xmlParser;		
		
		Map<String, String> serverSettings = new HashMap<String, String>();
		serverSettings.put(KEY_SOCKET_TIMEOUT, String.valueOf(SOCKET_TIMEOUT));
		serverSettings.put(KEY_CONN_TIMEOUT, String.valueOf(CONNECTION_TIMEOUT));
		serverSettings.put(KEY_DEF_MAX_CONN_PER_HOST, String.valueOf(DEFAULT_MAX_CONNECTION_PER_HOST));
		serverSettings.put(KEY_MAX_TOTAL_CONN, String.valueOf(MAX_TOTAL_CONNECTIONS));
		serverSettings.put(KEY_FOLLOW_REDIRECTS, String.valueOf(FOLLOW_REDIRECTS));
		serverSettings.put(KEY_ALLOW_COMPRESSION, String.valueOf(ALLOW_COMPRESSION));
		serverSettings.put(KEY_MAX_RETRIES, String.valueOf(MAX_RETRIES));
		
		bookCore = server.getMultiCore(SOLR_HOME_URL, CORE_BOOK, serverSettings);
		chapterCore = server.getMultiCore(SOLR_HOME_URL, CORE_CHAPTER, serverSettings);
		searchCore = server.getMultiCore(SOLR_HOME_URL, CORE_SEARCH, serverSettings);
		pageCore = server.getMultiCore(SOLR_HOME_URL, ApplicationProperties.CORE_PAGE, serverSettings);
	}

	public void optimize(String indexName) {
		try {
			if(CORE_BOOK.equals(indexName)) {
				bookCore.optimize();
			} else if(CORE_CHAPTER.equals(indexName)) {
				chapterCore.optimize();
			} else if(CORE_SEARCH.equals(indexName)) {
				searchCore.optimize();
			} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {
				pageCore.optimize();
			} else {
				bookCore.optimize();
				chapterCore.optimize();
				searchCore.optimize();
				pageCore.optimize();
			}	
		} catch (Exception e) {
			System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
		}
	}

	public <T> void add(List<T> indexDataList, String indexName) {	
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);	    
	    System.out.println("===== " + sdf.format(cal.getTime()) + " =====");
		System.out.println("=== ADD/UPDATE[" + indexName + "] ===");
		System.out.println("indexDataList size = " + indexDataList.size());
		
		int indexCount = 0;
		int bookCount = 0;
		int docCountToCommit = 0;
		
		for(T t : indexDataList) {		
			
			bookCount++;
			
			String isbn = "";
			String onlineDateText = "";
			String flag = "";
			String publisherId = "";
			String authorSingleline = "";
			BookMetaDataSolrDocument biblioBookDoc = null;
			SearchSolrDocument biblioSearchDoc = null;				
				
			try {	
					
				if(t instanceof Map<?, ?>) {
					Map<String, Object> indexDataMap = (Map<String, Object>)t;
					isbn = (String)indexDataMap.get("isbn");
					onlineDateText = (String)indexDataMap.get("lastModified");
					flag = (String)indexDataMap.get("flag");
					publisherId = (String)indexDataMap.get("publisherId");
					authorSingleline = (String)indexDataMap.get("authorSingleline");
				} else if(t instanceof ReindexInfoBean) {
					ReindexInfoBean reindexInfoBean = (ReindexInfoBean)t;
					isbn = reindexInfoBean.getIsbn();
					onlineDateText = reindexInfoBean.getDate_ddMMMyyyy();
					flag = reindexInfoBean.getFlag();
					publisherId = reindexInfoBean.getPublisherId();
					authorSingleline = reindexInfoBean.getAuthorSingleline();
				} else if(t instanceof BookMetaDataSolrDocument){
					biblioBookDoc = (BookMetaDataSolrDocument)t;
					isbn = biblioBookDoc.getIsbn();
					onlineDateText = biblioBookDoc.getOnlineDate();
					flag = biblioBookDoc.getFlag();
					publisherId = biblioBookDoc.getPublisherId();
					authorSingleline = biblioBookDoc.getAuthorSingleline();
				} else if(t instanceof SearchSolrDocument){
					biblioSearchDoc = (SearchSolrDocument)t;
					isbn = biblioSearchDoc.getIsbnIssn();
					onlineDateText = biblioSearchDoc.getOnlineDate();
					flag = biblioSearchDoc.getFlag();
					publisherId = biblioSearchDoc.getPublisherId();
					authorSingleline = biblioSearchDoc.getAuthorSingleline();
				} else {
					System.out.println("[Exception]indexData type must be Map<String, Object> or ReindexInfoBean");
					return;
				}	
				
				if(StringUtils.isEmpty(authorSingleline)) {
					authorSingleline = "Missing Author";
				}
				
				Book book = null;		
				
				System.out.println("[" + bookCount + "] isbn: " + isbn);	
				
				String headerFile = ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + isbn + ".xml";
				if(new File(headerFile).exists()) {						
					book = xmlParser.parse(new Book(), headerFile);
				} else {
					System.out.println("[Exception]header file does not exists.");
					continue;
				}		
	
				if(CORE_BOOK.equals(indexName)) {
					bookCore.deleteById(book.getId());
					
					BookMetaDataSolrDocument bookSolrDoc = EbookContentBuilder.generateBookMetaDataDocument( 
							book, 
							biblioBookDoc,
							onlineDateText, 
							flag,
							publisherId,
							authorSingleline);					
					bookCore.addBean(bookSolrDoc);
					
					indexCount++;
					docCountToCommit++;
				} else if(CORE_CHAPTER.equals(indexName)) {	
					chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
					
					List<BookContentItemSolrDocument> chapterDocs = EbookContentBuilder.generateBookContentItemDocument( 
							book, 
							onlineDateText, 
							flag, 
							publisherId);				
					
					chapterCore.addBeans(chapterDocs);
					
					indexCount++;
					docCountToCommit++;
				} else if(CORE_SEARCH.equals(indexName)) {		
					searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
					
					List<SearchSolrDocument> searchSolrDocs = EbookContentBuilder.generateSearchDocument(
							book, 
							biblioSearchDoc,
							onlineDateText, 
							flag, 
							publisherId,
							authorSingleline);					
					searchCore.addBeans(searchSolrDocs);			
					
					indexCount++;
					docCountToCommit++;
				} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {		
					pageCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
					
					List<SolrInputDocument> docs = EbookContentBuilder.generatePageDocs(book);					
					pageCore.add(docs);
					
					indexCount++;
					docCountToCommit++;
				} else {
					bookCore.deleteById(book.getId());	
					chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
					searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
					
					BookMetaDataSolrDocument bookSolrDoc = EbookContentBuilder.generateBookMetaDataDocument( 
							book, 
							biblioBookDoc,
							onlineDateText, 
							flag, 
							publisherId,
							authorSingleline);					
					bookCore.addBean(bookSolrDoc);	
					
					List<BookContentItemSolrDocument> chapterDocs = EbookContentBuilder.generateBookContentItemDocument( 
							book,
							onlineDateText, 
							flag, 
							publisherId);					
					chapterCore.addBeans(chapterDocs);
					
					List<SearchSolrDocument> searchSolrDocs = EbookContentBuilder.generateSearchDocument( 
							book,
							biblioSearchDoc,
							onlineDateText, 
							flag, 
							publisherId,
							authorSingleline);					
					searchCore.addBeans(searchSolrDocs);	
					
					
					List<SolrInputDocument> docs = EbookContentBuilder.generatePageDocs(book);					
					pageCore.add(docs);
					
					indexCount++;
					docCountToCommit++;
				}
				
				if(docCountToCommit > (ApplicationProperties.MAX_DOC_COMMIT - 1)) {
					docCountToCommit = 0;
					if(CORE_BOOK.equals(indexName)) {
						bookCore.commit();		
					} else if(CORE_CHAPTER.equals(indexName)) {
						chapterCore.commit();
					} else if(CORE_SEARCH.equals(indexName)) {
						searchCore.commit();
					} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {
						pageCore.commit();
					} else {
						bookCore.commit();		
						chapterCore.commit();
						searchCore.commit();
						pageCore.commit();
					}
				}

			} catch (Exception e) {
				System.out.println("[Exception]isbn:" + isbn); 
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			} 		
		}
		
		try {
			if(docCountToCommit > 0) {
				if(CORE_BOOK.equals(indexName)) {
					bookCore.commit();		
				} else if(CORE_CHAPTER.equals(indexName)) {
					chapterCore.commit();
				} else if(CORE_SEARCH.equals(indexName)) {
					searchCore.commit();
				} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {
					pageCore.commit();
				} else {
					bookCore.commit();		
					chapterCore.commit();
					searchCore.commit();
					pageCore.commit();
				}
			}
		} catch (Exception e) {
			System.out.println("[Exception]commit");
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		System.out.println("Number of books commited to index: " + indexCount++);
	}

	public <T> void add(T t, String indexName) {
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);	    
	    System.out.println("===== " + sdf.format(cal.getTime()) + " =====");
		System.out.println("=== ADD/UPDATE[" + indexName + "] ===");
		Book book = null;
		
		String isbn = "";
		String onlineDateText = "";
		String flag = "";
		String publisherId = "";
		String authorSingleline = "";
		BookMetaDataSolrDocument biblioBookDoc = null;
		SearchSolrDocument biblioSearchDoc = null;				

		try {				
			if(t instanceof Map<?, ?>) {
				Map<String, Object> indexDataMap = (Map<String, Object>)t;
				isbn = (String)indexDataMap.get("isbn");
				onlineDateText = (String)indexDataMap.get("lastModified");
				flag = (String)indexDataMap.get("flag");
				publisherId = (String)indexDataMap.get("publisherId");
				authorSingleline = (String)indexDataMap.get("authorSingleline");
			} else if(t instanceof ReindexInfoBean) {
				ReindexInfoBean bean = (ReindexInfoBean)t;
				isbn = bean.getIsbn();
				onlineDateText = bean.getDate_ddMMMyyyy();
				flag = bean.getFlag();
				publisherId = bean.getPublisherId();
				authorSingleline = bean.getAuthorSingleline();
			} else if(t instanceof BookMetaDataSolrDocument){
				biblioBookDoc = (BookMetaDataSolrDocument)t;
				isbn = biblioBookDoc.getIsbn();
				onlineDateText = biblioBookDoc.getOnlineDate();
				flag = biblioBookDoc.getFlag();
				publisherId = biblioBookDoc.getPublisherId();
				authorSingleline = biblioBookDoc.getAuthorSingleline();
			} else if(t instanceof SearchSolrDocument){
				biblioSearchDoc = (SearchSolrDocument)t;
				isbn = biblioSearchDoc.getIsbnIssn();
				onlineDateText = biblioSearchDoc.getOnlineDate();
				flag = biblioSearchDoc.getFlag();
				publisherId = biblioSearchDoc.getPublisherId();
				authorSingleline = biblioSearchDoc.getAuthorSingleline();
			} else {
				System.out.println("[Exception]indexData type must be Map<String, Object> or ReindexInfoBean");
				return;
			}
			

			if(StringUtils.isEmpty(authorSingleline)) {
				authorSingleline = "Missing Author";
			}

			System.out.println("isbn: " + isbn);		
			
			String headerFile = ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + isbn + ".xml";
			if(new File(headerFile).exists()) {				
				book = xmlParser.parse(new Book(), headerFile);
			} else {
				System.out.println("[Exception]header file does not exists.");
				return;
			}
			
			if(CORE_BOOK.equals(indexName)) {
				bookCore.deleteById(book.getId());	
				
				BookMetaDataSolrDocument bookSolrDoc = EbookContentBuilder.generateBookMetaDataDocument( 
						book,
						biblioBookDoc,
						onlineDateText, 
						flag, 
						publisherId,
						authorSingleline);					
				bookCore.addBean(bookSolrDoc);
			} else if(CORE_CHAPTER.equals(indexName)) {			
				chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
				
				List<BookContentItemSolrDocument> chapterDocs = EbookContentBuilder.generateBookContentItemDocument(
						book, 
						onlineDateText, 
						flag, 
						publisherId);					
				chapterCore.addBeans(chapterDocs);
			} else if(CORE_SEARCH.equals(indexName)) {	
				searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
				
				List<SearchSolrDocument> searchSolrDocs = EbookContentBuilder.generateSearchDocument( 
						book, 
						biblioSearchDoc,
						onlineDateText, 
						flag, 
						publisherId,
						authorSingleline);					
				searchCore.addBeans(searchSolrDocs);		
			} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {		
				pageCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
				
				List<SolrInputDocument> docs = EbookContentBuilder.generatePageDocs(book);					
				pageCore.add(docs);
				
			} else {
				bookCore.deleteById(book.getId());	
				chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
				searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
				
				BookMetaDataSolrDocument bookSolrDoc = EbookContentBuilder.generateBookMetaDataDocument( 
						book, 
						biblioBookDoc,
						onlineDateText, 
						flag, 
						publisherId,
						authorSingleline);					
				bookCore.addBean(bookSolrDoc);	
				
				List<BookContentItemSolrDocument> chapterDocs = EbookContentBuilder.generateBookContentItemDocument( 
						book, 
						onlineDateText, 
						flag, 
						publisherId);					
				chapterCore.addBeans(chapterDocs);
				
				List<SearchSolrDocument> searchSolrDocs = EbookContentBuilder.generateSearchDocument( 
						book, 
						biblioSearchDoc,
						onlineDateText, 
						flag, 
						publisherId,
						authorSingleline);					
				searchCore.addBeans(searchSolrDocs);	
				
				List<SolrInputDocument> docs = EbookContentBuilder.generatePageDocs(book);					
				pageCore.add(docs);
			}
			
			if(CORE_BOOK.equals(indexName)) {
				bookCore.commit();		
			} else if(CORE_CHAPTER.equals(indexName)) {
				chapterCore.commit();
			} else if(CORE_SEARCH.equals(indexName)) {
				searchCore.commit();
			} else if(ApplicationProperties.CORE_PAGE.equals(indexName)) {
				pageCore.commit();
			} else {
				bookCore.commit();		
				chapterCore.commit();
				searchCore.commit();
				pageCore.commit();
			}
		} catch (Exception e) {
			System.out.println("[Exception]isbn:" + isbn); 
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} 
	}		
	
	public void delete(List<String> isbnList, String indexName) {
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);	    
	    System.out.println("===== " + sdf.format(cal.getTime()) + " =====");
		System.out.println("=== DELETE ===");
		int indexCount = 0;
		int bookCount = 0;
		int docCountToCommit = 0;
		for(String isbn : isbnList) {
			bookCount++;
			try {					
				Book book = null;

				System.out.println("[" + bookCount + "] isbn: " + isbn);	
				
				String headerFile = ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + isbn + ".xml";
				if(new File(headerFile).exists()) {				
					book = xmlParser.parse(new Book(), headerFile);
				} else {
					System.out.println("[Exception]header file does not exists.");
					continue;
				}		
	
				if(CORE_BOOK.equals(indexName)) {					
					bookCore.deleteById(book.getId());			
					indexCount++;
					docCountToCommit++;
				} else if(CORE_CHAPTER.equals(indexName)) {
					System.out.println("Delete by query --- isbn:" + book.getMetadata().getIsbn());
					chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
					indexCount++;
					docCountToCommit++;
				}  else if(CORE_SEARCH.equals(indexName)) {
					System.out.println("Delete by query --- isbn_issn:" + book.getMetadata().getIsbn());
					searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
					indexCount++;
					docCountToCommit++;
				} else {
					bookCore.deleteById(book.getId());	
					chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
					searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
					indexCount++;
					docCountToCommit++;
				}
				
				if(docCountToCommit > (ApplicationProperties.MAX_DOC_COMMIT - 1)) {
					docCountToCommit = 0;
					if(CORE_BOOK.equals(indexName)) {
						bookCore.commit();		
					} else if(CORE_CHAPTER.equals(indexName)) {
						chapterCore.commit();
					} else if(CORE_SEARCH.equals(indexName)) {
						searchCore.commit();
					} else {
						bookCore.commit();
						chapterCore.commit();
						searchCore.commit();
					}
				}
			} catch (Exception e) {
				System.out.println("[Exception]isbn:" + isbn); 
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			} 		
		}	

		try {
			if(docCountToCommit > 0) {
				if(CORE_BOOK.equals(indexName)) {
					bookCore.commit();		
				} else if(CORE_CHAPTER.equals(indexName)) {
					chapterCore.commit();
				} else if(CORE_SEARCH.equals(indexName)) {
					searchCore.commit();
				}else {
					bookCore.commit();
					chapterCore.commit();
					searchCore.commit();
				}
			}
		} catch (Exception e) {
			System.out.println("[Exception]commit");
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		System.out.println("Number of books deleted to index: " + indexCount++);		
	}

	public void delete(String isbn, String indexName) {
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);	    
	    System.out.println("===== " + sdf.format(cal.getTime()) + " =====");
		System.out.println("=== DELETE ===");
		try {					
			Book book = null;
			
			System.out.println("isbn: " + isbn);		
			
			String headerFile = ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + isbn + ".xml";
			if(new File(headerFile).exists()) {		
				book = xmlParser.parse(new Book(), headerFile);
			} else {
				System.out.println("[Exception]header file does not exists.");
				return;
			}

			if(CORE_BOOK.equals(indexName)) {					
				bookCore.deleteById(book.getId());			
			} else if(CORE_CHAPTER.equals(indexName)) {	
				System.out.println("Delete by query --- isbn:" + book.getMetadata().getIsbn());
				chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
			} else if(CORE_SEARCH.equals(indexName)) {			
				System.out.println("Delete by query --- isbn_issn:" + book.getMetadata().getIsbn());
				searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
			} else {
				bookCore.deleteById(book.getId());	
				chapterCore.deleteByQuery("isbn:" + book.getMetadata().getIsbn());
				searchCore.deleteByQuery("isbn_issn:" + book.getMetadata().getIsbn());
//				deleteContentItems(book.getContentItems().getContentItem(), chapterCore);
//				deleteContentItems(book.getContentItems().getContentItem(), searchCore);
			}
			
			if(CORE_BOOK.equals(indexName)) {
				bookCore.commit();		
			} else if(CORE_CHAPTER.equals(indexName)) {
				chapterCore.commit();
			} else if(CORE_SEARCH.equals(indexName)) {
				searchCore.commit();
			} else {
				bookCore.commit();
				chapterCore.commit();
				searchCore.commit();
			}
		} catch (Exception e) {
			System.out.println("[Exception]isbn:" + isbn); 
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} 
	}
}
