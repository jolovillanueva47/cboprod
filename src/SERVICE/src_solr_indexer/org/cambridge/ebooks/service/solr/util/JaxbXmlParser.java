package org.cambridge.ebooks.service.solr.util;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class JaxbXmlParser implements XmlParser {

	private JAXBContext jaxbContext;
	private Unmarshaller unmarshaller;
	
	public JaxbXmlParser(String beanPackage) {
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "net.sf.saxon.dom.DocumentBuilderFactoryImpl");
		System.setProperty("javax.xml.xpath.XPathFactory", "net.sf.saxon.xpath.XPathFactoryImpl");
		System.setProperty("javax.xml.parsers.SAXParserFactory", "net.sf.saxon.aelfred.SAXParserFactoryImpl");		
		
		try {
			jaxbContext = JAXBContext.newInstance(beanPackage);
			unmarshaller = jaxbContext.createUnmarshaller();
		} catch (Exception e) {
			System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public <T> T parse(T objectType, String uri) {
		T t = objectType;
		try {
			InputStream fis = new FileInputStream(uri);		
			t = (T)unmarshaller.unmarshal(fis);
		} catch (Exception e) {
			System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
		}		
		return t ;
	}
}
