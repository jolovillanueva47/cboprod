package org.cambridge.ebooks.service.solr.server;

import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;


/**
 * @author Karlson A. Mulingtapang
 * CupSolrServer.java 
 */
public interface CupSolrServer {	
	public static final String KEY_SOCKET_TIMEOUT = "socket.timeout";
	public static final String KEY_CONN_TIMEOUT = "connection.timeout";
	public static final String KEY_DEF_MAX_CONN_PER_HOST = "def.max.connection.per.host";
	public static final String KEY_MAX_TOTAL_CONN = "max.total.connections";
	public static final String KEY_FOLLOW_REDIRECTS = "follow.redirects";
	public static final String KEY_ALLOW_COMPRESSION = "allow.compression";
	public static final String KEY_MAX_RETRIES = "max.retries";
	
	public SolrServer getSingleCore(String solrHome, Map<String, String> settings);
	public SolrServer getMultiCore(String solrHome, String coreName, Map<String, String> settings);
}
