package org.cambridge.ebooks.service.solr.bean.jackson;

public class Author {
	private String id;
	private String position;
	private String role;
	private String name;
	private String alphasort;
	private String affiliation;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlphasort() {
		return alphasort;
	}
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}	
}
