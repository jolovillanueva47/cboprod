package org.cambridge.ebooks.service.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

public class BookContentItemSolrDocument {

	@Field("id")
	private String id;
	
	@Field("parent_id")
	private String parentId;
	
	@Field("book_id")
	private String bookId;
	
	@Field("isbn")
	private String isbn;
	
	@Field("publisher_id")
	private String publisherId;
	
	@Field("flag")
	private String flag;
	
	@Field("content_type")
	private String contentType;		
	
	@Field("author_name")
	private List<String> authorNameList;
	
	@Field("author_name_lf")
	private List<String> authorNameLfList;
	
	@Field("author_name_alphasort")
	private String authorNameAlphasort;
	
	@Field("author_affiliation")
	private List<String> authorAffiliationList;
	
	@Field("author_role")
	private List<String> authorRoleList;	
	
	@Field("doi")
	private String doi;
	
	@Field("page_start")
	private String pageStart;
	
	@Field("page_end")
	private String pageEnd;
	
	@Field("type")
	private String type;
	
	@Field("position")
	private String position;
	
	@Field("label")
	private String label;
	
	@Field("title")
	private String title;
	
	@Field("title_alphasort")
	private String titleAlphasort;
	
	@Field("subtitle")
	private String subtitle;	
	
	@Field("pdf_filename")
	private String pdfFilename;	
	
	@Field("abstract_alt_filename")
	private String abstractFilename;
	
	@Field("abstract_problem")
	private String abstractProblem;
	
	@Field("abstract_text")
	private String abstractText;
	
	@Field("chapter_id")
	private String chapterId;
	
	@Field("toc_text")
	private String tocText;
	
	@Field("keyword_text")
	private List<String> keywordTextList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public List<String> getAuthorNameList() {
		return authorNameList;
	}

	public void setAuthorNameList(List<String> authorNameList) {
		this.authorNameList = authorNameList;
	}

	public List<String> getAuthorNameLfList() {
		return authorNameLfList;
	}

	public void setAuthorNameLfList(List<String> authorNameLfList) {
		this.authorNameLfList = authorNameLfList;
	}

	public String getAuthorNameAlphasort() {
		return authorNameAlphasort;
	}

	public void setAuthorNameAlphasort(String authorNameAlphasort) {
		this.authorNameAlphasort = authorNameAlphasort;
	}

	public List<String> getAuthorAffiliationList() {
		return authorAffiliationList;
	}

	public void setAuthorAffiliationList(List<String> authorAffiliationList) {
		this.authorAffiliationList = authorAffiliationList;
	}

	public List<String> getAuthorRoleList() {
		return authorRoleList;
	}

	public void setAuthorRoleList(List<String> authorRoleList) {
		this.authorRoleList = authorRoleList;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleAlphasort() {
		return titleAlphasort;
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getAbstractFilename() {
		return abstractFilename;
	}

	public void setAbstractFilename(String abstractFilename) {
		this.abstractFilename = abstractFilename;
	}

	public String getAbstractProblem() {
		return abstractProblem;
	}

	public void setAbstractProblem(String abstractProblem) {
		this.abstractProblem = abstractProblem;
	}
	
	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getChapterId() {
		return chapterId;
	}

	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	public String getTocText() {
		return tocText;
	}

	public void setTocText(String tocText) {
		this.tocText = tocText;
	}

	public List<String> getKeywordTextList() {
		return keywordTextList;
	}

	public void setKeywordTextList(List<String> keywordTextList) {
		this.keywordTextList = keywordTextList;
	}
}
