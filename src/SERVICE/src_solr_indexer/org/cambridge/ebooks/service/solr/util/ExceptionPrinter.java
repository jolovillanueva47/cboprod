package org.cambridge.ebooks.service.solr.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionPrinter {

	public static String getStackTraceAsString(Exception exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.print(exception.getMessage());
		exception.printStackTrace(pw);
		return sw.toString();
	}
}
