package org.cambridge.ebooks.service.solr;

import java.util.List;

public interface Indexer {	
	public <T> void add(T indexData, String indexName);
	public <T> void add(List<T> indexDataList, String indexName);
	public void delete(String isbn, String indexName);
	public void delete(List<String> isbnList, String indexName);
	public void optimize(String indexName);
}
