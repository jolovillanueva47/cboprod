package org.cambridge.ebooks.service.solr.constant;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ApplicationProperties {

	private static Configuration config;
	
	static {
		try {
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + ".properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static final String SOLR_HOME_URL = config.getString("solr.home.url");
	
	public static final int SOCKET_TIMEOUT = config.getInt("socket.timeout");
	public static final int CONNECTION_TIMEOUT = config.getInt("connection.timeout");
	public static final int DEFAULT_MAX_CONNECTION_PER_HOST = config.getInt("default.max.connection.per.host");
	public static final int MAX_TOTAL_CONNECTIONS = config.getInt("max.total.connections");
	public static final boolean FOLLOW_REDIRECTS = config.getBoolean("follow.redirects");
	public static final boolean ALLOW_COMPRESSION = config.getBoolean("allow.compression");
	public static final int MAX_RETRIES = config.getInt("max.retries");
	
	public static final String CORE_BOOK = config.getString("core.book");
	public static final String CORE_CHAPTER = config.getString("core.chapter");
	public static final String CORE_SEARCH = config.getString("core.search");
	public static final String CORE_PAGE = config.getString("core.page");
	
	public static final String DIR_EBOOK_CONTENT = config.getString("dir.ebook.content");
	
	public static final String FILE_ISBN_LIST = config.getString("file.isbn.list");
	public static final String FILE_ISBN_DELETE_LIST = config.getString("file.isbn.delete.list");
	public static final String FILE_ISBN_CLC_LIST = config.getString("file.isbn.clc.list");
	
	public static final int MAX_DOC_COMMIT = config.getInt("max.doc.commit");
	
	public static final String ISBN_LIST_DELIMITER = config.getString("isbn.list.delimiter");
}
