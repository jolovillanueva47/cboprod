package org.cambridge.ebooks.service.solr.util;

import static org.cambridge.util.Misc.isNotEmpty;

import java.util.Arrays;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EbookXpathXmlParser implements XmlParser {

	// book
	private static final String BOOK_ID = "/book/@id";
	private static final String BOOK_GROUP = "/book/@group";
	
	// metadata
	private static final String METADATA_MAIN_TITLE = "/book/metadata/main-title";
	private static final String METADATA_MAIN_TITLE_ALPHASORT = "/book/metadata/main-title/@alphasort";	
	private static final String METADATA_SUBTITLE = "/book/metadata/subtitle";
	private static final String METADATA_TRANSTITLE = "/book/metadata/trans-title";
	private static final String METADATA_EDITION = "/book/metadata/edition";
	private static final String METADATA_EDITION_NUMBER = "/book/metadata/edition/@number";
	private static final String METADATA_OTHER_EDITION = "/book/metadata/other-edition";
	private static final String METADATA_VOLUME_NUMBER = "/book/metadata/volume-number";
	private static final String METADATA_VOLUME_TITLE = "/book/metadata/volume-title";
	private static final String METADATA_PART_NUMBER = "/book/metadata/part-number";
	private static final String METADATA_PART_TITLE = "/book/metadata/part-title";
	private static final String METADATA_AUTHOR = "/book/metadata/author";	
	private static final String METADATA_DOI = "/book/metadata/doi";
	private static final String METADATA_ALT_DOI = "/book/metadata/alt-doi";
	private static final String METADATA_ALT_DOI_TYPE = "/book/metadata/alt-doi/@type";
	private static final String METADATA_ISBN = "/book/metadata/isbn";
	private static final String METADATA_ALT_ISBN = "/book/metadata/alt-isbn";
	private static final String METADATA_SERIES = "/book/metadata/series";
	private static final String METADATA_SERIES_NUMBER = "/book/metadata/series/@number";
	private static final String METADATA_SERIES_POSITION = "/book/metadata/series/@position";
	private static final String METADATA_SERIES_CODE = "/book/metadata/series/@code";
	private static final String METADATA_SERIES_ALPHASORT = "/book/metadata/series/@alphasort";
	private static final String METADATA_PUBLISHER_NAME = "/book/metadata/publisher-name";
	private static final String METADATA_PUBLISHER_LOC = "/book/metadata/publisher-loc"; 
	private static final String METADATA_PRINT_DATE = "/book/metadata/pub-dates/print-date";
	private static final String METADATA_ONLINE_DATE = "/book/metadata/pub-dates/online-date";
	private static final String METADATA_COPYRIGHT = "/book/metadata/copyright-statement";
	private static final String METADATA_COPYRIGHT_HOLDER = "/book/metadata/copyright-statement/copyright-holder";
	private static final String METADATA_COPYRIGHT_DATE = "/book/metadata/copyright-statement/copyright-date";
	private static final String METADATA_SUBJECT = "/book/metadata/subject-group/subject";
	private static final String METADATA_BLURB = "/book/metadata/blurb";
	private static final String METADATA_PAGES = "/book/metadata/pages";
	private static final String METADATA_COVER_IMAGE = "/book/metadata/cover-image";
	
	// content-items
	private static final String CONTENT_ITEMS_ID = "/book/content-items/@id";
	private static final String CONTENT_ITEM = "/book/content-items//content-item";
	private static final String CONTENT_ITEM_PDF_FILENAME = "pdf/@filename";
	private static final String CONTENT_ITEM_DOI = "doi";
	private static final String CONTENT_ITEM_LABEL = "heading/label/@alphasort";
	private static final String CONTENT_ITEM_TITLE = "heading/title";
	private static final String CONTENT_ITEM_TITLE_ALPHASORT = "heading/title/@alphasort";
	private static final String CONTENT_ITEM_SUBTITLE = "heading/subtitle";
	private static final String CONTENT_ITEM_CONTRIBUTOR_GROUP = "contributor-group//contributor";
	private static final String CONTENT_ITEM_ABSTRACT = "abstract";
	private static final String CONTENT_ITEM_KEYWORDS = "/keyword-group/keyword/kwd-text";
	private static final String CONTENT_ITEM_TOC = "toc//toc-item";
	
	// common
	private static final String FORENAMES = "name/forenames";
	private static final String SURNAME = "name/surname"; 
	private static final String NAME_LINK = "name/name-link";
	private static final String SUFFIX = "name/suffix";
	private static final String COLLAB = "name/collab";
	private static final String AFFILIATION = "affiliation";
	
	private Document doc;
	private XPathFactory xpathFactory;
	private XPath xpath;

	public <T> T parse(T objectType, String uri) {
		T result = objectType;
		try {
//			init(uri);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		

		try {			
//			Metadata metadata = generateMetadata();		
//			ContentItems contentItems = generateContentItems();
//			result = (T)generateBook(metadata, contentItems);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
//	private ContentItems generateContentItems() throws Exception {
//		ContentItems contentItems = new ContentItems(getContentItemList());
//		contentItems.setId(processNode(getNode(CONTENT_ITEMS_ID)));
//		
//		return contentItems;
//	}
//	
//	private Metadata generateMetadata() throws Exception {
//		
//		Metadata metadata = new Metadata(getAuthorList(), 
//				getPublisherList(), 
//				getCopyrightStatementList(), 
//				getSubjectList(), 
//				getDateList(getNodeList(METADATA_PRINT_DATE)), 
//				getDateList(getNodeList(METADATA_ONLINE_DATE)));
//		
//		metadata.setMainTitle(processHtmlFormattedNode(getNode(METADATA_MAIN_TITLE), "main-title"));
//		metadata.setMainTitleAlphasort(processNode(getNode(METADATA_MAIN_TITLE_ALPHASORT)));
//		metadata.setSubtitle(processHtmlFormattedNode(getNode(METADATA_SUBTITLE), "subtitle"));
//		metadata.setTransTitle(processHtmlFormattedNode(getNode(METADATA_TRANSTITLE), "trans-title"));		
//		metadata.setEdition(processNode(getNode(METADATA_EDITION)));
//		metadata.setEditionNumber(processNode(getNode(METADATA_EDITION_NUMBER)));
//		metadata.setOtherEditionList(getOtherEditionList());
//		metadata.setVolumeNumber(processNode(getNode(METADATA_VOLUME_NUMBER)));
//		metadata.setVolumeTitle(processNode(getNode(METADATA_VOLUME_TITLE)));
//		metadata.setPartNumber(processNode(getNode(METADATA_PART_NUMBER)));
//		metadata.setPartTitle(processNode(getNode(METADATA_PART_TITLE)));
//		metadata.setDoi(processNode(getNode(METADATA_DOI)));		
//		metadata.setAltDoi(processNode(getNode(METADATA_ALT_DOI)));
//		metadata.setAltDoiType(processNode(getNode(METADATA_ALT_DOI_TYPE)));
//		metadata.setIsbn(processNode(getNode(METADATA_ISBN)));
//		metadata.setAltIsbnList(getAltIsbnList());
//		metadata.setSeries(processHtmlFormattedNode(getNode(METADATA_SERIES), "series"));
//		metadata.setSeriesNumber(processNode(getNode(METADATA_SERIES_NUMBER)));
//		metadata.setSeriesPosition(processNode(getNode(METADATA_SERIES_POSITION)));
//		metadata.setSeriesCode(processNode(getNode(METADATA_SERIES_CODE)));
//		metadata.setSeriesAlphasort(processNode(getNode(METADATA_SERIES_ALPHASORT)));
//		metadata.setBlurb(processHtmlFormattedNode(getNode(METADATA_BLURB), "blurb"));
//		metadata.setPages(processNode(getNode(METADATA_PAGES)));
//		metadata.setCoverImageList(getCoverImageList());
//		
//		return metadata;
//	}
//	
//	private List<ContentItem> getContentItemList() throws Exception {
//		List<ContentItem> result = new ArrayList<ContentItem>();
//		
//		NodeList nodeList = getNodeList(CONTENT_ITEM);
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);				
//				Node parentNode = node.getParentNode();
//				
//				if(node != null) {
//					ContentItem contentItem = new ContentItem();				
//					
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//						contentItem.setId(nMap.getNamedItem("id").getTextContent());
//						contentItem.setPageStart(nMap.getNamedItem("page-start").getTextContent());
//						contentItem.setPageEnd(nMap.getNamedItem("page-end").getTextContent());
//						contentItem.setType(nMap.getNamedItem("type").getTextContent());
//						contentItem.setPosition(nMap.getNamedItem("position").getTextContent());
//					}	
//					
//					contentItem.setParentNode(parentNode.getNodeName());
//					if(parentNode.hasAttributes()) {
//						contentItem.setParentId(parentNode.getAttributes().getNamedItem("id").getTextContent());
//					}
//					
//					String xpathContentItem = CONTENT_ITEM + "[@id='" + contentItem.getId() + "']/";
//					
//					contentItem.setPdfFilename(processNode(
//							getNode(xpathContentItem + CONTENT_ITEM_PDF_FILENAME)));
//					contentItem.setDoi(processNode(getNode(xpathContentItem + CONTENT_ITEM_DOI)));
//					contentItem.setLabel(processNode(getNode(xpathContentItem + CONTENT_ITEM_LABEL)));
//					contentItem.setTitle(processHtmlFormattedNode(
//							getNode(xpathContentItem + CONTENT_ITEM_TITLE), "title"));
//					contentItem.setTitleAlphasort(
//							processNode(getNode(xpathContentItem + CONTENT_ITEM_TITLE_ALPHASORT)));
//					contentItem.setSubtitle(processHtmlFormattedNode(
//							getNode(xpathContentItem + CONTENT_ITEM_SUBTITLE), "subtitle"));
//					
//					// ========= contributor-group ==========												
//					contentItem.setContributorList(getContributorList(xpathContentItem));
//					
//					// ========= abstract ==========
//					contentItem.setAbstractText(processHtmlFormattedNode(
//								getNode(xpathContentItem + CONTENT_ITEM_ABSTRACT), "abstract"));
//					
//					// ========= keywords ==========
//					NodeList keywordNodeList = getNodeList(xpathContentItem + CONTENT_ITEM_KEYWORDS);
//					List<String> keywordList = new ArrayList<String>();
//					for(int j = 0; j < keywordNodeList.getLength(); j++) {
//						Node keywordNode = keywordNodeList.item(j);
//						keywordList.add(processHtmlFormattedNode(keywordNode, "kwd-text"));
//					}
//					contentItem.setKeywordList(keywordList);
//					
//					// ========= TOC ==========
//					contentItem.setTocItemList(getTocItemList(xpathContentItem));
//					
//					result.add(contentItem);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<Author> getContributorList(String xpathContentItem) throws Exception {
//		List<Author> result = new ArrayList<Author>();
//		
//		NodeList contributorNodeList = getNodeList(xpathContentItem + CONTENT_ITEM_CONTRIBUTOR_GROUP);
//		
//		for(int i = 0; i < contributorNodeList.getLength(); i++) {
//			Node contributorNode = contributorNodeList.item(i);
//			Author contributor = new Author();
//			if(contributorNode.hasAttributes()) {
//				contributor.setPosition(
//						contributorNode.getAttributes().getNamedItem("position").getTextContent());
//				contributor.setAlphasort(
//						contributorNode.getAttributes().getNamedItem("alphasort").getTextContent());
//			}
//			String xpathContributor = xpathContentItem + 
//				CONTENT_ITEM_CONTRIBUTOR_GROUP + "[@position='" + contributor.getPosition() + "']/";
//			contributor.setForenames(processHtmlFormattedNode(
//					getNode(xpathContributor + FORENAMES), "forenames"));
//			contributor.setSurname(processHtmlFormattedNode(
//					getNode(xpathContributor + SURNAME), "surname"));
//			contributor.setNameLink(processNode(getNode(xpathContributor + NAME_LINK)));
//			contributor.setSuffix(processNode(getNode(xpathContributor + SUFFIX)));
//			contributor.setCollab(processHtmlFormattedNode(
//					getNode(xpathContributor + COLLAB), "collab"));
//			contributor.setAffiliation(processHtmlFormattedNode(
//					getNode(xpathContributor + AFFILIATION), "affiliation"));
//			
//			result.add(contributor);
//		}				
//		
//		return result;
//	}
//	
//	private List<TocItem> getTocItemList(String xpathContentItem) throws Exception {
//		List<TocItem> result = new ArrayList<TocItem>();		
//		
//		NodeList tocItemNodeList = getNodeList(xpathContentItem + CONTENT_ITEM_TOC);
//		
//		for(int i = 0; i < tocItemNodeList.getLength(); i++) {
//			Node tocItemNode = tocItemNodeList.item(i);
//			Node parentNode = tocItemNode.getParentNode();
//			TocItem tocItem = new TocItem();
//			if(tocItemNode.hasAttributes()) {
//				tocItem.setId(
//						tocItemNode.getAttributes().getNamedItem("id").getTextContent());
//				tocItem.setStartPage(
//						tocItemNode.getAttributes().getNamedItem("startpage").getTextContent());
//			}
//			String xpathTocItem = xpathContentItem + 
//					CONTENT_ITEM_TOC + "[@id='" + tocItem.getId() + "']/toc-text";
//			tocItem.setText(processHtmlFormattedNode(getNode(xpathTocItem), "toc-text"));
//			
//			if(parentNode.hasAttributes()) {
//				tocItem.setParentId(parentNode.getAttributes().getNamedItem("id").getTextContent());
//			}
//			tocItem.setParentNode(parentNode.getNodeName());			
//			
//			result.add(tocItem);
//		}				
//		
//		return result;
//	}
//	
//	private List<Subject> getSubjectList() throws Exception {
//		List<Subject> result = new ArrayList<Subject>();
//		
//		NodeList nodeList = getNodeList(METADATA_SUBJECT);
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);				
//				
//				if(node != null) {
//					Subject subject = new Subject();
//					subject.setName(node.getTextContent());
//					
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//						subject.setLevel(nMap.getNamedItem("level").getTextContent());
//						subject.setCode(nMap.getNamedItem("code").getTextContent());
//					}	
//					
//					result.add(subject);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<CoverImage> getCoverImageList() throws Exception {
//		List<CoverImage> result = new ArrayList<CoverImage>();
//		
//		NodeList nodeList = getNodeList(METADATA_COVER_IMAGE);
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);				
//				
//				if(node != null) {
//					CoverImage coverImage = new CoverImage();
//					
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//						coverImage.setType(nMap.getNamedItem("type").getTextContent());
//						coverImage.setFilename(nMap.getNamedItem("filename").getTextContent());
//					}	
//					
//					result.add(coverImage);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<CopyrightStatement> getCopyrightStatementList() throws Exception {
//		List<CopyrightStatement> result = new ArrayList<CopyrightStatement>();
//		
//		NodeList copyright = getNodeList(METADATA_COPYRIGHT);		
//		
//		if(copyright != null){
//			for(int i = 0; i < copyright.getLength(); i++) {
//				Node node = copyright.item(i);
//
//				NodeList holderNodeList = getNodeList(METADATA_COPYRIGHT_HOLDER);
//				NodeList dateNodeList = getNodeList(METADATA_COPYRIGHT_DATE);
//				List<String> holderList = new ArrayList<String>();
//				List<String> dateList = new ArrayList<String>();
//				
//				for(int j = 0; j < holderNodeList.getLength(); j++) {
//					Node holder = holderNodeList.item(j);
//					holderList.add(processHtmlFormattedNode(holder, "copyright-holder"));
//				}
//				
//				for(int j = 0; j < dateNodeList.getLength(); j++) {
//					Node date = dateNodeList.item(j);
//					dateList.add(date.getTextContent());
//				}
//				
//				if(node != null) {
//					CopyrightStatement cs = new CopyrightStatement();
//					cs.setStatement(processHtmlFormattedNode(node, "copyright-statement")
//							.replaceAll("</.*?>", " ").replaceAll("<.*?>", ""));
//					cs.setHolderList(holderList);
//					cs.setDateList(dateList);
//					
//					result.add(cs);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<String> getDateList(NodeList nodeList) {
//		List<String> result = new ArrayList<String>();
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);
//				
//				if(node != null) {
//					result.add(node.getTextContent());	
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<Publisher> getPublisherList() throws Exception {
//		List<Publisher> result = new ArrayList<Publisher>();
//		
//		NodeList nameList = getNodeList(METADATA_PUBLISHER_NAME);
//		NodeList locList = getNodeList(METADATA_PUBLISHER_LOC);
//		
//		if(nameList != null && locList != null){
//			for(int i = 0; i < nameList.getLength(); i++) {
//				Node name = nameList.item(i);
//				Node loc = locList.item(i);
//				
//				if(name != null) {
//					Publisher publisher = new Publisher();
//					publisher.setName(name.getTextContent());	
//					publisher.setLoc(loc.getTextContent());	
//					
//					result.add(publisher);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<AltIsbn> getAltIsbnList() throws Exception {
//		List<AltIsbn> result = new ArrayList<AltIsbn>();
//		NodeList nodeList = getNodeList(METADATA_ALT_ISBN);
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);
//				
//				if(node != null) {
//					AltIsbn altIsbn = new AltIsbn();
//					altIsbn.setIsbn(node.getTextContent());					
//
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//
//						altIsbn.setType(nMap.getNamedItem("type").getTextContent());
//					}	
//					
//					result.add(altIsbn);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private List<Author> getAuthorList() throws Exception {
//		List<Author> result = new ArrayList<Author>(); 
//		NodeList nodeList = getNodeList(METADATA_AUTHOR);
//				
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);
//				
//				if(node != null) {
//					Author author = new Author();										
//
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//
//						author.setId(nMap.getNamedItem("id").getTextContent());
//						author.setPosition(nMap.getNamedItem("position").getTextContent());
//						author.setAlphasort(nMap.getNamedItem("alphasort").getTextContent());
//						author.setRole(nMap.getNamedItem("role").getTextContent());
//					}	
//					
//					String xpathAuthorIdCondition = "[attribute::id='" + author.getId() + "']/";
//					author.setForenames(
//							processHtmlFormattedNode(
//									getNode(METADATA_AUTHOR + xpathAuthorIdCondition + FORENAMES), 
//							"forenames"));					
//					author.setSurname(
//							processHtmlFormattedNode(
//									getNode(METADATA_AUTHOR + xpathAuthorIdCondition + SURNAME), 
//							"surname"));					
//					author.setNameLink(processNode(
//							getNode(METADATA_AUTHOR + xpathAuthorIdCondition + NAME_LINK)));					
//					author.setSuffix(processNode(
//							getNode(METADATA_AUTHOR + xpathAuthorIdCondition + SUFFIX)));					
//					author.setCollab(
//							processHtmlFormattedNode(
//									getNode(METADATA_AUTHOR + xpathAuthorIdCondition + COLLAB), 
//							"collab"));					
//					author.setAffiliation(
//							processHtmlFormattedNode(
//									getNode(METADATA_AUTHOR + xpathAuthorIdCondition + AFFILIATION), 
//							"affiliation"));
//					
//					result.add(author);
//				}
//			}
//		}		
//		
//		return result;
//	}
//	
//	private List<OtherEdition> getOtherEditionList() throws Exception {
//		List<OtherEdition> result = new ArrayList<OtherEdition>();
//		NodeList nodeList = getNodeList(METADATA_OTHER_EDITION);
//		
//		if(nodeList != null){
//			for(int i = 0; i < nodeList.getLength(); i++) {
//				Node node = nodeList.item(i);
//				
//				if(node != null) {
//					OtherEdition otherEdition = new OtherEdition();
//					otherEdition.setEdition(node.getTextContent());					
//
//					if(node.hasAttributes()) {
//						NamedNodeMap nMap = node.getAttributes();
//
//						otherEdition.setNumber(nMap.getNamedItem("number").getTextContent());
//						otherEdition.setIsbn(nMap.getNamedItem("isbn").getTextContent());
//					}	
//					
//					result.add(otherEdition);
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private String processNode(Node node) {
//		String result = "";
//		if(node != null) {
//			result = node.getTextContent();
//			if(isEmpty(result)) {
//				result = node.getNodeValue();
//			}
//		}
//		
//		return result;
//	}
//		
//	private void init(String uri) throws Exception {
////		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "net.sf.saxon.dom.DocumentBuilderFactoryImpl");
////		System.setProperty("javax.xml.xpath.XPathFactory", "net.sf.saxon.xpath.XPathFactoryImpl");
////		System.setProperty("javax.xml.parsers.SAXParserFactory", "net.sf.saxon.aelfred.SAXParserFactoryImpl");		
//				
//		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
////		domFactory.setNamespaceAware(true);
//		
//		File file = new File(uri);
//		doc = domFactory.newDocumentBuilder().parse(file);
//		xpathFactory = XPathFactory.newInstance();
//		xpath = xpathFactory.newXPath();
//	}
//	
//	private NodeList getNodeList(String expression) throws Exception {	
//		XPathExpression xpathExpression = xpath.compile(expression);
////		XPathExpression xpathExpression = XPathFactory.newInstance().newXPath().compile(expression);
//		return (NodeList)xpathExpression.evaluate(doc, NODESET);
//	}
//	
//	private Node getNode(String expression) throws Exception {	
//		XPathExpression xpathExpression = xpath.compile(expression);
////		XPathExpression xpathExpression = XPathFactory.newInstance().newXPath().compile(expression);
//		NodeList nList = (NodeList)xpathExpression.evaluate(doc, NODESET);
//		Node n = null;
//		if(nList != null) {
//			n = nList.item(0);
//		}
//		return n;
//	}
//	
//	private NodeList getNewXpathNodeList(String expression) throws Exception {	
//		XPathExpression xpathExpression = XPathFactory.newInstance().newXPath().compile(expression);
//		return (NodeList)xpathExpression.evaluate(doc, NODESET);
//	}
//	
//	private Node getNewXpathNode(String expression) throws Exception {	
//		XPathExpression xpathExpression = XPathFactory.newInstance().newXPath().compile(expression);
//		NodeList nList = (NodeList)xpathExpression.evaluate(doc, NODESET);
//		Node n = null;
//		if(nList != null) {
//			n = nList.item(0);
//		}
//		return n;
//	}
//	
//	private Book generateBook(Metadata metadata, ContentItems contentItems) throws Exception {
//		Book book = new Book(metadata, contentItems);
//		book.setId(processNode(getNode(BOOK_ID)));
//		book.setGroup(processNode(getNode(BOOK_GROUP)));		
//		return book;
//	}
	
	private String processHtmlFormattedNode(Node node, String nodeTag) {
		String result = "";
			
		if(node != null){
			if(nodeTag.equalsIgnoreCase(node.getNodeName())){
				result = (cleanTextNode(getChildNodeTree(nodeTag, node))).replaceAll("'", "''");				
			}				
		}
		
		return result;
	}
	
	private static String cleanTextNode(String val) {
		if (isNotEmpty(val)) {
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll("\\s+", " ");  //.trim()
		}
		return val;
	}
	
	private static String getChildNodeTree(final String parentName, Node node) {
		
		StringBuilder result = new StringBuilder();
		StringBuilder attributes = new StringBuilder();
		String root = null;
		
		if(node != null && node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			if(node.hasAttributes() && !excludeTags().contains(node.getNodeName())) {
				NamedNodeMap nmap = node.getAttributes();
				for(int index=0; index<nmap.getLength(); index++) {
					Node attributeNode = nmap.item(index);
					if(attributeNode != null && !attributeNode.getNodeName().equals("xmlns:xml")) {
						attributes.append(" ").append(attributeNode.getNodeName()).append("=\"").append(attributeNode.getTextContent()).append("\"");						
					}
				}
			}
			
			root = node.getNodeName();
			
			for(int index=0; index<nodeList.getLength(); index++) {
				Node nodeFromList = nodeList.item(index);	
				if(nodeFromList != null ) {					
					if(!nodeFromList.hasChildNodes()) {
						result.append(nodeFromList.getTextContent());
					} else {
						result.append(getChildNodeTree(parentName, nodeFromList));
					}
				}
			}
			
			if(root != null && !root.equalsIgnoreCase(parentName) && !excludeTags().contains(root)) {
				result.insert(0, "[LTHAN]" + root + attributes.toString() + "[GTHAN]").append("[LTHAN]/" + root + "[GTHAN]");	
			}
			
		}
		
		if(node != null && node.getNodeName().equals(parentName)) {
			result = new StringBuilder(convertToUTF8(result.toString()));
			result = new StringBuilder(result.toString().replace("[LTHAN]", "<").replace("[GTHAN]", ">"));
			result = new StringBuilder(correctHtmlTags(result.toString()));
		}
		
		return result.toString();		
	}
	
	private static List<String> excludeTags() {
		String[] tags = {"book-title", "journal-title", "chapter-title",
				         "volume", "issue", "article-title", "startpage",
				         "author", "name", "forenames", "surname", "name-link",
				         "suffix", "collab", "affiliation",	"publisher-name", 
				         "publisher-loc", "year" };
		
		return Arrays.asList(tags);
	}
	
	private static String convertToUTF8(String toUTF8) {
		StringBuilder utf8 = new StringBuilder();
		if(toUTF8 != null && toUTF8.length() > 0) {
			utf8 = new StringBuilder(toUTF8);
			try {
				//if( !isUTF8(utf8.toString()) ) //removed because of UTF-8 checking issue on solaris
				//{
					boolean isDone = false;
					while(!isDone) {
						int index = 0;
						char[] c = utf8.toString().toCharArray();
						for(index=0; index<c.length; index++) {
							int intValue = (int)c[index];
							if(intValue == 38 || intValue == 60 || intValue == 62 || intValue > 127) { //0 -127 common, no need to check							
								String stringValue = Character.toString(c[index]);
								String temp = new String(stringValue.getBytes("ISO-8859-1"), "UTF-8");
								if(temp != null) {
									if(temp.equals("?") || stringValue.equals("<") || stringValue.equals(">") || intValue > 160) { //160 up are special characters 
										utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
										break;
									} else if(temp.equals("&"))	{
										if((c.length == 1) || (index != c.length - 1 && c[index + 1] != '#') || 
										   (index == c.length - 1)) {
											utf8.replace(index, index + 1, "&#" + (int)c[index] + ";");
											break;
										}										
									}
								}
							}
						}
						
						if(index == utf8.toString().length()) isDone = true;
					}
				//}
				
			} catch (Exception e) {
				System.err.println("[Exception] convertToUTF8() " + e.getMessage());
			}
			
		}
		return utf8.toString();
	}
	
	private static String correctHtmlTags(String text) {		
		String output = "";
		try {
			output = text.replaceAll("<italic>", "<i>")
			.replaceAll("</italic>", "</i>")
			.replaceAll("<block>", "<blockquote id=\"indent\">")
			.replaceAll("</block>", "</blockquote>")
			.replaceAll("<underline>", "<u>")
			.replaceAll("</underline>", "</u>")
			.replaceAll("<bold>", "<b>")
			.replaceAll("</bold>", "</b>")
			.replaceAll("<list-item>", "<li>")
			.replaceAll("</list-item>", "</li>")
			.replaceAll("<list", "<ul")
			.replaceAll("style=\"", "style=\"list-style:")
			.replaceAll("style=\"list-style:bullet", "style=\"padding: 20px 20px 20px 30px; list-style:disc !important;")
			.replaceAll("style=\"list-style:number", "style=\"padding: 20px 20px 20px 30px; list-style:decimal !important;")
			.replaceAll("style=\"list-style:none", "style=\"padding: 20px 20px 20px 30px; list-style:none !important;")
			.replaceAll("</list>", "</ul>")
			.replaceAll("<small-caps>", "<span style='font-variant:small-caps'>")
			.replaceAll("</small-caps>", "</span>")
			.replaceAll("<source>", "<p align=\"right\">")
			.replaceAll("</source>", "</p>");
		} catch (Exception e) {
			output = "";
		}
		
		return output;
	}	
}
