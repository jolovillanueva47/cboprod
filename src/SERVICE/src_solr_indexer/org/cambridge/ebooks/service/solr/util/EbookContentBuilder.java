package org.cambridge.ebooks.service.solr.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.solr.common.SolrInputDocument;
import org.cambridge.ebooks.service.solr.bean.BookContentItemSolrDocument;
import org.cambridge.ebooks.service.solr.bean.BookMetaDataSolrDocument;
import org.cambridge.ebooks.service.solr.bean.SearchSolrDocument;
import org.cambridge.ebooks.service.solr.bean.jaxb.AltIsbn;
import org.cambridge.ebooks.service.solr.bean.jaxb.Author;
import org.cambridge.ebooks.service.solr.bean.jaxb.Block;
import org.cambridge.ebooks.service.solr.bean.jaxb.Bold;
import org.cambridge.ebooks.service.solr.bean.jaxb.Book;
import org.cambridge.ebooks.service.solr.bean.jaxb.Collab;
import org.cambridge.ebooks.service.solr.bean.jaxb.ContentItem;
import org.cambridge.ebooks.service.solr.bean.jaxb.Contributor;
import org.cambridge.ebooks.service.solr.bean.jaxb.CopyrightHolder;
import org.cambridge.ebooks.service.solr.bean.jaxb.CopyrightStatement;
import org.cambridge.ebooks.service.solr.bean.jaxb.CoverImage;
import org.cambridge.ebooks.service.solr.bean.jaxb.Forenames;
import org.cambridge.ebooks.service.solr.bean.jaxb.Italic;
import org.cambridge.ebooks.service.solr.bean.jaxb.Keyword;
import org.cambridge.ebooks.service.solr.bean.jaxb.KeywordGroup;
import org.cambridge.ebooks.service.solr.bean.jaxb.ListItem;
import org.cambridge.ebooks.service.solr.bean.jaxb.P;
import org.cambridge.ebooks.service.solr.bean.jaxb.SmallCaps;
import org.cambridge.ebooks.service.solr.bean.jaxb.Source;
import org.cambridge.ebooks.service.solr.bean.jaxb.Sub;
import org.cambridge.ebooks.service.solr.bean.jaxb.Subject;
import org.cambridge.ebooks.service.solr.bean.jaxb.Sup;
import org.cambridge.ebooks.service.solr.bean.jaxb.Surname;
import org.cambridge.ebooks.service.solr.bean.jaxb.TocItem;
import org.cambridge.ebooks.service.solr.bean.jaxb.Underline;
import org.cambridge.ebooks.service.solr.constant.ApplicationProperties;


public class EbookContentBuilder {	
		
	public static final String FORMAT_DATE_TEXT = "MMMMM yyyy";
	public static final String FORMAT_DATE_NUM = "yyyyMMdd";
		
	
	public static List<SearchSolrDocument> generateSearchDocument(Book book, SearchSolrDocument biblioDoc, 
			String onlineDateText, String flag, String publisherId, String authorSingleline) throws Exception {
		
		List<SearchSolrDocument> result = null;		
		
		try {
			result = generateSearchData(book, biblioDoc, onlineDateText, flag, publisherId, authorSingleline);
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	public static List<SolrInputDocument> generatePageDocs(Book book) throws Exception  {
		List<SolrInputDocument> result = null;
		
		try {
			result = generatePage(book.getContentItems().getContentItem(), book.getId(), book.getMetadata().getIsbn());
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	public static BookMetaDataSolrDocument generateBookMetaDataDocument(Book book, BookMetaDataSolrDocument biblioDoc, 
			String onlineDateText, String flag, String publisherId, String authorSingleline) throws Exception {
		
		BookMetaDataSolrDocument result = null;	
		
		try {
			result = generateBookData(book, biblioDoc, onlineDateText, flag, publisherId, authorSingleline);
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	public static List<BookContentItemSolrDocument> generateBookContentItemDocument(Book book, String onlineDateText, String flag, 
			String publisherId) throws Exception {
		List<BookContentItemSolrDocument> result = null;
		
		try {
			result = generateChapterData(book, onlineDateText, flag, publisherId);
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	private static BookMetaDataSolrDocument generateBookData(Book book, BookMetaDataSolrDocument biblioDoc, 
			String onlineDateText, String flag, String publisherId, String authorSingleline) throws Exception {
		
		BookMetaDataSolrDocument doc = new BookMetaDataSolrDocument();
		
		doc.setFlag(flag);
		doc.setId(book.getId());
		doc.setBookId(book.getId());
		doc.setContentType("book");
		doc.setPublisherId(publisherId);
		doc.setTitle(getContent(book.getMetadata().getMainTitle().getContent()));
		doc.setTitleAlphasort(book.getMetadata().getMainTitle().getAlphasort().toLowerCase());
		
		if(null != book.getMetadata().getSubtitle()) {
			doc.setSubtitle(getContent(book.getMetadata().getSubtitle().getContent()));
		}
		
		String printDate = EbookContentBuilder.getPrintDate(book.getMetadata().getPubDates().getPrintDate());
		Date onlineDate = EbookContentBuilder.getOnlineDate(onlineDateText);
		String onlineDateStr = EbookContentBuilder.getFormatedDate(onlineDate, EbookContentBuilder.FORMAT_DATE_NUM); 
		
		doc.setPrintDateDisplay(printDate);
		doc.setPrintDate(printDate);		
		doc.setOnlineDateDisplay(EbookContentBuilder.getFormatedDate(onlineDate, EbookContentBuilder.FORMAT_DATE_TEXT));
		doc.setOnlineDate(onlineDateStr);			
		
		if(null != book.getMetadata().getEdition()) {
			doc.setEdition(book.getMetadata().getEdition().getContent());
			doc.setEditionNumber(book.getMetadata().getEdition().getNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeNumber())) {
			doc.setVolumeNumber(book.getMetadata().getVolumeNumber());
		}		
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeTitle())) {
			doc.setVolumeTitle(book.getMetadata().getVolumeTitle());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartNumber())) {
			doc.setPartNumber(book.getMetadata().getPartNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartTitle())) {
			doc.setPartTitle(book.getMetadata().getPartTitle());
		}
		
		if(null != book.getMetadata().getSeries()) {
			doc.setSeriesCode(book.getMetadata().getSeries().getCode());
			doc.setSeries(getContent(book.getMetadata().getSeries().getContent()));
			doc.setSeriesAlphasort(book.getMetadata().getSeries().getAlphasort());
			if(StringUtils.isNotEmpty(book.getMetadata().getSeries().getNumber())) {
				doc.setSeriesNumber(Integer.parseInt(book.getMetadata().getSeries().getNumber()));
			}
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getDoi())) {
			doc.setDoi(book.getMetadata().getDoi());
		}
		
		if(null != book.getMetadata().getAltDoi()) {
			doc.setAltDoi(book.getMetadata().getAltDoi().getContent());
		}
		
		doc.setIsbn(book.getMetadata().getIsbn());
		
		for(AltIsbn altIsbn : book.getMetadata().getAltIsbn()) {
			if("paperback".equalsIgnoreCase(altIsbn.getType())) {
				doc.setAltIsbnPaperback(altIsbn.getContent());
			} else if("hardback".equalsIgnoreCase(altIsbn.getType())) {
				doc.setAltIsbnHardback(altIsbn.getContent());
			} else if("e-isbn".equalsIgnoreCase(altIsbn.getType())) {
				doc.setAltIsbnEisbn(altIsbn.getContent());
			} else if("other".equalsIgnoreCase(altIsbn.getType())) {
				doc.setAltIsbnOther(altIsbn.getContent());
			}
		}
		
		List<String> coverFilenameList = new ArrayList<String>();
		List<String> coverTypeList = new ArrayList<String>();
		for(CoverImage cover : book.getMetadata().getCoverImage()) {
			coverFilenameList.add(cover.getFilename());
			coverTypeList.add(cover.getType());
		}
		doc.setCoverImageFilenameList(coverFilenameList);
		doc.setCoverImageTypeList(coverTypeList);
		
		List<String> authorNameList = new ArrayList<String>();		
		List<String> authorNameLfList = new ArrayList<String>();
		List<String> authorAffiliationList = new ArrayList<String>();		
		List<String> authorRoleList = new ArrayList<String>();		
		List<String> authorPositionList = new ArrayList<String>();
		StringBuilder authorNameAlphasort = new StringBuilder();
		int ctr = 0;
		for(Author author : book.getMetadata().getAuthor()) {
			ctr++;
			authorNameList.add(getFirstLastName(author.getName().getContent()));
			authorNameLfList.add(getLastFirstName(author.getName().getContent()));
			authorRoleList.add(author.getRole());
			authorPositionList.add(author.getPosition());
			if(null != author.getAffiliation()) {
				authorAffiliationList.add(getContent(author.getAffiliation().getContent()));
			} else {
				authorAffiliationList.add("none");
			}
			authorNameAlphasort.append(author.getAlphasort());
			if(ctr < book.getMetadata().getAuthor().size()) {
				authorNameAlphasort.append(" ");
				authorNameAlphasort.append(SearchSolrDocument.DELIMITER);
				authorNameAlphasort.append(" ");
			}
		}				
		doc.setAuthorNameList(authorNameList);
		doc.setAuthorNameLfList(authorNameLfList);
		doc.setAuthorNameAlphasort(authorNameAlphasort.toString());
		doc.setAuthorAffiliationList(authorAffiliationList);
		doc.setAuthorRoleList(authorRoleList);
		doc.setAuthorPositionList(authorPositionList);
		if(StringUtils.isNotEmpty(authorSingleline)) {
			doc.setAuthorSingleline(authorSingleline);
		}
		
		List<String> subjectCodeList = new ArrayList<String>();
		List<String> subjectList = new ArrayList<String>();
		List<String> subjectLevelList = new ArrayList<String>();
		for(Subject subject : book.getMetadata().getSubjectGroup().getSubject()) {
			subjectCodeList.add(subject.getCode());
			subjectList.add(subject.getContent());
			subjectLevelList.add(subject.getLevel());
		}
		doc.setSubjectCodeList(subjectCodeList);
		doc.setSubjectList(subjectList);
		doc.setSubjectLevelList(subjectLevelList);	
		
		if(null != book.getMetadata().getBlurb()) {
			StringBuilder blurb = new StringBuilder();
			for(P p : book.getMetadata().getBlurb().getP()) {
				blurb.append(getContent(p.getContent()));			
			}
//			File dir = new File(ApplicationProperties.DIR_EBOOK_FILE_DUMP + doc.getIsbn());
//			if(!dir.exists()) {
//				dir.mkdir();	
//			}
//			
//			File file = new File(ApplicationProperties.DIR_EBOOK_FILE_DUMP + doc.getIsbn() + "/" + doc.getIsbn() + "_blurb.txt");				
//			FileWriter fileWriter = new FileWriter(file);
//			fileWriter.write(blurb.toString());
//			fileWriter.close();
			doc.setBlurb(blurb.toString());
		}
		
		setPublisher(book.getMetadata().getPublisherNameAndPublisherLoc(), doc);
		
		doc.setCopyrightStatementList(getCopyrightStatement(book.getMetadata().getCopyrightStatement()));
		
		// start ==== biblio data
		if(null != biblioDoc) {
			if(StringUtils.isNotEmpty(biblioDoc.getId())) {
				doc.setId(biblioDoc.getId());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getBookId())) {
				doc.setBookId(biblioDoc.getBookId());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getIsbn())) {
				doc.setIsbn(biblioDoc.getIsbn());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getSeriesCode())) {
				doc.setSeriesCode(biblioDoc.getSeriesCode());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getSeries())) {
				doc.setSeries(biblioDoc.getSeries());
			}
			
			if(biblioDoc.getSeriesNumber() > 0) {
				doc.setSeriesNumber(biblioDoc.getSeriesNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getTitle())) {
				doc.setTitle(biblioDoc.getTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getSubtitle())) {
				doc.setSubtitle(biblioDoc.getSubtitle());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getVolumeNumber())) {
				doc.setVolumeNumber(biblioDoc.getVolumeNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getVolumeTitle())) {
				doc.setVolumeTitle(biblioDoc.getVolumeTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getPartNumber())) {
				doc.setPartNumber(biblioDoc.getPartNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getPartTitle())) {
				doc.setPartTitle(biblioDoc.getPartTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getEditionNumber())) {
				doc.setEditionNumber(biblioDoc.getEditionNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getPrintDate())) {
				doc.setPrintDate(biblioDoc.getPrintDate());
				doc.setPrintDateDisplay(biblioDoc.getPrintDate());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getAltIsbnHardback())) {
				doc.setAltIsbnHardback(biblioDoc.getAltIsbnHardback());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getAltIsbnPaperback())) {
				doc.setAltIsbnPaperback(biblioDoc.getAltIsbnPaperback());
			}
			
			if(null != biblioDoc.getAuthorPositionList() &&  biblioDoc.getAuthorPositionList().size() > 0) {
				int authCtr = 0;
				for(String s : biblioDoc.getAuthorPositionList()) {
					int position = Integer.parseInt(s);
					int index = position - 1;
					
					if(position <= doc.getAuthorNameList().size()) {
						doc.getAuthorNameList().set(index, biblioDoc.getAuthorNameList().get(authCtr));
						doc.getAuthorNameLfList().set(index, biblioDoc.getAuthorNameLfList().get(authCtr));						
						doc.getAuthorRoleList().set(index, biblioDoc.getAuthorRoleList().get(authCtr));						
						doc.getAuthorAffiliationList().set(index, biblioDoc.getAuthorAffiliationList().get(authCtr));
					} else {
						doc.getAuthorNameList().add(biblioDoc.getAuthorNameList().get(authCtr));
						doc.getAuthorNameLfList().add(biblioDoc.getAuthorNameLfList().get(authCtr));
						doc.getAuthorRoleList().add(biblioDoc.getAuthorRoleList().get(authCtr));
						doc.getAuthorAffiliationList().add(biblioDoc.getAuthorAffiliationList().get(authCtr));
						doc.getAuthorPositionList().add(s);
					}
					authCtr++;
				}
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getAuthorSingleline())) {
				doc.setAuthorSingleline(biblioDoc.getAuthorSingleline());
			}
			
			if(StringUtils.isNotEmpty(biblioDoc.getAuthorNameAlphasort())) {
				doc.setAuthorNameAlphasort(biblioDoc.getAuthorNameAlphasort());
			}
			
			if(null != biblioDoc.getSubjectCodeList() && biblioDoc.getSubjectCodeList().size() > 0) {
				doc.setSubjectCodeList(biblioDoc.getSubjectCodeList());
				doc.setSubjectLevelList(biblioDoc.getSubjectLevelList());
				doc.setSubjectList(biblioDoc.getSubjectList());
				
//				int i = 0;
//				for(String biblioSubjectCode : biblioDoc.getSubjectCodeList()) {					
//					int j = 0;
//					boolean isSubjectUpdated = false;
//					for(String subjectCode : doc.getSubjectCodeList()) {						
//						if(biblioSubjectCode.equals(subjectCode)) {						
//							isSubjectUpdated = true;
//							doc.getSubjectLevelList().set(j, biblioDoc.getSubjectLevelList().get(i));
//							doc.getSubjectList().set(j, biblioDoc.getSubjectList().get(i));
//							break;
//						}		
//						j++;
//					}
//					
//					if(!isSubjectUpdated) {
//						doc.getSubjectLevelList().add(biblioDoc.getSubjectLevelList().get(i));
//						doc.getSubjectList().add(biblioDoc.getSubjectLevelList().get(i));
//						doc.getSubjectCodeList().add(biblioSubjectCode);
//					}					
//					i++;
//				}			
			}
		}
		// end ==== biblio data	
		
		return doc;
	}
	
	private static List<BookContentItemSolrDocument> generateChapterData(Book book, String onlineDateText, 
			String flag, String publisherId) throws Exception {
		
		List<BookContentItemSolrDocument> resultList = new ArrayList<BookContentItemSolrDocument>();
		BookMetaDataSolrDocument doc = new BookMetaDataSolrDocument();
		
		doc.setFlag(flag);		
		doc.setIsbn(book.getMetadata().getIsbn());
		doc.setId(book.getId());
		doc.setPublisherId(publisherId);
		
		resultList.addAll(generateChapterData(book.getContentItems().getContentItem(), "root", doc));
		
		return resultList;
	}
	
	private static List<SearchSolrDocument> generateSearchData(Book book, SearchSolrDocument biblioSearchDoc, 
			String onlineDateText, String flag, String publisherId, String authorSingleline) throws Exception {
		SearchSolrDocument result = new SearchSolrDocument();
		
		result.setFlag(flag);
		result.setClusterId(book.getId());
		result.setBookId(book.getId());
		result.setPublisherId(publisherId);
		result.setMainTitle(getContent(book.getMetadata().getMainTitle().getContent()));
		result.setMainTitleAlphasort(book.getMetadata().getMainTitle().getAlphasort().toLowerCase());
		result.setBookTitleFacet(getContent(book.getMetadata().getMainTitle().getContent()));
		
		if(null != book.getMetadata().getSubtitle()) {
			result.setMainSubtitle(getContent(book.getMetadata().getSubtitle().getContent()));
		}
		
		String printDate = getPrintDate(book.getMetadata().getPubDates().getPrintDate());
		Date onlineDate = getOnlineDate(onlineDateText);
		String onlineDateStr = getFormatedDate(onlineDate, EbookContentBuilder.FORMAT_DATE_NUM); 
		
		result.setPrintDateDisplay(printDate);
		result.setPrintDate(printDate);		
		result.setOnlineDateDisplay(getFormatedDate(onlineDate, EbookContentBuilder.FORMAT_DATE_TEXT));
		result.setOnlineDate(onlineDateStr);		
		
		if(null != book.getMetadata().getEdition()) {
			result.setEdition(book.getMetadata().getEdition().getContent());
			result.setEditionNumber(book.getMetadata().getEdition().getNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeNumber())) {
			result.setVolumeNumber(book.getMetadata().getVolumeNumber());
		}		
		
		if(StringUtils.isNotEmpty(book.getMetadata().getVolumeTitle())) {
			result.setVolumeTitle(book.getMetadata().getVolumeTitle());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartNumber())) {
			result.setPartNumber(book.getMetadata().getPartNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getPartTitle())) {
			result.setPartTitle(book.getMetadata().getPartTitle());
		}
		
		if(null != book.getMetadata().getSeries()) {
			result.setSeriesCode(book.getMetadata().getSeries().getCode());
			result.setSeries(getContent(book.getMetadata().getSeries().getContent()));
			result.setSeriesFacet(result.getSeries());
			result.setSeriesNumber(book.getMetadata().getSeries().getNumber());
		}
		
		if(StringUtils.isNotEmpty(book.getMetadata().getDoi())) {
			result.setBookDoi(book.getMetadata().getDoi());
		}
		
		if(null != book.getMetadata().getAltDoi()) {
			result.setAltDoi(book.getMetadata().getAltDoi().getContent());
		}
		
		result.setIsbnIssn(book.getMetadata().getIsbn());
		
		for(AltIsbn altIsbn : book.getMetadata().getAltIsbn()) {
			if("paperback".equalsIgnoreCase(altIsbn.getType())) {
				result.setAltIsbnPaperback(altIsbn.getContent());
			} else if("hardback".equalsIgnoreCase(altIsbn.getType())) {
				result.setAltIsbnHardback(altIsbn.getContent());
			} else if("e-isbn".equalsIgnoreCase(altIsbn.getType())) {
				result.setAltEisbnEissn(altIsbn.getContent());
			} else if("other".equalsIgnoreCase(altIsbn.getType())) {
				result.setAltIsbnOther(altIsbn.getContent());
			}
		}
		
		StringBuilder authorName = new StringBuilder();
		List<String> authorNameFacetList = new ArrayList<String>();
		StringBuilder authorAlphasort = new StringBuilder();
		StringBuilder authorAffiliation = new StringBuilder();		
		StringBuilder authorRole = new StringBuilder();	
		StringBuilder authorPosition = new StringBuilder();
		int ctr = 0;
		for(Author author : book.getMetadata().getAuthor()) {
			ctr++;
			authorName.append(getFirstLastName(author.getName().getContent()));
			authorNameFacetList.add(fixAuthorNameFacet(getFirstLastName(author.getName().getContent())));
			
			// get only first author
			if(ctr == 1) {
				authorAlphasort.append(author.getAlphasort());
			}
			
			authorRole.append(author.getRole());
			authorPosition.append(author.getPosition());
			if(ctr < book.getMetadata().getAuthor().size()) {
				authorName.append(" ");
				authorName.append(SearchSolrDocument.DELIMITER);
				authorName.append(" ");				
				
				authorRole.append(" ");
				authorRole.append(SearchSolrDocument.DELIMITER);
				authorRole.append(" ");
				
				authorPosition.append(" ");
				authorPosition.append(SearchSolrDocument.DELIMITER);
				authorPosition.append(" ");
			}
			if(null != author.getAffiliation()) {
				authorAffiliation.append(getContent(author.getAffiliation().getContent()));				
			} else {
				authorAffiliation.append("none");
			}
			if(ctr < book.getMetadata().getAuthor().size()) {
				authorAffiliation.append(" ");
				authorAffiliation.append(SearchSolrDocument.DELIMITER);
				authorAffiliation.append(" ");
			}
		}				
		result.setAuthorName(authorName.toString());
		result.setAuthorNameFacetList(authorNameFacetList);
		result.setAuthorAlphasort(authorAlphasort.toString());
		if(authorAffiliation.length() > 0) {
			result.setAuthorAffiliation(authorAffiliation.toString());
		}
		result.setAuthorRole(authorRole.toString());
		result.setAuthorPosition(authorPosition.toString());
		if(StringUtils.isNotEmpty(authorSingleline)) {
			result.setAuthorSingleline(authorSingleline);
		}
		
		List<String> subjectCodeList = new ArrayList<String>();
		List<String> subjectLevelList = new ArrayList<String>();
		List<String> subjectList = new ArrayList<String>();
		List<String> subjectFacet = new ArrayList<String>();
		for(Subject subject : book.getMetadata().getSubjectGroup().getSubject()) {
			subjectLevelList.add(subject.getLevel());
			subjectCodeList.add(subject.getCode());
			subjectList.add(subject.getContent());
			subjectFacet.add(subject.getContent());
		}
		result.setSubjectLevelList(subjectLevelList);
		result.setSubjectCodeList(subjectCodeList);
		result.setSubjectFacet(subjectFacet);
		result.setSubjectList(subjectList);
		
		// === start biblio
		if(null != biblioSearchDoc) {
			if(StringUtils.isNotEmpty(biblioSearchDoc.getBookId())) {
				result.setBookId(biblioSearchDoc.getBookId());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getIsbnIssn())) {
				result.setIsbnIssn(biblioSearchDoc.getIsbnIssn());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getSeriesCode())) {
				result.setSeriesCode(biblioSearchDoc.getSeriesCode());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getSeries())) {
				result.setSeries(biblioSearchDoc.getSeries());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getSeriesNumber())) {
				result.setSeriesNumber(biblioSearchDoc.getSeriesNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getMainTitle())) {
				result.setMainTitle(biblioSearchDoc.getMainTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getMainSubtitle())) {
				result.setMainSubtitle(biblioSearchDoc.getMainSubtitle());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getVolumeNumber())) {
				result.setVolumeNumber(biblioSearchDoc.getVolumeNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getVolumeTitle())) {
				result.setVolumeTitle(biblioSearchDoc.getVolumeTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getPartNumber())) {
				result.setPartNumber(biblioSearchDoc.getPartNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getPartTitle())) {
				result.setPartTitle(biblioSearchDoc.getPartTitle());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getEditionNumber())) {
				result.setEditionNumber(biblioSearchDoc.getEditionNumber());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getPrintDate())) {
				result.setPrintDateDisplay(biblioSearchDoc.getPrintDate());
				result.setPrintDate(biblioSearchDoc.getPrintDate());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getAltIsbnHardback())) {
				result.setAltIsbnHardback(biblioSearchDoc.getAltIsbnHardback());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getAltIsbnPaperback())) {
				result.setAltIsbnPaperback(biblioSearchDoc.getAltIsbnPaperback());
			}		
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getAuthorPosition())) {
				String[] posArr = biblioSearchDoc.getAuthorPosition().split(SearchSolrDocument.DELIMITER);
				String[] nameArr = biblioSearchDoc.getAuthorName().split(SearchSolrDocument.DELIMITER);
				String[] nameAlphaArr = biblioSearchDoc.getAuthorAlphasort().split(SearchSolrDocument.DELIMITER);
				String[] roleArr = biblioSearchDoc.getAuthorRole().split(SearchSolrDocument.DELIMITER);
				String[] affArr = biblioSearchDoc.getAuthorAffiliation().split(SearchSolrDocument.DELIMITER);
				
				String[] origPosArr = result.getAuthorPosition().split(SearchSolrDocument.DELIMITER);
				String[] origNameArr = result.getAuthorName().split(SearchSolrDocument.DELIMITER);
				String[] origNameAlphaArr = result.getAuthorAlphasort().split(SearchSolrDocument.DELIMITER);
				String[] origRoleArr = result.getAuthorRole().split(SearchSolrDocument.DELIMITER);
				String[] origAffArr = result.getAuthorAffiliation().split(SearchSolrDocument.DELIMITER);
				
				List<String> posList = Arrays.asList(origPosArr);
				List<String> nameList = Arrays.asList(origNameArr);
				List<String> nameAlphaList = Arrays.asList(origNameAlphaArr);
				List<String> roleList = Arrays.asList(origRoleArr);
				List<String> affList = Arrays.asList(origAffArr);
				
				
				// update
				int authCtr = 0;
				for(String s : posArr) {
					int position = Integer.parseInt(s);
					int index = position - 1;
					
					if(position <= nameArr.length) {						
						nameList.set(index, nameArr[authCtr]); 
						if(authCtr == 0) {
							nameAlphaList.set(index, nameAlphaArr[authCtr]);
						}
						roleList.set(index, roleArr[authCtr]);		
						affList.set(index, affArr[authCtr]);
					} else {
						nameList.add(nameArr[authCtr]);
						if(authCtr == 0) {
							nameAlphaList.add(nameAlphaArr[authCtr]);
						}
						roleList.add(roleArr[authCtr]);
						affList.add(affArr[authCtr]);
						posList.add(s);
					}
					authCtr++;
				}
				
				// build
				StringBuilder pos = new StringBuilder();
				StringBuilder name = new StringBuilder();
				StringBuilder nameAlpha = new StringBuilder();
				StringBuilder role = new StringBuilder();
				StringBuilder aff = new StringBuilder();
				List<String> bibAuthorNameFacetList = new ArrayList<String>();
				int i = 0;
				for(String s : posList) {
					i++;
					pos.append(s);
					name.append(nameList.get(i - 1));
					bibAuthorNameFacetList.add(fixAuthorNameFacet(nameList.get(i - 1)));
					
					if(i == 1) {
						nameAlpha.append(nameAlphaList.get(i - 1));
					}
					
					role.append(roleList.get(i - 1));
					aff.append(affList.get(i - 1));
					if(i < posList.size()) {
						pos.append(" ");
						pos.append(SearchSolrDocument.DELIMITER);
						pos.append(" ");
						
						name.append(" ");
						name.append(SearchSolrDocument.DELIMITER);
						name.append(" ");
						
						role.append(" ");
						role.append(SearchSolrDocument.DELIMITER);
						role.append(" ");
						
						aff.append(" ");
						aff.append(SearchSolrDocument.DELIMITER);
						aff.append(" ");
					}
				}
				result.setAuthorPosition(pos.toString());
				result.setAuthorName(name.toString());
				result.setAuthorNameFacetList(bibAuthorNameFacetList);
				result.setAuthorAlphasort(nameAlpha.toString());
				result.setAuthorRole(role.toString());
				result.setAuthorAffiliation(aff.toString());
			}
			
			if(StringUtils.isNotEmpty(biblioSearchDoc.getAuthorSingleline())) {
				result.setAuthorSingleline(biblioSearchDoc.getAuthorSingleline());
			}
						
			if(null != biblioSearchDoc.getSubjectCodeList() && biblioSearchDoc.getSubjectCodeList().size() > 0) {
				result.setSubjectCodeList(biblioSearchDoc.getSubjectCodeList());
				result.setSubjectLevelList(biblioSearchDoc.getSubjectLevelList());
				result.setSubjectList(biblioSearchDoc.getSubjectList());
				result.setSubjectFacet(result.getSubjectList());				
			}
		}
		// === end biblio
				
		return generateSearchData(book.getContentItems().getContentItem(), result, "root", onlineDateText);
	}
	
	private static List<SearchSolrDocument> generateSearchData(List<ContentItem> contentItemList, 
			SearchSolrDocument bookDoc, String parentId, String onlineDateText) throws Exception {
		List<SearchSolrDocument> result = new ArrayList<SearchSolrDocument>();
		
		for(ContentItem contentItem : contentItemList) {
			SearchSolrDocument doc = new SearchSolrDocument();
			
			BeanUtils.copyProperties(doc, bookDoc);
			
			doc.setId(contentItem.getId());
			doc.setParentId(parentId);
			doc.setContentType("Chapter");
			doc.setTitle(getContent(contentItem.getHeading().getTitle().getContent()));
			doc.setTitleAlphasort(contentItem.getHeading().getTitle().getAlphasort());
			if(StringUtils.isNotEmpty(contentItem.getDoi())) {
				doc.setDoi(contentItem.getDoi());
			}
			if(null != contentItem.getHeading().getSubtitle()) {				
				doc.setSubtitle(getContent(contentItem.getHeading().getSubtitle().getContent()));		
			}			

			if(null != contentItem.getHeading().getLabel()) {
				doc.setLabel(contentItem.getHeading().getLabel());
			}
			doc.setPageStart(contentItem.getPageStart());			
			doc.setPageEnd(contentItem.getPageEnd());			
			doc.setType(contentItem.getType());
			doc.setPosition(contentItem.getPosition());
			
			if(null != contentItem.getContributorGroup()) {
				StringBuilder contributorName = new StringBuilder();	
				StringBuilder contributorAlphasort = new StringBuilder();
				StringBuilder contributorAffiliation = new StringBuilder();
				int ctr = 0;
				for(Contributor contributor : contentItem.getContributorGroup().getContributor()) {
					ctr++;
					contributorName.append(getFirstLastName(contributor.getName().getContent()));
					contributorAlphasort.append(contributor.getAlphasort());
					if(ctr < contentItem.getContributorGroup().getContributor().size()) {
						contributorName.append(" ");
						contributorName.append(SearchSolrDocument.DELIMITER);
						contributorName.append(" ");
						
						contributorAlphasort.append(" ");
						contributorAlphasort.append(SearchSolrDocument.DELIMITER);
						contributorAlphasort.append(" ");
					}
					if(null != contributor.getAffiliation()) {
						contributorAffiliation.append(getContent(contributor.getAffiliation().getContent()));						
					} else {
						contributorAffiliation.append("none");
					}
					if(ctr < contentItem.getContributorGroup().getContributor().size()) {
						contributorAffiliation.append(" ");
						contributorAffiliation.append(SearchSolrDocument.DELIMITER);
						contributorAffiliation.append(" ");
					}
				}
				doc.setContributorName(contributorName.toString());
				doc.setContributorAlphasort(contributorAlphasort.toString());
				if(contributorAffiliation.length() > 0) {
					doc.setContributorAffiliation(contributorAffiliation.toString());
				}		
			}
			
			if(null != contentItem.getPdf()) {
				doc.setPdfFilename(contentItem.getPdf().getFilename());
				doc.setChapterFulltext(getFulltext(doc.getIsbnIssn(), doc.getPdfFilename()));
			}						
			
			if(null != contentItem.getKeywordGroup()) {
				List<String> keywordTextList = new ArrayList<String>();
				for(KeywordGroup keywordGroup : contentItem.getKeywordGroup()) {
					keywordTextList.addAll(getKeywordTextList(keywordGroup.getKeyword()));
				}
				doc.setKeywordTextList(keywordTextList);
			}
						
			result.add(doc);
			
			if(null != contentItem.getContentItem() && contentItem.getContentItem().size() > 0) {
				result.addAll(generateSearchData(contentItem.getContentItem(), bookDoc, doc.getId(), onlineDateText));
			}
		}
		
		return result;
	}
	
	private static String getContent(List<Object> content) throws Exception {
		StringBuffer result = new StringBuffer();
		
		for(Object object : content) {
			if(object instanceof Italic) {
				result.append("<i>");
				result.append(getContent(((Italic)object).getContent()));
				result.append("</i>");
			} else if(object instanceof Bold) {
				result.append("<b>");
				result.append(getContent(((Bold)object).getContent()));
				result.append("</b>");
			} else if(object instanceof SmallCaps) {
				result.append("<span style='font-variant:small-caps'>");
				result.append(getContent(((SmallCaps)object).getContent()));
				result.append("</span>");
			} else if(object instanceof Underline) {
				result.append("<u>");
				result.append(getContent(((Underline)object).getContent()));
				result.append("</u>");
			} else if(object instanceof Sup) {
				result.append(getContent(((Sup)object).getContent()));
			} else if(object instanceof Sub) {
				result.append(getContent(((Sub)object).getContent()));
			} else if(object instanceof P) {
				result.append("<p>");
				result.append(getContent(((P)object).getContent()));
				result.append("</p>");
			} else if(object instanceof Block) {
				result.append(getBlock((Block)object));
			} else if(object instanceof org.cambridge.ebooks.service.solr.bean.jaxb.List) {
				result.append(getList((org.cambridge.ebooks.service.solr.bean.jaxb.List)object));
			} else if(object instanceof Source) {
				result.append(getContent(((Source)object).getContent()));
			} else if(object instanceof CopyrightHolder) {
				result.append(getContent(((CopyrightHolder)object).getContent()));
			} else if(object instanceof CopyrightStatement) {
				result.append(getContent(((CopyrightStatement)object).getContent()));
			} else if(object instanceof JAXBElement) {
				result.append(" ");
				result.append((String)((JAXBElement)object).getValue());
			} else if(object instanceof Forenames) {
				result.append(getContent(((Forenames)object).getContent()));
			} else if(object instanceof Surname) {
				result.append(" ");
				result.append(getContent(((Surname)object).getContent()));
			} else if(object instanceof Collab) {
				result.append(getContent(((Collab)object).getContent()));
			} else {
				result.append(cleanTextNode((String)object));
			}
		}
		
		return result.toString();
	}
	
	private static String getName(List<Object> content, List<String> lastName, 
			List<String> firstName, List<String> collab, List<String> suffix, List<String> nameLink) throws Exception {
		
		StringBuffer result = new StringBuffer();
		
		for(Object object : content) {
			if(object instanceof Italic) {
				result.append("<i>");
				result.append(getContent(((Italic)object).getContent()));
				result.append("</i>");
			} else if(object instanceof Bold) {
				result.append("<b>");
				result.append(getContent(((Bold)object).getContent()));
				result.append("</b>");
			} else if(object instanceof SmallCaps) {
				result.append("<span style='font-variant:small-caps'>");
				result.append(getContent(((SmallCaps)object).getContent()));
				result.append("</span>");
			} else if(object instanceof Underline) {
				result.append("<u>");
				result.append(getContent(((Underline)object).getContent()));
				result.append("</u>");
			} else if(object instanceof Sup) {
				result.append(getContent(((Sup)object).getContent()));
			} else if(object instanceof Sub) {
				result.append(getContent(((Sub)object).getContent()));
			} else if(object instanceof P) {
				result.append("<p>");
				result.append(getContent(((P)object).getContent()));
				result.append("</p>");
			} else if(object instanceof Block) {
				result.append(getBlock((Block)object));
			} else if(object instanceof org.cambridge.ebooks.service.solr.bean.jaxb.List) {
				result.append(getList((org.cambridge.ebooks.service.solr.bean.jaxb.List)object));
			} else if(object instanceof Source) {
				result.append(getContent(((Source)object).getContent()));
			} else if(object instanceof CopyrightHolder) {
				result.append(getContent(((CopyrightHolder)object).getContent()));
			} else if(object instanceof CopyrightStatement) {
				result.append(getContent(((CopyrightStatement)object).getContent()));
			} else if(object instanceof JAXBElement) {
				JAXBElement jaxb = (JAXBElement)object;
				if("suffix".equalsIgnoreCase(jaxb.getName().toString())) {
					suffix.add((String)jaxb.getValue());
				} else if("name-link".equalsIgnoreCase(jaxb.getName().toString())) {
					nameLink.add((String)jaxb.getValue());
				} 
			} else if(object instanceof Forenames) {
				firstName.add(getName(((Forenames)object).getContent(), lastName, firstName, collab,
						suffix, nameLink));
			} else if(object instanceof Surname) {				
				lastName.add(getName(((Surname)object).getContent(), lastName, firstName, collab, 
						suffix, nameLink));
			} else if(object instanceof Collab) {
				collab.add(getName(((Collab)object).getContent(), lastName, firstName, collab, 
						suffix, nameLink));
			} else {
				result.append(cleanTextNode((String)object));
			}
		}
		
		return result.toString();
	}
	
	private static String getFirstLastName(List<Object> content) throws Exception {
		StringBuilder result = new StringBuilder();
		List<String> lastName = new ArrayList<String>();
		List<String> firstName = new ArrayList<String>();
		List<String> collab = new ArrayList<String>();
		List<String> suffix = new ArrayList<String>();
		List<String> nameLink = new ArrayList<String>();
		
		getName(content, lastName, firstName, collab, suffix, nameLink);
		
		for(String s : firstName) {
			result.append(s).append(" ");
		}
				
		for(String s : suffix) {
			result.append(s).append(" ");
		}		
		
		for(String s : nameLink) {
			result.append(s).append(" ");
		}
		
		for(String s : lastName) {
			result.append(s).append(" ");
		}
		
		for(String s : collab) {
			result.append(s).append(" ");
		}
		
		String trimed = result.toString();
		if(StringUtils.isNotEmpty(trimed)) {
			trimed = trimed.trim();
		}
		
		return trimed;
	}
	
	private static String getLastFirstName(List<Object> content) throws Exception {
		StringBuilder result = new StringBuilder();
		List<String> lastName = new ArrayList<String>();
		List<String> firstName = new ArrayList<String>();
		List<String> collab = new ArrayList<String>();
		List<String> suffix = new ArrayList<String>();
		List<String> nameLink = new ArrayList<String>();
		
		getName(content, lastName, firstName, collab, suffix, nameLink);
		
		for(String s : nameLink) {
			result.append(s).append(" ");
		}
		
		int ctr = 0;
		for(String s : lastName) {
			ctr++;
			result.append(s);
			if(ctr < lastName.size()) {
				result.append(" ");
			} else {
				result.append(", ");				
			}
		}		
		
		for(String s : firstName) {
			result.append(s).append(" ");
		}
		
		for(String s : suffix) {
			result.append(s).append(" ");
		}	
		
		for(String s : collab) {
			result.append(s).append(" ");
		}
		
		String trimed = result.toString();
		if(StringUtils.isNotEmpty(trimed)) {
			trimed = trimed.trim();
		}
		
		return trimed;
	}
	
	private static List<String> getKeywordTextList(List<Keyword> keywordList) throws Exception {
		List<String> result = new ArrayList<String>();
		for(Keyword keyword : keywordList) {
			result.add(getContent(keyword.getKwdText().getContent()));
			if(null != keyword.getKeyword() && keyword.getKeyword().size() > 0) {
				result.addAll(getKeywordTextList(keyword.getKeyword()));
			}
		}
		return result;
	}
	
	private static String getFulltext(String isbn, String pdfFilename) throws Exception {		
		StringBuilder result = new StringBuilder();
		File pdfFile = new File(ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + pdfFilename.replace("pdf", "txt"));
		
		if(pdfFile != null && pdfFile.exists() && pdfFile.isFile()) {
			StringBuilder pageContent = new StringBuilder();	        
			
			Reader reader = new InputStreamReader(new FileInputStream(pdfFile), "UTF-8");
			
			int data;
			while ((data = reader.read()) > -1) {
				char dataChar = (char)data;
				
				if(data == 12) { 
					// '\f'
					result.append(pageContent.toString().replaceAll("(?i)(Cambridge Books Online &#169; Cambridge University Press, \\d\\d\\d\\d)", ""));
					result.append("\n");
					pageContent = new StringBuilder();
				} else if(data == 10) { 
					// next line					
					pageContent.append(" ");
				} else {										
					pageContent.append(dataChar);
				}
			}

			reader.close();
		}
		
		return result.toString();
	}	
	
	private static List<SolrInputDocument> generatePerPageFulltextDocs(String isbn, String pdfFilename, String bookId, String chapterId) throws Exception {		
		List<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		File pdfFile = new File(ApplicationProperties.DIR_EBOOK_CONTENT + isbn + "/" + pdfFilename.replace("pdf", "txt"));
		
		if(pdfFile != null && pdfFile.exists() && pdfFile.isFile()) {
			StringBuilder pageContent = new StringBuilder();	        
			
			Reader reader = new InputStreamReader(new FileInputStream(pdfFile), "UTF-8");
			
			int data;
			int page = 0;
			while ((data = reader.read()) > -1) {
				char dataChar = (char)data;
				
				if(data == 12) { 
					// '\f'
					SolrInputDocument doc = new SolrInputDocument();	
					page++;					
					doc.addField("page", page);
					doc.addField("fulltext", pageContent.toString().replaceAll("(?i)(Cambridge Books Online &#169; Cambridge University Press, \\d\\d\\d\\d)", ""));
					doc.addField("id", chapterId + "_" + page);
					doc.addField("chapter_id", chapterId);
					doc.addField("book_id", bookId);
					doc.addField("isbn", isbn);
					
					docs.add(doc);
					
					pageContent = new StringBuilder();
				} else if(data == 10) { 
					// next line					
					pageContent.append(" ");
				} else {										
					pageContent.append(dataChar);
				}
			}

			reader.close();
		}
		
		return docs;
	}	
	
	private static List<BookContentItemSolrDocument> generateChapterData(List<ContentItem> contentItemList, String parentId, BookMetaDataSolrDocument bookDoc) throws Exception {
		List<BookContentItemSolrDocument> resultList = new ArrayList<BookContentItemSolrDocument>();
		
		for(ContentItem contentItem : contentItemList) {			
			BookContentItemSolrDocument doc = new BookContentItemSolrDocument();	
			
			doc.setContentType("chapter");
			doc.setId(contentItem.getId());
			doc.setParentId(parentId);
			doc.setBookId(bookDoc.getId());
			doc.setIsbn(bookDoc.getIsbn());
			doc.setFlag(bookDoc.getFlag());
			doc.setPublisherId(bookDoc.getPublisherId());
			doc.setPageStart(contentItem.getPageStart());
			doc.setPageEnd(contentItem.getPageEnd());
			doc.setType(contentItem.getType());
			doc.setPosition(contentItem.getPosition());			
			doc.setTitle(getContent(contentItem.getHeading().getTitle().getContent()));				
			doc.setDoi(contentItem.getDoi());		
						
			if(null != contentItem.getHeading().getLabel()) {				
				doc.setLabel(contentItem.getHeading().getLabel());
			}			
			
			if(null != contentItem.getHeading().getSubtitle()) {				
				doc.setSubtitle(getContent(contentItem.getHeading().getSubtitle().getContent()));
			}
			
			if(null != contentItem.getPdf()) {				
				doc.setPdfFilename(contentItem.getPdf().getFilename());
			}
			
			if(null != contentItem.getContributorGroup()) {
				List<String> contributorNameList = new ArrayList<String>();
				List<String> contributorNameLfList = new ArrayList<String>();
				List<String> contributorAffiliationList = new ArrayList<String>();
				for(Contributor contributor : contentItem.getContributorGroup().getContributor()) {						
					contributorNameList.add(getFirstLastName(contributor.getName().getContent()));
					contributorNameLfList.add(getLastFirstName(contributor.getName().getContent()));
					if(null != contributor.getAffiliation()) {						
						contributorAffiliationList.add(getContent(contributor.getAffiliation().getContent()));
					} else {
						contributorAffiliationList.add("none");
					}
				}
				doc.setAuthorNameList(contributorNameList);
				doc.setAuthorNameLfList(contributorNameLfList);
				doc.setAuthorAffiliationList(contributorAffiliationList);
			}
			
			if(null != contentItem.getAbstract()) {					
//				File dir = new File(ApplicationProperties.DIR_EBOOK_FILE_DUMP + bookDoc.getIsbn());
//				if(!dir.exists()) {
//					dir.mkdir();	
//				}
//				
//				File file = new File(ApplicationProperties.DIR_EBOOK_FILE_DUMP + bookDoc.getIsbn() + "/" + doc.getId() + "_abstract.txt");
//				FileWriter fileWriter = new FileWriter(file);		
//				fileWriter.write(getContent(contentItem.getAbstract().getPOrBlockOrList()));
//				fileWriter.close();
				
				doc.setAbstractText(getContent(contentItem.getAbstract().getPOrBlockOrList()));
				doc.setAbstractFilename(contentItem.getAbstract().getAltFilename());
				doc.setAbstractProblem(contentItem.getAbstract().getProblem());
			}
			
			if(null != contentItem.getToc()) {
				resultList.addAll(getTocItem(contentItem.getToc().getTocItem(), "root", doc.getId(), bookDoc.getId()));
			}			
			
			if(null != contentItem.getKeywordGroup()) {
				List<String> keywordTextList = new ArrayList<String>();
				for(KeywordGroup keywordGroup : contentItem.getKeywordGroup()) {
					keywordTextList.addAll(getKeywordTextList(keywordGroup.getKeyword()));
				}
				doc.setKeywordTextList(keywordTextList);
			}
			resultList.add(doc);
			
			if(null != contentItem.getContentItem() && contentItem.getContentItem().size() > 0) {
				resultList.addAll(generateChapterData(contentItem.getContentItem(), contentItem.getId(), bookDoc));
			}
		}
		return resultList;
	}
	
	private static List<SolrInputDocument> generatePage(List<ContentItem> contentItemList, String bookId, String isbn) throws Exception {
		List<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		
		for(ContentItem contentItem : contentItemList) {	
			
			if(null != contentItem.getPdf()) {
				docs.addAll(generatePerPageFulltextDocs(isbn, 
						contentItem.getPdf().getFilename(), 
						bookId, 
						contentItem.getId()));
			}
			
			if(null != contentItem.getContentItem() && contentItem.getContentItem().size() > 0) {
				docs.addAll(generatePage(contentItem.getContentItem(), bookId, isbn));
			}
		}
		return docs;
	}
	
	private static List<BookContentItemSolrDocument> getTocItem(List<TocItem> tocList, String parentId, String contentId, String bookId) throws Exception {
		List<BookContentItemSolrDocument> resultList = new ArrayList<BookContentItemSolrDocument>();
		
		for(TocItem tocItem : tocList) {
			BookContentItemSolrDocument doc = new BookContentItemSolrDocument();
			doc.setContentType("toc");
			doc.setBookId(bookId);
			doc.setParentId(parentId);
			doc.setChapterId(contentId);
			doc.setId(tocItem.getId());
			doc.setPageStart(tocItem.getStartpage());
			doc.setTocText(getContent(tocItem.getTocText().getContent()));
			if(null != tocItem.getAuthor() && tocItem.getAuthor().size() > 0) {
				List<String> authorNameList = new ArrayList<String>();
				List<String> authorRoleList = new ArrayList<String>();
				List<String> authorAffiliationList = new ArrayList<String>();
				for(Author author : tocItem.getAuthor()) {
					authorNameList.add(getContent(author.getName().getContent()));
					authorRoleList.add(author.getRole());
					if(null != author.getAffiliation()) {
						authorAffiliationList.add(getContent(author.getAffiliation().getContent()));
					}
				}
				
				doc.setAuthorNameList(authorNameList);
				doc.setAuthorRoleList(authorRoleList);
				if(authorAffiliationList.size() > 0) {
					doc.setAuthorAffiliationList(authorAffiliationList);
				}
			}	
			
			resultList.add(doc);
			if(null != tocItem.getTocItem() && tocItem.getTocItem().size() > 0) {				
				resultList.addAll(getTocItem(tocItem.getTocItem(), tocItem.getId(), contentId, bookId));
			}
			
		}
		
		return resultList;
	}
	
	private static String getBlock(Block block) throws Exception {
		StringBuffer result = new StringBuffer();
		List<P> pList = block.getP();
		Source source = block.getSource();

		result.append("<blockquote id='indent'>");
		for(P p : pList) {
			result.append("<p>");
			result.append(getContent(p.getContent()));
			result.append("</p>");
		}
		if(null != source) {
			result.append(getContent(source.getContent()));
		}
		result.append("</blockquote>");
		
		return result.toString();
	}
	
	private static String getList(org.cambridge.ebooks.service.solr.bean.jaxb.List ul) throws Exception {
		StringBuffer result = new StringBuffer();
		result.append("<ul ");
		if("bullet".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:disc !important;'>");
		} else if("number".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:decimal !important;'>");
		} else {
			result.append("style='padding: 20px 20px 20px 30px; list-style:none !important;'>");
		}
		for(ListItem li : ul.getListItem()) {
			result.append("<li>");		
			result.append(getContent(li.getContent()));
			result.append("</li>");		
		}		
		result.append("</ul>");
		
		return result.toString();
	}
	
	private static void setPublisher(List<JAXBElement<String>> publisherList, BookMetaDataSolrDocument doc) {		
		int ctr = 0;
		for(JAXBElement<String> jaxb : publisherList) {
			ctr++;		
			if("publisher-name".equalsIgnoreCase(jaxb.getName().toString())) {				
				doc.setPublisherName(jaxb.getValue());
			} else {				
				doc.setPublisherLoc(jaxb.getValue());
			}			
		}
	}
	
	private static List<String> getCopyrightStatement(List<CopyrightStatement> copyrightStatementList) throws Exception {
		List<String> result = new ArrayList<String>();
		
		for(CopyrightStatement cs : copyrightStatementList) {
			result.add(getContent(cs.getContent()));
		}
		
		return result;
	}
	
	private static String cleanTextNode(String val) {
		if (null != val) {
			val = val.replaceAll("(\r\n|\r|\n|\n\r)", "")
			.replaceAll("\\s+", " ") //.trim()
			.replaceAll("\\\\", "&#92;"); // backslash unicode			
		}
		return val;
	}
	
	private static String getFormatedDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	private static Date getOnlineDate(String dateText) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("ddMMMyyyy");
		try {
			date = dateFormat.parse(dateText);
		} catch (ParseException e) {
			System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
		}
		return date;
	}
	
	private static String getPrintDate(List<String> printDateList) {
		StringBuffer result = new StringBuffer();
		
		if(printDateList.size() > 0) {
			result.append(printDateList.get(0));
		}	
		
		return result.toString();		
	}
	
	private static String fixAuthorNameFacet(String name){
		if(null != name){
			if(name.indexOf(".") > 0 || name.indexOf("-") > 0 || name.indexOf("'") > 0 ){
				name = name.replace(".",". ");
				name = name.replace("-","- ");
				name = name.replace("'","' ");
			}
			name = name.replace("&nbsp;"," ");
			name = WordUtils.capitalizeFully(name);
			if(name.indexOf(".") > 0 || name.indexOf("-") > 0 || name.indexOf("'") > 0 ){
				name = name.replace(". ",".");
				name = name.replace("- ","-");
				name = name.replace("' ","'");
			}
			name = name.replace("  "," ").trim();
			//System.out.println(name);
		}
		return name;
	}
}
