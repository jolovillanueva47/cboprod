package org.cambridge.ebooks.service.solr.util;

import java.io.StringReader;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonToBean {
	private static ObjectMapper mapper = new ObjectMapper();
	
	public static <T> T generateBean(T objectType, String jsonSrc, Class<T> c) throws Exception {
		return (T)mapper.readValue(new StringReader(jsonSrc), c);
	}
}
