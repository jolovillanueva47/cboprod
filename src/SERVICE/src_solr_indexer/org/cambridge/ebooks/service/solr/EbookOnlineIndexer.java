package org.cambridge.ebooks.service.solr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.service.solr.constant.ApplicationProperties;
import org.cambridge.ebooks.service.solr.server.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.service.solr.server.CupSolrServer;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;
import org.cambridge.ebooks.service.solr.util.JaxbXmlParser;
import org.cambridge.ebooks.service.solr.util.XmlParser;


public class EbookOnlineIndexer {

	private static final String DEL = "=DEL=";
	
	public static void main (String[] args) {
		XmlParser xmlParser = new JaxbXmlParser("org.cambridge.ebooks.service.solr.bean.jaxb");
		CupSolrServer server = new CupCommonsHttpSolrServer();		
		Indexer indexer = new EbookSolrIndexer(server, xmlParser);
		
		if(args.length > 0) {		
			String param = args[0];
			if("optimize".equalsIgnoreCase(param)) {
				String core = "";
				if(args.length > 1) {
					core = args[1];
				} else {
					System.out.println("[Error] Supply 2nd param: core name");
					return;
				}
				
				System.out.println("Start Optimizing Online Index [" + core + " Core]");
				long startTime = System.currentTimeMillis();
				
				indexer.optimize(core);
				
				System.out.println("End Optimizing Online Index");
				System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs");
			} else if("delete".equalsIgnoreCase(param))  {
				String core = "";
				if(args.length > 1) {
					core = args[1];
				} else {
					System.out.println("[Error] Supply 2nd param: core name");
					return;
				}
				
				System.out.println("Start Online Indexer [delete in " + core + " Core]");
				
				long startTime = System.currentTimeMillis();
				
				File file = new File(ApplicationProperties.FILE_ISBN_DELETE_LIST);
				System.out.println("File:" + file.getName());
				
				FileReader fr;
				BufferedReader br;		
				List<String> isbnList = new ArrayList<String>();
				
				try {
					fr = new FileReader(file);
					br = new BufferedReader(fr);
				
					String line = "";
					while((line = br.readLine()) != null){		
						try {				
							isbnList.add(line.trim());
						} catch (Exception e) {
							System.out.println("[Exception]line:" + line);
							System.out.println(ExceptionPrinter.getStackTraceAsString(e));
							continue;
						}
					}
	
					if(ApplicationProperties.CORE_BOOK.equalsIgnoreCase(core)) {
						System.out.println("Book");
						indexer.delete(isbnList, ApplicationProperties.CORE_BOOK);						
					} else if(ApplicationProperties.CORE_CHAPTER.equalsIgnoreCase(core)) {
						System.out.println("Chapter");	
						indexer.delete(isbnList, ApplicationProperties.CORE_CHAPTER);						
					} else if(ApplicationProperties.CORE_SEARCH.equalsIgnoreCase(core)) {
						System.out.println("Search");	
						indexer.delete(isbnList, ApplicationProperties.CORE_SEARCH);						
					} else {
						System.out.println("All");	
						indexer.delete(isbnList, "All");
					}
				} catch (Exception e) {
					System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
				}
					
				System.out.println("End Online Indexer");
				System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs");
								
			} else if("clc".equalsIgnoreCase(param)) { 
				String core = "";
				if(args.length > 1) {
					core = args[1];
				} else {
					System.out.println("[Error] Supply 2nd param: core name");
					return;
				}
				
				System.out.println("Start Online Indexer [" + core + " Core]");
				
				long startTime = System.currentTimeMillis();
				
				File file = new File(ApplicationProperties.FILE_ISBN_CLC_LIST);
				System.out.println("File:" + file.getName());
				
				FileReader fr;
				BufferedReader br;		
				List<Map<String, Object>> bookList = new ArrayList<Map<String, Object>>();
				
				try {
					String line = "";
					fr = new FileReader(file);
					br = new BufferedReader(fr);	
					
					while((line = br.readLine()) != null){
						try {				
							String[] s = line.split(DEL);
							if(s.length > 0) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("isbn", s[0]);
								map.put("lastModified", s[1]);
								map.put("flag", s[2]);
								map.put("publisherId", s[3]);
								map.put("authorSingleline", s[4]);
								bookList.add(map);
							}
						} catch (Exception e) {
							System.out.println("[Exception]line:" + line);
							System.out.println(ExceptionPrinter.getStackTraceAsString(e));
							continue;
						}

					}
	
					if(ApplicationProperties.CORE_BOOK.equalsIgnoreCase(core)) {
						System.out.println("Book");
						indexer.add(bookList, ApplicationProperties.CORE_BOOK);						
					} else if(ApplicationProperties.CORE_CHAPTER.equalsIgnoreCase(core)) {
						System.out.println("Chapter");	
						indexer.add(bookList, ApplicationProperties.CORE_CHAPTER);						
					} else if(ApplicationProperties.CORE_SEARCH.equalsIgnoreCase(core)) {
						System.out.println("Search");	
						indexer.add(bookList, ApplicationProperties.CORE_SEARCH);						
					} 
				} catch (Exception e) {
					System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
				}
				
				System.out.println("End Online Indexer");
				System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs");
				
			} else {
				System.out.println("Start Online Indexer [" + param + " Core]");
				long startTime = System.currentTimeMillis();
				
				File file = new File(ApplicationProperties.FILE_ISBN_LIST);
				System.out.println("File:" + file.getName());
				
				FileReader fr;
				BufferedReader br;		
				List<Map<String, Object>> bookList = new ArrayList<Map<String, Object>>();

				try {

					String line = "";
					fr = new FileReader(file);
					br = new BufferedReader(fr);	
					
					while((line = br.readLine()) != null){
						try {				
							String[] s = line.split(DEL);
							if(s.length > 0) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("isbn", s[0]);
								map.put("lastModified", s[1]);
								map.put("flag", s[2]);
								map.put("publisherId", s[3]);
								map.put("authorSingleline", s[4]);
								bookList.add(map);
							}
						} catch (Exception e) {
							System.out.println("[Exception]line:" + line);
							System.out.println(ExceptionPrinter.getStackTraceAsString(e));
							continue;
						}
	
					}

					if(ApplicationProperties.CORE_BOOK.equalsIgnoreCase(param)) {
						System.out.println("Book");
						indexer.add(bookList, ApplicationProperties.CORE_BOOK);						
					} else if(ApplicationProperties.CORE_CHAPTER.equalsIgnoreCase(param)) {
						System.out.println("Chapter");	
						indexer.add(bookList, ApplicationProperties.CORE_CHAPTER);						
					} else if(ApplicationProperties.CORE_SEARCH.equalsIgnoreCase(param)) {
						System.out.println("Search");	
						indexer.add(bookList, ApplicationProperties.CORE_SEARCH);						
					} else if(ApplicationProperties.CORE_PAGE.equals(param)) {
						System.out.println("Page");	
						indexer.add(bookList, ApplicationProperties.CORE_PAGE);
					}
				} catch (Exception e) {
					System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
				}
				
				System.out.println("End Online Indexer");
				System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs");
			}
		} else {
			System.out.println("Start Online Indexer [All Cores]");
			long startTime = System.currentTimeMillis();
			
			File file = new File(ApplicationProperties.FILE_ISBN_LIST);
			System.out.println("File:" + file.getName());
			
			FileReader fr;
			BufferedReader br;		
			List<Map<String, Object>> bookList = new ArrayList<Map<String, Object>>();
			
			try {

				String line = "";
				fr = new FileReader(file);
				br = new BufferedReader(fr);	
				
				while((line = br.readLine()) != null){
					try {				
						String[] s = line.split(DEL);
						if(s.length > 0) {
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("isbn", s[0]);
							map.put("lastModified", s[1]);
							map.put("flag", s[2]);
							map.put("publisherId", s[3]);
							map.put("authorSingleline", s[4]);
							bookList.add(map);
						}
					} catch (Exception e) {
						System.out.println("[Exception]line:" + line);
						System.out.println(ExceptionPrinter.getStackTraceAsString(e));
						continue;
					}

				}
				
				indexer.add(bookList, "ALL");
			} catch (Exception e) {
				System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
			}
			
			System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs");
			System.out.println("End Online Indexer");
		}
	}
}
