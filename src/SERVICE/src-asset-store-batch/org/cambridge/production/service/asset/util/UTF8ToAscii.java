package org.cambridge.production.service.asset.util;

/**
 * Adapted from source at http://www.xinotes.org/notes/note/812/
 * -------------------------------------------------------------
 * Reads stringin UTF-8 encoding and outputs string in ASCII with unicode
 * escaped sequence for characters outside of ASCII.
 * Also handles specified characters (e.g. ampersand).
 * Intended for use as external function for Saxon 9.1 in processing CBO header XML.
 */
public class UTF8ToAscii {
    public static String UTF8ToAscii1(String args) throws Exception {
    	if (args == "") {
	    return args + "BAD";	//if no arguments supplied, the output will be "BAD"
	}
	return unicodeEscape(args);	//takes input string, returns Unicode-escaped version of input
    }

    private static final char[] hexChar = {
        '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
    };
    
    private static String stest = "#&/<>"; //used to look up extra special chars that are in ASCII - can be appended to if necessary

    private static String unicodeEscape(String s) {
	StringBuilder sb = new StringBuilder();	//used to construct output string
	for (int i = 0; i < s.length(); i++) {	//examine each character in the input string
	    char c = s.charAt(i);				//cast it to a char
	    if ((c >> 7) > 0) {					//if the second byte is not zero - i.e. if the character is outside ASCII
		sb.append("&#x");					//begin character reference
		sb.append(hexChar[(c >> 12) & 0xF]); 	// append the hex character for the left-most 4-bits (looked up in hexChar array)
		sb.append(hexChar[(c >> 8) & 0xF]);  	// hex for the second group of 4-bits from the left
		sb.append(hexChar[(c >> 4) & 0xF]);  	// hex for the third group
		sb.append(hexChar[c & 0xF]);         	// hex for the last group, e.g., the right most 4-bits
		sb.append(";");						//end character reference (without appending c)
	    }
	    else if (c == stest.charAt(0)) {	//hash
	  sb.append("&#x0023;");
	    }	    
	    else if (c == stest.charAt(1)) {	//ampersand (VERY IMPORTANT)
	  sb.append("&#x0026;");
	    }
	    else if (c == stest.charAt(2)) {	//slash
	  sb.append("&#x002F;");
	    }
	    else if (c == stest.charAt(3)) {	//less-than
	  sb.append("&#x003C;");
	    }	    	    
	    else if (c == stest.charAt(4)) {	//greater-than
	  sb.append("&#x003E;");
	    }
	    else {
		sb.append(c);	//the character was neither outside ASCII nor among the extras, so append it to the new string untouched
	    }
	}
	return sb.toString();	//spit out the constructed string
    }
}