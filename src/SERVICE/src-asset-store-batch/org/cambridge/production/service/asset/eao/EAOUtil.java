package org.cambridge.production.service.asset.eao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class EAOUtil extends EAO{
	
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> nativeQuery(Class c, String q, Object... params) { 
		List<T> result = null;
		EntityManager em = createEntityManager();
		
		try
		{
			Query query = em.createNativeQuery(q, c);
	        if (params != null && params.length > 0) 
	        { 
	        	int ctr = 1;
		        for (Object param : params)  
		        	query.setParameter(ctr++, param); 
	        }
	        
	        result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EAOUtil.class + " query(T, String, String...) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return  result;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> nativeQuery(Class c, String q){
		List<T> result = null;
		EntityManager em = createEntityManager();
		try
		{
			Query query = em.createNativeQuery(q, Class.forName(c.getCanonicalName()));
			result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EAOUtil.class + " nativeQuery(T, String) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> query(Class c, String q){
		List<T> result = null;
		EntityManager em = createEntityManager();
		try
		{
			Query query = em.createQuery(q, c);
			result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EAOUtil.class + " query(T, String) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return result;
	}
	


}
