package org.cambridge.production.service.asset.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "SR_PUBLISHER")
public class Publisher {
	
	@Id
	@Column(name = "PUBLISHER_CODE")
	private String publisher_id;
	
	@Column(name ="PUBLISHER_LEGEND")
	private String publisher_name;

	public String getPublisher_id() {
		return publisher_id;
	}

	public void setPublisher_id(String publisher_id) {
		this.publisher_id = publisher_id;
	}

	public String getPublisher_name() {
		return publisher_name;
	}

	public void setPublisher_name(String publisher_name) {
		this.publisher_name = publisher_name;
	}
	
	

}
