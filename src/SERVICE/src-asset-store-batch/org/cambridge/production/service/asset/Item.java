package org.cambridge.production.service.asset;

public class Item implements Comparable<Item>{

	private String isbn;
	private String fileName;
	private String dataType;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public boolean equals(Object anotherObject){
		
		if(anotherObject instanceof Item)
		{
			if(getFileName() != null && ((Item)anotherObject).getFileName() != null)
				if(getFileName().equals(((Item)anotherObject).getFileName()) && getDataType().equals(((Item)anotherObject).getDataType()))
					return true;
		}
		
		return false;
	}
	
	public int hashCode(){
		String filename = getFileName();
		if(filename != null)
			return filename.length();
		
		return 0;
	}

	public String toString(){
		return "*" + fileName;  //for creating batch script
	}
	
	public int compareTo(Item item) {
		
		return getFileName().compareTo(item.getFileName());
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
}
