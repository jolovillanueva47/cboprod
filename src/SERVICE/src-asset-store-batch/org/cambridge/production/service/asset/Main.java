package org.cambridge.production.service.asset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.cambridge.ebooks.production.xslSaxon.XslSaxonCall;
import org.cambridge.production.service.asset.eao.AssetEAO;
import org.cambridge.production.service.asset.entities.Publisher;
import org.cambridge.production.service.asset.properties.ApplicationProperties;
import org.cambridge.production.service.asset.util.BookContentItemDocument;
import org.cambridge.production.service.asset.util.ContentFiles;
import org.cambridge.production.service.asset.util.CreateZipFile;
import org.cambridge.production.service.asset.util.IsbnContentDirUtil;
import org.cambridge.production.service.asset.util.Loader;
import org.cambridge.production.service.asset.util.ManifestToDlnConverter;
import org.cambridge.production.service.asset.util.SOLRSearchWorker;
import org.cambridge.production.service.asset.util.SOLRServer;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class Main {
    
    private static final String MANIFEST_FTL = "manifest.ftl";
    private static final String ALL_SIGNED_OFF_BOOKS = "ALL";
    private static final String ALL_SIGNED_OFF_BOOKS_FROM_FILE = "ALL_FROM_FILE";
    private static final String TEST_SIGNED_OFF_BOOKS = "978";
    
    public static void main(String[] args){
        try{    
        deliverBooksToAssetStore(getSignedOffBooks(args.length > 0 ? args[0] : ""));
        System.out.println("done");
        }catch(Exception e){
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
    
    private static void deliverBooksToAssetStore(List<?> isbns) {
        Template manifestTemplate;
        try {
            manifestTemplate = initTemplate(MANIFEST_FTL);
            if(isbns != null && !isbns.isEmpty()) {
                for(Object isbn : isbns)
                    processSignedOffBook(isbn.toString(), manifestTemplate);
            }
        } 
        catch (Exception e) {
            // TODO Auto-generated catch block
            System.err.println("[IOException] Main.deliverBooksToAssetStore(List<?>) " + e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void processSignedOffBook(String isbn, Template manifestTemplate) throws Exception{
        final String manifestFind = "\"-//W3C//DTD HTML 4.01//EN\"";
        final String manifestReplace = "\"-//CUP//DTD Manifest DTD//EN\"";
        
        final String manifestFind1 = "SYSTEM \"../manifest.dtd\"";
        final String manifestReplace1 = "PUBLIC \"-//CUP//DTD Manifest DTD//EN\" \"http://dtd.cambridge.org/schemas/2009/01/manifest.dtd\"";
        
        // make sure the process will go through by dropping a header.dtd to the folder
        String dummyDTD = IsbnContentDirUtil.getPath(isbn).substring(0, IsbnContentDirUtil.getPath(isbn).length() - 2)+"header.dtd";
        copyfile(ApplicationProperties.CONTENT_DIR+"header.dtd", dummyDTD);
        File toDelete = new File (dummyDTD);
        
        try {
            List<Publisher> pub = AssetEAO.getPublisherInfo(isbn);
            String publisher = null;
            publisher = pub.get(0).getPublisher_id();
            
            XslSaxonCall.convert(IsbnContentDirUtil.getPath(isbn)+isbn+".xml", ApplicationProperties.XSL_TRANSFORM);
            File xmlFile = getHeaderFileFromDirectory(new File(IsbnContentDirUtil.getPath(isbn)));
            File manifestFile = new File(IsbnContentDirUtil.getPath(isbn) + File.separator + "manifest.xml");
            System.out.println("manifestFile = "+manifestFile);
            
            if(xmlFile != null && xmlFile.exists() && publisher.equals("CUP")){
                ArrayList<Item> xmlContents = getContentFilesFromHeaderFile(xmlFile);
                File[] tobeZippedFiles = generateFilesToZipFromContentItems(xmlContents, publisher);
                createDeliveryNote(xmlFile, manifestTemplate, xmlContents);
                ManifestToDlnConverter.deleteDLN(isbn);
                ManifestToDlnConverter.createDLN(isbn);
                
                modifyXMLDTD(manifestFile, manifestFind, manifestReplace);
                modifyXMLDTD(manifestFile, manifestFind1, manifestReplace1);
                createDigitalCurDir(publisher); 
                removePreviouslyCreatedZipFile(isbn, publisher);
                CreateZipFile.createZipArchive(new File(ApplicationProperties.SPOOL_DIR +"/"+ publisher  + "/cams_pkg_" + isbn + ".zip"), tobeZippedFiles, isbn, publisher);
            } else if(xmlFile != null && xmlFile.exists() && !publisher.equals("CUP")){
                ArrayList<Item> xmlContents = getContentFilesFromHeaderFile(xmlFile);
                File[] tobeZippedFiles = generateFilesToZipFromContentItems(xmlContents, publisher);
                createDeliveryNote(xmlFile, manifestTemplate, xmlContents);
                modifyXMLDTD(manifestFile, manifestFind, manifestReplace);
                modifyXMLDTD(manifestFile, manifestFind1, manifestReplace1);
                
                createDigitalCurDir("UPO/"+publisher); 
                removePreviouslyCreatedZipFile(isbn, publisher);
                CreateZipFile.createZipArchive(new File(ApplicationProperties.SPOOL_DIR +"/UPO/"+ publisher  + "/" + isbn + ".zip"), tobeZippedFiles, isbn, publisher);
            } else {
                System.err.println("[ISBN:" + isbn + "] doesn't exist");
            }
        } catch(Exception ex) {
            System.err.println("[Exception] Main.processSignedOffBook(String) [ISBN:" + isbn + "] message: " + ex.getMessage());
            ex.printStackTrace();
//            System.exit(1);
            throw new Exception(ex.getMessage());
        } finally {
            toDelete.delete();
            System.out.println("dummy dtd deleted");
        }
    }
    
    private static Template initTemplate(String templateName) throws IOException{
        Configuration cfg = new Configuration();

        cfg.setDirectoryForTemplateLoading(new File(ApplicationProperties.TEMPLATE));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        
        return cfg.getTemplate(templateName);
    }

    private static void createDigitalCurDir(String publisher) throws IOException{
        File dir = new File(ApplicationProperties.SPOOL_DIR + "/" + publisher );
        if(!dir.exists()) {
            boolean created = dir.mkdirs();
            if(!created) {
                throw new IOException("Directories were not created!");
            }
        }
    }
    
    private static void removePreviouslyCreatedZipFile(String isbn, String publisher) throws IOException{
        String filePath = "";
        if(publisher.equals("CUP")){
            filePath = ApplicationProperties.SPOOL_DIR +"/"+ publisher +"/cams_pkg_" + isbn + ".zip";
        } else {
            filePath = ApplicationProperties.SPOOL_DIR +"UPO/"+ publisher +"/" + isbn + ".zip";
        }
        
        File zipFile = new File(filePath);
        if(zipFile.exists()) {
            boolean deleted = zipFile.delete();
            if(!deleted) {
                throw new IOException("Previously created zip file was not removed! [" + filePath+ "]");
            }
        }
    }
    
    /**
     * Creates the manifest file to list all the contents in the zip file
     * 
     * @param contentFolder - Folder path to the contents of the book
     * @param eisbn
     * @throws IOException 
     * @throws TemplateException 
     */
    private static void createDeliveryNote(File xmlHeaderFile, Template manifestTemplate, ArrayList<Item> xmlContents) throws IOException, TemplateException {
    
        String eisbn = xmlHeaderFile.getName().replace(".xml", "");

        /* Create data-model */
        Map<String, Serializable> root = new HashMap<String, Serializable>();
        root.put("referenceID", eisbn);
        root.put("items", xmlContents);

        /* Merge data-model with template */
        OutputStream f1 = new FileOutputStream(IsbnContentDirUtil.getPath(eisbn) + "/" + "manifest.xml");
        Writer output = new OutputStreamWriter(f1);
        manifestTemplate.process(root, output);
        
        output.flush();
        output.close();

        f1.flush();
        f1.close();                    
    }
    
    private static File[] generateFilesToZipFromContentItems(ArrayList<Item> xmlContents, String publisher){
        List<File> result = new ArrayList<File>();
        String isbn = "";
        for(Item item : xmlContents) {
            isbn = item.getIsbn();
            File contentFile = new File(IsbnContentDirUtil.getPath(isbn) + "/" + item.getFileName());
            if(contentFile.exists()) {
                result.add(contentFile);
            } else {
                System.err.println("[" + contentFile + "] does not exist!");
            }
        }
        
        if(publisher.equals("CUP")) {
            result.add(new File(IsbnContentDirUtil.getPath(isbn) + "/" + "manifestdln.xml"));
        }
        result.add(new File(IsbnContentDirUtil.getPath(isbn) + "/" + "manifest.xml"));
        
        return result.toArray(new File[0]);
    }
    
    //return ArrayList instead of List because List is not Serializable
    private static ArrayList<Item> getContentFilesFromHeaderFile(File xmlFile){
        
        String isbn = xmlFile.getName().replace(".xml", "");
        ArrayList<Item> items = new ArrayList<Item>();    
        
        List<String> pdfs =  ContentFiles.getContentFiles(Loader.XPATH_PDF_ALL, xmlFile);
        if(pdfs.isEmpty()) {
            try {
                SOLRSearchWorker solrWorker = new SOLRSearchWorker();
                List<BookContentItemDocument> pdfsSOLR =  solrWorker.searchCore(new BookContentItemDocument(), "isbn:"+isbn, SOLRServer.getInstance().getContentCore());
                for(BookContentItemDocument book: pdfsSOLR){
                    if(! (book.getPdfFilename() == null || book.getPdfFilename().isEmpty() )){
                        pdfs.add(book.getPdfFilename());
                        System.out.println("pdfFilename = "+book.getPdfFilename());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        for(String fileName : pdfs) {
            Item item = new Item();
            item.setIsbn(isbn);
            item.setFileName(fileName);
            item.setDataType("application/pdf");
            items.add(item);
        }
        
        List<String> abstractImages =  ContentFiles.getContentFiles(Loader.XPATH_ABSTRACT_IMAGES, xmlFile);
        for(String fileName : abstractImages) {
            Item item = new Item();
            item.setIsbn(isbn);
            item.setFileName(fileName);
            item.setDataType("abstract_image");
            items.add(item);
        }
        
        List<String> thumb =  ContentFiles.getContentFiles(Loader.XPATH_COVER_THUMB, xmlFile);
        for(String fileName : thumb) {
            Item item = new Item();
            item.setIsbn(isbn);
            item.setFileName(fileName);
            item.setDataType("title_image");
            items.add(item);
        }
        
        List<String> standard =  ContentFiles.getContentFiles(Loader.XPATH_COVER_STANDARD, xmlFile);
        for(String fileName : standard) {
            Item item = new Item();
            item.setIsbn(isbn);
            item.setFileName(fileName);
            item.setDataType("title_image");
            items.add(item);
        }
        
        Item item = new Item();
        item.setIsbn(isbn);
        item.setFileName(xmlFile.getName());
        item.setDataType("pdf_metadata");
        items.add(item);
        
        return items;
    }
    
    private static File getHeaderFileFromDirectory(File indexDir){
        File[] headerFile = indexDir.listFiles(
                new FileFilter() {
                    Pattern p = Pattern.compile("^\\d\\d+\\.xml$");
                    
                    public boolean accept(File f) {
                        return f.isFile() && p.matcher(f.getName()).find();
                    }
                });
        
        return (headerFile != null && headerFile.length > 0) ? headerFile[0] : null;
    }
    
    private static void modifyXMLDTD(File xmlFile, String replace, String replacement) throws IOException {
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        
        File bak = new File(xmlFile.getAbsolutePath().replace(".xml", "_" + format.format(new Date()) + "_bak.xml"));
        FileReader reader = new FileReader(xmlFile);
        FileWriter fw = null;
        
        StringBuilder sb = new StringBuilder();
        int temp = 0;
        while((temp = reader.read()) != -1){
            sb.append((char)temp);
        }

        //back up header file first before modifying
        fw = new FileWriter(bak);
        fw.write(sb.toString());
        fw.close() ;
        
//        sb = new StringBuilder(sb.toString().replace(headerFind, replacement)); //to make sure old header files with old DTD declaration get replaced
        sb = new StringBuilder(sb.toString().replace(replace, replacement));
        fw = new FileWriter(xmlFile);
        fw.write(sb.toString());
        fw.close() ;
            
    }
    
    private static List<?> getSignedOffBooks(String option){
        List<?> signedOffBooks = null;
        
        if(option != null) {
            if(ALL_SIGNED_OFF_BOOKS.equals(option)) {
                signedOffBooks = AssetEAO.signedOffBooks();
            } else if(ALL_SIGNED_OFF_BOOKS_FROM_FILE.equals(option)) {
                signedOffBooks = getIsbnsFromFile();
            } else if(option.startsWith(TEST_SIGNED_OFF_BOOKS)) {
                signedOffBooks = AssetEAO.signedOffBook(option);
            } else {
                signedOffBooks = AssetEAO.signedOffBooksToday();
            }
        }

        return signedOffBooks;
    }
    
    private static List<String> getIsbnsFromFile(){
        List<String> result = new ArrayList<String>();
        try  {
            File isbnListFile = new File(ApplicationProperties.ISBN_LIST);
            BufferedReader buff = new BufferedReader(new FileReader(isbnListFile));
            
            String temp = null;
            while((temp = buff.readLine()) != null){
                result.add(temp);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.err.println("[FileNotFoundException] Main.getIsbnsFromFile() " + ApplicationProperties.ISBN_LIST + " doesn't exist");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.err.println("[IOException] Main.getIsbnsFromFile() message: " + e.getMessage());
        }
        return result;
    }
    
    public static void copyfile(String srFile, String dtFile){
        try{
          File f1 = new File(srFile);
          File f2 = new File(dtFile);
          InputStream in = new FileInputStream(f1);
          
          //For Append the file.
//          OutputStream out = new FileOutputStream(f2,true);

          //For Overwrite the file.
          OutputStream out = new FileOutputStream(f2);

          byte[] buf = new byte[1024];
          int len;
          while ((len = in.read(buf)) > 0){
            out.write(buf, 0, len);
          }
          in.close();
          out.close();
          System.out.println("dummy dtd created..");
        }
        catch(FileNotFoundException ex){
          System.out.println(ex.getMessage() + " in the specified directory.");
          System.exit(0);
        }
        catch(IOException e){
          System.out.println(e.getMessage());      
        }
      }

}
