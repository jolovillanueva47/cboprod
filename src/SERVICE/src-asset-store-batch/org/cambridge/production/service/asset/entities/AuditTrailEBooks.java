package org.cambridge.production.service.asset.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * 
 * @author rvillamor
 *			   
 *
 */

@Entity
@Table(name = "AUDIT_TRAIL_EBOOKS")
public class AuditTrailEBooks  {
	
	@Id
	@Column(name = "AUDIT_EBOOKS_ID")
	private String auditId;
	
	private String eisbn;
	
	private int status;
	
	@Column(name = "SERIES_CODE")
	private String seriesId;
	
	@Column(name = "TITLE")
	private String title;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AUDIT_DATE")
	private Date auditDate;

	@Column(name = "USERNAME")
	private String userName;

	public String getAuditId() {
		return auditId;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
