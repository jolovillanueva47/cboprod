package org.cambridge.production.service.asset.util;

import java.io.File;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.cambridge.production.service.asset.properties.ApplicationProperties;

public class XmlTransformer {
	private File xmlFile;
	private File xsltFile;
	private File resultFile;
	private Source xmlSource;
	private Source xsltSource;
	private Result result;
	
	private XmlTransformer() {		
	}
	
	public static XmlTransformer newInstance() {
		return new XmlTransformer();
	}
	
	public void transform() throws TransformerConfigurationException, 
	TransformerException, NullPointerException {
		
		if(this.xmlFile == null) {
			throw new NullPointerException("[NULL]:xmlFile");
		}
		
		if(this.xsltFile == null) {
			throw new NullPointerException("[NULL]:xsltFile");
		}
		
		xmlSource = new StreamSource(this.xmlFile);
		xsltSource = new StreamSource(this.xsltFile);
		result = new StreamResult(this.resultFile);
		
		TransformerFactory transFact = TransformerFactory.newInstance();
		Transformer trans = transFact.newTransformer(this.xsltSource);
		trans.setParameter("supplier_name", ApplicationProperties.SUPPLIER_NAME);
		trans.transform(this.xmlSource, this.result);
	}

	public File getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}

	public File getXsltFile() {
		return xsltFile;
	}

	public void setXsltFile(File xsltFile) {
		this.xsltFile = xsltFile;
	}

	public File getResultFile() {
		return resultFile;
	}

	public void setResultFile(File resultFile) {
		this.resultFile = resultFile;
	}	
}
