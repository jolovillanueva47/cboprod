package org.cambridge.production.service.asset.properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;


public class ApplicationProperties {
	
	private Configuration config;
	private static final String PROPERTIES_FILE = System.getProperty("property.file") + ".properties";
	private static final ApplicationProperties APP_PROPERTIES = new ApplicationProperties();

	public static final String SPOOL_DIR 	= APP_PROPERTIES.config.getString("asset.spool.dir");
	public static final String TEMPLATE 	= APP_PROPERTIES.config.getString("asset.template");
	public static final String BASE_CONTENT_PATH = APP_PROPERTIES.config.getString("content.dir");
	public static final String CONTENT_DIR = APP_PROPERTIES.config.getString("dir.header.files");
	public static final String JDBC_URL = APP_PROPERTIES.config.getString("persistence.property");
	public static final String JDBC_URL_VALUE = APP_PROPERTIES.config.getString("jdbc.url");
	public static final String ISBN_LIST = APP_PROPERTIES.config.getString("isbn.list");
	public static final String XSL_DIR = APP_PROPERTIES.config.getString("xsl.dir");
	public static final String SUPPLIER_NAME = APP_PROPERTIES.config.getString("supplier.name");
	public static final String XSL_TRANSFORM = APP_PROPERTIES.config.getString("xsl.transform");
	
	public static final String SOCKET_TIMEOUT = APP_PROPERTIES.config.getString("socket.timeout");
	public static final String CONNECTION_TIMEOUT = APP_PROPERTIES.config.getString("connection.timeout");
	public static final String MAX_CONNECTION = APP_PROPERTIES.config.getString("default.max.connection.per.host");
	public static final String MAX_TOTAL_CONNECTION = APP_PROPERTIES.config.getString("max.total.connections");
	public static final String FOLLOW_REDIRECTS = APP_PROPERTIES.config.getString("follow.redirects");
	public static final String ALLOW_COMPRESSION = APP_PROPERTIES.config.getString("allow.compression");
	public static final String MAX_RETRIES = APP_PROPERTIES.config.getString("max.retries");
	
	public static final String SOLR_HOME_URL = APP_PROPERTIES.config.getString("solr.home.url");
	public static final String CORE_BOOK = APP_PROPERTIES.config.getString("core.book");
	public static final String CORE_CONTENT = APP_PROPERTIES.config.getString("core.content");
	
	private ApplicationProperties(){
		try 
		{
			config = new PropertiesConfiguration(PROPERTIES_FILE);	
		}
		catch (Exception e) 
		{
			System.err.println("AssetProperties() message: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

}
