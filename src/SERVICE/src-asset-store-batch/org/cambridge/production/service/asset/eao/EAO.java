package org.cambridge.production.service.asset.eao;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.production.service.asset.properties.ApplicationProperties;


public class EAO {
	
	private static final Map<String, String> props;
	static
	{
		props= new HashMap<String, String>();
        props.put(ApplicationProperties.JDBC_URL, ApplicationProperties.JDBC_URL_VALUE);
	}
	
	private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("assetUnit", props);
	
	public static EntityManager createEntityManager(){
		return EMF.createEntityManager();
	}
	
	public static void closeEntityManager(EntityManager em){
		if(em != null && em.isOpen())
			em.close();
	}
	
	public static EntityManagerFactory getEntityManagerFactory(){
		return EMF;
	}

}
