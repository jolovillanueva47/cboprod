package org.cambridge.production.service.asset.util;

import java.io.File;

import org.cambridge.production.service.asset.properties.ApplicationProperties;

public class ManifestToDlnConverter {
	
	private void transform(File pkgRoot, File camsPkg) {
		
		File manifest = new File(camsPkg + "/CBO/manifest.xml");
		File dln = new File(camsPkg + "/manifestdln.xml");
		File manifestXsl = new File(pkgRoot + "/xsl/manifest2dln.xsl");
		
		XmlTransformer transformer = XmlTransformer.newInstance();
		transformer.setXmlFile(manifest);
		transformer.setXsltFile(manifestXsl);
		transformer.setResultFile(dln);
		
		try {
			transformer.transform();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void transformManifest(File ebookContentDir, File xslDir) throws Exception {
		
		File manifest = new File(ebookContentDir + File.separator + "manifest.xml");
		File dln = new File(ebookContentDir + File.separator + "manifestdln.xml");
		File manifestXsl = new File(xslDir, "manifest2dln.xsl");
		
		XmlTransformer transformer = XmlTransformer.newInstance();
		transformer.setXmlFile(manifest);
		transformer.setXsltFile(manifestXsl);
		transformer.setResultFile(dln);
				
		try {
			transformer.transform();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		
	}
	
	public static void createDLN(String isbn) throws Exception {

		ManifestToDlnConverter converter = new ManifestToDlnConverter();
		File ebookContentDir = new File(IsbnContentDirUtil.getPath(isbn));
		File xslDir = new File(ApplicationProperties.XSL_DIR);
		
		System.out.println("Processing: " + isbn);
		converter.transformManifest(ebookContentDir, xslDir);
			
	}
	
	public static void deleteDLN(String isbn) throws Exception {
		File ebookContentDir = new File(IsbnContentDirUtil.getPath(isbn));
		File dln = new File(ebookContentDir + File.separator + "manifestdln.xml");
		
		if(dln.exists())
			if(!dln.delete())
				throw new Exception("manifestdln.xml was not removed!");
	}
	
	
	public static void main(String[] args) {

		ManifestToDlnConverter converter = new ManifestToDlnConverter();
		File pkgRoot = new File(System.getProperty("user.dir"));
		
		if (args.length > 0) {
			for (String camsPkg : args) {
				System.out.println("Processing: " + camsPkg);
				converter.transform(pkgRoot, new File(pkgRoot.getParentFile() + "/" + camsPkg));
			}
		} else {
			
			for (File f : pkgRoot.getParentFile().listFiles()) {
				if (f.isDirectory() && !pkgRoot.getName().equals(f.getName())) {
					System.out.println("Processing: " + f.getName());
					converter.transform(pkgRoot, f);
				}
			}
		}
		
	}
	
	
}
