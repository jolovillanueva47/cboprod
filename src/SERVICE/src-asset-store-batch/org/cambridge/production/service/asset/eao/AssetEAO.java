package org.cambridge.production.service.asset.eao;


import java.util.List;

import org.cambridge.production.service.asset.entities.EBook;
import org.cambridge.production.service.asset.entities.Publisher;





public class AssetEAO {
	
	public static List<EBook> signedOffBooks(){
		String sql = "SELECT DISTINCT e.* " + 
				     "FROM oraebooks.ebook e " + 
				     "JOIN oraebooks.ebook_isbn_data_load d " +
				     "ON(e.isbn = d.isbn) " +
				     "WHERE e.status = 7 "// + 
				     //"AND d.online_flag = 'Y' "
				     ;
		
		return EAOUtil.nativeQuery(EBook.class, sql);
	}
	
	public static List<EBook> signedOffBook(String isbn){
		return EAOUtil.nativeQuery(EBook.class, "SELECT e.* FROM oraebooks.EBook e WHERE e.status = 7 AND e.isbn = ?1", isbn);
	}

	public static List<EBook> signedOffBooksToday(){
		final String sql = 	"SELECT DISTINCT e.* " + 
							"FROM ebook e " + 
							"JOIN audit_trail_ebooks a " +
							"ON(e.isbn = a.eisbn) " +
							"JOIN ebook_isbn_data_load d " +
							"ON(e.isbn = d.isbn) " + 
							"WHERE e.status = 7 " +
							"AND a.status = 7 " +
							//"AND d.online_flag = 'Y' " +
							"AND TO_CHAR(a.audit_date, 'DD-MON-YY') = TO_CHAR(SYSDATE - 1, 'DD-MON-YY') ";
		
		return EAOUtil.nativeQuery(EBook.class, sql);
	}
	
	public static List<Publisher> getPublisherInfo(String isbn){
		final String sql = "SELECT PUBLISHER_CODE, PUBLISHER_LEGEND " +
			"FROM oraebooks.SR_PUBLISHER " +
			"WHERE PUBLISHER_CODE = " +
			"   (SELECT publisher_code from oraebooks.SR_CBO_PRODUCT_GROUP_ORA where CBO_PRODUCT_GROUP = " +
			"      (select CBO_PRODUCT_GROUP from oraebooks.core_isbn_cbo where ean_number = ?1))" +
			"AND PUBLISHER_ACTIVE_FLAG = 'Y'"; 
		return EAOUtil.nativeQuery(Publisher.class, sql, isbn);
	}
	
}