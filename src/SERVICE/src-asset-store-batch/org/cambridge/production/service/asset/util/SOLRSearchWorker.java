package org.cambridge.production.service.asset.util;


import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;


public class SOLRSearchWorker{
	public SOLRSearchWorker(){}	


	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer) {
		List<T> result = new ArrayList<T>();
		
		SolrQuery q = generateQuery(searchText);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] List<T> SOLRSearchWorker.searchCore(T, String, SolrServer)" + ex.getMessage());
		}
			
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T searchContentCoreSinglePager(T t, String searchText, SolrServer solrServer, int page) {
		T result = t;
		
		SolrQuery q = generateQuery(searchText);
		onePagerPagination(q, page);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (T)response.getBeans(t.getClass()).get(0);
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] T SOLRSearchWorker.searchContentCoreSinglePager(T, String, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer, int rowcount) {
		List<T> result = new ArrayList<T>();
		
		SolrQuery q = generateQuery(searchText);
		setQueryRowCount(q, rowcount);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] SOLRSearchWorker.searchContentCoreSinglePager(T, String, SolrServer, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	public SolrQuery generateQuery(String searchText){
		SolrQuery query = new SolrQuery();
		query.setQuery(searchText);
		query.setRows(1000);
		return query;
	}
	
	
	public QueryResponse generateResponse(SolrQuery q, SolrServer server, SolrRequest.METHOD m){
		QueryResponse response = null;
		try 
		{
			response = server.query(q, m);
		} 
		catch (SolrServerException e) {
			// TODO Auto-generated catch block
			System.err.println("[SolrServerException] QueryResponse SOLRSearchWorker. generateResponse(SolrQuery, SolrServer, SolrRequest.METHOD) message: " + e.getMessage());
		}
		
		return response;
	}

	
	private void onePagerPagination(SolrQuery q, int pageNum) {
		q.setRows(1);
		q.setStart(pageNum);
	}
	
	private void setQueryRowCount(SolrQuery q, int rowcount) {
		q.setRows(rowcount);
	}
}
