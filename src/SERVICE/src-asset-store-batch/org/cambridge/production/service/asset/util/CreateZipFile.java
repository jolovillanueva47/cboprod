package org.cambridge.production.service.asset.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CreateZipFile {

	public static int BUFFER_SIZE = 10240;

	public static void createZipArchive(File archiveFile, File[] tobeZippedFiles, String isbn, String publisher) throws Exception {
		byte buffer[] = new byte[BUFFER_SIZE];
		
		// Open archive file
		FileOutputStream stream = new FileOutputStream(archiveFile);
		ZipOutputStream out = new ZipOutputStream(stream);

		for (int i = 0; i < tobeZippedFiles.length; i++) 
		{
			if (tobeZippedFiles[i] == null || !tobeZippedFiles[i].exists() || tobeZippedFiles[i].isDirectory())
				continue;
			
			//System.out.println("Adding " + tobeZippedFiles[i].getName());

			// Add archive entry
			
			String pathInZipFile = "";
			if (publisher.equals("CUP")){
				pathInZipFile = !"manifestdln.xml".equals(tobeZippedFiles[i].getName()) ? "cams_pkg_" + isbn + "/CBO/" : "cams_pkg_" + isbn + "/";
			} else {
				pathInZipFile = isbn + "/";
			}
			ZipEntry zipAdd = new ZipEntry(pathInZipFile + tobeZippedFiles[i].getName());
			zipAdd.setTime(tobeZippedFiles[i].lastModified());
			out.putNextEntry(zipAdd);

			// Read input & write to output
			FileInputStream in = new FileInputStream(tobeZippedFiles[i]);
			while(true) 
			{
				int nRead = in.read(buffer, 0, buffer.length);
				if (nRead <= 0)
					break;
				out.write(buffer, 0, nRead);
			}
			in.close();
		}

		out.close();
		stream.close();
		//System.out.println("Adding completed OK [" + archiveFile + "] [" + new Date() + "]");
	}

}
