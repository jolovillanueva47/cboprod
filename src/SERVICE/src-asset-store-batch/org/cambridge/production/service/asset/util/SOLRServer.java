package org.cambridge.production.service.asset.util;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.cambridge.production.service.asset.properties.ApplicationProperties;

public class SOLRServer {
	
	private SolrServer bookCore;
	private SolrServer contentCore;
	private static final SOLRServer SOLRSERVER = new SOLRServer();
	
	public static final String KEY_SOCKET_TIMEOUT = "KEY_SOCKET_TIMEOUT";
	public static final String KEY_CONN_TIMEOUT = "KEY_CONN_TIMEOUT";
	public static final String KEY_DEF_MAX_CONN_PER_HOST = "KEY_DEF_MAX_CONN_PER_HOST";
	public static final String KEY_MAX_TOTAL_CONN = "KEY_MAX_TOTAL_CONN";
	public static final String KEY_FOLLOW_REDIRECTS = "KEY_FOLLOW_REDIRECTS";
	public static final String KEY_ALLOW_COMPRESSION = "KEY_ALLOW_COMPRESSION";
	public static final String KEY_MAX_RETRIES = "KEY_MAX_RETRIES";
	
	private SOLRServer(){
		try 
		{
			Map<String, String> settings = new HashMap<String, String>();
			settings.put(SOLRServer.KEY_SOCKET_TIMEOUT, ApplicationProperties.SOCKET_TIMEOUT);
			settings.put(SOLRServer.KEY_CONN_TIMEOUT, ApplicationProperties.CONNECTION_TIMEOUT);
			settings.put(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST, ApplicationProperties.MAX_CONNECTION);
			settings.put(SOLRServer.KEY_MAX_TOTAL_CONN, ApplicationProperties.MAX_TOTAL_CONNECTION);
			settings.put(SOLRServer.KEY_FOLLOW_REDIRECTS, ApplicationProperties.FOLLOW_REDIRECTS);
			settings.put(SOLRServer.KEY_ALLOW_COMPRESSION, ApplicationProperties.ALLOW_COMPRESSION);
			settings.put(SOLRServer.KEY_MAX_RETRIES, ApplicationProperties.MAX_RETRIES);
			
			bookCore = initServer(new CommonsHttpSolrServer(ApplicationProperties.SOLR_HOME_URL + ApplicationProperties.CORE_BOOK), settings);;	
			contentCore = initServer(new CommonsHttpSolrServer(ApplicationProperties.SOLR_HOME_URL + ApplicationProperties.CORE_CONTENT), settings);		

		} 
		catch(MalformedURLException e)
		{
			System.err.println("[MalformedURLException] SOLRServer() message: " + e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			System.err.println("[Exception] SOLRServer() message: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
		
	public SolrServer getBookCore() {
		return bookCore;
	}
	
	public SolrServer getContentCore() {
		return contentCore;
	}
	
	public static SOLRServer getInstance(){
		return SOLRSERVER;
	}
	
	private SolrServer initServer(CommonsHttpSolrServer server, Map<String, String> settings){

		server.setSoTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_SOCKET_TIMEOUT)));
		server.setConnectionTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_CONN_TIMEOUT)));
		server.setDefaultMaxConnectionsPerHost(Integer.parseInt(settings.get(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST)));
		server.setMaxTotalConnections(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_TOTAL_CONN)));
		server.setFollowRedirects(Boolean.parseBoolean(settings.get(SOLRServer.KEY_FOLLOW_REDIRECTS)));
		server.setAllowCompression(Boolean.parseBoolean(settings.get(SOLRServer.KEY_ALLOW_COMPRESSION)));
		server.setMaxRetries(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_RETRIES)));
		

		return server;
	}
}
