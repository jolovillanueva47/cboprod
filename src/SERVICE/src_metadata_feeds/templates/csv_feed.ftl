ID,Group,Pub Price,Subscription Price,Title (Alphasort),Subtitle,DOI,Print Date,Online Date,Volume Number,Volume Title,Part Number,Part Title
${book.id},${book.group},<#if book.price??>"${book.price} USD"</#if>,<#if book.tieredPrice??>"${book.tieredPrice} USD"</#if>,"${book.title.text} (${book.title.alphasort})",<#if book.subtitle??>"${book.subtitle}"</#if>,<#if book.doi??>${book.doi}</#if>,="${book.pubDate.print}",="${book.pubDate.online}",<#if book.volumeNumber??>="${book.volumeNumber}"</#if>,<#if book.volumeTitle??>"${book.volumeTitle}"</#if>,<#if book.partNumber??>="${book.partNumber}"</#if>,<#if book.partTitle??>"${book.partTitle}"</#if>

	,ISBN
	,Type,Value
	,e-isbn,="${book.isbn}"
	<#list book.altIsbns as altIsbn>			
	,${altIsbn.type},="${altIsbn.value}"
	</#list>

	,Author(s)
	,Position,Role,Name (Alphasort),Affiliation
	<#list book.authors as author>
	<#assign name = author.name?replace("<forenames>","","ri")>
	<#assign name = name?replace("</forenames>"," ","ri")>
	<#assign name = name?replace("<surname>","","ri")>
	<#assign name = name?replace("</surname>"," ","ri")>
	<#assign name = name?replace("<name-link>","","ri")>
	<#assign name = name?replace("</name-link>"," ","ri")>
	<#assign name = name?replace("<suffix>","","ri")>
	<#assign name = name?replace("</suffix>"," ","ri")>
	<#assign name = name?replace("<collab>",", ","ri")>
	<#assign name = name?replace("</collab>","","ri")>
	<#assign name = name?replace("<.*?>","","ri")>
	<#assign name = name?replace("</.*?>","","ri")>
	,${author.position},${author.role},"${name} (${author.alphasort})",<#if author.affiliation??>"${author.affiliation}"</#if>
	</#list>

	<#if book.series??>
	,Series
	,Position,Code,Series (Alphasort),Number
	,<#if book.series.position??>${book.series.position}</#if>,<#if book.series.code??>${book.series.code}</#if>,"${book.series.title} (<#if book.series.alphasort??>${book.series.alphasort}</#if>)",<#if book.series.number??>="${book.series.number}"</#if>
	
	</#if>
	<#if book.subjects?has_content>
	,Subject
	,Level,Code,Name
	<#list book.subjects as subject>
	,="${subject.level}",="${subject.code}","${subject.name}"
	</#list>
	
	</#if>
	<#if book.edition??>
	,Edition
	,Number,Text
	,<#if book.edition.number??>="${book.edition.number}"</#if>,"${book.edition.text}"
	
	</#if>
	,Blurb
	<#assign blurb = book.blurb?replace("<.*?>","","ri")>
	<#assign blurb = name?replace("</.*?>","","ri")>
	,"${book.blurb}"

	,Cover Image
	,Type,Filename
	<#list book.coverImages as coverImage>		
	,${coverImage.type},${coverImage.filename}
	</#list>

Content Items
<#list book.contentItems as contentItem>
	,Content Item
	,Parent ID,ID,Type,Position,Page Start,Page End,PDF Filename,DOI
	,${contentItem.parentId},${contentItem.id},${contentItem.type},="${contentItem.position}",<#if contentItem.pageStart??>="${contentItem.pageStart}"</#if>,<#if contentItem.pageEnd??>="${contentItem.pageEnd}"</#if>,<#if contentItem.pdfFilename??>${contentItem.pdfFilename}</#if>,<#if contentItem.doi??>${contentItem.doi}</#if>

	,Header
	,Label,Title (Alphasort),Subtitle
	,<#if contentItem.heading.label??>${contentItem.heading.label}</#if>,"${contentItem.heading.title.text} (${contentItem.heading.title.alphasort})",<#if contentItem.heading.subtitle??>"${contentItem.heading.subtitle}"</#if>
		
	<#if contentItem.contributors?has_content>
	,Contributor(s)
	,Position,Name (Alphasort),Affiliation
	<#list contentItem.contributors as contributor>
	<#assign name = contributor.name?replace("<forenames>","","ri")>
	<#assign name = name?replace("</forenames>"," ","ri")>
	<#assign name = name?replace("<surname>","","ri")>
	<#assign name = name?replace("</surname>"," ","ri")>
	<#assign name = name?replace("<name-link>","","ri")>
	<#assign name = name?replace("</name-link>"," ","ri")>
	<#assign name = name?replace("<suffix>","","ri")>
	<#assign name = name?replace("</suffix>"," ","ri")>
	<#assign name = name?replace("<collab>",", ","ri")>
	<#assign name = name?replace("</collab>","","ri")>
	<#assign name = name?replace("<.*?>","","ri")>
	<#assign name = name?replace("</.*?>","","ri")>
	,${contributor.position},"${name} (${contributor.alphasort})",<#if contributor.affiliation??>"${contributor.affiliation}"</#if>
	</#list>
	
	</#if>
	<#if contentItem.keywordGroups?has_content>
	<#list contentItem.keywordGroups as keywordGroup>
	<#assign _keyword = "">
	<#list keywordGroup.keywords as keyword>
	<#assign _keyword = _keyword + keyword + ";">
	</#list>
	<#assign _keyword = _keyword?replace("<.*?>","","ri")>
	<#assign _keyword = _keyword?replace("</.*?>","","ri")>
	,Keyword Group(s)
	,Source,Keywords
	,${keywordGroup.source},"${_keyword}"
	</#list>
	
	</#if>
	<#if contentItem.tocItems?has_content>
	<#list contentItem.tocItems as tocItem>
	,TOC
	,Parent ID,ID,Start Page,Text
	<#assign tocText = tocItem.text?replace("<.*?>","","ri")>
	<#assign tocText = tocText?replace("</.*?>","","ri")>
	,${tocItem.parentId},${tocItem.id},${tocItem.startPage},${tocText}
	
		<#if tocItem.authors?has_content>
		,,Author(s)
		,,Position,Role,Name (Alphasort),Affiliation
		<#list tocItem.authors as author>	
		<#assign name = author.name?replace("<forenames>","","ri")>
		<#assign name = name?replace("</forenames>"," ","ri")>
		<#assign name = name?replace("<surname>","","ri")>
		<#assign name = name?replace("</surname>"," ","ri")>
		<#assign name = name?replace("<name-link>","","ri")>
		<#assign name = name?replace("</name-link>"," ","ri")>
		<#assign name = name?replace("<suffix>","","ri")>
		<#assign name = name?replace("</suffix>"," ","ri")>
		<#assign name = name?replace("<collab>",", ","ri")>
		<#assign name = name?replace("</collab>","","ri")>
		<#assign name = name?replace("<.*?>","","ri")>
		<#assign name = name?replace("</.*?>","","ri")>
		,,${author.position},${author.role},"${name} (${author.alphasort})",<#if author.affiliation??>"${author.affiliation}"</#if>
		</#list>
		</#if>
	</#list>
	
	</#if>
	<#if contentItem.contentAbstract??>
	<#assign abstractText = contentItem.contentAbstract.text?replace("<.*?>","","ri")>
	<#assign abstractText = abstractText?replace("</.*?>","","ri")>
	,Abstract
	,Filename,Problem,Text
	,${contentItem.contentAbstract.filename},${contentItem.contentAbstract.problem},"${abstractText}"
	
	</#if>
	<#if contentItem.references?has_content>
	<#list contentItem.references as reference>
	<#assign refTitle = "">
	<#if reference.title??>
	<#if reference.title.text??>
	<#assign refTitle = reference.title.text?replace("<.*?>","","ri")>
	<#assign refTitle = refTitle?replace("</.*?>","","ri")>
	</#if>
	</#if>
	,Reference
	,Type,Title (Alphasort)
	,<#if reference.type??>"${reference.type}"</#if>,<#if reference.title??>"${refTitle} (${reference.title.alphasort})"</#if>

		<#if reference.citations?has_content>
		,,Citations
		,,ID,Type,Text
		<#list reference.citations as citation>		
		,,${citation.id},${citation.type},"${citation.text}"
		</#list>
		</#if>
	</#list>
	</#if>
</#list>