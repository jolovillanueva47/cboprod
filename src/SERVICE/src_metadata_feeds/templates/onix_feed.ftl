<?xml version='1.0' encoding='UTF-8'?>
<ONIXMessage release="3.0">
	<Header>
		<Sender>
	   		<SenderName>Cambridge University Press</SenderName>
	   		<ContactName>Cambridge Books Online</ContactName>
	   		<EmailAddress>cbo@cambridge.org</EmailAddress>
	  	</Sender>
	  	<SentDateTime>YYYYMMDDTHHMM</SentDateTime>
	  	<DefaultLanguageOfText>eng</DefaultLanguageOfText>
	</Header>
	<Product>
		<RecordReference>${book.isbn}</RecordReference>
		<NotificationType>01</NotificationType>
		<RecordSourceType>01</RecordSourceType>
		<RecordSourceName>Cambridge University Press</RecordSourceName>
		<ProductIdentifier>
			<ProductIDType>02</ProductIDType>
		   	<IDValue>${book.isbn}</IDValue>
		</ProductIdentifier>
		<DescriptiveDetail>
			<ProductComposition>00</ProductComposition>
			<ProductForm>00</ProductForm>
			<EpubTechnicalProtection>01</EpubTechnicalProtection>
			<NoCollection/>
			<TitleDetail>		
				<#assign has01TitleElementLevel = false>			
				<#if book.series.code?? || book.volumeNumber?? || book.partNumber?? || book.volumeTitle?? || book.partTitle??>
				<TitleType>00</TitleType>
				<#else>
				<TitleType>01</TitleType>
				</#if>				
				<#if book.series.code??>
				<TitleElement>
        			<TitleElementLevel>02</TitleElementLevel>
        			<TitleText>${book.series.title}</TitleText>        			
 				</TitleElement>
				<#elseif book.volumeNumber?? || book.partNumber?? || book.volumeTitle?? || book.partTitle??>				
				<TitleElement>
        			<TitleElementLevel>02</TitleElementLevel>
        			<TitleText>${book.series.title}</TitleText>        			
 				</TitleElement>
 				</#if>
				<#if book.partNumber??>
				<#assign has01TitleElementLevel = true>	
				<TitleElement>
			        <TitleElementLevel>01</TitleElementLevel>
			        <PartNumber>Part ${book.partNumber}</PartNumber>
			        <TitleText>${book.partTitle}</TitleText>
		      	</TitleElement>
		      	</#if>
		      	<#if book.volumeNumber??>
				<TitleElement>
					<#if book.partNumber?? && book.volumeNumber != book.title.text>
			        <TitleElementLevel>03</TitleElementLevel>
			        <#else>
			        <#assign has01TitleElementLevel = true>	
			        <TitleElementLevel>01</TitleElementLevel>
			        </#if>
			        <PartNumber>Volume ${book.volumeNumber}</PartNumber>
			        <TitleText>${book.volumeTitle}</TitleText>
		      	</TitleElement>
		      	</#if>		      	
				<TitleElement>
					<#if has01TitleElementLevel>
					<TitleElementLevel>03</TitleElementLevel>
					<#else>
					<TitleElementLevel>01</TitleElementLevel>
					</#if>
					<#if book.series.number?? && book.series.number != 0>
					<PartNumber>Series Number ${book.series.number}</PartNumber>
					</#if>
        			<TitleText>${book.title.text}</TitleText>
        			<#if book.subtitle??><Subtitle>${book.subtitle}</Subtitle></#if>			
 				</TitleElement>
			</TitleDetail>
			<#list book.authors as author>	
			<#assign name = author.name?replace("<forenames>","","ri")>
			<#assign name = name?replace("</forenames>"," ","ri")>
			<#assign name = name?replace("<surname>","","ri")>
			<#assign name = name?replace("</surname>"," ","ri")>
			<#assign name = name?replace("<name-link>","","ri")>
			<#assign name = name?replace("</name-link>"," ","ri")>
			<#assign name = name?replace("<suffix>","","ri")>
			<#assign name = name?replace("</suffix>"," ","ri")>
			<#assign name = name?replace("<collab>",", ","ri")>
			<#assign name = name?replace("</collab>","","ri")>
			<#assign name = name?replace("<.*?>","","ri")>
			<#assign name = name?replace("</.*?>","","ri")>
			<#assign forenames = author.name?replace("<forenames>","","ri")>
			<#assign forenames = forename?replace("</forenames>*,?","","ri")>
			<#assign surname = author.name?replace("<surname>","","ri")>
			<#assign surname = surname?replace("</surname>*,?","","ri")>
			<#assign surname = surname?replace("<forenames>.*?</forenames>","","ri")>
			<#assign surname = surname?replace("<.*?>.*?</.*?>","","ri")>
			<Contributor>
				<SequenceNumber>${author.position}</SequenceNumber>
				<ContributorRole>${author.role!"AU"}</ContributorRole>
				<PersonName>${name}</PersonName>
				<PersonNameInverted>${author.alphasort}</PersonNameInverted>
				<NamesBeforeKey>${forenames}</NamesBeforeKey>
				<KeyNames>${surname}</KeyNames>
				<ProfessionalAffiliation>
			    	<Affiliation>${author.affiliation}</Affiliation>
			    </ProfessionalAffiliation>
			</Contributor>
			</#list>
			<#if book.edition??>
			<#if book.edition.number??><EditionNumber>${book.edition.number}</EditionNumber></#if> 
			<EditionStatement>${book.edition.text}</EditionStatement>
			<#else>
			<NoEdition />
			</#if>
			<#list book.subjects as subject>
			<Subject>
				<SubjectCode>${subject.code}</SubjectCode>
				<SubjectHeadingText>${subject.name}</SubjectHeadingText>
			</Subject>
			</#list>
		</DescriptiveDetail>
		<CollateralDetail>
			<TextContent>
				<TextType>02</TextType>
    			<ContentAudience>00</ContentAudience>
    			<Text>${book.blurb}</Text>
			</TextContent>
		</CollateralDetail>
		<PublishingDetail>
			<PublishingRole>01</PublishingRole>
			<PublisherName>Cambridge University Press</PublisherName>
		</PublishingDetail>
		<PublishingDate>
			<PublishingDateRole>01</PublishingDateRole>
			<DateFormat>00</DateFormat>
			<Date>${book.pubDate.print}</Date>
		</PublishingDate>
		<ContentDetail> 
			<#list book.contentItems as contentItem>
			<ContentItem>
				<LevelSequenceNumber>${contentItem.id}<LevelSequenceNumber>  
			</ContentItem>
			</#list>
		</ContentDetail> 
	</Product>
</ONIXMessage>