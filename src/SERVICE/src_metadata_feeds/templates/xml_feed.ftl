<?xml version="1.0" encoding="UTF-8"?>
<#list books as book>
<book id="${book.id}" <#if book.group??>group="${book.group}"</#if>>		
	<metadata>		
		<title alphasort="${book.title.alphasort}">${book.title.text}</title>
		<#if book.subtitle??><subtitle>${book.subtitle}</subtitle></#if>
		
		<#if book.price??><price>${book.price} USD</price></#if>
		<#if book.tieredPrice??><tiered-prices>${book.tieredPrice} USD</tiered-prices></#if>
		
		<#list book.authors as author>			
		<author <#if author.id??>id="${author.id}"</#if> <#if author.position??>position="${author.position}"</#if> <#if author.alphasort??>alphasort="${author.alphasort}"</#if> role="${author.role!"AU"}">
			<name>${author.name}</name>
			<#if author.affiliation??><affiliation>${author.affiliation}</affiliation></#if>
		</author>
		</#list>
		<#if book.doi??><doi>${book.doi}</doi></#if>
		<isbn>${book.isbn}</isbn>
		<#list book.altIsbns as altIsbn>			
		<alt-isbn type="${altIsbn.type}">${altIsbn.value}</alt-isbn>
		</#list>
		<#if book.series??><series <#if book.series.number??>number="${book.series.number}"</#if> <#if book.series.code??>code="${book.series.code}"</#if> <#if book.series.position??>position="${book.series.position}"</#if> <#if book.series.alphasort??>alphasort="${book.series.alphasort}"</#if>>${book.series.title}</series></#if>
		<pub-dates>
			<print-date>${book.pubDate.print}</print-date>
			<online-date>${book.pubDate.online}</online-date>
		</pub-dates>
		<#if book.subjects?has_content>
		<subject-group>
			<#list book.subjects as subject>
			<subject level="${subject.level}" code="${subject.code}">${subject.name}</subject>
			</#list>
		</subject-group>
		</#if>
		<#if book.edition??><edition <#if book.edition.number??>number="${book.edition.number}"</#if>>${book.edition.text}</edition></#if>
		<#if book.volumeNumber??><volume-number>${book.volumeNumber}</volume-number></#if>
		<#if book.volumeTitle??><volume-title>${book.volumeTitle}</volume-title></#if>
		<#if book.partNumber??><part-number>${book.partNumber}</part-number></#if>
		<#if book.partTitle??><part-title>${book.partTitle}</part-title></#if>
		<blurb>${book.blurb}</blurb>
		<#list book.coverImages as coverImage>		
		<cover-image filename="${coverImage.filename}" type="${coverImage.type}" />
		</#list>
	</metadata>
	<content-items id="${book.contentItemsId}">
		<#list book.contentItems as contentItem>
		<content-item parent-id="${contentItem.parentId}" id="${contentItem.id}" type="${contentItem.type}" position="${contentItem.position}" <#if contentItem.pageStart??>page-start="${contentItem.pageStart}"</#if> <#if contentItem.pageEnd??>page-end="${contentItem.pageEnd}"</#if>>
			<#if contentItem.pdfFilename??><pdf filename="${contentItem.pdfFilename}" /></#if>
			<#if contentItem.doi??><doi>${contentItem.doi}</doi></#if>
			<heading>
				<#if contentItem.heading.label??><label>${contentItem.heading.label}</label></#if>
				<title alphasort="${contentItem.heading.title.alphasort}">${contentItem.heading.title.text}</title>		
				<#if contentItem.heading.subtitle??><subtitle>${contentItem.heading.subtitle}</subtitle></#if>		
			</heading>
			<#if contentItem.contributors?has_content>
			<contributor-group>
				<#list contentItem.contributors as contributor>
				<contributor position="${contributor.position}" alphasort="${contributor.alphasort}">
					<name>${contributor.name}</name>
					<#if contributor.affiliation??><affiliation>${contributor.affiliation}</affiliation></#if>
				</contributor>
				</#list>
			</contributor-group>
			</#if>
			<#if contentItem.keywordGroups?has_content>
			<#list contentItem.keywordGroups as keywordGroup>
			<keyword-group source="${keywordGroup.source}">
				<#list keywordGroup.keywords as keyword>
				<keyword>
					<kwd-text>${keyword}</kwd-text>
				</keyword>
				</#list>
			</keyword-group>
			</#list>
			</#if>
			<#if contentItem.tocItems?has_content>
			<toc>
				<#list contentItem.tocItems as tocItem>
				<toc-item startpage="${tocItem.startPage}" id="${tocItem.id}" parent-id="${tocItem.parentId}">
					<toc-text>${tocItem.text}</toc-text>
					<#if tocItem.authors?has_content>
					<#list tocItem.authors as author>			
					<author <#if author.id??>id="${author.id}"</#if> <#if author.position??>position="${author.position}"</#if> <#if author.alphasort??>alphasort="${author.alphasort}"</#if> role="${author.role}">
						<name>${author.name}</name>
						<#if author.affiliation??><affiliation>${author.affiliation}</affiliation></#if>
					</author>
					</#list>
					</#if>
				</toc-item>
				</#list>
			</toc>
			</#if>
			<#if contentItem.contentAbstract??>
			<abstract alt-filename="${contentItem.contentAbstract.filename}" problem="${contentItem.contentAbstract.problem}">
				${contentItem.contentAbstract.text}
			</abstract>
			</#if>
			<#if contentItem.references?has_content>
			<#list contentItem.references as reference>
			<references <#if reference.type??>type="${reference.type}"</#if>>
				<#if reference.title??>
				<title alphasort="${reference.title.alphasort}">${reference.title.text}</title>
				</#if>
				<#if reference.citations?has_content>
				<#list reference.citations as citation>
				<citation id="${citation.id}" type="${citation.type}">
					${citation.text}
				</citation>
				</#list>
				</#if>
			</references>
			</#list>
			</#if>
		</content-item>
		</#list>
	</content-items>
</book>
</#list>