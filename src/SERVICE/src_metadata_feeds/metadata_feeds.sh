#!/bin/sh
HOME="/app/ebooks/service/metadata_feeds"
LIB="$HOME/lib"
BIN="$HOME/bin"
EXEC_JAR=$LIB"/metadata_feeds.jar"
NOW=`date '+metadata.feeds.log.%Y-%d-%m'`
LOG_FILE="$HOME/log/$NOW.txt"
JAVA="/app/jdk1.6.0_20/bin/java"

OUTPUT="/app/ebooks/service/metadata_feeds/output"
PROCESSED="/app/ebooks/service/metadata_feeds/processed"

LOCK=$HOME/process.lock

echo "[metadata_feeds.sh] Executing script..." >> $LOG_FILE


#check for lockfile, create if there is none
if [ -f $LOCK ]; then
  echo "Still processing previous job" >> $LOG_FILE
  exit
fi 
	echo "Creating lock..." >> $LOG_FILE
	touch $LOCK
	echo "Lock created." >> $LOG_FILE



#execute jar
$JAVA -jar -Xms128m -Xmx512m $EXEC_JAR >> $LOG_FILE



#move directories from output to processed
echo "Moving transfered input directories..." >> $LOG_FILE
find $OUTPUT \( ! -name output -prune \) \
-exec mv {} $PROCESSED \;
echo "Directories transfered." >> $LOG_FILE

#remove lockfile
if [ -f $LOCK ]; then
	echo "Removing lock..." >> $LOG_FILE
  	rm $LOCK
  	echo "Lock removed." >> $LOG_FILE
fi

echo "[metadata_feeds.sh] Process completed." >> $LOG_FILE