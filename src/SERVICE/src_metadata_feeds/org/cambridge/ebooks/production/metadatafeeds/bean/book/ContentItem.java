package org.cambridge.ebooks.production.metadatafeeds.bean.book;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class ContentItem {
	private String parentId;
	private String id;
    private String pageStart;
    private String pageEnd;
    private String type;
    private String position;
    private String doi;    
    private String pdfFilename;
    
    private Heading heading;
    private Abstract contentAbstract;
    
    private List<Author> contributors;
    private List<KeywordGroup> keywordGroups;
    private List<TocItem> tocItems;   
    private List<References> references;
    
    public ContentItem(){}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public Heading getHeading() {
		return heading;
	}

	public void setHeading(Heading heading) {
		this.heading = heading;
	}

	public Abstract getContentAbstract() {
		return contentAbstract;
	}

	public void setContentAbstract(Abstract contentAbstract) {
		this.contentAbstract = contentAbstract;
	}

	public List<Author> getContributors() {
		return contributors;
	}

	public void setContributors(List<Author> contributors) {
		this.contributors = contributors;
	}
	
	public List<KeywordGroup> getKeywordGroups() {
		return keywordGroups;
	}

	public void setKeywordGroups(List<KeywordGroup> keywordGroups) {
		this.keywordGroups = keywordGroups;
	}

	public List<TocItem> getTocItems() {
		return tocItems;
	}

	public void setTocItems(List<TocItem> tocItems) {
		this.tocItems = tocItems;
	}

	public List<References> getReferences() {
		return references;
	}

	public void setReferences(List<References> references) {
		this.references = references;
	}	
}
