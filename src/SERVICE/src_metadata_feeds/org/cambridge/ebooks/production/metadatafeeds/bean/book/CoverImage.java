package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class CoverImage {
	private String filename;
	private String type;
	
	public CoverImage(){}

	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
