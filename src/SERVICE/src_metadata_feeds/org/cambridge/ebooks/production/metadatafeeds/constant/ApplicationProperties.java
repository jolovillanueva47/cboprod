package org.cambridge.ebooks.production.metadatafeeds.constant;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.cambridge.ebooks.production.metadatafeeds.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class ApplicationProperties {

	private static Configuration config;
	
	static {
		try {
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + ".properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			System.err.println("[Error] " + ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static final String INPUT_DIR = config.getString("input.dir");
	public static final String OUTPUT_DIR = config.getString("output.dir");
	public static final String PROCESSED_DIR = config.getString("processed.dir");
	public static final String CONTENT_DIR = config.getString("content.dir");
	
	public static final String STAMP_DRM_IMAGE = config.getString("stamp.drm.image");
	
	public static final String FREEMARKER_TEMPLATE_DIR = config.getString("freemarker.template.dir");
	public static final String FREEMARKER_XML_FEED_TEMPLATE = config.getString("freemarker.xml.feed.template");
	public static final String FREEMARKER_CSV_FEED_TEMPLATE = config.getString("freemarker.csv.feed.template");
	
	public static final String JDBC_DRIVER = config.getString("jdbc.driver");
	public static final String JDBC_URL = config.getString("jdbc.url");
	public static final String JDBC_USER = config.getString("jdbc.user");
	public static final String JDBC_PASS = config.getString("jdbc.pass");
}
