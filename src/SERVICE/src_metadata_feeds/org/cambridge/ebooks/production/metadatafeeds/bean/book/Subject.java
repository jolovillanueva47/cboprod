package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class Subject {
	private String code;
	private String level;
	private String name;
	
	public Subject(){}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
