package org.cambridge.ebooks.production.metadatafeeds;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.metadatafeeds.bean.BiblioData;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.AltIsbn;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Author;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Book;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Edition;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.MainTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.PublicationDate;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Series;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Subject;
import org.cambridge.ebooks.production.metadatafeeds.util.JdbcUtils;

/**
 * @author kmulingtapang
 */
public class DbWorker {
	
	public static BiblioData getBiblioData(List<String> isbns) throws Exception {
		List<StringBuilder> inIsbns = new ArrayList<StringBuilder>();
		StringBuilder inIsbn = new StringBuilder();
		inIsbn.append("(");
		int ctr = 0;
		int ctr2 = 0;
		for(String isbn : isbns) {
			ctr++;
			ctr2++;
			
			// 900 limit
			if(ctr > 900) {
				ctr = 1;
				inIsbn = new StringBuilder();
				inIsbn.append("(");
			}
			
			inIsbn.append(isbn);
			if(ctr2 < isbns.size() && ctr < 900) {
				inIsbn.append(",");
			}
			
			if(ctr == 900) {
				inIsbn.append(")");
				inIsbns.add(inIsbn);
			}
			
		}	
		if(ctr < 900) {
			inIsbn.append(")");	
			inIsbns.add(inIsbn);
		}
		
		BiblioData biblioData = new BiblioData();
		biblioData.setBookMap(generateBiblioBookMap(inIsbns));
		biblioData.setSubjectMap(generateBiblioSubjectMap(inIsbns));	
		biblioData.setAuthorMap(generateBiblioAuthorMap(inIsbns));
		
		return biblioData;
	}
	
	private static Map<String, Book> generateBiblioBookMap(List<StringBuilder> inIsbns) throws Exception {
		System.out.println("Generate biblio book map...");
		Map<String, Book> bookMap = new HashMap<String, Book>();
		
		for(StringBuilder inIsbn : inIsbns) {		
			Connection conn = JdbcUtils.getConnection();
			Statement st = null;		
			ResultSet rs = null;
			try {			
				st = conn.createStatement();
				
				try {
					StringBuilder sql = new StringBuilder();
					
					sql.append("select eidl.pub_price, eidl.pub_price_subscription, eidl.isbn,")
					.append(" eidl.title, eidl.subtitle, eidl.series_code,")
					.append(" s.series_legend, eidl.series_number, eidl.volume_number,")
					.append(" eidl.volume_title, eidl.part_number, eidl.part_title,")
					.append(" eidl.edition_number, to_char(eidl.pub_date_print, 'YYYY') as print_date, eidl.isbn_print_hb,")
					.append(" eidl.isbn_print_pb, e.book_id, to_char(e.modified_date, 'ddMonYYYY') as online_date,")
					.append(" decode(eidl.cbo_product_group, 'CLC', '1367', decode(p.publisher_id, null, '1168', p.publisher_id)) as publisher_id")
					.append(" from")
					.append(" oraebooks.ebook_isbn_data_load eidl,")
					.append(" oraebooks.ebook e,")
					.append(" oraebooks.ebook_series_load s,")
					.append(" oraebooks.ebook_publisher_id_isbn p")
					.append(" where")
					.append(" eidl.isbn = e.isbn(+)")
					.append(" and eidl.series_code = s.series_code(+)")
					.append(" and eidl.isbn = p.isbn(+)")
					.append(" and eidl.isbn in ")
					.append(inIsbn.toString());
					
					System.out.println("sql statement: " + sql.toString());
					
					rs = st.executeQuery(sql.toString());
					
					try {
						while(rs.next()) {
							Book book = new Book();
							
							String price = rs.getString("pub_price");
							String subscriptionPrice = rs.getString("pub_price_subscription");
							String isbn = rs.getString("isbn");
							String title = rs.getString("title");
							String subtitle = rs.getString("subtitle");
							String seriesCode = rs.getString("series_code");
							String seriesName = rs.getString("series_legend");
							String seriesNumber = rs.getString("series_number");
							String volumeNumber = rs.getString("volume_number");
							String volumeTitle = rs.getString("volume_title");
							String partNumber = rs.getString("part_number");
							String partTitle = rs.getString("part_title");
							String editionNumber = rs.getString("edition_number");
							String printDate = rs.getString("print_date");
							String onlineDate = rs.getString("online_date");
							String hardback = rs.getString("isbn_print_hb");
							String paperback = rs.getString("isbn_print_pb");							
							
							book.setPrice(price);
							book.setTieredPrice(subscriptionPrice);
							book.setIsbn(isbn);
							
							MainTitle _title = new MainTitle();
							_title.setText(title);
							book.setTitle(_title);
							
							book.setSubtitle(subtitle);
							
							if(StringUtils.isEmpty(seriesCode) && Integer.parseInt(seriesNumber) <= 0 && StringUtils.isEmpty(seriesName)) {
								book.setSeries(null);
							} else {
								Series series = new Series();
								series.setCode(seriesCode);
								series.setNumber(seriesNumber);
								series.setTitle(seriesName);
								book.setSeries(series);
							}
							
							book.setVolumeNumber(volumeNumber);
							book.setVolumeTitle(volumeTitle);
							book.setPartNumber(partNumber);
							book.setPartTitle(partTitle);
							
							if(StringUtils.isEmpty(editionNumber) || Integer.parseInt(editionNumber) <= 0) {
								book.setEdition(null);
							} else {
								Edition edition = new Edition();
								edition.setNumber(editionNumber);							
								book.setEdition(edition);
							}
							
							PublicationDate pubDate = new PublicationDate();
							pubDate.setPrint(printDate);
							pubDate.setOnline(onlineDate);							
							book.setPubDate(pubDate);
							
							List<AltIsbn> altIsbns = new ArrayList<AltIsbn>();
							if(StringUtils.isNotEmpty(hardback)) {
								AltIsbn altIsbn = new AltIsbn();
								altIsbn.setType("hardback");
								altIsbn.setValue(hardback);
								
								altIsbns.add(altIsbn);
							}							
							if(StringUtils.isNotEmpty(paperback)) {
								AltIsbn altIsbn = new AltIsbn();
								altIsbn.setType("paperback");
								altIsbn.setValue(paperback);
								
								altIsbns.add(altIsbn);
							}
							book.setAltIsbns(altIsbns);
							
							bookMap.put(isbn, book);
						}
					} finally {
						rs.close();
					}				
					
				} finally {
					st.close();
				}
				
			} finally {
				conn.close();
			}
		}
		System.out.println("Generate biblio book map complete.");
		return bookMap;
	}
	
	
	private static Map<String, List<Subject>> generateBiblioSubjectMap(List<StringBuilder> inIsbns) throws Exception {
		System.out.println("Generate biblio subject map...");
		Map<String, List<Subject>> subjectMap = new HashMap<String, List<Subject>>();
		
		for(StringBuilder inIsbn : inIsbns) {		
			Connection conn = JdbcUtils.getConnection();
			Statement st = null;		
			ResultSet rs = null;
			try {			
				st = conn.createStatement();
				
				try {
					StringBuilder sql = new StringBuilder();
					
					sql.append("select eidl.isbn,")
					.append(" s.subject_id, h.subject_name, h.subject_level")
					.append(" from")
					.append(" oraebooks.ebook_isbn_data_load eidl,")
					.append(" oraebooks.ebook_subject_isbn s,")
					.append(" oraebooks.ebook_subject_hierarchy h")
					.append(" where")
					.append(" s.subject_id = h.subject_id")
					.append(" and s.isbn = eidl.isbn")
					.append(" and eidl.isbn in ")
					.append(inIsbn.toString());
					
					System.out.println("sql statement: " + sql.toString());
					
					rs = st.executeQuery(sql.toString());
					
					try {
						while(rs.next()) {
							Subject subject = new Subject();
							
							String isbn = rs.getString("isbn");
							String code = rs.getString("subject_id");
							String name = rs.getString("subject_name");
							String level = rs.getString("subject_level");
							
							subject.setCode(code);
							subject.setName(name);
							subject.setLevel(level);
							
							if(null != subjectMap.get(isbn)) {
								subjectMap.get(isbn).add(subject);
							} else {
								List<Subject> subjects = new ArrayList<Subject>();
								subjects.add(subject);

								subjectMap.put(isbn, subjects);
							}
								
						}
					} finally {
						rs.close();
					}				
					
				} finally {
					st.close();
				}
				
			} finally {
				conn.close();
			}
		}
		System.out.println("Generate biblio subject map complete.");
		return subjectMap;
	}
	
	private static Map<String, List<Author>> generateBiblioAuthorMap(List<StringBuilder> inIsbns) throws Exception {
		System.out.println("Generate biblio author map...");
		Map<String, List<Author>> authorMap = new HashMap<String, List<Author>>();
		
		for(StringBuilder inIsbn : inIsbns) {		
			Connection conn = JdbcUtils.getConnection();
			Statement st = null;		
			ResultSet rs = null;
			try {			
				st = conn.createStatement();
				
				try {
					StringBuilder sql = new StringBuilder();
					
					sql.append("select a.isbn,")
					.append(" a.sequence_number, a.author_role,")
					.append(" a.forename, a.surname,")
					.append(" a.affiliation")
					.append(" from")
					.append(" oraebooks.ebook_isbn_data_load eidl,")
					.append(" oraebooks.ebook_isbn_author a")
					.append(" where")
					.append(" a.isbn = eidl.isbn")
					.append(" and eidl.isbn in ")
					.append(inIsbn.toString());
					
					System.out.println("sql statement: " + sql.toString());
					
					rs = st.executeQuery(sql.toString());
					
					try {
						while(rs.next()) {
							Author author = new Author();
							
							String isbn = rs.getString("isbn");
							String position = rs.getString("sequence_number");
							String role = rs.getString("author_role");
							String forename = rs.getString("forename");
							String surname = rs.getString("surname");
							String affiliation = rs.getString("affiliation");
							
							author.setPosition(position);
							author.setRole(role);
							author.setForename(forename);
							author.setSurname(surname);
							author.setAffiliation(affiliation);
							
							if(null != authorMap.get(isbn)) {
								authorMap.get(isbn).add(author);
							} else {
								List<Author> authors = new ArrayList<Author>();
								authors.add(author);
								authorMap.put(isbn, authors);								
							}
						}
					} finally {
						rs.close();
					}				
					
				} finally {
					st.close();
				}
				
			} finally {
				conn.close();
			}
		}
		System.out.println("Generate biblio author map complete.");
		return authorMap;
	}
}
