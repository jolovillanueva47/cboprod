package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class Series {
	private String number;
	private String position;
	private String code;
	private String alphasort;
	private String title;
	
	public Series(){}

	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAlphasort() {
		return alphasort;
	}
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	
}
