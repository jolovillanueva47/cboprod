package org.cambridge.ebooks.production.metadatafeeds.bean;

import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.production.metadatafeeds.bean.book.Author;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Book;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Subject;

/**
 * @author kmulingtapang
 */
public class BiblioData {
	
	private Map<String, Book> bookMap;
	private Map<String, List<Subject>> subjectMap;
	private Map<String, List<Author>> authorMap;
	
	public BiblioData(){}

	public Map<String, Book> getBookMap() {
		return bookMap;
	}

	public void setBookMap(Map<String, Book> bookMap) {
		this.bookMap = bookMap;
	}

	public Map<String, List<Subject>> getSubjectMap() {
		return subjectMap;
	}

	public void setSubjectMap(Map<String, List<Subject>> subjectMap) {
		this.subjectMap = subjectMap;
	}

	public Map<String, List<Author>> getAuthorMap() {
		return authorMap;
	}

	public void setAuthorMap(Map<String, List<Author>> authorMap) {
		this.authorMap = authorMap;
	}		
}
