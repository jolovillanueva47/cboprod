package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class Edition {
	private String number;
	private String text;
	
	public Edition(){}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}	
}
