package org.cambridge.ebooks.production.metadatafeeds.bean.book;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class References {

	private String type;
	private MainTitle title;
	
	private List<Citation> citations;
	
	public References(){}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MainTitle getTitle() {
		return title;
	}

	public void setTitle(MainTitle title) {
		this.title = title;
	}

	public List<Citation> getCitations() {
		return citations;
	}

	public void setCitations(List<Citation> citations) {
		this.citations = citations;
	}	
}
