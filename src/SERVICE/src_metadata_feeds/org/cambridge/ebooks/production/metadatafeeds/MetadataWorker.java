package org.cambridge.ebooks.production.metadatafeeds;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.AltIsbn;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Author;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Book;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.CoverImage;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Subject;
import org.cambridge.ebooks.production.metadatafeeds.constant.ApplicationProperties;
import org.cambridge.ebooks.production.metadatafeeds.util.ExceptionPrinter;
import org.cambridge.ebooks.production.metadatafeeds.util.ZipUtils;

import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

/**
 * @author kmulingtapang
 */
public class MetadataWorker {

	public static final String FILE_OPT_PDF = "pdf";
	public static final String FILE_OPT_JPG_COVER = "jpg_cover";
	// jpg 1st page
	public static final String FILE_OPT_JPG_ABSTRACT = "jpg_abstract";
	public static final String FILE_OPT_HEADER_XML = "header_xml";
	public static final String FILE_OPT_CSV = "csv";
	public static final String FILE_OPT_ZIPPED = "zipped";
	
	private static final String FTP_HOST = "ftp.host";
	private static final String FTP_USER = "ftp.user";
	private static final String FTP_PASSWORD = "ftp.password";
	private static final String FTP_FOLDER = "ftp.folder";
	private static final String OPTION = "option";
	private static final String ISBN = "isbn";
	
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("ddMMMyyyy_HHmmss");
	private static Configuration cfg;

	static {
		cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File(
					ApplicationProperties.FREEMARKER_TEMPLATE_DIR));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static void transferByFtp() {	
		System.out.println("Execute transfer...");
		
		String outputDir = ApplicationProperties.OUTPUT_DIR;				
		File inputDir = new File(outputDir);
		
		// user dir
		for(File userDir : inputDir.listFiles()) {		
			System.out.println("[user dir] " + userDir.getName());
			FTPClient ftpClient = null;
			try {
				org.apache.commons.configuration.Configuration dataFile = null;
				
				for(File file : userDir.listFiles()) {				
					// get .data file
					if(file.getName().endsWith(".data")) {
						System.out.println("[data] " + file.getName());
						dataFile = new PropertiesConfiguration(file.getAbsoluteFile());
					}
				}
				
				String ftpHost = dataFile.getString(FTP_HOST);
				String ftpUser = dataFile.getString(FTP_USER);
				String ftpPass = dataFile.getString(FTP_PASSWORD);
				String ftpFolder = dataFile.getString(FTP_FOLDER);
				
				// ftp client
				ftpClient = new FTPClient();
				
				// establish a connection on ftp client
				ftpClient.connect(ftpHost);
				
				// ftp login
				boolean isLoggedin = ftpClient.login(ftpUser, ftpPass);	
				if(!isLoggedin) {
					System.err.println("[Exception] Could not logged-in to ftp host: " + ftpHost);
				} else {
					System.out.println("Connected to " + ftpHost);
				}
				
				System.out.println("Transfering files in " + ftpFolder);				
				for(File isbnDir : userDir.listFiles()) {				
					// get .data file
					if(!isbnDir.getName().endsWith(".data")) {
						System.out.println("[input dir] " + isbnDir.getName());

						// create dir on recipient's server
						String ftpDir = ftpFolder + "/" + isbnDir.getName();						
						if(!ftpClient.makeDirectory(ftpFolder + "/" + isbnDir.getName())) {
							System.err.println(ftpDir + " is not created!");
							continue;
						}
						
						for(File file : isbnDir.listFiles()) {
							FileInputStream fis = null;
							String fileToStore = ftpDir + "/" + file.getName();
							System.out.println("[file] " + fileToStore);
							try {
								// create FileInputStream to transfer on ftp
								fis = new FileInputStream(file);	
								
								// ftp transfer file
								ftpClient.storeFile(fileToStore, fis);							
							} catch (Exception e) {
								System.err.println("[Exception]");
								System.err.println(ExceptionPrinter.getStackTraceAsString(e));
								continue;
							} finally {		
								// close FileInputStream
								fis.close();
							}
						}
					}
				}
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			} finally {
				if(null != ftpClient) {
					// logout on ftp client
					try {
						ftpClient.logout();
					} catch (Exception e) {
						System.err.println("[Exception]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
						continue;
					}
				}
			}
			
		}		
		System.out.println("Transfer complete.");
	}
	
	public static void storeFile() {
		System.out.println("Store files...");
		
		String inputDir = ApplicationProperties.INPUT_DIR;
		String outputDir = ApplicationProperties.OUTPUT_DIR;
		
		// get input files
		File[] files = getInputFiles(inputDir);
		
		// traverse files in the input dir
		for (File file : files) {
			System.out.println("[input file] " + file.getName());			
			String outputUserDir = outputDir + "/" + file.getName().replaceAll(".data", "_" + DATE_FORMAT.format(new Date()));
			System.out.println("[output dir] " + outputUserDir);
			
			// output dir per user
			File userDir = new File(outputUserDir);
			if(!userDir.exists() && !userDir.mkdir()) {
				System.err.println(userDir.getAbsolutePath() + " is not created!");
				continue;
			}
			
			try {
				// initialize prop file
				org.apache.commons.configuration.Configuration  dataFile = new PropertiesConfiguration(file);
				
				String host = dataFile.getString(FTP_HOST);
				String[] isbns = dataFile.getStringArray(ISBN);
				List<String> options = Arrays.asList(dataFile.getStringArray(OPTION));
				System.out.println("[options] " + options.toString());
				
				// generate bookMap
				Map<String, Book> bookMap = null;
				if(options.contains(FILE_OPT_CSV)) {				
					bookMap = JaxbWorker.generateBookMap(Arrays.asList(isbns), false);
				}				
				
				// traverse isbn in a file
				System.out.println("Process isbn...");
				for(String isbn : isbns) {
					System.out.println("[isbn] " + isbn);
					
					try {						
						// isbn dir per user dir
						File outIsbnDir = new File(userDir, isbn);
						if(!outIsbnDir.exists() && !outIsbnDir.mkdir()) {
							System.err.println(outIsbnDir.getAbsolutePath() + " is not created.");
							continue;
						}
						
						// source dir
						String contentDir = ApplicationProperties.CONTENT_DIR;
						File inIsbnDir = new File(contentDir + "/" + isbn);
						String headerFile = inIsbnDir.getAbsolutePath() + "/" + isbn + ".xml";
						
						// === start store files ===
						if(options.contains(FILE_OPT_JPG_COVER)) {							
							// xpath process
							List<String> coverImages = XpathWorker.getCoverImages(headerFile);							
							storeJpgFiles(inIsbnDir, outIsbnDir, coverImages);
						}
						
						if(options.contains(FILE_OPT_JPG_ABSTRACT)) {							
							// xpath process
							List<String> abstractImages = XpathWorker.getAbstractImages(headerFile);							
							storeJpgFiles(inIsbnDir, outIsbnDir, abstractImages);
						}
						
						if(options.contains(FILE_OPT_HEADER_XML)) {							
							storeXmlFile(inIsbnDir, outIsbnDir, isbn + ".xml");
						}						
						
						if(options.contains(FILE_OPT_PDF)) {								
							storePdfFile(inIsbnDir, outIsbnDir, headerFile, host);
						}
						
						if(options.contains(FILE_OPT_ZIPPED)) {							
							// isbn dir to be compress in a zipped dir
							File isbnTempDir = new File(outIsbnDir, "temp");
							if(!isbnTempDir.exists() && !isbnTempDir.mkdir()) {
								System.out.println(outIsbnDir.getAbsolutePath() + " is not created.");
							} else {								
								storePdfFile(inIsbnDir, isbnTempDir, headerFile, host);							
								buildZippedFile(inIsbnDir, outIsbnDir, isbnTempDir, isbn);
							}
						}
						
						if(options.contains(FILE_OPT_CSV)) {							
							buildCsvFile(inIsbnDir, outIsbnDir, bookMap.get(isbn));
						}
						// === end store files ===
					} catch (Exception e) {
						System.err.println("[Exception]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
						continue;
					}
				}
				System.out.println("Process isbn complete...");
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			}
			
			// move .data files to output dir
			File dir = new File(outputUserDir);
			
			boolean isSuccess = file.renameTo(new File(dir, file.getName()));
			if (!isSuccess) {
				System.err.println(file.getName() + " was not moved to " + outputUserDir);
			}
			
		}
		System.out.println("Store files complete.");
	}
	
	public static void buildCsvFile(File inDir, File outDir, Book book) {	
		System.out.println("Build csv file...");
		String fmTemplate = ApplicationProperties.FREEMARKER_CSV_FEED_TEMPLATE;
		
		Map<String, Object> root = new HashMap<String, Object>();
		root.put("book", book);

		File f = new File(outDir + "/" + book.getIsbn() + ".csv");
		
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f)));
			Template xml = cfg.getTemplate(fmTemplate);
			xml.process(root, bw);
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				bw.close();
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}		
		System.out.println("Build csv file complete.");
		
	}
	
	public static void buildZippedFile(File inDir, File outDir, File tempDir, String isbn) {
		System.out.println("Build zip file...");
		try {							
			buildFilesForCompression(inDir, tempDir, isbn);
			
			// compress copied files
			ZipUtils.compressDirectory(tempDir, outDir, isbn + ".zip");
			
			// delete files inside tempDir
			for(File file : tempDir.listFiles()) {
				file.delete();
			}
			
			// delete tempDir			
			tempDir.delete();
			
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} 
		System.out.println("Build zip file complete.");
	}
	
	public static void storeJpgFiles(File inDir, File outDir, List<String> images) {
		System.out.println("Store jpg files...");
		try {	
			// traverse cover images
			for(String image : images) {							
				try {
					// input file
					File coverImageFile = new File(inDir.getAbsolutePath() + "/" + image);
					
					// output file
					File outputFile = new File(outDir.getAbsolutePath() + "/" + coverImageFile.getName());
					
					copyFile(coverImageFile, outputFile);
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				}
			}
			
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}		
		System.out.println("Store jpg files complete.");
	}
	
	public static void storeXmlFile(File inDir, File outDir, String xmlFile) {
		System.out.println("Store xml/header file");
		try {			
			// input file
			File headerFile = new File(inDir.getAbsolutePath() + "/" + xmlFile);			
			
			// output file
			File outputFile = new File(outDir.getAbsolutePath() + "/" + headerFile.getName());
			
			copyFile(headerFile, outputFile);	

		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} 					
		System.out.println("Store xml/header file complete.");
	}
	
	public static void storePdfFile(File inDir, File outDir, String headerFile, String host) {
		System.out.println("Store stamped pdf files...");		
		// xpath
		List<String> contentIds = XpathWorker.getContentIds(headerFile);
		
		for(String contentId : contentIds) {
			String pdfFile = XpathWorker.getPdfFile(headerFile, contentId);
			// no pdffile data on xml
			if(StringUtils.isEmpty(pdfFile)) {
				continue;
			}
			String doi = XpathWorker.getDoi(headerFile, contentId);								
			buildStampedPdf(inDir, outDir, pdfFile, host, doi);
		}
		System.out.println("Store stamped pdf files complete.");
	}
	
	public static File[] getInputFiles(String inputDir) {
		File[] files = null;
		File input = new File(inputDir);		
		if (input.isDirectory()) {
			files = input.listFiles();
		}
		return files;
	}
	
	public static void generateXmlFile(String inputDir, String outputDir, String template) {
		System.out.println("[" + outputDir + "] generating xml files...");

		// traverse input dir
		File outbox = new File(inputDir);
		if (outbox.isDirectory()) {
			for (File file : outbox.listFiles()) {
				System.out.println("input file:" + file.getName());
				
				// traverse input isbn
				List<Book> books = null;
				BufferedReader br = null;
				try {

					// put isbns to ArrayList
					br = new BufferedReader(new FileReader(file));
					String isbn = "";
					List<String> isbns = new ArrayList<String>();
					while ((isbn = br.readLine()) != null) {
						isbns.add(isbn);
					}

					books = JaxbWorker.generateBooks(isbns, true);

				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						System.err.println("[IOException]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					}
				}

				Map<String, Object> root = new HashMap<String, Object>();
				root.put("books", books);

				// === start output ===
				File f = new File(outputDir
						+ "/"
						+ file.getName().replaceAll(".txt", "_")
						+ DATE_FORMAT.format(new Date()) + ".xml");
				
				BufferedWriter bw = null;

				try {
					bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f)));
					Template xml = cfg.getTemplate(template);
					xml.process(root, bw);
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				} finally {
					try {
						bw.close();
					} catch (IOException e) {
						System.err.println("[IOException]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					}

					System.out.println("xml file has been dumped...");
				}
				// === end output ===

				// === start move to processed ===
				File processedDir = new File(ApplicationProperties.PROCESSED_DIR);
				
				boolean isSuccess = file.renameTo(new File(processedDir, file.getName().replaceAll(".txt", "_") 
						+ DATE_FORMAT.format(new Date()) + ".txt"));
				if (!isSuccess) {
					System.err.println(file.getName() + " was not moved to " + ApplicationProperties.PROCESSED_DIR);
				}
				// === end move to processed ===
			}
		}
		System.out.println("End process.");
	}

	public static void generatePdfFile(String inputDir, String outputDir) {	
		System.out.println("[" + outputDir + "] generating pdf file...");
		// traverse input dir
		File outbox = new File(inputDir);
		if (outbox.isDirectory()) {
			for (File file : outbox.listFiles()) {
				System.out.println("input file:" + file.getName());
				
				// traverse input isbn
				List<Book> books = null;
				BufferedReader br = null;
				try {

					// put isbns to ArrayList
					br = new BufferedReader(new FileReader(file));
					String isbn = "";
					List<String> isbns = new ArrayList<String>();
					while ((isbn = br.readLine()) != null) {
						isbns.add(isbn);
					}

					books = JaxbWorker.generateBooks(isbns, false);					

				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						System.err.println("[IOException]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					}
				}
				
				// === start iText ===
				Document document = new Document();
				try {
					PdfWriter.getInstance(document, new FileOutputStream(outputDir + "/" 
							+ file.getName().replaceAll(".txt", "_") 
							+ DATE_FORMAT.format(new Date()) + ".pdf"));
					
					document.open();
					
					buildPdfFile(document, books);
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				} finally {
					document.close();
				}
				// === end iText ===
				
				// === start move to processed ===
//				File processedDir = new File(ApplicationProperties.PROCESSED_DIR);
//				
//				boolean isSuccess = file.renameTo(new File(processedDir, file.getName().replaceAll(".txt", "_") 
//						+ DATE_FORMAT.format(new Date()) + ".txt"));
//				if (!isSuccess) {
//					System.err.println(file.getName() + " was not moved to " + ApplicationProperties.PROCESSED_DIR);
//				}
				// === end move to processed ===
			}
		}
		System.out.println("End process.");
	}
	
	private static void buildFilesForCompression(File inDir, File outDir, String isbn) throws Exception {
		// traverse source dir
		// copy all jpg, pdf, header file and manifest file
		if(inDir.isDirectory()) {
			for(File file : inDir.listFiles()) {
				File outputFile = null;
				
				if(file.getName().endsWith(".jpg")) {
					// output file
					outputFile = new File(outDir.getAbsolutePath() + "/" + file.getName());						
					copyFile(file, outputFile);							
				}
				// header file
				else if(file.getName().equals(isbn + ".xml")) {
					// output file
					outputFile = new File(outDir.getAbsolutePath() + "/" + file.getName());						
					copyFile(file, outputFile);	
				} else if(file.getName().equals("manifest.xml")) {
					// output file
					outputFile = new File(outDir.getAbsolutePath() + "/" + file.getName());						
					copyFile(file, outputFile);	
				}
			}
		}
	}
	
	private static void buildStampedPdf(File inDir, File outDir, String pdfFile, String host, String doi) {
		try {			
			String sourcePdfFile = inDir.getAbsolutePath() + "/" + pdfFile;
			String outputPdfFile = outDir.getAbsolutePath() + "/" + pdfFile;				
			
			PdfWorker.stampPdf(sourcePdfFile, outputPdfFile, host, doi);
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} 					
	}
	
//	private static void traverseDir(File dir, String dirName) {
//		String parentDir = "";
//		String userName = "";
//		String ftpHost = "";
//		String ftpUser = "";
//		String ftpPass = "";
//		String ftpFolder = "";
//		org.apache.commons.configuration.Configuration propFile = null;
//		FTPClient ftpClient = null;
//		
//		int ctr = 0;
//		try {
//			// traverse files in the directory
//			for (File file : dir.listFiles()) {
//				if(file.isDirectory()) {
//					traverseDir(file, file.getName());
//				} else {
//					ctr++;
//					
//					// initialize
//					if(ctr == 1) {
//						parentDir = file.getParent();
//						userName = dirName.substring(0, dirName.indexOf("_"));
//						
//						// get property file
//						propFile = new PropertiesConfiguration(parentDir + "/" + userName + ".properties");
//						
//						ftpHost = propFile.getString(FTP_HOST);
//						ftpUser = propFile.getString(FTP_USER);
//						ftpPass = propFile.getString(FTP_PASSWORD);
//						ftpFolder = propFile.getString(FTP_FOLDER);
//						
//						if(!ftpFolder.endsWith("/")) {
//							ftpFolder = ftpFolder + "/";
//						}
//						
//						// ftp client
//						ftpClient = new FTPClient();
//						
//						// establish a connection on ftp client
//						ftpClient.connect(ftpHost);
//						
//						// ftp login
//						boolean isLoggedin = ftpClient.login(ftpUser, ftpPass);	
//						if(!isLoggedin) {
//							System.err.println("[Exception] Could not logged-in to ftp host: " + ftpHost);
//							return;
//						} else {
//							System.out.println("Connected to " + ftpHost);
//						}
//						
//						System.out.println("[" + ftpFolder + "] transfering files...");
//					}
//					
//					if(!file.getName().endsWith(".properties")) {						
//						System.out.println("input file:" + file.getName());
//						FileInputStream fis = null;
//						try {
//							// create FileInputStream to transfer on ftp
//							fis = new FileInputStream(file);					
//							
//							// ftp transfer file
//							ftpClient.storeFile(ftpFolder + file.getName(), fis);							
//						} catch (Exception e) {
//							System.err.println("[Exception]");
//							System.err.println(ExceptionPrinter.getStackTraceAsString(e));
//						} finally {		
//							// close FileInputStream
//							fis.close();
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//			System.err.println("[Exception]");
//			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
//		} finally {
//			try {
//				if(null != ftpClient) {
//					// logout on ftp client
//					ftpClient.logout();
//				}
//			} catch (Exception e) {
//				System.err.println("[Exception]");
//				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
//			}
//		}
//	}
	
	private static void copyFile(File in, File out) throws IOException {
	    FileChannel inChannel = new FileInputStream(in).getChannel();
	    FileChannel outChannel = new FileOutputStream(out).getChannel();
	    try {
	        inChannel.transferTo(0, inChannel.size(), outChannel);
	    } catch (IOException e) {
	        throw e;
	    } finally {
	        if (inChannel != null) {
	        	inChannel.close();
	        }
	        if (outChannel != null){
	        	outChannel.close();
	        }
	    }
	}

	
	private static void buildPdfFile(Document document, List<Book> books) throws Exception {
		String indentation = "\t\t";
		String newLine = "\n";
		String doubleNewLine = "\n\n";
		String seperator = "================================================================";
		
		for(Book book : books) {
			Paragraph p = new Paragraph();	
			p.add("Book");
			p.add(newLine);
			p.add(seperator);
			p.add(newLine);
			p.add("ID: " + book.getId());
			p.add(doubleNewLine);
			p.add("Title: " + StringEscapeUtils.unescapeXml(book.getTitle().getText()) + " (" + book.getTitle().getAlphasort() + ")");			
			if(StringUtils.isNotEmpty(book.getSubtitle())) {
				p.add(doubleNewLine);
				p.add("Subtitle: " + StringEscapeUtils.unescapeXml(book.getSubtitle()));
			}
			p.add(doubleNewLine);
			p.add("Group: " + book.getGroup());
			if(StringUtils.isNotEmpty(book.getPrice())) {
				p.add(doubleNewLine);
				p.add("Price: " + book.getPrice() + " USD");
			}
			if(StringUtils.isNotEmpty(book.getTieredPrice())) {
				p.add(doubleNewLine);
				p.add("Tiered price: " + book.getTieredPrice() + " USD");
			}
			p.add(doubleNewLine);
			p.add("Authors:");			
			for(Author author : book.getAuthors()){
				p.add(newLine);
				p.add(StringEscapeUtils.unescapeXml(author.getName().replaceAll("<forenames>", "")
						.replaceAll("</forenames>", ", ")
						.replaceAll("<surname>", "")
						.replaceAll("</surname>", "")));
				p.add(" (" + author.getAlphasort() + ")");
				if(StringUtils.isNotEmpty(author.getAffiliation())) {
					p.add(", " + StringEscapeUtils.unescapeXml(author.getAffiliation()));
				}
			}
			if(StringUtils.isNotEmpty(book.getDoi())) {
				p.add(doubleNewLine);
				p.add("DOI: " + book.getDoi());
			}
			p.add(doubleNewLine);
			p.add("E-ISBN: " + book.getIsbn());
			for(AltIsbn altIsbn : book.getAltIsbns()) {
				if(altIsbn.getType().equals("paperback")) {
					p.add(newLine);
					p.add("Paperback: " + altIsbn.getValue());
				} else if(altIsbn.getType().equals("hardback")) {
					p.add(newLine);
					p.add("Hardback: " + altIsbn.getValue());
				}				
			}
			if(null != book.getSeries()) {	
				p.add(doubleNewLine);
				p.add("Series Code, Series:");				
				p.add(newLine);
				if(StringUtils.isNotEmpty(book.getSeries().getCode())) {						
					p.add(book.getSeries().getCode() + ", ");
				}
				p.add(StringEscapeUtils.unescapeXml(book.getSeries().getTitle()) 
						+ " (" + book.getSeries().getAlphasort() + ")");
				if(StringUtils.isNotEmpty(book.getSeries().getNumber())) {					
					p.add(newLine);
					p.add("Series Number: " + book.getSeries().getNumber());
				}				
			}
			p.add(doubleNewLine);
			p.add("Print Date: " + book.getPubDate().getPrint());
			p.add(newLine);
			p.add("Online Date: " + book.getPubDate().getOnline());
			
			if(null != book.getEdition()) {
				p.add(doubleNewLine);			
				p.add("Edition: " + book.getEdition().getNumber() + " - " + book.getEdition().getText());
			}
			
			if(StringUtils.isNotEmpty(book.getVolumeNumber()) || StringUtils.isNotEmpty(book.getVolumeTitle())) {
				p.add(doubleNewLine);
				p.add("Volume Number, Volume:");
				p.add(newLine);
				p.add(book.getVolumeNumber() + ", " + book.getVolumeTitle());
			}			
			
			if(StringUtils.isNotEmpty(book.getPartNumber()) || StringUtils.isNotEmpty(book.getPartTitle())) {
				p.add(doubleNewLine);
				p.add("Part Number, Part:");
				p.add(newLine);
				p.add(book.getPartNumber() + ", " + book.getPartTitle());
			}	
			
			if(null != book.getSubjects() && book.getSubjects().size() > 0) {
				p.add(doubleNewLine);
				p.add("Subject(s):");				
				for(Subject subject : book.getSubjects()) {
					p.add(newLine);
					p.add(subject.getCode() + ", " + StringEscapeUtils.unescapeXml(subject.getName()));	
				}
			}
			
			p.add(doubleNewLine);
			p.add("Book Blurb:");
			p.add(newLine);
			p.add(StringEscapeUtils.unescapeXml(book.getBlurb()));
			
			if(null != book.getCoverImages() && book.getCoverImages().size() > 0) {
				p.add(doubleNewLine);
				p.add("Cover Image:");
				for(CoverImage coverImage : book.getCoverImages()) {
					p.add(newLine);
					if(coverImage.getType().equals("standard")) {
						p.add("Standard: " + coverImage.getFilename());
					} else if(coverImage.getType().equals("thumb")) {
						p.add("Thumb: " + coverImage.getFilename());
					}
				}
			}

			p.add(newLine);
			p.add(seperator);
			p.add(doubleNewLine);
			
			document.add(p);
		}
	}
}
