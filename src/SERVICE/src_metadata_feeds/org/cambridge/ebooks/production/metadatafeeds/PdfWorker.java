package org.cambridge.ebooks.production.metadatafeeds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;
import org.cambridge.ebooks.production.metadatafeeds.constant.ApplicationProperties;
import org.cambridge.ebooks.production.metadatafeeds.util.ExceptionPrinter;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

/**
 * @author kmulingtapang
 */
public class PdfWorker {
	
	private static class CboPdfTextStripper extends PDFTextStripper {
		
		private float lastTextYPos;
		
		public CboPdfTextStripper() throws IOException {
			super();
		}

		public CboPdfTextStripper(Properties props) throws IOException {
			super(props);
		}

		public CboPdfTextStripper(String encoding) throws IOException {
			super(encoding);
		}

		protected void processTextPosition(TextPosition tp) {
			super.processTextPosition(tp);
			this.lastTextYPos = tp.getY();
		}

		/**
		 * @return the lastTextYPos
		 */
		public float getLastTextYPos() {
			return lastTextYPos;
		}	
	}
	
	public static void stampPdf(String inputFile, String outputFile, String host, String doi) throws Exception {
		File pdfFile = new File(inputFile);

		// check for file existence
		if(null == pdfFile || !pdfFile.exists()) {
			throw new Exception("File not found: " + pdfFile.getName());
		}
		
		FileInputStream fis = new FileInputStream(pdfFile);		

		// ==== start pdf box ====
		// textStripper is needed to check for an existence of a copyright statement 
		PDFParser pdfParser = new PDFParser(fis);
        pdfParser.parse();
        CboPdfTextStripper textStripper = new CboPdfTextStripper();
		
		COSDocument cosDoc = null;
		try {
			cosDoc = pdfParser.getDocument();
		} catch (Exception e) {
			throw e;
		}
		 
		PDDocument pdDoc = new PDDocument(cosDoc);
        // ==== end pdf box ====
		
		// white image to cover current copyright statement
		Image img = Image.getInstance(ApplicationProperties.STAMP_DRM_IMAGE);
		
		// font to be used in writing the stamp
		BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		
		// read pdf from source
		PdfReader reader = new PdfReader(pdfFile.getAbsolutePath());		
		
		// output of the stamped pdf		
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(outputFile));	
		
		PdfContentByte overBase = null;
		int i = 0;
        int p = 0;  
        try {
	        while (i < reader.getNumberOfPages()) {
	            i++;
	        	p++;
	        		        	
	        	// ==== start pdf box ====
	        	boolean hasCopyright = false;
	        	if(i == 1) {
		        	textStripper.setStartPage(i);
		            textStripper.setEndPage(i);
					String pdDocStr = "";
					try {
						pdDocStr = textStripper.getText(pdDoc);
					} catch (Exception e) {
						System.err.println("[Exception]");
						System.err.println(ExceptionPrinter.getStackTraceAsString(e));
						continue;
					} 
					
					hasCopyright = pdDocStr.contains("Cambridge Books Online � Cambridge University Press");
	        	}
				// ==== end pdf box ====
				
				try {
					int pageRotation = reader.getPageRotation(i);
		
		    		float width = reader.getPageSizeWithRotation(i).width();
		    		float cropWidth = reader.getCropBox(i).width();
		    		float cropLeft = reader.getCropBox(i).left();	    		
		    		float cropBottom = reader.getCropBox(i).bottom();
		    		
		    		if(pageRotation == 90 || pageRotation == 270) {		    			
		    			cropWidth = reader.getCropBox(i).rotate().width();
			    		cropLeft = reader.getCropBox(i).rotate().left();	    		
			    		cropBottom = reader.getCropBox(i).rotate().bottom();
		    		}	    		
		    		
		    		float widthPos = ((cropWidth / 2) + cropLeft);	 
		    		float widthPosImg = ((cropWidth / 2) + cropLeft) - (img.width() / 2);
		    		if(width < cropWidth) {
		    			widthPos = ((width / 2) + cropLeft);	    		
			    		widthPosImg = ((width / 2) + cropLeft) - (img.width() / 2);
		    		}
		    		
		    		float heightPos = cropBottom + 3;
		    		float heightPosImg = ((reader.getPageSizeWithRotation(i).height() - textStripper.getLastTextYPos()) 
		    				- (img.height() / 2));
					
		    		if(pageRotation == 90 || pageRotation == 270) {
		    			heightPos += 10;
		    		}
		    				    		
		    		if(hasCopyright) {
		    			img.setAbsolutePosition(widthPosImg, heightPosImg);
		    		} else {
		    			img.setAbsolutePosition(widthPosImg, heightPos);
		    		}
					
		    		overBase = stamper.getOverContent(i);
		    		overBase.addImage(img);
		    		overBase.beginText(); 
		    		
		    		writeStampText(bf, overBase, host, doi, widthPos, heightPos);
		            
		            overBase.endText();
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				}     		
	        }
        } catch (Exception e) {
        	System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
        } finally {            
            if(stamper != null) {
    			stamper.close();
    		}
    		if(pdDoc != null) {
    			pdDoc.close();
    		}
    		if(cosDoc != null) {
    			cosDoc.close();	
    		}			
        }
	}
	
	private static void writeStampText(BaseFont bf, PdfContentByte cb, String host, String doi, float widthPos, float heightPos) throws Exception {		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		String copyright = "Cambridge Books Online \u00A9 Cambridge University Press, " + year;
		
		cb.setFontAndSize(bf, 4.75f);
		
		StringBuffer drm = new StringBuffer();
		drm = new StringBuffer("Downloaded from Cambridge Books Online by IP ");
		drm.append(host);
		drm.append(" on ");
		drm.append(new Date());
		drm.append(".");		
		
		if (StringUtils.isNotEmpty(doi)) {			
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, 
					drm.toString(), 
	        		widthPos, 
	        		heightPos + 10, 
	        		0);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, 
					"http://dx.doi.org/" + doi, 
	        		widthPos, 
	        		heightPos + 5, 
	        		0);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, 
					copyright, 
	        		widthPos, 
	        		heightPos, 
	        		0);	
		} else {
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, 
					drm.toString(), 
	        		widthPos, 
	        		heightPos + 5, 
	        		0);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, 
					copyright, 
	        		widthPos, 
	        		heightPos, 
	        		0);	
		}
	}	
}
