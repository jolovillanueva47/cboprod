package org.cambridge.ebooks.production.metadatafeeds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.cambridge.ebooks.production.metadatafeeds.util.ExceptionPrinter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author kmulingtapang
 */
public class XpathWorker {
	
	private static final String XPATH_COVER_IMAGE = "book/metadata/cover-image/attribute::filename";
	private static final String XPATH_ABSTRACT_IMAGE = "book/content-items/descendant::abstract/attribute::alt-filename";
	private static final String XPATH_PDF_FILE = "book/content-items/descendant::pdf/attribute::filename";
	private static final String XPATH_CONTENT_ID = "book/content-items/descendant::content-item/attribute::id";
	
	private static DocumentBuilderFactory factory = null;
	
	static {
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();
			}
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static List<String> getCoverImages(String inputFile) {
		return getValues(inputFile, XPATH_COVER_IMAGE);
	}
	
	public static List<String> getAbstractImages(String inputFile) {
		return getValues(inputFile, XPATH_ABSTRACT_IMAGE);
	}
	
	public static List<String> getPdfFiles(String inputFile) {
		return getValues(inputFile, XPATH_PDF_FILE);
	}
	
	public static List<String> getContentIds(String inputFile) {
		return getValues(inputFile, XPATH_CONTENT_ID);
	}
	
	public static String getPdfFile(String inputFile, String contentId) {
		String expression = "book/content-items/descendant::content-item[attribute::id=\"" + contentId + "\"]/pdf/attribute::filename";
		
		return getValue(inputFile, expression);
	}
	
	public static String getDoi(String inputFile, String contentId) {
		String expression = "book/content-items/descendant::content-item[attribute::id=\"" + contentId + "\"]/doi/text()";
		
		return getValue(inputFile, expression);
	}
	
	public static List<String> getValues(String inputFile, String expression) {
		List<String> values = new ArrayList<String>();
		
		Document dom = getXmlDom(inputFile);
		
		NodeList nodeList = searchNodes(dom, expression);
		
		if (nodeList != null && nodeList.getLength() > 0) {
			for(int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				values.add(node.getNodeValue());
			}
		}
		
		return values;
	}
	
	public static String getValue(String inputFile, String expression) {		
		String value = "";
		Document dom = getXmlDom(inputFile);
		
		Node node = searchNode(dom, expression);
		
		if(null != node) {
			value = node.getNodeValue();
		}
		
		return value;
	}
	
	private static NodeList searchNodes(Node xmlDom, String xpathExpression) { 
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodes = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODESET);			
			nodes = (NodeList) result;			
		} catch (Exception e) { 			
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return nodes;
	}
	
	private static Node searchNode(Node xmlDom, String xpathExpression) {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		Node node = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODE);	
			node = (Node) result;			
		} catch (Exception e) { 			
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return node;
	}
	
	private static Document getXmlDom(String filename) { 
		Document doc = null;
		File file = new File(filename);
		try { 
			doc = factory.newDocumentBuilder().parse(file);			
		} catch (Exception e) { 
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return doc;
	}
}
