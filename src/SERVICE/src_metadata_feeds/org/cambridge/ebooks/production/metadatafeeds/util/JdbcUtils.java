package org.cambridge.ebooks.production.metadatafeeds.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.metadatafeeds.constant.ApplicationProperties;

/**
 * @author kmulingtapang
 */
public class JdbcUtils {

	public static Connection getConnection() throws Exception {
		System.out.println("[getConnection] start ===");
		Connection connection = null;
		try {
			// Load the JDBC driver

			// oracle.jdbc.driver.OracleDriver
			String driverName = ApplicationProperties.JDBC_DRIVER;
			System.out.println("driverName: " + driverName);
			Class.forName(driverName);

			// Create a connection to the database
			String url = ApplicationProperties.JDBC_URL;
			// username
			String username = ApplicationProperties.JDBC_USER;
			// password
			String password = ApplicationProperties.JDBC_PASS;

			if (StringUtils.isEmpty(url)) {
				throw new Exception("connection url is emtpy");
			}

			System.out.println("conntecting to url: " + url);
			connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			System.err.println("[ClassNotFoundException]getConnection");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} catch (SQLException e) {
			// Could not connect to the database
			System.err.println("[SQLException]getConnection");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		System.out.println("[getConnection] end ===");
		return connection;
	}

	public static void closeConnection(Connection con) {
		System.out.println("[closeConnection] start...");
		try {
			con.close();
		} catch (Exception e) {
			System.err.println("[Exception] closeConnection");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		System.out.println("[closeConnection] end.\n");
	}
}
