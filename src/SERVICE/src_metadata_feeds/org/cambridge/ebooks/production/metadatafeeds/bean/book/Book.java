package org.cambridge.ebooks.production.metadatafeeds.bean.book;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class Book {
	private String id;
	private String group;
	private String price;
	private String tieredPrice;
	private String subtitle;
	private String doi;
	private String isbn;
	private String volumeNumber;
	private String volumeTitle;
	private String partNumber;
	private String partTitle;
	private String blurb;
	private String publisherName;
	private String publisherLoc;
	private String contentItemsId;

	private MainTitle title;
	private Series series;
	private PublicationDate pubDate;
	private Edition edition;
	
	private List<Author> authors;
	private List<AltIsbn> altIsbns;	
	private List<Subject> subjects;
	private List<CoverImage> coverImages;	
	
	private List<ContentItem> contentItems;
	
	public Book(){}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getTieredPrice() {
		return tieredPrice;
	}
	public void setTieredPrice(String tieredPrice) {
		this.tieredPrice = tieredPrice;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getVolumeNumber() {
		return volumeNumber;
	}
	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}
	public String getVolumeTitle() {
		return volumeTitle;
	}
	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartTitle() {
		return partTitle;
	}
	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}
	public String getBlurb() {
		return blurb;
	}
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
	public MainTitle getTitle() {
		return title;
	}
	public void setTitle(MainTitle title) {
		this.title = title;
	}
	public Series getSeries() {
		return series;
	}
	public void setSeries(Series series) {
		this.series = series;
	}
	public PublicationDate getPubDate() {
		return pubDate;
	}
	public void setPubDate(PublicationDate pubDate) {
		this.pubDate = pubDate;
	}
	public Edition getEdition() {
		return edition;
	}
	public void setEdition(Edition edition) {
		this.edition = edition;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	public List<AltIsbn> getAltIsbns() {
		return altIsbns;
	}
	public void setAltIsbns(List<AltIsbn> altIsbns) {
		this.altIsbns = altIsbns;
	}
	public List<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
	public List<CoverImage> getCoverImages() {
		return coverImages;
	}
	public void setCoverImages(List<CoverImage> coverImages) {
		this.coverImages = coverImages;
	}
	public List<ContentItem> getContentItems() {
		return contentItems;
	}
	public void setContentItems(List<ContentItem> contentItems) {
		this.contentItems = contentItems;
	}
	public String getContentItemsId() {
		return contentItemsId;
	}
	public void setContentItemsId(String contentItemsId) {
		this.contentItemsId = contentItemsId;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	public String getPublisherLoc() {
		return publisherLoc;
	}
	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}		
}
