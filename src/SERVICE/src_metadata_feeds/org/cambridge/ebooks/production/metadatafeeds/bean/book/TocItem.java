package org.cambridge.ebooks.production.metadatafeeds.bean.book;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class TocItem {
	private String parentId;
	private String id;
	private String startPage;
	private String text;
	
	private List<Author> authors;

	public TocItem(){}
	
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStartPage() {
		return startPage;
	}
	public void setStartPage(String startPage) {
		this.startPage = startPage;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}	
}
