package org.cambridge.ebooks.production.metadatafeeds.bean.book;


/**
 * @author kmulingtapang
 */
public class Heading {
	private String label;
	private MainTitle title;
	private String subtitle;
	
	public Heading(){}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}		
	public MainTitle getTitle() {
		return title;
	}
	public void setTitle(MainTitle title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
}
