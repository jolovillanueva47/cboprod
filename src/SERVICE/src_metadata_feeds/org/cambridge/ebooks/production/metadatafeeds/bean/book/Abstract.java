package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class Abstract {
	private String filename;
	private String problem;
	private String text;
	
	public Abstract(){}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
