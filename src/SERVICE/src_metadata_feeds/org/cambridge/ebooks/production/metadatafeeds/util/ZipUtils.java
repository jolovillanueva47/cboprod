package org.cambridge.ebooks.production.metadatafeeds.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

/**
 * @author kmulingtapang
 */
public class ZipUtils {

	public static void compressDirectory(String inDir, String outDir, String zipFilename) {
		// create File instance of inDir string
    	File _inDir = new File(inDir);    	
    	
    	// create File instance of outDir string
    	File _outDir = new File(outDir); 
    	
		compressDirectory(_inDir, _outDir, zipFilename);
	}
	
	public static void compressDirectory(File inDir, File outDir, String zipFilename) {
		
		// create a buffer for reading the files
	    byte[] buf = new byte[2048];	    
		
	    ZipOutputStream zos = null;
	    try {
	    	
	    	if(!outDir.isDirectory()) {
	    		throw new Exception(outDir.getName() + " is not a directory");
	    	}
	    	
			// create the ZIP file
			String outZip = outDir.getAbsolutePath() + "/" + zipFilename;
			zos = new ZipOutputStream(new FileOutputStream(outZip));
			
	    	// check if dir is a directory
	    	if(!inDir.isDirectory()) {
	    		throw new Exception(inDir.getName() + " is not a directory");
	    	}
	    	
			// compress the files
			for(File file : inDir.listFiles()) {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					
					// add ZIP entry to output stream.
					zos.putNextEntry(new ZipEntry(inDir.getName() + "/" + file.getName()));
					
					// transfer bytes from the file to the ZIP file
//					int len;
//					while ((len = fis.read(buf)) > 0) {
//					    out.write(buf, 0, len);
//					}			
					
					IOUtils.copy(fis, zos);
					
					// complete the entry
//		            zos.closeEntry();
					
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				} finally {
					if(null != fis) {
						// close file stream
						fis.close();
					}
					
					if(null != zos) {
						// complete the entry
			            zos.closeEntry();
					}
				}
			}
			
	    } catch (Exception e) {
	    	System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
	    } finally {
	    	// complete zip file
	    	if(null != zos) {
	    		try {
	    			zos.finish();
	    			zos.close();
				} catch (Exception e) {
			    	System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				}
	    	}
	    }
		
	}
}
