package org.cambridge.ebooks.production.metadatafeeds;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.production.metadatafeeds.bean.BiblioData;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Abstract;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.AltIsbn;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Author;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Book;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Citation;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.ContentItem;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.CoverImage;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Edition;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Heading;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.KeywordGroup;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.MainTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.PublicationDate;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.References;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Series;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Subject;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.TocItem;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.ArticleTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Block;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Bold;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.BookTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.ChapterTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Collab;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Contributor;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.CopyrightHolder;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.CopyrightStatement;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Forenames;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Italic;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.JournalTitle;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Keyword;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.ListItem;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.OnlineDate;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.P;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.SmallCaps;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Source;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Sub;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Sup;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Surname;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Underline;
import org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Uri;
import org.cambridge.ebooks.production.metadatafeeds.constant.ApplicationProperties;
import org.cambridge.ebooks.production.metadatafeeds.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class JaxbWorker {

	private static final String ONLINE_DATE_FORMAT = "MMMMM yyyy";
	
	private static JAXBContext jaxbContext;
	private static Unmarshaller unmarshaller;
	
	private static boolean isEscapeSpecialChar;

	static {
//		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "net.sf.saxon.dom.DocumentBuilderFactoryImpl");
//		System.setProperty("javax.xml.xpath.XPathFactory", "net.sf.saxon.xpath.XPathFactoryImpl");
//		System.setProperty("javax.xml.parsers.SAXParserFactory", "net.sf.saxon.aelfred.SAXParserFactoryImpl");

		try {
			jaxbContext = JAXBContext.newInstance("org.cambridge.ebooks.production.metadatafeeds.bean.jaxb");
			unmarshaller = jaxbContext.createUnmarshaller();
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}

	public static List<Book> generateBooks(List<String> isbns, boolean escapeSpecialChar) throws Exception {
		isEscapeSpecialChar = escapeSpecialChar;
		
		List<Book> list = new ArrayList<Book>();

		System.out.println("=== [generateBooks] start ===");		
		BiblioData biblioData = DbWorker.getBiblioData(isbns);		
		for (String isbn : isbns) {
			System.out.println("isbn: " + isbn);
			try {
				list.add(generateBook(isbn, biblioData));
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			}
		}
		System.out.println("=== [generateBooks] end ===\n");		

		return list;
	}
	
	public static Map<String, Book> generateBookMap(List<String> isbns, boolean escapeSpecialChar) throws Exception {
		isEscapeSpecialChar = escapeSpecialChar;
		
		Map<String, Book> map = new HashMap<String, Book>();

		System.out.println("Generate book map...");		
		BiblioData biblioData = DbWorker.getBiblioData(isbns);		
		for (String isbn : isbns) {
			System.out.println("isbn: " + isbn);
			try {
				map.put(isbn, generateBook(isbn, biblioData));
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			}
		}
		System.out.println("Generate book map complete.");		
		
		return map;
	}

	private static Book generateBook(String isbn, BiblioData biblioData) throws Exception {
		Book book = new Book();

		org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Book jaxbBook = generateJaxbBook(isbn);

		// id
		book.setId(jaxbBook.getId());

		// group
		book.setGroup(jaxbBook.getGroup());

		// title
		MainTitle title = new MainTitle();
		title.setText(getContent(jaxbBook.getMetadata().getMainTitle().getContent()));
		title.setAlphasort(jaxbBook.getMetadata().getMainTitle().getAlphasort());
		book.setTitle(title);

		// subtitle
		if (null != jaxbBook.getMetadata().getSubtitle()) {
			book.setSubtitle(getContent(jaxbBook.getMetadata().getSubtitle().getContent()));
		}

		// authors
		List<Author> authors = new ArrayList<Author>();
		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Author jaxbAuthor : jaxbBook.getMetadata().getAuthor()) {
			Author author = new Author();
			author.setId(jaxbAuthor.getId());
			author.setPosition(jaxbAuthor.getPosition());
			author.setRole(jaxbAuthor.getRole());
			author.setAlphasort(jaxbAuthor.getAlphasort());
			if(null != jaxbAuthor.getAffiliation()) {
				author.setAffiliation(getContent(jaxbAuthor.getAffiliation().getContent()));
			}
			author.setName(getContent(jaxbAuthor.getName().getContent()));

			authors.add(author);
		}
		book.setAuthors(authors);

		// doi
		book.setDoi(jaxbBook.getMetadata().getDoi());

		// isbn
		book.setIsbn(jaxbBook.getMetadata().getIsbn());

		// alt-isbn
		List<AltIsbn> altIsbns = new ArrayList<AltIsbn>();
		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.AltIsbn jaxbAltIsbn : jaxbBook
				.getMetadata().getAltIsbn()) {
			AltIsbn altIsbn = new AltIsbn();
			altIsbn.setType(jaxbAltIsbn.getType());
			altIsbn.setValue(jaxbAltIsbn.getContent());

			altIsbns.add(altIsbn);
		}
		book.setAltIsbns(altIsbns);

		// series
		if (null != jaxbBook.getMetadata().getSeries()) {
			Series series = new Series();
			series.setPosition(jaxbBook.getMetadata().getSeries().getPosition());
			series.setNumber(jaxbBook.getMetadata().getSeries().getNumber());
			series.setCode(jaxbBook.getMetadata().getSeries().getCode());
			series.setAlphasort(jaxbBook.getMetadata().getSeries().getAlphasort());
			series.setTitle(getContent(jaxbBook.getMetadata().getSeries().getContent()));
			book.setSeries(series);
		}
		
		// publisher name
		
		// publisher loc

		// pub-dates
		PublicationDate pubDate = new PublicationDate();
		pubDate.setPrint(getPrintDate(jaxbBook.getMetadata().getPubDates().getPrintDate()));
		pubDate.setOnline(getOnlineDate(jaxbBook.getMetadata().getPubDates().getOnlineDate()));
		book.setPubDate(pubDate);

		// subject
		List<Subject> subjects = new ArrayList<Subject>();
		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Subject jaxbSubject : jaxbBook.getMetadata().getSubjectGroup().getSubject()) {
			Subject subject = new Subject();
			subject.setCode(jaxbSubject.getCode());
			subject.setLevel(jaxbSubject.getLevel());
			subject.setName(jaxbSubject.getContent());

			subjects.add(subject);
		}
		book.setSubjects(subjects);

		// edition
		if (null != jaxbBook.getMetadata().getEdition()) {
			Edition edition = new Edition();
			edition.setNumber(jaxbBook.getMetadata().getEdition().getNumber());
			edition.setText(formatEdition(edition.getNumber()));
			book.setEdition(edition);
		}

		// volume-number
		book.setVolumeNumber(jaxbBook.getMetadata().getVolumeNumber());
		// volume-title
		book.setVolumeTitle(jaxbBook.getMetadata().getVolumeTitle());

		// part-number
		book.setPartNumber(jaxbBook.getMetadata().getPartNumber());
		// part-title
		book.setPartTitle(jaxbBook.getMetadata().getPartTitle());

		// blurb
		if (null != jaxbBook.getMetadata().getBlurb()) {
			StringBuilder blurb = new StringBuilder();
			for (P p : jaxbBook.getMetadata().getBlurb().getP()) {
				blurb.append(getContent(p.getContent()));
			}
			book.setBlurb(blurb.toString());
		}

		// cover-image
		List<CoverImage> coverImages = new ArrayList<CoverImage>();
		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.CoverImage jaxbCoverImage : jaxbBook.getMetadata().getCoverImage()) {
			CoverImage coverImage = new CoverImage();
			coverImage.setFilename(jaxbCoverImage.getFilename());
			coverImage.setType(jaxbCoverImage.getType());

			coverImages.add(coverImage);
		}
		book.setCoverImages(coverImages);

		// content-items
		book.setContentItemsId(jaxbBook.getContentItems().getId());
		book.setContentItems(generateContentItems(jaxbBook.getContentItems().getContentItem(), jaxbBook.getContentItems().getId()));
		
		// === Start Biblio Data ===
		Book bibBook = biblioData.getBookMap().get(isbn);
		
		// price
		book.setPrice(bibBook.getPrice());
		
		// tiered price
		book.setTieredPrice(bibBook.getTieredPrice());
		
		// title
		if(StringUtils.isNotEmpty(bibBook.getTitle().getText())) {
			book.getTitle().setText(bibBook.getTitle().getText());
		}
		
		// subtitle
		if(StringUtils.isNotEmpty(bibBook.getSubtitle())) {
			book.setSubtitle(bibBook.getSubtitle());
		}
		
		if(null != bibBook.getSeries()) {
			if(null == book.getSeries()) {
				book.setSeries(new Series());
			}
			
			// series code
			if(StringUtils.isNotEmpty(bibBook.getSeries().getCode())) {
				book.getSeries().setCode(bibBook.getSeries().getCode());
			}
			
			// series number
			if(StringUtils.isNotEmpty(bibBook.getSeries().getNumber())) {
				book.getSeries().setNumber(bibBook.getSeries().getNumber());
			}
			
			// series title
			if(StringUtils.isNotEmpty(bibBook.getSeries().getTitle())) {
				book.getSeries().setTitle(bibBook.getSeries().getTitle());
			}			
		}
		
		// volume number
		if(StringUtils.isNotEmpty(bibBook.getVolumeNumber()) && Integer.parseInt(bibBook.getVolumeNumber()) > 0) {
			book.setVolumeNumber(bibBook.getVolumeNumber());
		}
		
		// volume title
		if(StringUtils.isNotEmpty(bibBook.getVolumeTitle())) {
			book.setVolumeTitle(bibBook.getVolumeTitle());
		}
		
		// part number
		if(StringUtils.isNotEmpty(bibBook.getPartNumber()) && Integer.parseInt(bibBook.getPartNumber()) > 0) {
			book.setPartNumber(bibBook.getPartNumber());
		}
		
		// part title
		if(StringUtils.isNotEmpty(bibBook.getPartTitle())) {
			book.setPartTitle(bibBook.getPartTitle());
		}
		
		// edition number
		if(null != bibBook.getEdition()) {			
			if(null == book.getEdition()) {
				book.setEdition(new Edition());
			} 
			
			if(StringUtils.isNotEmpty(bibBook.getEdition().getNumber())) {
				book.getEdition().setNumber(bibBook.getEdition().getNumber());
				book.getEdition().setText(formatEdition(book.getEdition().getNumber()));
			}			
		}
		
		// print date
		if(StringUtils.isNotEmpty(bibBook.getPubDate().getPrint())) {
			book.getPubDate().setPrint(bibBook.getPubDate().getPrint());
		}
		
		// online date
		if(StringUtils.isNotEmpty(bibBook.getPubDate().getOnline())) {
			book.getPubDate().setOnline(getFormatedDate(getOnlineDate(bibBook.getPubDate().getOnline()), ONLINE_DATE_FORMAT));
		}
		
		// alt isbn		
		if(null != bibBook.getAltIsbns() && bibBook.getAltIsbns().size() > 0) {
			if(null == book.getAltIsbns()) {
				book.setAltIsbns(new ArrayList<AltIsbn>());				
				for(AltIsbn bibAltIsbn : bibBook.getAltIsbns()) {
					AltIsbn altIsbn = new AltIsbn();
					altIsbn.setType(bibAltIsbn.getType());
					altIsbn.setValue(bibAltIsbn.getValue());					
					book.getAltIsbns().add(altIsbn);
				}	
			} else {
				for(AltIsbn bibAltIsbn : bibBook.getAltIsbns()) {
					boolean isExists = false;
					for(AltIsbn altIsbnt : book.getAltIsbns()) {				
						if(altIsbnt.getType().equals(bibAltIsbn.getType())) {
							isExists = true;
							altIsbnt.setValue(bibAltIsbn.getValue());
						}
					}					
					if(!isExists) {
						AltIsbn altIsbn = new AltIsbn();
						altIsbn.setType(bibAltIsbn.getType());
						altIsbn.setValue(bibAltIsbn.getValue());
						book.getAltIsbns().add(altIsbn);
					}
				}	
			}
		}		
		
		// subjects
		List<Subject> bibSubjects = biblioData.getSubjectMap().get(isbn);		
		if(null != bibSubjects && bibSubjects.size() > 0) {
			if(null == book.getSubjects()) {
				book.setSubjects(new ArrayList<Subject>());				
				for(Subject bibSubject : bibSubjects) {
					Subject subject = new Subject();
					subject.setCode(bibSubject.getCode());
					subject.setLevel(bibSubject.getLevel());
					subject.setName(bibSubject.getName());					
					book.getSubjects().add(subject);
				}
			} else {
				for(Subject bibSubject : bibSubjects) {
					boolean isExists = false;					
					for(Subject subject : book.getSubjects()) {
						if(subject.getLevel().equals(bibSubject.getLevel())) {
							isExists = true;
							subject.setCode(bibSubject.getCode());
							subject.setName(bibSubject.getName());
						}
					}					
					if(!isExists) {
						Subject subject = new Subject();
						subject.setCode(bibSubject.getCode());
						subject.setLevel(bibSubject.getLevel());
						subject.setName(bibSubject.getName());
						book.getSubjects().add(subject);
					}
				}
			}
		}
		
		// authors
		List<Author> bibAuthors = biblioData.getAuthorMap().get(isbn);
		if(null != bibAuthors && bibAuthors.size() > 0) {
			if(null == book.getAuthors()) {
				book.setAuthors(new ArrayList<Author>());
				for(Author bibAuthor : bibAuthors) {
					Author author = new Author();
					author.setPosition(bibAuthor.getPosition());
					author.setRole(bibAuthor.getRole());
					StringBuilder name = new StringBuilder();
					name.append("<forenames>")
					.append(bibAuthor.getForename()).append("</forenames>")
					.append("<surname>").append(bibAuthor.getSurname()).append("</surname>");
					author.setName(name.toString());
					author.setAffiliation(bibAuthor.getAffiliation());
				}
			} else {
				for(Author bibAuthor : bibAuthors) {
					boolean isExists = false;	
					for(Author author : book.getAuthors()) {
						if(author.getPosition().equals(bibAuthor.getPosition())) {
							isExists = true;
							if(StringUtils.isNotEmpty(bibAuthor.getRole())) {
								author.setRole(bibAuthor.getRole());
							}
							
							String name = author.getName();							
							StringBuilder forenames = new StringBuilder();
							if(StringUtils.isNotEmpty(bibAuthor.getForename())) {
								forenames.append("<forenames>").append(bibAuthor.getForename()).append("</forenames>");
								name = author.getName().replaceAll("<forenames>.*?</forenames>", forenames.toString());
							}
							
							StringBuilder surname = new StringBuilder();
							if(StringUtils.isNotEmpty(bibAuthor.getSurname())) {
								surname.append("<surname>").append(bibAuthor.getSurname()).append("</surname>");
								name = name.replaceAll("<surname>.*?</surname>", surname.toString());
							}
							
							author.setName(name);
							
							if(StringUtils.isNotEmpty(bibAuthor.getAffiliation())) {
								author.setAffiliation(bibAuthor.getAffiliation());
							}
						}
					}
					if(!isExists) {
						Author author = new Author();
						author.setPosition(bibAuthor.getPosition());
						author.setRole(bibAuthor.getRole());
						StringBuilder name = new StringBuilder();
						name.append("<forenames>")
						.append(bibAuthor.getForename()).append("</forenames>")
						.append("<surname>").append(bibAuthor.getSurname()).append("</surname>");
						author.setName(name.toString());
						author.setAffiliation(bibAuthor.getAffiliation());
					}
				}
			}
		}
		
		// === End Biblio Data ===

		return book;
	}
	
	private static String formatEdition(String edition) {
		if(StringUtils.isEmpty(edition)){
			return null;
		}
		else {
			String tensUp = "";
			int indexOfDash = edition.lastIndexOf('-');
			edition = edition.substring(indexOfDash+1).trim();
			int editionNum = Integer.parseInt(edition);
			if (editionNum > 10){
				int edlength = edition.length();
				tensUp = edition.substring(0, edlength-1);
			}
			
			// if less than 10
			if (editionNum <= 10) {
				if(editionNum % 10 == 1 ){
					edition = "1st Edition";
				} else if (editionNum % 10 == 2){
					edition = "2nd Edition";
				} else if (editionNum % 10 == 3){
					edition = "3rd Edition";
				} else {
					edition = edition + "th Edition";
				}
			}
			
			// for cases where edition > 10
			else if (editionNum > 10){
				if (editionNum % 100 == 11 && editionNum < 100){ 
					edition = "11th Edition";
				} else if (editionNum % 100 == 11){
					edition = tensUp.substring(0, tensUp.length()-1) + "11th Edition";
				} else if (editionNum % 100 == 12 && editionNum < 100){ 
					edition = "12th Edition";
				} else if (editionNum % 100 == 12){
					edition = tensUp.substring(0, tensUp.length()-1) + "12th Edition";
				} else if (editionNum % 100 == 13 && editionNum < 100){ 
					edition = "13th Edition";
				} else if (editionNum % 100 == 13){
					edition = tensUp.substring(0, tensUp.length()-1) + "13th Edition";
				} else if (editionNum % 10 == 1 ){
					edition = tensUp + "1st Edition";
				} else if (editionNum % 10 == 2 ){
					edition = tensUp + "2nd Edition";
				} else if (editionNum % 10 == 3 ){
					edition = tensUp + "3rd Edition";
				} else {
					edition = edition + "th Edition";
				}
			}
			return edition;
		}
	}
	
	private static Date getOnlineDate(String dateText) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("ddMMMyyyy");
		try {
			Date specifiedDate = dateFormat.parse("16Nov2009"); // 16 NOV 2009
			date = dateFormat.parse(dateText);
			if(date.before(specifiedDate)) { 
				return specifiedDate;
			}
		} catch (ParseException e) {
			System.out.println("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
		}
		return date;
	}
	
	private static String getFormatedDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	private static List<ContentItem> generateContentItems(
			List<org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.ContentItem> jaxbContentItems,
			String parentId) throws Exception {

		List<ContentItem> contentItems = new ArrayList<ContentItem>();

		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.ContentItem jaxbContentItem : jaxbContentItems) {
			ContentItem contentItem = new ContentItem();

			// parent-id
			contentItem.setParentId(parentId);

			// id
			contentItem.setId(jaxbContentItem.getId());

			// page-start
			contentItem.setPageStart(jaxbContentItem.getPageStart());

			// page-end
			contentItem.setPageEnd(jaxbContentItem.getPageEnd());

			// type
			contentItem.setType(jaxbContentItem.getType());

			// position
			contentItem.setPosition(jaxbContentItem.getPosition());

			// heading
			Heading heading = new Heading();
			if (null != jaxbContentItem.getHeading().getLabel()) {
				heading.setLabel(jaxbContentItem.getHeading().getLabel());
			}
			if (null != jaxbContentItem.getHeading().getSubtitle()) {
				heading.setSubtitle(getContent(jaxbContentItem.getHeading().getSubtitle().getContent()));
			}
			MainTitle title = new MainTitle();
			title.setAlphasort(jaxbContentItem.getHeading().getTitle().getAlphasort());
			title.setText(getContent(jaxbContentItem.getHeading().getTitle().getContent()));
			heading.setTitle(title);
			contentItem.setHeading(heading);

			// doi
			contentItem.setDoi(jaxbContentItem.getDoi());

			// pdf-filename
			if (null != jaxbContentItem.getPdf()) {
				contentItem.setPdfFilename(jaxbContentItem.getPdf().getFilename());
			}

			// contributor
			if (null != jaxbContentItem.getContributorGroup()) {
				List<Author> contributors = new ArrayList<Author>();
				for (Contributor jaxbContributor : jaxbContentItem.getContributorGroup().getContributor()) {
					Author contributor = new Author();
					contributor.setPosition(jaxbContributor.getPosition());
					contributor.setAlphasort(jaxbContributor.getAlphasort());
					contributor.setAffiliation(getContent(jaxbContributor.getAffiliation().getContent()));
					contributor.setName(getContent(jaxbContributor.getName().getContent()));

					contributors.add(contributor);
				}
				contentItem.setContributors(contributors);
			}

			// keyword-group
			List<KeywordGroup> keywordGroups = new ArrayList<KeywordGroup>();
			for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.KeywordGroup jaxbKeywordGroup : jaxbContentItem.getKeywordGroup()) {
				KeywordGroup keywordGroup = new KeywordGroup();
				keywordGroup.setSource(jaxbKeywordGroup.getSource());
				keywordGroup.setKeywords(getKeywords(jaxbKeywordGroup.getKeyword()));
				keywordGroups.add(keywordGroup);
			}
			contentItem.setKeywordGroups(keywordGroups);

			// toc-item
			if (null != jaxbContentItem.getToc()) {
				contentItem.setTocItems(getTocItem(jaxbContentItem.getToc().getTocItem(), "root"));
			}

			// abstract
			if (null != jaxbContentItem.getAbstract()) {
				Abstract contentAbstract = new Abstract();
				contentAbstract.setText(getContent(jaxbContentItem.getAbstract().getPOrBlockOrList()));
				contentAbstract.setFilename(jaxbContentItem.getAbstract().getAltFilename());
				contentAbstract.setProblem(jaxbContentItem.getAbstract().getProblem());
				contentItem.setContentAbstract(contentAbstract);
			}

			// reference
			if (null != jaxbContentItem.getReferences()) {
				contentItem.setReferences(getReferences(jaxbContentItem.getReferences()));
			}

			contentItems.add(contentItem);

			// content-item
			if (null != jaxbContentItem.getContentItem()
					&& jaxbContentItem.getContentItem().size() > 0) {
				contentItems.addAll(generateContentItems(jaxbContentItem.getContentItem(), jaxbContentItem.getId()));
			}
		}

		return contentItems;
	}

	private static org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Book generateJaxbBook(
			String isbn) throws Exception {
		
		StringBuilder headerFile = new StringBuilder(ApplicationProperties.CONTENT_DIR);
		headerFile.append("/").append(isbn).append("/").append(isbn).append(".xml");

		InputStream fis = new FileInputStream(headerFile.toString());
		org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Book book 
			= (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Book) unmarshaller.unmarshal(fis);

		return book;
	}

	private static List<String> getKeywords(List<Keyword> jaxbKeywords)
			throws Exception {
		List<String> keywords = new ArrayList<String>();
		for (Keyword keyword : jaxbKeywords) {
			keywords.add(getContent(keyword.getKwdText().getContent()));

			if (null != keyword.getKeyword() && keyword.getKeyword().size() > 0) {
				keywords.addAll(getKeywords(keyword.getKeyword()));
			}
		}

		return keywords;
	}

	private static List<TocItem> getTocItem(
			List<org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.TocItem> jaxbTocItems,
			String parentId) throws Exception {
		List<TocItem> tocItems = new ArrayList<TocItem>();

		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.TocItem jaxbTocItem : jaxbTocItems) {
			TocItem tocItem = new TocItem();

			// parent-id
			tocItem.setParentId(parentId);

			// id
			tocItem.setId(jaxbTocItem.getId());

			// start-page
			tocItem.setStartPage(jaxbTocItem.getStartpage());

			// text
			tocItem.setText(getContent(jaxbTocItem.getTocText().getContent()));

			// author
			if (null != jaxbTocItem.getAuthor()
					&& jaxbTocItem.getAuthor().size() > 0) {
				List<Author> authors = new ArrayList<Author>();
				for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Author jaxbAuthor : jaxbTocItem
						.getAuthor()) {
					Author author = new Author();
					author.setId(jaxbAuthor.getId());
					author.setPosition(jaxbAuthor.getPosition());
					author.setRole(jaxbAuthor.getRole());
					author.setAlphasort(jaxbAuthor.getAlphasort());
					author.setAffiliation(getContent(jaxbAuthor.getAffiliation().getContent()));
					author.setName(getContent(jaxbAuthor.getName().getContent()));

					authors.add(author);
				}
				tocItem.setAuthors(authors);
			}

			tocItems.add(tocItem);

			// toc-item
			if (null != jaxbTocItem.getTocItem() && jaxbTocItem.getTocItem().size() > 0) {
				tocItems.addAll(getTocItem(jaxbTocItem.getTocItem(),jaxbTocItem.getId()));
			}

		}

		return tocItems;
	}

	private static List<References> getReferences(
			List<org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.References> jaxbReferences)
			throws Exception {

		List<References> references = new ArrayList<References>();

		for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.References jaxbReference : jaxbReferences) {
			References reference = new References();

			// type
			reference.setType(jaxbReference.getType());

			// title
			if (null != jaxbReference.getTitle()) {
				MainTitle title = new MainTitle();
				title.setAlphasort(jaxbReference.getTitle().getAlphasort());
				title.setText(getContent(jaxbReference.getTitle().getContent()));
				reference.setTitle(title);
			}

			// citations
			List<Citation> citations = new ArrayList<Citation>();
			for (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Citation jaxbCitation : jaxbReference.getCitation()) {
				Citation citation = new Citation();
				citation.setId(jaxbCitation.getId());
				citation.setType(jaxbCitation.getType());
				citation.setText(getContent(jaxbCitation.getContent()));

				citations.add(citation);
			}
			reference.setCitations(citations);

			references.add(reference);

			// references
			if (null != jaxbReference.getReferences() && jaxbReference.getReferences().size() > 0) {
				references.addAll(getReferences(jaxbReference.getReferences()));
			}
		}

		return references;
	}

	private static String getContent(List<Object> content) throws Exception {
		StringBuffer result = new StringBuffer();

		for (Object object : content) {
			if (object instanceof Italic) {
				result.append("<i>");
				result.append(getContent(((Italic) object).getContent()));
				result.append("</i>");
			} else if (object instanceof Bold) {
				result.append("<b>");
				result.append(getContent(((Bold) object).getContent()));
				result.append("</b>");
			} else if (object instanceof SmallCaps) {
				result.append("<span style='font-variant:small-caps'>");
				result.append(getContent(((SmallCaps) object).getContent()));
				result.append("</span>");
			} else if (object instanceof Underline) {
				result.append("<u>");
				result.append(getContent(((Underline) object).getContent()));
				result.append("</u>");
			} else if (object instanceof Sup) {
				result.append(getContent(((Sup) object).getContent()));
			} else if (object instanceof Sub) {
				result.append(getContent(((Sub) object).getContent()));
			} else if (object instanceof P) {
				result.append("<p>");
				result.append(getContent(((P) object).getContent()));
				result.append("</p>");
			} else if (object instanceof Block) {
				result.append(getBlock((Block) object));
			} else if (object instanceof org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.List) {
				result.append(getList((org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.List) object));
			} else if (object instanceof Source) {
				result.append(getContent(((Source) object).getContent()));
			} else if (object instanceof CopyrightHolder) {
				result.append(getContent(((CopyrightHolder) object).getContent()));
			} else if (object instanceof CopyrightStatement) {
				result.append(getContent(((CopyrightStatement) object).getContent()));
			} else if (object instanceof JAXBElement) {
				JAXBElement jaxb = (JAXBElement) object;
				if ("suffix".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<suffix>");
					result.append((String) jaxb.getValue());
					result.append("</suffix>");
				} else if ("name-link".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<name-link>");
					result.append((String) jaxb.getValue());
					result.append("</name-link>");
				}
				// ==== start for citation ====
				else if ("volume".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<volume>");
					if(isEscapeSpecialChar) {
						result.append(StringEscapeUtils.escapeXml((String) jaxb.getValue()));
					} else {
						result.append((String) jaxb.getValue());
					}
					result.append("</volume>");
				} else if ("issue".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<issue>");
					if(isEscapeSpecialChar) {
						result.append(StringEscapeUtils.escapeXml((String) jaxb.getValue()));
					} else {
						result.append((String) jaxb.getValue());
					}					
					result.append("</issue>");
				} else if ("startpage".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<startpage>");
					result.append((String) jaxb.getValue());
					result.append("</startpage>");
				} else if ("year".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<year>");
					result.append((String) jaxb.getValue());
					result.append("</year>");
				} else if ("publisher-loc".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<publisher-loc>");
					if(isEscapeSpecialChar) {
						result.append(StringEscapeUtils.escapeXml((String) jaxb.getValue()));
					} else {
						result.append((String) jaxb.getValue());
					}
					result.append("</publisher-loc>");
				} else if ("publisher-name".equalsIgnoreCase(jaxb.getName().toString())) {
					result.append("<publisher-name>");
					if(isEscapeSpecialChar) {
						result.append(StringEscapeUtils.escapeXml((String) jaxb.getValue()));
					} else {
						result.append((String) jaxb.getValue());
					}
					result.append("</publisher-name>");
				}
				// ==== end for citation ====
				else {
					result.append(" ");
					result.append((String) jaxb.getValue());
				}
			} else if (object instanceof Forenames) {
				result.append("<forenames>");
				result.append(getContent(((Forenames) object).getContent()));
				result.append("</forenames>");
			} else if (object instanceof Surname) {
				result.append("<surname>");
				result.append(getContent(((Surname) object).getContent()));
				result.append("</surname>");
			} else if (object instanceof Collab) {
				result.append("<collab>");
				result.append(getContent(((Collab) object).getContent()));
				result.append("</collab>");
			}
			// ==== start for citation ====
			else if (object instanceof org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Author) {

				org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Author jaxbAuthor = (org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.Author) object;
				result.append("<author");
				if (StringUtils.isNotEmpty(jaxbAuthor.getId())) {
					result.append(" id=\"").append(jaxbAuthor.getId()).append("\"");
				}
				if (StringUtils.isNotEmpty(jaxbAuthor.getPosition())) {
					result.append(" position=\"").append(jaxbAuthor.getPosition()).append("\"");
				}
				if (StringUtils.isNotEmpty(jaxbAuthor.getRole())) {
					result.append(" role=\"").append(jaxbAuthor.getRole()).append("\"");
				}
				if (StringUtils.isNotEmpty(jaxbAuthor.getAlphasort())) {
					result.append(" alphasort=\"").append(jaxbAuthor.getAlphasort()).append("\"");
				}
				result.append(">");

				// name
				result.append("<name>");
				result.append(getContent(jaxbAuthor.getName().getContent()));
				result.append("</name>");

				// affiliation
				if (null != jaxbAuthor.getAffiliation()) {
					result.append("<affiliation>");
					result.append(getContent(jaxbAuthor.getAffiliation().getContent()));
					result.append("</affiliation>");
				}
			} else if (object instanceof BookTitle) {
				result.append("<book-title>");
				result.append(getContent(((BookTitle) object).getContent()));
				result.append("</book-title>");
			} else if (object instanceof ChapterTitle) {
				result.append("<chapter-title>");
				result.append(getContent(((ChapterTitle) object).getContent()));
				result.append("</chapter-title>");
			} else if (object instanceof JournalTitle) {
				result.append("<journal-title>");
				result.append(getContent(((JournalTitle) object).getContent()));
				result.append("</journal-title>");
			} else if (object instanceof ArticleTitle) {
				result.append("<article-title>");
				result.append(getContent(((ArticleTitle) object).getContent()));
				result.append("</article-title>");
			} else if (object instanceof Uri) {
				Uri uri = (Uri) object;

				result.append("<uri web-address=\"").append(uri.getWebAddress()).append("\">");
				result.append(uri.getContent());
				result.append("</uri>");
			}
			// ==== end for citation ====
			else {
				if(isEscapeSpecialChar) {
					result.append(StringEscapeUtils.escapeXml((String) object));
				} else {
					result.append((String) object);
				}
			}
		}

		return result.toString();
	}

	private static String getBlock(Block block) throws Exception {
		StringBuffer result = new StringBuffer();
		List<P> pList = block.getP();
		Source source = block.getSource();

		result.append("<blockquote id='indent'>");
		for (P p : pList) {
			result.append("<p>");
			result.append(getContent(p.getContent()));
			result.append("</p>");
		}
		if (null != source) {
			result.append(getContent(source.getContent()));
		}
		result.append("</blockquote>");

		return result.toString();
	}

	private static String getList(
			org.cambridge.ebooks.production.metadatafeeds.bean.jaxb.List ul)
			throws Exception {
		StringBuffer result = new StringBuffer();
		result.append("<ul ");
		if ("bullet".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:disc !important;'>");
		} else if ("number".equalsIgnoreCase(ul.getStyle())) {
			result.append("style='padding: 20px 20px 20px 30px; list-style:decimal !important;'>");
		} else {
			result.append("style='padding: 20px 20px 20px 30px; list-style:none !important;'>");
		}
		for (ListItem li : ul.getListItem()) {
			result.append("<li>");
			result.append(getContent(li.getContent()));
			result.append("</li>");
		}
		result.append("</ul>");

		return result.toString();
	}

	private static String getPrintDate(List<String> dateList) {
		StringBuffer result = new StringBuffer();

		if (dateList.size() > 0) {
			result.append(dateList.get(0));
		}

		return result.toString();
	}

	private static String getOnlineDate(List<OnlineDate> dateList) {
		StringBuffer result = new StringBuffer();

		if (dateList.size() > 0) {
			result.append(dateList.get(0).getContent());
		}

		return result.toString();
	}
}
