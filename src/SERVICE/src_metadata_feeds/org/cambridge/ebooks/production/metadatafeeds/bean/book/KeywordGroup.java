package org.cambridge.ebooks.production.metadatafeeds.bean.book;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class KeywordGroup {

	private String source;
	private List<String> keywords;
	
	public KeywordGroup(){}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}	
}
