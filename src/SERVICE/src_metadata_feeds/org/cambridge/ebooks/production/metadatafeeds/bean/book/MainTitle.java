/**
 * 
 */
package org.cambridge.ebooks.production.metadatafeeds.bean.book;

/**
 * @author kmulingtapang
 */
public class MainTitle {
	private String text;
	private String alphasort;
	
	public MainTitle(){}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getAlphasort() {
		return alphasort;
	}
	public void setAlphasort(String alphasort) {
		this.alphasort = alphasort;
	}
}
