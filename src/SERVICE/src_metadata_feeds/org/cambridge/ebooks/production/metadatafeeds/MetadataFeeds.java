package org.cambridge.ebooks.production.metadatafeeds;

/**
 * @author kmulingtapang
 */
public class MetadataFeeds {
	public static void main (String[] args) {
		long startTime = System.currentTimeMillis();			
		System.out.println("\nExecute metadata feeds...");
		if(args.length > 0) {
			System.out.println("[" + args[0] + "]");
			if(args[0].equals("store")) {
				MetadataWorker.storeFile();
			} else if(args[0].equals("ftp")) {
				MetadataWorker.transferByFtp();
			} else if(args[0].equals("all")) {
				MetadataWorker.storeFile();
				MetadataWorker.transferByFtp();
			} else {
				System.out.println("[Error] use either 'store' or 'ftp' as parameter value.");
			}
		} else {
			System.out.println("[all]");
			MetadataWorker.storeFile();
			MetadataWorker.transferByFtp();
		}
		System.out.println("Execute metadata feeds complete.");
		System.out.println("Total time: " + ((System.currentTimeMillis() - startTime) / 1000) + " secs\n");
	}
}
