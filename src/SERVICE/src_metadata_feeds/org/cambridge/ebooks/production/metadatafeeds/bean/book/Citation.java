package org.cambridge.ebooks.production.metadatafeeds.bean.book;


/**
 * @author kmulingtapang
 */
public class Citation {
	
	private String id;
	private String type;
	private String text;
	
	public Citation(){}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
