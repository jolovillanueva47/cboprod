package org.cambridge.ebooks.service.batchjobs.worker;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.cambridge.ebooks.service.batchjobs.util.ExceptionPrinter;
import org.cambridge.ebooks.service.batchjobs.util.IsbnContentDirUtil;
import org.cambridge.ebooks.service.batchjobs.util.JdbcUtils;

/**
 * @author kmulingtapang
 */
public class DbWorker {
	
	public static void updateEbook(List<String> isbns) {
		System.out.println("update ebook...");
		Connection conn = null;
		try {
			conn = JdbcUtils.getConnection();
			
			Statement st = null;
			try {
				st = conn.createStatement();
				
				int count = 0;
				for(String isbn : isbns) {
					count++;
					File toDelete = IsbnContentDirUtil.copyDummyDtD(isbn);
					System.out.println("[" + count + "]isbn: " + isbn);
					updateFromEbook(isbn, st);
					
					toDelete.delete();
				}				
				
			} catch (SQLException e) {
				System.err.println("[SQLException]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}finally {
				st.close();
			}
			
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				System.err.println("[SQLException]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		System.out.println("update ebook finish.");
	}
	
	private static void updateFromEbook(String isbn, Statement st) throws Exception {		
		String headerFile = IsbnContentDirUtil.getFilePath(isbn, isbn+".xml");
		System.out.println(headerFile);
		
		String doi = XpathWorker.getBookDoi(headerFile);			
		StringBuilder sql = new StringBuilder();
		
		sql.append("update ebook set doi = '")
		.append(doi)
		.append("' where isbn = '").append(isbn).append("'");
		
		System.out.println("sql: " + sql.toString());
		
		st.executeUpdate(sql.toString());		
		
		// update from ebook_content
		List<String> contentIds = XpathWorker.getContentIds(headerFile);
		for(String contentId : contentIds) {
			updateEbookContent(headerFile, contentId, st);
		}		
	}
	
	private static void updateEbookContent(String headerFile, String contentId, Statement st) throws Exception {
		String doi = XpathWorker.getContentDoi(headerFile, contentId);
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("update ebook_content set doi = '")
		.append(doi)
		.append("' where content_id = '").append(contentId).append("'");		
		
		System.out.println("sql: " + sql.toString());
		
		st.executeUpdate(sql.toString());
	}
}
