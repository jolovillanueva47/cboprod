package org.cambridge.ebooks.service.batchjobs.constant;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.cambridge.ebooks.service.batchjobs.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class ApplicationProperties {

	private static Configuration config;
	
	static {
		try {
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + ".properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			System.err.println("[Error] " + ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static final String CONTENT_DIR = config.getString("content.dir");
	public static final String INPUT_FILE = config.getString("input.file");
	
	public static final String JDBC_DRIVER = config.getString("jdbc.driver");
	public static final String JDBC_URL = config.getString("jdbc.url");
	public static final String JDBC_USER = config.getString("jdbc.user");
	public static final String JDBC_PASS = config.getString("jdbc.pass");
}
