package org.cambridge.ebooks.service.batchjobs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.service.batchjobs.constant.ApplicationProperties;
import org.cambridge.ebooks.service.batchjobs.util.ExceptionPrinter;
import org.cambridge.ebooks.service.batchjobs.worker.DbWorker;

public class BatchJob {
	public static void main(String[] args) {
		if(args.length > 0) {
			String arg = args[0];			
			if("1".equals(arg)) {
				updateEbook();
			}
		}
	}
	
	public static void updateEbook() {
		String inputFile = ApplicationProperties.INPUT_FILE;
		
		FileReader fr;
		BufferedReader br;	
		try {
			fr = new FileReader(inputFile);
			br = new BufferedReader(fr);
			List<String> isbns = new ArrayList<String>();
			
			String line = "";
			while((line = br.readLine()) != null){		
				try {				
					isbns.add(line.trim());
				} catch (Exception e) {
					System.out.println("[Exception]");
					System.out.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				}
			}
			
			DbWorker.updateEbook(isbns);
			
		} catch (Exception e) {
			System.err.println("[SQLException]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
}
