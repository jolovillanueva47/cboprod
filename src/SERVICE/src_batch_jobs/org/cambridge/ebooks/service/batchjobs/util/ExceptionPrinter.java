package org.cambridge.ebooks.service.batchjobs.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionPrinter {

	public static String getStackTraceAsString(Exception exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.print("[");
		pw.print(exception.getClass().getName());
		pw.print("]");
		pw.print(exception.getMessage());
		exception.printStackTrace(pw);
		return sw.toString();
	}
}
