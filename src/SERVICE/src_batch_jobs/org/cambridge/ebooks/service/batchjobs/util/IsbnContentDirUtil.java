package org.cambridge.ebooks.service.batchjobs.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.service.batchjobs.constant.ApplicationProperties;

public class IsbnContentDirUtil {
	
	/**
	 * http://en.wikipedia.org/wiki/International_Standard_Book_Number
 	 * http://en.wikipedia.org/wiki/File:ISBN_Details.svg
	 */
	
	private static final String CONTENT_DIR = ApplicationProperties.CONTENT_DIR;
	private static final String CONTENT_DIR_ABSOLUTE = "/app/ebooks/content/";
	private static final String SEPARATOR = File.separator;
	
	public static String getPath(String isbn) {
		
		String ean = isbn.substring(0, 3);
		String group = isbn.substring(3, 5);
		String publisher = isbn.substring(5, 9);
		String title = isbn.substring(9, 12);
		String checkDigit = isbn.substring(12, 13);
		
		String isbnPath = 
			getContentDir() + 
			ean + SEPARATOR + 
			group + SEPARATOR + 
			publisher + SEPARATOR + 
			title + SEPARATOR + 
			checkDigit + SEPARATOR;
		
		File f = new File(isbnPath);
		return f.isDirectory() ? isbnPath : getContentDir() + isbn + SEPARATOR;
	}
	
	private static String getContentDir() {
		String contentDir = CONTENT_DIR;		
		if(!StringUtils.endsWith(CONTENT_DIR, "/")) {
			contentDir = CONTENT_DIR + "/";
		}		
		return contentDir;
	}
	
	
	public static String getFilePath(String isbn, String filename) {
		
		String ean = isbn.substring(0, 3);
		String group = isbn.substring(3, 5);
		String publisher = isbn.substring(5, 9);
		String title = isbn.substring(9, 12);
		String checkDigit = isbn.substring(12, 13);
		
		String isbnPath = 
			getContentDir() + 
			ean + SEPARATOR + 
			group + SEPARATOR + 
			publisher + SEPARATOR + 
			title + SEPARATOR + 
			checkDigit;
		
		File f = new File(isbnPath + SEPARATOR + filename);
		return f.exists() ? isbnPath + SEPARATOR + filename: getContentDir() + isbn + SEPARATOR + filename;
		
	}
	
	
	public static String getFilePathIsbnBreakdown(String isbn, String filename) {		
		return getFilePath(isbn, filename).replaceAll(CONTENT_DIR_ABSOLUTE, "");
	}
	

	public static String bookIdToIsbn(String bookId){
		String eIsbn = null;
		if(isNotEmpty(bookId))
		{
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(bookId);
			
			if(m.find())
			{
				eIsbn = m.group();
			}
		}
		
		return eIsbn;
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
	
	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static File copyDummyDtD(String isbn){
		String dummyDTD = getPath(isbn).substring(0, getPath(isbn).length() - 2)+"header.dtd";
		copyfile(CONTENT_DIR+"/header.dtd", dummyDTD);
		return new File(dummyDTD);
	}
	
	public static void copyfile(String srFile, String dtFile){
	    try{
	      File f1 = new File(srFile);
	      File f2 = new File(dtFile);
	      InputStream in = new FileInputStream(f1);
	      
	      //For Append the file.
//	      OutputStream out = new FileOutputStream(f2,true);

	      //For Overwrite the file.
	      OutputStream out = new FileOutputStream(f2);

	      byte[] buf = new byte[1024];
	      int len;
	      while ((len = in.read(buf)) > 0){
	        out.write(buf, 0, len);
	      }
	      in.close();
	      out.close();
	      System.out.println("dummy dtd created..");
	    }
	    catch(FileNotFoundException ex){
	      System.out.println(ex.getMessage() + " in the specified directory.");
	      System.exit(0);
	    }
	    catch(IOException e){
	      System.out.println(e.getMessage());      
	    }
	  }
}
