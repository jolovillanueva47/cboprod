#!/bin/sh
HOME="/app/ebooks/service/batch_jobs"
LIB="$HOME/lib"
BIN="$HOME/bin"
LOG="$HOME/log"
EXEC_JAR=$LIB"/batch_jobs.jar"
JAVA="/app/jdk1.6.0_20/bin/java"
NOW=`date '+batch.jobs.log.%Y-%d-%m'`
LOG_FILE="$LOG/$NOW.txt"

mkdir $LOG

nohup $JAVA -jar -Xms128m -Xmx512m $EXEC_JAR 1 >> $LOG_FILE &