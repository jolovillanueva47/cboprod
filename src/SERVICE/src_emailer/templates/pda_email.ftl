from:${from}
to:${to}
<#if cc??>cc:${cc}</#if>
<#if bcc??>bcc:${bcc}</#if>
subject:${subject}
body:
<p>
	Order ID:${ebookOrder.orderId}<br />
	Organisation:${ebookOrder.organisationDisplayName}<br />
	<#if ebookOrder.consortiaDisplayName??>Consortia:${ebookOrder.consortiaDisplayName}<br /></#if>
</p>