package org.cambridge.ebooks.service.emailer.constant;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.cambridge.ebooks.service.emailer.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class ApplicationProperties {

	private static Configuration config;
	
	static {
		try {
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + ".properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			System.err.println("[Error] " + ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static final String EMAIL_TEMPLATE_DIR = config.getString("email.template.dir");
	public static final String PDA_EMAIL_TEMPLATE = config.getString("pda.email.template");
	public static final String EMAIL_OUTBOX = config.getString("email.outbox");
	public static final String EMAIL_FROM = config.getString("email.from");
	public static final String EMAIL_SUBJECT = config.getString("email.subject");
	public static final String EMAIL_TO_DEFAULT = config.getString("email.to.default");
	
	public static final String JDBC_DRIVER = config.getString("jdbc.driver");
	public static final String JDBC_URL = config.getString("jdbc.url");
	public static final String JDBC_USER = config.getString("jdbc.user");
	public static final String JDBC_PASS = config.getString("jdbc.pass");
}
