package org.cambridge.ebooks.service.emailer.bean;



/**
 * @author kmulingtapang
 */
public class PdaEbookOrder {
	private String orderId;
	private String bodyId;
	private String organisationDisplayName;
	private String consortiaDisplayName;
	
	public PdaEbookOrder(){}
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrganisationDisplayName() {
		return organisationDisplayName;
	}
	public void setOrganisationDisplayName(String organisationDisplayName) {
		this.organisationDisplayName = organisationDisplayName;
	}
	public String getConsortiaDisplayName() {
		return consortiaDisplayName;
	}
	public void setConsortiaDisplayName(String consortiaDisplayName) {
		this.consortiaDisplayName = consortiaDisplayName;
	}
	public String getBodyId() {
		return bodyId;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}	
}
