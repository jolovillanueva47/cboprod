package org.cambridge.ebooks.service.emailer.worker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.service.emailer.bean.PdaEbookOrder;
import org.cambridge.ebooks.service.emailer.constant.ApplicationProperties;
import org.cambridge.ebooks.service.emailer.util.ExceptionPrinter;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

/**
 * @author kmulingtapang
 */
public class EmailWorker {
	
	private static Configuration cfg;
	
	static {
		cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File(ApplicationProperties.EMAIL_TEMPLATE_DIR));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static void sendPdaEmail(PdaEbookOrder pdaEbookOrder) {
		String template = ApplicationProperties.PDA_EMAIL_TEMPLATE;
		String outbox = ApplicationProperties.EMAIL_OUTBOX;
		
		String from = ApplicationProperties.EMAIL_FROM;
		String to = ApplicationProperties.EMAIL_TO_DEFAULT;
		String cc = null;
		String bcc = null;
		String subject = ApplicationProperties.EMAIL_SUBJECT;		
		
		// get recipients from db
		List<String> recipients = DbWorker.getPdaEbookOrderEmailRecipients(pdaEbookOrder.getBodyId());
		
		// build recipients to StringBuilder
		StringBuilder _recipients = new StringBuilder();
		int ctr = 0;
		for(String recipient : recipients) {
			ctr++;			
			_recipients.append(recipient);
			if(ctr < recipients.size()) {
				_recipients.append(",");
			}
		}		
		
		// if there is atlease 1 recipient, replace the default value of 'to'
		if(_recipients.length() > 0) {
			to = _recipients.toString();
		}
		
		Map<String, Object> root = new HashMap<String, Object>();
		root.put("ebookOrder", pdaEbookOrder);
		root.put("from", from);
		root.put("to", to);
		root.put("cc", cc);
		root.put("bcc", bcc);
		root.put("subject", subject);
		
		File f = new File(outbox + "/" + (new Date()).toString().replace(" ", "_").replace(":", "_") + ".mail");
		
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f)));
			Template xml = cfg.getTemplate(template);
			xml.process(root, bw);
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				bw.close();
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
}
