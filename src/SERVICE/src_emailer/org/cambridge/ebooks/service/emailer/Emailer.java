package org.cambridge.ebooks.service.emailer;

import java.util.List;

import org.cambridge.ebooks.service.emailer.bean.PdaEbookOrder;
import org.cambridge.ebooks.service.emailer.worker.DbWorker;
import org.cambridge.ebooks.service.emailer.worker.EmailWorker;

public class Emailer {
	public static void main(String[] args) {
		System.out.println("execute emailer...");
		
		if(args.length > 0) {
			String arg = args[0];
			System.out.println("arg: " + arg);
			
			if(arg.equals("pda")) {
				List<PdaEbookOrder> pdfEbookOrders = DbWorker.getPdaEbookOrder();
				if(pdfEbookOrders.size() < 1) {
					System.out.println("No orders found.");
				}
				for(PdaEbookOrder pdaEbookOrder : pdfEbookOrders) {
					EmailWorker.sendPdaEmail(pdaEbookOrder);
				}	
			}
		}		
		
		System.out.println("execute emailer finish.");
	}
}
