package org.cambridge.ebooks.service.emailer.worker;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.service.emailer.bean.PdaEbookOrder;
import org.cambridge.ebooks.service.emailer.util.ExceptionPrinter;
import org.cambridge.ebooks.service.emailer.util.JdbcUtils;

/**
 * @author kmulingtapang
 */
public class DbWorker {	
	
	public static List<String> getPdaEbookOrderEmailRecipients(String bodyId) {
		System.out.println("fetching email recipients from BLADE_MEMBER table...");
		List<String> recipients = new ArrayList<String>();
		Connection conn = null; 
		
		try {
			conn = JdbcUtils.getConnection();
			
			Statement st = null;
			ResultSet rs = null;
			
			try {
				st = conn.createStatement();
				
				StringBuilder sql = new StringBuilder("select b.member_email_1 as recipient");
				sql.append(" from oracjoc.admin a, oracjoc.blade_member b")
				.append(" where a.blade_member_id = b.member_id(+)")
				.append(" and a.organisation_id = '")
				.append(bodyId)
				.append("'");
				
				System.out.println("sql: " + sql.toString());
				
				rs = st.executeQuery(sql.toString());
				
				try {					
					while(rs.next()) {
						recipients.add(rs.getString("recipient"));
					}					
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				} finally {
					rs.close();
				}				
				
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			} finally {
				st.close();
			}
			
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		System.out.println("fetching email recipients from BLADE_MEMBER table finish.");
		
		return recipients;
	}
	
	public static List<PdaEbookOrder> getPdaEbookOrder() {
		List<PdaEbookOrder> pdaEbookOrders = new ArrayList<PdaEbookOrder>();
		
		System.out.println("fetching EBOOK_ORDER table...");
		Connection conn = null; 
		
		try {
			conn = JdbcUtils.getConnection();
			
			Statement st = null;
			ResultSet rs = null;
			
			try {
				st = conn.createStatement();
				
				StringBuilder sql = new StringBuilder("select e.order_id as oid, e.body_id as bid, o.display_name as org_disp_name, c.display_name as cons_disp_name");
				sql.append(" from ebook_order e, organisation o, oracjoc.consortium c")
				.append(" where e.body_id = o.body_id")
				.append(" and o.body_id = c.body_id(+)")
				.append(" and e.order_type = 'T'")
				.append(" and e.order_status = '4'");
//				.append(" and (trunc(e.end_date-60) = trunc(sysdate)")
//				.append(" or trunc(e.end_date-30) = trunc(sysdate)")
//				.append(" or trunc(e.end_date-7) = trunc(sysdate)")
//				.append(" or trunc(e.end_date-0) = trunc(sysdate))");
				
				System.out.println("sql: " + sql.toString());
				
				rs = st.executeQuery(sql.toString());
				
				try {					
					while(rs.next()) {
						PdaEbookOrder pdaEbookOrder = new PdaEbookOrder();
						pdaEbookOrder.setOrderId(rs.getString("oid"));
						pdaEbookOrder.setBodyId(rs.getString("bid"));
						pdaEbookOrder.setOrganisationDisplayName(rs.getString("org_disp_name"));
						pdaEbookOrder.setConsortiaDisplayName(rs.getString("cons_disp_name"));
						pdaEbookOrders.add(pdaEbookOrder);
					}					
				} catch (Exception e) {
					System.err.println("[Exception]");
					System.err.println(ExceptionPrinter.getStackTraceAsString(e));
				} finally {
					rs.close();
				}				
				
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			} finally {
				st.close();
			}
			
		} catch (Exception e) {
			System.err.println("[Exception]");
			System.err.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				System.err.println("[Exception]");
				System.err.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		System.out.println("fetching EBOOK_ORDER table finish.");
		
		return pdaEbookOrders;
	}
}
