#!/bin/sh
HOME="/app/ebooks/service/emailer"
LIB="$HOME/lib"
BIN="$HOME/bin"
LOG="$HOME/log"
EXEC_JAR=$LIB"/emailer.jar"
JAVA="/app/jdk1.6.0_20/bin/java"
NOW=`date '+emailer.log.%Y-%d-%m'`
LOG_FILE="$LOG/$NOW.txt"

mkdir $LOG

nohup $JAVA -jar -Xms128m -Xmx512m $EXEC_JAR pda >> $LOG_FILE &