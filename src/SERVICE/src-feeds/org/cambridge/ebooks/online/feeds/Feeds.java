package org.cambridge.ebooks.online.feeds;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.feeds.entities.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.feeds.properties.FeedsProperties;
import org.cambridge.ebooks.online.feeds.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.online.feeds.util.ExceptionPrinter;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.SyndFeedOutput;


public class Feeds {

	private static final long serialVersionUID = -4370917411774187555L;
	private static final String RSS_2 = "rss_2.0";
	private static final String ENCODING = "UTF-8";
	private static final String CONTENT_TYPE_XML = "text/xml";
	private static final String BR = "<br />";
	private static final String EBOOK_JSF = "ebook.jsf?bid=";
	private static final String PUBLISHED_ON = "Published online: ";		
	private static final String BLURB = "Blurb:";
	public static final String FEED_TITLE_PARAM = "FEED_TITLE";	
	
	protected void createFeeds(EBookSubjectHierarchy subject, List<BookMetadataDocument> results)throws IOException {
		SyndFeedOutput output = new SyndFeedOutput();
		PrintWriter w = null;
		
		try {	
			String rss = output.outputString(generateRss(subject, results));
			File file = new File(FeedsProperties.FEEDS_DIR + "feed_" + subject.getSubjectId() + "_rss_2.0.xml");
			w = new PrintWriter(file, ENCODING);			
			w.write(rss);
		} catch (Exception e) {			
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			w.close();
		}		
	}
	
	private String generateFeedTitle(){				
		return FeedsProperties.CHANNEL_TITLE;
	}	
	
	protected SyndFeed generateRss(EBookSubjectHierarchy subject, List<BookMetadataDocument> results){
		SyndFeed feed = new SyndFeedImpl();
		feed.setFeedType(RSS_2);
		feed.setTitle(generateFeedTitle());
		feed.setLink(FeedsProperties.CONTEXT_PATH);				
		feed.setDescription("New Cambridge Books Online Titles in " + subject.getSubjectName());		
		feed.setEntries(generateEntries(results));		
		return feed;
	}
	
	private List<SyndEntry> generateEntries(List<BookMetadataDocument> results){
		List<SyndEntry> alEntries = new ArrayList<SyndEntry>();
		List<BookMetadataDocument> srList =  results;
		for(BookMetadataDocument sr : srList) 
		{						
			SyndEntry entry = new SyndEntryImpl();	
			entry.setTitle(stripHtmlTags(sr.getTitle()));
			entry.setLink(getBookLink(sr.getId()));			
			entry.setDescription(createEntryContent(sr));
			
			alEntries.add(entry);
		}
		return alEntries;
	}
	
	protected String getBookLink(String bookId){		
		return FeedsProperties.CONTEXT_PATH + EBOOK_JSF + bookId;
	}
	
	
	protected SyndContent createEntryContent(BookMetadataDocument metadata){
		SyndContent content = new SyndContentImpl();
		content.setType(CONTENT_TYPE_XML);
		StringBuffer sb = new StringBuffer();
		sb.append("DOI: " + metadata.getDoi());
		sb.append(BR);
		sb.append("Print Date: " + metadata.getPrintDate());
		sb.append(BR);
		sb.append(PUBLISHED_ON + metadata.getOnlineDate());
		sb.append(BR + BR);
		
		String blurb = null;
		try {
			blurb = stripHtmlTags(metadata.getBlurb());
		} catch (Exception e) {
			System.out.println("[Exception]blurb:" + e.getMessage());
			blurb = "";
		}
		
		if(StringUtils.isNotEmpty(blurb)) {
			sb.append(BLURB);
			sb.append(BR);
			sb.append(blurb);
			sb.append(BR + BR);	
		}
		content.setValue(sb.toString());
		return content;
	}
	
	protected static String stripHtmlTags(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("(?i)(<[\\s]*[^>]*(" +
				"a|abbr|acronym|address|applet|area|b|base|basefont|bdo|big|blockquote|body|" +
				"br|button|caption|center|cite|code|col|colgroup|dd|del|dfn|" +
				"dir|div|dl|dt|em|fieldset|font|form|frame|frameset|" +
				"h1|h2|h3|h4|h5|h6|head|html|i|iframe|img|input|ins|isindex|kbd|label|" +
				"legend|li|link|map|menu|meta|noframes|noscript|object|ol|optgroup|option|" +
				"p|param|pre|q|s|samp|script|select|small|span|strike|strong|style|sub|sup|" +
				"table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var|xmp)[^>]*>)", "");
	}
}
