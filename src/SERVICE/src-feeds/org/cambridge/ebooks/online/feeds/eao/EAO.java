package org.cambridge.ebooks.online.feeds.eao;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.cambridge.ebooks.online.feeds.properties.FeedsProperties;


public class EAO {
	
	private static final Map<String, String> props;
	static
	{
		props= new HashMap<String, String>();
        props.put(FeedsProperties.JDBC_URL, FeedsProperties.JDBC_URL_VALUE);
	}
	
	private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("feedsUnit", props);
	
	public static EntityManager createEntityManager(){
		return EMF.createEntityManager();
	}
	
	public static void closeEntityManager(EntityManager em){
		if(em != null && em.isOpen())
			em.close();
	}
	
	public static EntityManagerFactory getEntityManagerFactory(){
		return EMF;
	}
}
