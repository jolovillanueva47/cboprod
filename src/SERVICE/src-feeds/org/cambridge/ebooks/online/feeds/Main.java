package org.cambridge.ebooks.online.feeds;

import org.cambridge.ebooks.online.feeds.util.ExceptionPrinter;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		try  {
			FeedsSolrWorker.createFeeds();
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}

}
