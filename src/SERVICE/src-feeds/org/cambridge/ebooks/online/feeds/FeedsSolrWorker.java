package org.cambridge.ebooks.online.feeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.cambridge.ebooks.online.feeds.eao.FeedsEAO;
import org.cambridge.ebooks.online.feeds.entities.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.feeds.solr.bean.BookMetadataDocument;
import org.cambridge.ebooks.online.feeds.solr.util.SOLRSearchWorker;
import org.cambridge.ebooks.online.feeds.solr.util.SOLRServer;
import org.cambridge.ebooks.online.feeds.util.ListUtil;



public class FeedsSolrWorker {
	
	public enum DAYS_TO_SUBTRACT{
		LESS_30_DAYS(30), LESS_90_DAYS(130); //for testing
		
		
		private int days;
		
		DAYS_TO_SUBTRACT(int days){ this.days = days; }

		public int getDays() {
			return days;
		}

		public void setDays(int days) {
			this.days = days;
		}
	}
	
	public static void createFeeds() throws IOException{
		Feeds feeds = new Feeds();
		
		//List<EBookSubjectHierarchy2> subjectHierarchies = FeedsEAO.newPublishedSubjectAreas(DAYS_TO_SUBTRACT.LESS_90_DAYS);
//		List<EBookSubjectHierarchy2> subjectHierarchies = FeedsEAO.publishedTitlesFromSubjectAreas();
//		if(ListUtil.isNotNullAndEmpty(subjectHierarchies))
//		{
//			
//			for(int index=0; index<subjectHierarchies.size(); index++)
//			{
//				List<EBookSubjectIsbn> subjectIsbns = subjectHierarchies.get(index).getSubjectIsbn();
//				
//				List<String> bookIds = new ArrayList<String>();
//				for(EBookSubjectIsbn subjectIsbn : subjectIsbns)
//					bookIds.add(subjectIsbn.getEbook().getBookId());
//				
//				if(bookIds != null && bookIds.isEmpty())
//				{
//					int ctr = 1;
//					try
//					{
//						while(subjectHierarchies.get(index + ctr).getParentId().startsWith(subjectHierarchies.get(index).getSubjectId()))
//						{
//							subjectIsbns = subjectHierarchies.get(index + ctr).getSubjectIsbn();
//							for(EBookSubjectIsbn subjectIsbn : subjectIsbns)
//								bookIds.add(subjectIsbn.getEbook().getBookId());
//							
//							ctr++;
//						}
//
//					}
//					catch(Exception ex)
//					{
//						System.out.println("index: " + ctr);
//					}
//					
//					if(bookIds != null && !bookIds.isEmpty())
//					{
//						List<BookMetadataDocument> books = generateDocument(bookIds);
//						feeds.createFeeds(subjectHierarchies.get(index), books);
//					}
//				}
//				else if(ListUtil.isNotNullAndEmpty(bookIds))
//				{
//					List<BookMetadataDocument> books = generateDocument(bookIds);
//					feeds.createFeeds(subjectHierarchies.get(index), books);
//				}
//				
//			}			
//			System.out.println("Done.");
//		}	
		
		
		// all subjects
		List<EBookSubjectHierarchy> subjects = FeedsEAO.getAllSubjectAreas();
		
		if(ListUtil.isNotNullAndEmpty(subjects)) {
			// iterate all subjects
			for(EBookSubjectHierarchy subject : subjects) {
				System.out.println("subject_id:" + subject.getSubjectId() + " subject_name:" + subject.getSubjectName());
				if("N".equalsIgnoreCase(subject.getFinalLevel())) { // has sub nodes 						

					// sub subjects
					List<EBookSubjectHierarchy> subSubjects = FeedsEAO.getSubSubject(subject.getSubjectId());
					
					// vista subject codes
					List<String> vistaSubjectCodes = new ArrayList<String>();
					for(EBookSubjectHierarchy subSubject : subSubjects) {
						vistaSubjectCodes.add(subSubject.getSubjectId()); // query return of vista_subject_code is alias with subject_id(primary key)
					}
					
					// book documents
					List<BookMetadataDocument> books = generateBooks(vistaSubjectCodes);
					
					// generate rss feeds
					feeds.createFeeds(subject, books);
					
				} else { // final level Y - final node		
					
					// final subject
					EBookSubjectHierarchy finalSubject = FeedsEAO.getSubject(subject.getSubjectId());
					
					// vista subject codes
					List<String> vistaSubjectCodes = new ArrayList<String>();
					vistaSubjectCodes.add(finalSubject.getVistaSubjectCode());
					
					// book documents
					List<BookMetadataDocument> books = generateBooks(vistaSubjectCodes);
					
					// generate rss feeds
					feeds.createFeeds(subject, books);
				}
			}
			
			System.out.println("done.");
		}		
	}

	public static synchronized List<BookMetadataDocument> generateDocument(List<String> bookIds)
	{
		List<BookMetadataDocument> result = new ArrayList<BookMetadataDocument>();
		if(ListUtil.isNotNullAndEmpty(bookIds))
		{
			SOLRSearchWorker solrWorker = new SOLRSearchWorker();
			StringBuffer search = new StringBuffer();
			int ctr = 0;
			
			
			search.append("id:(");
			for(String bookId : bookIds)
			{
				search.append(bookId).append(" OR ");
				
				if(ctr == 1000)
				{ 
					search.replace(search.length()-4, search.length(), "");
					search.append(")").append(" AND content_type:book");
					
					List<BookMetadataDocument> temp = solrWorker.searchCore(new BookMetadataDocument(), search.toString(), SOLRServer.getBookCore());
					if(ListUtil.isNotNullAndEmpty(temp))
						result.addAll(temp); 
					
					search = new StringBuffer("id:(");
					ctr = 0;
					continue;
				}
				ctr++;
			}
			
			search.replace(search.length()-4, search.length(), "");
			search.append(")").append(" AND content_type:book");
			
			if(search.toString().equals(") AND content_type:book"))
			{
				System.err.println("bookIds: " + bookIds);
				System.err.println("bookIds length: " + bookIds.size());
				System.err.println("search: " + search.toString());
			}
			
			List<BookMetadataDocument> temp = solrWorker.searchCore(new BookMetadataDocument(), search.toString(), SOLRServer.getBookCore());
			if(ListUtil.isNotNullAndEmpty(temp))
				result.addAll(temp); 
		}	
		
		return result;
	}
	
	public static synchronized List<BookMetadataDocument> generateBooks(List<String> vistaSubjectCodes) {
		List<BookMetadataDocument> result = new ArrayList<BookMetadataDocument>();
		if(ListUtil.isNotNullAndEmpty(vistaSubjectCodes)) {
			SOLRSearchWorker solrWorker = new SOLRSearchWorker();
			StringBuffer search = new StringBuffer();
			int ctr = 0;
			
			search.append("subject_code:(");
			for(String vistaSubjectCode : vistaSubjectCodes) {
				search.append(vistaSubjectCode).append(" OR ");
				
				if(ctr == 1000) { 
					search.replace(search.length()-4, search.length(), "");
					search.append(")").append(" AND content_type:book");
					
					List<BookMetadataDocument> temp = solrWorker.searchCore(new BookMetadataDocument(), search.toString(), SOLRServer.getBookCore());
					if(ListUtil.isNotNullAndEmpty(temp)) {
						result.addAll(temp); 
					}
					
					search = new StringBuffer("subject_code:(");
					ctr = 0;
					continue;
				}
				ctr++;
			}
			
			search.replace(search.length()-4, search.length(), "");
			search.append(")").append(" AND content_type:book");
			
			if(search.toString().equals(") AND content_type:book")) {
				System.err.println("vistaSubjectCodes: " + vistaSubjectCodes);
				System.err.println("vistaSubjectCodes length: " + vistaSubjectCodes.size());
				System.err.println("search: " + search.toString());
			}
			
			List<BookMetadataDocument> temp = solrWorker.searchCore(new BookMetadataDocument(), search.toString(), SOLRServer.getBookCore());
			if(ListUtil.isNotNullAndEmpty(temp)) {
				result.addAll(temp); 
			}
		}	
		
		return result;
	}
	
	
//	public static void main(String[] args){
//		System.out.println("AAI0".startsWith("AAA"));
//	}
	
}

