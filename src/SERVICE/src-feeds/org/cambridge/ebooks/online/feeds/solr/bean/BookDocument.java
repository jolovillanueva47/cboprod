package org.cambridge.ebooks.online.feeds.solr.bean;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

public class BookDocument {

//META-DATA
	
	@Field("id")
	private String id;
	
	@Field("book_id")
	private String bookId;
	
	@Field("book_group")
	private String bookGroup;
	
	@Field("content_type")
	private String contentType;

	
	@Field("title")
	private String title;
	
	@Field("title_alphasort")
	private String titleAlphasort;
	
	@Field("subtitle")
	private String subtitle;
	
	
	@Field("edition")
	private String edition;
	
	@Field("edition_number")
	private String editionNumber;
	
	
	@Field("author_name")
	private List<String> authorNameList;
		
	@Field("author_name_alphasort")
	private String authorNameAlphasort;
	
	@Field("author_affiliation")
	private List<String> authorAffiliationList;
	
	@Field("author_role")
	private List<String> authorRoleList;
	
	@Field("author_position")
	private List<String> authorPositionList;
	
	
	
	@Field("publisher_id")
	private String publisherId;
	
	@Field("publisher_name")
	private String publisherName;
	
	@Field("publisher_loc")
	private String publisherLoc;
	
	
	@Field("doi")
	private String doi;
	
	@Field("alt_doi")
	private String altDoi;
	
	
	@Field("isbn")
	private String isbn;
	
	@Field("alt_isbn_paperback")
	private String altIsbnPaperback;
	
	@Field("alt_isbn_hardback")
	private String altIsbnHardback;
	
	@Field("alt_isbn_eisbn")
	private String altIsbnEisbn;
	
	@Field("alt_isbn_other")
	private String altIsbnOther;
	

	@Field("volume_number")
	private String volumeNumber;
	
	@Field("volume_title")
	private String volumeTitle;
	
	@Field("part_number")
	private String partNumber;
	
	@Field("part_title")
	private String partTitle;
	
	
	@Field("series")
	private String series;
	
	@Field("series_alphasort")
	private String seriesAlphasort;
	
	@Field("series_number")
	private String seriesNumber;
	
	@Field("series_code")
	private String seriesCode;
	
	@Field("series_position")
	private String seriesPosition;
	
		
	@Field("print_date")
	private String printDate;
		
	@Field("online_date")
	private String onlineDate;
	

	@Field("copyright_statement")
	private List<String> copyrightStatementList;

	
	@Field("subject")
	private List<String> subjectList;
	
	@Field("subject_level")
	private List<String> subjectLevelList;
	
	@Field("subject_code")
	private List<String> subjectCodeList;
	
	
	@Field("cover_image_type")
	private List<String> coverImageTypeList;
	
	@Field("cover_image_filename")
	private List<String> coverImageFilenameList;
	
	
	
//CONTENT-ITEM
	
//	@Field("id")
//	private String id;
	
	@Field("parent_id")
	private String parentId;
	
//	@Field("book_id")
//	private String bookId;
//	
//	@Field("content_type")
//	private String contentType;
	
//	@Field("isbn")
//	private String isbn;
	
	
	@Field("content_id")
	private String contentId;
	
	@Field("page_start")
	private String pageStart;
	
	@Field("page_end")
	private String pageEnd;
	
	@Field("type")
	private String type;
	
	@Field("position")
	private String position;
	
	
	
	@Field("pdf_filename")
	private String pdfFilename;	
	
//	@Field("doi")
//	private String doi;
	
	
	
	@Field("label")
	private String label;
	
//	@Field("title")
//	private String title;
//	
//	@Field("title_alphasort")
//	private String titleAlphasort;
//	
//	@Field("subtitle")
//	private String subtitle;	
	
	
	
	@Field("abstract_alt_filename")
	private String abstractFilename;
	
	@Field("abstract_problem")
	private String abstractProblem;
	
	@Field("keyword_text")
	private List<String> keywordTextList;
	
	
	
	@Field("toc_text")
	private String tocText;
	
//	@Field("author_name")
//	private List<String> authorNameList;
//	
//	@Field("author_affiliation")
//	private List<String> authorAffiliationList;
	
	@Field("author_name_alphasort")
	private List<String> authorNameAlphasortList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookGroup() {
		return bookGroup;
	}

	public void setBookGroup(String bookGroup) {
		this.bookGroup = bookGroup;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleAlphasort() {
		return titleAlphasort;
	}

	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public List<String> getAuthorNameList() {
		return authorNameList;
	}

	public void setAuthorNameList(List<String> authorNameList) {
		this.authorNameList = authorNameList;
	}

	public String getAuthorNameAlphasort() {
		return authorNameAlphasort;
	}

	public void setAuthorNameAlphasort(String authorNameAlphasort) {
		this.authorNameAlphasort = authorNameAlphasort;
	}

	public List<String> getAuthorAffiliationList() {
		return authorAffiliationList;
	}

	public void setAuthorAffiliationList(List<String> authorAffiliationList) {
		this.authorAffiliationList = authorAffiliationList;
	}

	public List<String> getAuthorRoleList() {
		return authorRoleList;
	}

	public void setAuthorRoleList(List<String> authorRoleList) {
		this.authorRoleList = authorRoleList;
	}

	public List<String> getAuthorPositionList() {
		return authorPositionList;
	}

	public void setAuthorPositionList(List<String> authorPositionList) {
		this.authorPositionList = authorPositionList;
	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublisherLoc() {
		return publisherLoc;
	}

	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getAltDoi() {
		return altDoi;
	}

	public void setAltDoi(String altDoi) {
		this.altDoi = altDoi;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAltIsbnPaperback() {
		return altIsbnPaperback;
	}

	public void setAltIsbnPaperback(String altIsbnPaperback) {
		this.altIsbnPaperback = altIsbnPaperback;
	}

	public String getAltIsbnHardback() {
		return altIsbnHardback;
	}

	public void setAltIsbnHardback(String altIsbnHardback) {
		this.altIsbnHardback = altIsbnHardback;
	}

	public String getAltIsbnEisbn() {
		return altIsbnEisbn;
	}

	public void setAltIsbnEisbn(String altIsbnEisbn) {
		this.altIsbnEisbn = altIsbnEisbn;
	}

	public String getAltIsbnOther() {
		return altIsbnOther;
	}

	public void setAltIsbnOther(String altIsbnOther) {
		this.altIsbnOther = altIsbnOther;
	}

	public String getVolumeNumber() {
		return volumeNumber;
	}

	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}

	public String getVolumeTitle() {
		return volumeTitle;
	}

	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartTitle() {
		return partTitle;
	}

	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getSeriesAlphasort() {
		return seriesAlphasort;
	}

	public void setSeriesAlphasort(String seriesAlphasort) {
		this.seriesAlphasort = seriesAlphasort;
	}


	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getSeriesPosition() {
		return seriesPosition;
	}

	public void setSeriesPosition(String seriesPosition) {
		this.seriesPosition = seriesPosition;
	}

	public String getPrintDate() {
		return printDate;
	}

	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}

	public String getOnlineDate() {
		return onlineDate;
	}

	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	public List<String> getCopyrightStatementList() {
		return copyrightStatementList;
	}

	public void setCopyrightStatementList(List<String> copyrightStatementList) {
		this.copyrightStatementList = copyrightStatementList;
	}

	public List<String> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}

	public List<String> getSubjectLevelList() {
		return subjectLevelList;
	}

	public void setSubjectLevelList(List<String> subjectLevelList) {
		this.subjectLevelList = subjectLevelList;
	}

	public List<String> getSubjectCodeList() {
		return subjectCodeList;
	}

	public void setSubjectCodeList(List<String> subjectCodeList) {
		this.subjectCodeList = subjectCodeList;
	}

	public List<String> getCoverImageTypeList() {
		return coverImageTypeList;
	}

	public void setCoverImageTypeList(List<String> coverImageTypeList) {
		this.coverImageTypeList = coverImageTypeList;
	}

	public List<String> getCoverImageFilenameList() {
		return coverImageFilenameList;
	}

	public void setCoverImageFilenameList(List<String> coverImageFilenameList) {
		this.coverImageFilenameList = coverImageFilenameList;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPdfFilename() {
		return pdfFilename;
	}

	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAbstractFilename() {
		return abstractFilename;
	}

	public void setAbstractFilename(String abstractFilename) {
		this.abstractFilename = abstractFilename;
	}

	public String getAbstractProblem() {
		return abstractProblem;
	}

	public void setAbstractProblem(String abstractProblem) {
		this.abstractProblem = abstractProblem;
	}

	public List<String> getKeywordTextList() {
		return keywordTextList;
	}

	public void setKeywordTextList(List<String> keywordTextList) {
		this.keywordTextList = keywordTextList;
	}

	public String getTocText() {
		return tocText;
	}

	public void setTocText(String tocText) {
		this.tocText = tocText;
	}

	public List<String> getAuthorNameAlphasortList() {
		return authorNameAlphasortList;
	}

	public void setAuthorNameAlphasortList(List<String> authorNameAlphasortList) {
		this.authorNameAlphasortList = authorNameAlphasortList;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
	
//	@Field("author_role")
//	private List<String> authorRoleList;

}
