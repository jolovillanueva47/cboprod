package org.cambridge.ebooks.online.feeds.solr.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.cambridge.ebooks.online.feeds.properties.FeedsProperties;

public class SOLRServer {

	private static SolrServer bookCore;
	private static SolrServer contentCore;
	
	public static final String KEY_SOCKET_TIMEOUT = "KEY_SOCKET_TIMEOUT";
	public static final String KEY_CONN_TIMEOUT = "KEY_CONN_TIMEOUT";
	public static final String KEY_DEF_MAX_CONN_PER_HOST = "KEY_DEF_MAX_CONN_PER_HOST";
	public static final String KEY_MAX_TOTAL_CONN = "KEY_MAX_TOTAL_CONN";
	public static final String KEY_FOLLOW_REDIRECTS = "KEY_FOLLOW_REDIRECTS";
	public static final String KEY_ALLOW_COMPRESSION = "KEY_ALLOW_COMPRESSION";
	public static final String KEY_MAX_RETRIES = "KEY_MAX_RETRIES";
	
	static 
	{
		try 
		{
			Map<String, String> settings = new HashMap<String, String>();
			settings.put(SOLRServer.KEY_SOCKET_TIMEOUT, FeedsProperties.SOCKET_TIMEOUT);
			settings.put(SOLRServer.KEY_CONN_TIMEOUT, FeedsProperties.CONNECTION_TIMEOUT);
			settings.put(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST, FeedsProperties.MAX_DEFAULT_CONNECTION);
			settings.put(SOLRServer.KEY_MAX_TOTAL_CONN, FeedsProperties.MAX_TOTAL_CONNECTION);
			settings.put(SOLRServer.KEY_FOLLOW_REDIRECTS, FeedsProperties.FOLLOW_REDIRECTS);
			settings.put(SOLRServer.KEY_ALLOW_COMPRESSION, FeedsProperties.ALLOW_COMPRESSION);
			settings.put(SOLRServer.KEY_MAX_RETRIES, FeedsProperties.MAX_RETRIES);
			
			bookCore = initServer(new CommonsHttpSolrServer(FeedsProperties.SOLR_HOME_URL + FeedsProperties.CORE_BOOK), settings);;	
			contentCore = initServer(new CommonsHttpSolrServer(FeedsProperties.SOLR_HOME_URL + FeedsProperties.CORE_CONTENT), settings);		

		} 
		catch (Exception e) 
		{
			System.err.println("[Exception] SOLRServer message: " + e.getMessage());
		}
	}	
		
	public static SolrServer getBookCore() {
		return bookCore;
	}
	
	public static SolrServer getContentCore() {
		return contentCore;
	}
	
	public static SolrServer initServer(CommonsHttpSolrServer server, Map<String, String> settings){

		server.setSoTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_SOCKET_TIMEOUT)));
		server.setConnectionTimeout(Integer.parseInt(settings.get(SOLRServer.KEY_CONN_TIMEOUT)));
		server.setDefaultMaxConnectionsPerHost(Integer.parseInt(settings.get(SOLRServer.KEY_DEF_MAX_CONN_PER_HOST)));
		server.setMaxTotalConnections(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_TOTAL_CONN)));
		server.setFollowRedirects(Boolean.parseBoolean(settings.get(SOLRServer.KEY_FOLLOW_REDIRECTS)));
		server.setAllowCompression(Boolean.parseBoolean(settings.get(SOLRServer.KEY_ALLOW_COMPRESSION)));
		server.setMaxRetries(Integer.parseInt(settings.get(SOLRServer.KEY_MAX_RETRIES)));

		return server;
	}
}
