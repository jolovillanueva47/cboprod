package org.cambridge.ebooks.online.feeds.eao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cambridge.ebooks.online.feeds.entities.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.feeds.util.ExceptionPrinter;


public class FeedsEAO extends EAO{
	
//	@SuppressWarnings("unchecked")
//	public static <T> List<T> query(T type, String q, Object... params) { 
//		List<T> result = null;
//		EntityManager em = createEntityManager();
//		
//		try
//		{
//			Query query = em.createNativeQuery(q, type.getClass());
//	        if (params != null && params.length > 0) 
//	        { 
//	        	int ctr = 1;
//		        for (Object param : params)  
//		        	query.setParameter( ctr++, param); 
//	        }
//	        
//	        result = (List<T>)query.getResultList();
//		}
//		catch(Throwable t)
//		{
//			System.err.println("[ERROR] " + FeedsEAO.class + " query(T, String, String...) " + t.getMessage());
//		}
//		finally
//		{
//			closeEntityManager(em);
//		}
//		
//		return  result;
//	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> query(T type, String q, String... params){
		List<T> result = null;
		EntityManager em = createEntityManager();
		try {
			Query query = em.createNativeQuery(q, type.getClass());
			if(null != params && params.length > 0) {
				int ctr = 1;
				for(String param : params) {
					query.setParameter(ctr++, param);
				}
			}
			result = (List<T>)query.getResultList();
		} catch(Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			closeEntityManager(em);
		}
		
		return result;
	}
	
	public static <T> T querySingle(T type, String q, String... params) {
		T result = null;
		EntityManager em = createEntityManager();
		try {
			Query query = em.createNativeQuery(q, type.getClass());
			if(null != params && params.length > 0) {
				int ctr = 1;
				for(String param : params) {
					query.setParameter(ctr++, param);
				}
			}
			result = (T)query.getSingleResult();
		} catch(Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			closeEntityManager(em);
		}		
		return result;
	}
	
//	@SuppressWarnings("unchecked")
//	private static List<Object[]> query(String q){
//		List result = null;
//		EntityManager em = createEntityManager();
//		try
//		{
//			Query query = em.createNativeQuery(q);
//			result = query.getResultList();
//		}
//		catch(Throwable t)
//		{
//			System.err.println("[ERROR] " + FeedsEAO.class + " query(String) " + t.getMessage());
//		}
//		finally
//		{
//			closeEntityManager(em);
//		}
//		
//		return result;
//	}
	
//	public static List<EBookSubjectHierarchy2> newPublishedSubjectAreas(DAYS_TO_SUBTRACT toMinusFromCurrentDate){
//
//		StringBuilder sql = new StringBuilder();
//		sql.append("SELECT hie.* ");
//		sql.append("FROM ebook_subject_hierarchy hie ");
//		sql.append("LEFT JOIN ebook_subject_isbn subject_isbn ");
//		sql.append("ON(hie.subject_id = subject_isbn.subject_id OR subject_isbn.subject_id LIKE hie.subject_id||'%') ");
//		sql.append("LEFT JOIN ebook_isbn_data_load load ");
//		sql.append("ON (load.isbn  = subject_isbn.isbn) ");
//		sql.append("LEFT JOIN ebook e ");
//		sql.append("ON (e.isbn  = load.isbn) ");
//		sql.append("WHERE (hie.active_subject = 'Y'  AND load.online_flag = 'Y' AND e.status = 7) ");
//		sql.append("AND load.pub_date_online BETWEEN SYSDATE-?1 AND SYSDATE ");
//		sql.append("ORDER BY hie.subject_id ");
//		
//		return query(new EBookSubjectHierarchy2(), sql.toString(), toMinusFromCurrentDate.getDays());
//	}
	
//	@Deprecated
//	public static List<EBookSubjectHierarchy2> publishedTitlesFromSubjectAreas(){
//
//		StringBuilder sql = new StringBuilder();
//		
//		sql.append("SELECT DISTINCT * FROM ( ");
//		
//		sql.append("SELECT hie.* ");
//		sql.append("FROM ebook_subject_hierarchy hie ");
//		sql.append("LEFT JOIN ebook_subject_isbn subject_isbn ");
//		sql.append("ON(hie.subject_id = subject_isbn.subject_id OR subject_isbn.subject_id LIKE hie.subject_id||'%') ");
//		sql.append("LEFT JOIN ebook_isbn_data_load load ");
//		sql.append("ON (load.isbn  = subject_isbn.isbn) ");
//		sql.append("LEFT JOIN ebook e ");
//		sql.append("ON (e.isbn  = load.isbn) ");
//		sql.append("WHERE (e.status = 7) ");
////		sql.append("AND load.pub_date_online BETWEEN SYSDATE-?1 AND SYSDATE ");
////		sql.append("AND hie.subject_id LIKE 'D%' ");
////		sql.append("ORDER BY hie.subject_id, load.pub_date_online ");
//		
//		sql.append(") z ORDER BY subject_id");
//		
//		return query(new EBookSubjectHierarchy2(), sql.toString());
//	}

	public static List<EBookSubjectHierarchy> getAllSubjectAreas() {		
		StringBuilder query = new StringBuilder();
		query.append("SELECT * FROM")
		.append(" (SELECT path, subject_id, COUNT(*)-1 AS \"SERIES_COUNT\" FROM")
			.append(" (SELECT")
			.append(" distinct SYS_CONNECT_BY_PATH(hie.subject_id,';') path,")
			.append(" hie.subject_id,")
			.append(" hie.subject_name,")
			.append(" hie.subject_level,")
			.append(" hie.parent_id,")
			.append(" hie.vista_subject_code,")
			.append(" hie.final_level,")
			.append(" hie.title_count,")
			.append(" hie.active_subject,")
			.append(" load.series_code")
			.append(" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn")
			.append(" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook_isbn_data_load load")
			.append(" ON load.isbn = isbn.isbn")
			.append(" WHERE active_subject = 'Y'")
			.append(" CONNECT BY PRIOR hie.subject_id = hie.parent_id START WITH hie.subject_level = 1)")
		.append(" GROUP BY path, subject_id) res,")
		.append(" ebook_subject_hierarchy esh")
		.append(" WHERE res.subject_id = esh.subject_id")
		.append(" ORDER BY esh.subject_level, esh.parent_id, esh.subject_id");
		
		
		return query(new EBookSubjectHierarchy(), query.toString());
	}
	
	public static EBookSubjectHierarchy getSubject(String subjectId) {		
		StringBuilder query = new StringBuilder();
		query.append("select * from ebook_subject_hierarchy where subject_id = ?1");
		
		return querySingle(new EBookSubjectHierarchy(), query.toString(), subjectId);
	}
	
	public static List<EBookSubjectHierarchy> getSubSubject(String parentId) {
		StringBuilder query = new StringBuilder();
		query.append("(SELECT DISTINCT vista_subject_code AS \"SUBJECT_ID\" FROM")
				.append(" (SELECT")
				.append(" CONNECT_BY_ROOT hie.subject_id ROOT_ID,")
				.append(" hie.subject_id,")
				.append(" hie.subject_name,")
				.append(" hie.subject_level,")
				.append(" hie.parent_id,")
				.append(" hie.vista_subject_code,")
				.append(" hie.final_level,")
				.append(" hie.title_count,")
				.append(" hie.active_subject,")
				.append(" isbn.isbn,")
				.append(" eb.series_code")
			.append(" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn")
			.append(" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook eb")
			.append(" ON eb.isbn = isbn.isbn")
			.append(" CONNECT BY PRIOR hie.subject_id = hie.parent_id")
			.append(" START WITH hie.parent_id = ?1)")
			.append(" WHERE vista_subject_code IS NOT NULL AND isbn IS NOT NULL)")
			.append(" ORDER BY vista_subject_code");
		
		return query(new EBookSubjectHierarchy(), query.toString(), parentId);
	}
}
