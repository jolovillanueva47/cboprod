package org.cambridge.ebooks.online.feeds.properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;


public class FeedsProperties {

	private Configuration config;
	
	private static final String propertyFile = System.getProperty("property.file"); 
	private static final FeedsProperties APP_PROPERTIES = new FeedsProperties();
	
	private FeedsProperties(){
		try 
		{
			config = new PropertiesConfiguration(propertyFile + ".properties");
		} 
		catch (ConfigurationException e) 
		{
			System.err.println("[ConfigurationException] FeedsProperties() message: " + e.getMessage());
		}
	}
	
	
	public static final String JDBC_URL = "javax.persistence.jdbc.url";
	public static final String JDBC_URL_VALUE = APP_PROPERTIES.config.getString("jdbc.url");
	
	public static final String CHANNEL_TITLE = APP_PROPERTIES.config.getString("channel.title");
	public static final String CONTEXT_PATH	 = APP_PROPERTIES.config.getString("ebooks.context.path");
	public static final String FEEDS_DIR	 = APP_PROPERTIES.config.getString("feeds.rss");
	
	public static final String SOLR_HOME_URL = APP_PROPERTIES.config.getString("solr.home.url");
	public static final String CORE_BOOK = APP_PROPERTIES.config.getString("core.book");
	public static final String CORE_CONTENT = APP_PROPERTIES.config.getString("core.content");
	public static final String SOCKET_TIMEOUT = APP_PROPERTIES.config.getString("socket.timeout");
	public static final String CONNECTION_TIMEOUT = APP_PROPERTIES.config.getString("connection.timeout");
	public static final String MAX_DEFAULT_CONNECTION = APP_PROPERTIES.config.getString("default.max.connection.per.host");
	public static final String MAX_TOTAL_CONNECTION = APP_PROPERTIES.config.getString("max.total.connections");
	public static final String FOLLOW_REDIRECTS = APP_PROPERTIES.config.getString("follow.redirects");
	public static final String ALLOW_COMPRESSION = APP_PROPERTIES.config.getString("allow.compression");
	public static final String MAX_RETRIES = APP_PROPERTIES.config.getString("max.retries");
	public static final String DIR_BLURB = APP_PROPERTIES.config.getString("abstract.dir");
	
	
}
