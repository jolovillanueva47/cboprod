package org.cambridge.ebooks.online.feeds.solr.util;


import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.feeds.properties.FeedsProperties;


public class SOLRSearchWorker{
		
	private static final String URL_SOLR_HOME = FeedsProperties.SOLR_HOME_URL;
	private static final String CORE_BOOK = FeedsProperties.CORE_BOOK;
	private static final String CORE_CHAPTER = FeedsProperties.CORE_CONTENT;
	
	private static final SolrServer bookCore = SOLRServer.getBookCore();	
	
	public SOLRSearchWorker(){}	


	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer) {
		List<T> result = null;
		
		SolrQuery q = generateQuery(searchText);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.err.println("[Exception] SOLRSearchWorker.searchCore(T, String, SolrServer)" + ex.getMessage());
			System.err.println("searchText: " + searchText);
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T searchContentCoreSinglePager(T t, String searchText, SolrServer solrServer, int page) {
		T result = null;
		
		SolrQuery q = generateQuery(searchText);
		onePagerPagination(q, page);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (T)response.getBeans(t.getClass()).get(0);
		}
		catch(Exception ex)
		{
			System.out.println("[Exception] SOLRSearchWorker.searchContentCoreSinglePager(T, String, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCore(T t, String searchText, SolrServer solrServer, int rowcount) {
		List<T> result = null;
		
		SolrQuery q = generateQuery(searchText);
		setQueryRowCount(q, rowcount);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(q, solrServer,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());
		}
		catch(Exception ex)
		{
			System.err.println("[Exception] SOLRSearchWorker.searchContentCoreSinglePager(T, String, SolrServer, int)" + ex.getMessage());
		}
			
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> searchCores(T t, String searchText) {
		List<T> result = null;
		String urlShardsSolrHome = URL_SOLR_HOME.replaceAll("http://", "");
		
		SolrQuery query = generateQuery(searchText);
		query.setRows(1000);
		query.setParam("shards", urlShardsSolrHome + CORE_BOOK + "," + urlShardsSolrHome + CORE_CHAPTER);
		QueryResponse response = null;
		try 
		{
			response = generateResponse(query, bookCore,  METHOD.POST);
			result = (List<T>)response.getBeans(t.getClass());	
		}
		catch(Exception ex)
		{
			System.err.println("[Exception] SOLRSearchWorker.searchCores(T, String) message: " + ex.getMessage());
		}
			
		return result;
	}
	

	public SolrQuery generateQuery(String searchText){
		SolrQuery query = new SolrQuery();
		query.setQuery(searchText);
		query.setRows(1000);
		query.setSortField("online_date", ORDER.desc);
		query.addFilterQuery("-flag:(0 OR 2)");
		return query;
	}
	
	
	public QueryResponse generateResponse(SolrQuery q, SolrServer server, SolrRequest.METHOD m){
		QueryResponse response = null;
		try 
		{
			response = server.query(q, m);
		} 
		catch (SolrServerException e) 
		{
			// TODO Auto-generated catch block
			System.err.println("[Exception] SOLRSearchWorker.generateResponse(SolrQuery, SolrServer, SolrRequest.METHOD) message: " + e.getMessage());
		}
		
		return response;
	}

	
	private void onePagerPagination(SolrQuery q, int pageNum) {
		q.setRows(1);
		q.setStart(pageNum);
	}
	
	private void setQueryRowCount(SolrQuery q, int rowcount) {
		q.setRows(rowcount);
	}
}
