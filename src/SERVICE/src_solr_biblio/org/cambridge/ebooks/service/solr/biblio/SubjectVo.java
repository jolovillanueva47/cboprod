package org.cambridge.ebooks.service.solr.biblio;

import java.util.ArrayList;
import java.util.List;

public class SubjectVo {
	private List<String> subjectCodeList = new ArrayList<String>();
	private List<String> subjectLevelList = new ArrayList<String>();
	private List<String> subjectList = new ArrayList<String>();
	
	public List<String> getSubjectCodeList() {
		return subjectCodeList;
	}
	public void setSubjectCodeList(List<String> subjectCodeList) {
		this.subjectCodeList = subjectCodeList;
	}
	public List<String> getSubjectLevelList() {
		return subjectLevelList;
	}
	public void setSubjectLevelList(List<String> subjectLevelList) {
		this.subjectLevelList = subjectLevelList;
	}
	public List<String> getSubjectList() {
		return subjectList;
	}
	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}
}
