package org.cambridge.ebooks.service.solr.biblio;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.cambridge.ebooks.service.solr.constant.ApplicationProperties;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;

public class SolrBiblioUtil{

	String updateType;
	
	public static Date processTargetDateFromCmdLineArg(String args[]) {
		java.sql.Date targetDate = null; 
		String dateNow = strDateNow();
		
		if (args.length == 0){
			System.out.println("NO CMD LINE ARGUMENTS PROVIDED, using default value [dateNow]: " + dateNow);
			targetDate = convStrDateToSqlDate( dateNow );
			System.out.println("target date: "+ targetDate);
		} else {
			if("ALL".equalsIgnoreCase(args[0])){
				System.out.println("args[0]: " + args[0]);
				targetDate = null;
				System.out.println("targetDate: "+ targetDate);
			} else if("test".equalsIgnoreCase(args[0])) {
				targetDate = convStrDateToSqlDate(args[1]);
				System.out.println("TARGET DATE::::>>>" + targetDate);
			} else {
				targetDate = convStrDateToSqlDate(args[0]);
				System.out.println("TARGET DATE::::>>>" + targetDate);
			}
			if (args.length == 2){
				
			}
		}
		return targetDate;
	}
	
	public static String processTargetCoreFromCmdLineArg(String[] args) {
		String targetCore = null;
		if (args != null && args.length == 2){
			targetCore = args[1];
		} else {
			System.out.println("targetCore: ALL");
		}
		
		return targetCore;
	}
	
	public static Date convStrDateToSqlDate(String targetDate) {
		SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" ) ;
		java.util.Date convDate = null;
		try {
			convDate = sdf.parse(targetDate);
		} catch (ParseException e) {
			ExceptionPrinter.getStackTraceAsString(e);
		}
		java.sql.Date convSQLDate = new java.sql.Date(convDate.getTime());
		System.out.println("convSQLDate: " + convSQLDate );
		
		return convSQLDate;
	}
	
	public static java.util.Date convStrDateToDate(String targetDate) {
		SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" ) ;
		java.util.Date convDate = null;
		try {
			convDate = sdf.parse(targetDate);
		} catch (ParseException e) {
			ExceptionPrinter.getStackTraceAsString(e);
		}
		System.out.println("convDate: " + convDate );
		
		return convDate;
	}
	
	public static String strDateNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = sdf.format( (java.util.Date)Calendar.getInstance().getTime() );
		return dateNow;
	}
	
	public static void displayValidCmdLineArgs() {
		System.out.println("-----------------------------------------------------------");
		System.out.println("------- VALID CMD LINE ARGUMENTS: MM/dd/yyyy or ALL -------");
		System.out.println("------- DEFAULT: [ " + strDateNow() +" ]	                    -------" );
		System.out.println("-----------------------------------------------------------");
	}

	private static void getSubj(String conn, Date targetDate) {
		
		if (targetDate != null){
			System.out.println("getting subjList with provided targetDate: " + targetDate); //either 
		}else{
			System.out.println("UPDATE ALL : targetDate== null!");
		}
	}
	
	private static void getAuth(String conn, Date targetDate) {
		System.out.println("getting authList");
	}
	
	private static void getBook(String conn, Date targetDate) {
		System.out.println("getting bookList");
	}
}

