package org.cambridge.ebooks.service.solr.biblio;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class SolrBiblioProperties {

	private static Configuration config;
	
	static {
		try {				
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + "_b.properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);		
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final String JDBC_DRIVER = config.getString("jdbc.driver");
	public static final String JDBC_URL = config.getString("jdbc.url");
	public static final String JDBC_USERNAME = config.getString("jdbc.user");
	public static final String JDBC_PASSWORD = config.getString("jdbc.password");	
	public static final String FILE_INPUT = config.getString("file.input");
}





