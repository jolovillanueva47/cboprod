package org.cambridge.ebooks.service.solr.biblio;

import java.util.ArrayList;
import java.util.List;

public class AuthorVo {
	private List<String> authorList = new ArrayList<String>();
	private List<String> authorLfList = new ArrayList<String>();
	private List<String> authorRoleList = new ArrayList<String>();
	private List<String> authorAffiliationList = new ArrayList<String>();
	private List<String> authorPositionList = new ArrayList<String>();
	
	private final String DELIMITER = "=DEL=";
	
	public List<String> getAuthorList() {
		return authorList;
	}
	public void setAuthorList(List<String> authorList) {
		this.authorList = authorList;
	}
	public List<String> getAuthorLfList() {
		return authorLfList;
	}
	public void setAuthorLfList(List<String> authorLfList) {
		this.authorLfList = authorLfList;
	}
	public List<String> getAuthorRoleList() {
		return authorRoleList;
	}
	public void setAuthorRoleList(List<String> authorRoleList) {
		this.authorRoleList = authorRoleList;
	}
	public List<String> getAuthorAffiliationList() {
		return authorAffiliationList;
	}
	public void setAuthorAffiliationList(List<String> authorAffiliationList) {
		this.authorAffiliationList = authorAffiliationList;
	}
	public List<String> getAuthorPositionList() {
		return authorPositionList;
	}
	public void setAuthorPositionList(List<String> authorPositionList) {
		this.authorPositionList = authorPositionList;
	}
	public String getAuthorsDelimited(){
		StringBuilder sb = new StringBuilder();
		String strAuthors = null;
		for (String element: authorList){
			sb.append(element);
			sb.append(DELIMITER);
		}
		strAuthors = sb.toString();
		if ( strAuthors.lastIndexOf(DELIMITER) != -1 ){
			strAuthors = strAuthors.substring(0, strAuthors.lastIndexOf(DELIMITER));
		}
		return strAuthors;
	}
	public String getAuthorsLfDelimited(){
		StringBuilder sb = new StringBuilder();
		String strAuthorsLf = null;
		for (String element: authorLfList){
			sb.append(element);
			sb.append(DELIMITER);
		}
		strAuthorsLf = removeLastDelimiter(sb);
//		strAuthorsLf = sb.toString();
//		if ( strAuthorsLf.lastIndexOf(DELIMITER) != -1 ){
//			strAuthorsLf = strAuthorsLf.substring(0, strAuthorsLf.lastIndexOf(DELIMITER));
//		}
		return strAuthorsLf;
	}
	public String getAuthorsRoleDelimited(){
		StringBuilder sb = new StringBuilder();
		String strAuthorsRole = null;
		for (String element: authorRoleList){
			sb.append(element);
			sb.append(DELIMITER);
		}
		strAuthorsRole = sb.toString();
		if ( strAuthorsRole.lastIndexOf(DELIMITER) != -1 ){
			strAuthorsRole = strAuthorsRole.substring(0, strAuthorsRole.lastIndexOf(DELIMITER));
		}
		return strAuthorsRole;
	}
	public String getAuthorsAffiliationDelimited(){
		StringBuilder sb = new StringBuilder();
		String strAuthorsAffiliation = null;
		for (String element: authorAffiliationList){
			sb.append(element);
			sb.append(DELIMITER);
		}
		strAuthorsAffiliation = sb.toString();
		if ( strAuthorsAffiliation.lastIndexOf(DELIMITER) != -1 ){
			strAuthorsAffiliation = strAuthorsAffiliation.substring(0, strAuthorsAffiliation.lastIndexOf(DELIMITER));
		}
		return strAuthorsAffiliation;
	}
	public String getAuthorsPositionDelimited(){
		StringBuilder sb = new StringBuilder();
		String strAuthorsPosition = null;
		for (String element: authorPositionList){
			sb.append(element);
			sb.append(DELIMITER);
		}
		strAuthorsPosition = removeLastDelimiter(sb);
//		strAuthorsPosition = sb.toString();
//		if ( strAuthorsPosition.lastIndexOf(DELIMITER) != -1 ){
//			strAuthorsPosition = strAuthorsPosition.substring(0, strAuthorsPosition.lastIndexOf(DELIMITER));
//		}
		return strAuthorsPosition;
	}
	
	private String removeLastDelimiter(StringBuilder sb){
		String str = null;
		if (null != sb){
			str = sb.toString();
			if ( str.lastIndexOf(DELIMITER) != -1 ){
				str = str.substring(0, str.lastIndexOf(DELIMITER));
			}
		}
		return str;
	}
	
}
