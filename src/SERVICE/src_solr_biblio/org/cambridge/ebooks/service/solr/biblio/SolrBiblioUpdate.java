package org.cambridge.ebooks.service.solr.biblio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.service.solr.EbookSolrIndexer;
import org.cambridge.ebooks.service.solr.Indexer;
import org.cambridge.ebooks.service.solr.bean.BookMetaDataSolrDocument;
import org.cambridge.ebooks.service.solr.bean.SearchSolrDocument;
import org.cambridge.ebooks.service.solr.constant.ApplicationProperties;
import org.cambridge.ebooks.service.solr.server.CupCommonsHttpSolrServer;
import org.cambridge.ebooks.service.solr.server.CupSolrServer;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;
import org.cambridge.ebooks.service.solr.util.JaxbXmlParser;
import org.cambridge.ebooks.service.solr.util.XmlParser;

public class SolrBiblioUpdate {
	
	// start == kmulingtapang: integrate to solr
	private static CupSolrServer server = new CupCommonsHttpSolrServer();
	private static XmlParser xmlParser = new JaxbXmlParser("org.cambridge.ebooks.service.solr.bean.jaxb");	

	private Map<String, AuthorVo> isbnAuthorMap = new HashMap<String, AuthorVo>();
	private Map<String, SubjectVo> isbnSubjectMap = new HashMap<String, SubjectVo>();

	private BookMetaDataSolrDocument bookSolrDocument;
	private List<BookMetaDataSolrDocument> bookSolrDocumentList = new ArrayList<BookMetaDataSolrDocument>();
	
	private SearchSolrDocument searchSolrDocument;
	private List<SearchSolrDocument> searchSolrDocumentList = new ArrayList<SearchSolrDocument>();
	
	
	public static void main(String[] args) {
		System.out.println("\nargs:" + Arrays.toString(args));
		
		Connection conn = SolrBiblioDataSource.getConnection();
		
		try {
			SolrBiblioUpdate solrBiblioUpdate = new SolrBiblioUpdate();
			if(null != args && args.length > 0 && args[0].equals("file")) {
				doInputFromFile(args, conn, solrBiblioUpdate);				
			} else {			
				doInputFromDb(args, conn, solrBiblioUpdate);			
			}
			
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			SolrBiblioDataSource.closeConnection(conn);
		}
	}
	
	private static void doInputFromFile(String[] args, Connection conn, SolrBiblioUpdate solrBiblioUpdate) {		
		File file = new File(SolrBiblioProperties.FILE_INPUT);
		System.out.println("File:" + file.getName());
		
		FileReader fr;
		BufferedReader br;		
		List<String> isbns = new ArrayList<String>();
		
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		
			String line = "";
			while((line = br.readLine()) != null){		
				try {				
					isbns.add(line.trim());
				} catch (Exception e) {
					System.out.println(ExceptionPrinter.getStackTraceAsString(e));
					continue;
				}
			}
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		List<String> whereInClauses = new ArrayList<String>();				
		StringBuilder whereInClause = new StringBuilder();
		int isbnListSize = isbns.size();
		int mainCtr = 0;
		int subCtr = 0;
		for(String isbn : isbns) {
			mainCtr++;
			subCtr++;
			if(mainCtr == isbnListSize) {
				whereInClause.append("'").append(isbn).append("',");
				int lastIndex = whereInClause.toString().lastIndexOf(",");						
				whereInClauses.add(whereInClause.toString().substring(0, lastIndex));
			} if(subCtr > 800) {
				int lastIndex = whereInClause.toString().lastIndexOf(",");						
				whereInClauses.add(whereInClause.toString().substring(0, lastIndex));
				
				// reset
				whereInClause = new StringBuilder();
				subCtr = 1;
				
				whereInClause.append("'").append(isbn).append("',");
			} else {					
				whereInClause.append("'").append(isbn).append("',");
			}					
		}
		
		System.out.println("DATABASE: " + SolrBiblioProperties.JDBC_URL);		

		// start == kmulingtapang: integrate to solr				
		Indexer indexer = new EbookSolrIndexer(server, xmlParser);	
		// end == kmulingtapang: integrate to solr 
		
		for(String clause : whereInClauses) {
			solrBiblioUpdate.getSubjectList(conn, clause);
			solrBiblioUpdate.getAuthorList(conn, clause);
			solrBiblioUpdate.getEbookList(conn, clause);	
			
			String targetCore = SolrBiblioUtil.processTargetCoreFromCmdLineArg(args);
			
			if (targetCore != null && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_BOOK)) {
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_BOOK);
				
			} else if (targetCore != null && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_SEARCH)) {						
				indexer.add(solrBiblioUpdate.getSearchSolrDocumentList(), ApplicationProperties.CORE_SEARCH);
				
			} else if (targetCore != null && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_CHAPTER)) {
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_CHAPTER);
				
			} if (null != targetCore && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_PAGE)){
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_PAGE);
			} else {
				System.out.println("targetCore: ALL");
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_BOOK); // kmulingtapang: index to book core
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_CHAPTER); // kmulingtapang: index to chapter core
				indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_PAGE);
				indexer.add(solrBiblioUpdate.getSearchSolrDocumentList(), ApplicationProperties.CORE_SEARCH); // kmulingtapang: index to search core
			}
		}
	}
	
	private static void doInputFromDb(String[] args, Connection conn, SolrBiblioUpdate solrBiblioUpdate) {
		System.out.println("DATABASE: " + SolrBiblioProperties.JDBC_URL);
		String targetCore = SolrBiblioUtil.processTargetCoreFromCmdLineArg(args);

		
		solrBiblioUpdate.getSubjectList(conn, args);
		solrBiblioUpdate.getAuthorList(conn, args);
		solrBiblioUpdate.getEbookList(conn, args);			
		
		// start == kmulingtapang: integrate to solr				
		Indexer indexer = new EbookSolrIndexer(server, xmlParser);	
		// end == kmulingtapang: integrate to solr 
		
		if (null != targetCore && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_BOOK)){
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_BOOK);
		} else if (null != targetCore && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_SEARCH)){
			indexer.add(solrBiblioUpdate.getSearchSolrDocumentList(), ApplicationProperties.CORE_SEARCH);
		} else if (null != targetCore && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_CHAPTER)){
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_CHAPTER);
		} if (null != targetCore && targetCore.equalsIgnoreCase(ApplicationProperties.CORE_PAGE)){
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_PAGE);
		} else {
			System.out.println("targetCore: ALL");
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_BOOK); // kmulingtapang: index to book core
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_CHAPTER); // kmulingtapang: index to chapter core
			indexer.add(solrBiblioUpdate.getBookSolrDocumentList(), ApplicationProperties.CORE_PAGE);
			indexer.add(solrBiblioUpdate.getSearchSolrDocumentList(), ApplicationProperties.CORE_SEARCH); // kmulingtapang: index to search core
		}
	}

	private void getSubjectList(Connection conn, String[] args){
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;
		
		String pstSqlAll =	"SELECT eidl.isbn, s.sequence_number, sh.vista_subject_code, s.subject_id, sh.subject_level, sh.subject_name " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_subject_isbn s, oraebooks.ebook_subject_hierarchy sh " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = s.isbn and s.subject_id = sh.subject_id and eb.status = '7' " +
								"and eidl.isbn in (" +
							      "SELECT eidl.isbn FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl WHERE eidl.isbn = eb.isbn and eb.status = '7' " +
								") " +
							"order by isbn, sequence_number, subject_level, subject_id";
		String pstSqlNow =	"SELECT eidl.isbn, s.sequence_number, sh.vista_subject_code, s.subject_id, sh.subject_level, sh.subject_name " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_subject_isbn s, oraebooks.ebook_subject_hierarchy sh " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = s.isbn and s.subject_id = sh.subject_id and eb.status = '7' " +
								"and eidl.isbn in (" +
							      "SELECT eidl.isbn " +
							      "FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, (select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
							      "WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7' " +
							      "and (  trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(sysdate-1) = trunc(ESAMPLE_CHANGE_DATE) or trunc(sysdate-1) = trunc(ateb.AUDIT_DATE) ) " +
								") " +
							"order by isbn, sequence_number, subject_level, subject_id";
		
		
		String pstSqlTargetDate = "SELECT eidl.isbn, s.sequence_number, sh.vista_subject_code, s.subject_id, sh.subject_level, sh.subject_name " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_subject_isbn s, oraebooks.ebook_subject_hierarchy sh " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = s.isbn and s.subject_id = sh.subject_id and eb.status = '7' " +
								"and eidl.isbn in (" +
							      "SELECT eidl.isbn " +
							      "FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, (select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
							      "WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7' " +
							      "and (  trunc(?) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(?) = trunc(ESAMPLE_CHANGE_DATE) or trunc(?) = trunc(ateb.AUDIT_DATE) ) " +
								") " +
							"order by isbn, sequence_number, subject_level, subject_id";
		
		SubjectVo vo = null;
		String isbn = null;
		String subjId = null;
		String subject = null;
		String subjectLevel = null;

		try {
			
			if (null != args && args.length > 0 && args[0].equals("daily")){ 
				pStatement = conn.prepareStatement(pstSqlNow);
			} else if(null != args && args.length > 0 && args[0].equals("test")) {
				Date targetDate = SolrBiblioUtil.convStrDateToSqlDate(args[1]);
				pStatement = conn.prepareStatement(pstSqlTargetDate);
				pStatement.setDate(1, targetDate);
				pStatement.setDate(2, targetDate);
				pStatement.setDate(3, targetDate);
			} else { //select all entries
				pStatement = conn.prepareStatement(pstSqlAll);
			}
			
			rs =  pStatement.executeQuery();
			System.out.println("==========================        SubjectList         ==========================");
			System.out.println("processing result set...");
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
//				subjId = rs.getString("SUBJECT_ID");
				subjId = rs.getString("VISTA_SUBJECT_CODE");
				subject = rs.getString("SUBJECT_NAME");
				subjectLevel = rs.getString("SUBJECT_LEVEL");
//				System.out.println("isbn: " + isbn + " subjLvl: " + subjectLevel + " subjId: " + subjId + ":\t" +subject);
				if(isbnSubjectMap.containsKey(isbn)){
					isbnSubjectMap.get(isbn).getSubjectLevelList().add(subjectLevel);
					isbnSubjectMap.get(isbn).getSubjectCodeList().add(subjId);
					isbnSubjectMap.get(isbn).getSubjectList().add(subject);
				}else{
					vo = new SubjectVo();
					if(subjId != null && rs.getString("SUBJECT_NAME")!= null){
						vo.getSubjectCodeList().add(subjId);
						vo.getSubjectList().add(subject);
						vo.getSubjectLevelList().add(subjectLevel);
					}else{
						System.out.println(isbn + "SUBJECT_ID/SUBJECT_NAME is null");
					}
					isbnSubjectMap.put(isbn, vo);
				}
			}
			System.out.println("subjectList processed!");
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			try {
				rs.close();
				System.out.println("getSubjectList() successful");
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	private void getSubjectList(Connection conn, String whereInClause){		
		isbnSubjectMap = new HashMap<String, SubjectVo>();
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;
		
		String pstSqlAll =	"SELECT eidl.isbn, s.sequence_number, sh.vista_subject_code, s.subject_id, sh.subject_level, sh.subject_name " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_subject_isbn s, oraebooks.ebook_subject_hierarchy sh " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = s.isbn and s.subject_id = sh.subject_id and eb.status = '7' " +
								"and eidl.isbn in (" + whereInClause + ") " +
							"order by isbn, sequence_number, subject_level, subject_id";
		
		SubjectVo vo = null;
		String isbn = null;
		String subjId = null;
		String subject = null;
		String subjectLevel = null;

		try {
			
			pStatement = conn.prepareStatement(pstSqlAll);
			
			rs =  pStatement.executeQuery();
			System.out.println("==========================        SubjectList         ==========================");
			System.out.println("processing result set...");
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
				subjId = rs.getString("VISTA_SUBJECT_CODE");
				subject = rs.getString("SUBJECT_NAME");
				subjectLevel = rs.getString("SUBJECT_LEVEL");
				if(isbnSubjectMap.containsKey(isbn)){
					isbnSubjectMap.get(isbn).getSubjectLevelList().add(subjectLevel);
					isbnSubjectMap.get(isbn).getSubjectCodeList().add(subjId);
					isbnSubjectMap.get(isbn).getSubjectList().add(subject);
				} else {
					vo = new SubjectVo();
					if(subjId != null && rs.getString("SUBJECT_NAME")!= null){
						vo.getSubjectCodeList().add(subjId);
						vo.getSubjectList().add(subject);
						vo.getSubjectLevelList().add(subjectLevel);
					} else {
						System.out.println(isbn + "SUBJECT_ID/SUBJECT_NAME is null");
					}
					isbnSubjectMap.put(isbn, vo);
				}
			}
			System.out.println("subjectList processed!");
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			try {
				rs.close();
				System.out.println("getSubjectList() successful");
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	private void getAuthorList(Connection conn, String[] args){
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;
		
		String pstSqlAll =	"SELECT eidl.isbn, eidl.online_flag, eidl.online_flag_change_date, eb.status, a.sequence_number, nvl(a.author_role, 'none') as author_role, a.full_name, a.forename, a.surname, a.surname || ', ' ||a.forename AS author_lf, nvl(a.affiliation, 'none') as affiliation " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_isbn_author a " +
							"WHERE eidl.isbn(+) = eb.isbn and a.isbn = eidl.isbn and eb.status = '7' " +
							  "and eidl.isbn in (" +
							      "SELECT eidl.isbn FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl WHERE eidl.isbn = eb.isbn and eb.status = '7' " +
							  ") " +
							"ORDER BY isbn, sequence_number";
		
		
		String pstSqlNow =	"SELECT eidl.isbn, eidl.online_flag, eidl.online_flag_change_date, eb.status, a.sequence_number, nvl(a.author_role, 'none') as author_role, a.full_name, a.forename, a.surname, a.surname || ', ' ||a.forename AS author_lf, nvl(a.affiliation, 'none') as affiliation " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_isbn_author a " +
							"WHERE eidl.isbn(+) = eb.isbn and a.isbn = eidl.isbn and eb.status = '7' " +
							  "and eidl.isbn in (" +
							      "SELECT eidl.isbn " +
							      "FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, (select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
							      "WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7' " +
							        "and (  trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(sysdate-1) = trunc(ESAMPLE_CHANGE_DATE) or trunc(sysdate-1) = trunc(ateb.AUDIT_DATE) ) " +
							  ") " +
							"ORDER BY isbn, sequence_number";
		
		
		String pstSqlTargetDate = "SELECT eidl.isbn, eidl.online_flag, eidl.online_flag_change_date, eb.status, a.sequence_number, nvl(a.author_role, 'none') as author_role, a.full_name, a.forename, a.surname, a.surname || ', ' ||a.forename AS author_lf, nvl(a.affiliation, 'none') as affiliation " +
								"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_isbn_author a " +
								"WHERE eidl.isbn(+) = eb.isbn and a.isbn = eidl.isbn and eb.status = '7' " +
								  "and eidl.isbn in (" +
								      "SELECT eidl.isbn " +
								      "FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, (select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
								      "WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7' " +
								        "and (  trunc(?) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(?) = trunc(ESAMPLE_CHANGE_DATE) or trunc(?) = trunc(ateb.AUDIT_DATE) ) " +
								  ") " +
								"ORDER BY isbn, sequence_number";

		AuthorVo vo = null;
		String isbn = null;
		String authorPosition = null;
		String author = null;
		String authorLf = null;
		String authorRole= null;
		String authorAffiliation= null;

		try{
			
			if (null != args && args.length > 0 && args[0].equals("daily")){ 
				pStatement = conn.prepareStatement(pstSqlNow);
			} else if(null != args && args.length > 0 && args[0].equals("test")) {
				Date targetDate = SolrBiblioUtil.convStrDateToSqlDate(args[1]);
				pStatement = conn.prepareStatement(pstSqlTargetDate);
				pStatement.setDate(1, targetDate);
				pStatement.setDate(2, targetDate);
				pStatement.setDate(3, targetDate);
			} else { //select all entries
				pStatement = conn.prepareStatement(pstSqlAll);
			}
			
			rs =  pStatement.executeQuery();
			
			System.out.println("==========================        AuthorList          ==========================");
			System.out.println("processing result set...");
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
				authorPosition = rs.getString("SEQUENCE_NUMBER");
				author = rs.getString("FULL_NAME");
				authorLf = rs.getString("AUTHOR_LF");
				authorRole = rs.getString("AUTHOR_ROLE");
				authorAffiliation = rs.getString("AFFILIATION");				
				
//				System.out.println("isbn: " + isbn + " seq#: " + authorPosition + " role: " + authorRole + "\t:"+ authorLf);
				if(isbnAuthorMap.containsKey(isbn)){
					isbnAuthorMap.get(isbn).getAuthorList().add(author);
					isbnAuthorMap.get(isbn).getAuthorLfList().add(authorLf);
					isbnAuthorMap.get(isbn).getAuthorRoleList().add(authorRole);
					isbnAuthorMap.get(isbn).getAuthorAffiliationList().add(authorAffiliation);
					isbnAuthorMap.get(isbn).getAuthorPositionList().add(authorPosition);
				}else{
					vo = new AuthorVo();
					if (authorPosition != null){
						vo.getAuthorList().add(author);
						vo.getAuthorLfList().add(authorLf);
						vo.getAuthorPositionList().add(authorPosition);
						vo.getAuthorRoleList().add(authorRole); //if (rs==null) then 'none'  (sql)
						vo.getAuthorAffiliationList().add(authorAffiliation); //if (rs==null) then 'none' (sql)
					}					
				}
				isbnAuthorMap.put(isbn, vo);
			}
			System.out.println("authorList processed!");
			
		}catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}finally{
			try {
				rs.close();
				System.out.println("getAuthorList() successful");
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}		
	}
	
	private void getAuthorList(Connection conn, String whereInClause){
		isbnAuthorMap = new HashMap<String, AuthorVo>();
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;
		
		String pstSqlAll =	"SELECT eidl.isbn, eidl.online_flag, eidl.online_flag_change_date, eb.status, a.sequence_number, nvl(a.author_role, 'none') as author_role, a.full_name, a.forename, a.surname, a.surname || ', ' ||a.forename AS author_lf, nvl(a.affiliation, 'none') as affiliation " +
							"FROM oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook eb, oraebooks.ebook_isbn_author a " +
							"WHERE eidl.isbn(+) = eb.isbn and a.isbn = eidl.isbn and eb.status = '7' " +
							  "and eidl.isbn in (" + whereInClause + ") " +
							"ORDER BY isbn, sequence_number";
		

		AuthorVo vo = null;
		String isbn = null;
		String authorPosition = null;
		String author = null;
		String authorLf = null;
		String authorRole= null;
		String authorAffiliation= null;

		try{
			pStatement = conn.prepareStatement(pstSqlAll);
			rs =  pStatement.executeQuery();
			
			System.out.println("==========================        AuthorList          ==========================");
			System.out.println("processing result set...");
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
				authorPosition = rs.getString("SEQUENCE_NUMBER");
				author = rs.getString("FULL_NAME");
				authorLf = rs.getString("AUTHOR_LF");
				authorRole = rs.getString("AUTHOR_ROLE");
				authorAffiliation = rs.getString("AFFILIATION");
				
				if(isbnAuthorMap.containsKey(isbn)){
					isbnAuthorMap.get(isbn).getAuthorList().add(author);
					isbnAuthorMap.get(isbn).getAuthorLfList().add(authorLf);
					isbnAuthorMap.get(isbn).getAuthorRoleList().add(authorRole);
					isbnAuthorMap.get(isbn).getAuthorAffiliationList().add(authorAffiliation);
					isbnAuthorMap.get(isbn).getAuthorPositionList().add(authorPosition);
				}else{
					vo = new AuthorVo();
					if (authorPosition != null){
						vo.getAuthorList().add(author);
						vo.getAuthorLfList().add(authorLf);
						vo.getAuthorPositionList().add(authorPosition);
						vo.getAuthorRoleList().add(authorRole); //if (rs==null) then 'none'  (sql)
						vo.getAuthorAffiliationList().add(authorAffiliation); //if (rs==null) then 'none' (sql)
					}					
				}
				isbnAuthorMap.put(isbn, vo);
			}
			System.out.println("authorList processed!");
			
		}catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}finally{
			try {
				rs.close();
				System.out.println("getAuthorList() successful");
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}		
	}	

	private void getEbookList(Connection conn, String[] args){
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;

		String pstSqlAll =	"SELECT eidl.isbn, eidl.online_flag, eidl.sales_module_flag, eidl.online_flag_change_date, eb.status, " +
								"eidl.TITLE, eidl.SUBTITLE,  eidl.SERIES_CODE, eidl.SERIES_NUMBER, eidl.VOLUME_NUMBER, eidl.VOLUME_TITLE,  eidl.PART_NUMBER, eidl.PART_TITLE, eidl.EDITION_NUMBER, TO_CHAR(eidl.PUB_DATE_PRINT, 'YYYY') AS PUB_DATE_PRINT, eidl.ISBN_PRINT_HB, eidl.ISBN_PRINT_PB, eidl.AUTHOR_SINGLELINE, " +
								"S.SERIES_LEGEND, eb.BOOK_ID, COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE, DECODE(ep.PUBLISHER_ID, null, '1168', ep.PUBLISHER_ID) AS PUBLISHER_ID " +
							"FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook_series_load s, oraebooks.ebook_publisher_id_isbn ep " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = ep.isbn (+) and eidl.series_code = s.series_code(+) and eb.status = '7' " +
							"ORDER BY ISBN";
		
		String pstSqlNow =	"SELECT eidl.isbn, eidl.online_flag, eidl.sales_module_flag, eidl.online_flag_change_date, eb.status, " +
								"eidl.TITLE, eidl.SUBTITLE,  eidl.SERIES_CODE, eidl.SERIES_NUMBER, eidl.VOLUME_NUMBER, eidl.VOLUME_TITLE,  eidl.PART_NUMBER, eidl.PART_TITLE, eidl.EDITION_NUMBER, TO_CHAR(eidl.PUB_DATE_PRINT, 'YYYY') AS PUB_DATE_PRINT, eidl.ISBN_PRINT_HB, eidl.ISBN_PRINT_PB, eidl.AUTHOR_SINGLELINE, " +
								"S.SERIES_LEGEND, eb.BOOK_ID, COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE, DECODE(ep.PUBLISHER_ID, null, '1168', ep.PUBLISHER_ID) AS PUBLISHER_ID " +
							"FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook_series_load s, oraebooks.ebook_publisher_id_isbn ep, " +
								"(select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
							"WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eidl.isbn = ep.isbn (+) and eidl.series_code = s.series_code(+) and eb.status = '7' " +
								"and (trunc(sysdate-1) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(sysdate-1) = trunc(ESAMPLE_CHANGE_DATE) or trunc(sysdate-1) = trunc(ateb.AUDIT_DATE)) " +
							"ORDER BY ISBN";
		
		String pstSqlTargetDate = "SELECT eidl.isbn, eidl.online_flag, eidl.sales_module_flag, eidl.online_flag_change_date, eb.status, " +
								"eidl.TITLE, eidl.SUBTITLE,  eidl.SERIES_CODE, eidl.SERIES_NUMBER, eidl.VOLUME_NUMBER, eidl.VOLUME_TITLE,  eidl.PART_NUMBER, eidl.PART_TITLE, eidl.EDITION_NUMBER, TO_CHAR(eidl.PUB_DATE_PRINT, 'YYYY') AS PUB_DATE_PRINT, eidl.ISBN_PRINT_HB, eidl.ISBN_PRINT_PB, eidl.AUTHOR_SINGLELINE, " +
								"S.SERIES_LEGEND, eb.BOOK_ID, COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE, DECODE(ep.PUBLISHER_ID, null, '1168', ep.PUBLISHER_ID) AS PUBLISHER_ID " +
							"FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook_series_load s, oraebooks.ebook_publisher_id_isbn ep, " +
								"(select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " +
							"WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eidl.isbn = ep.isbn (+) and eidl.series_code = s.series_code(+) and eb.status = '7' " +
								"and (trunc(?) = trunc(ONLINE_FLAG_CHANGE_DATE) or trunc(?) = trunc(ESAMPLE_CHANGE_DATE) or trunc(?) = trunc(ateb.AUDIT_DATE)) " +
							"ORDER BY ISBN";
		String isbn = null;
		String bookId = null;
		String onlineFlag = null;
		String salesModuleFlag = null;
		String onlineDate = null;
		String title = null;
		String subtitle = null;
		String seriesCode = null;
		String series = null;
//		String seriesNumber = null;
		String seriesNumberStr = null;
		int seriesNumberInt = 0;
		String volumeNumber = null;
		String volumeTitle = null;
		String partNumber = null;
		String partTitle = null;
		String editionNumber = null;
		String pubPrintDate = null;
		String isbnPrintHb = null;
		String isbnPrintPb = null;
		String authorSingleLine = null;
		String publisherId = null;
		
		try {
			if (null != args && args.length > 0 && args[0].equals("daily")){ 
				pStatement = conn.prepareStatement(pstSqlNow);
			} else if(null != args && args.length > 0 && args[0].equals("test")) {
				Date targetDate = SolrBiblioUtil.convStrDateToSqlDate(args[1]);
				pStatement = conn.prepareStatement(pstSqlTargetDate);
				pStatement.setDate(1, targetDate);
				pStatement.setDate(2, targetDate);
				pStatement.setDate(3, targetDate);
			} else { //select all entries
				pStatement = conn.prepareStatement(pstSqlAll);
			}
			rs =  pStatement.executeQuery();
			
			System.out.println("==========================        BookList         ==========================");
			System.out.println("processing result set...");
			int ctr=0;
			
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
				onlineFlag = rs.getString("ONLINE_FLAG");
				salesModuleFlag = rs.getString("SALES_MODULE_FLAG");
				title = rs.getString("TITLE");
				subtitle = rs.getString("SUBTITLE");
				seriesCode = rs.getString("SERIES_CODE");
				series = rs.getString("SERIES_LEGEND");
				seriesNumberInt = rs.getInt("SERIES_NUMBER");
				seriesNumberStr = rs.getString("SERIES_NUMBER");
				volumeNumber = rs.getString("VOLUME_NUMBER");	// decode(eidl.VOLUME_NUMBER, '0', null , eidl.VOLUME_NUMBER) AS VOLUME_NUMBER
				volumeTitle = rs.getString("VOLUME_TITLE");
				partNumber = rs.getString("PART_NUMBER");		//decode(eidl.PART_NUMBER, '0', null , eidl.PART_NUMBER) AS PART_NUMBER
				partTitle = rs.getString("PART_TITLE");
				editionNumber = rs.getString("EDITION_NUMBER");
				pubPrintDate = rs.getString("PUB_DATE_PRINT");
				isbnPrintHb = rs.getString("ISBN_PRINT_HB");	//can be null
				isbnPrintPb = rs.getString("ISBN_PRINT_PB");	// can be null
				bookId = rs.getString("BOOK_ID");
				onlineDate = rs.getString("MODIFIED_DATE");		//COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE 
				publisherId = rs.getString("PUBLISHER_ID");
				authorSingleLine = rs.getString("AUTHOR_SINGLELINE");
				
				if ("0".equals(volumeNumber)){		// decode(eidl.VOLUME_NUMBER, '0', null , eidl.VOLUME_NUMBER) AS VOLUME_NUMBER
					volumeNumber="";
				}
				if ("0".equals(partNumber)){		// decode(eidl.PART_NUMBER, '0', null , eidl.PART_NUMBER) AS PART_NUMBER
					partNumber="";
				}
				
//				System.out.println("BOOK_ID\t: " + bookId);
//				System.out.println("ISBN\t: " + isbn);
//				System.out.println("TITLE\t: " + rs.getString("TITLE"));
//				System.out.println("MODIFIED_DATE as ONLINE_DATE: " + onlineDate);
//				System.out.println("PUB ID: " + publisherId);
				
			//Create a bookSolrDocument
				bookSolrDocument = new BookMetaDataSolrDocument();
				
				bookSolrDocument.setId(bookId);
				bookSolrDocument.setBookId(bookId);
				bookSolrDocument.setOnlineDate(onlineDate);
				bookSolrDocument.setFlag(getFlag(onlineFlag, salesModuleFlag));
				bookSolrDocument.setIsbn(isbn);
				bookSolrDocument.setSeriesCode(seriesCode);
				bookSolrDocument.setSeries(series);
				bookSolrDocument.setSeriesNumber(seriesNumberInt);
				bookSolrDocument.setTitle(title);
				bookSolrDocument.setSubtitle(subtitle);
				bookSolrDocument.setVolumeNumber(volumeNumber);
				bookSolrDocument.setVolumeTitle(volumeTitle);
				bookSolrDocument.setPartNumber(partNumber);
				bookSolrDocument.setPartTitle(partTitle);
				bookSolrDocument.setEditionNumber(editionNumber);
				bookSolrDocument.setPrintDate(pubPrintDate);
				bookSolrDocument.setAltIsbnHardback(isbnPrintHb);
				bookSolrDocument.setAltIsbnPaperback(isbnPrintPb);
				bookSolrDocument.setPublisherId(publisherId);
				bookSolrDocument.setAuthorSingleline(authorSingleLine);
				
				//Create a searchSolrDocument
				searchSolrDocument = new SearchSolrDocument();

				searchSolrDocument.setBookId(bookId);
				searchSolrDocument.setOnlineDate(onlineDate);
				searchSolrDocument.setFlag(getFlag(onlineFlag, salesModuleFlag));
				searchSolrDocument.setIsbnIssn(isbn);
				searchSolrDocument.setSeriesCode(seriesCode);
				searchSolrDocument.setSeries(series);
				searchSolrDocument.setSeriesNumber(seriesNumberStr);
				searchSolrDocument.setMainTitle(title);
				searchSolrDocument.setMainSubtitle(subtitle);
				searchSolrDocument.setVolumeNumber(volumeNumber);
				searchSolrDocument.setVolumeTitle(volumeTitle);
				searchSolrDocument.setPartNumber(partNumber);
				searchSolrDocument.setPartTitle(partTitle);
				searchSolrDocument.setEditionNumber(editionNumber);
				searchSolrDocument.setPrintDate(pubPrintDate);
				searchSolrDocument.setAltIsbnHardback(isbnPrintHb);
				searchSolrDocument.setAltIsbnPaperback(isbnPrintPb);
				searchSolrDocument.setPublisherId(publisherId);
				searchSolrDocument.setAuthorSingleline(authorSingleLine);

				//AUTHORS
				setAuthorSolrDocuments(isbn);
				
				//SUBJECTS
				setSubjectSolrDocuments(isbn);
			
				bookSolrDocumentList.add(bookSolrDocument);
				searchSolrDocumentList.add(searchSolrDocument);
				ctr++;
//				System.out.println("--------------------------");
			}
			System.out.println("bookSolrDocument ctr\t\t: " +ctr);
			System.out.println("bookSolrDocumentList.size()\t: " + bookSolrDocumentList.size());
			System.out.println("searchSolrDocumentList.size()\t: " + searchSolrDocumentList.size());
			
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			try {
				rs.close();
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	private void getEbookList(Connection conn, String whereInClause){
		bookSolrDocumentList = new ArrayList<BookMetaDataSolrDocument>();
		searchSolrDocumentList = new ArrayList<SearchSolrDocument>();
		
		PreparedStatement pStatement = null;
		ResultSet rs = null;
		
		String pstSqlAll =	"SELECT eidl.isbn, eidl.online_flag, eidl.sales_module_flag, eidl.online_flag_change_date, eb.status, " +
								"eidl.TITLE, eidl.SUBTITLE,  eidl.SERIES_CODE, eidl.SERIES_NUMBER, eidl.VOLUME_NUMBER, eidl.VOLUME_TITLE,  eidl.PART_NUMBER, eidl.PART_TITLE, eidl.EDITION_NUMBER, TO_CHAR(eidl.PUB_DATE_PRINT, 'YYYY') AS PUB_DATE_PRINT, eidl.ISBN_PRINT_HB, eidl.ISBN_PRINT_PB, eidl.AUTHOR_SINGLELINE, " +
								"S.SERIES_LEGEND, eb.BOOK_ID, COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE, DECODE(ep.PUBLISHER_ID, null, '1168', ep.PUBLISHER_ID) AS PUBLISHER_ID " +
							"FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, oraebooks.ebook_series_load s, oraebooks.ebook_publisher_id_isbn ep " +
							"WHERE eidl.isbn = eb.isbn and eidl.isbn = ep.isbn (+) and eidl.series_code = s.series_code(+) and eb.status = '7' " +
								"and eidl.isbn in (" + whereInClause + ") " +
							"ORDER BY ISBN";
		
		String isbn = null;
		String bookId = null;
		String onlineFlag = null;
		String salesModuleFlag = null;
		String onlineDate = null;
		String title = null;
		String subtitle = null;
		String seriesCode = null;
		String series = null;
//		String seriesNumber = null;
		String seriesNumberStr = null;
		int seriesNumberInt = 0;
		String volumeNumber = null;
		String volumeTitle = null;
		String partNumber = null;
		String partTitle = null;
		String editionNumber = null;
		String pubPrintDate = null;
		String isbnPrintHb = null;
		String isbnPrintPb = null;
		String authorSingleLine = null;
		String publisherId = null;
		
		try {			
			pStatement = conn.prepareStatement(pstSqlAll);
			rs =  pStatement.executeQuery();
			
			System.out.println("==========================        BookList         ==========================");
			System.out.println("processing result set...");
			int ctr=0;
			
			while (rs != null && rs.next()){
				isbn = rs.getString("ISBN");
				onlineFlag = rs.getString("ONLINE_FLAG");
				salesModuleFlag = rs.getString("SALES_MODULE_FLAG");
				title = rs.getString("TITLE");
				subtitle = rs.getString("SUBTITLE");
				seriesCode = rs.getString("SERIES_CODE");
				series = rs.getString("SERIES_LEGEND");
				seriesNumberInt = rs.getInt("SERIES_NUMBER");
				seriesNumberStr = rs.getString("SERIES_NUMBER");
				volumeNumber = rs.getString("VOLUME_NUMBER");	// decode(eidl.VOLUME_NUMBER, '0', null , eidl.VOLUME_NUMBER) AS VOLUME_NUMBER
				volumeTitle = rs.getString("VOLUME_TITLE");
				partNumber = rs.getString("PART_NUMBER");		//decode(eidl.PART_NUMBER, '0', null , eidl.PART_NUMBER) AS PART_NUMBER
				partTitle = rs.getString("PART_TITLE");
				editionNumber = rs.getString("EDITION_NUMBER");
				pubPrintDate = rs.getString("PUB_DATE_PRINT");
				isbnPrintHb = rs.getString("ISBN_PRINT_HB");	//can be null
				isbnPrintPb = rs.getString("ISBN_PRINT_PB");	// can be null
				bookId = rs.getString("BOOK_ID");
				onlineDate = rs.getString("MODIFIED_DATE");		//COALESCE( TO_CHAR(eidl.PUB_DATE_ONLINE, 'ddMonYYYY'), TO_CHAR(eb.modified_date, 'ddMonYYYY') ) as MODIFIED_DATE 
				publisherId = rs.getString("PUBLISHER_ID");
				authorSingleLine = rs.getString("AUTHOR_SINGLELINE");
				
				if ("0".equals(volumeNumber)){		// decode(eidl.VOLUME_NUMBER, '0', null , eidl.VOLUME_NUMBER) AS VOLUME_NUMBER
					volumeNumber="";
				}
				if ("0".equals(partNumber)){		// decode(eidl.PART_NUMBER, '0', null , eidl.PART_NUMBER) AS PART_NUMBER
					partNumber="";
				}
				
//				System.out.println("BOOK_ID\t: " + bookId);
//				System.out.println("ISBN\t: " + isbn);
//				System.out.println("TITLE\t: " + rs.getString("TITLE"));
//				System.out.println("MODIFIED_DATE as ONLINE_DATE: " + onlineDate);
//				System.out.println("PUB ID: " + publisherId);
				
			//Create a bookSolrDocument
				bookSolrDocument = new BookMetaDataSolrDocument();
				
				bookSolrDocument.setId(bookId);
				bookSolrDocument.setBookId(bookId);
				bookSolrDocument.setOnlineDate(onlineDate);
				bookSolrDocument.setFlag(getFlag(onlineFlag, salesModuleFlag));
				bookSolrDocument.setIsbn(isbn);
				bookSolrDocument.setSeriesCode(seriesCode);
				bookSolrDocument.setSeries(series);
				bookSolrDocument.setSeriesNumber(seriesNumberInt);
				bookSolrDocument.setTitle(title);
				bookSolrDocument.setSubtitle(subtitle);
				bookSolrDocument.setVolumeNumber(volumeNumber);
				bookSolrDocument.setVolumeTitle(volumeTitle);
				bookSolrDocument.setPartNumber(partNumber);
				bookSolrDocument.setPartTitle(partTitle);
				bookSolrDocument.setEditionNumber(editionNumber);
				bookSolrDocument.setPrintDate(pubPrintDate);
				bookSolrDocument.setAltIsbnHardback(isbnPrintHb);
				bookSolrDocument.setAltIsbnPaperback(isbnPrintPb);
				bookSolrDocument.setPublisherId(publisherId);
				bookSolrDocument.setAuthorSingleline(authorSingleLine);
				
				//Create a searchSolrDocument
				searchSolrDocument = new SearchSolrDocument();

				searchSolrDocument.setBookId(bookId);
				searchSolrDocument.setOnlineDate(onlineDate);
				searchSolrDocument.setFlag(getFlag(onlineFlag, salesModuleFlag));
				searchSolrDocument.setIsbnIssn(isbn);
				searchSolrDocument.setSeriesCode(seriesCode);
				searchSolrDocument.setSeries(series);
				searchSolrDocument.setSeriesNumber(seriesNumberStr);
				searchSolrDocument.setMainTitle(title);
				searchSolrDocument.setMainSubtitle(subtitle);
				searchSolrDocument.setVolumeNumber(volumeNumber);
				searchSolrDocument.setVolumeTitle(volumeTitle);
				searchSolrDocument.setPartNumber(partNumber);
				searchSolrDocument.setPartTitle(partTitle);
				searchSolrDocument.setEditionNumber(editionNumber);
				searchSolrDocument.setPrintDate(pubPrintDate);
				searchSolrDocument.setAltIsbnHardback(isbnPrintHb);
				searchSolrDocument.setAltIsbnPaperback(isbnPrintPb);
				searchSolrDocument.setPublisherId(publisherId);
				searchSolrDocument.setAuthorSingleline(authorSingleLine);

				//AUTHORS
				setAuthorSolrDocuments(isbn);
				
				//SUBJECTS
				setSubjectSolrDocuments(isbn);
			
				bookSolrDocumentList.add(bookSolrDocument);
				searchSolrDocumentList.add(searchSolrDocument);
				ctr++;
			}
			System.out.println("bookSolrDocument ctr\t\t: " +ctr);
			System.out.println("bookSolrDocumentList.size()\t: " + bookSolrDocumentList.size());
			System.out.println("searchSolrDocumentList.size()\t: " + searchSolrDocumentList.size());
			
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			try {
				rs.close();
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	private void setAuthorSolrDocuments(String isbn) {
		if( isbnAuthorMap.get(isbn) == null ){
//			System.out.println("AUTHOR\t: -=[ NO AUTHOR UPDATES ]=-");
		}else{
			
			if(isbnAuthorMap.get(isbn).getAuthorAffiliationList() != null && isbnAuthorMap.get(isbn).getAuthorAffiliationList().size() != 0){
				bookSolrDocument.setAuthorAffiliationList(isbnAuthorMap.get(isbn).getAuthorAffiliationList());
				searchSolrDocument.setAuthorAffiliation(isbnAuthorMap.get(isbn).getAuthorsAffiliationDelimited()); //delimited
			}
			if(isbnAuthorMap.get(isbn).getAuthorLfList() != null && isbnAuthorMap.get(isbn).getAuthorLfList().size() != 0 ){
				bookSolrDocument.setAuthorNameAlphasort(isbnAuthorMap.get(isbn).getAuthorsLfDelimited());
				bookSolrDocument.setAuthorNameLfList(isbnAuthorMap.get(isbn).getAuthorLfList());				
				searchSolrDocument.setAuthorAlphasort(isbnAuthorMap.get(isbn).getAuthorsLfDelimited());
			}
			if(isbnAuthorMap.get(isbn).getAuthorList() != null && isbnAuthorMap.get(isbn).getAuthorList().size() != 0){
				bookSolrDocument.setAuthorNameList(isbnAuthorMap.get(isbn).getAuthorList());
				searchSolrDocument.setAuthorName(isbnAuthorMap.get(isbn).getAuthorsDelimited());
				searchSolrDocument.setAuthorNameFacetList(isbnAuthorMap.get(isbn).getAuthorList());
			}
			if(isbnAuthorMap.get(isbn).getAuthorPositionList() != null && isbnAuthorMap.get(isbn).getAuthorPositionList().size() != 0){
				bookSolrDocument.setAuthorPositionList(isbnAuthorMap.get(isbn).getAuthorPositionList());				
				searchSolrDocument.setAuthorPosition(isbnAuthorMap.get(isbn).getAuthorsPositionDelimited());
			}
			if(isbnAuthorMap.get(isbn).getAuthorRoleList() != null && isbnAuthorMap.get(isbn).getAuthorRoleList().size() != 0){
				bookSolrDocument.setAuthorRoleList(isbnAuthorMap.get(isbn).getAuthorRoleList());
				searchSolrDocument.setAuthorRole(isbnAuthorMap.get(isbn).getAuthorsRoleDelimited()); //delimited
			}
		}
	}

	private void setSubjectSolrDocuments(String isbn){		
		if(isbnSubjectMap.get(isbn) == null){
//			System.out.println("subject\t: -=[ NO SUBJECT UPDATES ]=-");
		}else{
			if (isbnSubjectMap.get(isbn).getSubjectCodeList() == null || isbnSubjectMap.get(isbn).getSubjectList() == null){
//				System.out.println("subject\t: -= no update available =-");
			} else {
				bookSolrDocument.setSubjectCodeList(isbnSubjectMap.get(isbn).getSubjectCodeList());
				bookSolrDocument.setSubjectLevelList(isbnSubjectMap.get(isbn).getSubjectLevelList());
				bookSolrDocument.setSubjectList(isbnSubjectMap.get(isbn).getSubjectList());				
				searchSolrDocument.setSubjectCodeList(isbnSubjectMap.get(isbn).getSubjectCodeList());
				searchSolrDocument.setSubjectLevelList(isbnSubjectMap.get(isbn).getSubjectLevelList());
				searchSolrDocument.setSubjectList(isbnSubjectMap.get(isbn).getSubjectList());
			}
		}
		
	}

	public List<BookMetaDataSolrDocument> getBookSolrDocumentList() {
		return bookSolrDocumentList;
	}

	public List<SearchSolrDocument> getSearchSolrDocumentList() {
		return searchSolrDocumentList;
	}
	
	
	private static String getFlag(String onlineFlag, String salesModuleFlag) {
		String f = "1"; // default
		
		// 0 - Review Copies
		// 1 - Online/Available for sale
		// 2 - Online/Not available for sale
		
		if("N".equalsIgnoreCase(onlineFlag)) {
			f = "0";
		} else { // online flag is Y			
			
			// online flag = Y and sales module flag = N
			if("N".equalsIgnoreCase(salesModuleFlag)) {
				f = "2";
			}
		}
		
		return f;
	}
}
