package org.cambridge.ebooks.service.solr.biblio;

import java.sql.Connection;
import java.sql.DriverManager;

import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;


public class SolrBiblioDataSource {
	
	static {
		try {
			String driverName = SolrBiblioProperties.JDBC_DRIVER;//"oracle.jdbc.driver.OracleDriver";
			System.out.println("driverName=" + driverName);
			Class.forName(driverName);	//Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}

	public static Connection getConnection() {
		Connection con = null;
		try {
			String url = SolrBiblioProperties.JDBC_URL;
			String username = SolrBiblioProperties.JDBC_USERNAME;
			String password = SolrBiblioProperties.JDBC_PASSWORD;

			if(url == null){
				System.out.println("---------- URL=null ----------");
			}
			System.out.println("connecting to url=" + url);
			con = DriverManager.getConnection(url, username, password); 
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	} 
}
