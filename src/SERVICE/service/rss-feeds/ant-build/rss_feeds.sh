#!/bin/sh

cd /app/ebooks/service/rss/lib

EXEC_JAR="feeds.jar"
NOW=`date '+rss.feeds.log.%Y-%d-%m'`
ERROR_LOG_FILE=`date '+rss.feeds.error.log.%Y-%d-%m'`
LOG_FILE="/app/ebooks/service/rss/log/$NOW.txt"
ERROR_LOG="/app/ebooks/service/rss/log/$ERROR_LOG_FILE.txt"

nohup /app/jdk1.6.0_20/bin/java -jar -Dproperty.file=`hostname` -Dencoding=UTF-8 -Xms512m -Xmx1024m $EXEC_JAR $1 >> $LOG_FILE 2>> $ERROR_LOG &