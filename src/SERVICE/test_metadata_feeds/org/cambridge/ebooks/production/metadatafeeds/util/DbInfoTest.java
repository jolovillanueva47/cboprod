/**
 * 
 */
package org.cambridge.ebooks.production.metadatafeeds.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.production.metadatafeeds.DbWorker;
import org.cambridge.ebooks.production.metadatafeeds.bean.BiblioData;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.AltIsbn;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Author;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Book;
import org.cambridge.ebooks.production.metadatafeeds.bean.book.Subject;

/**
 * @author kmulingtapang
 *
 */
public class DbInfoTest {

	public static void main(String[] args) {
		List<String> isbns = new ArrayList<String>();
		
		isbns.add("9780511544545");
		isbns.add("9780511750700");
		
		try {
			String isbn = "9780511750700";
			
			BiblioData bibData = DbWorker.getBiblioData(isbns);
			Map<String, Book> bookMap = bibData.getBookMap();
			Book book = bookMap.get(isbn);
			
			System.out.println("=== BOOK ===");
			System.out.println("isbn:" + book.getIsbn());
			System.out.println("price:" + book.getPrice());
			System.out.println("tiered price:" + book.getTieredPrice());
			System.out.println("title:" + book.getTitle().getText());
			System.out.println("subtitle:" + book.getSubtitle());
			System.out.println("series code:" + book.getSeries().getCode());
			System.out.println("series title:" + book.getSeries().getTitle());
			System.out.println("series number:" + book.getSeries().getNumber());
			System.out.println("vol num:" + book.getVolumeNumber());
			System.out.println("vol title:" + book.getVolumeTitle());
			System.out.println("part num:" + book.getPartNumber());
			System.out.println("part title:" + book.getPartTitle());
			System.out.println("ed num:" + book.getEdition().getNumber());
			System.out.println("print date:" + book.getPubDate().getPrint());
			System.out.println("online date:" + book.getPubDate().getOnline());
			
			for(AltIsbn altIsbn : book.getAltIsbns()) {
				System.out.println("alt isbn type:" + altIsbn.getType());
				System.out.println("alt isbn:" + altIsbn.getValue());
			}			
			
			
			Map<String, List<Subject>> subjectMap = bibData.getSubjectMap();
			List<Subject> subjects = subjectMap.get(isbn);
			System.out.println("=== SUBJECTS ===");
			for(Subject subject : subjects) {
				System.out.println("code:" + subject.getCode());
				System.out.println("level:" + subject.getLevel());
				System.out.println("name:" + subject.getName());
			}
			
			Map<String, List<Author>> authorMap = bibData.getAuthorMap();
			List<Author> authors = authorMap.get(isbn);
			System.out.println("=== AUTHORS ===");
			for(Author author : authors) {
				System.out.println("position:" + author.getPosition());
				System.out.println("role:" + author.getRole());
				System.out.println("affiliation:" + author.getAffiliation());
				System.out.println("forename:" + author.getForename());
				System.out.println("surname:" + author.getSurname());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
