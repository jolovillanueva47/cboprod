package org.cambridge.ebooks.production.metadatafeeds;

import org.cambridge.ebooks.production.metadatafeeds.constant.ApplicationProperties;

public class XpathWorkerTest {
	public static void main(String[] args) {
		String source = ApplicationProperties.CONTENT_DIR + "/9780511544545/9780511544545.xml";
//		XpathWorker.getCoverImages(source);
//		System.out.println(XpathWorker.getPdfFile(source, "CBO9780511544545A003"));
		System.out.println(XpathWorker.getDoi(source, "CBO9780511544545A007"));
	}
}
