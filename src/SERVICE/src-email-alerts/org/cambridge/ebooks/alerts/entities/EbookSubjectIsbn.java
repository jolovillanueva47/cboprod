package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="EBOOK_SUBJECT_ISBN")
public class EbookSubjectIsbn
  implements Serializable
{
  private static final long serialVersionUID = 1L;

  @EmbeddedId
  private EbookSubjectIsbnPK id;

  @Column(name="ORIG_SUBJECT_CODE", length=3)
  private String origSubjectCode;

  @Column(name="SEQUENCE_NUMBER", precision=2)
  private BigDecimal sequenceNumber;

  @OneToOne
  @JoinColumns({@javax.persistence.JoinColumn(name="ISBN", referencedColumnName="ISBN", unique=true, insertable=false, updatable=false), @javax.persistence.JoinColumn(name="ISBN", referencedColumnName="ISBN", unique=true, insertable=false, updatable=false)})
  private EbookIsbnDataLoad ebookIsbnDataLoad;

  @ManyToOne
  @JoinColumns({@javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false), @javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false)})
  private EbookSubjectAlert ebookSubjectAlert;

  @OneToOne
  @JoinColumns({@javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false), @javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false)})
  private EbookSubjectHierarchy ebookSubjectHierarchy;

  public EbookSubjectIsbnPK getId()
  {
    return this.id;
  }

  public void setId(EbookSubjectIsbnPK id) {
    this.id = id;
  }

  public String getOrigSubjectCode() {
    return this.origSubjectCode;
  }

  public void setOrigSubjectCode(String origSubjectCode) {
    this.origSubjectCode = origSubjectCode;
  }

  public BigDecimal getSequenceNumber() {
    return this.sequenceNumber;
  }

  public void setSequenceNumber(BigDecimal sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
  }

  public EbookIsbnDataLoad getEbookIsbnDataLoad() {
    return this.ebookIsbnDataLoad;
  }

  public void setEbookIsbnDataLoad(EbookIsbnDataLoad ebookIsbnDataLoad) {
    this.ebookIsbnDataLoad = ebookIsbnDataLoad;
  }

  public EbookSubjectAlert getEbookSubjectAlert() {
    return this.ebookSubjectAlert;
  }

  public void setEbookSubjectAlert(EbookSubjectAlert ebookSubjectAlert) {
    this.ebookSubjectAlert = ebookSubjectAlert;
  }

  public EbookSubjectHierarchy getEbookSubjectHierarchy() {
    return this.ebookSubjectHierarchy;
  }

  public void setEbookSubjectHierarchy(EbookSubjectHierarchy ebookSubjectHierarchy)
  {
    this.ebookSubjectHierarchy = ebookSubjectHierarchy;
  }
}