package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="EBOOK_SERIES_LOAD")
public class EbookSeriesLoad  implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name="SERIES_CODE")
  private String seriesCode;

  @Column(name="SERIES_LEGEND")
  private String seriesLegend;

  @OneToOne(mappedBy="ebookSeriesLoad")
  private EbookIsbnDataLoad ebookIsbnDataLoad;

  public String getSeriesCode()
  {
    return this.seriesCode;
  }

  public void setSeriesCode(String seriesCode) {
    this.seriesCode = seriesCode;
  }

  public String getSeriesLegend() {
    return this.seriesLegend;
  }

  public void setSeriesLegend(String seriesLegend) {
    this.seriesLegend = seriesLegend;
  }

  public EbookIsbnDataLoad getEbookIsbnDataLoad() {
    return this.ebookIsbnDataLoad;
  }

  public void setEbookIsbnDataLoad(EbookIsbnDataLoad ebookIsbnDataLoad) {
    this.ebookIsbnDataLoad = ebookIsbnDataLoad;
  }
}