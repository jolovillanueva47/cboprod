package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the EBOOK_ISBN_DATA_LOAD database table.
 * 
 */
@Entity
@Table(name="EBOOK_ISBN_DATA_LOAD")
public class EbookIsbnDataLoad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(unique=true, nullable=false, length=13)
	private String isbn;

	@Column(name="ANSWER_CODE", length=3)
	private String answerCode;

	@Column(name="AUTHOR_SHORT", length=35)
	private String authorShort;

//	@Column(name="AUTHOR_SINGLELINE", length=250)
//	private String authorSingleline;

	@Column(name="CBO_PRODUCT_GROUP", nullable=false, length=3)
	private String cboProductGroup;

	@Column(name="DIVISIONAL_SUBJECT_CODE", length=2)
	private String divisionalSubjectCode;

	@Column(name="EDITION_NUMBER", precision=3)
	private BigDecimal editionNumber;

//    @Temporal( TemporalType.DATE)
//	@Column(name="ESAMPLE_CHANGE_DATE")
//	private Date esampleChangeDate;
//
//	@Column(name="ESAMPLE_FLAG", nullable=false, length=1)
//	private String esampleFlag;

	@Column(name="FORMAT_CODE", length=2)
	private String formatCode;

	@Column(name="ISBN_PRINT", length=13)
	private String isbnPrint;

	@Column(name="ISBN_PRINT_HB", length=13)
	private String isbnPrintHb;

	@Column(name="ISBN_PRINT_PB", length=13)
	private String isbnPrintPb;

	@Column(name="ONLINE_FLAG", nullable=false, length=1)
	private String onlineFlag;

    @Temporal( TemporalType.DATE)
	@Column(name="ONLINE_FLAG_CHANGE_DATE")
	private Date onlineFlagChangeDate;

	@Column(name="PART_NUMBER", length=5)
	private String partNumber;

	@Column(name="PART_TITLE", length=200)
	private String partTitle;

	@Column(name="PROFIT_CENTRE", length=1)
	private String profitCentre;

    @Temporal( TemporalType.DATE)
	@Column(name="PUB_DATE_ONLINE")
	private Date pubDateOnline;

    @Temporal( TemporalType.DATE)
	@Column(name="PUB_DATE_PRINT")
	private Date pubDatePrint;

	@Column(name="PUB_PRICE", precision=7, scale=2)
	private BigDecimal pubPrice;

	@Column(name="PUB_PRICE_SUBSCRIPTION", precision=9, scale=2)
	private BigDecimal pubPriceSubscription;

	@Column(name="PUBLISHER_CODE", length=6)
	private String publisherCode;

	@Column(name="SALES_MODULE_FLAG", nullable=false, length=1)
	private String salesModuleFlag;

	@Column(name="SALES_MODULE_SUBS_FLAG", nullable=false, length=1)
	private String salesModuleSubsFlag;

	@Column(name="SERIES_CODE", length=4)
	private String seriesCode;

	@Column(name="SERIES_NUMBER", precision=4)
	private BigDecimal seriesNumber;

	@Column(name="SUBJECT_2_CODE", length=3)
	private String subject2Code;

	@Column(name="SUBJECT_3_CODE", length=3)
	private String subject3Code;

	@Column(length=200)
	private String subtitle;

	@Column(name="SUPPLEMENTAL_MATERIAL", length=1)
	private String supplementalMaterial;

	@Column(name="SUPPLEMENTAL_MATERIAL_URL", length=300)
	private String supplementalMaterialUrl;

	@Column(length=500)
	private String title;

	@Column(name="TITLE_CODE", nullable=false, length=6)
	private String titleCode;

	@Column(name="VOLUME_NUMBER", length=5)
	private String volumeNumber;

	@Column(name="VOLUME_TITLE", length=200)
	private String volumeTitle;

	//bi-directional one-to-one association to Ebook
	@OneToOne
	@JoinColumn(name="ISBN", referencedColumnName="ISBN", nullable=false, insertable=false, updatable=false)
	private Ebook ebook;

	//bi-directional one-to-one association to EbookSubjectIsbn
	@OneToOne(mappedBy="ebookIsbnDataLoad")
	private EbookSubjectIsbn ebookSubjectIsbn;
	
	@OneToOne
	@JoinColumn(name="SERIES_CODE", referencedColumnName="SERIES_CODE", nullable=false, insertable=false, updatable=false)
	private EbookSeriesLoad ebookSeriesLoad;

    public EbookIsbnDataLoad() {
    }

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAnswerCode() {
		return this.answerCode;
	}

	public void setAnswerCode(String answerCode) {
		this.answerCode = answerCode;
	}

	public String getAuthorShort() {
		return this.authorShort;
	}

	public void setAuthorShort(String authorShort) {
		this.authorShort = authorShort;
	}

	public String getCboProductGroup() {
		return this.cboProductGroup;
	}

	public void setCboProductGroup(String cboProductGroup) {
		this.cboProductGroup = cboProductGroup;
	}

	public String getDivisionalSubjectCode() {
		return this.divisionalSubjectCode;
	}

	public void setDivisionalSubjectCode(String divisionalSubjectCode) {
		this.divisionalSubjectCode = divisionalSubjectCode;
	}

	public BigDecimal getEditionNumber() {
		return this.editionNumber;
	}

	public void setEditionNumber(BigDecimal editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getFormatCode() {
		return this.formatCode;
	}

	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}

	public String getIsbnPrint() {
		return this.isbnPrint;
	}

	public void setIsbnPrint(String isbnPrint) {
		this.isbnPrint = isbnPrint;
	}

	public String getIsbnPrintHb() {
		return this.isbnPrintHb;
	}

	public void setIsbnPrintHb(String isbnPrintHb) {
		this.isbnPrintHb = isbnPrintHb;
	}

	public String getIsbnPrintPb() {
		return this.isbnPrintPb;
	}

	public void setIsbnPrintPb(String isbnPrintPb) {
		this.isbnPrintPb = isbnPrintPb;
	}

	public String getOnlineFlag() {
		return this.onlineFlag;
	}

	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}

	public Date getOnlineFlagChangeDate() {
		return this.onlineFlagChangeDate;
	}

	public void setOnlineFlagChangeDate(Date onlineFlagChangeDate) {
		this.onlineFlagChangeDate = onlineFlagChangeDate;
	}

	public String getPartNumber() {
		return this.partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartTitle() {
		return this.partTitle;
	}

	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}

	public String getProfitCentre() {
		return this.profitCentre;
	}

	public void setProfitCentre(String profitCentre) {
		this.profitCentre = profitCentre;
	}

	public Date getPubDateOnline() {
		return this.pubDateOnline;
	}

	public void setPubDateOnline(Date pubDateOnline) {
		this.pubDateOnline = pubDateOnline;
	}

	public Date getPubDatePrint() {
		return this.pubDatePrint;
	}

	public void setPubDatePrint(Date pubDatePrint) {
		this.pubDatePrint = pubDatePrint;
	}

	public BigDecimal getPubPrice() {
		return this.pubPrice;
	}

	public void setPubPrice(BigDecimal pubPrice) {
		this.pubPrice = pubPrice;
	}

	public BigDecimal getPubPriceSubscription() {
		return this.pubPriceSubscription;
	}

	public void setPubPriceSubscription(BigDecimal pubPriceSubscription) {
		this.pubPriceSubscription = pubPriceSubscription;
	}

	public String getPublisherCode() {
		return this.publisherCode;
	}

	public void setPublisherCode(String publisherCode) {
		this.publisherCode = publisherCode;
	}

	public String getSalesModuleFlag() {
		return this.salesModuleFlag;
	}

	public void setSalesModuleFlag(String salesModuleFlag) {
		this.salesModuleFlag = salesModuleFlag;
	}

	public String getSalesModuleSubsFlag() {
		return this.salesModuleSubsFlag;
	}

	public void setSalesModuleSubsFlag(String salesModuleSubsFlag) {
		this.salesModuleSubsFlag = salesModuleSubsFlag;
	}

	public String getSeriesCode() {
		return this.seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public BigDecimal getSeriesNumber() {
		return this.seriesNumber;
	}

	public void setSeriesNumber(BigDecimal seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getSubject2Code() {
		return this.subject2Code;
	}

	public void setSubject2Code(String subject2Code) {
		this.subject2Code = subject2Code;
	}

	public String getSubject3Code() {
		return this.subject3Code;
	}

	public void setSubject3Code(String subject3Code) {
		this.subject3Code = subject3Code;
	}

	public String getSubtitle() {
		return this.subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getSupplementalMaterial() {
		return this.supplementalMaterial;
	}

	public void setSupplementalMaterial(String supplementalMaterial) {
		this.supplementalMaterial = supplementalMaterial;
	}

	public String getSupplementalMaterialUrl() {
		return this.supplementalMaterialUrl;
	}

	public void setSupplementalMaterialUrl(String supplementalMaterialUrl) {
		this.supplementalMaterialUrl = supplementalMaterialUrl;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleCode() {
		return this.titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}

	public String getVolumeNumber() {
		return this.volumeNumber;
	}

	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}

	public String getVolumeTitle() {
		return this.volumeTitle;
	}

	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	public Ebook getEbook() {
		return this.ebook;
	}

	public void setEbook(Ebook ebook) {
		this.ebook = ebook;
	}
	
	public EbookSubjectIsbn getEbookSubjectIsbn() {
		return this.ebookSubjectIsbn;
	}

	public void setEbookSubjectIsbn(EbookSubjectIsbn ebookSubjectIsbn) {
		this.ebookSubjectIsbn = ebookSubjectIsbn;
	}

	public EbookSeriesLoad getEbookSeriesLoad() {
		return ebookSeriesLoad;
	}

	public void setEbookSeriesLoad(EbookSeriesLoad ebookSeriesLoad) {
		this.ebookSeriesLoad = ebookSeriesLoad;
	}
	
}