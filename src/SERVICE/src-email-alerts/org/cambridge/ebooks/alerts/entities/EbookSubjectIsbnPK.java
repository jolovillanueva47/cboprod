package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EbookSubjectIsbnPK implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name="SUBJECT_ID", unique=true, nullable=false, length=10)
  private String subjectId;

  @Column(unique=true, nullable=false, length=13)
  private String isbn;

  public String getSubjectId()
  {
    return this.subjectId;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getIsbn() {
    return this.isbn;
  }
  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof EbookSubjectIsbnPK)) {
      return false;
    }
    EbookSubjectIsbnPK castOther = (EbookSubjectIsbnPK)other;

    return (this.subjectId.equals(castOther.subjectId)) && 
      (this.isbn.equals(castOther.isbn));
  }

  public int hashCode()
  {
    int prime = 31;
    int hash = 17;
    hash = hash * prime + this.subjectId.hashCode();
    hash = hash * prime + this.isbn.hashCode();

    return hash;
  }
}