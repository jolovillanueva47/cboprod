package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EbookSubjectAlertPK
  implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Column(name="MEMBER_ID", unique=true, nullable=false, precision=22)
  private long memberId;

  @Column(name="SUBJECT_ID", unique=true, nullable=false, length=20)
  private String subjectId;

  public long getMemberId()
  {
    return this.memberId;
  }
  public void setMemberId(long memberId) {
    this.memberId = memberId;
  }
  public String getSubjectId() {
    return this.subjectId;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof EbookSubjectAlertPK)) {
      return false;
    }
    EbookSubjectAlertPK castOther = (EbookSubjectAlertPK)other;

    return (this.memberId == castOther.memberId) && 
      (this.subjectId.equals(castOther.subjectId));
  }

  public int hashCode()
  {
    int prime = 31;
    int hash = 17;
    hash = hash * prime + (int)(this.memberId ^ this.memberId >>> 32);
    hash = hash * prime + this.subjectId.hashCode();

    return hash;
  }
}