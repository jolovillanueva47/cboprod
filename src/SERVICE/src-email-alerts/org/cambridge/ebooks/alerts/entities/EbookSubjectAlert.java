package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EBOOK_SUBJECT_ALERT")
public class EbookSubjectAlert
  implements Serializable
{
  private static final long serialVersionUID = 1L;

  @EmbeddedId
  private EbookSubjectAlertPK id;

  @Column(name="MODIFIED_BY", length=100)
  private String modifiedBy;

  @Temporal(TemporalType.DATE)
  @Column(name="MODIFIED_DATE")
  private Date modifiedDate;

  @OneToMany(mappedBy="ebookSubjectAlert")
  @JoinColumns({@javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false), @javax.persistence.JoinColumn(name="SUBJECT_ID", referencedColumnName="SUBJECT_ID", unique=true, insertable=false, updatable=false)})
  private List<EbookSubjectIsbn> ebookSubjectIsbns;

  public EbookSubjectAlertPK getId()
  {
    return this.id;
  }

  public void setId(EbookSubjectAlertPK id) {
    this.id = id;
  }

  public String getModifiedBy() {
    return this.modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public List<EbookSubjectIsbn> getEbookSubjectIsbns() {
    return this.ebookSubjectIsbns;
  }

  public void setEbookSubjectIsbns(List<EbookSubjectIsbn> ebookSubjectIsbns) {
    this.ebookSubjectIsbns = ebookSubjectIsbns;
  }
}