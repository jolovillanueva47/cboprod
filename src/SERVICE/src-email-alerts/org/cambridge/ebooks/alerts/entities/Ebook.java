package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EBOOK")
public class Ebook
  implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.TABLE)
  @Column(name="BOOK_ID", unique=true, nullable=false, length=20)
  private String bookId;

  @Column(length=20)
  private String isbn;

  @Column(length=30)
  private String doi;

  @Column(name="MODIFIED_BY", length=48)
  private String modifiedBy;

  @Temporal(TemporalType.DATE)
  @Column(name="MODIFIED_DATE")
  private Date modifiedDate;

  @Column(name="SERIES_CODE", length=40)
  private String seriesCode;

  @Column(nullable=false, precision=22)
  private BigDecimal status;

  @Column(length=500)
  private String title;

  @Column(name="TITLE_ALPHASORT", length=500)
  private String titleAlphasort;

  @OneToOne(mappedBy="ebook")
  private EbookIsbnDataLoad ebookIsbnDataLoad;

  public String getBookId()
  {
    return this.bookId;
  }

  public void setBookId(String bookId) {
    this.bookId = bookId;
  }

  public String getDoi() {
    return this.doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getModifiedBy() {
    return this.modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getSeriesCode() {
    return this.seriesCode;
  }

  public void setSeriesCode(String seriesCode) {
    this.seriesCode = seriesCode;
  }

  public BigDecimal getStatus() {
    return this.status;
  }

  public void setStatus(BigDecimal status) {
    this.status = status;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitleAlphasort() {
    return this.titleAlphasort;
  }

  public void setTitleAlphasort(String titleAlphasort) {
    this.titleAlphasort = titleAlphasort;
  }

  public EbookIsbnDataLoad getEbookIsbnDataLoad() {
    return this.ebookIsbnDataLoad;
  }

  public void setEbookIsbnDataLoad(EbookIsbnDataLoad ebookIsbnDataLoad) {
    this.ebookIsbnDataLoad = ebookIsbnDataLoad;
  }

  public String getIsbn() {
    return this.isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }
}