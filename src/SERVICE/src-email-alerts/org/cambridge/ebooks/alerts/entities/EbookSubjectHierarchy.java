package org.cambridge.ebooks.alerts.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="EBOOK_SUBJECT_HIERARCHY")
public class EbookSubjectHierarchy
  implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.TABLE)
  @Column(name="SUBJECT_ID", unique=true, nullable=false, length=10)
  private String subjectId;

  @Column(name="ACTIVE_SUBJECT", length=1)
  private String activeSubject;

  @Column(name="FINAL_LEVEL", length=1)
  private String finalLevel;

  @Column(name="PARENT_ID", length=10)
  private String parentId;

  @Column(name="SUBJECT_LEVEL", precision=22)
  private BigDecimal subjectLevel;

  @Column(name="SUBJECT_NAME", length=100)
  private String subjectName;

  @Column(name="TITLE_COUNT", precision=22)
  private BigDecimal titleCount;

  @Column(name="VISTA_SUBJECT_CODE", length=10)
  private String vistaSubjectCode;

  @OneToOne(mappedBy="ebookSubjectHierarchy", fetch=FetchType.EAGER)
  private EbookSubjectIsbn ebookSubjectIsbn;

  public String getSubjectId()
  {
    return this.subjectId;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getActiveSubject() {
    return this.activeSubject;
  }

  public void setActiveSubject(String activeSubject) {
    this.activeSubject = activeSubject;
  }

  public String getFinalLevel() {
    return this.finalLevel;
  }

  public void setFinalLevel(String finalLevel) {
    this.finalLevel = finalLevel;
  }

  public String getParentId() {
    return this.parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public BigDecimal getSubjectLevel() {
    return this.subjectLevel;
  }

  public void setSubjectLevel(BigDecimal subjectLevel) {
    this.subjectLevel = subjectLevel;
  }

  public String getSubjectName() {
    return this.subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public BigDecimal getTitleCount() {
    return this.titleCount;
  }

  public void setTitleCount(BigDecimal titleCount) {
    this.titleCount = titleCount;
  }

  public String getVistaSubjectCode() {
    return this.vistaSubjectCode;
  }

  public void setVistaSubjectCode(String vistaSubjectCode) {
    this.vistaSubjectCode = vistaSubjectCode;
  }

  public EbookSubjectIsbn getEbookSubjectIsbn() {
    return this.ebookSubjectIsbn;
  }

  public void setEbookSubjectIsbn(EbookSubjectIsbn ebookSubjectIsbn) {
    this.ebookSubjectIsbn = ebookSubjectIsbn;
  }
}