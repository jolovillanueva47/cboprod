package org.cambridge.ebooks.alerts.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.cambridge.ebooks.alerts.Main;
import org.cambridge.ebooks.alerts.entities.EbookSubjectIsbn;

public class EBookEAO extends EAO{

	public static List<EbookSubjectIsbn> getPublishedTitlesFromSubjectAreas(Main.MONTHS_TO_SUBTRACT month){

		String jpql = "SELECT ebookSubjectIsbns " +
		 			  "FROM EbookSubjectAlert e " +
		 			  "JOIN e.ebookSubjectIsbns ebookSubjectIsbns " +
		 			  "WHERE ebookSubjectIsbns.ebookIsbnDataLoad.ebook.status = 7 " +
		 			  "AND ebookSubjectIsbns.ebookIsbnDataLoad.onlineFlag = 'Y' " +
		 			  "AND ebookSubjectIsbns.ebookIsbnDataLoad.pubDateOnline BETWEEN ?1 AND ?2";  
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -(Math.abs(month.getMonths())));
		
		return EAOUtil.query(EbookSubjectIsbn.class, jpql, cal.getTime(), new Date());
	}
	
	public static Object publishedTitlesFromSubjectAreas(){
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT hie.* ");
		sql.append("FROM ebook_subject_hierarchy hie ");
		sql.append("JOIN ebook_subject_isbn subject_isbn ");
		sql.append("ON(hie.subject_id = subject_isbn.subject_id OR subject_isbn.subject_id LIKE hie.subject_id||'%') ");
		sql.append("JOIN ebook_isbn_data_load load ");
		sql.append("ON (load.isbn  = subject_isbn.isbn) ");
		sql.append("JOIN ebook e ");
		sql.append("ON (e.isbn  = load.isbn) ");
		sql.append("WHERE (hie.active_subject = 'Y'  AND load.online_flag = 'Y' AND e.status = 7) ");
		sql.append("AND e.isbn IS NOT NULL ");
		sql.append("AND load.pub_date_online BETWEEN ADD_MONTHS(SYSDATE, -(?1)) AND SYSDATE ");
		sql.append("ORDER BY hie.subject_id ");
		
		return EAOUtil.queryNativeTest(sql.toString(), 1);
	}
	
	public static List<EbookSubjectIsbn> getMemberSubjectId(Main.MONTHS_TO_SUBTRACT month, Long memberId){

		String jpql = "SELECT ebookSubjectIsbns " +
		 			  "FROM EbookSubjectAlert e " +
		 			  "JOIN e.ebookSubjectIsbns ebookSubjectIsbns " +
		 			  "WHERE ebookSubjectIsbns.ebookIsbnDataLoad.ebook.status = 7 " +
		 			  "AND ebookSubjectIsbns.ebookIsbnDataLoad.onlineFlag = 'Y' " +
		 			  "AND ebookSubjectIsbns.ebookIsbnDataLoad.pubDateOnline BETWEEN ?1 AND ?2 " +
		 			  "AND e.id.memberId = ?3";  
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -(Math.abs(month.getMonths())));
		
		return EAOUtil.query(EbookSubjectIsbn.class, jpql, cal.getTime(), new Date(), memberId);
	}
	
}
