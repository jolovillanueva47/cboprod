package org.cambridge.ebooks.alerts.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class EAOUtil extends EAO{
	
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> queryNative(Class<T> type, String q, Object... params) { 
		List<T> result = null;
		EntityManager em = createEntityManager();
		
		try
		{
			Query query = em.createNativeQuery(q, type);
	        if ( params != null && params.length > 0 ) 
	        { 
	        	int ctr = 1;
		        for ( Object param : params )  
		        	query.setParameter( ctr++, param); 
	        }
	        
	        result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EBookEAO.class + " query(T, String, Object...) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return  result;
	}
	

	public static Object queryNativeTest(String q, Object... params) { 
		Object result = null;
		EntityManager em = createEntityManager();
		
		try
		{
			Query query = em.createNativeQuery(q);
	        if ( params != null && params.length > 0 ) 
	        { 
	        	int ctr = 1;
		        for ( Object param : params )  
		        	query.setParameter( ctr++, param); 
	        }
	        
	        result = query.getSingleResult();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EBookEAO.class + " query(T, String, Object...) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return  result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> queryNative(T type, String q){
		List<T> result = null;
		EntityManager em = createEntityManager();
		try
		{
			Query query = em.createNativeQuery(q, type.getClass());
			result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EBookEAO.class + " query(T, String) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return result;
	}
		
	@SuppressWarnings("unchecked")
	public static <T> List<T> query(Class<T> type, String jpql, Object... params) { 
		List<T> result = null;
		EntityManager em = createEntityManager();
		
		try
		{
			Query query = em.createQuery(jpql);
	        if ( params != null && params.length > 0 ) 
	        { 
	        	int ctr = 1;
		        for ( Object param : params )  
		        	query.setParameter( ctr++, param); 
	        }
	        
	        result = query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + EBookEAO.class + " query(Class<T>, String, Object...) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return  result;
	}	

}
