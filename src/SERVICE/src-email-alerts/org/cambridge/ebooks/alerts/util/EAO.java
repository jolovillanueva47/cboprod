package org.cambridge.ebooks.alerts.util;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EAO {

	private static final Map<String, String> props;
	static
	{
		props= new HashMap<String, String>();
        props.put(AlertsProperties.JDBC_URL, AlertsProperties.JDBC_URL_VALUE);
	}
	
	private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("eBooksService", props);
	
	public static EntityManager createEntityManager(){
		return EMF.createEntityManager();
	}
	
	public static void closeEntityManager(EntityManager em){
		if(em != null && em.isOpen())
			em.close();
	}
	
	public static EntityManagerFactory getEntityManagerFactory(){
		return EMF;
	}
}
