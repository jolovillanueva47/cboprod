/*
 * Created on July 27, 2010
 */
package org.cambridge.ebooks.alerts.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author rvillamor
 *
 */
public class AlertsProperties {
		
	private Configuration config;
	
	private static final String propertyFile = System.getProperty("property.file"); 
	private static final AlertsProperties APP_PROPERTIES = new AlertsProperties();
	
	private AlertsProperties(){
		try 
		{
			config = new PropertiesConfiguration(propertyFile + ".properties");
		} 
		catch (ConfigurationException e) 
		{
			System.err.println("[ConfigurationException] AlertsProperties() message: " + e.getMessage());
		}
	}
	
	public static String TEMPLATE_DIR = APP_PROPERTIES.config.getString("templates.dir");
	public static String TEMPLATE_FILE = APP_PROPERTIES.config.getString("mail.user.info.template");
	public static String OUTPUT_DIR = APP_PROPERTIES.config.getString("mail.store.path");
	public static String FROM_EMAIL_ADD = APP_PROPERTIES.config.getString("email.cbo");
	public static String CSV_DIR = APP_PROPERTIES.config.getString("csv.dir");
	public static String CSV_URL = APP_PROPERTIES.config.getString("csv.url");
	public static String SERVLET_URL = APP_PROPERTIES.config.getString("newtitles.servlet.url");
	public static String MONTHS = APP_PROPERTIES.config.getString("months");
	
	public static String JDBC_URL = "javax.persistence.jdbc.url";
	public static String JDBC_URL_VALUE = APP_PROPERTIES.config.getString("jdbc.url");
	

}
