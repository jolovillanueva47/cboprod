package org.cambridge.ebooks.alerts;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cambridge.ebooks.alerts.entities.EbookIsbnDataLoad;
import org.cambridge.ebooks.alerts.entities.EbookSeriesLoad;
import org.cambridge.ebooks.alerts.entities.EbookSubjectHierarchy;
import org.cambridge.ebooks.alerts.entities.EbookSubjectIsbn;
import org.cambridge.ebooks.alerts.util.AlertsProperties;
import org.cambridge.ebooks.alerts.util.EBookEAO;
import org.cambridge.ebooks.alerts.util.MailManager;
import org.cambridge.ebooks.alerts.util.MailManager.EmailUtilException;


public class Main {
		
	private static final String TEMPLATE_DIR = AlertsProperties.TEMPLATE_DIR;
	private static final String TEMPLATE = AlertsProperties.TEMPLATE_FILE;
	private static final String OUTPUT_DIR = AlertsProperties.OUTPUT_DIR;
	private static final String FROM_EMAIL_ADD = AlertsProperties.FROM_EMAIL_ADD;
	private static final String CSV_DIR = AlertsProperties.CSV_DIR;
	private static final String SERVLET_URL = AlertsProperties.SERVLET_URL;
	private static int VALUE = 1;
	
	public enum MONTHS_TO_SUBTRACT{
		MONTHS(VALUE);
		
		private int months;
		
		MONTHS_TO_SUBTRACT(int months){ this.months = months; }

		public int getMonths() {
			return months;
		}

		public void setDays(int months) {
			this.months = months;
		}
	}
		
	public static void main(String[] args){
		
		try
		{
			try
			{
				VALUE = Integer.parseInt(args[0]);
			}
			catch(Exception ne)
			{
				System.out.println("Using default value for month.");
			}
						
			List<EbookSubjectIsbn> ebookSubjectIsbns = EBookEAO.getPublishedTitlesFromSubjectAreas(MONTHS_TO_SUBTRACT.MONTHS);
			boolean sendEmail = generateCSV(ebookSubjectIsbns);
			if(sendEmail)
				email(ebookSubjectIsbns);
			
			System.out.println("Done.");
		}
		catch(Exception ex)
		{
			System.err.println("[ERROR] "  + " main(String[]) " + ex.getMessage());
		}
	}
	
	
	private static boolean generateCSV(List<EbookSubjectIsbn> ebookSubjectIsbns){
		boolean result = false;
		Set<String> uSubjectId = new HashSet<String>();
		
		try 
		{
			SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
			File csvDir = new File(CSV_DIR);
			File[] files = csvDir.listFiles();
			for(File csv : files)
				csv.delete();			
			int ctr = 0;
			for(EbookSubjectIsbn ebookSubjectIsbn : ebookSubjectIsbns)
			{
				
				String subjectId = ebookSubjectIsbn.getEbookSubjectAlert().getId().getSubjectId();
				/*if(uSubjectId.contains(subjectId))
					continue;
				else*/
					uSubjectId.add(subjectId);
				
				StringBuilder sb = new StringBuilder();
				FileWriter writer = null;
				EbookIsbnDataLoad dataLoad = ebookSubjectIsbn.getEbookIsbnDataLoad();
				EbookSubjectHierarchy hierarchy = ebookSubjectIsbn.getEbookSubjectHierarchy();
				EbookSeriesLoad series = dataLoad.getEbookSeriesLoad();
									
				File csv = new File(csvDir, ebookSubjectIsbn.getEbookSubjectAlert().getId().getSubjectId() + ".csv"); 
				if(csv.exists()){
					csv = new File(csvDir, ebookSubjectIsbn.getEbookSubjectAlert().getId().getSubjectId() + "-" + ++ctr + ".csv");
					//writer = new FileWriter(csv, true);
				}
					writer = new FileWriter(csv);
				

				sb.append("=\"" + dataLoad.getIsbn() + "\",");
				
				sb.append((dataLoad.getPubDateOnline() != null ? date.format(dataLoad.getPubDateOnline()) : "") + ",");
				sb.append("=\"" + dataLoad.getIsbnPrintHb() + "\",");
				sb.append((dataLoad.getPubDatePrint() != null ? date.format(dataLoad.getPubDatePrint()) : "") + ",");
				
				String pbIsbn = dataLoad.getIsbnPrintPb();
				sb.append((pbIsbn != null ? "=\"" + pbIsbn +"\"" : "") + ",");
				
				String author = dataLoad.getAuthorShort();
				if(author != null)
					sb.append((author.contains(",") ? "\"" + author + "\"" : author) + ",");
				
				String title = dataLoad.getTitle();
				sb.append((title.contains(",") ? "\"" + title + "\"" : title) + ",");
				
				sb.append("=\"" + dataLoad.getVolumeNumber() + "\",");
				sb.append("=\"" + dataLoad.getEditionNumber() + "\",");
				
				String subjectName = hierarchy.getSubjectName();
				String vistaCode = hierarchy.getVistaSubjectCode();
				if(series != null)
				{
					String seriesLegend = series.getSeriesLegend();
					sb.append((subjectName.contains(",") ? "\"" + subjectName + "\"" : subjectName) + ",");
					sb.append(vistaCode + ",");
					sb.append((seriesLegend.contains(",") ? "\"" + seriesLegend + "\"" : seriesLegend));
				}
				else
				{
					sb.append((subjectName.contains(",") ? "\"" + subjectName + "\"" : subjectName) + ",");	
					sb.append(vistaCode);
				}

				PrintWriter p = new PrintWriter(writer);
				p.println(sb.toString());
				
				p.close();
				result = true;
				
			}
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.err.println("[IOException] createCSV(List<EBookSubjectAlert>) " + ex.getMessage());
			ex.printStackTrace();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			System.err.println("[Exception] createCSV(List<EBookSubjectAlert>) " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return result;
		
	}	
	
		
	private static void email(List<EbookSubjectIsbn> toEmail){
		Set<Long> uMemberId = new HashSet<Long>();
		Set<String> uSubjectId = null;
		String memSubjectId = null;
		
		for(EbookSubjectIsbn to : toEmail)
		{
			uSubjectId = new HashSet<String>();
			Long memberId = to.getEbookSubjectAlert().getId().getMemberId();
			if(uMemberId.contains(memberId))
				continue;
			else
				uMemberId.add(memberId);
			
			List<EbookSubjectIsbn> ebookSubjectIsbns = EBookEAO.getMemberSubjectId(MONTHS_TO_SUBTRACT.MONTHS , memberId);
			
			String subj = "New Content Available on Cambridge Books Online";
			
			StringBuilder message = new StringBuilder();
			message.append("New books are available on Cambridge Books Online. To see the details of the new \n<br/>");
			message.append("titles that have recently been loaded to the site, click the link below: \n<br/><br/>");
			for(EbookSubjectIsbn subjectList : ebookSubjectIsbns){
				String origSubjectId = subjectList.getEbookSubjectAlert().getId().getSubjectId();
				String subjectName = subjectList.getEbookSubjectHierarchy().getSubjectName();
				if(uSubjectId.contains(subjectName))
					continue;
				else{
					uSubjectId.add(subjectName);
					message.append("<a href=\"" + (SERVLET_URL + "?memberId=" + memberId + "&amp;subjectId=" + origSubjectId ) + "\">" + subjectName +"</a> \n<br/><br/>");
				}
			}
			
			message.append("To learn more about Cambridge Books Online, and to browse all available titles, \n<br/>");
			message.append("please go to <a href=\"http://ebooks.cambridge.org\">http://ebooks.cambridge.org.</a> \n<br/><br/>");
			
			message.append("Please contact your regional sales representative for information about trials and pricing.\n<br/>");
			message.append("Best regards,\n<br/><br/>");
			
			message.append("<b>The Americas</b>\n<br/>");
			message.append("Cambridge University Press\n<br/>");
			message.append("32 Avenue of the Americas\n<br/>");
			message.append("New York, NY 10013-2473\n<br/>");
			message.append("Email: <a href=\"mailto:online@cambridge.org\">online@cambridge.org</a> to reach the library sales team. \n<br/>");
			message.append("Email: <a href=\"mailto:techsupp@cambridge.org\">techsupp@cambridge.org</a> to reach technical support. \n<br/><br/>");
							
			message.append("<b>United Kingdom and Rest of World</b>\n<br/>");
			message.append("Cambridge University Press\n<br/>");
			message.append("Online Publications\n<br/>");
			message.append("Shaftesbury Road\n<br/>");
			message.append("Cambridge, CB2 8RU\n<br/>");
			message.append("Email: <a href=\"mailto:academicsales@cambridge.org\">academicsales@cambridge.org</a> for sales enquiries \n<br/>");
			message.append("Email: <a href=\"mailto:onlinepublications@cambridge.org\">onlinepublications@cambridge.org</a> for customer services. \n<br/><br/>");

			message.append("We will continue to provide details as new content becomes available, based on the \n<br/>");
			message.append("parameters you have set on the Cambridge Books Online system. For further \n<br/>");
			message.append("information or to unsubscribe, go to <a href=\"http://ebooks.cambridge.org\">http://ebooks.cambridge.org.</a> \n<br/>");
			
			try 
			{
				MailManager.storeMail(TEMPLATE_DIR, 
									  TEMPLATE, 
									  OUTPUT_DIR, 
									  FROM_EMAIL_ADD, 
									  new String[]{to.getEbookSubjectAlert().getModifiedBy()}, 
									  null, 
									  null, 
									  subj, 
									  message.toString());
			} 
			catch (EmailUtilException e) 
			{
				System.err.println(" [EmailUtilException] email(List<EBookSubjectAlert>): " + e.getMessage());
			}			
		}
		
		
	}
	

}
