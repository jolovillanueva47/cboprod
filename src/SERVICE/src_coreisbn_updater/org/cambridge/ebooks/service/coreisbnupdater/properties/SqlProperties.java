package org.cambridge.ebooks.service.coreisbnupdater.properties;

public class SqlProperties {

	public static final String SQL_GET_ALL_ISBN = "select isbn from ebook where status = '7'";
	
	public static final String SQL_GET_DAILY_ISBN = "SELECT eidl.isbn " + 
		"FROM oraebooks.ebook eb, oraebooks.ebook_isbn_data_load eidl, " +
			"(select null book_id, eisbn isbn, status, max(audit_date) audit_date from audit_trail_ebooks where status = 7 group by null, eisbn, status) ateb " + 
		"WHERE eidl.isbn = ateb.isbn and eidl.isbn = eb.isbn and eb.status = '7' " +  
			"and (trunc(sysdate-1) = trunc(online_flag_change_date) or trunc(sysdate-1) = trunc(esample_change_date) or trunc(sysdate-1) = trunc(ateb.audit_date)) " + 
		"ORDER BY eidl.isbn";
	
	public static final String SQL_UPDATE_CORE_ISBN_EBOOK_DOI = "update core_isbn set ebook_doi = ? where format_code = 'OC' and ean_number = ?";
}
