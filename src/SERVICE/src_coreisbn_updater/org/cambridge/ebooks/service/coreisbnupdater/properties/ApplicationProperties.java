package org.cambridge.ebooks.service.coreisbnupdater.properties;

import java.net.InetAddress;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.cambridge.ebooks.service.coreisbnupdater.util.ExceptionPrinter;

public class ApplicationProperties {	
	private static Configuration config;
	
	static {
		try {				
			InetAddress inetAdd = InetAddress.getLocalHost();
			String propertyFile = inetAdd.getHostName() + ".properties";
			System.out.println("====================================================");
			System.out.println("Host: " + inetAdd.getHostName());			
			System.out.println("Property file: " + propertyFile);		
			config = new PropertiesConfiguration(propertyFile);
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static final String JDBC_DRIVER = config.getString("jdbc.driver");
	
	public static final String ORALIV_JDBC_URL = config.getString("oraliv.jdbc.url");
	public static final String ORALIV_JDBC_USERNAME = config.getString("oraliv.jdbc.user");
	public static final String ORALIV_JDBC_PASSWORD = config.getString("oraliv.jdbc.password");
	
	public static final String ORAEBOOKS_JDBC_URL = config.getString("oraebooks.jdbc.url");
	public static final String ORAEBOOKS_JDBC_USERNAME = config.getString("oraebooks.jdbc.user");
	public static final String ORAEBOOKS_JDBC_PASSWORD = config.getString("oraebooks.jdbc.password");	
	
	public static final String CONTENT_DIR = config.getString("content.dir");
}
