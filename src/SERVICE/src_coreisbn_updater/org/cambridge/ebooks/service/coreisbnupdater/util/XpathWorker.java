package org.cambridge.ebooks.service.coreisbnupdater.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author kmulingtapang
 */
public class XpathWorker {
	
	private static final String XPATH_BOOK_DOI = "book/metadata/doi/text()";
	
	private static DocumentBuilderFactory factory = null;
	
	static {
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();
			}
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}		
	
	public static String getBookDoi(String inputFile) {		
		return getValue(inputFile, XPATH_BOOK_DOI);
	}
		
	private static List<String> getValues(String inputFile, String expression) {
		List<String> values = new ArrayList<String>();
		
		Document dom = getXmlDom(inputFile);
		
		NodeList nodeList = searchNodes(dom, expression);
		
		if (nodeList != null && nodeList.getLength() > 0) {
			for(int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				values.add(node.getNodeValue());
			}
		}
		
		return values;
	}
	
	private static String getValue(String inputFile, String expression) {		
		String value = "";
		Document dom = getXmlDom(inputFile);
		
		Node node = searchNode(dom, expression);
		
		if(null != node) {
			value = node.getNodeValue();
		}
		
		return value;
	}
	
	private static NodeList searchNodes(Node xmlDom, String xpathExpression) { 
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodes = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODESET);			
			nodes = (NodeList) result;			
		} catch (Exception e) { 			
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return nodes;
	}
	
	private static Node searchNode(Node xmlDom, String xpathExpression) {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		Node node = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODE);	
			node = (Node) result;			
		} catch (Exception e) { 			
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return node;
	}
	
	private static Document getXmlDom(String filename) { 
		Document doc = null;
		File file = new File(filename);
		try { 
			doc = factory.newDocumentBuilder().parse(file);			
		} catch (Exception e) { 
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return doc;
	}
}
