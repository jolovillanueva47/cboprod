package org.cambridge.ebooks.service.coreisbnupdater;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cambridge.ebooks.service.coreisbnupdater.properties.ApplicationProperties;
import org.cambridge.ebooks.service.coreisbnupdater.properties.SqlProperties;
import org.cambridge.ebooks.service.coreisbnupdater.util.DbUtil;
import org.cambridge.ebooks.service.coreisbnupdater.util.ExceptionPrinter;
import org.cambridge.ebooks.service.coreisbnupdater.util.XpathWorker;

public class CoreIsbnUpdater {

	private static final String PARAM_ALL = "all";
	private static final String PARAM_DAILY = "daily";
	private static final String ERROR_ARGS = "args must be 'all' | 'daily'";
	
	public static void main(String[] args) {
		System.out.println("=== CoreIsbnUpdater ===");
		System.out.println("args:" + Arrays.toString(args));
		if(null != args && args.length > 0) {
			String arg1 = args[0];
			
			if(PARAM_ALL.equalsIgnoreCase(arg1) || PARAM_DAILY.equalsIgnoreCase(arg1)) {
				updateCoreIsbn(arg1);				
			} else {
				System.out.println(ERROR_ARGS);
			}
			
		} else {
			System.out.println(ERROR_ARGS);
		}
	}
	
	private static void updateCoreIsbn(String param) {
		List<String> isbnList = getIsbnList(param);
		
		Connection conn = DbUtil.getConnection(ApplicationProperties.ORALIV_JDBC_URL, 
				ApplicationProperties.ORALIV_JDBC_USERNAME, 
				ApplicationProperties.ORALIV_JDBC_PASSWORD);
		
		PreparedStatement ps = null;
		int ctr = 0;
		int updateCtr = 0;
		System.out.println("isbn count:" + isbnList.size());
		for(String isbn : isbnList) {
			ctr++;
			System.out.println("[" + ctr + "]" + isbn);
			try {
				StringBuilder headerFile = new StringBuilder(ApplicationProperties.CONTENT_DIR);
				headerFile.append("/").append(isbn).append("/").append(isbn).append(".xml"); 
				
				String bookDoi = XpathWorker.getBookDoi(headerFile.toString());
				
				ps = conn.prepareStatement(SqlProperties.SQL_UPDATE_CORE_ISBN_EBOOK_DOI);
				ps.setString(1, bookDoi);
				ps.setString(2, isbn);
				
				int row = ps.executeUpdate();				
				System.out.println("update:" + row);
				if(1 == row) {
					updateCtr++;
				}
			} catch (SQLException e) {
				System.out.println(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			} finally {
				DbUtil.closeStatement(ps);
			}
		}		
		DbUtil.closeConnection(conn);
		
		System.out.println("isbn update count:" + updateCtr);
	}
	
	private static List<String> getIsbnList(String param) {
		List<String> isbnList = new ArrayList<String>();
		
		Connection conn = DbUtil.getConnection(ApplicationProperties.ORAEBOOKS_JDBC_URL, 
				ApplicationProperties.ORAEBOOKS_JDBC_USERNAME, 
				ApplicationProperties.ORAEBOOKS_JDBC_PASSWORD);
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = SqlProperties.SQL_GET_ALL_ISBN;
			
			if(param.equalsIgnoreCase(PARAM_DAILY)) {
				sql = SqlProperties.SQL_GET_DAILY_ISBN;
			}
			
			ps = conn.prepareStatement(sql);			
			rs = ps.executeQuery();
			
			// populate isbnList
			while (null != rs && rs.next()) {
				isbnList.add(rs.getString("ISBN"));
			}
			
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			DbUtil.closeResultSet(rs);
			DbUtil.closeStatement(ps);
		}

		DbUtil.closeConnection(conn);
		
		return isbnList;
	}
}
