package org.cambridge.ebooks.service.coreisbnupdater.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.cambridge.ebooks.service.coreisbnupdater.properties.ApplicationProperties;

public class DbUtil {
	
	static {
		try {
			String driverName = ApplicationProperties.JDBC_DRIVER;
			System.out.println("driverName=" + driverName);
			Class.forName(driverName);
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static Connection getConnection(String url, String username, String password) {
		Connection con = null;
		try {
			System.out.println("connecting to url=" + url);
			con = DriverManager.getConnection(url, username, password); 
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
		return con;
	}
	
	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	} 
	
	public static void closeResultSet(ResultSet resultSet) {
		try {
			resultSet.close();
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static void closeStatement(Statement statement) {
		try {
			statement.close();
		} catch (Exception e) {
			System.out.println(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
}
