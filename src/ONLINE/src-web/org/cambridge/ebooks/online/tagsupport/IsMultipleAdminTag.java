/**
 * 
 */
package org.cambridge.ebooks.online.tagsupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

/**
 * @author kmulingtapang
 */
public class IsMultipleAdminTag extends BodyTagSupport {

	private static final long serialVersionUID = 8709588840912190197L;

	@Override
	public int doStartTag() throws JspException {
		int result = SKIP_BODY;
		
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		
		if(null != orgConLinkedMap.getUserLogin() && orgConLinkedMap.getUserLogin().isMultipleAdmin()) {
			result = EVAL_BODY_INCLUDE;
		}
		
		return result;
	}	
}
