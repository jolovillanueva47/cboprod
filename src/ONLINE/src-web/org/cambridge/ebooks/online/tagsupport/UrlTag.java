package org.cambridge.ebooks.online.tagsupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.util.UrlUtil;

/**
 * @author kmulingtapang
 */
public class UrlTag extends TagSupport {
	
	private static final long serialVersionUID = 4580448663322561613L;
	
	private String var;
	private String option;	
	private String append;
	
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getOption() {
		return option;
	}
	
	public void setOption(String option) {
		this.option = option;
	}
	
	public String getAppend() {
		return append;
	}

	public void setAppend(String append) {
		this.append = append;
	}

	@Override
	public int doStartTag() throws JspException {	
		boolean isSslOn = Boolean.parseBoolean(System.getProperty("ssl.on"));		
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();		
				
		try {
			String result = request.getRequestURL().toString(); 
			
			if("current_dns".equals(option)) {
				result = UrlUtil.getClientDns(request); 
			} else if("current_dns_to_https".equals(option)){		
				result = UrlUtil.getClientDns(request); 
				if(isSslOn) {
					result = UrlUtil.getClientHttpsDns(request);
				} 
			} else if("context_root".equals(option)) {
				result = UrlUtil.getContextRoot();
			} else if("context_root_cjo".equals(option)) {
				result = UrlUtil.getContextRootCjo();
			}

			pageContext.setAttribute(var, result + (StringUtils.isEmpty(append) ? "" : append));
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return SKIP_BODY;
	}
}
