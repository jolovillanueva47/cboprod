/**
 * 
 */
package org.cambridge.ebooks.online.tagsupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

/**
 * @author kmulingtapang
 */
public class IsAdminTag extends BodyTagSupport {

	private static final long serialVersionUID = -571721812685847433L;

	@Override
	public int doStartTag() throws JspException {
		int result = SKIP_BODY;
		
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		
		if(null != orgConLinkedMap.getUserLogin() && "Y".equalsIgnoreCase(orgConLinkedMap.getUserLogin().getIsAdmin())) {		        	
			result = EVAL_BODY_INCLUDE;
    	}
		
		return result;
	}
	
}
