/**
 * 
 */
package org.cambridge.ebooks.online.tagsupport;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.cambridge.ebooks.online.subscription.AccessList;

/**
 * @author kmulingtapang
 */
public class IsPurchasedAccessTag extends BodyTagSupport {

	private static final long serialVersionUID = -8560204552239347758L;
	
	private static final String NON_ACCESS_TYPES = AccessList.NO_ACCESS + "," + AccessList.FREE_ACCESS;

	private String accessType;	
	
	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	@Override
	public int doStartTag() throws JspException {
		int result = EVAL_BODY_INCLUDE;
		
		if(NON_ACCESS_TYPES.contains(accessType)) {
			result = SKIP_BODY;
		}
		
		return result;
	}
}
