package org.cambridge.ebooks.online.cite;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.online.accessibility.Accessible;


public class CitationServlet_aaa extends CitationServlet implements Accessible{
	/**
	 * Accesible version of CitationServlet
	 * forwardTo now goes to the accessible jsp page
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void forwardToUrl(HttpServletRequest req, HttpServletResponse resp, String path) {
		
		String newPath = accUrlFormatter(path);
		super.forwardToUrl(req, resp, newPath);
	}
	
	public String accUrlFormatter(String path){
		
		String accPath = ACC_URL_IDENTIFIER + path;
//		String accPath = path;
//		String token = "../";
//		if( null != path && path.contains("../") ){
//			accPath = path.substring(  0, (path.lastIndexOf("../")+token.length()) ) +
//					ACC_URL_IDENTIFIER +  
//					path.substring( (path.lastIndexOf("../")+token.length()), path.length())  ;
//		}
		return accPath;
	}
}
