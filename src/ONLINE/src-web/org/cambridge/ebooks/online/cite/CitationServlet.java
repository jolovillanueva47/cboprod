package org.cambridge.ebooks.online.cite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class CitationServlet extends HttpServlet{

	private static final Logger LOGGER = Logger.getLogger(CitationServlet.class);
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException {

		String doi = req.getParameter("doi");
		CiteWorker citeWorker = new CiteWorker();
		List result = new ArrayList();
		String path = "/popups/citedBy.jsf";

		try{
//			result = citeWorker.getCitation("10.1017/S0954102009990472");
//			result = citeWorker.getCitation("10.1017/CBO9780511482472"); //CBO The Play of Character in Plato's Dialogues
			result = citeWorker.getCitation(doi);

			if (result == null ){
				LOGGER.info("result = citeWorker.getCitation(doi) ----> NULL");
			}
			
			//add request
			req.setAttribute("result", result);
			req.setAttribute("citedByCrossref", result);
			
		}catch (Exception e) {
			LOGGER.error("citeWorker Error" + e.getMessage());
		}
		
//		System.out.println("path: ========================>" + req.getHeader("referer"));
//		if ( req.getHeader("referer").contains(ACC_URL_IDENTIFIER) ){
//			forwardPath = ACC_URL_IDENTIFIER + forwardPath;
//		}
		forwardToUrl(req, resp, path);
	}
	
	protected void forwardToUrl(HttpServletRequest req, HttpServletResponse resp, String path){
		RequestDispatcher dispatcher = null;
		try {
			dispatcher = req.getRequestDispatcher(path);
			dispatcher.forward(req, resp);
		} catch (Exception e) {
			LOGGER.error("Error" + e.getMessage() );
		}
	}
}
