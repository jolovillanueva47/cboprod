package org.cambridge.ebooks.online.cite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CiteWorker {
	
	private static final Logger LOGGER = Logger.getLogger(CiteWorker.class);
	
	public ArrayList getCitation(String doi) throws Exception {
		ArrayList citedByArticles = getCitation(doi,"",""); 
		return citedByArticles;
	}
	
	public ArrayList getCitation(String doi, String startDate, String endDate) throws Exception {
			
			LOGGER.info("getCitation running");
			
			ArrayList citedByArticles = new ArrayList();
			Socket socket = null;
			PrintWriter out = null;
			BufferedReader in = null;
			BufferedWriter bw = null;
			OutputStreamWriter ow = null;
			try {
				// Build the Parameters
				String crossrefSite = (System.getProperty("crossref.org.site") != null) ?System.getProperty("crossref.org.site") :"doi.crossref.org";
				String username = (System.getProperty("crossref.username") != null) ?System.getProperty("crossref.username") :"cup"; 
				String password = (System.getProperty("crossref.password") != null) ?System.getProperty("crossref.password") :"crpw584"; 
				String host = (System.getProperty("crossref.org.site") != null) ?System.getProperty("crossref.org.site") :"doi.crossref.org"; 
				int port = (System.getProperty("crossref.org.port") != null) ?Integer.parseInt(System.getProperty("crossref.org.port")) :80; 

				String param = "usr=" + username + "&pwd=" + password + "&doi=" + doi
								+ ((startDate!=null && startDate.trim().length()!=0)?"&startDate="+startDate:"")
								+ ((endDate!=null && endDate.trim().length()!=0)?"&endDate="+endDate:"");
				
				// Start Connection to doi.crossref.org
				socket= new Socket(host, port);
				ow=new OutputStreamWriter(socket.getOutputStream());
				 bw=new BufferedWriter(ow);
				 out = new PrintWriter(bw,true);
				
				 in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out.println("GET /servlet/getForwardLinks?"+ param +" HTTP/1.0\r\nHost: " + crossrefSite + "\r\n\r\n");
				
				LOGGER.info("CRSREF URL: http://"+ crossrefSite + "/servlet/getForwardLinks?" + param );
				
				// Print XML
				String temp = "";
				String xml = "";
				while ((temp = in.readLine()) != null) {
					xml += temp;
					xml += "\r\n";
				}
				
				// Check if returned document is XML
				if (xml!=null && (xml.toUpperCase().indexOf("403 NOT AUTHORIZED")!=-1 || xml.toUpperCase().indexOf("<HTML>")!=-1)) {
					
					LOGGER.debug("Crossref did not return an XML document. " +
								"Probably the article to be referenced was " +
								"not published by CUP or Crossref server is down.");
					
				} else {
					
					LOGGER.debug("XML: \r\n"+ xml);
					
					// Parse XML
					DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
					DocumentBuilder b = f.newDocumentBuilder();
					Document d = b.parse("http://doi.crossref.org/servlet/getForwardLinks?" + param);
					
					NodeList fl = d.getElementsByTagName("forward_link");
					
//					System.out.println( "Found "+ fl.getLength() + " forward_link tags \r\n");
					LOGGER.info("Found "+ fl.getLength() + " forward_link tags \r\n");
					
					CitedByVo vo =null;
					Element edl =null;
					
					for (int i = 0; i < fl.getLength(); i++) {
						
						 vo = new CitedByVo();
						 edl = (Element) fl.item(i);
						
						try {
							
							// Journal Title
							NodeList jtl = edl.getElementsByTagName("journal_title").item(0).getChildNodes();
							if (jtl.getLength() > 0) vo.setJournalTitle(jtl.item(0).getNodeValue()); 
							
							// Article Title
							NodeList atl = edl.getElementsByTagName("article_title").item(0).getChildNodes();
							if (atl.getLength() > 0) vo.setArticleTitle(atl.item(0).getNodeValue()); 
							
							// Year
							NodeList year = edl.getElementsByTagName("year").item(0).getChildNodes();
							if (year.getLength() > 0) vo.setYear(year.item(0).getNodeValue()); 
							
							// DOI
							NodeList doiB = edl.getElementsByTagName("doi").item(0).getChildNodes();
							if (doiB.getLength() > 0) vo.setDoi(doiB.item(0).getNodeValue()); 
							
							
							try {
								NodeList volTitle = edl.getElementsByTagName("volume_title").item(0).getChildNodes();
								if ( null!= volTitle && null!= volTitle.item(0).getNodeValue() && volTitle.getLength() > 0) 
									{vo.setVolume_title(volTitle.item(0).getNodeValue());} 
								}catch (Exception n) {
									LOGGER.debug("Ooops! no volume title found " );
									//break;
								}

							// Volume
//							NodeList vol = edl.getElementsByTagName("volume").item(0).getChildNodes();
//							if (vol.getLength() > 0) vo.setVolume(vol.item(0).getNodeValue()); 
							try {
								NodeList vol = edl.getElementsByTagName("volume").item(0).getChildNodes();
								if ( null!= vol && null!= vol.item(0).getNodeValue() && vol.getLength() > 0) 
									{vo.setVolume(vol.item(0).getNodeValue());} 
								}catch (Exception n) {
									LOGGER.debug("Ooops! no volume found " );
									//break;
								}
								

//							//book specific start
//							NodeList isbn = edl.getElementsByTagName("isbn").item(0).getChildNodes();
//							if (isbn.getLength() > 0) vo.setIsbn(isbn.item(0).getNodeValue()); 
//							
//							NodeList ser_title = edl.getElementsByTagName("series_title").item(0).getChildNodes();
//							if (ser_title.getLength() > 0) vo.setSeries_title(ser_title.item(0).getNodeValue()); 
//							
//							NodeList vol_title = edl.getElementsByTagName("volume_title").item(0).getChildNodes();
//							if (vol_title.getLength() > 0) vo.setVolume_title(vol_title.item(0).getNodeValue()); 
//							
//							NodeList ed_num = edl.getElementsByTagName("edition_number").item(0).getChildNodes();
//							if (ed_num.getLength() > 0) vo.setEdition_number(ed_num.item(0).getNodeValue()); 
//							
//							NodeList comp_num = edl.getElementsByTagName("component_number").item(0).getChildNodes();
//							if (comp_num.getLength() > 0) vo.setComponent_number(comp_num.item(0).getNodeValue()); 
//							//book specific end
							
							
							try {
							NodeList page = edl.getElementsByTagName("first_page").item(0).getChildNodes();
							if ( null!= page && null!= page.item(0).getNodeValue() && page.getLength() > 0) 
								{vo.setPage(page.item(0).getNodeValue());} 
							}catch (Exception n) {
								LOGGER.debug("Ooops! no page found " );
								//break;
							}
							
							// Publication Type 
							//NodeList pubtype = edl.getElementsByTagName("publication_type").item(0).getChildNodes();
							//if (pubtype.getLength() > 0) vo.setPublicationType(pubtype.item(0).getNodeValue()); 
							
							// ISSN
							/*String type = "";
							NodeList value;
							NodeList issn = edl.getElementsByTagName("issn");
							for (int j = 0; j < issn.getLength(); j++) {
								Element issnElem = (Element) issn.item(j);
								type = issnElem.getAttribute("type");
								value = edl.getElementsByTagName("issn").item(j).getChildNodes();
								if ("electronic".equals(type))
									vo.setIssnElectronic(value.item(0).getNodeValue());
								else if ("print".equals(type))
									vo.setIssnPrint(value.item(0).getNodeValue());
							}*/
											
							// Contributors
							NodeList contList = edl.getElementsByTagName("contributor");
							String cont[] = new String[contList.getLength()];
							for (int k = 0; k < contList.getLength(); k++) {
								Element contElem = (Element) contList.item(k);
								NodeList contGN = contElem.getElementsByTagName("given_name").item(0).getChildNodes();
								NodeList contSN = contElem.getElementsByTagName("surname").item(0).getChildNodes();
								cont[k] = contGN.item(0).getNodeValue() + " " + contSN.item(0).getNodeValue();
							}
							vo.setContributors(cont);
							
							// Add to ArrayList
							citedByArticles.add(vo);
							Collections.sort(citedByArticles);
							
							
							
						} catch (NullPointerException n) {
							LOGGER.debug("Ooops! No journal cited by articles for doi: " + doi);
							//break;
						}
					}
				}
				
			} catch (Exception e) {
				LOGGER.error("getCitedByArticles ERROR: " + e.getMessage());
				throw e;
			}finally{
				if (null!= socket) socket.close();
				if (null!= bw) bw.close();
				if (null!= ow) ow.close();
				if (null!= out) out.close();
				if (null!= in) in.close();
				
			}
			
			return citedByArticles;
		}
}