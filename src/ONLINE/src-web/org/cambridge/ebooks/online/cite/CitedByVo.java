package org.cambridge.ebooks.online.cite;

public class CitedByVo implements Comparable{
	public String doi;
	public String issnElectronic;
	public String journalTitle;
	public String articleTitle;
	public String volume;
	public String issue;
	public String page;
	public String year;
	public String contributors[];
	private String compid;
	
	//book specific
	private String isbn;
	private String series_title;
	private String volume_title;
	private String edition_number;
	private String component_number;
	
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getSeries_title() {
		return series_title;
	}
	public void setSeries_title(String seriesTitle) {
		series_title = seriesTitle;
	}
	public String getVolume_title() {
		return volume_title;
	}
	public void setVolume_title(String volumeTitle) {
		volume_title = volumeTitle;
	}
	public String getEdition_number() {
		return edition_number;
	}
	public void setEdition_number(String editionNumber) {
		edition_number = editionNumber;
	}
	public String getComponent_number() {
		return component_number;
	}
	public void setComponent_number(String componentNumber) {
		component_number = componentNumber;
	}

	
	
	
	/**
	 * @return Returns the articleTitle.
	 */
	public String getArticleTitle() {
		return articleTitle;
	}
	/**
	 * @param articleTitle The articleTitle to set.
	 */
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}
	/**
	 * @return Returns the contributors.
	 */
	public String[] getContributors() {
		return contributors;
	}
	/**
	 * @param contributors The contributors to set.
	 */
	public void setContributors(String[] contributors) {
		this.contributors = contributors;
	}
	/**
	 * @return Returns the doi.
	 */
	public String getDoi() {
		return doi;
	}
	/**
	 * @param doi The doi to set.
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}
	/**
	 * @return Returns the issnElectronic.
	 */
	public String getIssnElectronic() {
		return issnElectronic;
	}
	/**
	 * @param issnElectronic The issnElectronic to set.
	 */
	public void setIssnElectronic(String issnElectronic) {
		this.issnElectronic = issnElectronic;
	}
	/**
	 * @return Returns the issue.
	 */
	public String getIssue() {
		return issue;
	}
	/**
	 * @param issue The issue to set.
	 */
	public void setIssue(String issue) {
		this.issue = issue;
	}
	/**
	 * @return Returns the journalTitle.
	 */
	public String getJournalTitle() {
		return journalTitle;
	}
	/**
	 * @param journalTitle The journalTitle to set.
	 */
	public void setJournalTitle(String journalTitle) {
		this.journalTitle = journalTitle;
	}
	/**
	 * @return Returns the page.
	 */
	public String getPage() {
		return page;
	}
	/**
	 * @param page The page to set.
	 */
	public void setPage(String page) {
		this.page = page;
	}
	/**
	 * @return Returns the volume.
	 */
	public String getVolume() {
		return volume;
	}
	/**
	 * @param volume The volume to set.
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}
	/**
	 * @return Returns the year.
	 */
	public String getYear() {
		return year;
	}
	/**
	 * @param year The year to set.
	 */
	public void setYear(String year) {
		this.year = year;
	}
	/**
	 * @return Returns the compid.
	 */
	public String getCompid() {
		return compid;
	}
	/**
	 * @param compid The compid to set.
	 */
	public void setCompid(String compid) {
		this.compid = compid;
	}
	
	public int compareTo(Object o) {
		CitedByVo obj = (CitedByVo) o;
		return obj.year.compareTo(this.year);
	}
}
