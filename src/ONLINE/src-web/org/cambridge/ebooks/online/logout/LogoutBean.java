//package org.cambridge.ebooks.online.logout;
//
//import javax.faces.context.FacesContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//
//import org.cambridge.ebooks.online.jpa.user.User;
//import org.cambridge.ebooks.online.organisation.OrganisationWorker;
//import org.cambridge.ebooks.online.util.HttpUtil;
//import org.cambridge.ebooks.online.util.LogManager;
//import org.cambridge.ebooks.online.util.RedirectTo;
//import org.cambridge.ebooks.online.util.StringUtil;
//import org.cambridge.util.Misc;
//
//public class LogoutBean {
//	
//	public String logout() { 
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
//		
//		logout(session, HttpUtil.getHttpServletRequest());
//		
//		return RedirectTo.HOME_PAGE;
//	}
//	
//	public String logoutAthens() { 
//		
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
//		
//		// STEP 1 IF USER HAS LOGGED IN ON ATHENS, LOGOUT USER IN ATHENS AS WELL
//		String athensId = ( String ) session.getAttribute("athensId");
//		boolean hasAthensSession = StringUtil.isNotEmpty(athensId);
//		if ( hasAthensSession ) { 
//			
//			session.removeAttribute("athensId");
//			//session.removeAttribute("orgNames");
//			session.removeAttribute("userInfo");
//			
//			OrganisationWorker.removeOrganisationInSession( session );
//			
//			//OrganisationWorker.setOrganisationDisplayInSession( session );
//			
//			session.setAttribute("logoutAthens", true);
//		}
//		
//		return RedirectTo.HOME_PAGE;
//		
//	}
//	
//	public void logout(HttpSession session, HttpServletRequest request) { 
//		//CLEAR SESSION IN !CBO!
//		Object userObj = session.getAttribute("userInfo");
//		
//		if (userObj instanceof User) { 
//			String username = ((User) userObj).getUsername();
//			LogManager.info(this, username + " has logged out...");
//		}
//		
//		Object orgObj = session.getAttribute("orgName");
//		if (orgObj instanceof String) { 
//			String orgName = (String) orgObj;
//			LogManager.info(this, orgName + " has logged out...");
//		}
//		
//		Object athensObj = session.getAttribute("athensId");
//		String athensId = null;
//		if (athensObj instanceof String) {
//			athensId = (String) athensObj;
//		}
//		
//		Object shibbObj = session.getAttribute("shibbId");
//		String shibbId = null;
//		if (shibbObj instanceof String) {
//			shibbId = (String) athensObj;
//		}
//		
//		session.invalidate();
//		session = request.getSession(true);
//		
//		if (Misc.isNotEmpty(athensId)) {
//			session.setAttribute("athensId", athensId);
//		}
//		
//		if (Misc.isNotEmpty(shibbId)) {
//			session.setAttribute("shibbId", shibbId);
//		}
//		
////		Map<String, Object> sessionMap = saveSessionToMap(session);
//		
//		// Create new session
////		HttpSession newSession = request.getSession(true);
////		for(String key : sessionMap.keySet()) {
////			newSession.setAttribute(key, sessionMap.get(key));
////		}
//	}
//	
//	public void athensLogout(HttpSession session, HttpServletRequest request) {
//		String athensId = ( String ) session.getAttribute("athensId");
//		boolean hasAthensSession = StringUtil.isNotEmpty(athensId);
//		if ( hasAthensSession ) {
//			session.removeAttribute("athensId");
//		}
//	}
//	
//	public void shibbolethLogout(HttpSession session, HttpServletRequest request) {
//		String shibbId = ( String ) session.getAttribute("shibbId");
//		boolean hasShibbolethSession = StringUtil.isNotEmpty(shibbId);
//		if ( hasShibbolethSession ) {
//			session.removeAttribute("shibbId");
//		}
//	}
//	
////	private Map<String, Object> saveSessionToMap(HttpSession session) {
////		Enumeration<String> attrNames = session.getAttributeNames();
////		Map<String, Object> sessionMap = new HashMap<String, Object>();
////		
////		while(attrNames.hasMoreElements()) {
////			String attrName = attrNames.nextElement();
////			if(!attrName.contains("org") 
////					&& !attrName.contains("user") 
////					&& !attrName.equalsIgnoreCase("isAdmin")
////					&& !attrName.equalsIgnoreCase("checkedIp")) {
////				sessionMap.put(attrName, session.getAttribute(attrName));
////			}
////		}
////		
////		return sessionMap;
////	}
//	
//}
