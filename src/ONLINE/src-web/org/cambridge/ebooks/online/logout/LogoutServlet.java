//package org.cambridge.ebooks.online.logout;
//
///*
// * This is a temporary login servlet
// */
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.cambridge.ebooks.online.organisation.OrganisationWorker;
//import org.cambridge.ebooks.online.util.StringUtil;
//
//@SuppressWarnings("serial")
//public class LogoutServlet extends HttpServlet {
//	
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		doPost(req, resp);		
//	}
//	
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		logout(request, response);
//	}
//	
//	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException { 
//	
//		HttpSession session = request.getSession(false);
//		
//		if(session != null)
//		{
//			String athensId = ( String ) session.getAttribute("athensId");
//			String shibbId = ( String ) session.getAttribute("shibbId");
//			
//			boolean hasAthensSession = StringUtil.isNotEmpty(athensId);
//			boolean hasShibbSession = StringUtil.isNotEmpty(shibbId);
//			
//			if ( hasAthensSession ) 
//			{ 
//				session.removeAttribute("athensId");
//				session.setAttribute("logoutAthens", true);
//			}
//			else if(hasShibbSession)
//			{
//				session.removeAttribute("shibbId");
//				session.setAttribute("logoutShibb", true);
//			}
//			
//			//session.removeAttribute("orgNames");
//			session.removeAttribute("userInfo");
//			OrganisationWorker.removeOrganisationInSession( session );
//		}
//		
//	}
//	
//}
