package org.cambridge.ebooks.online.user;

import java.io.IOException;
import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.LoginManager;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class UserWorker {
	
	private static final Logger LOGGER = Logger.getLogger(UserWorker.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	
	public static void recordLoginTime( HttpSession session ) { 
		session.setAttribute( "loginTime", System.currentTimeMillis() );
	}	
	
//	public static void setUserInfoInSession( HttpSession session , User user ) { 
//		session.setAttribute("userInfo", user );
//	}
	
	public static User getUserInfoInSession(HttpSession session) {
		if(session != null)
		{
			Object obj = session.getAttribute("userInfo");
			if  ( obj instanceof User )
				return (User) obj; 
		}
		
		return null;
	}
		
	public static String getUserBodyIdInSession( HttpSession session ) {
		User user = getUserInfoInSession(session);
		if ( user != null )  {
			return user.getBodyId();
		} else { 
			return null;
		}
	}

	public static String[] getCJOLoginDetails( final String username, final String password ) throws IOException, ValidatorException  {
		return getCJOLoginDetails(null, username, password);
	}
	
	public static String[] getCJOLoginDetails( HttpServletRequest request,final String username, final String password ) throws IOException, ValidatorException  {
		String[] prop = null;
//		String cjoAuthenticationServlet = EBooksConfiguration.getProperty("cjo.authentication.url");
//		String[] params = new String[] {"userName","passWord"};
//		Object[] values = new Object[] { username , password };
		
		try { 
			String response = LoginManager.loginRedirect(request, username, password);			
			//String response = HttpUtil.connectTo( cjoAuthenticationServlet, params, values ).toString();
			System.out.println("response = " + response);
			LOGGER.info("getting CJO Login Details -----"   );
			boolean notAuthorized = response.indexOf("AUTHORIZED") == -1 ;
			if ( notAuthorized ) { 
				LOGGER.info("getCJOLoginDetails ----- FAILED"    );
				FacesMessage message = new FacesMessage("Invalid username/password");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(message);	
			} else {
				
				LOGGER.info("getCJOLoginDetails ----- SUCCESS"   );
				prop = response.split( HttpUtil.RESPONSE_DELIMETER );
				
			}
		} catch (ValidatorException ve) {
			throw new ValidatorException(new FacesMessage("Invalid username/password"));			
		} catch ( Exception ioe ) { 
			ioe.printStackTrace();
			throw new IOException(HttpUtil.SERVER_DOWN_MESSAGE);
		}
		LOGGER.info("getCJOLoginDetails -- prop -----" + Arrays.toString(prop));
		
		return prop;
	}
	
	public static String getOrganisationBodyIdInLoginDetails( String[] loginDetails )   {
		String result = null;
		boolean isAdministrator = loginDetails.length > 4 && StringUtil.isNumeric(loginDetails[4]);
		System.out.println("[UserWorker] isAdministrator: "+isAdministrator);
		System.out.println("[UserWorker] loginDetails.length: "+loginDetails.length);
		System.out.println("[UserWorker] loginDetails[0]: "+loginDetails[0]);
		System.out.println("[UserWorker] loginDetails[1]: "+loginDetails[1]);
		System.out.println("[UserWorker] loginDetails[2]: "+loginDetails[2]);		
		if (  isAdministrator ) { 
			result = loginDetails[4];
		}else{
			//remove, because if no amdin, there should be no body id 
			/*
			if(loginDetails[2].split("=").length > 1)
				result = loginDetails[2].split("=")[1].split(",")[0];
			*/	
		}
		System.out.println("userWorker result for body id = " + result);
		return result;
	}
	
//	public static void saveMembershipInSession( HttpSession session, String[] loginDetails ) { 
//		String[] result = null;
////		String societyId = System.getProperty("stahl.society.id");
////		boolean societyMember = false;
//		boolean hasMemberships = loginDetails.length > 2 && loginDetails[2].indexOf("MEMBERSHIPS") > -1;
//		
//		//to check if user is an admin
//		boolean isAdmin = loginDetails.length > 4  && loginDetails[3].indexOf("ADMINISTRATOR") > -1 && loginDetails[4] != null && !"".equals(loginDetails[4]);
//		
//		final String ORGANISATION = "0";
//		final String SOCIETY = "1";
//				
//		if (  hasMemberships ) {
//			String[] pair = loginDetails[2].split("=");
//			if ( pair.length == 2 ) { 
//				String memberships = pair[1];
//				result = memberships.split(",");
//				
////				ArrayList<String> orgNames = new ArrayList<String>();
////				ArrayList<String> orgIds = new ArrayList<String>();
//				
//				ArrayList<String> membershipNameList = new ArrayList<String>();
//				ArrayList<String> membershipIdList = new ArrayList<String>();
//				List<Organisation> membershipOrgs = new ArrayList<Organisation>();
//				
//				//ArrayList<String> consortiaIdList = new ArrayList<String>();
//				
////				boolean hasMembershipLogo = false;
//			
//				for ( String bodyId : result ) 
//				{ 
//					Organisation org = OrganisationWorker.findOrganisation( bodyId );
//					//String consortiaId = ConsortiaWorker.checkIfInConsortia( bodyId );
//					
//					//if(StringUtil.isNotEmpty(consortiaId))
//						//consortiaIdList.add(consortiaId);
//					
//					if ( org != null ) 
//					{ 
//						membershipNameList.add( org.getDisplayName() );
//						membershipIdList.add( bodyId );		
//						membershipOrgs.add(org);
//					}	
//						
//				}
//				/*
//				if(isAdmin)
//				{
//					session.setAttribute("isAdmin", isAdmin );
//					membershipIdList.add( loginDetails[4] );
//				}
//				*/
//				
//				if ( membershipNameList.size() > 0 ) 
//				{
//					String[] membershipNames = membershipNameList.toArray(new String[0]);
//					String[] membershipIds   = membershipIdList.toArray(new String[0]);					
//					
//					//session.setAttribute("membershipNames", membershipNames );  // StringUtil.join(membershipNames,',')
//					//session.setAttribute("membership", membershipIds );
//					//session.setAttribute("membershipOrgs", membershipOrgs);
//				}
//				
//				/*
//				if(consortiaIdList.size() > 0)
//				{
//					String[] consortiaIds   = consortiaIdList.toArray(new String[0]);
//					session.setAttribute("consortiaIds", consortiaIds );
//				}*/
//
//			}
//		}
//	}
	
	public static String[] getMembershipInSession( HttpSession session ) { 
		Object obj = session.getAttribute( "membership" );
		if ( obj instanceof String[] ) { 
			return (String[]) obj;
		} else { 
			return null;
		}
	}
	
//	public static void main(String[] args) {
//		
//		String[] loginDetails = 
//		{
//		"AUTHORIZED",
//		"AO",
//		"MEMBERSHIPS=214242,525252,252525",
	
	
//		"ADMINISTRATOR",
//		"1285204",
//		"2"
//		};
//		
//		System.out.println(getMembershipInLoginDetails(loginDetails));
//	}
	
	
//	public static void setUserInSession( HttpSession session, User user ) {
//		setUserInfoInSession( session, user );
//		//SessionCurrencyBean.setToSession( session, user.getCurrency(), user.getCountryId(), user.getCountryName());
//	}
	
	public static User findUserByBodyId( String bodyId ) {
		if ( StringUtil.isNumeric( bodyId ) ) { 
			EntityManager em = emf.createEntityManager();
			
			User result = null;
			try {
				Query query = em.createNamedQuery( User.SEARCH_USER_BY_BODY_ID );
				result = ( User ) query.setParameter( 1, bodyId ).getSingleResult();
				if ( result != null ) { 
					em.refresh(result);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			} finally {
				em.close();
			}
			return result;
		} else {
			return null;
		}
	}
	

	
	public static User findUserByMemberId( String memberId ) {
		if ( StringUtil.isNumeric( memberId ) ) { 
			EntityManager em = emf.createEntityManager();
			User user = em.find( User.class, memberId );
			if ( user != null ) { 
				em.refresh(user);
			}
			return user;
		} else {
			return null;
		}
	}
	
	public static void copyUpdateableFields( User copyToThisObject, User copyFromThisObject) { 
		copyToThisObject.setTitle( copyFromThisObject.getTitle());
		copyToThisObject.setFirstName( copyFromThisObject.getFirstName());
		copyToThisObject.setLastName(  copyFromThisObject.getLastName());
		copyToThisObject.setEmail(     copyFromThisObject.getEmail()) ;
		copyToThisObject.setCountryId(  copyFromThisObject.getCountryId());
		copyToThisObject.setStateId(  copyFromThisObject.getStateId());
		copyToThisObject.setTownCity(  copyFromThisObject.getTownCity() );
		copyToThisObject.setPostCode(  copyFromThisObject.getPostCode() );
		copyToThisObject.setAddress1(  copyFromThisObject.getAddress1());
		copyToThisObject.setAddress2(  copyFromThisObject.getAddress2() );
	}
	
	
	public static User clone( User copyFromThisObject) { 
		User copyToThisObject = new User();
		copyToThisObject.setTitle( copyFromThisObject.getTitle());
		copyToThisObject.setFirstName( copyFromThisObject.getFirstName());
		copyToThisObject.setLastName(  copyFromThisObject.getLastName());
		copyToThisObject.setEmail(     copyFromThisObject.getEmail()) ;
		copyToThisObject.setCountryId(  copyFromThisObject.getCountryId());
		copyToThisObject.setStateId(  copyFromThisObject.getStateId());
		copyToThisObject.setTownCity(  copyFromThisObject.getTownCity() );
		copyToThisObject.setPostCode(  copyFromThisObject.getPostCode() );
		copyToThisObject.setAddress1(  copyFromThisObject.getAddress1());
		copyToThisObject.setAddress2(  copyFromThisObject.getAddress2() );
		return copyToThisObject;
	}
	
	public static void updateUserDetails( User user ) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
//		User userInfo = em.find( User.class , user.getMemberId() ) ;
		em.merge(user);
		em.getTransaction().commit();
	    em.close();
	}
	
	
	
	
	
	
}

//return PersistenceUtil.searchEntity(new User(), User.SEARCH_USER_BY_BODY_ID, bodyId );


