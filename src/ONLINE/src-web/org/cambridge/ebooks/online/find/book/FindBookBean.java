package org.cambridge.ebooks.online.find.book;

import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.find.book.FindBook;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class FindBookBean {

	private static final Logger LOGGER = Logger.getLogger(FindBookBean.class);
	
	private String bodyId;
	private String eisbn;
	private String location;
	private String findBookId;
	
	private String initBookLocation;
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public void setInitBookLocation(String initBookLocation){
		this.initBookLocation = initBookLocation;
	}
	
	public String getInitBookLocation(){
		LOGGER.info("Initialize book location ");
		
		String eisbn = (String) HttpUtil.getHttpServletRequest().getParameter("eisbn");
		if(eisbn != null && eisbn.length() > 0)
			setEisbn(eisbn);
		
		String bodyId = (String) HttpUtil.getHttpServletRequest().getParameter("bodyId");
		if(bodyId != null && bodyId.length() > 0)
			setBodyId(bodyId);
		
		FindBook book = PersistenceUtil.searchEntity(
				new FindBook(),
				FindBook.SEARCH_BOOK_LOCATION,
				getBodyId(), getEisbn());
		
		if(book != null){
			setFindBookId(book.getFindBookId());
			setBodyId(book.getBodyId());
			setEisbn(book.getEisbn());
			setLocation(book.getLocation());
		}else{
			setLocation("");
			setFindBookId("");
		}
		
		return initBookLocation;
	}
	
	public void updateBookLocation(ActionEvent event){
		LOGGER.info("Update book location " + getLocation());
		
		FindBook book;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			book = em.find(FindBook.class, getFindBookId());
		
			if(book == null){
				book = new FindBook();
				book.setBodyId(getBodyId());
				book.setEisbn(getEisbn());
			}
			
			book.setLocation(getLocation());
			
			em.getTransaction().begin();
			em.merge(book);
			em.getTransaction().commit();
			
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			em.close();
		}
	}
	
	public String getFindBookId() {
		return findBookId;
	}
	public void setFindBookId(String findBookId) {
		this.findBookId = findBookId;
	}
	public String getBodyId() {
		return bodyId;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
	public String getEisbn() {
		return eisbn;
	}
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
