package org.cambridge.ebooks.online.countrystates;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.faces.model.SelectItem;

import org.cambridge.ebooks.online.jpa.countrystates.Country;
import org.cambridge.ebooks.online.jpa.countrystates.State;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author rvillamor
 *
 */

public class CountryStateMapWorker {
	
	public static LinkedHashMap<String,String> countryMap;
	public static LinkedHashMap<String,String> countryCurrencyMap;
	public static List<SelectItem> countrySelectItems;
	
	static { 
		if ( countryMap == null && countrySelectItems == null ) { 
			List<Country> countryList = PersistenceUtil.searchList(new Country(), Country.GET_COUNTRY_LIST);
			
			countryMap = new LinkedHashMap<String, String>();
			countryCurrencyMap = new LinkedHashMap<String, String>();
			
			countrySelectItems = new ArrayList<SelectItem>(); //[countryList.size()];
			
			Country UK = null;
			Country US = null;
			
			for  ( Country country : countryList ) { 
				countryMap.put(country.getCountryId(), country.getCountryName());
				countryCurrencyMap.put(country.getCountryId(), country.getCurrency());
				
				
				boolean notUKorUS = !(Country.UK.equals(country.getCountryId()) || Country.US.equals(country.getCountryId()));
				
				
				if ( notUKorUS ) { 
					countrySelectItems.add( new SelectItem(country.getCountryId(), country.getCountryName() ) );
				} else if ( Country.UK.equals(country.getCountryId())) { 
					UK = country; 
				} else if ( Country.US.equals(country.getCountryId()) ) { 
					US = country;
				} 

			}
			
			if ( UK !=null && US != null ) { 
				countrySelectItems.add( 0, new SelectItem(US.getCountryId(), US.getCountryName() ) );
				countrySelectItems.add( 1, new SelectItem(UK.getCountryId(), UK.getCountryName() ) );
			}
		}
	}
	
	public LinkedHashMap<String,String> getCountryMap() { 
		return countryMap;
	}
	
	public List<SelectItem> getCountrySelectItems(){
		return countrySelectItems;
	}
	
	public ArrayList<SelectItem> getStateSelectItems(final String countryId) { 
		ArrayList<State> stateList = PersistenceUtil.searchList(new State(),  State.GET_STATE_BY_COUNTRY_ID, countryId);
		
		ArrayList<SelectItem> stateitems =  new ArrayList<SelectItem>();
		
		State otherState = new State();
		otherState.setStateId("0");
		otherState.setStateName("Other");
		
		stateitems.add( new SelectItem(otherState.getStateId(), otherState.getStateName()));
		for ( State state : stateList) {
			if ( !"0".equals(state.getStateId())) { 
				stateitems.add( new SelectItem(state.getStateId(), state.getStateName()));
			} 
		}
		return stateitems;
	}
	

	public static LinkedHashMap<String, String> getStateMap(final String countryId) { 
		
		List<State> stateList = PersistenceUtil.searchList(new State(), State.GET_STATE_BY_COUNTRY_ID, countryId);
		
		LinkedHashMap<String, String> stateMap = new LinkedHashMap<String, String>();
		
		stateMap.put( "Other", "0" );
		for ( State state : stateList) { 
			if ( !"0".equals(state.getStateId())) {
				stateMap.put( state.getStateName() , state.getStateId());
			}
		}
		return stateMap;
	}
	
	//NOTE: same functionality with getStateMap(), the only difference is getStateMap() uses State Name as key
	public static LinkedHashMap<String, String> getCountyStateMap(final String countryId) { 
		
		List<State> stateList = PersistenceUtil.searchList(new State(), State.GET_STATE_BY_COUNTRY_ID, countryId);
		
		LinkedHashMap<String, String> stateMap = new LinkedHashMap<String, String>();
		
		stateMap.put( "0" , "Other");
		for ( State state : stateList) { 
			if ( !"0".equals(state.getStateId())) {
				stateMap.put( state.getStateId(),state.getStateName() );
			}
		}
		return stateMap;
	}
	
}
