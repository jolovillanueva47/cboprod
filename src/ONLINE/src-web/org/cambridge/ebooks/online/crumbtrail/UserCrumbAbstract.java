package org.cambridge.ebooks.online.crumbtrail;

import javax.servlet.http.HttpServletRequest;

public abstract class UserCrumbAbstract {
	public abstract void doBeforeAddingToStack(HttpServletRequest request, 
			CrumbtrailStack stack, CrumbtrailInfoBean bean);
	
	public abstract void doAfterAddingtToStack(HttpServletRequest request,
			CrumbtrailStack stack, CrumbtrailInfoBean bean);
}
