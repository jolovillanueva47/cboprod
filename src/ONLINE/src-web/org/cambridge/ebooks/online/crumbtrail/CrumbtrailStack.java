package org.cambridge.ebooks.online.crumbtrail;

import java.util.Stack;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author mmanalo
 *
 */
public class CrumbtrailStack extends Stack<CrumbtrailInfoBean> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5958739597872701126L;
	private static final int NOT_FOUND = -1;	
	public static final String CRUMB = "CRUMB_DO_NOT_OVERRIDE";
	
	
	/* static method */
	/**
	 * recreates the stack from session
	 * @param session
	 * @return
	 */
	public static CrumbtrailStack recreateStack(HttpSession session, boolean create){
		CrumbtrailStack stack = (CrumbtrailStack) session.getAttribute(CRUMB);
		if(stack == null || create){
			//create a new stack
			stack = createNewStack();
			session.setAttribute(CRUMB, stack);
		}
		return stack;
	}
	
	/**
	 * recreates the stack from session
	 * @param session
	 * @return
	 */
	public static CrumbtrailStack recreateStack(HttpSession session){
		return recreateStack(session, false);
	}
	
	
	public static CrumbtrailStack cleanStack(HttpSession session){
		CrumbtrailStack stack = recreateStack(session, false);
		stack.clear();
		return stack;
	}
	
	/**
	 * create a new stack
	 * @return
	 */
	private static CrumbtrailStack createNewStack(){
		return new CrumbtrailStack(); 
	}
	
	
	public void process(CrumbtrailInfoBean bean){
		
		//if empty add new bean
		if(this.empty()){
			this.push(bean);
			return;
		}
		
		//push or pop the element
		int index = containsExact(bean);
		//System.out.println("TEST INDEX = " + index);
		//System.out.println("SIZE INDEX = " + this.size());
		if(index == NOT_FOUND){
			//System.out.println("push");
			//this.push(bean);
			pushLogic(bean);
		}else{
			//already found
			//System.out.println("pop");
			removeUpToIndex(index);
		}
	}
	
	/**
	 * push or do not push depending if allow dups
	 * @param bean
	 */
	private void pushLogic(CrumbtrailInfoBean bean){
		String allowDups = bean.getAllowDups();
		if(CrumbtrailInfoBean.ALLOW_DUPS.equals(allowDups)){
			this.push(bean);
		}else if(CrumbtrailInfoBean.FIRST_PAGE.equals(allowDups)){
			while(!this.isEmpty()){
				this.pop();
			}
			this.push(bean);
		}else{			
			int index = containsJsp(bean);
			if(index != NOT_FOUND){
				removeUpToIndex(index);
				
				//do not retain even the last one, this should be replaced
				//remove up to index does not remove up to the current index
				this.pop();
			}
			this.push(bean);
		}
	}
	
	/**
	 * checks if the stack already has this element
	 * @param bean
	 * @return
	 */
	private int containsExact(CrumbtrailInfoBean bean){
		int stackSize = this.size();		
		for(int i=0; i < stackSize ;i++){
			CrumbtrailInfoBean stackBean = (CrumbtrailInfoBean) this.get(i);			
			if(stackBean.getExactUrl().equals(bean.getExactUrl())){				
				return i;				
			}
		}
		return NOT_FOUND;
	}
	
	
	/**
	 * checks if the stack already has this element
	 * @param bean
	 * @return
	 */
	private int containsJsp(CrumbtrailInfoBean bean){
		int stackSize = this.size();		
		for(int i=0; i < stackSize ;i++){
			CrumbtrailInfoBean stackBean = (CrumbtrailInfoBean) this.get(i);			
			if(stackBean.getJsp().equals(bean.getJsp())){				
				return i;				
			}
		}
		return NOT_FOUND;
	}
	
	
	
	/**
	 * remove all elements till the given index
	 * @param index
	 */
	private void removeUpToIndex(int index){
		int stackSize = this.size();
		//we want to retain the old
		for(int i = stackSize-1; i > 0 ; i--){			
			if(i == index){
				return;
			}
			this.pop();
			
		}
	}
	
	@Override
	public String toString() {
		int stackSize = this.size();
		StringBuffer sb = new StringBuffer();
		for(int i=0; i < stackSize ;i++){
			sb.append(this.get(i).getValue() + ">");
		}
		return sb.toString();
	}
	
	/**
	 * checks if this stack contains a particular jsp file.
	 * @param jsp
	 * @return
	 */
	public int containsJsp(String jsp){
		if(StringUtils.isEmpty(jsp)){
			return NOT_FOUND;
		}
		int stackSize = this.size();		
		for(int i=0; i < stackSize ;i++){
			CrumbtrailInfoBean stackBean = (CrumbtrailInfoBean) this.get(i);
			String jspStack = stackBean.getJsp();
			if(jsp.equals(jspStack)){
				return i;
			}
		}
		return NOT_FOUND;
	}
	
	
}
