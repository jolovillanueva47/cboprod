package org.cambridge.ebooks.online.crumbtrail;

public class CrumbtrailInfoBean {
	
	private static final String DOT_JSP = ".jsp";
	
	private static final String DOT_JSF = ".jsf";
	
	public static final String ALLOW_DUPS = "1";	
	
	public static final String DO_NOT_ALLOW_DUPS = "0";
	
	public static final String FIRST_PAGE = "F";
	
	private String jsp;
	private String value;
	private String exactUrl;
	private String allowDups;
	
	
	public CrumbtrailInfoBean(String jsp, String value, String exactUrl,
			String allowDups) {
		super();
		this.jsp = jsp;
		this.value = value;
		setExactUrl(exactUrl);
		this.allowDups = allowDups;
		
		if(this.value == null){
			
		}
	}
	
	public String getJsp() {
		return jsp;
	}
	public void setJsp(String jsp) {		
		this.jsp = jsp;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getExactUrl() {
		return exactUrl;
	}
	public void setExactUrl(String exactUrl) {
		this.exactUrl = exactUrl.replaceAll(DOT_JSP, DOT_JSF);;
	}

	public String getAllowDups() {
		return allowDups;
	}

	public void setAllowDups(String allowDups) {
		this.allowDups = allowDups;
	}
	
	
}
