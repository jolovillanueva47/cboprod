package org.cambridge.ebooks.online.crumbtrail;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.crumbtrail.subject_tree.SubjectTree;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.landingpage.BookBean;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.search.solr.SearchServlet;
import org.cambridge.ebooks.online.subjecttree.SeriesWorker;
import org.cambridge.ebooks.online.subjecttree.SubjectWorker;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * 
 * @author mmanalo
 *
 */
public class UserCrumb extends UserCrumbAbstract {
	
	protected static final String DOT_JSP = ".jsp";
	
	protected static final String DOT_JSF = ".jsf";
	
	protected static final String SEARCH_RESULTS = "adv_search_results";	
	
	protected static final String SUBJECT_TREE = "subject_tree";
	
	protected static final String HOME = "home";
	
	protected static final String SERIES = "series";
	
	protected static final String SERIES_TREE = "series_tree";
	
	protected static final String SUBJECT_LANDING = "/subject_landing.jsp";
	protected static final String SUBJECT_LANDING_JSF = "/subject_landing.jsp";
	protected static final String BOOK_LANDING = "/ebook.jsp";
	protected static final String CHAPTER_LANDING = "/chapter.jsp";
	protected static final String SERIES_LANDING = "/series_landing.jsp";
	protected static final String SEARCH_RESULTS_LANDING = "/search_results.jsp";
	
	protected static final String ALL_SERIES_LANDING = "/all_series.jsp";
	protected static final String ALL_SERIES_LANDING_JSF = "all_series.jsf";
	
	protected static final String SERIES_TREE_LANDING = "/series_tree.jsp";
	
	protected static final String SEARCH = "search";
	
	public static final String CRUMB = "CRUMB";
	public static final String SUBJECT_CODE = "SUBJECT_CODE";
	private static final String SUBJECT_CODE_PARAM = "subjectCode";
	private static final String SUBJECT_ID_PARAM = "subjectId";
	private static final String SERIES_CODE_PARAM = "seriesCode";
	public static final String UTF8 = "UTF-8";
	
	private static final String QUESTION_MARK_ESC = "\\?";
	private static final String QUESTION_MARK = "?";
	
	private static final String LESS_THAN = "&lt;";
	private static final String GREATER_THAN = "&gt;";
	
	private static final String BOOK_BEAN = "bookBean";
	
	private static final String SEARCH_TYPE_PARAM = "searchType";
	
	private static final String allSubjectBook = "allSubjectBook";
	
	
	
	private static final String allSeriesSubLevel = "allSeriesSubLevel";
	
	private static final Logger LOGGER = LogManager.getLogger(UserCrumb.class);
	
	/* public methods */
	
	public void doAfterAddingtToStack(HttpServletRequest request, 
			CrumbtrailStack stack, CrumbtrailInfoBean bean) {
		String path = request.getRequestURI();
		if(path.contains(SUBJECT_LANDING)){			
			doSubjectLandingCustom(request);
		}else if(path.contains(ALL_SERIES_LANDING)){
			doAllSeriesCustom(request);
		}else if(path.contains(SERIES_TREE_LANDING)){
			doClearBrowserBack(request);
		}else if(path.contains(SEARCH_RESULTS_LANDING)){
			doSearchLanding(request, stack);
		}
		
		clcCrumbTrail(request, stack);
		
		aaaCrumbtrail(request, stack);
	}
	
	public void doBeforeAddingToStack(HttpServletRequest request, 
			CrumbtrailStack stack, CrumbtrailInfoBean bean) {		
		String path = request.getRequestURI();
		String jsp = bean.getJsp();		
		if((SEARCH_RESULTS + DOT_JSP).equals(jsp)){
			String exactUrl = bean.getExactUrl();		
			exactUrl = exactUrl.replaceAll("&", "&amp;");
			bean.setExactUrl(exactUrl.replaceFirst((SEARCH_RESULTS + DOT_JSF), SEARCH));
		}else if((SUBJECT_TREE + DOT_JSP).equals(jsp) || (SERIES_TREE + DOT_JSP).equals(jsp) ){
			if(!stack.isEmpty()){
				String stackJsp = stack.peek().getJsp();			
				if(stackJsp.equals(HOME + DOT_JSP) || stackJsp.equals(SERIES + DOT_JSP)){
					String exactUrl = bean.getExactUrl();
					bean.setExactUrl(HttpUtil.removeURLParam(SUBJECT_CODE, exactUrl));
				}
			}
		}else if(path.contains(BOOK_LANDING)){
			doBookLandingCustom(request, null);
		}else if(path.contains(CHAPTER_LANDING)){
			doChapterLandingCustom(request);
		}else if(path.contains(SERIES_LANDING)){
			doSeriesLandingCustom(request);
		}
		
		//method for subject tree crumb
		fullCrumb(request, stack, bean);
		
	}
	
	

	
	/* private methods */
	private void doSearchLanding(HttpServletRequest request, CrumbtrailStack stack){
		CrumbtrailInfoBean search = stack.pop();
		stack = CrumbtrailStack.cleanStack(request.getSession());
		addHome(stack);		
		stack.push(search);
	}
	
	private void aaaCrumbtrail(HttpServletRequest request, CrumbtrailStack stack){
		if(isAAARequest(request)){
			for(CrumbtrailInfoBean bean : stack){
				String url = bean.getExactUrl();				
				if(url != null && !url.contains("aaa")){
					String s = url.substring(0,1);
					if(!"/".equals(s)){
						url = "/" + url;
					}
					if(url.indexOf("/") == 1){
						bean.setExactUrl("/aaa" + url);
						//System.out.println(bean.getExactUrl());
					}else{
						bean.setExactUrl("/aaa" + url);
					}
				}
			}
		}		
	}
	
	private boolean isAAARequest(HttpServletRequest request){
		String path = request.getRequestURI();
		boolean aaa = false;
		if(path.contains("aaa/") || path.contains("aaa_")){
			aaa = true;
		}
		return aaa;
	}
	
	
	protected void fullCrumb(HttpServletRequest request, 
			CrumbtrailStack stack, CrumbtrailInfoBean bean) {		
		String fullCrumb = request.getParameter(CRUMB);
		String subjectCode = request.getParameter(SUBJECT_CODE);
		if(StringUtils.isNotEmpty(fullCrumb) && StringUtils.isEmpty(subjectCode)){
			String url = getPreviousUrl(stack);
			if(url != null){
				parseCrumbParam(stack, fullCrumb, url);
			}
		}
	}	
	
	
	
	private String getPreviousUrl(CrumbtrailStack stack){
		if(!stack.isEmpty()){
			CrumbtrailInfoBean info =  stack.peek();		
			String url = info.getExactUrl().split(QUESTION_MARK_ESC)[0] + QUESTION_MARK;		
			return url;
		}
		return null;
	}
	
	
	private void parseCrumbParam(CrumbtrailStack stack, String fullCrumb, String prevUrl){
		String[] levels = fullCrumb.split(LESS_THAN);
		int level = 1;
		
		for(String subjNameCode : levels){
			String[] nameCode = subjNameCode.split(GREATER_THAN);
			CrumbtrailInfoBean info = new CrumbtrailInfoBean(
					//level will determine if there is a duplicate process
				String.valueOf(level),
				nameCode[0],
				getNewUrl(nameCode[1], prevUrl, fullCrumb),
				CrumbtrailInfoBean.DO_NOT_ALLOW_DUPS);			
			stack.process(info);
			level++;
		}
	}
	
	private String getNewUrl(String subjectCode, String url, String fullCrumb){		
		return url + "&amp;SUBJECT_CODE=" + subjectCode + "&amp;CRUMB=" + fullCrumb;		
	}
	
	private String getSubjectCodeId(HttpServletRequest request){
		String subjectCode = request.getParameter(SUBJECT_CODE_PARAM);
		if(subjectCode == null){
			subjectCode = request.getParameter(SUBJECT_ID_PARAM);
		}
		return subjectCode;
	}
	
	private void doSubjectLandingCustom(HttpServletRequest request){
		String subjectCode = getSubjectCodeId(request);		
		HttpSession session = request.getSession();
		CrumbtrailStack stack = CrumbtrailStack.cleanStack(session);	
		
		addHome(stack);
		addBrowseBySubject(stack);
		
		doTreeCustom(request, subjectCode, stack, true);
	}
	
	private void doTreeCustom(HttpServletRequest request, String subjectCode, CrumbtrailStack stack, boolean isSubject){
		List<EBookSubjectHierarchy> list = SubjectTree.createInstance().getTrail(subjectCode);
		for(EBookSubjectHierarchy s : list){
			CrumbtrailInfoBean info = new CrumbtrailInfoBean(
				//unimportant will overide custome logic anyway, hence ""
				"",
				s.getSubjectName(),
				recreateTreeCustomUrl(s, request, isSubject),
				"");
			stack.push(info);
		}
	}
	
	private void addHome(CrumbtrailStack stack){
		CrumbtrailInfoBean info = new CrumbtrailInfoBean(
				//unimportant will overide custome logic anyway, hence ""
				"",
				"Home",
				"home.jsf",
				"");
		stack.push(info);
	}
	
	private void addBrowseBySubject(CrumbtrailStack stack){
		CrumbtrailInfoBean info = new CrumbtrailInfoBean(
				//unimportant will overide custome logic anyway, hence ""
				"",
				"Browse by Subject",
				"subject_tree.jsf",
				"");
		stack.push(info);
	}
	
	private String recreateTreeCustomUrl(EBookSubjectHierarchy subject, HttpServletRequest request, boolean isSubject){
		String url = null;
		if(isSubject){
			url = recreateSubjectLandingCustomUrl(subject, request);
		}else{
			url = recreateSeriesLandingCustomUrl(subject, request);
		}
		return url;
	}
	
	private String recreateSubjectLandingCustomUrl(EBookSubjectHierarchy subject, HttpServletRequest request){		
		String path = SUBJECT_LANDING_JSF;
		//String subjectIdCodeParam = subject.getVistaSubjectCode() != null ? SUBJECT_CODE_PARAM : SUBJECT_ID_PARAM;
		String subjectIdCodeValue = null;
		String url = "";		
		//LOGGER.info("SUBJECT BRYAN vsubjectCode = " + subject.getVistaSubjectCode());
		//LOGGER.info("SUBJECT BRYAN subjectId = " + subject.getSubjectId());
		if("Y".equals(subject.getFinalLevel())){
			subjectIdCodeValue = subject.getVistaSubjectCode();
			//subjectIdCodeValue = subject.getSubjectId();
			url = path + "?searchType=allTitles&amp;subjectCode=" + subjectIdCodeValue + "&amp;subjectName=" + subject.getSubjectName();
		}else{			
			subjectIdCodeValue = subject.getSubjectId();
			//subjectIdCodeValue = subject.getVistaSubjectCode();
			url = path + "?searchType=allSubjectBook&amp;subjectId=" + subjectIdCodeValue + "&amp;subjectName=" + subject.getSubjectName();
		}
		
		return url;
	}
	
	private String recreateSeriesLandingCustomUrl(EBookSubjectHierarchy subject, HttpServletRequest request){		
		String path = ALL_SERIES_LANDING_JSF;		
		String url = path + "?searchType=allSubjectBook&amp;seriesCode=" + subject.getSubjectId()+ "&amp;firstLetter=" + subject.getSubjectName() 
				+ "&amp;seriesName=" + subject.getSubjectName();
		return url;
	}
	
	private void doBookLandingCustom(HttpServletRequest request, CrumbtrailStack stack){
		//get subjectCode for subject trail
		BookBean book = (BookBean) request.getAttribute(BOOK_BEAN);
		BookMetaData meta = book.getBookMetaData();
		String subjectCode = SubjectWorker.getFirstThreeLevelSubjectCode(meta.getSubjectCodeList());
		
		HttpSession session = request.getSession();
		if(stack == null){
			stack = CrumbtrailStack.cleanStack(session);
		}
		
		addHome(stack);
				
		addBrowseBySubject(stack);
		
		//add subject trail
		doTreeCustom(request, subjectCode, stack, true);	
		
		//ebook is automatically added by crumbtrail tag		
	}
	
	private void doChapterLandingCustom(HttpServletRequest request){		
		//do booklanding custom
		HttpSession session = request.getSession();
		CrumbtrailStack stack = CrumbtrailStack.cleanStack(session);
		doBookLandingCustom(request, stack);
		
		BookBean book = (BookBean) request.getAttribute(BOOK_BEAN);
		
		CrumbtrailInfoBean info = new CrumbtrailInfoBean(				
				"ebook.jsp",
				book.getBookMetaData().getTitle(),
				recreateBookLandingCustomUrl(book),
				CrumbtrailInfoBean.DO_NOT_ALLOW_DUPS);
		stack.push(info);
		
		//chapter is automatically added by crumtrail
	}
	
	private String recreateBookLandingCustomUrl(BookBean book){
		String url = "ebook.jsf?bid=" + book.getBookMetaData().getId();
		return url;
	}
	
	
	private void doAllSeriesCustom(HttpServletRequest request){
		String type = request.getParameter(SEARCH_TYPE_PARAM);
		if(allSubjectBook.equals(type) || allSeriesSubLevel.equals(type)){
			String seriesCode = request.getParameter(SERIES_CODE_PARAM);	
			HttpSession session = request.getSession();			
			CrumbtrailStack stack = CrumbtrailStack.recreateStack(session, true);
			
			addSeriesTreeTrail(stack);
			
			doTreeCustom(request, seriesCode, stack, false);
		}else{
			doClearBrowserBack(request);			
		}
		
	}
	
	
	/*
	 * method for accumalation of trail for browser back in this area
	 */
	private void doClearBrowserBack(HttpServletRequest request){		
		HttpSession session = request.getSession();
		CrumbtrailStack stack = CrumbtrailStack.recreateStack(session);
		int size = stack.size();
		if(size > 0){
			//remove the first and last element
			CrumbtrailInfoBean info1 = stack.get(0);
			CrumbtrailInfoBean info2 = stack.get(1);
			CrumbtrailInfoBean info3 = stack.get(size-1);
			
			//new stack and add the first and last
			CrumbtrailStack _stack = CrumbtrailStack.cleanStack(session);
			_stack.push(info1);
			_stack.push(info2);
			_stack.push(info3);
		}	
	}
	
	private void addSeriesTreeTrail(CrumbtrailStack stack){
		addHome(stack);
		
		//add series
		stack.push(new CrumbtrailInfoBean("series.jsp", "Browse by Series", "series.jsf", "0"));
		
		//add series_tree
		stack.push(new CrumbtrailInfoBean("series_tree.jsp", "Browse by Series Subject", "series_tree.jsf", "0"));
	}
	
	private void doSeriesLandingCustom(HttpServletRequest request){
		String pubcode = SearchServlet.publisherCode(request);
		if( pubcode != null && !"".equals(pubcode.toString())){
			//doSeriesLandingCustomCLC(request);
			doSeriesLandingCustomCBO(request);
		}else{
			doSeriesLandingCustomCBO(request);
		}
	}
	
	private void doSeriesLandingCustomCLC(HttpServletRequest request){
		CrumbtrailStack stack = CrumbtrailStack.cleanStack(request.getSession());
		addHome(stack);		
	}
	
	private void doSeriesLandingCustomCBO(HttpServletRequest request){
		String seriesCode = request.getParameter(SERIES_CODE_PARAM);	
		  
		HttpSession session = request.getSession();			
		CrumbtrailStack stack = CrumbtrailStack.cleanStack(session);
		
		addSeriesTreeTrail(stack);
				
		String subjectCode = SeriesWorker.getSubjectCodeFromSeries(seriesCode);
		doTreeCustom(request, subjectCode, stack, false);
		
		//add the trail
	}
	
	
	//TODO:temp method for CLC
	private void clcCrumbTrail(HttpServletRequest request, CrumbtrailStack stack){
		String pubcode = SearchServlet.publisherCode(request);
		if( pubcode != null && !"".equals(pubcode.toString()) && stack != null)
		{
			CrumbtrailInfoBean infoBean = stack.get(0);
			if(infoBean.getExactUrl().contains("home.jsf"))
			{
				infoBean.setExactUrl("/clc/home.jsf"); 
				infoBean.setValue("CLC Home"); 
			}
		}
		
	}
}

