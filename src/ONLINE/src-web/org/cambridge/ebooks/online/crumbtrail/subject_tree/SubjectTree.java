package org.cambridge.ebooks.online.crumbtrail.subject_tree;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.subjecttree.SubjectWorker;

/**
 * Based on c2Galang's SubjectWorker
 * @author mmanalo
 *
 */
public class SubjectTree extends LinkedHashMap<EBookSubjectHierarchy, Object>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3205423253079175986L;
	private static SubjectTree subjectTree;
	
	private SubjectTree(){
		if(SubjectWorker.allSubjectMap == null){
			SubjectWorker.buildAllSubjectMap();
		}
		this.putAll(SubjectWorker.allSubjectMap);
	}
	
	/* static methods */
	
	public static SubjectTree createInstance(){
		if(subjectTree == null){
			subjectTree = new SubjectTree();
		}
		return subjectTree;		 
	}	
	
	/* public methods */
	
	public List<EBookSubjectHierarchy> getTrail(String code){
		List<EBookSubjectHierarchy> list = new ArrayList<EBookSubjectHierarchy>();
		LinkedHashMap<EBookSubjectHierarchy, Object> map = new LinkedHashMap<EBookSubjectHierarchy, Object>();
		
		//generate the initial map
		for(EBookSubjectHierarchy subject : this.keySet()){			
			map.put(subject, this.get(subject));			
		}
		getTrail(code, list, map);
		return list;
	}
	
	/* private methods */
	private boolean matchedCode(List<EBookSubjectHierarchy> subjectList, String code){
		EBookSubjectHierarchy subject = subjectList.get(subjectList.size()-1);
		return matchedCode(subject, code);
	}
	
	private boolean matchedCode(EBookSubjectHierarchy subject, String code){
		if(code.equals(subject.getSubjectId()) || code.equals(subject.getVistaSubjectCode())){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private void getTrail(String code, List<EBookSubjectHierarchy> _hierarchy, Object object){
		List<EBookSubjectHierarchy> hierarchy =  _hierarchy;		
		if(object instanceof EBookSubjectHierarchy){
			EBookSubjectHierarchy subject = (EBookSubjectHierarchy) object;
			if(matchedCode(subject, code)){
				hierarchy.add(subject);
				return;
			}			
		}else{	
			//breadth first search
			LinkedHashMap<EBookSubjectHierarchy, Object> map =  (LinkedHashMap<EBookSubjectHierarchy, Object>) object;
			for(EBookSubjectHierarchy _subject : map.keySet()){
				if(matchedCode(_subject, code)){
					hierarchy.add(_subject);
					
					//should exit if found already
					return;
				}
			}		
			
			//go down one level if not found on this level
			for(EBookSubjectHierarchy _subject : map.keySet()){				
				hierarchy.add(_subject);				
				getTrail(code, hierarchy, map.get(_subject));
				if(!matchedCode(hierarchy, code)){
					hierarchy.remove(_subject);
				}else{
					return;
				}
			}				
		}
		return;
	}
	
	
}
