package org.cambridge.ebooks.online.crumbtrail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * 
 * @author mmanalo
 *
 */
public class CrumbtrailTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4343234459585630064L;
	
	private String propertyFile;
	
	private String styleId;
	
	private String styleClass;
	
	private String lastElementStyle;
	
	private String userCrumb;
	
	private String hide;
		
	@SuppressWarnings("unused")
	private static Logger LOGGER = LogManager.getLogger(CrumbtrailTag.class);

	public String getPropertyFile() {
		return propertyFile;
	}

	public void setPropertyFile(String propertyFile) {
		this.propertyFile = propertyFile;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public String getLastElementStyle() {
		return lastElementStyle;
	}

	public void setLastElementStyle(String lastElementStyle) {
		this.lastElementStyle = lastElementStyle;
	}
		
	public void setHide(String hide) {
		this.hide = hide;
	}

	public String getHide() {
		return hide;
	}
	
	/*
	 * business logic 
	 */
	
	public String getUserCrumb() {
		return userCrumb;
	}

	public void setUserCrumb(String userCrumb) {
		this.userCrumb = userCrumb;
	}



	private ResourceBundle rb;
	
	private static final String SEMI_COLON = ";";
	
	
	
//	private static final String QUESTION_MARK = "?";
	
	private static final String TRUE = "true";
	
	@SuppressWarnings("unused")
	private static final String FALSE = "false";
	
	/**
	 * initializes the properties in the selected properties file
	 * @throws Exception
	 */
	private void initProperties() throws Exception {
        this.rb = ResourceBundle.getBundle(this.getPropertyFile());
    }
	
	private boolean doHide(){
		if(StringUtils.isEmpty(this.hide)){
			return false;
		}
		if(TRUE.toLowerCase().equals(this.hide.toLowerCase())){
			return true;
		}
		return false;
	}
	
	private void writeToJsp(JspWriter out) throws IOException{
		if(doHide() == false){
			String html = writeJsp();
			
			out.print(html);
		}		
	}
	
	@Override
	public int doStartTag() throws JspException {
		//save to session
		JspWriter out = pageContext.getOut();
		
		try {			
			saveToSession();
			
			writeToJsp(out);
			//out.flush();
		} catch (Exception e) {			
			e.printStackTrace();
			throw new JspException();
		}
		//create html components
		return SKIP_BODY;		
	}	
	
	/**
	 * saves the history in session
	 * @throws Exception 
	 */
	private void saveToSession() throws Exception{
		HttpSession session = pageContext.getSession();
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();		
		initProperties();		
			
		ArrayList<String> values = getJspKeyValues(request);	
		
		CrumbtrailInfoBean bean = new CrumbtrailInfoBean(values.get(0), 
				values.get(1), values.get(2), values.get(3));		
				
		//modify bean
		prepareBean(bean);
		
		//do before adding to stack
		doBeforeAddingToStack(request, session, bean);

		addToStack(session, bean); 
		
		//do after adding to stack
		doAfterAddingToStack(request, session, bean);
	}
	
	
	private void doBeforeAddingToStack(HttpServletRequest request, 
			HttpSession session, CrumbtrailInfoBean bean) throws Exception {
		String userCrumb = this.userCrumb;
		if(userCrumb != null && userCrumb.trim().length() > 0){
			UserCrumbAbstract user = (UserCrumbAbstract) Class.forName(userCrumb).newInstance();
			user.doBeforeAddingToStack(request, CrumbtrailStack.recreateStack(session), bean);
		}
	}
	
	private void doAfterAddingToStack(HttpServletRequest request, 
			HttpSession session, CrumbtrailInfoBean bean) throws Exception {
		String userCrumb = this.userCrumb;
		CrumbtrailStack stack = CrumbtrailStack.recreateStack(session);
		if(userCrumb != null && userCrumb.trim().length() > 0){
			UserCrumbAbstract user = (UserCrumbAbstract) Class.forName(userCrumb).newInstance();
			user.doAfterAddingtToStack(request, CrumbtrailStack.recreateStack(session), bean);
		} 
		
		if(bean.getExactUrl().contains("allSubjectBook") && stack.size() > 2 && stack.get(stack.size()-2).getValue().equals(bean.getValue())){
			stack.pop();
		}
	}
	
	private static final String CONTAINS_EL = "${";
	/**
	 * modify bean if it contains $ in its label
	 * @param bean
	 * @throws Exception
	 */
	private void prepareBean(CrumbtrailInfoBean bean) throws Exception {
		String label = bean.getValue();
		if(label.contains(CONTAINS_EL)){
			String[] keyValArr = getKeyValuePairs(label);			
			Object obj = HttpUtil.getAttributeFromRequestOrSession(keyValArr[0]);			
			String labelFromBean = (String) BeanUtils.getNestedProperty(obj, keyValArr[1]);
			bean.setValue(labelFromBean);			
		}
	}
	
	
	
	
	
	
	private static final String BRACE_OPEN_ESCAPED = "\\{";
	
	private static final String BRACE_CLOSE = "}";
	
	private static final String DOT_ESCAPED = "\\.";
	/**
	 * split the label to get the key value pairs
	 * @param label
	 * @return
	 */	
	private String[] getKeyValuePairs(String label){
		label = label.split(BRACE_OPEN_ESCAPED)[1];
		label = label.split(BRACE_CLOSE)[0];
		return label.split(DOT_ESCAPED, 2);
	}	
	
	
	
	/**
	 * adds the current bean to the stack
	 * @param session
	 * @param bean
	 */
	private void addToStack(HttpSession session, CrumbtrailInfoBean bean){
		CrumbtrailStack stack = CrumbtrailStack.recreateStack(session);		
		stack.process(bean);	
		LOGGER.info(stack);
	}
	
	
	
	/**
	 * gets the jsp key
	 * @param request
	 * @return
	 */
	private String getJspKey(HttpServletRequest request){
		return request.getServletPath().substring(1);
	}
	
	/**
	 * get exact url
	 * @param request
	 * @return
	 */
	private String getExactUrl(HttpServletRequest request){
		return HttpUtil.getExactUrl(request);
	}
	

	
	/**
	 * retrieves values from property file
	 * @param request
	 * @return
	 */
	private ArrayList<String> getJspKeyValues(HttpServletRequest request) {	
		String key = getJspKey(request);
		String values = rb.getString(key);
		String label; 
		if(values != null){
			String[] valuesArr = values.split(SEMI_COLON);
			label = valuesArr[0];
			String allowDups = valuesArr[1];
			ArrayList<String> alVal = new ArrayList<String>();
			alVal.add(key);
			alVal.add(label);
			alVal.add(getExactUrl(request));
			alVal.add(allowDups);
			return alVal;
		}		
		return null;
	}
	
	/**
	 * writes the jsp output
	 */
	private String writeJsp(){
		StringBuilder sb = new StringBuilder();
		HttpSession session = pageContext.getSession();
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		
		sb.append("<ul");
		if (StringUtils.isNotEmpty(this.getStyleId())) {
			sb.append(" id=\"" + this.getStyleId() + "\"");
		}
		if (StringUtils.isNotEmpty(this.getStyleClass())) {
			sb.append(" class=\"" + this.getStyleClass() + "\"");
		}
		sb.append(">\n" );
		
		writeStackValues(session, sb, request);
		sb.append("</ul>");
		
		return sb.toString();
	}
	
	/**
	 * write values stored in the stack
	 * @param session
	 * @param sb
	 */
	private void writeStackValues(HttpSession session, StringBuilder sb, HttpServletRequest request){
		CrumbtrailStack stack = CrumbtrailStack.recreateStack(session);
		int stackSize = stack.size();
		for(int i = 0; i < stackSize; i++){
			writeStackValue(stack.get(i), sb, stackSize, i, request);
		}		
	}
	
	/**
	 * write values stored int the stack
	 * @param bean
	 * @param sb
	 * @param stackSize
	 * @param currentVal
	 * @param request 
	 */
	private void writeStackValue(CrumbtrailInfoBean bean, StringBuilder sb,
			int stackSize, int currentVal, HttpServletRequest request){
		String label = bean.getValue();
		String jsp = bean.getExactUrl();
			
		//this if condition checks if this is the last element in the stack
		if(stackSize - 1 != currentVal ){
			sb.append("<li><span class=\"icons_img breadcrumbs\">&nbsp;</span><a href='" + jsp +"'>" + label + "</a></li>\n");
		}else{
			sb.append("<li class='"+ this.lastElementStyle +"'><span class=\"icons_img breadcrumbs\">&nbsp;</span>" + label + "</li>\n");
		}
	}	
}
