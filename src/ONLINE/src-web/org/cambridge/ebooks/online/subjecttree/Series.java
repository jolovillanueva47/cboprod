package org.cambridge.ebooks.online.subjecttree;


public class Series {

	private String seriesCode;
	private String seriesName;
	private String seriesNumber;
	
	public boolean equals(Object obj) {
		if ( obj instanceof Series ) { 
			Series c = (Series) obj;
			return c.getSeriesCode().equals(this.getSeriesCode());
		} else {
			return false;
		}
	}
	
	public int hashCode() {
		return seriesCode.hashCode();
	}

	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
}
