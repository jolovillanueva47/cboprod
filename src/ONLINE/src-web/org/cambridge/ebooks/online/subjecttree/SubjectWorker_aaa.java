package org.cambridge.ebooks.online.subjecttree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;

/**
 * 
 * @author apirante
 * 
 */

public class SubjectWorker_aaa extends SubjectWorker {
	
	private static final Logger LOGGER = Logger.getLogger(SubjectWorker_aaa.class);
	
	//private static long lastDay;
	
	public static LinkedHashMap<EBookSubjectHierarchy, Object> parseAllSubjects() {
		if(null==allSubjectMap || allSubjectMap.size() == 0){
			buildData();
		}
		
		return allSubjectMap;
	}
	
	public static List<EBookSubjectHierarchy> getPathNodes(String path) {
		List<EBookSubjectHierarchy> nodes  = new ArrayList<EBookSubjectHierarchy>();
		
		String treePath[] = path.indexOf(";") == 0 ? path.substring(1).split(";") : path.split(";");
		LinkedHashMap<EBookSubjectHierarchy, Object> tmp = allSubjectMap;
		for (int x = 0 ; x<treePath.length ; x++) {
			EBookSubjectHierarchy parent = new EBookSubjectHierarchy(treePath[x]);
			
			if(tmp.get(parent) instanceof LinkedHashMap){
				
				Set<EBookSubjectHierarchy> result = tmp.keySet();
				
				EBookSubjectHierarchy ebs = getNodeObjectKey(parent, result);
				
				nodes.add(ebs);
				
				tmp = (LinkedHashMap<EBookSubjectHierarchy, Object>) tmp.get(parent);
			}else if(tmp.get(parent) instanceof EBookSubjectHierarchy){
				EBookSubjectHierarchy ebs = (EBookSubjectHierarchy) tmp.get(parent);
				nodes.add(ebs);
			}
			
		}
		return nodes;
	}
	
	public static  List<EBookSubjectHierarchy> getAllLeaves(){
		testMethod();
		return null;
	}

	public static void testMethod(){
		
		LinkedHashMap<EBookSubjectHierarchy, Object> allSubjectMap = SubjectWorker.allSubjectMap;
		testRecursion(allSubjectMap);
		
	}
	
	public static void testRecursion(LinkedHashMap<EBookSubjectHierarchy, Object> allSubjectMap) {
		Iterator it = allSubjectMap.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			
			EBookSubjectHierarchy key = (EBookSubjectHierarchy) pairs.getKey();
			//System.out.println("id="+key.getSubjectId()+"; name="+key.getSubjectName());
			Object obj = pairs.getValue();
			if(obj instanceof LinkedHashMap ){
				LinkedHashMap<EBookSubjectHierarchy, Object> value = (LinkedHashMap<EBookSubjectHierarchy, Object>) pairs.getValue();
				testRecursion(value);	
			}else if(obj instanceof EBookSubjectHierarchy){
				
				EBookSubjectHierarchy ebs = (EBookSubjectHierarchy) obj;
				
				//System.out.println("leaves == id="+ebs.getSubjectId()+"; name="+ebs.getSubjectName());
				
			}
			
		}

	}
	
	private static EBookSubjectHierarchy getNodeObjectKey(EBookSubjectHierarchy parent, Set<EBookSubjectHierarchy> set){
		
		List<EBookSubjectHierarchy> list = new ArrayList<EBookSubjectHierarchy>(set); 

		EBookSubjectHierarchy ebs = (EBookSubjectHierarchy)list.get(list.indexOf(parent));
		
		return ebs;
		
		
	}
	
	
	private static void buildData(){

		boolean letsSearchForToday;
		long now = System.currentTimeMillis() / (1000*60*60*24);
		if (lastDay == 0) {
			letsSearchForToday = true;
			lastDay = now;
		} else {
			letsSearchForToday = now > lastDay;
		}
		
		if (letsSearchForToday) {
			lastDay = now;
			LOGGER.info("Searching all the subjects for the day.");
			buildAllSubjectMap();
		}

		
	}
	

	
}
	


