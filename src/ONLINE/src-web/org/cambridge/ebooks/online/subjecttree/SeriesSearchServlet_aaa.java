package org.cambridge.ebooks.online.subjecttree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;


/**
 * 
 * @author apirante
 * 
 */

public class SeriesSearchServlet_aaa extends HttpServlet {
	/**
	 * 
	 */
	
	//TODO: THIS SERVLET AND comparator may not be used
	private static final long serialVersionUID = 1L;
	
	//
	private List<EBookSubjectHierarchy> allSubjects; 
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("DUMAAN SA SERIES SERVLET"); 
		
		//get all available subjects
		//allSubjects = SeriesWorker_aaa.getAvailableSubjects();
		
		
//		System.out.println("before====");
//		for (EBookSubjectHierarchy bookSubjectHierarchy : allSubjects) {
//			System.out.println("book="+bookSubjectHierarchy.getSubjectName());
//		}	
//		
		Collections.sort(allSubjects, new EBookSubjectHierarchyComparator_aaa());
		
//		System.out.println("after====");
//		for (EBookSubjectHierarchy bookSubjectHierarchy : allSubjects) {
//			System.out.println("book="+bookSubjectHierarchy.getSubjectName());
//		}	
		
		
		System.out.println("before="+allSubjects.size());		
		
		//GET MAJOR SUBJECTS
		List<EBookSubjectHierarchy> majorSubjects = getChildNodes("");
		
		System.out.println("Major subjects returned [" + majorSubjects.size()+"] items.");
		
		//get major roots
		for (EBookSubjectHierarchy bookSubjectHierarchy : majorSubjects) {
			System.out.println("book="+bookSubjectHierarchy.getSubjectName());
			//check the child nodes of each
			boolean results = checkChildNodes(bookSubjectHierarchy);
			if(results == false){
				allSubjects.remove(bookSubjectHierarchy);
			}
			
		}	
		
		
		
		System.out.println("after="+allSubjects.size());
		
		request.setAttribute("arrayObj_aaa", allSubjects);
		
		request.getRequestDispatcher("/aaa/series_tree.jsf?").forward(request, response);
		
	}
	
  
	private boolean checkChildNodes(EBookSubjectHierarchy parent){
		
		//dont check child objects if parent is already finallevel
		List<EBookSubjectHierarchy> childObjects = getChildNodes(parent.getSubjectId());
		
		//if has childObjects
		if(!childObjects.isEmpty()){
			
			int count = 1;
			
			//scan child
			
			Iterator< EBookSubjectHierarchy > it = childObjects.iterator();
			
			while (it.hasNext()) {
				  EBookSubjectHierarchy bshier = it.next();
				  
				  boolean result = checkChildNodes(bshier);
				 
				  if(result == false){
						if("Y".equals(bshier.getFinalLevel())){
							
							HashSet<Series> seriesObjects = SeriesWorker_aaa.getSeriesObjects(bshier.getVistaSubjectCode());
							
							if(seriesObjects.isEmpty()){
								allSubjects.remove(bshier);
								count = count + 1;
							}
						}
					}
			}
			if(count > childObjects.size()){
				//remove the parent
				allSubjects.remove(parent);
			}
			
		}else{
			return false;
		}
		
		return true;
		
	}
	
	
	private List<EBookSubjectHierarchy> getChildNodes(String subjectId){
		
		List<EBookSubjectHierarchy> results = new ArrayList<EBookSubjectHierarchy>();
		
		for (EBookSubjectHierarchy bshier : allSubjects) {	
			String parentId = bshier.getParentId();
			if (subjectId.equals((parentId == null ? "" : parentId))) {
				results.add(bshier);
			}
		}
				
		return results;
		
	}
	
	
	
	
	
	
	

}
