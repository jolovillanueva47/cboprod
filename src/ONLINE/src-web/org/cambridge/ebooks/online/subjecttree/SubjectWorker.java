package org.cambridge.ebooks.online.subjecttree;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author jgalang
 * 
 */

public class SubjectWorker {
	
	private static Logger LOGGER = Logger.getLogger(SubjectWorker.class);
	
	protected static long lastDay;
//	public static ArrayList<EBookSubjectHierarchy> allSubjectList;
	public static LinkedHashMap<EBookSubjectHierarchy, Object> allSubjectMap;
	
	
	
	@SuppressWarnings("unchecked")
	public static void buildAllSubjectMap() {
		ArrayList<EBookSubjectHierarchy> allSubjectList = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL);
		allSubjectMap = new LinkedHashMap<EBookSubjectHierarchy, Object>();
		
		for (EBookSubjectHierarchy ebs : allSubjectList) {
			LinkedHashMap<EBookSubjectHierarchy, Object> map = null;
			if (ebs.getSubjectLevel() == 1) {
				map = new LinkedHashMap<EBookSubjectHierarchy, Object>();
				allSubjectMap.put(ebs, map);
			} else {
				
				String path[] = ebs.getPath().substring(1).split(";");
				LinkedHashMap<EBookSubjectHierarchy, Object> tmp = allSubjectMap;
				
				boolean hasSeries = ebs.getSeriesCount() > 0;
				
				for (String subjectId : path) {
					
					if (hasSeries) {
						for (EBookSubjectHierarchy e : tmp.keySet()) {
							if (subjectId.equals(e.getSubjectId())) {
								e.setSeriesCount(e.getSeriesCount() + ebs.getSeriesCount());
								break;
							}
						}
					}
					
					if (!subjectId.equals(ebs.getSubjectId())) {
						EBookSubjectHierarchy parent = new EBookSubjectHierarchy(subjectId);
						
						tmp = (LinkedHashMap<EBookSubjectHierarchy, Object>) tmp.get(parent);
					} else {
						if (ebs.getFinalLevel().equals("N")) {
							EBookSubjectHierarchy parent = new EBookSubjectHierarchy(subjectId);
							map = (LinkedHashMap<EBookSubjectHierarchy, Object>) tmp.get(parent);
							if (map == null) map = new LinkedHashMap<EBookSubjectHierarchy, Object>();
							
							tmp.put(ebs, map);
						} else {
							tmp.put(ebs, ebs);
						}
						
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Set<EBookSubjectHierarchy> getSubjectsSet(String path) {
		 
		boolean letsSearchForToday;
		long now = System.currentTimeMillis() / (1000*60*60*24);
		if (lastDay == 0) {
			letsSearchForToday = true;
			lastDay = now;
		} else {
			letsSearchForToday = now > lastDay;
		}
		
		if (letsSearchForToday) {
			lastDay = now;
			LOGGER.info("Searching all the subjects for the day.");
			buildAllSubjectMap();
//			allSubjectList = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL);
		}
		
		String treePath[] = path.indexOf(";") == 0 ? path.substring(1).split(";") : path.split(";");
		LinkedHashMap<EBookSubjectHierarchy, Object> tmp = allSubjectMap;
		for (int x = 0 ; x<treePath.length ; x++) {
			EBookSubjectHierarchy parent = new EBookSubjectHierarchy(treePath[x]);
			tmp = (LinkedHashMap<EBookSubjectHierarchy, Object>) tmp.get(parent);
		}
		
		Set<EBookSubjectHierarchy> result = tmp.keySet();
		
		return result;
	}
	
	public static JSONArray getSubjectsFromStaticList(String path) {

		
		JSONArray result = new JSONArray();
		result.addAll(getSubjectsSet(path));
		
		
		/*JSONArray result = new JSONArray();
		for (EBookSubjectHierarchy ebs : allSubjectList) {
			if (null != ebs.getParentId() && ebs.getParentId().equals(subjectId)) {
				result.add(ebs);
			}
		}*/
		 
				
		return result;
	}
	
	public static JSONArray getAllLeavesJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SUBJECT, parentId);
		result.addAll(list);
		return result;
	}
	
	@Deprecated
	public static JSONArray getAvailableSubjectsJSONArray(String subjectId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_BY_PARENT, subjectId);
		result.addAll(list);
		return result;
	}
	
	public static String getFirstThreeLevelSubjectCode(List<String> subjectCodes){
		String code = null;
		for(String _code : subjectCodes){
			if(_code.length() == 3){
				code = _code;  
				break;
			}
		}
		code = code == null ? subjectCodes.get(0) : code;
		return code;
	}
	
}
