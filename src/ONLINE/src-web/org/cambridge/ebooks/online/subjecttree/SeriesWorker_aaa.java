package org.cambridge.ebooks.online.subjecttree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

/**
 * 
 * @author apirante
 * 
 */

public class SeriesWorker_aaa implements EBooksProperties {
	
	private static final Logger LOGGER = Logger.getLogger(SeriesWorker_aaa.class);
	
	private HashSet<Series> seriesObjects;	
	private static HashMap<String, HashSet<Series>> mAllSeries;
	private static LinkedHashMap<EBookSubjectHierarchy, Object> series_allSubjectMap;
	
	
	public static LinkedHashMap<EBookSubjectHierarchy, Object> getSeriesTree() {
		if(null == mAllSeries || mAllSeries.size() == 0){
			LOGGER.info("Building series objects for this day");;
			List<EBookSubjectHierarchy> list = getAllLeaves();
			buildSeriesObjects(list);
		}

		return series_allSubjectMap;
	}
	
	public static LinkedHashMap<EBookSubjectHierarchy, Object> parseAllSubjects() {
		series_allSubjectMap = SubjectWorker_aaa.parseAllSubjects();
		return series_allSubjectMap;
	}
	
	public HashSet<Series> getSeriesObjects() {
		//String subjectCode = HttpUtil.getHttpServletRequestParameter("subjectCode");
		String subjectCode = (String) HttpUtil.getAttributeFromRequestOrSession("subjectCode");

		
		//seriesObjects = getSeriesObjects(subjectCode);
		
		seriesObjects = getSeriesObjects_fromStaticVariable(subjectCode);
		
		
		if (seriesObjects.isEmpty()) {
			seriesObjects = null;
		}
		return seriesObjects;
				
	}
	
	public static HashSet<Series> getSeriesObjects_fromStaticVariable(String subjectCode) {
		//String subjectCode = HttpUtil.getHttpServletRequestParameter("subjectCode");
		//String subjectCode = (String) HttpUtil.getAttributeFromRequestOrSession("subjectCode");
		
		HashSet<Series> results = mAllSeries.get(subjectCode);
//		System.out.println("subjectcode="+subjectCode+"; resutls="+results.size());
//		for (Series series : results) {
//			System.out.println("      id="+series.getSeriesCode()+"; name="+series.getSeriesName());
//		}
		return results;
				
	}
	

	private static void buildSeriesObjects(List<EBookSubjectHierarchy> list){

		mAllSeries = new HashMap<String, HashSet<Series>>();
		//add the series from index
		for (EBookSubjectHierarchy ebs : list) {
		
			HashSet<Series> seriesSet = SeriesWorker_aaa.getSeriesObjects(ebs.getVistaSubjectCode());
			//System.out.println("adding to allseries= id=" + ebs.getSubjectId() + "; name="+ebs.getSubjectName()+"; vistaid="+ebs.getVistaSubjectCode());
			mAllSeries.put(ebs.getVistaSubjectCode(), seriesSet);
				
			
		}
		
		
		
		
		
	}
	

	
	public static  List<EBookSubjectHierarchy> getAllLeaves(){
		List<EBookSubjectHierarchy> results = new ArrayList<EBookSubjectHierarchy>();
		getLeavesRecursion(series_allSubjectMap, results);
		return results;
	}
	
	public static void getLeavesRecursion(LinkedHashMap<EBookSubjectHierarchy, Object> allSubjectMap, List<EBookSubjectHierarchy> list) {
		Iterator it = allSubjectMap.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			
			EBookSubjectHierarchy key = (EBookSubjectHierarchy) pairs.getKey();
			//System.out.println("id="+key.getSubjectId()+"; name="+key.getSubjectName());
			Object obj = pairs.getValue();
			if(obj instanceof LinkedHashMap ){
				LinkedHashMap<EBookSubjectHierarchy, Object> value = (LinkedHashMap<EBookSubjectHierarchy, Object>) pairs.getValue();
				getLeavesRecursion(value,list);	
			}else if(obj instanceof EBookSubjectHierarchy){
				
				EBookSubjectHierarchy ebs = (EBookSubjectHierarchy) obj;
				list.add(ebs);
				//System.out.println("leaves == id="+ebs.getSubjectId()+"; name="+ebs.getSubjectName());
			}
			
		}

	}
	
	
	
	public static HashSet<Series> getSeriesObjects(String subjectCode) {
		HashSet<Series> result = new HashSet<Series>();
		HashSet<Series> tmp = new HashSet<Series>();
		
		List<BookAndChapterSolrDto> docs = SolrUtil.getSeriesDetailsBySubjectCode(subjectCode);		
		
		for (BookAndChapterSolrDto doc : docs) {
			if (StringUtils.isNotEmpty(doc.getSeriesCode())) {
				Series series = new Series();
				series.setSeriesCode(doc.getSeriesCode());
				series.setSeriesName(doc.getSeries());
				if(doc.getSeriesNumber() > 0) {
					series.setSeriesNumber(String.valueOf(doc.getSeriesNumber()));
				}
				if (tmp.add(series)) {
					result.add(series);
				}
			}
		}
		
		return result;

	}
	
	
	
	
	
	
	
	
}
