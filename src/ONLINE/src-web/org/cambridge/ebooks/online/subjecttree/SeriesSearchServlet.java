package org.cambridge.ebooks.online.subjecttree;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

/**
 * 
 * @author jgalang
 * 
 */

public class SeriesSearchServlet extends HttpServlet {	
	
	private static final Logger LOGGER = Logger.getLogger(SeriesSearchServlet.class);
	
	private static final long serialVersionUID = -6414472964562670446L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean getSeries = "1".equals(request.getParameter("getSeries"));
		String subjectCode = request.getParameter("subjectCode");
		String path = request.getParameter("path");
		
		JSONArray arrayObj = null;
		if (getSeries) {
			arrayObj = SeriesWorker.getSeriesJSONArray(subjectCode);
		} else {
//			arrayObj = SeriesWorker.getAvailableSubjectsJSONArray(subjectId);
			arrayObj = SeriesWorker.getSubjectsFromStaticList(path);
		}
		
		LOGGER.info("JSON search. path: " + path + ". Results: " + arrayObj.size());

		response.setContentType("text/json");
		PrintWriter out = response.getWriter();
		out.println(arrayObj);
	}
}
