package org.cambridge.ebooks.online.subjecttree;

import java.util.Comparator;

import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;

//public class EBookSubjectHierarchyComparator_aaa implements Comparator<Object> {
//	
//		public int compareTo(EBookSubjectHierarchy o) {
//			if (!(o instanceof EBookSubjectHierarchy))
//				throw new ClassCastException();
//
//			EBookSubjectHierarchy e = (EBookSubjectHierarchy) o;
//
//			return o.getSubjectId().compareTo(e.getSubjectId());
//		}
//
//		public int compare(Object o1, Object o2) {
//			if (!(o1 instanceof EBookSubjectHierarchy) || !(o2 instanceof EBookSubjectHierarchy))
//		        throw new ClassCastException();
//
//		      EBookSubjectHierarchy e1 = (EBookSubjectHierarchy) o1;
//		      EBookSubjectHierarchy e2 = (EBookSubjectHierarchy) o2;
//
//		      return (int) (e1.getSubjectId().compareTo(e2.getSubjectId()));
//			
//		}
//
//		
//}


public class EBookSubjectHierarchyComparator_aaa implements Comparator<EBookSubjectHierarchy> {
//TODO: THIS CLASS MAY BE UNUSED
	public int compare(EBookSubjectHierarchy o1, EBookSubjectHierarchy o2) {
		return o1.getSubjectId().compareTo(o2.getSubjectId());
	}
}

//public class EBookSubjectHierarchyComparator_aaa implements Comparator<EBookSubjectHierarchy> {
//
//	private String type;
//	
//	public EBookSubjectHierarchyComparator_aaa() {
//		type = "subject";
//	}
//	public EBookSubjectHierarchyComparator_aaa(String type) {
//		this.type = type;
//	}
//	
//	public int compare(EBookSubjectHierarchy o1, EBookSubjectHierarchy o2) {
//		
//		int res = -1;
//		
//		if(type.equals("subject")){
//			res = o1.getSubjectId().compareTo(o2.getSubjectId());
//		}else if(type.equals("finallevel")){
//			res = o1.getFinalLevel().compareTo(o2.getFinalLevel());
//		}
//		
//		return res;
//	}
//}