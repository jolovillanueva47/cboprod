package org.cambridge.ebooks.online.subjecttree;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author apirante
 * 
 */

public class SubjectSearchServlet_aaa extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//TODO: THIS SERVLET MAY NOT BE INUSED 
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//String path = request.getParameter("paths");
		
		//List<EBookSubjectHierarchy> arrayObj = getAvailableSubjectsNotJSONArray_aaa();
		
		//Collections.sort(arrayObj, new EBookSubjectHierarchyComparator_aaa());

		//LogManager.info(SubjectSearchServlet_aaa.class, "JSON search_aaa. subjectId: Results: " + arrayObj.size());
				
		//request.setAttribute("arrayObj_aaa", arrayObj);
		
//		//List<EBookSubjectHierarchy> arrayObj = SubjectWorker_aaa.getAllLeaves();
//		Collections.sort(arrayObj, new EBookSubjectHierarchyComparator_aaa());
//		request.setAttribute("arrayObj_aaa", arrayObj);
		
		//response.sendRedirect("/aaa/subject_tree.jsf");
		request.getRequestDispatcher("/aaa/subject_tree.jsf").forward(request, response);
			
	}	
	
}
