package org.cambridge.ebooks.online.subjecttree;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

/**
 * 
 * @author jgalang
 * 
 */

public class SubjectSearchServlet extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(SubjectSearchServlet.class);
	
	private static final long serialVersionUID = 7158812453808784124L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String path = request.getParameter("path");
		
//		JSONArray arrayObj = SubjectWorker.getAvailableSubjectsJSONArray(subjectId);
		JSONArray arrayObj = SubjectWorker.getSubjectsFromStaticList(path);
		
		LOGGER.info("JSON search. subjectId: " + path + ". Results: " + arrayObj.size());
		
		response.setContentType("text/json");
		PrintWriter out = response.getWriter();
		
		out.println(arrayObj);
		
		out.close();
	}
}
