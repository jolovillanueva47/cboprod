package org.cambridge.ebooks.online.subjecttree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

/**
 * 
 * @author jgalang
 * 
 */

public class SeriesWorker implements EBooksProperties {
	
	public static JSONArray getSubjectsFromStaticList(String path) {

		JSONArray result = new JSONArray();
		
		Set<EBookSubjectHierarchy> set = SubjectWorker.getSubjectsSet(path);
		Set<EBookSubjectHierarchy> noSeries = new HashSet<EBookSubjectHierarchy>();
		
		for (EBookSubjectHierarchy e : set) {
			if (e.getSeriesCount() < 1) {
				noSeries.add(e);
			}
		}
		set.removeAll(noSeries);
		result.addAll(set);
		
		
		return result;
	}
	
	public static JSONArray getAvailableSubjectsJSONArray(String subjectId) {
		JSONArray result = new JSONArray();

		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SERIES_SEARCH_BY_PARENT, subjectId);
		result.addAll(list);
		return result;
	}
	
	public static JSONArray getAllLeavesJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES, parentId);
		result.addAll(list);
		return result;
	}
	
	public static JSONArray getSeriesSublevelJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES_SUBLEVEL, parentId);
		result.addAll(list);
		return result;
	}
	
	public static JSONArray getSeriesJSONArray(String subjectCode) {
		JSONArray result = new JSONArray();
		HashSet<Series> tmp = new HashSet<Series>();
		
		List<BookAndChapterSolrDto> docs = SolrUtil.getSeriesDetailsBySubjectCode(subjectCode);		
		
		for (BookAndChapterSolrDto doc : docs) {
			if (StringUtils.isNotEmpty(doc.getSeriesCode())) {
				Series series = new Series();
				series.setSeriesCode(doc.getSeriesCode());
				series.setSeriesName(doc.getSeries());
				if(doc.getSeriesNumber() > 0) {
					series.setSeriesNumber(String.valueOf(doc.getSeriesNumber()));
				}
				if (tmp.add(series)) {
					result.add(series);
				}
			}
		}
		
		return result;
	}
	
	
	public static String getSubjectCodeFromSeries(String seriesCode){
		List<BookAndChapterSolrDto> docs = SolrUtil.getSubjectBySeriesCode(seriesCode);		
		BookAndChapterSolrDto any = docs.get(0);				
		return SubjectWorker.getFirstThreeLevelSubjectCode(any.getSubjectCodeList());
	}
	
	
	
	
}
