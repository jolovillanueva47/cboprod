package org.cambridge.ebooks.online.trackback;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author Karlson A. Mulingtapang
 * TrackbackBean.java - Trackbacks
 */
public class TrackbackBean {

	private String referenceId;	
	private String title;
	private String blogName;
	private String blogUrl;
	private String authorIp;
	private Date modifiedDate;
	private String excerpt;
	private String agent;
	
	private String initializeTrackbackList;
	
	private List<TrackbackBean> trackbacks;
	
	/**
	 * @return the initializeTrackbackList
	 */
	public String getInitializeTrackbackList() {	
		String bid = HttpUtil.getHttpServletRequestParameter("bid");
		String cid = HttpUtil.getHttpServletRequestParameter("cid");
		
		String id = "";
		if(StringUtils.isNotEmpty(cid) && !cid.equals("null")) {
			id = cid;
		} else {
			id = bid;
		}
		
		TrackbackWorker worker = new TrackbackWorker();
		List<TrackbackBean> trackbacks = worker.searchByReferenceId(id);
		
		this.trackbacks = trackbacks;

		return "initialize";
	}
	
	/**
	 * @return the referenceId
	 */
	public String getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the blogName
	 */
	public String getBlogName() {
		return blogName;
	}
	/**
	 * @param blogName the blogName to set
	 */
	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}
	/**
	 * @return the blogUrl
	 */
	public String getBlogUrl() {
		return blogUrl;
	}
	/**
	 * @param blogUrl the blogUrl to set
	 */
	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}
	/**
	 * @return the authorIp
	 */
	public String getAuthorIp() {
		return authorIp;
	}
	/**
	 * @param authorIp the authorIp to set
	 */
	public void setAuthorIp(String authorIp) {
		this.authorIp = authorIp;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the excerpt
	 */
	public String getExcerpt() {
		return excerpt;
	}
	/**
	 * @param excerpt the excerpt to set
	 */
	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}
	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}
	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}
	/**
	 * @return the trackbacks
	 */
	public List<TrackbackBean> getTrackbacks() {
		return trackbacks;
	}
	/**
	 * @param trackbacks the trackbacks to set
	 */
	public void setTrackbacks(List<TrackbackBean> trackbacks) {
		this.trackbacks = trackbacks;
	}	
}
