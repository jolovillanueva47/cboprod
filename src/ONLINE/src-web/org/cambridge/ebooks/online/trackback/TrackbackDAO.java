package org.cambridge.ebooks.online.trackback;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.trackback.Trackbacks;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil.PersistentUnits;

/**
 * @author Karlson A. Mulingtapang
 * TrackbackDAO.java - Trackback Data Access Object
 */
public class TrackbackDAO {
	private static EntityManagerFactory emf = PersistenceUtil.emf;

	static {
		emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS
				.toString());
	}
	
	public static List<Trackbacks> searchByReferenceId(String referenceId) {
		List<Trackbacks> result;

		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(Trackbacks.SEARCH_BY_REFERENCE_ID);

		try {
			query.setParameter(1, referenceId);
			result = (List<Trackbacks>) query.getResultList();
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	};
	
	public static void updateTrackback(TrackbackBean trackbackBean) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createNamedQuery(Trackbacks.UPDATE);

		try {
			query.setParameter(1, trackbackBean.getReferenceId());
			query.setParameter(2, trackbackBean.getBlogUrl());
			query.setParameter(3, trackbackBean.getTitle());
			query.setParameter(4, trackbackBean.getBlogName());
			query.setParameter(5, trackbackBean.getAuthorIp());
			query.setParameter(6, trackbackBean.getExcerpt());
			query.setParameter(7, trackbackBean.getAgent());
			query.setParameter(8, trackbackBean.getReferenceId());
			query.setParameter(9, trackbackBean.getTitle());
			query.setParameter(10, trackbackBean.getBlogName());
			query.setParameter(11, trackbackBean.getBlogUrl());
			query.setParameter(12, trackbackBean.getAuthorIp());
			query.setParameter(13, trackbackBean.getExcerpt());
			query.setParameter(14, trackbackBean.getAgent());
			query.executeUpdate();
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			em.close();
		}
	};
}
