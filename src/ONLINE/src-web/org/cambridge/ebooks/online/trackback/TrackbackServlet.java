package org.cambridge.ebooks.online.trackback;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Karlson A. Mulingtapang
 * TrackbackServlet.java - Handles Trackback
 */
public class TrackbackServlet extends HttpServlet {
	
	private static final String CONTENT_TYPE_XML = "text/xml";
	private static final String CHANNEL_TITLE = "channel.title";
	private static final String RSS_2 = "rss_2.0";
	public static final String EBOOKS_CONTEXT_PATH = "ebooks.context.path";
	private static final String CONTENT_TYPE_HTML = "text/HTML";
	
	private TrackbackWorker worker = new TrackbackWorker();
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		this.doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		saveTrackback(req);
	}
	
	private void saveTrackback(HttpServletRequest req) {
		String id = req.getParameter("id");
		String title = req.getParameter("title");
		String blogName = req.getParameter("blog_name");
		String url = req.getParameter("url");		
		String excerpt = req.getParameter("excerpt").length() > 2000 
			? req.getParameter("excerpt").substring(0, 2001) : req.getParameter("excerpt");
		String remoteIp = req.getRemoteAddr();
		String agent = req.getHeader("User-Agent");
		
		TrackbackBean trackbackBean = new TrackbackBean();
		trackbackBean.setReferenceId(id);
		trackbackBean.setTitle(title);
		trackbackBean.setBlogName(blogName);
		trackbackBean.setBlogUrl(url);
		trackbackBean.setExcerpt(excerpt);
		trackbackBean.setAuthorIp(remoteIp);
		trackbackBean.setAgent(agent);
		
		worker.updateTrackback(trackbackBean);
	}
}
