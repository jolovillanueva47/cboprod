package org.cambridge.ebooks.online.trackback;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.online.jpa.trackback.Trackbacks;


/**
 * @author Karlson A. Mulingtapang
 * TrackbackWorker.java - Description
 */
public class TrackbackWorker {
	
	public List<TrackbackBean> searchByReferenceId(String referenceId) {
		try {
			return populateTrackbackBean(TrackbackDAO.searchByReferenceId(referenceId));
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public void updateTrackback(TrackbackBean trackbackBean) {
		TrackbackDAO.updateTrackback(trackbackBean);
	}
	
	private List<TrackbackBean> populateTrackbackBean(List<Trackbacks> records)
		throws InvocationTargetException, IllegalAccessException {

		if(records == null) {
			return null;
		}
		
		List<TrackbackBean> beans = new ArrayList<TrackbackBean>();
		TrackbackBean bean = null;
		
		for (Trackbacks record : records) {
			bean = new TrackbackBean();
			BeanUtils.copyProperties(bean, record);
			beans.add(bean);
		}
		
		return beans;
	}
}
