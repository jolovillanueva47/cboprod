package org.cambridge.ebooks.online.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.user.DESHEXUtil;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.CJOLoginBean;
import org.cambridge.ebooks.online.login.RelogBean;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.subscription.FreeTrialBean;
import org.cambridge.ebooks.online.user.UserWorker;
import org.cambridge.ebooks.online.util.UrlUtil;

/**
 * 
 * @author jgalang
 * @modified rvillamor
 * @modified mmanalo - add doSetAccessList
 */

public class AuthenticationFilter implements Filter {
	
	private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class);
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	
	public static final String IP_MEMBERSHIPS = "IPmembership";
	
	public static final String COMMA = ",";
	
	public static final String FREE_TRIAL_YES = "FREE_TRIAL_YES";
	
	public static final String FREE_TRIAL_NO = "FREE_TRIAL_NO";
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void doFilter(ServletRequest httpRequest, ServletResponse httpResponse, FilterChain chain ) throws IOException, ServletException { 
		LOGGER.info("=== Start Filter ===");
		
		HttpServletRequest 	filteredRequest  = (HttpServletRequest)  httpRequest;
		HttpServletResponse response = (HttpServletResponse) httpResponse;			
				
		
		
		LOGGER.info("Start");		
		
		try {
			doLogoutRequest(filteredRequest, response);
		} catch (Exception e) {
			throw new IOException(e);
		}
		
		if("y".equals(filteredRequest.getParameter("lo")) || "y".equals(filteredRequest.getParameter("slo"))
				|| "y".equals(filteredRequest.getParameter("alo"))){
			return;
		}
		
		doAutoLogin(filteredRequest, response);		
		
		doShibbolethLogin(filteredRequest, response);
		
		doAthensLogin(filteredRequest, response);
		
		doRelogin(filteredRequest, response);
		
		LOGGER.info("=== End Filter ===");
		
		chain.doFilter(filteredRequest, response);
	
	}
	
	
	@SuppressWarnings("unused")
	private static String SERIALIZE_PATH = "serialize.dir";

	
//	/**
//	 * stores a comma seperated body Ids in session via key parameter
//	 * @param request
//	 * @param organisationList
//	 * @param key
//	 */
//	private void storeOrgsInSession(HttpServletRequest request, 
//			List<Organisation> organisationList, String key ){
//		int orgSize = organisationList.size();
//		String[] bodyIdArr = new String[orgSize];
//		LogManager.info(this.getClass(), "Athens or Shibb = " + key);
//		for (int i = 0; i < orgSize; i++) {
//			bodyIdArr[i] = organisationList.get(i).getBodyId();
//			LogManager.info(this.getClass(), bodyIdArr[i]);
//		} 
//			
//		String orgList = StringUtil.join(bodyIdArr, ',');
//		HttpSession session = request.getSession();
//		
//		session.setAttribute(key, orgList);		
//	}
	
	public static final String ATHENS_BODY_IDS = "ATHENS_BODY_IDS";
	
	public void doAthensLogin ( HttpServletRequest request, HttpServletResponse response ) { 
		String athensId = 		request.getParameter("ath_id");
		String athensUsername = request.getParameter("ath_user");		
		doAthensLogin(request, response, athensId, athensUsername);
	}
	
	/**
	 * Sets user details via Athens Login
	 * Account used for testing:
	 * Username: cjo_personal_t
	 * Password: test123
	 * 
	 * @param request
	 * @param response
	 */
	public void doAthensLogin ( HttpServletRequest request, HttpServletResponse response, String athensId, String athensUsername ) { 
		HttpSession session = request.getSession();
		
		
		boolean noAthensIdInSession = null == OrgConLinkedMap.getFromSession(session).getAthensId();  
		boolean performAthensLogin = StringUtils.isNotEmpty(athensId) && StringUtils.isNotEmpty(athensUsername);
		
		
		if ( noAthensIdInSession && performAthensLogin ) { 
			
			LOGGER.info("Performing Athens log in :" + athensUsername);
			
			List<Organisation> organisationList = OrganisationWorker.findOrganisationByAthensId(athensId);			
			
			
			if(organisationList != null){
				
				OrgConLinkedMap.addOrgFromAthensOrShibb(organisationList, request, true, athensId);
				
				//storeOrgsInSession(request, organisationList, ATHENS_BODY_IDS);
				
				//Organisation organisation = organisationList.get(0);
				
				//OrganisationWorker.setOrganisationInSession( session, organisation);
				
				//OrganisationWorker.setOrganisationDisplayInSession( request.getSession() );
				
				OrganisationWorker.trackOrganisationLogin( session );
				
				//session.setAttribute("athensId",  athensId );
				
				//this is required for mulitple login to logout successfully	
				RelogBean.saveLogin(request, RelogBean.ATH);
				
				//required to recheck free trial
				FreeTrialBean.recheckFreeTrial(session);
							
				LOGGER.info("Organisation " + organisationList.get(0).getName() + " is validated by Athens" );
			} else {
				LOGGER.info("No organisation affiliated to Athens" );
			}	
		}
	}
	
	public static final String ORGANISATION_LIST = "ORGANISATION_LIST";
	
	
	
	
	
		
	private void doLogoutRequest(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		String logoutRequest = request.getParameter("lo");
		boolean logoutRequestPresent = StringUtils.isNotEmpty(logoutRequest);
		if ( logoutRequestPresent ) { 
			//new LogoutBean().logout(request.getSession(), request);
			RelogBean.logout(request, RelogBean.MAIN);
		}

		String athensLogoutRequest = request.getParameter("alo");
		boolean athensLogoutRequestPresent = StringUtils.isNotEmpty(athensLogoutRequest);
		if ( athensLogoutRequestPresent ) { 
			//new LogoutBean().athensLogout(request.getSession(), request);
			RelogBean.logout(request, RelogBean.ATH);
		}

		String shibbolethLogoutRequest = request.getParameter("slo");
		boolean shibbolethLogoutRequestPresent = StringUtils.isNotEmpty(shibbolethLogoutRequest);
		if ( shibbolethLogoutRequestPresent ) {
			//new LogoutBean().shibbolethLogout(request.getSession(), request);
			RelogBean.logout(request, RelogBean.SHB);
		}
		
		//for proper logging out
		if(logoutRequestPresent || athensLogoutRequestPresent || shibbolethLogoutRequestPresent ){
			HttpSession session = request.getSession();
			String id = session.getId();
			RelogBean.put(id, RelogBean.recreateRelogBean(request));
			
			
			
			Cookie[] cookies = request.getCookies();
			Cookie sessCookie = null;
			for(Cookie cookie : cookies){
				if("JSESSIONID".equals(cookie.getName())){
					cookie.setMaxAge(0);
					sessCookie = cookie;
				}
			}
			response.addCookie(sessCookie);			
			session.invalidate();
			doLogoutRedirect(request, response, id);
		}
	}
	
	private void doLogoutRedirect(HttpServletRequest request, HttpServletResponse response, String id) throws Exception {
		String path = request.getServletPath();		
		String fullpath = "";
		if(path.contains("/aaa/")){
			if(path.contains("/clc/")){
				fullpath = UrlUtil.getClientDns(request) + "aaa/clc/home.jsf?JSESS=" + id;
			}else{
				fullpath = UrlUtil.getClientDns(request) + "aaa/home.jsf?JSESS=" + id;
			}
		}else if(path.contains("/clc/")){
			fullpath = UrlUtil.getClientDns(request) + "clc/home.jsf?JSESS=" + id;
		}
		else{
			fullpath = UrlUtil.getClientDns(request) + "home.jsf?JSESS=" + id;
		}	
		String ip = request.getParameter("ip");
		if(StringUtils.isNotEmpty(ip)){
			fullpath = fullpath + "&ip=" + ip;
		}
		response.sendRedirect(fullpath);
	}
	
	
	private boolean checkIfUserExistsInSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		Object bean = session.getAttribute("userInfo");
		
		if(bean instanceof User){
			User user = (User) bean;
			if(StringUtils.isNotEmpty(user.getUsername())){
				return true;
			}
		}
		
		return false;
	}

	
	
	private void doAutoLogin( HttpServletRequest request, HttpServletResponse response ){ 
		String authLoginId = request.getParameter("lid");
		
		LOGGER.info("before lid: " + authLoginId);
		
		//does not to auto log if user already exist in session
		if(checkIfUserExistsInSession(request)){
			return;
		}
		
		LOGGER.info("lid: " + authLoginId);
		
		if (StringUtils.isNotEmpty(authLoginId)) { 
			HttpSession session = request.getSession();
			String bodyId = DESHEXUtil.decrypt( authLoginId );
			
			LOGGER.info("bodyid: " + bodyId);
			
			User userInfo = null;
			boolean checkByMemberId = StringUtils.isNotEmpty(request.getParameter("checkmid")); 
			
			if (checkByMemberId) {
				userInfo = UserWorker.findUserByMemberId(bodyId);
			} else { 
				userInfo = UserWorker.findUserByBodyId(bodyId);
			}
				
			if (userInfo != null) { 
				//UserWorker.setUserInfoInSession( session, userInfo );
				
				try { 
					String[] loginDetails = UserWorker.getCJOLoginDetails(request, userInfo.getUsername(), userInfo.getPassword());
					
					//required for seperating Ebooks login from CJO Login
					CJOLoginBean.setEncryptedDetails(request, userInfo.getEncryptedDetails());
					
					//required to recheck free trial
					FreeTrialBean.recheckFreeTrial(session);
					
					//UserWorker.setUserTypeInSession( session ,loginDetails[1] );
					
					UserWorker.recordLoginTime(session);
					
					//OrganisationWorker.removeOrganisationInSession( session );
					
					//String orgBodyId  = UserWorker.getOrganisationBodyIdInLoginDetails( loginDetails );

					//if ( StringUtil.isNumeric( orgBodyId ) ) 
					//{ 
						//Organisation organisation = new Organisation();
						//organisation.setBodyId( orgBodyId );
						//organisation = OrganisationWorker.findOrganisation( organisation.getBodyId() );
						//OrganisationWorker.setOrganisationInSession( session , organisation);
					//}
					
					//UserWorker.saveMembershipInSession( session ,loginDetails );
					//OrganisationWorker.setOrganisationDisplayInSession( session );
					OrganisationWorker.trackOrganisationLogin(session);
					
				} catch (Exception e) { 
					LOGGER.info("Error in doAutoLogin()!!! ");
				}	
			}
		}
	}
	
	public static final String SHIBB_BODY_IDS = "SHIBB_BODY_IDS";
	
	public void doShibbolethLogin ( HttpServletRequest request, HttpServletResponse response ) {
		String shibbId = request.getParameter("shib_id");
		doShibbolethLogin(request, response, shibbId);
	}
	
	/**
	 * Sets user details via Shibboleth.
	 * Account used for testing: 
	 * username: gavena
	 * password: g0d0fr3d0
	 * 
	 * @param request
	 * @param response
	 */
	public void doShibbolethLogin (HttpServletRequest request, HttpServletResponse response, String shibbId) {
		HttpSession session = request.getSession();
		
		
		boolean noShibbIdInSession = null == OrgConLinkedMap.getFromSession(session).getShibbolethId();  
		boolean performShibbolethLogin = StringUtils.isNotEmpty(shibbId);
		
		if (noShibbIdInSession && performShibbolethLogin) { 
			LOGGER.info("Performing Shibboleth log in shibbId:[" + shibbId + "]");
			
			List<Organisation> organisationList = OrganisationWorker.findOrganisationByShibbId(shibbId);
			
			if(organisationList != null) {
				OrgConLinkedMap.addOrgFromAthensOrShibb(organisationList, request, false, shibbId);
				//storeOrgsInSession(request, organisationList, SHIBB_BODY_IDS);
				//Organisation organisation = organisationList.get(0);
				//OrganisationWorker.setOrganisationInSession( session, organisation);
				//OrganisationWorker.setOrganisationDisplayInSession( request.getSession() );
				OrganisationWorker.trackOrganisationLogin( session );
				
				//session.setAttribute("shibbId",  shibbId );
				
				//this is required for mulitple login to logout successfully	
				RelogBean.saveLogin(request, RelogBean.SHB);
				
				//required to recheck free trial
				FreeTrialBean.recheckFreeTrial(session);
				
				LOGGER.info("Organisation " + organisationList.get(0).getName() + " is validated by Shibboleth" );
			} else {
				LOGGER.info("No organisation affiliated to Shibboleth " );
			}
		}
	}
	
	private void doRelogin(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = request.getParameter("JSESS");
		if(sessionId != null){
			RelogBean rb = RelogBean.map.get(sessionId);
			if(rb != null) {
				request.getSession().setAttribute(RelogBean.RELOGIN_BEAN, rb);
				RelogBean.map.remove(sessionId);
				RelogBean.relog(request, response);
			}
		}
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
}
