package org.cambridge.ebooks.online.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class SessionManagerFilter implements Filter {

	private static final Logger logger = Logger.getLogger(SessionManagerFilter.class);
	
	private FilterConfig filterConfig;
	
	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest _request = (HttpServletRequest)request; 
		String uri = _request.getRequestURI();
		logger.info("uri:" + uri);
		
		if(!uri.contains("search_results.jsf") 
				&& !uri.contains("search")
				&& !uri.contains("pdf_viewer.jsf")
				&& !uri.contains("login.jsf")) {
						
			if(!uri.contains("ebook.jsf") && !uri.contains("chapter.jsf")) {
				_request.getSession().removeAttribute("hithighlight");
				logger.info("remove session attribute: hithighlight");

				_request.getSession().removeAttribute("facet");
				logger.info("remove session attribute: facet");
			}
		}
		else if(!uri.contains("ebook.jsf") && !uri.contains("chapter.jsf")
				 && !uri.contains("email_link.jsf") && !uri.contains("export_citation.jsf")){
			_request.getSession().removeAttribute("printDateSession");
			logger.info("remove session attribute: printDateSession");
			
			_request.getSession().removeAttribute("onlineDateSession");
			logger.info("remove session attribute: onlineDateSession");
			
			_request.getSession().removeAttribute("bookTitleSession");
			logger.info("remove session attribute: bookTitleSession");
			
			_request.getSession().removeAttribute("doiSession");
			logger.info("remove session attribute: doiSession");
			
			_request.getSession().removeAttribute("chapterDoiSession");
			logger.info("remove session attribute: chapterDoiSession");
			
			_request.getSession().removeAttribute("authorNamesSession");
			logger.info("remove session attribute: authorNamesSession");
			
			_request.getSession().removeAttribute("exactURLSession");
			logger.info("remove session attribute: exactURLSession");
			
			_request.getSession().removeAttribute("onlineIsbnSession");
			logger.info("remove session attribute: onlineIsbnSession");
			
		}
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
}
