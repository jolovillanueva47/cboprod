package org.cambridge.ebooks.online.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.society.autologin.SocietyAutoLoginWorker;

/**
 * 
 * @author mmanalo
 *
 */
public class SocietyAutoLoginFilter implements Filter {
		
	private static final Logger LOGGER = Logger.getLogger(SocietyAutoLoginFilter.class);
	
	/* implements */
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;	
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
		checkAutoLoginSociety((HttpServletRequest) request, (HttpServletResponse) response);		
		chain.doFilter(request, response);
	}
	
	
	public static final String AUTOLOGIN_ID = "autologinId";
	
	
	/* private methods */
	
	private void checkAutoLoginSociety(HttpServletRequest request, HttpServletResponse response){		
		if(isAutologinRequest(request)){
			String loginId = getLoginId(request);
			try{
				SocietyAutoLoginWorker worker = new SocietyAutoLoginWorker(loginId);
				if(isAutologinRequest(request) && worker.isValidReferrer(request)){		
					worker.loginSociety(request, response);
				}			
			}catch (IllegalArgumentException e) {
				e.printStackTrace();
				LOGGER.info("Login ID is invalid: " + request.getQueryString() );
			}
		}
		
	}
	
	private boolean isAutologinRequest(HttpServletRequest request){
		String queryString = request.getQueryString();
		if(queryString != null && queryString.contains(AUTOLOGIN_ID)){
			return true;
		}
		return false;		
	}
	
	private String getLoginId(HttpServletRequest request){
		String loginId = request.getParameter(AUTOLOGIN_ID);
		return loginId;
	}
	
	
	
	
	
}
