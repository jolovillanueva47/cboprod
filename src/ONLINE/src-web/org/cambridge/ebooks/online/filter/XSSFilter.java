package org.cambridge.ebooks.online.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class XSSFilter implements Filter{
	
	private static final Logger logger = Logger.getLogger(XSSFilter.class);
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {		
		HttpServletRequest _request = (HttpServletRequest) request;
		String uriPath = _request.getRequestURI();
		
		if(uriPath.indexOf("/open_pdf") < 0) {			
			response.setCharacterEncoding("UTF-8");
			request.setCharacterEncoding("UTF-8");
		}
		
		XSSFilteredHttpServletRequest filteredRequest = new XSSFilteredHttpServletRequest(_request);		
		
		if(isCached(filteredRequest, (HttpServletResponse) response)) {
			logger.info(filteredRequest.getRequestURI() + " is cached...");			
			return;
		}
							
		chain.doFilter(filteredRequest, response);
	}
	
	private boolean isCached(HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
	
		boolean isCached = false;
		String requestURI = request.getRequestURI();
		
		// cache faq and help
		if(requestURI.contains("seriesSearch")
				|| requestURI.contains("subjectSearch")) {
			
			
			// Thu, 18 Mar 2010 10:55:44 GMT
			SimpleDateFormat dateFormatter = new SimpleDateFormat("E, dd MMM yyyy H:m:s z"); 	
			TimeZone timeZone = dateFormatter.getTimeZone();
			timeZone.setID("GMT");
			dateFormatter.setTimeZone(timeZone);
			
			Calendar calNow = GregorianCalendar.getInstance();
			calNow.setTimeZone(timeZone);			
			
			String lastModified = request.getHeader("If-Modified-Since");
			if(lastModified == null) {
				String dateNow = dateFormatter.format(calNow.getTime());
				
				calNow.add(Calendar.YEAR, 1); // plus 1 yr
				String expiryDate = dateFormatter.format(calNow.getTime());
				
				response.setHeader("Last-Modified", dateNow);
				response.setHeader("Cache-Control", "public");
				response.setHeader("Expires", expiryDate);
			} else {
				isCached = true;
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			}			
		}		
		
		return isCached;
	}

}
