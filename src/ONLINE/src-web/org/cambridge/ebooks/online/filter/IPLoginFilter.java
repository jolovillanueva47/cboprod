package org.cambridge.ebooks.online.filter;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.login.authentication.OrgConLoginWorker;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

public class IPLoginFilter implements Filter {
	
	private static final Logger LOGGER = Logger.getLogger(IPLoginFilter.class);
		
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		LOGGER.info("=== Start Filter ===");
		
		/* logs */		
		long beforeIpLoginTime = new Date().getTime();		
		/* logs */
		
		doIpLogin((HttpServletRequest) request, (HttpServletResponse) response);
		
		/* logs */
		long afterIpLoginTime = new Date().getTime();
		LOGGER.info("IP Login Time = " + (afterIpLoginTime - beforeIpLoginTime)/1000.0d);
		/* logs */
		
		LOGGER.info("=== End Filter ===");		
		chain.doFilter(request, response);
	}
	
	private void doIpLogin(HttpServletRequest request, HttpServletResponse response) {		
		HttpSession session = request.getSession();
		LOGGER.info("Checking ip :" + request.getRemoteAddr());
		LOGGER.info("ipIsNotYetChecked :" + session.getAttribute("checkedIp"));
		boolean ipIsNotYetChecked = null == session.getAttribute("checkedIp");
		String testip = request.getParameter("ip");
		if (StringUtils.isNotEmpty(testip) || ipIsNotYetChecked) { 
			//Set flag checkedIp to true so this would run only once
			String ipAddress = StringUtils.isNotEmpty(testip) ? testip : request.getParameter("ip");
			LOGGER.info("Checking ip :" + ipAddress);
			
			if (StringUtils.isEmpty(ipAddress)) {  
				ipAddress = request.getHeader("X-Cluster-Client-Ip");
				LOGGER.info("X-Cluster-Client-Ip :" + ipAddress);
			}
			if (StringUtils.isEmpty(ipAddress)) {
				ipAddress = request.getRemoteAddr();
				LOGGER.info("request.getRemoteAddr() :" + ipAddress);
			}
			if (StringUtils.isNotEmpty(ipAddress)) { 
				session.setAttribute("checkedIp","true");
				LOGGER.info("----------------------------------------------  Validatin ip in CJO :" + ipAddress );
//				String ipLoginServlet = EBooksConfiguration.getProperty("cjo.ip.login.url");
//				String[] param = {"ip"};
//				String[] value = {ipAddress};
			
				try { 				
					//String bodyIds = HttpUtil.connectTo( ipLoginServlet, param, value ).toString().trim();
					
					
					List<Organisation> orgList = (List<Organisation>) OrganisationWorker.getIpAuthenticatedOrgs(ipAddress);
					
					//sets this in session, will be used many times, contains org/consortia informations limited to body_id, name, 
					//session.setAttribute(ORGANISATION_LIST, orgList);
					OrgConLinkedMap map = OrgConLinkedMap.getFromSession(request.getSession());
					map.appendList(orgList);					
					session.setAttribute(OrgConLinkedMap.ORG_LINKED_MAP, map);
					
					//consortium organisation
					Map<String, List<ConsortiumOrganisation>> consOrgMap = OrgConLinkedMap.getConsOrgFromSession(session);										
					OrgConLoginWorker.populateConsOrgMap(OrgConLoginWorker.getBodyIdsAsString(orgList), consOrgMap);
					
					
					//String bodyIds = map.getIpAuthenticatedBodyIds();
					
					OrganisationWorker.trackOrganisationLogin(session);
					
					//get 
//					System.out.println("authen filter iplogin item = " + bodyIds);
//					boolean ipIsRecognized = StringUtil.isNotEmpty( bodyIds ) && bodyIds.indexOf("FAILED") == -1;
//					if ( ipIsRecognized ) {
////						String[] memberships = bodyIds.split(",");
//						//OrganisationWorker.setIPMembershipInSession( session , map);
//						
//						//OrganisationWorker.setOrganisationDisplayInSession( session );
//						
//						
//						
//					} else { 
//						LogManager.info(this.getClass(), "IP address is not recognized: " + ipAddress +  " response is " +bodyIds);
//					}
					
					// put books that are 'not available for sale' which were purchased by these orgs/cons in a session attribute
					String isbns = SubscriptionUtil.getBooksNotAvailableForSale(OrgConLinkedMap.getAllBodyIds(request.getSession()), 
							OrgConLinkedMap.getDirectBodyIds(request.getSession()));
					session.setAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE, isbns);
					
				} catch (Exception ioe) { 
					LOGGER.error(ExceptionPrinter.getStackTraceAsString(ioe));
				}
				
				LOGGER.info("----------------------------------------------  Validated ip in CJO :" + ipAddress );
			}
		}
	}
}
