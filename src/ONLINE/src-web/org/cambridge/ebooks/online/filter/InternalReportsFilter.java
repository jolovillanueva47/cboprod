package org.cambridge.ebooks.online.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.internal_reports.InternalReportsWorker;

public class InternalReportsFilter implements Filter {
		
	private static final Logger LOGGER = Logger.getLogger(InternalReportsFilter.class);
	
	/* implements */
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;	
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {		
		LOGGER.info("=== Internal Reports ===");
		InternalReportsWorker.listenEvent((HttpServletRequest)request, (HttpServletResponse)response);		
		chain.doFilter(request, response);
	}

}
