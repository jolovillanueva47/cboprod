package org.cambridge.ebooks.online.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.subscription.FreeTrialBean;

public class AccessListFilter implements Filter{
	
	private static final Logger LOGGER = Logger.getLogger(AccessListFilter.class);
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	
	public static final String FREE_TRIAL_NO_ACCESS_LIST = "FREE_TRIAL_NO_ACCESS_LIST";
	
	public void init (FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	public void destroy() {
		filterConfig = null;
	}
	
	public void doFilter(ServletRequest httpRequest, ServletResponse httpResponse, FilterChain chain ) throws IOException, ServletException {
		LOGGER.info("=== Start Filter ===");
		
		/*log*/
		long startTime = new Date().getTime();
		/*log*/
		
		//doCheckAccessList((HttpServletRequest)httpRequest,(HttpServletResponse) httpResponse);
		HttpSession session = ((HttpServletRequest)httpRequest).getSession();
		checkTrial(session, OrgConLinkedMap.getAllBodyIds(session));		
		
		/*log*/
		long endTime = new Date().getTime();
		LOGGER.info("checkAccess time= " + (endTime - startTime)/1000.0d);
		/*log*/
		
		LOGGER.info("=== End Filter ===");		
		chain.doFilter(httpRequest, httpResponse);
	}
	
	/**
	 * sets the access list for session
	 * @param request
	 * @param response
	 * @throws ServletException 
	 * @throws Exception 
	 */
//	@SuppressWarnings("unused")
//	private void doCheckAccessList(HttpServletRequest request, ServletResponse response) throws ServletException {
//		HttpSession session = request.getSession();
//					
//		//dalawa source, login, tsaka ip login
//		StringBuffer sb = new StringBuffer();
//				
//		//normal login
//		String bodyId = (String) session.getAttribute(AuthenticationFilter.ORG_BODY_ID);			
//		if(Misc.isNotEmpty(bodyId)){
//			sb.append(bodyId);
//		}
//		
//		
//		//membership logins
//		String[] memberships = (String[]) session.getAttribute("membership");
//		if(sb.toString().length() > 0 && memberships != null && memberships.length > 0){
//			sb.append(AuthenticationFilter.COMMA);
//		}	
//		
//		LogManager.info(AuthenticationFilter.class, "ip Memberships");
//		int memlength = memberships != null ? memberships.length : 0;
//		for (int i = 0; i < memlength; i++) {
//			if(i == memberships.length - 1){
//				sb.append(memberships[i]);
//			}else{
//				sb.append(memberships[i] + AuthenticationFilter.COMMA);
//			}
//			LogManager.info(AuthenticationFilter.class, memberships[i]);
//		}
//		
//		//iplogin
//		String[] ipMemberships = (String[]) session.getAttribute(AuthenticationFilter.IP_MEMBERSHIPS);
//		
//		if(sb.toString().length() > 0 && ipMemberships != null && ipMemberships.length > 0){
//			sb.append(AuthenticationFilter.COMMA);
//		}		
//				
//		LogManager.info(AuthenticationFilter.class, "ip Memberships");
//		int ipMemlength = ipMemberships != null ? ipMemberships.length : 0;
//		for (int i = 0; i < ipMemlength; i++) {
//			if(i == ipMemberships.length - 1){
//				sb.append(ipMemberships[i]);
//			}else{
//				sb.append(ipMemberships[i] + AuthenticationFilter.COMMA);
//			}
//			LogManager.info(AuthenticationFilter.class, ipMemberships[i]);
//		}
//		
//		
//		
//		//athens login		
//		String athendsBodyIds = (String) session.getAttribute(AuthenticationFilter.ATHENS_BODY_IDS);
//		LogManager.info(this.getClass(), "athens bodyIds = x" + athendsBodyIds + "x" );
//		if(sb.toString().length() > 0 && athendsBodyIds != null && athendsBodyIds.trim().length() > 0 
//				&& !athendsBodyIds.equals("null")){
//			sb.append(AuthenticationFilter.COMMA);
//		}
//		if(StringUtil.isNotEmpty(athendsBodyIds) && !athendsBodyIds.equals("null")){
//			sb.append(athendsBodyIds);
//		}
//		
//		
//		//shibboleth login
//		String shibbBodyIds = (String) session.getAttribute(AuthenticationFilter.SHIBB_BODY_IDS);
//		LogManager.info(this.getClass(), "shibb bodyIds = x" + shibbBodyIds + "x" );
//		if(sb.toString().length() > 0 && shibbBodyIds != null && shibbBodyIds.trim().length() > 0 
//				&& !shibbBodyIds.equals("null")){
//			sb.append(AuthenticationFilter.COMMA);
//		}
//		if(StringUtil.isNotEmpty(shibbBodyIds) && !shibbBodyIds.equals("null")){
//			sb.append(shibbBodyIds);
//		}
//		
//		
//			
//		//session ip list
//		@SuppressWarnings("unused")
//		String accessListBodyIds = (String) session.getAttribute(AuthenticationFilter.ACCESS_LIST_BODY_IDS);
//		String newAccessListBodyIds = sb.toString();
//		
//		//check for access list body ids 
//		session.setAttribute(AuthenticationFilter.ACCESS_LIST_BODY_IDS, newAccessListBodyIds);
//		
//		
//		LogManager.info(AuthenticationFilter.class, "accessList bodyids = " + newAccessListBodyIds);
//		LogManager.info(AuthenticationFilter.class, "accessList free-trial/isbn = " + 
//				FreeTrialBean.getFromSession(session).isFreeTrial() );
//		
//		checkTrial(session, newAccessListBodyIds);		
//	}
	
	 
	
	private static void checkTrial(HttpSession session, String bodyId) throws ServletException{
		if(StringUtils.isEmpty(bodyId)){
			return;
		}
		FreeTrialBean ftb = (FreeTrialBean) FreeTrialBean.getFromSession(session);
		//static check
		FreeTrialBean.initFreeTrial();
		
		LOGGER.info("skipForTrial accessList = " + ftb.isFreeTrial());
		if(!ftb.isFreeTrialChecked()){
			String free = OrganisationWorker.checkFreeTrial(bodyId);
			if(free != null){
				ftb.setFreeTrial(true);				
			}			
		}
		ftb.setFreeTrialChecked(true);
	}

}
