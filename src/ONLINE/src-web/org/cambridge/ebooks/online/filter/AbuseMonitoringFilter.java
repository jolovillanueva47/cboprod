package org.cambridge.ebooks.online.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dos.AccessVo;
import org.cambridge.ebooks.online.dos.DosLogger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

/**
 * @author Karlson A. Mulingtapang
 * LimitDownloadFilter.java - Limit number of downloads
 */
public class AbuseMonitoringFilter implements Filter {
	
	private static final Logger logger = Logger.getLogger(AbuseMonitoringFilter.class);
	
	private static final String ACCESS = "access";
	@SuppressWarnings("unused")
	private static final String REDIRECT_PAGE = "/error/error.jsp";
	
	
	private static final String[] HEADER_TYPE = {"X-Cluster-Client-Ip", "Via", "Forwarded", 
		"X-Forwarded-For", "Client-ip"};

	public static final String[] IP_TYPE = {"User", "Proxy"};
	
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	
	//private long sessionDuration;
	//private long previousTime;
	//private AccessVo accessVo;
	
	public void destroy() {
		this.filterConfig = null;	
	}

	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain filterChain) throws IOException, ServletException {

		logger.info("=== Start Filter ===");		
		XSSFilteredHttpServletRequest filteredRequest = (XSSFilteredHttpServletRequest) req;
		HttpSession session = filteredRequest.getSession(false);
		
		if(null == session) {
			logger.info("=== session is null ===");		
			filterChain.doFilter(filteredRequest, resp);
			return;
		}
		
		String currentSessionId = session.getId();
		
		AccessVo accessVo = (AccessVo)session.getAttribute(ACCESS);
		
		String uriPath = filteredRequest.getRequestURI();
		logger.info("uri path:" + uriPath);
		
		if(null != accessVo && currentSessionId.equals(accessVo.getSessionId())) {				
			// session duration
			accessVo.setPreviousTime(session.getLastAccessedTime());
			if(null != accessVo) {				
				accessVo.setSessionDuration((accessVo.getPreviousTime() - accessVo.getStartTime()) / 1000);
				logger.info("session duration:" + accessVo.getSessionDuration());
				if(isPdfView(uriPath)) {
					accessVo.setPdfAccessCount(accessVo.getPdfAccessCount() + 1);
					logger.info("pdf access count:" + accessVo.getPdfAccessCount());
				}
			}
			
			if(accessVo.isDenied()) {		
				if(isPdfView(uriPath)) {
					// redirect to error page
					//((HttpServletResponse)resp).sendRedirect(filteredRequest.getContextPath() + REDIRECT_PAGE);
					//return;
				}				
			} else {		
				if(accessVo.isSessionDurationExceeded()) {					
					accessVo.setPdfAccessCount(0);				
					accessVo.setStartTime(session.getLastAccessedTime());						
				} else if(accessVo.isLimitExceeded()) {
					try {	
						accessVo.populateAccessVoDetails(session);
						DosLogger.saveEvent(accessVo);
					} catch (Exception e) {
						logger.error("[Exception]");
						logger.error(ExceptionPrinter.getStackTraceAsString(e));
					}
				}
			}
			
		} else {			
			if(null == accessVo) {				
				accessVo = initAccessVo(filteredRequest);
				
				if(isPdfView(uriPath)) {
					accessVo.setPdfAccessCount(accessVo.getPdfAccessCount() + 1);
					logger.info("pdf access count:" + accessVo.getPdfAccessCount());
				}
			}			
		}
		
		//session.setAttribute(ACCESS, accessVo);
		
		logger.info("session id:" + session.getId());
					
		logger.info("=== End Filter ===");
		
		filterChain.doFilter(filteredRequest, resp);
	}	
	
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}	

	private boolean isPdfView(String uriPath) {
		return (uriPath.indexOf("/open_pdf") > -1);
	}
	
	private AccessVo initAccessVo(XSSFilteredHttpServletRequest req) {
		AccessVo accessVo = new AccessVo();
		accessVo.setDenied(false);
		accessVo.setPdfAccessCount(0);								
		accessVo.setSessionId(req.getSession().getId());
		accessVo.setStartTime(req.getSession().getCreationTime());
		setIpDetails(accessVo, req);		
		
		req.getSession().setAttribute(ACCESS, accessVo);
		return accessVo;
	}	

	private void setIpDetails(AccessVo accessVO, XSSFilteredHttpServletRequest req) {				
		if(StringUtils.isEmpty(accessVO.getIpAddress())) {
			accessVO.setIpAddress((String)req.getHeader(HEADER_TYPE[0])); // X-Cluster-Client-Ip
			accessVO.setIpType(IP_TYPE[0]);
		}
		
		if(StringUtils.isNotEmpty(req.getHeader(HEADER_TYPE[1])) // Via
				&& !IP_TYPE[1].equals(accessVO.getIpType())) {
			accessVO.setIpType(IP_TYPE[1]);
		}
		if(StringUtils.isNotEmpty(req.getHeader(HEADER_TYPE[2])) // Forwarded
				&& !IP_TYPE[1].equals(accessVO.getIpType())) {
			accessVO.setIpType(IP_TYPE[1]);
		}
		if(StringUtils.isNotEmpty(req.getHeader(HEADER_TYPE[3])) // X-Forwarded-For
				&& !IP_TYPE[1].equals(accessVO.getIpType())) {
			accessVO.setIpType(IP_TYPE[1]);
		}
		if(StringUtils.isNotEmpty(req.getHeader(HEADER_TYPE[4])) // Client-ip
				&& !IP_TYPE[1].equals(accessVO.getIpType())) {
			accessVO.setIpType(IP_TYPE[1]);
		}		
			
		//if ipaddress is still null at this point then get it from http address
		if (StringUtils.isEmpty(accessVO.getIpAddress())) {
			accessVO.setIpAddress(req.getRemoteAddr()); 
			accessVO.setIpType(IP_TYPE[0]);
		}
	}
}
