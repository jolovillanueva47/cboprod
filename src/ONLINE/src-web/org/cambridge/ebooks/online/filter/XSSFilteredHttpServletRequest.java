package org.cambridge.ebooks.online.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.cambridge.ebooks.online.util.StringUtil;

/**
 * @author Karlson A. Mulingtapang
 * CustomHttpServletRequest.java - Description
 */
public class XSSFilteredHttpServletRequest extends HttpServletRequestWrapper {
	
	public XSSFilteredHttpServletRequest(HttpServletRequest request) {
		super(request);
	}	
	
	public String getParameter(String name) {		
		return filterValue(super.getParameter(name));
	}
	
	/**
	 * Used for XSS (Cross-Site Scripting
	 * @param value String
	 * @return String
	 */
	private String filterValue(String value) {
		if(StringUtil.isNotEmpty(value)) {
	        value = value.replaceAll("(?i)(<script(.*?)>)", "")
	        .replaceAll("(?i)(</script>)", "")
	        .replaceAll("<", "&lt;")
	        .replaceAll(">", "&gt;")
	        .replaceAll("(?i)(eval\\((.*?)\\))", "")
	        .replaceAll("(?i)(alert\\((.*?)\\))", "")
	        .replaceAll("[\\\"\\\'][\\s]*(?i)(javascript):(.*?)[\\\"\\\']", "\"\"");
		}
        return value;
    }
}
