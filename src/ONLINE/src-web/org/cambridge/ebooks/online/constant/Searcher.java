//package org.cambridge.ebooks.online.constant;
//
///**
// * @author Karlson A. Mulingtapang
// * Constants.java - Lucene related contant attributes
// */
//public interface Searcher {
//	
//	// QUERY BUILDER
//	public final static String TARGET_DASH = "---------------------------";
//	public final static String TARGET_AUTHOR_EDITOR_CONTRIBUTOR = "Author / Editor / Contributor";
//	public final static String TARGET_TITLE_VOLUME = "Title / Volume";
//	public final static String TARGET_SUBJECT = "Subject";
//	public final static String TARGET_PUBLICATION_DATE = "Publication Year";
//	public final static String TARGET_PUBLICATION_DATE_ONLINE = "Publication Year ONLINE";
//	public final static String TARGET_PUBLICATION_DATE_PRINT = "Publication Year PRINT";
//	public final static String TARGET_ISBN = "ISBN";
//	public final static String TARGET_SERIES = "Series Name";
//	public final static String TARGET_DOI = "Digital Object Identifier";
//	public final static String TARGET_CONTENT = "Content";
//	public final static String TARGET_KEYWORDS = "Keyword";
//	public final static String TARGET_AUTH_AFF = "Author Affiliation";
//	public final static String TARGET_CONTENT_TITLE = "Content Title";
//	public final static String TARGET_ANYTHING = "Anything";
//	public final static String[] TARGET_ALL = {TARGET_AUTHOR_EDITOR_CONTRIBUTOR,
//		TARGET_TITLE_VOLUME, TARGET_SUBJECT, TARGET_PUBLICATION_DATE, TARGET_ISBN, 
//		TARGET_SERIES, TARGET_DOI, TARGET_CONTENT, TARGET_KEYWORDS, TARGET_AUTH_AFF, 
//		TARGET_CONTENT_TITLE};
//	
//	public static final String[] TARGET_PUBLICATION_DATE_ARR = new String[]{TARGET_PUBLICATION_DATE_ONLINE, TARGET_PUBLICATION_DATE_PRINT};
//	
//	
//
//	public final static String CONDITION_CONTAINS = "Contains";
//	public final static String CONDITION_DOES_NOT_CONTAIN = "Does not contain";
//	public final static String CONDITION_IS_EXACTLY = "Is exactly";
//	public final static String CONDITION_IS_BETWEEN = "Is between";
//	public final static String CONDITION_IS_FROM = "Is from";
//}

