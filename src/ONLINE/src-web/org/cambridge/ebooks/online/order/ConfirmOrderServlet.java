package org.cambridge.ebooks.online.order;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.order.EBookOrder;
import org.cambridge.ebooks.online.jpa.user.BladeMember;
import org.cambridge.ebooks.online.util.FileUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class ConfirmOrderServlet extends HttpServlet{
	
	private static final Logger LOGGER = Logger.getLogger(ConfirmOrderServlet.class);
	
	private static final long serialVersionUID = -4054761493320713735L;
	
	public static final String BLADE_ID_PARAM = "memberId";
	public static final String ORDER_ID_PARAM = "orderId";
	public static final String NAME_PARAM = "name";
	public static final String TITLE_PARAM = "title";
	private static final String CONFIRMED_ORDER = "4";
	
	private static final String ORDER = "ORDER";
	
	private static final String DESTINATION_DIR = System.getProperty("mail.store.path");
	
	private static final String SOURCE_DIR = System.getProperty("order.agree.dir");
	
	private static final String REDIRECT_PATH = "/order_PDF.jsf";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {	
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info("Confirm Order Servlet");
		confirmOrder(req, resp);
	}
	
	private void confirmOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String blade = request.getParameter(BLADE_ID_PARAM);
		String orderId = request.getParameter(ORDER_ID_PARAM);
		
		BladeMember bm = PersistenceUtil.searchEntity(new BladeMember(), BladeMember.FIND_BLADE, blade);
		
		EntityManager em = PersistenceUtil.emf.createEntityManager();
		try {			
			EntityTransaction tx = em.getTransaction();			
			tx.begin();
			
			//update 
			EBookOrder order = em.find(EBookOrder.class, orderId);
			
			//check if order is already confirmedsdsdf
			String confirmee = order.getOrderConfirmeeName();
			if(confirmee != null || StringUtils.isNotEmpty(confirmee)){
				order.setError(true);
				redirectUser(request, response, order);
				return;
			}
			
			//continue if correct
			updateConfirmedOrder(order, bm, em, request);
			copyPasteFile(orderId, SOURCE_DIR, DESTINATION_DIR);
			
			//will only commmit if whole transaction is valid that is why tx.commit is placed outside			
			tx.commit();
			
			//redirect user to homepage
			redirectUser(request, response, order);
		} catch (Exception e) {
			e.printStackTrace();
			//on exception will remove the files and move back to the source dir
			try{
				deleteFile(orderId, DESTINATION_DIR);
			}catch(Exception _e){
				//super error na ito
			}			
			e.printStackTrace();
			throw new IOException(e.getMessage());
		} finally {
			em.close();
		}
	}	
	
	private void redirectUser(HttpServletRequest request, HttpServletResponse response, EBookOrder order) throws ServletException, IOException{
		request.setAttribute(ORDER, order);		
		RequestDispatcher rd = request.getRequestDispatcher(recreateUrl(request));
		rd.forward(request, response);	
	}
	
	private String recreateUrl(HttpServletRequest request){
		return REDIRECT_PATH + "?" + BLADE_ID_PARAM + "=" + request.getParameter(BLADE_ID_PARAM) + "&" + ORDER_ID_PARAM +
				"=" + request.getParameter(ORDER_ID_PARAM);
	}
	
	private EBookOrder updateConfirmedOrder(EBookOrder order, BladeMember bm, EntityManager em, HttpServletRequest request){		
		order.setEbookOrderTimestamp(new Timestamp(new Date().getTime()));
		order.setOrderStatus(CONFIRMED_ORDER);
		order.setOrderConfirmeeEmail(bm.getMemberEmail());
		order.setOrderConfirmeeName(request.getParameter(NAME_PARAM));
		order.setOrderConfirmeeTitle(request.getParameter(TITLE_PARAM));
		em.merge(order);
		return order;		
	}
	
	public static void deleteFile(String orderId, String _sourceDir) throws Exception{
		File sourceDir = new File(_sourceDir); 
		if(!sourceDir.isDirectory() || !sourceDir.canWrite()){
			throw new Exception("destination dir does not exist - " + sourceDir.getAbsolutePath());
		}	
		
		String[] fileNames = sourceDir.list();
		for(String fileName : fileNames){
			if(fileName.indexOf(orderId) == 0){
				LOGGER.info("Delete File file: " + _sourceDir + fileName);
				File _file = new File(_sourceDir + fileName);				
				if(_file.isFile() && _file.canWrite()){
					//FileUtil.moveFile(_file, destDir);
					_file.delete();
				}else{
					LOGGER.info("isFile: " + _file.isFile() + " canWrite:" +  _file.canWrite());
					throw new Exception("move of file error - filename :" + fileName);
				}
			}
		}		
	}
		
	public static void copyPasteFile(String orderId, String _sourceDir, String _destinationDir) throws Exception{
		LOGGER.info("sourceDir: " + _sourceDir + " destDir:" + _destinationDir);
		
		File sourceDir = new File(_sourceDir);
		
		if(!sourceDir.isDirectory() || !sourceDir.canRead()){
			throw new Exception("source dir does not exist - " + sourceDir.getAbsolutePath());
		}
		
		File destDir = new File(_destinationDir); 
		if(!destDir.isDirectory() || !destDir.canWrite()){
			throw new Exception("destination dir does not exist - " + destDir.getAbsolutePath());
		}		
		
		String[] fileNames = sourceDir.list();
		for(String fileName : fileNames){
			if(fileName.indexOf(orderId) == 0){
				LOGGER.info("Transferring file: " + _sourceDir + fileName);
				File _file = new File(_sourceDir + fileName);				
				if(_file.isFile() && _file.canRead()){
					//FileUtil.moveFile(_file, destDir);
					FileUtil.copyPasteFile(_file.getAbsolutePath(), destDir.getAbsolutePath() + "/" + fileName);
				}else{
					LOGGER.info("isFile: " + _file.isFile() + " canWrite:" +  _file.canWrite());
					throw new Exception("copy file error - filename :" + fileName);
				}
			}
		}		
	}
	
}
