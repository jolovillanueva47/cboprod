package org.cambridge.ebooks.online.order;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.JDBCUtil;

public class renewSubscriptions extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(renewSubscriptions.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
		
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOGGER.info("Confirm New Order Servlet");
				
		
		try {
			confirmNewOrder(req, resp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void confirmNewOrder(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException, SQLException {
		//System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StringBuilder htmlResponse = new StringBuilder();
		htmlResponse.append("<html><body>");
		
		String emailOrderId = request.getParameter("orderId");
		String renewal_requested = null;
		int mail = Integer.parseInt(emailOrderId);
		
//		Connection con = DataSource.getConnection();
		Connection con = JDBCUtil.getConnection();
		Statement stmt = con.createStatement();
		con.setAutoCommit(false);			
		CallableStatement pc = null;
		ResultSet rs = null;
		String orderId = null;
			
		try{
			rs = stmt.executeQuery("SELECT renewal_requested FROM ebook_order_subscription WHERE order_id= "+ mail );
			while(rs.next())
			renewal_requested = rs.getString(1);
			
			System.out.println(renewal_requested);
			
			if( "Y".equalsIgnoreCase(renewal_requested) ){
				htmlResponse.append("Record has already been updated.\n");
			}else{
				pc = con.prepareCall("{call duplicate_records(?)}");
				pc.setInt(1, mail);
				pc.execute();
				int update = pc.getUpdateCount();
	
				if(emailOrderId != null){
					if (update == 0){
						htmlResponse.append("NO RECORD HAS BEEN UPDATED!\n");
					}else{
						htmlResponse.append("Renewal_requested has been updated.\n");
					}
				}else{
					htmlResponse.append("NO ORDER ID IS AVAILABLE.\n");
				}
				//htmlResponse.append("</body></html>");
				con.commit();
				rs = stmt.executeQuery("SELECT order_id FROM ebook_order WHERE subs_previous_order_id= " + mail);
				while(rs.next())
				orderId = rs.getString(1);
//				String orderId = "10001115";
//				createmail(orderId);
					
//				String sourceFile = System.getProperty("mail.subscriptions.renewal.path")+emailOrderId+".mail";
//				String destinationDir = System.getProperty("mail.store.path")+emailOrderId+".mail";
				String sourceFile = System.getProperty("mail.subscriptions.renewal.path");
				String destinationDir = System.getProperty("mail.store.path");
				boolean success = copyPasteFile(sourceFile, destinationDir, emailOrderId+".mail");
				if (! success){
					htmlResponse.append("Email not sent, problem occurred when transferring email. Rolling back DB. Please contact system admin.");
					throw new Exception("Email not sent, problem occurred when transferring email. Rolling back DB");
				}
			}
		}
		catch(Exception e){
			//e.printStackTrace();
			LOGGER.error(e.getMessage());
			con.rollback();
		}
		finally{
			con.close();
			stmt.close();
			if( "N".equalsIgnoreCase(renewal_requested) ){
				pc.close();
				rs.close();
			}
			htmlResponse.append("</body></html>");
			out.println(htmlResponse.toString());
		}
	}
	
	public static boolean copyPasteFile(String sourceFilePath, String destinationDirPath, String filename){
		boolean isSuccess = false; 
		InputStream in = null;
		OutputStream out = null;
		 try{
			  LOGGER.info("copy sourceFilePath: " + sourceFilePath+filename + " destinationDirPath:" + destinationDirPath+filename);
		      File f1 = new File(sourceFilePath+filename);
		      File f2 = new File(destinationDirPath+filename);
		      in = new FileInputStream(f1);
		      
		      //For Overwrite the file.
		      out = new FileOutputStream(f2);

		      byte[] buf = new byte[3072];
		      int len;
		      while ((len = in.read(buf)) > 0){
		        out.write(buf, 0, len);
		      }
		      in.close();
		      out.close();
		      //System.out.println("File copied.");
		      isSuccess = true;
		    }
		    catch(FileNotFoundException ex){
		    	ex.printStackTrace();
		    }
		    catch(IOException e){
		      System.out.println(e.getMessage());      
		    } finally {
		    	try {
					in.close();
					out.close();
				} catch (Exception e) {
					//e.printStackTrace();
				}
		    }
		    return isSuccess;
		    
	}
//	private void createmail(String orderId){
//		
//		System.out.println("---------------------------------- creating txt -----------------------------------");
//		
//		String path = System.getProperty("mail.store.path") ;
//		StringBuffer sb = new StringBuffer();
//		
//		sb.append("from: "+System.getProperty("view.access.email.us"));
//		sb.append("\nto: hotrando@cambridge.org");
//		sb.append("\ncc: hotrando@cambridge.org, techsupp@cambridge.org");
//		sb.append("\nbcc: jrillera@cambridge.org");
//		sb.append("\nsubject: CBO - Access Agreement");
//		sb.append("\nbody:");
//		sb.append("\nSubscription renewal created, see order_id " + orderId + ". Please review and action");
//		
//		try{
//			FileUtil.generateFile(sb, path+"test.txt");
//		}catch (Exception e){
//			e.printStackTrace();
//		}
//
//		
//	}

}
