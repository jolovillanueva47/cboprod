package org.cambridge.ebooks.online.order;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.order.EBookAccessAgreement;
import org.cambridge.ebooks.online.jpa.order.EBookAccessAgreementKey;
import org.cambridge.ebooks.online.jpa.user.BladeMember;
import org.cambridge.ebooks.online.util.FileUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class AccessAgreementServlet extends HttpServlet{
	
	private static final Logger LOGGER = Logger.getLogger(AccessAgreementServlet.class);
	
	private static final String BLADE_ID_PARAM = ConfirmOrderServlet.BLADE_ID_PARAM;
	private static final String ORDER_ID_PARAM = ConfirmOrderServlet.ORDER_ID_PARAM;
	private static final String BODY_ID_PARAM = "bodyId";
	private static final String NAME_PARAM = ConfirmOrderServlet.NAME_PARAM;
	private static final String TITLE_PARAM = ConfirmOrderServlet.TITLE_PARAM;
	
	private static final String ACCESS_AGREEMENT = "ACCESS_AGREEMENT";
	
	private static final String REDIRECT_PATH = "/access_agreement.jsf";
	
	private static final String DESTINATION_DIR = System.getProperty("mail.store.path");
	
	private static final String SOURCE_DIR = System.getProperty("order.ack.dir");

	/**
	 * 
	 */
	private static final long serialVersionUID = -6611187787761664164L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info("Confirm Order Servlet");
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {	
		agreeOnAccess(req, resp);
	}
	
	private void agreeOnAccess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String blade = request.getParameter(BLADE_ID_PARAM);
		String orderId = request.getParameter(ORDER_ID_PARAM);
		String bodyId = request.getParameter(BODY_ID_PARAM);
		
		if(StringUtils.isEmpty(orderId) || StringUtils.isEmpty(bodyId)){
			return;
		}
		
		BladeMember bm = PersistenceUtil.searchEntity(new BladeMember(), BladeMember.FIND_BLADE, blade);
		
		if(bm == null){
			return;
		}
		
		EntityManager em = PersistenceUtil.emf.createEntityManager();		
		try{
			EntityTransaction tx = em.getTransaction();			
			tx.begin();
			
			EBookAccessAgreement access = em.find(EBookAccessAgreement.class, 
					new EBookAccessAgreementKey(Long.parseLong(orderId), 
							Long.parseLong(bodyId)) );
			//check if already exist, already exists means confirmed already
			if(access != null){
				access.setError(true);
				redirectUser(request, response, access);
				return;
			}
			
			//continue if correct
			access = new EBookAccessAgreement();
			access.setAcccessConfirmeeEmail(bm.getMemberEmail());
			initAccessBean(request, access);			
			em.persist(access);
			copyPasteFiles(SOURCE_DIR + orderId + "_" + blade + "_ack.mail" , DESTINATION_DIR);
			
			//update also other beans
			
			//will only commmit if whole transaction is valid that is why tx.commit is placed outside			
			tx.commit();
			
			//redirect user to homepage
			redirectUser(request, response, access);			
		}catch (Exception e) {
			e.printStackTrace();
			//on exception will remove the files and move back to the source dir
			try{
				deleteFiles(DESTINATION_DIR + orderId + "_" + blade + "_ack.mail");
			}catch(Exception _e){
				//super error na ito
			}			
			e.printStackTrace();
			throw new IOException(e.getMessage());
		}finally{
			em.close();
		}
	}
	
	private void redirectUser(HttpServletRequest request, HttpServletResponse response, EBookAccessAgreement access) throws ServletException, IOException{
		request.setAttribute(ACCESS_AGREEMENT, access);		
		RequestDispatcher rd = request.getRequestDispatcher(recreateUrl(request));
		rd.forward(request, response);	
	}
	
	private String recreateUrl(HttpServletRequest request){
		return REDIRECT_PATH + "?" + BLADE_ID_PARAM + "=" + request.getParameter(BLADE_ID_PARAM) + "&" + ORDER_ID_PARAM +
				"=" + request.getParameter(ORDER_ID_PARAM) + "&" + BODY_ID_PARAM + "=" + request.getParameter(BODY_ID_PARAM);
	}
	
	private void initAccessBean(HttpServletRequest request, EBookAccessAgreement access){
		access.setAccessConfirmeeName(request.getParameter(NAME_PARAM));
		access.setAccessConfirmeeTitle(request.getParameter(TITLE_PARAM));
		access.setAccessConfirmeeTimestamp(new Timestamp(new Date().getTime()));
		access.setMemberId(Long.parseLong(request.getParameter(BLADE_ID_PARAM)));
		access.setBodyId(Long.parseLong(request.getParameter(BODY_ID_PARAM)));
		access.setOrderId(Long.parseLong(request.getParameter(ORDER_ID_PARAM)));
	}
	
	public static void copyPasteFiles(String _sourceFile, String _destinationDir) throws Exception{
		LOGGER.info("sourceFile: " + _sourceFile + " destDir:" + _destinationDir);
		
		File sourceFile = new File(_sourceFile);

		if(!sourceFile.isFile() || !sourceFile.canRead()){
			throw new Exception("source dir does not exist - " + sourceFile.getAbsolutePath());
		}
		
		File destDir = new File(_destinationDir); 
		if(!destDir.isDirectory() || !destDir.canWrite()){
			throw new Exception("destination dir does not exist - " + destDir.getAbsolutePath()  + "/" + sourceFile.getName());
		}		
		
		FileUtil.copyPasteFile(sourceFile.getAbsolutePath(), destDir.getAbsolutePath() + "/" + sourceFile.getName());			
	}
	
	public static void deleteFiles(String _sourceFile) throws Exception{
		LOGGER.info("sourceFile: " + _sourceFile);
		
		File sourceFile = new File(_sourceFile);

		if(!sourceFile.isFile() || !sourceFile.canWrite()){
			throw new Exception("source dir does not exist - " + sourceFile.getAbsolutePath());
		}				
		
		sourceFile.delete();					
	}
}
