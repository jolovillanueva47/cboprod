package org.cambridge.ebooks.online.pda;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.EjbUtil;

public class PdaLogger {
	private static final Logger LOGGER = Logger.getLogger(PdaLogger.class);
	
	private static final String CONNECTION_FACTORY = "ConnectionFactory";
	
	private static final String JMS_DESTINATION = "queue/pda_mdb/logger";
	
	private static final String[] properties = new String[]{
			"contentId",
			"ipAddress",
			"orderId",
			"referrer",
			"sessionId"		
		};
	
	public static void logEvent(PdaBean bean){		
		try {
			ConnectionFactory connectionFactory = EjbUtil.getJmsConnectionFactory( CONNECTION_FACTORY );
			Connection connection = connectionFactory.createConnection();
	        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        
	        Destination destination = EjbUtil.getJmsDestination( JMS_DESTINATION );            
	        MessageProducer messageProducer = session.createProducer(destination);
	        Message payload = session.createMapMessage();
	        
	        for(String prop : properties){
	        	payload.setStringProperty(prop, BeanUtils.getProperty(bean, prop));
	        }	    	
	    	
	    	try { 
	            messageProducer.send( payload );	            
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}

        
		
	}
}
