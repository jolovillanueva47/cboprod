package org.cambridge.ebooks.online.pda;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class PdaBean {	
	
	private static final String Client_ip = "X-Cluster-Client-Ip";
	private static final String ip_magic = "ip";
	private static final String Referrer = "referer";
	
	private String agreementId;
	
	private String contentId;
	
	private Timestamp timestampCreated;
	
	private String ipAddress;
	
	private String referrer;
	
	private String sessionId;
	
	private String orderId;

	public String getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Timestamp getTimestampCreated() {
		return timestampCreated;
	}

	public void setTimestampCreated(Timestamp timestampCreated) {
		this.timestampCreated = timestampCreated;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	
	public void setIpAddress(HttpServletRequest request){			
		String ipAddress = request.getHeader(Client_ip);
		String ipMagic = request.getParameter(ip_magic);
		String ipFinal = "";
		if(StringUtils.isNotEmpty(ipMagic)){
			ipFinal = ipMagic;
		}
		if(StringUtils.isNotEmpty(ipAddress)){
			ipFinal = ipAddress;
		}else{
			ipFinal = request.getRemoteAddr();
		}
		
		this.ipAddress = ipFinal;
	}
	
	public void setReferrer(HttpServletRequest request){
		setReferrer(request.getHeader(Referrer));
	}

}
