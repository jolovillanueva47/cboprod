package org.cambridge.ebooks.online.ebook;


/**
 * 
 * @author jgalang
 * 
 */

public class Author implements Comparable<Author>{

	private String position;
	private String name;
	private String affiliation;
	private String role;
	private String fixName;

	public int compareTo(Author o) {
		try{
			int self = Integer.parseInt(this.position);
			int arg = Integer.parseInt(o.getPosition());
			if(self < arg ){
				return -1;
			}else if(self > arg){
				return 1;
			}else{
				return 0;
			}
		}catch(Exception e){
			return 0;
		}
				
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the fixName
	 */
	public String getFixName() {
		return fixName;
	}

	/**
	 * @param fixName the fixName to set
	 */
	public void setFixName(String fixName) {
		this.fixName = fixName;
	}	
}
