package org.cambridge.ebooks.online.annotations;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.flexpaper.FlexPaper;
import org.cambridge.ebooks.online.util.StringUtil;

public class AnnotationServlet extends HttpServlet {
	static Logger logger = Logger.getLogger(AnnotationServlet.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	private static final Logger logger = Logger.getLogger(AnnotationServlet.class);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//Object obj = req.getParameter("objectList");
		String message = handleAnnotations(req,resp);
		PrintWriter out = resp.getWriter();
		out.println("" + message + "");
		out.flush();
		out.close();
		
	}
	
	protected String handleAnnotations(HttpServletRequest req, HttpServletResponse resp){
		String objectList = req.getParameter("objectList");
		String accountId = req.getParameter("account_id");
		String pdfPath =  req.getParameter("pdf_path");
		String deleteItems = req.getParameter("deleteItems");
		String updateItems = req.getParameter("updateItems");
		
		String reply = "";
		
		try{
			if(! StringUtil.isEmpty(objectList)){
				saveToDB(objectList);
				reply = "done saving";
			}
			else if (! StringUtil.isEmpty(accountId)){
				reply = getAnnotations(accountId,pdfPath);
				if(StringUtils.isEmpty(reply)){
					reply = "No Annotations found with this id="+accountId;
				}
			}else if (! StringUtil.isEmpty(deleteItems)){
				deleteToDb(deleteItems);
				reply = "done deleting="+deleteItems;
			}else if (! StringUtil.isEmpty(updateItems)){
				updateToDb(updateItems);
				reply = "done update="+updateItems;	
			}else{
				reply = "Invalid post/get to servlet";
			}
		}catch(Exception e){
			return "error occurs";
		}
		
		return reply;
	}
	
	protected void updateToDb(String obj) throws Exception{
		try{
			System.out.println("I got the object: "+obj.toString());
			String id[] = obj.split(";");
			for (String string : id) {
				System.out.println("update="+string);
				StringBuffer sb = new StringBuffer((String) string);	
				if(sb.indexOf("pageIndex") > -1){
					String note = getActualString("note",sb);
					String pageIndex = getActualString("pageIndex",sb);
					String positionX = getActualString("positionX",sb);
					String positionY = getActualString("positionY",sb);
					String width = getActualString("width",sb);
					String height = getActualString("height",sb);
					String accountId = getActualString("account_id",sb);
					String pdfPath = getActualString("pdf_path",sb);
					String flexpaperId = getActualString("category",sb);
					
					FlexPaperWorker.udpateNotes(flexpaperId, note, pageIndex, positionX, positionY, width, height, accountId, pdfPath);
				}
				
			}
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	protected void deleteToDb(String obj) throws Exception{
		try{
			System.out.println("I got the object: "+obj.toString());
			String id[] = obj.split(";");
			for (String string : id) {
				System.out.println("delete="+string);
				FlexPaperWorker.deleteNotes(string);
			}
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	protected void saveToDB(String obj) throws Exception{
		try{
			System.out.println("I got the object: "+obj.toString());
			
			
			
			String objectList[] = obj.split(";");
			for (String string : objectList) {
				StringBuffer sb = new StringBuffer((String) string);
				
				if(sb.indexOf("has_selection") > -1){				
					String selectionText = getActualString("selection_text",sb);
					
					String hasSelection = getActualString("has_selection",sb);
					String color = getActualString("color",sb);
					String selectionInfo = getActualString("selection_info",sb);
					
					String accountId = getActualString("account_id",sb);
					String pdfPath = getActualString("pdf_path",sb);
//					System.out.println("inserting to db");
//					System.out.println(" selectiontext="+selectionText);
//					System.out.println(" hasselection="+hasSelection);
//					System.out.println(" color="+color);
//					System.out.println(" selectioninfo="+selectionInfo);
//					System.out.println(" accountid="+accountId);
//					System.out.println(" pdfpath="+pdfPath);
					FlexPaperWorker.insertSelection(selectionText, hasSelection, color, selectionInfo, accountId, pdfPath);
				}else if(sb.indexOf("pageIndex") > -1){
					
					String note = getActualString("note",sb);
					String pageIndex = getActualString("pageIndex",sb);
					String positionX = getActualString("positionX",sb);
					String positionY = getActualString("positionY",sb);
					String width = getActualString("width",sb);
					String height = getActualString("height",sb);

					String accountId = getActualString("account_id",sb);
					String pdfPath = getActualString("pdf_path",sb);
					
					System.out.println("inserting to db");
					System.out.println(" note="+note);
					System.out.println(" pageindex="+pageIndex);
					System.out.println(" positionx="+positionX);
					System.out.println(" positiony="+positionY);
					System.out.println(" width="+width);
					System.out.println(" height="+height);
					System.out.println(" accountid="+accountId);
					System.out.println(" pdfpath="+pdfPath);
					FlexPaperWorker.insertNotes(note, pageIndex, positionX, positionY, width, height, accountId, pdfPath);
					
				}	
				
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	protected String getAnnotations(String accountId, String pdfPath){

		List<FlexPaper> results = FlexPaperWorker.getAllNotes(accountId, pdfPath);

		StringBuffer sb = new StringBuffer();
		sb.append("");
		for (FlexPaper flexPaper : results) {
				sb.append("id:" + flexPaper.getFlexPaperId() + ", ");
			if(StringUtils.isNotEmpty(flexPaper.getHasSelection())){
				sb.append("has_selection:" + flexPaper.getHasSelection() + ", ");
				sb.append("color:" + flexPaper.getColor() + ", ");
				sb.append("selection_text:" + flexPaper.getSelectionText() + "', ");
				sb.append("selection_info:" + flexPaper.getSelectionInfo());
			}else if(StringUtils.isNotEmpty(flexPaper.getPageIndex())){
				sb.append("pageIndex:" + flexPaper.getPageIndex() + ", ");
				sb.append("height:" + flexPaper.getHeight() + ", ") ;
				sb.append("width:" + flexPaper.getWidth() + ", ");
				sb.append("positionX:" + flexPaper.getPositionX() + ", ");
				sb.append("positionY:" + flexPaper.getPositionY() + ", ");
				sb.append("note:" +  flexPaper.getNote());
			}
			sb.append(";");
		}
		
		//System.out.println("sb="+sb.toString());
		
		return sb.toString(); //"has_selection:true, color:#fffc15, selection_text:null, selection_info:1%3B0%3B47;has_selection:true, color:#fffc15, selection_text:%20acquisition%20to%20acquire%20professional%20and%20advanced%20professional%28near-native%29%20levels%20of%20foreign%20language%20pro%uE04Bciency.%20To%20date%2C%20there%20is%20not%20a%20lotknown%20about%20how%20to%20do%20this.%20Among%20the%20authors%20of%20this%20book%20are%20those%20who%20haveachievednear-nativepro%uE04Bciencyinoneormorelanguages%2Cthosewhohavetaughtorsupervisedforeign-languageprogramsforhighlyadvancedstudents%2Candthosewho%20have%20conducted%20research%20on%20the%20at, selection_info:1%3B642%3B1036;pageindex:1, height:200, width:200, positionX:38, positionY:136, note:note%201;pageindex:1, height:200, width:200, positionX:204, positionY:405, note:note%202;has_selection:true, color:#fffc15, selection_text:%20stages%20and%20others%20at%20later%20stages%20in%20the%20typically%20long%20processof%20reaching%20near-native%20levels%20%285%u201317%20years%20is%20the%20norm%29.rIndividuals%20with%20high%20scores%20on%20language%20aptitude%20tests%20reach%20highlevels%20of%20pro%uE04Bciency%3B%20so%20can%20students%20with%20low%20scores%20on%20languageaptitude%20tests.rStudents%20who%20were%20slow%20star, selection_info:2%3B43%3B338;pageindex:2, height:200, width:200, positionX:184, positionY:103, note:note%20on%20page%202;has_selection:true, color:#fffc15, selection_text:%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CF%u25CFWays%20to%20reach%20high%20levels%20of%20profciencyAlthough%20students%20of%20all%20types%20reach%20high%20levels%20of%20pro%uE04Bciency%2C%20theydo%20have%20a%20number%20of%20learning%20approaches%20in%20common.%20T, selection_info:5%3B1012%3B2617;"; //sb.toString();
	}
	
	private String getActualString(String name, StringBuffer sb) {
		int startIndex = sb.indexOf(name);
		String results = "";
		int endIndex = sb.indexOf(",", startIndex);
		if(endIndex == -1){
			results = sb.substring(startIndex);
		}else{
			results = sb.substring(startIndex,endIndex);
		}
		results = results.replace(name+":", "");
		results = StringUtils.trim(results);
		
		//System.out.println("name="+name+"; results="+results);
		return results;
	}
	
	

	
	
	
	
}
