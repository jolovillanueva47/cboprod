package org.cambridge.ebooks.online.annotations;

import java.util.List;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.flexpaper.FlexPaper;

/**
 * 
 * @author apirante
 * 
 */

public class FlexPaperBean {
	
	private static Logger logger = Logger.getLogger(FlexPaperBean.class);

	private String flexPaperId;
	private String selectionText;
	private String hasSelection;
	private String color;
	private String selectionInfo;
	
	private String note;
	private String pageIndex;
	private String positionX;
	private String positionY;
	private String width;
	private String height;
	
	private String accountId;
	private String pdfPath;
	
	public String getInitBean() {
		logger.info("Initialize...");		
		
		
		logger.info("Finished...");		
		return "";
	}
	
	public List<FlexPaper> getAllSelection(){	
		
	
		
		//List<FlexPaper> results = (List<FlexPaper>) FlexPaperWorker.getAllNotes();
		return null;
		
	}

	public String getFlexPaperId() {
		return flexPaperId;
	}

	public void setFlexPaperId(String flexPaperId) {
		this.flexPaperId = flexPaperId;
	}

	public String getSelectionText() {
		return selectionText;
	}

	public void setSelectionText(String selectionText) {
		this.selectionText = selectionText;
	}

	public String getHasSelection() {
		return hasSelection;
	}

	public void setHasSelection(String hasSelection) {
		this.hasSelection = hasSelection;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSelectionInfo() {
		return selectionInfo;
	}

	public void setSelectionInfo(String selectionInfo) {
		this.selectionInfo = selectionInfo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getPositionX() {
		return positionX;
	}

	public void setPositionX(String positionX) {
		this.positionX = positionX;
	}

	public String getPositionY() {
		return positionY;
	}

	public void setPositionY(String positionY) {
		this.positionY = positionY;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

}
