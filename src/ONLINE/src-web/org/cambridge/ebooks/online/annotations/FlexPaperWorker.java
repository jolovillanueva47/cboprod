package org.cambridge.ebooks.online.annotations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.flexpaper.FlexPaper;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author apirante
 * 
 */

public class FlexPaperWorker {

	public static List<FlexPaper> getAllNotes(String accountId, String pdfPath){
		List<FlexPaper> list = PersistenceUtil.searchList(new FlexPaper(), FlexPaper.SEARCH_ALL, accountId, pdfPath);
		return list;
	}

	public static void insertSelection(String selectionText, String hasSelection, String color, String selectionInfo, String accountId, String pdfPath){
		EntityManager em = PersistenceUtil.emf.createEntityManager();  
		try {
			em.getTransaction().begin();
	        Query q = em.createNativeQuery( "INSERT INTO EBOOK_FLEX_PAPER_TABLE(ID, SELECTION_TEXT, HAS_SELECTION, COLOR, SELECTION_INFO, ACCOUNT_ID, PDF_PATH) VALUES(EBOOK_FLEX_PAPER_ID_SEQ.nextval,?1,?2,?3,?4,?5,?6)" );
			q.setParameter(1, selectionText);
			q.setParameter(2, hasSelection);
			q.setParameter(3, color);
			q.setParameter(4, selectionInfo);
			q.setParameter(5, accountId);
			q.setParameter(6, pdfPath);
			q.executeUpdate();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}		
	}
		
	public static void insertNotes(String note, String pageIndex, String positionX, String positionY, String width, String height, String accountId, String pdfPath){
		EntityManager em = PersistenceUtil.emf.createEntityManager();  
		try {
			em.getTransaction().begin();
	        Query q = em.createNativeQuery( "INSERT INTO EBOOK_FLEX_PAPER_TABLE(ID, NOTE, PAGE_INDEX, POSITION_X, POSITION_Y, WIDTH, HEIGHT, ACCOUNT_ID, PDF_PATH) VALUES(EBOOK_FLEX_PAPER_ID_SEQ.nextval,?1,?2,?3,?4,?5,?6,?7,?8)" );
			q.setParameter(1, note);
			q.setParameter(2, pageIndex);
			q.setParameter(3, positionX);
			q.setParameter(4, positionY);
			q.setParameter(5, width);
			q.setParameter(6, height);
			q.setParameter(7, accountId);
			q.setParameter(8, pdfPath);
			q.executeUpdate();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}		
	}

	public static void deleteNotes(String id){
		EntityManager em = PersistenceUtil.emf.createEntityManager();
		try{
			em.getTransaction().begin();
			FlexPaper n = em.find(FlexPaper.class,id);
			em.remove(n);
			em.getTransaction().commit();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally{
			em.close();
		}
	}
	
	public static void udpateNotes(String flexpaperId, String note, String pageIndex, String positionX, String positionY, 
			String width, String height, String accountId, String pdfPath){
		EntityManager em = PersistenceUtil.emf.createEntityManager();
		try{
		    em.getTransaction().begin();
		    FlexPaper fpx = em.find(FlexPaper.class, flexpaperId);
		    fpx.setNote(note);
		    fpx.setPageIndex(pageIndex);
		    fpx.setPositionX(positionX);
		    fpx.setPositionY(positionY);
		    fpx.setWidth(width);
		    fpx.setHeight(height);
		    em.getTransaction().commit();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally{
			em.close();
		}

//		try{
//			em.getTransaction().begin();
//			FlexPaper n = em.find(FlexPaper.class,id);
//			em.remove(n);
//			em.getTransaction().commit();
//		}
//		catch(Exception e){
//			System.out.println(e.getMessage());
//		}
//		finally{
//			em.close();
//		}
	}
	
	
	
	
//
//	public static void updateNotes(String text, String xPos, String yPos, String notepadId){
//		EntityManager em = PersistenceUtil.emf.createEntityManager();  
//		try {
//			em.getTransaction().begin();
//	        Query q = em.createNativeQuery( "UPDATE EBOOKS_NOTEPAD_TABLE SET TEXT = ?1, XPOS = ?2, YPOS = ?3 WHERE ID = ?4" );
//			q.setParameter(1, text);
//			q.setParameter(2, xPos);
//			q.setParameter(3, yPos);
//			q.setParameter(4, notepadId);
//			q.executeUpdate();
//			em.getTransaction().commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			em.close();
//		}
//	}
//	
//	public static List<Notepad> getNotes(String pageId, String bodyId){
//		List<Notepad> list = PersistenceUtil.searchList(new Notepad(), Notepad.GET_NOTES, pageId, bodyId);
//		return list;
//	}
//	

}
