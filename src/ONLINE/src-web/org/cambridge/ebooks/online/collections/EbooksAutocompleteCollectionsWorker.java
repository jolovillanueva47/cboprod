package org.cambridge.ebooks.online.collections;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.collections.EbooksAutocompleteCollections;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.StringUtil;


public class EbooksAutocompleteCollectionsWorker {
	
	private static final Logger LOGGER = Logger.getLogger(EbooksAutocompleteCollectionsWorker.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	private static String ORDER_BY_TITLE_ALPHASORT =  "ORDER BY TITLE_ALPHASORT ";
	private static String ORDER_BY_AUTHOR_ALPHASORT = "ORDER BY AUTHOR_ALPHASORT ";
	
	@SuppressWarnings("unchecked")
	public static List<EbooksAutocompleteCollections> getIsbnList( String collection_id, String sortBy ) { 
		
		if ( StringUtil.isEmpty(collection_id) ) 
			throw new IllegalArgumentException(" getIsbnList(" + collection_id + ") cannot be null ");
		
		final String sql = 
		"select EIDL.ISBN, COLLECTION_ID, EIDL.TITLE || ': ' || EIDL.SUBTITLE TITLE , upper(E.TITLE_ALPHASORT) as TITLE_ALPHASORT , upper(EIDL.author_short) as AUTHOR_ALPHASORT " +
		"from EBOOK_COLLECTION_LIST ECL, EBOOK_ISBN_DATA_LOAD EIDL, EBOOK E " +
		"where COLLECTION_ID = '"+ collection_id + "' " + 
		"and ECL.ISBN = EIDL.ISBN and E.ISBN = ECL.ISBN " +
		getOrderBy(sortBy);
		
		EntityManager em = emf.createEntityManager();

		try {
			List<EbooksAutocompleteCollections> isbnList = new LinkedList<EbooksAutocompleteCollections>();
			isbnList = (List<EbooksAutocompleteCollections>) em.createNativeQuery( sql , EbooksAutocompleteCollections.class).getResultList();
			return isbnList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return null;
	}
	public static String getIsbnListString(String collection_id, String sortBy){
		
		List<EbooksAutocompleteCollections> isbnList = getIsbnList(collection_id, sortBy);
		
		StringBuffer sb = new StringBuffer();
		
		int sizeList = isbnList == null ? 0 : isbnList.size();
		for (int i = 0 ; i < sizeList ; i++) {
			sb.append(isbnList.get(i).getIsbn());
			if((i < isbnList.size() - 1)){
				sb.append(" ");
				sb.append("OR");
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	public static String getIsbnListString(String collection_id){
		
		
		return getIsbnListString(collection_id, "");
	}
	public static List<EbooksAutocompleteCollections> getIsbnListStartsWith(String collection_id, String letter, String sortBy ) { 
		
		if ( StringUtil.isEmpty(collection_id) || StringUtil.isEmpty(letter) ) 
			throw new IllegalArgumentException(" cannot be null ");
				
		String sql = 
			"SELECT s.collection_id, e.ISBN, e.title, upper(e.title_alphasort) AS TITLE_ALPHASORT, " +
			"upper(s.author_short) AS AUTHOR_ALPHASORT  " +
			"FROM " +
			"( " +
			"  SELECT EIDL.ISBN, COLLECTION_ID, EIDL.TITLE || ': ' || EIDL.SUBTITLE TITLE , E.TITLE_ALPHASORT, EIDL.AUTHOR_SHORT " +
			"  FROM EBOOK_COLLECTION_LIST ECL, EBOOK_ISBN_DATA_LOAD EIDL, EBOOK E " +
			"  WHERE COLLECTION_ID = '"+ collection_id + "' " +
			"  AND ECL.ISBN = EIDL.ISBN " +
			"  AND E.ISBN = ECL.ISBN " +
			") s, " +
			"ebook e " +
			"WHERE e.isbn = s.isbn ";
        
		sql +=
			"AND upper(substr( e.title_alphasort,1,1)) LIKE '"+ letter + "' "; 

		
        sql += getOrderBy(sortBy) ; 
				
		EntityManager em = emf.createEntityManager();

		try {
			List<EbooksAutocompleteCollections> isbnList = new LinkedList<EbooksAutocompleteCollections>();
			isbnList = (List<EbooksAutocompleteCollections>) em.createNativeQuery( sql , EbooksAutocompleteCollections.class).getResultList();
			return isbnList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return null;
	}
	private static String getOrderBy(String sortBy) {
		if(CboCollectionDataWorker.AUTHOR_ALPHASORT.equals(sortBy)){
			return ORDER_BY_AUTHOR_ALPHASORT;
		}else{
			return ORDER_BY_TITLE_ALPHASORT;
		}
	}

}
