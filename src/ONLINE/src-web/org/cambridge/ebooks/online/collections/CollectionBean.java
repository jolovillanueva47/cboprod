package org.cambridge.ebooks.online.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.collections.EbooksAutocompleteCollections;
import org.cambridge.ebooks.online.jpa.topbooks.EBookTopFeaturedCollections;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.topbooks.TopBooksBean;
import org.cambridge.ebooks.online.util.HttpUtil;

public class CollectionBean {
	private static Logger logger = Logger.getLogger(CollectionBean.class);	

	private List<BookMetaData> bookMetaDataList;
	private List<EbooksAutocompleteCollections> isbnlist;
		
	private Map<String, String> alphaMap;
	
	private HttpServletRequest req = HttpUtil.getHttpServletRequest();
	
	private String collectionName;
	private String collectionId;

	private String firstLetter;
	
	private String showVolumeSort;
	
	private long resultsFound;
	private long pages;
	private int resultsPerPage;
	private int currentPage;

    public String getInitAlphaMap() {
    	logger.info(" == START INIT ALPHA MAP == ");
    	List<EBookTopFeaturedCollections> topCollection = new TopBooksBean().getTopFeaturedCollectionsList();
		EBookTopFeaturedCollections e = new EBookTopFeaturedCollections();
		e.setCollectionId(getCollectionId());
		int index = topCollection.indexOf(e);
		if(index != -1){
			EBookTopFeaturedCollections res = topCollection.get(index);
			
			Map<String,String> alphaMap = new LinkedHashMap<String, String>();
			
			for (String string : CboCollectionDataWorker.ALPHABETS) {
				String count = "";
				
				if(string.contains("A")){
					count = res.getLetterA();
				}else if(string.contains("B")){
					count = res.getLetterB();
				}else if(string.contains("C")){
					count = res.getLetterC();
				}else if(string.contains("D")){
					count = res.getLetterD();
				}else if(string.contains("E")){
					count = res.getLetterE();
				}else if(string.contains("F")){
					count = res.getLetterF();
				}else if(string.contains("G")){
					count = res.getLetterG();
				}else if(string.contains("H")){
					count = res.getLetterH();
				}else if(string.contains("I")){
					count = res.getLetterI();
				}else if(string.contains("J")){
					count = res.getLetterJ();
				}else if(string.contains("K")){
					count = res.getLetterK();
				}else if(string.contains("L")){
					count = res.getLetterL();
				}else if(string.contains("M")){
					count = res.getLetterM();
				}else if(string.contains("N")){
					count = res.getLetterN();
				}else if(string.contains("O")){
					count = res.getLetterO();
				}else if(string.contains("P")){
					count = res.getLetterP();
				}else if(string.contains("Q")){
					count = res.getLetterQ();
				}else if(string.contains("R")){
					count = res.getLetterR();
				}else if(string.contains("S")){
					count = res.getLetterS();
				}else if(string.contains("T")){
					count = res.getLetterT();
				}else if(string.contains("U")){
					count = res.getLetterU();
				}else if(string.contains("V")){
					count = res.getLetterV();
				}else if(string.contains("W")){
					count = res.getLetterW();
				}else if(string.contains("X")){
					count = res.getLetterX();
				}else if(string.contains("Y")){
					count = res.getLetterY();
				}else if(string.contains("Z")){
					count = res.getLetterZ();
				}
				
				if(StringUtils.isNotEmpty(count)){
					count = "link";
				}
				
				alphaMap.put(string, count);
			}
			
			setAlphaMap(alphaMap);
		}else{
			logger.info("bean not found for alpha map process");
		}
		logger.info(" == END INIT ALPHA MAP == ");
    	return "";
    }
   
	public String getInitPage() {
		logger.info("Initialize...");
		
		req = HttpUtil.getHttpServletRequest();		

		String firstLetter = req.getParameter("firstLetter");
		String sortBy = req.getParameter("sort");
		
		String collectionId = req.getParameter("collectionId");
		String collectionName = req.getParameter("collectionName");
		
		List<EbooksAutocompleteCollections> isbnlist = new LinkedList<EbooksAutocompleteCollections>();
		if(StringUtils.isNotEmpty(firstLetter)&& !(firstLetter.equals("allTitles"))){
			isbnlist = EbooksAutocompleteCollectionsWorker.getIsbnListStartsWith(collectionId, firstLetter,sortBy);
		}else{
			isbnlist = EbooksAutocompleteCollectionsWorker.getIsbnList(collectionId,sortBy);
		}
		
		setIsbnlist(isbnlist);
					
		int results = 0;
		int page = 0;
		
		if((StringUtils.isNotEmpty(req.getParameter("results")) 
				&& StringUtils.isNumeric(req.getParameter("results")))) {
			results = Integer.parseInt(req.getParameter("results"));
		}
		if(StringUtils.isNotEmpty(req.getParameter("page")) && StringUtils.isNumeric(req.getParameter("page"))) {
			page = Integer.parseInt(req.getParameter("page"));
		}
		
		CollectionDataWorker worker = new CboCollectionDataWorker(req);		
		CollectionBean bean = null;
		
		Map<String, Object> param = new HashMap<String, Object>();
    	param.put(CboCollectionDataWorker.getKeyByLetter(), firstLetter);
    	param.put(CboCollectionDataWorker.getKeyPageNumber(), page);
    	param.put(CboCollectionDataWorker.getKeyPageSize(), results);
    	param.put(CboCollectionDataWorker.getKeySortBy(), sortBy);
    	param.put(CboCollectionDataWorker.getKeyCollectionId(), collectionId);
    	param.put(CboCollectionDataWorker.getKeyIsbnList(), isbnlist);
    	
    	bean = worker.generateResult(param); 
    	
    	populateBean(bean);
		setResultsPerPage(results);
    	setCollectionId(collectionId);
   
		setFirstLetter(firstLetter);
		setCollectionName(collectionName);
		
		logger.info("Finished...");		
		return "";
	}			
	
	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getCollectionId() {
		return collectionId;
	}	
	
	public String getShowVolumeSort() {
		return showVolumeSort;
	}
	public void setShowVolumeSort(String showVolumeSort) {
		this.showVolumeSort = showVolumeSort;
	}
	public Map<String, String> getAlphaMap() {
		return alphaMap;
	}
	public void setAlphaMap(Map<String, String> alphaMap) {
		this.alphaMap = alphaMap;
	}
	public String getCollectionDesc() {
		String desc = "";
		List<EBookTopFeaturedCollections> topCollection = new TopBooksBean().getTopFeaturedCollectionsList();
		EBookTopFeaturedCollections e = new EBookTopFeaturedCollections();
		e.setCollectionId(getCollectionId());
		int index = topCollection.indexOf(e);
		if(index != -1){
			EBookTopFeaturedCollections res = topCollection.get(index);
			desc = StringEscapeUtils.escapeXml(res.getCollectionDescription());
		}
		return desc;
	}
	public String getCollectionCreationDate() {
		String creation_date = null;
		List<EBookTopFeaturedCollections> topCollection = new TopBooksBean().getTopFeaturedCollectionsList();
		EBookTopFeaturedCollections e = new EBookTopFeaturedCollections();
		e.setCollectionId(getCollectionId());
		int index = topCollection.indexOf(e);
		if(index != -1){
			EBookTopFeaturedCollections res = topCollection.get(index);
			creation_date = res.getFormattedCreationDate("MMMM dd, yyyy");
		}
		return creation_date;
	}
	
	
	public String getFirstLetter() {
		return firstLetter;
	}
	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}
	public List<BookMetaData> getBookMetaDataList() {
		return bookMetaDataList;
	}
	public void setBookMetaDataList(List<BookMetaData> bookMetaDataList) {
		this.bookMetaDataList = bookMetaDataList;
	}
	public long getResultsFound() {
		return resultsFound;
	}
	public void setResultsFound(long resultsFound) {
		this.resultsFound = resultsFound;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	public long getPages() {		
		pages = 0;
		if(resultsFound > 0) {
			pages = ((int)(resultsFound / resultsPerPage) + (resultsFound % resultsPerPage > 0 ? 1 : 0));
		}
		return pages;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	private void populateBean(CollectionBean bean) {	
		setBookMetaDataList(bean.getBookMetaDataList());
		setResultsFound(getIsbnlist().size());
		setResultsPerPage(bean.getResultsPerPage());
		setCurrentPage(bean.getCurrentPage());
		setShowVolumeSort(bean.getShowVolumeSort());
	}
	
	public void setIsbnlist(List<EbooksAutocompleteCollections> isbnlist) {
		this.isbnlist = new LinkedList<EbooksAutocompleteCollections>();
		this.isbnlist = isbnlist;
	}

	public List<EbooksAutocompleteCollections> getIsbnlist() {
		return isbnlist;
	}

	public List<String> getIsbnlist_isbnOnly() {
		
		List<String> temp = new ArrayList<String>();
		for (EbooksAutocompleteCollections item : isbnlist) {
			temp.add(item.getIsbn());
		}
		
		return temp;
	}
	public String getBookMeta(){
		return new CboCollectionDataWorker(req).getMetaDataString(getBookMetaDataList());
	}
	public String getViewMore(){
		logger.info(" == START doJsProcess == ");
		String isbn = req.getParameter("paramIsbn");
		String pageSize = req.getParameter("pageSize");
		String sortBy = req.getParameter("paramSortBy");
		
		CboCollectionDataWorker worker = new CboCollectionDataWorker(req);
		
		List<BookMetaData> bookMetaDataList = worker.getBookMetaData(isbn, sortBy, Integer.parseInt(pageSize));
		String res = worker.getMetaDataString(bookMetaDataList);
		res = res.replace("\"content_wrapper04\"", "\"content_wrapper05\"");
		logger.info(" == END doJsProcess == ");
		return res;
	}
}
