package org.cambridge.ebooks.online.collections;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.cambridge.ebooks.online.jpa.collections.EbooksAutocompleteCollections;

public interface CollectionDataWorker {
	public SolrQuery generateQuery(String collectionId, List<EbooksAutocompleteCollections> isbnlist);
	public CollectionBean generateResult(Map<String, Object> param);
	public int getDefaultPageSize();
}
