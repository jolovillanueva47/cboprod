package org.cambridge.ebooks.online.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.accessibility.AccessibilityWorker;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.collections.EbooksAutocompleteCollections;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.search.solr.SolrQueryFactory;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrServerUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

public class CboCollectionDataWorker implements CollectionDataWorker {

	private static final Logger logger = Logger.getLogger(CboCollectionDataWorker.class);	
	
	public static final String[] ALPHABETS = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	
	protected static final int DEF_PAGE_SIZE = 50;
	
	private static final String KEY_SORT_BY = "sort_by";
	private static final String KEY_BY_LETTER = "by_letter";
	private static final String KEY_PAGE_NUMBER = "page_number";
	private static final String KEY_PAGE_SIZE = "page_size";
	private static final String KEY_HTTP_SESSION = "http_session";
	private static final String KEY_COLLECTION_ID = "collection_id";
	private static final String KEY_ISBN_LIST = "isbn_list";
	
	public static final String TITLE_ALPHASORT = "title_alphasort";
	public static final String AUTHOR_ALPHASORT = "author_name_alphasort";
	
	private List<EbooksAutocompleteCollections> isbnlist;
	
	private HttpServletRequest request;
	
	public CboCollectionDataWorker(HttpServletRequest request){
		this.request = request;
	}
	
	public static String getKeyByLetter() {
		return KEY_BY_LETTER;
	}	
	public static String getKeyPageNumber() {
		return KEY_PAGE_NUMBER;
	}
	public static String getKeyPageSize() {
		return KEY_PAGE_SIZE;
	}
	public static String getKeyHttpSession() {
		return KEY_HTTP_SESSION;
	}	
	public static String getKeySortBy() {
		return KEY_SORT_BY;
	}
	public static String getKeyCollectionId() {
		return KEY_COLLECTION_ID;
	}
	public static String getKeyIsbnList() {
		return KEY_ISBN_LIST;
	}
	
	public CollectionBean generateResult(Map<String, Object> param) {		
		
		String collectionId= "";
		int pageNum = 0;
		int pageSize = 0;
		String sortBy = "";
		String byLetter = "";
		isbnlist = new LinkedList<EbooksAutocompleteCollections>();
		
		if(null != param.get(KEY_BY_LETTER)) {
			byLetter = (String)param.get(KEY_BY_LETTER);
		}		
		
		if(null != param.get(KEY_PAGE_NUMBER)) {
			pageNum = ((Integer)param.get(KEY_PAGE_NUMBER)).intValue();
		}
		if(null != param.get(KEY_PAGE_SIZE)) {
			pageSize = ((Integer)param.get(KEY_PAGE_SIZE)).intValue();
		}
		if(null != param.get(KEY_SORT_BY)) {
			sortBy = (String)param.get(KEY_SORT_BY);
		}
		if(null != param.get(KEY_COLLECTION_ID)) {
			collectionId = (String)param.get(KEY_COLLECTION_ID);
		}
		if(null != param.get(KEY_ISBN_LIST)) {
			isbnlist = (List<EbooksAutocompleteCollections>) param.get(KEY_ISBN_LIST);
		}
		
		CollectionBean bean = new CollectionBean();
		if(null!=isbnlist && (AccessibilityWorker.isPathAAA() || isbnlist.size() <= pageSize)){
			populateAll(isbnlist, pageNum, pageSize, sortBy, byLetter, bean);
		}else{
			populateAll(getIsbnChunk(isbnlist, 0, pageSize), pageNum, pageSize, sortBy, byLetter, bean);
		}		
		bean.setIsbnlist(isbnlist);	
				
		return bean;
	}

	public int getDefaultPageSize() {
		return DEF_PAGE_SIZE;
	}

	private void configureSort(SolrQuery q, String sortBy) {
		if(StringUtils.isEmpty(sortBy)) {
			//q.setSortField("series_alphasort", ORDER.asc);
			q.setSortField("title_alphasort", ORDER.asc);
		} else {
			if("print_date".equals(sortBy) || "online_date".equals(sortBy)) {
				q.setSortField(sortBy, ORDER.desc);	
			} else {
				q.setSortField(sortBy, ORDER.asc);
			}
		}
	}
	
	private void configurePager(SolrQuery q, int pageNum, int pageSize) {
		if(pageNum > 0) {
			q.setStart((pageNum - 1) * getDefaultPageSize());
		} else {
			q.setStart(0);
		}
		if(pageSize > 0) {
			q.setRows(pageSize);
		} else {
			q.setRows(getDefaultPageSize());
		}
	}
	
	public SolrQuery generateQuery(String collectionId, List<EbooksAutocompleteCollections> isbnlist ) {
		
		String isbns = "";		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}		
		SolrQuery q = SolrQueryFactory.newCboOnlyInstance(isbns);
		
		String query = SolrUtil.generateLongOrQuery(isbnlist, "isbn");
		
		q.setQuery("isbn:" + query);	
		
		configureSort(q, "");
		configurePager(q, 0, 0);	
		
		return q;	
	}
		
	public void populateAll(List<EbooksAutocompleteCollections> isbnlist,
			int pageNum, int pageSize, String sortBy, 
			String byLetter, CollectionBean bean){
		logger.info(" == start populateAll == ");
		if(null==isbnlist){
			return;
		}
		
		int size = isbnlist.size();
		
		int rem = size % pageSize;
		int quo = (size - rem) / pageSize;
		int start = 0;
		int stop = start + pageSize;
		
		if(rem!=0){
			quo++;
		}	
		List<BookMetaData> meta = new ArrayList<BookMetaData>();
		
		for(int i = 1 ; i <= quo ; i++){
			if(stop > size){
				stop = size;
			}
			logger.info("start="+start+"; stop="+stop);
			List<EbooksAutocompleteCollections> chunk_isbn =
				getIsbnChunk(isbnlist, start, stop);

			StringBuffer sb = new StringBuffer();
			for (EbooksAutocompleteCollections e : chunk_isbn) {
				sb.append(e.getIsbn());
				sb.append(",");
			}
			
			List<BookMetaData> tmp = getBookMetaData(sb.toString(), sortBy, pageSize);
			
			for (BookMetaData b : tmp) {
				meta.add(b);
			}
			
			start+=pageSize;
			stop+=pageSize;	
			

		}//end for loop
		
		bean.setBookMetaDataList(meta);
		logger.info(" == end populateAll == ");
	}

	private List<EbooksAutocompleteCollections> getIsbnChunk(List<EbooksAutocompleteCollections> isbnlist, int start, int stop) {
		List<EbooksAutocompleteCollections> temp = new ArrayList<EbooksAutocompleteCollections>();
		for(int i = start ; i < stop ; i++){
			temp.add(isbnlist.get(i));
		}
		return temp;
	}
	
	public List<BookMetaData> getBookMetaData(String isbn, String sortBy, int pageSize){
		logger.info(" == START getBookMetaData == ");
		
		if(StringUtils.isEmpty(isbn)){
			logger.error(" == ERROR ISBN IS EMPTY == ");
			return null;
		}
		
		//variables
		SubscriptionUtil subscription = new SubscriptionUtil(OrgConLinkedMap.getAllBodyIds(request.getSession()), OrgConLinkedMap.getDirectBodyIds(request.getSession()));
		List<BookAndChapterSolrDto> bookDocList = null;
		QueryResponse response = null;
		// SolrQuery start =================
		String isbns = "";		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}		
		SolrQuery q = SolrQueryFactory.newCboOnlyInstance(isbns);
		// SolrQuery end =================
		List<BookMetaData> bookMetaDataList = new ArrayList<BookMetaData>();
		String[] sArr = isbn.split(",");
		
		
		//do solr query
		q.setQuery("isbn:" + SolrUtil.generateLongOrQuery(isbn));
		
		configureSort(q,sortBy);
		configurePager(q, 0, pageSize);
		
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			bookDocList = response.getBeans(BookAndChapterSolrDto.class);	
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}//end try
		
		//iterate bookdoclist
		for(BookAndChapterSolrDto doc : bookDocList) {					
			BookMetaData bookMetaData = new BookMetaData();	
			//setbookmeta start
			bookMetaData.setId(doc.getId());
			bookMetaData.setTitle(doc.getTitle());
			bookMetaData.setSubtitle(doc.getSubtitle());
			bookMetaData.setEdition(doc.getEdition());
			bookMetaData.setEditionNumber(doc.getEditionNumber());
			bookMetaData.setVolumeNumber(doc.getVolumeNumber());
			bookMetaData.setVolumeTitle(doc.getVolumeTitle());
			bookMetaData.setPartNumber(doc.getPartNumber());
			bookMetaData.setPartTitle(doc.getPartTitle());
			int imageCtr = 0;
			if(null != doc.getCoverImageTypeList()) {
				for(String imageType : doc.getCoverImageTypeList()) {
					if("standard".equalsIgnoreCase(imageType)) {
						bookMetaData.setStandardImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
					} else  {
						bookMetaData.setThumbnailImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
					}
					imageCtr++;
				}
			}
			bookMetaData.setAuthorNameList(doc.getAuthorNameList());
			bookMetaData.setAuthorNameLfList(doc.getAuthorNameLfList());
			bookMetaData.setAuthorAffiliationList(doc.getAuthorAffiliationList());				
			bookMetaData.setAuthorRoleList(doc.getAuthorRoleList());
			bookMetaData.setOnlineIsbn(doc.getIsbn());
			bookMetaData.setHardbackIsbn(doc.getAltIsbnHardback());
			bookMetaData.setPaperbackIsbn(doc.getAltIsbnPaperback());
			bookMetaData.setAltIsbnEisbn(doc.getAltIsbnEisbn());
			bookMetaData.setAltIsbnOther(doc.getAltIsbnOther());
			bookMetaData.setSeries(doc.getSeries());
			bookMetaData.setSeriesCode(doc.getSeriesCode());
			//if(doc.getSeriesNumber() > 0) {
				//bookMetaData.setSeriesNumber(String.valueOf(doc.getSeriesNumber()));
			//}
			bookMetaData.setPrintDate(doc.getPrintDateDisplay());
			bookMetaData.setOnlineDate(doc.getOnlineDateDisplay());
			bookMetaData.setDoi(doc.getDoi());
			bookMetaData.setSubjectCodeList(doc.getSubjectCodeList());
			bookMetaData.setSubjectList(doc.getSubjectList());
			bookMetaData.setSubjectLevelList(doc.getSubjectLevelList());					
			bookMetaData.setPublisherId(doc.getPublisherId());
			bookMetaData.setPublisherName(doc.getPublisherName());
			bookMetaData.setPublisherLoc(doc.getPublisherLoc());
			
			StringBuilder copyrightStatement = new StringBuilder();
			int ctr = 0;
			for(String s : doc.getCopyrightStatementList()) {
				ctr++;					
				copyrightStatement.append(s);
				if(ctr < doc.getCopyrightStatementList().size()) {
					copyrightStatement.append(", ");
				}
			}
			bookMetaData.setAuthorSingleline(doc.getAuthorSingleline());
			bookMetaData.setCopyrightStatement(copyrightStatement.toString());

			bookMetaData.setBlurb(doc.getBlurb());
			//setbookmeta end
			
			EBooksAccessListView vw = subscription.getAccessType(bookMetaData.getOnlineIsbn(), request, false);			
			bookMetaData.setAccessType(vw.hasAccessDisplay());
			
			bookMetaDataList.add(bookMetaData);
		}//end loop	
		
		//recreate the bookMetaDataList according to isbnList
		logger.info(" == START recreate bookMetaDataList == ");
		List<BookMetaData> recreatedList = new ArrayList<BookMetaData>();
		int current_index = 0;
		for (BookMetaData currentBmd : bookMetaDataList) {
			String currentIsbn = currentBmd.getOnlineIsbn();
			if(!currentIsbn.equals(sArr[current_index])){
				int new_index = -1;
				
				int counter = 0;
				for (BookMetaData bmd : bookMetaDataList) {
					String strOnlineIsbn = bmd.getOnlineIsbn();
					if(strOnlineIsbn.equals(sArr[current_index])){
						new_index = counter;
						break;
					}
					counter++;
				}
				
				if(new_index != -1){
					recreatedList.add(bookMetaDataList.get(new_index));
				}else{
					recreatedList.set(recreatedList.size()-1, currentBmd);
				}
			}else{
				recreatedList.add(currentBmd);
			}
			current_index++;
		}
		logger.info(" == END recreate bookMetaDataList == ");
		
		logger.info(" == END getBookMetaData == ");
		
		return recreatedList;
	}

	public String getMetaDataString(List<BookMetaData> bookMetaData){
		logger.info(" == START getMetaDataString == ");
		StringBuffer sb = new StringBuffer();
		boolean firstItem = true;
		for (BookMetaData bmd : bookMetaData) {
		
			if(firstItem){
				sb.append("<div class=\"content_wrapper04\">");
				firstItem=false;
			} else {
				sb.append("<div class=\"content_wrapper05\">");	
			}
				
			String accessType = bmd.getAccessType();
			if(accessType.equalsIgnoreCase("P") || accessType.equalsIgnoreCase("S") || accessType.equalsIgnoreCase("M")){
				sb.append("<div class=\"icons_img purchase\" title=\"You have access.\"></div>");
			} else {
				sb.append("<div class=\"\" title=\"You don't have access.\"></div>");
			}
			sb.append("<div class=\"div_search_info\">");
			sb.append("<span class=\"search_info_item\"><a class=\"link05\" href=\"ebook.jsf?bid="+bmd.getId()+"\">" + bmd.getTitle() + "</a></span><br/>");

			if(StringUtils.isNotEmpty(bmd.getVolumeTitle())){
				sb.append("<b>"+ bmd.getVolumeTitle() + ",</b>");
			}
			if(StringUtils.isNotEmpty(bmd.getEdition())){
				sb.append("<b>"+ bmd.getEdition() + ",</b>");
			}
			if(StringUtils.isNotEmpty(bmd.getSubtitle())){
				sb.append("<b>"+ bmd.getSubtitle() + "</b>");
			}

			if(StringUtils.isNotEmpty(bmd.getAuthorSingleline())){
				if(StringUtils.isNotEmpty(bmd.getVolumeTitle()) || StringUtils.isNotEmpty(bmd.getEdition()) || (StringUtils.isNotEmpty(bmd.getSubtitle())) ){
					sb.append("<br/>");
				}
				sb.append(bmd.getAuthorSingleline() + "<br/>");
			}
			
			sb.append("<div class=\"clear_bottom\"></div>");

			if(StringUtils.isNotEmpty(bmd.getSeries())){
				sb.append(bmd.getSeries());
				if(StringUtils.isNotEmpty(bmd.getSeriesNumber())){
					sb.append("(No." + bmd.getSeriesNumber() + ")");
				}
				
			}

			sb.append("<div class=\"list_divider01\">");
			if(StringUtils.isNotEmpty(bmd.getPrintDate())){
				sb.append("<b>Print Publication Year:</b>&nbsp;" + bmd.getPrintDate() + "<br/>");
			}
			String pisbn = null;
			if(StringUtils.isNotEmpty(bmd.getHardbackIsbn())){
				pisbn = bmd.getHardbackIsbn();
			}else if(StringUtils.isNotEmpty(bmd.getPaperbackIsbn())){
				pisbn = bmd.getPaperbackIsbn();
			}else if(StringUtils.isNotEmpty(bmd.getAltIsbnEisbn())){
				pisbn = bmd.getAltIsbnEisbn();
			}else if(StringUtils.isNotEmpty(bmd.getAltIsbnOther())){
				pisbn = bmd.getAltIsbnOther();
			}
			if( StringUtils.isNotEmpty(pisbn) ){
				sb.append("<b>Print ISBN:</b>&nbsp;" + pisbn);
			}
			sb.append("</div>");

			sb.append("<div class=\"list_divider01\">");
			if(StringUtils.isNotEmpty(bmd.getOnlineDate())){
				sb.append("<b>Online Publication Date:</b>&nbsp;" + bmd.getOnlineDate() + "<br/>");
			}
			if(StringUtils.isNotEmpty(bmd.getOnlineIsbn())){
				sb.append("<b>Online ISBN:</b>&nbsp;" + bmd.getOnlineIsbn() + "<br/>");
			}
			sb.append("</div>");

			sb.append("<div class=\"list_divider01\">");
			if(StringUtils.isNotEmpty(bmd.getDoi())){
				sb.append("<b>Book DOI:</b>&nbsp;<a href=\"http://dx.doi.org/" + bmd.getDoi() + "\">http://dx.doi.org/" + bmd.getDoi() + "</a><br/>");
			}
			sb.append("</div>");
			sb.append("<div class=\"clear_bottom\"></div>");
			sb.append("<p>" + bmd.getBlurb() + "</p>");		
			sb.append("</div></div>");

		}
		
		logger.info(" == END getMetaDataString == ");
		return sb.toString();
	}
	
}
