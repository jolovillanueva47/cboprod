package org.cambridge.ebooks.online.topbooks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.topbooks.EBookTopFeaturedCollections;
import org.cambridge.ebooks.online.jpa.topbooks.EBookTopFeaturedTitles;
import org.cambridge.ebooks.online.jpa.topbooks.EBookTopSeries;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.SolrUtil;


public class TopBooksBean {

private static final Logger LOGGER = Logger.getLogger(TopBooksBean.class);
	
	private static long lastDay;
	private static List<EBookTopSeries> topSeriesList;
	
	private static List<EBookTopSeries> topSeriesListCLC;
	private static List<EBookTopFeaturedCollections> topFeaturedCollectionsList;
	private static List<EBookTopFeaturedTitles> topFeaturedTitleList;
	
	private String initTopBooksSeries;	
	
	public String getInitTopBooksSeries(){
		
		boolean letsSearchForToday;
		long now = System.currentTimeMillis() / (1000*60*60*24);
		if (lastDay == 0) {
			letsSearchForToday = true;
			lastDay = now;
		} else {
			letsSearchForToday = now > lastDay;
		}
		
		if (letsSearchForToday) {
			initOnStartup();
			lastDay = now;
		}
		
		return initTopBooksSeries;
	}
	
	public void initOnStartup(){
		getSeriesListCLC();			
		getFeaturedCollectionList();
		getFeaturedTitleList();
	}	
	
	private void getSeriesListCLC() {
		ArrayList<EBookTopSeries> seriesList = PersistenceUtil.searchList(new EBookTopSeries(), EBookTopSeries.ALL_SERIES_CLC, "CLC");
		ArrayList<String> seriesCodeList = new ArrayList<String>();
		ArrayList<EBookTopSeries> _seriesList = new ArrayList<EBookTopSeries>();
		if(seriesList.size() > 0){			
			String query = SolrUtil.generateLongOrQuery(seriesList, "seriesCode");
			List<BookAndChapterSolrDto> docs = SolrUtil.seriesExistsMapBySeriesCodes(query);
			
			Map<String, String> map = new HashMap<String, String>();
			
			for(BookAndChapterSolrDto doc : docs){			
				map.put(doc.getSeriesCode(), doc.getSeriesCode());			
			}
			
			for(EBookTopSeries series : seriesList){
				if(map.containsKey(series.getSeriesCode())){
					String seriesCode = series.getSeriesCode();
					if(!seriesCodeList.contains(seriesCode)){
						_seriesList.add(series);
						seriesCodeList.add(seriesCode);
					}
				}			
			}
		}
		setTopSeriesListCLC(_seriesList);		
	}
	
	
	private void getFeaturedCollectionList() {
		LOGGER.info("===== generate Featured Collections");
		
		ArrayList<EBookTopFeaturedCollections> collectionList = 
			PersistenceUtil.searchList(new EBookTopFeaturedCollections(), 
				EBookTopFeaturedCollections.SEARCH_TOP_COLLECTIONS);
		setTopFeaturedCollectionsList(collectionList);
	}

	private void getFeaturedTitleList() {
		LOGGER.info("===== generate Featured Titles");
		
		ArrayList<EBookTopFeaturedTitles> collectionList = 
			PersistenceUtil.searchList(new EBookTopFeaturedTitles(), 
				EBookTopFeaturedTitles.SEARCH_TOP_FEATURED_TITLES);
		setTopFeaturedTitleList(collectionList);
	}
	
	public List<EBookTopSeries> getTopSeriesList() {
		return topSeriesList;
	}

	public void setTopSeriesList(List<EBookTopSeries> topSeriesList) {
		TopBooksBean.topSeriesList = topSeriesList;
	}
	
	public List<EBookTopFeaturedTitles> getTopFeaturedTitleList() {
		return topFeaturedTitleList;
	}
	
	public void setTopFeaturedTitleList(List<EBookTopFeaturedTitles> topFeaturedTitleList) {
		TopBooksBean.topFeaturedTitleList = topFeaturedTitleList;
	}
		
	public List<EBookTopSeries> getTopSeriesListCLC() {
		return topSeriesListCLC;
	}

	public void setTopSeriesListCLC(List<EBookTopSeries> topSeriesListCLC) {
		TopBooksBean.topSeriesListCLC = topSeriesListCLC;
	}
		
	public List<EBookTopFeaturedCollections> getTopFeaturedCollectionsList() {
		return topFeaturedCollectionsList;
	}

	public void setTopFeaturedCollectionsList(List<EBookTopFeaturedCollections> topFeaturedCollections) {
		TopBooksBean.topFeaturedCollectionsList = topFeaturedCollections;
	}
	
}
