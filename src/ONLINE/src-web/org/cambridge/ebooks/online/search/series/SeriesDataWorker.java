package org.cambridge.ebooks.online.search.series;

import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.solr.client.solrj.SolrQuery;

public interface SeriesDataWorker {
	public SolrQuery generateAllQuery(String byLetter, JSONArray allLeaves);
	public SeriesBean generateAllResult(Map<String, Object> param);
	public SeriesBean generateAlphaMap(Map<String, Object> param);
	public SolrQuery generateQuery(String seriesCode, String byLetter, JSONArray allSubject);
	public SeriesBean generateResult(Map<String, Object> param);
	public int getDefaultPageSize();
}
