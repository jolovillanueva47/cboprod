/**
 * 
 */
package org.cambridge.ebooks.online.search.solr;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.cambridge.ebooks.online.dto.solr.SearchSolrDto;
import org.cambridge.ebooks.online.util.SolrQueryResponseUtil;

/**
 * @author kmulingtapang
 *
 */
public class ResultGroup {

	private String query;
	private String exclusionQuery;
	private boolean isEmpty;	
	private int totalGroups;
	private int totalDocuments;
	private List<SearchSolrDto> searchSolrDocs;
	
	public ResultGroup(QueryResponse response) {
		query = "";
		exclusionQuery = "";
		isEmpty = true;
		totalGroups = SolrQueryResponseUtil.getTotalGroups(response);
		totalDocuments = SolrQueryResponseUtil.getTotalDocuments(response);		
		searchSolrDocs = new ArrayList<SearchSolrDto>();
		
		if(totalGroups > 0) {			
			isEmpty = false;
		}
		
		GroupField groupField = SolrQueryResponseUtil.getGroups(response);		
		if(null != groupField) {		
			StringBuilder querySb = new StringBuilder("(");
			StringBuilder notQuerySb = new StringBuilder("(");	
						
			int ctr = 0;			
			for(Group group : groupField) {				
				ctr++;					
				String bookId = group.getGroupValue();				
				querySb.append(bookId);
				
				SolrDocumentList solrDocumentList = group.getResult();	
				int i = 0;
				for(SolrDocument solrDocument : solrDocumentList) {
					i++;
					if(1 == i) { // result set 
						SearchSolrDto searchDto = SolrQueryResponseUtil.getSearchSolrDto(solrDocument);
						searchSolrDocs.add(searchDto);
						
						notQuerySb.append(searchDto.getId());
					} 
				}
				
				if(ctr < groupField.size()) {
					querySb.append(" OR ");
					notQuerySb.append(" OR ");
				}
			}
			querySb.append(")");	
			notQuerySb.append(")");
			
			query = querySb.toString();
			exclusionQuery = notQuerySb.toString();
		}
	}

	public String getQuery() {
		return query;
	}

	public String getExclusionQuery() {
		return exclusionQuery;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public int getTotalGroups() {
		return totalGroups;
	}

	public int getTotalDocuments() {
		return totalDocuments;
	}

	public List<SearchSolrDto> getSearchSolrDocs() {
		return searchSolrDocs;
	}	
}
