package org.cambridge.ebooks.online.search.solr;

import java.util.List;

public class SearchResultBean {	
	private List<SearchResult> searchResultList;
	private String spellCheckTerm;
	private long resultsFound;
	private long pages;
	private int resultsPerPage;
	private int currentPage;
	private boolean hasPrevious;
    private boolean hasFirst;
    private boolean hasNext;
    private boolean hasLast;    
	
	public List<SearchResult> getSearchResultList() {
		return searchResultList;
	}
	public void setSearchResultList(List<SearchResult> searchResultList) {
		this.searchResultList = searchResultList;
	}	
	public String getSpellCheckTerm() {
		return spellCheckTerm;
	}
	public void setSpellCheckTerm(String spellCheckTerm) {
		this.spellCheckTerm = spellCheckTerm;
	}
	public long getResultsFound() {
		return resultsFound;
	}
	public void setResultsFound(long resultsFound) {
		this.resultsFound = resultsFound;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	public long getPages() {		
		pages = 0;
		if(resultsFound > 0) {
			pages = ((int)(resultsFound / resultsPerPage) + (resultsFound % resultsPerPage > 0 ? 1 : 0));
		}
		return pages;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public boolean isHasPrevious() {
        int first = (currentPage - 1) *  resultsPerPage;
        hasPrevious = first != 0;
        
		return hasPrevious;
	}
	public boolean isHasFirst() {
		int first = (currentPage - 1) *  resultsPerPage;
        hasFirst = first != 0;
        
		return hasFirst;
	}
	public boolean isHasNext() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasNext = last < resultsFound;
        
		return hasNext;
	}
	public boolean isHasLast() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasLast = last < resultsFound;
        
		return hasLast;
	}
}
