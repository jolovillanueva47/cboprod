package org.cambridge.ebooks.online.search.solr;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.search.AdvanceSearchBean;
import org.cambridge.ebooks.online.util.ExceptionPrinter;


public class SearchServlet_aaa extends SearchServlet {	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(SearchServlet_aaa.class);	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		String searchType = req.getParameter("searchType");
		
		//for filter settings in AAA reset,select all button
		if(null != req.getParameter("selectAll_aaa") ){
			logger.info(" AAA selectAll_aaa is Found!");
			try {
				this.doPageSearch(req, resp);
			} catch (Exception e) {
				logger.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}else{
			super.doPost(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	protected void doPageSearch(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		logger.info("aaa page search started");
		super.doPageSearch(req, resp, req.getParameter("currentPage_aaa"));
		forwardToJsp(req, resp);
	}
	
	@Override
	protected void doAdvanceSearch(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		logger.info("aaa advance search is called.");
		String[] target = req.getParameterValues("target");
		String[] condition = req.getParameterValues("condition");
		String[] searchText = req.getParameterValues("search_word");
		String[] searchText2 = req.getParameterValues("search_word_2");
		
		boolean isValidSearch = true;
		String errorMessage = "";
			
		if( isStringArrEmpty(searchText) && isStringArrEmpty(searchText2)){
			errorMessage = "No search term provided.";
			isValidSearch = false;			
		}else{
			//get the bean
			AdvanceSearchBean advsearchbean_aaa = new AdvanceSearchBean();

			for (int i = 0; i < searchText.length; i++) {
				String strTarget = target[i];
				String strCondition = condition[i];
				String strSearchText = searchText[i];
				String strSearchText2 = searchText2[i];
				
				strTarget =  StringUtils.trim(strTarget);
				strCondition =  StringUtils.trim(strCondition);
				strSearchText =  StringUtils.trim(strSearchText);
				strSearchText2 =  StringUtils.trim(strSearchText2);

				if(strTarget.equals(advsearchbean_aaa.getMapTarget2().get("Print Publication Year"))){
					if(strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is between")) || strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is from")) ){
						if(!StringUtils.isEmpty(strSearchText) || !StringUtils.isEmpty(strSearchText2)){
							if(StringUtils.isEmpty(strSearchText) || StringUtils.isAlpha(strSearchText2)){
								errorMessage = "Incomplete Input for Print Publication Year.";
								isValidSearch = false;
								break;
							}else{
								if(!(isValidDate(strSearchText, "yyyy") && isValidDate(strSearchText2, "yyyy"))){
									errorMessage = "Invalid Year for Print Publication Year. Format should be in yyyy.";
									isValidSearch = false;
									break;
								}
							}
						}
					}
				}else if(strTarget.equals(advsearchbean_aaa.getMapTarget2().get("Online Publication Date"))){
					if(strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is between")) || strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is from")) ){
						if(!StringUtils.isEmpty(strSearchText) || !StringUtils.isEmpty(strSearchText2)){
							if(StringUtils.isEmpty(strSearchText) || StringUtils.isAlpha(strSearchText2)){
								errorMessage = "Incomplete Input for Online Publication Date.";
								isValidSearch = false;
								break;
							}else{
								if(!(isValidDate(strSearchText, "yyyymmdd") && isValidDate(strSearchText2, "yyyymmdd"))){
									errorMessage = "Invalid format for Online Publication Date. Format should be in yyyymmdd.";
									isValidSearch = false;
									break;
								}
							}
						}			
					}
				}else if(strTarget.equals(advsearchbean_aaa.getMapTarget2().get("ISBN"))){
					if(strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is between")) || strCondition.equals(advsearchbean_aaa.getMapCondition2().get("Is from")) ){
						if(!StringUtils.isEmpty(strSearchText) || !StringUtils.isEmpty(strSearchText2)){
							if(StringUtils.isEmpty(strSearchText) || StringUtils.isAlpha(strSearchText2)){
								errorMessage = "Incomplete Input for ISBN.";
								isValidSearch = false;
								break;
							}else{
								if(!(StringUtils.isNumeric(strSearchText) && StringUtils.isNumeric(strSearchText2))){
									errorMessage = "Invalid Input for ISBN.";								
									isValidSearch = false;
									break;
								}else if(strSearchText.length() < 2 || strSearchText2.length() < 2){
									errorMessage = "Invalid Input for ISBN. Must be at least 2 digits.";								
									isValidSearch = false;
									break;
								}
							}
						}			
					}
				}
				
			}//end for loop
		}
		if(isValidSearch){
			super.doAdvanceSearch(req, resp);
		}
		else{
			req.setAttribute("advance_search_error_aaa", errorMessage);
		}
	}
	
	public boolean isValidDate(String inDate, String format) {

		    if (inDate == null)
		      return false;

		    //set the format to use as a constructor argument
		    SimpleDateFormat dateFormat = new SimpleDateFormat(format);//"yyyy-MM-dd");
		    
		    if (inDate.trim().length() != dateFormat.toPattern().length())
		      return false;

		    dateFormat.setLenient(false);
		    
		    try {
		      //parse the inDate parameter
		      dateFormat.parse(inDate.trim());
		    }
		    catch (ParseException pe) {
		      return false;
		    }
		    return true;
	}
	
	private boolean isStringArrEmpty(String[] stringArr){
		boolean results = false;
		
		for (String string : stringArr) {
			if (StringUtils.isNotEmpty(string)) {
				 results = false;
				break;
			}else{
				results = true;
			}
		}
		return results;	
	}
	
	
}
