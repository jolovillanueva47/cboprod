package org.cambridge.ebooks.online.search.subject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class SubjectBean {
	
	private static Logger logger = Logger.getLogger(SubjectBean.class);
	
	private String firstLetter;
	private String subject;
	private String subjectCode;
	private String subjectId;
	
	private List<BookMetaData> bookMetaDataList;
	private Map<String, String> alphaMap;
	
	private HttpServletRequest req = HttpUtil.getHttpServletRequest();
	
	private long resultsFound;
	private long pages;
	private int resultsPerPage;
	private int currentPage;
	private boolean hasPrevious;
    private boolean hasFirst;
    private boolean hasNext;
    private boolean hasLast;  
	
    public String getInitAlphaMap() {
    	String subjectCode = req.getParameter("subjectCode");
    	String subjectId = req.getParameter("subjectId");
    	String searchType = req.getParameter("searchType");

    	SubjectDataWorker worker = new CboSubjectDataWorker(req);
    	SubjectBean bean = null;
    	JSONArray allLeaves = null;
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put(CboSubjectDataWorker.getKeySubjectCode(), subjectCode);    	
    	if("allSubjectBook".equalsIgnoreCase(searchType)) {
			allLeaves = getAllLeavesJSONArray(subjectId);		
		} else if (StringUtils.isNotEmpty(subjectId) && isAllSubjectBook(subjectId)) {
			allLeaves = getAllLeavesJSONArray(subjectId);
		}
    	param.put(CboSubjectDataWorker.getKeyJsonArray(), allLeaves);
    	bean = worker.generateAlphaMap(param);
    	setAlphaMap(bean.getAlphaMap());
    	return "";
    }
    
	public String getInitPage() {
		logger.info("Initialize...");		
		String subjectId = req.getParameter("subjectId");
		String subjectCode = req.getParameter("subjectCode");
		String subjectName = req.getParameter("subjectName");
		String firstLetter = req.getParameter("firstLetter");
		String searchType = req.getParameter("searchType");
		String sortBy = req.getParameter("sort");
		int results = 0;
		int page = 0;
		if(StringUtils.isNotEmpty(req.getParameter("results")) && StringUtils.isNumeric(req.getParameter("results"))) {
			results = Integer.parseInt(req.getParameter("results"));
		}
		if(StringUtils.isNotEmpty(req.getParameter("page")) && StringUtils.isNumeric(req.getParameter("page"))) {
			page = Integer.parseInt(req.getParameter("page"));
		}

		SubjectDataWorker worker = new CboSubjectDataWorker(req);
		SubjectBean bean = null;
		JSONArray allLeaves = null;		
		if("allSubjectBook".equalsIgnoreCase(searchType)) {
			allLeaves = getAllLeavesJSONArray(subjectId);	
		} else if (StringUtils.isNotEmpty(subjectId) && isAllSubjectBook(subjectId)) {
			allLeaves = getAllLeavesJSONArray(subjectId);
		} 
		
		Map<String, Object> param = new HashMap<String, Object>();
    	param.put(CboSubjectDataWorker.getKeySubjectCode(), subjectCode);
    	param.put(CboSubjectDataWorker.getKeyPageNumber(), page);    	
    	param.put(CboSubjectDataWorker.getKeyPageSize(), results);    	
    	param.put(CboSubjectDataWorker.getKeySortBy(), sortBy);
    	param.put(CboSubjectDataWorker.getKeyByLetter(), firstLetter);
    	param.put(CboSubjectDataWorker.getKeyJsonArray(), allLeaves);  	
		
		bean = worker.generateResult(param);				
		populateBean(bean);
		
		setSubjectCode(subjectCode);
		setSubjectId(subjectId);
		setSubject(subjectName);
		
		logger.info("Finished...");		
		return "";
	}			
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectCode() {
		return subjectCode;
	}
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	public String getFirstLetter() {
		return firstLetter;
	}
	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}
	public List<BookMetaData> getBookMetaDataList() {
		return bookMetaDataList;
	}
	public void setBookMetaDataList(List<BookMetaData> bookMetaDataList) {
		this.bookMetaDataList = bookMetaDataList;
	}	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}		
	public Map<String, String> getAlphaMap() {
		return alphaMap;
	}
	public void setAlphaMap(Map<String, String> alphaMap) {
		this.alphaMap = alphaMap;
	}
	public long getResultsFound() {
		return resultsFound;
	}
	public void setResultsFound(long resultsFound) {
		this.resultsFound = resultsFound;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	public long getPages() {		
		pages = 0;
		if(resultsFound > 0) {
			pages = ((int)(resultsFound / resultsPerPage) + (resultsFound % resultsPerPage > 0 ? 1 : 0));
		}
		return pages;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public boolean isHasPrevious() {
        int first = (currentPage - 1) *  resultsPerPage;
        hasPrevious = first != 0;
        
		return hasPrevious;
	}
	public boolean isHasFirst() {
		int first = (currentPage - 1) *  resultsPerPage;
        hasFirst = first != 0;
        
		return hasFirst;
	}
	public boolean isHasNext() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasNext = last < resultsFound;
        
		return hasNext;
	}
	public boolean isHasLast() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasLast = last < resultsFound;
        
		return hasLast;
	}
	
	private void populateBean(SubjectBean bean) {		
		setFirstLetter(bean.getFirstLetter());
		setBookMetaDataList(bean.getBookMetaDataList());
		setResultsFound(bean.getResultsFound());
		setResultsPerPage(bean.getResultsPerPage());
		setCurrentPage(bean.getCurrentPage());
	}
		
	private static boolean isAllSubjectBook(String code){
		boolean isAllSubject = false;
		if(StringUtils.isEmpty(code)) {
			isAllSubject = false;
		} else {
			code = code.substring(0, 1);
			if ("A".equals(code) || "B".equals(code) || "C".equals(code) || "D".equals(code) || "E".equals(code)) {
				isAllSubject = true;
			} else {
				isAllSubject = false;
			}
		}
		
		return isAllSubject;
	}
	
	private static JSONArray getAllLeavesJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SUBJECT, parentId);
		result.addAll(list);
		return result;
	}
	
	public String getInitPage_aaa() {
		
		logger.info("AAA modifying book metadata");		
		
		List<BookMetaData> results = getBookMetaDataList();
		
		for (int i=0 ; i < results.size() ; i++) {
			results.set(i, BookMetaData.customReplace_aaa(results.get(i)));
		}

		return "";
	}	
}
