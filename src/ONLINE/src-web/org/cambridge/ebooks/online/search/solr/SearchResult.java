package org.cambridge.ebooks.online.search.solr;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.landingpage.BookContentItem;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

public class SearchResult {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(SearchResult.class);
	
	private String accessType;
	
	private QueryResponse queryResponse;

	private HttpServletRequest request;
	private SubscriptionUtil subscription;
	
	private BookMetaData bookMetaData;
	private BookContentItem bookContentItem;
	
	private String contentType;	
	private String id;
	private List<CollapsedChapter> collapsedChapterList;
	
	private String journalId;
	private String journalIssn;
	private String journalPrintIsbn;	
	private String articleDoi;
	private String articleTitle;
	private String articleType;
	private String articlePrintDate;
	private String articleOnlineDate;
	private String articlePageStart;
	private String articlePageEnd;
	private String articleIssueNumber;
	private String articleVolumeNumber;
	private List<String> articlePrice;
	private String articleAbstractFilename;	
	private boolean hasAccess;
		
	public SearchResult(){
		this(null, "", null, null, "");
	}
	public SearchResult(QueryResponse queryResponse, String id, HttpServletRequest request, SubscriptionUtil subscription, String isbn) {
		this.queryResponse = queryResponse;
		this.id = id;
		this.request = request;
		this.subscription = subscription;		
		//this.accessType = this.subscription.getAccessType(isbn, this.request, false);
		EBooksAccessListView vw = subscription.getAccessType(isbn, request, false);
		this.accessType = vw.hasAccessDisplay();
		this.hasAccess = vw.hasAccess();
		//this.setAccessType = 
		
	}	
	public BookMetaData getBookMetaData() {
		return bookMetaData;
	}
	public void setBookMetaData(BookMetaData bookMetaData) {
		this.bookMetaData = bookMetaData;
	}
	public BookContentItem getBookContentItem() {
		return bookContentItem;
	}
	public void setBookContentItem(BookContentItem bookContentItem) {
		this.bookContentItem = bookContentItem;
	}	
	public String getBookVolumeTitle() {
		return getHighlightedValue("volume_title");
	}
	public String getBookPartTitle() {
		return getHighlightedValue("part_title");
	}
	public String getBookPartNumber() {
		return getBookMetaData().getPartNumber();
	}
	public String getBookEdition() {
		return getHighlightedValue("edition");
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getBookTitle() {
		return getHighlightedValue("main_title");
	}
	public String getBookSubtitle() {
		return getHighlightedValue("main_subtitle");
	}
	public String getAuthor() {
		return parseDelimitedValues(getHighlightedValue("author_name"), SolrUtil.DELIMITER, ",");
	}
	public String getAuthorSingleline() {
		return getHighlightedValue("author_singleline");
	}
	public String getBookPrintDate() {
		return getHighlightedValue("print_date_display");
	}
	public String getArticlePrintDate() throws Exception {
		String result = "";
		if(StringUtils.isNotEmpty(articlePrintDate)) {
			DateFormat printDateFormat = new SimpleDateFormat("MMM yyyy");
			DateFormat solrDateFormat = new SimpleDateFormat("yyyy-MM-dd");			
			result = printDateFormat.format(solrDateFormat.parse(articlePrintDate));
		}
		return result;
	}
	public void setArticlePrintDate(String articlePrintDate) {
		this.articlePrintDate = articlePrintDate;
	}
	public String getBookOnlineDate() {
		return getHighlightedValue("online_date_display");
	}
	public String getArticleOnlineDate() throws Exception {
		String result = "";
		if(StringUtils.isNotEmpty(articleOnlineDate)) {
			DateFormat onlineDateFormat = new SimpleDateFormat("dd MMM yyyy");
			DateFormat solrDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			result = onlineDateFormat.format(solrDateFormat.parse(articleOnlineDate));
		}
		return result;
	}
	public void setArticleOnlineDate(String articleOnlineDate) {			
		this.articleOnlineDate = articleOnlineDate;
	}
	public String getBookSeries() {
		return getBookMetaData().getSeries();
	}
	public String getBookHighlightedSeries() {
		return getHighlightedValue("series");
	}
	public String getBookSeriesNumber() {
		return getBookMetaData().getSeriesNumber();
	}
	public String getBookSeriesCode() {
		return getBookMetaData().getSeriesCode();
	}
	public String getBookOnlineIsbn() {
		return getBookMetaData().getOnlineIsbn();
	}
	public String getBookHighlightedOnlineIsbn() {
		return getHighlightedValue("isbn_issn");
	}
	public String getBookPrintIsbn() {
		return getHighlightedValue(getBookMetaData().getPrintIsbnType());
	}
	public String getChapterTitle() {
		return getHighlightedValue("title");
	}
	public String getChapterSubtitle() {
		return getHighlightedValue("subtitle");
	}
	public String getContributor() {
		return parseDelimitedValues(getHighlightedValue("contributor_name"), SolrUtil.DELIMITER, ",");
	}
	public String getChapterDoi() {
		return getHighlightedValue("doi");
	}
	public String getChapterFulltext() {
		return getHighlightedValue("chapter_fulltext");
	}
	public String getChapterPageRange() {
		return getBookContentItem().getPageStart() + "-" + getBookContentItem().getPageEnd();
	}
	public String getChapterPdfFilename() {
		return getBookContentItem().getPdfFilename();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBookId() {
		return getBookMetaData().getId();
	}
	public List<CollapsedChapter> getCollapsedChapterList() {
		return collapsedChapterList;
	}
	public void setCollapsedChapterList(List<CollapsedChapter> collapsedChapterList) {
		this.collapsedChapterList = collapsedChapterList;
	}
	public String getChapterLabel() {
		return getBookContentItem().getLabel();
	}
	public String getBookVolumeNumber() {
		return getBookMetaData().getVolumeNumber();
	}	
	public String getArticleIssueNumber() {
		return articleIssueNumber;
	}
	public void setArticleIssueNumber(String articleIssueNumber) {
		this.articleIssueNumber = articleIssueNumber;
	}
	public String getArticlePrice() {
		StringBuilder sb = new StringBuilder();
		int ctr = 0;
		for(String s : articlePrice) {
			ctr++;					
			sb.append("$").append(s).append(".00");
			if(ctr < articlePrice.size()) {
				sb.append(" / ");
			}
		}
		return sb.toString();
	}	
	public void setArticlePrice(List<String> articlePrice) {
		this.articlePrice = articlePrice;
	}
	public String getJournalId() {
		return journalId;
	}
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	public String getArticleAbstractFilename() {
		return articleAbstractFilename;
	}
	public void setArticleAbstractFilename(String articleAbstractFilename) {
		this.articleAbstractFilename = articleAbstractFilename;
	}
	public String getArticleFileId() {
		String fileId = "";
		if(StringUtils.isNotEmpty(getArticleAbstractFilename())) {
			fileId = getArticleAbstractFilename().substring(getArticleAbstractFilename().lastIndexOf("/") + 1);
			fileId = fileId.substring(0, 17);			
		}
		return fileId;
	}
	public String getChapterType() {
		return getBookContentItem().getType();
	}
	public String getAccessType() {
		return this.accessType;
	}		
	
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public SubscriptionUtil getSubscription() {
		return subscription;
	}
	public void setSubscription(SubscriptionUtil subscription) {
		this.subscription = subscription;
	}	
	public String getJournalTitle() {
		return getHighlightedValue("main_title");
	}
	public String getJournalSubtitle() {
		return getHighlightedValue("main_subtitle");
	}
	public String getJournalIssn() {
		return journalIssn;
	}
	public void setJournalIssn(String journalIssn) {
		this.journalIssn = journalIssn;
	}
	public String getJournalPrintIsbn() {
		return journalPrintIsbn;
	}
	public void setJournalPrintIsbn(String journalPrintIsbn) {
		this.journalPrintIsbn = journalPrintIsbn;
	}	
	public String getArticleDoi() {
		return articleDoi;
	}
	public String getArticleHighlightedDoi() {
		return getHighlightedValue("doi");
	}
	public void setArticleDoi(String articleDoi) {
		this.articleDoi = articleDoi;
	}
	public String getArticleHighlightedTitle() {
		return getHighlightedValue("title");
	}
	public String getArticleTitle() {
		return articleTitle;
	}
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}
	public String getArticleType() {
		return articleType;
	}
	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	public String getArticlePageStart() {
		return articlePageStart;
	}
	public void setArticlePageStart(String articlePageStart) {
		this.articlePageStart = articlePageStart;
	}
	public String getArticlePageEnd() {
		return articlePageEnd;
	}
	public void setArticlePageEnd(String articlePageEnd) {
		this.articlePageEnd = articlePageEnd;
	}
	public String getArticleVolumeNumber() {
		return articleVolumeNumber;
	}
	public void setArticleVolumeNumber(String articleVolumeNumber) {
		this.articleVolumeNumber = articleVolumeNumber;
	}
	
	
	public boolean isHasAccess() {
		return hasAccess;
	}
	
	
	private String getHighlightedValue(String fieldName) {
		StringBuilder hFieldBuilder = new StringBuilder();
		Map<String, Map<String, List<String>>> highlighter = queryResponse.getHighlighting();
		
		if(null != highlighter.get(id) 
				&& null != highlighter.get(id).get(fieldName)) {		
			
			for(String s : highlighter.get(id).get(fieldName)) {
				hFieldBuilder.append(s);
				if("chapter_fulltext".equalsIgnoreCase(fieldName)) {
					hFieldBuilder.append("...");
				}
			}
		} 
		String hField = "";
		if(hFieldBuilder.length() > 0) {
			hField = hFieldBuilder.toString().replaceAll("&amp;", "&");
		}
		
		return hField;
	}	
	private String parseDelimitedValues(String val, String splitDelimiter, String delimiter) {
		String result = "";
		StringBuilder resultBuilder = new StringBuilder();
		String[] sArr = val.split(splitDelimiter);
		if(sArr.length > 4) {
			resultBuilder.append(sArr[0]).append(" et al.");
		} else {
			int ctr = 0;
			for(String s : sArr) {
				ctr++;
				resultBuilder.append(s);
				if(ctr<sArr.length-1){
					if(ctr==sArr.length - 2){
						if((sArr.length - 1)==2){
							resultBuilder.append("and ");
						}else{
							resultBuilder.append(", and ");
						}
					}else{
						resultBuilder.append(delimiter).append(" ");//comma space
					}
				}
			}
		}
		if(resultBuilder.length() > 0) {
			result = resultBuilder.toString().replaceAll("&amp;", "&");
		}
		return result;
	}
}
