package org.cambridge.ebooks.online.search.solr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.cambridge.ebooks.online.util.StringUtil;


public class FacetManager {	
	private String filterType;
	private String filterValue;
	private String fromLogin;
	private String[] filterQueries;
	private String[] filterParamValues;
	
	private Map<String, Integer> facetQueryMap;
	private List<String> facetQueryList;
	
	private List<Category> contentTypeList;
	private List<Category> subjectFacetList;
	private List<Category> authorNameFacetList;
	private List<Category> bookTitleFacetList;
	private List<Category> journalTitleFacetList;
	private List<Category> seriesFacetList;
	private List<Category> printDateFacetList;
	
	private List<String> multipleFilterList;
	
	private Map<String, String> filterDisplayMap;
	private Map<String, String> categorySelectionMap;	

	public static class Category {
		private String name;
		private long count;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public long getCount() {
			return count;
		}
		public void setCount(long count) {
			this.count = count;
		}
		public String getHtmlTitle() {
			String s = "";
			if(StringUtils.isNotEmpty(name)) {
				if("chapter".equalsIgnoreCase(name)) {
					s = "Book Chapters";
				} else if("article".equalsIgnoreCase(name)) {
					s = "Journal Articles";
				} else {
					s = name;
				}
			}
			return s;
		}
		public String getEscapeName() {
			String s = "";
			if(StringUtils.isNotEmpty(name)) {
				s = StringUtil.escapeUnicode(name);
			}
			return s;
		}
		public String getNoHtmlName() {
			String s = "";
			if(StringUtils.isNotEmpty(name)) {
				s = StringUtil.stripHTMLTags(name);
			}
			return s;
		}
		public String getDisplay() {
			String s = "";
			
			if(StringUtils.isNotEmpty(name)) {
				if("chapter".equalsIgnoreCase(name)) {
					s = "Book Chapters";
				} else if("article".equalsIgnoreCase(name)) {
					s = "Journal Articles";
				} else {
					name = StringUtil.stripHTMLTags(name);
					if(name.length() > 27) {
						s = name.substring(0, 28) + "...";
					} else {
						s = name;
					}
				}
			}
			
			return s;
		}
	}
	
	public FacetManager() {
		this.filterDisplayMap = new LinkedHashMap<String, String>();
		this.categorySelectionMap = new HashMap<String, String>();
		this.multipleFilterList = new ArrayList<String>();
	}
	
	
	public String getFromLogin() {
		return fromLogin;
	}
	public void setFromLogin(String fromLogin) {
		this.fromLogin = fromLogin;
	}
	public String[] getFilterParamValues() {
		return filterParamValues;
	}
	public void setFilterParamValues(String[] filterParamValues) {
		this.filterParamValues = filterParamValues;
	}
	public void setFilterDisplayMap(Map<String, String> filterDisplayMap) {
		this.filterDisplayMap = filterDisplayMap;
	}
	public Map<String, String> getFilterDisplayMap() {
		return this.filterDisplayMap;
	}		
	public Map<String, String> getCategorySelectionMap() {
		return categorySelectionMap;
	}
	public void setCategorySelectionMap(Map<String, String> categorySelectionMap) {
		this.categorySelectionMap = categorySelectionMap;
	}	
	public List<String> getMultipleFilterList() {
		return multipleFilterList;
	}
	public void setMultipleFilterList(List<String> multipleFilterList) {
		this.multipleFilterList = multipleFilterList;
	}
	public String getFilterType() {
		return filterType;
	}
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}			
	public String getFilterValue() {
		return filterValue;
	}
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}
	public Map<String, Integer> getFacetQueryMap() {
		return facetQueryMap;
	}
	public void setFacetQueryMap(Map<String, Integer> facetQueryMap) {
		this.facetQueryMap = facetQueryMap;
	}
	public List<String> getFacetQueryList() {
		return facetQueryList;
	}
	public void setFacetQueryList(List<String> facetQueryList) {
		this.facetQueryList = facetQueryList;
	}
	public List<Category> getContentTypeList() {
		return contentTypeList;
	}
	public void setContentTypeList(List<Category> contentTypeList) {
		this.contentTypeList = contentTypeList;
	}
	public List<Category> getSubjectFacetList() {
		return subjectFacetList;
	}
	public void setSubjectFacetList(List<Category> subjectFacetList) {
		this.subjectFacetList = subjectFacetList;
	}	
	public List<Category> getAuthorNameFacetList() {
		return authorNameFacetList;
	}
	public void setAuthorNameFacetList(List<Category> authorNameFacetList) {
		this.authorNameFacetList = authorNameFacetList;
	}
	public List<Category> getBookTitleFacetList() {
		return bookTitleFacetList;
	}
	public void setBookTitleFacetList(List<Category> bookTitleFacetList) {
		this.bookTitleFacetList = bookTitleFacetList;
	}
	public List<Category> getJournalTitleFacetList() {
		return journalTitleFacetList;
	}
	public void setJournalTitleFacetList(List<Category> journalTitleFacetList) {
		this.journalTitleFacetList = journalTitleFacetList;
	}
	public List<Category> getSeriesFacetList() {
		return seriesFacetList;
	}
	public void setSeriesFacetList(List<Category> seriesFacetList) {
		this.seriesFacetList = seriesFacetList;
	}	
	public List<Category> getPrintDateFacetList() {
		return printDateFacetList;
	}
	public void setPrintDateFacetList(List<Category> printDateFacetList) {
		this.printDateFacetList = printDateFacetList;
	}
	public String[] getFilterQueries() {
		return filterQueries;
	}
	public void setFilterQueries(String[] filterQueries) {
		this.filterQueries = filterQueries;
	}	
	public void configureFacet(SolrQuery q) {	
		// carry over last filter
		if(null != getFilterQueries() && getFilterQueries().length > 0) {
			q.setFilterQueries(getFilterQueries());
		}
		
		// remove multiple filter
		for (String s : getMultipleFilterList()) {
			q.removeFilterQuery(s);
		}		
		getMultipleFilterList().clear();
		
		if(null == getFromLogin()) {
			getCategorySelectionMap().clear();
		}
		
		if("remove".equalsIgnoreCase(getFilterType())) {			
			String filterId = getFilterValue();
			String[] filterIdArr = filterId.split("=COL=");
			String filterType = "";
			String filterValue = "";
			if(!filterId.contains("print_date")) {
				filterType = filterIdArr[0];
				filterValue = StringUtil.unescapeUnicode(filterIdArr[1]);
			}

			StringBuilder filter = new StringBuilder();			
			if("chapter".equalsIgnoreCase(filterValue) || "article".equalsIgnoreCase(filterValue)) {	
				filter.append(filterType).append(":").append(filterValue);
			} else if(filterId.contains("print_date")) {
				filter = new StringBuilder(filterId);
			} else {
				filter.append(filterType).append(":\"")
						.append(filterValue).append("\"");
				
			}
			q.removeFilterQuery(filter.toString());
			getFilterDisplayMap().remove(filterId);
		} else if("reset".equalsIgnoreCase(getFilterType())) {
			removeFilters(q);
			getFilterDisplayMap().clear();
		} else if("multiple".equalsIgnoreCase(getFilterType())) {			
			String[] filters = null;
			
			if(null == getFilterParamValues()) {
				List<String> filterList = new ArrayList<String>();
				for(String key : getCategorySelectionMap().keySet()) {
					filterList.add(key);
				}
				filters = filterList.toArray(new String[0]);
			} else {
				filters = getFilterParamValues();
			}
			
			Map<String, String> categoryMap = new HashMap<String, String>();
			for(String f : filters) {
				
				String[] filterIdArr = f.split("=COL=");
				String filterType = filterIdArr[0];
				String filterValue = StringUtil.unescapeUnicode(filterIdArr[1]);
				
				getCategorySelectionMap().put(f, "checked");				
				
				StringBuilder filterValueBuilder = null;
				if(null != categoryMap.get(filterType)) {
					filterValueBuilder = new StringBuilder(categoryMap.get(filterType));
					
					if(!filterType.contains("content_type") && !filterType.contains("print_date")) {
						filterValueBuilder.append(" OR ").append("\"").append(filterValue).append("\"");
					} else {
						filterValueBuilder.append(" OR ").append(filterValue);
					}						
				} else {
					filterValueBuilder = new StringBuilder();					
					if(!filterType.contains("content_type") && !filterType.contains("print_date")) {
						filterValueBuilder.append("\"").append(filterValue).append("\"");
					} else {
						filterValueBuilder.append(filterValue);
					}
					
				}				
				categoryMap.put(filterType, filterValueBuilder.toString());
				
			}			
			
			for(String key : categoryMap.keySet()) {
				String categoryValue = categoryMap.get(key);
				String filter = key + ":(" + categoryValue + ")";
				q.addFilterQuery(filter);
				getMultipleFilterList().add(filter);
			}
			
		} else if("content_type".equalsIgnoreCase(getFilterType())) {	
			String filterId = getFilterValue();
			String[] filterIdArr = filterId.split("=COL=");
			String filterType = filterIdArr[0];
			String filterValue = filterIdArr[1];
			StringBuilder filter = new StringBuilder();
			filter.append(filterType).append(":").append(filterValue);
			
			q.addFilterQuery(filter.toString());
			getFilterDisplayMap().put(filterId, filter.toString()
					.replaceAll("content_type", "Content Type")
					.replaceAll("chapter", "Book Chapters")
					.replaceAll("article", "Journal Articles"));
		} else if(StringUtils.isNotEmpty(getFilterType())) {
			
			String filterId = getFilterValue();
			String[] filterIdArr = filterId.split("=COL=");
			String filterType = "";
			String filterValue = "";
			if(!filterId.contains("print_date")) {
				filterType = filterIdArr[0];
				filterValue = StringUtil.unescapeUnicode(filterIdArr[1]);
			}
			
			StringBuilder filter = new StringBuilder();
			filter.append(filterType).append(":\"")
					.append(filterValue).append("\"");
						
			String filterValueDisplay = filterValue;
			
			if(filterValueDisplay.length() > 20) {
				filterValueDisplay = StringUtil.stripHTMLTags(filterValueDisplay).substring(0, 20) + "...";
			}
			
			if("subject_facet".equalsIgnoreCase(filterType)) {
				getFilterDisplayMap().put(filterId, "Subject:" + filterValueDisplay);
			} else if("series_facet".equalsIgnoreCase(filterType)) {
				getFilterDisplayMap().put(filterId, "Series:" + filterValueDisplay);
			} else if("author_name_facet".equalsIgnoreCase(filterType)) {
				getFilterDisplayMap().put(filterId, "Author:" + filterValueDisplay);
			} else if("book_title_facet".equalsIgnoreCase(filterType)) {
				getFilterDisplayMap().put(filterId, "Top Book:" + filterValueDisplay);
			} else if("journal_title_facet".equalsIgnoreCase(filterType)) {
				getFilterDisplayMap().put(filterId, "Top Journal:" + filterValueDisplay);
			} else if(filterId.contains("print_date")) {
				filter = new StringBuilder(filterId);
				getFilterDisplayMap().put(filterId, filterId.replaceAll("print_date", "Year").replaceAll("TO", "-"));
			}

			q.addFilterQuery(filter.toString());		
		}
		
		setFilterQueries(q.getFilterQueries());
	}
	
	private static void removeFilters(SolrQuery q) {
		for(String fq : q.getFilterQueries()) {
			if("doi:[0 TO 9]".equalsIgnoreCase(fq)) {
				continue;
			}
			
			q.removeFilterQuery(fq);
		}
	}
}
