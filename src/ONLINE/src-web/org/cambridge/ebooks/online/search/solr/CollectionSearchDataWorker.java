package org.cambridge.ebooks.online.search.solr;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.cambridge.ebooks.online.collections.EbooksAutocompleteCollectionsWorker;

public class CollectionSearchDataWorker extends CboSearchDataWorker {	
	public CollectionSearchDataWorker(){}
	
	private static final String KEY_COLLECTION_ID = "collection_id";
	
	public SolrQuery generateQuickQuery(String searchText,
			String id) {
		SolrQuery q = generateQuickQuery(searchText);
		
		//book isbn from collections
		String isbnList = EbooksAutocompleteCollectionsWorker.getIsbnListString(id);
		if(StringUtils.isEmpty(isbnList)){
			isbnList = null;
		}
		String fq = "isbn_issn:(" + isbnList + ")";
		q.addFilterQuery(fq);
		
		return q;
	}

	public static String getKeyCollectionId() {
		return KEY_COLLECTION_ID;
	}
}
