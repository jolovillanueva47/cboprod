package org.cambridge.ebooks.online.search.series;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.series.EBookSeriesLoad;
import org.cambridge.ebooks.online.jpa.subjecttree.EBookSubjectHierarchy;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class SeriesBean {
	private static Logger logger = Logger.getLogger(SeriesBean.class);	

	private List<BookMetaData> bookMetaDataList;
	private Map<String, String> alphaMap;
	
	private HttpServletRequest req = HttpUtil.getHttpServletRequest();
	
	private String firstLetter;
	private String seriesCode;
	private String series;
	private String seriesNumber;
	
	private String showVolumeSort;
	
	private long resultsFound;
	private long pages;
	private int resultsPerPage;
	private int currentPage;
	private boolean hasPrevious;
    private boolean hasFirst;
    private boolean hasNext;
    private boolean hasLast;    
    
    public String getInitAlphaMap() {
    	String searchType = req.getParameter("searchType");
		String seriesCode = req.getParameter("seriesCode"); //jubs 20100429
		
    	SeriesDataWorker worker = new CboSeriesDataWorker(req);		
    	SeriesBean bean = null;
    	JSONArray allLeaves = null;

    	if ("allSubjectBook".equalsIgnoreCase(searchType) || "allSeriesSubLevel".equalsIgnoreCase(searchType)
				|| (StringUtils.isNotEmpty(seriesCode) && isAllSubjectBook(seriesCode))){
			allLeaves = getSeriesSublevelJSONArray(seriesCode);			
    	}
    	
    	Map<String, Object> param = new HashMap<String, Object>();
    	param.put(CboSeriesDataWorker.getKeyByLetter(), "all");
    	param.put(CboSeriesDataWorker.getKeyJsonArray(), allLeaves);
    	
    	bean = worker.generateAlphaMap(param);
    	setAlphaMap(bean.getAlphaMap());
    	return "";
    }
    
    public String getInitAllSeriesPage() {    	
    	String firstLetter =  "All";
		String searchType = req.getParameter("searchType");
		String seriesCode = req.getParameter("seriesCode"); //jubs 20100429
		String series = req.getParameter("seriesName"); //jubs 20100429
		
		int results = 0;
		int page = 0;
		if(StringUtils.isNotEmpty(req.getParameter("firstLetter")) && req.getParameter("firstLetter").length() == 1) {
			firstLetter = req.getParameter("firstLetter");
		}
		if(StringUtils.isNotEmpty(req.getParameter("results")) && StringUtils.isNumeric(req.getParameter("results"))) {
			results = Integer.parseInt(req.getParameter("results"));
		}
		if(StringUtils.isNotEmpty(req.getParameter("page")) && StringUtils.isNumeric(req.getParameter("page"))) {
			page = Integer.parseInt(req.getParameter("page"));
		}
				
    	SeriesDataWorker worker = new CboSeriesDataWorker(req);		
    	SeriesBean bean = null;
		JSONArray allLeaves = null;
		if ("allSubjectBook".equalsIgnoreCase(searchType) || "allSeriesSubLevel".equalsIgnoreCase(searchType)
				|| (StringUtils.isNotEmpty(seriesCode) && isAllSubjectBook(seriesCode))){
			allLeaves = getSeriesSublevelJSONArray(seriesCode);				
		} 
		
		Map<String, Object> param = new HashMap<String, Object>();
    	param.put(CboSeriesDataWorker.getKeyByLetter(), firstLetter);
    	param.put(CboSeriesDataWorker.getKeyJsonArray(), allLeaves);
    	param.put(CboSeriesDataWorker.getKeyPageNumber(), page);
    	param.put(CboSeriesDataWorker.getKeyPageSize(), results);
		
    	bean = worker.generateAllResult(param);
    	
		populateBean(bean);			
		
		setSeriesCode(seriesCode);
		setSeries(series);
		setFirstLetter(firstLetter);
    	 	
    	return "";
    }
    
	public String getInitSeriesPage() {
		logger.info("Initialize...");		
		req = HttpUtil.getHttpServletRequest();		

		String seriesCode = req.getParameter("seriesCode");
		String seriesTitle = req.getParameter("seriesTitle");
		String seriesNumber = req.getParameter("seriesNumber");
		String searchType = req.getParameter("searchType");
		String firstLetter = req.getParameter("firstLetter");
		String sortBy = req.getParameter("sort");		
		
		int results = 0;
		int page = 0;
		
		if(StringUtils.isNotEmpty(req.getParameter("results")) && StringUtils.isNumeric(req.getParameter("results"))) {
			results = Integer.parseInt(req.getParameter("results"));
		}
		if(StringUtils.isNotEmpty(req.getParameter("page")) && StringUtils.isNumeric(req.getParameter("page"))) {
			page = Integer.parseInt(req.getParameter("page"));
		}
		
		JSONArray allLeaves = null;
		
		SeriesDataWorker worker = new CboSeriesDataWorker(req);		
		SeriesBean bean = null;
		if("allSubjectBook".equalsIgnoreCase(searchType)) {
			allLeaves = getAllLeavesJSONArray(seriesCode);
		} else if("allSeriesSubLevel".equalsIgnoreCase(searchType)) {
			allLeaves = getSeriesSublevelJSONArray(seriesCode);
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboSeriesDataWorker.getKeySeriesCode(), seriesCode);
    	param.put(CboSeriesDataWorker.getKeyByLetter(), firstLetter);
    	param.put(CboSeriesDataWorker.getKeyJsonArray(), allLeaves);
    	param.put(CboSeriesDataWorker.getKeyPageNumber(), page);
    	param.put(CboSeriesDataWorker.getKeyPageSize(), results);
    	param.put(CboSeriesDataWorker.getKeySortBy(), sortBy);
    	param.put(CboSeriesDataWorker.getKeyHttpSession(), req.getSession());
    	
    	bean = worker.generateResult(param);   
    	populateBean(bean);
		
		setSeriesCode(seriesCode);
		setSeries(seriesTitle);
		setSeriesNumber(seriesNumber);		
		setFirstLetter(firstLetter);
		
		logger.info("Finished...");		
		return "";
	}			
	
	public String getShowVolumeSort() {
		return showVolumeSort;
	}
	public void setShowVolumeSort(String showVolumeSort) {
		this.showVolumeSort = showVolumeSort;
	}
	public Map<String, String> getAlphaMap() {
		return alphaMap;
	}
	public void setAlphaMap(Map<String, String> alphaMap) {
		this.alphaMap = alphaMap;
	}
	public String getSeriesCode() {
		return seriesCode;
	}
	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public String getSeriesNumber() {
		return seriesNumber;
	}
	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
	public String getSeriesDesc() {
		String desc = "";
		EBookSeriesLoad seriesLoad = PersistenceUtil.searchEntity(
				new EBookSeriesLoad(),
				EBookSeriesLoad.SEARCH_SERIES_DESC,
				getSeriesCode().toUpperCase());
		
		if(seriesLoad != null) {
			desc = StringEscapeUtils.escapeXml(seriesLoad.getSeriesDesc());
		}
		return desc;
	}
	public String getFirstLetter() {
		return firstLetter;
	}
	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}
	public List<BookMetaData> getBookMetaDataList() {
		return bookMetaDataList;
	}
	public void setBookMetaDataList(List<BookMetaData> bookMetaDataList) {
		this.bookMetaDataList = bookMetaDataList;
	}
	public long getResultsFound() {
		return resultsFound;
	}
	public void setResultsFound(long resultsFound) {
		this.resultsFound = resultsFound;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	public long getPages() {		
		pages = 0;
		if(resultsFound > 0) {
			pages = ((int)(resultsFound / resultsPerPage) + (resultsFound % resultsPerPage > 0 ? 1 : 0));
		}
		return pages;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public boolean isHasPrevious() {
        int first = (currentPage - 1) *  resultsPerPage;
        hasPrevious = first != 0;
        
		return hasPrevious;
	}
	public boolean isHasFirst() {
		int first = (currentPage - 1) *  resultsPerPage;
        hasFirst = first != 0;
        
		return hasFirst;
	}
	public boolean isHasNext() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasNext = last < resultsFound;
        
		return hasNext;
	}
	public boolean isHasLast() {
		int first = (currentPage - 1) *  resultsPerPage;
        int last = first + resultsPerPage;
        hasLast = last < resultsFound;
        
		return hasLast;
	}
	
	private void populateBean(SeriesBean bean) {	
		setBookMetaDataList(bean.getBookMetaDataList());
		setResultsFound(bean.getResultsFound());
		setResultsPerPage(bean.getResultsPerPage());
		setCurrentPage(bean.getCurrentPage());
		setShowVolumeSort(bean.getShowVolumeSort());
	}
	private static JSONArray getAllLeavesJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES, parentId);
		result.addAll(list);
		return result;
	}	
	private static JSONArray getSeriesSublevelJSONArray(String parentId) {
		JSONArray result = new JSONArray();
		
		List<EBookSubjectHierarchy> list = PersistenceUtil.searchList(new EBookSubjectHierarchy(), EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES_SUBLEVEL, parentId);
		result.addAll(list);
		return result;
	}	
	private static boolean isAllSubjectBook(String subjectCode){
		boolean isAllSubject = false;
		if(StringUtils.isEmpty(subjectCode)) {
			isAllSubject = false;
		} else {
			subjectCode = subjectCode.substring(0, 1);
			if ("A".equals(subjectCode) || "B".equals(subjectCode) || "C".equals(subjectCode) || "D".equals(subjectCode) || "E".equals(subjectCode)) {
				isAllSubject = true;
			} else {
				isAllSubject = false;
			}
		}
		
		return isAllSubject;
	}
	
	public String getInitSeriesPage_aaa() {
		
		logger.info("AAA modifying book metadata");		
		
		List<BookMetaData> results = getBookMetaDataList();
		
		for (int i=0 ; i < results.size() ; i++) {
			results.set(i, BookMetaData.customReplace_aaa(results.get(i)));
		}

		return "";
	}	
	
}
