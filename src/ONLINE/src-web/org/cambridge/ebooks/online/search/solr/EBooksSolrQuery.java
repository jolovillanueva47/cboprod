package org.cambridge.ebooks.online.search.solr;

import org.apache.solr.client.solrj.SolrQuery;

/**
 * 
 * @author mmanalo
 *
 */
public class EBooksSolrQuery extends SolrQuery{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7422126408463408736L;
	
	/* static methods */
	
	public static SolrQuery createNewInstance(){
		return new EBooksSolrQuery();
	}
	

	private EBooksSolrQuery(){
		super();
		addEBooksDefaults();
	}
	
	
	/* public methods */
	@Override
	public SolrQuery setFilterQueries(String... fq) {
		super.setFilterQueries(fq);
		addEBooksDefaults();
		return this;
	};
	
	
	/* private methods */	
	private void addEBooksDefaults(){
		super.addFilterQuery("flag:1");
	}
	
}
