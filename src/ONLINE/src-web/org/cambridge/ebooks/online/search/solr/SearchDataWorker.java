package org.cambridge.ebooks.online.search.solr;

import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;


public interface SearchDataWorker {	
	public SolrQuery generateQuickQuery(String searchText);
	public SolrQuery generateQuickQuery(String searchText, String id);
	public SearchResultBean generateQuickResult(Map<String, Object> param);
	public SolrQuery generateAdvanceQuery(Map<String, Object> param);
	public SearchResultBean generateAdvanceResult(Map<String, Object> param);
	public SearchResultBean generatePageResult(Map<String, Object> param);
	public SearchResultBean generateFilteredResult(Map<String, Object> param);
	public int getDefaultPageSize();
}
