package org.cambridge.ebooks.online.search;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;

public class AdvanceSearchBean {
	
	private Map<String, String> mapOptions;
	
	public Map<String, String> getMapTarget(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("Anything","all");
		mapOptions.put("Author / Editor / Contributor","author");
		mapOptions.put("Title / Volume","title");
		mapOptions.put("Subject","subject");
		mapOptions.put("Series Name","series");
		mapOptions.put("Digital Object Identifier","doi");
		mapOptions.put("Keyword","keyword");
		mapOptions.put("Author Affiliation","auth_aff");
		return mapOptions;
	}
	public Map<String, String> getMapTarget2(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("Print Publication Year","print_date");
		mapOptions.put("Online Publication Date","online_date");
		mapOptions.put("ISBN","isbn_issn");
		return mapOptions;
	}
	public Map<String, String> getMapCondition(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("Contains","contain");
		mapOptions.put("Does not contain","not_contain");
		mapOptions.put("Is exactly","exactly");
		return mapOptions;
	}
	public Map<String, String> getMapCondition2(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("Is between","between");
		mapOptions.put("Is from","from");
		return mapOptions;
	}
	public Map<String, String> getMapAndOr(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("AND","and");
		mapOptions.put("OR","or");
		return mapOptions;
	}
	public Map<String, String> getMapSFilter(){
		mapOptions = new LinkedHashMap<String, String>();
		mapOptions.put("Book","book");
		mapOptions.put("CLC","clc");
		mapOptions.put("Journal","journal");
		return mapOptions;
	}
	
	public static void main(String args[]) {
		System.out.println("confuse&amp;12345; the cat".replaceAll("&amp;(\\d+);", "&$1;"));
	}
}
