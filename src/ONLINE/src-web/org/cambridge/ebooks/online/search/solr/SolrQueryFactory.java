package org.cambridge.ebooks.online.search.solr;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;

/**
 * 
 * @author mmanalo
 *
 */
public class SolrQueryFactory {
	
	private static final Logger logger = Logger.getLogger(SolrQueryFactory.class);	
	
	public static SolrQuery newCboWithDoiOnlyInstance() {
		logger.info("=== Cbo with Doi Instance ===");
		
		SolrQuery q = new SolrQuery();
		q.addFilterQuery("doi:[0 TO 9] AND -flag:0");
		
		return q;		
	}
	
	public static SolrQuery newCboSearchInstance() {
		logger.info("=== Cbo Search Instance ===");
		
		SolrQuery q = newCboWithDoiOnlyInstance();
		q.setQueryType("quick");
		
		return q;			
	}
	
	public static SolrQuery newCboSearchInstance(String isbns) {
		logger.info("=== Cbo Search Instance ===");
		StringBuilder sb = new StringBuilder("(((doi:[0 TO 9] AND content_type:chapter) OR (content_type:article)) AND -flag:(0 OR 2))");
		if(StringUtils.isNotEmpty(isbns)) {
			sb.append(" OR (").append("isbn_issn:(").append(isbns.replaceAll(" ", " OR ")).append(")").append(")");
		}
		SolrQuery q = new SolrQuery();
		q.addFilterQuery(sb.toString());	
		q.setQueryType("quick");
		
		return q;			
	}	
	
	public static SolrQuery newPublisherInstance(String publisherId) {
		logger.info("=== Publisher Instance ===");
		logger.info("publisher id:" + publisherId);
		
		SolrQuery q = newCboWithDoiOnlyInstance();
		q.addFilterQuery("publisher_id:" + publisherId);
		
		return q;
	}
	
	public static SolrQuery newCboOnlyInstance() {
		logger.info("=== Cbo only Instance ===");
		
		SolrQuery q = new SolrQuery();
		q.addFilterQuery("-flag:0");
		
		return q;	
	}	
	
	public static SolrQuery newCboOnlyInstance(String isbns) {
		logger.info("=== Cbo Search Instance ===");
		StringBuilder sb = new StringBuilder("(doi:[0 TO 9] AND -flag:(0 OR 2))");
		if(StringUtils.isNotEmpty(isbns)) {
			sb.append(" OR (").append("isbn:(").append(isbns.replaceAll(" ", " OR ")).append(")").append(")");
		}
		SolrQuery q = new SolrQuery();
		q.addFilterQuery(sb.toString());
		
		return q;			
	}
}
