package org.cambridge.ebooks.online.search.subject;

import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.solr.client.solrj.SolrQuery;

public interface SubjectDataWorker {
	public SubjectBean generateAlphaMap(Map<String, Object> param);
	public SolrQuery generateQuery(String subjectCode, String byLetter, JSONArray jsonArray);
	public SubjectBean generateResult(Map<String, Object> param);
	public int getDefaultPageSize();
}
