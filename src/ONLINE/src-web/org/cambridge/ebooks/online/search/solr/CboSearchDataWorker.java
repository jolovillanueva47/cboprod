package org.cambridge.ebooks.online.search.solr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.cambridge.ebooks.online.dto.solr.SearchSolrDto;
import org.cambridge.ebooks.online.landingpage.BookContentItem;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.search.solr.FacetManager.Category;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrServerUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class CboSearchDataWorker implements SearchDataWorker {

	public static final String PUBLISHER_ID_CLC = System.getProperty("publisher.id.clc").trim();
	
	private static final Logger LOGGER = Logger.getLogger(CboSearchDataWorker.class);
	private static final Logger QRESP_LOGGER = Logger.getLogger(QueryResponse.class);
	private static final Logger SORT_BY_LOGGER = Logger.getLogger(Sorting.class);
		
	private static final String SORT_TITLE = "main_title_alphasort";
	private static final String SORT_AUTHOR = "author_alphasort";
		
	private static final int DEF_PAGE_SIZE = 50;	

	private static final String KEY_SEARCH_TEXT = "search_text";
	private static final String KEY_ID = "id";
	private static final String KEY_SORT_BY = "sort_by";
	private static final String KEY_PAGE_NUMBER = "page_number";
	private static final String KEY_PAGE_SIZE = "page_size";
	private static final String KEY_HTTP_REQUEST = "http_request";
	private static final String KEY_FACET_MANAGER = "facet_manager";
	private static final String KEY_OMIT_NORMS = "omit_norms";	
	private static final String KEY_SEARCH_FILTER = "search_filter";	
	
	private HttpServletRequest request;
	
	private String omitNorms = "false"; 
	
	public class Sorting{}
	
	public CboSearchDataWorker(){}
	public CboSearchDataWorker(HttpServletRequest request){
		this.request = request;
	}	
	
	public static String getKeySearchText() {
		return KEY_SEARCH_TEXT;
	}
	public static String getKeyId() {
		return KEY_ID;
	}
	public static String getKeySortBy() {
		return KEY_SORT_BY;
	}
	public static String getKeyPageNumber() {
		return KEY_PAGE_NUMBER;
	}
	public static String getKeyPageSize() {
		return KEY_PAGE_SIZE;
	}
	public static String getKeyHttpRequest() {
		return KEY_HTTP_REQUEST;
	}
	public static String getKeyFacetManager() {
		return KEY_FACET_MANAGER;
	}	
	public static String getKeyOmitNorms() {
		return KEY_OMIT_NORMS;
	}
	public static String getKeySearchFilter() {
		return KEY_SEARCH_FILTER;
	}
	
	public int getDefaultPageSize() {
		return DEF_PAGE_SIZE;
	}

	public SearchResultBean generateFilteredResult(Map<String, Object> param) {		
		
		int pageNum = 0;
		int pageSize = 0;
		String sortBy = "";
		FacetManager facetManager = null;
		
		if(null != param.get(KEY_PAGE_NUMBER)) {
			pageNum = ((Integer)param.get(KEY_PAGE_NUMBER)).intValue();
		}
		
		if(null != param.get(KEY_PAGE_SIZE)) {
			pageSize = ((Integer)param.get(KEY_PAGE_SIZE)).intValue();
		}
		
		if(null != param.get(KEY_SORT_BY)) {
			sortBy = (String)param.get(KEY_SORT_BY);
		}
				
		if(null != param.get(KEY_FACET_MANAGER)) {
			facetManager = (FacetManager)param.get(KEY_FACET_MANAGER);
		}
		
		SolrQuery q = null;
		if(null == request.getSession().getAttribute("searchText")) {
			Map<String, Object> advanceSearchParam = (Map<String, Object>)request.getSession().getAttribute("advanceSearchParam");
			q = generateAdvanceQuery(advanceSearchParam);
			
			//add filter clc, book, journal
			addAdvanceSearchFilter(q, param);
		} else {
			String _searchText = (String)request.getSession().getAttribute("searchText");
			q = generateQuickQuery(_searchText);
		}
		
		configurePager(q, pageNum, pageSize);
		
		if(StringUtils.isNotEmpty(sortBy)) {
			configureSort(q, sortBy);
		}
		
		facetManager.configureFacet(q);
		
		SearchResultBean bean = new SearchResultBean();
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getSearchCore().query(q, METHOD.GET);
			QRESP_LOGGER.info("[filter]query: " + q.toString());
			QRESP_LOGGER.info("[filter]response elapse time: " + response.getElapsedTime());
			QRESP_LOGGER.info("[filter]response status: " + response.getStatus());
		} catch (SolrServerException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}				
		populateResult(response, q, bean, pageNum, q.getRows(), false);
		
		FacetManager newFacetManager = facetManager;
		if(StringUtils.isNotEmpty(facetManager.getFilterType()) && !"multiple".equalsIgnoreCase(facetManager.getFilterType())) {	
			newFacetManager = generateFacet(response);
			
			List<String> facetQueryList = generateFacetQueryList(q, response);	
			
			newFacetManager.setFilterType(facetManager.getFilterType());
			newFacetManager.setFilterValue(facetManager.getFilterValue());
			newFacetManager.setFacetQueryList(facetQueryList);
			newFacetManager.setFacetQueryMap(response.getFacetQuery());
			newFacetManager.setFilterDisplayMap(facetManager.getFilterDisplayMap());
			newFacetManager.setFilterQueries(facetManager.getFilterQueries());
		} 		
		
		request.getSession().setAttribute("facet", newFacetManager);
		
		return bean;
	}
	
	public SearchResultBean generatePageResult(Map<String, Object> param) {	
		int pageNum = 0;
		int pageSize = 0;
		String sortBy = "";
		
		if(null != param.get(KEY_PAGE_NUMBER)) {
			pageNum = ((Integer)param.get(KEY_PAGE_NUMBER)).intValue();
		}
		
		if(null != param.get(KEY_PAGE_SIZE)) {
			pageSize = ((Integer)param.get(KEY_PAGE_SIZE)).intValue();
		}
		
		if(null != param.get(KEY_SORT_BY)) {
			sortBy = (String)param.get(KEY_SORT_BY);
		}		
						
		SolrQuery q = null;
		if(null == request.getSession().getAttribute("searchText")) {
			Map<String, Object> advanceSearchParam = (Map<String, Object>)request.getSession().getAttribute("advanceSearchParam");
			q = generateAdvanceQuery(advanceSearchParam);
			
			//add filter clc, book, journal
			addAdvanceSearchFilter(q, param);
		} else {
			String searchText = (String)request.getSession().getAttribute("searchText");
			q = generateQuickQuery(searchText);
		}				
		
		if(null != request.getSession().getAttribute("facet")) {
			FacetManager facet = (FacetManager)request.getSession().getAttribute("facet");
			q.setFilterQueries(facet.getFilterQueries());
		}
				
		configurePager(q, pageNum, pageSize);
		
		if(StringUtils.isNotEmpty(sortBy)) {
			configureSort(q, sortBy);
		}
		
		SearchResultBean bean = new SearchResultBean();
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getSearchCore().query(q, METHOD.GET);
			QRESP_LOGGER.info("[page]query: " + q.toString());
			QRESP_LOGGER.info("[page]response elapse time: " + response.getElapsedTime());
			QRESP_LOGGER.info("[page]response status: " + response.getStatus());
		} catch (SolrServerException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}				
		populateResult(response, q, bean, pageNum, q.getRows(), false);
		
		return bean;
	}

	public SearchResultBean generateQuickResult(Map<String, Object> param) {
		
		String searchText = "";
		String bid = "";
		String sortBy = "";
		int pageNum = 0;
		int pageSize = 0;
		
		boolean isSearchWithin = false;
		
		if(null != param.get(KEY_SEARCH_TEXT)) {
			searchText = (String)param.get(KEY_SEARCH_TEXT);			
		}
		
		if(null != param.get(KEY_ID)) {
			bid = (String)param.get(KEY_ID);
			isSearchWithin = true;
		}
		
		if(null != param.get(KEY_SORT_BY)) {
			sortBy = (String)param.get(KEY_SORT_BY);
		}
		
		if(null != param.get(KEY_PAGE_NUMBER)) {
			pageNum = ((Integer)param.get(KEY_PAGE_NUMBER)).intValue();
		}
		
		if(null != param.get(KEY_PAGE_SIZE)) {
			pageSize = ((Integer)param.get(KEY_PAGE_SIZE)).intValue();
		}
				
		if(null != param.get(KEY_OMIT_NORMS)) {
			omitNorms = (String)param.get(KEY_OMIT_NORMS);
		}
		
		// clear session attributes		
		request.getSession().removeAttribute("searchText");
		request.getSession().removeAttribute("advanceSearchParam");
		request.getSession().removeAttribute("facet");
				
		SearchResultBean bean = new SearchResultBean();

		SolrQuery q = null;		
		if(isSearchWithin) {
			q = generateQuickQuery(searchText, bid);
			q.setQueryType("quick_no_collapse");
		} else {
			q = generateQuickQuery(searchText);
		}
				
		configurePager(q, pageNum, pageSize);
		
		if(StringUtils.isNotEmpty(sortBy)) {
			configureSort(q, sortBy);
		}
				
		LOGGER.info("quick query:" + q.toString());	
		
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getSearchCore().query(q, METHOD.GET);
			QRESP_LOGGER.info("[quick]query: " + q.toString());
			QRESP_LOGGER.info("[quick]response elapse time: " + response.getElapsedTime());
			QRESP_LOGGER.info("[quick]response status: " + response.getStatus());
		} catch (SolrServerException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}				
		populateResult(response, q, bean, pageNum, q.getRows(), isSearchWithin);
		
		List<String> facetQueryList = generateFacetQueryList(q, response);
		
		FacetManager facetHandler = generateFacet(response);
		facetHandler.setFacetQueryList(facetQueryList);
		facetHandler.setFacetQueryMap(response.getFacetQuery());
		facetHandler.setFilterQueries(q.getFilterQueries());
		
		request.getSession().setAttribute("searchText", searchText);
		request.getSession().setAttribute("facet", facetHandler);
		request.getSession().setAttribute("hithighlight", "on");
				
		buildWebSearchTerms();
		
		return bean;
	}
	
	public SearchResultBean generateAdvanceResult(Map<String, Object> param) {			

		if(null == param.get("target")) {
			param = (Map<String, Object>)request.getSession().getAttribute("advanceSearchParam");
		}
		
		// clear session attributes		
		request.getSession().removeAttribute("searchText");
		request.getSession().removeAttribute("advanceSearchParam");
		request.getSession().removeAttribute("facet");
		
		SearchResultBean bean = new SearchResultBean();
		
		SolrQuery q = generateAdvanceQuery(param);
		
		//add filter clc, book, journal
		addAdvanceSearchFilter(q, param);
		
		LOGGER.info("advance query:" + q.toString());	
		
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getSearchCore().query(q, METHOD.GET);
			QRESP_LOGGER.info("[advance]query: " + q.toString());
			QRESP_LOGGER.info("[advance]response elapse time: " + response.getElapsedTime());
			QRESP_LOGGER.info("[advance]response status: " + response.getStatus());
		} catch (SolrServerException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}				
		populateResult(response, q, bean, 0, q.getRows(), false);
		
		List<String> facetQueryList = generateFacetQueryList(q, response);		
		
		FacetManager facetHandler = generateFacet(response);
		facetHandler.setFacetQueryList(facetQueryList);
		facetHandler.setFacetQueryMap(response.getFacetQuery());
		facetHandler.setFilterQueries(q.getFilterQueries());
		
		request.getSession().setAttribute("advanceSearchParam", param);
		request.getSession().setAttribute("facet", facetHandler);
		request.getSession().setAttribute("hithighlight", "on");
		
		buildWebSearchTerms();
		
		return bean;
	}
	
	public SolrQuery generateAdvanceQuery(Map<String, Object> param) {
		
		String isbns = "";
		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}
		
		SolrQuery q = SolrQueryFactory.newCboSearchInstance(isbns);
		q.setQuery(generateAdvanceQueryString(param));
		
		// print date
		configurePublicationDateFacet(q);
		
		configurePager(q, 0, 0);
		
		return q;
	}
	public SolrQuery generateQuickQuery(String searchText) {
		String isbns = "";
		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}
		
		SolrQuery q = SolrQueryFactory.newCboSearchInstance(isbns);
		q.setQuery(generateQueryString(StringUtil.escapeSolrCharacters(searchText)));
		
		// print date
		configurePublicationDateFacet(q);
		
		configurePager(q, 0, 0);
		
		return q;
	}
	
	public SolrQuery generateQuickQuery(String searchText, String id) {
		String isbns = "";
		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}
		
		SolrQuery q = SolrQueryFactory.newCboSearchInstance(isbns);
		q.setQuery(generateSearchWithinQueryString(StringUtil.escapeSolrCharacters(searchText)));
		q.addFilterQuery("book_id:" + id);
		
		// print date
		configurePublicationDateFacet(q);
		
		return q;
	}
	
	protected void configurePublicationDateFacet(SolrQuery q) {
		// print date
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		StringBuilder s = null;
		for(int i = 1900, j = 0; i <= currentYear;) {
			if(j == 0) {
				s = new StringBuilder();				
				s.append("print_date:[").append(i).append(" TO ");
				
				i = i + 9;
				if(i > currentYear) {
					i = currentYear;
				}
				j++;
			} else {
				s.append(i).append("]");
				q.addFacetQuery(s.toString());
				i++;
				j = 0;				
			}			
		}	
	}
	
	protected String generateAdvanceQueryString(Map<String, Object> param) {				
		String[] target = (String[])param.get("target");
		String[] condition = (String[])param.get("condition");
		String[] logicalOp = (String[])param.get("logical_op");
		String[] searchText = (String[])param.get("search_text");
		String[] searchText2 = (String[])param.get("search_text_2");
		
		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder preParenthesisBuilder = new StringBuilder();
		int validLogicalOpCtr = 0;
		int logicalOpCtr = 0;
		for(int i = 0; i < target.length; i++) {
			if(StringUtils.isEmpty(searchText[i])) {
				continue;
			} 
			validLogicalOpCtr++;
			if(validLogicalOpCtr > 1 && StringUtils.isNotEmpty(logicalOp[logicalOpCtr])) {
				queryBuilder.append(" ").append(logicalOp[logicalOpCtr].toUpperCase()).append(" ");
				logicalOpCtr++;
			}
			preParenthesisBuilder.append("(");
			if(searchText2.length > 0 && ("between".equalsIgnoreCase(condition[i]) || "from".equalsIgnoreCase(condition[i]))) {
				queryBuilder.append(buildTargetQuery(target[i], condition[i], 
						StringUtil.escapeSolrCharacters(searchText[i]), 
						StringUtil.escapeSolrCharacters(searchText2[i])));
			} else {
				queryBuilder.append(buildTargetQuery(target[i], condition[i], 
						StringUtil.escapeSolrCharacters(searchText[i]), ""));
			}
			
			queryBuilder.append(")");
		}
		LOGGER.info("advance query:" + preParenthesisBuilder.toString() + queryBuilder.toString());
		
		return preParenthesisBuilder.toString() + queryBuilder.toString();
	}
	
	protected String generateQueryString(String searchText) {
		if(StringUtils.isNotEmpty(searchText)) {
			searchText = searchText.toLowerCase();
		}
		
		/*
		 * Searchables
		 * 
		 * author_name
		 * author_affiliation
		 * contributor_name
		 * main_title
		 * main_subtitle
		 * title
		 * subtitle
		 * subject
		 * print_date_text
		 * online_date_text
		 * isbn_issn
		 * series
		 * chapter_fulltext
		 * keyword_text
		 */
		StringBuilder results = new StringBuilder();
		
		if(StringUtils.isNumeric(searchText)) {			
			results.append("doi:(")
			.append(searchText)
			.append(") OR isbn_issn:(")
			.append(searchText)
			.append(") OR alt_isbn_hardback:(")
			.append(searchText)
			.append(") OR alt_isbn_paperback:(")
			.append(searchText)
			.append(") OR print_date:(")
			.append(searchText)
			.append(")");
		} else if(StringUtils.isAlpha(searchText) || StringUtils.isAlphaSpace(searchText)) {
			results.append("chapter_fulltext:(")
			.append(searchText)
			.append(") OR article_fulltext:(")
			.append(searchText)
			.append(") OR title:(")
			.append(searchText)
			.append(") OR author_name:(")
			.append(searchText)
			.append(") OR author_affiliation:(")
			.append(searchText)
			.append(") OR abstract:(")
			.append(searchText);
			if("true".equalsIgnoreCase(omitNorms)) {
				results.append(") OR keyword_text:(");
			} else {
				results.append(") OR keyword_norms:(");
			}
			results.append(searchText)
			.append(") OR contributor_name:(")
			.append(searchText)
			.append(") OR main_title:(")
			.append(searchText)
			.append(") OR main_subtitle:(")
			.append(searchText)
			.append(") OR volume_title:(")
			.append(searchText)
			.append(") OR subject:(")
			.append(searchText)
			.append(") OR series:(")
			.append(searchText)
			.append(") OR subtitle:(")
			.append(searchText)
			.append(")");
		} else {		
			results.append("chapter_fulltext:(")
			.append(searchText)
			.append(") OR article_fulltext:(")
			.append(searchText)
			.append(") OR title:(")
			.append(searchText)
			.append(") OR author_name:(")
			.append(searchText)
			.append(") OR author_affiliation:(")
			.append(searchText)
			.append(") OR abstract:(")
			.append(searchText);
			if("true".equalsIgnoreCase(omitNorms)) {
				results.append(") OR keyword_text:(");
			} else {
				results.append(") OR keyword_norms:(");
			}
			results.append(searchText)
			.append(") OR contributor_name:(")
			.append(searchText)
			.append(") OR main_title:(")
			.append(searchText)
			.append(") OR main_subtitle:(")
			.append(searchText)
			.append(") OR volume_title:(")
			.append(searchText)
			.append(") OR subject:(")
			.append(searchText)
			.append(") OR series:(")
			.append(searchText)
			.append(") OR subtitle:(")
			.append(searchText)
			.append(") OR doi:(")
			.append(searchText)
			.append(") OR isbn_issn:(")
			.append(searchText)
			.append(") OR alt_isbn_hardback:(")
			.append(searchText)
			.append(") OR alt_isbn_paperback:(")
			.append(searchText)
			.append(") OR print_date:(")
			.append(searchText)
			.append(")");
		}
		
		LOGGER.info("raw_query:" + results.toString());
		
		return results.toString();
	}
	
	protected String generateSearchWithinQueryString(String searchText) {
		if(StringUtils.isNotEmpty(searchText)) {
			searchText = searchText.toLowerCase();
		}
		
		StringBuilder results = new StringBuilder();
		
		if(StringUtils.isNumeric(searchText)) {			
			results.append("doi:(")
			.append(searchText)
			.append(")");
		} else if(StringUtils.isAlpha(searchText) || StringUtils.isAlphaSpace(searchText)) {
			results.append("chapter_fulltext:(")
			.append(searchText)
			.append(") OR title:(")
			.append(searchText)
			.append(") OR abstract:(")
			.append(searchText);
			if("true".equalsIgnoreCase(omitNorms)) {
				results.append(") OR keyword_text:(");
			} else {
				results.append(") OR keyword_norms:(");
			}
			results.append(searchText)
			.append(") OR contributor_name:(")
			.append(searchText)
			.append(") OR subtitle:(")
			.append(searchText)
			.append(")");
		} else {		
			results.append("chapter_fulltext:(")
			.append(searchText)
			.append(") OR title:(")
			.append(searchText)
			.append(") OR abstract:(")
			.append(searchText);
			if("true".equalsIgnoreCase(omitNorms)) {
				results.append(") OR keyword_text:(");
			} else {
				results.append(") OR keyword_norms:(");
			}
			results.append(searchText)
			.append(") OR contributor_name:(")
			.append(searchText)
			.append(") OR subtitle:(")
			.append(searchText)
			.append(") OR doi:(")
			.append(searchText)
			.append(")");
		}
		
		LOGGER.info("raw_query:" + results.toString());
		
		return results.toString();
	}
	
	protected void configurePager(SolrQuery q, int pageNum, int pageSize) {
		if(pageNum > 0) {
			q.setStart((pageNum - 1) * getDefaultPageSize());
		} else {
			q.setStart(0);
		}
		if(pageSize > 0) {
			q.setRows(pageSize);
		} else {
			q.setRows(getDefaultPageSize());
		}
	}
	
	protected void configureSort(SolrQuery q, String sortBy) {
		SORT_BY_LOGGER.info("sort_by:" + sortBy);
		if(SORT_TITLE.equalsIgnoreCase(sortBy) || SORT_AUTHOR.equalsIgnoreCase(sortBy)) {
			q.setSortField(sortBy, ORDER.asc);
		} else {
			q.setSortField(sortBy, ORDER.desc);
		}			
	}
	
	protected static Map<String, List<CollapsedChapter>> generateBookClusterMap(ResultGroup resultGroup, SolrQuery q) {
		Map<String, List<CollapsedChapter>> bookClusterMap = new HashMap<String, List<CollapsedChapter>>();
					
		if(!resultGroup.isEmpty()) {
			SolrQuery _q = new SolrQuery();
			_q.addFilterQuery("doi:[0 TO 9]");
			_q.addFilterQuery("book_id:" + resultGroup.getQuery());
			_q.addFilterQuery("-id:" + resultGroup.getExclusionQuery());
			_q.setQueryType("book_cluster");
			_q.setQuery(q.getQuery());
			
			try {
				QueryResponse qResp = SolrServerUtil.getSearchCore().query(_q, METHOD.GET);
				QRESP_LOGGER.info("[cluster]query: " + _q.toString());
				QRESP_LOGGER.info("[cluster]response elapse time: " + qResp.getElapsedTime());
				QRESP_LOGGER.info("[cluster]response status: " + qResp.getStatus());
				
				List<SearchSolrDto> docs = qResp.getBeans(SearchSolrDto.class);					
				
				for(SearchSolrDto doc : docs) {
					String bookId = doc.getBookId();
					
					CollapsedChapter collapseChapter = new CollapsedChapter();
					collapseChapter.setId(doc.getId());
					collapseChapter.setLabel(doc.getLabel());
					collapseChapter.setTitle(doc.getTitle());
											
					List<CollapsedChapter> collapseChapterList = new ArrayList<CollapsedChapter>();
					collapseChapterList.add(collapseChapter);
					
					if(null == bookClusterMap.get(bookId)) {
						bookClusterMap.put(bookId, collapseChapterList);
					} else {
						List<CollapsedChapter> _collapseChapterList = bookClusterMap.get(bookId);
						_collapseChapterList.addAll(collapseChapterList);
						bookClusterMap.put(bookId, _collapseChapterList);
					}
				}

			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}	
		}		
		
		return bookClusterMap;
	}
	
	protected void addAdvanceSearchFilter(SolrQuery q, Map<String, Object> param) {
		if(param.get(KEY_SEARCH_FILTER) != null) {
			StringBuilder sb = new StringBuilder();
			List<String> filters = Arrays.asList((String[])param.get(KEY_SEARCH_FILTER));

			if(filters.contains("clc")) {
				sb.append("publisher_id:" + PUBLISHER_ID_CLC);
			}

			if(filters.contains("book") && filters.contains("clc")) {
				sb.append(" OR content_type:chapter");
			} else if(filters.contains("book")) {
				sb.append("content_type:chapter");
			}
		
			if(filters.contains("journal") && (filters.contains("book") || filters.contains("clc"))) {
				sb.append(" OR content_type:article");
			} else if(filters.contains("journal")) {
				sb.append("content_type:article");
			}
			
			q.addFilterQuery(sb.toString());
		}
	}
	
	private void populateResult(QueryResponse response, SolrQuery q, SearchResultBean bean, int pageNum, int pageSize, boolean isSearchWithin) {		
		ResultGroup resultGroup = new ResultGroup(response);
		
		List<SearchSolrDto> searchSolrDocs = resultGroup.getSearchSolrDocs();
		
		Map<String, List<CollapsedChapter>> bookClusterMap = null;
		if(!isSearchWithin) {
			bookClusterMap = generateBookClusterMap(resultGroup, q);
		}
		SubscriptionUtil subscription = new SubscriptionUtil(OrgConLinkedMap.getAllBodyIds(request.getSession()), OrgConLinkedMap.getDirectBodyIds(request.getSession()));

		SpellCheckResponse spellCheckResponse = response.getSpellCheckResponse();
		if(null != spellCheckResponse && null != spellCheckResponse.getSuggestions() && spellCheckResponse.getSuggestions().size() > 0) {
			List<String> alternatives = spellCheckResponse.getSuggestions().get(0).getAlternatives();
			if(null != alternatives && alternatives.size() > 0) {
				bean.setSpellCheckTerm(alternatives.get(0));
			}
		}
		List<SearchResult> searchResultList = new ArrayList<SearchResult>();
		for(SearchSolrDto doc : searchSolrDocs) {
			SearchResult searchResult = new SearchResult(response, doc.getId(), 
					request, subscription, doc.getIsbnIssn());	
			
			searchResult.setContentType(doc.getContentType());
			
			if("chapter".equalsIgnoreCase(doc.getContentType())){	
				BookMetaData bookMetaData = new BookMetaData();
				BookContentItem bookContentItem = new BookContentItem();
				
				bookMetaData.setAuthorNameList(getAuthorNameList(doc.getAuthorName()));
				
				bookMetaData.setId(doc.getBookId());
				bookMetaData.setTitle(doc.getMainTitle());
				bookMetaData.setSubtitle(doc.getSubtitle());
				bookMetaData.setEdition(doc.getEdition());
				bookMetaData.setEditionNumber(doc.getEditionNumber());
				bookMetaData.setVolumeNumber(doc.getVolumeNumber());				
				bookMetaData.setVolumeTitle(doc.getVolumeTitle());
				bookMetaData.setPartNumber(doc.getPartNumber());
				bookMetaData.setPartTitle(doc.getPartTitle());				
				
				bookMetaData.setPublisherId(doc.getPublisherId());
				bookMetaData.setPublisherName(getPublisherName(doc.getPublisherId()));
				bookMetaData.setPrintDate(doc.getPrintDateDisplay());
				
				bookMetaData.setSeries(doc.getSeries());
				bookMetaData.setSeriesNumber(doc.getSeriesNumber());
				bookMetaData.setSeriesCode(doc.getSeriesCode());
				bookMetaData.setOnlineIsbn(doc.getIsbnIssn());
				bookMetaData.setDoi(doc.getBookDoi());
				
				if (StringUtils.isNotEmpty(doc.getAltIsbnHardback())) {					
					bookMetaData.setPrintIsbnType("alt_isbn_hardback");
				} else if(StringUtils.isNotEmpty(doc.getAltIsbnPaperback())) {
					bookMetaData.setPrintIsbnType("alt_isbn_paperback");
				} else if(StringUtils.isNotEmpty(doc.getAltEisbnEissn())) {
					bookMetaData.setPrintIsbnType("alt_eisbn_eissn");
				} else if(StringUtils.isNotEmpty(doc.getAltIsbnOther())) {
					bookMetaData.setPrintIsbnType("alt_isbn_other");
				}

				bookContentItem.setType(doc.getType());
				bookContentItem.setLabel(StringEscapeUtils.escapeXml(doc.getLabel()));		
				bookContentItem.setTitle(doc.getTitle());
				bookContentItem.setPageStart(doc.getPageStart());
				bookContentItem.setPageEnd(doc.getPageEnd());
				bookContentItem.setPdfFilename(doc.getPdfFilename());				
				
				if(!isSearchWithin) {
					searchResult.setCollapsedChapterList(bookClusterMap.get(doc.getBookId()));
				}
				
				bookMetaData.setCoinString(StringUtil.generateCoinString(bookMetaData, null));
				 
				searchResult.setBookMetaData(bookMetaData);
				searchResult.setBookContentItem(bookContentItem);
			} else {				
				searchResult.setJournalId(doc.getJournalId());
				searchResult.setArticleDoi(doc.getDoi());
				searchResult.setArticleType(doc.getType());
				searchResult.setArticleTitle(doc.getTitle());				
				searchResult.setArticleVolumeNumber(doc.getVolumeNumber());
				searchResult.setArticleIssueNumber(doc.getIssueNumber());				
				searchResult.setArticlePrice(doc.getPrice());
				searchResult.setArticleAbstractFilename(doc.getPdfFilename());
				searchResult.setArticlePageStart(doc.getPageStart());
				searchResult.setArticlePageEnd(doc.getPageEnd());
				searchResult.setArticleOnlineDate(doc.getOnlineDateDisplay());
				searchResult.setArticlePrintDate(doc.getPrintDateDisplay());
			}
			searchResultList.add(searchResult);
		}		
		bean.setSearchResultList(searchResultList);
		bean.setResultsFound(resultGroup.getTotalGroups());
		bean.setResultsPerPage(pageSize);
		if(bean.getResultsFound() > 0 ) {
			if(pageNum > 0) {
				bean.setCurrentPage(pageNum);
			} else {
				bean.setCurrentPage(1);
			}
		} else {
			bean.setCurrentPage(0);
		}
	}
	
	private void buildWebSearchTerms(){
		StringBuilder searchTerms = new StringBuilder();
		if(null == request.getSession().getAttribute("searchText")) {	
			if(null == request.getSession().getAttribute("advanceSearchParam")) {
				return;
			}
			
			Map<String, String[]> param = (Map<String, String[]>)request.getSession().getAttribute("advanceSearchParam");
			String[] searchText1 = param.get("search_text");
			if(null != searchText1 && searchText1.length > 0) {
				for(String t : searchText1) {
					String searchText = t.replaceAll("\"", "\\\"");
					searchTerms.append(searchText).append(" ");
				}
			}			
			String[] searchText2 = param.get("search_text_2");
			if(null != searchText2 && searchText2.length > 0) {
				for(String t : searchText2) {
					String searchText = t.replaceAll("\"", "\\\"");
					searchTerms.append(searchText).append(" ");
				}
			}
		} else {
			String searchText = ((String)request.getSession().getAttribute("searchText")).replaceAll("\"", "\\\"");
			searchTerms.append(searchText);
		}
		
		if(searchTerms.length() > 0) {
			searchTerms = new StringBuilder(searchTerms.toString().trim());
		}
		
		request.getSession().setAttribute("searchTerms", searchTerms.toString());
	}
	
	private String buildTargetQuery(String target, String condition, String searchText, String searchText2) {
		if(StringUtils.isNotEmpty(searchText)) {
			searchText = searchText.toLowerCase();			
		}
		
		if(StringUtils.isNotEmpty(searchText2)) {
			searchText2 = searchText2.toLowerCase();			
		}
		
		StringBuilder result = new StringBuilder();
		String preMod = "";
		String postMod = "";
		String mod = "";
		if("between".equalsIgnoreCase(condition)) {
			preMod = "{";
			postMod = "}";
		} else if("from".equalsIgnoreCase(condition)) {
			preMod = "[";
			postMod = "]";
		} else if("contain".equalsIgnoreCase(condition)) {
			preMod = "(";
			postMod = ")";
		} else if("not_contain".equalsIgnoreCase(condition)) {
			mod = "-";
			preMod = "(";
			postMod = ")";
		} else if("exactly".equalsIgnoreCase(condition)) {
			preMod = "\"";
			postMod = "\"";
			searchText = searchText.replaceAll("\"", "");
		}
		
		if("author".equalsIgnoreCase(target)) {
			result.append(mod).append("author_name:").append(preMod).append(searchText).append(postMod);
		} else if("title".equals(target)) {
			result.append(mod).append("(main_title:").append(preMod).append(searchText).append(postMod)
			.append(" OR ").append("main_subtitle:").append(preMod).append(searchText).append(postMod)
			.append(" OR ").append("title:").append(preMod).append(searchText).append(postMod)
			.append(" OR ").append("subtitle:").append(preMod).append(searchText).append(postMod)
			.append(" OR ").append("volume_title:").append(preMod).append(searchText).append(postMod)
			.append(" OR ").append("main_subtitle:").append(preMod).append(searchText).append(postMod)
			.append(")");
		} else if("subject".equals(target)) {
			result.append(mod).append("subject:").append(preMod).append(searchText).append(postMod);
		} else if("print_date".equals(target)) {
			if("between".equalsIgnoreCase(condition) || "from".equalsIgnoreCase(condition)) {
				result.append("print_date:").append(preMod).append(searchText).append(" TO ")
				.append(searchText2).append(postMod);
			} else {
				result.append(mod).append("print_date:").append(preMod).append(searchText).append(postMod);
			}
		} else if("online_date".equals(target)) {
			if("between".equalsIgnoreCase(condition) || "from".equalsIgnoreCase(condition)) {
				result.append("online_date:").append(preMod).append(searchText).append(" TO ")
				.append(searchText2).append(postMod);
			} else {
				result.append(mod).append("online_date_display:").append(preMod).append(searchText).append(postMod);
			}
		} else if("isbn_issn".equals(target)) {
			if("between".equalsIgnoreCase(condition) || "from".equalsIgnoreCase(condition)) {
				result.append("isbn_issn:").append(preMod).append(searchText).append(" TO ")
				.append(searchText2).append(postMod).append(" OR ")
				.append("alt_isbn_hardback:").append(preMod).append(searchText).append(" TO ")
				.append(searchText2).append(postMod).append(" OR ")
				.append("alt_isbn_paperback:").append(preMod).append(searchText).append(" TO ")
				.append(searchText2).append(postMod);
			} else {
				result.append(mod).append("isbn_issn:").append(preMod).append(searchText).append(postMod).append(" OR ")
				.append(mod).append("alt_isbn_hardback:").append(preMod).append(searchText).append(postMod).append(" OR ")
				.append(mod).append("alt_isbn_paperback:").append(preMod).append(searchText).append(postMod);
			}			
		} else if("series".equals(target)) {
			result.append(mod).append("series:").append(preMod).append(searchText).append(postMod);
		} else if("doi".equals(target)) {
			result.append(mod).append("doi:").append(preMod).append(searchText).append(postMod);
		} else if("keyword".equals(target)) {
			result.append(mod).append("keyword_text:").append(preMod).append(searchText).append(postMod);
		} else if("auth_aff".equals(target)) {
			result.append(mod).append("author_affiliation:").append(preMod).append(searchText).append(postMod);
		} else {
			result.append(mod).append("(author_name:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR author_affiliation:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR contributor_name:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR main_title:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR main_subtitle:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR title:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR subtitle:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR volume_title:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR subject:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR print_date:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR online_date_display:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR isbn_issn:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR series:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR chapter_fulltext:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR keyword_text:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR article_fulltext:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR doi:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(" OR abstract:")
			.append(preMod)
			.append(searchText)
			.append(postMod)
			.append(")");
		}
		
		return result.toString();
	}	
	
	private List<String> generateFacetQueryList(SolrQuery query, QueryResponse response) {		
		Set<String> facetQuerySet = response.getFacetQuery().keySet();
		List<String> facetQueryList = new ArrayList<String>();
		Map<String, String> tmpMap = new HashMap<String, String>();
		String tmpKey = "";
		for(String qKey : facetQuerySet) {
			tmpKey = qKey.replaceAll("print_date:\\[", "").replaceAll(" TO \\d\\d\\d\\d\\]", "");
			facetQueryList.add(tmpKey);				
			tmpMap.put(tmpKey, qKey);
		}
		
		Collections.sort(facetQueryList);
		Collections.reverse(facetQueryList);
		
		List<String> result = new ArrayList<String>();
		if(null != facetQueryList) {
			int ctr = 0;
			for(String key : facetQueryList) {		
				String fQKey = (String)tmpMap.get(key);
				if(response.getFacetQuery().get(fQKey) > 0) {					
					result.add(fQKey);
					ctr++;
				}
				if(ctr == 6) {
					break;
				}
			}
		}
		
		return result;
	}
	
	private FacetManager generateFacet(QueryResponse response) {				
		FacetManager facet = new FacetManager();
		List<FacetField> facetFieldList = response.getFacetFields();	
		for(FacetField facetField : facetFieldList) {
			if(null == facetField) {
				continue;
			}
			
			if("content_type".equalsIgnoreCase(facetField.getName())) {
				List<Category> contentTypeList = new ArrayList<Category>(2);
				contentTypeList.add(null); //dummy
				contentTypeList.add(null); //dummy
				
				if(null != facetField.getValues() && facetField.getValues().size() > 1 
						&& facetField.getValues().size() < 3) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();
						if("chapter".equalsIgnoreCase(c.getName())) {
							cat.setName(c.getName());
							cat.setCount(c.getCount());
							contentTypeList.add(0, cat);									
						} else {
							cat.setName(c.getName());
							cat.setCount(c.getCount());
							contentTypeList.add(1, cat);							
						}
					}
				} else if(null != facetField.getValues()) {
					int ctr = 0;
					contentTypeList.clear();
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();					
						cat.setName(c.getName());
						cat.setCount(c.getCount());
						contentTypeList.add(cat);
					}
				}
				facet.setContentTypeList(contentTypeList);
			} else if("subject_facet".equalsIgnoreCase(facetField.getName())) {
				List<Category> subjectFacetList = new ArrayList<Category>();
				if(null != facetField.getValues()) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();			
						cat.setName(c.getName());
						cat.setCount(c.getCount());
						subjectFacetList.add(cat);						
					}
				}
				facet.setSubjectFacetList(subjectFacetList);
			} else if("author_name_facet".equalsIgnoreCase(facetField.getName())) {
				List<Category> authorNameFacetList = new ArrayList<Category>();
				if(null != facetField.getValues()) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();	
						if(!"no article author".equalsIgnoreCase(c.getName())) {
							cat.setName(c.getName());
							cat.setCount(c.getCount());
							authorNameFacetList.add(cat);
						}						
					}
				}				
				facet.setAuthorNameFacetList(authorNameFacetList);
			} else if("book_title_facet".equalsIgnoreCase(facetField.getName())) {
				List<Category> bookTitleFacetList = new ArrayList<Category>();
				if(null != facetField.getValues()) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();			
						cat.setName(c.getName());
						cat.setCount(c.getCount());
						bookTitleFacetList.add(cat);
					}
				}
				facet.setBookTitleFacetList(bookTitleFacetList);
			} else if("journal_title_facet".equalsIgnoreCase(facetField.getName())) {
				List<Category> journalTitleFacetList = new ArrayList<Category>();
				if(null != facetField.getValues()) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();			
						cat.setName(c.getName());
						cat.setCount(c.getCount());
						journalTitleFacetList.add(cat);
					}
				}
				facet.setJournalTitleFacetList(journalTitleFacetList);
			} else if("series_facet".equalsIgnoreCase(facetField.getName())) {
				List<Category> seriesFacetList = new ArrayList<Category>();
				if(null != facetField.getValues()) {
					int ctr = 0;
					for(Count c : facetField.getValues()) {
						ctr++;
						Category cat = new Category();			
						cat.setName(c.getName());
						cat.setCount(c.getCount());
						seriesFacetList.add(cat);
					}
				}
				facet.setSeriesFacetList(seriesFacetList);
			}
		}
		
		return facet;
	}
	
	private List<String> getAuthorNameList(String authorName){
		String[] nameList = authorName.split("=DEL=");
		List <String> authorNameList = new ArrayList<String>();
		for(String name : nameList){
			authorNameList.add(name); 
		}
		return authorNameList;
	}
	
	
	//TODO TEMPORARY solution.. wala kasing DB to @_@
	private String getPublisherName(String publisherId){
		String publisherName = "";
		
		if("1168".equals(publisherId)){
			publisherName = "Cambridge University Press";
		}
		
		return publisherName;
	}
}