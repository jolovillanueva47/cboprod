package org.cambridge.ebooks.online.search.subject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.search.solr.SolrQueryFactory;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrServerUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class CboSubjectDataWorker implements SubjectDataWorker {

	private static final Logger logger = Logger.getLogger(CboSubjectDataWorker.class);	
	
	private static final String[] ALPHABETS = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
		
	private static final int DEF_PAGE_SIZE = 50;
	
	private static final String KEY_SUBJECT_CODE = "subject_code";
	private static final String KEY_JSON_ARRAY = "json_array";
	private static final String KEY_SORT_BY = "sort_by";
	private static final String KEY_BY_LETTER = "by_letter";
	private static final String KEY_PAGE_NUMBER = "page_number";
	private static final String KEY_PAGE_SIZE = "page_size";
	private static final String KEY_HTTP_SESSION = "http_session";
	private HttpServletRequest request;
	
	
	public CboSubjectDataWorker(HttpServletRequest request){
		this.request = request;
	}
	
	public static String getKeySubjectCode() {
		return KEY_SUBJECT_CODE;
	}
	public static String getKeyJsonArray() {
		return KEY_JSON_ARRAY;
	}	
	public static String getKeySortBy() {
		return KEY_SORT_BY;
	}
	public static String getKeyByLetter() {
		return KEY_BY_LETTER;
	}
	public static String getKeyPageNumber() {
		return KEY_PAGE_NUMBER;
	}
	public static String getKeyPageSize() {
		return KEY_PAGE_SIZE;
	}	
	public static String getKeyHttpSession() {
		return KEY_HTTP_SESSION;
	}

	public SubjectBean generateAlphaMap(Map<String, Object> param) {
		SubjectBean bean = new SubjectBean();
		
		String subjectCode = "";
		JSONArray jsonArray = null;
		if(null != param.get(KEY_SUBJECT_CODE)) {
			subjectCode = (String)param.get(KEY_SUBJECT_CODE);
		}
		if(null != param.get(KEY_JSON_ARRAY)) {
			jsonArray = (JSONArray)param.get(KEY_JSON_ARRAY);
		}		
		
		SolrQuery q = generateQuery(subjectCode, "", jsonArray);		
		
		populateAlpaMap(q, bean);		
		
		return bean;
	}
	
	public SolrQuery generateQuery(String subjectCode, String byLetter, JSONArray allLeaves) {
		
		String isbns = "";
		
		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
		if(null != obj) {
			isbns = (String)obj;
		}
		
		SolrQuery q = SolrQueryFactory.newCboOnlyInstance(isbns);
				
		StringBuilder queryBuilder = new StringBuilder();				
		queryBuilder.append("subject_code:");		
		
		if(null != allLeaves) {
			int start = 0, end = 0;
			queryBuilder.append("(");
			for(int x = 0; x < allLeaves.size(); x++){
				subjectCode = allLeaves.getString(x);
				start = subjectCode.indexOf("subjectId") + 12;
				end = subjectCode.indexOf(",", start) - 1;
				subjectCode = subjectCode.substring(start, end);					
				queryBuilder.append(subjectCode);
				if(x < allLeaves.size() - 1) {
					queryBuilder.append(" OR ");
				}
			}		
			queryBuilder.append(")");
		} else {
			queryBuilder.append(subjectCode);
		}
		
		if(StringUtils.isNotEmpty(byLetter)) {
			queryBuilder.append(" AND title_alphasort:(")
			.append(String.valueOf(byLetter).toLowerCase())
			.append("* ")
			.append(String.valueOf(byLetter).toUpperCase())
			.append("*)");
		}
		
		q.setQuery(queryBuilder.toString());	
		
		configureSort(q, "");
		configurePager(q, 0, 0);
		
		return q;
	}	
	
	public SubjectBean generateResult(Map<String, Object> param) {
		
		String subjectCode = "";
		String sortBy = "";
		String byLetter = "";
		int pageNum = 0;
		int pageSize = 0;
		JSONArray jsonArray = null;
		
		if(null != param.get(KEY_SUBJECT_CODE)) {
			subjectCode = (String)param.get(KEY_SUBJECT_CODE);
		}
		if(null != param.get(KEY_JSON_ARRAY)) {
			jsonArray = (JSONArray)param.get(KEY_JSON_ARRAY);
		}		
		if(null != param.get(KEY_SORT_BY)) {
			sortBy = (String)param.get(KEY_SORT_BY);
		}
		if(null != param.get(KEY_BY_LETTER)) {
			byLetter = (String)param.get(KEY_BY_LETTER);
		}
		if(null != param.get(KEY_PAGE_NUMBER)) {
			pageNum = ((Integer)param.get(KEY_PAGE_NUMBER)).intValue();
		}	
		if(null != param.get(KEY_PAGE_SIZE)) {
			pageSize = ((Integer)param.get(KEY_PAGE_SIZE)).intValue();
		}
		
		SubjectBean bean = new SubjectBean();
		
		SolrQuery q = generateQuery(subjectCode, byLetter, jsonArray);
		
		configurePager(q, pageNum, pageSize);
		configureSort(q, sortBy);
		
		if(StringUtils.isNotEmpty(sortBy)) {
			configureSort(q, sortBy);
		}
		
		populateResult(q, bean, pageNum, q.getRows(), byLetter);		
		
		return bean;
	}
	
	private void populateAlpaMap(SolrQuery q, SubjectBean bean) {
		StringBuilder alphaBuilder = new StringBuilder();

		QueryResponse response = null;
		try {			
			// for alpha map			
			q.setRows(Integer.MAX_VALUE);
			q.setStart(0);
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);		
			List<BookAndChapterSolrDto> bookDocAlphaList = response.getBeans(BookAndChapterSolrDto.class);
			for(BookAndChapterSolrDto doc : bookDocAlphaList) {	
				char titleAlpha = doc.getTitleAlphasort().charAt(0);
				if(!alphaBuilder.toString().contains(String.valueOf(titleAlpha))) {						
					alphaBuilder.append(titleAlpha);
				}
			}			
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		Map<String, String> alphaMap = new LinkedHashMap<String, String>();	
		
		for(String alpha : ALPHABETS) {
			if(alphaBuilder.toString().toUpperCase().contains(alpha)) {
				alphaMap.put(alpha, "link");
			} else {
				alphaMap.put(alpha, "");
			}
		}
		bean.setAlphaMap(alphaMap);		
	}

	private void populateResult(SolrQuery q, SubjectBean bean, int pageNum, int pageSize, String byLetter) {
		SubscriptionUtil subscription = new SubscriptionUtil(OrgConLinkedMap.getAllBodyIds(request.getSession()), OrgConLinkedMap.getDirectBodyIds(request.getSession()));
	
		QueryResponse response = null;
		List<BookAndChapterSolrDto> bookDocList = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);			
			bookDocList = response.getBeans(BookAndChapterSolrDto.class);			
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		List<BookMetaData> bookMetaDataList = new ArrayList<BookMetaData>();	
		
		int ctr = 0;
		for(BookAndChapterSolrDto doc : bookDocList) {	
			ctr++;
			
			BookMetaData bookMetaData = new BookMetaData();		
			char titleAlpha = doc.getTitleAlphasort().charAt(0);
			if(StringUtils.isNotEmpty(byLetter) && !"all".equalsIgnoreCase(byLetter)) {
				bean.setFirstLetter(String.valueOf(titleAlpha).toUpperCase());
			} else {
				bean.setFirstLetter("All");
			}
			
			bookMetaData.setAuthorNameList(doc.getAuthorNameList());
			bookMetaData.setAuthorNameLfList(doc.getAuthorNameLfList());
			bookMetaData.setAuthorAffiliationList(doc.getAuthorAffiliationList());			
			bookMetaData.setAuthorRoleList(doc.getAuthorRoleList());		

			bookMetaData.setSubjectCodeList(doc.getSubjectCodeList());
			bookMetaData.setSubjectList(doc.getSubjectList());
			bookMetaData.setSubjectLevelList(doc.getSubjectLevelList());	

			bookMetaData.setOnlineIsbn(doc.getIsbn());
			bookMetaData.setHardbackIsbn(doc.getAltIsbnHardback());
			bookMetaData.setPaperbackIsbn(doc.getAltIsbnPaperback());
			bookMetaData.setAltIsbnEisbn(doc.getAltIsbnEisbn());
			bookMetaData.setAltIsbnOther(doc.getAltIsbnOther());
			
			bookMetaData.setId(doc.getId());
			bookMetaData.setDoi(doc.getDoi());
			bookMetaData.setTitle(doc.getTitle());
			bookMetaData.setSubtitle(doc.getSubtitle());
			bookMetaData.setEdition(doc.getEdition());
			bookMetaData.setEditionNumber(doc.getEditionNumber());
			bookMetaData.setVolumeNumber(doc.getVolumeNumber());
			bookMetaData.setVolumeTitle(doc.getVolumeTitle());
			bookMetaData.setPartNumber(doc.getPartNumber());
			bookMetaData.setPartTitle(doc.getPartTitle());			
			
			bookMetaData.setPrintDate(doc.getPrintDateDisplay());
			bookMetaData.setOnlineDate(doc.getOnlineDateDisplay());
			
			bookMetaData.setSeries(doc.getSeries());
			bookMetaData.setSeriesCode(doc.getSeriesCode());
			if(doc.getSeriesNumber() > 0) {
				bookMetaData.setSeriesNumber(String.valueOf(doc.getSeriesNumber()));
			}
			
			EBooksAccessListView vw = subscription.getAccessType(bookMetaData.getOnlineIsbn(), request, false);			
			bookMetaData.setAccessType(vw.hasAccessDisplay());
			
			bookMetaData.setBlurb(doc.getBlurb());
			bookMetaData.setAuthorSingleline(doc.getAuthorSingleline());
			
			bookMetaData.setPublisherId(doc.getPublisherId());
			bookMetaData.setPublisherName(doc.getPublisherName());
			bookMetaData.setPublisherLoc(doc.getPublisherLoc());
			
			bookMetaData.setCoinString(StringUtil.generateCoinString(bookMetaData, null));
			
			bookMetaDataList.add(bookMetaData);
		}		
		bean.setBookMetaDataList(bookMetaDataList);
		
		bean.setResultsFound(response.getResults().getNumFound());
		bean.setResultsPerPage(pageSize);
		if(bean.getResultsFound() > 0 ) {
			if(pageNum > 0) {
				bean.setCurrentPage(pageNum);
			} else {
				bean.setCurrentPage(1);
			}
		} else {
			bean.setCurrentPage(0);
		}
	}

	public int getDefaultPageSize() {
		return DEF_PAGE_SIZE;
	}	
	
	private void configureSort(SolrQuery q, String sortBy) {
		if(StringUtils.isEmpty(sortBy)) {
			q.setSortField("title_alphasort", ORDER.asc);
		} else {
			if("print_date".equals(sortBy) || "online_date".equals(sortBy)) {
				q.setSortField(sortBy, ORDER.desc);	
			} else {
				q.setSortField(sortBy, ORDER.asc);
			}
		}
	}
	
	private void configurePager(SolrQuery q, int pageNum, int pageSize) {
		if(pageNum > 0) {
			q.setStart((pageNum - 1) * getDefaultPageSize());
		} else {
			q.setStart(0);
		}
		if(pageSize > 0) {
			q.setRows(pageSize);
		} else {
			q.setRows(getDefaultPageSize());
		}
	}
}
