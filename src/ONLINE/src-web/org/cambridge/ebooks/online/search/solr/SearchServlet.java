package org.cambridge.ebooks.online.search.solr;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.rss.search.faceted.RssServletFaceted;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

public class SearchServlet extends HttpServlet {	
	private static final long serialVersionUID = 1L;	
	
	private static final Logger logger = Logger.getLogger(SearchServlet.class);	
	
	public static final String SEARCH_QUICK = "quick";
	public static final String SEARCH_ADVANCE = "advance";
	private static final String SEARCH_PAGE = "page";
	private static final String SEARCH_FILTER = "filter";
	
	private static final String SEARCH_COLLECTIONS = "collections";
	
	private static final String SORT_RELEVANCY = "score";
	
	private static final String DOMAIN_CJO = System.getProperty("cjo.domain");
	private static final String DOMAIN_CBO = System.getProperty("cbo.domain");
	private static final String CONTENT_CJO = System.getProperty("cjo.content");
	private static final String URL_SSL = System.getProperty("path.ssl");
	private static final String PUBLISHER_ID_CLC = System.getProperty("publisher.id.clc");
	private static final String PUBLISHER_ID_CBO = System.getProperty("publisher.id.cbo");
	
	private static final String ACC_URL_IDENTIFIER = "aaa/";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {			
		logger.info("=== BEGIN SEARCH SERVLET ===");	
		
		String searchType = req.getParameter("searchType");
		String publisherCode = publisherCode(req);
		
		logger.info("search type:" + searchType);		
		try {
			
			if(SEARCH_QUICK.equalsIgnoreCase(searchType)) {
				doQuickSearch(req, resp);
			} else if(SEARCH_PAGE.equalsIgnoreCase(searchType)) {
				doPageSearch(req, resp,req.getParameter("currentPage"));
			} else if(SEARCH_FILTER.equalsIgnoreCase(searchType)) {
				doFilterSearch(req, resp);
			} else if(SEARCH_ADVANCE.equalsIgnoreCase(searchType)) { 				
				doAdvanceSearch(req, resp);
			} else if (SEARCH_COLLECTIONS.equalsIgnoreCase(searchType)){
				doCollectionSearch(req, resp);
			} else {
				
			}
			
			forwardToJsp(req, resp);
		} catch (Exception e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("=== END SEARCH SERVLET ===");
	}

	protected static void forwardToJsp(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String path = req.getRequestURL().toString();
		
		//add rss changes
		if(path.contains(RssServletFaceted.RSS_FACETED_URL)){
			//should not forward to page
			return;
		}
		String url = "search_results.jsf";
		
		if (path.contains("search_aaa")){ // for accessibility -jubs
			if(StringUtils.isNotEmpty((String) req.getAttribute("advance_search_error_aaa"))){
				//forwardback to advance_search
				url = ACC_URL_IDENTIFIER + "advance_search.jsf";
			}else{
				url = ACC_URL_IDENTIFIER + url;
			}
		}
		
		req.setAttribute("domainCjo", DOMAIN_CJO);
		req.setAttribute("domainCbo", DOMAIN_CBO);
		req.setAttribute("contentCjo", CONTENT_CJO);
		req.setAttribute("urlSsl", URL_SSL);
		req.setAttribute("publisherIdClc", PUBLISHER_ID_CLC);
		req.setAttribute("publisherIdCbo", PUBLISHER_ID_CBO);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher(url);		
		dispatcher.forward(req, resp);
	}
	
	private void doFilterSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN FILTER SEARCH ===");			
		SearchDataWorker worker = new CboSearchDataWorker(req);		
		
		String filterType = req.getParameter("filterType");
		String filterValue = req.getParameter("filterValue");
		
		FacetManager facetManager = (FacetManager)req.getSession().getAttribute("facet");
		if(null != facetManager) {
			if(null != filterType) {
				facetManager.setFilterType(filterType);
			}
			
			if(null != filterValue) {
				facetManager.setFilterValue(filterValue);
			}
			
			facetManager.setFilterParamValues(req.getParameterValues("filter"));
		}
		facetManager.setFromLogin(req.getParameter("fromLogin"));
		
		int pageSize = worker.getDefaultPageSize();
		if(StringUtils.isNotEmpty(req.getParameter("resultsPerPage")) 
				&& StringUtils.isNumeric(req.getParameter("resultsPerPage"))) {
			
			pageSize = Integer.parseInt(req.getParameter("resultsPerPage"));
		}
		
		logger.info("processing search results...");		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboSearchDataWorker.getKeyPageNumber(), 0);
		param.put(CboSearchDataWorker.getKeyPageSize(), pageSize);
		param.put(CboSearchDataWorker.getKeySortBy(), "");
		param.put(CboSearchDataWorker.getKeyFacetManager(), facetManager);
		SearchResultBean bean = worker.generateFilteredResult(param);
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", bean);
		
		logger.info("=== END FILTER SEARCH ===");
	}
	
	protected void doPageSearch(HttpServletRequest req, HttpServletResponse resp, String currentPage) throws Exception {
		logger.info("=== BEGIN PAGE SEARCH ===");
		
		SearchDataWorker worker = new CboSearchDataWorker(req);
		
		// variables
		int pageSize = worker.getDefaultPageSize();
		int pageNum = 1;
		String sortBy = SORT_RELEVANCY;
		
		if(StringUtils.isNotEmpty(req.getParameter("resultsPerPage")) 
				&& StringUtils.isNumeric(req.getParameter("resultsPerPage"))) {
			pageSize = Integer.parseInt(req.getParameter("resultsPerPage"));
		} 
		
		//for aaa
		if(StringUtils.isNotEmpty(currentPage)
				&& StringUtils.isNumeric(currentPage)) {
			pageNum = Integer.parseInt(currentPage);
		}
		
		if(StringUtils.isNotEmpty(req.getParameter("sortType"))) {
			sortBy = req.getParameter("sortType");
		}
		
		logger.info("processing search results...");	
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboSearchDataWorker.getKeyPageNumber(), pageNum);
		param.put(CboSearchDataWorker.getKeyPageSize(), pageSize);
		param.put(CboSearchDataWorker.getKeySortBy(), sortBy);
		SearchResultBean searchResultBean = worker.generatePageResult(param);			
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", searchResultBean);
		
		logger.info("=== END PAGE SEARCH ===");
	}
	
	protected void doAdvanceSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN ADVANCE SEARCH ===");
		
		//get search filter
		String[] searchFilters = req.getParameterValues("sfilter");
		
		// variables
		String[] target = req.getParameterValues("target");
		String[] condition = req.getParameterValues("condition");
		String[] logicalOp = req.getParameterValues("logical_op");
		String[] searchText = req.getParameterValues("search_word");
		String[] searchText2 = req.getParameterValues("search_word_2");
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("target", target);
		param.put("condition", condition);
		param.put("logical_op", logicalOp);
		param.put("search_text", searchText);
		param.put("search_text_2", searchText2);
		param.put(CboSearchDataWorker.getKeySearchFilter(), searchFilters);
		
		SearchDataWorker worker = new CboSearchDataWorker(req);
		
		logger.info("processing search results...");			
		SearchResultBean searchResultBean = worker.generateAdvanceResult(param);			
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", searchResultBean);
		
		logger.info("=== END ADVANCE SEARCH ===");
	}	
	
	protected void doQuickSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN QUICK SEARCH ===");
				
		// variables
		String searchText = req.getParameter("searchText");
		String searchWithin = req.getParameter("searchWithinContent");
		String omitNorms = req.getParameter("omitNorms");
		String bid = "";
		if(StringUtils.isNotEmpty(searchWithin)) {
			bid = req.getParameter("bid");
		}
		
		logger.info("search text:" + searchText);
		SearchDataWorker worker = new CboSearchDataWorker(req);
		
		if(StringUtils.isNotEmpty(searchText)) {			
			logger.info("processing search results...");
			SearchResultBean searchResultBean = null;
			logger.info("search within:" + searchWithin);
			logger.info("bid:" + bid);
			
			Map<String, Object> param = new HashMap<String, Object>();
			if("on".equalsIgnoreCase(searchWithin) && StringUtils.isNotEmpty(bid)) {
				param.put(CboSearchDataWorker.getKeyId(), bid);
			} 
			param.put(CboSearchDataWorker.getKeySearchText(), searchText);
			param.put(CboSearchDataWorker.getKeyPageNumber(), 0);
			param.put(CboSearchDataWorker.getKeyPageSize(), 0);
			param.put(CboSearchDataWorker.getKeySortBy(), "");
			if(null != omitNorms) {
				param.put(CboSearchDataWorker.getKeyOmitNorms(), omitNorms);
			} 
			
			searchResultBean = worker.generateQuickResult(param);
			logger.info("search results done...");
			
			req.setAttribute("searchResultBean", searchResultBean);
		}		
		
		logger.info("=== END QUICK SEARCH ===");
	}	
	
	private void doCollectionSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN COLLECTION SEARCH ===");
		
		// variables
		String searchText = req.getParameter("searchText");
		String collection_id = req.getParameter("collection_id"); 
		
		logger.info("search text:" + searchText + ", collection_id:" + collection_id);
		SearchDataWorker worker = new CollectionSearchDataWorker();
		
		if(StringUtils.isNotEmpty(searchText) && StringUtils.isNotEmpty(collection_id)) {			
			logger.info("processing search results...");			
			Map<String, Object> param = new HashMap<String, Object>();
			
			param.put(CollectionSearchDataWorker.getKeySearchText(), searchText);
			param.put(CollectionSearchDataWorker.getKeyId(), collection_id);
			param.put(CollectionSearchDataWorker.getKeyPageNumber(), 0);
			param.put(CollectionSearchDataWorker.getKeyPageSize(), 0);
			param.put(CollectionSearchDataWorker.getKeySortBy(), "");
			
			SearchResultBean searchResultBean = worker.generateQuickResult(param);		
			
			logger.info("search results done...");
			
			req.setAttribute("searchResultBean", searchResultBean);
		}		
		
		logger.info("=== END COLLECTION SEARCH ===");
	}
	
	private void doPublisherFilteredQuickSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN PUBLISHER FILTERED QUICK SEARCH ===");
		
		// variables
		String searchText = req.getParameter("searchText");
		String searchWithin = req.getParameter("searchWithinContent");
		String bid = "";
		if(StringUtils.isNotEmpty(searchWithin)) {
			bid = req.getParameter("bid");
		}
		
		logger.info("search text:" + searchText);
		SearchDataWorker worker = new ByPublisherSearchDataWorker();
		
		if(StringUtils.isNotEmpty(searchText)) 
		{			
			logger.info("processing search results...");
			SearchResultBean searchResultBean = null;
			
			Map<String, Object> param = new HashMap<String, Object>();
			if("on".equalsIgnoreCase(searchWithin) && StringUtils.isNotEmpty(bid)) 
			{
				param.put(ByPublisherSearchDataWorker.getKeyId(), bid);
				
			} 
			param.put(ByPublisherSearchDataWorker.getKeySearchText(), searchText);
			param.put(ByPublisherSearchDataWorker.getKeyPageNumber(), 0);
			param.put(ByPublisherSearchDataWorker.getKeyPageSize(), 0);
			param.put(ByPublisherSearchDataWorker.getKeySortBy(), "");
			param.put(ByPublisherSearchDataWorker.getKeyHttpRequest(), req);
			
			searchResultBean = worker.generateQuickResult(param);
			logger.info("search results done...");
			
			req.setAttribute("searchResultBean", searchResultBean);
		}		
		
		logger.info("=== END PUBLISHER FILTERED QUICK SEARCH ===");
	}	
	
	private void doPublisherFilteredAdvanceSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN PUBLISHER FILTERED ADVANCE SEARCH ===");
		
		// variables
		String[] target = req.getParameterValues("target");
		String[] condition = req.getParameterValues("condition");
		String[] logicalOp = req.getParameterValues("logical_op");
		String[] searchText = req.getParameterValues("search_word");
		String[] searchText2 = req.getParameterValues("search_word_2");
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("target", target);
		param.put("condition", condition);
		param.put("logical_op", logicalOp);
		param.put("search_text", searchText);
		param.put("search_text_2", searchText2);
		param.put(ByPublisherSearchDataWorker.getKeyHttpRequest(), req);
		
		SearchDataWorker worker = new ByPublisherSearchDataWorker();
		
		logger.info("processing search results...");			
		SearchResultBean searchResultBean = worker.generateAdvanceResult(param);			
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", searchResultBean);
		
		logger.info("=== END PUBLISHER FILTERED ADVANCE SEARCH ===");
	}
	
	private void doPublisherFilteredSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN FILTER SEARCH ===");			
		SearchDataWorker worker = new ByPublisherSearchDataWorker();		
		
		String filterType = req.getParameter("filterType");
		String filterValue = req.getParameter("filterValue");
		
		FacetManager facetManager = (FacetManager)req.getSession().getAttribute("facet");
		if(null != facetManager) {
			if(null != filterType) {
				facetManager.setFilterType(filterType);
			}
			
			if(null != filterValue) {
				facetManager.setFilterValue(filterValue);
			}
			
			facetManager.setFilterParamValues(req.getParameterValues("filter"));
		}
		facetManager.setFromLogin(req.getParameter("fromLogin"));
		
		int pageSize = worker.getDefaultPageSize();
		if(StringUtils.isNotEmpty(req.getParameter("resultsPerPage")) 
				&& StringUtils.isNumeric(req.getParameter("resultsPerPage"))) {
			
			pageSize = Integer.parseInt(req.getParameter("resultsPerPage"));
		}
		
		logger.info("processing search results...");		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboSearchDataWorker.getKeyPageNumber(), 0);
		param.put(CboSearchDataWorker.getKeyPageSize(), pageSize);
		param.put(CboSearchDataWorker.getKeySortBy(), "");
		param.put(CboSearchDataWorker.getKeyFacetManager(), facetManager);
		SearchResultBean bean = worker.generateFilteredResult(param);
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", bean);
		
		logger.info("=== END FILTER SEARCH ===");
	}
	
	private void doPublisherFilteredPageSearch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		logger.info("=== BEGIN PAGE SEARCH ===");
		
		SearchDataWorker worker = new ByPublisherSearchDataWorker();
		
		// variables
		int pageSize = worker.getDefaultPageSize();
		int pageNum = 1;
		String sortBy = SORT_RELEVANCY;
		
		if(StringUtils.isNotEmpty(req.getParameter("resultsPerPage")) 
				&& StringUtils.isNumeric(req.getParameter("resultsPerPage"))) {
			pageSize = Integer.parseInt(req.getParameter("resultsPerPage"));
		} 
		
		if(StringUtils.isNotEmpty(req.getParameter("currentPage"))
				&& StringUtils.isNumeric(req.getParameter("currentPage"))) {
			pageNum = Integer.parseInt(req.getParameter("currentPage"));
		}
		
		if(StringUtils.isNotEmpty(req.getParameter("sortType"))) {
			sortBy = req.getParameter("sortType");
		}
		
		//aaa filter search when reset and select all button is clicked
		if(null !=  req.getParameter("currentPage_aaa") && StringUtils.isNumeric(req.getParameter("currentPage_aaa"))){
			logger.info("  --BEGIN AAA--");
			logger.info("  currentPage_aaa:" + req.getParameter("currentPage_aaa"));
			logger.info("  currentPage:" + pageNum);
			logger.info("  set page to currently selected page");
			pageNum = Integer.parseInt(req.getParameter("currentPage_aaa"));
			logger.info("  currentPage:" + pageNum);
			logger.info("  --END AAA--");
		}
		
		logger.info("processing search results...");	
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboSearchDataWorker.getKeyPageNumber(), pageNum);
		param.put(CboSearchDataWorker.getKeyPageSize(), pageSize);
		param.put(CboSearchDataWorker.getKeySortBy(), sortBy);
		SearchResultBean searchResultBean = worker.generatePageResult(param);			
		logger.info("search results done...");
		
		req.setAttribute("searchResultBean", searchResultBean);
		
		logger.info("=== END PAGE SEARCH ===");
	}
		
	public static String publisherCode(HttpServletRequest request){
		Object pubcode = request.getSession().getAttribute("publisherCode");
		return pubcode != null ? pubcode.toString() : "";
	}	
}
