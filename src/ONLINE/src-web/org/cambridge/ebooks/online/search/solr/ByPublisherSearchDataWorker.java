package org.cambridge.ebooks.online.search.solr;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;

public class ByPublisherSearchDataWorker extends CboSearchDataWorker {
	
	private static final Logger logger = Logger.getLogger(ByPublisherSearchDataWorker.class);	
	
	public SolrQuery generateQuickQuery(String searchText, String id) {
		SolrQuery q = generateQuickQuery(searchText);
		q.addFilterQuery("book_id:" + id);
		
		return q;
	}

	public SolrQuery generateQuickQuery(String searchText) {
		SolrQuery q = SolrQueryFactory.newPublisherInstance(PUBLISHER_ID_CLC);
		q.setQuery(generateQueryString(searchText));		
		
		configurePager(q, 0, 0);
		
		System.out.println("ByPublisherSearchDataWorker.generateQuickQuery() " + q.getQuery());
		return q;
	}

	public SolrQuery generateAdvanceQuery(Map<String, Object> param) {
		SolrQuery q = SolrQueryFactory.newPublisherInstance(PUBLISHER_ID_CLC);		
		q.setQuery(generateAdvanceQueryString(param));	
		
		configurePager(q, 0, 0);
		
		return q;
	}
	
	public static String getCLCId(){
		String result = "";

		EntityManagerFactory emf = null;
		EntityManager em = null;
			
		try
		{
			emf = Persistence.createEntityManagerFactory("eBooksService");
			em = emf.createEntityManager();
				
			Query query = em.createNativeQuery("SELECT publisher_id FROM ebook_publisher_details WHERE publisher_name = 'CLC'");
				
			result = (String)query.getSingleResult();
		}
		catch(Exception e){ logger.error("getPublisherId(String) " + e.getMessage()); }
		finally{ em.close(); }
		

		return result;
	}	
}
