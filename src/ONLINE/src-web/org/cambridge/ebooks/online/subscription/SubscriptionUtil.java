package org.cambridge.ebooks.online.subscription;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.concurrency.Book;
import org.cambridge.ebooks.online.concurrency.ConcurrencyListener;
import org.cambridge.ebooks.online.concurrency.ConcurrencyWorker;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author C2 Galang
 * 
 */

public class SubscriptionUtil {
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	private List<EBooksAccessListView> accessibleEisbns;
	
	public static final String SID_BOOK_NOT_AVAILABLE_FOR_SALE = "booknotavailableforsale";
	
	@SuppressWarnings("unused")
	private String bodyIds;
	
	private static Logger LOGGER = LogManager.getLogger(SubscriptionUtil.class); 
		
	public static final String[] FREE_CONTENT = new String[] {"toc", "index"};
	
//	private static final String SHARED_ORDER_TYPE_ACCESS = "'S','M','L'";

	private static final String NOT_AVAIL_FOR_SALE_ORDER_TYPE = "'S','T','M','L'";
	
	public SubscriptionUtil(){
		
	}
/*
	public SubscriptionUtil(String bodyIds) {
		this(bodyIds, null);
	}
*/
	
	
	public SubscriptionUtil(String bodyIds, String directBodyIds) {
		this(bodyIds, directBodyIds, null);
	}
	
	@SuppressWarnings("unchecked")
	public SubscriptionUtil(String bodyIds, String directBodyIds, String eisbn) {		
		//directBodyIds = "1898352," + directBodyIds;
		LOGGER.info("bodyId = " + bodyIds);
		LOGGER.info("directBodyId = " +directBodyIds);
		this.bodyIds = bodyIds;
		if(StringUtils.isEmpty(bodyIds)){
			accessibleEisbns = new ArrayList<EBooksAccessListView>();
			return;
		}		
		
		EntityManager em = emf.createEntityManager();
		
		try {
			long start = (new java.util.Date()).getTime();	
			
			LOGGER.info("QUERY START!!!");
						
			//get Demo Perpetual
			Query query;
			if(eisbn == null){
				query = em.createNativeQuery(getQueryDemoPerp(bodyIds, directBodyIds), EBooksAccessListView.class);
				accessibleEisbns = (List<EBooksAccessListView>) query.getResultList();
			}else{
				query = em.createNativeQuery(getIsbnAccessQueryDemoPerp(bodyIds, directBodyIds), EBooksAccessListView.class);
				query.setParameter(1, eisbn);
				accessibleEisbns = (List<EBooksAccessListView>) query.getResultList();
			}
						
			if(accessibleEisbns == null){
				accessibleEisbns = new ArrayList<EBooksAccessListView>();
			}			
			
			long endTime = (new java.util.Date()).getTime();
			LOGGER.info( "QUERY DONE!!!");
			LOGGER.info( "TIME : " + (endTime-start)/1000.0d);
		} catch (Exception e) {
			LOGGER.error( e.getMessage());	
			accessibleEisbns = new ArrayList<EBooksAccessListView>();
		} finally {
			em.close();
		}	
	}
	
	
	@SuppressWarnings("unused")
	private void viewIsbns(List<EBooksAccessListView> books){
		LOGGER.debug( "isbn access list");
		for(EBooksAccessListView view : books){
			LOGGER.debug( view.getEisbn() + ",");
		}
	}
	
	
	@Deprecated
	public String isEisbnAccessibleDisplay(String eisbn, HttpSession session) {
		String accessType = AccessList.NO_ACCESS;
		boolean accessible = isFreeTrial(session, eisbn);		
		if(accessible){
			accessType = AccessList.HAS_ACCESS;
		}
		
		if (!accessible) {
			AccessList acl = new AccessList(accessibleEisbns);
			String _accessType = acl.getAccessTypeString(eisbn);
				
			if(AccessList.HAS_ACCESS.equals(_accessType) || AccessList.DEMO.equals(_accessType) 
					|| AccessList.SUBSCRIPTION.equals(_accessType) || AccessList.PATRON_DRIVEN.equals(_accessType)){
				accessType = AccessList.HAS_ACCESS;
			} 
			// always use 'P' icon
//			else if(AccessList.SUBSCRIPTION.equals(_accessType) || AccessList.PATRON_DRIVEN.equals(_accessType)){
//				accessType = AccessList.SUBSCRIPTION;
//			}			
		}
		return accessType;
	}
	
	
	
	private EBooksAccessListView getAccessType(String eisbn, HttpSession session) {
		EBooksAccessListView view = null;
		
		AccessList list = new AccessList(accessibleEisbns);
		view = list.getAccessType(eisbn);
		
		if(null == view) {
			view = new EBooksAccessListView();
			boolean accessible = isFreeTrial(session, eisbn);		
			if(accessible){			
				view.setFreeTrial(true);
			}
		}
		
//		boolean accessible = isFreeTrial(session, eisbn);		
//		if(accessible){			
//			view.setFreeTrial(true);
//		}
//		
//		if (!accessible) {
//			AccessList list = new AccessList(accessibleEisbns);
//			view = list.getAccessType(eisbn);
//		}
		return view;
	}
	
//	public String getAccessType(String isbn, HttpServletRequest request, boolean includeCheckout) {
//		StringBuilder accessType = new StringBuilder(isEisbnAccessibleDisplay(isbn, request.getSession()));		
//		EBooksAccessListView ealv = getAccessType(isbn, request.getSession());
//		
//		// check for concurrent subscription	
//		if(isConcurrentSubscription(ealv)) {					
//			Book book = new Book(ealv.getBodyId(), isbn, request.getSession().getId(), ConcurrencyListener.getServerIp(), ealv.getAccessLimit());
//			boolean isSuccess = false;
//			if(includeCheckout) {
//				isSuccess = ConcurrencyWorker.checkoutBook(book);
//				if(isSuccess) {
//					accessType.append(":").append(AccessList.CONCURRENCY_ACTIVE); // checkout accepted
//					request.getSession().setAttribute(Book.SESSION_ID, book); // save in session currently checked-out
//				} else {
//					accessType.append(":").append(AccessList.CONCURRENCY_INVALID); // checkout invalid
//				}
//			} else {
//				isSuccess = ConcurrencyWorker.isBookValidForConcurrency(book);
//				if(isSuccess) {
//					accessType.append(":").append(AccessList.CONCURRENCY_INACTIVE); // book already checked-out					
//				} else {
//					accessType.append(":").append(AccessList.CONCURRENCY_INVALID); // checkout invalid
//				}
//			}			
//		} 
//		
//		return accessType.toString();
//	}
	
	
	@SuppressWarnings("unchecked")
	public EBooksAccessListView getAccessType(String isbn, HttpServletRequest request, boolean includeBookLocking) {
		//String accessType = isEisbnAccessibleDisplay(isbn, request.getSession());
//		StringBuilder accessType = new StringBuilder(isEisbnAccessibleDisplay(isbn, request.getSession()));		
		EBooksAccessListView ealv = getAccessType(isbn, request.getSession());
		
		//String accessType = ealv.getOrderType();
		
		// check for concurrent subscription	
		if(ealv.hasConcurrency()) {		
						
			//accessType = AccessList.CONCURRENCY_VALID;
			ealv.setHasBookSlots(false);
			
			boolean isSuccess = false;
			if(includeBookLocking) {
				ealv.setCheckForConcurrency(true);				
				Book book = new Book(ealv.getOrderId(), isbn, request.getSession().getId(), ConcurrencyListener.getServerIp(), ealv.getAccessLimit());
				isSuccess = ConcurrencyWorker.lockBook(book, request);				
				if(isSuccess) {
					ealv.setHasBookSlots(true);
					Map<String, Book> bookCheckoutMap = (Map<String, Book>)request.getSession().getAttribute(Book.SESSION_ID);
					if(null == bookCheckoutMap) {
						bookCheckoutMap = new HashMap<String, Book>();
					} 			
					bookCheckoutMap.put(isbn, book);
					request.getSession().setAttribute(Book.SESSION_ID, bookCheckoutMap); // save in session currently checked-out
				} else {
					//accessType = AccessList.CONCURRENCY_INVALID;
					ealv.setHasBookSlots(false);
				}
			}	
		} 
		
		return ealv;
	}
	
	
//	public static boolean hasAccess(String accessType) {
//		boolean hasAccess = false;
//		String[] s = accessType.split(":");
//		
//		for(String subscriptions : SUBSCRIPTIONS) {
//			if(s.length > 1) {
//				if(subscriptions.equals(s[0]) && 
//						(AccessList.CONCURRENCY_ACTIVE.equals(s[1]) 
//								|| AccessList.CONCURRENCY_INACTIVE.equals(s[1]))){
//					hasAccess = true;
//					break;
//				}
//			} else {
//				if(subscriptions.equals(s[0])){
//					hasAccess = true;
//					break;
//				}
//			}
//		}
//		
//		return hasAccess;
//	}
	
	public static boolean hasAccess(String contentType, String accessType) {
		boolean hasAccess = false;		
		
		if(isFreeContent(contentType)) {
			hasAccess = true;
		} else {		
			for(String subscriptions : AccessList.ACCESS_TYPES) { 			
				if(subscriptions.equals(accessType)){
					hasAccess = true;
					break;
				}
			}
		}
		
		return hasAccess;
	}

	public static boolean isFreeTrial(HttpSession session, String isbn) {
		if(FreeTrialBean.hasFreeTrial(session) == false){
			return false;
		}
		
		for(String _isbn : FreeTrialBean.getFreeTrialExcuded()){
			if(_isbn.equals(isbn)){
				return false;
			}
		}
		return true;
		
	}
	

	
	//orig
	@SuppressWarnings("unused")
	private static String getQuery(String bodyIds) {
		String sql =		
			"SELECT rownum ACCESS_LIST_ID, BODY_ID, EISBN " +  
			"FROM  oraebooks.ebooks_access_list_matview  " +
			"where BODY_ID in ("+ bodyIds +") " +
			  "or ORGANISATION_ID in("+ bodyIds +") ";
		LOGGER.info(sql);
		return sql;
	}
	
	private static String getQueryDemoPerp(String bodyIds, String directBodyIds) {
//		SELECT ROWNUM ACCESS_LIST_ID,
//		BODY_ID,
//		EISBN,
//		ORDER_ID,
//		STATUS,
//		ORDER_TYPE,
//		access_limit
//		  from (
//		    select body_id,
//		    EISBN,
//		    ORDER_ID,
//		    STATUS,
//		    ORDER_TYPE,
//		    access_limit
//		    from oraebooks.ebooks_access_list_final
//		    where (body_id in (119931046) and order_type = 'P' and status = 4) or (body_id in (119931046) and (status = 0 or order_type in ('S','M')))
//		  );
		
		
//		StringBuffer sb = new StringBuffer();
//		sb.append("select rownum ACCESS_LIST_ID, BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
//		sb.append("from ( ");
//		sb.append("SELECT BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
//		sb.append("FROM oraebooks.ebooks_access_list_final ");
//		sb.append("where ");
//		if(StringUtils.isNotEmpty(directBodyIds)){   //direct body id is for orgs and access agreement
//			sb.append("(body_id in (").append(directBodyIds).append(") and order_type = 'P') or ");
//		}
//		sb.append("(body_id in (").append(bodyIds).append(") and (status = 0 or order_type in (").append(SHARED_ORDER_TYPE_ACCESS).append("))))"); //pda line
		
		StringBuffer sb = new StringBuffer();
		sb.append("select rownum ACCESS_LIST_ID, BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
		sb.append("from ( ");
		sb.append("SELECT BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
		sb.append("FROM oraebooks.ebooks_access_list_final ");
		sb.append("where ");		
		sb.append("body_id in (").append(directBodyIds).append("))");		
			
		
		LOGGER.info( sb.toString());
		return sb.toString();
	}
	
	private static String getSqlBooksNotAvailableForSale(String bodyIds, String directBodyIds) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("select distinct a.eisbn ")
		  .append("from oraebooks.ebooks_access_list_final a, oraebooks.ebook_isbn_data_load e ")
		  .append("where a.eisbn = e.isbn and e.sales_module_flag = 'N' ")
		  .append("and ((a.body_id in (").append(directBodyIds).append(") and a.order_type = 'P') ")
          .append("or (a.body_id in (").append(bodyIds).append(") and a.order_type in (").append(NOT_AVAIL_FOR_SALE_ORDER_TYPE).append(")))");
            
        return sb.toString();
	}
			
	private static String getIsbnAccessQueryDemoPerp(String bodyIds, String directBodyIds) {
//		SELECT ROWNUM ACCESS_LIST_ID,
//		BODY_ID,
//		EISBN,
//		ORDER_ID,
//		STATUS,
//		ORDER_TYPE,
//		access_limit
//		  from (
//		    select body_id,
//		    EISBN,
//		    ORDER_ID,
//		    STATUS,
//		    ORDER_TYPE,
//		    access_limit
//		    from oraebooks.ebooks_access_list_final
//		    where (body_id in (119931046) and order_type = 'P' and status = 4) or (body_id in (119931046) and (status = 0 or order_type in ('S','M')))
//		  );
		
		
//		StringBuffer sb = new StringBuffer();
//		sb.append("select rownum ACCESS_LIST_ID, BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
//		sb.append("from ( ");
//		sb.append("SELECT BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
//		sb.append("FROM oraebooks.ebooks_access_list_final ");
//		sb.append("where ");
//		if(StringUtils.isNotEmpty(directBodyIds)){   //direct body id is for orgs and access agreement
//			sb.append("(body_id in (").append(directBodyIds).append(") and order_type = 'P' and and EISBN = ?1) or ");
//		}
//		sb.append("(body_id in (").append(bodyIds).append(") and (status = 0 or order_type in (").append(SHARED_ORDER_TYPE_ACCESS).append(")) and EISBN = ?1))"); //pda line
		
		StringBuffer sb = new StringBuffer();
		sb.append("select rownum ACCESS_LIST_ID, BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
		sb.append("from ( ");
		sb.append("SELECT BODY_ID, EISBN, ORDER_ID, STATUS, ORDER_TYPE, ACCESS_LIMIT ");
		sb.append("FROM oraebooks.ebooks_access_list_final ");
		sb.append("where ");
		sb.append("body_id in (").append(directBodyIds).append(") and EISBN = ?1)");		
		
		LOGGER.info(sb.toString());
		return sb.toString();			
	}
	
	
	//orig
	public static String getIsbnAccessQuery(String bodyIds, String isbn) {
		String sql = 
			"SELECT count(1) ACCESS_LIST_ID " +
			"FROM  oraebooks.ebooks_access_list_matview " +
			"where (BODY_ID in ("+ bodyIds +") or ORGANISATION_ID in ("+ bodyIds +")) " +
			  "and EISBN = ?1";
		return sql;			
	}
	
	/**
	 * checks if the user has what type of access: free, has access, no access
	 * @param doc
	 * @param session
	 * @param subscription
	 * @return
	 */
	@Deprecated
	public static String hasAccessDisplay(String contentType, String isbn, HttpSession session, SubscriptionUtil subscription ){			
		if(isFreeContent(contentType)){
			return AccessList.FREE_ACCESS;
		}else{			
			return subscription.isEisbnAccessibleDisplay(isbn, session);
		}
	}
	
	
	/**
	 * creates new subscription on every request, used for opening pdf
	 * @param bodyIds
	 * @param isbn
	 * @param contentType
	 * @param request
	 * @return
	 */	
//	@Deprecated
//	public static EBooksAccessListView hasAccess(String bodyIds, String directBodyIds, String isbn, String contentType, HttpServletRequest request){
//		EntityManager em = emf.createEntityManager();
//		LOGGER.info( 
//				"pdf access check ---> bodyIds:("+ bodyIds +") isbn:" +isbn+ " contentType:" + contentType );
//		EBooksAccessListView view = new EBooksAccessListView();		
//		try {
//			if(isFreeContent(contentType)){				
//				view.setFreeContent(true);
//			}else{			
//				SubscriptionUtil s = new SubscriptionUtil(bodyIds, directBodyIds, isbn);
//				view = s.getAccessType(isbn, request.getSession());
//			}
//		} catch (Exception e) {
//			LOGGER.error( e.getMessage());
//		} finally {
//			em.close();
//		}
//		return view;
//	}
	
	public static boolean isFreeContent(String contentType){
		boolean isFreeContent = false;		
		if(StringUtils.isNotEmpty(contentType)) {
			for(String content : FREE_CONTENT ){
				if(content.equals(contentType)){
					isFreeContent = true;
					break;
				}
			}
		}
		return isFreeContent;
	}
	
	public List<EBooksAccessListView> getAccessibleEisbns() {
		return accessibleEisbns;
	}
	
	public static String getBooksNotAvailableForSale(String bodyIds, String directBodyIds) {
		StringBuilder isbns = new StringBuilder();
		
		if(StringUtils.isNotEmpty(bodyIds) && StringUtils.isNotEmpty(directBodyIds)) {		
			EntityManager em = emf.createEntityManager();		
			Query q;
			try {
				q = em.createNativeQuery(getSqlBooksNotAvailableForSale(bodyIds, directBodyIds));
				List<String> results = q.getResultList();
				for(String isbn : results) {
					isbns.append(isbn).append(" ");
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			} finally {
				em.close();
			}
		}
		
		return isbns.toString().trim();
	}
}
