package org.cambridge.ebooks.online.subscription;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.subscription.Book;
import org.cambridge.ebooks.online.jpa.subscription.BookCollection;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

public class SubscriptionServlet extends HttpServlet{
	
	private static final long serialVersionUID = 4313278214395195411L;
	
	private static final Logger logger = Logger.getLogger(SubscriptionServlet.class);	
	
	public static final String ACTION = "ACTION";
	
	public static final String INDI = "INDI";
	
	public static final String COLL = "COLL";
	
	public static final String ALL = "ALL";
	
	public static final String INDI_ORG = "INDI_ORG";
	
	public static final String COLL_ORG = "COLL_ORG";
	
	public static final String INDI_DEMO = "INDI_DEMO";
	
	public static final String COLL_DEMO = "COLL_DEMO";
	
	public static final String FIND_BOOKS = "FIND_BOOKS";
	
	public static final String ORG_ID = "ORG_ID";
	
	public static final String TRIAL = "TRIAL";
	
	public static final String COLLECTION_ID = "COLLECTION_ID";
	
	public static final String SELECTED_ORG = "SELECTED_ORG";
	
	public static final String CONTENT_TYPE = "plain/text";
	
	public static final String EMPTY = "<<<Empty>>>";
	
	@SuppressWarnings("unused")
	private static final String[] COLL_ORG_KEYS = new String[]{"collectionId","title","description"};
	
	private static final String[] FIND_BOOK_KEYS = new String[]{"bookId","isbn","title"};	
	
	public static final String PERPETUAL = "P";
	
	public static final String DEMO = "D";
	
	public static final String LESS_THAN = "<<<";
	
	public static final String GREATER_THAN = ">>>";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter(ACTION);
 		String sort = request.getParameter("sortBy");
		String allBodyId = getAllOrgId(request, action);
		String directBodyId = getDirectOrgId(request, action);
		String path = "/access_details.jsf";

			if(FIND_BOOKS.equals(action)){
				logger.info("=== BEGIN FIND BOOKS ===");
				
				String collectionId = request.getParameter(COLLECTION_ID);
				List<Book> list = Book.getBooksUnderCollection(collectionId);
				//String output = parseItemsList(list, FIND_BOOK_KEYS);
				String output = parseItemsList(sortByTitle(sortByNumber(list, sort), sort), FIND_BOOK_KEYS);
			
				//output = "CBO9780511510014<<<9780511510014<<<Clan Politics and Regime Transition in Central Asia>>>CBO9780511511967<<<9780511511967<<<Ancient China and Its Enemies>>>CBO9780511552267<<<9780511552267<<<Achieving Industrialization in East Asia>>>";
				//output = "{CBO9780511510014,9780511510014,Clan Politics and Regime Transition in Central Asia},{CBO9780511511967,9780511511967,Ancient China and Its Enemies},{CBO9780511552267,9780511552267,Achieving Industrialization in East Asia}";
				
				String aaa_import = request.getParameter("aaa_import");

				if(aaa_import != null && StringUtils.isNotEmpty(output) ){
					logger.info("aaa_import: "+aaa_import);
					logger.info("replace spaces...");
					
					output = output.trim();
					output = output.replaceAll("<<<", ",");
					output = output.replaceAll(">>>", ";");
					
					if(output.charAt(output.length()-1) == ';' ){
						output = output.substring(0,output.length()-1);
					}

				}
				
				if(list == null){
					logger.info("list is null, collectionid: "+collectionId);
				}else{
					logger.info("   collection_id:\t" + collectionId);
					logger.info("   listsize:\t" + list.size());
					logger.info("   output:\t" + output);
				}
				
				logger.info("=== END FIND BOOKS ===");
				
				logger.info("(sending response...)");
				response.setContentType(CONTENT_TYPE);
				Writer writer = response.getWriter();
				writer.write(output);
				writer.close();
				logger.info("(response sent)");
				
				
			}else if(ALL.equals(action)){
				if(StringUtils.isNotEmpty(allBodyId)){
					
					//make indi
					List<Book> list_all_indi = Book.getBooks(directBodyId, allBodyId);	
//					list_all_indi = removeCBOFromId(list_all_indi);
					list_all_indi = sortByTitle(sortByNumber(list_all_indi, sort), sort);
					
					request.setAttribute("indi_servlet", list_all_indi);
					
					//make a set COLL					
					List<BookCollection> list_all_coll = BookCollection.getCollection(directBodyId, allBodyId);					
					request.setAttribute("coll_servlet", list_all_coll);
				}				
				
				//request.getRequestDispatcher("../access_details.jsf").forward(request, response);
				forwardToUrl(request, response, path);
			}
	}
	
//	private List<Book> removeCBOFromId(List<Book> list){
//		for(int i = 0; i < list.size(); i++){
//			Book p1 = list.get(i);
//			p1.setBookId(p1.getBookId().replaceFirst("CBO",""));
//			list.set(i, p1);
//		}
//		
//		return list;
//	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Book> sortByNumber(List<Book> list, final String finalSort){
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;
					
					
						if(Character.isDigit(p1.getTitle().charAt(0)) && !Character.isDigit(p2.getTitle().charAt(0)))
							return -1;
						else if(!Character.isDigit(p1.getTitle().charAt(0)) && Character.isDigit(p2.getTitle().charAt(0)))
							return 1;
						else{
							return p1.getTitle().compareToIgnoreCase(p2.getTitle());				
						}
				}
			});	
		}else{
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;
					
					
						if(null != p1 && StringUtils.isNotEmpty(p1.getIsbn()) && Character.isDigit(p1.getIsbn().charAt(0)) && !Character.isDigit(p2.getIsbn().charAt(0)))
							return -1;
						else if(!Character.isDigit(p1.getIsbn().charAt(0)) && Character.isDigit(p2.getIsbn().charAt(0)))
							return 1;
						else{
							return p1.getIsbn().compareToIgnoreCase(p2.getIsbn());				
						}
				}
			});	
			
		}
		return list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Book> sortByTitle(List<Book> list, String finalSort){	
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;
					
					
					if(p1.getTitle().startsWith("\"") && !p2.getTitle().startsWith("\""))
						return -1;
					else if(!p1.getTitle().startsWith("\"") && p2.getTitle().startsWith("\""))
						return 1;
					else
						if(p1.getTitle().startsWith("The"))
							return p1.getTitle().substring(4).compareToIgnoreCase(p2.getTitle());
						else if(p2.getTitle().startsWith("The"))
							return p1.getTitle().compareToIgnoreCase(p2.getTitle().substring(4));
						else 
							return p1.getTitle().compareToIgnoreCase(p2.getTitle());	
				}
			});	
		}else{
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;
					
					
					if(p1.getIsbn().startsWith("\"") && !p2.getIsbn().startsWith("\""))
						return -1;
					else if(!p1.getIsbn().startsWith("\"") && p2.getIsbn().startsWith("\""))
						return 1;
					else
						if(p1.getIsbn().startsWith("The"))
							return p1.getIsbn().substring(4).compareToIgnoreCase(p2.getIsbn());
						else if(p2.getIsbn().startsWith("The"))
							return p1.getIsbn().compareToIgnoreCase(p2.getIsbn().substring(4));
						else 
							return p1.getIsbn().compareToIgnoreCase(p2.getIsbn());	
				}
			});	
		}
		return list;
	}
            
	protected void forwardToUrl(HttpServletRequest req, HttpServletResponse resp, String path){
		RequestDispatcher dispatcher = null;
		try {
			dispatcher = req.getRequestDispatcher(path);
			dispatcher.forward(req, resp);
		} catch (Exception e) {
			logger.error( "Error" + e.getMessage() );
		}
	}
		
	@SuppressWarnings("unchecked")
	private String parseItemsList(Collection _list, String[] keys) throws ServletException{
		
		List<Object> list = null;
		if(_list instanceof List){
			list = (List<Object>) _list;
		}else{
			throw new ServletException("type not a list");
		}
		
		if(list.size()==0){
			return EMPTY;
		}
		
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			Object object = list.get(i);
			//sb.append(ebook.getBookId() + LESS_THAN + ebook.getEisbn() + LESS_THAN + ebook.getTitle() + GREATER_THAN);			
			for (int j = 0; j < keys.length; j++) {
				String prop = "";
				try {
					prop = BeanUtils.getProperty(object, keys[j]);
				} catch (Exception e) {					
					e.printStackTrace();
					prop = "null";
				}
				sb.append(prop);
				if(j != keys.length - 1){
					sb.append(LESS_THAN);
				}				
			}
			sb.append(GREATER_THAN);
		}
		return sb.toString();
		
	}
	
	private String getDirectOrgId(HttpServletRequest request, String action){
		String bodyId = request.getParameter(SELECTED_ORG);
		//undefined is from javascript
		if("0".equals(bodyId) || bodyId == null || StringUtils.isEmpty(bodyId)
				|| "undefined".equals(bodyId)){
			HttpSession session = request.getSession();
			
			//return only direct bodyids if organisation
			return OrgConLinkedMap.getDirectBodyIds(session);			
		}else{
			return bodyId;
		}
	}
	
	private String getAllOrgId(HttpServletRequest request, String action){
		String bodyId = request.getParameter(SELECTED_ORG);
		//undefined is from javascript
		if("0".equals(bodyId) || bodyId == null || StringUtils.isEmpty(bodyId)
				|| "undefined".equals(bodyId)){
			HttpSession session = request.getSession();			
			return OrgConLinkedMap.getAllBodyIds(session);
		}else{
			return bodyId;
		}
	}
}
