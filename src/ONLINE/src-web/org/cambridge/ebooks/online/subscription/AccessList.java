package org.cambridge.ebooks.online.subscription;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;

public class AccessList extends ArrayList<EBooksAccessListView>{
	
	private static final Logger LOGGER = Logger.getLogger(AccessList.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -584836078274007757L;
	
	public static final String[] ACCESS_TYPES = new String[] {
		AccessList.FREE_ACCESS, 
		AccessList.FREE_TRIAL, 
		AccessList.PATRON_DRIVEN, 
		AccessList.SUBSCRIPTION, 
		AccessList.HAS_ACCESS, 
		AccessList.DEMO
		//AccessList.CONCURRENCY_VALID,
		//AccessList.CONCURRENCY_INVALID
	};
	
	public static final String NO_ACCESS = "0";
	public static final String HAS_ACCESS = "P"; 
	public static final String SUBSCRIPTION = "S";
	public static final String FREE_ACCESS = "F"; 
	public static final String PATRON_DRIVEN = "M";
	public static final String FREE_TRIAL = "R";
	public static final String DEMO = "D";
	public static final String LIMITED_TRIAL = "L";
//	public static final String CONCURRENCY_ACTIVE = "CA";
//	public static final String CONCURRENCY_INACTIVE = "CI";
//	public static final String CONCURRENCY_INVALID = "CN";
	public static final String CONCURRENCY_VALID = "CV";
	public static final String CONCURRENCY_INVALID = "CI";
	
	public AccessList(List<EBooksAccessListView> list){
		this.addAll(list);
	}
	
	public EBooksAccessListView getAccessType(String eisbn){
		EBooksAccessListView view = null;
		boolean accessible = false;
		for (EBooksAccessListView e : this) {
			accessible = e.getEisbn().equals(eisbn);
			if (accessible) { 
				if(e.hasConcurrency()) {
					LOGGER.info("order id:" + e.getOrderId());
					LOGGER.info("access limit:" + e.getAccessLimit());
					if(view.getAccessLimit() < e.getAccessLimit()) {
						view = e;
					}					
				} else {
					if(HAS_ACCESS.equals(e.getOrderType())){
						view = e;
						break;
					}
					if(DEMO.equals(e.getOrderType())){
						view = e;
						break;
					}
					if(LIMITED_TRIAL.equals(e.getOrderType())) {
						view = e;
						break;
					}
					if(SUBSCRIPTION.equals(e.getOrderType())){						
						view = e;
						//cannot exit yet, because when both have S and P access priority should be P, should not exit yet
					}	
					if(PATRON_DRIVEN.equals(e.getOrderType()) && !SUBSCRIPTION.equals(view.getOrderType())){
						view = e;				
						//cannot exit yet, because when both have T and P access priority should be P, should not exit yet
					}
				}
			}
		}		
		return view; 
	}
	
	public String getAccessTypeString(String eisbn){
		EBooksAccessListView view = getAccessType(eisbn);
		return view.getOrderType();
	}
}
