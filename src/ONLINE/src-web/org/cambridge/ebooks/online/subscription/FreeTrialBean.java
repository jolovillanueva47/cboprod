package org.cambridge.ebooks.online.subscription;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class FreeTrialBean {
	public static final String FREE_TRIAL_BEAN = "FREE_TRIAL_BEAN";
	
	
	private boolean freeTrialChecked;
	private boolean freeTrial;
	
	private static ArrayList<String> freeTrialExcuded;
	private static Date freeLastChecked;

	public boolean isFreeTrialChecked() {
		return freeTrialChecked;
	}

	public void setFreeTrialChecked(boolean freeTrialChecked) {
		this.freeTrialChecked = freeTrialChecked;
	}

	public boolean isFreeTrial() {
		return freeTrial;
	}

	public void setFreeTrial(boolean freeTrial) {
		this.freeTrial = freeTrial;
	}

	public static ArrayList<String> getFreeTrialExcuded() {
		return freeTrialExcuded;
	}

	public static void setFreeTrialExcuded(ArrayList<String> freeTrialExcuded) {
		FreeTrialBean.freeTrialExcuded = freeTrialExcuded;
	}
	
	public static void initFreeTrial(){		
		Date today = new Date();
		Date lastChecked = freeLastChecked;
		
		if(freeLastChecked == null){
			getAllExcludedBooks();
			return;
		}
		
		Calendar calLast = Calendar.getInstance();
		calLast.setTime(lastChecked);
		
		Calendar calToday = Calendar.getInstance();
		calToday.setTime(today);
		
		if(calLast.get(Calendar.DAY_OF_MONTH) != calToday.get(Calendar.DAY_OF_MONTH)){
			getAllExcludedBooks();
		}		
	}
	
				
	
	@SuppressWarnings("unchecked")
	private static void getAllExcludedBooks(){
		EntityManagerFactory emf = PersistenceUtil.emf;
		EntityManager em = emf.createEntityManager();
		try {		
			String sql = 
				"select rownum ACCESS_LIST_ID, ISBN EISBN " + 
				"from ebook_isbn_data_load " + 
				"where SALES_MODULE_FLAG = 'N' ";
			
			Query query = em.createNativeQuery(sql, EBooksAccessListView.class);
			List<EBooksAccessListView> list = query.getResultList();
			ArrayList<String> _list = new ArrayList<String>();
			for(EBooksAccessListView book : list){
				_list.add(book.getEisbn());
			}
			
			freeTrialExcuded = _list;			
			freeLastChecked = new Date();
		} catch (Exception e) {
			em.close();
						
		}
	}
	
	
	public static FreeTrialBean getFromSession(HttpSession session){
		Object ftb = (Object) session.getAttribute(FREE_TRIAL_BEAN);
		if(ftb == null){
			ftb = new FreeTrialBean();
			session.setAttribute(FREE_TRIAL_BEAN, ftb);
		}
		
		return (FreeTrialBean) ftb;
	}
	
	public static boolean hasFreeTrial(HttpSession session){
		FreeTrialBean ftb = getFromSession(session);
		return ftb.isFreeTrial();
	}
	
	public static void recheckFreeTrial(HttpSession session){
		FreeTrialBean ftb = getFromSession(session);
		ftb.setFreeTrialChecked(false);
	}
	
}
