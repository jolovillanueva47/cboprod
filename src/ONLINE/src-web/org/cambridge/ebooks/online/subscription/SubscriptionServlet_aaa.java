package org.cambridge.ebooks.online.subscription;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.online.accessibility.Accessible;

public class SubscriptionServlet_aaa extends SubscriptionServlet implements Accessible {
	
	private static final long serialVersionUID = 1L;
	
//	@Override
//	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		doPost(request, response);
//	}
//	@Override
//	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		super.doPost(request, response);
//	}
	
	@Override
	protected void forwardToUrl(HttpServletRequest req, HttpServletResponse resp, String path){
		
		String newPath = accUrlFormatter(path);
		super.forwardToUrl(req, resp, newPath);
	}
	
	public String accUrlFormatter(String path){
		String accPath = ACC_URL_IDENTIFIER + path;
//		String token = "../";
//		if( null != path && path.contains("../") ){
//			accPath = path.substring(  0, (path.lastIndexOf("../")+token.length()) ) +
//					ACC_URL_IDENTIFIER +  
//					path.substring( (path.lastIndexOf("../")+token.length()), path.length())  ;
//		}
		return accPath;
	}
}
