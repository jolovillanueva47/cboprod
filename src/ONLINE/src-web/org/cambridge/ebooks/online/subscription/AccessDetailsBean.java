package org.cambridge.ebooks.online.subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.util.HttpUtil;

public class AccessDetailsBean {
		
	private static Logger LOGGER = Logger.getLogger(AccessDetailsBean.class);
	private static final String DOMAIN_CBO = System.getProperty("cbo.domain");
	
	private List<Organisation> orgList;
	
	private String selectedOrg;
	
	public static final String ORG_MAP = "organisationMap";
			
	public static final String ALL_ORGS = "All Organisations"; 
	
	public static final String ALL_ORG_VALUE = "0";
	
	public static final String SELECTED_ORG = "SELECTED_ORG";
	
	
	public String getInitBean(){
		LOGGER.info("Initialize...");		
		HttpServletRequest req = HttpUtil.getHttpServletRequest();				
		setRequestAttributes(req);
		
		HttpSession session = HttpUtil.getHttpSession(false);		
		//System.out.println("hello i came first");
		/*
		 * ORG_IDS and ORG_MAP are taken from OrganisationWorker.setOrganisationDisplayInSession		
		 */
		
		//get org ids
		Set<String> orgIds = OrgConLinkedMap.getAllBodyIdsAsSet(session);
		if(orgIds == null || orgIds.size() == 0){
			orgList = null;
			return "";
		}
		
		OrgConLinkedMap map = OrgConLinkedMap.getFromSession(session);
		
		//fills up selected orgs
		List<Organisation> orgList = new ArrayList<Organisation>();		
		addDefaultOrg(orgList);		
		addOrgsInSession(map, orgList);
		
		this.orgList = orgList;
		
		//sets the selected org
		setSelectedOrg();
		LOGGER.info("Finished...");	
		
		return "";
	}

	public List<Organisation> getOrgList() {
		return orgList;	
	}

	public String getSelectedOrg() {
		return selectedOrg;
	}
	
	
	private void addDefaultOrg(List<Organisation> orgList){
		//add default org -- all orgs
		Organisation org = new Organisation();
		org.setBodyId(ALL_ORG_VALUE);
		org.setName(ALL_ORGS);
		orgList.add(org);
	}
	
	private void addOrgsInSession(OrgConLinkedMap map, List<Organisation> orgList){
		for(String id : map.getMap().keySet()){
			Organisation org = map.getMap().get(id);		
			Organisation _org = new Organisation();
			_org.setBodyId(org.getBodyId());
			_org.setName(org.getBaseProperty(Organisation.DISPLAY_NAME));			
			orgList.add(_org);
		}		
	}
	
	private void setSelectedOrg(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String id = request.getParameter(SELECTED_ORG);
		this.selectedOrg = id; 
	}
	
	public boolean getFreeTrial() {
		if(hasFreeTrial() != null){
			return true;
		}else{
			return false;
		}
	}
	
	private String hasFreeTrial() {
		String selectedOrg = this.selectedOrg;
		LOGGER.info("Free Trial selected org : " + selectedOrg);
		HttpSession session = HttpUtil.getHttpSession(false);
		if(selectedOrg == null || selectedOrg.equals(ALL_ORG_VALUE)){
			selectedOrg = OrgConLinkedMap.getAllBodyIds(session);
			return OrganisationWorker.checkFreeTrial(selectedOrg);				
		}else{
			return OrganisationWorker.checkFreeTrial(selectedOrg);	
		}		
	}
	
	private void setRequestAttributes(HttpServletRequest req) {
		LOGGER.info("Setting request attributes...");
		req.setAttribute("domainCbo", DOMAIN_CBO);
	}
	
	
	
	
}
