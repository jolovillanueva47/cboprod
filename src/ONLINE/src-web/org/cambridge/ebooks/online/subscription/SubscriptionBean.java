package org.cambridge.ebooks.online.subscription;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.util.HttpUtil;

public class SubscriptionBean {
	private static final Logger LOGGER = Logger.getLogger(SubscriptionBean.class);
	
	private boolean noActiveSubscriptions;
	
	@SuppressWarnings("unused")
	private boolean initSubscription;
	
	public SubscriptionBean() {
		
	}
	
	public static final String HAS_NO_ACTIVE_SUBS = "HAS_NO_ACTIVE_SUBS";
	
	public static final String HAS_ACTIVE_SUBS = "HAS_ACTIVE_SUBS";
	               
	public String getInitSubscription(){		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		HttpSession session = request.getSession();
		
		/*
		 * this will prevent the method from querying into the database again
		 * checks the session
		 */
		String noActiveSubs = (String) session.getAttribute(HAS_NO_ACTIVE_SUBS);
		
		LOGGER.info("noactivesubs = " + noActiveSubs);
		
		if(noActiveSubs != null && HAS_NO_ACTIVE_SUBS.equals(noActiveSubs)){
			noActiveSubscriptions = true;
			return "";
		}
		
		if(noActiveSubs != null && HAS_ACTIVE_SUBS.equals(noActiveSubs)){
			noActiveSubscriptions = false;
			return "";
		}
		
		setSubscription(request);		
	
		return "";
	}
	
	
	public void setSubscription(HttpServletRequest request){
		HttpSession session = request.getSession();
		String bodyIds = getBodyIds(request);		
		LOGGER.info("bodyids = " + bodyIds);
		if(StringUtils.isNotEmpty(bodyIds)){
			noActiveSubscriptions = OrganisationWorker.hasNoActiveSubscription(bodyIds);
		}else{
			noActiveSubscriptions = true;
		}
		
		LOGGER.info("noActiveSubs2 = " + noActiveSubscriptions);
	
		if(noActiveSubscriptions){
			session.setAttribute(HAS_NO_ACTIVE_SUBS, HAS_NO_ACTIVE_SUBS);
		}else{
			session.setAttribute(HAS_NO_ACTIVE_SUBS, HAS_ACTIVE_SUBS);
		}
	}
	
	private static final String USER = "userInfo";
		
	private String getBodyIds(HttpServletRequest request){
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(USER);
		if(user != null && StringUtils.isNotEmpty(user.getUsername())){
			return (String) OrgConLinkedMap.getFromSession(session).getCurrentlyLoggedBodyId();
		}else{
			return (String) OrgConLinkedMap.getAllBodyIds(session); 
		}
	}
	
	public boolean isNoActiveSubscriptions() {
		return noActiveSubscriptions;
	}

}
