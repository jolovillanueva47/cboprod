package org.cambridge.ebooks.online.society.autologin;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.filter.SocietyAutoLoginFilter;
import org.cambridge.ebooks.online.internal_reports.InternalReportsBean;
import org.cambridge.ebooks.online.internal_reports.InternalReportsWorker;
import org.cambridge.ebooks.online.jpa.society.autologin.AutoLoginCBO;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.login.authentication.OrgConLoginWorker;
import org.cambridge.ebooks.online.util.PersistenceUtil;
/**
 * 
 * @author mmanalo
 *
 */
public class SocietyAutoLoginWorker {
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	private AutoLoginCBO autoLoginCBO;	
	
	public static final String DUP_REFERRER = "dupReferrer";
	
	public static final String addAutlogin = "addAutlogin";
	
	private static Logger LOGGER = Logger.getLogger(SocietyAutoLoginWorker.class);
	
	public SocietyAutoLoginWorker(String loginId){
		populateAutoLoginCBO(loginId);
	}
	
	
	/* public methods */
	
	public boolean isValidReferrer(HttpServletRequest request){
		String referrer = getReferrer(request);
		LOGGER.info("referrer = " + referrer);
		if(StringUtils.isEmpty(referrer)){
			return false;
		}
		
		String loginId = request.getParameter(SocietyAutoLoginFilter.AUTOLOGIN_ID);
		boolean isValidReferrer = false;
		if(loginId != null){			
			LOGGER.info("autologin url = " + this.autoLoginCBO.getUrlPath());
			if(referrer.equals(this.autoLoginCBO.getUrlPath())){
				isValidReferrer = true;
			}			
		}		
		return isValidReferrer;		
	}
	
	
	public void loginSociety(HttpServletRequest request, HttpServletResponse response){		
		AutoLoginCBO bean = this.autoLoginCBO;
		
		OrgConLinkedMap orgConMap = OrgConLinkedMap.getFromSession(request);
		
		OrgConLoginWorker orgConWorker = new OrgConLoginWorker(orgConMap.getMap());
		orgConWorker.addOrgs(bean.getSocietyId(), "Y");
		
		orgConMap.setAutologinId(bean.getLoginId());
		orgConMap.setCurrentlyLoggedBodyId(bean.getSocietyId());
		
		//internal reports worker
		request.setAttribute(addAutlogin, addAutlogin); //this attribute
		InternalReportsWorker.listenEvent(request, response);
		//though not necessary, as this line is already added inside internal reports, just want to emphasize that it is necessary to remove this
		request.setAttribute(SocietyAutoLoginWorker.addAutlogin, "");
	}
	
	
	/* private methods */
	
	private String getReferrer(HttpServletRequest request){		
		//referrer test dupReferrer param
		String dupReferrer = request.getParameter(DUP_REFERRER);
		String referrer = dupReferrer != null ? dupReferrer : request.getHeader(InternalReportsBean.Referrer);
		
		return referrer;
	}
	
	private AutoLoginCBO populateAutoLoginCBO(String loginId){
		EntityManager em = emf.createEntityManager();
		AutoLoginCBO bean = null;
		try{
			bean = em.find(AutoLoginCBO.class, loginId);			
			LOGGER.info( "found org ? " + bean + "where login id = " + loginId );
			this.autoLoginCBO = bean == null ? new AutoLoginCBO() : bean;
		}catch(Exception e){
			this.autoLoginCBO = new AutoLoginCBO();
		}finally{
			em.close();
		}
		
		return bean;
	}
	
	
	/*Getters and setters*/

	public AutoLoginCBO getAutoLoginCBO() {
		return autoLoginCBO;
	}
	
	
	
		
	
	
	
	
	
	
	
	
	
		
	
}
