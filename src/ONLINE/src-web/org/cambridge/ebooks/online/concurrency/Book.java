package org.cambridge.ebooks.online.concurrency;

/**
 * @author kmulingtapang
 */
public class Book {
	public static final String SESSION_ID = "concurrency_book";
	
	private String orderId;
	private String isbn;
	private String sessionId;
	private String serverIp;
	private int maxAccess;
	
	public Book() {
		this("", "", "", "", 0);
	}
	
	public Book(String orderId, String isbn, String sessionId, String serverIp, int maxAccess) {
		this.orderId = orderId;
		this.isbn = isbn;
		this.sessionId = sessionId;
		this.serverIp = serverIp;
		this.maxAccess = maxAccess;
	}	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getMaxAccess() {
		return maxAccess;
	}
	public void setMaxAccess(int maxAccess) {
		this.maxAccess = maxAccess;
	}
	public String getServerIp() {
		return serverIp;
	}
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}			
}
