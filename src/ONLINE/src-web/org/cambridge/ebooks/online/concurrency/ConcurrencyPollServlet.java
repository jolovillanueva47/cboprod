package org.cambridge.ebooks.online.concurrency;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 */
public class ConcurrencyPollServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(ConcurrencyPollServlet.class);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {		
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		logger.info("===[doPost]");
		String id = (String)req.getParameter("cid");
		logger.info("====id:" + id);
				
		// get map in session
		Map<String, Book> bookCheckoutMap = (Map<String, Book>)req.getSession().getAttribute(Book.SESSION_ID);
		if(null != bookCheckoutMap) {			
			for(String key : bookCheckoutMap.keySet()) {
				if(id.contains(key)) {
					// update book for release
					Book book = bookCheckoutMap.get(key);

					// update book
					ConcurrencyWorker.updateBookAccessTime(book);
										
					break;
				}
			}			
		}
	}
	
}
