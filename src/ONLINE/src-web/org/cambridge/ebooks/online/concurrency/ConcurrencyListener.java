package org.cambridge.ebooks.online.concurrency;

import java.net.InetAddress;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class ConcurrencyListener implements ServletContextListener, HttpSessionListener {

	private static final Logger logger = Logger.getLogger(ConcurrencyListener.class);
	private static String serverIp;
	private static long concurrencyPollTime;
	
	public void contextDestroyed(ServletContextEvent sce) {
	
	}

	public void contextInitialized(ServletContextEvent sce) {
		logger.info("===[contextInitialized]");
		
		try {
			InetAddress inetAdd = InetAddress.getLocalHost();
			serverIp = inetAdd.getHostAddress();
			
			concurrencyPollTime = Long.parseLong(System.getProperty("concurrency.poll.time"));
			logger.info("====concurrency poll time:" + concurrencyPollTime);
		} catch (Exception e) {
			ExceptionPrinter.getStackTraceAsString(e);
		}
	}

	public void sessionCreated(HttpSessionEvent hse) {
		logger.info("===[sessionCreated]");
		logger.info("====session id:" + hse.getSession().getId());
	}

	public void sessionDestroyed(HttpSessionEvent hse) {
		logger.info("===[sessionDestroyed]");
		logger.info("====session id:" + hse.getSession().getId());		
		ConcurrencyWorker.releaseBookBySession(new Book("", "", hse.getSession().getId(), "", 0));
	}

	public static String getServerIp() {
		return serverIp;
	}
	
	public static long getConcurrencyPollTime() {
		return concurrencyPollTime;
	}
}
