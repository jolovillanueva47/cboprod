package org.cambridge.ebooks.online.concurrency;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.concurrency.EbookConcurrency;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil.PersistentUnits;


/**
 * @author kmulingtapang
 */
public class ConcurrencyWorker {
	
	private static final Logger logger = Logger.getLogger(ConcurrencyWorker.class);	
	
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static boolean lockBook(Book book, HttpServletRequest request) {
		boolean isBookLocked = true;
		if(isBookAccessedByThisSession(book)) {
			updateBookAccessTime(book);		
		} else {
			if(isBookAccessible(book, request)) {
				// do insert				
				addBookToConcurrency(book);
			} else {
				isBookLocked = false;
			}		
		}
		return isBookLocked;
	}	
	
	public static boolean isBookValidForConcurrency(Book book, HttpServletRequest request) {
		boolean isBookValidForConcurrency = true;
		if(!isBookAccessedByThisSession(book)) {
			if(!isBookAccessible(book, request)) {
				isBookValidForConcurrency = false;
			}		
		}		
		return isBookValidForConcurrency;
	}	
	
	public static void releaseBook(Book book) {		
		EntityManager em = emf.createEntityManager();
		
		try {
			logger.info("===[releaseBook]DELETE FROM EBOOK_CONCURRENCY");
			logger.info("====orderId:" + book.getOrderId());
			logger.info("====isbn:" + book.getIsbn());
			logger.info("====sessionId:" + book.getSessionId());
			
			Query q = em.createNativeQuery("delete from EBOOK_CONCURRENCY where order_id = ? and isbn = ? and session_id = ?");
			
			q.setParameter(1, book.getOrderId());
			q.setParameter(2, book.getIsbn());
			q.setParameter(3, book.getSessionId());
			
			em.getTransaction().begin();
			int recordCount = q.executeUpdate();
			em.getTransaction().commit();			
			
			logger.info("===[releaseBook]records deleted:" + recordCount);
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}
	
	public static void releaseBookBySession(Book book) {		
		EntityManager em = emf.createEntityManager();
		
		try {
			logger.info("===[releaseBookPerSession]DELETE FROM EBOOK_CONCURRENCY");
			logger.info("====sessionId:" + book.getSessionId());
			
			Query q = em.createNativeQuery("delete from EBOOK_CONCURRENCY where session_id = ?");
			
			q.setParameter(1, book.getSessionId());
			
			em.getTransaction().begin();
			int recordCount = q.executeUpdate();
			em.getTransaction().commit();			
			
			logger.info("===[releaseBook]records deleted:" + recordCount);
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}
	
	public static void releaseBookByServerIp(Book book) {		
		EntityManager em = emf.createEntityManager();
		
		try {
			logger.info("===[releaseBookByServerIp]DELETE FROM EBOOK_CONCURRENCY");
			logger.info("====serverIp:" + book.getServerIp());
			
			Query q = em.createNativeQuery("delete from EBOOK_CONCURRENCY where server_ip = ?");
			
			q.setParameter(1, book.getServerIp());
			
			em.getTransaction().begin();
			int recordCount = q.executeUpdate();
			em.getTransaction().commit();			
			
			logger.info("===[releaseBook]records deleted:" + recordCount);
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}
	
	public static void releaseAllBook() {
		EntityManager em = emf.createEntityManager();
		
		try {
			logger.info("===[releaseAllBook]DELETE FROM EBOOK_CONCURRENCY");
			
			Query q = em.createNativeQuery("delete from EBOOK_CONCURRENCY");
			
			em.getTransaction().begin();
			int recordCount = q.executeUpdate();
			em.getTransaction().commit();			
			
			logger.info("===[releaseAllBook]records deleted:" + recordCount);
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}
	
	public static void updateBookAccessTime(Book book) {
		EntityManager em = emf.createEntityManager();
		
		try {
			Timestamp timestamp = new Timestamp(new Date().getTime());
			
			logger.info("===[updateBookAccessTime]UPDATE EBOOK_CONCURRENCY");
			logger.info("====orderId:" + book.getOrderId());
			logger.info("====isbn:" + book.getIsbn());
			logger.info("====sessionId:" + book.getSessionId());
			logger.info("====timestamp:" + timestamp.toString());
			
			Query q = em.createNativeQuery("update EBOOK_CONCURRENCY set ACCESS_TIME = ?1, MAX_ACCESS = ?2 where isbn = ?3 and order_id = ?4 and session_id = ?5");
			q.setParameter(1, timestamp);
			q.setParameter(2, book.getMaxAccess());
			q.setParameter(3, book.getIsbn());
			q.setParameter(4, book.getOrderId());
			q.setParameter(5, book.getSessionId());
			
			em.getTransaction().begin();	
			logger.info("====updated record count:" + q.executeUpdate());
			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}

	protected static void addBookToConcurrency(Book book) {
		EntityManager em = emf.createEntityManager();
		
		try {
			logger.info("===[addBookToConcurrency]INSERT INTO EBOOK_CONCURRENCY");
			logger.info("====orderId:" + book.getOrderId());
			logger.info("====maxAccess:" + book.getMaxAccess());
			logger.info("====isbn:" + book.getIsbn());
			logger.info("====sessionId:" + book.getSessionId());
			logger.info("====serverIp:" + book.getServerIp());
			
			EbookConcurrency ebookConcurrency = new EbookConcurrency();
			ebookConcurrency.setOrderId(book.getOrderId());
			ebookConcurrency.setMaxAccess(book.getMaxAccess());
			ebookConcurrency.setIsbn(book.getIsbn());
			ebookConcurrency.setSessionId(book.getSessionId());			
			ebookConcurrency.setAccessTime(new Timestamp(new Date().getTime()));
			ebookConcurrency.setServerIp(book.getServerIp());
			
			em.getTransaction().begin();	
			em.persist(ebookConcurrency);
			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
	}
	
	protected static boolean isBookAccessedByThisSession(Book book) {
		boolean isBookAccessedByThisSession = false;
		
		EbookConcurrency result = null;
		EntityManager em = emf.createEntityManager();		
		try {
			Query query = em.createNamedQuery(EbookConcurrency.CHECK_BOOK_ACCESS_BY_THIS_SESSION);
			query.setParameter(1, book.getOrderId());
			query.setParameter(2, book.getIsbn());
			query.setParameter(3, book.getSessionId());
						
			result = (EbookConcurrency)query.getSingleResult();
		} catch(NoResultException e) {
			result = null;
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
		
		if(null != result) {
			isBookAccessedByThisSession = true;
		}
		
		return isBookAccessedByThisSession;
	}	
	
	protected static boolean isBookAccessible(Book book, HttpServletRequest request) {
		boolean isBookAccessible = false;
		
		List <EbookConcurrency> results = null;
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createNamedQuery(EbookConcurrency.CHECK_BOOK_ACCESS);
			query.setParameter(1, book.getOrderId());
			query.setParameter(2, book.getIsbn());
			
			results = query.getResultList();
		} catch(NoResultException e) {
			results = null;
		} catch (Exception e) {
			logger.error("===[Exception]");
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
		
		if(null != results && results.size() > 0) {					
			long concurrencyPollTime = ConcurrencyListener.getConcurrencyPollTime();
			Timestamp timestamp = new Timestamp(new Date().getTime());
			long timeMark = timestamp.getTime() - concurrencyPollTime;
			
			// has slot
			if(results.size() < results.get(0).getMaxAccess()) {				
				isBookAccessible = true;
			} else {	
				// take over slot older than concurrencyPollTime
				for(EbookConcurrency ec : results) {
					if(timeMark > ec.getAccessTime().getTime()) {
						// remove book
						Book _book = new Book();
						_book.setOrderId(ec.getOrderId());
						_book.setIsbn(ec.getIsbn());
						_book.setSessionId(ec.getSessionId());
						releaseBook(_book);
						releaseBookFromSessionMap(request, _book.getIsbn());
						
						isBookAccessible = true;
						
						break;
					} 
				}
			}
		} else {
			isBookAccessible = true;
		}
		
		return isBookAccessible;
	}	
	
	protected static void releaseBookFromSessionMap(HttpServletRequest request, String isbn) {	
		logger.info("===[releaseBookFromSessionMap]");
		logger.info("====isbn:" + isbn);
		
		// get map in session
		Map<String, Book> bookCheckoutMap = (Map<String, Book>)request.getSession().getAttribute(Book.SESSION_ID);		
		if(null != bookCheckoutMap) {	
			bookCheckoutMap.remove(isbn);
		}
	}
}
