package org.cambridge.ebooks.online.urgent_alert;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author Joseph Mendiola
 * Based on NewsBean.java
 */

public class UrgentAlertBean {
	
	private static final Logger LOGGER = Logger.getLogger(UrgentAlertBean.class);
	
	@SuppressWarnings("unused")
	private String initialize;
	private static final int INTERVAL = 1;

	private static Calendar lastHour = null;
	
	private static  String title;
	private static  String description;
	private static  String display_online;
	private static  Date end_date;
	private static  String end_time;
	private static  String application_type;
	private static  String email;
	private static  Date updated_date;
	private static  String updated_user;
	private static  String email_status;
	private static  String update_lib_newsfeed;
	private static  String shouldDisplay;
	private Calendar dateEnd;
	
	public String getInitialize() {
		boolean isToday;
		Calendar dateNao = Calendar.getInstance();
		this.dateEnd = Calendar.getInstance();
		
		if (lastHour == null){
			isToday = true;
			lastHour = Calendar.getInstance();
			lastHour.set(Calendar.MINUTE, lastHour.get(Calendar.MINUTE)+INTERVAL);
		} else {
			isToday = dateNao.after(lastHour);
		}
		
		if (isToday) {
			LOGGER.info("Initializing UrgentAlertBean...searching for an emergency announcement");
			UrgentAlertWorker worker = new UrgentAlertWorker();
			setUab(worker.searchAll(), dateNao);
			lastHour.set(Calendar.MINUTE, lastHour.get(Calendar.MINUTE)+INTERVAL);
		}

		return "";
	}


	public void setUab(UrgentAlertBean uab, Calendar dateNao) {
		if(null != uab) {
			UrgentAlertBean.title = uab.getTitle();
			UrgentAlertBean.description = uab.getDescription();
			UrgentAlertBean.display_online = uab.getDisplay_online();
			UrgentAlertBean.end_date = uab.getEnd_date();
			UrgentAlertBean.end_time = uab.getEnd_time();
			UrgentAlertBean.application_type = uab.getApplication_type();
			UrgentAlertBean.email = uab.getEmail();
			UrgentAlertBean.updated_date = uab.getUpdated_date();
			UrgentAlertBean.updated_user = uab.getUpdated_user();
			UrgentAlertBean.email_status = uab.getEmail_status();
			UrgentAlertBean.update_lib_newsfeed = uab.getUpdate_lib_newsfeed();
			dateEnd.setTime(uab.getEnd_date());
			if (dateNao.before(this.dateEnd) && "Y".equals(UrgentAlertBean.display_online)){
				UrgentAlertBean.display_online = "true";
			} else {
				UrgentAlertBean.display_online = "false";
			}
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		UrgentAlertBean.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		UrgentAlertBean.description = description;
	}

	public String getDisplay_online() {
		return display_online;
	}

	public void setDisplay_online(String display_online) {
		UrgentAlertBean.display_online = display_online;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		UrgentAlertBean.end_date = end_date;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		UrgentAlertBean.end_time = end_time;
	}

	public String getApplication_type() {
		return application_type;
	}

	public void setApplication_type(String application_type) {
		UrgentAlertBean.application_type = application_type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		UrgentAlertBean.email = email;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		UrgentAlertBean.updated_date = updated_date;
	}

	public String getUpdated_user() {
		return updated_user;
	}

	public void setUpdated_user(String updated_user) {
		UrgentAlertBean.updated_user = updated_user;
	}

	public String getEmail_status() {
		return email_status;
	}

	public void setEmail_status(String email_status) {
		UrgentAlertBean.email_status = email_status;
	}

	public String getUpdate_lib_newsfeed() {
		return update_lib_newsfeed;
	}

	public void setUpdate_lib_newsfeed(String update_lib_newsfeed) {
		UrgentAlertBean.update_lib_newsfeed = update_lib_newsfeed;
	}

	public static String getShouldDisplay() {
		return shouldDisplay;
	}

	public static void setShouldDisplay(String shouldDisplay) {
		UrgentAlertBean.shouldDisplay = shouldDisplay;
	}
	
	



}
