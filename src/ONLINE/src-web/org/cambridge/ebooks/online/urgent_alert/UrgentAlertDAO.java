package org.cambridge.ebooks.online.urgent_alert;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.urgent_alert.UrgentAlert;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * @author Joseph Mendiola
 * Based on NewsDAO.java 
 */

public class UrgentAlertDAO {
private static EntityManagerFactory emf = PersistenceUtil.emf;;
	
	public static UrgentAlert searchAll() {
		UrgentAlert result;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(UrgentAlert.SEARCH_ALL);
		
		try {
			result = (UrgentAlert)query.getSingleResult();
		} catch (Exception e) {
			System.out.println("[UrgentAlertDAO][Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}
}
