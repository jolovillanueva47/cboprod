package org.cambridge.ebooks.online.urgent_alert;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.online.jpa.urgent_alert.UrgentAlert;

public class UrgentAlertWorker {
	public UrgentAlertBean searchAll() {
		try {
			return populateUrgentAlertBean(UrgentAlertDAO.searchAll());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private UrgentAlertBean populateUrgentAlertBean(UrgentAlert record)
	throws InvocationTargetException, IllegalAccessException {

	if (record == null) {
		return null;
	}
	
	UrgentAlertBean UrgentAlertBean = new UrgentAlertBean();		
	BeanUtils.copyProperties(UrgentAlertBean, record);

	return UrgentAlertBean;
}
}
