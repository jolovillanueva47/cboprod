package org.cambridge.ebooks.online.url_resolver;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.url_resolver.OpenUrlResolver;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class OpenUrlResolverWorker {
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	@SuppressWarnings("unchecked")
	public List<OpenUrlResolver> getUrlResolversY(String bodyIds){
		EntityManager em = emf.createEntityManager();
		List<OpenUrlResolver> list = null;
		String sql = 
				"select BODY_ID, URL_PATH  " +
				"from URL_RESOLVER " +
				"where BODY_ID in (" + bodyIds + ") " +
				"and ENABLE = 'Y' " +
				"and URL_PATH is not null ";				
		try{
			Query query = em.createNativeQuery(sql, OpenUrlResolver.class);
			list = query.getResultList();
		}finally{
			em.close();
		}
		return list;
	}
		
}
