package org.cambridge.ebooks.online.url_resolver;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.jpa.url_resolver.OpenUrlResolver;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class OpenUrlResolverBean {
	
	private List<OpenUrlResolver> urlResolvers;
	
	private OrgConLinkedMap orgConMap;
	
	public OpenUrlResolverBean(){
		HttpSession session = HttpUtil.getHttpServletRequest().getSession();
		
		this.orgConMap = OrgConLinkedMap.getFromSession(session);
		
		populateUrlResolvers();;
	}
	
	
	/* private methods */
	
	private void populateUrlResolvers(){
		String bodyIds = this.orgConMap.getAllBodyIds();
		OpenUrlResolverWorker worker = new OpenUrlResolverWorker();
		if(StringUtils.isNotEmpty(bodyIds)){
			List<OpenUrlResolver> list = worker.getUrlResolversY(bodyIds);
			this.urlResolvers = list;
		}
	}
	

	/* gettters / setters */	
	
	public List<OpenUrlResolver> getUrlResolvers() {
		return urlResolvers;
	}

	public void setUrlResolvers(List<OpenUrlResolver> urlResolvers) {
		this.urlResolvers = urlResolvers;
	}

	public OrgConLinkedMap getOrgConMap() {
		return orgConMap;
	}

	public void setOrgConMap(OrgConLinkedMap orgConMap) {
		this.orgConMap = orgConMap;
	}
	
	
	
}
