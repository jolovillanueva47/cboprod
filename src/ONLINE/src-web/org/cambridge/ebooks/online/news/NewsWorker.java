package org.cambridge.ebooks.online.news;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.online.jpa.news.News;

/**
 * @author Karlson A. Mulingtapang
 * NewsWorker.java - Sets data from jpa to bean
 */
public class NewsWorker {
	
	public List<NewsBean> searchAllValidNews() {
		try {
			return populateNewsBean(NewsDAO.searchAllValid());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private List<NewsBean> populateNewsBean(List<News> newsRecords)
		throws InvocationTargetException, IllegalAccessException {
		
		if (newsRecords == null) {
			return null;
		}
		
		List<NewsBean> beanList = new ArrayList<NewsBean>();
		NewsBean newsBean = null;
		
		for(News news : newsRecords) {
			newsBean = new NewsBean();
			BeanUtils.copyProperties(newsBean, news);
			beanList.add(newsBean);
		}
		
		return beanList;
	}
	
	public NewsBean searchById(String id) {
		try {
			return populateNewsBean(NewsDAO.searchById(id));
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private NewsBean populateNewsBean(News newsRecord)
		throws InvocationTargetException, IllegalAccessException {
	
		if (newsRecord == null) {
			return null;
		}
		
		NewsBean newsBean = new NewsBean();		
		BeanUtils.copyProperties(newsBean, newsRecord);
	
		return newsBean;
	}
}
