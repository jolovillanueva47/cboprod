package org.cambridge.ebooks.online.news;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author Karlson A. Mulingtapang
 * NewsBean.java - News Bean
 */
public class NewsBean {
	
	private static final Logger LOGGER = Logger.getLogger(NewsBean.class);
	
	private String messageId;
	
	private String messageTitle;
	private String message;
	private Date startDate;
	private Date endDate;
	private String priority;
	private String link;
	private String archive;
	private String displayOnMainPage;
	private String journalId;
	private String applicationType;
	private int hits;
	
	private String initialize;
	private String populateNews;
	
	private static long lastDay;
	private static List<NewsBean> newsList;
	
	public String getInitialize() {
		boolean isToday;
		long dateNow = System.currentTimeMillis() / (1000*60*60*24);
		
		if (lastDay == 0) {
			isToday = true;
			lastDay = dateNow;
		} else {
			isToday = dateNow > lastDay;
		}
		
		if (isToday) {
			LOGGER.info("Initializing NewsBean...searchAllValidNews()");
			NewsWorker worker = new NewsWorker();
			setNewsList(worker.searchAllValidNews());
			
			lastDay = dateNow;
		}
		
		// dummy
//		List<NewsBean> dummyList = new ArrayList<NewsBean>();
//		
//		NewsBean dummyBean = new NewsBean();
//		dummyBean.setMessageId("1");
//		dummyBean.setMessageTitle("dummy title");
//		dummyBean.setMessage("test test test");
//		dummyBean.setLink("http://ebooks.cambridge.org");
//		
//		dummyList.add(dummyBean);
//		
//		this.newsList = dummyList;
		
		return "";
	}
	
	public String getPopulateNews() {
		String messageId = HttpUtil.getHttpServletRequestParameter("messageId");
		NewsWorker worker = new NewsWorker();
		NewsBean newsBean = worker.searchById(messageId);
		
		// dummy
//		setMessageId("1");
//		setMessageTitle("dummy title");
//		setMessage("test test test");
//		setLink("http://ebooks.cambridge.org");
		
		setMessageId(newsBean.getMessageId());
		setMessageTitle(newsBean.getMessageTitle());
		setMessage(newsBean.getMessage());
		setLink(newsBean.getLink());
		
		return "";
	}
	
	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the messageTitle
	 */
	public String getMessageTitle() {
		return messageTitle;
	}
	/**
	 * @param messageTitle the messageTitle to set
	 */
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}
	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * @return the archive
	 */
	public String getArchive() {
		return archive;
	}
	/**
	 * @param archive the archive to set
	 */
	public void setArchive(String archive) {
		this.archive = archive;
	}
	/**
	 * @return the displayOnMainPage
	 */
	public String getDisplayOnMainPage() {
		return displayOnMainPage;
	}
	/**
	 * @param displayOnMainPage the displayOnMainPage to set
	 */
	public void setDisplayOnMainPage(String displayOnMainPage) {
		this.displayOnMainPage = displayOnMainPage;
	}
	/**
	 * @return the journalId
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * @param journalId the journalId to set
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * @return the applicationType
	 */
	public String getApplicationType() {
		return applicationType;
	}
	/**
	 * @param applicationType the applicationType to set
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	/**
	 * @return the hits
	 */
	public int getHits() {
		return hits;
	}
	/**
	 * @param hits the hits to set
	 */
	public void setHits(int hits) {
		this.hits = hits;
	}
	/**
	 * @return the newsList
	 */
	public List<NewsBean> getNewsList() {
		return newsList;
	}
	/**
	 * @param newsList the newsList to set
	 */
	public void setNewsList(List<NewsBean> newsList) {
		this.newsList = newsList;
	}	
}
