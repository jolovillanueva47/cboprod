package org.cambridge.ebooks.online.news;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.news.News;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * @author Karlson A. Mulingtapang
 * NewsDAO.java - News Data Access Object
 */
public class NewsDAO {

	private static EntityManagerFactory emf = PersistenceUtil.emf;;

	
	
	public static List<News> searchAllValid() {
		List<News> result;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(News.SEARCH_ALL_VALID);
		
		try {
			result = (List<News>) query.getResultList();
		} catch (Exception e) {
			System.out.println("[NewsDAO][Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}
	
	public static News searchById(String id) {
		News result = null;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(News.SEARCH_BY_ID);
		
		try {
			query.setParameter(1, id);
			result = (News)query.getSingleResult();
		} catch (Exception e) {
			System.out.println("[NewsDAO][Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}
		
		return result;
	}
}
