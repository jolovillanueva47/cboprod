package org.cambridge.ebooks.online.supplemental;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.supplemental.EBookIsbnDataLoad;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;


/**
 * @author Karlson A. Mulingtapang
 * EBookIsbnDataLoadBean.java - Bean for table EBOOK_ISBN_DATA_LOAD
 */
public class EBookIsbnDataLoadBean {

	private static final Logger LOGGER = Logger.getLogger(EBookIsbnDataLoadBean.class);
	
	private String isbn;	
	private String supplementalMaterial;
	private String supplementalMaterialUrl;
	private String answerCode;
	private String salesModuleFlag;
	
	@SuppressWarnings("unused")
	private String populate;
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	
	public EBookIsbnDataLoadBean() {
		getPopulate();
	}
	
	public String getPopulate() {			
		String isbn = (String)HttpUtil.getAttributeFromRequestOrSession("currentIsbn");
		if(StringUtils.isEmpty(isbn)) {
			isbn = "";
		} 
		
		EntityManager em = emf.createEntityManager();	
		try{
			EBookIsbnDataLoad bean = em.find(EBookIsbnDataLoad.class, isbn);
			if(bean != null) {
				this.isbn = bean.getIsbn();
				this.supplementalMaterial = bean.getSupplementalMaterial();
				this.supplementalMaterialUrl = bean.getSupplementalMaterialUrl();
				this.answerCode = bean.getAnswerCode();
				this.salesModuleFlag = bean.getSalesModuleFlag(); 
			}
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally{
			em.close();			
		}
		
		return "";
	}
	
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	/**
	 * @return the supplementalMaterial
	 */
	public String getSupplementalMaterial() {
		return supplementalMaterial;
	}
	/**
	 * @param supplementalMaterial the supplementalMaterial to set
	 */
	public void setSupplementalMaterial(String supplementalMaterial) {
		this.supplementalMaterial = supplementalMaterial;
	}
	/**
	 * @return the supplementalMaterialUrl
	 */
	public String getSupplementalMaterialUrl() {
		return supplementalMaterialUrl;
	}
	/**
	 * @param supplementalMaterialUrl the supplementalMaterialUrl to set
	 */
	public void setSupplementalMaterialUrl(String supplementalMaterialUrl) {
		this.supplementalMaterialUrl = supplementalMaterialUrl;
	}

	public String getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(String answerCode) {
		this.answerCode = answerCode;
	}

	public String getSalesModuleFlag() {
		return salesModuleFlag;
	}

	public void setSalesModuleFlag(String salesModuleFlag) {
		this.salesModuleFlag = salesModuleFlag;
	}
	
	
	
	
}
