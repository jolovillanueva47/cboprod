package org.cambridge.ebooks.online.landingpage;

import java.util.List;

public class TocItem {

	private String id;
	private String parentId;
	private String bookId;
	private String chapterId;
	private String pageStart;
	private String text;
	private List<String> authorNameList;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	public String getPageStart() {
		return pageStart;
	}
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<String> getAuthorNameList() {
		return authorNameList;
	}
	public void setAuthorNameList(List<String> authorNameList) {
		this.authorNameList = authorNameList;
	}
}
