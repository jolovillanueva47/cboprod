package org.cambridge.ebooks.online.landingpage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.prize.PrizeLoad;
import org.cambridge.ebooks.online.jpa.textfields.TextfieldsLoad;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.StringUtil;


public class BookBean {
	
	private static Logger logger = Logger.getLogger(BookBean.class);
	private static final String DOMAIN_CBO = System.getProperty("cbo.domain");
	private static final String URL_CONTENT = System.getProperty("content.url.absolute");
	private static final String URL_SSL = System.getProperty("path.ssl");
	private static final String GOOGLE_SCHOLAR_SRC = System.getProperty("google.scholar.src");
		
	private String contentType;
	private String accessType;
	private BookMetaData bookMetaData;	
	private BookContentItem bookContentItem;
	
	private List<BookTocItem> bookTocItemList;	
	private Map<String, String> bookTocItemLevelMap;

	private Map<String, List<TocItem>> tocItemMap;
	private Map<String, String> tocItemLevelMap;
	
	private BookDataWorker worker;
	
	private String currChapterPosition;
	
	private List<BookTocItem> chapNav;
	
	private List<BookMetaData> volumeList;
	
	private boolean hasAccess;
	
	public String getInitPage() throws Exception {
		logger.info("Initialize...");		
		HttpServletRequest req = HttpUtil.getHttpServletRequest();	
		setRequestAttributes(req);		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboBookDataWorker.getKeyHttpRequest(), req);
		param.put(CboBookDataWorker.getKeyBookId(), req.getParameter("bid"));		
		param.put(CboBookDataWorker.getKeyChapterId(), req.getParameter("cid"));
		param.put(CboBookDataWorker.getKeyRequestUri(), req.getRequestURI());
		worker = new CboBookDataWorker(param);
		
		setCurrChapterPosition(null!=req.getParameter("cid")?req.getParameter("cid"):"");
		
		try {
			setBeanAttributes(worker.generateResult());
		} catch (Exception e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
			
		if(StringUtils.isEmpty(getBookMetaData().getId())) {
			throw new Exception("Cannot find Book or is not accessible.");
		}
		
		initChapNav();
		
		logger.info("Finished...");		
		return "";
	}
	
	public  List<BookTocItem> getChapNav() {
		return chapNav;
	}

	public void initChapNav(){
		logger.info("Initialize chapter navigation list....");	
		chapNav = new ArrayList<BookTocItem>();
		List<BookTocItem> temp = getBookTocItemList();
		for (BookTocItem bookTocItem : temp) {
			if(bookTocItem.getType().contains("chapter") || StringUtils.isNotEmpty(bookTocItem.getAbstractText())){
				chapNav.add(bookTocItem);
			}
		}
	}

	public void setCurrChapterPosition(String currChapterPosition) {
		
		this.currChapterPosition = currChapterPosition;
	}

	public String getCurrChapterPosition() {
		String position = "-1";
		List<BookTocItem> bookTocItemList = getChapNav();	
		int i = 0;
		for (BookTocItem bookTocItem : bookTocItemList) {
			String id = bookTocItem.getId();
			if(StringUtils.isNotEmpty(id) && id.equals(this.currChapterPosition)){
				position = String.valueOf(i);
				break;
			}
			i++;
		}
		return position;
	}
	
	/*
	public boolean getHasAccess() {
		return SubscriptionUtil.hasAccess("", this.accessType);
	}
	*/
	
	
	
	private void setBeanAttributes(BookBean bean) {
		setBookMetaData(bean.getBookMetaData());
		setBookContentItem(bean.getBookContentItem());
		setBookTocItemList(bean.getBookTocItemList());
		setBookTocItemLevelMap(bean.getBookTocItemLevelMap());
		setTocItemMap(bean.getTocItemMap());
		setTocItemLevelMap(bean.getTocItemLevelMap());
		setAccessType(bean.getAccessType());
		setVolumeList(bean.getVolumeList());
		setHasAccess(bean.isHasAccess());
		setContentType(bean.getContentType());
	}
	
	public boolean isHasAccess() {
		return hasAccess;
	}

	public void setHasAccess(boolean hasAccess) {
		this.hasAccess = hasAccess;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Map<String, List<TocItem>> getTocItemMap() {
		return tocItemMap;
	}

	public void setTocItemMap(Map<String, List<TocItem>> tocItemMap) {
		this.tocItemMap = tocItemMap;
	}

	public Map<String, String> getTocItemLevelMap() {
		return tocItemLevelMap;
	}

	public void setTocItemLevelMap(Map<String, String> tocItemLevelMap) {
		this.tocItemLevelMap = tocItemLevelMap;
	}
	
	public TextfieldsLoad getTextfieldsLoad() {		
		return worker.generateReviews();
	}
	
	public List<PrizeLoad> getPrizeLoadList() {
		return worker.generatePrizeLoads();
	}
	
	public String getCoinString() {		
		return worker.generateCoinString();
	}
	
	public String getBookKeywordMeta(){		
		return worker.generateBookKeywordMeta();
	}
	
	public String getChapterKeywordMeta() {		
		return StringUtil.stripHTMLTags(worker.generateChapterKeywordMeta());
	}
	
	public String getCountry() {
		HttpServletRequest req = HttpUtil.getHttpServletRequest();	
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CboBookDataWorker.getKeyHttpRequest(), req);		
		return worker.generateCurrentCountry(param);
	}
	
	public String getWorldCatLink() {
		return worker.generateWorldCatLink();
	}

	public String getAccessType() {
		return accessType;
	}
//	public boolean getIsConcurrent() {
//		boolean isConcurrent = false;
//		String[] s = accessType.split(":");
//		if(s.length > 1) {
//			isConcurrent = true;
//		}		
//		return isConcurrent;
//	}
//	public boolean getIsValidConcurrent() {
//		boolean isValidConcurrent = false;
//		String[] s = accessType.split(":");
//		if(s.length > 1) {
//			if(AccessList.CONCURRENCY_ACTIVE.equals(s[1]) 
//					|| AccessList.CONCURRENCY_INACTIVE.equals(s[1])) {
//				isValidConcurrent = true;
//			}
//		}			
//		return isValidConcurrent;
//	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public BookMetaData getBookMetaData() {
		return bookMetaData;
	}

	public void setBookMetaData(BookMetaData bookMetaData) {
		this.bookMetaData = bookMetaData;
	}

	public BookContentItem getBookContentItem() {
		return bookContentItem;
	}

	public void setBookContentItem(BookContentItem bookContentItem) {
		this.bookContentItem = bookContentItem;
	}
	
	public List<BookTocItem> getBookTocItemList() {
		return bookTocItemList;
	}

	public void setBookTocItemList(List<BookTocItem> bookTocItemList) {
		this.bookTocItemList = bookTocItemList;
	}

	public Map<String, String> getBookTocItemLevelMap() {
		return bookTocItemLevelMap;
	}

	public void setBookTocItemLevelMap(Map<String, String> bookTocItemLevelMap) {
		this.bookTocItemLevelMap = bookTocItemLevelMap;
	}
	
	public void setVolumeList(List<BookMetaData> volumeList) {
		this.volumeList = volumeList;
	}

	public List<BookMetaData> getVolumeList() {
		return volumeList;
	}
	
	private void setRequestAttributes(HttpServletRequest req) {
		logger.info("Setting request attributes...");
		req.setAttribute("domainCbo", DOMAIN_CBO);
		req.setAttribute("urlContent", URL_CONTENT);	
		req.setAttribute("urlSsl", URL_SSL);
		req.setAttribute("googleScholarSrc", GOOGLE_SCHOLAR_SRC);
		req.setAttribute("bid", (String)req.getParameter("bid"));
	}
	
	public String getCookieValue(){
		String cookieValue = "";
		boolean langCookieFound = false;
		String cookieVal = HttpUtil.getHttpServletRequestParameter("reader");
		Cookie[] cookies = HttpUtil.getHttpServletRequest().getCookies();
		int i;
		
		if(cookieVal == null){
			if(cookies != null){
				for(i = 0; i < cookies.length; i++){
					if("PDFREADER".equals(cookies[i].getName())){
		            	if(cookies[i].getValue() == null){
		            		cookies[i].setValue("0");
		            		cookies[i].setMaxAge(60*60*24*365);   
							cookies[i].setPath("/");   
							((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookies[i]);
							
		            	}
		            	else{
		            		cookieValue = cookies[i].getValue();
		            	}
		            	langCookieFound = true;
					}
				}
			}
			if(!langCookieFound){
				Cookie cookie = new Cookie("PDFREADER", "0");
				cookie.setMaxAge(60*60*24*365);
				cookie.setPath("/");
				((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookie);
				cookieValue = "0";	
			}	
		}
		else if(cookieVal != null){
					Cookie cookie = new Cookie("PDFREADER", cookieVal);
					cookie.setMaxAge(60*60*24*365);
					cookie.setPath("/");
					((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookie);
					
					cookieValue = cookieVal;
					
		}
		
	
		return cookieValue;
		
	}
}
