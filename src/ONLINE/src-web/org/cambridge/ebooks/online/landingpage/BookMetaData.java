package org.cambridge.ebooks.online.landingpage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrServerUtil;

public class BookMetaData {
	private static final Logger logger = Logger.getLogger(BookMetaData.class);
	private static final String dirContent = System.getProperty("content.dir");
	
	private String id;
	private String publisherId;
	private String contentType;
	private String title;
	private String titleAlphasort;
	private String subtitle;
	private String doi;
	private String altDoi;
	private String series;
	private String seriesAlphasort;
	private String seriesNumber;
	private String seriesCode;
	private String printDate;
	private String onlineDate;
	private String onlineDateDisplay;
	private String onlineIsbn;
	private String printIsbnType;
	private String hardbackIsbn;
	private String paperbackIsbn;
	private String altIsbnEisbn;
	private String altIsbnOther;
	private String volumeNumber;
	private String volumeTitle;
	private String partNumber;
	private String partTitle;
	private String edition;
	private String editionNumber;	
	private String standardImageFilename;
	private String thumbnailImageFilename;
	private String authorNameAlphasort;
	private String copyrightStatement;
	private String publisherName;
	private String publisherLoc;
	private String accessType;
	private String blurb;
	private String coinString;
	
	private List<String> authorRoleList;
	private List<String> authorNameList;
	private List<String> authorNameLfList;
	private List<String> authorAffiliationList;
	private Map<String,String> authorNameAffiliationMap;
	private String authorSingleline;
	
	private List<String> subjectList;
	private List<String> subjectLevelList;
	private List<String> subjectCodeList;
	
	private List<Reference> referenceList;	
	
	private String searchText;
	
	public BookMetaData(){}
	
	
	public String getBlurb() {
		return blurb;
	}
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getAltIsbnEisbn() {
		return altIsbnEisbn;
	}
	public void setAltIsbnEisbn(String altIsbnEisbn) {
		this.altIsbnEisbn = altIsbnEisbn;
	}
	public String getAltIsbnOther() {
		return altIsbnOther;
	}
	public void setAltIsbnOther(String altIsbnOther) {
		this.altIsbnOther = altIsbnOther;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitleAlphasort() {
		return titleAlphasort;
	}
	public void setTitleAlphasort(String titleAlphasort) {
		this.titleAlphasort = titleAlphasort;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getAltDoi() {
		return altDoi;
	}
	public void setAltDoi(String altDoi) {
		this.altDoi = altDoi;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public String getSeriesAlphasort() {
		return seriesAlphasort;
	}
	public void setSeriesAlphasort(String seriesAlphasort) {
		this.seriesAlphasort = seriesAlphasort;
	}		
	public String getSeriesNumber() {
		return seriesNumber;
	}
	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
	public String getSeriesCode() {
		return seriesCode;
	}
	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getOnlineDate() {
		return onlineDate;
	}
	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}
	public String getOnlineDateDisplay() {
		return onlineDateDisplay;
	}
	public void setOnlineDateDisplay(String onlineDateDisplay) {
		this.onlineDateDisplay = onlineDateDisplay;
	}
	public String getOnlineIsbn() {
		return onlineIsbn;
	}
	public void setOnlineIsbn(String onlineIsbn) {
		this.onlineIsbn = onlineIsbn;
	}
	public String getPrintIsbn() {
		String pisbn = "";
		if (StringUtils.isNotEmpty(getHardbackIsbn())) {
			pisbn = getHardbackIsbn();
		} else if(StringUtils.isNotEmpty(getPaperbackIsbn())) {
			pisbn = getPaperbackIsbn();
		} else if(StringUtils.isNotEmpty(getAltIsbnEisbn())) {
			pisbn = getAltIsbnEisbn();
		} else if(StringUtils.isNotEmpty(getAltIsbnOther())) {
			pisbn = getAltIsbnOther();
		}		
		return pisbn;
	}
	public String getPrintIsbnType() {
		return printIsbnType;
	}
	public void setPrintIsbnType(String printIsbnType) {
		this.printIsbnType = printIsbnType;
	}
	public String getHardbackIsbn() {
		return hardbackIsbn;
	}
	public void setHardbackIsbn(String hardbackIsbn) {
		this.hardbackIsbn = hardbackIsbn;
	}
	public String getPaperbackIsbn() {
		return paperbackIsbn;
	}
	public void setPaperbackIsbn(String paperbackIsbn) {
		this.paperbackIsbn = paperbackIsbn;
	}
	public String getVolumeNumber() {
		return volumeNumber;
	}
	public void setVolumeNumber(String volumeNumber) {
		this.volumeNumber = volumeNumber;
	}
	public String getVolumeTitle() {
		return volumeTitle;
	}
	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartTitle() {
		return partTitle;
	}
	public void setPartTitle(String partTitle) {
		this.partTitle = partTitle;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getEditionNumber() {
		return editionNumber;
	}
	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}
	public String getStandardImageFilename() {
		return standardImageFilename;
	}
	public void setStandardImageFilename(String standardImageFilename) {
		this.standardImageFilename = standardImageFilename;
	}
	public String getThumbnailImageFilename() {
		return thumbnailImageFilename;
	}
	public void setThumbnailImageFilename(String thumbnailImageFilename) {
		this.thumbnailImageFilename = thumbnailImageFilename;
	}
	public String getAuthorNameAlphasort() {
		return authorNameAlphasort;
	}
	public void setAuthorNameAlphasort(String authorNameAlphasort) {
		this.authorNameAlphasort = authorNameAlphasort;
	}
	public String getCopyrightStatement() {
		return copyrightStatement;
	}
	public void setCopyrightStatement(String copyrightStatement) {
		this.copyrightStatement = copyrightStatement;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	public String getPublisherLoc() {
		return publisherLoc;
	}
	public void setPublisherLoc(String publisherLoc) {
		this.publisherLoc = publisherLoc;
	}
	public List<String> getAuthorRoleList() {
		return authorRoleList;
	}
	public List<String> getAuthorRoleTitleList() {
		List<String> roleTitleList = new ArrayList<String>();
		for(String roleCode : getAuthorRoleList()) {
			roleTitleList.add(getAuthorRoleTitle(roleCode));
		}
		return roleTitleList;
	}
	public void setAuthorRoleList(List<String> authorRoleList) {
		this.authorRoleList = authorRoleList;
	}
	public List<String> getAuthorNameList() {
		return authorNameList;
	}
	public String getAuthorNames() {
		StringBuilder result = new StringBuilder();		
		if(null != getAuthorNameList() && getAuthorNameList().size() > 0) {
			if(getAuthorNameList().size() > 4) {				
				result.append(getAuthorNameList().get(0)).append(" et al.");
			} else {
				int ctr = 0;
				for(String au : getAuthorNameList()) {
					ctr++;
					result.append(au);					
					if(ctr < getAuthorNameList().size() - 1) {
						result.append(", ");
					} else if(ctr >= 1 && ctr < getAuthorNameList().size()) {
						result.append(" and ");
					}
				}
			}			
		}		
		return result.toString();
	}	
	public void setAuthorNameList(List<String> authorNameList) {
		this.authorNameList = authorNameList;
	}
	public List<String> getAuthorNameLfList() {
		return authorNameLfList;
	}
	public String getAuthorNamesLf() {
		StringBuilder result = new StringBuilder();		
		if(null != getAuthorNameLfList() && getAuthorNameLfList().size() > 0) {
			if(getAuthorNameLfList().size() > 4) {							
				result.append(getAuthorNameLfList().get(0)).append(" et al.");
			} else {
				int ctr = 0;
				for(String au : getAuthorNameLfList()) {
					ctr++;					
					result.append(au);					
					if(ctr < getAuthorNameLfList().size() - 1) {
						result.append("; ");
					} else if(ctr >= 1 && ctr < getAuthorNameLfList().size()) {
						result.append("; and ");
					}
				}
			}			
		}		
		return result.toString();
	}
	public void setAuthorNameLfList(List<String> authorNameLfList) {
		this.authorNameLfList = authorNameLfList;
	}
	public List<String> getAuthorAffiliationList() {
		return authorAffiliationList;
	}
	public void setAuthorAffiliationList(List<String> authorAffiliationList) {
		this.authorAffiliationList = authorAffiliationList;
	}	
	public Map<String, String> getAuthorNameAffiliationMap() {
		return authorNameAffiliationMap;
	}
	public void setAuthorNameAffiliationMap(Map<String, String> authorNameAffiliationMap) {
		this.authorNameAffiliationMap = authorNameAffiliationMap;
	}
	public String getAuthorSingleline() {
		return authorSingleline;
	}
	public void setAuthorSingleline(String authorSingleline) {
		this.authorSingleline = authorSingleline;
	}
	public List<String> getSubjectList() {
		return subjectList;
	}
	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}
	public List<String> getSubjectLevelList() {
		return subjectLevelList;
	}
	public void setSubjectLevelList(List<String> subjectLevelList) {
		this.subjectLevelList = subjectLevelList;
	}
	public List<String> getSubjectCodeList() {
		return subjectCodeList;
	}
	public void setSubjectCodeList(List<String> subjectCodeList) {
		this.subjectCodeList = subjectCodeList;
	}
	public List<Reference> getReferenceList() {
		return referenceList;
	}
	public void setReferenceList(List<Reference> referenceList) {
		this.referenceList = referenceList;
	}		
	public String getAccessType() {
		return accessType;
	}
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}	
	public String getHighlightedBlurb() {			
		String blurb = getBlurb();
		
		if(StringUtils.isNotEmpty(getSearchText())) {			
			SolrQuery q = new SolrQuery();
			
			// filter
			q.addFilterQuery("id:" + getId());
			
			// highlighter
			q.setHighlight(true);
			q.setHighlightFragsize(0);
			q.setHighlightSimplePre("<span class='searchWord'>");
			q.setHighlightSimplePost("&nbsp;</span>");
			q.setHighlightSnippets(1);
			q.setParam("hl.fl", "blurb");
			q.setParam("f.blurb.hl.alternateField", "blurb");
			q.setParam("hl.usePhraseHighlighter", true);		
			
			// query
			q.setQuery("blurb:(" + getSearchText().toLowerCase() + ")");			
			logger.info("query:" + q.getQuery());
			
			QueryResponse response = null;			
			try {
				response = SolrServerUtil.getBookCore().query(q);
				
				StringBuilder hFieldBuilder = new StringBuilder();
				Map<String, Map<String, List<String>>> highlighter = response.getHighlighting();
				
				if(null != highlighter.get(getId()) 
						&& null != highlighter.get(getId()).get("blurb")) {		
					
					for(String s : highlighter.get(getId()).get("blurb")) {
						hFieldBuilder.append(s);
					}
					blurb = hFieldBuilder.toString();
				} 
			} catch (SolrServerException e) {
				logger.error(ExceptionPrinter.getStackTraceAsString(e));
			}			
		}
		
		return blurb;
	}
	
	public static String getAuthorRoleTitle(String roleCode) {
		String phrase = "";
		if("CS".equalsIgnoreCase(roleCode)) {
			phrase = "Course Consultant";
		} else if("AU".equalsIgnoreCase(roleCode)) {
			phrase = "By";
		} else if("EDT".equalsIgnoreCase(roleCode)) {
			phrase = "Edited and translated by";
		} else if("FW".equalsIgnoreCase(roleCode)) {
			phrase = "Foreword by";
		} else if("AW".equalsIgnoreCase(roleCode)) {
			phrase = "Afterword by";	
		} else if("IN".equalsIgnoreCase(roleCode)) {
			phrase = "Introduction by";
		} else if("WI".equalsIgnoreCase(roleCode)) {
			phrase = "With";
		} else if("PR".equalsIgnoreCase(roleCode)) {
			phrase = "Preface by";
		} else if("PH".equalsIgnoreCase(roleCode)) {
			phrase = "Photographs by";
		} else if("AS".equalsIgnoreCase(roleCode)) {
			phrase = "Assisted by";
		} else if("IL".equalsIgnoreCase(roleCode)) {
			phrase = "Illustrated by";
		} else if("AD".equalsIgnoreCase(roleCode)) {
			phrase = "Adaptation by";
		} else if("NA".equalsIgnoreCase(roleCode)) {
			phrase = "Narrated by";
		} else if("AP".equalsIgnoreCase(roleCode)) {
			phrase = "Appendix by";
		} else if("CO".equalsIgnoreCase(roleCode)) {
			phrase = "Edited in consultation with";
		} else if("PP".equalsIgnoreCase(roleCode)) {
			phrase = "Prepared for publication by";
		} else if("GE".equalsIgnoreCase(roleCode)) {
			phrase = "General editor";
		} else if("COR".equalsIgnoreCase(roleCode)) {
			phrase = "Corporate Author";
		} else if("ED".equalsIgnoreCase(roleCode)) {
			phrase = "Edited by";
		} else if("TR".equalsIgnoreCase(roleCode)) {
			phrase = "Translated by";
		} else if("RA".equalsIgnoreCase(roleCode)) {
			phrase = "By";
		} else if("AE".equalsIgnoreCase(roleCode)) {
			phrase = "Edited in association with";
		} else if("WC".equalsIgnoreCase(roleCode)) {
			phrase = "Contribution by";
		} else if("PL".equalsIgnoreCase(roleCode)) {
			phrase = "Prologue by";
		} else if("PB".equalsIgnoreCase(roleCode)) {
			phrase = "Photographed and recorded by";
		} else if("TX".equalsIgnoreCase(roleCode)) {
			phrase = "Text by";
		} else if("SD".equalsIgnoreCase(roleCode)) {
			phrase = "Software developed by";
		} else if("CB".equalsIgnoreCase(roleCode)) {
			phrase = "Compiled by";
		} else if("GR".equalsIgnoreCase(roleCode)) {
			phrase = "General Rapporteur";
		} else if("IC".equalsIgnoreCase(roleCode)) {
			phrase = "In collaboration with";
		} else if("SE".equalsIgnoreCase(roleCode)) {
			phrase = "Series Editor";
		} else if("none".equals(roleCode)) {
			phrase = "By";
		} else if(roleCode.length() > 3) {
			phrase = roleCode; // should be already a role description
		} else {
			phrase = "By";
		}
		
		return phrase;
	}
	
	public String getCoinString() {
		return coinString;
	}


	public void setCoinString(String coinString) {
		this.coinString = coinString;
	}


	//for aaa issue
	public String replaceString_aaa(String origText){		
		String results = "";
		
		if(null == origText) {
			return origText;
		}
		
		results = origText.replaceAll("<i>", "<em>");
		results = results.replaceAll("</i>", "</em>");
		
		results = results.replaceAll("<b>", "<strong>");
		results = results.replaceAll("</b>", "</strong>");
	
		return results;		
	}
	
	public static BookMetaData customReplace_aaa(BookMetaData bookMetaData){
		
		BookMetaData results = new BookMetaData();
		results = bookMetaData;
		
		results.setTitle(results.replaceString_aaa(bookMetaData.getTitle()));
		
		results.setVolumeTitle(results.replaceString_aaa(bookMetaData.getVolumeTitle()));
		results.setEdition(results.replaceString_aaa(bookMetaData.getEdition()));
		results.setSubtitle(results.replaceString_aaa(bookMetaData.getSubtitle()));
		
		results.setSeries(results.replaceString_aaa(bookMetaData.getSeries()));
		
		return results;
		
		
	}
	public String getReferenceFile() {		
		return getReferenceUrl(getOnlineIsbn(), getId());
	}
	
	private String getReferenceUrl(String isbn, String id) {
		String f = isbn + "/" + id + "ref.html";	
		
		File file = new File(dirContent + f);
		if(!file.exists() || 10 >= file.length()) {
			f = "";
		}
		
		return f;
	}	
}
