package org.cambridge.ebooks.online.landingpage;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrServerUtil;

public class BookContentItem {	
	private static final Logger logger = Logger.getLogger(BookContentItem.class);
	private static final String dirContent = System.getProperty("content.dir");
	
	private int position;
	private String id;
	private String parentId;
	private String bookId;
	private String onlineIsbn;
	private String type;
	private String doi;
	private String label;
	private String title;
	private String subtitle;
	private String pdfFilename;
	private String pageStart;
	private String pageEnd;
	private String abstractProblem;
	private String abstractFilename;
	private String abstractText;
	private String accessType;
	private String fulltext;	
	private String displayFlag;

	private List<String> contributorNameList;
	private List<String> keywordList;
	
	private String searchText;
	
	public BookContentItem(){}		
	
	public String getAbstractText() {
		return abstractText;
	}
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}		
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getPdfFilename() {
		return pdfFilename;
	}
	public void setPdfFilename(String pdfFilename) {
		this.pdfFilename = pdfFilename;
	}
	public String getPageStart() {
		return pageStart;
	}
	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}
	public String getPageEnd() {
		return pageEnd;
	}
	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}
	public List<String> getContributorNameList() {
		return contributorNameList;
	}
	public String getContributorNames() {
		StringBuilder result = new StringBuilder();		
		if(null != getContributorNameList() && getContributorNameList().size() > 0) {
			if(getContributorNameList().size() > 4) {				
				result.append(getContributorNameList().get(0)).append(" et al.");
			} else {
				int ctr = 0;
				for(String au : getContributorNameList()) {
					ctr++;
					result.append(au);					
					if(ctr < getContributorNameList().size() - 1) {
						result.append(", ");
					} else if(ctr >= 1 && ctr < getContributorNameList().size()) {
						result.append(" and ");
					}
				}
			}			
		}		
		return result.toString();
	}
	public void setContributorNameList(List<String> contributorNameList) {
		this.contributorNameList = contributorNameList;
	}			
	public String getKeywords() {
		StringBuilder sb = new StringBuilder();
		
		int ctr = 0;
		if(null != getKeywordList() && getKeywordList().size() > 0) {
			for(String s : getKeywordList()) {
				ctr++;
				sb.append(s);
				if(ctr < getKeywordList().size()) {
					sb.append(",");
				}
			}
		}
		
		return sb.toString();
	}
	public List<String> getKeywordList() {
		return keywordList;
	}
	public void setKeywordList(List<String> keywordList) {
		this.keywordList = keywordList;
	}
	public String getOnlineIsbn() {
		return onlineIsbn;
	}
	public void setOnlineIsbn(String onlineIsbn) {
		this.onlineIsbn = onlineIsbn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getReferenceFile() {		
		return getReferenceUrl(getOnlineIsbn(), getId());
	}
	public String getAbstractProblem() {
		return abstractProblem;
	}
	public void setAbstractProblem(String abstractProblem) {
		this.abstractProblem = abstractProblem;
	}
	public String getAbstractFilename() {
		return abstractFilename;
	}
	public void setAbstractFilename(String abstractFilename) {
		this.abstractFilename = abstractFilename;
	}			
	public String getAccessType() {
		return accessType;
	}
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}	
	public String getFulltext() {
		return fulltext;
	}
	public void setFulltext(String fulltext) {
		this.fulltext = fulltext;
	}		
	public String getDisplayFlag() {
		return displayFlag;
	}
	public void setDisplayFlag(String displayFlag) {
		this.displayFlag = displayFlag;
	}
	public String getHighlightedAbstractText() {
		String abstractText = getAbstractText();
		
		if(StringUtils.isNotEmpty(getSearchText())) {			
			SolrQuery q = new SolrQuery();
			
			// filter
			q.addFilterQuery("id:" + getId());
			
			// highlighter
			q.setHighlight(true);
			q.setHighlightFragsize(0);
			q.setHighlightSimplePre("<span class='searchWord'>");
			q.setHighlightSimplePost("</span>");
			q.setHighlightSnippets(1);
			q.setParam("hl.fl", "abstract_text");
			q.setParam("f.abstract_text.hl.alternateField", "abstract_text");
			q.setParam("hl.usePhraseHighlighter", true);		
			
			// query
			q.setQuery("abstract_text:(" + getSearchText().toLowerCase() + ")");			
			logger.info("query:" + q.getQuery());
			
			QueryResponse response = null;			
			try {
				response = SolrServerUtil.getChapterCore().query(q);
				
				StringBuilder hFieldBuilder = new StringBuilder();
				Map<String, Map<String, List<String>>> highlighter = response.getHighlighting();
				
				if(null != highlighter.get(getId()) 
						&& null != highlighter.get(getId()).get("abstract_text")) {		
					
					for(String s : highlighter.get(getId()).get("abstract_text")) {
						hFieldBuilder.append(s);
					}
					abstractText = hFieldBuilder.toString();
				} 
			} catch (SolrServerException e) {
				logger.error(ExceptionPrinter.getStackTraceAsString(e));
			}			
		}
		
		return abstractText;
	}
	
	public boolean getIsFreeContent() {
		return SubscriptionUtil.isFreeContent(this.type);
	}

	private String getReferenceUrl(String isbn, String id) {
		String f = isbn + "/" + id + "ref.html";	
		
		File file = new File(dirContent + f);
		if(!file.exists()) {
			f = "";
		}
		
		return f;
	}	
}
