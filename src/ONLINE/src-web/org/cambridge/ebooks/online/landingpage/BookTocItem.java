package org.cambridge.ebooks.online.landingpage;


public class BookTocItem extends BookContentItem implements Comparable<BookTocItem> {	
	public int compareTo(BookTocItem o) {		
		return getPosition() - o.getPosition();
	}
}
