package org.cambridge.ebooks.online.landingpage;

import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.online.jpa.prize.PrizeLoad;
import org.cambridge.ebooks.online.jpa.textfields.TextfieldsLoad;

public interface BookDataWorker {
	public TextfieldsLoad generateReviews();
	public List<PrizeLoad> generatePrizeLoads();
	public String generateCoinString();
	public String generateBookKeywordMeta();
	public String generateChapterKeywordMeta();
	public String generateCurrentCountry(Map<String, Object>param);
	public String generateWorldCatLink();
	public BookBean generateResult();
}
