package org.cambridge.ebooks.online.landingpage;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.countrystates.Country;
import org.cambridge.ebooks.online.jpa.prize.PrizeLoad;
import org.cambridge.ebooks.online.jpa.textfields.TextfieldsLoad;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.search.solr.SolrQueryFactory;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.SolrServerUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class CboBookDataWorker implements BookDataWorker {
	
	private static final Logger LOGGER = Logger.getLogger(CboBookDataWorker.class);
	
	private static final String KEY_HTTP_SESSION = "http_session";
	private static final String KEY_HTTP_REQUEST = "http_request";
	private static final String KEY_SUBSCRIPTION = "subscription";
	private static final String KEY_BOOK_ID = "book_id";
	private static final String KEY_CHAPTER_ID = "chapter_id";
	private static final String KEY_REQUEST_URI = "request_uri";
	
	private BookBean bookBean;
	private HttpServletRequest request;
	
	public CboBookDataWorker() {}
	public CboBookDataWorker(Map<String, Object> param) {
		String bookId = "";
		String chapterId = "";
		String requestUri = "";
				
		if(null != param.get(KEY_BOOK_ID)) {
			bookId = (String)param.get(KEY_BOOK_ID);
		}
		
		if(null != param.get(KEY_CHAPTER_ID)) {
			chapterId = (String)param.get(KEY_CHAPTER_ID);
		}
		
		if(null != param.get(KEY_REQUEST_URI)) {
			requestUri = (String)param.get(KEY_REQUEST_URI);
		}
		
		if(null != param.get(KEY_HTTP_REQUEST)) {
			request = (HttpServletRequest)param.get(KEY_HTTP_REQUEST);
		}
		
		String contentType = "book";
		if(requestUri.contains("chapter.jsp")) {			
			contentType = "chapter";
		} 
		
		this.bookBean = generateData(bookId, chapterId, contentType);		
	}	

	public static String getKeyHttpSession() {
		return KEY_HTTP_SESSION;
	}
	public static String getKeyHttpRequest() {
		return KEY_HTTP_REQUEST;
	}
	public static String getKeySubscription() {
		return KEY_SUBSCRIPTION;
	}
	public static String getKeyBookId() {
		return KEY_BOOK_ID;
	}
	public static String getKeyChapterId() {
		return KEY_CHAPTER_ID;
	}
	public static String getKeyRequestUri() {
		return KEY_REQUEST_URI;
	}


	public String generateBookKeywordMeta() {
		BookMetaData bookMetaData = bookBean.getBookMetaData();
		StringBuilder sb = new StringBuilder();
		//get all subjects		
		if(null != bookMetaData.getSubjectList() && bookMetaData.getSubjectList().size() > 0) {
			for(String subject : bookMetaData.getSubjectList()){
				if(subject.equals(bookMetaData.getSubjectList().get(bookMetaData.getSubjectList().size() - 1))){
					sb.append(subject);
				}else{
					sb.append(subject + ", ");
				}			 
			}
		}
		
		if(StringUtils.isNotEmpty(bookMetaData.getSeries())){
			if(sb.length() > 0){
				sb.append(", ");
			}
			sb.append(bookMetaData.getSeries());
		}
		return sb.toString();
	}

	public String generateChapterKeywordMeta() {
		return bookBean.getBookContentItem().getKeywords();
	}

	public String generateCoinString() {
		return StringUtil.generateCoinString(bookBean);
	}

	public String generateCurrentCountry(Map<String, Object> param) {
		HttpServletRequest req = null;
		
		if(null != param.get(KEY_HTTP_REQUEST)) {
			req = (HttpServletRequest)param.get(KEY_HTTP_REQUEST);
		}
		
		return Country.getCountry(req);
	}

	public BookBean generateResult() {
		return this.bookBean;
	}

	public List<PrizeLoad> generatePrizeLoads() {		
		return PersistenceUtil.searchList(new PrizeLoad(), PrizeLoad.SEARCH_BY_ISBN, bookBean.getBookMetaData().getOnlineIsbn());
	}

	public TextfieldsLoad generateReviews() {
		TextfieldsLoad textfieldsLoad = PersistenceUtil.searchEntity(new TextfieldsLoad(), TextfieldsLoad.SEARCH_BY_ISBN, bookBean.getBookMetaData().getOnlineIsbn());
		if (textfieldsLoad != null) {
			if (StringUtils.isNotEmpty(textfieldsLoad.getReview1())){				
				String review = StringEscapeUtils.escapeXml(textfieldsLoad.getReview1()).replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;", "&#39;");
				textfieldsLoad.setReview1(review);
			}
			if (StringUtils.isNotEmpty(textfieldsLoad.getReview2())) {
				String review = StringEscapeUtils.escapeXml(textfieldsLoad.getReview2()).replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;", "&#39;");
				textfieldsLoad.setReview2(review);
			}
			if (StringUtils.isNotEmpty(textfieldsLoad.getReview3())) {
				String review = StringEscapeUtils.escapeXml(textfieldsLoad.getReview3()).replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;", "&#39;");
				textfieldsLoad.setReview3(review);
			}
		}		
		return textfieldsLoad;
	}

	public String generateWorldCatLink() {
		StringBuilder q = new StringBuilder();				
		try {
			q.append("http://www.worldcat.org/search?q=")
			.append(URLEncoder.encode("ti:" + bookBean.getBookMetaData().getTitle() + " au:" + bookBean.getBookMetaData().getAuthorNameLfList().get(0), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		return q.toString();
	}

	@SuppressWarnings("unchecked")
	private BookBean generateData(String bookId, String chapterId, String contentType) {
		LOGGER.info("Setting bean attributes...");
		
		// searchText
		String searchText = buildSearchText(request);
		
		BookBean bookBean = new BookBean();
		BookMetaData bookMetaData = new BookMetaData();
		BookContentItem bookContentItem = new BookContentItem();
		
		// SOLR query/response
		List<BookAndChapterSolrDto> bookDocList = buildDocList(bookId, chapterId);
		
		List<BookTocItem> bookTocItemList = new ArrayList<BookTocItem>();
		List<Reference> referenceList = new ArrayList<Reference>();
		
		Map<String, List<TocItem>> tocItemMap = new HashMap<String, List<TocItem>>();			

		Map<String, String> bookTocItemStructure = new HashMap<String, String>();		
		Map<String, String> bookTocItemLevelMap = new HashMap<String, String>();
		
		Map<String, String> tocItemStructure = new HashMap<String, String>();	
		Map<String, String> tocItemLevelMap = new HashMap<String, String>();				
		
		int chapterCtr = 0;
		for(BookAndChapterSolrDto doc : bookDocList) {		
			if("book".equals(doc.getContentType())) {					
				bookMetaData.setId(doc.getId());
				bookMetaData.setTitle(doc.getTitle());
				bookMetaData.setSubtitle(doc.getSubtitle());
				bookMetaData.setEdition(doc.getEdition());
				bookMetaData.setEditionNumber(doc.getEditionNumber());
				bookMetaData.setVolumeNumber(doc.getVolumeNumber());
				bookMetaData.setVolumeTitle(doc.getVolumeTitle());
				bookMetaData.setPartNumber(doc.getPartNumber());
				bookMetaData.setPartTitle(doc.getPartTitle());
				int imageCtr = 0;
				if(null != doc.getCoverImageTypeList()) {
					for(String imageType : doc.getCoverImageTypeList()) {
						if("standard".equalsIgnoreCase(imageType)) {
							bookMetaData.setStandardImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
						} else  {
							bookMetaData.setThumbnailImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
						}
						imageCtr++;
					}
				}
				bookMetaData.setAuthorNameList(doc.getAuthorNameList());
				bookMetaData.setAuthorNameLfList(doc.getAuthorNameLfList());
				bookMetaData.setAuthorAffiliationList(doc.getAuthorAffiliationList());				
				bookMetaData.setAuthorRoleList(doc.getAuthorRoleList());
				bookMetaData.setOnlineIsbn(doc.getIsbn());
				bookMetaData.setHardbackIsbn(doc.getAltIsbnHardback());
				bookMetaData.setPaperbackIsbn(doc.getAltIsbnPaperback());
				bookMetaData.setAltIsbnEisbn(doc.getAltIsbnEisbn());
				bookMetaData.setAltIsbnOther(doc.getAltIsbnOther());
				bookMetaData.setSeries(doc.getSeries());
				bookMetaData.setSeriesCode(doc.getSeriesCode());
				//if(doc.getSeriesNumber() > 0) {
					//bookMetaData.setSeriesNumber(String.valueOf(doc.getSeriesNumber()));
				//}
				bookMetaData.setPrintDate(doc.getPrintDateDisplay());
				bookMetaData.setOnlineDate(doc.getOnlineDateDisplay());
				bookMetaData.setDoi(doc.getDoi());
				bookMetaData.setSubjectCodeList(doc.getSubjectCodeList());
				bookMetaData.setSubjectList(doc.getSubjectList());
				bookMetaData.setSubjectLevelList(doc.getSubjectLevelList());					
				bookMetaData.setPublisherId(doc.getPublisherId());
				bookMetaData.setPublisherName(doc.getPublisherName());
				bookMetaData.setPublisherLoc(doc.getPublisherLoc());
				
				StringBuilder copyrightStatement = new StringBuilder();
				int ctr = 0;
				for(String s : doc.getCopyrightStatementList()) {
					ctr++;					
					copyrightStatement.append(s);
					if(ctr < doc.getCopyrightStatementList().size()) {
						copyrightStatement.append(", ");
					}
				}
				bookMetaData.setAuthorSingleline(doc.getAuthorSingleline());
				bookMetaData.setCopyrightStatement(copyrightStatement.toString());
				if(StringUtils.isNotEmpty(searchText)) {
					bookMetaData.setSearchText(searchText);
				}

				bookMetaData.setBlurb(doc.getBlurb());
				
				if("book".equals(contentType)) {
					String referenceFile = bookMetaData.getReferenceFile();
					if (StringUtils.isNotEmpty(referenceFile)) {
						Reference reference = new Reference();
						reference.setBookId(bookId);
						reference.setFile(referenceFile);					
						if(referenceList.isEmpty()) {
							referenceList.add(reference);
						} 
					}
				}
								
			} else if("chapter".equalsIgnoreCase(doc.getContentType())) {
				chapterCtr++; 
				BookTocItem bookTocItem = new BookTocItem();
				
				String pdfPath = EBooksProperties.CONTENT_DIR + doc.getIsbn() + "/";
				File pdfFile = new File(pdfPath + doc.getPdfFilename());
				if(null == pdfFile || !pdfFile.exists()) {
					LOGGER.info("=== pdf not existing for cid:" + doc.getId());
					bookTocItem.setDisplayFlag("N");
				} else {
					bookTocItem.setDisplayFlag("Y");
				}				
				
				bookTocItem.setId(doc.getId());
				bookTocItem.setPosition(Integer.parseInt(doc.getPosition()));
				bookTocItem.setParentId(doc.getParentId());
				bookTocItem.setBookId(doc.getBookId());
				bookTocItem.setLabel(doc.getLabel());
				bookTocItem.setTitle(doc.getTitle());
				bookTocItem.setSubtitle(doc.getSubtitle());
				bookTocItem.setPdfFilename(doc.getPdfFilename());
				bookTocItem.setPageStart(doc.getPageStart());
				bookTocItem.setPageEnd(doc.getPageEnd());
				bookTocItem.setContributorNameList(doc.getAuthorNameList());
				bookTocItem.setType(doc.getType());
				bookTocItem.setDoi(doc.getDoi());
				bookTocItem.setOnlineIsbn(doc.getIsbn());				
//				bookTocItem.setAccessType(SubscriptionUtil.hasAccessDisplay(bookTocItem.getType(), bookTocItem.getOnlineIsbn(), request.getSession(), subscription));
				bookTocItem.setAbstractText(doc.getAbstractText());
				
				bookTocItemStructure.put(bookTocItem.getParentId(), bookTocItem.getId());
				
				if("root".equalsIgnoreCase(bookTocItem.getParentId())) {
					bookTocItemLevelMap.put(bookTocItem.getId(), "root");
				} else {
					bookTocItemLevelMap.put(bookTocItem.getId(), String.valueOf(getTocItemLevel(bookTocItemStructure, bookTocItem.getId())));
				}
				
				bookTocItemList.add(bookTocItem);
				
				if(chapterCtr == 1) {					
					bookContentItem.setId(bookTocItem.getId());
					bookContentItem.setOnlineIsbn(bookTocItem.getOnlineIsbn());
					bookContentItem.setLabel(bookTocItem.getLabel());
					bookContentItem.setTitle(bookTocItem.getTitle());
					bookContentItem.setSubtitle(bookTocItem.getSubtitle());
					bookContentItem.setPageStart(bookTocItem.getPageStart());
					bookContentItem.setPageEnd(bookTocItem.getPageEnd());
					bookContentItem.setType(bookTocItem.getType());
					bookContentItem.setDoi(bookTocItem.getDoi());
					bookContentItem.setContributorNameList(bookTocItem.getContributorNameList());
					bookContentItem.setAbstractProblem(doc.getAbstractProblem());
					bookContentItem.setAbstractFilename(doc.getAbstractFilename());
					bookContentItem.setKeywordList(doc.getKeywordTextList());	
					bookContentItem.setAbstractText(doc.getAbstractText());
					if(StringUtils.isNotEmpty(searchText)) {
						bookContentItem.setSearchText(searchText);
					}
					
					if("chapter".equals(contentType)) {
						String referenceFile = bookTocItem.getReferenceFile();
						if (StringUtils.isNotEmpty(referenceFile)) {
							Reference reference = new Reference();
							reference.setBookId(bookId);
							reference.setContentId(bookTocItem.getId());
							reference.setContentLabel(bookTocItem.getLabel());
							reference.setContentTitle(bookTocItem.getTitle());
							reference.setFile(referenceFile);	
							referenceList.add(reference);
						}
					}
				}				
				
			} else if("toc".equalsIgnoreCase(doc.getContentType())) {
				TocItem tocItem = new TocItem();
				tocItem.setId(doc.getId());
				tocItem.setParentId(doc.getParentId());
				tocItem.setBookId(doc.getBookId());
				tocItem.setChapterId(doc.getChapterId());
				tocItem.setPageStart(doc.getPageStart());
				tocItem.setText(doc.getTocText());
				tocItem.setAuthorNameList(doc.getAuthorNameList());
				
				tocItemStructure.put(tocItem.getParentId(), tocItem.getId());
				
				if("root".equalsIgnoreCase(tocItem.getParentId())) {
					tocItemLevelMap.put(tocItem.getId(), "root");
				} else {
					tocItemLevelMap.put(tocItem.getId(), String.valueOf(getTocItemLevel(tocItemStructure, tocItem.getId())));
				}
				
				List<TocItem> tocItemList = null;
				if(null != tocItemMap.get(tocItem.getChapterId())) {
					tocItemList = (List<TocItem>)tocItemMap.get(tocItem.getChapterId());
				} else {
					tocItemList = new ArrayList<TocItem>();
				}					
				tocItemList.add(tocItem);
				tocItemMap.put(tocItem.getChapterId(), tocItemList);					
			}
		}
		
		bookMetaData.setReferenceList(referenceList);
		
		//parse relatedvolumes
		if(null!=bookMetaData.getVolumeNumber()){
			bookBean.setVolumeList(parseVolumes(bookMetaData, request.getSession()));
		}
		
		bookBean.setBookMetaData(bookMetaData);
		bookBean.setBookContentItem(bookContentItem);			

		// subscription
		String bodyIds = OrgConLinkedMap.getAllBodyIds(request.getSession());
		String directBodyIds = OrgConLinkedMap.getDirectBodyIds(request.getSession());
		SubscriptionUtil subscription = new SubscriptionUtil(bodyIds, directBodyIds);
		//bookBean.setAccessType(subscription.getAccessType(bookMetaData.getOnlineIsbn(), request, false));
		EBooksAccessListView vw = subscription.getAccessType(bookBean.getBookMetaData().getOnlineIsbn(), request, false);
		
		bookBean.setHasAccess(vw.hasAccess());

		Collections.sort(bookTocItemList);
		bookBean.setBookTocItemList(bookTocItemList);
		bookBean.setBookTocItemLevelMap(bookTocItemLevelMap);
		bookBean.setTocItemMap(tocItemMap);
		bookBean.setTocItemLevelMap(tocItemLevelMap);
		bookBean.setContentType(contentType);
		
		LOGGER.info("bid: " + bookId);
		LOGGER.info("cid: " + chapterId);
		
		return bookBean;
	}
	
	private int getTocItemLevel(Map<String, String> map, String key) {
		int level = 1;
		
		if(null != map.get(key)) {
			level += getTocItemLevel(map, (String)map.get(key));
		} 	
		
		return level;		
	}
	
	private List<BookMetaData> parseVolumes(BookMetaData bookMetaData,HttpSession session){
		LOGGER.info("== parsing other volumes start ==");
		List<BookMetaData> volumeList = new ArrayList<BookMetaData>();
			
		QueryResponse response = null;

		SolrQuery q_bookMeta = new SolrQuery();
		String strTitle = bookMetaData.getTitle().replace("�", "-");
		
		q_bookMeta.setQuery("title:" + "\"" + strTitle + "\"");	
		
		q_bookMeta.setStart(0);
		q_bookMeta.setRows(1000);
		//q_bookMeta.setSortField("volume_number", ORDER.asc);
		LOGGER.info("volumes query: " + q_bookMeta.getQuery());
		response = null;
		List<BookAndChapterSolrDto> bookDocList_bookMeta = null;
		try {
			response = SolrServerUtil.getBookCore().query(q_bookMeta, METHOD.GET);
			bookDocList_bookMeta = response.getBeans(BookAndChapterSolrDto.class);
		} catch (SolrServerException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		for(BookAndChapterSolrDto doc : bookDocList_bookMeta) {
			BookMetaData bmd = new BookMetaData();		
			if(bookMetaData.getTitle().equals(doc.getTitle())){
				//copied from this class - if("book".equals(doc.getContentType())) { 
				//this one is not actualy the same as the copied one.
				bmd.setId(doc.getId());
				bmd.setTitle(doc.getTitle());
				bmd.setSubtitle(doc.getSubtitle());
				bmd.setEdition(doc.getEdition());
				bmd.setEditionNumber(doc.getEditionNumber());
				bmd.setVolumeNumber(doc.getVolumeNumber());
				bmd.setVolumeTitle(doc.getVolumeTitle());
				bmd.setPartNumber(doc.getPartNumber());
				bmd.setPartTitle(doc.getPartTitle());
				int imageCtr = 0;
				if(null != doc.getCoverImageTypeList()) {
					for(String imageType : doc.getCoverImageTypeList()) {
						if("standard".equalsIgnoreCase(imageType)) {
							bmd.setStandardImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
						} else  {
							bmd.setThumbnailImageFilename(doc.getCoverImageFilenameList().get(imageCtr));
						}
						imageCtr++;
					}
				}
				bmd.setAuthorNameList(doc.getAuthorNameList());
				bmd.setAuthorNameLfList(doc.getAuthorNameLfList());
				bmd.setAuthorAffiliationList(doc.getAuthorAffiliationList());				
				bmd.setAuthorRoleList(doc.getAuthorRoleList());
				bmd.setOnlineIsbn(doc.getIsbn());
				bmd.setHardbackIsbn(doc.getAltIsbnHardback());
				bmd.setPaperbackIsbn(doc.getAltIsbnPaperback());
				bmd.setAltIsbnEisbn(doc.getAltIsbnEisbn());
				bmd.setAltIsbnOther(doc.getAltIsbnOther());
				bmd.setSeries(doc.getSeries());
				bmd.setSeriesCode(doc.getSeriesCode());

				bmd.setPrintDate(doc.getPrintDateDisplay());
				bmd.setOnlineDate(doc.getOnlineDateDisplay());
				bmd.setDoi(doc.getDoi());
				bmd.setSubjectCodeList(doc.getSubjectCodeList());
				bmd.setSubjectList(doc.getSubjectList());
				bmd.setSubjectLevelList(doc.getSubjectLevelList());					
				bmd.setPublisherId(doc.getPublisherId());
				bmd.setPublisherName(doc.getPublisherName());
				bmd.setPublisherLoc(doc.getPublisherLoc());
				
				StringBuilder copyrightStatement = new StringBuilder();
				int ctr = 0;
				for(String s : doc.getCopyrightStatementList()) {
					ctr++;					
					copyrightStatement.append(s);
					if(ctr < doc.getCopyrightStatementList().size()) {
						copyrightStatement.append(", ");
					}
				}
			
				bmd.setAuthorSingleline(doc.getAuthorSingleline());
				bmd.setCopyrightStatement(copyrightStatement.toString());
				
				bmd.setBlurb(doc.getBlurb());
				
				volumeList.add(bmd);
			}
			
		}
		
		//do sort
		LOGGER.info("== START SORT ==");		
	    int n = volumeList.size();
	    for (int pass=1; pass < n; pass++) {  // count how many times
	        // This next loop becomes shorter and shorter
	        for (int i=0; i < n-pass; i++) {
	        	String stringvol1 = volumeList.get(i).getVolumeNumber();
	        	String stringvol2 = volumeList.get(i+1).getVolumeNumber();
	        	int vol1 = 0;
	        	int vol2 = 0;
	        	
	        	if(StringUtils.isNotEmpty(stringvol1) && StringUtils.isNotEmpty(stringvol2)){
	        		vol1 = Integer.parseInt(fixVolumeNumber(stringvol1));
		        	vol2 = Integer.parseInt(fixVolumeNumber(stringvol2));
	        	}
	        	
	            if (vol1 > vol2) {
	                // exchange elements
	            	BookMetaData temp = volumeList.get(i);  
	            	volumeList.set(i, volumeList.get(i+1));  
	            	volumeList.set(i+1, temp);
	            }
	        }
	    }
	    LOGGER.info("== END SORT ==");	
		LOGGER.info("== parsing other volumes end ==");
		return volumeList;	
	}
		
	
	private String fixVolumeNumber(String volumeNumber) {
		StringBuilder sb = new StringBuilder();
		
		// check if not numeric
		if(!StringUtils.isNumeric(volumeNumber)) {			
			for(int i = 0; i < volumeNumber.length(); i++) {
				char c = volumeNumber.charAt(i);				
				if(CharUtils.isAsciiNumeric(c)) {
					sb.append(c);
				} else {
					break;
				}
			}
			
			if(sb.length() < 1) {
				sb.append("-1");
			}
		} else {
			sb.append(volumeNumber);
		}
		
		return sb.toString();
	}
	
	private List<BookAndChapterSolrDto> buildDocList(String bookId, String chapterId) {
		List<BookAndChapterSolrDto> bookDocList = null;
		StringBuilder bookQueryStr = new StringBuilder();		
		StringBuilder chapterQueryStr = new StringBuilder();
		QueryResponse response = null;
		
//		String isbns = "";
//		
//		Object obj = request.getSession().getAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE);
//		if(null != obj) {
//			isbns = (String)obj;
//		}
		
		if(StringUtils.isNotEmpty(chapterId)) {
			chapterQueryStr.append("id:").append(chapterId)
			.append(" OR book_id:").append(bookId);			
			SolrQuery chapterQuery = SolrQueryFactory.newCboOnlyInstance();
			chapterQuery.setQuery(chapterQueryStr.toString());	
			chapterQuery.setRows(1000);
			LOGGER.info("chapter query:" + chapterQuery.toString());
			
			bookQueryStr.append("id:").append(bookId);			
			SolrQuery bookQuery = SolrQueryFactory.newCboOnlyInstance();
			bookQuery.setQuery(bookQueryStr.toString());			
			LOGGER.info("book query:" + bookQuery.toString());	
					
			try {
				response = SolrServerUtil.getChapterCore().query(chapterQuery, METHOD.GET);
				List<BookAndChapterSolrDto> chapterDocList = response.getBeans(BookAndChapterSolrDto.class);				

				response = SolrServerUtil.getBookCore().query(bookQuery, METHOD.GET);
				bookDocList = response.getBeans(BookAndChapterSolrDto.class);
				
				chapterDocList.addAll(bookDocList);
				bookDocList = chapterDocList;
			} catch (SolrServerException e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
			
		} else {
			bookQueryStr.append("id:").append(bookId);			
			SolrQuery bookQuery = SolrQueryFactory.newCboOnlyInstance();
			bookQuery.setQuery(bookQueryStr.toString());			
			LOGGER.info("book query:" + bookQuery.toString());			

			chapterQueryStr.append("book_id:").append(bookId);
			SolrQuery chapterQuery = SolrQueryFactory.newCboOnlyInstance();
			chapterQuery.setQuery(chapterQueryStr.toString());		
			chapterQuery.setRows(1000);
			LOGGER.info("chapter query:" + chapterQuery.toString());
				
			try {
				response = SolrServerUtil.getBookCore().query(bookQuery, METHOD.GET);
				bookDocList = response.getBeans(BookAndChapterSolrDto.class);
				
				response = SolrServerUtil.getChapterCore().query(chapterQuery, METHOD.GET);				
				List<BookAndChapterSolrDto> chapterDocList = response.getBeans(BookAndChapterSolrDto.class);
				
				bookDocList.addAll(chapterDocList);
			} catch (SolrServerException e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return bookDocList;
	}
	
	private String buildSearchText(HttpServletRequest request) {
		StringBuilder searchTextBuilder = new StringBuilder();
		if(null != request.getSession().getAttribute("hithighlight")) {			
			if(null == request.getSession().getAttribute("searchText")) {					
				Map<String, String[]> param = (Map<String, String[]>)request.getSession().getAttribute("advanceSearchParam");
				String[] searchText1 = param.get("search_text");
				if(null != searchText1 && searchText1.length > 0) {
					for(String t : searchText1) {
						searchTextBuilder.append(t).append(" ");
					}
				}			
				String[] searchText2 = param.get("search_text_2");
				if(null != searchText2 && searchText2.length > 0) {
					for(String t : searchText2) {
						searchTextBuilder.append(t).append(" ");
					}
				}
			} else {
				searchTextBuilder.append(((String)request.getSession().getAttribute("searchText")));		
			}
		}
		return searchTextBuilder.toString();
	}
}