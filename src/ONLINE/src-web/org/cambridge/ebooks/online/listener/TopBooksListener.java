package org.cambridge.ebooks.online.listener;

import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.topbooks.TopBooksBean;

public class TopBooksListener implements ServletContextListener{

	private static final Logger logger = Logger.getLogger(TopBooksListener.class);
	
	public void contextDestroyed(ServletContextEvent arg0) {
		//do nothing		
	}

	public void contextInitialized(ServletContextEvent arg0) {
		long start = new Date().getTime();
		logger.info("Top books running startup! Start");		
		TopBooksBean tb = new TopBooksBean();
		tb.initOnStartup();
		long end = new Date().getTime();
		logger.info("Top books running startup! End = " + (end - start)/1000d );		
	}
	
}
