package org.cambridge.ebooks.online.email;

import org.cambridge.ebooks.online.util.EBooksConfiguration;
import org.cambridge.ebooks.online.util.StringUtil;

public class HowToAccessBean extends Email {

	private static final String ADMIN_NAME = EBooksConfiguration.getProperty("email.administrator.name");
	private static final String ADMIN_EMAIL = EBooksConfiguration.getProperty("email.administrator");
	private static final String ACCESS_SUBJ = "Cambridge Books Online: Book Access";
	private static final String ACCESS_SEND_SUCCESS = "How To Access This Book: Message Sent.";
	private static final String ACCESS_SEND_FAILURE = "How To Access This Book: Sending Message Failure. Please try Again.";
	private String mailmessage;


	@Override
	protected String getSendEmailSuccessMsg() {
		return ACCESS_SEND_SUCCESS;
	}
	
	@Override
	protected String getSendEmailFailureMsg() {
		return ACCESS_SEND_FAILURE;
	}
	
	
	@Override
	public String getRecipientName() {
		return ADMIN_NAME;
	}
		
	@Override
	public String getRecipientEmail() {
		return ADMIN_EMAIL;
	}
		
	@Override
	public String getSubject() {
		return ACCESS_SUBJ;
	}

	@Override
	public String getMailmessage() {			
		return getMessageTemplate();
	}
	
	@Override
	public void setMailmessage(String mailmessage) {
		this.mailmessage = mailmessage;
	}

	@Override
	public void setRecipientName(String recipientName){}
	@Override
	public void setRecipientEmail(String recipientEmail) {}

	@Override
	protected String getMessageTemplate() {
		String message = generateBookInfo();
		
		if(StringUtil.isNotEmpty(message)){
			resetMailMessageBackup();
		} else if(StringUtil.isNotEmpty(getMailMessageBackup())) {
			message = getMailMessageBackup();
		}
		return message;
	}

	@Override
	protected String getBid() {
		// TODO Auto-generated method stub
		return null;
	}
}
