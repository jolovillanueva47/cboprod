package org.cambridge.ebooks.online.email.aaa;


import java.util.LinkedHashMap;
import java.util.Map;

public class Email_LIB extends Email_aaa {
	
//	private Librarian_VO vo;
	
	private static final Map<String, String> LIB_REQUIRED_FIELDS_MAP;

	static {
		LIB_REQUIRED_FIELDS_MAP = new LinkedHashMap<String, String>();
		
		LIB_REQUIRED_FIELDS_MAP.put("userName", "Your name");
		LIB_REQUIRED_FIELDS_MAP.put("userEmail", "user e-mail address");
		LIB_REQUIRED_FIELDS_MAP.put("recipientName", "Librarian name");
		LIB_REQUIRED_FIELDS_MAP.put("recipientEmail", "Librarian email address");
        
    }
	
	@Override
	public Map<String, String> getRequiredFieldsMap() {
		return LIB_REQUIRED_FIELDS_MAP;
		
	}
	
}
