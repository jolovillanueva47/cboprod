package org.cambridge.ebooks.online.email.aaa;

//import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.MailManager;


public class Email_aaa {

	private static final Logger LOGGER = Logger.getLogger(Email_aaa.class);	
	
	protected static final String TEMPLATE_DIR = System.getProperty("templates.dir").trim();
	protected static final String TEMPLATE_FILE = System.getProperty("mail.user.info.template").trim();
	protected static final String OUTPUT_DIR = System.getProperty("mail.store.path").trim();
	protected static final String FROM_EMAIL_ADD = System.getProperty("email.cbo").trim();

	protected static final String BR = "<br />";
	protected static final String NEW_LINE = "\n";
	
	private String userName;
	private String userEmail;
	private String recipientName;
	private String recipientEmail;
	private String subject;
	private String mailmessage;
	private String emailType;	//for Email_Factory_aaa.createEmail    (hidden field)
	
	public String sendEmail() {
		
//		uses JSF implementations
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpUtil.getHttpSession(false).setAttribute("mailMessageBackup", this.getMailmessage());
		
		String from = this.getUserName();
		String fromEmailAdd = this.getUserEmail();
		String recipient = this.getRecipientName();
		String recipientEmailAdd = this.getRecipientEmail();
		String subj = this.getSubject();
		StringBuilder bodyMessage = new StringBuilder();
		
		bodyMessage.append("From: ").append(from).append(BR);
		bodyMessage.append("Email: ").append(fromEmailAdd).append(BR).append(BR);
		
		// replace all \n with br
		//redo	bodyMessage.append(this.getMessageTemplate().replaceAll(NEW_LINE, BR));
		bodyMessage.append(this.getMailmessage().replaceAll(NEW_LINE, BR));
		
		LOGGER.info("sending email...");
		LOGGER.info("   TEMPLATE_DIR="+TEMPLATE_DIR);
		LOGGER.info("   TEMPLATE_FILE="+TEMPLATE_FILE);
		
		try {
			StringBuilder body = new StringBuilder();
			
			body.append("TO: " + recipient + "<br /><br />\n" + "<b>Message: </b><br />\n" + bodyMessage.toString() + "<br />");
			MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  FROM_EMAIL_ADD, 					//from:	user Email
								  new String[]{recipientEmailAdd},	//to:	recipient Email 
								  null, 							//cc:
								  null,								//bcc:	email, 
								  subj, 							//subject:
								  null,								//attachment (cant use old storemail method w/o the attachment
								  body.toString());					//messageBody
			
			
			
//			JSF Implementations
//			FacesMessage message = new FacesMessage(getSendEmailSuccessMsg());
//			context.addMessage("emailMessage", message);	
			
//needed?			setDisplayDetails(false);			
			
		} catch (Exception e) {
			//LogManager.info(Email_aaa.class, "[Exception] " + e.getMessage());
			//e.printStackTrace();
	        //LogManager.error(Email_aaa.class, e.getMessage());
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
	        
//			JSF implementations
//			FacesMessage message = new FacesMessage(getSendEmailFailureMsg());
//			context.addMessage("emailMessage", message);	
			
//needed?			setDisplayDetails(false);
		} finally {
//			this.reset();
		}
		
		return null;
	}
	
	public void reset(){
		setUserName("");
		setUserEmail("");
		setRecipientName("");
		setRecipientEmail("");
		setSubject("");
		setMailmessage("");
	}
	
	public static String correctCopyrightSymbol(String copyright){		
		//System.out.println("\n\n\n***[Email]copyright: "+copyright);
		LOGGER.info("\n\n\n***[Email]copyright: "+copyright);
		return (copyright != null && copyright.charAt(0) == '?') ? 
				copyright.replaceFirst("\\?", "&#169;") : copyright;
	}
	
	public static String removeCopyrightSymbol(String copyright){
		if(copyright != null){
			copyright = (copyright.charAt(0) == '?') ?copyright.replaceFirst("\\?", "") : copyright;
			
			copyright = copyright.replaceFirst("&#169;", "");
		}
		
		return copyright;
	}
	
	
	protected String unEncode(final String encoded){
		
		StringBuilder unencode = new StringBuilder();
		String[] strings = encoded.split(" ");
		
		for(String str : strings){
			if(str != null && str.length() > 0 && str.contains("&#")){
				String str1 = str.substring(str.indexOf('&'), str.indexOf(';')).replace("&#", "");
				char c = (char)Integer.parseInt(str1);
				str = new StringBuilder(str).replace(str.indexOf('&'), str.indexOf(';') + 1, Character.toString(c)).toString();
			}
			unencode.append(str).append(" ");
		}
		
		if(unencode.toString().contains("&#"))
			unencode = new StringBuilder(unEncode(unencode.toString()));
			
		return unencode.toString();
	}




	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMailmessage() {
		return mailmessage;
	}

	public void setMailmessage(String mailmessage) {
		this.mailmessage = mailmessage;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public Map<String, String> getRequiredFieldsMap() {
		
		return null;
	}
	
	public Map<String, String> validateEmail(Map<?,?> emailRequiredFieldsMap, Map<?,?> paramMap){
		
		Map<String, String> errors   = new LinkedHashMap<String, String>();
		Map<String, String> errors2   = new LinkedHashMap<String, String>();
		
		//scan request parameters
		for (Entry<?, ?> entry : paramMap.entrySet()) {
		
			String key = entry.getKey().toString();
		    String[] temp = (String[]) entry.getValue();
	
		    if(emailRequiredFieldsMap.containsKey(key)){
				
				String value = temp[0];
				String errorMessage = "";
				
				boolean hasErrors = false;				
						
				if(StringUtils.isEmpty(value)){
					hasErrors = true;
					errorMessage = emailRequiredFieldsMap.get(key) + " is required";
					
				}else if(key.contains("Email") && value.indexOf("@") < 0 ){
					hasErrors = true;
					errorMessage = "Invalid " + emailRequiredFieldsMap.get(key);
					//System.out.println(errorMessage+"msg");
				}
				
				
				if(hasErrors == true){				
					errors.put(key, errorMessage);	
					
				}
				
			}//end if
		    //System.out.println("key="+key+"; value="+temp[0]);	
		}//end for
		
		
		//scan required fields map
		for (Entry<?, ?> entry : emailRequiredFieldsMap.entrySet()) {
			
			String key = entry.getKey().toString();
		    String temp = (String) entry.getValue();
		    String value = temp;
		    
		    if(errors.containsKey(key)){
		    	errors2.put(key, errors.get(key));
		    }
		      //System.out.println("sorting ---- key="+key+"; value="+value);	
		}//end for
		
		
		
		return errors2;
	
	}//end isValidemail method
	
	

	
}//end class
