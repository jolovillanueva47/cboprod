package org.cambridge.ebooks.online.email;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.BladeMember;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.UrlUtil;
import org.cambridge.ebooks.service.solr.util.ExceptionPrinter;

public class RecommendToLibrarianBean extends Email {

	private static final Logger LOGGER = Logger.getLogger(RecommendToLibrarianBean.class);
//	private static final String LIB_NAME = EBooksConfiguration.getProperty("email.administrator.name");
//	private static final String LIB_EMAIL = EBooksConfiguration.getProperty("email.administrator");
	private static final String LIB_SUBJ = "Cambridge Books Online: Recommended Book";
	private static final String LIB_SEND_SUCCESS = "Recommend This to a Librarian: Message Sent.";
	private static final String LIB_SEND_FAILURE = "Recommend This to a Librarian: Sending Message Failure. Please try Again.";
		
//	private Map<String, String> organisationInSession = new LinkedHashMap<String, String>();
	private static List<SelectItem> allAvailableRoles;
		
	private boolean resetFields;
	private String recipientName;
	private String recipientEmail;
	private Integer education = null;
	private String education2 = null;
	private SelectItem[] educationItems = {
			new SelectItem(new Integer(1), "High School"), //value, label
			new SelectItem(new Integer(2), "Bachelors"),
			new SelectItem(new Integer(3), "Maseters"),
			new SelectItem(new Integer(4), "PHD")
	};
	private String mailmessage;
	
	public RecommendToLibrarianBean() {
		LOGGER.info("===constructor===");		
		recipientName = getRecipientNameFromDb();
		recipientEmail = getRecipientEmailFromDb();		
	}
	
	public List<SelectItem> getAllAvailableRoles() {		
		return allAvailableRoles;
	}
	
	public String resetRecommendationFields(){
		setUserName("");
		setUserEmail("");
		setRecipientName("");
		setRecipientEmail("");
		//setSubject("");
		//setMailmessage("");
		
		setResetFields(true);
		return "";
	}
	
	public Integer getEducation(){
		return education;
	}
	public void setEducation(Integer newValue){
		education = newValue;
	}
	public String getEducation2(){
		return education2;
	}
	public void setEducation2(String newValue){
		education2 = newValue;
	}
	public SelectItem[] getEducationItems(){
		return educationItems;
	}	

	public void setResetFields(boolean resetFields) {
		this.resetFields = resetFields;
	}
	
	@Override
	public String getUserName(){
		if(this.resetFields){
			return null;
		} else {
			return super.getUserName();
		}
	}
	
	@Override
	public String getUserEmail(){
		if(this.resetFields){
			return null;
		} else {
			return super.getUserEmail();
		}
	}
	
	@Override
	protected String getSendEmailSuccessMsg() {
		return LIB_SEND_SUCCESS;
	}
	
	@Override
	protected String getSendEmailFailureMsg() {
		return LIB_SEND_FAILURE;
	}

	
	@Override
	public String getRecipientName() {		
		return recipientName;
	}
		
	@Override
	public String getRecipientEmail() {
		return recipientEmail;
	}	

	@Override
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;		
	}

	@Override
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;		
	}

	@Override
	public void validateEmail(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		// don't validate on reset
		if(!this.resetFields) {
			super.validateEmail(context, component, value);
		}
	}

	@Override
	public String getSubject() {
		return LIB_SUBJ;
	}

	@Override
	public String getMailmessage() {	
		this.mailmessage = getMessageTemplate();
		return this.mailmessage;
	}
	
	
	@Override
	public void setMailmessage(String mailmessage) {
		this.mailmessage = mailmessage;
	}

	@Override
	protected String getMessageTemplate() {
		String result = "";
		String url = "";
		try {
			url = UrlUtil.getClientDns(HttpUtil.getHttpServletRequest());
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		
		StringBuilder mailMessage = new StringBuilder();				
		mailMessage.append("I would like to recommend that the " +
				"library purchase access to the following title " +
				"through Cambridge Books Online (").append(url).append(")");
		mailMessage.append("\n\n");
		String bookInfo = generateBookInfo();
		mailMessage.append(bookInfo);
		
		if(StringUtils.isNotEmpty(bookInfo)) {
			resetMailMessageBackup();
			result = mailMessage.toString() ;			
		} else if(StringUtils.isNotEmpty(getMailMessageBackup())) {
			result = getMailMessageBackup();
		} 
		
		return result;
	}		
	
	@Override
	protected String getBid() {
		HttpServletRequest req = HttpUtil.getHttpServletRequest();
		return req.getParameter("bid");
	}
	
	private String getRecipientNameFromDb() {
		HttpSession session = HttpUtil.getHttpSession(false);		
		
		String[] membershipIds = OrgConLinkedMap.getFromSession(session).getMembershipBodyIdsAsArray();
		
		String sessionOrgBodyId = OrgConLinkedMap.getFromSession(session).getCurrentlyLoggedBodyId();
		int orgId = StringUtils.isNotEmpty(sessionOrgBodyId) ? Integer.parseInt(sessionOrgBodyId) : 0;
		
		if(membershipIds != null && membershipIds.length > 0){
			for(String id : membershipIds){
				orgId = Integer.parseInt(id);
			}
		}
		
		LOGGER.info("sql: "+BladeMember.BLADE_MEMBER_SELECT);
		LOGGER.info("orgId: "+orgId);
		
		String recipientName = "";
		if(orgId == 0){
			recipientName = this.resetFields ? "" : this.recipientName;
		} else {
			List<BladeMember> bm = PersistenceUtil.searchList(
					new BladeMember(),
					BladeMember.BLADE_MEMBER_SELECT,
					orgId,orgId);
			try {
				LOGGER.info(" fullname: "+bm.get(0).getMemberFullname());
				recipientName = this.resetFields ? "" : bm.get(0).getMemberFullname();
			} catch (IndexOutOfBoundsException e){
				LOGGER.error("error getting fullname from BladeMember");
				recipientName = "";
			}
		}
		
		return recipientName;
	}
	
	private String getRecipientEmailFromDb() {
		String email = "";
		
		HttpSession session = HttpUtil.getHttpSession(false);
		String[] membershipIds =(String[]) session.getAttribute("membership");

		String sessionOrgBodyId = OrgConLinkedMap.getFromSession(session).getCurrentlyLoggedBodyId();
		int orgId = StringUtils.isNotEmpty(sessionOrgBodyId) ? Integer.parseInt(sessionOrgBodyId) : 0;
		
		if(membershipIds != null && membershipIds.length > 0){
			for(String id : membershipIds){
				orgId = Integer.parseInt(id);
			}
		}
		
		if(orgId == 0){
			email = this.resetFields ? "" : this.recipientEmail;
		} else {
			List<BladeMember> bm = PersistenceUtil.searchList(
					new BladeMember(),
					BladeMember.BLADE_MEMBER_SELECT,
					orgId,orgId);
			
			email = this.resetFields ? "" : bm.get(0).getMemberEmail();
		}
		
		return email;
	}
}
