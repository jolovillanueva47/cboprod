package org.cambridge.ebooks.online.email.aaa.vo;

import java.util.List;

import org.cambridge.ebooks.online.ebook.Author;
import org.cambridge.ebooks.online.util.HttpUtil;

public class BookLink_VO extends Email_VO{
	
	private String emailType;
	private String bookLink;
	private String Title;
	private String Copyright;
	private String PrintpubDate;
	private String OnlinepubDate;
	private String OnlineIsbn;
	private String HardbackIsbn;
	private String Doi;
	private List<Author> authors;
	
	public BookLink_VO() {
		//check if working
		String testVOparam = (String)HttpUtil.getHttpServletRequest().getSession().getAttribute("vo");
		//System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	}
	
//	getters and setters	
	
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	
	public String getBookLink() {
		return bookLink;
	}
	public void setBookLlink(String bookLink) {
		this.bookLink = bookLink;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getCopyright() {
		return Copyright;
	}
	public void setCopyright(String copyright) {
		Copyright = copyright;
	}
	public String getPrintpubDate() {
		return PrintpubDate;
	}
	public void setPrintpubDate(String printpubDate) {
		PrintpubDate = printpubDate;
	}
	public String getOnlinepubDate() {
		return OnlinepubDate;
	}
	public void setOnlinepubDate(String onlinepubDate) {
		OnlinepubDate = onlinepubDate;
	}
	public String getOnlineIsbn() {
		return OnlineIsbn;
	}
	public void setOnlineIsbn(String onlineIsbn) {
		OnlineIsbn = onlineIsbn;
	}
	public String getHardbackIsbn() {
		return HardbackIsbn;
	}
	public void setHardbackIsbn(String hardbackIsbn) {
		HardbackIsbn = hardbackIsbn;
	}
	public String getDoi() {
		return Doi;
	}
	public void setDoi(String doi) {
		Doi = doi;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	
	
}
