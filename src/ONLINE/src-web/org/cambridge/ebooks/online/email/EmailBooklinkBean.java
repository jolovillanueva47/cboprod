package org.cambridge.ebooks.online.email;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

public class EmailBooklinkBean extends Email {

	private static final Logger LOGGER = Logger.getLogger(EmailBooklinkBean.class);
	
	private static final String EMAIL_BOOKLINK_SUBJ = "Cambridge Books Online: Book Link";
	private static final String BOOKLINK_SEND_SUCCESS = "Email Link to This Book: Message Sent.";
	private static final String BOOKLINK_SEND_FAILURE = "Email Link to This Book: Message Sending Failure! Please try Again.";
	
	private String bookLink = (String) HttpUtil.getSessionAttribute("exactURLSession");
	private String mailmessage;	
	private String recipientName;
	private String recipientEmail;
	
	public EmailBooklinkBean() {
		LOGGER.info("===constructor===");
		
		HttpServletRequest req = HttpUtil.getHttpServletRequest();		
		String bid = req.getParameter("bid");
		String contentType = req.getParameter("contentType");
		
		LOGGER.info("===contentType:" + contentType);
		
		if(StringUtils.isNotEmpty(bid)) {
			List<BookAndChapterSolrDto> info;
			if("chapter".equalsIgnoreCase(contentType)) {
				String cid = req.getParameter("cid");
				info = SolrUtil.getBookAndChapterInfoById(bid, cid);
				if(null != info && info.size() > 1) {
					bookLink = "http://dx.doi.org/" + info.get(1).getDoi();
				}
			} else {
				info = SolrUtil.getBookAndChapterInfoById(bid, "");
				if(null != info && info.size() > 0) {
					bookLink = "http://dx.doi.org/" + info.get(0).getDoi();
				}
			}			
		}
	}
	
	public String getBookLink() {
		return bookLink;
	}	

	public void setBookLink(String bookLink) {
		this.bookLink = bookLink;
	}
	
	@Override
	public String getSubject() {
		return EMAIL_BOOKLINK_SUBJ;
	}
	

	@Override
	public String getMailmessage() {
		this.mailmessage = getMessageTemplate();
		return this.mailmessage;
	}

	@Override
	public void setMailmessage(String mailmessage) {
		this.mailmessage = mailmessage;
	}
	
	@Override
	public String getRecipientName() {
		return this.recipientName;
	}
	
	@Override
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	
	@Override
	public String getRecipientEmail() {
		return this.recipientEmail;
	}
	
	@Override
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	@Override
	protected String getSendEmailSuccessMsg() {
		return BOOKLINK_SEND_SUCCESS;
	}
	
	
	@Override
	protected String getSendEmailFailureMsg() {
		return BOOKLINK_SEND_FAILURE;
	}

	@Override
	protected String getMessageTemplate() {
		String bookInfo = generateBookInfo();
		String message = "";
		
		if(StringUtils.isNotEmpty(bookInfo)){
			message = (getBookLink() + "\n\n" + bookInfo);
		}
		
		if(StringUtils.isNotEmpty(message)){
			resetMailMessageBackup();
		} else if(StringUtils.isNotEmpty(getMailMessageBackup())) {
			message = getMailMessageBackup();
		} 
		
		return message;
	}

	@Override
	protected String getBid() {
		HttpServletRequest req = HttpUtil.getHttpServletRequest();
		return req.getParameter("bid");
	}
}
