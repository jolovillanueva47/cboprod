package org.cambridge.ebooks.online.email;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.common.EBookSubjectAlert;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class NewTitlesServlet extends HttpServlet {
	
	private static final Logger LOG = Logger.getLogger(NewTitlesServlet.class);
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response){
		doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){
		PrintWriter out = null;
		try 
		{
			StringBuilder content = null;
			String memberId = request.getParameter("memberId");
			String subjectId = request.getParameter("subjectId");
			
			response.setHeader("Content-Disposition", "attachment;filename="+ subjectId +".csv");		
			response.setContentType("application/csv");		
			response.setCharacterEncoding("UTF-8"); 
			
			out = response.getWriter();
			content = createResponse(memberId, subjectId);
			
			if(content == null || "".equals(content))
				out.print("Member not found.");
			else
				out.print(content);
			out.flush();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			LOG.equals(" doPost(HttpServletRequest,HttpServletResponse): " + e.getMessage());
		}
		finally
		{
			if(out != null)
				out.close();
		}
	}
	
	private StringBuilder getCSVContent(String subjectIds){
		StringBuilder result = new StringBuilder();
		BufferedReader reader = null;
		Set<String> uContent = new HashSet<String>();
		try
		{
			File dir = new File(System.getProperty("csv.dir"));
			File[] csvs = dir.listFiles();
			for(File csv : csvs)
			{
				LOG.info(" FILE TITLE ------------------>>>>>>>>>>> " + csv.getName().replace(".csv", ""));
				LOG.info(" SubjectIds ------------------>>>>>>>>>>> " + subjectIds);
				LOG.info(" CSV ------------------>>>>>>>>>>> " + csv);
				if((csv.getName().replace(".csv", "")).contains(subjectIds))
				{
					reader = new BufferedReader(new FileReader(csv));
					String content = null;
					while((content = reader.readLine()) != null){	
						LOG.info(" CONTENT ------------------>>>>>>>>>>> " + content);
						if(uContent.contains(content))
							continue;
						else{
							LOG.info("ADDITIONAL CONTENT ------------------>>>>>>>>>>> " + content);
							uContent.add(content);
							result.append(content).append("\n");
						}
					}
				}
			}
		}
		catch(IOException ioe)
		{
			LOG.equals(" getCSVContent(List<String>): " + ioe.getMessage());
		}
		finally
		{
			try 
			{
				if(reader != null)
					reader.close();
			} 
			catch (IOException e) 
			{
				LOG.equals(" inside finally getCSVContent(List<String>): " + e.getMessage());
			}
		}
		
		return result;
	}
	
	private StringBuilder createResponse(String memberId, String subjectId){
		StringBuilder result = null;
		//List<String> subjectIds = new ArrayList<String>();
		String sql = "SELECT * FROM ebook_subject_alert WHERE member_id = ?1 and subject_id = ?2";
		List<EBookSubjectAlert> subjectAlerts = PersistenceUtil.searchListNative(new EBookSubjectAlert(), sql, memberId, subjectId);
		 
		 LOG.info(" SubjectIdsRESULTQUERY ------------------>>>>>>>>>>> " + subjectId);
		for(EBookSubjectAlert subjectAlert : subjectAlerts){
			subjectId = subjectAlert.getPk().getSubjectId();
		 LOG.info(" SubjectIdsSubjectAlert ------------------>>>>>>>>>>> " + subjectAlert.getPk().getSubjectId());
		}
		if(subjectId != null && !subjectId.isEmpty())
		{
			result = new StringBuilder();
			result.append("ISBN,CBO Pub Date,HB ISBN,HB Pub Date,PB ISBN,PB Pub Date,Author,Title,Volume,Edition,Subject,Series\n\n");
			result.append(getCSVContent(subjectId));
		}
		
		return result;	
	}

}
