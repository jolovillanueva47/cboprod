package org.cambridge.ebooks.online.email.aaa;

import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.MailManager;
import org.cambridge.ebooks.online.util.StringUtil;

public class Email_VAD extends Email_aaa {
	
	private static final Logger LOGGER = Logger.getLogger(Email_VAD.class);
	
	private static final String[] EMAIL_ADMIN_BCC = new String[] {System.getProperty("email.admin").trim(), System.getProperty("email.admin.bcc").trim()};	
	private static final String DO_NOT_REPLY_EMAIL = System.getProperty("do.not.reply.email").trim();
	
	private final String usEmail = System.getProperty("view.access.email.us").trim();
	private final String ukEmail = System.getProperty("view.access.email.uk").trim();
	
	private String userId;
	private String bodyId;
	private String sessionId;
	private String fullName;
	private String emailAddress;
	
	private String userEmailAddress;
	
	private String emailSubject = "Cambridge Books Online";
	private String typeOfProblem;
	private String problemDescription;
	private String location;
	private String browser;
	private String ipAddress;

	private String domainName;
	private String node;
	
	private String organisationInSession ;
	private String consortiaInSession ;
	private String societyInSession  ;
	private String offerInSession ;
	
	private static final Map<String, String> VAD_REQUIRED_FIELDS_MAP;
	static {
		VAD_REQUIRED_FIELDS_MAP = new LinkedHashMap<String, String>();
        
		VAD_REQUIRED_FIELDS_MAP.put("userEmailAddress", "User email address");
		VAD_REQUIRED_FIELDS_MAP.put("subject", "Email subject");
		VAD_REQUIRED_FIELDS_MAP.put("typeOfProblem", "Type of problem");
		VAD_REQUIRED_FIELDS_MAP.put("problemDescription", "Problem description");
    }

	
	@Override
	public Map<String, String> getRequiredFieldsMap() {
		return VAD_REQUIRED_FIELDS_MAP;
		
	}

	@Override
	public String sendEmail() {
		//FacesContext context = FacesContext.getCurrentInstance();
		String recipient = "";
		String location = "";
		try {
						
			StringBuilder body = new StringBuilder();			
			
			if(StringUtil.isNotEmpty(this.getTypeOfProblem())){
				body.append("<b>Predefined Problem:</b> " + this.getTypeOfProblem() + "<br>");
			}
			
			body.append("<b>Describe Your Problem:</b> " + this.getProblemDescription() + "<br>");
			
			body.append("<b>Reported by:</b> " + this.getUserEmailAddress() + "<br>");
			
			if(StringUtil.isNotEmpty(this.getLocation())){
				String locationCode = this.getLocation().trim().split(":")[0];
				String locationLabel = this.getLocation().trim().split(":")[1];
				
				if("uk".equals(locationCode)){
					recipient = this.ukEmail;
					location = locationLabel;
					
				}else if("us".equals(locationCode)){
					recipient = this.usEmail;	
					location = locationLabel;
				}
	
				body.append("<b>Location:</b> " + location);
			}
			
			body.append(this.getAccessDetails());
			
			
			//System.out.println("----SEND EMAIL----");
			LOGGER.info("----SEND EMAIL----");
		
			
			
			
			MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  DO_NOT_REPLY_EMAIL, 
								  new String[]{recipient}, 
								  null, 
								  EMAIL_ADMIN_BCC, 
								  this.getEmailSubject(), 
								  null,
								  body.toString());
				
			
		} catch (Exception e) {
			LOGGER.error("[Exception] " + e.getMessage());
		} 
		
		return "view_access_details";		
		
	}
	
	public String getAccessDetails() throws UnknownHostException{	
		//HttpServletRequest request = HttpUtil.getHttpServletRequest();
		StringBuffer sb = new StringBuffer();
		sb.append("<h1>View Access Details</h1>");
		sb.append("<p>Below is a summary of your user details. You may use this information in order to work with your local administrator to troubleshoot any access issues you have, or you can contact Cambridge University Press Customer Services by filling in the form below and submitting it.</p>");
		sb.append("<h2>User Details</h2>");		
		sb.append("<p>Browser & OS: " + this.getBrowser() + "</p>" );
		sb.append("<p>IP Address: " + this.getIpAddress() + "</p>");
		sb.append("<p>Domain Name: " + this.getDomainName() + "</p>");
		sb.append("<p>Node: " + this.getNode() + "</p>");
		sb.append("<p>Body Id: " + this.getBodyId() + "</p>");
		sb.append("<p>Session Id: " + this.getSessionId() + "</p>");
		sb.append("<p>User&#8217;s Full Name: " + this.getFullName() + "</p>");
		sb.append("Email Address: " + this.getEmailAddress() + "</p>");
		sb.append("<h2>Organisation Details</h2>");		
		sb.append(this.getOrgList());
		sb.append("<h2>Consortia Details</h2>");
		sb.append(this.getConsortiaList());
		sb.append("<h2>Society Details</h2>");
		sb.append(this.getSocietyList());
		sb.append("<h2>Offer Details</h2>");
		sb.append(this.getOfferList());		
		return sb.toString();
	}

	//organisation
	public void setOrganisationInSession(String organisationInSession) {
		this.organisationInSession = organisationInSession;
	}

	public String getOrganisationInSession() {
		return organisationInSession;
	}
	private String getOrgList(){		
		String organisationInSession = this.getOrganisationInSession();
		return getPropertyMap(organisationInSession);
	}

	//consortia
	public String getConsortiaInSession() {
		return consortiaInSession;
	}

	public void setConsortiaInSession(String consortiaInSession) {
		this.consortiaInSession = consortiaInSession;
	}

	private String getConsortiaList(){
		String consortia = this.getConsortiaInSession();
		return getPropertyMap(consortia);
	}
	

	//society
	public String getSocietyInSession() {
		return societyInSession;
	}

	public void setSocietyInSession(String societyInSession) {
		this.societyInSession = societyInSession;
	}
	private String getSocietyList(){
		String soc = this.getSocietyInSession();
		return getPropertyMap(soc);
	}

	//offer
	public String getOfferInSession() {
		return offerInSession;
	}

	public void setOfferInSession(String offerInSession) {
		this.offerInSession = offerInSession;
	}
	private String getOfferList(){
		String offer = this.getSocietyInSession();
		return getPropertyMap(offer);
	}

	//util
	private String getPropertyMap(String strings){
		StringBuffer sb = new StringBuffer();
		
		String[] stringArray = strings.trim().split(",");
		
		for (String string : stringArray) {
			string = string.replace('{', ' ');
			string = string.replace('}', ' ');
			String key = string.trim().split("=")[0];
			String value = string.trim().split("=")[1];
			sb.append("<p>Body ID: " +  key + "</p>");
			sb.append("<p>Display Name: " +  value + "</p>");
		}
		 
		return sb.toString();
		
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUserEmailAddress() {
		return userEmailAddress;
	}

	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getTypeOfProblem() {
		return typeOfProblem;
	}

	public void setTypeOfProblem(String typeOfProblem) {
		this.typeOfProblem = typeOfProblem;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

}
