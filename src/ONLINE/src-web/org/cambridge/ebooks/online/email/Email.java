package org.cambridge.ebooks.online.email;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.MailManager;
import org.cambridge.ebooks.online.util.SolrUtil;


public abstract class Email {
	
	private static final Logger LOGGER = Logger.getLogger(Email.class);
	
	private static final String TEMPLATE_DIR = System.getProperty("templates.dir").trim();
	private static final String TEMPLATE_FILE = System.getProperty("mail.user.info.template").trim();
	private static final String OUTPUT_DIR = System.getProperty("mail.store.path").trim();
	private static final String FROM_EMAIL_ADD = System.getProperty("do.not.reply.email").trim();
	
	private String userName;
	private String userEmail;
	
	private List<BookAndChapterSolrDto> bookInfoList;
	
	private boolean displayDetails = true;		
	private static final String BR = "<br />";	
	private static final String NEW_LINE = "\n";
		
	public abstract String getMailmessage();
	public abstract void setMailmessage(String mailmessage);
	public abstract String getRecipientName();
	public abstract void setRecipientName(String recipientName);
	public abstract String getRecipientEmail();
	public abstract void setRecipientEmail(String recipientEmail);	
	public abstract String getSubject();	
	
	public void validateEmail(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(null != value) {
			if(!(value instanceof String)) {
				throw new IllegalArgumentException("The value must be a String.");
			}
			
			String email = (String) value;
			
			if (email.indexOf("@") < 0) {
				throw new ValidatorException(new FacesMessage("Invalid Email Address."));  
			}
		}
	}
	
	public String getUserName() {
		User user = HttpUtil.getUserFromCurrentSession();
		if(user != null) {
			this.userName = user.getFirstName() + " " + user.getLastName();
		}
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		User user = HttpUtil.getUserFromCurrentSession();
		if(user != null) {
			this.userEmail = user.getEmail();
		}
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	protected abstract String getMessageTemplate(); 
	protected abstract String getSendEmailSuccessMsg();
	protected abstract String getSendEmailFailureMsg();
	protected abstract String getBid();
	
	protected String unEncode(final String encoded){
		
		StringBuilder unencode = new StringBuilder();
		
		String[] strings = encoded.split(" ");
		
		for(String str : strings)
		{
			if(str != null && str.length() > 0 && str.contains("&#"))
			{
				String str1 = str.substring(str.indexOf('&'), str.indexOf(';')).replace("&#", "");
				char c = (char)Integer.parseInt(str1);
				str = new StringBuilder(str).replace(str.indexOf('&'), str.indexOf(';') + 1, Character.toString(c)).toString();
			}
			unencode.append(str).append(" ");
		}
		
		if(unencode.toString().contains("&#"))
			unencode = new StringBuilder(unEncode(unencode.toString()));
			
		return unencode.toString();
	}
	
	protected String generateBookInfo(){
		StringBuilder result = new StringBuilder();
		
		bookInfoList = SolrUtil.getBookAndChapterInfoById(getBid(), "");		
		
		String title = bookInfoList.get(0).getTitle();
		String copyright = bookInfoList.get(0).getCopyrightStatementListAsString();
		String printPub = bookInfoList.get(0).getPrintDate();
		String onlinePub = bookInfoList.get(0).getOnlineDate();
		String onlineIsbn13 = bookInfoList.get(0).getIsbn();
		String hbIsbn13 = bookInfoList.get(0).getAltIsbnHardback();
		String bookDoi = bookInfoList.get(0).getDoi();
		
		List<String> authors = bookInfoList.get(0).getAuthorNameList();
		List<String> roles = bookInfoList.get(0).getAuthorRoleTitleList();
		List<String> affiliation = bookInfoList.get(0).getAuthorAffiliationList();
		
		result.append("Title: " +title).append(NEW_LINE);
		
		// Build Author-Affiliation combo
		if(authors != null && authors.size() > 0) {
			int ctr = 0;
			for(String au : authors) {
				result.append(roles.get(ctr));
				result.append(": ");
				if(StringUtils.isNotEmpty(au)){
					result.append(au.trim());
				}
				result.append(NEW_LINE);
				if(StringUtils.isNotEmpty(affiliation.get(ctr)) && !"null".equalsIgnoreCase(affiliation.get(ctr))) {
					result.append("Affiliation: ");
					result.append(affiliation.get(ctr).trim());
					result.append(NEW_LINE);
					result.append(NEW_LINE);
				} else {
					result.append(NEW_LINE);
				}
				ctr++;
			}
		}
		
		result.append("Copyright:" + removeCopyrightSymbol(copyright)).append(NEW_LINE);
		result.append("Print Publication Date: " + printPub).append(NEW_LINE);
		result.append("Online Publication Date: " + onlinePub).append(NEW_LINE + NEW_LINE);
		
		result.append("Online ISBN-13: " + onlineIsbn13).append(NEW_LINE);
		if(StringUtils.isNotEmpty(hbIsbn13) && !hbIsbn13.equalsIgnoreCase("null")) {
			result.append("Hardback ISBN-13: " + hbIsbn13).append(NEW_LINE + NEW_LINE);
		} else {
			result.append(NEW_LINE);
		}
		result.append("Book DOI: http://dx.doi.org/" + bookDoi).append(NEW_LINE);
		
		return title != null ? unEncode(result.toString()) : "";
	}
	
	public String sendEmail() {
		LOGGER.info("===sendEmail");
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		HttpUtil.getHttpSession(false).setAttribute("mailMessageBackup", getMailmessage());
		
		String from = getUserName();
		String fromEmailAdd = getUserEmail();
		String recipient = getRecipientName();
		String recipientEmailAdd = getRecipientEmail();
		String subj = getSubject();
		StringBuilder bodyMessage = new StringBuilder();
		
		bodyMessage.append("From: ").append(from).append(BR);
		bodyMessage.append("Email: ").append(fromEmailAdd).append(BR).append(BR);
		
		// replace all \n with br
		String unicodedMessage = StringEscapeUtils.escapeXml(this.getMessageTemplate()).replaceAll("&apos;", "&#39;");
		bodyMessage.append(unicodedMessage.replaceAll(NEW_LINE, BR));
		
		try {
			StringBuilder body = new StringBuilder();
			
			body.append("TO: " + recipient + "<br /><br />\n" + "<b>Message: </b><br />\n" + bodyMessage.toString() + "<br />");
			MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  FROM_EMAIL_ADD, 					//from:	user Email
								  new String[]{recipientEmailAdd},	//to:	recipient Email 
								  null, 							//cc:
								  null,								//bcc:	email, 
								  subj, 							//subject:
								  body.toString());					//messageBody
			
			FacesMessage message = new FacesMessage(getSendEmailSuccessMsg());
			context.addMessage("emailMessage", message);	
			
			setDisplayDetails(false);			
			
		} catch (Exception e) {
			LOGGER.info(ExceptionPrinter.getStackTraceAsString(e));
			FacesMessage message = new FacesMessage(getSendEmailFailureMsg());
			context.addMessage("emailMessage", message);	
			
			setDisplayDetails(false);
		} 
		
		return "";
	}	
	
	public static String correctCopyrightSymbol(String copyright){		
		System.out.println("\n\n\n***[Email]copyright: "+copyright);
		return (copyright != null && copyright.charAt(0) == '?') ? 
				copyright.replaceFirst("\\?", "&#169;") : copyright;
	}
	
	public static String removeCopyrightSymbol(String copyright){
		if(StringUtils.isNotEmpty(copyright)){
			copyright = (copyright.charAt(0) == '?') ? 
					copyright.replaceFirst("\\?", "") : copyright;
			
			copyright = copyright.replaceFirst("&#169;", "");
		}
		
		return copyright;
	}
	
	public String goBack() {
		setDisplayDetails(true);
		return "";
	}

	/**
	 * @return the displayDetails
	 */
	public boolean isDisplayDetails() {
		return displayDetails;
	}

	/**
	 * @param displayDetails the displayDetails to set
	 */
	public void setDisplayDetails(boolean displayDetails) {
		this.displayDetails = displayDetails;
	}
	
	public void resetMailMessageBackup() {
		HttpUtil.getHttpSession(false).setAttribute("mailMessageBackup", "");
	}
		
	public String getMailMessageBackup() {
		return (String)HttpUtil.getAttributeFromRequestOrSession("mailMessageBackup");
	}
}
