package org.cambridge.ebooks.online.email.aaa;

import java.util.LinkedHashMap;
import java.util.Map;



public class Email_BL extends Email_aaa {
	
	
//	private static String BL_SUBJECT = "Cambridge Books Online: Book Link";
	
	private static final Map<String, String> BL_REQUIRED_FIELDS_MAP;

		static {
			BL_REQUIRED_FIELDS_MAP = new LinkedHashMap<String, String>();
	        
			BL_REQUIRED_FIELDS_MAP.put("userName", "Your name");
	        BL_REQUIRED_FIELDS_MAP.put("userEmail", "user e-mail address");
	        BL_REQUIRED_FIELDS_MAP.put("recipientName", "Recipient's name");
	        BL_REQUIRED_FIELDS_MAP.put("recipientEmail", "Recipient's Email Address");
	    }
	
	@Override
	public Map<String, String> getRequiredFieldsMap() {
		
		return BL_REQUIRED_FIELDS_MAP; 
	}

}
