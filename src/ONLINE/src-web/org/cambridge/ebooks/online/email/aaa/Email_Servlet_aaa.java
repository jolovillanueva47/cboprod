package org.cambridge.ebooks.online.email.aaa;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/**
* apirante
*/
public class Email_Servlet_aaa extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(Email_Servlet_aaa.class);
	
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req,  HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				
		if(StringUtils.isNotEmpty(req.getParameter("reset_aaa"))){
			
			dispatchRequest(req, resp, getPath(req)+"?reset=all");
		
		}else{
			
			//create instances of appropriate email using factory
			Email_aaa email = Email_Factory.createEmail(req.getParameterMap());
						
			try {
				
				//POPULATE BEAN FROM MATCHING FORM
				BeanUtils.populate(email, req.getParameterMap());

				String path = getPath(req);
				
//				if("EC".equals(email.getEmailType())){
//					
//					Email_EC ec = (Email_EC) email;
//					
//					if("download".equals(ec.getType())){
//					//	ec.download(req, resp, email);
//					}else if("email".equals(ec.getType())){
//						sendEmail(req, resp, email, path);
//					}
//					
//				}else{
					
					sendEmail(req, resp, email, path);
				//}
					
		
			} catch (IllegalAccessException e) {
				LOGGER.error(e.getMessage());
			} catch (InvocationTargetException e) {
				LOGGER.error(e.getMessage());
			}
		}


	}
	
	public void dispatchRequest(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
		
		req.getRequestDispatcher(path).forward(req, resp);
		
	}
	
	public String getPath(HttpServletRequest req){
		
		String type = req.getParameter("emailType");
		String path = "";
		
		if("BL".equals(type)){
			path = "/aaa/popups/email_link.jsf";
		}else if("LIB".equals(type)){
			path = "/aaa/popups/recommendToLibrarian.jsf";
		}else if("VAD".equals(type)){
			path = "/aaa/popups/view_access.jsf";
		}else if("EC".equals(type)){
			path = "/aaa/popups/export_citation.jsf";
		}
		
		return path;
		
	}
	
	private void sendEmail(HttpServletRequest req, HttpServletResponse resp, Email_aaa email, String path) 
		throws ServletException, IOException{
		
		Map<String, String> errorMsgsMap = email.validateEmail(email.getRequiredFieldsMap(), req.getParameterMap());
		if(errorMsgsMap.size() == 0){
				
			//SEND EMAIL
			email.sendEmail();
			
			if("VAD".equals(email.getEmailType())){
				req.setAttribute("vadEmail", "yes");
			}
			
			path = "/aaa/popups/email_success.jsf";
		

		}else{
		
			req.setAttribute(("errorMessage_" + email.getEmailType()) , errorMsgsMap);
		}
	
		//redirect to page/path
		dispatchRequest(req, resp, path);
		
		
	}
	
	
}//end class
