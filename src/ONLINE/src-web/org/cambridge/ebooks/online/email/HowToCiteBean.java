package org.cambridge.ebooks.online.email;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.SolrUtil;

/**
 * @author kmulingtapang
 */
public class HowToCiteBean {

	private BookAndChapterSolrDto bookInfo;
	private BookAndChapterSolrDto chapterInfo;
	
	public HowToCiteBean() {
		bookInfo = new BookAndChapterSolrDto();
		chapterInfo = new BookAndChapterSolrDto();
		
		HttpServletRequest req = HttpUtil.getHttpServletRequest();
		String bid = req.getParameter("bid");
		if(StringUtils.isNotEmpty(bid)) {
			List<BookAndChapterSolrDto> bookAndChapterInfoList;
			
			String cid = req.getParameter("cid");
			if(StringUtils.isNotEmpty(cid)) {
				bookAndChapterInfoList = SolrUtil.getBookAndChapterInfoById(bid, cid);			
			} else {
				bookAndChapterInfoList = SolrUtil.getBookAndChapterInfoById(bid, "");	
			}
			
			if(null != bookAndChapterInfoList && bookAndChapterInfoList.size() > 0) {
				bookInfo = bookAndChapterInfoList.get(0);
			}	
			
			if(null != bookAndChapterInfoList && bookAndChapterInfoList.size() > 1) {
				chapterInfo = bookAndChapterInfoList.get(1);
			}	
		}
	}

	public BookAndChapterSolrDto getBookInfo() {
		return bookInfo;
	}

	public BookAndChapterSolrDto getChapterInfo() {
		return chapterInfo;
	}
}
