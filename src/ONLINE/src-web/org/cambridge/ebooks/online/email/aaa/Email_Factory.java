package org.cambridge.ebooks.online.email.aaa;

import java.util.Map;

import org.apache.log4j.Logger;

public class Email_Factory {
	
	private static final Logger LOGGER = Logger.getLogger(Email_Factory.class);
	
	public static Email_aaa createEmail(Map<?, ?> paramMap) {

		String []emailTypeRaw = (String[])paramMap.get("emailType");
		String emailType = emailTypeRaw[0];
			
		//System.out.println("emailType from email_link.jsp -----------> "+ paramMap.get("emailType"));
		Email_aaa email = null;
		
		if ("LIB".equals(emailType)){
			email = new Email_LIB();
		}else if("BL".equals(emailType)){
			email = new Email_BL();
		}else if("VAD".equals(emailType)){
			email = new Email_VAD();
		}else{
			//System.out.println("Creating instance of Email_Unknown");
			LOGGER.error("No email of this type = "+emailType);
		}
		
		return email;   
	}
	
	
	
}
