package org.cambridge.ebooks.online.internal_reports;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.EjbUtil;


public class InternalReportsLogger {
	private static final Logger LOGGER = Logger.getLogger(InternalReportsLogger.class);
	
	private static final String CONNECTION_FACTORY = "ConnectionFactory";
	
	private static final String JMS_DESTINATION = "queue/internal_reports/logger";
	
	private static final String[] properties = new String[]{
			"sessionId",			
			"ipAddress",
			"eventNo",
			"httpReferrer",
			"browser",
			"eventStr1",
			"eventStr2",
			"eventStr3",
			"athensId",
			"shibbolethId",
			"memberId",
			"userId",
			"debug",
			"ipOrgs",
			"directBodyId",
			"autologinId",
			"sequence"
		};
	
	public static void logEvent(InternalReportsBean bean, HttpServletRequest request){
		LOGGER.info("-----logEvent");
		LOGGER.info("event no:" + bean.getEventNo());
		LOGGER.info("event str 1:" + bean.getEventStr1());
		LOGGER.info("event str 2:" + bean.getEventStr2());
		LOGGER.info("event str 3:" + bean.getEventStr3());
		
		try {
			HttpSession sess = request.getSession();
			ConnectionFactory connectionFactory = EjbUtil.getJmsConnectionFactory( CONNECTION_FACTORY );
			Connection connection = connectionFactory.createConnection();
	        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        
	        Destination destination = EjbUtil.getJmsDestination( JMS_DESTINATION );            
	        MessageProducer messageProducer = session.createProducer(destination);
	        Message payload = session.createMapMessage();
	        
	        for(String prop : properties){
	        	if(prop.equals("sequence")){
	        		bean.setSequence(bean.getSequence()+1);
	        		sess.setAttribute("sequence", bean.getSequence());
	        	}
	        	payload.setStringProperty(prop, BeanUtils.getProperty(bean, prop));
	        }	    	
	    	
	    	try { 
	            messageProducer.send( payload );
            } finally { 
            	messageProducer.close();
                session.close();
                connection.close();	
            }
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}

        
		
	}
}
