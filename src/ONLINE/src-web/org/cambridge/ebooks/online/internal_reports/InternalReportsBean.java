package org.cambridge.ebooks.online.internal_reports;

import java.sql.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;


public class InternalReportsBean {
	private static final Logger LOGGER = Logger.getLogger(InternalReportsBean.class);
	
	public static final String TABLE_NAME = "CBO_SESSION_EVNT";
	
	
	private String sessionId;
	private long sequence;
	private Date notifiedTime;
	private String ipAddress;
	private long eventNo;
	private String httpReferrer;
	private String browser;
	private String eventStr1;
	private String eventStr2;
	private String eventStr3;
	private String athensId;
	private String shibbolethId;
	private long memberId;
	private String userId;
	private String debug;	
	private String ipOrgs;
	private String directBodyId;
	private String autologinId;
	
	public InternalReportsBean() {}
	
	public InternalReportsBean(HttpServletRequest request, int logType, String eventStr1, String eventStr2) {
		initInternalReportsBean(request, logType, eventStr1, eventStr2);	
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	public Date getNotifiedTime() {
		return notifiedTime;
	}
	public void setNotifiedTime(Date notifiedTime) {
		this.notifiedTime = notifiedTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public long getEventNo() {
		return eventNo;
	}
	public void setEventNo(long eventNo) {
		this.eventNo = eventNo;
	}
	public String getHttpReferrer() {
		return httpReferrer;
	}
	public void setHttpReferrer(String httpReferrer) {
		this.httpReferrer = httpReferrer;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getEventStr1() {
		return eventStr1;
	}
	public void setEventStr1(String eventStr1) {
		this.eventStr1 = eventStr1;
	}
	public String getEventStr2() {
		return eventStr2;
	}
	public void setEventStr2(String eventStr2) {
		this.eventStr2 = eventStr2;
	}
	public String getEventStr3() {
		return eventStr3;
	}
	public void setEventStr3(String eventStr3) {
		this.eventStr3 = eventStr3;
	}
	public String getAthensId() {
		return athensId;
	}
	public void setAthensId(String athensId) {
		this.athensId = athensId;
	}
	public String getShibbolethId() {
		return shibbolethId;
	}
	public void setShibbolethId(String shibbolethId) {
		this.shibbolethId = shibbolethId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDebug() {
		return debug;
	}

	public void setDebug(String url) {
		this.debug = url;
	}
	
	public String getIpOrgs() {
		return ipOrgs;
	}

	public void setIpOrgs(String ipOrgs) {
		this.ipOrgs = ipOrgs;
	}
	
	public String getDirectBodyId() {
		return directBodyId;
	}

	public void setDirectBodyId(String directBodyId) {
		this.directBodyId = directBodyId;
	}
		

	public String getAutologinId() {
		return autologinId;
	}

	public void setAutologinId(String autologinId) {
		this.autologinId = autologinId;
	}

	public void initInternalReportsBean(HttpServletRequest request, int eventNo, String eventStr1, String eventStr2){
		HttpSession session = request.getSession();		
		this.sessionId = session.getId();
		this.sequence = getSequenceNumber(request);//-- will be supplied by mdb processing
		//this.notifiedTime -- will be provided by mdb processing
		this.ipAddress = getIpAddress(request);
		this.eventNo = eventNo;
		this.httpReferrer = request.getHeader(Referrer);
		
		//view headers
		getHeaders(request);
		
		this.browser = getBrowser(request);
		this.eventStr1 = eventStr1;
		this.eventStr2 = eventStr2;
		this.eventStr3 = OrgConLinkedMap.getAllBodyIds(session);
		this.athensId = getAthensId(request, this); 
		this.shibbolethId = getShibbId(request, this);
		this.memberId = getMemberId(session);		
		this.userId = request.getParameter("username");
		this.debug = getServletPath(request);		
		this.ipOrgs = OrgConLinkedMap.getFromSession(session).getIpAuthenticatedBodyIds();
		this.autologinId = OrgConLinkedMap.getFromSession(session).getAutologinId();
		
		this.directBodyId = OrgConLinkedMap.getDirectBodyIds(session);
	}
	
	private long getSequenceNumber(HttpServletRequest request){
		HttpSession session = request.getSession();
		long returnVal;
//		Enumeration al = session.getAttributeNames();
		if (session.getAttribute("sequence") == null){
			returnVal = 0;
		} else {
			returnVal = Long.parseLong(session.getAttribute("sequence").toString());
		}
//		returnVal++;
//		session.setAttribute("sequence", Long.toString(returnVal));
		
		return returnVal;
	}
	
	private String getServletPath(HttpServletRequest request){
		String url = request.getServletPath() + "?" + request.getQueryString();
		return url;
	}
	
	private static final String Client_ip = "X-Cluster-Client-Ip";
	private static final String ip_magic = "ip";
	
	public static final String Referrer = "referer";
	private static final String user_agent = "user-agent";
	
	private static String getIpAddress(HttpServletRequest request){		
		String ipAddress = request.getHeader(Client_ip);
		String ipMagic = request.getParameter(ip_magic);
		if(StringUtils.isNotEmpty(ipMagic)){
			return ipMagic;
		}
		if(StringUtils.isNotEmpty(ipAddress)){
			return ipAddress;
		}else{
			return request.getRemoteAddr();
		}
	}
	
	private static String getBrowser(HttpServletRequest request){		
		String browser = request.getHeader(user_agent);
		return browser;
	}
		
	private static String getAthensId(HttpServletRequest request, InternalReportsBean bean){
		HttpSession session = request.getSession();		
		Object athensId = OrgConLinkedMap.getFromSession(session).getAthensId();
		String athensParam = request.getParameter("ath_id");
		if(athensId != null){
			return (String) athensId;
		}else if( athensParam != null){
			//invalid athens login event 13
			bean.setEventNo(InternalEventEnum.INVALID_ATHENS_LOGIN.getEventNum());
			return athensParam;
		}else{
			return null;			
		}
	}
	
	
	private static String getShibbId(HttpServletRequest request, InternalReportsBean bean){
		HttpSession session = request.getSession();
		Object shibId = OrgConLinkedMap.getFromSession(session).getShibbolethId();		
		String shibParam = request.getParameter("shib_id");
		if(shibId != null){
			return (String) shibId;
		}else if( shibParam != null ){
			//invalid athens login event 14
			bean.setEventNo(InternalEventEnum.INVALID_SHIBB_LOGIN.getEventNum());
			return shibParam;
		}else{			
			return null;
		}
	}
	
	private static final String userInfo = "userInfo";
	private long getMemberId(HttpSession session){
		Object user = session.getAttribute(userInfo);
		if(user != null && user instanceof User){
			return Integer.parseInt(((User) user).getMemberId());
		}
		return -1;
	}
		
	@SuppressWarnings("unchecked")
	private static void getHeaders(HttpServletRequest request){
		String headername;
		for(Enumeration<String> e = (Enumeration<String>) request.getHeaderNames(); e.hasMoreElements(); ){
			headername = (String)e.nextElement();
			LOGGER.info(headername + " = " + request.getHeader (headername));			
		}
	}


	
	
}
