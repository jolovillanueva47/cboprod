/**
 * 
 */
package org.cambridge.ebooks.online.internal_reports;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 *
 */
public class InternalReportsBackupLogger {
	private static final Logger LOGGER = Logger.getLogger(InternalReportsBackupLogger.class);
	
	private static final String INTERNAL_REPORTS_BACKUP_DIR = System.getProperty("internal.reports.backup.dir");
	
	public static void createBackupFile(InternalReportsBean bean) throws Exception {
		LOGGER.info("===createBackupFile");
		
		DateFormat formatter = new SimpleDateFormat("MM_dd_yyyy");
		
		StringBuilder filename = new StringBuilder(INTERNAL_REPORTS_BACKUP_DIR);
		String dateToday = formatter.format(new Date());
		filename.append("/").append("CBO_SESSION_EVNT_").append(dateToday).append(".dat");
		
		File file = new File(filename.toString());
		
		FileOutputStream fos = null;
		FileChannel fosChannel = null;
		FileLock lock = null;
		
		try {
			if (file.createNewFile()) {
				fos = new FileOutputStream(file);
				fosChannel = fos.getChannel();
				lock = fosChannel.lock();
				
				fos.write(getFlatFileHeaders());
				
				lock.release();
				fosChannel.close();
				fos.close();
			}			
			writeFile(bean, filename.toString());
		} catch (Exception e) {
			throw e;
		} 		
	}
	
	private static byte[] getFlatFileHeaders() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("\"SESSION_ID\",");
		sb.append("\"SEQUENCE\",");
		sb.append("\"NOTIFIED_TIME\",");
		sb.append("\"IP_ADDRESS\",");
		sb.append("\"EVENT_NO\",");
		sb.append("\"HTTP_REFERRER\",");
		sb.append("\"BROWSER\",");
		sb.append("\"EVENT_STR1\",");
		sb.append("\"EVENT_STR2\",");
		sb.append("\"EVENT_STR3\",");
		sb.append("\"ATHENS_ID\",");
		sb.append("\"SHIBBOLETH_ID\",");
		sb.append("\"MEMBER_ID\",");
		sb.append("\"DEBUG\",");
		sb.append("\"IP_ORGS\",");
		sb.append("\"AUTOLOGIN_ID\"\n");
		
		return sb.toString().getBytes();
	}
	
	private static void writeFile(InternalReportsBean bean, String backupFilePath) throws Exception {
		FileOutputStream fos = null;
		FileChannel fosChannel = null;
		FileLock lock = null;
		
		try {
			fos = new FileOutputStream(backupFilePath, true);
			fosChannel = fos.getChannel();
			lock = fosChannel.tryLock();
			
			while(null == lock) {
				lock = fosChannel.tryLock();
			}
			
			fos.write(getFlatFileValues(bean));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			lock.release();
			fosChannel.close();
			fos.close();
		}
	}
	
	private static byte[] getFlatFileValues(InternalReportsBean bean) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSSSSSSSS a");		
		
		String sessionId = bean.getSessionId();
		String sequence = Long.toString(bean.getSequence());
		String notifiedTime = formatter.format(new Date());
		String ipAddress = bean.getIpAddress();
		String eventNo = Long.toString(bean.getEventNo());
		String httpReferrer = bean.getHttpReferrer();
		String browser = bean.getBrowser();
		String eventStr1 = bean.getEventStr1();
		String eventStr2 = bean.getEventStr2();
		String eventStr3 = bean.getEventStr3();
		String athensId = bean.getAthensId();
		String shibbolethId = bean.getShibbolethId();
		String memberId = Long.toString(bean.getMemberId());
		String debug = bean.getDebug();
		String ipOrgs = bean.getIpOrgs();
		String autoLoginId = bean.getAutologinId();
		
		
		sb.append("\"" + (StringUtils.isEmpty(sessionId) ? "" : sessionId) + "\",");
		sb.append((StringUtils.isEmpty(sequence) ? "" : sequence) + ",");
		sb.append("\"" + (StringUtils.isEmpty(notifiedTime) ? "" : notifiedTime) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(ipAddress) ? "" : ipAddress) + "\",");
		sb.append((StringUtils.isEmpty(eventNo) ? "" : eventNo) + ",");
		sb.append("\"" + (StringUtils.isEmpty(httpReferrer) ? "" : httpReferrer) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(browser) ? "" : browser) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(eventStr1) ? "" : eventStr1) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(eventStr2) ? "" : eventStr2) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(eventStr3) ? "" : eventStr3) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(athensId) ? "" : athensId) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(shibbolethId) ? "" : shibbolethId) + "\",");
		sb.append((StringUtils.isEmpty(memberId) ? "-1" : memberId) + ",");
		sb.append("\"" + (StringUtils.isEmpty(debug) ? "" : debug) + "\",");
		sb.append("\"" + (StringUtils.isEmpty(ipOrgs) ? "" : ipOrgs) + "\",");
		sb.append((StringUtils.isEmpty(autoLoginId) ? "" : autoLoginId) + "\n");
		
		return sb.toString().getBytes();
	}
}
