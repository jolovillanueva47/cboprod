package org.cambridge.ebooks.online.internal_reports;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.society.autologin.SocietyAutoLoginWorker;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class InternalReportsWorker {
	private static final Logger LOGGER = Logger.getLogger(InternalReportsWorker.class);
	
	public static final String ADVANCE_SEARCH_PARAM = "advanceSearchParam"; 
	
	private static final String search_text = "search_text";
		
	private static final String search_text2 = "search_text_2";	
	
	private static final String LOCATION10056 = "LOCATION10556";	
	
	private static final String bid = "bid";
	private static final String cid = "cid";
	private static final String subjectCode = "subjectCode";
	private static final String subjectId = "subjectId";
	private static final String seriesCode = "seriesCode";
	private static final String isbn = "isbn";
	
	private static final String searchWords = "searchText";
	
	public static void listenEvent(HttpServletRequest request, HttpServletResponse response){		
		String servletPath = request.getServletPath();
		LOGGER.info("=================================Start InternalReportWorker Log============================= ");
		LOGGER.info("servletPath = " + servletPath);
		int logType = getLogType(servletPath, request);		
		LOGGER.info("LogType = " + logType);
		if(logType != InternalEventEnum.INVALID.getEventNum() 
				&& notIeThickboxBug(request, logType )){
			String eventStr1 = getEventStr1(request, logType);
			String eventStr2 = getEventStr2(request, logType);
			
			InternalReportsBean bean = new InternalReportsBean(request, logType, eventStr1, eventStr2);
			LOGGER.info("athensID >> " +bean.getAthensId());
			LOGGER.info("autoLoginId >> " +bean.getAutologinId());
			LOGGER.info("browser >> " +bean.getBrowser());
			LOGGER.info("debug >> " +bean.getDebug());
			LOGGER.info("directBodyId >> " +bean.getDirectBodyId());
			LOGGER.info("eventNo >> " +bean.getEventNo());
			LOGGER.info("event str 1 >> " +bean.getEventStr1());
			LOGGER.info("event str 2 >> " +bean.getEventStr2());
			LOGGER.info("event str 3 >> " +bean.getEventStr3());
			LOGGER.info("httpReferrer >> " +bean.getHttpReferrer());
			LOGGER.info("ipAddress >> " +bean.getIpAddress());
			LOGGER.info("ipOrgs >> " +bean.getIpOrgs());
			LOGGER.info("memberID >> " +bean.getMemberId());
			LOGGER.info("sequence >> " +bean.getSequence());
			LOGGER.info("sessionId >> " +bean.getSessionId());
			LOGGER.info("shibbolethId >> " +bean.getShibbolethId());
			LOGGER.info("userId >> " +bean.getUserId());
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSSSSSSSS a");	
			LOGGER.info("notifiedTime >> " + formatter.format(new Date()));
			
			try {
				InternalReportsBackupLogger.createBackupFile(bean);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
			
			if(isSearchValid(logType, eventStr2) && notFromLogout(request)  && validEvent234(logType, eventStr1) && validEvent9(logType, eventStr2)){
				if( notFromServerStartup(bean) && isLoginValid(request, logType, bean)){	
					InternalReportsLogger.logEvent(bean, request);					
				}				
			}
		}
		LOGGER.info("=================================End InternalReportWorker Log============================= ");
	}
	
	
	
	/*
	 * Look at the referrer to determine if it is a thickbox request 
	 */
	private static boolean isThickboxRequest(HttpServletRequest request){
		boolean isThickBoxRequest = false;
		String referrer = request.getHeader(InternalReportsBean.Referrer);
		if(referrer != null && referrer.contains(LOCATION10056)){
			isThickBoxRequest = true;
		}
		return isThickBoxRequest;
	}
	
	/*
	 * reported by Nik Louch 0067740: Minor issue with usage recording of homepage hits
	 * ie submits home logs whenever thickbox login is called 
	 */
	private static boolean notIeThickboxBug(HttpServletRequest request, int logType){
		boolean pass = true;
		boolean isThickboxRequest = isThickboxRequest(request);		
		if(logType == 1 && isThickboxRequest){
			pass = false;		
		}		
		return pass;
	}
	
	/*
	 * events 2,3,4 should return a non empty eventStr1
	 */
	private static boolean validEvent234(int logType, String eventStr1){		
		if( logType == 2 || logType == 3 || logType ==4 ){
			if(StringUtils.isEmpty(eventStr1)){
				return false;
			}
			return true;
		}
		return true;
	}
	
	/*
	 * event 9 should not return a non empty eventStr1
	 */
	private static boolean validEvent9(int logType, String eventStr2){
		if( logType == InternalEventEnum.SEARCH_ADVANCE.getEventNum() ){
			if(StringUtils.isEmpty(eventStr2)){
				return false;
			}
			return true;
		}
		return true;
	}
	
	/*
	 * startup runs the logger for java
	 */
	private static boolean notFromServerStartup(InternalReportsBean bean){
		if(!(bean.getBrowser().toLowerCase().indexOf("java") == 0)){
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * logout has redirect option, will not log the first, but will log the 2nd
	 */
	private static boolean notFromLogout(HttpServletRequest request){
		if(!("y".equals(request.getParameter("lo")) || "y".equals(request.getParameter("slo")) || "y".equals(request.getParameter("alo")) )){
			return true;
		}
		
		return false;
	}
	
	/*
	 * event_str2 must not be null if event is search
	 */
	private static boolean isSearchValid(int logType, String eventStr2){
		if((InternalEventEnum.SEARCH_QUICK.getEventNum() != logType ||
			InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum() != logType ||
			InternalEventEnum.SEARCH_ADVANCE.getEventNum() != logType) ||
				((InternalEventEnum.SEARCH_QUICK.getEventNum() == logType ||
						InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum() == logType ||
						InternalEventEnum.SEARCH_ADVANCE.getEventNum() == logType)
							&& 	eventStr2 != null )){
			return true;
		}
		
		return false;
	}
	
	/*
	 * 
	 */
	private static boolean isLoginValid(HttpServletRequest request, int logType, InternalReportsBean bean){
		//should check only if login type
		if(logType == InternalEventEnum.USER_LOGIN.getEventNum()){
			if(bean.getMemberId() != -1){
				return true;
			}else{
				return false;
			}
		}
		//otherwise return true
		return true;
	}
	
	private static int getLogType(String servletPath, HttpServletRequest request){
		/*Athens and shibboleth are at the top because they can be mistaken as event 1 because they go to home page*/
		/* order is important here */
		if(isAthens(request)){
			return InternalEventEnum.ATHENS_LOGIN.getEventNum();
		}else if(isShibb(request)){
			return InternalEventEnum.SHIBB_LOGIN.getEventNum();
		}else if(isSocAutoLogin(request)){
			return InternalEventEnum.SOCIETY_AUTOLOGIN.getEventNum();
		}else if(servletPath.contains("/home.jsf")){
			return InternalEventEnum.VIEW_HOME.getEventNum();
		}else if(servletPath.contains("/ebook.jsf")){
			String ref = request.getParameter("ref");
			if(ref == null){
				return InternalEventEnum.VIEW_TITLE.getEventNum();
			}
		}else if(servletPath.contains("/chapter.jsf")){
			return InternalEventEnum.VIEW_CHAPTER.getEventNum();
		}else if(servletPath.contains("/subject_landing.jsf")){
			String searchType = request.getParameter("searchType");
			if("allTitles".equals(searchType) || "allSubjectBook".equals(searchType)){
				String subjAlphalist = request.getParameter("sa");
				if(subjAlphalist == null){
					return InternalEventEnum.VIEW_SUBJECT.getEventNum();
				}
			}
		}else if(servletPath.contains("/series_landing.jsf") || servletPath.contains("/all_series.jsf")){			
			String seriesCode = request.getParameter("seriesCode");
			if(seriesCode != null ){
				return InternalEventEnum.VIEW_SERIES.getEventNum();
			}			
		}else if(servletPath.contains("/open_pdf")){
			return InternalEventEnum.DOWNLOAD_PDF.getEventNum();
		}else if(servletPath.contains("/search_results")){
			//beware of regex
			String searchType = request.getParameter("searchType");
			String searchWithinContent = request.getParameter("searchWithinContent");

			if(searchType.equals("quick")){
				if(searchWithinContent == null){
					return InternalEventEnum.SEARCH_QUICK.getEventNum();
				}else{
					return InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum();
				}
			}else if(searchType.equals("advance")){
				return InternalEventEnum.SEARCH_ADVANCE.getEventNum();
			}
		}else if(servletPath.contains("/login")){
			return InternalEventEnum.USER_LOGIN.getEventNum();
		} else if(servletPath.contains("concurrency_error.jsf")) {
			return InternalEventEnum.TURNAWAYS.getEventNum();
		}
		return InternalEventEnum.INVALID.getEventNum();		
	}
	
	private static boolean isSocAutoLogin(HttpServletRequest request){
		String query = request.getQueryString();
		if(query == null){
			return false;
		}
		if(query.contains("autologinId") && 
				SocietyAutoLoginWorker.addAutlogin.equals(request.getAttribute(SocietyAutoLoginWorker.addAutlogin))){
			request.setAttribute(SocietyAutoLoginWorker.addAutlogin, "");
			return true;
		}
		return false;
	}
	
	private static boolean isAthens(HttpServletRequest request){
		String athensId = 		request.getParameter("ath_id");
		String athensUsername = request.getParameter("ath_user");
		if(StringUtils.isNotEmpty(athensId) && StringUtils.isNotEmpty(athensUsername)){
			return true;
		}		
		return false;
	}
	
	
	private static boolean isShibb(HttpServletRequest request){
		String shibbId = request.getParameter("shib_id");
		if(StringUtils.isNotEmpty(shibbId)){
			return true;
		}
		return false;
	}
	
	
	private static String getEventStr1(HttpServletRequest request, int logType){
		if(InternalEventEnum.VIEW_HOME.getEventNum() == logType ||
				InternalEventEnum.SEARCH_QUICK.getEventNum() == logType ||
				InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum() == logType ||
				InternalEventEnum.SEARCH_ADVANCE.getEventNum() == logType){
			if(InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum() == logType){		
				String bid = request.getParameter(InternalReportsWorker.bid);
				return getIsbn(bid);
			}else{
				return null;
			}			
		}else if(InternalEventEnum.VIEW_TITLE.getEventNum() == logType){
			//bid might be null on facebook bcid instead of bid
			String bid = request.getParameter(InternalReportsWorker.bid);
			return getIsbn(bid);			
		}else if(InternalEventEnum.VIEW_CHAPTER.getEventNum() == logType){
			//cid might be null on facebook bcid instead of cid
			String cid = request.getParameter(InternalReportsWorker.cid);
			if(StringUtils.isEmpty(cid)){
				return null;
			}
			return cid;			
		}else if(InternalEventEnum.DOWNLOAD_PDF.getEventNum() == logType){
			String cid = request.getPathInfo().substring(1);
			if(StringUtils.isEmpty(cid)){
				return null;
			}
			return cid;			
		}else if(InternalEventEnum.VIEW_SUBJECT.getEventNum() == logType){
			String sc = request.getParameter(subjectCode);
			if(sc == null){
				sc = request.getParameter(subjectId);
			}
			return sc == null ? sc : sc.toUpperCase();
		}else if(InternalEventEnum.VIEW_SERIES.getEventNum() == logType){		
			String sc = request.getParameter(seriesCode);			
			return sc == null ? sc : sc.toUpperCase();
		} else if(InternalEventEnum.TURNAWAYS.getEventNum() == logType) {
			String isbn = request.getParameter(InternalReportsWorker.isbn);
			
			if(StringUtils.isEmpty(isbn)) {
				isbn = null;
			}
			
			return isbn;
		} else{
			return null;
		}
	}
	
	private static String getIsbn(String bid) {				
		return SolrUtil.getIsbnByBookId(bid);
	}
	
	@SuppressWarnings("unchecked")
	private static String getEventStr2(HttpServletRequest request, int logType){
		String sw = "";
		if(InternalEventEnum.SEARCH_QUICK.getEventNum() == logType ||
			InternalEventEnum.SEARCH_QUICK_WITHIN_BOOK.getEventNum() == logType	){
			sw = request.getParameter(searchWords);						
		}else if(InternalEventEnum.SEARCH_ADVANCE.getEventNum() == logType){			
			//do advanced search param
			HttpSession session = request.getSession();
			Map<String, String[]> map = (Map<String, String[]>)session.getAttribute(ADVANCE_SEARCH_PARAM);
			String[] searchText = map.get(search_text);			
			String[] searchText2 = map.get(search_text2);
			List<String> searchAll = new ArrayList<String>();
			if(searchText != null){
				searchAll.addAll(Arrays.asList(searchText));
			}
			if(searchText2 != null){
				searchAll.addAll(Arrays.asList(searchText2));
			}
			String result = StringUtil.join(searchAll, " ");
			
			return result;
									
		}		
		return StringUtils.isNotEmpty(sw) ? sw.trim() : null ;
	}
	
	public static void main(String[] args) {
		
	}
	
	
}

