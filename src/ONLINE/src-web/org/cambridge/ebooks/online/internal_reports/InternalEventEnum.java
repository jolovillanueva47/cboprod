package org.cambridge.ebooks.online.internal_reports;

public enum InternalEventEnum {
	INVALID(-1, "Invalid Event"),
	VIEW_HOME(1, "View Home page"),
	VIEW_TITLE(2, "View Book Landing"),
	VIEW_CHAPTER(3, "View Chapter Landing"),
	DOWNLOAD_PDF(4, "Download PDF"),
	VIEW_SUBJECT(5, "View Subject Landing"),
	VIEW_SERIES(6, "View Series Landing"),
	SEARCH_QUICK_WITHIN_BOOK(7, "Quick Search Within Book"),
	SEARCH_QUICK(8, "Quick Search Not Within Book"),
	SEARCH_ADVANCE(9, "Advanced Search"),
	ATHENS_LOGIN(10, "Athens Login"),
	SHIBB_LOGIN(11, "Shibboleth Login"),
	USER_LOGIN(12, "User Login"),
	INVALID_ATHENS_LOGIN(13, "Invalid Athens Login"),
	INVALID_SHIBB_LOGIN(14, "Invalid Shibboleth Login"),
	SOCIETY_AUTOLOGIN(15, "Society Autologin"),
	TURNAWAYS(16, "Turnaways");
	
	
	
	private int eventNum;	
	private String description;
	
	InternalEventEnum(int eventNum, String description) {
		this.eventNum = eventNum;
		this.description = description;
	}
	
	public int getEventNum() {
		return eventNum;
	}

	public void setEventNum(int eventNum) {
		this.eventNum = eventNum;
	}
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static void main(String[] args) {
		System.out.println(InternalEventEnum.VIEW_HOME.name());
	}
}
