package org.cambridge.ebooks.online.login;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class LoginManager {
	private static Logger LOGGER = LogManager.getLogger(LoginManager.class);
	
	public static String loginRedirect(HttpServletRequest request, String username, String password){
		if(request == null){
			return login(username, password);
		}else{
			return loginDirectInfo(request, username, password);
		}
	}
	public static String login(String username, String password){
		Login login = findUser(username, password);
		if(login == null){
			return "Invalid";
		}
		
		if(!isUserPassValid(login, password)){
			return "Invalid";
		}		
		
		//get additional details
		getAdminDetails(login);
		
		//get memberships
		getMemberships(login);
		
		
		return login.toString();
	}
	
	
	
	public static String loginDirectInfo(HttpServletRequest request, String username, String password){
		Login login = findUser(username, password);
		if(login == null){
			return "Invalid";
		}
		
		if(!isUserPassValid(login, password)){
			return "Invalid";
		}		
		
		//set user in session
		setUserInSession(login, request);		
		
		//get additional details
		getAdminDetails(login);
		
		//get memberships
		getMembershipsUnconcat(login, request);
		
		
		//order is important, last is the main org
		//for authentication with direct advice
		LOGGER.info( "getting CJO Login Details init -----"  + login.getMemberId() );
		LOGGER.info( "getting CJO Login Details init -----"  + login.getMemberUserId() );
		LOGGER.info( "getting CJO Login Details init -----"  + login.getUser() );
		LOGGER.info( "getting CJO Login Details -----"  + login.toString() );
		OrgConLinkedMap.addOrgFromLogin(login, request);	
		
		
		return login.toString();
	}
	
	private static void setUserInSession(Login login, HttpServletRequest request){
		HttpSession session = request.getSession();
		OrgConLinkedMap map = OrgConLinkedMap.getFromSession(session);
		map.setUserLogin(login);
		
		//additional used by other jsps, i do not want to edit anymore jsps, hence i placed it in the same location in session
		User user = login.getUser();
		session.setAttribute("userInfo", user);
	}
	
	
	private static void getAdminDetails(Login login){
		//Login _login = PersistenceUtil.searchEntity(new Login(), Login.ADMIN, login.getMemberUserId() );
		Login _login = null;
		
		Vector<Login> _list = PersistenceUtil.searchListVector(new Login(), Login.ADMIN, login.getMemberUserId() );
		
		//get the default admin
		for(Login l : _list){
			if("true".equals(l.getDefaultFlag())){
				_login = l;
			}
		}
		
		//if no default admin then just choose 1 // get last to match journals
		if(_login == null && _list.size() > 0){
			
			_login = _list.get(_list.size()-1);
		}		
		
		//if login is still null, then no login
		if(_login == null){
			return;
		}
		
		login.setOrgBodyId(_login.getOrgBodyId());
		login.setBodyType(_login.getBodyType());
	}
	
	private static void getMemberships(Login login){
		LOGGER.info("-----getMemberships");
		LOGGER.info("org body id:" + login.getOrgBodyId());
		LOGGER.info("member user id:" + login.getMemberUserId());
		
		Login _login = PersistenceUtil.searchEntity(new Login(), Login.MEMBERSHIP, 
				login.getOrgBodyId(), login.getMemberUserId() );
		
		
		
		if(_login == null){
			return;
		}
		
		login.setOrgList(_login.getOrgList());
	}
	
	private static void getMembershipsUnconcat(Login login, HttpServletRequest request){
		LOGGER.info("-----getMembershipsUnconcat");
		LOGGER.info("org body id:" + login.getOrgBodyId());
		LOGGER.info("member user id:" + login.getMemberUserId());
		
		Vector<Organisation> orgVector = (Vector<Organisation>) PersistenceUtil.searchListVector(new Organisation(), Login.MEMBERSHIP_UNCONCAT, 
				login.getOrgBodyId(), login.getMemberUserId() );
		
		ArrayList<Organisation> orgList = new ArrayList<Organisation>();
		for(Organisation org : orgVector){
			orgList.add(org);
			org.setMembershipOrg(true);
		}
			
//		if(orgList == null){
//			return;
//		}
//		
//		OrgConLinkedMap.addOrgFromMembership(orgList, request);		
//		//OrgConLinkedMap map = (OrgConLinkedMap) request.getSession().getAttribute(OrgConLinkedMap.ORG_LINKED_MAP);
//		login.setOrgList(getOrgListBodyIds(orgList));
		if(orgList != null){
			OrgConLinkedMap.addOrgFromMembership(orgList, request);		
			//OrgConLinkedMap map = (OrgConLinkedMap) request.getSession().getAttribute(OrgConLinkedMap.ORG_LINKED_MAP);
			login.setOrgList(getOrgListBodyIds(orgList));
		}
	}
	
	
	
	private static Login findUser(String username, String password){
		return PersistenceUtil.searchEntity(new Login(), Login.LOGIN, username  );				
	}
	
	private static boolean isUserPassValid(Login login, String password){
		return UnixCrypt.matches(login.getMemberPassword(), password) 
			|| password.equals(login.getMemberPassword());
	}
	
	public static String getOrgListBodyIds(ArrayList<Organisation> orgList){
		StringBuffer sb = new StringBuffer();	
		for(Organisation org : orgList){
			String key = org.getBodyId();
			sb.append(key + ",");			
		}
		int length = sb.length();
		if(length > 0){
			String bodyIds = sb.toString().substring(0, length-1);
			return bodyIds;
		}		
		return "";
	}

		
	public static void main(String[] args) {
		//login("mpakyaw", "nt2tFyM8b3DaQ");	
		
		
	}
}
