package org.cambridge.ebooks.online.login.authentication;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;

public class OrgConLoginWorker {
	
	private static final Logger LOGGER = Logger.getLogger(OrgConLoginWorker.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	private MapOrgConProper ocmap;
	
	public OrgConLoginWorker(MapOrgConProper map){
		this.ocmap = map;
	}
	
	public void addOrgs(String bodyIds){
		addOrgs(bodyIds, null);
	}
	
	@SuppressWarnings("unchecked")
	public List<Organisation> addOrgs(String bodyIds, String direct){
		LOGGER.info("----addOrgs");
		
		List<Organisation> orgList = new ArrayList<Organisation>();
		String sql = 
			"select BODY_ID, NAME, DISPLAY_NAME, 2 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.ORGANISATION " +
			"where BODY_ID in (" + bodyIds + ") " +
			"union " +
			"select BODY_ID, NAME, DISPLAY_NAME, 1 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.CONSORTIUM " +
			"where BODY_ID in (" + bodyIds + ") ";
		
		LOGGER.info("sql:" + sql);		
		
		EntityManager em = emf.createEntityManager();
		try{
			Query query = em.createNativeQuery(sql, Organisation.class);
			orgList = query.getResultList();
			for(Organisation org : orgList){	
				org.setAutologin(true);
				org.setDirect(direct);
				ocmap.put(org.getBodyId(), org);
			}						
		}finally{
			em.close();
		}
		return orgList;
	}
	
	public void populateFromDb(Organisation org){
		String sql = 
				"select BODY_ID " +
				"from ORACJOC.ORGANISATION " +
				"where BODY_ID = ?1 " +
				"union " +
				"select BODY_ID " +
				"from ORACJOC.CONSORTIUM " +
				"where BODY_ID = ?1 ";
		
		EntityManager em = emf.createEntityManager();
		try{
			Query query = em.createNativeQuery(sql, Organisation.class);
			query.setParameter(1, org.getBodyId());
			try{
				Organisation _org = (Organisation) query.getSingleResult();
				
				//manually populate orgs and consortium
				org.setConsortium(_org.getConsortum());
				org.setOrganisation(_org.getOrganisation());
			}catch(NoResultException e){
				LOGGER.error(e.getMessage());
			}
		}finally{
			em.close();
		}		
	}
	
	public static void populateConsOrgMap(String bodyIdsStr, Map<String, List<ConsortiumOrganisation>> map) {
		LOGGER.info("-----populateConsOrgMap");		
		if(StringUtils.isEmpty(bodyIdsStr)) {
			LOGGER.info("empty body id");
			return;
		}
		
		StringBuilder qBuilder = new StringBuilder();		
	
		qBuilder.append("select ORGANISATION_ID ,CONSORTIUM_ID, OVERRIDE_CONSORTIA_LOGO, OVERRIDE_CONSORTIA_LOGO_NAME ")
			.append("from ORACJOC.CONSORTIUM_ORGANISATION ")
			.append("where ORGANISATION_ID in (").append(bodyIdsStr).append(")");
		
		EntityManager em = emf.createEntityManager();
		
		LOGGER.info("sql:" + qBuilder);		
		
		try {
			Query query = em.createNativeQuery(qBuilder.toString(), ConsortiumOrganisation.class);		
			List<ConsortiumOrganisation> consOrgList = query.getResultList();
			for(ConsortiumOrganisation consOrg : consOrgList) {
				addConsortiumOrganisationToMap(map, consOrg.getOrganisationId(), consOrg);
			}
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
		
	}
	
	public static String getBodyIdsAsString(List<Organisation> orgList) {
		StringBuilder bodyIdsBuilder = new StringBuilder();
		int ctr = 0;
		for(Organisation org : orgList) {
			ctr++;
			bodyIdsBuilder.append(org.getBodyId());
			if(ctr < orgList.size()) {
				bodyIdsBuilder.append(",");
			}
		}		
		
		return bodyIdsBuilder.toString();
	}
	
	private static void addConsortiumOrganisationToMap(Map<String, List<ConsortiumOrganisation>> map, String bodyId, ConsortiumOrganisation consOrg) {		
		if(map.containsKey(bodyId)) {
			map.get(bodyId).add(consOrg);
		} else {
			List<ConsortiumOrganisation> consOrgList = new ArrayList<ConsortiumOrganisation>();
			consOrgList.add(consOrg);
			map.put(bodyId, consOrgList);
		}
	}
}
