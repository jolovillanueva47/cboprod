package org.cambridge.ebooks.online.login.authentication;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.organisation.OrganisationBase;
import org.cambridge.ebooks.online.jpa.organisation.OrganisationTypeBase;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.util.HttpUtil;



/**
 * 
 * @author mmanalo
 *
 */
public class OrgConLinkedMap {

	private static final long serialVersionUID = 7516397395128124302L;
	
	private static final Logger LOGGER = Logger.getLogger(OrgConLinkedMap.class);
	
	public static final String ORG_LINKED_MAP = "orgConLinkedMap";
	
	public static final String CONS_ORG_MAP = "consortiumOrganisationMap";
		
	private Login userLogin;	
	
	private String athensId;
	
	private String shibbolethId;
	
	private String autologinId;
	
	private String currentlyLoggedBodyId;
			
	private MapOrgConProper map = new MapOrgConProper(); 
	
	/* static methods */	
		
	public static void addOrgFromLogin(Login login, HttpServletRequest request){		
		String bodyId = login.getOrgBodyId();
		LOGGER.info("Login org " + login.getOrgBodyId());
		if(StringUtils.isNotEmpty(bodyId)){			
			HttpSession session = request.getSession();
			OrgConLinkedMap map = getFromSession(session);			
			OrgConLoginWorker worker = new OrgConLoginWorker(map.getMap());
			
			//login is direct
			worker.addOrgs(bodyId, "Y");	
			
			//consortium organisation
			Map<String, List<ConsortiumOrganisation>> consOrgMap = getConsOrgFromSession(session);
			OrgConLoginWorker.populateConsOrgMap(bodyId, consOrgMap);			
				
			map.setCurrentlyLoggedBodyId(bodyId);			
		}
	}	
	
	public static void addOrgFromMembership(ArrayList<Organisation> orgList, HttpServletRequest request){
		HttpSession session = request.getSession();
		OrgConLinkedMap map = getFromSession(session);
		map.appendList(orgList);
	}
	
	public static void addOrgFromAthensOrShibb(List<Organisation> orgList, HttpServletRequest request, boolean isAthens, String id){		
		if(orgList != null){
			HttpSession session = request.getSession();
			OrgConLinkedMap map = getFromSession(session);
			if(isAthens){
				map.setAthensId(id);
			}else{
				map.setShibbolethId(id);
			}			
			map.setCurrentlyLoggedBodyId(orgList.get(0).getBodyId());			
			map.appendListSetDirect(orgList, "Y");
		}
	}
		
	public static Set<String> getAllBodyIdsAsSet(HttpSession session){
		if(session != null){
			OrgConLinkedMap map = getFromSession(session);
			return map.getAllBodyIdsAsSet();
		}
		return null;	
	}

	public static String getAllBodyIds(HttpSession session){
		if(session != null){
			OrgConLinkedMap map = getFromSession(session);
			return map.getAllBodyIds();
		}
		return null;	
	}
	
	public static String getDirectBodyIds(HttpSession session){		
		if(session != null){
			OrgConLinkedMap map = getFromSession(session);
			return map.getAllDirectBodyIds();
		}
		return null;
	}
	
	public static OrgConLinkedMap getFromSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		return getFromSession(session);
	}
	
	public static OrgConLinkedMap getFromSession(HttpSession session){
		if(session == null){
			session = HttpUtil.getHttpSession(false);
		}
		Object obj = session.getAttribute(ORG_LINKED_MAP);		
		if(obj == null){
			OrgConLinkedMap map = new OrgConLinkedMap();
			session.setAttribute(ORG_LINKED_MAP, map);
			return map;			
		}
		return (OrgConLinkedMap) obj;
	}	

	public static Map<String, List<ConsortiumOrganisation>> getConsOrgFromSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		return getConsOrgFromSession(session);
	}
	
	public static Map<String, List<ConsortiumOrganisation>> getConsOrgFromSession(HttpSession session){
		Map<String, List<ConsortiumOrganisation>> consOrgMap = new HashMap<String, List<ConsortiumOrganisation>>();
		
		if(session == null){
			session = HttpUtil.getHttpSession(false);
		}
		
		Object obj = session.getAttribute(CONS_ORG_MAP);		
		if(obj == null){
			session.setAttribute(CONS_ORG_MAP, consOrgMap); 
		} else {
			consOrgMap = (Map<String, List<ConsortiumOrganisation>>)obj;
		}
		
		return consOrgMap;
	}	
	
	
	/* public methods */	
	
	public String getCurrentlyLoggedDisplayName(){
		return getCurrentlyLoggedBaseProperty("displayName");
	}
	
	public String getCurrentlyLoggedBaseProperty(String prop){
		Organisation org = getCurrentlyLoggedOrganisation();
		if(org != null){
			return org.getBaseProperty(prop);
		}
		return null;
	}
	
	
	public Organisation getCurrentlyLoggedOrganisation(){		
		String orgBodyId = this.currentlyLoggedBodyId;
		if(StringUtils.isNotEmpty(orgBodyId)){		
			for(String key : this.map.keySet()){
				Organisation org = this.map.get(key);
				if(orgBodyId.equals(org.getBodyId())){
					return org;
				}
			}
		}
		return null;
	}
	
	public String[] getMembershipBodyIdsAsArray(){
		List<Organisation> orgList = getMembershipOrgs();
		String[] orgIds = new String[orgList.size()];
		int i = 0;
		for(Organisation org : orgList){
			orgIds[i] = org.getBodyId();
			i++;
		}		
		return orgIds;
	}
	
	public List<Organisation> getMembershipOrgs(){
		List<Organisation> orgList = new ArrayList<Organisation>();
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			if(org.isMembershipOrg()){
				orgList.add(org);
			}
		}
		return orgList;
	}
	
	public List<Organisation> getIpAuthenticatedOrgs(){
		List<Organisation> orgList = new ArrayList<Organisation>();
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			if(org.isIpAuthenticated()){
				orgList.add(org);
			}
		}
		return orgList;
	}
	
	public List<Organisation> getAllConsortium(){
		LOGGER.info("----getAllConsortium");
		List<Organisation> orgList = new ArrayList<Organisation>();
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			if(org.getConsortum() != null){
				orgList.add(org);
				LOGGER.info("bodyId:" + orgId);
			}
		}
		return orgList;
	}
	
	public Map<String, Organisation> getConsortiaMap() {
		Map<String, Organisation> consMap = new HashMap<String, Organisation>();
		
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			if(org.getConsortum() != null){
				consMap.put(org.getBodyId(), org);
			}
		}
		
		return consMap;		
	}
	
	public List<Organisation> getAllOrganisation(){
		List<Organisation> orgList = new ArrayList<Organisation>();
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			OrganisationBase base = org.getOrganisation();
			OrganisationTypeBase type = null;
			if(base != null){
				type = base.getOrganisationType();
			}			
			if(base != null && type != null && !type.isSociety()){
				orgList.add(org);
			}
		}
		return orgList;
	}
	
	public Map<String, Organisation> getOrganisationMap() {
		Map<String, Organisation> orgMap = new HashMap<String, Organisation>();
		
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			OrganisationBase base = org.getOrganisation();
			OrganisationTypeBase type = null;
			if(null != base){
				type = base.getOrganisationType();
			}			
			if(null != base && null != type && !type.isSociety()){
				orgMap.put(org.getBodyId(), org);
			}
		}
		
		return orgMap;		
	}
	
	public List<Organisation> getAllSociety(){
		List<Organisation> orgList = new ArrayList<Organisation>();
		for(String orgId : this.map.keySet()){
			Organisation org = this.map.get(orgId);
			if(org.getOrganisation() != null){
				OrganisationTypeBase orgType = org.getOrganisation().getOrganisationType();
				if(orgType != null && orgType.isSociety()){
					orgList.add(org);
				}
			}
		}
		return orgList;
	}
	
	
	
	public Set<String> getAllBodyIdsAsSet(){
		return this.map.keySet();
	}
	
	public String getAllBodyIds(){
		StringBuffer sb = new StringBuffer();	
		for(String key : this.map.keySet()){
			sb.append(key + ",");			
		}
		int length = sb.length();
		if(length > 0){
			String bodyIds = sb.toString().substring(0, length-1);
			return bodyIds;
		}		
		return "";
	}
	
	public String getAllDirectBodyIds(){
		StringBuffer sb = new StringBuffer();	
		for(String key : this.map.keySet()){
			Organisation org = this.map.get(key);
			if("Y".equals(org.getDirect())){
				sb.append(key + ",");
			}						
		}
		int length = sb.length();
		if(length > 0){
			String bodyIds = sb.toString().substring(0, length-1);
			return bodyIds;
		}		
		return "";
	}
	
	public String getIpAuthenticatedBodyIds(){
		StringBuffer sb = new StringBuffer();	
		for(Organisation org : getIpAuthenticatedOrgs()){			
			if(org.isIpAuthenticated()){
				sb.append(org.getBodyId() + ",");
			}						
		}
		int length = sb.length();
		if(length > 0){
			String bodyIds = sb.toString().substring(0, length-1);
			return bodyIds;
		}		
		return "";
	}
		
	public void appendList(List<Organisation> orgList){
		for(Organisation org : orgList){
			this.map.put(org.getBodyId(), org);	
		}
	}
	
	public void appendListSetDirect(List<Organisation> orgList, String direct){
		for(Organisation org : orgList){
			org.setDirect(direct);
			this.map.put(org.getBodyId(), org);	
		}
	}
	
	public Organisation getFirst(){
		for(String key : this.map.keySet()){
			return this.map.get(key);	
		}
		return null;
	}
	

	
	public Organisation put(String key, Organisation value) {		
		return this.map.put(key, value);
	}
	
	
	
	/* getters / setters */
	
	public Login getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(Login login) {
		this.userLogin = login;
	}

	public boolean isUserLoggedIn(){
		if(this.userLogin != null && StringUtils.isNotEmpty(this.userLogin.getMemberUserId())){
			return true;
		}
		return false;
	}

	public String getAthensId() {
		return athensId;
	}

	public void setAthensId(String athensId) {
		this.athensId = athensId;
	}

	public String getShibbolethId() {
		return shibbolethId;
	}

	public void setShibbolethId(String shibbolethId) {
		this.shibbolethId = shibbolethId;
	}
	
	public boolean isAthensLoggedIn(){
		if(StringUtils.isNotEmpty(athensId)){
			return true;
		}
		return false;
	}
	
	public boolean isShibbolethLoggedIn(){
		if(StringUtils.isNotEmpty(shibbolethId)){
			return true;
		}
		return false;
	}

	public String getCurrentlyLoggedBodyId() {
		return currentlyLoggedBodyId;
	}

	public void setCurrentlyLoggedBodyId(String currentlyLoggedBodyId) {
		this.currentlyLoggedBodyId = currentlyLoggedBodyId;
	}

	public MapOrgConProper getMap() {
		return map;
	}

	public void setMap(MapOrgConProper map) {
		this.map = map;
	}

	public String getAutologinId() {
		return autologinId;
	}

	public void setAutologinId(String autologinId) {
		this.autologinId = autologinId;
	}	
}
