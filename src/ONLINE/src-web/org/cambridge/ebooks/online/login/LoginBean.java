//package org.cambridge.ebooks.online.login;
//
//import java.io.IOException;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.component.UIComponent;
//import javax.faces.component.UIInput;
//import javax.faces.context.FacesContext;
//import javax.faces.validator.ValidatorException;
//import javax.servlet.http.HttpSession;
//
//import org.cambridge.ebooks.online.jpa.organisation.Organisation;
//import org.cambridge.ebooks.online.jpa.user.User;
//import org.cambridge.ebooks.online.organisation.OrganisationWorker;
//import org.cambridge.ebooks.online.user.UserWorker;
//import org.cambridge.ebooks.online.util.HttpUtil;
//import org.cambridge.ebooks.online.util.LogManager;
//import org.cambridge.ebooks.online.util.PersistenceUtil;
//import org.cambridge.ebooks.online.util.StringUtil;
//
///**
// * 
// * @author rvillamor
// *
// */
//
//public class LoginBean {
//	
//	private User user = new User();
//	
//	private UIInput inputUsername;
//	
//	private UIInput inputPassword;
//	
//	private boolean withError;
//	
////	private String login;
//	
//	
//	public void setLogin(String login) {
//		login();
//	}
//	
//	public void login() {
//					
//		LogManager.info( this, " -------------------------------------------  Start User login  " );
//		
//		boolean validUsernamePassword = StringUtil.isNotEmpty( user.getUsername() ) && StringUtil.isNotEmpty( user.getPassword() );
//		
//		if ( validUsernamePassword )  { 
//			try { 
//				//populate value object if user is authenticated
//				user = findUser( user.getUsername(), user.getPassword() );
//				
//				LogManager.info( this, " -------------------------------------------  Found user  " + user.getUsername() ); 
//				HttpSession session = HttpUtil.getHttpSession(true);
//				
//				UserWorker.setUserInSession( session , user );
//				UserWorker.recordLoginTime(session);
//				
//				LogManager.info( this, " ------------------------------------------- " + user.getUsername() + " has logged in" );
//				
//				doLogin( FacesContext.getCurrentInstance() );
//				
//			} catch ( Exception ex ) { 
//				FacesContext context = FacesContext.getCurrentInstance();
//				FacesMessage message = new FacesMessage("Invalid Username/Password.");
//				context.addMessage("loginMessages", message);
//			}
//			
//		} else { 
//			withError = true;			
//			HttpUtil.getHttpServletRequest().setAttribute("loginMessages", "Username/Password required");
//			
//		}
//		
//		
//	}
//	
//	
//	public void doLogin( FacesContext facesContext ) { 
//		try {
//			String url = HttpUtil.getHeaderValue("Referer");
//			if ( url.indexOf("lo=y") > -1 || url.indexOf("login") > -1)  
//				url = "home.jsf";
//			
//			facesContext.getExternalContext().redirect(url);
//			 
//		} catch ( IOException ioe ) { 
//			LogManager.error( this, "Ooopsss failed to refresh page!");
//		}
//	}
//	
//	
//	public User findUser( final String username,final String password ) {
//		User userInfo = PersistenceUtil.searchEntity(new User(), User.SEARCH_BLADE_MEMBER_BY_USERNAME, username );
//		
//		if(StringUtil.isNotEmpty(userInfo) && StringUtil.isNotEmpty(userInfo.getBodyId()))
//			userInfo = UserWorker.findUserByBodyId(userInfo.getBodyId());
//		
//		return userInfo;
//	}
//	
//
//	public void validateDetails( FacesContext context, UIComponent component, Object value  ) {
//		final String userName = (String) inputUsername.getLocalValue();
//		final String passWord = (String) value;
//		
//		final boolean usernameIsInvalid = !StringUtil.validateLength( userName, 4, 24);
//		final boolean passwordIsInvalid = !StringUtil.validateLength( passWord, 4, 24);
//		
//		LogManager.info(LoginBean.class, "validateDetails Login -------" + 1);	
//		if ( usernameIsInvalid || passwordIsInvalid   )  { 
//			withError = true;
//			FacesMessage message = new FacesMessage("Invalid username/password");
//			message.setSeverity(FacesMessage.SEVERITY_ERROR);
//			throw new ValidatorException(message);	
//		}
//		
//		LogManager.info(LoginBean.class, "validateDetails Login -------" + 2);	
//		// CHECK USERNAME AND PASSWORD
//		// AND IF USER IS AN ADMINISTRATOR, 
//		// GET THE ORG DETAILS AND SET IT TO THE SESSION
//		try { 
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 3);	
//			String[] loginDetails = UserWorker.getCJOLoginDetails( userName ,  passWord);
//			
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 4);	
//			//UserWorker.setUserTypeInSession( HttpUtil.getHttpSession( false ) ,loginDetails[1] );
//			
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 5);	
//			String orgBodyId = UserWorker.getOrganisationBodyIdInLoginDetails( loginDetails );
//			
//			if ( StringUtil.isNumeric( orgBodyId ) ) { 
//				LogManager.info(LoginBean.class, "validateDetails Login -------" + 6);	
//				Organisation organisation = new Organisation();
//				organisation.setBodyId( orgBodyId );
//				organisation = OrganisationWorker.findOrganisation( organisation.getBodyId() );
//				OrganisationWorker.setOrganisationInSession( HttpUtil.getHttpSession(true) , organisation);
//				LogManager.info(LoginBean.class, "validateDetails Login -------" + 7);	
//			}
//			
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 8);	
//			UserWorker.saveMembershipInSession( HttpUtil.getHttpSession( false ), loginDetails );
//			
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 9);	
//			//OrganisationWorker.setOrganisationDisplayInSession( HttpUtil.getHttpSession( false ) );
//			
//			OrganisationWorker.trackOrganisationLogin( HttpUtil.getHttpSession( false ) );
//			LogManager.info(LoginBean.class, "validateDetails Login -------" + 10);	
//			
//			
//		} catch ( ValidatorException ve ) { 
//			withError = true;
//			throw ve;
//		} catch ( IOException ve ) { 
//			withError = true;
//			FacesMessage message = new FacesMessage( ve.getMessage() );
//			message.setSeverity(FacesMessage.SEVERITY_ERROR);
//			throw new ValidatorException(message);	
//		}
//	}
//	
//	public boolean isWithError() {
//		return withError;
//	}
//	
//	public void setWithError(boolean withError) {
//		this.withError = withError;
//	}
//	
//	
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public UIInput getInputPassword() {
//		return inputPassword;
//	}
//
//	public void setInputPassword(UIInput inputPassword) {
//		this.inputPassword = inputPassword;
//	}
//
//	public UIInput getInputUsername() {
//		return inputUsername;
//	}
//
//	public void setInputUsername(UIInput inputUsername) {
//		this.inputUsername = inputUsername;
//	}
//
//	
//
//}

