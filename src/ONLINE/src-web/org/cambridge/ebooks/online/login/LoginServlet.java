package org.cambridge.ebooks.online.login;

/*
 * This is a temporary login servlet
 */

import java.io.IOException;
import java.io.PrintWriter;

import javax.faces.validator.ValidatorException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.internal_reports.InternalReportsWorker;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.subscription.FreeTrialBean;
import org.cambridge.ebooks.online.subscription.SubscriptionBean;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.user.UserWorker;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.StringUtil;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(LoginServlet.class);
	
	//private User user = new User();
	
	//protected String message;
	private static final String SUCCESS_MESSAGE = "success";
	private static final String REDIRECT_MESSAGE = "redirect";
	private static final String INVALID_ERROR_MESSAGE = "Invalid Username/Password.";
	private static final String ERROR_MESSAGE = "An error occurred. Please try again.";
	private static final String USERNAME_PASSWORD_REQUIRED = "Username/Password required.";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			if("Y".equals(request.getParameter("confirm"))){
				String updatedTerms = session.getAttribute("termsDate").toString();
				String memberId = session.getAttribute("memberId").toString();
				LOGGER.info(" ------------------------------------------- BEGIN ConfirmationBean.updateTerms" );
				ConfirmationBean.updateTerms(session, updatedTerms, memberId);
				return;
			}
			login(request, response);
			String isbns = SubscriptionUtil.getBooksNotAvailableForSale(OrgConLinkedMap.getAllBodyIds(request.getSession()), OrgConLinkedMap.getDirectBodyIds(request.getSession()));
			request.getSession().setAttribute(SubscriptionUtil.SID_BOOK_NOT_AVAILABLE_FOR_SALE, isbns);
		} catch (Exception e) {			
			LOGGER.info(e.getMessage());
		}
	}
	
	
	protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		login(request, response, username, password);		
	}
	
	protected void loginIFRAME(HttpServletRequest request, HttpServletResponse response){
		
	}
	
	
	protected void login(HttpServletRequest request, HttpServletResponse response, String username, String password) 
			throws ServletException, IOException, Exception {
		String message = "";
		message = loginNoResponse(request, response, username, password);
		PrintWriter out = response.getWriter();
		out.println("" + message + "");
		out.flush();
		out.close();
		
		if(StringUtils.isNotEmpty(message) && !message.contains(LoginServlet.SUCCESS_MESSAGE)) {
			LOGGER.info("message:" + message);
			throw new Exception("Login failed.");
		}
	}
	
	public static final String SUCCESS_DELIMITER = "--->";
	
	protected String loginNoResponse(HttpServletRequest request, HttpServletResponse response, String username, String password) 
		throws ServletException, IOException {
		
		String message = "";
		response.setContentType("plain/text");
			
		
		message = LoginServlet.SUCCESS_MESSAGE;
		
		boolean isUsernameLengthInvalid = false;
		boolean isPasswordLengthInvalid = false;
		boolean isUsernamePasswordValid = StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password);
		
		if (isUsernamePasswordValid) {
			isUsernameLengthInvalid = !StringUtil.validateLength(username, 4, 24);
			isPasswordLengthInvalid = !StringUtil.validateLength(password, 4, 24);
		} else {
			if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
				message = LoginServlet.USERNAME_PASSWORD_REQUIRED;
			else
				message = LoginServlet.INVALID_ERROR_MESSAGE;
		}
			
		
		LOGGER.info("validateDetails Login -------" + 1);	
		if (isUsernameLengthInvalid || isPasswordLengthInvalid) {
			LOGGER.info("Login FAILED.");
			isUsernamePasswordValid = false;
			message = LoginServlet.INVALID_ERROR_MESSAGE;
		}
		
		if (isUsernamePasswordValid) {
			
			try { 
				int isOrgLoginSuccessful = loginOrganisation(request, username, password);
				//considered successful even if user is not a member of an organisation
				if(isOrgLoginSuccessful == 1) {
					LOGGER.info(" -------------------------------------------  Start User login  " );
					User user = findUser(username, password);
					
					if(StringUtils.isNotEmpty(user.getUsername())) {
						LOGGER.info(" -------------------------------------------  Found user  " + user.getUsername() ); 
						HttpSession session = request.getSession();
						//UserWorker.setUserInSession( session , user );
						UserWorker.recordLoginTime(session);
						LOGGER.info(" ------------------------------------------- " + user.getUsername() + " has logged in" );
						
						LOGGER.info(" ------------------------------------------- BEGIN ConfirmationBean.setDateTermsUpdated" );
						ConfirmationBean.setDateTermsUpdated(session,username,password);
						
						message = message + "--->" + user.getEncryptedDetails();
						
						//required for seperating Ebooks login from CJO Login
						CJOLoginBean.setEncryptedDetails(request, user.getEncryptedDetails());
						
						//this is required for mulitple login to logout successfully	
						RelogBean.saveLogin(request, RelogBean.MAIN);
						
						//required for sign-up trial link. checks if has subscription
						(new SubscriptionBean()).setSubscription(request);
												
						//for login in login
						InternalReportsWorker.listenEvent(request, response);
						
						//required to recheck free trial on login
						FreeTrialBean.recheckFreeTrial(session);
					} else {
						message = LoginServlet.INVALID_ERROR_MESSAGE;
						LOGGER.info(" User Login FAILED.");
					}
				} else if (isOrgLoginSuccessful == 2) {
					message = LoginServlet.INVALID_ERROR_MESSAGE;
				} else if (isOrgLoginSuccessful == 3) {
					message = LoginServlet.ERROR_MESSAGE;
				}
				
			} catch (Exception ex) { 
				ex.printStackTrace();
				message = LoginServlet.ERROR_MESSAGE;
				ex.getMessage();
				LOGGER.error(" User Login FAILED.");
			}	
		}		
		
		//this is required for mulitple login to logout successfully		
		//checkIfWillRelog(password, request);
		
		return message;
		
	}
	
	
	public User findUser( final String username,final String password ) {
		return findUserStatic(username, password);
	}
	
	public static User findUserStatic(String username, String password){
		User userInfo = PersistenceUtil.searchEntity(new User(), User.SEARCH_BLADE_MEMBER_BY_USERNAME, username );
		
		if(StringUtil.isNotEmpty(userInfo) && StringUtils.isNotEmpty(userInfo.getBodyId()))
			userInfo = UserWorker.findUserByBodyId(userInfo.getBodyId());
		
		return userInfo;
	}
	
	
	public int loginOrganisation(HttpServletRequest request, String userName, String passWord) {
		int result = 1;		
		
		try { 	
			String[] loginDetails = UserWorker.getCJOLoginDetails( request, userName ,  passWord);
			System.out.println("login Details (LoginServlet.loginOrganisation)" + loginDetails);
			//UserWorker.setUserTypeInSession( request.getSession(false),loginDetails[1] );
			//String orgBodyId = UserWorker.getOrganisationBodyIdInLoginDetails( loginDetails );
			
			//if ( StringUtil.isNumeric( orgBodyId )) 
			//{ 	
				//System.out.println("theres is AN ORG ORG ORG");
				//Organisation organisation = new Organisation();
				//organisation.setBodyId( orgBodyId );
				//organisation = OrganisationWorker.findOrganisation( organisation.getBodyId() );
				
				//OrganisationWorker.setOrganisationInSession( request.getSession(false) , organisation);	
				
			//}
			
		
			//UserWorker.saveMembershipInSession( request.getSession(false), loginDetails );	
			
			//OrganisationWorker.setOrganisationDisplayInSession(request.getSession(false) );
			
			OrganisationWorker.trackOrganisationLogin( request.getSession(false));
			
			
		} catch (Exception ve) { 
			//result = false;
			if(ve instanceof ValidatorException) {
				//message = LoginServlet.INVALID_ERROR_MESSAGE;
				result = 2;
			} else {
				//message = LoginServlet.ERROR_MESSAGE;
				result = 3;
				LOGGER.error(" organisationDetails Login FAILED.");
				//ve.printStackTrace();
			}
		}
		
		return result;
	}
	
	public void setFailedAttemptCounter(HttpSession session, String message) {
		
		if (!(message.equals(SUCCESS_MESSAGE) || message.equals(ERROR_MESSAGE))) {
			Integer loginAttempt = (Integer) session.getAttribute("loginAttempt");
			loginAttempt = loginAttempt != null ? ++loginAttempt : 1;
			session.setAttribute("loginAttempt", loginAttempt);
			
			if (loginAttempt > 2) {
				message = REDIRECT_MESSAGE;
				session.setAttribute("loginAttempt", 0);
			}
			
		} else {
			session.setAttribute("loginAttempt", 0);
		}
	}
	
}
