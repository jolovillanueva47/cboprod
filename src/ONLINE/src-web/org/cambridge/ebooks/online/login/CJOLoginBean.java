package org.cambridge.ebooks.online.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * this class aims to seperate EBooks login from CJO Login. actually, this is the CJO login which allows 
 * separation of CJO and Ebooks. This class will handle cjo login related stuff.
 * @author mmanalo
 *
 */
public class CJOLoginBean {
	public static final String SESSION_KEY = "CJOLoginBean"; 
	
	private String encryptedId;
	private boolean isLoggedCJO;
	private boolean isLoggedEbooks;
	
	public String getEncryptedId() {
		return encryptedId;
	}
	public void setEncryptedId(String encryptedId) {
		this.encryptedId = encryptedId;
	}
	public boolean isLoggedCJO() {
		return isLoggedCJO;
	}	
	public void setLoggedCJO(boolean isLoggedCJO) {
		this.isLoggedCJO = isLoggedCJO;
	}	
	public boolean isLoggedEbooks() {
		return isLoggedEbooks;
	}
	public void setLoggedEbooks(boolean isLoggedEbooks) {
		this.isLoggedEbooks = isLoggedEbooks;
	}
	
	
	public static CJOLoginBean getBeanInSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		Object cjoBean = session.getAttribute(SESSION_KEY);
		if( cjoBean == null ){
			CJOLoginBean _cjoBean = new CJOLoginBean();
			session.setAttribute(SESSION_KEY, _cjoBean);
			return _cjoBean;
		}
		return (CJOLoginBean) cjoBean;
	}
	
	public static void setEncryptedDetails(HttpServletRequest request, String encryptedId){
		CJOLoginBean bean = getBeanInSession(request);
		bean.setEncryptedId(encryptedId);
		bean.setLoggedEbooks(true);
	}
	
}
