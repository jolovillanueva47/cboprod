package org.cambridge.ebooks.online.login.authentication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.cambridge.ebooks.online.jpa.organisation.Organisation;

public class MapOrgConProper extends LinkedHashMap<String, Organisation>{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6079662716800477473L;


	@Override
	public Organisation put(String key, Organisation value) {
		if(value.isHasNoOrgNorConsortia()){
			populateOrganisationFromDb(value);
		}	
		//if after forced population, is still has no org nor consortia via isHasNoOrgNorConsortia, then do not put, exit already
		if(value.isHasNoOrgNorConsortia()){
			return null;
		}		
		
		if(this.containsKey(key)){		
			directLogic(value, this.get(key));			
			putLogic(key, value);			
		}else{
			putLogic(key, value);			
		}
		return value;
	}
	
	/* private methods */
	
	private void populateOrganisationFromDb(Organisation org){
		if(org.getBodyId() != null && org.isHasNoOrgNorConsortia()){
			OrgConLoginWorker worker = new OrgConLoginWorker(this);
			worker.populateFromDb(org);
		}
	}
	
	private void directLogic(Organisation external, Organisation internal){
		if("Y".equals(internal.getDirect())){
			external.setDirect("Y");
		}		
	}
	
	private void putLogic(String key, Organisation value) {
		reverseOrder();			
		super.put(key, value);			
		reverseOrder();		
	}
	
	
	private void reverseOrder(){
		if(this.size() > 0){			 
			List<Entry<String,Organisation>> _lorg = new ArrayList<Entry<String,Organisation>>();						
			Set<Entry<String,Organisation>> set = this.entrySet();
			Iterator<Entry<String,Organisation>> iterator = set.iterator();
			while(iterator.hasNext()){
				Entry<String,Organisation> org = iterator.next();
				_lorg.add(org);		
			}
			Collections.reverse(_lorg);
			this.clear();
			for(Entry<String,Organisation> org : _lorg){
				super.put(org.getKey(), org.getValue());
			}			
		}		
	}
}
