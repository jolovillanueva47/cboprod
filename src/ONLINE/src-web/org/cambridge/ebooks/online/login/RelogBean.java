package org.cambridge.ebooks.online.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.cambridge.ebooks.online.filter.AuthenticationFilter;

public class RelogBean {
	
	public static LinkedHashMap<String, RelogBean> map = new LinkedHashMap<String, RelogBean>();
	
	public static final String USERNAME_GET = "username";
	public static final String PASSWORD_GET = "password";
	public static final String SHIBB_GET = "shib_id";
	public static final String ATHENS_GET = "ath_id";
	
	public static final String MAIN = "MAIN";
	public static final String ATH = "ATH";
	public static final String SHB = "SHB";
	
	public static final String RELOGIN_BEAN = "RELOGIN_BEAN";
	
	
	private ArrayList<String> logOrder = new ArrayList<String>();	
	
	//main login properties
	private String username;
	private String password;
	 
	
	//athens login
	private String athensId;
	
	//shib login
	private String shibbId;
	
		
	public void storeMain(HttpServletRequest request) {
		this.username = request.getParameter(USERNAME_GET);
		this.password = request.getParameter(PASSWORD_GET);
		this.logOrder.add(MAIN);
	}
	
	public void storeAthens(HttpServletRequest request) {
		this.athensId = request.getParameter(ATHENS_GET);		
		this.logOrder.add(ATH);
	}
	
	public void storeShibb(HttpServletRequest request) {
		this.shibbId = request.getParameter(SHIBB_GET);
		this.logOrder.add(SHB);
	}
	
	public static void put(String id, RelogBean bean){
		 int size = map.size();		 
		 if(size >= 500000){
			Set<String> keys = RelogBean.map.keySet();
			String key = keys.iterator().next();			
			map.remove(key);			
		 }
		 map.put(id, bean);
	}
	
	/**
	 * stores login information
	 * @param request
	 * @param type
	 */
	public static void saveLogin(HttpServletRequest request, String type){
		RelogBean rb = recreateRelogBean(request);
		
		/* do not save login if already existing */
		if(rb.getLogOrder().contains(type)){
			return;
		}
		
		if(MAIN.equals(type)){
			rb.storeMain(request);
		}else if(ATH.equals(type)){
			rb.storeAthens(request);
		}else if(SHB.equals(type)){
			rb.storeShibb(request);
		}
	}
	
	/**
	 * recreates the relog bean
	 * @param request
	 * @return
	 */
	public static RelogBean recreateRelogBean(HttpServletRequest request){
		HttpSession session = request.getSession();
		Object rb = session.getAttribute(RELOGIN_BEAN);
		if(rb == null){
			RelogBean _rb = new RelogBean();
			session.setAttribute(RELOGIN_BEAN, _rb);
			return _rb;
		}else{
			return (RelogBean) rb;
		}
	}
	
	/**
	 * remove items in the logOrder
	 * @param type
	 */
	private void removeOrder(String type){
		ArrayList<String> logOrder = this.logOrder;
		for(String log : logOrder){
			if(log.equals(type)){
				logOrder.remove(log);	
				break;
			}
		}
	}
	
	public void relogUser(HttpServletRequest request, HttpServletResponse response){
		ArrayList<String> al = this.logOrder;		
		for(String type : al){
			if(MAIN.equals(type)){
				LoginServlet login = new LoginServlet();
				try {
					login.loginNoResponse(request, response, this.username, this.password);					
				} catch (ServletException e) {				
					e.printStackTrace();
				} catch (IOException e) {				
					e.printStackTrace();
				}
			}else if(ATH.equals(type)){
				AuthenticationFilter authen = new AuthenticationFilter();
				authen.doAthensLogin(request, response, this.athensId, this.athensId);
			}else if(SHB.equals(type)){
				AuthenticationFilter authen = new AuthenticationFilter();
				authen.doShibbolethLogin(request, response, this.shibbId);				
			}
		}
		 
	}
	
	/**
	 * logs out the user type
	 * @param request
	 * @param type
	 */
	public static void logout(HttpServletRequest request, String type){
		RelogBean rb = recreateRelogBean(request);
		rb.removeOrder(type);		
	}
	
	public static void relog(HttpServletRequest request, HttpServletResponse response) {
		RelogBean rb = recreateRelogBean(request);
		rb.relogUser(request, response);
	}

	public static void main(String[] args) {
		RelogBean rb = new RelogBean();
		ArrayList<String> str = new ArrayList<String>();
		str.add(RelogBean.ATH);
		str.add(RelogBean.SHB);
		str.add(RelogBean.MAIN);
		rb.setLogOrder(str);
		rb.removeOrder(RelogBean.SHB);		
	}
	
	/* Getters and Setters */
	
	public void setLogOrder(ArrayList<String> logOrder) {
		this.logOrder = logOrder;
	}

	public ArrayList<String> getLogOrder() {
		return logOrder;
	}

	
	
	
}
