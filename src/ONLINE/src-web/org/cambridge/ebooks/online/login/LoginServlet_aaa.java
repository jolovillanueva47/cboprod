package org.cambridge.ebooks.online.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



public class LoginServlet_aaa extends LoginServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(LoginServlet_aaa.class); 

	private String message;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);
	}

	@Override
	protected void login(HttpServletRequest request, HttpServletResponse response, String username, String password) 
	throws ServletException, IOException {
		message = loginNoResponse(request, response, username, password); 
		String lastUrl = "";
		if (message.contains("success")){
			lastUrl = (String)request.getSession().getAttribute("lastUrl").toString();
			if(StringUtils.isEmpty(lastUrl)){
				if(StringUtils.isNotEmpty(request.getParameter("clcLogin_aaa"))){
					lastUrl = "/aaa/clc/home.jsf";
				}else{
					lastUrl = "/aaa/home.jsf";
				}
			}else if(lastUrl.contains("/aaa/search_results.jsf")){
				//values comes from login.jsp-ip_logo.jsp-search_results.jsp
				String queryString = "?searchType=page" ;
				queryString = queryString + "&currentPage=" + request.getParameter("currentPage");
				queryString = queryString + "&resultsPerPage=" + request.getParameter("resultsPerPage");
				queryString = queryString + "&sortType=" + request.getParameter("sortType");
				lastUrl = "/search_aaa" + queryString;
			}
			logger.info("lastUrl = "+lastUrl);
			
			response.sendRedirect(lastUrl);
		} else {
			request.setAttribute("errorMessage", message);
			String url = "/aaa/login.jsf";
			String path = (String)request.getSession().getAttribute("lastUrl").toString();
			if(path.contains("/aaa/clc/")){
				url = "/aaa/clc/login.jsf";
			}
			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}
