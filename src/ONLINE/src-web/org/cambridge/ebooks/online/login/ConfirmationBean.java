package org.cambridge.ebooks.online.login;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Admin;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil.PersistentUnits;
import org.cambridge.ebooks.online.util.StringUtil;

public class ConfirmationBean {

	private static final Logger LOGGER = Logger.getLogger(ConfirmationBean.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	public static final String FORMAT_DATE = "yyyy-MM-dd";  
	
	public static void setDateTermsUpdated(  HttpSession session, String username, String password ){
		String date = "";
		Admin _admin = null;
		
		List<Admin> userInfo = PersistenceUtil.searchListVector(new Admin(), Admin.SEARCH_ADMIN_BY_TERMS_CONFIRM, username);
		try{
				if(StringUtil.isNotEmpty(userInfo) && StringUtil.isNotEmpty(userInfo.get(0).getBladeMemberId())){
					session.setAttribute("username", username);
					session.setAttribute("password", password);
					 if(userInfo.get(0).getDateTermsAgreed() != null){
						 date = ConfirmationBean.getFormatedDate(userInfo.get(0).getDateTermsAgreed(), FORMAT_DATE).toString();
					 }
					session.setAttribute("dateUpdatedTerms", date);
					session.setAttribute("memberId", userInfo.get(0).getBladeMemberId().toString());
				}
				LOGGER.info("====================GET the User Info=====================");
		}
		catch(IndexOutOfBoundsException e){
			System.out.println(e.getMessage());
			LOGGER.error("error getting userinfo from Admin");
		}
	}
	
	private static String getFormatedDate(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	public String getUpdateTermsDate(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String resp = request.getParameter("confirm");
		
		if("Y".equals(resp)){
			HttpSession session = request.getSession();
			String updatedTerms = session.getAttribute("termsDate").toString();
			String memberId = session.getAttribute("memberId").toString();
			LOGGER.info("====================Start to Update the termsDate=====================");
			updateTerms(session, updatedTerms, memberId);
		}
		return "";
	}
	
	public static void updateTerms(HttpSession session, String updatedTerms, String memberId){
		EntityManager em = emf.createEntityManager();  
		//String dateUpdatedTerms = (String) session.getAttribute("dateUpdatedTerms");
		try {
			Query q = em.createNativeQuery("Update ORACJOC.ADMIN SET admin_termsagreed = to_date(?1,'YYYY-MM-DD') where blade_member_id = ?2");
			LOGGER.info("====================Update the termsDate=====================");
			q.setParameter(1, updatedTerms);
			q.setParameter(2, memberId);
			em.getTransaction().begin();
			q.executeUpdate();
			em.getTransaction().commit();
			setDateTermsUpdated(session, session.getAttribute("username").toString(), session.getAttribute("password").toString());
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
			LOGGER.info("====================Finished to Update the termsDate=====================");
		}		
	}
	
}
