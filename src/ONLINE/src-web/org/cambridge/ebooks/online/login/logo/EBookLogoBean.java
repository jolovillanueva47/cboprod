package org.cambridge.ebooks.online.login.logo;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * 
 * @author mmanalo
 *
 */
public class EBookLogoBean {
	
	private static final String ORG_LOGO_DIR = System.getProperty("org.logo.dir");
	
	private static final int MAX_ORG_DISPLAY_SIZE = 65;
	private static final int MAX_DISPLAY_MESSAGE_SIZE = 60;
	
	private OrgConLinkedMap orgConMap;
	private HttpSession session;
	private String displayName;	
	
	private String orgLogoId;
	
	private int maxLogoShow = 3;
	
	private static Logger LOGGER = Logger.getLogger(EBookLogoBean.class);  
	
	private HttpServletRequest request;
	
	private int logoNameCount;	
	
	/* static method */
	public EBookLogoBean(HttpServletRequest request){
		this.request = request;
		initLogo(request.getSession());
	}
		
	public EBookLogoBean(){
		this.request = HttpUtil.getHttpServletRequest();
		HttpSession session = this.request.getSession();		
		initLogo(session);
	}
	
	
	
	public void populateLogo(){			
		LOGGER.info("populateLogo");
		
		Map<String, List<ConsortiumOrganisation>> consOrgMap = OrgConLinkedMap.getConsOrgFromSession(request);
		if(null == consOrgMap) {
			this.orgLogoId = buildDefaultLogo();
		} else {			
			this.orgLogoId = buildOrgLogo(consOrgMap);
		}		
	}	
	

	public String getShowLogoNamesUnderMax(){		
		LOGGER.info("getShowLogoNamesUnderMax");
		
		this.logoNameCount = orgConMap.getMap().size();
		
		String results = "";
		
		Map<String, List<ConsortiumOrganisation>> consOrgMap = OrgConLinkedMap.getConsOrgFromSession(request);			
		if(null == consOrgMap) {
			results = buildDefaultLogoNamesUnderMax();
		} else {		
			results = fixStringEndTrail(buildOrgLogoNamesUnderMax(consOrgMap));	
		}
		
		// cut long text to 65 chars
		if(StringUtils.isNotEmpty(results) && results.length() > MAX_ORG_DISPLAY_SIZE) {
			results = results.substring(0, MAX_ORG_DISPLAY_SIZE - 1) + "...";
		}
		
		return results;		
	}
	
	public String getTextMessage(){
		StringBuilder sb = new StringBuilder();
		
		int i = 0;
		for(String key : orgConMap.getMap().keySet()) {
			Organisation org = orgConMap.getMap().get(key);
			String orgTextMessage = org.getBaseProperty(Organisation.MESSAGE_TEXT);
			orgTextMessage = orgTextMessage != null ? orgTextMessage.trim() : orgTextMessage;
			
			if(orgTextMessage != null){
				sb.append(orgTextMessage).append(", ");
			}
			
			if(i == maxLogoShow-1){
				break;
			}
			
			i++;
		}
		
		String results = fixStringEndTrail(sb.toString());
		
		// cut long text to 60 chars
		if(StringUtils.isNotEmpty(results) && results.length() > MAX_DISPLAY_MESSAGE_SIZE) {
			results = results.substring(0, MAX_DISPLAY_MESSAGE_SIZE - 1) + "...";
		}
		
		return results;
	}

	/* private methods */
	
	private String fixStringEndTrail(String s) {
		// check if string ends with ", "
		if(StringUtils.isNotEmpty(s) && s.endsWith(", ")) {
			int idx = s.lastIndexOf(", ");
			// remove ", " at the end
			s = s.substring(0, idx);
		}
		
		return s;
	}
	
	private String buildConsLogo() {		
		String orgLogoId = "";
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> consList = orgConLinkedMap.getAllConsortium();
		
		for(Organisation con : consList) {	
			String image = con.getBaseProperty(Organisation.IMAGE);	
			if("Y".equals(image) && new File(ORG_LOGO_DIR + "org" + con.getBodyId()).exists()) {
				orgLogoId = con.getBodyId();
				break;
			}
		}	
		
		return orgLogoId;
	}
	
	private String buildOrgLogo() {		
		String orgLogoId = "";
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> orgList = orgConLinkedMap.getAllOrganisation();
		
		for(Organisation org : orgList) {	
			String image = org.getBaseProperty(Organisation.IMAGE);			
			if("Y".equals(image) && new File(ORG_LOGO_DIR + "org" + org.getBodyId()).exists()) {
				orgLogoId = org.getBodyId();
				break;
			}
		}	
		
		return orgLogoId;
	}
	
	private String buildOrgLogo(Map<String, List<ConsortiumOrganisation>> consOrgMap) {		
		String orgLogoId = "";
		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> orgList = orgConLinkedMap.getAllOrganisation();
		Map<String, Organisation> consMap = orgConLinkedMap.getConsortiaMap();
		
		for(Organisation org : orgList) {	
			List<ConsortiumOrganisation> consOrgList = consOrgMap.get(org.getBodyId());	
			
			// check if this org has a consortia
			if(null != consOrgList && consOrgList.size() > 0) {
				// consortia logo
				for(ConsortiumOrganisation consOrg : consOrgList) {
					// override consortia logo = N
					if(!isOverrideConsortiaLogo(consOrg)) {
						if(consMap.containsKey(consOrg.getConsortiumId())) {
							Organisation _cons = consMap.get(consOrg.getConsortiumId());
							
							String image = _cons.getBaseProperty(Organisation.IMAGE);	
							if("Y".equals(image) && new File(ORG_LOGO_DIR + "org" + _cons.getBodyId()).exists()) {
								orgLogoId = _cons.getBodyId();
								
								// only get one consortia associated with this org
								break;
							}					
						}
					}
				}
			}
			
			if(StringUtils.isNotEmpty(orgLogoId)) {
				break;
			}
			
			String image = org.getBaseProperty(Organisation.IMAGE);			
			// organisation logo
			if("Y".equals(image) && new File(ORG_LOGO_DIR + "org" + org.getBodyId()).exists()) {
				orgLogoId = org.getBodyId();
				break;
			}
		}
		
		return orgLogoId;
	}
	
	private String buildConsLogoNamesUnderMax(StringBuilder sb, int counter, Map<String, String> overrideConsMap) {
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> consList = orgConLinkedMap.getAllConsortium();
		
		for(Organisation con : consList) {	
			// only get 3 entries
			if(counter >= maxLogoShow) {
				break;
			}
			
			String overrideCons = overrideConsMap.get(con.getBodyId());
			
			// skip if consortia is overriden
			if(StringUtils.isNotEmpty(overrideCons) && "Y".equals(overrideCons)) {
				continue;
			}
			
			// add to counter
			counter++;
			
			// add consortia name			
			sb.append(con.getBaseProperty(Organisation.DISPLAY_NAME)).append(", ");
		}	
		
		return sb.toString();
	}
	
	private String buildOrgLogoNamesUnderMax(Map<String, List<ConsortiumOrganisation>> consOrgMap) {
		LOGGER.info("----buildOrgLogoNamesUnderMax");
		StringBuilder result = new StringBuilder();	
		
		int counter = 0;
		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> orgList = orgConLinkedMap.getAllOrganisation();
		Map<String, String> overrideConsMap = new HashMap<String, String>();
		
		for(Organisation org : orgList) {
			LOGGER.info("body id:" + org.getBodyId());
			
			// only get 3 entries
			if(counter >= maxLogoShow) {
				break;
			}
			
			// add to counter
			counter++;
			
			// add organisation name
			StringBuilder displayNameBuilder = new StringBuilder(org.getBaseProperty(Organisation.DISPLAY_NAME));
			
			List<ConsortiumOrganisation> consOrgList = consOrgMap.get(org.getBodyId());				
			
			if(null != consOrgList && consOrgList.size() > 0) {
				for(ConsortiumOrganisation consOrg : consOrgList) {	
					// override consortia name = N
					if(!isOverrideConsortiaName(consOrg)) {
						overrideConsMap.put(consOrg.getConsortiumId(), "N");
					} else {
						overrideConsMap.put(consOrg.getConsortiumId(), "Y");
					}	
				}
			}
			
			result.append(displayNameBuilder.toString()).append(", ");
		}
		
		return buildConsLogoNamesUnderMax(result, counter, overrideConsMap);
	}
	
	private String buildOrgLogoNamesUnderMax() {
		StringBuilder result = new StringBuilder();	
		int counter = 0;	
		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
		List<Organisation> orgList = orgConLinkedMap.getAllOrganisation();
		
		for(Organisation org : orgList) {
			// only get 3 entries
			if(counter >= maxLogoShow) {
				break;
			}
			
			// add to counter
			counter++;
			
			// add organisation name			
			result.append(org.getBaseProperty(Organisation.DISPLAY_NAME)).append(", ");
		}
		
		return buildConsLogoNamesUnderMax(result, counter, null);		
	}
	
	private boolean isOverrideConsortiaLogo(ConsortiumOrganisation consOrg) {
		boolean flag = false;
		if("Y".equals(consOrg.getOverrideConsortiaLogo())) {
			flag = true;
		}
		
		return flag;
	}
	
	private boolean isOverrideConsortiaName(ConsortiumOrganisation consOrg) {
		boolean flag = false;
		if("Y".equals(consOrg.getOverrideConsortiaLogoName())) {
			flag = true;
		}
		
		return flag;
	}
	
	private String buildDefaultLogoNamesUnderMax() {
		return fixStringEndTrail(buildOrgLogoNamesUnderMax());
	}
	
	private String buildDefaultLogo() {
		String orgLogoId = "";
		
		orgLogoId = buildConsLogo();
		if(StringUtils.isEmpty(orgLogoId)) {
			orgLogoId = buildOrgLogo();
		}
		
		return orgLogoId;
	}
	
	
	private void initLogo(HttpSession session){		
		//LOGGER.info("LOGO DEBUG 1 = " + session);
		//LOGGER.info("LOGO DEBUG 1 LN= " + session);
		this.orgConMap = OrgConLinkedMap.getFromSession(session);
		//LOGGER.info("LOGO DEBUG 2 = " + orgConMap);
		//LOGGER.info("LOGO DEBUG 2 LN = " + orgConMap);
		this.session = session;	
		
		populateLogo();
	}
		

	/* getters / setters */
	
	public String getOrgName(){
		Organisation org = orgConMap.getFirst();		
		if(org != null){
			return org.getBaseProperty("displayName");
		}
		return null;
	}
	
	public boolean isHasLogo(){
		if(StringUtils.isNotEmpty(this.orgLogoId)){
			return true;
		}
		return false;
	}
		
	public OrgConLinkedMap getOrgConMap() {
		return orgConMap;
	}
	
	public String getOrgLogoId() {
		return orgLogoId;
	}

	public void setOrgLogoId(String orgLogoId) {
		this.orgLogoId = orgLogoId;
	}	
	public int getLogoNameCount() {
		return logoNameCount;
	}
	public void setLogoNameCount(int logoNameCount) {
		this.logoNameCount = logoNameCount;
	}
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public boolean isUserLoggedIn(){
		return OrgConLinkedMap.getFromSession(session).isUserLoggedIn();
	}
	
	public boolean isAthensLoggedIn(){
		return OrgConLinkedMap.getFromSession(session).isAthensLoggedIn();
	}
	
	public boolean isShibbolethLoggedIn(){
		return OrgConLinkedMap.getFromSession(session).isShibbolethLoggedIn();
	}
		
	public int getMaxLogoShow() {
		return maxLogoShow;
	}

	public void setMaxLogoShow(int maxLogoShow) {
		this.maxLogoShow = maxLogoShow;
	}
	
	
}
