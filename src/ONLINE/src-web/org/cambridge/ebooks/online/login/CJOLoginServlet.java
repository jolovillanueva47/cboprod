package org.cambridge.ebooks.online.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CJOLoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2004971587796445985L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {	
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String updateCJO = req.getParameter(UPDATE_CJO_LOGGED);
		if("Y".equals(updateCJO)){
			updateCJOLogged(req);
		}else{
			checkIfLoggedIn(req, resp);
		}
	}
	
	public static final String YES = "Y";
	
	public static final String UPDATE_CJO_LOGGED = "UPDATE_CJO_LOGGED";
	
	private static final String CONTENT_TYPE = "plain/text";
	
	public static final String LOGGED_EBOOKS = "LOGGED_EBOOKS";
	
	public static final String LOGGED_CJO = "LOGGED_CJO";
	
	public static final String NOT_LOGGED = "NOT_LOGGED";
	
	private void checkIfLoggedIn(HttpServletRequest request, HttpServletResponse resp) throws IOException{
		CJOLoginBean cjo = CJOLoginBean.getBeanInSession(request);
		resp.setContentType(CONTENT_TYPE);
		PrintWriter pw = resp.getWriter();		
		if(cjo.isLoggedCJO()){			
			pw.write(LOGGED_CJO);			
		}else if(cjo.isLoggedEbooks()){
			pw.write(LOGGED_EBOOKS + "-->" + cjo.getEncryptedId());
			//updateCJOLogged(request);
		}else{
			pw.write(NOT_LOGGED);
		}
		pw.flush();
		pw.close();
	}
	
	
	
	private void updateCJOLogged(HttpServletRequest request){
		CJOLoginBean cjo = CJOLoginBean.getBeanInSession(request);
		cjo.setLoggedCJO(true);
	}
}
