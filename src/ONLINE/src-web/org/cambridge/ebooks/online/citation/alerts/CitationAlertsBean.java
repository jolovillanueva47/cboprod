package org.cambridge.ebooks.online.citation.alerts;

import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.citation.alert.CitationAlert;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author jgalang
 * 
 */

public class CitationAlertsBean {

	private static final Logger LOGGER = Logger.getLogger(CitationAlertsBean.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	private ArrayList<CitationAlert> citationAlerts;
	private String initThickBox;
	private boolean loggedIn;

	public String getInitThickBox() {
		
		boolean renderResponse = FacesContext.getCurrentInstance().getRenderResponse();
		if (!renderResponse) return null;
		
		LOGGER.info("Initialize CitationAlertsBean");
		
		User user = HttpUtil.getUserFromCurrentSession();
		loggedIn = user != null;
		
		if (loggedIn) {
			String bodyId = user.getBodyId();
			String emailAddress = user.getEmail();

			String bookId = HttpUtil.getHttpServletRequestParameter("bookId");
			String doi = HttpUtil.getHttpServletRequestParameter("doi");
			String contentId = HttpUtil.getHttpServletRequestParameter("contentId");
			
			if (StringUtils.isNotEmpty(bookId)) {
				CitationAlert newAlert = new CitationAlert();
				
				newAlert.setAuthor(HttpUtil.getHttpServletRequestParameter("author"));
				newAlert.setBodyId(bodyId);
				newAlert.setBookId(bookId);
				newAlert.setBookTitle(HttpUtil.getHttpServletRequestParameter("bookTitle"));
				newAlert.setContentId(StringUtils.isNotEmpty(contentId) ? contentId : "0");
				newAlert.setContentTitle(HttpUtil.getHttpServletRequestParameter("contentTitle"));
				newAlert.setDoi(doi);
				newAlert.setEmailAddress(emailAddress);
				newAlert.setFrequencyType("0");
				newAlert.setPage(HttpUtil.getHttpServletRequestParameter("page"));
				newAlert.setPublishDate(HttpUtil.getHttpServletRequestParameter("publishDate"));
				newAlert.setTurnOffAlert("N");

				mergeCitationAlert(newAlert);
			}
			
			citationAlerts = getUserCitationAlerts(bodyId);
		}
		
		return initThickBox;
	}
	
	public static ArrayList<CitationAlert> getUserCitationAlerts(String bodyId) {
		ArrayList<CitationAlert> citationAlerts = new ArrayList<CitationAlert>();
		citationAlerts = PersistenceUtil.searchList(new CitationAlert(), CitationAlert.SEARCH_CITATION_ALERTS, bodyId);
		return citationAlerts;
	}
	
	public void updateCitationAlerts(ActionEvent event) {
		System.out.println("my gaaahd!");
		EntityManager em = emf.createEntityManager();
		for (CitationAlert alert : citationAlerts) {
			em.getTransaction().begin();
			if (alert.isDelete()) {
				alert = em.find(CitationAlert.class, alert.getCitationAlertId());
				em.remove(alert);
			} else {
				em.merge(alert);
			}
			em.getTransaction().commit();
		}
		em.close();
	}
	
	public static void mergeCitationAlert(CitationAlert alert) {
		
		CitationAlert newAlert = PersistenceUtil.searchEntity(
				new CitationAlert(),
				CitationAlert.SEARCH_CITATION_ALERTS_BY_IDS,
				alert.getBodyId(), alert.getBookId(), alert.getContentId());
		
		if (newAlert == null) newAlert = new CitationAlert();
		
		newAlert.setAuthor(alert.getAuthor());
		newAlert.setBodyId(alert.getBodyId());
		newAlert.setBookId(alert.getBookId());
		newAlert.setBookTitle(alert.getBookTitle());
		newAlert.setContentId(alert.getContentId());
		newAlert.setContentTitle(alert.getContentTitle());
		newAlert.setDoi(alert.getDoi());
		newAlert.setEmailAddress(alert.getEmailAddress());
		
		if (StringUtils.isNotEmpty(newAlert.getFrequencyType()) && !newAlert.getFrequencyType().equals("1"))
			newAlert.setFrequencyType(alert.getFrequencyType());
		
		newAlert.setLastAlertDate(alert.getLastAlertDate());
		newAlert.setPage(alert.getPage());
		newAlert.setPublishDate(alert.getPublishDate());
		newAlert.setTurnOffAlert(alert.getTurnOffAlert());
		

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(newAlert);
		em.getTransaction().commit();
		LOGGER.info("Merged " + newAlert.getBookTitle());
		em.close();
		
	}
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public void setInitThickBox(String initThickBox) {
		this.initThickBox = initThickBox;
	}
	public ArrayList<CitationAlert> getCitationAlerts() {
		return citationAlerts;
	}
	public void setCitationAlerts(ArrayList<CitationAlert> citationAlerts) {
		this.citationAlerts = citationAlerts;
	}
	
}
