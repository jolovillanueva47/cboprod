package org.cambridge.ebooks.online.citation.alerts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.alerts.EmailAlerts;

/**
 * 
 * @author jgalang
 * 
 */

public class RunCitationAlertsServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EmailAlerts.runAlerts();
		response.sendRedirect("home.jsf");
	}
	
}
