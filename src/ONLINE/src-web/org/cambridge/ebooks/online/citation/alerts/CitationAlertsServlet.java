package org.cambridge.ebooks.online.citation.alerts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.citation.alert.CitationAlert;
import org.cambridge.ebooks.online.jpa.user.User;

/**
 * 
 * @author jmendiola
 * 
 */

public class CitationAlertsServlet extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(CitationAlertsServlet.class);
	
//	private static EntityManagerFactory emf = PersistenceUtil.emf;
//	private ArrayList<CitationAlert> citationAlerts2;
//	private String initThickBox;
//	private boolean loggedIn;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int iterator;
		String delete, turnOff;
		
		try{
//		System.out.println("CitationAlertsServlet: inside try");	
//		
//		if(request.getParameter("Turn Off Alert") != null ){
//			System.out.println("------TURN OFF ALERT SELECTED");
//		}
//		if(request.getParameter("Delete") != null ){
//			System.out.println("------DELETE ALERT SELECTED");
//		}
		
		HttpSession session = request.getSession();
		
		CitationAlertsBean alert = (CitationAlertsBean)session.getAttribute("citationAlertsBean");
		iterator = 0;
		
		for (CitationAlert ca : alert.getCitationAlerts()) {

			delete = request.getParameter("delete"+iterator);
			turnOff = request.getParameter("turnOff"+iterator);
			ca.setDelete(false);
			ca.setTurnOff(false);
			
			if(! (StringUtils.isEmpty(delete)))			
				ca.setDelete(true);
			
			if(! (StringUtils.isEmpty(turnOff)))
				ca.setTurnOff(true);
			
			ca.setEmailAddress(request.getParameter("emailAddress"+iterator));
			ca.setFrequencyType(request.getParameter("frequencyType"+iterator));
			
//			System.out.println("delete"+iterator+"="+request.getParameter("delete"+iterator));
//			System.out.println("frequencyType"+iterator+"="+request.getParameter("frequencyType"+iterator));
//			System.out.println("emailAddress"+iterator+"="+request.getParameter("emailAddress"+iterator));
//			System.out.println("turnOff"+iterator+"="+request.getParameter("turnOff"+iterator));

			iterator++;
			CitationAlertsBean.mergeCitationAlert(ca);
		}
		
		alert.updateCitationAlerts(null);
//		request.setAttribute("citationAlerts", citationAlerts);
		request.getRequestDispatcher("/aaa/popups/alert_me.jsf").forward(request, response);
		//session.removeAttribute("citationAlertsBean");
//		response.sendRedirect("alert_me.jsf");
		
		} catch (Exception e){
//			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}
	
	public User getRequestUser(HttpServletRequest request){
		HttpSession session = request.getSession(false);
		return (User) session.getAttribute("userInfo"); 
	}

}
