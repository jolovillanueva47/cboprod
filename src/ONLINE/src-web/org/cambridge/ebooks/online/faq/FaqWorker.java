package org.cambridge.ebooks.online.faq;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.online.jpa.faq.Faq;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectAreaDetailsBooks;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectAreaTypeBooks;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectTypeDetailsMapBooks;



/**
 * @author Karlson A. Mulingtapang 
 * FaqWorker.java - Sets data from jpa to bean
 */
public class FaqWorker {

	public List<FaqBean> searchAllFaqs() {
		try {
			return populateFaqBean(FaqDAO.searchAllFaq());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<FaqSubjectAreaTypeBooksBean> searchAllFaqSubjectAreaTypeBooks() {
		try {
			return populateFaqSubjectAreaTypeBooksBean(FaqDAO
					.searchAllFaqSubjectAreaTypeBooks());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<FaqSubjectAreaDetailsBooksBean> searchAllFaqSubjectAreaDetailsBooks() {
		try {
			return populateFaqSubjectAreaDetailsBooksBean(FaqDAO
					.searchAllFaqSubjectAreaDetailsBooks());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public FaqSubjectAreaDetailsBooksBean searchFaqSubjectAreaDetailsBooksById(
			int id) {
		try {
			return populateFaqSubjectAreaDetailsBooksBean(FaqDAO
					.searchFaqSubjectAreaDetailsBooksById(id));
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public List<FaqSubjectAreaTypeBooksBean> searchFaqSubjectAreaTypeBooksById(int id) {
		try {
			return populateFaqSubjectAreaTypeBooksBean(FaqDAO
					.searchFaqSubjectAreaTypeBooksById(id));
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	private List<FaqBean> populateFaqBean(List<Faq> records)
			throws InvocationTargetException, IllegalAccessException {

		if(records == null) {
			return null;
		}
		
		List<FaqBean> beans = new ArrayList<FaqBean>();
		FaqBean bean = null;

		for (Faq record : records) {
			bean = new FaqBean();
			BeanUtils.copyProperties(bean, record);
			beans.add(bean);
		}

		return beans;
	}

	private List<FaqSubjectAreaTypeBooksBean> populateFaqSubjectAreaTypeBooksBean(
			List<FaqSubjectAreaTypeBooks> records)
			throws InvocationTargetException, IllegalAccessException {

		if(records == null) {
			return null;
		}
		List<FaqSubjectAreaTypeBooksBean> beans = new ArrayList<FaqSubjectAreaTypeBooksBean>();
		for (FaqSubjectAreaTypeBooks record : records) {
			FaqSubjectAreaTypeBooksBean bean = new FaqSubjectAreaTypeBooksBean();

			System.out.println("faqid="+record.getId());
			// subject area details books
			List<FaqSubjectAreaDetailsBooksBean> subjectAreaDetailsBooksBeans = populateFaqSubjectAreaDetailsBooksBeanFromMap(FaqDAO
					.searchFaqSubjectTypeDetailsMapBooksBySubjectAreaTypeId(record
							.getId()));

			BeanUtils.copyProperties(bean, record);
			bean.setFaqSubjectAreaDetailsBooksBeans(subjectAreaDetailsBooksBeans);

			beans.add(bean);
		}
		
		return beans;
	}

	private List<FaqSubjectAreaDetailsBooksBean> populateFaqSubjectAreaDetailsBooksBeanFromMap(
			List<FaqSubjectTypeDetailsMapBooks> records)
			throws InvocationTargetException, IllegalAccessException {
		
		if(records == null) {
			return null;
		}
		
		List<FaqSubjectAreaDetailsBooksBean> beans = new LinkedList<FaqSubjectAreaDetailsBooksBean>();
		
//		for (FaqSubjectTypeDetailsMapBooks record : records) {
//			System.out.println("---FaqWorker[FaqSubjectAreaDetailsId]:" + record.getFaqSubjectAreaDetailsId());
//			
//			FaqSubjectAreaDetailsBooksBean bean = new FaqSubjectAreaDetailsBooksBean();			
//			FaqSubjectAreaDetailsBooks subjectAreaDetailsBean = FaqDAO.searchFaqSubjectAreaDetailsBooksById(record.getFaqSubjectAreaDetailsId());
//			
//			if(subjectAreaDetailsBean != null) {
//				BeanUtils.copyProperties(bean, subjectAreaDetailsBean);
//			}
//			beans.add(bean);
//		}

	
		//jpa class
		List<FaqSubjectAreaDetailsBooks> beans2 = new ArrayList<FaqSubjectAreaDetailsBooks>();
//		
		beans2 = FaqDAO.searchFaqSubjectAreaDetailsBooksById_all(records);
//		
		//System.out.println("-------->beans 2 sizemo="+beans2.size());
		if(beans2 != null){
			for (FaqSubjectAreaDetailsBooks f : beans2) {
				//System.out.println("-name="+f.getQuestion()+", displayorder="+f.getDisplayOrder());
				FaqSubjectAreaDetailsBooksBean bean = new FaqSubjectAreaDetailsBooksBean();
				
				bean.setAnswer(f.getAnswer());
				bean.setDisplayOrder(f.getDisplayOrder());
				bean.setEnable(f.getEnable());
				bean.setId(f.getId());
				bean.setModifiedBy(f.getModifiedBy());
				bean.setModifiedDate(f.getModifiedDate());
				bean.setQuestion(f.getQuestion());
				
				beans.add(bean);
			}
		}
	
		
		
		
		return  beans;
	}

	private List<FaqSubjectAreaDetailsBooksBean> populateFaqSubjectAreaDetailsBooksBean(
			List<FaqSubjectAreaDetailsBooks> records)
			throws InvocationTargetException, IllegalAccessException {

		if(records == null) {
			return null;
		}
		
		List<FaqSubjectAreaDetailsBooksBean> beans = new ArrayList<FaqSubjectAreaDetailsBooksBean>();
		for (FaqSubjectAreaDetailsBooks record : records) {
			FaqSubjectAreaDetailsBooksBean bean = new FaqSubjectAreaDetailsBooksBean();
			BeanUtils.copyProperties(bean, record);
			beans.add(bean);
		}

		return beans;
	}

	private FaqSubjectAreaDetailsBooksBean populateFaqSubjectAreaDetailsBooksBean(
			FaqSubjectAreaDetailsBooks record)
			throws InvocationTargetException, IllegalAccessException {

		if(record == null) {
			return null;
		}
		
		FaqSubjectAreaDetailsBooksBean bean = new FaqSubjectAreaDetailsBooksBean();
		BeanUtils.copyProperties(bean, record);

		return bean;
	}
}
