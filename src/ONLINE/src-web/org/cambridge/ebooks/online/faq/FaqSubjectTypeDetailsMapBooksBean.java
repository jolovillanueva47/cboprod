package org.cambridge.ebooks.online.faq;


/**
 * @author Karlson A. Mulingtapang
 * FaqSubjectTypeDetailsMapBooksBean.java - FaqSubjectTypeDetailsMapBooks Bean
 */
public class FaqSubjectTypeDetailsMapBooksBean {
	
	private int faqSubjectAreaTypeId;
	private int faqSubjectAreaDetailsId;
	
	private FaqSubjectAreaTypeBooksBean faqSubjectAreaTypeBooksBean;
	private FaqSubjectAreaDetailsBooksBean faqSubjectAreaDetailsBooksBean;
	
	/**
	 * @return the faqSubjectAreaTypeId
	 */
	public int getFaqSubjectAreaTypeId() {
		return faqSubjectAreaTypeId;
	}
	/**
	 * @param faqSubjectAreaTypeId the faqSubjectAreaTypeId to set
	 */
	public void setFaqSubjectAreaTypeId(int faqSubjectAreaTypeId) {
		this.faqSubjectAreaTypeId = faqSubjectAreaTypeId;
	}
	/**
	 * @return the faqSubjectAreaDetailsId
	 */
	public int getFaqSubjectAreaDetailsId() {
		return faqSubjectAreaDetailsId;
	}
	/**
	 * @param faqSubjectAreaDetailsId the faqSubjectAreaDetailsId to set
	 */
	public void setFaqSubjectAreaDetailsId(int faqSubjectAreaDetailsId) {
		this.faqSubjectAreaDetailsId = faqSubjectAreaDetailsId;
	}
	/**
	 * @return the faqSubjectAreaTypeBooksBean
	 */
	public FaqSubjectAreaTypeBooksBean getFaqSubjectAreaTypeBooksBean() {
		return faqSubjectAreaTypeBooksBean;
	}
	/**
	 * @param faqSubjectAreaTypeBooksBean the faqSubjectAreaTypeBooksBean to set
	 */
	public void setFaqSubjectAreaTypeBooksBean(
			FaqSubjectAreaTypeBooksBean faqSubjectAreaTypeBooksBean) {
		this.faqSubjectAreaTypeBooksBean = faqSubjectAreaTypeBooksBean;
	}
	/**
	 * @return the faqSubjectAreaDetailsBooksBean
	 */
	public FaqSubjectAreaDetailsBooksBean getFaqSubjectAreaDetailsBooksBean() {
		return faqSubjectAreaDetailsBooksBean;
	}
	/**
	 * @param faqSubjectAreaDetailsBooksBean the faqSubjectAreaDetailsBooksBean to set
	 */
	public void setFaqSubjectAreaDetailsBooksBean(
			FaqSubjectAreaDetailsBooksBean faqSubjectAreaDetailsBooksBean) {
		this.faqSubjectAreaDetailsBooksBean = faqSubjectAreaDetailsBooksBean;
	}
}
