package org.cambridge.ebooks.online.faq;

import java.util.Date;
import java.util.List;

/**
 * @author Karlson A. Mulingtapang
 * FaqSubjectAreaTypeBooksBean.java - FaqSubjectAreaTypeBooks Bean
 */
public class FaqSubjectAreaTypeBooksBean {
	
	private int id;
	private String subjectAreaType;
	private String subjectAreaDescription;
	private int displayOrder;
	private String enable;
	private Date modifiedDate;
	private String modifiedBy;
	private int languageCode;
	
	
	private List<FaqSubjectAreaDetailsBooksBean> faqSubjectAreaDetailsBooksBeans;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the subjectAreaType
	 */
	public String getSubjectAreaType() {
		return subjectAreaType;
	}
	/**
	 * @param subjectAreaType the subjectAreaType to set
	 */
	public void setSubjectAreaType(String subjectAreaType) {
		this.subjectAreaType = subjectAreaType;
	}
	/**
	 * @return the subjectAreaDescription
	 */
	public String getSubjectAreaDescription() {
		return subjectAreaDescription;
	}
	/**
	 * @param subjectAreaDescription the subjectAreaDescription to set
	 */
	public void setSubjectAreaDescription(String subjectAreaDescription) {
		this.subjectAreaDescription = subjectAreaDescription;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the faqSubjectAreaDetailsBooksBeans
	 */

	public List<FaqSubjectAreaDetailsBooksBean> getFaqSubjectAreaDetailsBooksBeans() {
		return faqSubjectAreaDetailsBooksBeans;
	}
	/**
	 * @param faqSubjectAreaDetailsBooksBeans the faqSubjectAreaDetailsBooksBeans to set
	 */
	public void setFaqSubjectAreaDetailsBooksBeans(
			List<FaqSubjectAreaDetailsBooksBean> faqSubjectAreaDetailsBooksBeans) {
		this.faqSubjectAreaDetailsBooksBeans = faqSubjectAreaDetailsBooksBeans;
	}
	
	public void setLanguageCode(int languageCode) {
		this.languageCode = languageCode;
	}
	public int getLanguageCode() {
		return languageCode;
	}


}
