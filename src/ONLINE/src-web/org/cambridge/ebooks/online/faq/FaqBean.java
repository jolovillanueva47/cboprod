package org.cambridge.ebooks.online.faq;

import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author Karlson A. Mulingtapang
 * FaqBean.java - FAQ Bean
 */
public class FaqBean {
	
	private static final Logger LOGGER = Logger.getLogger(FaqBean.class); 
	
	private int faqId;
	private String question;
	private String answer;
	private String subjectAreaType;
	private int displayOrder; 
	private String enable;
	private Date modifiedDate;
	private String modifiedBy;
	private String helpPage;
	private String content;
	private String url;
	
	private List<FaqBean> faqs;
	private List<FaqSubjectAreaTypeBooksBean> faqSubjectAreaTypeBooksBeans;
	private List<FaqSubjectAreaDetailsBooksBean> faqSubjectAreaDetailsBooksBeans;
	
	private List<FaqSubjectAreaTypeBooksBean> faqSubjectAreaTypeBookByIdBeans;
	private FaqSubjectAreaDetailsBooksBean faqSubjectAreaDetailsBooksBean;
	
//	private String initialize;
//	private String populateSubjectAreaDetails;
	
	private FaqWorker worker = new FaqWorker();

	public String getInitialize() {				
		LOGGER.info("=== Start to Initialize ===");
		this.faqSubjectAreaTypeBooksBeans = worker.searchAllFaqSubjectAreaTypeBooks();

		return "";
		
	}
	
	public String getPopulateSubjectAreaDetails() {
		String detailsId = HttpUtil.getHttpServletRequestParameter("detailsId");
		String typeId = HttpUtil.getHttpServletRequestParameter("typeId");
		if(StringUtils.isEmpty(detailsId)) {
			detailsId = "-1";
		}
		
		if(StringUtils.isEmpty(typeId)) {
			typeId = "-1";
		}
		
		this.faqSubjectAreaDetailsBooksBean = worker.searchFaqSubjectAreaDetailsBooksById(Integer.parseInt(detailsId));
		this.faqSubjectAreaTypeBookByIdBeans = worker.searchFaqSubjectAreaTypeBooksById(Integer.parseInt(typeId));
		
		return "";
	}
	
	/**
	 * @return the faqId
	 */
	public int getFaqId() {
		return faqId;
	}
	/**
	 * @param faqId the faqId to set
	 */
	public void setFaqId(int faqId) {
		this.faqId = faqId;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * @return the subjectAreaType
	 */
	public String getSubjectAreaType() {
		return subjectAreaType;
	}
	/**
	 * @param subjectAreaType the subjectAreaType to set
	 */
	public void setSubjectAreaType(String subjectAreaType) {
		this.subjectAreaType = subjectAreaType;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the helpPage
	 */
	public String getHelpPage() {
		return helpPage;
	}
	/**
	 * @param helpPage the helpPage to set
	 */
	public void setHelpPage(String helpPage) {
		this.helpPage = helpPage;
	}	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the faqs
	 */
	public List<FaqBean> getFaqs() {
		return faqs;
	}
	/**
	 * @param faqs the faqs to set
	 */
	public void setFaqs(List<FaqBean> faqs) {
		this.faqs = faqs;
	}

	/**
	 * @return the faqSubjectAreaTypeBooksBeans
	 */
	public List<FaqSubjectAreaTypeBooksBean> getFaqSubjectAreaTypeBooksBeans() {
		return faqSubjectAreaTypeBooksBeans;
	}

	/**
	 * @param faqSubjectAreaTypeBooksBeans the faqSubjectAreaTypeBooksBeans to set
	 */
	public void setFaqSubjectAreaTypeBooksBeans(
			List<FaqSubjectAreaTypeBooksBean> faqSubjectAreaTypeBooksBeans) {
		this.faqSubjectAreaTypeBooksBeans = faqSubjectAreaTypeBooksBeans;
	}

	/**
	 * @return the faqSubjectAreaDetailsBooksBeans
	 */
	public List<FaqSubjectAreaDetailsBooksBean> getFaqSubjectAreaDetailsBooksBeans() {
		return faqSubjectAreaDetailsBooksBeans;
	}

	/**
	 * @param faqSubjectAreaDetailsBooksBeans the faqSubjectAreaDetailsBooksBeans to set
	 */
	public void setFaqSubjectAreaDetailsBooksBeans(
			List<FaqSubjectAreaDetailsBooksBean> faqSubjectAreaDetailsBooksBeans) {
		this.faqSubjectAreaDetailsBooksBeans = faqSubjectAreaDetailsBooksBeans;
	}

	/**
	 * @return the faqSubjectAreaDetailsBooksBean
	 */
	public FaqSubjectAreaDetailsBooksBean getFaqSubjectAreaDetailsBooksBean() {
		return faqSubjectAreaDetailsBooksBean;
	}

	/**
	 * @param faqSubjectAreaDetailsBooksBean the faqSubjectAreaDetailsBooksBean to set
	 */
	public void setFaqSubjectAreaDetailsBooksBean(
			FaqSubjectAreaDetailsBooksBean faqSubjectAreaDetailsBooksBean) {
		this.faqSubjectAreaDetailsBooksBean = faqSubjectAreaDetailsBooksBean;
	}

	/**
	 * @return the faqSubjectAreaTypeBookByIdBeans
	 */
	public List<FaqSubjectAreaTypeBooksBean> getFaqSubjectAreaTypeBookByIdBeans() {
		return faqSubjectAreaTypeBookByIdBeans;
	}

	/**
	 * @param faqSubjectAreaTypeBookByIdBeans the faqSubjectAreaTypeBookByIdBeans to set
	 */
	public void setFaqSubjectAreaTypeBookByIdBeans(
			List<FaqSubjectAreaTypeBooksBean> faqSubjectAreaTypeBookByIdBeans) {
		this.faqSubjectAreaTypeBookByIdBeans = faqSubjectAreaTypeBookByIdBeans;
	}	

	public String getCookieValue() {
		
		String cookieValue = "";
		boolean langCookieFound = false;
		String cookieVal = HttpUtil.getHttpServletRequestParameter("languageCode");
		Cookie[] cookies = HttpUtil.getHttpServletRequest().getCookies();
		int i;
		
		if(cookieVal == null){
			if(cookies != null){
				for(i = 0; i < cookies.length; i++){
					if("LanguageCookie".equals(cookies[i].getName())){
		            	if(cookies[i].getValue() == null){
		            		cookies[i].setValue("1");
		            		cookies[i].setMaxAge(60*60*24*365);   
							cookies[i].setPath("/");   
							((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookies[i]);
							
		            	}
		            	else{
		            		cookieValue = cookies[i].getValue();
		            		
		            	}
		            	langCookieFound = true;
					}
				}
			}
			
			if(!langCookieFound){
				
				Cookie cookie = new Cookie("LanguageCookie", "1");
				cookie.setMaxAge(60*60*24*365);
				cookie.setPath("/");
				((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookie);
				cookieValue = "1";
				
			}
				
		}
		else if(cookieVal != null){

					Cookie cookie = new Cookie("LanguageCookie", cookieVal);
					cookie.setMaxAge(60*60*24*365);
					cookie.setPath("/");
					((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookie);
					
					cookieValue = cookieVal;
					
		}
		
	
		return cookieValue;
	}
	 public String CookieDummy(){
		 return "faq";
	 }
	
}
