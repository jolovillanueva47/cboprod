package org.cambridge.ebooks.online.faq;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.jpa.faq.Faq;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectAreaDetailsBooks;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectAreaTypeBooks;
import org.cambridge.ebooks.online.jpa.faq.FaqSubjectTypeDetailsMapBooks;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * @author Karlson A. Mulingtapang 
 * FAQDAO.java - FAQ Data Access Object
 */
public class FaqDAO {

	private static EntityManagerFactory emf = PersistenceUtil.emf;



	@SuppressWarnings("unchecked")
	public static List<Faq> searchAllFaq() {
		List<Faq> result;

		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(Faq.SEARCH_ALL);

		try {
			result = (List<Faq>) query.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<FaqSubjectAreaTypeBooks> searchAllFaqSubjectAreaTypeBooks() {
		List<FaqSubjectAreaTypeBooks> result;

		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(FaqSubjectAreaTypeBooks.SEARCH_ALL);

		try {
			result = (List<FaqSubjectAreaTypeBooks>) query.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}
	
	public static List<FaqSubjectAreaTypeBooks> searchFaqSubjectAreaTypeBooksById(int id) {
		List<FaqSubjectAreaTypeBooks> result;

		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(FaqSubjectAreaTypeBooks.SEARCH_BY_ID);
		
		try {	
			query.setParameter(1, id);
			result = (List<FaqSubjectAreaTypeBooks>) query.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<FaqSubjectTypeDetailsMapBooks> searchFaqSubjectTypeDetailsMapBooksBySubjectAreaTypeId(
			int id) {

		List<FaqSubjectTypeDetailsMapBooks> result;
		EntityManager em = emf.createEntityManager();
		Query query = em
				.createNamedQuery(FaqSubjectTypeDetailsMapBooks.SEARCH_BY_SUBJECT_AREA_TYPE_ID);

		try {
			query.setParameter(1, id);
			result = (List<FaqSubjectTypeDetailsMapBooks>) query
					.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}

	public static FaqSubjectAreaDetailsBooks searchFaqSubjectAreaDetailsBooksById(
			int id) {

		FaqSubjectAreaDetailsBooks result = null;
		EntityManager em = emf.createEntityManager();
		Query query = em
				.createNamedQuery(FaqSubjectAreaDetailsBooks.SEARCH_BY_ID);
		try {
			query.setParameter(1, id);
			if(query.getSingleResult() instanceof FaqSubjectAreaDetailsBooks) {
				result = (FaqSubjectAreaDetailsBooks) query.getSingleResult();
			}
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}
	
	public static List<FaqSubjectAreaDetailsBooks> searchFaqSubjectAreaDetailsBooksById_all(
			List<FaqSubjectTypeDetailsMapBooks> records	) {
		
		List<FaqSubjectAreaDetailsBooks> faqResults = new LinkedList<FaqSubjectAreaDetailsBooks>();
		
		EntityManager em = emf.createEntityManager();
		
		String sql = "Select * from FAQ_SUBJECT_AREA_DETAILS_BOOKS";
		sql = sql + " WHERE ID IN (";
		for(int i = 0 ; i < records.size() ; i++){
			sql = sql + records.get(i).getFaqSubjectAreaDetailsId() ;
			if(!(i == (records.size()-1))){
				sql = sql + ",";
			}
		}
		if(StringUtils.isEmpty(sql)){
			return null;
		}
		
		sql = sql + ") ORDER BY DISPLAY_ORDER, QUESTION";
		
		
		//System.out.println("the sql = "+sql);
		em.persist((Object)new FaqSubjectAreaDetailsBooks());
		Query query = em.createNativeQuery(sql,new FaqSubjectAreaDetailsBooks().getClass());

		try {
			faqResults =  query.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			faqResults = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			faqResults = null;
		} finally {
			em.close();
		}

		return faqResults;
	}
	//apiratne

	@SuppressWarnings("unchecked")
	public static List<FaqSubjectAreaDetailsBooks> searchAllFaqSubjectAreaDetailsBooks() {
		List<FaqSubjectAreaDetailsBooks> result;
		EntityManager em = emf.createEntityManager();
		Query query = em
				.createNamedQuery(FaqSubjectAreaDetailsBooks.SEARCH_ALL);
		try {
			result = (List<FaqSubjectAreaDetailsBooks>) query.getResultList();
		} catch (NoResultException e) {
			System.out.println("[NoResultException]: " + e.getLocalizedMessage());
			result = null;
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}

		return result;
	}
}
