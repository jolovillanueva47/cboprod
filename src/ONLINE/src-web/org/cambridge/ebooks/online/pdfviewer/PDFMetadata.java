package org.cambridge.ebooks.online.pdfviewer;

public class PDFMetadata {

	private String bookId;
	private String contentId;
	private String eisbn;
	private String doi;
	private String pdf;
	private String contentType;
	
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getEisbn() {
		return eisbn;
	}
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getPdf() {
		return pdf;
	}
	public void setPdf(String pdf) {
		this.pdf = pdf;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	
}
