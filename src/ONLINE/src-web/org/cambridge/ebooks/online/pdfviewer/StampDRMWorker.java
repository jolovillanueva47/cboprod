package org.cambridge.ebooks.online.pdfviewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.UrlUtil;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;

/**
 * 
 * @author jgalang
 * 
 */

public class StampDRMWorker {
	
	private static final Logger LOGGER = Logger.getLogger(StampDRMWorker.class);
	
	public static class _PdfTextStripper extends PDFTextStripper {
		
		private float lastTextYPos;
		
		public _PdfTextStripper() throws IOException {
			super();
		}

		public _PdfTextStripper(Properties props) throws IOException {
			super(props);
		}

		public _PdfTextStripper(String encoding) throws IOException {
			super(encoding);
		}

		protected void processTextPosition(TextPosition tp) {
			super.processTextPosition(tp);
			this.lastTextYPos = tp.getY();
		}

		/**
		 * @return the lastTextYPos
		 */
		public float getLastTextYPos() {
			return lastTextYPos;
		}	
	}
	
	public static void stampDRM(HttpServletRequest request, HttpServletResponse response, PDFMetadata meta) throws Exception {
		
		if (null == meta) {				
			///add pda logs
			//addPDALogs(accessType, request);
			throw new NullPointerException("PDFMetadata is null");
		}
		
//		boolean hasAccess = false;
		
		String booklandingUrl = UrlUtil.getClientDns(request) + "ebook.jsf?bid=" + meta.getBookId();
		
		LOGGER.info("is (meta != null) : " + (meta != null));
		

		String pdfPath = EBooksProperties.CONTENT_DIR + meta.getEisbn() + "/";
	
        String clusterClientIp = request.getHeader("X-Cluster-Client-Ip");
        LOGGER.info("X-Cluster-Client-Ip: " + clusterClientIp);
		final String clientIp = StringUtils.isNotEmpty(clusterClientIp) ? clusterClientIp : request.getRemoteAddr();
				
		LOGGER.info(pdfPath + meta.getPdf());
		
		File pdfFile = new File(pdfPath + meta.getPdf());
		if(null == pdfFile || !pdfFile.exists()) {
			throw new IOException("File not found: " + pdfFile.getName());
		}
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline; filename=" + meta.getContentId() + ".pdf");
//		response.setHeader("Cache-Control", "max-age=900");
		 
		PdfReader reader = null;
		PdfStamper stamper = null;

		PDFParser pdfParser = null;
		_PdfTextStripper textStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		
		try {								
	        Image img = Image.getInstance(System.getProperty("stamp.drm.image.path").trim());
	        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
	        
	        // we create a reader for a certain document
	        reader = new PdfReader(new RandomAccessFileOrArray(pdfPath + meta.getPdf()), null);
	        
	        // PDF BOX ============
	        pdfParser = new PDFParser(new FileInputStream(pdfPath + meta.getPdf()));
	        pdfParser.parse();
			cosDoc = pdfParser.getDocument();
			textStripper = new _PdfTextStripper();
			pdDoc = new PDDocument(cosDoc);
	        // =======================
			
	        stamper = new PdfStamper(reader, response.getOutputStream());
//		    stamper.setEncryption(PdfWriter.STRENGTH40BITS, null, null, PdfWriter.AllowCopy | PdfWriter.AllowPrinting | PdfWriter.AllowScreenReaders);
//	        stamper.setEncryption(true, null, null, 0);
	        
	        boolean hasCopyright = false;
	        int pdfPages = reader.getNumberOfPages();
	        for(int pageNum = 1; pageNum <= pdfPages; pageNum++) {
	        	
	        	// check copyright of 1st page only
	            if(pageNum == 1) {
	                textStripper.setStartPage(pageNum);
	                textStripper.setEndPage(pageNum);
	    			String pdDocStr = textStripper.getText(pdDoc);					
	    			hasCopyright = pdDocStr.contains("Cambridge Books Online © Cambridge University Press");
	    			LOGGER.info("hasCopyright:" + hasCopyright);					
	            }
	            	            
	        	doStamp(reader, hasCopyright, pageNum, textStripper, pdDoc, img, stamper, bf, clientIp, meta.getDoi(), booklandingUrl);	        	
	        }
	        	        
		} catch (Exception e) {
			e.printStackTrace();
		} finally {			
			if(null != stamper) {
				stamper.close();
			}
			if(null != reader) {
				reader.close();
			}
			if(null != pdDoc) {
				pdDoc.close();
			}
			if(null != cosDoc) {
				cosDoc.close();	
			}				
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
			
	}	
	
	private static void doStamp(PdfReader reader, boolean hasCopyright, int pageNum, _PdfTextStripper textStripper, 
			PDDocument pdDoc, Image img, PdfStamper stamper, BaseFont bf, 
			String clientIp, String doi, String booklandingUrl) throws Exception {
				
		int pageRotation = reader.getPageRotation(pageNum);

		float width = reader.getPageSizeWithRotation(pageNum).getWidth();
		float cropWidth = reader.getCropBox(pageNum).getWidth();
		float cropLeft = reader.getCropBox(pageNum).getLeft();	    		
		float cropBottom = reader.getCropBox(pageNum).getBottom();
		
		if(pageRotation == 90 || pageRotation == 270) {		    			
			cropWidth = reader.getCropBox(pageNum).rotate().getWidth();
    		cropLeft = reader.getCropBox(pageNum).rotate().getLeft();	    		
    		cropBottom = reader.getCropBox(pageNum).rotate().getBottom();
		}	 				    		
		
		PdfContentByte overBase = stamper.getOverContent(pageNum);
		
		if(hasCopyright) {		    			
			float widthPosImg = ((cropWidth / 2) + cropLeft) - (img.getWidth() / 2);
			if(width < cropWidth) {			    			  		
	    		widthPosImg = ((width / 2) + cropLeft) - (img.getWidth() / 2);
    		}
			float heightPosImg = ((reader.getPageSizeWithRotation(pageNum).getHeight() - textStripper.getLastTextYPos()) 
    				- (img.getHeight() / 2));
			
			img.setAbsolutePosition(widthPosImg, heightPosImg);
    		overBase.addImage(img);
		} 	
			    		
		setStampText(bf, overBase, clientIp, doi, width, cropWidth, cropBottom, cropLeft, booklandingUrl);
	}
	
	private static void setStampText(BaseFont bf, PdfContentByte cb, String clientIp, String doi, 
			float width, float cropWidth, float cropBottom, float cropLeft, String booklandingUrl) throws IOException, DocumentException {
		
		float totalWidth = width;
		if(totalWidth > cropWidth) {
			totalWidth = cropWidth;
		}
		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		String copyright = "Cambridge Books Online \u00A9 Cambridge University Press, " + year;
		
		
		PdfPTable footer = new PdfPTable(1); 
		footer.setTotalWidth(totalWidth);
        footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);  
                
        // ====== drm
		StringBuffer drm = new StringBuffer();
		drm = new StringBuffer("Downloaded from Cambridge Books Online by IP ");
		drm.append(clientIp);
		drm.append(" on ");
		drm.append(new Date());
		drm.append(".");        
		
		Font fDrm = new Font(bf);
		fDrm.setSize(4.75f);  
        
        Phrase pDrm = new Phrase();
        pDrm.setFont(fDrm);
        pDrm.add(drm.toString());
        
        PdfPCell cDrm = new PdfPCell();
        cDrm.setPhrase(pDrm);
        cDrm.setBorder(Rectangle.NO_BORDER);
        cDrm.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        cDrm.setPadding(0);
        
        footer.addCell(cDrm);  
        
        // ====== url
        Font fUrl = new Font(bf);
        fUrl.setStyle(Font.UNDERLINE);
        fUrl.setSize(4.75f);  
        fUrl.setColor(BaseColor.BLUE);
        
        float paddingUrl = 1;
        
        if (StringUtils.isNotEmpty(doi)) {	
        	Phrase pUrl = new Phrase();
        	pUrl.setFont(fUrl);
        	pUrl.add("http://dx.doi.org/" + doi);
        	
        	PdfPCell cUrl = new PdfPCell();
        	cUrl.setPhrase(pUrl);
        	cUrl.setBorder(Rectangle.NO_BORDER);
        	cUrl.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        	cUrl.setPadding(paddingUrl);
        	 
        	footer.addCell(cUrl);  
        } else {        	
        	Phrase pUrl = new Phrase();
        	pUrl.setFont(fUrl);
        	pUrl.add(booklandingUrl);
        	
        	PdfPCell cUrl = new PdfPCell();
        	cUrl.setPhrase(pUrl);
        	cUrl.setBorder(Rectangle.NO_BORDER);
        	cUrl.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        	cUrl.setPadding(paddingUrl);
        	
        	footer.addCell(cUrl);  
        }
        
        // ====== copyright
        Font fCopyright = new Font(fDrm);
        
        Phrase pCopyright = new Phrase();
        pCopyright.setFont(fCopyright);
        pCopyright.add(copyright);
        
        PdfPCell cCopyright = new PdfPCell();
        cCopyright.setPhrase(pCopyright);
        cCopyright.setBorder(Rectangle.NO_BORDER);
        cCopyright.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        cCopyright.setPadding(0);
        
        footer.addCell(cCopyright);  
        
        footer.writeSelectedRows(0, -1, cropLeft + 5, cropBottom + 20, cb);  		
	}		
}
