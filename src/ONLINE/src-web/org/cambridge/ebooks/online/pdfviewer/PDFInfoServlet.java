/**
 * 
 */
package org.cambridge.ebooks.online.pdfviewer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

import com.itextpdf.text.pdf.PdfReader;

/**
 * @author kmulingtapang
 */
public class PDFInfoServlet extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(PDFInfoServlet.class);
	
	private static final long serialVersionUID = -3221964177656951044L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		resp.setContentType("text/plain");
		PrintWriter out = resp.getWriter();
		
		PDFMetadata meta = PDFMetadataWorker.getMetadata(req);
		
		String pdfPath = EBooksProperties.CONTENT_DIR + meta.getEisbn() + "/";
		
		PdfReader reader = null;
		
		
		try {
			 reader = new PdfReader(pdfPath + meta.getPdf());
			 int pdfPages = reader.getNumberOfPages();
			 float totalHeight = 0;
			 float highestWidth = 0;
			 for(int pageNum = 1; pageNum <= pdfPages; pageNum++) {
				 totalHeight += getPdfHeight(reader, pageNum) + 8;//8 = space between pages
				 highestWidth = getPdfWidth(reader, pageNum, highestWidth);
			 }
			 totalHeight +=8; // for extra space in the last page
			 LOGGER.info("----totalHeight:" + totalHeight);
			 LOGGER.info("----highestWidth:" + highestWidth);
			 /*if(highestWidth>700){
				 highestWidth = 700;
			 }
			 highestWidth += 16; */// for extra space
			 LOGGER.info("----highestWidth:" + highestWidth);
			 out.print(highestWidth + "," + totalHeight);
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			out.close();
			
			if(null != reader) {
				reader.close();
			}
		}
	}	
	
	private static float getPdfHeight(PdfReader reader, int pageNum) {
		float h = 0;
		
		int pageRotation = reader.getPageRotation(pageNum);

//		float height = reader.getPageSizeWithRotation(pageNum).getHeight();
		float cropHeight = reader.getCropBox(pageNum).getHeight();
		
		if(pageRotation == 90 || pageRotation == 270) {		    			
			cropHeight = reader.getCropBox(pageNum).rotate().getHeight();
		}	
//		LOGGER.info("height " + height );
		LOGGER.info("cropHeight " + cropHeight );
		h = cropHeight;
//		if( height < cropHeight ) {	
//			h = height;		    		
//		} else {
//			h = cropHeight;
//		}
		
		LOGGER.info("page:" + pageNum + " height:" + h);
			
		return h;
	}
	
	private static float getPdfWidth(PdfReader reader, int pageNum, float highestWidth) {
		float w = 0;
		
		int pageRotation = reader.getPageRotation(pageNum);

//		float width = reader.getPageSizeWithRotation(pageNum).getWidth();
		float cropWidth = reader.getCropBox(pageNum).getWidth();
		
//		LOGGER.info("width " + width );
//		LOGGER.info("cropWidth " + cropWidth );
		if(pageRotation == 90 || pageRotation == 270) {		    			
			cropWidth = reader.getCropBox(pageNum).rotate().getWidth();
		}	
		LOGGER.info("cropWidth2 " + cropWidth );
		w = cropWidth;
//		if(cropWidth < width) {
//			w = width;		    		
//		} else {
//			w = cropWidth;
//		}
		
		if(highestWidth > w) {
			w = highestWidth;
		}
				
		return w;
	}
}
