package org.cambridge.ebooks.online.pdfviewer;

import javax.servlet.http.HttpServletRequest;

import org.cambridge.ebooks.online.util.SolrUtil;

public class PDFMetadataWorker {

	public static PDFMetadata getMetadata(HttpServletRequest request) {
//		String contentId = request.getParameter("cid");
		String contentId = request.getPathInfo().substring(1);

		return SolrUtil.getPdfMetaData(contentId);
	}	
}
