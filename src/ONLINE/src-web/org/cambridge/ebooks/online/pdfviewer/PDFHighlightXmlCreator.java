package org.cambridge.ebooks.online.pdfviewer;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFHighlighter;
import org.cambridge.ebooks.online.util.EBooksProperties;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrUtil;

/**
 * @author kmulingtapang
 * Java servlet that creates an xml as its content.
 */
public class PDFHighlightXmlCreator extends HttpServlet {

	private static final long serialVersionUID = -8758629538580365895L;
	
	private static final Logger LOGGER = Logger.getLogger(PDFHighlightXmlCreator.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info("doPost");
		
		String contentId = req.getParameter("cid");
		
		List<String> searchTexts = new ArrayList<String>();
		if(null == req.getSession().getAttribute("searchText")) {	
			if(null == req.getSession().getAttribute("advanceSearchParam")) {
				return;
			}
			
			Map<String, String[]> param = (Map<String, String[]>)req.getSession().getAttribute("advanceSearchParam");
			String[] searchText1 = param.get("search_text");
			if(null != searchText1 && searchText1.length > 0) {
				for(String t : searchText1) {
					String searchText = t.replaceAll("\"", "");
					String[] searchTextArr = searchText.split(" ");
					for(String s : searchTextArr) {
						searchTexts.add(s);
					}
				}
			}			
			String[] searchText2 = param.get("search_text_2");
			if(null != searchText2 && searchText2.length > 0) {
				for(String t : searchText2) {
					String searchText = t.replaceAll("\"", "");
					String[] searchTextArr = searchText.split(" ");
					for(String s : searchTextArr) {
						searchTexts.add(s);
					}
				}
			}
		} else {
			String searchText = ((String)req.getSession().getAttribute("searchText")).replaceAll("\"", "");
			String[] searchTextArr = searchText.split(" ");
			for(String s : searchTextArr) {
				searchTexts.add(s);
			}				
		}
		
		LOGGER.info("searchText:" + searchTexts.toString());
		
		PDFMetadata meta  = SolrUtil.getPdfMetaData(contentId);
		String pdfPath = EBooksProperties.CONTENT_DIR + meta.getEisbn() + "/";
	
		if(!new File(pdfPath + meta.getPdf()).exists()) {
			throw new IOException();
		}
				
		resp.setContentType("text/xml");
		PrintWriter out = resp.getWriter();
		PDFHighlighter highlighter = null;
		PDDocument pdDoc = null;
		try {
			highlighter = new PDFHighlighter();
			URL url = toUrl(pdfPath + meta.getPdf());		
			pdDoc = PDDocument.load(url);
			highlighter.generateXMLHighlight(pdDoc, searchTexts.toArray(new String[0]), out);
		} catch (IOException e) {
			throw e;
		} finally {
			out.close();
			pdDoc.close();			
		}
	}
	
	public URL toUrl(String directory){
	    File file = new File(directory);
	    URL url = null;
	    try {
	      url = file.toURI().toURL();   
	    } catch (Exception e){
	    	LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
	    }
	    return url; 
	}
}
