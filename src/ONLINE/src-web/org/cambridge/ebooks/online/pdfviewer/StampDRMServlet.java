package org.cambridge.ebooks.online.pdfviewer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.view.EBooksAccessListView;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.pda.PdaBean;
import org.cambridge.ebooks.online.subscription.AccessList;
import org.cambridge.ebooks.online.subscription.SubscriptionUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.SolrUtil;
import org.cambridge.ebooks.online.util.UrlUtil;

/**
 * 
 * @author jgalang
 * 
 */

public class StampDRMServlet extends HttpServlet {

	private static final long serialVersionUID = 4527866679492926998L;

	private static final Logger logger = Logger.getLogger(StampDRMServlet.class);
	
	@SuppressWarnings("unused")
	private static final String TITLE = "CambridgeBooksOnline-Chapter";

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	

	public static void addPDALogs(EBooksAccessListView accessType, PDFMetadata meta, HttpServletRequest request){
		logger.info("PATRON_DRIVEN = " + AccessList.PATRON_DRIVEN );
		logger.info("ORDER_TYPE accessType.getOrderType() = " + accessType.getOrderType() );
		if(AccessList.PATRON_DRIVEN.equals(accessType.getOrderType())){
			//add logs for mdb
			
			//String productId = accessType.getCollectionId() != null ? accessType.getCollectionId() : accessType.getEisbn();
			
			PdaBean b = new PdaBean();
			//b.setAgreementId(agreementId);
			b.setContentId(meta.getContentId());
			b.setIpAddress(request);
			b.setOrderId(accessType.getOrderId());
			b.setReferrer(request);
			b.setSessionId(request.getSession().getId());
						
			
			logger.info("Stamp DRM about to log pda");
//			PdaLogger.logEvent(b);			
			logger.info("Stamp DRM about sent to pda");
		}
		//PdaLogger.logEvent(new PdaBean());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			PDFMetadata meta = PDFMetadataWorker.getMetadata(request);
			
			String bodyIds = OrgConLinkedMap.getAllBodyIds(request.getSession());
			String directBodyIds = OrgConLinkedMap.getDirectBodyIds(request.getSession());
			SubscriptionUtil subscription = new SubscriptionUtil(bodyIds, directBodyIds);
			
			EBooksAccessListView ealv = subscription.getAccessType(meta.getEisbn(), request, true);
			logger.info("checking access = " + meta.getContentId());
			if(SubscriptionUtil.isFreeContent(meta.getContentType())){
				logger.info("free content");
				StampDRMWorker.stampDRM(request, response, meta);
			}else if(ealv.hasAccess()){
				logger.info("paid content");
				StampDRMWorker.stampDRM(request, response, meta);
				//add pda logs, do not add it for free content
				addPDALogs(ealv, meta, request);				
			}else{
				logger.info("no access");
				//no access, determine what type of no access
				if(ealv.isConcurrencyError()){
					logger.info("concurrency access denied");
					responseConcurrencyError(request, response, meta.getEisbn());
				}else{
					logger.info("really has no access");
					//really just have no access
					responseRedirect(request, response);
				}
			}
			
		
				
		} catch (Exception e) {			
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	@SuppressWarnings("unused")
	private void responseForbidden(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/html");
		
		PrintWriter writer = response.getWriter();
		
		writer.println("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">");
		writer.println("<html><head>");
		writer.println("<title>403 Forbidden</title>");
		writer.println("</head><body>");
		writer.println("<h1>Forbidden</h1>");
		
		String cid = request.getParameter("cid");
		if (StringUtils.isNotEmpty(cid)) {
			writer.println("<p>You don't have permission to access " + cid);
			writer.println("on this server.</p>");
		}

		writer.println("</body></html>");

		writer.close();
		
	}	
	
	/*
	 * redirect to chapterlandingpagebean -- PID 0066361: PDF viewer behavior for unknown users   
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void responseRedirect(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String cid = request.getPathInfo().substring(1);
		if(StringUtils.isNotEmpty(cid)){
			cid = cid.split("#")[0];
		}
		String path = request.getServletPath();		
		String bookId = SolrUtil.getBookIdByChapterId(cid);		
		if(path.contains("/aaa/")){
			response.sendRedirect(UrlUtil.getClientDns(request) + "aaa/chapter.jsf?bid=" + bookId + "&cid=" + cid);
		}else{
			response.sendRedirect(UrlUtil.getClientDns(request) + "chapter.jsf?bid=" + bookId + "&cid=" + cid);
		}
	}
	
	private void responseConcurrencyError(HttpServletRequest request, HttpServletResponse response, String isbn) throws Exception {
		response.sendRedirect(UrlUtil.getClientDns(request) + "error/concurrency_error.jsf?isbn=" + isbn);
	}	
}
