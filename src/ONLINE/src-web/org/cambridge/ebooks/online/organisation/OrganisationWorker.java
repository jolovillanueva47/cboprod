package org.cambridge.ebooks.online.organisation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.subscription.BookCollection;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.oracle.ip.ConvertIp;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.StringUtil;
/**
 * 
 * @author ??
 * edited mmanalo add findBookList method
 */
public class OrganisationWorker {
	
	private static final Logger LOGGER = Logger.getLogger(OrganisationWorker.class);
	
	public static final String ISBN_ACCESS_LIST = "ISBN_ACCESS_LIST"; 
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public static Organisation findOrganisation( final String id ) {
		if ( StringUtil.isEmpty(id) ) { 
			throw new IllegalArgumentException(" findOrganisation(" + id + ") cannot be null ");			
		}
		Organisation organisation = null;
		EntityManager em = emf.createEntityManager();
		try {			
			Query query = em.createNamedQuery(Organisation.GET_ORG);
			query.setParameter(1, id);
			
			organisation = (Organisation) query.getSingleResult();
		} catch (Exception e) {			
			organisation = null;
		} finally {
			em.close();
		}
		
		return organisation;
	}
	
//	public static void setOrganisationInSession( HttpSession session, Organisation organisation ) {
//		if ( organisation != null ) {
//			//session.setAttribute("orgName", organisation.getName());
//			//session.setAttribute("orgBodyId", organisation.getBodyId());
//			//session.setAttribute("orgDisplayName", organisation.getDisplayName());
//			//session.setAttribute(ISBN_ACCESS_LIST, getAccessList(session));
//		}
//	}
	
	
	/*
	public static void setOrganisationDisplayInSession( HttpSession session ) {
		
		//session.removeAttribute("orgNames");
		
		String orgName = StringUtil.nvl(session.getAttribute("orgDisplayName"));
		String[] ipMembershipNames =  (String[]) session.getAttribute("IPmembershipNames");   
		String[] membershipNames = (String[]) session.getAttribute("membershipNames");   
		
		String orgBodyId = StringUtil.nvl(session.getAttribute("orgBodyId"));
		String[] ipMembershipIds = (String[]) session.getAttribute("IPmembership"); 
		String[] membershipIds =(String[]) session.getAttribute("membership");
		
		List<Organisation> membershipOrgs = (List<Organisation>)session.getAttribute("membershipOrgs");
		List<Organisation> ipMembershipOrgs = (List<Organisation>)session.getAttribute("IPmembershipOrgs");
		
		Map<String,String> organisationMap = new LinkedHashMap<String,String>();
		Set<String>	organisationIdSet = new LinkedHashSet<String>();
		List<String> organisationNames = new LinkedList<String>();
		
		LogManager.info(OrganisationWorker.class,"LOGO check");
		LogManager.info(OrganisationWorker.class,"orgDisplayName = " + orgName);
		LogManager.info(OrganisationWorker.class,"orgBodyId = " + orgBodyId);		
		
		LogManager.info(OrganisationWorker.class,"orgnames before admin = " + organisationNames);
		if ( StringUtil.isNotEmpty( orgBodyId ) ) {
			if ( organisationIdSet.add(orgBodyId) ) { 
				organisationNames.add( orgName );
				organisationMap.put(orgBodyId, orgName);
			}
		}
		
		LogManager.info(OrganisationWorker.class,"orgnames before memberships = " + organisationNames);
		
		//C equals if for ipauthentication
		//1 equals for membership authenticaion,
		//both 1 and C are for consortium
		
		//get all consortium first for ipmembership and membership
		if(membershipOrgs != null){
			for(Organisation org : membershipOrgs){
				if(("C".equals(org.getType()) || "1".equals(org.getType()) ) 
						&& !organisationIdSet.contains(org.getBodyId())){		
					organisationIdSet.add(org.getBodyId());
					organisationNames.add( org.getDisplayName() );
					organisationMap.put(org.getBodyId(), org.getDisplayName());
				}
			}
		}
		
		if(ipMembershipOrgs != null){
			for(Organisation org : ipMembershipOrgs){
				if(("C".equals(org.getType()) || "1".equals(org.getType()) ) && !organisationIdSet.contains(org.getBodyId())){
					organisationIdSet.add(org.getBodyId());
					organisationNames.add( org.getDisplayName() );
					organisationMap.put(org.getBodyId(), org.getDisplayName());
				}
			}
		}
		
		//get all org first for ipmembership and membership
		
		if(membershipOrgs != null){
			for(Organisation org : membershipOrgs){
				if(!("C".equals(org.getType()) || "1".equals(org.getType()) ) && !organisationIdSet.contains(org.getBodyId())){		
					organisationIdSet.add(org.getBodyId());
					organisationNames.add( org.getDisplayName() );
					organisationMap.put(org.getBodyId(), org.getDisplayName());
				}
			}
		}
		
		if(ipMembershipOrgs != null){
			for(Organisation org : ipMembershipOrgs){
				if(!("C".equals(org.getType()) || "1".equals(org.getType()) ) && !organisationIdSet.contains(org.getBodyId())){					
					organisationIdSet.add(org.getBodyId());
					organisationNames.add( org.getDisplayName() );
					organisationMap.put(org.getBodyId(), org.getDisplayName());
				}
			}
		}
				
		LogManager.info(OrganisationWorker.class,"orgnames = " + organisationNames);
		
		
		boolean orgNamesMoreThan4 = organisationNames.size() > 4;
		
		if ( orgNamesMoreThan4 ) {
			organisationNames = organisationNames.subList( 0, 4 );
		}
		
		
		String[] orgNames = organisationNames.toArray(new String[0]);
		String listOfDisplayNames = StringUtil.join( orgNames, ", " ) ; 
		
		
		ArrayList<String> openURLresolverList = new ArrayList<String>();
		boolean hasLogo = false;
		boolean thisIsTheFirstOrg = true;
		String firstOrgBodyId = "";
		
		
		EntityManager em = emf.createEntityManager();
		try {
			for ( String orgId : organisationIdSet ) { 
				
				Organisation org = findOrganisation( orgId );
				if(org == null){
					continue;
				}
				
				if ( thisIsTheFirstOrg ) { 
					firstOrgBodyId = orgId;
					if ( StringUtil.isNumeric(firstOrgBodyId) ) {
						hasLogo = "Y".equals(org.getHasImage());
					}
					thisIsTheFirstOrg = false;
				}			
				
				
				OpenUrlResolver urlResolver = em.find( OpenUrlResolver.class, org.getBodyId() );
				if ( urlResolver != null ) { 
					em.refresh( urlResolver );
					String openUrlResolver = urlResolver.getUrlResolverPath();
					if(Misc.isNotEmpty(openUrlResolver)){
						openURLresolverList.add( openUrlResolver );
					}
				}			
			}
		} catch (Exception e) {
			LogManager.error(OrganisationWorker.class, e.getMessage());
		} finally {
			em.close();
		}
		
		String logoUrl = EBooksConfiguration.getProperty("org.logo.url").replaceAll("<BODYID>", firstOrgBodyId );
		
		if ( StringUtil.isNotEmpty(listOfDisplayNames) ) {
			//session.setAttribute("orgIdSet", organisationIdSet);
			//session.setAttribute("orgHasLogo", hasLogo );
			//session.setAttribute("orgLogo", logoUrl );
			//session.setAttribute("orgNames", listOfDisplayNames);
			//session.setAttribute("orgNamesMoreThan4", orgNamesMoreThan4);
			//session.setAttribute("openURLresolverList", openURLresolverList);
			//session.setAttribute("organisationMap",organisationMap);
		}
		
		//will be used for CJO login
		//setOrgIdSetInSession(session);
	}
	*/
	
	public static boolean hasNoActiveSubscription(String bodyIds) {		
		EntityManager em = emf.createEntityManager();
		String sql = 
			"select ORG.BODY_ID " +
			"from ORGANISATION org, " +
			    "(select org.body_id, nvl(vw.CNT, 0) CNT " +
			    "from organisation org, " +
			          "(select count(1) CNT, vw.body_id " + 
			          "from ebooks_access_list_view vw " +
			          "where vw.body_id in (" + bodyIds + ") " +
			          "group by vw.body_id) vw " +
			    "where org.body_id = vw.body_id (+) " +
			    "and org.body_id in (" + bodyIds + ") " +
			    "and (CNT is null or CNT = 0) " +
			    ") ACTIVE, " +
			    "(select BODY_ID " + 
			    "from ebook_order_trial trial, ebook_order ord " +
			    "where BODY_ID in (" + bodyIds + ") " +
			    "and trial.order_id = ord.order_id) TRIAL " +
			"where ORG.BODY_ID = TRIAL.BODY_ID (+)   " +
			"and ORG.BODY_ID = ACTIVE.BODY_ID (+) " +
			"and ORG.BODY_ID in (" + bodyIds + ") " +
			"and ACTIVE.BODY_ID is not null " +
			"and TRIAL.BODY_ID is null ";
		List<Organisation> list = null;
		try {
			Query query = em.createNativeQuery(sql, Organisation.class);
			list = (List<Organisation>) query.getResultList();
			
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		if(list != null && list.size() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	public static void trackOrganisationLogin(HttpSession session) { 
		LinkedHashMap<String,Organisation> organisationMap = (LinkedHashMap<String,Organisation>)
				((OrgConLinkedMap) OrgConLinkedMap.getFromSession(session)).getMap();
		if ( organisationMap != null ) { 
			
			for ( Map.Entry<String,Organisation> pairs : organisationMap.entrySet() ) { 
				//@TRACKEVENT
//				EventTracker.trackEvent( Actions.LOGIN, EventObject.ORGANISATION, pairs.getValue(), pairs.getKey(), session );
//				LogManager.info( OrganisationWorker.class, pairs.getKey() + " " + pairs.getValue() +" has logged in.." );
			}
			
			
		}
	}
		
//	public static void removeOrganisationInSession( HttpSession session  ) {
//		//session.removeAttribute("orgName" );
//		//session.removeAttribute("orgBodyId" );
//		//session.removeAttribute("orgDisplayName" );
//		//session.removeAttribute("orgHasLogo" );
//		//session.removeAttribute("orgLogo" );
//	}
	
	
	
//	public static void setIPMembershipInSession( HttpSession session, OrgConLinkedMap orgMap ) { 
//		ArrayList<String> orgNames = new ArrayList<String>();
//		ArrayList<String> orgIds = new ArrayList<String>();
//		ArrayList<Organisation> _orgList = new ArrayList<Organisation>();
//		
//		OrganisationWorker.getOrgs(orgMap.getAllBodyIds(), orgMap);
//		for ( String bodyId : orgMap.keySet() ) {			
//			Organisation _org = orgMap.get(bodyId);
//			_orgList.add(_org);
//			orgNames.add(_org.getDisplayName());			
//			orgIds.add(_org.getBodyId());				
//		}
//		
//		
//		if ( orgIds != null && orgIds.size() > 0 ) {
//			
//			Organisation firstOrg = orgMap.getFirst();
//			String[] membershipNames = orgNames.toArray(new String[0]);
//			String[] ipMemberships	 = orgIds.toArray(new String[0]);
//			String logoUrl = EBooksConfiguration.getProperty("org.logo.url").replaceAll("<BODYID>", firstOrg.getBodyId() );
//			
//			//session.setAttribute( "IPmembershipOrgs", _orgList );
//			//session.setAttribute( "hasIPMembershipLogo", firstOrg.getHasImage() );
//			//session.setAttribute( "IPmembershipLogo",  	logoUrl );
//			//session.setAttribute( "IPmembershipNames", 	membershipNames );   //StringUtil.join(,',')
//			//session.setAttribute( "IPmembership", 		ipMemberships );
//		
//		}
//	}
	
	public static String getOrgList(String bodyId) {
		if(StringUtils.isNotEmpty(bodyId)){
			return null;
		}
		
		String sql =
			" SELECT UNIQUE a.organisation_id " + 
			" FROM membership a, member b, blade_member c " + 
			" WHERE a.BODY_ID = b.BODY_ID and " + 
			" b.BLADE_MEMBER_ID = c.MEMBER_ID AND a.status='Y' and a.activation_date <= sysdate and (a.expiration_date>= sysdate or a.expiration_date is null) and " + 
			" c.member_userid = ? " + 
			" union all SELECT UNIQUE a.organisation_id " + 
			" FROM MEMBERSHIP_CODE a, MEMBER b, BLADE_MEMBER c, ORGANISATION d " + 
			" WHERE a.BODY_ID = b.BODY_ID AND " + 
			" b.BLADE_MEMBER_ID = c.MEMBER_ID " + 
			" AND d.BODY_ID=a.ORGANISATION_ID AND d.SUPPRESS_MEMBER='N' AND " + 
			" c.member_userid = ? "; 
		return null;
	}
	
	/**
	 * gets both consortium and org
	 * @param ip
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Organisation> getIpAuthenticatedOrgs(String ip){
		System.out.println("OrganisationWorker.getIpAuthenticatedOrgs ip = " + ip );
		if(StringUtils.isEmpty(ip)){
			return null;
		}
		// == 1 plsql block, 1 would mean that it is in range
		String sql = "";
		EntityManager em = emf.createEntityManager();
		
		List<Organisation> orgList = null;
		try {
			/*
			Query query = null;
			if(isIP(ip)){
				String ipSplit[] = ipSplit(ip);
				sql =
					"select BODY_ID, DISPLAY_NAME, TYPE, IMAGE " +
					"from oraebooks.ebooks_IP_range_view " +				
					"where ?1 between NET1E and NET1S " +
					  "and ?2 between NET2E and NET2S " +
					  "and ?3 between NET3E and NET3S " +
					  "and ?4 between NET4E and NET4S ";
				query = em.createNativeQuery( sql , Organisation.class);
				query.setParameter(1, ipSplit[0]);
				query.setParameter(2, ipSplit[1]);
				query.setParameter(3, ipSplit[2]);
				query.setParameter(4, ipSplit[3]);
			*/
			Query query = null;			
			if(isIP(ip)){
				//String ipSplit[] = ipSplit(ip);
				long _ip = ConvertIp.ipToLong(ip);
				sql =
					"select BODY_ID, DIRECT " +
					"from oraebooks.ipaddress_identification_final " +				
					"where ?1 between start_ip_long and end_ip_long " +
					"  and INCLUDE = 'Y'" +
					"  and BODY_ID not in ( " +
					"		select BODY_ID from ipaddress_identification_final " +
					"		where ?1 between start_ip_long and end_ip_long " +
					"		  and INCLUDE = 'N' " +								
					"  )";
				query = em.createNativeQuery( sql , Organisation.class);
				query.setParameter(1, _ip);					
			}else{			
				sql =
					"select BODY_ID, DISPLAY_NAME, TYPE, IMAGE " +
					"from oraebooks.ebooks_IP_range_view " +
					"where DOMAIN = 1" +
					  "and INTERNET_ADDRESS = ?1";
				query = em.createNativeQuery( sql , Organisation.class);
				query.setParameter(1, ip);
			}		
			
			orgList = (List<Organisation>) query.getResultList();
			
			setAsIpAuthenticatedOrgs(orgList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return orgList;
	}
	
	
	private static void setAsIpAuthenticatedOrgs(List<Organisation> orgList){
		for(Organisation org : orgList){
			org.setIpAuthenticated(true);
		}
	}
	
	/**
	 * checks if the ip is a domain
	 * @param input
	 * @return
	 */
	private static boolean isIP(String input){
		 Pattern p = Pattern.compile("([0-9]|\\.)*");
	     Matcher m = p.matcher(input.toLowerCase());
	     return m.matches();
	}
	
	public static void main(String[] args) {
		System.out.println(isIP("123.123.bryn"));
	}
	
	private static String[] ipSplit(String ip){		
		return ip.split("\\.");
	}
	
	public static List<Organisation> findOrganisationByAthensId( String athensId ) { 
		if ( StringUtil.isEmpty(athensId) ) { 
			throw new IllegalArgumentException(" findOrganisationByAthensId(" + athensId + ") cannot be null ");
		}
		final String sql = 
				" select org.BODY_ID,org.NAME,org.DISPLAY_NAME,org.TYPE,org.ADDRESS,org.MESSAGE_TEXT,org.IMAGE  " +
				" from athens_cjo_organisation ath, organisation org   " +
				" where ath.athens_id = ? and ath.body_id = org.body_id ";
		
		Organisation result = null;
		EntityManager em = emf.createEntityManager();
		
		try {
			List<Organisation> orgList = ( List<Organisation>) em.createNativeQuery( sql , Organisation.class).setParameter( 1, athensId ).getResultList();
			if ( orgList != null && orgList.size() > 0 ) { 
				
				setOrgsAsAthens(orgList);
				//result = orgList.get(0);   // GET THE FIRST...
				return orgList;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());		
		} finally {
			em.close();
		}
		
		return null;
	}
	
	private static void setOrgsAsAthens(List<Organisation> orgList){
		for(Organisation org : orgList){
			org.setAthensOrg(true);
		}
	}
	
	private static final int ORDER_CONFIRMED = 4;
	@SuppressWarnings("unchecked")
	@Deprecated
	public static String findBookList(String bodyIds) {
		String sql =  
			"select wm_concat(ISBN) NAME, 0 BODY_ID " +
			"from " +
			  "(select ORD.BODY_ID, HDR.COLLECTION_ID, " + 
			         "decode( LIST.ISBN, null, LINE.PRODUCT_ID, LIST.ISBN) ISBN, " +       
			         "decode( ORD.ORDER_TYPE, 'D', DEMO.START_DATE_DEMO, HOSTING.START_DATE ) START_DATE_FN, " +
			         "decode( ORD.ORDER_TYPE, 'D', DEMO.END_DATE_DEMO, HOSTING.END_DATE ) END_DATE_FN " +
			  "from EBOOK_ORDER ORD, EBOOK_ORDER_LINE LINE, " +
			       "EBOOK_COLLECTION_HEADER HDR, EBOOK_COLLECTION_LIST LIST, " +
			       "(select max(ORD.END_DATE) END_DATE,  " +
			               "min(ORD.START_DATE) START_DATE, ORD.BODY_ID " +
			       "from EBOOK_ORDER ORD " +
			       "where ORD.ORDER_TYPE in ('H','P') " +
			         "and ORD.ORDER_STATUS = " + ORDER_CONFIRMED + " " +
			         "and ORD.BODY_ID in (" + bodyIds + ") " +
			       "group by ORD.BODY_ID) HOSTING, " +
			       "(select ORD.ORDER_ID,  " +
			               "ORD.END_DATE END_DATE_DEMO, ORD.START_DATE START_DATE_DEMO " +
			          "from EBOOK_ORDER ORD, EBOOK_ORDER_LINE LINE " +
			         "where ORD.ORDER_TYPE in ('D') " +
			           "and ORD.ORDER_STATUS = " + ORDER_CONFIRMED + " " +
			           "and ORD.BODY_ID in (" + bodyIds + ") " +
			           "and ORD.ORDER_ID = LINE.ORDER_ID " +
			           "and sysdate between ORD.START_DATE and ORD.END_DATE " +
			       ") DEMO " +
			  "where ORD.ORDER_ID = LINE.ORDER_ID " +
			    "and (LINE.PRODUCT_ID = HDR.COLLECTION_ID (+) " + 
			          "and HDR.COLLECTION_ID = LIST.COLLECTION_ID (+)) " +      
			    "and HOSTING.BODY_ID  = ORD.BODY_ID " + //(+)caused errors, even when not confirmed, books are placed
			    "and DEMO.ORDER_ID  = ORD.ORDER_ID " +  //(+)caused errors, even when not confirmed, books are placed 
			    "and ORD.BODY_ID in (" + bodyIds + ") " +
			   ") ELEM "; //+
			//"where sysdate between START_DATE_FN and END_DATE_FN ";
			
		Organisation result = null;
		EntityManager em = emf.createEntityManager();
		
		
		try {
			Query query = em.createNativeQuery( sql , Organisation.class);
			
			LOGGER.info(sql);
			
			
			List<Organisation> orgList = (List<Organisation>) query.getResultList();  
			if ( orgList != null && orgList.size() > 0 ) { 
				result = orgList.get(0);   // GET THE FIRST...
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return result != null ? result.getName() : null;
	}
	
	
	private static final String COMMA = ",";
	
	
	
	public static String checkFreeTrial(String bodyId) {
		LOGGER.info("Check free trial body id : " + bodyId);
		if(StringUtils.isEmpty(bodyId)){
			return null;
		}		
		String sql = 
			"select ORD.BODY_ID " +
			"from ebook_order_trial TR, ebook_order ORD " +
			"where TR.ORDER_ID = ORD.ORDER_ID " +
			"and ORD.ORDER_TYPE <> 'L' " +
			"and (BODY_ID in " +     
			    "(select CONSORTIUM_ID from ORACJOC.CONSORTIUM_ORGANISATION where ORGANISATION_ID in (?)) or " +
			    "BODY_ID in (?)) " +    
			"and to_date(TO_CHAR(sysdate,'mm-dd-yyyy'),'mm-dd-yyyy') between TR.START_DATE and TR.END_DATE ";
				
		sql = BookCollection.arrangeQuery(sql, bodyId, 0);
		Organisation result = null;
		EntityManager em = emf.createEntityManager();		
		
		try {
			Query query = em.createNativeQuery( sql , Organisation.class);
			modifyDynamicQuery(query, bodyId);
			
			LOGGER.info(sql);
			
			List<Organisation> orgList = (List<Organisation>) query.getResultList();  
			if ( orgList != null && orgList.size() > 0 ) { 
				result = orgList.get(0);   // GET THE FIRST...
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return result != null ? result.getBodyId() : null;
	}
	
	private static void modifyDynamicQuery(Query query, String bodyId){
		String[] bodyIdArr = bodyId.split(",");
		int cnt = 1;
		for(String _bodyId : bodyIdArr){
			query.setParameter(cnt, _bodyId);
			cnt++;
		}
	}
	
		
	@SuppressWarnings("unchecked")
	public static List<Organisation> findOrganisationByShibbId( String shibbId ) { 
		
		if ( StringUtil.isEmpty(shibbId) ) 
			throw new IllegalArgumentException(" findOrganisationByShibbId(" + shibbId + ") cannot be null ");
		
		final String sql = 
				" select org.BODY_ID,org.NAME,org.DISPLAY_NAME,org.TYPE,org.ADDRESS,org.MESSAGE_TEXT,org.IMAGE  " +
				" from shibb_cjo_organisation shibb, organisation org   " +
				" where shibb.shibb_id like ? and shibb.body_id = org.body_id ";
		
		Organisation result = null;
		EntityManager em = emf.createEntityManager();
		
		try {
			List<Organisation> orgList = (List<Organisation>) em.createNativeQuery( sql , Organisation.class).setParameter( 1, "%" +shibbId + "%" ).getResultList();
			if ( orgList != null && orgList.size() > 0 ) {				
				setOrgsAsShibboleth(orgList);
				//result = orgList.get(0);   // GET THE FIRST...
				return orgList;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			em.close();
		}
		
		return null;
	}
	
	private static void setOrgsAsShibboleth(List<Organisation> orgList){
		for(Organisation org : orgList){
			org.setShibbolethOrg(true);
		}
	}
	
		
	
	@SuppressWarnings("unchecked")
	public static void getOrgs(String bodyIds, OrgConLinkedMap orgLinkedMap){
		String sql = 
			"select BODY_ID, NAME, DISPLAY_NAME, 2 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.ORGANISATION " +
			"where BODY_ID in (" + bodyIds + ") " +
			"union " +
			"select BODY_ID, NAME, DISPLAY_NAME, 1 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.CONSORTIUM " +
			"where BODY_ID in (" + bodyIds + ") ";
		
		EntityManager em = emf.createEntityManager();
		try{
			Query query = em.createNativeQuery(sql, Organisation.class);
			List<Organisation> orgList = query.getResultList();
			for(Organisation org : orgList){
				orgLinkedMap.put(org.getBodyId(), org);
			}						
		}finally{
			em.close();
		}
	}
}
