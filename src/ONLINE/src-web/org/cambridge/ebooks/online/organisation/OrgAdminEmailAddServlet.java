/**
 * 
 */
package org.cambridge.ebooks.online.organisation;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author apirante
 *
 */
public class OrgAdminEmailAddServlet extends HttpServlet{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 3946517912520726896L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	public static final String BODY_ID = "bodyid";
	private static final String CONTENT_TYPE = "text/plain";
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String bodyId = req.getParameter(BODY_ID);
		System.out.println("body Id ariel = " + bodyId);
		resp.setContentType(CONTENT_TYPE);
		
		Writer writer = resp.getWriter();
		writer.write(bodyId);
		writer.close();
		
	}
}
