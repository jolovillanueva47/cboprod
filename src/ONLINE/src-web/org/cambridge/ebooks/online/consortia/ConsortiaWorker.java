package org.cambridge.ebooks.online.consortia;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.PersistenceUtil;


public class ConsortiaWorker {
	
	private static final Logger LOGGER = Logger.getLogger(ConsortiaWorker.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public static String[] getConsortiaIdsInSession( HttpSession session ) { 
		Object obj = session.getAttribute( "consortiaIds" );
		if ( obj instanceof String[] ) { 
			return (String[]) obj;
		} else { 
			return null;
		}
	}
	
	public static String checkIfInConsortia ( final String id ) { 
		String consortiaId = null;
		StringBuilder sql = new StringBuilder();
		//sql.append(" SELECT consortium_id FROM consortium_organisation ");
		sql.append(" SELECT body_id FROM consortium ");
		sql.append(" WHERE body_id = ? ");
		//sql.append(" WHERE organisation_id = ? ");
		
		try{
			EntityManager em = emf.createEntityManager();
			Query query = 	em.createNativeQuery(sql.toString()).setParameter(1, id);
			
			Object obj = query.getSingleResult();
			consortiaId = obj.toString().replace("[", "").replace("]", "");
			LOGGER.info(" Found: " + consortiaId   );
			
		}catch(NoResultException ex){
			LOGGER.info(" query.getSingleResult() did not return any result."   );
		}
		
		return consortiaId;
	}
	
}
