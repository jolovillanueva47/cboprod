package org.cambridge.ebooks.online.help;

import java.util.Date;
import java.util.List;

/**
 * @author Karlson A. Mulingtapang
 * HelpBooksBean.java - HelpBooks Bean
 */
public class HelpBooksBean {

	private int pageId;
	private String helpFile;
	private Date modifiedDate;
	private String modifiedBy;
	private String pageName;
	private int notebookEventId;
	private String content;
	private String url;
	private int languageCode;
	
	private String chineseId;
	private String espanolId;
	private String japaneseId;
	private String portuguesId;
	
	private List<HelpBooksBean> relatedHelpBooksBeans;
	
	/**
	 * @return the pageId
	 */
	public int getPageId() {
		return pageId;
	}
	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	/**
	 * @return the helpFile
	 */
	public String getHelpFile() {
		return helpFile;
	}
	/**
	 * @param helpFile the helpFile to set
	 */
	public void setHelpFile(String helpFile) {
		this.helpFile = helpFile;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}
	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	/**
	 * @return the notebookEventId
	 */
	public int getNotebookEventId() {
		return notebookEventId;
	}
	/**
	 * @param notebookEventId the notebookEventId to set
	 */
	public void setNotebookEventId(int notebookEventId) {
		this.notebookEventId = notebookEventId;
	}	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the relatedHelpBooksBeans
	 */
	public List<HelpBooksBean> getRelatedHelpBooksBeans() {
		return relatedHelpBooksBeans;
	}
	/**
	 * @param relatedHelpBooksBeans the relatedHelpBooksBeans to set
	 */
	public void setRelatedHelpBooksBeans(List<HelpBooksBean> relatedHelpBooksBeans) {
		this.relatedHelpBooksBeans = relatedHelpBooksBeans;
	}
	public void setLanguageCode(int languageCode) {
		this.languageCode = languageCode;
	}
	public int getLanguageCode() {
		return languageCode;
	}
	public String getChineseId() {
		return chineseId;
	}
	public void setChineseId(String chineseId) {
		this.chineseId = chineseId;
	}
	public String getEspanolId() {
		return espanolId;
	}
	public void setEspanolId(String espanolId) {
		this.espanolId = espanolId;
	}
	public String getJapaneseId() {
		return japaneseId;
	}
	public void setJapaneseId(String japaneseId) {
		this.japaneseId = japaneseId;
	}
	public String getPortuguesId() {
		return portuguesId;
	}
	public void setPortuguesId(String portuguesId) {
		this.portuguesId = portuguesId;
	}
	
}
