package org.cambridge.ebooks.online.help;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.dto.viewaccess.OrganisationDetails;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.user.UserWorker;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.MailManager;
import org.cambridge.ebooks.online.util.StringUtil;

/**
 * @author Karlson A. Mulingtapang 
 * ViewAccessDetailsBean.java - Access Details
 */
public class ViewAccessDetailsBean {
	
	private static final Logger LOGGER = Logger.getLogger(ViewAccessDetailsBean.class);
	
	private static final String NONE = "none";
	
	private static final String[] EMAIL_ADMIN_BCC 
		= new String[] {System.getProperty("email.admin").trim(), System.getProperty("email.admin.bcc").trim()};	
	private static final String TEMPLATE_DIR 
		= System.getProperty("templates.dir").trim();
	private static final String TEMPLATE_FILE 
		= System.getProperty("mail.user.info.template").trim();
	private static final String OUTPUT_DIR = System.getProperty("mail.store.path").trim();

	private String userId;
	private String bodyId;
	private String sessionId;
	private String fullName;
	private String emailAddress;

	private String userEmailAddress;
	private String emailSubject = "Cambridge Books Online";
	private String typeOfProblem;
	private String problemDescription;
	private String location;
	
	private final String usEmail = System.getProperty("view.access.email.us").trim();
	private final String ukEmail = System.getProperty("view.access.email.uk").trim();
	public static final String DO_NOT_REPLY_EMAIL = System.getProperty("do.not.reply.email").trim();
	
	private final User user = UserWorker.getUserInfoInSession(HttpUtil.getHttpSession(false));

	private Map<String, String> consortiaInSession = new LinkedHashMap<String, String>();
	private Map<String, String> societyInSession = new LinkedHashMap<String, String>();
	private Map<String, String> offerInSession = new LinkedHashMap<String, String>();
	
	private List<OrganisationDetails> organisationDetailsInSession = new ArrayList<OrganisationDetails>();

	public ViewAccessDetailsBean() {
		
	}

	public String getUserId() {
		if(this.user != null){
			this.userId = this.user.getUsername();
			if(StringUtils.isEmpty(this.userId)) {
				return NONE;
			}
			LOGGER.info("getUserId(): " + this.userId);
		} else {
			return NONE;
		}
		
		return this.userId;
	}

	public String getBodyId() {
		if(this.user != null){
			this.bodyId = this.user.getBodyId();
			if(StringUtils.isEmpty(this.bodyId)) {
				return NONE;
			}
			LOGGER.info("getBodyId(): " + this.bodyId);
		} else {
			return NONE;
		}	
		
		return this.bodyId;
	}

	public String getSessionId() {
		HttpSession session = HttpUtil.getHttpSession(false);
		if (session != null) {
			this.sessionId = session.getId();
		}

		return this.sessionId;
	}    

	public String getFullName() {
		if(this.user != null){
			this.fullName = user.getFirstName() + " " + user.getLastName();
			LOGGER.info("getFullName(): " + this.fullName);
		} else {
			return NONE;
		}
		return this.fullName;
	}

	public String getEmailAddress() {
		if(this.user != null){
			this.emailAddress = this.user.getEmail();
			if(StringUtils.isEmpty(this.emailAddress)) {
				return NONE;
			}
			LOGGER.info("getEmailAddress(): " + this.emailAddress);
		} else {
			return NONE;
		}
		return this.emailAddress;
	}

	public String sendEmail(){
		FacesContext context = FacesContext.getCurrentInstance();
		String recipient = "";
		String location = "";
		try {
						
			StringBuilder body = new StringBuilder();			
			
			if(StringUtil.isNotEmpty(this.getTypeOfProblem())){
				body.append("<b>Predefined Problem:</b> " + this.getTypeOfProblem() + "<br>");
			}
			
			body.append("<b>Describe Your Problem:</b> " + this.getProblemDescription() + "<br>");
			
			body.append("<b>Reported by:</b> " + this.getUserEmailAddress() + "<br>");
			
			if(StringUtil.isNotEmpty(this.getLocation())){
				recipient = this.getLocation().trim().split(":")[0];
				location = this.getLocation().trim().split(":")[1];
				
				body.append("<b>Location:</b> " + location);
			}
			body.append(this.getAccessDetails());
			MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  DO_NOT_REPLY_EMAIL, 
								  new String[]{recipient}, 
								  null, 
								  EMAIL_ADMIN_BCC, 
								  this.getEmailSubject(), 
								  body.toString());
			
			FacesMessage message = new FacesMessage("Your e-mail has been sent.");
			context.addMessage("emailMessage", message);			
			
		} catch (Exception e) {
			LOGGER.info("[Exception] " + e.getMessage());
			FacesMessage message = new FacesMessage("Your e-mail has not been sent. Please try again.");
			context.addMessage("emailMessage", message);
		} finally {
			this.reset();
		}
		
		return "view_access_details";
	}
	
	
	public List<OrganisationDetails> getOrganisationInSession() {
		if (this.organisationDetailsInSession == null
				|| this.organisationDetailsInSession.isEmpty()) {

			List<OrganisationDetails> orgDetailsList = new ArrayList<OrganisationDetails>();
			
			OrgConLinkedMap map = OrgConLinkedMap.getFromSession(HttpUtil.getHttpServletRequest());
			List<Organisation> orgList = map.getAllOrganisation();
			
			for(Organisation org : orgList){
				OrganisationDetails orgDetails = new OrganisationDetails();
				orgDetails.setBodyId(org.getBodyId());
				orgDetails.setDisplayName(org.getBaseProperty(Organisation.DISPLAY_NAME));
				orgDetails.setMessageText(org.getBaseProperty(Organisation.MESSAGE_TEXT));
				
				orgDetailsList.add(orgDetails);				
			}		

			this.organisationDetailsInSession = orgDetailsList;
		}
		return organisationDetailsInSession;
	}
	
	public Map<String, String> getConsortiaInSession() {
		if (this.consortiaInSession == null
				|| this.consortiaInSession.isEmpty()) {

			Map<String, String> result = new LinkedHashMap<String, String>();
			
			OrgConLinkedMap map = OrgConLinkedMap.getFromSession(HttpUtil.getHttpServletRequest());
			List<Organisation> orgList = map.getAllConsortium();
			for(Organisation org : orgList){
				result.put(org.getBodyId(), org.getBaseProperty(Organisation.DISPLAY_NAME));
			}			
			
			if(result.isEmpty()){
				result.put(NONE, NONE);
			}
						

			this.consortiaInSession = result;
		}
		return consortiaInSession;
	}	
	
	public Map<String, String> getSocietyInSession() {
		if (this.societyInSession == null
				|| this.societyInSession.isEmpty()) {

			Map<String, String> result = new LinkedHashMap<String, String>();
			
			OrgConLinkedMap map = OrgConLinkedMap.getFromSession(HttpUtil.getHttpServletRequest());
			List<Organisation> orgList = map.getAllSociety();
			for(Organisation org : orgList){
				result.put(org.getBodyId(), org.getBaseProperty(Organisation.DISPLAY_NAME));
			}
			
			if(result.isEmpty()){
				result.put(NONE, NONE);
			}

			this.societyInSession = result;
		}
		return societyInSession;
	}
	
	public Map<String, String> getOfferInSession() {
		if (this.offerInSession == null
				|| this.offerInSession.isEmpty()) {

			Map<String, String> result = new LinkedHashMap<String, String>();
			
			result.put(NONE, NONE);			

			this.offerInSession = result;
		}
		return offerInSession;
	}

	public void reset() {
		this.userEmailAddress = "";
		this.emailSubject = "";
		this.typeOfProblem = "";
		this.problemDescription = "";
		this.location = "";
	}

	public void validateEmail(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		if(value != null) {
			if(!(value instanceof String)) {
				throw new ValidatorException(new FacesMessage("The value must be a String."));
			}
			
			String email = (String) value;
			
			if (email.indexOf("@") < 0) {
				throw new ValidatorException(new FacesMessage("Invalid Email Address."));  
			}
		}
	}
	
	public static final String BR = "<br/>";
	
	
	
	public String getAccessDetails() throws UnknownHostException{	
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		StringBuffer sb = new StringBuffer();
		sb.append("<h1>View Access Details</h1>");
		sb.append("<p>Below is a summary of your user details. You may use this information in order to work with your local administrator to troubleshoot any access issues you have, or you can contact Cambridge University Press Customer Services by filling in the form below and submitting it.</p>");
		sb.append("<h2>User Details</h2>");		
		sb.append("<p>Browser & OS: " + getBrowser() + "</p>" );
		sb.append("<p>IP Address: " + getIPAdress() + "</p>");
		sb.append("<p>Domain Name: " + request.getRemoteHost() + "</p>");
		sb.append("<p>Node: " + InetAddress.getLocalHost().getHostName() + "</p>");
		sb.append("<p>Body Id: " + this.getBodyId() + "</p>");
		sb.append("<p>Session Id: " + this.getSessionId() + "</p>");
		sb.append("<p>User&#8217;s Full Name: " + this.getFullName() + "</p>");
		sb.append("Email Address: " + this.getEmailAddress() + "</p>");
		sb.append("<h2>Organisation Details</h2>");		
		sb.append(getOrgList());	
		sb.append("<h2>Consortia Details</h2>");
		sb.append(getConsortiaList());
		sb.append("<h2>Society Details</h2>");
		sb.append(getSocietyList());
		sb.append("<h2>Offer Details</h2>");
		sb.append(getOfferList());		
		return sb.toString();
	}
	
	private String getIPAdress(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String ipAddress = request.getHeader("X-Cluster-Client-Ip");
		if(StringUtils.isNotEmpty(ipAddress)){
			return ipAddress;
		}else{
			return request.getRemoteAddr();
		}
	}
	
	private String getBrowser(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String browser = request.getHeader("user-agent");
		return browser;
	}
	
	private String getOrgList(){		
		List<OrganisationDetails> organisationDetailsInSession = this.getOrganisationInSession();
		return getPropertyFromOrganisationDetailsList(organisationDetailsInSession);
	}
	
	private String getConsortiaList(){
		Map<String, String> consortia = this.getConsortiaInSession();
		return getPropertyFromMap(consortia);
	}
	
	private String getSocietyList(){
		Map<String, String> soc = this.getSocietyInSession();
		return getPropertyFromMap(soc);
	}
	
	private String getOfferList(){
		Map<String, String> offer = this.getSocietyInSession();
		return getPropertyFromMap(offer);
	}
	
	
	
	private String getPropertyFromMap(Map<String, String> map){
		StringBuffer sb = new StringBuffer();
		Set<String> set = map.keySet();		
		for(String key : set){
			sb.append("<p>Body ID: " +  key + "</p>");
			String value = map.get(key);
			sb.append("<p>Display Name: " +  value + "</p>");			
		}
		return sb.toString();
		
	}
	
	private String getPropertyFromOrganisationDetailsList(List<OrganisationDetails> orgDetailsList) {				
		StringBuffer sb = new StringBuffer();
		for(OrganisationDetails orgDetails : orgDetailsList){
			sb.append("<p>Body ID: " +  orgDetails.getBodyId() + "</p>");
			sb.append("<p>Display Name: " +  orgDetails.getDisplayName() + "</p>");
			if(StringUtils.isNotEmpty(orgDetails.getMessageText())) {
				sb.append("<p>Message: " +  orgDetails.getMessageText() + "</p>");
			}
		}
		return sb.toString();		
	}
		

	/**
	 * @return the userEmailAddress
	 */
	public String getUserEmailAddress() {
		return userEmailAddress;
	}

	/**
	 * @param userEmailAddress the userEmailAddress to set
	 */
	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * @return the typeOfProblem
	 */
	public String getTypeOfProblem() {
		return typeOfProblem;
	}

	/**
	 * @param typeOfProblem the typeOfProblem to set
	 */
	public void setTypeOfProblem(String typeOfProblem) {
		this.typeOfProblem = typeOfProblem;
	}

	/**
	 * @return the problemDescription
	 */
	public String getProblemDescription() {
		return problemDescription;
	}

	/**
	 * @param problemDescription the problemDescription to set
	 */
	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the usEmail
	 */
	public String getUsEmail() {
		return usEmail;
	}

	/**
	 * @return the ukEmail
	 */
	public String getUkEmail() {
		return ukEmail;
	}
	
}
