package org.cambridge.ebooks.online.help;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.help.HelpBooks;
import org.cambridge.ebooks.online.jpa.help.HelpMapBooks;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * @author Karlson A. Mulingtapang
 * HelpDAO.java - HELP Data Access Object
 */
public class HelpDAO {
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;			
	
	
	@SuppressWarnings("unchecked")
	public static List<HelpBooks> searchAllHelpBooks() {
		List<HelpBooks> result;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(HelpBooks.SEARCH_ALL);
		
		try {
			result = (List<HelpBooks>)query.getResultList();
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}
		
		return result;
	}
	
	public static HelpBooks searchHelpBooksById(int id) {
		HelpBooks result = null;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(HelpBooks.SEARCH_BY_ID);
		
		try {
			query.setParameter(1, id);
			if(query.getSingleResult() instanceof HelpBooks) {
				result = (HelpBooks)query.getSingleResult();
			}
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static List<HelpMapBooks> searchHelpMapBooksByPageId(int id) {
		List<HelpMapBooks> result;
		
		EntityManager em = emf.createEntityManager();
		Query query = em.createNamedQuery(HelpMapBooks.SEARCH_BY_PAGE_ID);
		
		try {
			query.setParameter(1, id);
			result = (List<HelpMapBooks>)query.getResultList();
		} catch (Exception e) {
			System.out.println("[Exception]: " + e.getLocalizedMessage());
			e.printStackTrace();
			result = null;
		} finally {
			em.close();
		}
		
		return result;
	}
}
