package org.cambridge.ebooks.online.help;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author Karlson A. Mulingtapang
 * HelpBean.java - Help Bean
 */
public class HelpBean {	
	
	private List<HelpBooksBean> helpBooksBeans;
	private HelpBooksBean helpBooksBean;
	
	private String initializeHelpIndex;
	private String initializeHelp;

	/**
	 * @return the initializeHelpIndex
	 */
	public String getInitializeHelpIndex() {
		HelpWorker worker = new HelpWorker();
		
		this.helpBooksBeans = worker.searchAllHelpBooks();
		
		// dummy data
//		List<HelpBooksBean> helpBooksBeans = new ArrayList<HelpBooksBean>();
//		
//		HelpBooksBean helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(1537);
//		helpBooksBean.setPageName("Home");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(1536);
//		helpBooksBean.setPageName("Book Landing");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(1532);
//		helpBooksBean.setPageName("Chapter Landing");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(1527);
//		helpBooksBean.setPageName("Advance Search");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(86);
//		helpBooksBean.setPageName("Search Results");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(1);
//		helpBooksBean.setPageName("Subscription Sales");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(2);
//		helpBooksBean.setPageName("Developing Countries");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		helpBooksBean = new HelpBooksBean();
//		helpBooksBean.setPageId(3);
//		helpBooksBean.setPageName("Advertising and Corporate Sales");
//		helpBooksBean.setContent("No help text.");		
//		helpBooksBeans.add(helpBooksBean);
//		
//		this.helpBooksBeans = helpBooksBeans;
		
		return "";
	}	

	/**
	 * @return the initializeHelp
	 */
	public String getInitializeHelp() {				
		String pageId = HttpUtil.getHttpServletRequestParameter("pageId");
		String pageName = HttpUtil.getHttpServletRequestParameter("pageName");
		String fromTop = HttpUtil.getHttpServletRequestParameter("fromTop");
		HelpWorker worker = new HelpWorker();
		String cookieValue = getCookieValue();
		if("Y".equals(fromTop)){
			HelpBooksBean bean = worker.searchHelpBooksById(Integer.parseInt(pageId));
			pageId = getEquivalentPageId(bean, cookieValue);
		}
		
		initHelp(pageId, pageName);
	
		return "";
	}
	
	private String getEquivalentPageId(HelpBooksBean bean, String cookieValue){
		if("1".equals(cookieValue)){
			cookieValue = String.valueOf(bean.getPageId());
		}else if("2".equals(cookieValue)){
			cookieValue = bean.getChineseId();
		}else if("3".equals(cookieValue)){
			cookieValue = bean.getEspanolId();
		}else if("4".equals(cookieValue)){
			cookieValue = bean.getJapaneseId();
		}else if("5".equals(cookieValue)){
			cookieValue = bean.getPortuguesId();
		}
		
		return cookieValue;
	}
	
	public String getCookieValue(){

		Cookie[] cookies = HttpUtil.getHttpServletRequest().getCookies();
		String cookieValue = null;
		if(cookies != null){
			for(int i = 0; i < cookies.length; i++){
				if("LanguageCookie".equals(cookies[i].getName())){
	            	if(cookies[i].getValue() == null){
	            		cookies[i].setValue("1");
	            		cookies[i].setMaxAge(60*60*24*365);   
						cookies[i].setPath("/");   
						((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookies[i]);

	            	}
	            	else{
	            		cookieValue = cookies[i].getValue();

	            	}
				}
			}
		}
		return cookieValue;
	}
	
	
	public void initHelp(String pageId, String pageName){
		HelpWorker worker = new HelpWorker();
		if(pageId == null || pageId.equals("null")) {
			pageId = "-1";
		}
		if(pageName == null || pageName.equals("null")) {
			pageName = null;
		}
		
		try {
			this.helpBooksBean = worker.searchHelpBooksById(Integer.parseInt(pageId));
		} catch (Exception e) {		
			e.printStackTrace();
		}
		
		if(this.helpBooksBean == null) {
			HelpBooksBean helpBooksBean = new HelpBooksBean();			
			helpBooksBean.setPageId(Integer.parseInt(pageId));			
			helpBooksBean.setPageName(pageName);
			helpBooksBean.setContent("No help text.");
			helpBooksBean.setHelpFile("");
			
			this.helpBooksBean = helpBooksBean;
		} else {
			// Set helpFile path
			String helpFile = this.helpBooksBean.getHelpFile();
			this.helpBooksBean.setHelpFile(System.getProperty("cup.help.pages") + helpFile);
		}
		
	}

	/**
	 * @return the helpBooksBeans
	 */
	public List<HelpBooksBean> getHelpBooksBeans() {
		return helpBooksBeans;
	}

	/**
	 * @param helpBooksBeans the helpBooksBeans to set
	 */
	public void setHelpBooksBeans(List<HelpBooksBean> helpBooksBeans) {
		this.helpBooksBeans = helpBooksBeans;
	}

	/**
	 * @return the helpBooksBean
	 */
	public HelpBooksBean getHelpBooksBean() {
		return helpBooksBean;
	}

	/**
	 * @param helpBooksBean the helpBooksBean to set
	 */
	public void setHelpBooksBean(HelpBooksBean helpBooksBean) {
		this.helpBooksBean = helpBooksBean;
	}			
}
