package org.cambridge.ebooks.online.help;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.cambridge.ebooks.online.jpa.help.HelpBooks;
import org.cambridge.ebooks.online.jpa.help.HelpMapBooks;

/**
 * @author Karlson A. Mulingtapang
 * HelpWorker.java - Sets data from jpa to bean
 */
public class HelpWorker {
	
	public List<HelpBooksBean> searchAllHelpBooks() {			
		try {
			return populateHelpBooksBean(HelpDAO.searchAllHelpBooks());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public HelpBooksBean searchHelpBooksById(int id) {
		try {
			return populateHelpBooksBean(HelpDAO.searchHelpBooksById(id));
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private List<HelpBooksBean> populateHelpBooksBean(List<HelpBooks> records)
		throws InvocationTargetException, IllegalAccessException {
		
		if(records == null) {
			return null;
		}
		
		List<HelpBooksBean> beans = new ArrayList<HelpBooksBean>();
		
		for(HelpBooks record : records) {			
			HelpBooksBean bean = new HelpBooksBean();
			BeanUtils.copyProperties(bean, record);	
			beans.add(bean);
		}		
		
		return beans;
	}
	
	private List<HelpBooksBean> getRelatedHelpBooksFromMap(int id) 
		throws InvocationTargetException, IllegalAccessException {
		
		List<HelpBooksBean> beans = new ArrayList<HelpBooksBean>();
		for(HelpMapBooks record : HelpDAO.searchHelpMapBooksByPageId(id)) {
			HelpBooksBean bean = new HelpBooksBean();
			BeanUtils.copyProperties(bean, 
					HelpDAO.searchHelpBooksById(record.getRelatedPageId()));
			beans.add(bean);
		}
		
		return beans;
	}
	
	private HelpBooksBean populateHelpBooksBean(HelpBooks record)
		throws InvocationTargetException, IllegalAccessException {		
		
		if(record == null) {
			return null;
		}
		
		HelpBooksBean bean = new HelpBooksBean();
		BeanUtils.copyProperties(bean, record);
		
		bean.setRelatedHelpBooksBeans(getRelatedHelpBooksFromMap(record.getPageId()));
		
		return bean;
	}
}
