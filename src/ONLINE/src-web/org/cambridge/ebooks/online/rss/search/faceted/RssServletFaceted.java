package org.cambridge.ebooks.online.rss.search.faceted;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.rss.RssFaceted;
import org.cambridge.ebooks.online.landingpage.BookContentItem;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.search.solr.SearchResult;
import org.cambridge.ebooks.online.search.solr.SearchResultBean;
import org.cambridge.ebooks.online.search.solr.SearchServlet;
import org.cambridge.ebooks.online.subscription.FreeTrialBean;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.online.util.StringUtil;
import org.cambridge.ebooks.online.util.UrlUtil;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;




/**
 * 
 * @author mmanalo
 *
 */

public class RssServletFaceted extends SearchServlet {

	
	private static final Logger LOGGER = Logger.getLogger(RssServletFaceted.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7942022602334242322L;

	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	private static final String RSS_2 = "rss_2.0";
	
	private static final String CONTENT_TYPE_XML = "text/xml";
	
	protected static final String CONTENT_TYPE_HTML = "text/HTML";
			
	private static final String CHANNEL_TITLE = "channel.title";
			
	protected static final String BR = "<br />";
	
	private static final String EBOOK_JSF = "ebook.jsf?bid=";
	
	protected static final String PUBLISHED_ON = "Published online: ";
	
	protected static final String ABSTRACT = "Extract:";
	
	@SuppressWarnings("unused")
	private static final String EMPTY_ABSTRACT = "Abstract not available.";
	
	protected static final String BLURB = "Blurb:";
	
	private static final String SEARCHER_BEAN = "searchResultBean";
	
	public static final String SEARCH_TEXT = "searchText";
	
	public static final String RSS_FACETED_URL = "rssFacet";
		
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {		
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//modify the request		
		super.doPost(new RssHttpRequest(req), resp);
		
		
		//searcher bean will be created in session
		SearchResultBean results = (SearchResultBean) req.getAttribute(SEARCHER_BEAN);
		
		resp.setContentType(CONTENT_TYPE_XML);
		resp.setCharacterEncoding("UTF-8");
		//resp.setBufferSize(0);
		
		PrintWriter writer = resp.getWriter();
		
		
		SyndFeedOutput output = new SyndFeedOutput();
		try {			
			String rss = output.outputString(generateRss(results, req));			
			
			//cleanRss();
			rss = cleanRss(rss);
			//rss = "1234567" + rss;
						
			//resp.setContentLength(200000);
			//System.out.println(rss);
			
			//resp.flushBuffer();
			writer.write(rss);
			
			//writer.flush();
			
			//writer.close();
		} catch (FeedException e) {			
			e.printStackTrace();
		} finally {
			writer.close();
		}
		
	}
	
	/**
	 * rome.jar's feed titles automatically escapes html and unicode.  therefore, the fix here is to convert first the object to string, then manually unescape all &amp; to &
	 * @param rss
	 * @return
	 */
	private String cleanRss(String rss){		
		//rss = rss.replaceAll("&amp;#(\\d+);", "&#$1;");
		rss = rss.replaceAll("&amp;(?=#(\\d+);)", "&");
		//rss = rss.replaceAll("&(?!(amp;|#(\\d+);))", "&amp;");
		return rss;
	}
	
	
	
	
	public static final String FEED_TITLE_PARAM = "FEED_TITLE";
	
	private String generateFeedTitle(HttpServletRequest request){		
		String feedTitle = request.getParameter(FEED_TITLE_PARAM);		
		if(StringUtil.isEmpty(feedTitle)){			
			feedTitle = System.getProperty(CHANNEL_TITLE);
		}
		//feed title should not have space
		feedTitle = feedTitle.replaceAll("\\s", "_");
		return feedTitle;
	}
	
	
	protected SyndFeed generateRss(SearchResultBean results, 
			HttpServletRequest request){
		SyndFeed feed = new SyndFeedImpl();		
		feed.setFeedType(RSS_2);
		feed.setTitle(generateFeedTitle(request));
		try {
			feed.setLink(UrlUtil.getClientDns(request));
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}				
		feed.setDescription(System.getProperty(CHANNEL_TITLE));		
		feed.setEntries(generateEntries(results, request));		
		
		return feed;
	}
	
	
	private List<SyndEntry> generateEntries(SearchResultBean results,
			HttpServletRequest request){
		List<SyndEntry> alEntries = new ArrayList<SyndEntry>();
		List<SearchResult> srList =  results.getSearchResultList();		
		for(SearchResult sr : srList) {	
			if(StringUtils.isEmpty(sr.getJournalId())){
				SyndEntry entry = new SyndEntryImpl();				
				entry.setTitle(getEntryTitle(sr));				
				try {
					entry.setLink(getBookLink(request, sr));
				} catch (Exception e) {
					LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
				}			
				entry.setDescription(createEntryContent(sr, request));				
				alEntries.add(entry);
			}else{
				SyndEntry entry = new SyndEntryImpl();
				entry.setTitle(getEntryTitleJ(sr));
				entry.setLink(getJournalLink(request, sr));			
				entry.setDescription(createEntryContentJ(sr, request));				
				alEntries.add(entry);
			}
		}
		return alEntries;
	}
	
	
	
	protected String getBookLink(HttpServletRequest request, SearchResult sr) throws Exception {		 
		return UrlUtil.getClientDns(request) + EBOOK_JSF + removeHighlight(sr.getBookId());
	}
	
	
	/*
	public static void main(String[] args) {
		System.out.println("Psychosocial and demographic predictors of fruit, juice and vegetable consumption among 11&#8211;14-year-old Boy Scouts".replaceAll("&amp;#(\\d+);", "&#$1;"));
		
		Pattern p = Pattern.compile("(?=.*theft).*incident.*");
		Matcher m = p.matcher("The a incident involved a theft a");
		System.out.println(m.matches());
		
		Pattern p1 = Pattern.compile(".*");
		Matcher m1 = p1.matcher("aa");
		System.out.println(m1.matches());
		
		Pattern p2 = Pattern.compile("&(?=(amp;|#))");
		Matcher m2 = p2.matcher("&#123;");
		System.out.println(m2.find(0));
		
	}
	*/
	protected String getJournalLink(HttpServletRequest request, SearchResult sr){
		//http://journals.cambridge.org/action/displayAbstract?fromPage=online&aid=93945&fulltextType=RA&fileId=S0958067001023405
		String url =
			UrlUtil.getContextRootCjo() + 
			"action/displayAbstract?fromPage=online&aid=" + removeHighlight(sr.getId()) +
			"&fulltextType=" + removeHighlight(sr.getArticleType()) +
			"&fileId=" + removeHighlight(sr.getArticleFileId());
		return url;		
	}
	
	protected String getBookBlurb(SearchResult sr, HttpServletRequest request){		
		return sr.getBookMetaData().getBlurb();
	}
	
	private String getJournalTitleLink(SearchResult sr){		
		String urlJournal = UrlUtil.getContextRootCjo() + "action/displayJournal?jid=" + sr.getJournalId();		
		return urlJournal;		
	}
	
	private String getVolumeOrIssue(SearchResult sr){
		String volume = sr.getArticleVolumeNumber();
		String issue = sr.getArticleIssueNumber();
		String volis = "";
		if("-1".equals(volume)  && "-1".equals(issue)){
			volis = "<i>First View Articles</i> ";
		}else {
			volis = "Volume " + volume;
		}
		
		if(!"-1".equals(issue)){			
			String issueNumber = sr.getArticleIssueNumber();
			String issueWording = "";
			if(StringUtils.isNotEmpty(issueNumber)){
				issueNumber = issueNumber.toLowerCase();
				if((issueNumber.startsWith("s") || issueNumber.startsWith("i")) && "AUI".equals(sr.getJournalId())){
					issueWording = "Supplement";
				}else{
					issueWording = "Issue";
				}
			}
			volis = "Volume " + volume + ", " + issueWording + " " + issueNumber ;
		}
		return volis;
	}
	
	private String getJournalsPageStart(SearchResult sr){
		if(StringUtils.isNotEmpty(sr.getArticlePageStart()) && StringUtils.isNotEmpty(sr.getArticlePageEnd())){
			return "pp " + sr.getArticlePageStart() + " - " + sr.getArticlePageEnd();
		}else{
			return "";
		}
	}
	
	protected SyndContent createEntryContentJ(SearchResult sr, HttpServletRequest request){
		SyndContent content = new SyndContentImpl();
		
		//BookContentItem ci = sr.getBookContentItem();
		content.setType(CONTENT_TYPE_HTML);
		
		StringBuffer sb = new StringBuffer();
		
		/*
		sb.append("BOOK");		
		sb.append(BR);
		*/
		sb.append(" " + removeHighlight(sr.getAuthor()));
		
		sb.append(BR);		
		sb.append("<a href='"+ removeHighlight(getJournalTitleLink(sr)) + "' >" + sr.getJournalTitle() + "</a>");
		
		sb.append(", ");
		sb.append(" " + removeHighlight(getVolumeOrIssue(sr)));
		sb.append(", ");
		sb.append(" " + removeHighlight(getJournalsPageStart(sr)));
		sb.append(BR);
		sb.append("<i><b>Journal </b></i>");
		
		sb.append(BR + BR + BR);		
		content.setValue(sb.toString());		
		
		
		return content;
	}
	
	protected SyndContent createEntryContent(SearchResult sr, HttpServletRequest request){
		SyndContent content = new SyndContentImpl();
		
		BookContentItem ci = sr.getBookContentItem();
		content.setType(CONTENT_TYPE_HTML);
		
		StringBuffer sb = new StringBuffer();
		
		/*
		sb.append("BOOK");		
		sb.append(BR);
		*/
		sb.append(" " + removeHighlight(sr.getAuthorSingleline()));
		sb.append(BR);		
		sb.append("Online ISBN: " + removeHighlight(sr.getBookOnlineIsbn()));
		sb.append(BR);
		sb.append("<i><b>Book</b></i>");
		sb.append(BR);
		sb.append(PUBLISHED_ON + removeHighlight(sr.getBookOnlineDate()));
		sb.append(BR);
		String contentPath = "";
		try {
			contentPath = UrlUtil.getClientDns(request);
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}		
		String chapterUrl = contentPath + "chapter.jsf?bid=" + removeHighlight(sr.getBookId()) + "&cid=" + removeHighlight(sr.getId()); 
		sb.append("<a target=\"rss\" href='" + removeHighlight(chapterUrl) + "'>" + removeHighlight(ci.getTitle()) + "</a>");
		sb.append(BR);
		sb.append(sr.getChapterFulltext());
		sb.append(BR + BR + BR);		
		content.setValue(sb.toString());		
		
		
		return content;
	}
	
	
	private String removeHighlight(String value){
		value = value.replaceAll("<span class='searchWord'>", "");
		value = value.replace("</span>", "");
		return value;
	}
	
	private String stripHtmlAndUnicode(String value){
		value = StringUtil.stripHTMLTags(value);
		//value = StringEscapeUtils.unescapeXml(value);
		//value= "<![CDATA[" + value + "]]>";
		return value;
	}
	
	private String getEntryTitle(SearchResult sr){
		StringBuffer sb = new StringBuffer();
		
		sb.append(removeHighlight(sr.getBookTitle()));
		if(StringUtils.isNotEmpty(sr.getBookSubtitle())){
			sb.append(" (" + removeHighlight(sr.getBookSubtitle()) + ") ");		
		}
		
		return stripHtmlAndUnicode(sb.toString());		
		//return sb.toString();
	}
	
	private String getEntryTitleJ(SearchResult sr){
		StringBuffer sb = new StringBuffer();		
		sb.append(removeHighlight(sr.getArticleTitle()));
		return stripHtmlAndUnicode(sb.toString());		
		//return sb.toString();
	}
	
	
	
	public static String[] getRssUrlTitle(HttpServletRequest request) throws Exception{
		//System.getProperty(RssServlet.EBOOKS_CONTEXT_PATH) + "rss?" + request.getQueryString() + "&" + RssServlet.NO_REDIRECT_URL + "&"
		
		EntityManager em = emf.createEntityManager();		
		em.getTransaction().begin();
		RssFaceted rss = null;
		try{			
			rss = new RssFaceted(request);
			em.persist(rss);
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Something is wrong with JPA");		
		}finally{
			em.getTransaction().commit();
			em.close();			
		}
		String title = generateFeedTitle(rss).replaceAll("\\s", "_");
		
		String contentPath = "";
		try {
			contentPath = UrlUtil.getClientDns(request);
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}		
		
		String url = contentPath + 
				"rssFacet?" + RssFaceted.QID + "=" + rss.getQid() + "&" + RssFaceted.SEARCH_TYPE + "=" + RssFaceted.RSS + "&" +
				FEED_TITLE_PARAM + "=" + title;
		return new String[]{url, title};
	}
	
	public static String generateFeedTitle(RssFaceted rss){		
		String mainTitle = System.getProperty(CHANNEL_TITLE);		
		return mainTitle + " Search";
	}
	
	
	/**
	 * returns true if should displayRss
	 * @param request
	 * @return
	 */
	public static final boolean displayRss(HttpServletRequest request){
		HttpSession session = request.getSession();		
		if(isFreeTrial(session)){
			return true;
		}
		return isAuthenticated(session);		
	}
	
	/**
	 * check if this is a free trial
	 * @param session
	 * @return
	 */
	private static boolean isFreeTrial(HttpSession session){
		return FreeTrialBean.hasFreeTrial(session);
	}
	
	/**
	 * check body id, has body if ip recognized or logged
	 * @param session
	 * @return
	 */
	private static boolean isAuthenticated(HttpSession session){
		OrgConLinkedMap map = OrgConLinkedMap.getFromSession(session);
		if(map != null && map.getMap().size() > 0){
			return true;
		}
		return false;
	}
	
	
	
	
}
