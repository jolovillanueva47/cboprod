package org.cambridge.ebooks.online.rss.search.faceted;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.cambridge.ebooks.online.jpa.rss.RssFaceted;
import org.cambridge.ebooks.online.search.solr.SearchServlet;

public class RssHttpRequest extends HttpServletRequestWrapper {
	
	private Map<String,Object> rssRequestMap = new HashMap<String, Object>();

	public RssHttpRequest(HttpServletRequest request) {
		super(request);
		
		//init rss request
		initRssHttp(request);		
	}
	
	@Override
	public String getParameter(String name) {
		String value = (String) rssRequestMap.get(name);
		return value != null ? value : super.getParameter(name);
	}
	
	@Override
	public String[] getParameterValues(String name) {
		String[] values = (String[])rssRequestMap.get(name);
		return values != null ? values : super.getParameterValues(name);
	}
	
	private void initRssHttp(HttpServletRequest request){
		RssFaceted rss = RssFaceted.getRssFromDb(request);
		if(rss != null){
			if(RssFaceted.QUICK.equals(rss.getSearchType())){
				rssRequestMap.put(RssFaceted.SEARCH_TYPE,SearchServlet.SEARCH_QUICK);
			}else{
				rssRequestMap.put(RssFaceted.SEARCH_TYPE,SearchServlet.SEARCH_ADVANCE);
			}
		}else{
			throw new IllegalArgumentException("QID is not found");
		}
		
		
		//reloadRequestAttributes
		reloadRequestAttributes(request, rss);
	}
	
	private void reloadRequestAttributes(HttpServletRequest request, RssFaceted rss){
		if(RssFaceted.QUICK.equals(rss.getSearchType())){
			rssRequestMap.put(RssServletFaceted.SEARCH_TEXT, rss.getSearchTextQk());
		}else{
			rssRequestMap.put(RssFaceted.TARGET, RssFaceted.unprocessWithDelimiter(rss.getTarget()));
			rssRequestMap.put(RssFaceted.CONDITION, RssFaceted.unprocessWithDelimiter(rss.getCondition()));
			rssRequestMap.put(RssFaceted.LOGICAL_OP, RssFaceted.unprocessWithDelimiter(rss.getLogicalOp()));
			rssRequestMap.put(RssFaceted.SEARCH_WORD, RssFaceted.unprocessWithDelimiter(rss.getSearchText()));
			rssRequestMap.put(RssFaceted.SEARCH_WORD_2, RssFaceted.unprocessWithDelimiter(rss.getSearchText2()));		
		}
	}

}
