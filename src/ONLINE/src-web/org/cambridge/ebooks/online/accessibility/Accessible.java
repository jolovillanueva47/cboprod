package org.cambridge.ebooks.online.accessibility;

public interface Accessible {
	
	String ACC_URL_IDENTIFIER = "/aaa";
	
	public String accUrlFormatter(String path);
}
