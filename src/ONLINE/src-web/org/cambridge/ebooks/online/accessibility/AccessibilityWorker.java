package org.cambridge.ebooks.online.accessibility;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.cambridge.ebooks.online.jpa.textfields.TextfieldsLoad;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.landingpage.BookTocItem;
import org.cambridge.ebooks.online.search.solr.CollapsedChapter;
import org.cambridge.ebooks.online.util.AccessibilityManager;
import org.cambridge.ebooks.online.util.HttpUtil;

public class AccessibilityWorker {
	
	public static String replaceString_aaa(String origText){		
//		String results = "";
//		
//		if(null == origText) {
//			return origText;
//		}
//		
//		results = origText.replaceAll("<i>", "<em>");
//		results = results.replaceAll("</i>", "</em>");
//		
//		results = results.replaceAll("<b>", "<strong>");
//		results = results.replaceAll("</b>", "</strong>");
//	
//		return results;
		String result = origText;
		if(null != origText)
			result = origText.replaceAll("<i>", "<em>").replaceAll("</i>", "</em>").replaceAll("<b>", "<strong>").replaceAll("</b>", "</strong>");
		return result;		
	}
	
	public static void customReplace_aaa(BookTocItem meta){
//		if(null==meta){
//			return;
//		}
//		meta.setLabel(replaceString_aaa(meta.getLabel()));
//		meta.setTitle(replaceString_aaa(meta.getTitle()));
//		List<String> contributorList = new ArrayList<String>();
//		if(null!=meta.getContributorNameList()){
//			for (String string : meta.getContributorNameList()) {
//				contributorList.add(replaceString_aaa(string));
//			}
//		}
//		meta.setContributorNameList(contributorList);
		if(null!=meta){
			meta.setLabel(replaceString_aaa(meta.getLabel()));
			meta.setTitle(replaceString_aaa(meta.getTitle()));
			List<String> contributorList = new ArrayList<String>();
			if(null!=meta.getContributorNameList())
				for (String string : meta.getContributorNameList())
					contributorList.add(replaceString_aaa(string));
			meta.setContributorNameList(contributorList);
		}
	}
	
	public static void customReplace_aaa(BookMetaData meta){
//		if(null==meta){
//			return;
//		}
//		meta.setTitle(replaceString_aaa(meta.getTitle()));
//		
//		meta.setVolumeTitle(replaceString_aaa(meta.getVolumeTitle()));
//		meta.setEdition(replaceString_aaa(meta.getEdition()));
//		meta.setSubtitle(replaceString_aaa(meta.getSubtitle()));
//		
//		meta.setSeries(replaceString_aaa(meta.getSeries()));
//		meta.setBlurb(replaceString_aaa(meta.getBlurb()));
		if(null!=meta){
			meta.setTitle(replaceString_aaa(meta.getTitle()));
			
			meta.setVolumeTitle(replaceString_aaa(meta.getVolumeTitle()));
			meta.setEdition(replaceString_aaa(meta.getEdition()));
			meta.setSubtitle(replaceString_aaa(meta.getSubtitle()));
			
			meta.setSeries(replaceString_aaa(meta.getSeries()));
			meta.setBlurb(replaceString_aaa(meta.getBlurb()));
		}
	}	
	
	public static void customReplace_aaa(TextfieldsLoad meta){
//		if(null==meta){
//			return;
//		}
//		meta.setReview1(replaceString_aaa(meta.getReview1()));
//		meta.setReview2(replaceString_aaa(meta.getReview2()));
//		meta.setReview3(replaceString_aaa(meta.getReview3()));
		if(null!=meta){
			meta.setReview1(replaceString_aaa(meta.getReview1()));
			meta.setReview2(replaceString_aaa(meta.getReview2()));
			meta.setReview3(replaceString_aaa(meta.getReview3()));
		}
	}	
	public static void customReplace_aaa(String meta){
//		if(null==meta){
//			return;
//		}
//		meta = replaceString_aaa(meta);
		if(null!=meta) meta = replaceString_aaa(meta);
	}
	public static void customReplace_aaa(CollapsedChapter meta){
//		if(null==meta){
//			return;
//		}
//		meta.setLabel(replaceString_aaa(meta.getLabel()));
//		meta.setTitle(replaceString_aaa(meta.getTitle()));
		if(null!=meta){
			meta.setLabel(replaceString_aaa(meta.getLabel()));
			meta.setTitle(replaceString_aaa(meta.getTitle()));
		}
	}
	public static boolean isPathAAA(){
		boolean res = false;
		HttpServletRequest req = HttpUtil.getHttpServletRequest();		
		AccessibilityManager acc = (AccessibilityManager) req.getSession().getAttribute("accBean");
		if(null!=acc && acc.getPagePath()!= null && acc.getPagePath().contains("aaa/")){
			res = true;
		}
		return res;
	}
}
