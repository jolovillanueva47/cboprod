package org.cambridge.ebooks.online.widget;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

/**
 * 
 * @author apirante
 *
 */

public class WidgetServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(WidgetServlet.class);	
	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		logger.info("Starting widget servlet...");
		
		String cmd = req.getParameter("cmd");
		
		if(StringUtils.isNotEmpty(cmd)){
			if("openSearch".equals(cmd)){
				String xml = req.getParameter("xml");
				String servlet = req.getParameter("servlet");
				logger.info("  xml="+xml);
				
			       String filePath = StringUtils.trimToEmpty(System.getProperty("cbo.ebooks.war"))  +  xml;
			       File file = new File(filePath);
			       logger.info(" filepath="+filePath);
			       logger.info("  file="+file);
			       
					if(file.exists()){
						logger.info("  filefound="+file);
						logger.info("  servlet="+servlet);
						try{
					        List<String> contents = FileUtils.readLines(file);
					        StringBuffer sb = new StringBuffer();
					        for (String line : contents){
					            if(StringUtils.isNotEmpty(line)){
					            	sb.append(StringUtils.replace(line, "{ebooks.widget.path}", servlet));
					            }
					        }//end for
								resp.setContentType("text/xml");
								Writer writer = resp.getWriter();
								writer.write(sb.toString());
								writer.close();

					    }catch (IOException e){
					    	logger.error(ExceptionPrinter.getStackTraceAsString(e));
					    }//end try	
					}else{
						throw new FileNotFoundException();
					}//end if file exist		          
				}//end if    
			}//end if       
		logger.info("Widget Servlet End");
	}
	
}
