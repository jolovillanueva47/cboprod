package org.cambridge.ebooks.online.util;

import org.apache.log4j.Logger;

/**
 * 
 * @author rvillamor
 *
 */

public class EBooksConfiguration {

	private static final Logger LOGGER = Logger.getLogger(EBooksConfiguration.class);
	
	public static String getProperty(String propertyName){
		String result = "";
		
		if ( StringUtil.isEmpty( System.getProperty(propertyName)) )
		{			
			LOGGER.error("Property name: " + propertyName + " has not been set on property-service.xml");
		}
		else
		{
			result = System.getProperty(propertyName).trim();
		}
		
		return result;
		
	}
	
}
