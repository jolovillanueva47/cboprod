package org.cambridge.ebooks.online.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.pdfviewer.PDFMetadata;

public class SolrUtil {

	public static final String DELIMITER = "=DEL=";
	
	private static final Logger logger = Logger.getLogger(SolrUtil.class);	
		
	public static String getBookIdByChapterId(String cid) {
		logger.info("[getBookIdByChapterId]cid:" + cid);
		
		String bookId = "";
		SolrQuery q = new SolrQuery();
		q.setQuery("id:" + cid);
		
		QueryResponse response = null;
		
		try {
			response = SolrServerUtil.getChapterCore().query(q);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(docs.size() > 0) {
				bookId = docs.get(0).getBookId();
			}
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getBookIdByChapterId]book_id:" + bookId);
		
		return bookId;
	}
	
	public static String getIsbnByBookId(String bid) {
		logger.info("[getIsbnByBookId]bid:" + bid);
		
		String isbn = "";
		SolrQuery q = new SolrQuery();
		q.setQuery("book_id:" + bid);
		
		QueryResponse response = null;
		
		try {
			response = SolrServerUtil.getBookCore().query(q);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(docs.size() > 0) {
				isbn = docs.get(0).getIsbn();
			}
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getIsbnByBookId]isbn:" + isbn);
		
		return isbn;
	}
	
	public static String getAbstractByIsbn(String isbn){
		logger.info("[getAbstractByIsbn]isbn:" + isbn);
		
		String blur = "";
		SolrQuery q = new SolrQuery();
		q.setQuery("isbn:" + isbn);
		
		QueryResponse response = null;
		
		try{
			response = SolrServerUtil.getBookCore().query(q);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(docs.size() > 0){
				blur = docs.get(0).getBlurb();
			}
		}catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getAbstractByIsbn]abstract:" + blur);
		
		return blur;
	}
	
	public static String getPublisherNameByIsbn(String isbn){
		logger.info("[getPublisherNameByIsbn]isbn:" + isbn);
		
		String publisherName = "";
		SolrQuery q = new SolrQuery();
		q.setQuery("isbn:" + isbn);
		
		QueryResponse response = null;
		
		try{
			response = SolrServerUtil.getBookCore().query(q);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(docs.size() > 0){
				publisherName = docs.get(0).getPublisherName();
			}
		}catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getPublisherNameByIsbn]publisherName:" + publisherName);
		
		return publisherName;
	}
	
	public static String getPublisherLocationByIsbn(String isbn){
		logger.info("[getPublisherLocationByIsbn]isbn:" + isbn);
		
		String publisherLocation = "";
		SolrQuery q = new SolrQuery();
		q.setQuery("isbn:" + isbn);
		
		QueryResponse response = null;
		
		try{
			response = SolrServerUtil.getBookCore().query(q);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(docs.size() > 0){
				publisherLocation = docs.get(0).getPublisherLoc();
			}
		}catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getPublisherLocationByIsbn]publisherLocation:" + publisherLocation);
		
		return publisherLocation;
	}
	

	
	public static int getSeriesNumberByIsbn(String isbn) {
		logger.info("[getSeriesNumberByIsbn]isbn:" + isbn);
		int seriesNumber = 0;
		
		SolrQuery q = new SolrQuery();
		q.setQuery("isbn:" + isbn);
		
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			
			if(docs.size() > 0) {
				seriesNumber = docs.get(0).getSeriesNumber();
			}
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		logger.info("[getSeriesNumberByIsbn]series_number:" + seriesNumber);
		
		return seriesNumber;
	}
	
	public static List<BookAndChapterSolrDto> getSeriesNumberMapByIsbns(String isbns) {
		logger.info("[getSeriesNumberByIsbn]isbn:" + isbns);
		List<BookAndChapterSolrDto> docs = new ArrayList<BookAndChapterSolrDto>();
		
		SolrQuery q = new SolrQuery();
		q.setQuery("isbn:" + isbns);
		
		QueryResponse response = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			docs = response.getBeans(BookAndChapterSolrDto.class);
			
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}		
		
		return docs;
	}
	
	/**
	 * @param bid - Book Id
	 * @param cid - Chapter Id
	 * @return List<BookAndChapterSolrDto> - index 1 for book info; index 2 for chapter info
	 */
	public static List<BookAndChapterSolrDto> getBookAndChapterInfoById(String bid, String cid) {
		ArrayList<BookAndChapterSolrDto> resultList = new ArrayList<BookAndChapterSolrDto>(2);
		
		SolrQuery q = new SolrQuery();
		q.setQuery("id:" + bid);
		
		QueryResponse response = null;		
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			if(null != docs && docs.size() > 0) {
				resultList.add(docs.get(0));
			}
			
			// chapter
			if(StringUtils.isNotEmpty(cid)) {
				q.setQuery("id:" + cid);
				response = SolrServerUtil.getChapterCore().query(q, METHOD.GET);
				docs = response.getBeans(BookAndChapterSolrDto.class);
				if(null != docs && docs.size() > 0) {
					// add aditional info to result
					resultList.add(docs.get(0));
				}
			}
			
			resultList.trimToSize();
			
		} catch (SolrServerException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		
		return resultList;
	}
	
	public static PDFMetadata getPdfMetaData(String cid) {
		logger.info("[getBookContentItem]cid:" + cid);
			
		BookAndChapterSolrDto doc = null;
		PDFMetadata pdfMeta = null;
		
		SolrQuery q = new SolrQuery();
		q.setQuery("id:" + cid);

		QueryResponse response = null;
		try {
			response = SolrServerUtil.getChapterCore().query(q, METHOD.GET);
			List<BookAndChapterSolrDto> docs = response.getBeans(BookAndChapterSolrDto.class);
			pdfMeta = new PDFMetadata();			
			
			if(docs.size() > 0) {
				doc = docs.get(0);
				pdfMeta.setContentId(doc.getId());
				pdfMeta.setContentType(doc.getType());
				pdfMeta.setPdf(doc.getPdfFilename());
				pdfMeta.setDoi(doc.getDoi());
				pdfMeta.setBookId(doc.getBookId());
				pdfMeta.setEisbn(getIsbnByBookId(pdfMeta.getBookId()));	
			}
			
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}		
		
		return pdfMeta;
	}	
	
	public static List<BookAndChapterSolrDto> getSeriesDetailsBySubjectCode(String code) {
		logger.info("[getSeriesDetailsBySubjectCode]code:" + code);
		
		List<BookAndChapterSolrDto> docs = null;
		
		SolrQuery q = new SolrQuery();
		q.setQuery("subject_code:" + code);
		q.setRows(Integer.MAX_VALUE);

		QueryResponse response = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			docs = response.getBeans(BookAndChapterSolrDto.class);
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}		
		
		return docs;
	}
	
	public static List<BookAndChapterSolrDto> getSubjectBySeriesCode(String code){
		logger.info("[getSubjectBySeriesCode]code:" + code);
		
		List<BookAndChapterSolrDto> docs = null;
		
		SolrQuery q = new SolrQuery();
		q.setQuery("series_code:" + code);
		q.setRows(Integer.MAX_VALUE);

		QueryResponse response = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			docs = response.getBeans(BookAndChapterSolrDto.class);
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}		
		
		return docs;
	}
	
	
	public static List<BookAndChapterSolrDto> seriesExistsMapBySeriesCodes(String code){		
		logger.info("[getSubjectBySeriesCode]code:" + code);
		
		List<BookAndChapterSolrDto> docs = null;
		
		SolrQuery q = new SolrQuery();
		q.setQuery("series_code:" + code);
		q.setRows(Integer.MAX_VALUE);

		QueryResponse response = null;
		try {
			response = SolrServerUtil.getBookCore().query(q, METHOD.GET);
			docs = response.getBeans(BookAndChapterSolrDto.class);
		} catch (SolrServerException e) {
			logger.info(ExceptionPrinter.getStackTraceAsString(e));
		}		
				
		return docs;
	}
	
	
	public static <T> String generateLongOrQuery(List<T> seriesList, String beanProp){
		String output = "";
		if(seriesList.size() > 0){
			StringBuffer sb = new StringBuffer();
			sb.append(" ( ");
			for(T ts : seriesList){
				String value = null;
				try {
					value = BeanUtils.getProperty(ts, beanProp);
				} catch (Exception e) {
					//Do nothing
				}
				if(seriesList.get(seriesList.size()-1) != ts){					
					sb.append(value + " OR " );
				}else{
					sb.append(value);
				}
				
			}	
			sb.append(" ) ");
			
			output = sb.toString();
		}
		logger.info("[generateLongOrQuery]query:" + output);
		return output;
	}
	public static String generateLongOrQuery(String string){
		String output = "";
		
		string = string.replace("[", "");
		string = string.replace("]", "");
		
		if(StringUtil.isNotEmpty(string)){
			StringBuffer sb = new StringBuffer();
			sb.append(" ( ");
			String[] res = string.split(",");
			for (String str : res) {
				String value = str;
				if(res[res.length-1] != str){					
					sb.append(value + " OR " );
				}else{
					sb.append(value);
				}
			}
			
			sb.append(" ) ");
			
			output = sb.toString();
		}
		logger.info("[generateLongOrQuery]query:" + output);
		return output;
	}
}
