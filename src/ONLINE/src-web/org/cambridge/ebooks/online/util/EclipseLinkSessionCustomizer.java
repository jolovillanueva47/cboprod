package org.cambridge.ebooks.online.util;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.JNDIConnector;
import org.eclipse.persistence.sessions.Session;

public class EclipseLinkSessionCustomizer implements SessionCustomizer {
	public void customize(Session session) throws Exception {
		JNDIConnector connector = (JNDIConnector) session.getLogin().getConnector();
		connector.setLookupType(JNDIConnector.STRING_LOOKUP);
	}
}