package org.cambridge.ebooks.online.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

public class FileUtil {	
	
	private static final Logger LOGGER = Logger.getLogger(FileUtil.class);
	
	public static void generateFile(StringBuffer sb, String path) throws IOException{		
		FileWriter fstream = new FileWriter(path);
		try{
			BufferedWriter out = new BufferedWriter(fstream);
			try{
				out.write(sb.toString());			
			}finally{
				out.close();
			}
		}finally{
			fstream.close();
		}	    
	}
	
	public static void moveFile(String sourceFilePath, String destinationDir) throws Exception{
		// File (or directory) to be moved 
		File file = new File(sourceFilePath); 
		
		if(!file.isFile() || !file.canWrite()){
			throw new Exception("cannot move source file - " + sourceFilePath);
		}
		
		// Destination directory 
		File dir = new File(destinationDir);
		if(!dir.isDirectory() || !dir.canWrite()){
			throw new Exception("cannot move to destination directory - " + destinationDir);
		}
		
		moveFile(file, dir);		
	}
	
	
	public static void moveFile(File sourceFile, File destinationDir) throws Exception{
		// Move file to new directory 
		boolean success = sourceFile.renameTo(new File(destinationDir, sourceFile.getName())); 
		
		if (!success) {
			throw new Exception("move unsuccessful  --- I do not know why!!");
		}
	}
	
	
	
	public static void copyPasteFile(String sourceFilePath, String destinationDirPath){
		 InputStream in = null;
		 OutputStream out = null;
		 try{
			 LOGGER.info("copy sourceFilePath: " + sourceFilePath + " destinationDirPath:" + destinationDirPath);
		      File f1 = new File(sourceFilePath);
		      File f2 = new File(destinationDirPath);
		      in = new FileInputStream(f1);
		      
		      //For Append the file.
//		      OutputStream out = new FileOutputStream(f2,true);

		      //For Overwrite the file.
		      out = new FileOutputStream(f2);

		      byte[] buf = new byte[3072];
		      int len;
		      while ((len = in.read(buf)) > 0){
		        out.write(buf, 0, len);
		      }
		      in.close();
		      out.close();
		      //System.out.println("File copied.");
		    }
		    catch(FileNotFoundException ex){
//		      System.out.println(ex.getMessage() + " in the specified directory.");
//		      System.exit(0);
		    	ex.printStackTrace();
		    }
		    catch(IOException e){
		      System.out.println(e.getMessage());      
		    } finally {
		    	try {
					in.close();
					out.close();
				} catch (Exception e) {
					//
					//e.printStackTrace();
				}
		    }
		    
	}

	
	
}
