package org.cambridge.ebooks.online.util;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.ebook.Author;

public class EBooksUtil {

	private static final Logger LOGGER = Logger.getLogger(EBooksUtil.class);
		
	public static String getDisplayFileSize(String isbn, String fileName) {
		String result = null;
		File f = new File(EBooksProperties.CONTENT_DIR + isbn + "/" + fileName);
		
		if (f.exists() && f.isFile()) {
			float size = f.length()/1024;
			
			String sizeType = size > 1024 ? " MB" : " KB";
			
			size = size > 1024 ? size / 1024 : size;
			
			NumberFormat formatter = new DecimalFormat("#.#");
			result = "(" + (formatter.format(size)) + sizeType + ")";
			
		} else {
			LOGGER.info("File not found! " + f.getAbsolutePath());
		}
		
		return result;
	}
	
	public static String getReferenceUrl(String isbn, String id) {
		String f = isbn + "/" + id + "ref.html";
		File file = new File(EBooksProperties.CONTENT_DIR + f);
		f = file.exists() && file.length()>10 ? id + "ref.html" : null;
		return f;
	}
	
	public static String getAuthorDisplay(ArrayList<Author> authors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		boolean isEtAl = false;
		if(authors != null && authors.size() > 0) {
			if (authors.size() > 4 && !isEtAl) {
				result.append((authors.get(0)).getFixName());
				result.append(" et al.");
				isEtAl = true;
			}
			if (!isEtAl) {
				for(Author author : authors) {
					result.append(author.getFixName());
					result.append(", ");
				}
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ',') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	public static String getAuthors(ArrayList<Author> authors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		boolean isEtAl = false;
		if(authors != null && authors.size() > 0) {
			if (authors.size() > 4 && !isEtAl) {
				result.append((authors.get(0)).getName());
				result.append(" et al.");
				isEtAl = true;
			}
			if (!isEtAl) {
				for(Author author : authors) {
					result.append(author.getName());
					result.append("; ");
				}
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ';') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	public static String getContributorDisplay(List<String> contributors) {
		StringBuilder result = new StringBuilder();
		String resultString = "";
		boolean isEtAl = false;
		if(contributors != null && contributors.size() > 0) {
			if (contributors.size() > 4 && !isEtAl) {
				result.append(formatName(contributors.get(0)));
				result.append(" et al.");
				isEtAl = true;
			}
			if (!isEtAl) {
				for(String contributor : contributors) {
					result.append(formatName(contributor));
					result.append(", ");
				}
			}
			resultString = result.toString().trim();
			char lastChar = resultString.charAt(resultString.length() - 1);
			if(lastChar == ',') {
				resultString = resultString.substring(0, resultString.length() - 1);
			}
		}
		
		return resultString;
	}
	
	public static String getContributorDisplay(String contributors) {		
		StringBuilder result = new StringBuilder();
		String resultString = "";
		boolean isEtAl = false;
		if(contributors != null && contributors.length() > 0) {
			String[] contribArr = contributors.split(";");
			if (contribArr.length > 4 && !isEtAl) {
				result.append(contribArr[0]);
				result.append(" et al.");
				isEtAl = true;
			}
			if (!isEtAl) {
				result.append(contributors);
			}
			resultString = result.toString().trim();			
		}
		
		return resultString;
	}
	
	public static String formatName(String name) {
		String result = "";
		String[] names = name.split(",");
		if(names.length > 1) {
			result = names[1].trim() + " " + names[0].trim();
		} else {
			result = name;
		}
		
		return result;
	}
	
	public static String getFirstAuthor(ArrayList<Author> authors) {
		if(authors.size() > 0) {
			return authors.get(0).getName();
		}
		return "";
	}
	
	
	private static final String EBOOK = "ebook.jsp";		
	
	private static final String EBOOK_LANDING =  "eBookLandingPageBean";
	
	private static final String CHAPTER_LANDING =  "chapterLandingPageBean";
	
	
//	public static String getWorldCatLink(HttpServletRequest request, String worldCatUrl, String encoding) throws UnsupportedEncodingException {
//		String url = request.getRequestURL().toString();
//		String q = null;
//		
//		if(url.contains(EBOOK)){
//			EBookLandingPageBean bl = (EBookLandingPageBean) request.getAttribute(EBOOK_LANDING);
//			q = URLEncoder.encode("ti:" + bl.getBookTitle() + " au:" + bl.getFirstAuthor(), encoding);
//		}else{
//			ChapterLandingPageBean cl = (ChapterLandingPageBean) request.getAttribute(CHAPTER_LANDING);
//			q = URLEncoder.encode("ti:" + cl.getBookTitle() + " au:" + cl.getFirstAuthor(), encoding);
//		}
//		q = worldCatUrl + q;
//		//return URLEncoder.encode(q, encoding);
//		return q;
//	}
	
	public static String getCurrentDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String formatEdition(String edition) {
		if(StringUtils.isEmpty(edition)){
			return null;
		}
		else {
			String tensUp = "";
			int indexOfDash = edition.lastIndexOf('-');
			edition = edition.substring(indexOfDash+1).trim();
			int editionNum = Integer.parseInt(edition);
			if (editionNum > 10){
				int edlength = edition.length();
				tensUp = edition.substring(0, edlength-1);
			}
			
			// if less than 10
			if (editionNum <= 10) {
				if(editionNum % 10 == 1 ){
					edition = "1st Edition";
				} else if (editionNum % 10 == 2){
					edition = "2nd Edition";
				} else if (editionNum % 10 == 3){
					edition = "3rd Edition";
				} else {
					edition = edition + "th Edition";
				}
			}
			
			// for cases where edition > 10
			else if (editionNum > 10){
				if (editionNum % 100 == 11 && editionNum < 100){ 
					edition = "11th Edition";
				} else if (editionNum % 100 == 11){
					edition = tensUp.substring(0, tensUp.length()-1) + "11th Edition";
				} else if (editionNum % 100 == 12 && editionNum < 100){ 
					edition = "12th Edition";
				} else if (editionNum % 100 == 12){
					edition = tensUp.substring(0, tensUp.length()-1) + "12th Edition";
				} else if (editionNum % 100 == 13 && editionNum < 100){ 
					edition = "13th Edition";
				} else if (editionNum % 100 == 13){
					edition = tensUp.substring(0, tensUp.length()-1) + "13th Edition";
				} else if (editionNum % 10 == 1 ){
					edition = tensUp + "1st Edition";
				} else if (editionNum % 10 == 2 ){
					edition = tensUp + "2nd Edition";
				} else if (editionNum % 10 == 3 ){
					edition = tensUp + "3rd Edition";
				} else {
					edition = edition + "th Edition";
				}
			}
			return edition;
		}
	}
	
	public static boolean isAllSubjectBook(String subjectCode){
		if(StringUtils.isEmpty(subjectCode)) {
			return false;
		} else {
			subjectCode = subjectCode.substring(0, 1);
			if (subjectCode.equals("A") || subjectCode.equals("B") || subjectCode.equals("C") || subjectCode.equals("D") || subjectCode.equals("E"))
				return true;
			else
				return false;
		}
	}
	
	public static String formatPartNumber(String partNumber){
		if(StringUtils.isEmpty(partNumber)){
			return null;
		} else {
			String partNumberLowerCase = partNumber.toLowerCase();
			if(partNumberLowerCase.contains("part")) {
				return partNumber;
			} else {
				return "Part " + partNumber;
			}
		}
	}
	
	public static String formatVolumeNumber(String volumeNumber){
		if(StringUtils.isEmpty(volumeNumber)){
			return null;
		} else {
			String volumeNumberLowerCase = volumeNumber.toLowerCase();
			if(volumeNumberLowerCase.contains("volume") || volumeNumberLowerCase.contains("vol")) {
				return volumeNumber;
			} else {
				return "Volume " + volumeNumber;
			}
		}
	}
	
	
}
