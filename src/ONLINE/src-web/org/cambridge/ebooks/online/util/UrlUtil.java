/**
 * 
 */
package org.cambridge.ebooks.online.util;

import java.net.URL;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kmulingtapang
 *
 */
public class UrlUtil {

	public static String getClientDns(HttpServletRequest request) throws Exception {
		URL url = new URL(request.getRequestURL().toString());
		StringBuilder sb = new StringBuilder();			
		
		sb.append(url.getProtocol()).append("://").append(url.getHost())
		.append((url.getPort() > -1 ? ":" + url.getPort() : ""))
		.append(getContextRoot());	
		
		return sb.toString();
	}
	
	public static String getClientHttpsDns(HttpServletRequest request) throws Exception {		
		String dns = getClientDns(request);		
		return dns.replace("http", "https");
	}
	
	public static String getContextRoot() {
		return System.getProperty("context.root");
	}
	
	public static String getContextRootCjo() {
		return System.getProperty("context.root.cjo");
	}
}
