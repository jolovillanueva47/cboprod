package org.cambridge.ebooks.online.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
	
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(TIMESTAMP_FORMAT);

	/**
	 * convert String to Timestamp
	 * @param str
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp convStrToTs(String str) throws ParseException{
		return new Timestamp(SDF.parse(str).getTime());
	}
	
	/**
	 * Convert Timestamp to String
	 * @param ts
	 * @return
	 */
	public static String convTsToStr(Timestamp ts){
		return String.valueOf(ts);
	}

}
