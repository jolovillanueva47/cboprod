package org.cambridge.ebooks.online.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.User;

/**
 * 
 * @author jgalang
 * 
 */

public class HttpUtil {

	private static final Logger LOGGER = Logger.getLogger(HttpUtil.class);
	
	public static final String RESPONSE_DELIMETER = System.getProperty("line.separator"); 
	public static final String SERVER_DOWN_MESSAGE = "Server is down.";
	
	public static class HttpDownloadFile {
		private HttpServletResponse response;
		private String fileName;
		private String extension;
		private String contentType;
		private String encoding;
		private String content;
		
		/**
		 * @return the response
		 */
		public HttpServletResponse getResponse() {
			return response;
		}
		/**
		 * @param response the response to set
		 */
		public void setResponse(HttpServletResponse response) {
			this.response = response;
		}
		/**
		 * @return the fileName
		 */
		public String getFileName() {
			return fileName;
		}
		/**
		 * @param fileName the fileName to set
		 */
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		/**
		 * @return the extension
		 */
		public String getExtension() {
			return extension;
		}
		/**
		 * @param extension the extension to set
		 */
		public void setExtension(String extension) {
			this.extension = extension;
		}
		/**
		 * @return the contentType
		 */
		public String getContentType() {
			return contentType;
		}
		/**
		 * @param contentType the contentType to set
		 */
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		/**
		 * @return the encoding
		 */
		public String getEncoding() {
			return encoding;
		}
		/**
		 * @param encoding the encoding to set
		 */
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}
		/**
		 * @return the content
		 */
		public String getContent() {
			return content;
		}
		/**
		 * @param content the content to set
		 */
		public void setContent(String content) {
			this.content = content;
		}
	}
	
	public static HttpSession getHttpSession(boolean createNew) { 
		HttpSession session = null;
		session =  (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(createNew);
		return session;
	}
	
	public static HttpServletRequest getHttpServletRequest() { 
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	public static HttpServletResponse getHttpServletResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}
	
	public static void redirect(String url) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static String getHttpServletRequestParameter(String param) { 
		if(StringUtil.isNotEmpty(param))
		{
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String result = request.getParameter(param);
			
			if(StringUtil.isNotEmpty(result))
				return result;
		}
		
		return null;
	}
	
	public static User getUserFromCurrentSession() {
		return getUserFromSession(getHttpSession(false));
	}
	
	public static User getUserFromSession(HttpSession session) {
		return (User) session.getAttribute("userInfo");
	}
	
	public static Object getAttributeFromCurrentSession(String attribute) {
		return getHttpSession(false).getAttribute(attribute);
	}
	
	public static Object getAttributeFromRequestOrSession(String attribute) {
		return getAttributeFromRequestOrSession(attribute, null);
	}
	
	/**
	 * get Object either from Session or request
	 * @param attribute
	 * @return
	 */
	public static Object getAttributeFromRequestOrSession(String attribute, HttpServletRequest request) {
		if(request == null){
			request = getHttpServletRequest();
		}
		
		Object obj = request.getAttribute(attribute);
		
		if( obj != null ){
			return obj;
		}
		
		HttpSession session = request.getSession();
		obj = session.getAttribute(attribute);
		
		return obj;
	}
	
	public static StringBuffer connectTo(String servletLink, Object[] params, Object[] values) throws IOException { 
		StringBuffer response = new StringBuffer();
		StringBuffer queryString = new StringBuffer();
		HttpURLConnection ucn = null;
		DataOutputStream dos = null;
		InputStream is = null;
		
		boolean validParams = StringUtil.isNotEmpty(params) && StringUtil.isNotEmpty(values) && params.length == values.length;
		if ( validParams ) {
			int len = params.length;
			for (int i = 0; i < len; i++) {
				queryString.append(params[i]);
				queryString.append("=");
				queryString.append(StringUtil.nvl(values[i]));

				if (i < (len - 1)) {
					queryString.append("&");
				}
			}
		}
		
		LOGGER.info("connecting To: " + servletLink + "?" + queryString.toString() );
		
		try {
			URL url = new URL(servletLink);
			ucn = (HttpURLConnection) url.openConnection();
			ucn.setDoInput(true);
			ucn.setDoOutput(true);
			ucn.setUseCaches(false);
			ucn.addRequestProperty("accept", "true");
			ucn.setRequestMethod("POST");
			
			dos = new DataOutputStream(ucn.getOutputStream());
			dos.writeBytes(queryString.toString());
			
			LOGGER.info("connecting To: 3" );
			
			is = ucn.getInputStream();
			
			LOGGER.info("connecting To: 2" );
			if ( is != null ) { 
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String responseLines;
				while ( (responseLines = br.readLine()) != null ) { 
					response.append(responseLines + RESPONSE_DELIMETER);
				}
			}
			LOGGER.info("connecting To: 1" );
			
			
		} catch ( MalformedURLException mue )  { 
			LOGGER.error(mue.getMessage());
		} catch ( IOException ioe ) { 
			LOGGER.error(ioe.getMessage());
			throw ioe;
		} finally { 
			if ( dos != null ) {
				try { 
					dos.close();
				} catch (IOException ioe) {  
					LOGGER.error("[DOS] HttpUtil.java Exception..." );
				}
			}
			
			if ( is != null ) { 
				try { 
					is.close();
				} catch (IOException ioe) {  
					LOGGER.error("[IS] HttpUtil.java Exception..." );
				}
			}
			ucn.disconnect();
		}
		
		return response;
	}
	
	public static Object getSessionAttribute(String key) { 
		return getHttpSession(false).getAttribute(key);
	}
	
	public static String getHeaderValue(final String headerName ) { 
		if ( StringUtil.isEmpty(headerName)) { 
			throw new IllegalArgumentException("getHeaderValue(" +  headerName + ") cannot be an empty string... ");
		}
		
		String result = null;
		Map<String, String > header =  FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap();
		
		for (  Map.Entry<String, String> pairs : header.entrySet()) { 
				if ( headerName.toLowerCase().equals(pairs.getKey().toLowerCase() ) ) { 
					result = pairs.getValue();
					break;
				}
		}
		
		return result;
		
	}
	
	public static void download(HttpDownloadFile downloadFile) throws IOException, Exception {
		downloadFile.getResponse().setHeader("Content-Disposition", "attachment;filename=" 
				+ downloadFile.getFileName() + "." + downloadFile.getExtension());		
		downloadFile.getResponse().setContentType(downloadFile.getContentType());		
		downloadFile.getResponse().setCharacterEncoding(downloadFile.getEncoding()); 
		
		try {
			downloadFile.getResponse().getWriter().print(downloadFile.getContent());
			downloadFile.getResponse().getWriter().flush();
		} catch (IOException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			downloadFile.getResponse().getWriter().close();
		}
	}
	
	private static final String QUESTION_MARK = "?";
	
	public static String getExactUrl(HttpServletRequest request){
		return request.getRequestURL() + QUESTION_MARK + request.getQueryString();
	}
	
	private static final String AMP = "&";
	private static final String AMP_HTML = "&amp;";
	private static final String EQUALS = "=";
	private static final String QUESTION_MARK_ESC = "\\?";
	
	public static String removeURLParam(String param, String url){
		String[] urlQuery = url.split(QUESTION_MARK_ESC);
		String[] params = urlQuery[1].split(AMP);
				
		//recreate url
		StringBuffer sb = new StringBuffer();
		sb.append(urlQuery[0]).append(QUESTION_MARK);
		for(String _param : params){
			if(StringUtils.isNotEmpty(_param)){
				String paramKey = _param.split(EQUALS)[0];
				if(!param.equals(paramKey)){
					sb.append(_param).append(AMP_HTML);
				}
			}
		}
		
		return sb.toString();
	}
}
