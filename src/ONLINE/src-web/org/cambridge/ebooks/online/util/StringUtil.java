package org.cambridge.ebooks.online.util;


import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.landingpage.BookBean;
import org.cambridge.ebooks.online.landingpage.BookContentItem;
import org.cambridge.ebooks.online.landingpage.BookMetaData;
import org.cambridge.ebooks.online.landingpage.CboBookDataWorker;



/**
 * 
 * @author cmcastro
 *
 */

public class StringUtil {

	private static final String REAL_NUMBER = "^[-+]?\\d+(\\.\\d+)?$";	
	private static final String[] SPECIAL_CHARS = {"\\\\", "\\+", "\\-", "!", "\\(", "\\)", "\\{", "\\}", "\\[", "\\]", "\\^", "~", "\\*", "\\?", ":"};
	private static final String[] SPECIAL_CHARS_REPLACEMENT = {"\\\\\\", "\\\\+", "\\\\-", "\\\\!", "\\\\(", "\\\\)", "\\\\{", "\\\\}", "\\\\[", "\\\\]", "\\\\^", "\\\\~", "\\\\*", "\\\\?", "\\\\:"};
	private static final Logger logger = Logger.getLogger(CboBookDataWorker.class);

	public static boolean isNumeric(String val) {
	   return isEmpty( val ) ? false : val.matches(REAL_NUMBER);
	}

	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
	
	public static String nvl(String value, String replacement) { 
		return isEmpty(value) ? replacement : value;
	}
	
	public static String nvl(String value ) { 
		return isEmpty(value) ? "" : value;
	}
	
	public static <T> String nvl(T value) { 
		return ((T) (isEmpty(value) ? "" : value)).toString();
	}
	
	public static <T> String nvl(T value, T replacement) { 
		return ((T) (isEmpty(value) ? replacement : value)).toString();
	}
	
	public static boolean validateLength(Object obj, int minLength, int maxLength) { 
		boolean result = true;
		if ( obj  instanceof String ) { 
			String value = ( String ) obj;
			if ( isNotEmpty(value) ) { 
				result = value.length() >= minLength && value.length() <= maxLength;
			}
		}
		return result;
	}	
	
	public static String addPadding(final String value, final String padding ) {
		boolean ifValueIsLongerThanThePadding = nvl(value).length() > nvl(padding).length();
		if ( ifValueIsLongerThanThePadding ) { 
			return value;
		}
		String result = nvl(padding) + nvl(value);
		return result.substring( 0 + nvl(value).length(), result.length() );
	}
	
	
	public static String join(List<String> list, String separator) {
		String[] listArr = list.toArray(new String[list.size()]);
		return join(listArr, separator);		
	}
	
	public static String join(String[] array, String separator) {
		if (array == null) {
            return null;
        }
        int arraySize = array.length;
        StringBuilder  result = new StringBuilder ();
        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                result.append(separator);
            }
            if (array[i] != null) {
                result.append(array[i]);
            }
        }
        return result.toString();
	}
	
	public static String  join(String [] array, char separator) {
		return join(array, separator + "");
    }
	
	public static String[] split(String value){
		if(isEmpty(value)) return null;
		value = value.replace("[", "").replace("]", "");
		return value.split(",");
	}
	
	public static String removeHTMLTags(String input) {
		String delim1 = "&#.*?;.*?&#.*?;";
		String delim2 = "<.*?>";
		String delim3 = "&lt;.*?&gt;";
		String delim4 = "";
				
		String temp = input.replaceAll(delim1, "").replaceAll(delim2, "").replaceAll(delim3, "").replaceAll(delim4, " ").trim(); 

		return temp;
	}
	
	public static String correctHtml(String html) {
		if(isEmpty(html)) {
			return "";
		}
		return html.replaceAll("<p align=\"right\">", "<p style=\"text-align: right;\">");
	}
	
	public static String stripHTMLTags(String input) {
		String result = input;
		
		if(StringUtils.isNotEmpty(input)) {
			result = input.replaceAll("(?i)(<[\\s]*[^>]*(" +
					"a|abbr|acronym|address|applet|area|b|base|basefont|bdo|big|blockquote|body|" +
					"br|button|caption|center|cite|code|col|colgroup|dd|del|dfn|" +
					"dir|div|dl|dt|em|fieldset|font|form|frame|frameset|" +
					"h1|h2|h3|h4|h5|h6|head|html|i|iframe|img|input|ins|isindex|kbd|label|" +
					"legend|li|link|map|menu|meta|noframes|noscript|object|ol|optgroup|option|" +
					"p|param|pre|q|s|samp|script|select|small|span|strike|strong|style|sub|sup|" +
					"table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var|xmp)[^>]*>)", "");
		}
		
		return result;
	}
	
	public static String stripUnicodes(String input) {
		if(input == null || input.equals("")){
			return input;
		}
		
		return input.replaceAll("&#[^;]+;", " ");
	}
	
	public static String escapeUnicode(String input) {
		String result = input;
		if(StringUtils.isNotEmpty(input)){
			result = input.replaceAll("&", "=AMP=");
		}
		
		return result;
	}
	
	public static String unescapeUnicode(String input) {
		String result = input;
		if(StringUtils.isNotEmpty(input)){
			result = input.replaceAll("=AMP=", "&");
		}
		
		return result;
	}
	
	public static String escapeSolrCharacters(String text) {
		String result = text;
		for(int i = 0; i < SPECIAL_CHARS.length; i++) {
			result = result.replaceAll(SPECIAL_CHARS[i], SPECIAL_CHARS_REPLACEMENT[i]);
		}
		
		return result;
	}
	
	public static String generateCoinString(BookBean bookBean){
		String output = "";
		if(bookBean.getContentType().equals("book")){
			output = generateCoinString(bookBean.getBookMetaData(), null);
		} else {
			output = generateCoinString(bookBean.getBookMetaData(), bookBean.getBookContentItem());
		}
		return output;
	}
	
	public static String generateCoinString(BookMetaData bookMetaData, BookContentItem contentMetaData) {
//		BookMetaData bookMetaData = bookBean.getBookMetaData();
//		BookContentItem contentMetaData = bookBean.getBookContentItem();
		String coinsString = "";
		try {
			StringBuilder string = new StringBuilder();
			
			string.append("ctx_ver=Z39.88-2004");
			string.append("&amp;rft_val_fmt=info:ofi/fmt:kev:mtx:book");
			//string.append("&amp;rfr_id=info:sid/ocoins.info:generator");
			
//			if(bookBean.getContentType().equals("book")){
			if(isEmpty(contentMetaData)) {
				string.append("&amp;rft.genre=book");
			} else {
				string.append("&amp;rft.genre=bookitem");
				if(StringUtils.isNotEmpty(contentMetaData.getTitle())) {
					string.append("&amp;rft.atitle="+URLEncoder.encode(contentMetaData.getTitle(), "US-ASCII"));
				}
				
			}
			
			if(StringUtils.isNotEmpty(bookMetaData.getTitle())) {
				string.append("&amp;rft.btitle="+URLEncoder.encode(bookMetaData.getTitle(), "US-ASCII"));
				string.append("&amp;rft.title="+URLEncoder.encode(bookMetaData.getTitle(), "US-ASCII"));
			}
			if (StringUtils.isNotEmpty(bookMetaData.getSeries())) {
				string.append("&amp;rft.series="+URLEncoder.encode(bookMetaData.getSeries(), "US-ASCII"));
			}
			if (!bookMetaData.getAuthorNameList().isEmpty()) {
//				string.append("&amp;rft.aulast="+URLEncoder.encode(bookMetaData.getAuthorNameList().get(0), "US-ASCII"));
//				string.append("&amp;rft.aufirst="+URLEncoder.encode(bookMetaData.getAuthorNameList().get(1), "US-ASCII"));
				
//				if(StringUtils.isNotEmpty(bookMetaData.getAuthorAffiliationList().get(0))) {
//					string.append("&amp;rft.aucorp="+URLEncoder.encode(bookMetaData.getAuthorAffiliationList().get(0), "US-ASCII"));
//				}
				
				for(String au: bookMetaData.getAuthorNameList()){
					string.append("&amp;rft.au="+URLEncoder.encode(au, "US-ASCII"));
				}
			}
			
			if (StringUtils.isNotEmpty(bookMetaData.getOnlineIsbn())) {
				string.append("&amp;rft.isbn=" + URLEncoder.encode(bookMetaData.getOnlineIsbn(), "US-ASCII"));
			}
			if (StringUtils.isNotEmpty(bookMetaData.getVolumeNumber())) {
				string.append("&amp;rft.volume=" + URLEncoder.encode(bookMetaData.getVolumeNumber(), "US-ASCII"));
			}
			if (StringUtils.isNotEmpty(bookMetaData.getEdition())) {
				string.append("&amp;rft.edition=" + URLEncoder.encode(bookMetaData.getEdition(), "US-ASCII"));
			}
			if (StringUtils.isNotEmpty(bookMetaData.getPrintDate())) {
				string.append("&amp;rft.date=" + URLEncoder.encode(bookMetaData.getPrintDate(), "US-ASCII"));
			}
			if (StringUtils.isNotEmpty(bookMetaData.getPublisherName())) {
				string.append("&amp;rft.pub=" + URLEncoder.encode(bookMetaData.getPublisherName(), "US-ASCII"));
			}
			
			string.append("&amp;rft_id="+URLEncoder.encode("http://dx.doi.org/" + bookMetaData.getDoi(), "US-ASCII"));
			
			coinsString = string.toString();
		} catch (Exception e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		return coinsString;
	}
}
