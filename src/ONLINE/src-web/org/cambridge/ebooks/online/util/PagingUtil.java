package org.cambridge.ebooks.online.util;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PagingUtil {
	
	public static final String ATTRIBUTE = "_pageBean";
	public static final String PAGE_SIZE = "_pageSize";
	public static final String CURRENT_PAGE = "_currentPage";
	public static final String SORT_BY = "_sortBy";
	public static final String SORT_BY_2 = "_sortBy2";

	private int recordCount;
	private int pageSize = 10;
	private int currentPage = 1;
	private String sortBy = "startDate";
	private String sortBy2 = "startDate";
	private LinkedHashMap sortCollection;
	
    /**
     * @return Returns the sortCollections.
     */
    public LinkedHashMap getSortCollection() {
        return sortCollection;
    }
    /**
     * @param sortCollections The sortCollections to set.
     */
    public void setSortCollection(LinkedHashMap sortCollection) {
        this.sortCollection = sortCollection;
    }
    /**
     * @return Returns the soryBy.
     */
    public String getSortBy() {
        return sortBy;
    }
    /**
     * @param soryBy The soryBy to set.
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
    
    public void setSortBy(HttpServletRequest request) {
        String sort = request.getParameter(SORT_BY);
        
		String gotoSort = "startDate";
		if (sort != null) {
			try {
			    gotoSort = sort;
			} catch(Exception e) {
			    gotoSort = "startDate";
			}
		}        
        setSortBy(gotoSort);
    }
    
    public String getSortBy2() {
        return sortBy2;
    }
    
    public void setSortBy2(String sortBy) {
        this.sortBy2 = sortBy;
    }
    
    public void setSortBy2(HttpServletRequest request) {
        String sort = request.getParameter(SORT_BY_2);
        
		String gotoSort = "startDate";
		if (sort != null) {
			try {
			    gotoSort = sort;
			} catch(Exception e) {
			    gotoSort = "startDate";
			}
		}        
        setSortBy2(gotoSort);
    }
    
	public PagingUtil() {
	}

	/**
	 * Creates an instance of PagingUtil and adds it in the request as an attribute
	 * whose name is equal to the value of <code>PagingUtil.ATTRIBUTE</code>. 
	 * The <code>pageSize</code> is set to 10 by default. 
	 * It also sets the value of <code>currentPage</code> based on the <code>request</code>
	 * parameter <code>PagingUtil.CURRENT_PAGE</code>. If the parameter is not found or 
	 * is equal to zero, <code>currentPage</code> is set to 1.
	 * @param <code>request</code> - request where this instance is added as an attribute
	 */
	public PagingUtil(HttpServletRequest request) {
		this(request, 10);
	}
	
	/**
	 * Creates an instance of PagingUtil and adds it in the request as an attribute
	 * whose name is equal to the value of <code>PagingUtil.ATTRIBUTE</code>. 
	 * The value of the <code>currentPage</code> is set based on the parameter found 
	 * in the given <code>request</code>. 
	 * @param <code>request</code> - request where this instance is added as an attribute
	 * @param <code>pageSize</code> - number of elements in one page
	 */
	public PagingUtil(HttpServletRequest request, int pageSize) {
		init(request, pageSize);
	}

	private void init(HttpServletRequest request, int pageSize) {
		setPageSize(request, pageSize);
		setCurrentPage(request);
		setSortBy(request);
		setSortBy2(request);
		request.setAttribute(ATTRIBUTE, this);
	}

	/** Same as above except that this includes Sorting **/
	public PagingUtil(HttpServletRequest request,LinkedHashMap sortBy) {
		this(request, 10, sortBy);
	}	
	
	public PagingUtil(HttpServletRequest request, int pageSize, LinkedHashMap sortBy) {
		init(request, pageSize, sortBy);
	}
	
	private void init(HttpServletRequest request, int pageSize, LinkedHashMap sortBy) {
		setPageSize(request, pageSize);
		setCurrentPage(request);
		setSortCollection(sortBy);
		setSortBy(request);
		setSortBy2(request);
		request.setAttribute(ATTRIBUTE, this);
	}	
	
	
	/**
	 * Returns the total number of records.
	 * @return the total number of records 
	 */
	public int getRecordCount() {
		return recordCount;
	}

	/**
	 * Sets the total number of records
	 * @param i - the total number of records 
	 */
	public void setRecordCount(int i) {
		recordCount = i;
	}

	/**
	 * Returns the current page.
	 * @return the current page
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	
	public void setCurrentPage(int i) {
		if (i > 0) {
			currentPage = i;
		} else {
			currentPage = 1;
		}
	}


	/**
	 * @param request
	 */
	private void setCurrentPage(HttpServletRequest request) {
		String page = request.getParameter(CURRENT_PAGE);
		int gotoPage = 1;
		if (page != null) {
			try {
				gotoPage = Integer.parseInt(page);
			} catch(Exception e) {
				gotoPage = 1;
			}
		}
		setCurrentPage(gotoPage);
	}


	/**
	 * Returns the page size.
	 * @return the page size
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param i
	 */
	public void setPageSize(int i) {
		pageSize = i;
	}


	/**
	 * @param request
	 */
	private void setPageSize(HttpServletRequest request) {
		setPageSize(request, 10);
	}

	/**
	 * @param request
	 */
	private void setPageSize(HttpServletRequest request, int deflt) {
		String page = request.getParameter(PAGE_SIZE);
		if (page != null) {
			try {
				setPageSize(Integer.parseInt(page));
			} catch(Exception e) {
				setPageSize(deflt);
			}
		} else {
			setPageSize(deflt);
		}
	}

	
	/**
	 * Returns the page count.
	 * @return the page count
	 */
	public int getPageCount() {
		int pageCount = (int) getRecordCount()/getPageSize();
		if (getRecordCount()%getPageSize() > 0) {
			++pageCount;
		}
		pageCount = (pageCount==0) ? 1:pageCount;
		return pageCount;
	}

	
	/**
	 * Returns a list which contains page numbers. This is a convenience function
	 * for rendering page numbers in Struts.
	 * @return a list of page numbers
	 */
	public LinkedHashMap getPagesCollection() {
		int pageCount = getPageCount();
		LinkedHashMap pages = new LinkedHashMap();
		for(int i=0; i<pageCount; i++) {
			Integer p = new Integer(i+1);
			pages.put(p, p);
		}
		return pages;
	}
	
	
	/**
	 * 
	 */
	/*public LinkedHashMap getRecNumCollection() {
		LinkedHashMap pages = new LinkedHashMap();
		for(int i=10; i<=50; i=i+10) {
			Integer p = new Integer(i);
			pages.put(p, p);
		}
		Integer p100 = new Integer(100);
		pages.put(p100, p100);
		Integer pAll = new Integer(-1);
		pages.put("All", pAll);
		return pages;
	}*/	


	/**
	 * Returns the previous page, or <code>1</code> if current page is <code>1</code>. 
	 * @return the previous page, or <code>1</code> if current page is <code>1</code>
	 */
	public int getPreviousPage() {
		int prev = 1;
		if (getCurrentPage() > 1) {
			prev = getCurrentPage()-1; 
		}
		return prev;
	}	

	/**
	 * Returns the next page, or <code>1</code> if current page is <code>1</code>. 
	 * @return the next page, or <code>1</code> if current page is <code>1</code>
	 */
	public int getNextPage() {
		int next = getPageCount();
		if (getCurrentPage() < next) {
			next = getCurrentPage()+1; 
		}
		return next;
	}	


	/**
	 * Returns the index of the first record in the current page. 
	 * @return the index of the first record in the current page
	 */
	public int getFirstRecord() {
		return ((getCurrentPage()-1) * getPageSize()) + 1;
	}	

	
	/**
	 * Returns the index of the last record in the current page. 
	 * @return the index of the last record in the current page
	 */
	public int getLastRecord() {
		return ((getCurrentPage()-1) * getPageSize()) + getPageSize();
	}	

}

