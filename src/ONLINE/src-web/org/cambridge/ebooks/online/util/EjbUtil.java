package org.cambridge.ebooks.online.util;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Util for getting the JMS stuff
 * @author mmanalo
 *
 */
public class EjbUtil {
	/**
	 * gets the Jms Connection Factory
	 * @param jmsConnectionFactoryJndiName
	 * @return
	 * @throws ServiceLocatorException
	 */
	public static ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) throws Exception {
		ConnectionFactory jmsConnectionFactory = null;
		
		Context ctx = new InitialContext();
		jmsConnectionFactory = (ConnectionFactory) ctx.lookup(jmsConnectionFactoryJndiName);
		   
		return jmsConnectionFactory;
	}
	
	/**
	 * gets the JmsDestination
	 * @param jmsDestinationJndiName
	 * @return
	 * @throws ServiceLocatorException
	 */
	public static Destination getJmsDestination(String jmsDestinationJndiName) throws Exception {
		Destination jmsDestination = null;
		
		Context ctx = new InitialContext();
		jmsDestination = (Destination) ctx.lookup(jmsDestinationJndiName);
		
		return jmsDestination;
	}
}
