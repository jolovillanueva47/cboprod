
package org.cambridge.ebooks.online.util;

import static org.cambridge.ebooks.online.util.EBooksConfiguration.getProperty;

public interface EBooksProperties {

	public static final String CHAPTER_LANDING_INDEX_DIR = getProperty("chapter.landing.index.dir");
	public static final String BOOK_LANDING_INDEX_DIR = getProperty("book.landing.index.dir");
	public static final String SEARCH_INDEX_DIR = getProperty("search.index.dir");
	public static final String CONTENT_DIR = getProperty("content.dir");
	public static final String SERIES_INDEX_DIR = getProperty("series.index.dir");
	public static final String SUBJECT_INDEX_DIR = getProperty("subject.index.dir");
	public static final String CONTENT_URL = getProperty("content.url.absolute");
}
