package org.cambridge.ebooks.online.util;


/**
 * 
 * @author cmcastro
 *
 */

public class RedirectTo {

	public static final String CHECKOUT_PAGE = "checkout";
	public static final String CONFIRM_BASKET = "confirmbasket";
	public static final String BROWSEPRODUCTS = "browseproducts";
	public static final String CONFIRMED_PAYMENT = "confirmedpayment";
	public static final String PAYMENT_DETAILS = "creditcardinvalid";
	
	public static final String HOME_PAGE = "home_page";
	public static final String LOGIN_PAGE =  "login_page";
	
	public static final String IMAGE_BANK = "image_bank";
	public static final String CHAPTER = "chapter";
	public static final String SEARCH_RESULTS = "search_results";
	public static final String SEARCH_NEXT_PAGE = "search_results_np";
	public static final String ICON_SEARCH_RESULTS = "icon_search_results";
	public static final String ICON_SEARCH_NEXT_PAGE = "icon_search_results_np";
	public static final String ADVANCED_SEARCH_RESULTS = "advanced_search_results";
	public static final String ADVANCED_SEARCH_NEXT_PAGE = "advanced_search_results_np";
	public static final String ESSENTIAL_MYBOOKMARKS = "essential_mybookmarks";
	public static final String PRESCRIBERS_MYBOOKMARKS = "prescribers_mybookmarks";
	public static final String PRESCRIBERS_DRUG_MYBOOKMARKS = "prescribers_drug_mybookmarks";
	
}
