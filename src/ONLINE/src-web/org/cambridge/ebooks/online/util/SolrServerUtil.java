package org.cambridge.ebooks.online.util;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.impl.LBHttpSolrServer;

/**
 * @author kmulingtapang
 */
public class SolrServerUtil {

	private static final Logger LOGGER = Logger.getLogger(SolrServerUtil.class);
	
	// ==== SOLR Servers
	private static String solrServer1Url = "http://192.168.60.240:8180";
	private static String solrServer2Url = "http://192.168.60.64:8180";
	
	
	// ==== SOLR Core Names
	private static String bookCoreName = "book";
	private static String chapterCoreName = "chapter";
	private static String searchSlaveCoreName = "search";
	
	
	// ==== LBHttpSolrServer
	private static LBHttpSolrServer lbSearchCore;
	private static LBHttpSolrServer lbBookCore;	
	private static LBHttpSolrServer lbChapterCore;
	private static String lbAliveCheckInterval = "30000";
	private static String lbConnManagerTimeout = "300000";
	private static String lbConnTimeout = "60000";
	private static String lbSoTimeout = "60000";
	
	// ==== CommonsHttpSolrServer
	private static CommonsHttpSolrServer cBookCore;
	private static CommonsHttpSolrServer cChapterCore;
	private static CommonsHttpSolrServer cSearchCore;	
	private static String cSoTimeout = "60000";
	private static String cConnTimeout = "5000";
	private static String cDefMaxConnPerHost = "100";
	private static String cMaxTotalConn = "100";
	private static String cFollowRedirects = "false";
	private static String cAllowCompression = "false";
	private static String cMaxRetries = "1";
	
	static {		
		try {
			initVariables();
			initCommonServers();	
			initLoadBalancerServer();	
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	public static SolrServer getSearchCore() {		
		return lbSearchCore;
	}
	
	public static SolrServer getBookCore() {
		return lbBookCore;
	}
	
	public static SolrServer getChapterCore() {
		return lbChapterCore;
	}		

	
	private static void initVariables() {
		// SOLR Servers
		solrServer1Url = System.getProperty("solr.server1.url");
		solrServer2Url = System.getProperty("solr.server2.url");
		
		// SOLR Core Names
		bookCoreName = System.getProperty("core.book");
		chapterCoreName = System.getProperty("core.chapter");
		searchSlaveCoreName = System.getProperty("core.search");
		
		// CommonsHttpSolrServer 
		cSoTimeout = System.getProperty("c.socket.timeout");
		cConnTimeout = System.getProperty("c.connection.timeout");
		cDefMaxConnPerHost = System.getProperty("c.default.max.connection.per.host");
		cMaxTotalConn = System.getProperty("c.max.total.connections");
		cFollowRedirects = System.getProperty("c.follow.redirects");
		cAllowCompression = System.getProperty("c.allow.compression");
		cMaxRetries = System.getProperty("c.max.retries");
		
		// LBHttpSolrServer
		lbAliveCheckInterval = System.getProperty("lb.alive.check.interval"); 
		lbConnManagerTimeout = System.getProperty("lb.connection.manager.timeout");
		lbConnTimeout = System.getProperty("lb.connection.timeout"); 
		lbSoTimeout = System.getProperty("lb.socket.timeout");		
	}
	
	// calls --> initCommonServerSettings
	private static void initCommonServers() throws Exception {
		StringBuilder bookCoreUrl = new StringBuilder(solrServer1Url).append("/").append(bookCoreName);
		cBookCore = new CommonsHttpSolrServer(bookCoreUrl.toString());
		initCommonServerSettings(cBookCore);
		
		StringBuilder chapterCoreUrl = new StringBuilder(solrServer1Url).append("/").append(chapterCoreName);
		cChapterCore = new CommonsHttpSolrServer(chapterCoreUrl.toString());
		initCommonServerSettings(cChapterCore);
		
		StringBuilder searchCoreUrl = new StringBuilder(solrServer1Url).append("/").append(searchSlaveCoreName);
		cSearchCore = new CommonsHttpSolrServer(searchCoreUrl.toString());
		initCommonServerSettings(cSearchCore);
	}
	
	// calls --> initLoadBalancerServerSettings
	private static void initLoadBalancerServer() throws Exception {		
		StringBuilder searchSlaveCore1Url = new StringBuilder(solrServer1Url).append("/").append(searchSlaveCoreName);
		StringBuilder searchSlaveCore2Url = new StringBuilder(solrServer2Url).append("/").append(searchSlaveCoreName);		
		lbSearchCore = new LBHttpSolrServer(searchSlaveCore1Url.toString(), searchSlaveCore2Url.toString());
		initLoadBalancerServerSettings(lbSearchCore);
		
		StringBuilder bookSlaveCore1Url = new StringBuilder(solrServer1Url).append("/").append(bookCoreName);
		StringBuilder bookSlaveCore2Url = new StringBuilder(solrServer2Url).append("/").append(bookCoreName);
		lbBookCore = new LBHttpSolrServer(bookSlaveCore1Url.toString(), bookSlaveCore2Url.toString());	
		initLoadBalancerServerSettings(lbBookCore);
		
		StringBuilder chapterSlaveCore1Url = new StringBuilder(solrServer1Url).append("/").append(chapterCoreName);
		StringBuilder chapterSlaveCore2Url = new StringBuilder(solrServer2Url).append("/").append(chapterCoreName);
		lbChapterCore = new LBHttpSolrServer(chapterSlaveCore1Url.toString(), chapterSlaveCore2Url.toString());	
		initLoadBalancerServerSettings(lbChapterCore);
	}

	private static void initCommonServerSettings(CommonsHttpSolrServer commonServer) {		
		commonServer.setSoTimeout(Integer.parseInt(cSoTimeout));
		commonServer.setConnectionTimeout(Integer.parseInt(cConnTimeout));
		commonServer.setDefaultMaxConnectionsPerHost(Integer.parseInt(cDefMaxConnPerHost));
		commonServer.setMaxTotalConnections(Integer.parseInt(cMaxTotalConn));
		commonServer.setFollowRedirects(Boolean.parseBoolean(cFollowRedirects));
		commonServer.setAllowCompression(Boolean.parseBoolean(cAllowCompression));
		commonServer.setMaxRetries(Integer.parseInt(cMaxRetries));
	}
	
	private static void initLoadBalancerServerSettings(LBHttpSolrServer lbServer) {		
		lbServer.setAliveCheckInterval(Integer.parseInt(lbAliveCheckInterval));
		lbServer.setConnectionManagerTimeout(Integer.parseInt(lbConnManagerTimeout));
		lbServer.setConnectionTimeout(Integer.parseInt(lbConnTimeout));
		lbServer.setSoTimeout(Integer.parseInt(lbSoTimeout));
	}
	
}
