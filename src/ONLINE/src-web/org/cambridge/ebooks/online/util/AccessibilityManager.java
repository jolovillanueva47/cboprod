package org.cambridge.ebooks.online.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class AccessibilityManager {
	
	private static final Logger LOGGER = Logger.getLogger(AccessibilityManager.class);
	
	private String pagePath;
	private String pageName;
	private String queryString;
	private String queryStringNoKeyword;

	public String getqueryStringNoKeyword() {
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		String accBean_param = (String) request.getAttribute("accBean_param");
		
		if (StringUtils.isNotEmpty(accBean_param)) {
			queryStringNoKeyword = formatQueryNoDoubleKeywords();
		}else{
			queryStringNoKeyword = formatQueryNoKeywords();
		}
		
		return queryStringNoKeyword;
	}

	public void setqueryStringNoKeyword(String queryStringNoKeyword) {
		this.queryStringNoKeyword = queryStringNoKeyword;
	}

	private boolean accessible;
	
	public String getPageName() {
		pageName = formatName();
		return pageName;
	}

	public String getPagePath() {
		pagePath = formatPath();
		return pagePath;
	}
	public void setAccessible(boolean accessible) {
		this.accessible = accessible;
	}
	public boolean isAccessible() {
		return accessible;
	}
	private String formatPath(){
		String path;
		this.setAccessible(false);
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		path = request.getRequestURI();
		
		if(path.contains("aaa/") || path.contains("aaa_")){
			this.setAccessible(true);
		}
		//System.out.println("path: "+path);
		
		path = path.replace("jsp", "jsf");
		path = path + "?";
		return path;
	}
	
	private String formatName(){
		String path;
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		path = request.getRequestURI();
		//System.out.println("name: before "+path);
		path = path.replace("jsp", "jsf");
		path = path + "?";
		
		if(StringUtils.isNotEmpty(path) && path.lastIndexOf('/') >= 0){
			path = path.substring(path.lastIndexOf('/'), path.length());	
		}
			
		path =  path.substring(1,path.length());

		//System.out.println("name: after "+path);
		return path;
	}
	
	public String getQueryString() {
		queryString = formatQuery();
		return queryString;
	}
	public void setQueryString(String query) {
		query = formatQuery();
		this.queryString = query;
	}
	
	private String formatQuery(){
		String query;
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		String [] tempquery;
		String paramName, lastPageTab="", finalQuery="";
		
		try {			
			query = request.getQueryString();
			if(StringUtil.isNotEmpty(query)){ 
				tempquery = query.split("&");
				
				int len = tempquery.length;
				
				for(int x = 0 ; x < len; x++){
					if( tempquery[x].indexOf("=") > 0){
						paramName = tempquery[x].substring(0, tempquery[x].indexOf("="));
						//System.out.println("paramName = "+paramName);
						if (! (paramName.equalsIgnoreCase("stylez") ||
							paramName.equalsIgnoreCase("pageTab") || 
							paramName.equalsIgnoreCase("HideDetails"))) 
						{
							finalQuery = finalQuery + "&" + tempquery[x];
						}
						else if (paramName.equalsIgnoreCase("pageTab")) {
							lastPageTab = tempquery[x];
						}
					}
				}
			}
			
			if(StringUtil.isNotEmpty(lastPageTab)){
				finalQuery = finalQuery + "&" + lastPageTab;
			}
			
			finalQuery = finalQuery + "&";			
			finalQuery = finalQuery.charAt(0) == '&' ? finalQuery.substring(1,finalQuery.length()) : finalQuery;			
			//System.out.println("finalquery="+finalQuery);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return finalQuery;
	}	
	
	private String formatQueryNoKeywords(){
		String query;
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		String [] tempquery;
		String paramName, finalQuery="";
		
		try {			
			query = request.getQueryString();
			if(StringUtil.isNotEmpty(query)){ 
				tempquery = query.split("&");
				
				int len = tempquery.length;
				
				for(int x = 0 ; x < len; x++){
					if( tempquery[x].indexOf("=") > 0){
						paramName = tempquery[x].substring(0, tempquery[x].indexOf("="));
						//System.out.println("paramName = "+paramName);
						if (! (paramName.equalsIgnoreCase("stylez") ||
							paramName.equalsIgnoreCase("pageTab") || 
							paramName.equalsIgnoreCase("HideDetails"))) 
						{
							finalQuery = finalQuery + "&" + tempquery[x];
						}
					}
				}
			}
			
			finalQuery = finalQuery + "&";			
			finalQuery = finalQuery.charAt(0) == '&' ? finalQuery.substring(1,finalQuery.length()) : finalQuery;			
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return finalQuery;
	}
	
	private String formatQueryNoDoubleKeywords(){
		String query;
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		
		String [] tempquery;
		String paramName, finalQuery="";
		
		try {			
			query = request.getQueryString();
			

			//<c:set var="accBean_param" value="" scope="request" /> FORCE PARAMS
			String accBean_param = (String) request.getAttribute("accBean_param");
			
			if(StringUtils.isNotEmpty(accBean_param)){
				LOGGER.info("accBean_param IS FOUND");
				request.removeAttribute("accBean_param");
				query = query + accBean_param;
			}

			if(StringUtil.isNotEmpty(query)){ 
				tempquery = query.split("&");
				
				tempquery = removeDoubles(tempquery);
				
				int len = tempquery.length;
				
				
				for(int x = 0 ; x < len; x++){
					if( tempquery[x].indexOf("=") > 0){

						paramName = tempquery[x].substring(0, tempquery[x].indexOf("="));
						
						finalQuery = finalQuery + "&" + tempquery[x];
		
					}
				}
				
			}
			
			finalQuery = finalQuery + "&";			
			finalQuery = finalQuery.charAt(0) == '&' ? finalQuery.substring(1,finalQuery.length()) : finalQuery;
			
			if(StringUtils.isNotEmpty(finalQuery)){
				finalQuery = finalQuery.charAt(finalQuery.length()-1) == '&' ? finalQuery.substring(0,finalQuery.length()-1) : finalQuery;
			}
			
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return finalQuery;
	}	
	
	private String[] removeDoubles(String[] query) {
			
		String paramName;
		String paramValue;
		String finString = "";
		
		HashMap<String, String> mKeywords2 = new LinkedHashMap<String, String>();

		int len = query.length;

		for(int x = 0 ; x < len; x++){
			if( query[x].indexOf("=") > 0){

				paramName = query[x].substring(0, query[x].indexOf("="));
				paramValue = query[x].substring(query[x].indexOf("=")+1, query[x].length());
				
				if(mKeywords2.containsKey(paramName)){
					mKeywords2.remove(paramName);
				}
				
				mKeywords2.put(paramName, paramValue);
				
			}
		}
		
	
		for (Entry<?, ?> entry : mKeywords2.entrySet()) {
			
			String key = entry.getKey().toString();
		    String value = (String) entry.getValue();
		
		    finString = finString + key + "=" + value + "&";
		}
		
		return finString.split("&");
	}
	
	
	
}
