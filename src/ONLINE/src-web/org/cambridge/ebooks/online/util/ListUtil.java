package org.cambridge.ebooks.online.util;

import java.util.List;

/**
 * 
 * @author rvillamor
 *
 */

public class ListUtil {
	
	public static <T> T searchObject(final List<T> list, final T obj ) {
		T result = null;
		int index = list.indexOf(obj);
		if ( index > -1 ) { 
			result = list.get(index);
		}
		return result;
	}
	
	public static boolean notNullAndEmpty(List list) { 
		return list != null && !list.isEmpty();
	}

}
