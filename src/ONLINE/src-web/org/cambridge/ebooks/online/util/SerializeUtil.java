package org.cambridge.ebooks.online.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class SerializeUtil {
	
	public static final String FILE_PATH = "FILE_PATH";
	
	public static final String ATTR_NAME = "ATTR_NAME";
	public static void serialize(Object o, String filepath) throws Exception{		
		//use buffering
		OutputStream file = new FileOutputStream(filepath);
		OutputStream buffer = new BufferedOutputStream(file);
		ObjectOutput output = new ObjectOutputStream(buffer);
		try{
		   	output.writeObject(o);
		}
		finally{
		    output.close();
		}		
	}
	
	public static Object deserialize(String filepath) throws Exception{	
		InputStream file = new FileInputStream(filepath);
	    InputStream buffer = new BufferedInputStream(file);
	    ObjectInput input = new ObjectInputStream(buffer);
	    Object o;
	    try{
		    //deserialize the List
		    o = input.readObject();	    
	    }
	    finally{
	    	input.close();
	    }
	    return o;
	}
	
	public static void sendSerializeInfo(String path, String attrName, String servletLink) throws IOException{
		String[] params = new String[]{FILE_PATH,ATTR_NAME};
		String[] values = new String[]{path, attrName};
		HttpUtil.connectTo(servletLink, params, values);
	}
}
