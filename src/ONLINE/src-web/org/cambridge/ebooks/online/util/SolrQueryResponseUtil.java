package org.cambridge.ebooks.online.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.solr.client.solrj.response.GroupField;
import org.apache.solr.client.solrj.response.Grouped;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.cambridge.ebooks.online.dto.solr.BookAndChapterSolrDto;
import org.cambridge.ebooks.online.dto.solr.SearchSolrDto;

/**
 * @author kmulingtapang
 */
public class SolrQueryResponseUtil {

	public static SearchSolrDto getSearchSolrDto(SolrDocument solrDocument) {
		String id = getFieldValueAsString(solrDocument, "id");
		String flag = getFieldValueAsString(solrDocument, "flag");
		String contentType = getFieldValueAsString(solrDocument, "content_type");
		String clusterId = getFieldValueAsString(solrDocument, "cluster_id");
		String bookId = getFieldValueAsString(solrDocument, "book_id");
		String publisherId = getFieldValueAsString(solrDocument, "publisher_id");
		String publisherCode = getFieldValueAsString(solrDocument, "publisher_code");
		String publisherName = getFieldValueAsString(solrDocument, "publisher_name");
		String productCode = getFieldValueAsString(solrDocument, "product_code");
		String productName = getFieldValueAsString(solrDocument, "product_name");
		String journalId = getFieldValueAsString(solrDocument, "journal_id");
		String mainTitle = getFieldValueAsString(solrDocument, "main_title");
		String mainTitleAlphasort = getFieldValueAsString(solrDocument, "main_title_alphasort");
		String mainSubtitle = getFieldValueAsString(solrDocument, "main_subtitle");
		String printDateDisplay = getFieldValueAsString(solrDocument, "print_date_display");
		String printDate = getFieldValueAsString(solrDocument, "print_date");
		String onlineDateDisplay = getFieldValueAsString(solrDocument, "online_date_display");
		String onlineDate = getFieldValueAsString(solrDocument, "online_date");
		String clcDateDisplay = getFieldValueAsString(solrDocument, "clc_date_display");
		String clcDate = getFieldValueAsString(solrDocument, "clc_date");
		String edition = getFieldValueAsString(solrDocument, "edition");
		String editionNumber = getFieldValueAsString(solrDocument, "edition_number");
		String volumeNumber = getFieldValueAsString(solrDocument, "volume_number");
		String volumeTitle = getFieldValueAsString(solrDocument, "volume_title");
		String partNumber = getFieldValueAsString(solrDocument, "part_number");
		String partTitle = getFieldValueAsString(solrDocument, "part_title");
		String seriesCode = getFieldValueAsString(solrDocument, "series_code");
		String series = getFieldValueAsString(solrDocument, "series");
		String seriesFacet = getFieldValueAsString(solrDocument, "series_facet");
		String seriesNumber = getFieldValueAsString(solrDocument, "series_number");
		String bookDoi = getFieldValueAsString(solrDocument, "book_doi");
		String doi = getFieldValueAsString(solrDocument, "doi");
		String altDoi = getFieldValueAsString(solrDocument, "alt_doi");
		String isbnIssn = getFieldValueAsString(solrDocument, "isbn_issn");
		String altIsbnPaperback = getFieldValueAsString(solrDocument, "alt_isbn_paperback");
		String altIsbnHardback = getFieldValueAsString(solrDocument, "alt_isbn_hardback");
		String altEisbnEissn = getFieldValueAsString(solrDocument, "alt_eisbn_eissn");
		String altIsbnOther = getFieldValueAsString(solrDocument, "alt_isbn_other");
		String authorName = getFieldValueAsString(solrDocument, "author_name");
		String authorAlphasort = getFieldValueAsString(solrDocument, "author_alphasort");
		String authorAffiliation = getFieldValueAsString(solrDocument, "author_affiliation");
		String authorRole = getFieldValueAsString(solrDocument, "author_role");
		String authorPosition = getFieldValueAsString(solrDocument, "author_position");
		String authorSingleline = getFieldValueAsString(solrDocument, "author_singleline");	
		
		List<String> subjectCodeList = getFieldValueAsList(solrDocument, "subject_code");		
		List<String> subjectLevelList = getFieldValueAsList(solrDocument, "subject_level");		
		List<String> subjectList = getFieldValueAsList(solrDocument, "subject");		
		List<String> subjectFacet = getFieldValueAsList(solrDocument, "subject_facet");
			
		// Content Items
		String parentId = getFieldValueAsString(solrDocument, "parent_id");
		String title = getFieldValueAsString(solrDocument, "title");
		String titleAlphasort = getFieldValueAsString(solrDocument, "title_alphasort");
		String subtitle = getFieldValueAsString(solrDocument, "subtitle");
		String label = getFieldValueAsString(solrDocument, "label");
		String pageStart = getFieldValueAsString(solrDocument, "page_start");
		String pageEnd = getFieldValueAsString(solrDocument, "page_end");
		String type = getFieldValueAsString(solrDocument, "type");
		String position = getFieldValueAsString(solrDocument, "position");
		String contributorName = getFieldValueAsString(solrDocument, "contributor_name");
		String contributorAlphasort = getFieldValueAsString(solrDocument, "contributor_alphasort");
		String contributorAffiliation = getFieldValueAsString(solrDocument, "contributor_affiliation");
		String pdfFilename = getFieldValueAsString(solrDocument, "pdf_filename");
		String chapterFulltext = getFieldValueAsString(solrDocument, "chapter_fulltext");


		List<String> keywordTextList = getFieldValueAsList(solrDocument, "keyword_text");			
		List<String> keywordNormsList = getFieldValueAsList(solrDocument, "keyword_norms");	
		List<String> price = getFieldValueAsList(solrDocument, "price");	

		String articleFulltext = getFieldValueAsString(solrDocument, "article_fulltext");
		String issueNumber = getFieldValueAsString(solrDocument, "issue_number");
		String abstractText = getFieldValueAsString(solrDocument, "abstract");
		

		// Facet
		String bookTitleFacet = getFieldValueAsString(solrDocument, "book_title_facet");
		String journalTitleFacet = getFieldValueAsString(solrDocument, "journal_title_facet");
		List<String> authorNameFacetList =  getFieldValueAsList(solrDocument, "author_name_facet");	
		
		
		// DTO
		SearchSolrDto searchDto = new SearchSolrDto();
		searchDto.setId(id);
		searchDto.setFlag(flag);
		searchDto.setContentType(contentType);
		searchDto.setClusterId(clusterId);
		searchDto.setBookId(bookId);
		searchDto.setPublisherId(publisherId);
//		searchDto.setPublisherCode(publisherCode);
//		searchDto.setPublisherName(publisherName);
//		searchDto.setProductCode(productCode);
//		searchDto.setProductName(productName);
		searchDto.setJournalId(journalId);
		searchDto.setMainTitle(mainTitle);
		searchDto.setMainTitleAlphasort(mainTitleAlphasort);
		searchDto.setMainSubtitle(mainSubtitle);
		searchDto.setPrintDateDisplay(printDateDisplay);
		searchDto.setPrintDate(printDate);
		searchDto.setOnlineDateDisplay(onlineDateDisplay);
		searchDto.setOnlineDate(onlineDate);
//		searchDto.setClcDateDisplay(clcDateDisplay);
//		searchDto.setClcDate(clcDate);
		searchDto.setEdition(edition);
		searchDto.setEditionNumber(editionNumber);
		searchDto.setVolumeNumber(volumeNumber);
		searchDto.setVolumeTitle(volumeTitle);
		searchDto.setPartNumber(partNumber);
		searchDto.setPartTitle(partTitle);
		searchDto.setSeriesCode(seriesCode);
		searchDto.setSeries(series);
		searchDto.setSeriesFacet(seriesFacet);
		searchDto.setSeriesNumber(seriesNumber);
		searchDto.setBookDoi(bookDoi);
		searchDto.setDoi(doi);
		searchDto.setAltDoi(altDoi);
		searchDto.setIsbnIssn(isbnIssn);
		searchDto.setAltIsbnPaperback(altIsbnPaperback);
		searchDto.setAltIsbnHardback(altIsbnHardback);
		searchDto.setAltEisbnEissn(altEisbnEissn);
		searchDto.setAltIsbnOther(altIsbnOther);
		searchDto.setAuthorName(authorName);
		searchDto.setAuthorAlphasort(authorAlphasort);
		searchDto.setAuthorAffiliation(authorAffiliation);
		searchDto.setAuthorRole(authorRole);
		searchDto.setAuthorPosition(authorPosition);
		searchDto.setAuthorSingleline(authorSingleline);
		searchDto.setSubjectCodeList(subjectCodeList);
		searchDto.setSubjectLevelList(subjectLevelList);
		searchDto.setSubjectList(subjectList);
		searchDto.setSubjectFacet(subjectFacet);
		
		// Content Items
		searchDto.setParentId(parentId);
		searchDto.setTitle(title);
		searchDto.setTitleAlphasort(titleAlphasort);
		searchDto.setSubtitle(subtitle);
		searchDto.setLabel(label);
		searchDto.setPageStart(pageStart);
		searchDto.setPageEnd(pageEnd);
		searchDto.setType(type);
		searchDto.setPosition(position);
		searchDto.setContributorName(contributorName);
		searchDto.setContributorAlphasort(contributorAlphasort);
		searchDto.setContributorAffiliation(contributorAffiliation);
		searchDto.setPdfFilename(pdfFilename);
		searchDto.setChapterFulltext(chapterFulltext);
		searchDto.setKeywordTextList(keywordTextList);
		searchDto.setKeywordNormsList(keywordNormsList);
		searchDto.setPrice(price);
		searchDto.setArticleFulltext(articleFulltext);
		searchDto.setIssueNumber(issueNumber);
		searchDto.setAbstractText(abstractText);
		searchDto.setBookTitleFacet(bookTitleFacet);
		searchDto.setJournalTitleFacet(journalTitleFacet);
		searchDto.setAuthorNameFacetList(authorNameFacetList);
		
		return searchDto;
	}
	
	public static BookAndChapterSolrDto getBookAndChapterSolrDto(SolrDocument solrDocument) {
		String id = getFieldValueAsString(solrDocument, "id");
		String flag = getFieldValueAsString(solrDocument, "flag");
		String contentType = getFieldValueAsString(solrDocument, "content_type");
		String title = getFieldValueAsString(solrDocument, "title");
		String titleAlphasort = getFieldValueAsString(solrDocument, "title_alphasort");
		String subtitle = getFieldValueAsString(solrDocument, "subtitle");
		String series = getFieldValueAsString(solrDocument, "series");
		String seriesAlphasort = getFieldValueAsString(solrDocument, "series_alphasort");
		int seriesNumber = getFieldValueAsInt(solrDocument, "series_number");
		String seriesCode = getFieldValueAsString(solrDocument, "series_code");
		String printDateDisplay = getFieldValueAsString(solrDocument, "print_date_display");
		String printDate = getFieldValueAsString(solrDocument, "print_date");
		String onlineDateDisplay = getFieldValueAsString(solrDocument, "online_date_display");
		String onlineDate = getFieldValueAsString(solrDocument, "online_date");
		String clcDateDisplay = getFieldValueAsString(solrDocument, "clc_date_display");
		String clcDate = getFieldValueAsString(solrDocument, "clc_date");
		String doi = getFieldValueAsString(solrDocument, "doi");
		String altDoi = getFieldValueAsString(solrDocument, "alt_doi");
		String isbn = getFieldValueAsString(solrDocument, "isbn");
		String altIsbnPaperback = getFieldValueAsString(solrDocument, "alt_isbn_paperback");
		String altIsbnHardback = getFieldValueAsString(solrDocument, "alt_isbn_hardback");
		String altIsbnEisbn = getFieldValueAsString(solrDocument, "alt_isbn_eisbn");
		String altIsbnOther = getFieldValueAsString(solrDocument, "alt_isbn_other");
		String volumeNumber = getFieldValueAsString(solrDocument, "volume_number");
		String volumeTitle = getFieldValueAsString(solrDocument, "volume_title");
		String partNumber = getFieldValueAsString(solrDocument, "part_number");
		String partTitle = getFieldValueAsString(solrDocument, "part_title");
		String edition = getFieldValueAsString(solrDocument, "edition");
		String editionNumber = getFieldValueAsString(solrDocument, "edition_number");
		
		List<String> coverImageTypeList = getFieldValueAsList(solrDocument, "cover_image_type");		
		List<String> coverImageFilenameList = getFieldValueAsList(solrDocument, "cover_image_filename");		
		List<String> authorNameList = getFieldValueAsList(solrDocument, "author_name");		
		List<String> authorNameLfList = getFieldValueAsList(solrDocument, "author_name_lf");		
		
		String authorNameAlphasort = getFieldValueAsString(solrDocument, "author_name_alphasort");
		
		List<String> authorAffiliationList = getFieldValueAsList(solrDocument, "author_affiliation");
		List<String> authorRoleList = getFieldValueAsList(solrDocument, "author_role");
		List<String> authorPositionList = getFieldValueAsList(solrDocument, "author_position");
		
		String authorSingleline = getFieldValueAsString(solrDocument, "author_singleline");	
		
		List<String> subjectList = getFieldValueAsList(solrDocument, "subject");
		List<String> subjectLevelList = getFieldValueAsList(solrDocument, "subject_level");
		List<String> subjectCodeList = getFieldValueAsList(solrDocument, "subject_code");
		
		String bookId = getFieldValueAsString(solrDocument, "book_id");	
		String publisherId = getFieldValueAsString(solrDocument, "publisher_id");	
		String publisherCode = getFieldValueAsString(solrDocument, "publisher_code");
		String publisherName = getFieldValueAsString(solrDocument, "publisher_name");	
		String publisherLoc = getFieldValueAsString(solrDocument, "publisher_loc");
		String productCode = getFieldValueAsString(solrDocument, "product_code");
		String productName = getFieldValueAsString(solrDocument, "product_name");
		
		List<String> copyrightStatementList = getFieldValueAsList(solrDocument, "copyright_statement");
		
		String parentId = getFieldValueAsString(solrDocument, "parent_id");	
		String pageStart = getFieldValueAsString(solrDocument, "page_start");	
		String pageEnd = getFieldValueAsString(solrDocument, "page_end");	
		String type = getFieldValueAsString(solrDocument, "type");	
		String position = getFieldValueAsString(solrDocument, "position");	
		String label = getFieldValueAsString(solrDocument, "label");	
		String pdfFilename = getFieldValueAsString(solrDocument, "pdf_filename");	
		String abstractFilename = getFieldValueAsString(solrDocument, "abstract_alt_filename");	
		String abstractProblem = getFieldValueAsString(solrDocument, "abstract_problem");	
		String chapterId = getFieldValueAsString(solrDocument, "chapter_id");	
		String tocText = getFieldValueAsString(solrDocument, "toc_text");	
		
		List<String> keywordTextList = getFieldValueAsList(solrDocument, "keyword_text");
		
		String abstractText = getFieldValueAsString(solrDocument, "abstract_text");
		String blurb = getFieldValueAsString(solrDocument, "blurb");	
		
		BookAndChapterSolrDto dto = new BookAndChapterSolrDto();
		dto.setId(id);		
		dto.setFlag(flag);
		dto.setContentType(contentType);
		dto.setTitle(title);
		dto.setTitleAlphasort(titleAlphasort);
		dto.setSubtitle(subtitle);
		dto.setSeries(series);
		dto.setSeriesAlphasort(seriesAlphasort);
		dto.setSeriesNumber(seriesNumber);
		dto.setSeriesCode(seriesCode);
		dto.setPrintDateDisplay(printDateDisplay);
		dto.setPrintDate(printDate);
		dto.setOnlineDateDisplay(onlineDateDisplay);
		dto.setOnlineDate(onlineDate);
//		dto.setClcDateDisplay(clcDateDisplay);
//		dto.setClcDate(clcDate);
		dto.setDoi(doi);
		dto.setAltDoi(altDoi);
		dto.setIsbn(isbn);
		dto.setAltIsbnPaperback(altIsbnPaperback);
		dto.setAltIsbnHardback(altIsbnHardback);
		dto.setAltIsbnEisbn(altIsbnEisbn);
		dto.setAltIsbnOther(altIsbnOther);
		dto.setVolumeNumber(volumeNumber);
		dto.setVolumeTitle(volumeTitle);
		dto.setPartNumber(partNumber);
		dto.setPartTitle(partTitle);
		dto.setEdition(edition);
		dto.setEditionNumber(editionNumber);
		dto.setCoverImageTypeList(coverImageTypeList);
		dto.setCoverImageFilenameList(coverImageFilenameList);
		dto.setAuthorNameList(authorNameList);
		dto.setAuthorNameLfList(authorNameLfList);
		dto.setAuthorNameAlphasort(authorNameAlphasort);
		dto.setAuthorAffiliationList(authorAffiliationList);
		dto.setAuthorRoleList(authorRoleList);
		dto.setAuthorPositionList(authorPositionList);
		dto.setAuthorSingleline(authorSingleline);
		dto.setSubjectList(subjectList);
		dto.setSubjectLevelList(subjectLevelList);
		dto.setSubjectCodeList(subjectCodeList);
		dto.setBookId(bookId);
		dto.setPublisherId(publisherId);
//		dto.setPublisherCode(publisherCode);
		dto.setPublisherName(publisherName);
		dto.setPublisherLoc(publisherLoc);
//		dto.setProductCode(productCode);
//		dto.setProductName(productName);
		dto.setCopyrightStatementList(copyrightStatementList);
		dto.setParentId(parentId);
		dto.setPageStart(pageStart);
		dto.setPageEnd(pageEnd);
		dto.setType(type);
		dto.setPosition(position);
		dto.setLabel(label);
		dto.setPdfFilename(pdfFilename);
		dto.setAbstractFilename(abstractFilename);
		dto.setAbstractProblem(abstractProblem);
		dto.setChapterId(chapterId);
		dto.setTocText(tocText);
		dto.setKeywordTextList(keywordTextList);
		dto.setAbstractText(abstractText);
		dto.setBlurb(blurb);
		
		return dto;		
	}
	
	public static String getFieldValueAsString(SolrDocument solrDocument, String fieldName) {
		return solrDocument.getFieldValue(fieldName) == null ? "" : (String)solrDocument.getFieldValue(fieldName);
	}
	
	public static int getFieldValueAsInt(SolrDocument solrDocument, String fieldName) {
		return solrDocument.getFieldValue(fieldName) == null ? 0 : ((Integer)solrDocument.getFieldValue(fieldName)).intValue();
	}
	
	public static List<String> getFieldValueAsList(SolrDocument solrDocument, String fieldName) {
		Collection<Object> collection = solrDocument.getFieldValues(fieldName);
		List<String> list = new ArrayList<String>();
		if(null != collection) {
			for(Object obj : collection) {
				if(null != obj) {
					list.add((String)obj);
				}
			}
		}
		
		return list;
	}
	
	public static int getTotalGroups(QueryResponse response) {
		GroupField groupField = getGroups(response);
		int totalGroups = groupField == null ? 0 : groupField.getNGroups();		
		
		return totalGroups;
	}
	
	public static int getTotalDocuments(QueryResponse response) {
		GroupField groupField = getGroups(response);
		int totalDocuments = groupField == null ? 0 : groupField.getMatches();			
		
		return totalDocuments;
	}
	
	public static GroupField getGroups(QueryResponse response) {
		Grouped groupedResults = response.getGrouped();
		GroupField groupField = groupedResults == null ? null : groupedResults.get(0);		
		
		return groupField;
	}
}
