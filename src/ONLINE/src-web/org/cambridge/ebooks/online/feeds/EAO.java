package org.cambridge.ebooks.online.feeds;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EAO {
	
	private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("eBooksService");
	
	public static EntityManager createEntityManager(){
		return EMF.createEntityManager();
	}
	
	public static void closeEntityManager(EntityManager em){
		if(em != null && em.isOpen())
			em.close();
	}
	
	public static EntityManagerFactory getEntityManagerFactory(){
		return EMF;
	}
}
