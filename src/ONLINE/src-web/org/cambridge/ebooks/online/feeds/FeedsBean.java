package org.cambridge.ebooks.online.feeds;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cambridge.ebooks.online.jpa.common.SubjectArea;

public class FeedsBean {
	
	private static final File FEEDS_DIR = new File(System.getProperty("feeds.rss"));
	private List<String> files = new ArrayList<String>();
	private static final Map<String, String> fileMap = new HashMap<String, String>();
	private List<SubjectArea> subjectHierarchy;
	
	public FeedsBean(){
		if(FEEDS_DIR != null && FEEDS_DIR.list() != null && FEEDS_DIR.list().length > 0)
		{
			files = Arrays.asList(FEEDS_DIR.list());
			for(String subjectId : files)
			{
				subjectId = subjectId.replace("feed_", "").replace("_rss_2.0.xml", "");
				fileMap.put(subjectId, subjectId);
			}
		}
	}

	
	private FeedsWorker worker = new FeedsWorker();
	
	public List<SubjectArea> getInitSubjectHierarchy(){
		subjectHierarchy = worker.subjectHierarchy();
		return null;
	}
	
	public List<SubjectArea> getSubjectHierarchy() {
		return subjectHierarchy;
	}

	public void setSubjectHierarchy(List<SubjectArea> subjectHierarchy) {
		this.subjectHierarchy = subjectHierarchy;
	}
	
	public String getRssFeedList(){
		return (files != null && !files.isEmpty()) ? files.toString() : "";
	}
	
	public Map<String, String> getContains(){
		return fileMap;
	}
}
