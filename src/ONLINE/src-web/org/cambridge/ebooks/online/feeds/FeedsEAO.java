package org.cambridge.ebooks.online.feeds;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cambridge.ebooks.online.jpa.common.SubjectArea;


public class FeedsEAO extends EAO{
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> query(T type, String q, Object... params) { 
		List<T> result = null;
		EntityManager em = createEntityManager();
		
		try
		{
			Query query = em.createNativeQuery(q, type.getClass());
	        if (params != null && params.length > 0) 
	        { 
	        	int ctr = 1;
		        for (Object param : params)  
		        	query.setParameter( ctr++, param); 
	        }
	        
	        result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + FeedsEAO.class + " query(T, String, String...) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return  result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> query(T type, String q){
		List<T> result = null;
		EntityManager em = createEntityManager();
		try
		{
			Query query = em.createNativeQuery(q, type.getClass());
			result = (List<T>)query.getResultList();
		}
		catch(Throwable t)
		{
			System.err.println("[ERROR] " + FeedsEAO.class + " query(T, String) " + t.getMessage());
		}
		finally
		{
			closeEntityManager(em);
		}
		
		return result;
	}
	
	public static List<SubjectArea> subjectAreas(){

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT  esh.* ");
		sql.append("FROM " );
		sql.append("  (SELECT path, subject_id, COUNT(*)-1 AS SERIES_COUNT ");
	    sql.append("   FROM  " );
	    sql.append("    (SELECT DISTINCT SYS_CONNECT_BY_PATH(hie.subject_id,':') path, ");
	    sql.append("            hie.subject_id, hie.subject_name, hie.subject_level, ");
	    sql.append("            hie.parent_id, hie.vista_subject_code, hie.final_level, ");
	    sql.append("            hie.title_count, hie.active_subject, load.series_code ");
	    sql.append("     FROM (ebook_subject_hierarchy hie ");
	    sql.append("     LEFT JOIN ebook_subject_isbn isbn ");
	    sql.append("     ON hie.subject_id = isbn.subject_id) ");
	    sql.append("     LEFT JOIN ebook_isbn_data_load load ");
	    sql.append("     ON load.isbn = isbn.isbn ");
	    sql.append("     WHERE active_subject = 'Y' ");
	    sql.append("     CONNECT BY PRIOR hie.subject_id = hie.parent_id ");
	    sql.append("     START WITH hie.subject_level = 1)  ");
	    sql.append("   GROUP BY path, subject_id ) res, ebook_subject_hierarchy esh ");
	    sql.append("WHERE res.subject_id = esh.subject_id ");
	    sql.append("ORDER BY esh.subject_id");
		
		return query(new SubjectArea(), sql.toString());
	}
	
	
}
