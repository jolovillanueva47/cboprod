/**
 * 
 */
package org.cambridge.ebooks.online.dto.viewaccess;

/**
 * @author kmulingtapang
 *
 */
public class OrganisationDetails {
	private String bodyId;
	private String displayName;
	private String messageText;
	
	public String getBodyId() {
		return bodyId;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}	
}
