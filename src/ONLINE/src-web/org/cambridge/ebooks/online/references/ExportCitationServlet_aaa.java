package org.cambridge.ebooks.online.references;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.MailManager;

/**
 * @author apirante
 * 
 */
public class ExportCitationServlet_aaa extends ExportCitationServlet {
	
//	private static final long serialVersionUID = 1L;
//	
//	private static final String TEMPLATE_DIR 
//		= System.getProperty("templates.dir").trim();
//	private static final String TEMPLATE_FILE 
//		= System.getProperty("mail.export.citation.template").trim();
//	
	
	private static final long serialVersionUID = -1390940171614601417L;
	
	private static final Logger LOGGER = Logger.getLogger(ExportCitationServlet_aaa.class);
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info(" == START EXPORTCITATION AAA == ");
		String isReset = req.getParameter("reset_aaa");
		
		if(StringUtils.isNotEmpty(isReset)){
			try {
				//this is to ensure that info for the book is retained.
				StringBuilder prevParams = new StringBuilder();	
				prevParams = recreateParams(req);
				resp.sendRedirect(CONTEXT_PATH+"aaa/popups/export_citation.jsf?email=reset"+prevParams.toString());
			} catch (IOException e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
				//e.printStackTrace();
			}
		}else{
			super.doPost(req, resp);
		}
		
		LOGGER.info(" == END EXPORTCITATION AAA == ");
	}
	
	@Override
	protected void sendEmail(HttpServletRequest req, HttpServletResponse resp,
			String recipient, String attachment) {
		
		String successful = "success";

		LOGGER.info("Send Email...");
		
		try {						
			StringBuilder body = new StringBuilder();					 
			
			body.append("Attached is a citation from Cambridge Books Online that you, or a colleague of yours, requested to be sent to you.<br />");
			body.append("Visit Cambridge Books Online to freely browse and view tables of contents.<br />");
			body.append("Access to the full text is available to users whose institutions have purchased access.<br />");
			body.append("For further information or to unsubscribe, go to http://ebooks.cambridge.org<br /><br />");		
			
//			MailManager.storeMail(TEMPLATE_DIR, 
//								  TEMPLATE_FILE, 
//								  OUTPUT_DIR, 
//								  EXPORT_FROM_EMAIL, 
//								  new String[]{recipient}, 
//								  null, 
//								  null, 
//								  EXPORT_SUBJECT, 
//								  attachment,
//								  body.toString(),
//								  req.getSession().getServletContext());	// this line is jsfs
//			
			LOGGER.info("  TEMPLATE_DIR="+TEMPLATE_DIR);
			LOGGER.info("  TEMPLATE_FILE="+TEMPLATE_FILE);
			
						MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  EXPORT_FROM_EMAIL, 		//from:	user Email
								  new String[]{recipient},	//to:	recipient Email 
								  null, 					//cc:
								  null,						//bcc:	email, 
								  EXPORT_SUBJECT, 			//subject:
								  attachment,				//attachment (cant use old storemail method w/o the attachment
								  body.toString());			//messageBody
			
						
		LOGGER.info("Email Sent.");
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			//e.printStackTrace();
			successful = "fail";
		} 		
		
		try {
			
			//this is to ensure that info for the book is retained.
			StringBuilder prevParams = new StringBuilder();	
			
			prevParams = recreateParams(req);
			LOGGER.info("redirecting...");
			resp.sendRedirect(CONTEXT_PATH+"aaa/popups/export_citation.jsf?email=" + successful+prevParams.toString());
			//resp.sendRedirect("../../aaa/popups/export_citation.jsf?email=" + successful+prevParams.toString());
			//resp.sendRedirect("../../aaa/popups/export_citation.jsf?email=" + successful);
		} catch (IOException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			//e.printStackTrace();
		}
	}
	
	private StringBuilder recreateParams(HttpServletRequest req) {
		
		StringBuilder prevParams = new StringBuilder();	
		
		//these params should be checked if needed in the redirect page
		prevParams.append("&type="+req.getParameter("type"));
		prevParams.append("&emailAdd="+req.getParameter("emailAdd"));
		prevParams.append("&fileFormat="+req.getParameter("fileFormat"));
		
		prevParams.append("&id="+req.getParameter("id"));
		prevParams.append("&includeAbstract="+req.getParameter("includeAbstract"));
		prevParams.append("&author="+req.getParameter("author"));			
		prevParams.append("&contributor="+req.getParameter("contributor"));
		prevParams.append("&bookTitle="+req.getParameter("bookTitle"));
		prevParams.append("&contentTitle="+req.getParameter("contentTitle"));
		prevParams.append("&printDate="+req.getParameter("printDate"));
		prevParams.append("&doi="+req.getParameter("doi"));
		prevParams.append("&bookId="+req.getParameter("bookId"));
		prevParams.append("&contentId="+req.getParameter("contentId"));
		
		return prevParams;

	}
	

}
