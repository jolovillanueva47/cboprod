package org.cambridge.ebooks.online.references;

/**
 * @author Karlson A. Mulingtapang
 * Citation.java - Used by ExportCitationServlet
 */
public class Citation {

	private String includeAbstract;
	private String author;
	private String bookTitle;
	private String contentTitle;
	private String printDate;
	private String doi;
	private String bookId;
	private String contentId;
	private String bookBlurb;
	private String contributor;
	private String url;
	
	/**
	 * @return the includeAbstract
	 */
	public String getIncludeAbstract() {
		return includeAbstract;
	}
	/**
	 * @param includeAbstract the includeAbstract to set
	 */
	public void setIncludeAbstract(String includeAbstract) {
		this.includeAbstract = includeAbstract;
	}
	
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the bookTitle
	 */
	public String getBookTitle() {
		return bookTitle;
	}
	/**
	 * @param bookTitle the bookTitle to set
	 */
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	/**
	 * @return the contentTitle
	 */
	public String getContentTitle() {
		return contentTitle;
	}
	/**
	 * @param contentTitle the contentTitle to set
	 */
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}
	/**
	 * @return the printDate
	 */
	public String getPrintDate() {
		return printDate;
	}
	/**
	 * @param printDate the printDate to set
	 */
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}
	/**
	 * @param doi the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}
	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}
	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	/**
	 * @return the contentId
	 */
	public String getContentId() {
		return contentId;
	}
	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	/**
	 * @return the bookBlurb
	 */
	public String getBookBlurb() {
		return bookBlurb;
	}
	/**
	 * @param bookBlurb the bookBlurb to set
	 */
	public void setBookBlurb(String bookBlurb) {
		this.bookBlurb = bookBlurb;
	}
	/**
	 * @return the contributor
	 */
	public String getContributor() {
		return contributor;
	}
	/**
	 * @param contributor the contributor to set
	 */
	public void setContributor(String contributor) {
		this.contributor = contributor;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
