package org.cambridge.ebooks.online.references;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.EBooksConfiguration;
import org.cambridge.ebooks.online.util.EBooksUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.MailManager;

/**
 * @author Karlson A. Mulingtapang
 * ExportCitationServlet.java - Export Citation
 */
public class ExportCitationServlet extends HttpServlet {

	private static final long serialVersionUID = 4554934194072619355L;

	private static final Logger LOGGER = Logger.getLogger(ExportCitationServlet.class);
	
//	private static final String CONTENT_DIR = System.getProperty("content.dir").trim();
	protected static final String EXPORT_FROM_EMAIL = System.getProperty("do.not.reply.email");
	protected static final String EXPORT_SUBJECT = "Cambridge Books Online: Citation";
	protected static final String TEMPLATE_DIR 
		= System.getProperty("templates.dir").trim();
	protected static final String TEMPLATE_FILE 
		= System.getProperty("mail.export.citation.template").trim();
	protected static final String OUTPUT_DIR = System.getProperty("mail.store.path").trim();
	protected static final String CONTEXT_PATH = System.getProperty("ebooks.context.path").trim();
	protected static final String BOOK_LANDING_INDEX_DIR = EBooksConfiguration.getProperty("book.landing.index.dir");
	protected static final String APP_EBOOKS_PATH = System.getProperty("export.citation.temp").trim();
	protected static final String CURRENT_DATE_FORMAT = "dd MMMMM yyyy";
	
	private String dirContent = System.getProperty("path.book.blurb.files");
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info("---EXPORT CITATION---");
		
		String type = req.getParameter("type");
		String emailAddress = req.getParameter("emailAdd");
		String fileFormat = req.getParameter("fileFormat");
		String isbn = req.getParameter("isbn");
		String id = req.getParameter("id");
		
		Citation citation = new Citation();		
		citation.setIncludeAbstract(req.getParameter("includeAbstract"));
		citation.setAuthor(req.getParameter("author"));
		citation.setContributor(req.getParameter("contributor"));
		citation.setBookTitle(req.getParameter("bookTitle"));
		citation.setContentTitle(req.getParameter("contentTitle"));
		citation.setPrintDate(req.getParameter("printDate"));
		citation.setDoi(req.getParameter("doi"));
		citation.setBookId(req.getParameter("bookId"));
		citation.setContentId(req.getParameter("contentId"));
		try {
			citation.setBookBlurb(getBlurbText(isbn));
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
//		List<Document> docs = IndexSearchUtil.searchIndex(BOOK_LANDING_INDEX_DIR, "BOOK_ID:" + citation.getBookId());	
//		for (Document doc : docs) {
//			String docType = doc.get("DOC_TYPE");
//			if (docType.equals("book_details")) {
//				citation.setBookBlurb(doc.get("BOOK_BLURB"));
//				break;
//			}
//		}
		
		if(StringUtils.isNotEmpty(citation.getContentId()) && !"undefined".equalsIgnoreCase(citation.getContentId())) {
			citation.setUrl(CONTEXT_PATH + "chapter.jsf?bid=" + citation.getBookId() + "&cid=" + citation.getContentId());
		} else {
			citation.setUrl(CONTEXT_PATH + "ebook.jsf?bid=" + citation.getBookId());
		}
						
		if(type.equals("download")) {
//			download(req, resp, 
//					urlBuilder(isbn, id, includeAbstract, fileFormat, true));
			download(req, resp, citation, id, fileFormat);
		} else if(type.equals("email")) {
			String filePath = APP_EBOOKS_PATH + "/" + getFileName(id, fileFormat, citation.getIncludeAbstract()) 
				+ "_" + getCurrentDate("yyyyMMddHHmmss") + getFileExtension(fileFormat);
			
			createAttachmentFile(buildMessage(fileFormat, citation), filePath);
			
			sendEmail(req, resp, emailAddress, filePath);
		}		
	}
	
	protected void createAttachmentFile(String message, String filePath) {
		File file = new File(filePath);
		try {
			FileWriter writer = new FileWriter(file);
			writer.write(message);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
//	private String urlBuilder(String isbn, 
//			String contentId, 
//			String includeAbstract, 
//			String fileFormat,
//			boolean isUrl) {
//		
//		StringBuilder url = new StringBuilder();
//		StringBuilder filePath = new StringBuilder(CONTENT_DIR + isbn + "/");
//		
//		if(isUrl) {
//			url.append("downloads/citation/");
//		} else {
////			url.append(CONTEXT_PATH + "downloads/citation/");
//			url.append(CONTENT_DIR);
//			url.append(isbn);
//			url.append("/");
//		}
//		
//		url.append(contentId);
//		filePath.append(contentId);
//		
//		if(includeAbstract.equalsIgnoreCase("true")) {			
//			url.append("_WA-");
//			filePath.append("_WA-");
//		} else {
//			url.append("_WOA-");
//			filePath.append("_WOA-");
//		}
//		
//		if(fileFormat.equals("ASCII")) {	
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("Biblioscape")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("BibTex")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("CSV")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("Endnote")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("HTML")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("Medlars")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("Papyrus")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("ProCite")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("ReferenceManager")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("RefWorks")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} else if(fileFormat.equals("RIS")) {
//			url.append(fileFormat);
//			filePath.append(fileFormat);
//		} 			
//		url.append(getFileExtension(fileFormat));
//		filePath.append(getFileExtension(fileFormat));
//		
//		if(new File(filePath.toString()).exists()) {
//			return url.toString();	
//		} 
//		
//		return "popups/export_citation.jsf?download=none";
//	}
	
	protected void sendEmail(HttpServletRequest req, HttpServletResponse resp, 
			String recipient, String attachment) {
		
		String successful = "success";
//		if(attachmentPath.equals("popups/export_citation.jsf?download=none")) {
//			successful = "fail";
//		} else {
		try {						
			StringBuilder body = new StringBuilder();					 
			
			body.append("Attached is a citation from Cambridge Books Online that you, or a colleague of yours, requested to be sent to you.<br />");
			body.append("Visit Cambridge Books Online to freely browse and view tables of contents.<br />");
			body.append("Access to the full text is available to users whose institutions have purchased access.<br />");
			body.append("For further information or to unsubscribe, go to http://ebooks.cambridge.org<br /><br />");		
			
			MailManager.storeMail(TEMPLATE_DIR, 
								  TEMPLATE_FILE, 
								  OUTPUT_DIR, 
								  EXPORT_FROM_EMAIL, 
								  new String[]{recipient}, 
								  null, 
								  null, 
								  EXPORT_SUBJECT, 
								  attachment,
								  body.toString(),
								  req.getSession().getServletContext());		
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			successful = "fail";
		} 		
//		}
		
		try {
			resp.sendRedirect("popups/export_citation.jsf?email=" + successful);
		} catch (IOException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	protected void download(HttpServletRequest req, HttpServletResponse resp, Citation citation,
			String id, String fileFormat) {	
		LOGGER.info("---download(HttpServletRequest req, HttpServletResponse resp, String url)");
//		LogManager.info(ExportCitationServlet.class, "---URL:" + url);	
			
		resp.setHeader("Content-Disposition", "attachment;filename=" + getFileName(id, 
				fileFormat, citation.getIncludeAbstract()) 
				+ getFileExtension(fileFormat));		
		resp.setContentType("text");		
		resp.setCharacterEncoding("UTF-8"); 
		
		try {
//			resp.sendRedirect(url);
			resp.getWriter().print(buildMessage(fileFormat, citation));
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}		
	}
	
	protected String buildMessage(String fileFormat, Citation citation) {
		String message = "";
		if(fileFormat.equals("ASCII")) {				
			message = asciiFile(". ", citation);
		} else if(fileFormat.equals("Biblioscape")) {
			message = biblioscapeFile("\n", citation);			
		} else if(fileFormat.equals("BibTex")) {
			message = bibTexFile("\n", citation);
		} else if(fileFormat.equals("CSV")) {
			message = csvFile(",", citation);
		} else if(fileFormat.equals("Endnote")) {
			message = endNoteFile("\n", citation);
		} else if(fileFormat.equals("HTML")) {
			message = htmlFile("\n", citation);
		} else if(fileFormat.equals("Medlars")) {
			message = medlarsFile("\n", citation);
		} else if(fileFormat.equals("Papyrus")) {
			message = papyrusFile("\n", citation);
		} else if(fileFormat.equals("ProCite")) {
			message = proCiteFile("\n", citation);
		} else if(fileFormat.equals("ReferenceManager")) {
			message = refManFile("\n", citation);
		} else if(fileFormat.equals("RefWorks")) {
			message = refWorksFile("\n", citation);
		} else if(fileFormat.equals("RIS")) {
			message = risFile("\n", citation);
		} 
		
		return message;
	}
	
	protected String getFileName(String contentId, String fileFormat, String includeAbstract) {
		StringBuilder fileName = new StringBuilder(contentId);
		
		if(includeAbstract.equalsIgnoreCase("true")) {			
			fileName.append("_WA-");
		} else {
			fileName.append("_WOA-");
		}
		
		if(fileFormat.equals("ASCII")) {	
			fileName.append(fileFormat);
		} else if(fileFormat.equals("Biblioscape")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("BibTex")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("CSV")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("Endnote")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("HTML")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("Medlars")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("Papyrus")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("ProCite")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("ReferenceManager")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("RefWorks")) {
			fileName.append(fileFormat);
		} else if(fileFormat.equals("RIS")) {
			fileName.append(fileFormat);
		} 			
		
		return fileName.toString();
	}
	
	protected String getFileExtension(String fileFormat) {
	
		String extension = "";
		
		if(fileFormat.equals("ASCII")) {
			extension = ".txt";
		} else if(fileFormat.equals("Biblioscape")) {		
			extension = ".txt";
		} else if(fileFormat.equals("BibTex")) {			
			extension = ".bib";
		} else if(fileFormat.equals("CSV")) {			
			extension = ".csv";
		} else if(fileFormat.equals("Endnote")) {		
			extension = ".enw";
		} else if(fileFormat.equals("HTML")) {			
			extension = ".htm";
		} else if(fileFormat.equals("Medlars")) {
			extension = ".txt";
		} else if(fileFormat.equals("Papyrus")) {			
			extension = ".txt";
		} else if(fileFormat.equals("ProCite")) {
			extension = ".ris";
		} else if(fileFormat.equals("ReferenceManager")) {
			extension = ".ris";
		} else if(fileFormat.equals("RefWorks")) {
			extension = ".txt";
		} else if(fileFormat.equals("RIS")) {
			extension = ".ris";
		}
		
		return extension;
	}
	
	protected String asciiFile(String delimeter, Citation citation) {
        
		StringBuilder output = new StringBuilder();		
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append(citation.getContributor() + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append(citation.getContentTitle() + delimeter);
		}		
		output.append(citation.getAuthor() + delimeter);
		output.append(citation.getBookTitle() + delimeter);
		output.append("Cambridge University Press, " + citation.getPrintDate().trim() + delimeter);
		
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		output.append("http://dx.doi.org/" + citation.getDoi() + delimeter);

		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("\nBook blurb: " + citation.getBookBlurb());
		}
		
		return output.toString();
    }
	
	protected String biblioscapeFile(String delimeter, Citation citation) {
	                
		StringBuilder output = new StringBuilder();
		
		output.append("--RT-- Book" + delimeter);  	
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("--AU-- " + citation.getContributor() + delimeter);
		}		
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("--TI-- " + citation.getContentTitle() + delimeter);
		}		
		output.append("--AU-- " + citation.getAuthor() + delimeter);
		output.append("--TI-- " + citation.getBookTitle() + delimeter);
		output.append("--PB-- " + "Cambridge University Press" + delimeter);
		output.append("--YP-- " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("--AB-- " + citation.getBookBlurb() + delimeter);
		}
		output.append("--UR-- http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("--C1-- Cambridge Books Online" + delimeter);
		output.append("--C2-- Cambridge University Press" + delimeter);
		output.append("--CD-- " + getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString();
	}
	
	protected String bibTexFile(String delimeter, Citation citation) {
                
		StringBuilder output = new StringBuilder();
		
		output.append("@book{" + delimeter);		
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("contributor = {" + citation.getContributor() + "}," + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("contentTitle = {" + citation.getContentTitle() + "}," + delimeter);
		}		
		output.append("author = {" + citation.getAuthor() + "}," + delimeter);
		output.append("bookTitle = {" + citation.getBookTitle() + "}," + delimeter);
		output.append("publisher = {Cambridge University Press}," + delimeter);
		output.append("year = {" + citation.getPrintDate() + "}," + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("bookBlurb = {" + citation.getBookBlurb() + "}," + delimeter);
		}
		output.append("url = {http://dx.doi.org/" + citation.getDoi() + "}" + delimeter);
		output.append("}" + delimeter);
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString();
    }
	
	protected String csvFile(String delimeter, Citation citation) {
	
		StringBuilder output = new StringBuilder();
		
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("\"" + citation.getContributor() + "\"" + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("\"" + citation.getContentTitle().replaceAll("\"","\"\"") + "\"" + delimeter);
		}				
		output.append("\"" + citation.getAuthor() + "\"" + delimeter);
		output.append("\"" + citation.getBookTitle().replaceAll("\"","\"\"") + "\"" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("\"" + citation.getBookBlurb().replaceAll("\"","\"\"") + "\"" + delimeter);
		}
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		output.append("http://dx.doi.org/" + citation.getDoi() + delimeter);
		
		return output.toString();
	}
	
	protected String endNoteFile(String delimeter, Citation citation) {
                
		StringBuilder output = new StringBuilder();
		output.append("%0 Book" + delimeter);  	
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			String[] contributors = citation.getContributor().split(";");
			for(String c : contributors) {
				output.append("%A " + c + delimeter);
			}
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) && !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("%T " + citation.getContentTitle() + delimeter);
		}		
		String[] authors = citation.getAuthor().split(";");
		for(String a : authors) {
			output.append("%A " + a + delimeter);
		}
		output.append("%T " + citation.getBookTitle() + delimeter);
		output.append("%I Cambridge University Press" + delimeter);
		output.append("%D " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("%X " + citation.getBookBlurb() + delimeter);
		}
		output.append("%O Web: http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("%1 Cambridge Books Online" + delimeter);
		output.append("%2 Cambridge University Press" + delimeter);
		output.append("%3 " + getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString();        
    }
	
	protected String htmlFile(String delimeter, Citation citation) {
		
		StringBuilder output = new StringBuilder();
		
		output.append("<html>" + delimeter);
		output.append("<head>" + delimeter);		
		
		if(StringUtils.isNotEmpty(citation.getContentTitle()) && !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("<title>" + citation.getContentTitle() + "</title>" + delimeter);
		} else {
			output.append("<title>" + citation.getBookTitle() + "</title>" + delimeter);
		}
		output.append("</head>" + delimeter);
		output.append("<body bgcolor=\"#FFFFFF\" text=\"#000000\">" + delimeter);			
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("<b>Contributor:</b> " + citation.getContributor() + "<br />" + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("<b>Content Title:</b> " + citation.getContentTitle() + "<br />" + delimeter);
		}
		output.append("<b>Author:</b> " + citation.getAuthor() + "<br/>" + delimeter);
		output.append("<b>Book Title:</b> " + citation.getBookTitle() + " <br/>" + delimeter); 
		output.append("<b>Publisher:</b> Cambridge University Press <br />" + delimeter);
		output.append("<b>Print Date:</b> " + citation.getPrintDate() + "<br />" + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("<p><b>Book Blurb:</b> <p>" + citation.getBookBlurb() + "</p></p>" + delimeter);
		}
		output.append("Cambridge Books Online <br />" + delimeter);
		output.append("Cambridge University Press <br />" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + "<br />" + delimeter);
		output.append("<b>URL:</b> <a target=\"_blank\" href=\"http://dx.doi.org/" + citation.getDoi()
				+ "\">http://dx.doi.org/" + citation.getDoi() + "</a></p>");
		
		output.append("</body>" + delimeter);
		output.append("</html>" + delimeter);
        
		return output.toString();      
    }
	
	protected String medlarsFile(String delimeter, Citation citation) {
	                
		StringBuilder output = new StringBuilder();
		output.append("PT - Book" + delimeter);  
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("AU - " + citation.getContributor() + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("TA - " + citation.getContentTitle() + delimeter);
		}		
		output.append("AU - " + citation.getAuthor() + delimeter);
		output.append("TI - " + citation.getBookTitle() + delimeter);
		output.append("PB - Cambridge University Press" + delimeter);
		output.append("DP - " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB - " + citation.getBookBlurb() + delimeter);
		}
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		output.append("4099 - http://dx.doi.org/" + citation.getDoi() + delimeter);
		
		return output.toString(); 
	}
	
	protected String papyrusFile(String delimeter, Citation citation) {        
        
		StringBuilder output = new StringBuilder();
		output.append("TY - BOOK" + delimeter);  
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {			
			String[] contributors = citation.getContributor().split(";");
			for(String c : contributors) {
				output.append("A1 - " + c + delimeter);
			}
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("T1 - " + citation.getContentTitle() + delimeter);
		}		
		String[] authors = citation.getAuthor().split(";");
		for(String a : authors) {
			output.append("A1 - " + a + delimeter);
		}
		output.append("T1 - " + citation.getBookTitle() + delimeter);
		output.append("PB - Cambridge University Press" + delimeter);
		output.append("Y1 - " + citation.getPrintDate() + "///" + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB - " + citation.getBookBlurb() + delimeter);
		}
		output.append("N1 - Web: http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("ER - " + delimeter);
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString(); 
    }
	
	protected String proCiteFile(String delimeter, Citation citation) {
        
		StringBuilder output = new StringBuilder();
		output.append("TY - BOOK" + delimeter);  
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("AU - " + citation.getContributor() + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("TI - " + citation.getContentTitle() + delimeter);
		}		
		output.append("AU - " + citation.getAuthor() + delimeter);
		output.append("TI - " + citation.getBookTitle() + delimeter);
		output.append("PB - Cambridge University Press" + delimeter);
		output.append("PY - " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB - " + citation.getBookBlurb() + delimeter);
		}
		output.append("UR - http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("ER - " + delimeter);
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString(); 
    }
	
	protected String refManFile(String delimeter, Citation citation) {
        		
		StringBuilder output = new StringBuilder();
		output.append("TY - BOOK" + delimeter);  		
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			String[] contributors = citation.getContributor().split(";");
			for(String c : contributors) {
				output.append("A1 - " + c + delimeter);
			}
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("T1 - " + citation.getContentTitle() + delimeter);
		}
		String[] authors = citation.getAuthor().split(";");
		for(String a : authors) {
			output.append("A1 - " + a + delimeter);
		}
		output.append("T1 - " + citation.getBookTitle() + delimeter);
		output.append("PB - Cambridge University Press" + delimeter);
		output.append("Y1 - " + citation.getPrintDate() + "///" + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB - " + citation.getBookBlurb() + delimeter);
		}
		output.append("N1 - Web: http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("ER - " + delimeter);
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString(); 
    }
    
    protected String refWorksFile(String delimeter, Citation citation) {
        
    	StringBuilder output = new StringBuilder();
		output.append("RT eBook" + delimeter);  		
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			String[] contributors = citation.getContributor().split(";");
			for(String c : contributors) {
				output.append("A1 " + c + delimeter);
			}
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) && !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("T1 " + citation.getContentTitle() + delimeter);
		}
		String[] authors = citation.getAuthor().split(";");
		for(String a : authors) {
			output.append("A1 " + a + delimeter);
		}
		output.append("T1 " + citation.getBookTitle() + delimeter);
		output.append("PB Cambridge University Press" + delimeter);
		output.append("YR " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB " + citation.getBookBlurb() + delimeter);
		}
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		output.append("LK http://dx.doi.org/" + citation.getDoi() + delimeter);
		
		return output.toString(); 
    }
	
	protected String risFile(String delimeter, Citation citation) {      
                
		StringBuilder output = new StringBuilder();
		output.append("TY - BOOK" + delimeter);  
		if(StringUtils.isNotEmpty(citation.getContributor()) 
				&& !"undefined".equalsIgnoreCase(citation.getContributor())) {
			output.append("AU - " + citation.getContributor() + delimeter);
		}
		if(StringUtils.isNotEmpty(citation.getContentTitle()) 
				&& !"undefined".equalsIgnoreCase(citation.getContentTitle())) {
			output.append("TI - " + citation.getContentTitle() + delimeter);
		}		
		output.append("AU - " + citation.getAuthor() + delimeter);
		output.append("TI - " + citation.getBookTitle() + delimeter);
		output.append("PB - Cambridge University Press" + delimeter);
		output.append("PY - " + citation.getPrintDate() + delimeter);
		if(citation.getIncludeAbstract().equalsIgnoreCase("true")) {
			output.append("AB - " + citation.getBookBlurb() + delimeter);
		}
		output.append("UR - http://dx.doi.org/" + citation.getDoi() + delimeter);
		output.append("ER - " + delimeter);
		output.append("Cambridge Books Online" + delimeter);
		output.append("Cambridge University Press" + delimeter);
		output.append(getCurrentDate(CURRENT_DATE_FORMAT) + delimeter);
		
		return output.toString(); 
    }	
	
	protected String getCurrentDate(String format) {
		return EBooksUtil.getCurrentDate(format);
	}
	
	private String getBlurbText(String isbn) throws Exception {
		StringBuilder blurb = new StringBuilder(); 
		File file = new File(dirContent + isbn + "/" + isbn + "_blurb.txt");
		if(file.exists()) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String s = "";
			while(null != (s = br.readLine())) {
				blurb.append(s);
			}
		}
		
		return blurb.toString();
	}
}
