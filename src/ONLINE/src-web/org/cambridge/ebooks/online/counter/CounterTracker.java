//package org.cambridge.ebooks.online.counter;
//
//
//import javax.jms.Connection;
//import javax.jms.ConnectionFactory;
//import javax.jms.Destination;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageProducer;
//import javax.jms.Session;
//
//import org.cambridge.common.ServiceLocatorException;
//import org.cambridge.ebooks.online.util.DateUtil;
//import org.cambridge.ebooks.online.util.EjbUtil;
//import org.cambridge.ebooks.online.util.LogManager;
//
//public class CounterTracker {
//	
//	public static final String CONNECTION_FACTORY = "ConnectionFactory";
//	
//	public static final String JMS_DESTINATION = "queue/counter/logger";
//	
//	
//	public static void logEvent(CounterBean cb){
//		try {
//        	
//            ConnectionFactory connectionFactory = EjbUtil.getJmsConnectionFactory( CONNECTION_FACTORY );
//
//            Connection connection = connectionFactory.createConnection();
//            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            
//            Destination destination = EjbUtil.getJmsDestination( JMS_DESTINATION );            
//            MessageProducer messageProducer = session.createProducer(destination);
//            Message payload = session.createMapMessage();
//            
//        	payload.setStringProperty(CounterBean.LOG_TYPE, String.valueOf(cb.getLogType()));
//        	payload.setStringProperty(CounterBean.SERVICE, cb.getService());
//        	payload.setStringProperty(CounterBean.ISBN, cb.getIsbn());
//        	payload.setStringProperty(CounterBean.ISSN, cb.getIssn());
//        	payload.setStringProperty(CounterBean.TITLE, cb.getTitle());
//        	payload.setStringProperty(CounterBean.PUBLISHER, cb.getPublisher());
//        	payload.setStringProperty(CounterBean.PLATFORM, cb.getPlatform());
//        	payload.setStringProperty(CounterBean.SESSION_ID, cb.getSessionId());
//        	
//        	//COMPONENT_ID used for sales module, null so far
//        	payload.setStringProperty(CounterBean.COMPONENT_ID, cb.getComponentId());
//        	payload.setStringProperty(CounterBean.IS_SEARCH, cb.getIsSearch());
//        	payload.setStringProperty(CounterBean.TIMESTAMP, 
//        			DateUtil.convTsToStr(cb.getTimestamp()));        	
//        	payload.setStringProperty(CounterBean.BODY_ID, String.valueOf(cb.getBodyId()));
//        	payload.setStringProperty(CounterBean.CHECK_TIME, String.valueOf(cb.getCheckTime()));
//            
//            try { 
//	            messageProducer.send( payload );	            
//            } finally { 
//            	messageProducer.close();
//                session.close();
//                connection.close();	
//            }
//        } catch (JMSException je) {
//        	LogManager.error(CounterTracker.class, " Message not sent to counter-ejb: " + je.getMessage() );
//        	System.out.println("je: "+je);
//        } catch (ServiceLocatorException sle) {
//        	LogManager.error(CounterTracker.class, " Message not sent to counter-ejb:" + sle.getMessage() ); 
//        }
//	}	
//}
