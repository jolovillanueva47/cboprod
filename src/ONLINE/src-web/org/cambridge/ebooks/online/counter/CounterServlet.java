//package org.cambridge.ebooks.online.counter;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//import java.util.ArrayList;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.cambridge.common.LogManager;
//import org.cambridge.ebooks.online.crumbtrail.CrumbtrailStack;
//import org.cambridge.ebooks.online.crumbtrail.CrumbtrailTag;
//import org.cambridge.ebooks.online.filter.AuthenticationFilter;
//import org.cambridge.util.Misc;
//
//public class CounterServlet extends HttpServlet {
//	
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4113278814395195410L;
//	
//	private static final String ADV_SEARCH_RESULTS = "adv_search_results.jsp";
//	
//	public static final String BOOK_ID = "bid";
//	
//	
//	public static final String PLATFORM = "Cambridge Books Online";
//	
//	public static final String PUBLISHER = "Cambridge University Press";
//		
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		doGet(req, resp);
//	}
//	
//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		logCounter(req);
//		System.out.println("Log Counter Servlet called");
//	}
//	
//	private void logCounter(int bodyId, HttpServletRequest req, CounterBean cb){
//		LogManager.info(this.getClass(), "log counter");
//		// Log Counter Report 2
//		
//		cb.setLogType(2);		
//		cb.setBodyId(bodyId);		
//		CounterTracker.logEvent(cb);
//		
//		/*
//		//counter 5		
//		cb.setLogType(5);
//		cb.setIsSearch("N");
//		CounterTracker.logEvent(cb);
//		
//		//counter 5 search
//		if(isFromAdvanceSearch(req)){
//			cb.setIsSearch("Y");
//			CounterTracker.logEvent(cb);
//		}	
//		*/
//	}
//	
//	private void logCounter6Search(int bodyId, HttpServletRequest req, CounterBean cb){
//		cb.setLogType(6);
//		cb.setBodyId(bodyId);
//		cb.setService(PLATFORM);
//		cb.setIsSearch("Y");
//		CounterTracker.logEvent(cb);
//	}
//	
//	private void logCounter6Session(int bodyId, HttpServletRequest req, CounterBean cb){
//		cb.setLogType(6);
//		cb.setBodyId(bodyId);
//		cb.setService(PLATFORM);
//		cb.setIsSearch("N");
//		CounterTracker.logEvent(cb);
//	}
//	
//	@SuppressWarnings("unused")
//	private boolean isFromAdvanceSearch(HttpServletRequest request) {
//		HttpSession session = request.getSession();
//		CrumbtrailStack stack = (CrumbtrailStack) session.getAttribute(CrumbtrailTag.CRUMB);
//		if(stack == null){
//			return false;
//		}
//		if(stack.containsJsp(ADV_SEARCH_RESULTS) == -1){
//			return false;
//		}
//		return true;
//	}
//	
//	@SuppressWarnings("unused")
//	private String getTitle(HttpServletRequest request){
//		String title = request.getParameter("title");
//		try {
//			return URLDecoder.decode(title,"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		return "";
//	}
//	
//	public void logCounter(HttpServletRequest request, int type, String search){
//		HttpSession session = request.getSession();
//		String bodyIds = (String) session.getAttribute(AuthenticationFilter.ACCESS_LIST_BODY_IDS);
//		CounterBean cb = createCounterBean(request);		
//		if(bodyIds != null){
//			String[] bodyIdArr = cleanBodyIds(bodyIds);
//			//String[] bodyIdArr = new String[]{"1289592"};
//			if(bodyIdArr == null || bodyIdArr.length == 0){
//				return;
//			}
//			//String str = bodyIdArr[0];
//			for(String str: bodyIdArr){
//				System.out.println("Log counter body id = " + str);
//				if(Misc.isNotEmpty(str)) {
//					if(type == 6){
//						if("Y".equals(search)){
//							logCounter6Search(Integer.valueOf(str), request, cb);
//						}else{
//							logCounter6Session(Integer.valueOf(str), request, cb);
//						}						
//					}else{
//						logCounter(Integer.valueOf(str), request, cb);
//					}
//				}
//			}
//		}
//	}
//	
//	public void logCounter(HttpServletRequest request){
//		logCounter(request, 0, "");
//	}
//	
//	
//	private CounterBean createCounterBean(HttpServletRequest request){
//		CounterBean cb = new CounterBean();		
//		cb.initCounterBean(request);
//		return cb;		
//	}
//	
//	private static final String COMMA = ",";
//	private String[] cleanBodyIds(String bodyIds){
//		ArrayList<String> alStr = new ArrayList<String>();
//		String[] bodyIdArr = bodyIds.split(COMMA);
//		//String[] bodyIdArr = new String[]{"1289592"};
//		for(String str : bodyIdArr){
//			if(!alStr.contains(str)){
//				alStr.add(str);
//			}
//		}
//		return alStr.toArray(new String[alStr.size()]);
//	}
//	
//	@SuppressWarnings("unused")
//	private String getSessionId(HttpServletRequest req){
//		HttpSession session = req.getSession();
//		return session.getId();
//	}
//	
//	@SuppressWarnings("unused")
//	private int getBodyId(HttpServletRequest req){
//		HttpSession session = req.getSession();
//		String bodyId = (String)session.getAttribute("orgBodyId");
//		if(bodyId == null)
//			return 0;
//		else
//			return Integer.parseInt(bodyId);
//	}
//	
//	public static final String COUNTER6_SESSION_LOG = "COUNTER6_SESSION_LOG";
//	public static void logCounter6Session(HttpServletRequest request){
//		HttpSession session = request.getSession();
//		String counter6 = (String) session.getAttribute(COUNTER6_SESSION_LOG);
//		String bodyIds = (String) session.getAttribute(AuthenticationFilter.ACCESS_LIST_BODY_IDS);
//		//prevent log or log only once
//		if(counter6 != null && counter6.equals(bodyIds)){
//			return;
//		}		
//		
//		//actual log		
//		
//		session.setAttribute(COUNTER6_SESSION_LOG, bodyIds);
//		CounterServlet cs = new CounterServlet();
//		cs.logCounter(request, 6, "N");
//	}
//	
//	
//	
//	
//}
