package org.cambridge.ebooks.online.dos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.EjbUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

public class DosLogger {

	private static final Logger logger = Logger.getLogger(DosLogger.class);
	private static final String CONNECTION_FACTORY = "ConnectionFactory";
	private static final String JMS_DESTINATION = "queue/dos_tracker/logger";
	
	private static final String[] ACCESS_VO_PROPERTIES = {		
		"pdfAccessCount",
		"ipAddress",
		"ipType",
		"sessionId",
		"startTime",
		"endTime",
		"duration",
		"logDate",
		"bodyId"
	}; 
	
	public static void saveEvent(AccessVo accessVo) throws Exception {
		ConnectionFactory connectionFactory = EjbUtil.getJmsConnectionFactory(CONNECTION_FACTORY);
		Connection connection = connectionFactory.createConnection();
		
		Session connectionSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = EjbUtil.getJmsDestination(JMS_DESTINATION);
		MessageProducer messageProducer = connectionSession.createProducer(destination);
		Message payload = connectionSession.createMapMessage();

        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String value = "";
		for(String prop : ACCESS_VO_PROPERTIES) {
			value = BeanUtils.getProperty(accessVo, prop);
			try {
				if("startTime".equals(prop) || "endTime".equals(prop)) {
					value = timeFormat.format(new Date(Long.parseLong(value)));
				} 	
			} catch(Exception e) {
				logger.error("[Exception]");
				logger.error(ExceptionPrinter.getStackTraceAsString(e));
				continue;
			}
			payload.setStringProperty(prop, value);
		}		
		
		try {
			messageProducer.send(payload);
		} catch (Exception e) {
			throw e;
		} finally {
			messageProducer.close();
			connectionSession.close();
			connection.close();
		}
	}
}
