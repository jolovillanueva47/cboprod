package org.cambridge.ebooks.online.dos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.filter.AbuseMonitoringFilter;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;


public class AccessVo {

	private boolean isDenied;
	private int pdfAccessCount;
	private String ipAddress;
	private String ipType;
	private String sessionId;
	private long startTime;
	private long endTime;
	private long duration;
	private String logDate;
	private String bodyId;
	private long sessionDuration;
	
	
	private static final String[] IP_TYPE = AbuseMonitoringFilter.IP_TYPE;
	
	private static final int SESSION_DURATION 
			= Integer.parseInt(System.getProperty("session.duration"));
	
	private static final int PDF_ACCESS_LIMIT_USER 
			= Integer.parseInt(System.getProperty("pdf.access.user.limit"));
	
	private static final int PDF_ACCESS_LIMIT_PROXY 
			= Integer.parseInt(System.getProperty("pdf.access.proxy.limit"));
	
	public boolean isLimitExceeded() {
		return ((this.sessionDuration < SESSION_DURATION  
				&& this.getPdfAccessCount() >= PDF_ACCESS_LIMIT_PROXY 
				&& IP_TYPE[1].equals(this.getIpType()))
				|| 
				(this.sessionDuration < SESSION_DURATION 
				&& this.getPdfAccessCount() >= PDF_ACCESS_LIMIT_USER
				&& IP_TYPE[0].equals(this.getIpType())));
	}
	
	public boolean isSessionDurationExceeded() {
		return ((this.sessionDuration > SESSION_DURATION 
				&& this.getPdfAccessCount() <= PDF_ACCESS_LIMIT_PROXY 
				&& IP_TYPE[1].equals(this.getIpType()))
				|| 
				(this.sessionDuration > SESSION_DURATION 
				&& this.getPdfAccessCount() <= PDF_ACCESS_LIMIT_USER 
				&& IP_TYPE[0].equals(this.getIpType())));
	}
	
	public void populateAccessVoDetails(HttpSession session) throws Exception {
		long endTime = this.previousTime;
		this.sessionDuration = (endTime	- this.getStartTime()) / 1000;						
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        this.setLogDate(dateFormat.format(new Date(endTime)).toUpperCase());
        this.setEndTime(endTime);						
		this.setDenied(true);
		this.setDuration(this.sessionDuration);
		String bodyId = OrgConLinkedMap.getFromSession(session).getCurrentlyLoggedBodyId();
		if(StringUtils.isNotEmpty(bodyId)) {
			this.setBodyId(bodyId);
		}
	}
	
	
	public long getSessionDuration() {
		return sessionDuration;
	}
	public void setSessionDuration(long sessionDuration) {
		this.sessionDuration = sessionDuration;
	}
	public long getPreviousTime() {
		return previousTime;
	}
	public void setPreviousTime(long previousTime) {
		this.previousTime = previousTime;
	}

	private long previousTime; 
	
	
	/**
	 * @return the isDenied
	 */
	public boolean isDenied() {
		return isDenied;
	}
	/**
	 * @param isDenied the isDenied to set
	 */
	public void setDenied(boolean isDenied) {
		this.isDenied = isDenied;
	}
	/**
	 * @return the pdfAccessCount
	 */
	public int getPdfAccessCount() {
		return pdfAccessCount;
	}
	/**
	 * @param pdfAccessCount the pdfAccessCount to set
	 */
	public void setPdfAccessCount(int pdfAccessCount) {
		this.pdfAccessCount = pdfAccessCount;
	}
	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	/**
	 * @return the ipType
	 */
	public String getIpType() {
		return ipType;
	}
	/**
	 * @param ipType the ipType to set
	 */
	public void setIpType(String ipType) {
		this.ipType = ipType;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}	
	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the endTime
	 */
	public long getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}	
	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}		
	/**
	 * @return the logDate
	 */
	public String getLogDate() {
		return logDate;
	}
	/**
	 * @param logDate the logDate to set
	 */
	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}
	/**
	 * @return the bodyId
	 */
	public String getBodyId() {
		return bodyId;
	}
	/**
	 * @param bodyId the bodyId to set
	 */
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}	
	
	public String toString(long val) {		
		return String.valueOf(val);
	}
}
