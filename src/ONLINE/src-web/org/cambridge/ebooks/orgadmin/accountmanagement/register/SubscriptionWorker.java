package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class SubscriptionWorker {
	private static Logger LOGGER = Logger.getLogger(SubscriptionWorker.class);
	
	public static String activateSocietySubscriptions(String username,
			String subscriptions, String societyId, HttpServletRequest request) {
		LOGGER.info("=activateSocietySubscriptions");
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		int ret = 0;
		String societyName = "";
		String invalidSubscription = "";
		String alreadyActiveSubs = "";
		String invalidSociety = "";
		
		try{
			conn = DBManager.openConnection();
			
			cs = conn.prepareCall("{ call PKG_CJOC_REGISTERED_USER.ACTIVATE_SOCIETY_SUBSCRIPTIONS(?,?,?,?,?,?,?,?,?) }");
		    int i=1;
            cs.registerOutParameter(i++, Types.INTEGER);
            cs.registerOutParameter(i++, Types.VARCHAR);
            cs.registerOutParameter(i++, Types.VARCHAR);
            cs.registerOutParameter(i++, Types.VARCHAR);
            cs.registerOutParameter(i++, Types.VARCHAR);
            cs.registerOutParameter(i++, OracleTypes.CURSOR);
		    cs.setString(i++, username); 
		    cs.setString(i++, societyId);
		    cs.setString(i++, subscriptions);
		    
		    cs.execute(); 
		    
		    ret = cs.getInt(1);
		    
		    if (ret == 0 || ret ==200){
                invalidSubscription = cs.getString(3);
	    		alreadyActiveSubs = cs.getString(4);
	    		invalidSociety = cs.getString(5);            
            }
		    
		    if (ret == 0){
                rs = (ResultSet) cs.getObject(6);
	            while(rs.next()){
	                societyName = rs.getString("DISPLAY_NAME");
	            }
            }
		    request.setAttribute("invalidSubscription", invalidSubscription);
		    request.setAttribute("alreadyActiveSubs", alreadyActiveSubs);
		    request.setAttribute("invalidSociety", invalidSociety);
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return societyName;
	}
	
	public static void insertBookAccess(String memberId, String subscriptionType ) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" insert into book_subscribed ")
			.append(" (SUBSCRIPTION_NO, USER_ID, COMPONENT_ID, SUBSCRIPTION_START_DATE, SUBSCRIPTION_END_DATE, SUBSCRIPTION_TYPE) ")
			.append(" values ")
			.append(" ( SUBSCRIPTION_SEQ.NEXTVAL, ?, ?, ?, ?, ?) ");
				
		String bookId = getStahlId();
		
		try{
			conn = DBManager.openConnection();
			
			Date startOfTrial = new Date();
			
			Date endOfTrial = new Date();
			Calendar cal = Calendar.getInstance();
			
			cal.setTime( endOfTrial );
			cal.add(Calendar.YEAR, 1 );
			endOfTrial = cal.getTime();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
			
			
			ps = conn.prepareStatement( sql.toString() );
			ps.setString(1, memberId);
			ps.setString(2, bookId );
			ps.setString(3, sdf.format(startOfTrial) );
			ps.setString(4, sdf.format(endOfTrial) );
			ps.setString(5, subscriptionType);
			
			ps.executeUpdate();
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static String getStahlId() { 
		String stahlId = "";
		String sql = "select distinct value  from book where name = 'COMPONENT_ID' ";
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = DBManager.openConnection();
			
			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				stahlId = rs.getString("VALUE");
			}
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return stahlId;
	}
	
}
