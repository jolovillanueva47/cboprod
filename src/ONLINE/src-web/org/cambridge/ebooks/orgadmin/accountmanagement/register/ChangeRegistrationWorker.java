package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;
import org.cambridge.util.UnixCrypt;

public class ChangeRegistrationWorker {
	private static Logger LOGGER = Logger.getLogger(ChangeRegistrationWorker.class);
	public static String ANNUAL_SUBSCRIPTION = "ANNUAL";
	
	public static Register getUserDetails(String memberId){
		Register userDetail = new Register();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		
		
		try{
			conn = DBManager.openConnection();
			
			String sql = " select member_title, member_first_name, member_surname, " +
						 " member_country_id, member_state_county, member_city, " +
						 " member_post_code, member_address_1, member_address_2, " +
						 " member_telephone, member_userid, member_email_1, " +
						 " member_id, member_fax, member_affiliation, " +
						 " member_sms_alert, member_mobile_phone " +
						 " from blade_member " +
						 " where member_id = ?";
			ps = conn.prepareStatement(sql);
		    ps.setString(1, memberId);
		    
		    rs = ps.executeQuery();
			
		    while(rs.next()){
		    	userDetail.setTitle(rs.getString("member_title"));
		    	userDetail.setFirstName(rs.getString("member_first_name"));
		    	userDetail.setSurName(rs.getString("member_surname"));
		    	
		    	userDetail.setCountry(rs.getString("member_country_id"));
		    	userDetail.setState(rs.getString("member_state_county"));
		    	userDetail.setCity(rs.getString("member_city"));
		    	
		    	userDetail.setPostCode(rs.getString("member_post_code"));
				userDetail.setAddress(rs.getString("member_address_1"));
				userDetail.setAddress2(rs.getString("member_address_2"));
				
				userDetail.setTelephone(rs.getString("member_telephone"));
				userDetail.setUserName(rs.getString("member_userid"));
				userDetail.setEmail(rs.getString("member_email_1"));
				
		    	userDetail.setFax(rs.getString("member_fax"));
		    	userDetail.setAffiliation(rs.getString("member_affiliation"));
		    	
		    	userDetail.setSmsAlert("YES".equals(rs.getString("member_sms_alert")));
		    	userDetail.setMobilePhone(rs.getString("member_mobile_phone"));
		    }
		    
		    sql = "select member_id, pref_spell_remind, pref_email_remind, pref_third_party from blade_pref" +
		    		" where member_id = ?";
		    ps = conn.prepareStatement(sql);
		    ps.setString(1, memberId);
		    
		    rs = ps.executeQuery();
			
		    while(rs.next()){
		    	userDetail.setQuestion(rs.getString("pref_spell_remind"));
		    	userDetail.setEmailList("YES".equals(rs.getString("pref_email_remind")));
		    	userDetail.setThirdParty("YES".equals(rs.getString("pref_third_party")));
		    }
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return userDetail;
	}

	public static String getOldPassword(String username) {
		LOGGER.info("==getOldPassword");
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		String password = "";
		try{
			conn = DBManager.openConnection();
			
			String sql= "select MEMBER_SPELL " +
						"from BLADE_MEMBER " +
						"where MEMBER_USERID = '" + username + "'";
			
			
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()){
				password = rs.getString("MEMBER_SPELL");
			}
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return password;
	}
	
	//prepared statement
	public static String updateUser(Register userUpdate) {
	LOGGER.info("==updateUser");
	Connection conn = null;
	PreparedStatement ps = null;
	
	String errorMsg = "";
	try{
		conn = DBManager.openConnection();
		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE BLADE MEMBER ")
			.append("MEMBER_TITLE = ?, MEMBER_FIRST_NAME = ?, MEMBER_SURNAME = ?, ")
			.append("MEMBER_TELEPHONE = ?, MEMBER_FAX = ?, MEMBER_MOBILE_PHONE = ?, ")
			.append("MEMBER_ADDRESS_1 = ?, MEMBER_ADDRESS_2 = ?, MEMBER_CITY = ?, ")
			.append("MEMBER_STATE_COUNTY = ?, MEMBER_POST_CODE = ?, MEMBER_COUNTRY_ID = ?, ")
			.append("MEMBER_AFFILIATION = ?, MEMBER_SPELL = ?, MEMBER_EMAIL_1 = ?, MEMBER_PASSWORD = ?, ")
			.append(" MEMBER_FULLNAME = ? ")
			.append("WHERE MEMBER_USERID = ? ");
		
		ps = conn.prepareStatement(sql.toString());
		
		int i=1;
		
        ps.setString(i++, userUpdate.getTitle());
        ps.setString(i++, userUpdate.getFirstName());
        ps.setString(i++, userUpdate.getSurName());
        
 		ps.setString(i++, userUpdate.getTelephone());
 		ps.setString(i++, userUpdate.getFax());
 		ps.setString(i++, userUpdate.getMobilePhone());
 		
 		ps.setString(i++, userUpdate.getAddress());
 		ps.setString(i++, userUpdate.getAddress2());
 		//ps.setString(i++, userUpdate.getAddress3());     		
 		ps.setString(i++, userUpdate.getCity());
 		
 		ps.setString(i++, userUpdate.getState());
 		ps.setString(i++, userUpdate.getPostCode());
 		ps.setString(i++, userUpdate.getCountry());
		
 		
 		ps.setString(i++, userUpdate.getAffiliation());
 		if(userUpdate.getPassWord().length() <= 8) {
			ps.setString(i++, UnixCrypt.crypt(userUpdate.getPassWord()));
		}
		else {
			ps.setString(i++, userUpdate.getPassWord());
		}
 		ps.setString(i++, userUpdate.getEmail());
 		ps.setString(i++, userUpdate.getPassWord());
 		
 		String fullName = userUpdate.getFirstName() + " " + userUpdate.getSurName();
 		ps.setString(i++, fullName);
 		
 		ps.setString(i++, userUpdate.getUserName());
 		
 		int ret = ps.executeUpdate();
 		
// 		int ret = ps.getInt(1);
 		
// 		if(ret == -1 || ret == 3 || ret == 4){
// 			errorMsg = ps.getString(2);
// 		}
	}catch (Exception e) {
		LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
	} finally {
		try {
			DBManager.closeConnection(conn, ps);
		} catch (Exception e) {
			e.printStackTrace();
			errorMsg = e.getMessage();
		}
	}
	
	return errorMsg;
}
//	public static String updateUser(Register userUpdate) {
//		LOGGER.info("==updateUser");
//		Connection conn = null;
//		CallableStatement cs = null;
//		
//		String errorMsg = "";
//		try{
//			conn = DBManager.openConnection();
//			
//			cs = conn.prepareCall("{ call PKG_CJOC_USER.UPDATE_USER_FINAL(?,?,?,?,?,?," +
//																			"?,?,?,?,?,?," +
//																			"?,?,?,?,?,?," +
//																			"?,?,?,?,?) }");
//			
//			int i=1;
//    		cs.registerOutParameter(i++, Types.INTEGER);
//            cs.registerOutParameter(i++, Types.VARCHAR);
//            cs.setString(i++, userUpdate.getTitle());
//            cs.setString(i++, userUpdate.getFirstName());
//            cs.setString(i++, userUpdate.getSurName());
//     		cs.setString(i++, userUpdate.getTelephone());
//     		cs.setString(i++, userUpdate.getFax());
//     		cs.setString(i++, userUpdate.getMobilePhone());
//     		cs.setString(i++, userUpdate.getAddress());
//     		cs.setString(i++, userUpdate.getAddress2());
//     		cs.setString(i++, userUpdate.getAddress3());     		
//     		cs.setString(i++, userUpdate.getCity());
//     		cs.setString(i++, userUpdate.getState());
//     		cs.setString(i++, userUpdate.getPostCode());
//     		cs.setString(i++, userUpdate.getCountry());
//     		cs.setString(i++, userUpdate.getAffiliation());
//     		cs.setString(i++, userUpdate.getUserName());
//     		cs.setString(i++, userUpdate.getPassWord());
//     		cs.setString(i++, userUpdate.getEmail());
//     		
//     		if(userUpdate.getPassWord().length() <= 8) {
//    			cs.setString(i++, UnixCrypt.crypt(userUpdate.getPassWord()));
//    		}
//    		else {
//    			cs.setString(i++, userUpdate.getPassWord());
//    		}
//     		
//     		cs.setString(i++, userUpdate.getOfferCode());
//     		cs.setString(i++, ""+userUpdate.getAllowDuplicateEmail());
//     		
//     		// SPSA Email
//     		cs.setString(i++, userUpdate.isSpsaEmail() ? "Y" : "N");
//     		
//     		cs.execute();
//     		
//     		int ret = cs.getInt(1);
//     		
//     		if(ret == -1 || ret == 3 || ret == 4){
//     			errorMsg = cs.getString(2);
//     		}
//		}catch (Exception e) {
//			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
//		} finally {
//			try {
//				DBManager.closeConnection(conn, cs);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		
//		return errorMsg;
//	}
	

	public static void updateBladePrefAndMember(Register userUpdate) {
		LOGGER.info("==updateBladePrefAndMember");
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			conn = DBManager.openConnection();
			
			String sql1 = "UPDATE  BLADE_PREF " +
						  "SET PREF_SPELL_REMIND = ?, " +
								  "PREF_EMAIL_CHECK = ?, " +
								  "PREF_EMAIL_REMIND = ?, " +
								  "PREF_THIRD_PARTY = ? " +
								 // "PREF_COOKIE = ? " +
						  " WHERE MEMBER_ID in (SELECT MEMBER_ID " +
									  		  	"FROM BLADE_MEMBER " +
									  		  	"WHERE MEMBER_USERID = ?)";
			
			ps = conn.prepareStatement(sql1);
			
			
			int x=1;
			ps.setString(x++, userUpdate.getQuestion()); 
			    
     		if (userUpdate.isEmailList()){
    		    ps.setString(x++,"YES");
    		}else{
    		    ps.setString(x++,"NO");
    		}	    	     		   		
     		if(userUpdate.isEmailList()){
     		   ps.setString(x++,"YES");
    		}else{
    		    ps.setString(x++,"NO");
    		}   		
     		if(userUpdate.isThirdParty()){
     		    ps.setString(x++,"YES");
    		}else{
    		    ps.setString(x++,"NO");
    		}
//     		if(userUpdate.isCookie()){
//	     		ps.setString(x++,"YES");
//    		}else{
//    		    ps.setString(x++,"NO");
//	    	}
     		
     		ps.setString(x++, userUpdate.getUserName());
     		
     		ps.executeUpdate();
     		
     		
     		x = 1;
			String updateMember = "UPDATE member SET alert_me = ? WHERE blade_member_id IN " +
											"(SELECT member_id FROM blade_member WHERE member_userid = ?)";
			    
			ps = conn.prepareStatement(updateMember);
			if (userUpdate.isEmailList()){
				ps.setString(x++,"Y");
			} else {
			    ps.setString(x++,"N");
			}
			    
			ps.setString(x++, userUpdate.getUserName());
     		
			ps.executeUpdate();
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void processSubscriptionCode(String memberId, String userName, String offerCode, HttpServletRequest request) {
		String stahlBookSocietyId = System.getProperty("stahl.society.id");
		String societyName = SubscriptionWorker.activateSocietySubscriptions(userName, offerCode, stahlBookSocietyId, request);
		
		if(StringUtils.isNotEmpty(societyName)){
			SubscriptionWorker.insertBookAccess(memberId, ANNUAL_SUBSCRIPTION);
		}
	}

	

	
}
