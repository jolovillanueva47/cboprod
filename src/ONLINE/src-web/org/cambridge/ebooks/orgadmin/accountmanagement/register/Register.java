package org.cambridge.ebooks.orgadmin.accountmanagement.register;

public class Register {
	// set to 1 if duplicate email will be allowed. for SPSA and JHET users
	private int allowDuplicateEmail = 1;
	
	private String title = "";
    private String firstName = "";
    private String surName = "";
    private String org = "";
    
    private String telephone = "";
    private String fax = "";
    private String mobilePhone = "";
    private String address = "";
    private String address2 = "";
    private String address3 = "";
    private String address4 = "";
    private String city = "";
    private String state = "";
    private String stateText = "";
    private String postCode = "";
    private String country = "";
    private String countryText = "";
    private String passWord2 = "";
    private String email= "";
    private String email2= "";
    private String question= "";
    private String offerCode = "";
    private String affiliation = "";
    private String newIpAddr = "";
    private String newInclude = "";
    private String currency = "";
    
    private String categoryList;
    
    private String userName = "";
    private String passWord = "";
    
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	private boolean acceptTerms = false;
    private boolean emailList = false;
    private boolean mailMe = false;
    private boolean thirdParty = false;
    private boolean cookie = false;
    private boolean admin = false;
    private boolean smsAlert = false;
    private boolean spsaEmail = false;
    private boolean emailConsent = false;
    
    private boolean issuealert = false;
    private boolean forthcomingalert = false;
    private String jid="";

    private String oldPassWord;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateText() {
		return stateText;
	}

	public void setStateText(String stateText) {
		this.stateText = stateText;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryText() {
		return countryText;
	}

	public void setCountryText(String countryText) {
		this.countryText = countryText;
	}

	public String getPassWord2() {
		return passWord2;
	}

	public void setPassWord2(String passWord2) {
		this.passWord2 = passWord2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getNewIpAddr() {
		return newIpAddr;
	}

	public void setNewIpAddr(String newIpAddr) {
		this.newIpAddr = newIpAddr;
	}

	public String getNewInclude() {
		return newInclude;
	}

	public void setNewInclude(String newInclude) {
		this.newInclude = newInclude;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public boolean isAcceptTerms() {
		return acceptTerms;
	}

	public void setAcceptTerms(boolean acceptTerms) {
		this.acceptTerms = acceptTerms;
	}

	public boolean isEmailList() {
		return emailList;
	}

	public void setEmailList(boolean emailList) {
		this.emailList = emailList;
	}

	public boolean isMailMe() {
		return mailMe;
	}

	public void setMailMe(boolean mailMe) {
		this.mailMe = mailMe;
	}

	public boolean isThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(boolean thirdParty) {
		this.thirdParty = thirdParty;
	}

	public boolean isCookie() {
		return cookie;
	}

	public void setCookie(boolean cookie) {
		this.cookie = cookie;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isSmsAlert() {
		return smsAlert;
	}

	public void setSmsAlert(boolean smsAlert) {
		this.smsAlert = smsAlert;
	}

	public boolean isSpsaEmail() {
		return spsaEmail;
	}

	public void setSpsaEmail(boolean spsaEmail) {
		this.spsaEmail = spsaEmail;
	}

	public boolean isEmailConsent() {
		return emailConsent;
	}

	public void setEmailConsent(boolean emailConsent) {
		this.emailConsent = emailConsent;
	}

	public boolean isIssuealert() {
		return issuealert;
	}

	public void setIssuealert(boolean issuealert) {
		this.issuealert = issuealert;
	}

	public boolean isForthcomingalert() {
		return forthcomingalert;
	}

	public void setForthcomingalert(boolean forthcomingalert) {
		this.forthcomingalert = forthcomingalert;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getOldPassWord() {
		return oldPassWord;
	}

	public void setOldPassWord(String oldPassWord) {
		this.oldPassWord = oldPassWord;
	}

	public void setAllowDuplicateEmail(int allowDuplicateEmail) {
		this.allowDuplicateEmail = allowDuplicateEmail;
	}

	public int getAllowDuplicateEmail() {
		return allowDuplicateEmail;
	}

	public void setCategoryList(String categoryList) {
		this.categoryList = categoryList;
	}

	public String getCategoryList() {
		return categoryList;
	}
    
    
}
