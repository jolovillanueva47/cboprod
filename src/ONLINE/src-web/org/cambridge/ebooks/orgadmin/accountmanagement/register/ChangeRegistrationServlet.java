package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.util.UnixCrypt;

public class ChangeRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = -5910230138565404510L;
	private static Logger LOGGER = Logger.getLogger(ChangeRegistrationServlet.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		LOGGER.info("==updateRegServlet");
		
		if(StringUtils.isNotEmpty(request.getParameter("update"))){
			OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
			Login login = orgConLinkedMap.getUserLogin();
			
			if(null == login) {
				response.sendRedirect(request.getContextPath() + "/home.jsf");
			} else {
				String offerCode = request.getParameter("offerCode");
				boolean isPromotionCode = thisIsPromotionCode( offerCode );
				
				String username = request.getParameter("userName");
				String password = request.getParameter("oldPassWord");
				
				if(matchOldPassword(username, password)){
					Register userUpdate = setChanges(request);
					
					String errorMsg = ChangeRegistrationWorker.updateUser(userUpdate);
					
					if(StringUtils.isEmpty(errorMsg)){
						ChangeRegistrationWorker.updateBladePrefAndMember(userUpdate);
						
						if( isPromotionCode ){
							String memberId = login.getMemberId();
							ChangeRegistrationWorker.processSubscriptionCode(memberId, userUpdate.getUserName(),offerCode, request);
						}
						RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_management.jsf?page=up_success");
						rd.forward(request, response);
					}
					LOGGER.info(errorMsg);
				}
			}
		} else {
			response.sendRedirect("/orgadmin/account_management.jsf?page=updateReg");
		}
	}


	private boolean matchOldPassword(String username, String password) {
		String oldPassword = ChangeRegistrationWorker.getOldPassword(username);
		boolean matches = UnixCrypt.matches(oldPassword, password)|| oldPassword.equals(password);
		
		return matches;
	}

	private boolean thisIsPromotionCode(String offerCode) {
		if(StringUtils.isEmpty(offerCode)){
			offerCode = "";
		}
		return offerCode.startsWith( "8379" );	
	}
	
	private Register setChanges(HttpServletRequest request) {
		Register reg = new Register();
		reg.setTitle(request.getParameter("title"));
		reg.setFirstName(request.getParameter("firstName"));
		reg.setSurName(request.getParameter("surName"));
		//reg.setOrg(request.getParameter("organisation"));
		reg.setAdmin("on".equals(request.getParameter("admin")));
		reg.setAddress(request.getParameter("admin"));
		reg.setCountry(request.getParameter("country"));
		reg.setState(request.getParameter("state"));
		reg.setCity(request.getParameter("city"));
		reg.setPostCode(request.getParameter("postCode"));
		reg.setAddress(request.getParameter("address"));
		reg.setAddress2(request.getParameter("address2"));
		reg.setTelephone(request.getParameter("telephone"));
		reg.setUserName(request.getParameter("userName"));
		if(StringUtils.isNotEmpty(request.getParameter("passWord"))){
			reg.setPassWord(request.getParameter("passWord"));
		} else {
			reg.setPassWord(request.getParameter("oldPassWord"));
		}
		reg.setEmail(request.getParameter("email"));
		reg.setQuestion(request.getParameter("question"));
		reg.setAcceptTerms("on".equals(request.getParameter("acceptTerms")));
		reg.setEmailList("on".equals(request.getParameter("emailList")));
		reg.setThirdParty("on".equals(request.getParameter("thirdParty")));
		reg.setSmsAlert("on".equals(request.getParameter("smsAlert")));
		reg.setOfferCode(request.getParameter("offerCode"));
		
		return reg;
	}
}
