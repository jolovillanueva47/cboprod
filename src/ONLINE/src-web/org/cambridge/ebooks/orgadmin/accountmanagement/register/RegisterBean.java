package org.cambridge.ebooks.orgadmin.accountmanagement.register;
/**
 * @author cemanalo
 */
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.countrystates.CountryStateMapWorker;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class RegisterBean {
	private static Logger LOGGER = Logger.getLogger(RegisterBean.class);
			
	private LinkedHashMap<String, String> countryList;
	private LinkedHashMap<String, String> countyList;
	private LinkedHashMap<String, String> errorMap;
	
	public String getInitialize(){
		LOGGER.info("===getInitialize");
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		
		if(null==login){
			setCountryList(new CountryStateMapWorker().getCountryMap());
			String countryId = "226"; //unitedstates(default)
			setCountyList(RegisterWorker.getCountyListById(countryId ));
			//setErrorMap(RegisterWorker.getErrorMessages(request));
			
		} else {
//			String url = request.getContextPath() + "/orgadmin/account_management.jsf?page=updateReg";
//			try {
//				 HttpServletResponse response = HttpUtil.getHttpServletResponse();
//				response.sendRedirect(url);
//				//FacesContext.getCurrentInstance().getExternalContext().redirect(url);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			HttpUtil.redirect(request.getContextPath() + "/orgadmin/account_management.jsf?page=updateReg");
		}
		
		
		return "";
	}

	public void setCountyList(LinkedHashMap<String, String> countyList) {
		this.countyList = countyList;
	}

	public LinkedHashMap<String, String> getCountyList() {
		return countyList;
	}

	public void setCountryList(LinkedHashMap<String, String> countryList) {
		this.countryList = countryList;
	}

	public LinkedHashMap<String, String> getCountryList() {
		return countryList;
	}

	public void setErrorMap(LinkedHashMap<String, String> errorMap) {
		this.errorMap = errorMap;
	}

	public LinkedHashMap<String, String> getErrorMap() {
		return errorMap;
	}

	
	

	
}
