package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.internal_reports.InternalReportsWorker;
import org.cambridge.ebooks.online.jpa.user.StringUtil;
import org.cambridge.ebooks.online.jpa.user.User;
import org.cambridge.ebooks.online.login.CJOLoginBean;
import org.cambridge.ebooks.online.login.ConfirmationBean;
import org.cambridge.ebooks.online.login.LoginServlet;
import org.cambridge.ebooks.online.login.RelogBean;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.subscription.FreeTrialBean;
import org.cambridge.ebooks.online.subscription.SubscriptionBean;
import org.cambridge.ebooks.online.user.UserWorker;
import org.cambridge.util.UnixCrypt;
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = -5910230138565404510L;
	
	private static final Logger LOGGER = Logger.getLogger(RegisterServlet.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		LOGGER.info("==registerServlet");
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		
		if(null==orgConLinkedMap.getCurrentlyLoggedBodyId()){
			if (null != request.getParameter("register")){
				
				final int SUCCESS = 0;
				Register register = setRegister(request);
				
				int ret = RegisterWorker.addUser(register);
				LinkedHashMap<String, String> errorMap = RegisterWorker.getErrorMessages(request);
				
				String errorMessage = "";
				if(ret!=SUCCESS){
					switch(ret){
						case 1:
							//duplicate username
							errorMessage = errorMap.get("10001");
							break;
						case 2:
							//duplicate org
							errorMessage = errorMap.get("10002");
							break;
						case 3:
							//duplicate email
							errorMessage = errorMap.get("10003");
							break;
					}
					
					request.setAttribute("errorExist", errorMessage);
					RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_management.jsf?page=register");
					rd.forward(request, response);
					
				} else {
					request.setAttribute("firstName",register.getFirstName());
					request.setAttribute("surName",register.getSurName());
					request.setAttribute("email",register.getEmail());
					
					
					if (register.isAdmin() && StringUtil.isNotEmpty(register.getOrg())){
				    	request.setAttribute("orgNameForIpEntry",register.getOrg());	
				    	//req.setAttribute("memberId",vo.getAthensId());//this is actually the memberID	
				    }
					
					//loginUser(request,response,register);
					RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_management.jsf?page=reg_success");
					rd.forward(request, response);
				}
				
				LOGGER.info("REG");		
			} else {
				response.sendRedirect("/orgadmin/account_management.jsf?page=register");
			}
		} else {
			LOGGER.info("redirect");
		}
	}

	private void loginUser(HttpServletRequest request, HttpServletResponse response, Register register) {
		LOGGER.info("==loginUser");
		String username = register.getUserName();
		String password = UnixCrypt.crypt(register.getPassWord());
		OrganisationWorker.trackOrganisationLogin( request.getSession(false));
		
		LOGGER.info(" -------------------------------------------  Start User login  " );
		User user = LoginServlet.findUserStatic(username, password);
		
		if(StringUtils.isNotEmpty(user.getUsername())) {
			LOGGER.info(" -------------------------------------------  Found user  " + user.getUsername() ); 
			HttpSession session = request.getSession();
			//UserWorker.setUserInSession( session , user );
			UserWorker.recordLoginTime(session);
			LOGGER.info(" ------------------------------------------- " + user.getUsername() + " has logged in" );
			
			LOGGER.info(" ------------------------------------------- BEGIN ConfirmationBean.setDateTermsUpdated" );
			ConfirmationBean.setDateTermsUpdated(session,username,password);
			
			
			//required for seperating Ebooks login from CJO Login
			CJOLoginBean.setEncryptedDetails(request, user.getEncryptedDetails());
			
			//this is required for mulitple login to logout successfully	
			RelogBean.saveLogin(request, RelogBean.MAIN);
			
			//required for sign-up trial link. checks if has subscription
			(new SubscriptionBean()).setSubscription(request);
									
			//for login in login
			InternalReportsWorker.listenEvent(request, response);
			
			//required to recheck free trial on login
			FreeTrialBean.recheckFreeTrial(session);
		}
		
	}

	private Register setRegister(HttpServletRequest request) {
		Register reg = new Register();
		reg.setFirstName(request.getParameter("firstName"));
		reg.setSurName(request.getParameter("surName"));
		reg.setOrg(request.getParameter("organisation"));
		reg.setAdmin("on".equals(request.getParameter("admin")));
		reg.setAddress(request.getParameter("admin"));
		reg.setCountry(request.getParameter("country"));
		reg.setState(request.getParameter("state"));
		reg.setCity(request.getParameter("city"));
		reg.setPostCode(request.getParameter("postCode"));
		reg.setAddress(request.getParameter("address"));
		reg.setAddress2(request.getParameter("address2"));
		reg.setTelephone(request.getParameter("telephone"));
		reg.setUserName(request.getParameter("userName"));
		reg.setPassWord(request.getParameter("passWord"));
		reg.setEmail(request.getParameter("email"));
		reg.setQuestion(request.getParameter("question"));
		reg.setAcceptTerms("on".equals(request.getParameter("acceptTerms")));
		reg.setEmailList("on".equals(request.getParameter("emailList")));
		reg.setThirdParty("on".equals(request.getParameter("thirdParty")));
	
		return reg;
	}
}
