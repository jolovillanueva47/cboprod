package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.StringUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;
import org.cambridge.util.UnixCrypt;

public class RegisterWorker {
	private static Logger LOGGER = Logger.getLogger(RegisterWorker.class);
	
	public static LinkedHashMap<String, String> getCountyListById(String countryId ){
		LinkedHashMap<String, String> countyList = new LinkedHashMap<String, String>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = DBManager.openConnection();
			String sql = "select state_county_id, description from state_county where country = ? order by description asc";
			
			ps = conn.prepareStatement(sql);
		    ps.setString(1, countryId);
		    
		    rs = ps.executeQuery();
		    
		    //set for other
		    countyList.put("0", "Other");
		    while (rs.next()) {
				String key = rs.getString("state_county_id");
				String val = rs.getString("description");
				countyList.put(key, val);
			}
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return countyList;
	}

	public static LinkedHashMap<String, String> getErrorMessages(HttpServletRequest request) {
		LinkedHashMap<String, String> errorList = new LinkedHashMap<String, String>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		LOGGER.info(request.getParameter("email"));
		try{
			conn = DBManager.openConnection();
			String sql = "SELECT error_message_id, error_message FROM error_message_template";
			ps = conn.prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			 while (rs.next()) {
				String key = rs.getString("error_message_id");
				String val = rs.getString("error_message").replaceAll("&lt;", "<").replaceAll("&gt;", ">");
				
				if(val.contains("<username>")){
					val = val.replaceAll("<username>", request.getParameter("userName"));
				}
				if(val.contains("<organisation>")){
					val = val.replaceAll("<organisation>", request.getParameter("organisation"));
				}
				if(val.contains("<email>")){
					val = val.replaceAll("<email>", request.getParameter("email"));
				}
				errorList.put(key, val);
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return errorList;
	}
	
	public static int addUser(Register register) {
		LOGGER.info("addUser");
		
		Connection conn = null;
		CallableStatement cs = null;
		int ret = 0;
		try{
			conn = DBManager.openConnection();
			
			cs = conn.prepareCall("{ call PKG_CJOC_USER.ADD_USER(?,?,?,?,?,?,?,?,?,?," + 
																"?,?,?,?,?,?,?,?,?,?," + 
																"?,?,?,?,?,?,?,?,?,?," +
															    "?,?,?,?,?,?,?,?,?,?)}");
			
			//setCallableStatement(cs, register);
			int i=1;
			cs.registerOutParameter(i++, Types.INTEGER);
			cs.registerOutParameter(i++, Types.INTEGER);
			cs.registerOutParameter(i++, Types.VARCHAR);
			cs.registerOutParameter(i++, Types.VARCHAR);
			cs.registerOutParameter(i++, Types.VARCHAR);
			cs.registerOutParameter(i++, Types.VARCHAR);
			cs.setString(i++, register.getUserName());
			
			if(register.getPassWord().length() <= 8) {
				cs.setString(i++, UnixCrypt.crypt(register.getPassWord()));
			}
			else {
				cs.setString(i++, register.getPassWord());
			}
			
			if (StringUtil.isNotEmpty(register.getOrg())){
			    cs.setString(i++, "AO");
			}else{
			    cs.setString(i++, "rui");
			}
			
			cs.setString(i++, register.getTitle());
			cs.setString(i++, register.getFirstName());
			cs.setString(i++, register.getSurName());
			cs.setString(i++, register.getTelephone());
			cs.setString(i++, register.getFax());
			cs.setString(i++, register.getMobilePhone());
			cs.setString(i++, register.getAffiliation());
			
			if(register.isSmsAlert()) {
				cs.setString(i++,"YES");
			} else {
				cs.setString(i++,"NO");
			}
			
			cs.setString(i++,register.getEmail());	    		
			cs.setString(i++,register.getAddress());
			cs.setString(i++,register.getAddress2());
			//cs.setString(i++,register.getAddress3());
			cs.setString(i++,register.getCity());
			cs.setString(i++,register.getCity());
			cs.setString(i++,register.getPostCode());
			cs.setString(i++,register.getCountry());
			cs.setString(i++,register.getState());
			cs.setString(i++,register.getOrg());
			
		    cs.setString(i++,"YES"); // To Enable User
		    cs.setString(i++,"YES"); // Terms Agreed
		    
		    if (register.isEmailConsent()){
		    cs.setString(i++,"YES");
			}else{
			    cs.setString(i++,"NO");
			}
			    
			if (register.isEmailList()) {
			    cs.setString(i++,"YES");
			} else {
			    cs.setString(i++,"NO");
			}
			
			if (register.isCookie()) {
			    cs.setString(i++,"YES");
			} else {
			    cs.setString(i++,"NO");
			}
			
			if (register.isMailMe()) {
			    cs.setString(i++,"YES");
			} else {
			    cs.setString(i++,"NO");
			}
			
			if (register.isThirdParty()) {
			    cs.setString(i++,"YES");
			} else {
			    cs.setString(i++,"NO");
			}
			
			cs.setString(i++, register.getQuestion());
			cs.setString(i++, register.getOfferCode());
			cs.setString(i++, "false");
			cs.setString(i++, register.getPassWord());
			
			cs.setInt(i++, register.getAllowDuplicateEmail());
			
			// spsa email
		    cs.setString(i++, register.isSpsaEmail() ? "Y" : "N");
		    
		    // category list
		    cs.setString(i++, register.getCategoryList());
			
		    cs.execute();
				   
		    ret = cs.getInt(2);	
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return ret;
	}

}
