package org.cambridge.ebooks.orgadmin.accountmanagement.contentalerts;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class ManageContentAlertsBean {
	private static final Logger LOGGER = Logger.getLogger(ManageContentAlertsBean.class);
	private boolean hasError;
	
	public String getInitialize(){

		LOGGER.info("getInitialize");
    	
    	HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() +"/orgadmin/account_management.jsf?page=register");
//			try {
//				HttpUtil.getHttpServletResponse().sendRedirect("/orgadmin/account_management.jsf?page=register");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
		
		return "";
	}
	
	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}
}
