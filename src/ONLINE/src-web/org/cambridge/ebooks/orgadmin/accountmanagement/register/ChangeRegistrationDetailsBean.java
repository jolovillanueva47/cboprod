package org.cambridge.ebooks.orgadmin.accountmanagement.register;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.countrystates.CountryStateMapWorker;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;


public class ChangeRegistrationDetailsBean {
	
	private Register userDetails;
	private LinkedHashMap<String, String> countryList;
	private LinkedHashMap<String, String> countyList;
	

	private static Logger LOGGER = Logger.getLogger(ChangeRegistrationDetailsBean.class);
	public String getInitialize(){
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		//orgConLinkedMap.get
		if(null==login){
			HttpUtil.redirect(request.getContextPath() + "/orgadmin/account_management.jsf");
			
		} else {
			LOGGER.info("==updateRegisterBean");
			LOGGER.info(login.getMemberId());
			setUserDetails(login.getMemberId());
			setCountryList(new CountryStateMapWorker().getCountryMap());
			String countryId = userDetails.getCountry(); //unitedstates(default)
			setCountyList(RegisterWorker.getCountyListById(countryId ));
		}
		
		
		return "";
	}
	private void setUserDetails(String memberId) {
		Register userDetails = ChangeRegistrationWorker.getUserDetails(memberId);
//		userDetails.setQuestion(otherdetails.getQuestion());
//		userDetails.setFax(otherdetails.getFax());
//		userDetails.setAffiliation(otherdetails.getAffiliation());
//		userDetails.setSmsAlert(otherdetails.isSmsAlert());
//		userDetails.setEmailList(otherdetails.isEmailList());
//		userDetails.setThirdParty(otherdetails.isThirdParty());
//		
		this.userDetails = userDetails;
	}
	
	public void setUserDetails(Register userDetails) {
		this.userDetails = userDetails;
	}
	public Register getUserDetails() {
		return userDetails;
	}
	public LinkedHashMap<String, String> getCountryList() {
		return countryList;
	}
	public void setCountryList(LinkedHashMap<String, String> countryList) {
		this.countryList = countryList;
	}
	public LinkedHashMap<String, String> getCountyList() {
		return countyList;
	}
	public void setCountyList(LinkedHashMap<String, String> countyList) {
		this.countyList = countyList;
	}
}
