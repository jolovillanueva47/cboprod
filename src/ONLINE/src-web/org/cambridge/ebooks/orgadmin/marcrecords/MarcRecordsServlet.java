package org.cambridge.ebooks.orgadmin.marcrecords;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class MarcRecordsServlet extends HttpServlet {
	
	private static final long serialVersionUID = -612840838288504663L;
	private static final Logger LOGGER = Logger.getLogger(MarcRecordsServlet.class);
	private static final String ENCODING = "UTF-8";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info(req.getParameter("orderIds"));
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				
		int option = -1;
		String orderIds = "";
		String dateFrom = "";
		String dateTo = "";
		try  {
			String optionParam = req.getParameter("option");
			if(StringUtils.isNotEmpty(optionParam) && StringUtils.isNumeric(optionParam)) {
				option = Integer.parseInt(optionParam);
			} 
			orderIds = req.getParameter("orderIds");
			dateFrom = req.getParameter("dateFrom");
			dateTo = req.getParameter("dateTo");
			LOGGER.info("======= option:" + option);
			LOGGER.info("======= orderIds:" + orderIds);
			LOGGER.info("======= dateFrom:" + dateFrom);
			LOGGER.info(" ======= dateTo:" + dateTo);
			

		} catch(Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		
		String marcFix = req.getParameter("marcFix");
		if("Y".equals(marcFix)){
			buildMarcRecordsFix(req, resp);
		}else if(-1 == option) {
			// marc record for all published books
			if(noOption(orderIds,dateFrom,dateTo)){
				buildMarcRecords(req, resp);
			}
			else{
				buildAllMarcRecords(req,resp);
				//buildMarcRecords(req, resp);
			}
		} else {
			if(noOption(orderIds,dateFrom,dateTo)){
				buildMarcRecords(option, req, resp);
			}
			else{
				buildMarcRecordsBy(option,req,resp);
			}
		}
	}
		
	private void buildMarcRecords(HttpServletRequest req, HttpServletResponse resp) {
		LOGGER.info("======= buildMarcRecords(...)");
		StringBuffer marcRecordsSql = new StringBuffer(
			"select DISTINCT(MARC_RECORD) from ORACJOC.EBOOKS_ALL_MARC_RECORDS_VIEW");
		
		
   		Connection conn = null;
   		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter writer = null;
		boolean hasRecord = false;
		try {		
			
			conn = DBManager.openConnection();
			pstmt = conn.prepareStatement(marcRecordsSql.toString());
			rs = pstmt.executeQuery();
			
			StringBuffer marcRecords = new StringBuffer();
			
			while (rs.next()) {
				hasRecord = true;
				String record = rs.getString(1);
				String _record =  new String(record.getBytes(ENCODING), ENCODING);
				
				//LogManager.info(this, "marc recordyyy = " + _record);
				marcRecords.append(_record);			
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords");
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			} else {
				//LogManager.info(this, "======= marc records:" + marcRecords.toString());
				writer = getPrintWriter(resp);			
				writer.print(marcRecords.toString());
				writer.flush();
			}
		} catch(Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));	
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(_e));
			}
		} finally {
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	
	private void buildMarcRecordsFix(HttpServletRequest req, HttpServletResponse resp) {
		LOGGER.info("======= buildMarcRecords(...)");
		StringBuffer marcRecordsSql = new StringBuffer(
			"select MARC_RECORD from ORACJOC.EBOOKS_ALL_MARC_RECORDS_VIEW where isbn = ?");
		
		
   		Connection conn = null;
   		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter writer = null;
		boolean hasRecord = false;
		try {		
			
			conn = DBManager.openConnection();
			pstmt = conn.prepareStatement(marcRecordsSql.toString());
			pstmt.setString(1, req.getParameter("isbn"));
			rs = pstmt.executeQuery();
			
			StringBuffer marcRecords = new StringBuffer();
			
			while (rs.next()) {
				hasRecord = true;
				String record = rs.getString(1);				
				String _record = new String(record.getBytes(ENCODING), ENCODING);
				LOGGER.info("marc recordyyy = " + _record);
				marcRecords.append(_record);				
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords");
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			} else {
				LOGGER.info("======= marc records:" + marcRecords.toString());
				writer = getPrintWriter(resp);			
				writer.print(marcRecords.toString());
				writer.flush();
			}
		} catch(Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(_e));
			}
		} finally {		
			
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
			
		}
	}
	
	private void buildMarcRecords(int orgBodyId, HttpServletRequest req, HttpServletResponse resp) {
		LOGGER.info("======= buildMarcRecords(...)");
		StringBuffer marcRecordsSql = new StringBuffer(
			"select DISTINCT MARC_RECORD from oraebooks.EBOOKS_ACCESS_MARC_VIEW where BODY_ID = ?");
		
		
   		Connection conn = null;
   		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter writer = null;
		boolean hasRecord = false;
		try {		
			
			conn = DBManager.openConnection();
			pstmt = conn.prepareStatement(marcRecordsSql.toString());
			pstmt.setInt(1, orgBodyId);
			rs = pstmt.executeQuery();
			
			StringBuffer marcRecords = new StringBuffer();
			while (rs.next()) {
				hasRecord = true;
				String record = rs.getString(1);
				String _record = new String(record.getBytes(ENCODING), ENCODING);
				//LogManager.info(this, "marc recordyyy = " + _record);
				marcRecords.append(_record);			
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords");
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			} else {
				//LogManager.info(this, "======= marc records:" + marcRecords.toString());
				writer = getPrintWriter(resp);			
				writer.print(marcRecords.toString());
				writer.flush();
			}
		} catch(Exception e) {
			LOGGER.error("[buildMarcRecords]:" + e.getMessage());			
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(_e));
			}
		} finally {		
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
			
		}
	}

	private void buildAllMarcRecords(HttpServletRequest req, HttpServletResponse resp) {
		LOGGER.info("======= buildAllMarcRecords(...)");
		StringBuffer marcRecordsSql = new StringBuffer();
		String orderId = "";
		String dateFrom = "";
		String dateTo = "";
		marcRecordsSql.append("select DISTINCT mv.MARC_RECORD from oraebooks.ebooks_access_marc_view mv,oraebooks.ebook_order o where ");
		marcRecordsSql.append("mv.body_id = o.body_id and ");
		LOGGER.info(marcRecordsSql.toString());
		if(!"".equals(req.getParameter("orderIds"))&&req.getParameter("orderIds")!=null ){
			orderId = req.getParameter("orderIds");
			marcRecordsSql.append("(mv.ORDER_ID = " + orderId + ") ");
		}
		if(!"".equals(req.getParameter("dateFrom"))&&req.getParameter("dateFrom")!=null){
			if(!"".equals(req.getParameter("orderIds"))&&req.getParameter("orderIds")!=null ){
				marcRecordsSql.append("OR ");
			}
			dateFrom = getConvertDate(req.getParameter("dateFrom"));
		}
		if(!"".equals(req.getParameter("dateTo"))&&req.getParameter("dateTo")!=null){
			dateTo = getConvertDate(req.getParameter("dateTo"));		
			marcRecordsSql.append("(trunc(o.date_created) between \'" + dateFrom + "\' and \'" + dateTo + "\')");
		}
		LOGGER.info(marcRecordsSql.toString());
//		if("orderId".equals(criteria)){
//		marcRecordsSql.append("select DISTINCT MARC_RECORD from oraebooks.EBOOKS_ACCESS_MARC_VIEW where ORDER_ID = " + orderId);
//		}
//		else if("orderDate".equals(criteria)){
//		marcRecordsSql.append("select DISTINCT mv.MARC_RECORD from oraebooks.ebooks_access_marc_view mv,oraebooks.ebook_order o where "+
//				"mv.body_id = o.body_id and trunc(o.date_created) between \'" + dateFrom + "\' and \'" + dateTo + "\'");
//		System.out.print(marcRecordsSql.toString());
//		}

   		Connection conn = null;
   		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter writer = null;
		boolean hasRecord = false;
		
		try {
			conn = DBManager.openConnection();
			pstmt = conn.prepareStatement(marcRecordsSql.toString());
			rs = pstmt.executeQuery();
			
			StringBuffer marcRecords = new StringBuffer();
			while (rs.next()) {
				hasRecord = true;
				String record = rs.getString(1);
				String _record = new String(record.getBytes(ENCODING), ENCODING);
				//LogManager.info(this, "marc recordyyy = " + _record);
				marcRecords.append(_record);			
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords");
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			} else {
				//LogManager.info(this, "======= marc records:" + marcRecords.toString());
				writer = getPrintWriter(resp);			
				writer.print(marcRecords.toString());
				writer.flush();
			}
		} catch(Exception e) {
			LOGGER.error("[buildMarcRecords]:" + e.getMessage());			
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(_e));
			}
		} finally {		
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
			
		}
	}	

	private void buildMarcRecordsBy(int option, HttpServletRequest req, HttpServletResponse resp) {
		LOGGER.info("======= buildMarcRecordsBy(...)");
		String orderId = "";
		String dateFrom = "";
		String dateTo = "";
		StringBuffer marcRecordsSql = new StringBuffer();
		marcRecordsSql.append("select DISTINCT mv.MARC_RECORD from oraebooks.ebooks_access_marc_view mv,oraebooks.ebook_order o where "+
				"mv.body_id = o.body_id and o.body_id = ? AND (");
		if(!"".equals(req.getParameter("orderIds"))&&null!=req.getParameter("orderIds")){
			orderId = req.getParameter("orderIds");
			marcRecordsSql.append("(mv.ORDER_ID = " + orderId + ") ");
		}
		if(!"".equals(req.getParameter("dateFrom"))&&null!=req.getParameter("dateFrom")){
			if(!"".equals(req.getParameter("orderIds"))&&null!=req.getParameter("orderIds")){
				marcRecordsSql.append("OR ");
			}
			dateFrom = getConvertDate(req.getParameter("dateFrom"));
		}
		if(!"".equals(req.getParameter("dateTo"))&&null!=req.getParameter("dateTo")){
			dateTo = getConvertDate(req.getParameter("dateTo"));
			marcRecordsSql.append("(trunc(o.date_created) between \'" + dateFrom + "\' and \'" + dateTo + "\')");
		}
		marcRecordsSql.append(")");
		//System.out.print(marcRecordsSql.toString());	

//		if("orderId".equals(criteria)){
//		marcRecordsSql.append("select DISTINCT MARC_RECORD from oraebooks.EBOOKS_ACCESS_MARC_VIEW where BODY_ID = ? AND ORDER_ID = " + orderId);
//		}
//		else if("orderDate".equals(criteria)){
//			marcRecordsSql.append("select DISTINCT mv.MARC_RECORD from oraebooks.ebooks_access_marc_view mv,oraebooks.ebook_order o where "+
//					"mv.body_id = o.body_id and o.body_id = ? and trunc(o.date_created) between \'" + dateFrom + "\' and \'" + dateTo + "\'");	
//			}

   		Connection conn = null;
   		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter writer = null;
		boolean hasRecord = false;
		LOGGER.info(marcRecordsSql.toString());
		try {

			conn = DBManager.openConnection();
			pstmt = conn.prepareStatement(marcRecordsSql.toString());
			pstmt.setInt(1, option);
			rs = pstmt.executeQuery();
			
			StringBuffer marcRecords = new StringBuffer();
			while (rs.next()) {
				hasRecord = true;
				String record = rs.getString(1);
				String _record = new String(record.getBytes(ENCODING), ENCODING);
				//LogManager.info(this, "marc recordyyy = " + _record);
				marcRecords.append(_record);			
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords");
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			} else {
				//LogManager.info(this, "======= marc records:" + marcRecords.toString());
				writer = getPrintWriter(resp);			
				writer.print(marcRecords.toString());
				writer.flush();
			}
		} catch(Exception e) {
			LOGGER.error("[buildMarcRecords]:" + e.getMessage());			
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(_e));
			}
		} finally {		
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}

		}
	}		
	
	private PrintWriter getPrintWriter(HttpServletResponse resp) throws IOException {
		
		LOGGER.info("======= getPrintWriter(...)");
		Date date = new Date();
		DateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
		
		resp.setContentType("text/plain");
		resp.setHeader("Content-disposition", "attachment; filename=marc_record_" 
				+ formatter.format(date) + ".mrc");	
		resp.setCharacterEncoding(ENCODING);
		return resp.getWriter();
	}
	
	private String getConvertDate(String thedate){
	/*	//thedate = thedate.replaceAll(" ", "");
		//thedate = thedate.toUpperCase();
		System.out.println("the date: " + thedate);
		//thedate = thedate.replaceAll(" ", "");
		String[] newDate = thedate.split("-");
		String reverseDate = "";
		for(int i = newDate.length-1; i >=0;i--){
			reverseDate += newDate[i];
			if(i>0) reverseDate += "-";
		}
		System.out.print(reverseDate);
		
		 Format frmtr = new SimpleDateFormat("dd-MMM-yy");
		 String s = "";
			try {
				DateFormat df = new SimpleDateFormat("dd-MM-yy");
				Date date = df.parse(reverseDate);
				String d= frmtr.format(date);
				d.toString();
				s = d.toUpperCase();
				 System.out.println(s);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(s); */
		
		return thedate.toUpperCase();	
	}
	
	private boolean noOption(String id, String dateFrom, String dateTo ){
		return ( (null==id||"".equals(id)) && 
				 (null==dateFrom||"".equals(dateFrom)) &&
				 (null==dateTo||"".equals(dateTo)) );
	}
}
