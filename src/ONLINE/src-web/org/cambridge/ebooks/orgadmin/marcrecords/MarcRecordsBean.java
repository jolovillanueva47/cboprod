package org.cambridge.ebooks.orgadmin.marcrecords;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;


public class MarcRecordsBean {
	private static Logger LOGGER = Logger.getLogger(MarcRecordsBean.class);
	public static final String ORG_MAP = "organisationMap";
	private List<Organisation> orgList;
	
	public String getInitBean(){
		LOGGER.info("Initialize...");		
//		HttpServletRequest req = HttpUtil.getHttpServletRequest();				
		
		HttpSession session = HttpUtil.getHttpSession(false);		
		//System.out.println("hello i came first");
		/*
		 * ORG_IDS and ORG_MAP are taken from OrganisationWorker.setOrganisationDisplayInSession		
		 */
		
		//get org ids
		Set<String> orgIds = OrgConLinkedMap.getAllBodyIdsAsSet(session);
		if(orgIds == null || orgIds.size() == 0){
			orgList = null;
			return "";
		}
		
		OrgConLinkedMap map = OrgConLinkedMap.getFromSession(session);
		
		//fills up selected orgs
		List<Organisation> orgList = new ArrayList<Organisation>();			
		addOrgsInSession(map, orgList);
		
		this.orgList = orgList;
		
		LOGGER.info("Finished...");	
		
		return "";
	}
	
	private void addOrgsInSession(OrgConLinkedMap map, List<Organisation> orgList){
		for(String id : map.getMap().keySet()){
			Organisation org = map.getMap().get(id);		
			Organisation _org = new Organisation();
			_org.setBodyId(org.getBodyId());
			_org.setName(org.getBaseProperty(Organisation.DISPLAY_NAME));			
			orgList.add(_org);
		}		
	}
	
	public List<Organisation> getOrgList() {
		return orgList;	
	}
	
}
