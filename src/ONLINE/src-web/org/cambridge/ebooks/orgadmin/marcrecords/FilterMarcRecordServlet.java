package org.cambridge.ebooks.orgadmin.marcrecords;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;


/**
 * Servlet Class
 *
 * @web.servlet              name="FilterMarcRecord"
 *                           display-name="Name for FilterMarcRecord"
 *                           description="Description for FilterMarcRecord"
 * @web.servlet-mapping      url-pattern="/FilterMarcRecord"
 * @web.servlet-init-param   name="A parameter"
 *                           value="A value"
 */
public class FilterMarcRecordServlet extends HttpServlet {	
	private static final long serialVersionUID = -4620205534618929586L;
	private static final Logger LOGGER = Logger.getLogger(FilterMarcRecordServlet.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		resp.setContentType("text/plain");
		LOGGER.info("======= filterMarcrecords");
		int bodyId = 0;
		String filterType = "";
		try  {
			bodyId = Integer.parseInt(req.getParameter("bodyId"));
			filterType = req.getParameter("filterType");
			LOGGER.info("======= orderId:" + bodyId);
		} catch(Exception e) {
			e.printStackTrace();
			LOGGER.error("[doPost]:" + e.getMessage());
		}
	
		if(bodyId!=0){
			LOGGER.info(filterType);
			setOrderId(bodyId,filterType,req,resp);
		}		
	}
	
	private void setOrderId(int bodyId,String filterType, HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException {
		//order types
		final char HAS_ACCESS = 'P'; 
		final char SUBSCRIPTION = 'S';
		final char PATRON_DRIVEN = 'M';
			
		LOGGER.info("======= filterMarcRecord by order id");
		StringBuilder filtermarcRecordsSql = new StringBuilder();
		if(bodyId==-1&&"byOrderId".equals(filterType)){
			filtermarcRecordsSql.append("select  DISTINCT(ORDER_ID) from oraebooks.EBOOKS_ACCESS_MARC_VIEW order by ORDER_ID desc");
		}
		else if (bodyId>0&&"byOrderId".equals(filterType)){
			filtermarcRecordsSql.append("select  DISTINCT(ORDER_ID) ,BODY_ID from oraebooks.EBOOKS_ACCESS_MARC_VIEW where BODY_ID = ").append(bodyId).append(" order by ORDER_ID desc");
		}
		else if (bodyId==-1&&"byDate".equals(filterType)){
			//filtermarcRecordsSql ="select DISTINCT(trunc(DATE_CREATED)) from oraebooks.EBOOK_ORDER order by trunc(date_created) where order_type = ";
			filtermarcRecordsSql.append("select DISTINCT(trunc(DATE_CREATED)) from oraebooks.EBOOK_ORDER ");
			filtermarcRecordsSql.append("where (order_type = '").append(HAS_ACCESS);
			filtermarcRecordsSql.append("' or order_type = '").append(SUBSCRIPTION);
			filtermarcRecordsSql.append("' or order_type = '").append(PATRON_DRIVEN);
			filtermarcRecordsSql.append("') order by trunc(date_created)");
		}
		else if (bodyId>0&&"byDate".equals(filterType)){
			filtermarcRecordsSql.append("select  DISTINCT(trunc(DATE_CREATED)) from oraebooks.EBOOK_ORDER ");
			filtermarcRecordsSql.append("where BODY_ID = ").append(bodyId) ;
			filtermarcRecordsSql.append(" and ");
			filtermarcRecordsSql.append("(order_type = '").append(HAS_ACCESS);
			filtermarcRecordsSql.append("' or order_type = '").append(SUBSCRIPTION);
			filtermarcRecordsSql.append("' or order_type = '").append(PATRON_DRIVEN);
			filtermarcRecordsSql.append("') order by trunc(date_created)");
		}
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PrintWriter out = null;
		
		boolean hasRecord = false;
		try {
			LOGGER.info(filtermarcRecordsSql.toString());
			LOGGER.info(filterType);
			
			conn = DBManager.openConnection();
			
			pstmt = conn.prepareStatement(filtermarcRecordsSql.toString());
			rs = pstmt.executeQuery();
			
			ArrayList results = new ArrayList();
			while (rs.next()) {
				hasRecord=true;
				if("byOrderId".equals(filterType)){
			    	results.add(rs.getInt("ORDER_ID"));
				}
				else if("byDate".equals(filterType)){
					results.add(rs.getDate(1));
				}
			}
			
			if(!hasRecord) {
				LOGGER.info("======= has no record redirect to:" + req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr?pageId=5212&marc_record=true&error=noMarcRecords");
				//resp.sendRedirect(req.getContextPath() + "/action/ebooks/stream?pageId=5212&marc_record=true&error=noMarcRecords&errorMessage=No+MARC+Records+were+found+for+this+organisation.");
			}
			else{
				//System.out.print("orderIdlist -----" + results);
				resp.setContentType("text/plain");
				out = resp.getWriter();
				out.println(results.toString());
				out.flush();
			}
		} catch(Exception e) {
			LOGGER.error("[buildMarcRecords]:" + e.getMessage());			
			try {
				resp.sendRedirect(req.getContextPath() + "/orgadmin/for_librarians.jsf?page=mr&marc_record=true&error=noMarcRecords&errorMessage=Error: " + e.getMessage());
			} catch (Exception _e) {
				LOGGER.error("[buildMarcRecords]:" + _e.getMessage());	
			}
		}
		finally{
			try {
				DBManager.closeConnection(conn, pstmt, rs);
				if(out != null) {
					out.close();
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
	}
	

}


