package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.util.List;

public class CounterReportsBean {
	
	private String site = "";
	private String month = "";
	private String mem_org = "";
	private int accnt_id;
	private int logins;
	private String total_session = "";
	private String average_session = "";
	private int average_pages;
	private int full_content_units_requested;
	private int web_pages;
	private int hits;
	private int queries;
	private String units_reached_from_browse = "";
	private String turnways = "";
	private List<CounterReportsBean> searchResults;
	
	
	public int getAccnt_id() {
		return accnt_id;
	}
	public void setAccnt_id(int accnt_id) {
		this.accnt_id = accnt_id;
	}
	public int getAverage_pages() {
		return average_pages;
	}
	public void setAverage_pages(int average_pages) {
		this.average_pages = average_pages;
	}
	public String getAverage_session() {
		return average_session;
	}
	public void setAverage_session(String average_session) {
		this.average_session = average_session;
	}
	public int getFull_content_units_requested() {
		return full_content_units_requested;
	}
	public void setFull_content_units_requested(int full_content_units_requested) {
		this.full_content_units_requested = full_content_units_requested;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
	public int getLogins() {
		return logins;
	}
	public void setLogins(int logins) {
		this.logins = logins;
	}
	public String getMem_org() {
		return mem_org;
	}
	public void setMem_org(String mem_org) {
		this.mem_org = mem_org;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getQueries() {
		return queries;
	}
	public void setQueries(int queries) {
		this.queries = queries;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getTotal_session() {
		return total_session;
	}
	public void setTotal_session(String total_session) {
		this.total_session = total_session;
	}
	public String getTurnways() {
		return turnways;
	}
	public void setTurnways(String turnways) {
		this.turnways = turnways;
	}
	public String getUnits_reached_from_browse() {
		return units_reached_from_browse;
	}
	public void setUnits_reached_from_browse(String units_reached_from_browse) {
		this.units_reached_from_browse = units_reached_from_browse;
	}
	public int getWeb_pages() {
		return web_pages;
	}
	public void setWeb_pages(int web_pages) {
		this.web_pages = web_pages;
	}
	public List<CounterReportsBean> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(List<CounterReportsBean> searchResults) {
		this.searchResults = searchResults;
	}
	

}
