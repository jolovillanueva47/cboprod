/**
 * 
 */
package org.cambridge.ebooks.orgadmin.administrator.configureip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 *
 */
public class IpValidation {

	private static final Logger LOGGER = Logger.getLogger(IpValidation.class);		
	
	private IpErrorMessage ipErrorMessage;	
	private boolean isValid;
	private boolean hasDuplicates = false;
	
	private List<Ip> validIpList;
	private Map<String, Ip> ipMap;
	
	public IpValidation(List<Ip> ipList, String bodyId) {
		LOGGER.info("===IpValidation");

		ipErrorMessage = new IpErrorMessage(); 
		isValid = true;		
		validIpList = new ArrayList<Ip>();		
		ipMap = new HashMap<String, Ip>();
		
		Map<String, List<Ip>> overlapIpMap = ConfigureIpWorker.selectOverlapIp(ipList);
		List<String> ownIp = ConfigureIpWorker.selectOwnIP(bodyId);
		Map<String, String> invalidDomainMap = ConfigureIpWorker.selectInvalidDomain();
		
		int i = 0;
		for(Ip ip : ipList) {
			ipMap.put(ip.getAddress(), ip);		
			boolean _isValid = validateIp(ipErrorMessage, ownIp, invalidDomainMap, overlapIpMap, ip.getAddress());
			if(isValid && !_isValid) {
				isValid = false;
			}	
			i++;
		}		
		
		// add to list - valid ip
		for(String key : ipMap.keySet()) {
			validIpList.add(ipMap.get(key));
		}
	}
	
	public boolean validateIp(IpErrorMessage ipErrorMessage, List<String> ownIp, Map<String, String> invalidDomainMap, Map<String, List<Ip>> overlapIpMap, String ip) {
		boolean isValid = false;		
		boolean isValidRange = false;
		boolean dup;

		dup = ownIp.contains(ip);
		if(dup) {
			ipErrorMessage.addToMap(IpErrorMessage.DUP_ERRKEY, ip);		
			hasDuplicates = true;
			ipMap.remove(ip);
		} else {
			for(String domainIpAdd : invalidDomainMap.keySet()) {
				if(!IpUtil.validateCidrBlock(ipErrorMessage, domainIpAdd, ip)) {
					isValid = true;
				} else {
					isValid = IpUtil.validateIp(ipErrorMessage, domainIpAdd, ip);
				}
				
				if(!isValid) {
					ipErrorMessage.addToMap(IpErrorMessage.PATTERN_ERRKEY, invalidDomainMap.get(domainIpAdd));
					return isValid;
				}
				
				isValidRange = IpUtil.checkRange(ip);
				
				if (!isValidRange){
					isValid = isValidRange;
					ipErrorMessage.addToMap(IpErrorMessage.RANGE_ERRKEY, ip);
					return isValid;
				}
			}			
		}

		if (isValid) {
			String key = ip.substring(0, ip.indexOf('.'));
			if (null != overlapIpMap) {
				List<Ip> overlapIpList = overlapIpMap.get(key);
				
				for(Ip overlapIp : overlapIpList) {
					boolean overLap = IpUtil.isOverLap(overlapIp.getDomain(), ip);
					
					if (overLap) {
						ipErrorMessage.addToMap(IpErrorMessage.OVERLAP_ERRKEY, ip);
//						if("con".equals(overlapIp.getSource()) && "consortia".equals(overlapIp.getType())) {							
//							ipErrorMessage.addToMap(IpErrorMessage.OVERLAP_ERRKEY, ip + " <a class='red' href='/action/cupadmin/consortia/consortiaDetails?bodyID=" + overlapIp.getBodyId() + "'> " + overlapIp.getName() + "</a><br />");
//						} else if ("con_org".equals(overlapIp.getSource()) && "consortia".equals(overlapIp.getType())){
//							ipErrorMessage.addToMap(IpErrorMessage.OVERLAP_ERRKEY, ip + " <a class='red' href='/action/cupadmin/consortia/consortiaDetails?bodyID=" + overlapIp.getBodyId() + "'> " + overlapIp.getName() + "</a><br />");							
//						} else if ("con_org".equals(overlapIp.getSource()) && "organisation".equals(overlapIp.getType())){
//							ipErrorMessage.addToMap(IpErrorMessage.OVERLAP_ERRKEY, ip + " <a class='red' href='/action/cupadmin/organisation/organisationDetails?bodyID=" + overlapIp.getBodyId() + "'> " + overlapIp.getName() + "</a><br />");
//						} else if ("org".equals(overlapIp.getSource()) && "organisation".equals(overlapIp.getType())) {
//							ipErrorMessage.addToMap(IpErrorMessage.OVERLAP_ERRKEY, ip + " <a class='red' href='/action/cupadmin/organisation/organisationDetails?bodyID=" + overlapIp.getBodyId() + "'> " + overlapIp.getName() +"</a><br />");
//						}
						break;
					}
				}				
			}
		}
		
		return isValid;
	}

	public boolean isValid() {
		return isValid;
	}

	public IpErrorMessage getIpErrorMessage() {
		return ipErrorMessage;
	}

	public boolean hasDuplicates() {
		return hasDuplicates;
	}

	public List<Ip> getValidIpList() {
		return validIpList;
	}	
	
}
