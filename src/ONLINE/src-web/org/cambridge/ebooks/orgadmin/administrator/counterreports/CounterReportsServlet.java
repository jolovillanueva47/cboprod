package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

public class CounterReportsServlet extends HttpServlet  {
	
	private static final Logger LOGGER = Logger.getLogger(CounterReportsServlet.class);
	
	public static final String REPORT = "REPORT";
	
	public static final String YEAR = "YEAR";
	
	public static final String BODY_ID = "BODY_ID";
	
	private static final String INVALID_REPORT_TYPE = "Invalid Report Type";
	
	private static final String USER_INFO = "userInfo";
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6051895689009939296L;
	
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {		
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String reportTypeStr;
		String year;
		String userType;
		String strSessionId;
		String datePicker1;
		String datePicker2;
		String otherRepType;
		String cmd;
		
		try {
			OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);	
			Login login = orgConLinkedMap.getUserLogin();
			
			reportTypeStr = request.getParameter(REPORT);
			year = request.getParameter(YEAR);
			datePicker1 = request.getParameter("datepicker1");
			datePicker2 = request.getParameter("datepicker2");
			otherRepType = request.getParameter("otherRepType");
			cmd = request.getParameter("cmd");
			userType = login.getUserType();

			strSessionId = "";
			if("AC".equals(userType)){
				strSessionId = request.getSession().getId();				
			}

			HttpSession session = request.getSession();
			
			String bodyIdStr = login.getOrgBodyId();	
			
			if(reportTypeStr != null && year != null && bodyIdStr != null && "Run report".equals(cmd)){			
				int bodyId = Integer.parseInt(bodyIdStr);
				int reportType = Integer.parseInt(reportTypeStr);
				ArrayList erbArr = null;
				String csv;
				//summary row is included in the erbArr
				switch (reportType) {
					case 1: 
						erbArr = CounterReportsWorker.getReportQuery(year, bodyId, reportType);
						csv = GenCSVReports.generateReportStmt(erbArr, year, bodyId, userType, request, reportType);
						break;
					case 2: 
						erbArr = CounterReportsWorker.getReportQuery(year, bodyId, reportType);
						csv = GenCSVReports.generateReportStmt(erbArr, year, bodyId, userType, request, reportType);
						break;
					case 3: 
						erbArr = CounterReportsWorker.getReportQuery(year, bodyId, reportType);
						csv = GenCSVReports.generateReportStmt(erbArr, year, bodyId, userType, request, reportType);
						break;
					case 4: 
						erbArr = CounterReportsWorker.getReportQuery(year, bodyId, reportType);
						csv = GenCSVReports.generateReportStmt(erbArr, year, bodyId, userType, request, reportType);
						break;
					case 5: 
						erbArr = CounterReportsWorker.getReport5(year, bodyId);
						csv = GenCSVReports.generateReportStmt56(erbArr, year, bodyId, userType, request, reportType);
						break;
					case 6: 
						erbArr = CounterReportsWorker.getReport6(year, bodyId);
						csv = GenCSVReports.generateReportStmt56(erbArr, year, bodyId, userType, request, reportType);
						break;						
					default: 
						throw new ServletException(INVALID_REPORT_TYPE);
				}

				LOGGER.info("Sending file.. " + getFilename(request) + "; reportType=" + reportType + " year="+year+"; userType="+userType +"; no. of records="+ (erbArr == null ? -1 : erbArr.size()-1));				
				sendFile(request, response, csv);
			}
			else if(otherRepType != null && datePicker1 != null && datePicker2 != null && bodyIdStr != null){
				int bodyId = Integer.parseInt(bodyIdStr);
				int RepTypeInt = Integer.parseInt(otherRepType);
				String csv;
				CounterReportsBean reportVo = new CounterReportsBean();
				
				switch (RepTypeInt) {
				case 1: 
					GenHTMLReports.getExcelReport(datePicker1, datePicker2, bodyId, request, response);
					break;
				case 2:
					csv = GenCSVReports.generateOtherReport1(datePicker1, datePicker2, bodyId, reportVo, request);
					sendFile(request, response, csv);
					break;
				case 3:
					GenExcelReports.getExcelReport(datePicker1, datePicker2, bodyId, request, response);
					break;
				default: 
					throw new ServletException(INVALID_REPORT_TYPE);
				}
			}
			
			//String destination  ="/orgadmin/account_administrator.jsf?type=ao&page=usageStat";        
			//response.sendRedirect(response.encodeRedirectURL(destination));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	/**
	 * send the file to the browser
	 * @param response
	 * @param csv
	 * @throws IOException
	 */
	private void sendFile(HttpServletRequest request, HttpServletResponse response, String csv) throws IOException{	     
	     response.setContentType( "text/csv" );
	     //response.setContentLength( csv.length() );
	     response.setHeader( "Content-Disposition", "attachment; filename=\"" + getFilename(request) + "\"" );

	     PrintWriter pw = response.getWriter();
	     pw.write(csv);
	     
	}
	
	private String getFilename(HttpServletRequest request) {
		String report = request.getParameter(REPORT);
		String year = request.getParameter(YEAR);
		String cmd = request.getParameter("cmd");
		String datePicker1 = request.getParameter("datepicker1");
		String datePicker2 = request.getParameter("datepicker2");
		if("Run report".equals(cmd)){
			return "REPORT" + report + "_" + year + ".csv";
		}else{
			return "OTHER_REPORT_" + datePicker1 + "_to_" + datePicker2 + ".csv";
		}
	}


}
