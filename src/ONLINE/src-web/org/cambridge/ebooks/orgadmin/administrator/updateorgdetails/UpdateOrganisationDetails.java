package org.cambridge.ebooks.orgadmin.administrator.updateorgdetails;

public class UpdateOrganisationDetails {
	private String type;
	private String athensId;
	private String name;
	private String displayName;
	private String displayMessage;
	private String country;
	private String county;
	private String city;
	private String postCode;
	private String address1;
	private String address2;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAthensId() {
		return athensId;
	}

	public void setAthensId(String athensId) {
		this.athensId = athensId;
	}

	public String getType() {
		return type;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCounty() {
		return county;
	}
}
