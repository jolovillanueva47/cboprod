package org.cambridge.ebooks.orgadmin.administrator.configureip;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 */
public class IpUtil {

	private static final Logger LOGGER = Logger.getLogger(IpUtil.class);
	
	private static long MAX_IP = 4294967295l;
	
	private static final String IP_RANGE[] = {
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\*)",
		
		// 23
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 22
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
			
		// 21
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
			
		// 20
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
			
		// 19
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 18
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 17
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 16
		"(\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 15
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 14
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 13
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 12
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 11
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 10
		//"(\\d{1,3}\\.\\*\\.\\*\\.\\*)",
		"(\\d{1,3}\\.\\*\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\.\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3})",
			
		// 9
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*)",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",
			
		// 8
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
				
		// 7
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		// 6
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		// 5
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		// 4
		"(\\*\\.\\d{1,3}\\.\\*\\.\\*)",
		"(\\*\\.\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// 3
		"(\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		// 2
		"(\\*\\.\\*\\.\\d{1,3}\\.\\*)",
		"(\\*\\.\\*\\.\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		// 1
		"(\\*\\.\\*\\.\\*\\.\\*)",
		"(\\*\\.\\*\\.\\*\\.\\d{1,3})",
		"(\\*\\.\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3})",
			
		// ok
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
			
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
				
		"(\\d{1,3}\\.\\*.\\*)",
		"(\\d{1,3}\\.\\*.\\d{1,3})",
		"(\\d{1,3}\\.\\*.\\d{1,3}\\-\\d{1,3})",
			
		"(\\d{1,3}\\.\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		"(\\*\\.\\d{1,3}\\.\\*)",
		"(\\*\\.\\d{1,3}\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",

		"(\\*\\.\\*\\.\\*)",
		"(\\*\\.\\*\\.\\d{1,3})",
		"(\\*\\.\\*\\.\\d{1,3}\\-\\d{1,3})",

		// ok
		"(\\*\\.\\*)",
		"(\\*\\.\\d{1,3})",
		"(\\*\\.\\d{1,3}\\-\\d{1,3})",
			
		//"(\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			
		"(\\d{1,3}\\-\\d{1,3}\\.\\*)",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3})",
		"(\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})"
			   
	};
	
	public static int getIpPattern(String ip) {		
		int flag = 200;
		
		int i = 0;
		for(String range : IP_RANGE) {
			if(ip.matches(range)) {
				flag = i;
			}			
			i++;
		}

		return flag;
	}
	
	public static boolean validateCidrBlock(IpErrorMessage ipErrorMessage, String testIp, String ruleIp) {		
		boolean flag = false;
		
		if(StringUtils.isNotEmpty(ruleIp)) {
			ruleIp = ruleIp.trim();
		}
		String[] ip = ruleIp.split("/");
		if (1 == ip.length) {			
			flag = validateIp(ipErrorMessage, testIp, ruleIp);			
		} else if (2 == ip.length) {
			int cidr = 0;			
			try {				
				if (ip[0].indexOf("0.0.0") > -1) {
					cidr = -1;
				} else {
					cidr = Integer.parseInt(ip[1]);
				}
			} catch (NumberFormatException nfe) {
				ipErrorMessage.addToMap(IpErrorMessage.PATTERN_ERRKEY, ruleIp);
				return flag;
			}
			
			if (cidr >= 0 && cidr <= 32) {
				flag = validateIp(ipErrorMessage, testIp, ip[0]);
			} else {
				ipErrorMessage.addToMap(IpErrorMessage.PATTERN_ERRKEY, ruleIp);
				return flag;
			}
			
		} else {
			ipErrorMessage.addToMap(IpErrorMessage.PATTERN_ERRKEY, ruleIp);
		}
		
		return flag;
	}
	
	public static boolean validateIp(IpErrorMessage ipErrorMessage, String testIp, String ruleIp){
		boolean flag = false;	
		
		if(StringUtils.isNotEmpty(testIp)) {
			testIp = testIp.trim();
		}
		if(StringUtils.isNotEmpty(ruleIp)) {
			ruleIp = ruleIp.trim();
		}
		
		List<String> ipRangeList = getIpRange(ruleIp);
				
		if(ipRangeList.size() > 0) { 
			long ip = ip2Long(testIp);			
			long fromIp = 0;
			long toIp = 0;
			
			try {
				fromIp = ip2Long(ipRangeList.get(0).toString());
				toIp = ip2Long(ipRangeList.get(1).toString());				
			} catch (NumberFormatException e) {
				ipErrorMessage.addToMap(IpErrorMessage.RANGE_ERRKEY, ruleIp);
				return flag;
			}
				
			if (fromIp > MAX_IP || toIp > MAX_IP || fromIp < 0 || toIp < 0) {
				ipErrorMessage.addToMap(IpErrorMessage.RANGE_ERRKEY, ruleIp);
				return flag;
			}
			if (fromIp <= ip &&  toIp >= ip) {
				flag = true;	
			}
		} else {
			ipErrorMessage.addToMap(IpErrorMessage.PATTERN_ERRKEY, ruleIp);
		}		
		
		return flag;
	}
	
	public static boolean checkRange(String ip) {
		boolean isValid = true;
		if(StringUtils.isNotEmpty(ip)) {
			ip = ip.trim();
			String[] splitIpArr = ip.split("[.]");
			if(null != splitIpArr && splitIpArr.length > 0 ) {
				for(String splitIp : splitIpArr) {
					try {
						int ipInt = Integer.parseInt(splitIp);
						if(ipInt > 255 || ipInt < 0) {
							isValid = false;
							break;
						}
					} catch(NumberFormatException e) {
						//ignore
					}
				}			
			}			
		}
		return isValid;
	}
	
	public static boolean isOverLap(String range, String ip) {		
		boolean result = false;
				
		List<String> ipRange = getIpRange(range);
		List<String> testData = getIpRange(ip);
		
		if (null != ipRange && !ipRange.isEmpty() && null != testData && !testData.isEmpty()) {
			long fromRange = ip2Long(ipRange.get(0).toString());
			long toRange = ip2Long(ipRange.get(1).toString());
			long testFrom = ip2Long(testData.get(0).toString());
			long testTo = ip2Long(testData.get(1).toString());
			
			if ((fromRange <= testFrom && toRange >= testFrom)  
				|| (fromRange <= testTo && toRange >= testTo)) {
				result = true;
			}
		}
		return result;
	}
	
	private static long ip2Long(String ip) {		
		long result;
		
		if (!Character.isDigit(ip.charAt(0))) {
			result = -1;
		} else {
			int[] addr = ip2IntArray(ip);
			if (null == addr) {
				result = -1;
			} else {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < addr.length; ++i) {
					sb.append((addr[i] > 0 ? getBinary(Integer.toBinaryString(addr[i])) : "00000000"));
				}
				
				result = Long.parseLong(sb.toString(), 2);
			}
		}
		return result;
	}
	
	private static String getBinary(String ipBinary) {        
        StringBuilder correctIP = new StringBuilder();
        if (StringUtils.isNotEmpty(ipBinary) && ipBinary.length() < 8){
            for(int i=0; i < 8-ipBinary.length(); i++){
                correctIP.append("0");
            }
            correctIP.append(ipBinary);            
        } else { 
            correctIP.append(ipBinary);
        }
        return correctIP.toString();
    }
	
	private static int[] ip2IntArray(String host) {		
		int[] address = {-1, -1, -1, -1};
		StringTokenizer tokens = new StringTokenizer(host, ".");
		if (tokens.countTokens() > 4) {
			address = null;
		} else {
			int i = 0;
			while (tokens.hasMoreTokens()) {
				try {
					address[i++] = Integer.parseInt(tokens.nextToken());
				} catch (NumberFormatException nfe) {
					address = null;
					break;
				}
			}
		}
		return address;
	}
	
	private static List<String> getIpRange(String ipaddr) {
		int flag = getIpPattern(ipaddr);
		
		List<String> ipRangeList = new ArrayList<String>();		
		switch (flag) {
			case 200: {
				break;
			}			
			default: {
				String [] ips = ipaddr.split("[.]");
				int len = ips.length;
				
				String [] limit = null;
				
				String startIp = "";
				String endIp = "";
				
				for(String ip : ips) {
					if ("*".equals(ip)) {
						startIp += (startIp.length() == 0) ? "0" : ".0";
						endIp += (endIp.length() == 0) ? "255" : ".255";
					} else {
						limit = ip.split("[-]");
						if (limit.length > 1) {
							startIp += (startIp.length() == 0) ? limit[0].trim() : "." + limit[0].trim();							
							endIp += (endIp.length() == 0) ? limit[1].trim() : "." + limit[1].trim();
						} else {
							startIp += (startIp.length() == 0) ? limit[0].trim() : "." + limit[0].trim();
							endIp += (endIp.length() == 0) ? limit[0].trim() : "." + limit[0].trim();
						}
					}
					
					limit = null;
				}
				ips = null;
				
				ipRangeList.add(startIp + ((2 == len) ? ".0.0" : (3 == len) ? ".0" : ""));
				ipRangeList.add(endIp + ((2 == len) ? ".255.255" : (3 == len) ? ".255" : ""));
				
				break;	
			}
		}		
		return ipRangeList;		
	}
}
