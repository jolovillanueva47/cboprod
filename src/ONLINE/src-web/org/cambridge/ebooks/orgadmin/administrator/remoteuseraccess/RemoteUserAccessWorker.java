package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PagingUtil;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class RemoteUserAccessWorker {
	private static Logger LOGGER = Logger.getLogger(RemoteUserAccessWorker.class);
	
	public static ArrayList<RemoteUserAccess> getRemoteUserAccess(PagingUtil paging, String orgBodyId){
		LOGGER.info("getRemoteUserAccess");
		
		ArrayList<RemoteUserAccess> resultList = new ArrayList<RemoteUserAccess>();
		
		Connection conn = null;
		CallableStatement cstat = null;
		ResultSet rs = null;
				
		int ret = 0;
		String orderBy = "0";
		
		try {
			
			conn = DBManager.openConnection();
			if(!paging.getSortBy().equals("startDate")){
                orderBy = paging.getSortBy();
            }
			
            cstat = conn.prepareCall("{ call PKG_CJOC_ADMINISTRATOR.GET_REMOTE_USER_ACCESS(?,?,?,?,?,?,?,?) }");
		    int i=1;
            cstat.registerOutParameter(i++, Types.INTEGER);
            cstat.registerOutParameter(i++, Types.VARCHAR);
            cstat.registerOutParameter(i++, OracleTypes.CURSOR);
            cstat.registerOutParameter(i++, Types.INTEGER);
            cstat.setString(i++, orgBodyId);
            cstat.setInt(i++, Integer.parseInt(orderBy));
            LOGGER.info(paging.getFirstRecord());
            cstat.setInt(i++, paging.getFirstRecord());
            cstat.setInt(i++, paging.getLastRecord());
            LOGGER.info(paging.getLastRecord());
		    cstat.execute();
		    
		    ret = cstat.getInt(1);
		    
		    switch (ret) {
			case 0:
			    rs = (ResultSet) cstat.getObject(3);
			    String totalCount = "0";
				while(rs.next()){
					RemoteUserAccess tempRemoteUserAccess = new RemoteUserAccess();

					tempRemoteUserAccess.setMemberId(rs.getString("MEMBER_ID"));
					tempRemoteUserAccess.setUserName(rs.getString("MEMBER_USERID"));
					tempRemoteUserAccess.setFirstName(rs.getString("MEMBER_FIRST_NAME"));
					tempRemoteUserAccess.setSurName(rs.getString("MEMBER_SURNAME"));
					tempRemoteUserAccess.setExpirationDate(rs.getDate("EXPIRATION_DATE"));
					tempRemoteUserAccess.setActivationDate(rs.getDate("ACTIVATION_DATE"));	
				    
				    String status = rs.getString("STATUS");
				    if (null == status){
				        status = "N";
				    }
				    tempRemoteUserAccess.setStatus(status.equals("Y") ? "yes": "no");
				    totalCount = rs.getString("TOTAL_COUNT");
				    resultList.add(tempRemoteUserAccess);
				}
				//param.getPagination().setRecordCount(cstmt.getInt(4));
				paging.setRecordCount(Integer.parseInt(totalCount));
				break;
			
			case 100:
			    throw new Exception(cstat.getString(2));
		    }
		    
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cstat, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultList;
	}
}
