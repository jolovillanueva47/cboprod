package org.cambridge.ebooks.orgadmin.administrator.accessto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

/**
 * @author kmulingtapang
 */
public class AccessWorker {

	private static final Logger LOGGER = Logger.getLogger(AccessWorker.class); 
	
//	private static final int ORDER_CONFIRMED = 4;	
	
	private static final String ORG_PERPETUAL =
		"select distinct al.collection_id, title, description from " +
		"( " +
			"select distinct collection_id, order_id, status, order_type " +
			"from ebooks_access_list_final " +
			"where collection_id is not null " +
			  "and body_id in (?) " + // direct body id is for access_agreement " +
			  "and status = 4 and ORDER_TYPE = 'P' " +			
		") al, " + 
		"ebook_collection_header hdr " +
		"where al.collection_id = hdr.collection_id " +
		"order by title asc";			
	
	private static final String ORG_DEMO =
		"select distinct al.collection_id, title, description from " +
		"( " +		
			"select distinct collection_id, order_id, status, order_type " +
			"from ebooks_access_list_final " +
			"where collection_id is not null " +
			  "and body_id in (?) " + //-- all body ids
			  "and ( " +
			        "status = 0 and ORDER_TYPE = 'D'   " +
			        //" or (status = 4  and ORDER_TYPE = 'T') " +      
			") " +
		") al, " + 
		"ebook_collection_header hdr " +
		"where al.collection_id = hdr.collection_id " +
		"order by title asc";
	
	private static final String ORG_SUBS =
		"select distinct al.collection_id, title, description from " +
		"( " +		
			"select distinct collection_id, order_id, status, order_type " +
			"from ebooks_access_list_final " +
			"where collection_id is not null " +
			  "and body_id in (?) " + //-- all body ids
			  "and ( " +
			       " ORDER_TYPE = 'S' " +      
			") " +
		") al, " + 
		"ebook_collection_header hdr " +
		"where al.collection_id = hdr.collection_id " +
		"order by title asc";
	
	private static final String INDI_ORG =
		"select distinct eb.book_id, al.eisbn, eb.title from " + 
		"( " +
			"select distinct eisbn, order_id, status, order_type " +
			"from ebooks_access_list_final al " +
			"where collection_id is null " +
			" and body_id in (?) " + // direct body id is for access_agreement " +
			  "and status = 4 and ORDER_TYPE = 'P' " +			
		") al, " + 
		"ebook eb " +
		"where al.eisbn = eb.isbn ";
	
	private static final String INDI_DEMO =
		"select distinct eb.book_id, al.eisbn, eb.title from " + 
		"( " +	
			"select distinct eisbn, order_id, status, order_type " +
			"from ebooks_access_list_final " +
			"where collection_id is null " +
			" and body_id in (?) " + //-- all body ids
			  "and ( " +
			        "status = 0 and ORDER_TYPE = 'D'  " +
			        //"  or (status = 4  and ORDER_TYPE = 'T') " +      
			") " +
		") al, " + 
		"ebook eb " +
		"where al.eisbn = eb.isbn ";
	
	private static final String INDI_SUBS =
		"select distinct eb.book_id, al.eisbn, eb.title from " + 
		"( " +	
			"select distinct eisbn, order_id, status, order_type " +
			"from ebooks_access_list_final " +
			"where collection_id is null " +
			" and body_id in (?) " + //-- all body ids
			  "and ( " +			       
			        "   ORDER_TYPE = 'S' " +      
			") " +
		") al, " + 
		"ebook eb " +
		"where al.eisbn = eb.isbn ";
	
//	private static final String FREE_TRIAL =
//		"select ORD.BODY_ID " +
//		"from ORAEBOOKS.ebook_order_trial TR, " +
//			  "ORAEBOOKS.ebook_order ORD  " +
//		"where TR.ORDER_ID = ORD.ORDER_ID " +
//		  "and sysdate between TR.START_DATE and TR.END_DATE " +
//		  "and (BODY_ID in " +     
//	  		"(select CONSORTIUM_ID from ORACJOC.CONSORTIUM_ORGANISATION where ORGANISATION_ID = ?) or " + 
//	  		"BODY_ID = ?)";
	
	
	public static List<BookCollection> findSubsColl(String bodyId, String orderType) {	
		LOGGER.info("===findSubsColl");
		List<BookCollection> resultList = new ArrayList<BookCollection>();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {			
				st = conn.createStatement();
				String sql = "";
				if("P".equals(orderType)) {
					sql = ORG_PERPETUAL.replaceAll("[?]", bodyId);
					LOGGER.info(sql);
				} else if("S".equals(orderType)){
					sql = ORG_SUBS.replaceAll("[?]", bodyId);
					LOGGER.info(sql);
				} else {
					sql = ORG_DEMO.replaceAll("[?]", bodyId);
					LOGGER.info(sql);
				}				
				
				rs = st.executeQuery(sql);				
				while (rs.next()) { 
					BookCollection bookCollection = new BookCollection();
					bookCollection.setCollectionId(rs.getString("COLLECTION_ID"));
					bookCollection.setTitle(rs.getString("TITLE"));
					bookCollection.setDescription(rs.getString("DESCRIPTION"));
					
					resultList.add(bookCollection);
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}	
		
		return resultList;
	}
	
	public static List<Book> findSubsIndi(String bodyId, String orderType) {	
		LOGGER.info("===findSubsIndi");
		
		List<Book> resultList = new ArrayList<Book>();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {
				st = conn.createStatement();
				String sql = "";
				if("P".equals(orderType)) {
					sql = INDI_ORG.replaceAll("[?]", bodyId);
					LOGGER.info(sql);
				} else if("S".equals(orderType)) {
					sql = INDI_SUBS.replaceAll("[?]", bodyId);		
					LOGGER.info(sql);
				} else {
					sql = INDI_DEMO.replaceAll("[?]", bodyId);		
					LOGGER.info(sql);
				}
				
				rs = st.executeQuery(sql);
				while (rs.next()) { 
					Book ebook = new Book();
					ebook.setBookId(rs.getString("BOOK_ID"));
					ebook.setIsbn(rs.getString("EISBN"));
					ebook.setTitle(rs.getString("TITLE"));					
					resultList.add(ebook);
				}
			}
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}	
		
		return resultList;		
	}
	
	public static List<BookCollection> searchCollectionList(String collectionId, String orderType) throws Exception {
		LOGGER.info("===searchCollectionList");
		List<BookCollection> results = new ArrayList<BookCollection>();
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			if(null != conn) {		
				cs = conn.prepareCall("{ call PKG_CBO_CUP_COLLECTION_CONTENT.GET_ALL_ISBNS(?,?,?,?,?,?,?,?,?,?) }");
				cs.registerOutParameter(1, Types.INTEGER);
				cs.registerOutParameter(2, Types.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.setString(4, collectionId);
				cs.setInt(5, 1);
				cs.setInt(6, 999999999);
				cs.registerOutParameter(7, Types.INTEGER);
				cs.setString(8, "0");
				cs.setString(9, "0");
				cs.setString(10, orderType);
				
				cs.execute();
				int numberOfRecords = cs.getInt(1);
				switch (numberOfRecords) {
					case 0:
				   		rs = (ResultSet)cs.getObject(3);
				   		while (rs.next()) {
				   			String isbn = rs.getString("ISBN");
				   			String author = rs.getString("AUTHOR");
				   			String title = rs.getString("TITLE");
				   			String price = rs.getString("PUB_PRICE");
				   			String printDate = rs.getString("PUB_DATE_PRINT");

				   			BookCollection bookCollection = new BookCollection();				   			
				   			bookCollection.setAuthor(author);
				   			bookCollection.setTitle(title);
				   			bookCollection.setIsbn(isbn);
				   			bookCollection.setPrice(price);
				   			bookCollection.setPrintDate(printDate);
				   			
				   			results.add(bookCollection);
				   		}
						break;
					case 100: throw new Exception(cs.getString(2));
					default: LOGGER.error("INVALID RETURN TO PKG_CBO_CUPADMIN_COLLECTIONS o_retcd");
				}
			}
	        
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}	
		return results;
	}
}
