package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.sql.Timestamp;
import java.util.HashMap;

public class ReportsBean {
	
public static final String LOG_ID = "LOG_ID";
	
	public static final String ISBN = "ISBN";
	
	public static final String ISSN = "ISSN";
	
	public static final String TITLE = "TITLE";
	
	public static final String PUBLISHER = "PUBLISHER";
	
	public static final String PLATFORM = "PLATFORM";
	
	public static final String SESSION_ID = "SESSION_ID";
	
	public static final String COMPONENT_ID = "COMPONENT_ID";
	
	public static final String BODY_ID = "BODY_ID";
	
	public static final String TIMESTAMP = "TIMESTAMP";
	
	public static final String SERVICE = "SERVICE";
	
	public static final String IS_SEARCH = "IS_SEARCH";
	
	public static final String JAN = "JAN";
	
	public static final String FEB = "FEB";
	
	public static final String MAR = "MAR";
	
	public static final String APR = "APR";
	
	public static final String MAY = "MAY";	
	
	public static final String JUN = "JUN";
	
	public static final String JUL = "JUL";
	
	public static final String AUG = "AUG";
	
	public static final String SEP = "SEP";
	
	public static final String OCT = "OCT";
	
	public static final String NOV = "NOV";
	
	public static final String DEC = "DEC";
	
	public static final String YTD_BEAN = "ytd";
	
	public static final HashMap HASHMAP = new HashMap();
	
	static{
		HASHMAP.put(LOG_ID, "logId");
		HASHMAP.put(ISBN, "isbn");
		HASHMAP.put(ISSN, "issn");
		HASHMAP.put(TITLE, "title");
		HASHMAP.put(PUBLISHER, "publisher");
		HASHMAP.put(PLATFORM, "platform");
		HASHMAP.put(SESSION_ID, "sessionId");
		HASHMAP.put(COMPONENT_ID, "componentId");
		HASHMAP.put(BODY_ID, "bodyId");
		HASHMAP.put(TIMESTAMP, "timestamp");
		HASHMAP.put(SERVICE, "service");
		HASHMAP.put(IS_SEARCH, "isSearch");
		HASHMAP.put(JAN, "jan");
		HASHMAP.put(FEB, "feb");
		HASHMAP.put(MAR, "mar");
		HASHMAP.put(APR, "apr");
		HASHMAP.put(MAY, "may");
		HASHMAP.put(JUN, "jun");
		HASHMAP.put(JUL, "jul");
		HASHMAP.put(AUG, "aug");
		HASHMAP.put(SEP, "sep");
		HASHMAP.put(OCT, "oct");
		HASHMAP.put(NOV, "nov");
		HASHMAP.put(DEC, "dec");
	}
		
		
	private int logId;
	private String isbn;
	private String issn;
	private String title;
	private String publisher;
	private String platform;
	private String sessionId;
	private String componentId;
	private int bodyId;
	private Timestamp timestamp;
	
	private String service;
	private String isSearch;
	
	private int jan;
	private int feb;
	private int mar;
	private int apr;
	private int may;
	private int jun;
	private int jul;
	private int aug;
	private int sep;
	private int oct;
	private int nov;
	private int dec;
	private int ytd;
	
	
	public ReportsBean() {
		this.logId = 0;
		this.isbn = "";
		this.issn = "";
		this.title = "";
		this.publisher = "";
		this.platform = "";
		this.sessionId = "";
		this.componentId = "";
		this.bodyId = -1;
		this.timestamp = null;
		this.service = "";
		this.isSearch = "";
		this.jan = 0;
		this.feb = 0;
		this.mar = 0;
		this.apr = 0;
		this.may = 0;
		this.jun = 0;
		this.jul = 0;
		this.aug = 0;
		this.sep = 0;
		this.oct = 0;
		this.nov = 0;
		this.dec = 0;
		this.ytd = 0;
		
	
	}
	
	public ReportsBean(int logId, String isbn, String issn, String title,
			String publisher, String platform, String sessionId,
			String componentId, int bodyId, Timestamp timestamp,
			String service, String isSearch, int jan, int feb, int mar,
			int apr, int may, int jun, int jul, int aug, int sep, int oct,
			int nov, int dec, int ytd) {
		super();
		this.logId = logId;
		this.isbn = isbn;
		this.issn = issn;
		this.title = title;
		this.publisher = publisher;
		this.platform = platform;
		this.sessionId = sessionId;
		this.componentId = componentId;
		this.bodyId = bodyId;
		this.timestamp = timestamp;
		this.service = service;
		this.isSearch = isSearch;
		this.jan = jan;
		this.feb = feb;
		this.mar = mar;
		this.apr = apr;
		this.may = may;
		this.jun = jun;
		this.jul = jul;
		this.aug = aug;
		this.sep = sep;
		this.oct = oct;
		this.nov = nov;
		this.dec = dec;
		this.ytd = ytd;
	}
	
	
	
	private String addIsbnFormating(String isbn){
		//TODO check if "" in a cell is forbidden
		return "=\"" + isbn + "\"";
	}
	public String addTitleFormatting(String str){
		return "\"" + str + "\"";		
	}
	
	public int getYtd() {
		return ytd;
	}
	public void setYtd(int ytd) {
		this.ytd = ytd;
	}
	public int getBodyId() {
		return bodyId;
	}
	public void setBodyId(int bodyId) {
		this.bodyId = bodyId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getIsbn() {
		return isbn;
	}
	public String getIsbnFormatted() {
		return addIsbnFormating(isbn);
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getIsSearch() {
		return isSearch;
	}
	public void setIsSearch(String isSearch) {
		this.isSearch = isSearch;
	}
	public String getIssn() {
		return addIsbnFormating(issn);
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public int getLogId() {
		return logId;
	}
	public void setLogId(int logId) {
		this.logId = logId;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public String getTitle() {
		return title;
	}
	public String getTitleFormatted() {
		return addTitleFormatting(title);
	}	
	public void setTitle(String title) {
		this.title = title;
	}
	public int getApr() {
		return apr;
	}
	public void setApr(int apr) {
		this.apr = apr;
	}
	public int getAug() {
		return aug;
	}
	public void setAug(int aug) {
		this.aug = aug;
	}
	public int getDec() {
		return dec;
	}
	public void setDec(int dec) {
		this.dec = dec;
	}
	public int getFeb() {
		return feb;
	}
	public void setFeb(int feb) {
		this.feb = feb;
	}
	public int getJan() {
		return jan;
	}
	public void setJan(int jan) {
		this.jan = jan;
	}
	public int getJul() {
		return jul;
	}
	public void setJul(int jul) {
		this.jul = jul;
	}
	public int getJun() {
		return jun;
	}
	public void setJun(int jun) {
		this.jun = jun;
	}
	public int getMar() {
		return mar;
	}
	public void setMar(int mar) {
		this.mar = mar;
	}
	public int getMay() {
		return may;
	}
	public void setMay(int may) {
		this.may = may;
	}
	public int getNov() {
		return nov;
	}
	public void setNov(int nov) {
		this.nov = nov;
	}
	public int getOct() {
		return oct;
	}
	public void setOct(int oct) {
		this.oct = oct;
	}
	public int getSep() {
		return sep;
	}
	public void setSep(int sep) {
		this.sep = sep;
	}
	

}
