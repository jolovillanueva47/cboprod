package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class NewMemberServlet extends HttpServlet{
	
	private static final long serialVersionUID = -612840838288504663L;
	
	private static final Logger LOGGER = Logger.getLogger(NewMemberServlet.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info(req.getParameter("NewMemberServlet"));
		doPost(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("changeAdministrator");
		//LOGGER.info(request.getParameter("save"));	
		if(StringUtil.isNotEmpty(request.getParameter("save"))){
			OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
			Login login = orgConLinkedMap.getUserLogin();
			
			if(null == login) {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			} else {
				String userType = login.getUserType();
				
				if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
					String orgBodyId = login.getOrgBodyId();
					
					NewMember newMember = setNewMemberFromRequest(request);
					
					String error = NewMemberWorker.saveNewMember(newMember);
					
					if(StringUtils.isEmpty(error)){
						NewMemberWorker.addMemberToOrganisation(orgBodyId, newMember);
					} else {
						if(error.indexOf("ORA-00001: unique constraint")> -1){
							error = modifyErrorMessage(error);
						}
					}
					
					
					if(StringUtils.isNotEmpty(error)){
						request.setAttribute("existError", error);

						RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_administrator.jsf?type=ao&page=newMember");
						rd.forward(request, response);
					} else {
						response.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=remote");
					}
					
				}else {
					response.sendRedirect("/home.jsf");
				}
			}
		} else {
			response.sendRedirect("/home.jsf");
		}
	}

	private String modifyErrorMessage(String error) {
		
		int lastItem = error.indexOf("ORA-00001: unique constraint");
		String key = error.substring(0,lastItem);
		String _error = "That " + key + " is already in use in our system; please try another one, or <a href='" 
			+ "/templates/contact.jsf" 
			+ "'>contact us</a> for more help.";
		return _error;
	}

	private NewMember setNewMemberFromRequest(HttpServletRequest request) {
		NewMember newMember = new NewMember();
		
		if(request.getParameter("title").length()>1){
			newMember.setTitle(request.getParameter("title"));
		}
		newMember.setFirstName(request.getParameter("firstName"));
		newMember.setSurName(request.getParameter("surName"));
		newMember.setCountry(request.getParameter("country"));
		newMember.setUserName(request.getParameter("userName"));
		newMember.setPassWord(request.getParameter("passWord"));
		newMember.setPassWord2(request.getParameter("passWord2"));
		newMember.setEmail(request.getParameter("email"));
		newMember.setEmail2(request.getParameter("email2"));
		newMember.setActivation(request.getParameter("activation"));
		newMember.setExpiration(request.getParameter("expiration"));
		return newMember;
	}
}
