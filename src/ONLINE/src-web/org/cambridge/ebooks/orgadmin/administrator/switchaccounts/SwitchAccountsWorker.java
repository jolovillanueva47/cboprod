package org.cambridge.ebooks.orgadmin.administrator.switchaccounts;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

/**
 * @author cemanalo
 */
public class SwitchAccountsWorker {
	private static Logger LOGGER = Logger.getLogger(SwitchAccountsWorker.class);
	
	public static ArrayList<SwitchAccounts> getAllAccounts(SwitchAccounts account){
		LOGGER.info("===getAllAccounts");
		ArrayList<SwitchAccounts> resultList = new ArrayList<SwitchAccounts>();
		
		Connection conn = null;
		CallableStatement cstat = null;
		ResultSet rs = null;
		
		try {
			conn = DBManager.openConnection();
			
			cstat = conn.prepareCall("{ call ORACJOC.PKG_CJOC_ADMINISTRATOR.GET_ADMIN_ACCOUNTS(?,?) }");
			cstat.setString(1, account.getUserId());
			cstat.registerOutParameter(2, OracleTypes.CURSOR);
			cstat.execute();
			
			rs = (ResultSet) cstat.getObject(2);
			
			while(rs.next()){
				SwitchAccounts tempAccount = new SwitchAccounts();
				tempAccount.setBodyId(rs.getString("ORGANISATION_ID"));
				tempAccount.setDisplayName(rs.getString("DISPLAY_NAME"));
				tempAccount.setDefaultAccount(rs.getString("DEFAULT_FLAG"));
				tempAccount.setConfigureIPDisabled(rs.getString("CONFIGURE_IP_DISABLED"));
				tempAccount.setBodyType(rs.getString("BODY_TYPE"));
				tempAccount.setMemberBodyId(rs.getString("BODY_ID"));
				account.getAccountsList().add(tempAccount);
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				resultList = account.getAccountsList();
				DBManager.closeConnection(conn, cstat, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultList;
	}
	
	public static void updateDefaultAccount(SwitchAccounts account){
		LOGGER.info("==updateDefaultAccount");
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			conn = DBManager.openConnection();
			
			ps = conn.prepareStatement("{ call PKG_CJOC_ADMINISTRATOR.UPDATE_DEFAULT_ACCOUNT(?,?) }");
			ps.setInt(1, Integer.parseInt(account.getBladeMemberId()));
			ps.setInt(2, Integer.parseInt(account.getDefaultAccount()));
			
			ps.execute();
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
}
