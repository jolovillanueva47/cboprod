package org.cambridge.ebooks.orgadmin.administrator.accessto;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

/**
 * @author kmulingtapang
 */
public class AccessServlet extends HttpServlet {

	private static final long serialVersionUID = -5910230138565404510L;
	
	private static final Logger LOGGER = Logger.getLogger(AccessServlet.class);

	public static final String ACTION = "ACTION";
	
	public static final String INDI_ORG = "INDI_ORG";
	
	public static final String COLL_ORG = "COLL_ORG";
	
	public static final String INDI_DEMO = "INDI_DEMO";
	
	public static final String COLL_DEMO = "COLL_DEMO";
	
	public static final String INDI_SUBS = "INDI_SUBS";
	
	public static final String COLL_SUBS = "COLL_SUBS";
	
	public static final String FIND_BOOKS = "FIND_BOOKS";
	
	public static final String TRIAL = "TRIAL";
	
	public static final String COLLECTION_ID = "COLLECTION_ID";
	
	public static final String CONTENT_TYPE = "plain/text;charset=\"UTF-8\"";
	
	public static final String EMPTY = "<<<Empty>>>";
	
	private static final String[] COLL_ORG_KEYS = new String[]{"collectionId","title","description"};
	
	private static final String[] FIND_BOOK_KEYS = new String[]{"bookId","isbn","title"};
	
	public static final String LESS_THAN = "<<<";
	
	public static final String GREATER_THAN = ">>>";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		String bodyIds = orgConLinkedMap.getAllBodyIds();
		
		if(StringUtils.isEmpty(bodyIds)) {
			response.setContentType(CONTENT_TYPE);
			
			Writer writer = response.getWriter();
			String output = "";
			writer.write(StringEscapeUtils.unescapeXml(output));
			writer.close();
		} else {
			try {
				doAccessTo(request, response, bodyIds);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}		
		
	}
	
	private void doAccessTo(HttpServletRequest request, HttpServletResponse response, String bodyId) throws Exception {
		LOGGER.info("===doAccessTo");
		
		String action = request.getParameter(ACTION);
		String sort = request.getParameter("sortBy");
		String output = "";
		if(COLL_ORG.equals(action)) {
			List<BookCollection> list = AccessWorker.findSubsColl(bodyId, "P");
			output = parseItemsList(list, COLL_ORG_KEYS);
		} else if(FIND_BOOKS.equals(action)) {
			String collectionId = request.getParameter(COLLECTION_ID);
			List<BookCollection> list = null;
			if(StringUtils.isNotEmpty(collectionId)) {
				list = AccessWorker.searchCollectionList(collectionId, "");
			}
			
			try {
				list = sortByTitleColl(sortByNumberColl(list, sort), sort);
			} catch (Exception e) {				
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
				throw new ServletException();
			}
			output = parseItemsList(list, FIND_BOOK_KEYS);
		} else if(INDI_ORG.equals(action)) {
			List<Book> list = AccessWorker.findSubsIndi(bodyId, "P");			
			list = sortByTitleIndi(sortByNumberIndi(list, sort), sort);
			output = parseItemsList(list, FIND_BOOK_KEYS);
		} else if(INDI_DEMO.equals(action)) {
			List<Book> list = AccessWorker.findSubsIndi(bodyId, "D");
			list = sortByTitleIndi(sortByNumberIndi(list, sort), sort);
			output = parseItemsList(list, FIND_BOOK_KEYS);
		} else if(COLL_DEMO.equals(action)){
			List<BookCollection> list = AccessWorker.findSubsColl(bodyId, "D");	
			output = parseItemsList(list, COLL_ORG_KEYS);
		} else if(INDI_SUBS.equals(action)){
			List<Book> list = AccessWorker.findSubsIndi(bodyId, "S");
			list = sortByTitleIndi(sortByNumberIndi(list, sort), sort);
			output = parseItemsList(list, FIND_BOOK_KEYS);
		} else if(COLL_SUBS.equals(action)){
			List<BookCollection> list = AccessWorker.findSubsColl(bodyId, "S");	
			output = parseItemsList(list, COLL_ORG_KEYS);
		}		
		
		response.setContentType(CONTENT_TYPE);
		
		Writer writer = response.getWriter();
		output = output.replaceAll("null","");
		writer.write(StringEscapeUtils.unescapeXml(output));
		writer.close();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Book> sortByTitleIndi(List<Book> list, String finalSort){	
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					int result;
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;
					
					if(p1.getTitle().startsWith("\"") && !p2.getTitle().startsWith("\"")) {
						result = -1;
					} else if(!p1.getTitle().startsWith("\"") && p2.getTitle().startsWith("\"")) {
						result = 1;
					} else {
						if(p1.getTitle().startsWith("The")) {
							result = p1.getTitle().substring(4).compareToIgnoreCase(p2.getTitle());							
						} else if(p2.getTitle().startsWith("The")) {
							result = p1.getTitle().compareToIgnoreCase(p2.getTitle().substring(4));
						} else { 
							result = p1.getTitle().compareToIgnoreCase(p2.getTitle());
						}
					}
					
					return result;
				}
			});	
		} else {
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					int result;
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;					
					
					if(p1.getIsbn().startsWith("\"") && !p2.getIsbn().startsWith("\"")) {
						result = -1;
					} else if(!p1.getIsbn().startsWith("\"") && p2.getIsbn().startsWith("\"")) {
						result = 1;
					} else {
						if(p1.getIsbn().startsWith("The")) {
							result = p1.getIsbn().substring(4).compareToIgnoreCase(p2.getIsbn());
						} else if(p2.getIsbn().startsWith("The")) {
							result = p1.getIsbn().compareToIgnoreCase(p2.getIsbn().substring(4));
						} else { 
							result = p1.getIsbn().compareToIgnoreCase(p2.getIsbn());
						}
					}
					
					return result;
				}
			});	
		}
		return list;
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	private List<Book> sortByNumberIndi(List<Book> list, final String finalSort){
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					int result;
					
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;					
					
					if(Character.isDigit(p1.getTitle().charAt(0)) && !Character.isDigit(p2.getTitle().charAt(0))) {
						result = -1;
					} else if(!Character.isDigit(p1.getTitle().charAt(0)) && Character.isDigit(p2.getTitle().charAt(0))) {
						result = 1;
					} else {
						result = p1.getTitle().compareToIgnoreCase(p2.getTitle());				
					}
					
					return result;
				}
			});	
		} else {
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {					
					int result;
					
					Book p1 = (Book) o1;
					Book p2 = (Book) o2;					
					
					if(Character.isDigit(p1.getIsbn().charAt(0)) && !Character.isDigit(p2.getIsbn().charAt(0))) {
						result = -1;
					} else if(!Character.isDigit(p1.getIsbn().charAt(0)) && Character.isDigit(p2.getIsbn().charAt(0))) {
						result = 1;
					} else {
						result = p1.getIsbn().compareToIgnoreCase(p2.getIsbn());				
					}
					
					return result;
				}
			});				
		}
		return list;
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	private List<BookCollection> sortByTitleColl(List<BookCollection> list, String finalSort){	
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {
					int result;
					
					BookCollection p1 = (BookCollection) o1;
					BookCollection p2 = (BookCollection) o2;					
					
					if(p1.getTitle().startsWith("\"") && !p2.getTitle().startsWith("\"")) {
						result = -1;
					} else if(!p1.getTitle().startsWith("\"") && p2.getTitle().startsWith("\"")) {
						result = 1;
					} else {
						if(p1.getTitle().startsWith("The")) {
							result = p1.getTitle().substring(4).compareToIgnoreCase(p2.getTitle());
						} else if(p2.getTitle().startsWith("The")) {
							result = p1.getTitle().compareToIgnoreCase(p2.getTitle().substring(4));
						} else { 
							result = p1.getTitle().compareToIgnoreCase(p2.getTitle());
						}
					}
					
					return result;
				}
			});	
		} else {
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {					
					int result;
					
					BookCollection p1 = (BookCollection) o1;
					BookCollection p2 = (BookCollection) o2;					
					
					if(p1.getIsbn().startsWith("\"") && !p2.getIsbn().startsWith("\"")) {
						result = -1;
					} else if(!p1.getIsbn().startsWith("\"") && p2.getIsbn().startsWith("\"")) {
						result = 1;
					} else {
						if(p1.getIsbn().startsWith("The")) {
							result = p1.getIsbn().substring(4).compareToIgnoreCase(p2.getIsbn());
						} else if(p2.getIsbn().startsWith("The")) {
							result = p1.getIsbn().compareToIgnoreCase(p2.getIsbn().substring(4));
						} else { 
							result =  p1.getIsbn().compareToIgnoreCase(p2.getIsbn());
						}
					}
					
					return result;					
				}
			});	
		}
		return list;
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	private List<BookCollection> sortByNumberColl(List<BookCollection> list, final String finalSort){
		if("1".equals(finalSort)){
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {		
					int result;
					
					BookCollection p1 = (BookCollection) o1;
					BookCollection p2 = (BookCollection) o2;					
					
					if(Character.isDigit(p1.getTitle().charAt(0)) && !Character.isDigit(p2.getTitle().charAt(0))) {
						result = -1;
					} else if(!Character.isDigit(p1.getTitle().charAt(0)) && Character.isDigit(p2.getTitle().charAt(0))) {
						result = 1;
					} else {
						result = p1.getTitle().compareToIgnoreCase(p2.getTitle());				
					}
					
					return result;
				}
			});	
		} else {
			Collections.sort(list, new Comparator(){
				public int compare(Object o1, Object o2) {	
					int result;
					
					BookCollection p1 = (BookCollection) o1;
					BookCollection p2 = (BookCollection) o2;					
					
					if(Character.isDigit(p1.getIsbn().charAt(0)) && !Character.isDigit(p2.getIsbn().charAt(0))) {
						result = -1;
					} else if(!Character.isDigit(p1.getIsbn().charAt(0)) && Character.isDigit(p2.getIsbn().charAt(0))) {
						result = 1;
					} else {
						result = p1.getIsbn().compareToIgnoreCase(p2.getIsbn());				
					}
					
					return result;
				}
			});	
			
		}
		return list;
	}
	
	
//	/**
//	 * parses list of BookCollectionVo and returns string equivalent
//	 * @param list
//	 * @return
//	 */
//	private String parseBookCollection(List list) {
//		if(list.size()==0){
//			return EMPTY;
//		}
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < list.size(); i++) {
//			BookCollectionVo bookCollection = (BookCollectionVo) list.get(i);
//			sb.append(bookCollection.getCollectionId() + LESS_THAN + bookCollection.getTitle() 
//					+ LESS_THAN + bookCollection.getDescription() + GREATER_THAN);
//		}
//		return sb.toString();
//	}
//	
//	private String parseBooks(CollectionsDetailsVo vo){		
//		List list = vo.getSearchResults();
//		if( list.size()==0 ){
//			return EMPTY;
//		}
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < list.size(); i++) {
//			CollectionsDetailsVo _vo = (CollectionsDetailsVo) list.get(i);			
//			sb.append(_vo.getBookId() + LESS_THAN + _vo.getIsbn() + LESS_THAN + _vo.getBookTitle() + GREATER_THAN);
//		}
//		return sb.toString();
//	}
//	
//	private String parseBookIndi(List list){
//		if(list.size()==0){
//			return EMPTY;
//		}
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < list.size(); i++) {
//			EbookBean ebook = (EbookBean) list.get(i);
//			sb.append(ebook.getBookId() + LESS_THAN + ebook.getEisbn() + LESS_THAN + ebook.getTitle() + GREATER_THAN);
//		}
//		return sb.toString();
//	}
	
	private String parseItemsList(List list, String[] keys){
		if(list.size()==0){
			return EMPTY;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			Object object = list.get(i);
			//sb.append(ebook.getBookId() + LESS_THAN + ebook.getEisbn() + LESS_THAN + ebook.getTitle() + GREATER_THAN);			
			for (int j = 0; j < keys.length; j++) {
				String prop = "";
				try {
					prop = BeanUtils.getProperty(object, keys[j]);
				} catch (Exception e) {					
					LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
					prop = "null";
				}
				//escape xml formatting
				//prop = StringEscapeUtils.escapeHtml(prop);
				sb.append(prop);
				if(j != keys.length - 1){
					sb.append(LESS_THAN);
				}				
			}
			sb.append(GREATER_THAN);
		}
		return sb.toString();		
	}	
}
