package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.PagingUtil;

public class RemoteUserAccessBean {
	
	private static final Logger LOGGER = Logger.getLogger(RemoteUserAccessBean.class);
			
    private List<RemoteUserAccess> userList = new ArrayList<RemoteUserAccess>();
    private PagingUtil pagination;
    
    public String getInitialize(){
    	    	
    	HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			if(null==request.getParameter("newMember")){
				LOGGER.info("===getInitialize");
				String userType = login.getUserType();
				
				if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
					String orgBodyId = login.getOrgBodyId();
					
					LinkedHashMap sortList = new LinkedHashMap();
			        
			        sortList.put("1","Last Name");
			        sortList.put("2","First Name");	
			        sortList.put("7","Username");
			        sortList.put("3","Expiration Date");
			        sortList.put("4","Activation Date");
			        
			       setPagination(new PagingUtil(request,sortList));
			       
			       setUserList(RemoteUserAccessWorker.getRemoteUserAccess(getPagination(), orgBodyId));
			       
			       PagingUtil testPage = getPagination();
			       
			       LOGGER.info(testPage);
				} else {
					HttpUtil.redirect(request.getContextPath() + "/home.jsf");
				}
			}
		}
		
    	
    	
    	return "";
    }

    public PagingUtil getPagination() {
        return pagination;
    }

    public void setPagination(PagingUtil pagination) {
        this.pagination = pagination;
    }

    public List<RemoteUserAccess> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<RemoteUserAccess> userList) {
        this.userList = userList;
    }
}
