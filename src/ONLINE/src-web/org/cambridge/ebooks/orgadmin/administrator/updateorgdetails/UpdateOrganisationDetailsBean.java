package org.cambridge.ebooks.orgadmin.administrator.updateorgdetails;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.countrystates.CountryStateMapWorker;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class UpdateOrganisationDetailsBean {
	private static Logger LOGGER = Logger.getLogger(UpdateOrganisationDetailsBean.class);
	
	private LinkedHashMap<String, String> typeList;
	private UpdateOrganisationDetails orgDetails;
	private LinkedHashMap<String, String> countryList;
	private LinkedHashMap<String, String> countyList;
	public String getInitialize(){
		
    	HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			String userType = login.getUserType();
			if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
				LOGGER.info("==getinitialize");
				String bodyId = login.getOrgBodyId();			
			
				setTypeList(UpdateOrganisationWorker.getTypeList());
				setCountryList(new CountryStateMapWorker().getCountryMap());
				if("AO".equals(userType)){
					setOrgDetails(UpdateOrganisationWorker.getOrganisationDetails(bodyId));
				}
				setCountyList(UpdateOrganisationWorker.getCountyListById(orgDetails.getCountry()));
				//setCountyList(CountryStateMapWorker.getStateSelectItems(orgDetails.getCountry()));
				LOGGER.info("stop");
			}else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}
		
		return "";
	}

	public void setTypeList(LinkedHashMap<String, String> typeList) {
		this.typeList = typeList;
	}

	public LinkedHashMap<String, String> getTypeList() {
		return typeList;
	}

	public void setOrgDetails(UpdateOrganisationDetails orgDetails) {
		this.orgDetails = orgDetails;
	}

	public UpdateOrganisationDetails getOrgDetails() {
		return orgDetails;
	}
	
	public LinkedHashMap<String, String> getCountryList() {
		return countryList;
	}

	public void setCountryList(LinkedHashMap<String, String> countryList) {
		this.countryList = countryList;
	}

//	public void setCountyList(ArrayList<SelectItem> countyList) {
//		this.countyList = countyList;
//	}
//
//	public ArrayList<SelectItem> getCountyList() {
//		return countyList;
//	}
//
	public void setCountyList(LinkedHashMap<String, String> countyList) {
		this.countyList = countyList;
	}

	public LinkedHashMap<String, String> getCountyList() {
		return countyList;
	}
}
