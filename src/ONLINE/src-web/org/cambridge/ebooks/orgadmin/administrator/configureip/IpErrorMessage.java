package org.cambridge.ebooks.orgadmin.administrator.configureip;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 */
public class IpErrorMessage {

	public static final String RANGE_ERRKEY = "range";
	public static final String DUP_ERRKEY = "duplicate";
	public static final String PATTERN_ERRKEY = "pattern";
	public static final String OVERLAP_ERRKEY = "overlap";	
	
	private static final Logger LOGGER = Logger.getLogger(IpErrorMessage.class);
	
	private Map<String, String> map;
	
	public IpErrorMessage() {
		map = new HashMap<String, String>();
	}
	
	public void addToMap(String key, String value) {
		LOGGER.info("===addToMap");
		
		String message = map.get(key);
		if(StringUtils.isNotEmpty(message) && StringUtils.isNotEmpty(value)) {
			message += "," + value;
			map.put(key, message);
		} else {
			map.put(key, value);
		}
	}

	public Map<String, String> getMap() {
		return map;
	}	
}
