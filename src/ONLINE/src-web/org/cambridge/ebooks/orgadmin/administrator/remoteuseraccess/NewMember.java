package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

public class NewMember {
	private String memberBodyId = "";
	private String title = "";
    private String firstName = "";
    private String surName = "";
    private String email= "";
    private String email2= "";
    private String userName = "";
    private String passWord = ""; 
    private String passWord2 = "";
    private String country = "";
    private String activation = "";
    private String expiration = "";
    private String address = "-----";
    private String city = "-----";
    private String state = "0";
    private String zipCode = "-----";
    private String question = "-----";
    
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getSurName() {
		return surName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public String getEmail2() {
		return email2;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord2(String passWord2) {
		this.passWord2 = passWord2;
	}
	public String getPassWord2() {
		return passWord2;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	public void setActivation(String activation) {
		this.activation = activation;
	}
	public String getActivation() {
		return activation;
	}
	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}
	public String getExpiration() {
		return expiration;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddress() {
		return address;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return city;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getState() {
		return state;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestion() {
		return question;
	}
	public void setMemberBodyId(String memberBodyId) {
		this.memberBodyId = memberBodyId;
	}
	public String getMemberBodyId() {
		return memberBodyId;
	}
    
}
