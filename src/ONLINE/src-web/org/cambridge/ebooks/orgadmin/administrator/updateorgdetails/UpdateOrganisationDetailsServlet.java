package org.cambridge.ebooks.orgadmin.administrator.updateorgdetails;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;
import org.cambridge.ebooks.online.util.StringUtil;

public class UpdateOrganisationDetailsServlet extends HttpServlet{
	private static final long serialVersionUID = -612840838288504663L;
	private static final Logger LOGGER = Logger.getLogger(UpdateOrganisationDetailsServlet.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info(req.getParameter("NewMemberServlet"));
		doPost(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("updateOrganisationDetails");
		
		if(StringUtil.isNotEmpty(request.getParameter("updates"))){
			OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
			Login login = orgConLinkedMap.getUserLogin();
			
			if(null == login) {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			} else {
				String orgBodyId = login.getOrgBodyId();
				UpdateOrganisationWorker.updateOrganisationDetails(request, orgBodyId);
				
				response.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=orgDetails&success=1");
			}
			
		} else {
			response.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=orgDetails");
		}
	}
}
