/**
 * 
 */
package org.cambridge.ebooks.orgadmin.administrator.configureip;



/**
 * @author kmulingtapang
 *
 */
public class Ip implements Comparable<Ip> {
	private String name;
	private String addressId;
	private String address;
	private String includeFlag;
	
	private String domain;
	private String bodyId;
	private String source;
	private String type;
	
	// The values contain in the flag should 
	// Sort Order 0 - ASC not 0 DESC
	private static String sortOrder = "0";
	private static String sortBy = "address";
	
	private String inOrgIp;	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static String getSortOrder() {
		return sortOrder;
	}

	public static void setSortOrder(String sortOrder) {
		Ip.sortOrder = sortOrder;
	}

	public static String getSortBy() {
		return sortBy;
	}

	public static void setSortBy(String sortBy) {
		Ip.sortBy = sortBy;
	}

	public String getInOrgIp() {
		return inOrgIp;
	}

	public void setInOrgIp(String inOrgIp) {
		this.inOrgIp = inOrgIp;
	}	
	
	public int compareTo(Ip temp) {		
		if("0".equals(Ip.sortOrder)) {
			if("address".equals(sortBy)) {
				return this.address.compareTo(temp.address);
			} else if("name".equals(sortBy)) {
				return this.name.compareTo(temp.name);
			} else {
				return this.addressId.compareTo(temp.addressId);
			} 
		} else {
			if("address".equals(sortBy)) {
				return -(this.address.compareTo(temp.address));
			} else if("name".equals(sortBy)) {
				return -(this.name.compareTo(temp.name));
			} else {
				return -(this.addressId.compareTo(temp.addressId));
			} 
		} 
	}
	
	public boolean equals(Ip temp) {		
		if("address".equals(sortBy)) {
			return this.address.equals(temp.address);
		} else if("name".equals(sortBy)) {
			return this.name.equals(temp.name);
		} else {
			return this.addressId.equals(temp.addressId);
		}
	}

	public String getIncludeFlag() {
		return includeFlag;
	}

	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
}
