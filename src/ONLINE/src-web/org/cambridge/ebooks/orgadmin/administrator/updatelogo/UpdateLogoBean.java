package org.cambridge.ebooks.orgadmin.administrator.updatelogo;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class UpdateLogoBean {
	
	private static final Logger LOGGER = Logger.getLogger(UpdateLogoBean.class);
	
	private String name;
	private boolean image;
	private String bodyId;
	
	public String getInitialize(){
		LOGGER.info("===getInitialize");
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			String userType = login.getUserType();
			
			if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
				setName(orgConLinkedMap.getCurrentlyLoggedDisplayName());
				setImage(UpdateLogoWorker.getImageFlag("ORGANISATION",login.getOrgBodyId()));
				setBodyId(login.getOrgBodyId());
			} else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}
		return "";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public boolean isImage() {
		return image;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getBodyId() {
		return bodyId;
	}
	
}
