package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringEscapeUtils;

public class ReportCSVBean {
	
	private static final String DATE_RUN = "Date run:\n";
	
	private static final String COMMA = ",";	
	
	private static final String NEW_LINE = "\n";
	
	private static final String DASH = "-";
	
	private static final String YTD_TOTAL = "YTD Total";
	
	private static final String NULL = "=\"null\"";
	
	private static final String ISBN_BEAN = "isbn";
	
	private static final String SERVICE_BEAN = "service";
	
	private static final String[] MONTH_ARR = 
		new String[]{
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		};
		
	private String mainHeader;
	private String criteria = CRITERIA;
	private String dateRunHeader = DATE_RUN;
	private String dateRun = getCurrentDate();
	private String columnHeaders;
	private String columnOtherHeaders;
	private String summaryRow;
	private String reportBody;
	
	private String[] reportBodyKeys;
	
	private static final String SDF_FORMAT = "yyyy-MM-dd";	
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(SDF_FORMAT);	 
	
	private static final String CRITERIA = "Cambridge University Org";
	
	private String getCurrentDate() {		
		return SDF.format(new Date());
	}
	
	public String getColumnHeaders() {
		return columnHeaders;
	}
	public void setColumnHeaders(String columnHeaders, String year) {
		this.columnHeaders = columnHeaders + getMonthColHeaders(year);
	}
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	public String getDateRun() {
		return dateRun;
	}
	public String getDateRunFormatted() {
		return addDateFormatting(dateRun);		
	}	
	public void setDateRun(String dateRun) {
		this.dateRun = dateRun;
	}
	public String getDateRunHeader() {
		return dateRunHeader;
	}
	public void setDateRunHeader(String dateRunHeader) {
		this.dateRunHeader = dateRunHeader;
	}
	public String getMainHeader() {
		return mainHeader;
	}
	public void setMainHeader(String mainHeader) {
		this.mainHeader = mainHeader;
	}
	public String getReportBody() {
		return reportBody;
	}
	public void setReportBody(String reportBody) {
		this.reportBody = reportBody;
	}
	public String getSummaryRow() {
		return summaryRow;
	}
	public void setSummaryRow(String summaryRow) {
		this.summaryRow = summaryRow;
	}
	public String[] getReportBodyKeys() {
		return reportBodyKeys;
	}
	public void setReportBodyKeys(String[] reportBodyKeys) {
		this.reportBodyKeys = reportBodyKeys;
	}
	public String addDateFormatting(String str){
		return "=\"" + str + "\"";		
	}
	
	
	/**
	 * generic method for getting body report
	 * @param sb
	 * @param erbArr
	 * @param csvBean
	 * @throws Exception
	 */
	private void getBodyRep(StringBuffer sb, ArrayList erbArr, String year) throws Exception{
		int countEndI = erbArr.size();
		String[] bodyKeys = this.getReportBodyKeys();
		
		//-13 is for removing all months including ytd (12+1), then end month would add the months
		int endMonths = getEndMonth(year);		
		int countEndJ = endMonths != 12 ? bodyKeys.length - 13 + endMonths : bodyKeys.length;
		for(int i=0; i < countEndI; i++ ){
			ReportsBean rb = (ReportsBean)erbArr.get(i);			
			for(int j=0; j < countEndJ; j++ ){				
				writeProp(bodyKeys, sb, rb, j);
			}
			if(endMonths != 12){
				writeProp(bodyKeys, sb, rb, bodyKeys.length - 1);
			}
			sb.append(NEW_LINE);
		}
	}
	
	/**
	 * write the current property
	 * @param bodyKeys
	 * @param sb
	 * @param rb
	 * @param currentBodyKey
	 * @throws Exception
	 */
	private void writeProp(String[] bodyKeys, StringBuffer sb,
			ReportsBean rb, int currentBodyKey) throws Exception{
		String beanPropName = bodyKeys[currentBodyKey];
		String beanPropVal = BeanUtils.getProperty(rb, beanPropName);
		beanPropVal = decodeValue(beanPropVal);
 		if(beanPropVal == null || beanPropVal.equals(NULL)){
			sb.append(COMMA);
		}else{
			sb.append(beanPropVal + COMMA);
		}	
	}
	
	private static final String ENCODING_TYPE = "UTF-8";
	
	private String decodeValue(String val){
		if(val == null || val.trim().length() == 0){
			return val;
		}
		
		String decoded = "";
		try {
			decoded = URLDecoder.decode(val, ENCODING_TYPE);
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
			//impossible if UTF - 8 is unsupported
		}
		return StringEscapeUtils.unescapeHtml(decoded);
	}
	
	/**
	 * checks if should show all year
	 * @param year
	 * @return
	 */
	private int getEndMonth(String year){		
		String currentYear = SDF.format(new Date());
		String[] currentYearArr = currentYear.split(DASH);
		if(!year.equals(currentYearArr[0])){
			//show all months
			return 12;
		}else{
			return Integer.parseInt(currentYearArr[1]);
		}		
	}
	
	/**
	 * generic method for getting the entire report
	 * @param erbArr
	 * @param csvBean
	 * @return
	 * @throws Exception
	 */
	public String generateGenRep(ArrayList erbArr, String year) throws Exception{
		StringBuffer sb  = new StringBuffer();
		//header
		sb.append(this.getMainHeader());
		//criteria
		sb.append(this.criteria + NEW_LINE);	
		//date run header
		sb.append(this.getDateRunHeader());
		//date run
		sb.append(this.getDateRunFormatted() + NEW_LINE);
		//column headers
		sb.append(this.getColumnHeaders());
		
		getBodyRep(sb, erbArr, year);
		
		return sb.toString();
	}
	
	public String generateGenOtherRep(String colHeaders, 
			String sDate, String eDate, int bodyId, CounterReportsBean counterReportVo) throws Exception{
		CounterReportsWorker counterReport = new CounterReportsWorker();
		counterReport.getOtherReport(sDate, eDate, bodyId, counterReportVo);
		int orgNum = 0;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMMMM yyyy");
		String date = sdf1.format(cal.getTime());
		
		StringBuffer sb  = new StringBuffer();
		//header
		sb.append(this.getMainHeader());
		//criteria
		sb.append(this.criteria + NEW_LINE);
		//date run
		sb.append("Date Run: " + date + NEW_LINE);
		//column headers
		sb.append(this.getColumnOtherHeaders()+ NEW_LINE);
		sb.append(COMMA);
		
		
			for(CounterReportsBean repResults : counterReportVo.getSearchResults()){
				if( null == repResults.getSite() || NULL.equals(repResults.getSite())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getSite() + COMMA);
				}
				if( null == repResults.getMonth() || NULL.equals(repResults.getMonth())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getMonth() + COMMA);
				}
				if(null == repResults.getMem_org() || NULL.equals(repResults.getMem_org())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getMem_org() + COMMA);
				}
				if(0 == repResults.getAccnt_id() || NULL.equals(repResults.getAccnt_id())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getAccnt_id() + COMMA);
				}
				if(0 == repResults.getLogins() || NULL.equals(repResults.getLogins())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getLogins() + COMMA);
				}
				if(null == repResults.getTotal_session() || NULL.equals(repResults.getTotal_session())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getTotal_session() + COMMA);
				}
				if(null == repResults.getAverage_session() || NULL.equals(repResults.getAverage_session())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getAverage_session() + COMMA);
				}
				if(0 == repResults.getAverage_pages() || NULL.equals(repResults.getAverage_pages())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getAverage_pages() + COMMA);
				}
				if(0 == repResults.getFull_content_units_requested() || NULL.equals(repResults.getFull_content_units_requested())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getFull_content_units_requested() + COMMA);
				}
				if(0 == repResults.getWeb_pages() || NULL.equals(repResults.getWeb_pages())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getWeb_pages() + COMMA);
				}
				if(0 == repResults.getQueries() || NULL.equals(repResults.getQueries())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getQueries() + COMMA);
				}
				if(null == repResults.getUnits_reached_from_browse() || NULL.equals(repResults.getUnits_reached_from_browse())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getUnits_reached_from_browse() + COMMA);
				}
				if(null == repResults.getTurnways() || NULL.equals(repResults.getTurnways())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getTurnways() + COMMA);
				}
				orgNum++; 
				sb.append(NEW_LINE);
				sb.append(COMMA);
				
			}
			counterReport.getTotalOtherReport(sDate, eDate, bodyId, counterReportVo);
			
			for(CounterReportsBean repResults : counterReportVo.getSearchResults()){
				sb.append("Total" + COMMA );
				sb.append(COMMA);
				sb.append(Integer.toString(orgNum) + COMMA);
				sb.append(COMMA);
				if(0 == repResults.getLogins() || NULL.equals(repResults.getLogins())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getLogins() + COMMA);
				}
				if(null == repResults.getTotal_session() || NULL.equals(repResults.getTotal_session())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getTotal_session() + COMMA);
				}
				if(null == repResults.getAverage_session() || NULL.equals(repResults.getAverage_session())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getAverage_session() + COMMA);
				}
				if(0 == repResults.getAverage_pages() || NULL.equals(repResults.getAverage_pages())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getAverage_pages() + COMMA);
				}
				if(0 == repResults.getFull_content_units_requested() || NULL.equals(repResults.getFull_content_units_requested())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getFull_content_units_requested() + COMMA);
				}
				if(0 == repResults.getWeb_pages() || NULL.equals(repResults.getWeb_pages())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getWeb_pages() + COMMA);
				}
				if(0 == repResults.getQueries() || NULL.equals(repResults.getQueries())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getQueries() + COMMA);
				}
				if(null == repResults.getUnits_reached_from_browse() || NULL.equals(repResults.getUnits_reached_from_browse())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getUnits_reached_from_browse() + COMMA);
				}
				if(null == repResults.getTurnways() || NULL.equals(repResults.getTurnways())){
					sb.append(COMMA);
				}else{
					sb.append(repResults.getTurnways() + COMMA );
				}
				sb.append(NEW_LINE);
			}	
		
		return sb.toString();
	}
	
	/**
	 * gets the month column headers
	 * @param year
	 * @return
	 */
	private String getMonthColHeaders(String year) {
		StringBuffer sb = new StringBuffer();
		int countEnd = getEndMonth(year);
		for(int i=0; i < countEnd; i++){
			sb.append(COMMA + addDateFormatting(MONTH_ARR[i] + DASH + year));			
		}
		sb.append(COMMA + YTD_TOTAL);
		sb.append(NEW_LINE);
		return sb.toString();
	}
	
	public String generateGenRep56(ArrayList erbArrRep56, int reportType, String year) throws Exception{
		StringBuffer sb  = new StringBuffer();
		//header
		sb.append(this.getMainHeader());
		//criteria
		sb.append(this.criteria + NEW_LINE);		
		//date run header
		sb.append(this.getDateRunHeader());
		//date run
		sb.append(this.getDateRunFormatted() + NEW_LINE);
		//column headers
		sb.append(this.getColumnHeaders());
		
		getBodyRep56(sb, erbArrRep56, reportType, year);
		
		return sb.toString();
	}
	
	/**
	 * generic method for getting body report
	 * @param sb
	 * @param erbArr
	 * @param csvBean
	 * @throws Exception
	 */
	private void getBodyRep56(StringBuffer sb, ArrayList erbArrRep56, int reportType, String year) throws Exception{
		ArrayList arrSearch = (ArrayList) erbArrRep56.get(0);
		ArrayList arrSession = (ArrayList) erbArrRep56.get(1);
		
		int countEndI = arrSession.size();
		String[] bodyKeys = this.getReportBodyKeys();
		int endMonths = getEndMonth(year);
		int countEndJ = endMonths != 12 ? bodyKeys.length - 13 + endMonths : bodyKeys.length;
				
		for(int i=0; i < countEndI; i++ ){					
	
			ReportsBean rbSession = (ReportsBean) arrSession.get(i);						
			//write search
			searchForSearch(rbSession, arrSearch, reportType, sb, countEndJ, endMonths);
			//write session
			getBodyRow56(rbSession, countEndJ, bodyKeys, sb, endMonths);
						
			if(i > 0 || reportType == 6){
				sb.append(NEW_LINE);
			}
		}
	}	
	
	
	/**
	 * sessions and search should exists row by row,
	 * this method arranges such arrangement
	 * @param rbSession
	 * @param arrSearch
	 * @param reportType
	 * @param sb
	 * @param countEndJ
	 * @throws Exception
	 */
	private void searchForSearch(ReportsBean rbSession, ArrayList arrSearch, int reportType,
			StringBuffer sb, int countEndJ, int endMonths) throws Exception {
		String beanProp = null;
		if(reportType == 5){
			beanProp = ISBN_BEAN;
		}else if(reportType == 6){
			beanProp = SERVICE_BEAN;
		}	
		int countArrSrch = arrSearch.size();		
		for(int i=0; i < countArrSrch; i++ ){
			ReportsBean rbSearch = (ReportsBean)arrSearch.get(i);
			String sessionIsbn = BeanUtils.getProperty(rbSession,beanProp);
			String searchIsbn = BeanUtils.getProperty(rbSearch, beanProp);
			if((sessionIsbn == null && searchIsbn == null) || sessionIsbn.equals(searchIsbn)){
				getBodyRow56(rbSearch, countEndJ, this.getReportBodyKeys(), sb, endMonths);
				break;
			}
			
		}
	}
	
	
	/**
	 * generates row for report 56
	 * @param rb
	 * @param countEndJ
	 * @param bodyKeys
	 * @param sb
	 * @throws Exception
	 */
	private void getBodyRow56(ReportsBean rb, int countEndJ, String[] bodyKeys, StringBuffer sb, int endMonths) 
			throws Exception{					
		for(int j=0; j < countEndJ; j++ ){
			writeProp(bodyKeys, sb, rb, j);
		}
		if(endMonths != 12){
			writeProp(bodyKeys, sb, rb, bodyKeys.length - 1);
		}		
		
		sb.append(NEW_LINE);
	}

	public String getColumnOtherHeaders() {
		return columnOtherHeaders;
	}

	public void setColumnOtherHeaders(String columnOtherHeaders) {
		this.columnOtherHeaders = columnOtherHeaders;
	}
	
	

}
