package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.login.UnixCrypt;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class NewMemberWorker {
	private static Logger LOGGER = Logger.getLogger(NewMemberWorker.class);
	
	public static String saveNewMember(NewMember newMember) {
		LOGGER.info("==saveNewMember");
		Connection conn = null;
		CallableStatement cs = null;
		String error = "";
		try{
			conn = DBManager.openConnection();
			String sql = "{ call PKG_CJOC_USER.ADD_USER( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
			
			cs = conn.prepareCall(sql.toString());
			int i = 1;
		        cs.registerOutParameter(i++, 4);
		        cs.registerOutParameter(i++, 4);
		        cs.registerOutParameter(i++, 12);
		        cs.registerOutParameter(i++, 12);
		        cs.registerOutParameter(i++, 12);
		        cs.registerOutParameter(i++, 12);
		        cs.setString(i++, newMember.getUserName());
		        
		        if(newMember.getPassWord().length() <= 8) {
		            cs.setString(i++, UnixCrypt.crypt(newMember.getPassWord()));
		        } else {
		             cs.setString(i++, newMember.getPassWord());
		        }
		        cs.setString(i++, "rui");
		        
		        cs.setString(i++, newMember.getTitle());
		        cs.setString(i++, newMember.getFirstName());
		        cs.setString(i++, newMember.getSurName());
		        cs.setString(i++, ""); //newMember.getTelephone()
		        cs.setString(i++, "");//newMember.get("fax")
		        cs.setString(i++, "");//newMember.get("mobilePhone")
		        cs.setString(i++, "");//newMember.get("affiliation")
		        cs.setString(i++, "NO");
		        cs.setString(i++, newMember.getEmail());
		        cs.setString(i++, "");//newMember.get("address")
		        cs.setString(i++, "");//newMember.get("address2")
		        cs.setString(i++, "");//newMember.get("address3")
		        cs.setString(i++, "");//newMember.get("city")
		        cs.setString(i++, newMember.getZipCode());
		        cs.setString(i++, newMember.getCountry());
		        cs.setString(i++, newMember.getState());
		        cs.setString(i++, "");//newMember.get("organisation")
		        cs.setString(i++, "YES");
		        cs.setString(i++, "YES");
		        
		        cs.setString(i++, "NO");//emailConsent
		        cs.setString(i++, "NO");//emailList
		        cs.setString(i++, "NO");//cookie
		        cs.setString(i++, "NO");//mailMe
		        cs.setString(i++, "NO");//thirdparty
		        
		        cs.setString(i++, "");//pVo.getParameter("question")
		        cs.setString(i++, "");//pVo.getParameter("offerCode")
		        cs.setString(i++, "");//pVo.getParameter("admin")
		        cs.setString(i++, newMember.getPassWord());
		        
		        cs.setString(i++, "");// pVo.getParameter("isSPSA")
		        cs.setString(i++, "N");//spsaEmail
		        
		        cs.setString(i++, "");
		        
		        cs.execute();
		        
		        int ret = cs.getInt(2);

		        if(ret==0){
		        	newMember.setMemberBodyId(cs.getString(6));
		        } else {
		        	error = cs.getString(3);
		        }
		        LOGGER.info(error);
		        
		        
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return error;
	}

	public static void addMemberToOrganisation(String orgBodyId, NewMember newMember) {
		LOGGER.info("==addMemberToOorganisation");
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		
		try{
			conn = DBManager.openConnection();
			String sql = "insert into MEMBERSHIP values (?,?,sysdate,'ORACJO_DEV',?,?,'Y')";
			ps = conn.prepareStatement(sql);
		    ps.setString(1, orgBodyId);
		    ps.setString(2, newMember.getMemberBodyId());
		    ps.setDate(3, getDate(newMember.getActivation()));
		    ps.setDate(4, getDate(newMember.getExpiration()));
		    
		   ps.executeUpdate();
			
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
	}
	
	private static Date getDate(String input) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy");
		
		if (input == null || input.trim().length() == 0)
			return null;
		
		return new Date(format.parse(input).getTime());
	}
}
