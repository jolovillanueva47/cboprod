package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

public class GenExcelReports {
	
	public static final String 	GENERAL_DATA 	= "general_data";
	public static final String 	ISBN			= "isbn";
	public static final String 	CURRENCY		= "currency";
	public static final String 	CENTERED_DATA	= "centered_data";
	public static final String 	DATE			= "date";
	public static final int 	CHAR			= 256;

	public static void getExcelReport(String startDate, String endDate, int bodyId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		try {
			
			CounterReportsWorker counterReport = new CounterReportsWorker();
			CounterReportsBean counterReportVo = new CounterReportsBean();
			counterReport.getOtherReport(startDate, endDate, bodyId, counterReportVo);
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
			String date = sdf.format(cal.getTime());
			    
			Workbook wb = new HSSFWorkbook();
			Map<String, CellStyle> styles = createStyles(wb);

			Sheet sheet = wb.createSheet(Integer.toString(bodyId));

			writeData(sheet, styles.get(GENERAL_DATA), 20, 0, 0, "Monthly Breakdown Report");
			//writeData(sheet, styles.get(GENERAL_DATA), "$B$1:$J$1", 0, 1, "");
			
			writeData(sheet, styles.get(GENERAL_DATA), 20, 1, 0, "" + getOrgName(bodyId, request));
			//writeData(sheet, styles.get(GENERAL_DATA), "$B$2:$J$2", 1, 1, "");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 2, 0, "Date Run: " + date);
			//writeData(sheet, styles.get(GENERAL_DATA), "$B$3:$J$3", 2, 1, "");
			//writeData(sheet, styles.get(GENERAL_DATA), 20, 3, 0, "Member Organization");
			//writeData(sheet, styles.get(GENERAL_DATA), "$B$4:$J$4", 3, 1, "");
			//writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 0, "Account ID");
			//writeData(sheet, styles.get(GENERAL_DATA), "$B$5:$J$5", 4, 1, "");
			
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 1, "Site");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 2, "Month");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 3, "Member Organization");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 4, "Account ID");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 5, "Sessions(Logins)");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 6, "Total Session Time (hh:mm:ss)");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 7, "Average Session Time (hh:mm:ss)");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 8, "Average Pages per Session");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 9, "Full Content Units Requested");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 20, "Web Pages Requested");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 11, "Queries(Searches)");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 12, "Full Content Units Reached from Browse");
			writeData(sheet, styles.get(GENERAL_DATA), 20, 4, 13, "Turnaways");
			
			
			int rowNum = 5;
			if(null != counterReportVo.getSearchResults() || "".equals(counterReportVo.getSearchResults())){
				for(CounterReportsBean repResults : counterReportVo.getSearchResults()) {
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 1, repResults.getSite());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 2, repResults.getMonth());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 3, repResults.getMem_org());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 4, Integer.toString(repResults.getAccnt_id()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 5, Integer.toString(repResults.getLogins()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 6, repResults.getTotal_session());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 7, repResults.getAverage_session());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 8, Integer.toString(repResults.getAverage_pages()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 9, Integer.toString(repResults.getFull_content_units_requested()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 10, Integer.toString(repResults.getWeb_pages()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 11, Integer.toString(repResults.getQueries()));
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 12, repResults.getUnits_reached_from_browse());
					writeData(sheet, styles.get(GENERAL_DATA), 20, rowNum, 13, repResults.getTurnways());
					
					rowNum++;
				}
			}else{
				rowNum++;
				writeData(sheet, styles.get(GENERAL_DATA), 40, rowNum, 6, "No Records Found");
			}
	        
				response.setContentType("application/ms-excel");
		        response.setHeader("Content-Disposition", "attachment; filename=Other_Reports_"+startDate+"_to_"+endDate+".xls");
		        ServletOutputStream out = response.getOutputStream();
		        wb.write(out);
		        out.flush();
		        out.close();
	        
		} catch (Exception e) {
			e.printStackTrace();
			//LogManager.error(this, "execute()");
			request.setAttribute("error", e);
		}
		
	}
	
	private static Map<String, CellStyle> createStyles(Workbook wb){
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		
		CellStyle style;
		
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_LEFT);
		styles.put(GENERAL_DATA, style);
		
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setDataFormat(wb.createDataFormat().getFormat("###0"));
		styles.put(ISBN, style);
		
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setDataFormat(wb.createDataFormat().getFormat("##,##0.00"));
		styles.put(CURRENCY, style);
		
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		styles.put(CENTERED_DATA, style);
		
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setDataFormat(wb.createDataFormat().getFormat("dd MMM yyyy"));
		styles.put(DATE, style);

		return styles;
	}
	
	private static void writeData(Sheet sheet, CellStyle style, int width, int rowNum, int colNum, String data) {
		Row row = null == sheet.getRow(rowNum) ? sheet.createRow(rowNum) : sheet.getRow(rowNum);
		sheet.setColumnWidth(colNum, width*CHAR);
		Cell cell = row.createCell(colNum);
		cell.setCellStyle(style);
		cell.setCellValue(data);
	}
	
	private static String getOrgName(int bodyId, HttpServletRequest request) throws Exception {
		String orgName;
		
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		Organisation org = orgConLinkedMap.getMap().get(String.valueOf(bodyId));			
		orgName = org.getBaseProperty(Organisation.DISPLAY_NAME);
		
		return orgName;
		
		//return StringEscapeUtils.unescapeHtml(getOrganisation(bodyId).getName());
	}
	
	/*private static OrganisationDetailsVo getOrganisation(int bodyId) throws Exception{
		OrganisationWorker orgWorker = new OrganisationWorker();
		OrganisationDetailsVo orgVo = new OrganisationDetailsVo();
		orgVo.setBodyID(String.valueOf(bodyId));
		orgWorker.getOrganisationDetails(orgVo);
		return orgVo;
	}*/

}
