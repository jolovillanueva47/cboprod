package org.cambridge.ebooks.orgadmin.administrator.updateorgdetails;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.StringUtil;
import org.json.simple.JSONValue;

public class UpdateCountyListServlet extends HttpServlet {
	private static final long serialVersionUID = -4620205534618929586L;
	private static final Logger LOGGER = Logger.getLogger(UpdateCountyListServlet.class);
	
	public UpdateCountyListServlet() {
		super();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException,
		IOException {
		super.doGet(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException,
	IOException {
		
		LOGGER.info("==UpdateCountyListServlet");
		PrintWriter out = null;
		int countryId = 0;
		try  {
			if(StringUtil.isNotEmpty(request.getParameter("countryId"))){
				countryId = Integer.parseInt(request.getParameter("countryId"));
				LOGGER.info("======= countryId:" + countryId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error("[doPost]:" + e.getMessage());
		}
		
		if(countryId!=0){
			LinkedHashMap<String, String> countyList = UpdateOrganisationWorker.getCountyListById(request.getParameter("countryId"));
			String jsonText = JSONValue.toJSONString(countyList);
			//JSONObject.fromObject(countyList);
			response.setContentType("application/json");
			out = response.getWriter();
			out.write(jsonText);
			out.flush();

		}
	}

//	private LinkedHashMap<String, String> getCountyList(HttpServletRequest request,	HttpServletResponse response) {
//		LinkedHashMap<String, String> countyList = UpdateOrganisationWorker.
//						getCountyListById(request.getParameter("countryId"));
//		
//		
//		return countyList;
//		
//	}

}
