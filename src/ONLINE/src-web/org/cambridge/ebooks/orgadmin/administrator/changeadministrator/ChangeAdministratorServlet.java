package org.cambridge.ebooks.orgadmin.administrator.changeadministrator;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;


public class ChangeAdministratorServlet extends HttpServlet{
	
	private static final long serialVersionUID = -612840838288504663L;
	
	private static final Logger LOGGER = Logger.getLogger(ChangeAdministratorServlet.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info(req.getParameter("changeAdministrator"));
		doPost(req, resp);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("changeAdministrator");
				
		if(null!=request.getParameter("updateAdmin")){
			OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
			Login login = orgConLinkedMap.getUserLogin();
			
			if(null == login) {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			} else {
				String userType = login.getUserType();
				
				if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
					
					StringBuilder error = validate(request);
					if("".equals(error.toString())){
						int ret = 0;
						ret = ChangeAdministratorWorker.updateAdministrator(request,login.getMemberUserId(),login.getOrgBodyId());
						
						if (ret != 0){
		                    switch (ret) {
		                    	case 200: 
		                    	    	error.append("Invalid password for current administrator. <br />");
		                    			break;                    	    
		                    	case 300:
			                    	    error.append("Invalid username for new administrator. <br />");
			                			break; 
		                    	case 400:
			                    	    error.append("Invalid password for new administrator. <br />");
			                			break;
		                    	case 500:
		                    	    	error.append("Username is already an Administrator. <br />");
		                    	    	break;		                			
		                    }
		                    request.setAttribute("error", error.toString());

							RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_administrator.jsf?type=ao&page=admin");
							rd.forward(request, response);
						} else {
							userType = "ruo";
							HttpSession session = request.getSession(true);
		                    session.setAttribute("userType", userType);
		                    
		                    HttpUtil.redirect(request.getContextPath() + "/home.jsf");
						}
					} else {
						request.setAttribute("error", error.toString());

						RequestDispatcher rd = request.getRequestDispatcher("/orgadmin/account_administrator.jsf?type=ao&page=admin");
						rd.forward(request, response);
					}
					
				} else {
					HttpUtil.redirect(request.getContextPath() + "/home.jsf");
				}
			}
		} else {
			response.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=admin");
		}
		
	}


	private StringBuilder validate(HttpServletRequest request) {
		StringBuilder errors = new StringBuilder();
		if(null==request.getParameter("currentPassword")){
			errors.append("Current administrator password is required <br />");
		}
		if(null==request.getParameter("username")){
			errors.append("New administrator username is required <br />");
		}
		
		if(null==request.getParameter("newPassword")){
			errors.append("New administrator password is required <br />");
		}
		if(null==request.getParameter("newConfirmPassword")){
			errors.append("Confirm password is required <br />");
		}
		if(null!=request.getParameter("newPassword")&&null!=request.getParameter("newConfirmPassword")){
	        if(!request.getParameter("newPassword").equals(request.getParameter("newConfirmPassword"))){
	            errors.append("New Passwords should match");
	        }
		}
		return errors;
	}
}
