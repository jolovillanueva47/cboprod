package org.cambridge.ebooks.orgadmin.administrator.switchaccounts;

import java.util.ArrayList;

public class SwitchAccounts {
	private String cmd;
	
	private String defaultAccount;
	private String memberBodyId;
	private String bladeMemberId;
	private String userId;

	private String bodyId;
	private String displayName;
	private String bodyType;
	private String configureIPDisabled;
	
	private ArrayList<SwitchAccounts> accountsList = new ArrayList<SwitchAccounts>();

	public ArrayList<SwitchAccounts> getAccountsList() {
		return accountsList;
	}

	public void setAccountsList(ArrayList<SwitchAccounts> accountsList) {
		this.accountsList = accountsList;
	}

	public String getBladeMemberId() {
		return bladeMemberId;
	}

	public void setBladeMemberId(String bladeMemberId) {
		this.bladeMemberId = bladeMemberId;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getConfigureIPDisabled() {
		return configureIPDisabled;
	}

	public void setConfigureIPDisabled(String configureIPDisabled) {
		this.configureIPDisabled = configureIPDisabled;
	}

	public String getDefaultAccount() {
		return defaultAccount;
	}

	public void setDefaultAccount(String defaultAccount) {
		this.defaultAccount = defaultAccount;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMemberBodyId() {
		return memberBodyId;
	}

	public void setMemberBodyId(String memberBodyId) {
		this.memberBodyId = memberBodyId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
