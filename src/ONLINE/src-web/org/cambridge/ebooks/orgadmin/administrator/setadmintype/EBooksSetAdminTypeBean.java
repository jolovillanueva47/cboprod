package org.cambridge.ebooks.orgadmin.administrator.setadmintype;

/**
 * @author cemanalo
 */

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class EBooksSetAdminTypeBean {
	
	private static final Logger LOGGER = Logger.getLogger(EBooksSetAdminTypeBean.class);

	private String cboAdmin;
	private String cjoAdmin;
	private String bladeMemberId;
	private String bodyId;
	
	
	public String getInitialize() {
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		
		
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			LOGGER.info("===getInitialize");
			String userType = login.getUserType();
			
			if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
				setBodyId(login.getOrgBodyId());
				setBladeMemberId(login.getMemberId());
				
				if("update".equals(request.getParameter("action"))){
					EbooksSetAdminTypeWorker.updateAdmin(request);
				}
				
				Admin admin = EbooksSetAdminTypeWorker.getAdmin(this.getBladeMemberId(),this.getBodyId());
				setCboAdmin(admin.getCboAdmin());
				setCjoAdmin(admin.getCjoAdmin());
			} else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}
		return "";
	}
	
	
	
	public String getCboAdmin() {
		return cboAdmin;
	}

	public void setCboAdmin(String cboAdmin) {
		this.cboAdmin = cboAdmin;
	}

	public String getCjoAdmin() {
		return cjoAdmin;
	}

	public void setCjoAdmin(String cjoAdmin) {
		this.cjoAdmin = cjoAdmin;
	}

	public String getBladeMemberId() {
		return bladeMemberId;
	}

	public void setBladeMemberId(String bladeMemberId) {
		this.bladeMemberId = bladeMemberId;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
}
