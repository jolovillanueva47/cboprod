package org.cambridge.ebooks.orgadmin.administrator.remoteuseraccess;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.countrystates.CountryStateMapWorker;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;


public class NewMemberBean {
	
	private static final Logger LOGGER = Logger.getLogger(NewMemberBean.class);
	
	
	private LinkedHashMap<String, String> countryList = new CountryStateMapWorker().getCountryMap();
	private String activation;
	
	public String getInitialize(){
		LOGGER.info("getInitialize");
    	
    	HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			String userType = login.getUserType();
			if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
				setCountryList(countryList);
				SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMMyyyy");
				setActivation(dateFormatter.format(new Date()));
			} else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}
		
		return "";
	}

	public void setCountryList(LinkedHashMap<String, String> countryList) {
		this.countryList = countryList;
	}

	public LinkedHashMap<String, String> getCountryList() {
		return countryList;
	}

	public void setActivation(String activation) {
		this.activation = activation;
	}

	public String getActivation() {
		return activation;
	}


}
