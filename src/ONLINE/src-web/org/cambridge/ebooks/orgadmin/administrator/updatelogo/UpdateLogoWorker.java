package org.cambridge.ebooks.orgadmin.administrator.updatelogo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class UpdateLogoWorker {
	private static final String ORG_LOGO_DIR = System.getProperty("org.logo.dir");
	
	private static final Logger LOGGER = Logger.getLogger(UpdateLogoWorker.class);
	
	public static boolean getImageFlag(String table, String bodyId){
		LOGGER.info("getImageFlag");
		
		Connection conn = null;
		PreparedStatement pstat = null;
		ResultSet rs = null;
		String image = "N";
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT IMAGE ")
				.append("FROM ").append(table)
				.append(" WHERE BODY_ID = ?");
			
			pstat = conn.prepareStatement(sql.toString());
			pstat.setString(1, bodyId);
			
			rs = pstat.executeQuery();
			
			while(rs.next()){
				image = (null == rs.getString("IMAGE")? "N": rs.getString("IMAGE"));
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			
				try {
					DBManager.closeConnection(conn, pstat, rs);
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		LOGGER.info(image.equals("Y"));
		return (image.equals("Y"));
	}
	public static void uploadLogo(UpdateLogoParam logoParam, String table) {
		LOGGER.info("UploadLogo");
		
		String fileName = logoParam.getLogo().getName();
		int fileSize = (int)logoParam.getLogo().getSize();
		
		if(!"".equals(fileName) || fileSize != 0){
			saveLogo(logoParam);
			setLogo(logoParam.getBodyId(),"Y",table);
		}

	}

	private static void setLogo(String bodyId, String imageFlag, String table) {
		LOGGER.info("==setLogo");
		Connection conn = null;
		PreparedStatement pstat = null;
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE ").append(table)
				.append(" SET IMAGE = '").append(imageFlag).append("' ")
				.append("WHERE BODY_ID = ?");
			
			pstat = conn.prepareStatement(sql.toString());
			pstat.setString(1,bodyId);			
			pstat.executeUpdate();
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, pstat);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}

	private static void saveLogo(UpdateLogoParam logoParam){
		OutputStream bos = null;
		InputStream stream =null;
		FileItem logo = logoParam.getLogo();
		
		try{
			if(null != logo){
				stream = logo.getInputStream();
				bos = new FileOutputStream(getLogoFile(logoParam.getBodyId()));
				int bytesRead = 0;
		        byte[] buffer = new byte[8192];
		        
		        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
		            bos.write(buffer, 0, bytesRead);
		        }
			}
		}  catch (Exception e) {
			e.printStackTrace();
		} finally{
			try{
				if (null!=bos)bos.close();
				if (null!=stream) stream.close();
			}  catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}

	private static String getLogoFile(String bodyId) {
		return (ORG_LOGO_DIR + "org" + bodyId);
	}
	
	public static void removeLogo(UpdateLogoParam logoParam, String table) {
		setLogo(logoParam.getBodyId(),"N",table);		
	}
}
