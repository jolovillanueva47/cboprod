package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GenHTMLReports {
	
public static void getExcelReport(String startDate, String endDate, int bodyId, HttpServletRequest request, HttpServletResponse response ) {
		
		CounterReportsWorker counterReport = new CounterReportsWorker();
		CounterReportsBean counterReportVo = new CounterReportsBean();
		int orgNum = 0;
		try {
			counterReport.getOtherReport(startDate, endDate, bodyId, counterReportVo);
			String delimeter = "\n";
			StringBuilder output = new StringBuilder();
			
			output.append("<html>" + delimeter);
			output.append("<head>" + delimeter);		
			
			output.append("<title>Monthly Breakdown Report</title>" + delimeter);
			output.append("</head>" + delimeter);
			output.append("<body bgcolor=\"#FFFFFF\" text=\"#000000\">" + delimeter);
			output.append("<b><center>Monthly Breakdown Report</center></b>" + delimeter);
			output.append("<center><b>Period:</b>" + startDate + " - " + endDate + "</center><br /><br />" + delimeter);
			
			output.append("<table>");
			output.append("<tr><td width=150>Site</td>" +
						  "<td width=100>Month</td>" +
						  "<td width=150>Member Organisation</td>" +
						  "<td width=100>Account ID</td>" +
						  "<td width=150>Sessions (Logins)</td>" +
						  "<td width=150>Total Session Time (hh:mm:ss)</td>" +
						  "<td width=150>Average Session Time (hh:mm:ss)</td>" +
						  "<td width=150>Average Pages per Session</td>" +
						  "<td width=150>Full-Content Units Requested</td>" +
						  "<td width=150>Web Pages Requested</td>" +
						  "<td width=100>Queries (Searches)</td>" +
						  "<td width=150>Full Content Units Reached from Browse</td>" +
						  "<td width=100>Turnaways</td></tr>");
			
			
				for(CounterReportsBean repResults : counterReportVo.getSearchResults()) {
					
					output.append("<tr><td width=150>"+repResults.getSite()+"</td>" +
							  "<td width=100>"+repResults.getMonth()+"</td>" +
							  "<td width=150>"+repResults.getMem_org()+"</td>" +
							  "<td width=100>"+repResults.getAccnt_id()+"</td>" +
							  "<td width=150>"+repResults.getLogins()+"</td>" +
							  "<td width=150>"+repResults.getTotal_session()+"</td>" +
							  "<td width=150>"+repResults.getAverage_session()+"</td>" +
							  "<td width=150>"+repResults.getAverage_pages()+"</td>" +
							  "<td width=150>"+repResults.getFull_content_units_requested()+"</td>" +
							  "<td width=150>"+repResults.getWeb_pages()+"</td>" +
							  "<td width=100>"+repResults.getQueries()+"</td>" +
							  "<td width=150>"+repResults.getUnits_reached_from_browse()+"</td>" +
							  "<td width=100>"+repResults.getTurnways()+"</td></tr>");
					
					orgNum++;
				}
				
				counterReport.getTotalOtherReport(startDate, endDate, bodyId, counterReportVo);
				
				for(CounterReportsBean repResults : counterReportVo.getSearchResults()) {
					
					output.append("<tr><td width=150>TOTAL</td>" +
							  "<td width=100></td>" +
							  "<td width=150>"+ Integer.toString(orgNum) +"</td>" +
							  "<td width=100></td>" +
							  "<td width=150>"+repResults.getLogins()+"</td>" +
							  "<td width=150>"+repResults.getTotal_session()+"</td>" +
							  "<td width=150>"+repResults.getAverage_session()+"</td>" +
							  "<td width=150>"+repResults.getAverage_pages()+"</td>" +
							  "<td width=150>"+repResults.getFull_content_units_requested()+"</td>" +
							  "<td width=150>"+repResults.getWeb_pages()+"</td>" +
							  "<td width=100>"+repResults.getQueries()+"</td>" +
							  "<td width=150>"+repResults.getUnits_reached_from_browse()+"</td>" +
							  "<td width=100>"+repResults.getTurnways()+"</td></tr>");
				}
			
			
			output.append("</body>" + delimeter);
			output.append("</html>" + delimeter);
	        
			response.setContentType("text");
	        response.setHeader("Content-Disposition", "attachment; filename=Other_Reports" + startDate + "_to_" + endDate + ".html");
	        response.getWriter().print(output.toString());
	        response.getWriter().flush();
	        response.getWriter().close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
