package org.cambridge.ebooks.orgadmin.administrator.switchaccounts;
/**
 * @author cemanalo
 */
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;


public class SwitchAccountsBean {
	
	private static final Logger LOGGER = Logger.getLogger(SwitchAccountsBean.class);
	
	private List<SwitchAccounts> accountsList = new ArrayList<SwitchAccounts>(); 
	
	public String getInitialize() {
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		SwitchAccounts account = new SwitchAccounts();	
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			LOGGER.info("===getInitialize");
			String userType = login.getUserType();
			
			if("AO".equals(userType) || "AC".equals(userType) || "AS".equals(userType)) {
				account.setUserId(login.getMemberUserId());
				account.setBladeMemberId(login.getMemberId());
				
				if(!StringUtils.isEmpty(request.getParameter("Update"))){
					if("Update".equals(request.getParameter("Update"))){
						account.setDefaultAccount(request.getParameter("defaultAccountRadio"));
						SwitchAccountsWorker.updateDefaultAccount(account);
					}
					else if("Switch".equals(request.getParameter("Update"))){
						LOGGER.info("Switch");
					}
				}
				getAllAccounts(account);
			} else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}	
		
		return "";
	}

	private void getAllAccounts(SwitchAccounts account) {
		this.accountsList = SwitchAccountsWorker.getAllAccounts(account);
	}

	public void setAccountsList(ArrayList<SwitchAccounts> accountsList) {
		this.accountsList = accountsList;
	}

	public List<SwitchAccounts> getAccountsList() {
		return accountsList;
	}
}
