package org.cambridge.ebooks.orgadmin.administrator.configureip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;
import org.cambridge.ebooks.orgadmin.db.DBManager;

/**
 * @author kmulingtapang
 */
public class ConfigureIpWorker {

	private static Logger LOGGER = Logger.getLogger(ConfigureIpWorker.class);
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public static final String TYPE_ORG = "AO";
	public static final String TYPE_CONS = "AC";
	
	// JPA implementation
	public static List<ConsortiumOrganisationOption> selectConsOrgByConsId(String bodyId) {
		LOGGER.info("===selectConsOrgByConsId");
		List<ConsortiumOrganisationOption> resultList = new ArrayList<ConsortiumOrganisationOption>();
		List<ConsortiumOrganisation> queryList = new ArrayList<ConsortiumOrganisation>();
		
		EntityManager em = emf.createEntityManager();
		
		try {			
			Query query = em.createNamedQuery(ConsortiumOrganisation.SELECT_BY_CONS_ID);
			query.setParameter(1, bodyId);			
			queryList = query.getResultList();
		} catch (Exception e) {			
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			em.close();
		}
		
		for(ConsortiumOrganisation consOrg : queryList) {
			ConsortiumOrganisationOption consOrgOption = new ConsortiumOrganisationOption();
			consOrgOption.setConsortiumOrganisation(consOrg);
			consOrgOption.setSelected("N");
			resultList.add(consOrgOption);
		}
		
		return resultList;
	}
	
	public static List<Ip> selectOrgIp(String organisationId) {
		LOGGER.info("===selectOrgIP");
		List<Ip> resultList = new ArrayList<Ip>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {								
				StringBuilder sql = new StringBuilder();
	    		sql.append("select org.INTERNET_ADDRESS_ID, org.INTERNET_ADDRESS, org.INCLUDE, tmp.TAM ")		
				   .append("from ORGANISATION_IDENTIFICATION org, ")
					   .append("(select o.INTERNET_ADDRESS, CAST (MULTISET (select ci.CONSORTIA_ID ")
					   .append("from ORGANISATION_IDENTIFICATION oi, CONSORTIUM_ORG_IP_MAP ci ")
					   .append("where oi.INTERNET_ADDRESS = ci.INTERNET_ADDRESS ")
					   .append("and oi.ORGANISATION_ID = ci.ORGANISATION_ID ")
					   .append("and o.INTERNET_ADDRESS = oi.INTERNET_ADDRESS) AS myArrayType) as TAM ")
					   .append("from ORGANISATION_IDENTIFICATION o where o.ORGANISATION_ID = ?) tmp ")
				   .append("where tmp.INTERNET_ADDRESS = org.INTERNET_ADDRESS ")
				   .append("and org.ORGANISATION_ID = ? ");
	    		
	    		ps = conn.prepareStatement(sql.toString());
				ps.setString(1, organisationId);
				ps.setString(2, organisationId);
				
				rs = ps.executeQuery();
				
				while (rs.next()) {					
					String inetId = rs.getString("INTERNET_ADDRESS_ID");
					String address = rs.getString("INTERNET_ADDRESS");
					String include = rs.getString("INCLUDE");
					
					Ip ip = new Ip();
					ip.setAddressId(inetId);
					ip.setIncludeFlag(include);
					ip.setAddress(address);
					
					resultList.add(ip);
				}
				
				Collections.sort(resultList);
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultList;
	}
	
	public static List<Ip> selectConsIp(String consortiaId) {
		LOGGER.info("===selectConsIp");
		List<Ip> resultList = new ArrayList<Ip>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {								
				StringBuilder sql = new StringBuilder();
	    		sql.append("SELECT ID.internet_address_id, ID.internet_address, ID.include ")		
				   .append("FROM consortium_identification ID ")					  
				   .append("WHERE ID.consortia_id = ?");
	    		
	    		ps = conn.prepareStatement(sql.toString());
				ps.setString(1, consortiaId);
				
				rs = ps.executeQuery();
				
				while (rs.next()) {					
					String inetId = rs.getString("INTERNET_ADDRESS_ID");
					String address = rs.getString("INTERNET_ADDRESS");
					String include = rs.getString("INCLUDE");
					
					Ip ip = new Ip();
					ip.setAddressId(inetId);
					ip.setIncludeFlag(include);
					ip.setAddress(address);
					
					resultList.add(ip);
				}
				
				Collections.sort(resultList);
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultList;
	}
	
	public static List<String> selectOwnIP(String bodyId) {
		LOGGER.info("===selectOwnIP");
		List<String> resultList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {								
				StringBuilder sql = new StringBuilder();
	    		sql.append("SELECT INTERNET_ADDRESS ")		
				   .append("FROM ORGANISATION_IDENTIFICATION ")
				   .append("WHERE ORGANISATION_ID = ? ")
				   .append("UNION ")
				   .append("SELECT INTERNET_ADDRESS ")
				   .append("FROM consortium_identification ")
				   .append("WHERE consortia_id = ? ");
	    		
	    		ps = conn.prepareStatement(sql.toString());
				ps.setString(1, bodyId);
				ps.setString(2, bodyId);
				
				rs = ps.executeQuery();				
				while (rs.next()) {					
					String address = rs.getString("INTERNET_ADDRESS");					
					resultList.add(address);
				}				
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultList;
	}
	
	public static Map<String, String> selectInvalidDomain() {
		LOGGER.info("===selectInvalidDomain");
		Map<String, String> resultMap = new HashMap<String, String>();
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {								
				String sql = "SELECT UNIQUE DOMAIN_IP, ERRORMESSAGE FROM DOMAINS_TO_CHECK ";
	    		st = conn.createStatement();
	    		rs = st.executeQuery(sql);
				
				while (rs.next()) {					
					String domain = rs.getString("DOMAIN_IP");
					String errorMessage = rs.getString("ERRORMESSAGE");
					
					if (StringUtils.isNotEmpty(domain)){
					    if (StringUtils.isNotEmpty(errorMessage)){
					    	resultMap.put(domain, errorMessage);
					    } else{
					    	resultMap.put(domain, "");
					    }
					}
				}				
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultMap;
	}
	
	public static Map<String, List<Ip>> selectOverlapIp(List<Ip> ipList) {
		LOGGER.info("===selectOverlapIp");
		Map<String, List<Ip>> resultMap = new HashMap<String, List<Ip>>();
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {								
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT distinct ii.INTERNET_ADDRESS, ii.source, ci.consortia_id, c.body_id, c.name,'consortia' as body_type ")
				   .append("FROM IPADDRESS_IDENTIFICATION ii, consortium_identification ci, consortium c ");
				 
				String where = " WHERE (";
				String like = " ii.INTERNET_ADDRESS LIKE ";
				String or = " OR ";
				String orderBy = " ORDER BY INTERNET_ADDRESS, BODY_ID ";
				
				// Get the first IP num, assume X.*.*.* for filter
				boolean first = true;
				for(Ip ip : ipList) {
					String tmp = ip.getAddress();
					int tmpIndex = tmp.indexOf('.');
					if(tmpIndex > -1) {
						String likeClause = tmp.substring(0, tmpIndex);
						if(first) {
							sql.append(where);
							first = false;
						} else {
							sql.append(or);
						}
						sql.append(like);
						sql.append("'").append(likeClause).append("%'");
					}
				}
				sql.append(") and c.body_id=ci.consortia_id ");
				sql.append(" and ii.body_id = c.body_id and ii.include='Y' and ci.include='Y' ");
				sql.append(" UNION");
				sql.append(" SELECT distinct ii.INTERNET_ADDRESS, ii.source, oi.organisation_id, o.body_id, o.name,'organisation' as body_type ");
				sql.append(" FROM IPADDRESS_IDENTIFICATION ii, organisation_identification oi, organisation o ");

				//Get the first IP num, assume X.*.*.* for filter
				boolean first1 = true;
				for(Ip ip : ipList) {
					String tmp = ip.getAddress();
					int tmpIndex = tmp.indexOf('.');
					if(tmpIndex > -1) {
						String likeClause = tmp.substring(0, tmpIndex);
						if(first1) {
							sql.append(where);
							first1 = false;
						} else {
							sql.append(or);
						}
						sql.append(like);
						sql.append("'").append(likeClause).append("%'");
					}
				}
				sql.append(") and o.body_id=oi.organisation_id ");
				sql.append(" and ii.body_id = o.body_id and ii.include='Y' and oi.include='Y' ");
				sql.append(orderBy);
				
				LOGGER.info("IP Overlap SQL: " + sql.toString());
				
				st = conn.createStatement();
				rs = st.executeQuery(sql.toString());
				
				List<Ip> tmpIpList = null;
				while(rs.next()) {
					String domain = rs.getString("INTERNET_ADDRESS");
					String name = rs.getString("NAME");
					String bodyId = rs.getString("BODY_ID");
					String source = rs.getString("SOURCE");
					String type = rs.getString("BODY_TYPE");
					
					Ip ip = new Ip();
					ip.setDomain(domain);
					ip.setName(name);
					ip.setBodyId(bodyId);
					ip.setSource(source);
					ip.setType(type);
					
					if(StringUtils.isNotEmpty(domain)) {
					    int len = domain.indexOf(".");
						String key = domain.substring(0, len > -1 ? len : domain.length());
						
						if(null == resultMap.get(key)) {
							tmpIpList = new ArrayList<Ip>();
						} else {
							tmpIpList = resultMap.get(key);
						}
						tmpIpList.add(ip);
						resultMap.put(key, tmpIpList);
					}
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return resultMap;
	}
	
	public static void insertIp(List<Ip> ipList, String bodyId, String type) {
		LOGGER.info("===insertIp");
		Connection conn = null;
		PreparedStatement ps = null;		
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {		
				conn.setAutoCommit(false);
				
				String sql = "INSERT INTO ORGANISATION_IDENTIFICATION (INTERNET_ADDRESS_ID, INTERNET_ADDRESS, ORGANISATION_ID, INCLUDE) VALUES (INTERNET_ADDRESS_SEQ.NEXTVAL, ?, ?, ?)";
				if(TYPE_CONS.equals(type)) {
					sql = "INSERT INTO consortium_identification (INTERNET_ADDRESS_ID, INTERNET_ADDRESS, consortia_id, INCLUDE) VALUES (CONSORTIUM_INET_ID_SEQ.nextval, ?, ?, ?)";
				} 
				
				LOGGER.info("===sql:" + sql);
				
				for(Ip ip : ipList) {					
					ps = conn.prepareStatement(sql);
					ps.setString(1, ip.getAddress());
					ps.setString(2, bodyId);
					ps.setString(3, ip.getIncludeFlag());
					int updateCount = ps.executeUpdate();	
					LOGGER.info("===updateCount:" + updateCount);
				}
				
				conn.commit();
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		} finally {			
			try {
				if (null != conn) {
					conn.setAutoCommit(true);
				}				
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	public static void deleteIp(String[] deleteIpArr, String type) {
		LOGGER.info("===deleteIP");
		Connection conn = null;
		Statement st = null;		
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {		
				conn.setAutoCommit(false);
				
				StringBuilder sql = new StringBuilder("DELETE FROM ORGANISATION_IDENTIFICATION WHERE INTERNET_ADDRESS_ID IN (");
				
				if(TYPE_CONS.equals(type)) {
					sql = new StringBuilder("DELETE FROM consortium_identification WHERE INTERNET_ADDRESS_ID IN (");
				}
				
				int i = 0;
				for(String s : deleteIpArr) {
					i++;
					sql.append(s);
					if(i < deleteIpArr.length) {
						sql.append(", ");
					}
				}
				sql.append(")");
				
				LOGGER.info("===sql:" + sql.toString());
				
				st = conn.createStatement();
				int updateCount = st.executeUpdate(sql.toString());
				
				LOGGER.info("===updateCount:" + updateCount);
				
				conn.commit();
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		} finally {
			try {
				if (null != conn) {
					conn.setAutoCommit(true);
				}		
				DBManager.closeConnection(conn, st);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}	
	
	public static void includeOrgIp(String[] ipArr, String[] deleteIpArr, String includeFlag, String organisationId) {
		LOGGER.info("===includeOrgIp");
		Connection conn = null;
		Statement st = null;		
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {				
				conn.setAutoCommit(false);
				
				List<String> deleteIpList = null;
				if(null != deleteIpArr && deleteIpArr.length > 0) {
					deleteIpList = Arrays.asList(deleteIpArr);
				}
				
				StringBuilder sql = new StringBuilder("UPDATE ORGANISATION_IDENTIFICATION SET INCLUDE = '").append(includeFlag).append("' WHERE INTERNET_ADDRESS_ID IN (");				
				boolean hasExclude = false;
				int i = 0;
				for(String s : ipArr) {
					i++;
					// skip this ip
					if(null != deleteIpList && deleteIpList.contains(s)) {
						continue;
					}
					
					hasExclude = true;
					sql.append(s);
					if(i < ipArr.length) {
						sql.append(", ");
					}
				}
				sql.append(") AND organisation_id = ").append(organisationId);
				
				LOGGER.info("===sql:" + sql.toString());
				
				if(!hasExclude) {
					LOGGER.info("===No address to exclude");
				} else {					
					st = conn.createStatement();
					int updateCount = st.executeUpdate(sql.toString());
					
					LOGGER.info("===updateCount:" + updateCount);
					
					conn.commit();
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		} finally {
			try {
				if (null != conn) {
					conn.setAutoCommit(true);
				}		
				DBManager.closeConnection(conn, st);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
	public static void includeConsIp(String[] ipArr, String[] deleteIpArr, String includeFlag, String consortiaId) {
		LOGGER.info("===includeConsIp");
		Connection conn = null;
		Statement st = null;		
		try {
			conn = DBManager.openConnection();
			
			if(null != conn) {				
				conn.setAutoCommit(false);
				
				List<String> deleteIpList = null;
				if(null != deleteIpArr && deleteIpArr.length > 0) {
					deleteIpList = Arrays.asList(deleteIpArr);
				}
				
				StringBuilder sql = new StringBuilder("UPDATE consortium_identification SET INCLUDE = '").append(includeFlag).append("' WHERE INTERNET_ADDRESS_ID IN (");				
				boolean hasExclude = false;
				int i = 0;
				for(String s : ipArr) {
					i++;
					// skip this ip
					if(null != deleteIpList && deleteIpList.contains(s)) {
						continue;
					}
					
					hasExclude = true;
					sql.append(s);
					if(i < ipArr.length) {
						sql.append(", ");
					}
				}
				sql.append(") AND consortia_id = ").append(consortiaId);
				
				LOGGER.info("===sql:" + sql.toString());
				
				if(!hasExclude) {
					LOGGER.info("===No address to exclude");
				} else {					
					st = conn.createStatement();
					int updateCount = st.executeUpdate(sql.toString());
					
					LOGGER.info("===updateCount:" + updateCount);
					
					conn.commit();
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		} finally {
			try {
				if (null != conn) {
					conn.setAutoCommit(true);
				}		
				DBManager.closeConnection(conn, st);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
}
