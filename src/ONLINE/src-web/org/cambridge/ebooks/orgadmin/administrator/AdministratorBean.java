package org.cambridge.ebooks.orgadmin.administrator;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author kmulingtapang
 */
public class AdministratorBean {
	private static final Logger LOGGER = Logger.getLogger(AdministratorBean.class);
	
	private String orgName;
	
	public String getInitialize() {
		LOGGER.info("===getInitialize");
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		Login login = orgConLinkedMap.getUserLogin();
		
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {		
			String bodyId = login.getOrgBodyId();
			Organisation org = orgConLinkedMap.getMap().get(bodyId);			
			orgName = org.getBaseProperty(Organisation.DISPLAY_NAME);
		}
		
		return "";
	}
	
	public String getOrgName() {
		return orgName;
	}
}
