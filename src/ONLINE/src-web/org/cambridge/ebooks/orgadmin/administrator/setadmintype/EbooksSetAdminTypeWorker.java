package org.cambridge.ebooks.orgadmin.administrator.setadmintype;

/**
 * @author cemanalo
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class EbooksSetAdminTypeWorker {
	private static Logger LOGGER = Logger.getLogger(EbooksSetAdminTypeWorker.class);
	
	public static Admin getAdmin(String bladeMemberId, String bodyId) {
		LOGGER.info("===getAdmin");
		
		Admin admin = new Admin();
		Connection conn = null;
		PreparedStatement pstat = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT CBO_ADMIN,CJO_ADMIN from ADMIN ")
			.append("WHERE BLADE_MEMBER_ID = ? ")
			.append("AND ORGANISATION_ID = ?");
		
		try{
			conn = DBManager.openConnection();
			pstat = conn.prepareStatement(sql.toString());
			pstat.setString(1, bladeMemberId);
			pstat.setString(2, bodyId);
			
			rs = pstat.executeQuery();
			while(rs.next()){
				admin.setCboAdmin(rs.getString("CBO_ADMIN"));
				admin.setCjoAdmin(rs.getString("CJO_ADMIN"));
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, pstat, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		return admin;
	}

	public static void updateAdmin(HttpServletRequest request) {
		
		LOGGER.info("==updateAdmin");
		
		Connection conn = null;
		PreparedStatement pstat = null;
		
		String orgId = request.getParameter("orgId");
		String bladeMemberId = request.getParameter("bladeMemberId");
		String cboAdmin = request.getParameter("cboAdmin");
		String cjoAdmin = request.getParameter("cjoAdmin");
		
		StringBuilder sql = new StringBuilder();
		sql.append("update admin set CBO_ADMIN = ?, ")
			.append("CJO_ADMIN = ? where ")
			.append("blade_member_id = ? and organisation_id = ?");
		
		try{
			conn = DBManager.openConnection();
			pstat = conn.prepareStatement(sql.toString());
			
			pstat.setString(1, "Y".equals(cboAdmin)? "Y" : "N");
			pstat.setString(2, "Y".equals(cjoAdmin)? "Y" : "N");
			pstat.setString(3, bladeMemberId);
			pstat.setString(4, orgId);
			
			pstat.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, pstat);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
	}
	
}
