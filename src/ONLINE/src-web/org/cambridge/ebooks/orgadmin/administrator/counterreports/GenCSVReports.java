package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

public class GenCSVReports {
	
	private static final String BOOK_REPORT1_HEADER = 
		"Book Report 1 (R1),Number of Subscribed Titles by Month and Title\n";
		
	private static final String BOOK_REPORT1_COLUMN_HEADERS =
		",Publisher,Platform,ISBN,ISSN";
	
	private static final String[] REPORT1_BODY = new String[]{
		"titleFormatted",
		"publisher",
		"platform",
		"isbnFormatted",
		"issn",
		"jan",
		"feb",
		"mar",
		"apr",
		"may",
		"jun",
		"jul",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
		"ytd"
		};		
	
	private static final String BOOK_REPORT2_HEADER = 
		"Book Report 2 (R1),Number of Successful Section Requests by Month and Title\n";
	
	private static final String BOOK_REPORT3_HEADER = 
		"Book Report 3 (R1),Turnaways by Month and Title\n";
	
	private static final String BOOK_REPORT4_HEADER = 
		"Book Report 4 (R1),Turnaways by Month and Service\n";
	
	private static final String BOOK_REPORT4_COLUMN_HEADERS =
		",Publisher,Platform";
	
	private static final String[] REPORT4_BODY = new String[]{
		"service",
		"publisher",
		"platform",	
		"jan",
		"feb",
		"mar",
		"apr",
		"may",
		"jun",
		"jul",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
		"ytd"
		};	
	
	private static final String BOOK_REPORT5_HEADER = 
		"Book Report 5 (R1),Total Searches and Sessions by Month and Title\n";
	
	private static final String BOOK_REPORT5_COLUMN_HEADERS =
		",Publisher,Platform,ISBN,ISSN,";
	
	private static final String[] REPORT5_BODY = new String[]{
		"titleFormatted",
		"publisher",
		"platform",
		"isbnFormatted",
		"issn",
		"isSearch",
		"jan",
		"feb",
		"mar",
		"apr",
		"may",
		"jun",
		"jul",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
		"ytd"
		};		
	
	private static final String BOOK_REPORT6_HEADER = 
		"Book Report 6 (R1),Total Searches and Sessions by Month and Service\n";
	
	private static final String BOOK_REPORT6_COLUMN_HEADERS =
		",Publisher,Platform,";
	
	private static final String[] REPORT6_BODY = new String[]{
		"service",
		"publisher",
		"platform",	
		"isSearch",
		"jan",
		"feb",
		"mar",
		"apr",
		"may",
		"jun",
		"jul",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
		"ytd"
		};	
	
	private static final String BOOK_OTHER_REPORT_HEADER = 
		"Monthly Breakdown Report\n";
		
	private static final String BOOK_OTHER_REPORT_COLUMN_HEADERS =
		",Site,Month,Member Organisation,Account ID, Sessions (Logins),Total Session Time (hh:mm:ss), Average Session Time (hh:mm:ss), Average Pages per Session, Full Content Units Requested, Web Pages Requested, Queries (Searches), Full Content Units Reached from Browse, Turnaways ";
	/**
	 * generic reporting for reports 1 - 4
	 * @param header
	 * @param colHeaders
	 * @param year
	 * @param reportBody
	 * @param erbArr
	 * @returnR
	 * @throws Exception
	 */
	public static String generateReport(String header, String colHeaders, 
			String year, String[] reportBody, ArrayList erbArr, int bodyId, HttpServletRequest request) throws Exception{
		ReportCSVBean csvBean = new ReportCSVBean();
		csvBean.setMainHeader(header);		
		csvBean.setCriteria(getOrgName(bodyId, request));
		csvBean.setColumnHeaders(colHeaders, year);
		csvBean.setReportBodyKeys(reportBody);
		return csvBean.generateGenRep(erbArr, year);
	}

	public static String generateOtherReport(String header, String colHeaders, 
			String sDate, String eDate, int bodyId, CounterReportsBean counterReportVo, HttpServletRequest request) throws Exception{
		ReportCSVBean csvBean = new ReportCSVBean();
		csvBean.setMainHeader(header);	
		csvBean.setCriteria(getOrgName(bodyId, request));
		csvBean.setColumnOtherHeaders(colHeaders);
		return csvBean.generateGenOtherRep(colHeaders, sDate, eDate, bodyId, counterReportVo);
	}
	
	private static String getOrgName(int bodyId, HttpServletRequest request) throws Exception {
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);
		Organisation org = orgConLinkedMap.getMap().get(String.valueOf(bodyId));			
		String orgName = org.getBaseProperty(Organisation.DISPLAY_NAME);
		
		return orgName;
	}
	
	/**
	 * generic reporting for reports 5 - 6
	 * @param header
	 * @param colHeaders
	 * @param year
	 * @param reportBody
	 * @param erbArr
	 * @return
	 * @throws Exception
	 */
	public static String generateReport56(String header, String colHeaders, 
			String year, String[] reportBody, ArrayList erbArr, int reportType, int bodyId, HttpServletRequest request) throws Exception{
		ReportCSVBean csvBean = new ReportCSVBean();
		csvBean.setMainHeader(header);	
		csvBean.setCriteria(getOrgName(bodyId, request));
		csvBean.setColumnHeaders(colHeaders, year);
		csvBean.setReportBodyKeys(reportBody);
		return csvBean.generateGenRep56(erbArr, reportType, year);
		
	}
	
	/**
	 * generate report 1 || report 2 || report 3 || report 4
	 * @param erbArr
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception
	 */
	public static String generateReportStmt(ArrayList erbArr, String year, int bodyId, String strOrgType, HttpServletRequest request, int reportNum ) throws Exception{		
		
		/*if("AC".equals(strOrgType)){
			return generateReport_Consortia(BOOK_REPORT1_HEADER, BOOK_REPORT1_COLUMN_HEADERS,
					year, REPORT1_BODY, erbArr, bodyId, strSessionId);
		}*/
		
		String headerName = "";
		String columnName = "";
		String[] bodyName = null;
		
		if(1 == reportNum){
			headerName = BOOK_REPORT1_HEADER;
			columnName = BOOK_REPORT1_COLUMN_HEADERS;
			bodyName = REPORT1_BODY;
		}else if (2 == reportNum){
			headerName = BOOK_REPORT2_HEADER;
			columnName = BOOK_REPORT1_COLUMN_HEADERS;
			bodyName = REPORT1_BODY;
		}else if (3 == reportNum){
			headerName = BOOK_REPORT3_HEADER;
			columnName = BOOK_REPORT1_COLUMN_HEADERS;
			bodyName = REPORT1_BODY;
		}else if (4 == reportNum){
			headerName = "BOOK_REPORT4_HEADER";
			columnName = "BOOK_REPORT4_COLUMN_HEADERS";
			bodyName = REPORT4_BODY;
		}
		
		return generateReport(headerName, columnName,
			year, bodyName, erbArr, bodyId, request);
		
	}
	
	public static String generateOtherReport1(String startDate, String endDate, int bodyId, CounterReportsBean counterReportVo, HttpServletRequest request) throws Exception{		
		
		return generateOtherReport(BOOK_OTHER_REPORT_HEADER,BOOK_OTHER_REPORT_COLUMN_HEADERS,
				startDate, endDate, bodyId, counterReportVo, request);
		
	}
	
	/**
	 * generate report 5 || report 6
	 * @param erbArr
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception
	 */
	public static String generateReportStmt56(ArrayList erbArr56, String year, int bodyId, String strOrgType, HttpServletRequest request, int reportNum) throws Exception{
		//apirante
		
		String headerName = "";
		String columnName = "";
		String[] bodyName = null;
		
		if (5 == reportNum){
			headerName = BOOK_REPORT5_HEADER;
			columnName = BOOK_REPORT5_COLUMN_HEADERS;
			bodyName = REPORT5_BODY;
		}else if (6 == reportNum){
			headerName = BOOK_REPORT6_HEADER;
			columnName = BOOK_REPORT6_COLUMN_HEADERS;
			bodyName = REPORT6_BODY;
		}
		
		return generateReport56(headerName, columnName,
				year, bodyName, erbArr56, 5, bodyId, request);
	}
	
}
