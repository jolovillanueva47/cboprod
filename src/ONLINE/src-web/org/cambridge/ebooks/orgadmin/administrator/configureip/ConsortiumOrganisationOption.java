package org.cambridge.ebooks.orgadmin.administrator.configureip;

import org.cambridge.ebooks.online.jpa.consortium.ConsortiumOrganisation;

/**
 * @author kmulingtapang
 */
public class ConsortiumOrganisationOption {

	private String selected;
	private ConsortiumOrganisation consortiumOrganisation;
	
	public String getSelected() {
		return selected;
	}
	public void setSelected(String selected) {
		this.selected = selected;
	}
	public ConsortiumOrganisation getConsortiumOrganisation() {
		return consortiumOrganisation;
	}
	public void setConsortiumOrganisation(
			ConsortiumOrganisation consortiumOrganisation) {
		this.consortiumOrganisation = consortiumOrganisation;
	}
}
