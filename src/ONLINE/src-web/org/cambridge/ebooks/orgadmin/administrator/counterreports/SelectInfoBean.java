package org.cambridge.ebooks.orgadmin.administrator.counterreports;

public class SelectInfoBean {

	public static final int STRING = 0;
	
	public static final int INT = 1;
	
	public static final int TIMESTAMP = 2;
	
	private String select;
	
	private int dataType;
	
	
	public SelectInfoBean(String select, int dataType) {
		super();		
		this.select = select;
		this.dataType = dataType;
	}
	
	public int getDataType() {
		return dataType;
	}
	
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	
	public String getSelect() {
		return select;
	}
	
	public void setSelect(String select) {
		this.select = select;
	}
	
}
