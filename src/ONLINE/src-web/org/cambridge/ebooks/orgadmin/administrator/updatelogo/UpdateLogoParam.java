package org.cambridge.ebooks.orgadmin.administrator.updatelogo;

import java.io.File;

import org.apache.commons.fileupload.FileItem;



public class UpdateLogoParam {
    private FileItem logo;
    private String name = "";
    private boolean image;
    private String bodyId = "";
    

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setImage(boolean image) {
		this.image = image;
	}
	public boolean isImage() {
		return image;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
	public String getBodyId() {
		return bodyId;
	}
	public void setLogo(FileItem logo) {
		this.logo = logo;
	}
	public FileItem getLogo() {
		return logo;
	}

 
    
}
