package org.cambridge.ebooks.orgadmin.administrator.updatelogo;

import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;

public class UpdateLogoServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -612840838288504663L;
	private static final Logger LOGGER = Logger.getLogger(UpdateLogoServlet.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info(req.getParameter("uploadLogo"));
		doPost(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.info("doPost here");
		
		UpdateLogoParam logoParam = new UpdateLogoParam();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(req);		
		Login login = orgConLinkedMap.getUserLogin();
		String uploadLogoRequest = null;
		String errorMessage = null;
		logoParam.setBodyId(login.getOrgBodyId());
		try {
			List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);
			for(FileItem item:items){
				
				if(!item.isFormField()){
					logoParam.setLogo(item);
				} else {
					 String fieldname = item.getFieldName();
		             String fieldvalue = item.getString();
		             if("uploadLogoRequest".equals(fieldname)){
		            	 uploadLogoRequest = fieldvalue;
		             }
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
		if ("Upload Logo".equals(uploadLogoRequest)){
			String logoFileName = logoParam.getLogo().getName().toLowerCase().trim();
			if(logoFileName.indexOf(".jpg") > -1 ||
				logoFileName.indexOf(".jpeg") > -1 ||
				logoFileName.indexOf(".gif") > -1){
				if(null == ImageIO.read(logoParam.getLogo().getInputStream())){
					LOGGER.info("not Image");
					errorMessage = "The logo you have provided has an invalid image format.";
				} else {
					if(logoParam.getLogo().getSize() <= 15360){
						UpdateLogoWorker.uploadLogo(logoParam,"ORGANISATION");
						resp.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=logo");
					} else {
						errorMessage = "Invalid file size. File size of logo should not be greater than 15KB.";
					}
				}
				
			} else {
				errorMessage = "Invalid filename. Filename should have an extension of jpg, jpeg and gif.";
			}
			if(null !=errorMessage){
				resp.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=logo&errors="+errorMessage.replace(" ", "+"));
			}
			
		} else if ("Remove Logo".equals(uploadLogoRequest)){
			UpdateLogoWorker.removeLogo(logoParam,"ORGANISATION");
			resp.sendRedirect("/orgadmin/account_administrator.jsf?type=ao&page=logo");
		} else if("Done".equals(uploadLogoRequest)){
			resp.sendRedirect("/home.jsf");
		}	
		
	}

//	private void setLogoParam(HttpServletRequest req, UpdateLogoParam logoParam) {
//		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(req);	
//		
//		File file = new File(req.getParameter("logo"));
//		
//		logoParam.setLogo(file);
//		logoParam.setBodyId(orgConLinkedMap.getCurrentlyLoggedBodyId());
//		
//	}
}
