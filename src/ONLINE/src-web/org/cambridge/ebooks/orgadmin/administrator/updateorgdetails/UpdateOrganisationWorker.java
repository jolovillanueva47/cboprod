package org.cambridge.ebooks.orgadmin.administrator.updateorgdetails;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class UpdateOrganisationWorker {
	private static Logger LOGGER = Logger.getLogger(UpdateOrganisationWorker.class);
	
	public static LinkedHashMap<String,String> getTypeList() { 
		LOGGER.info("==getTypeList");
		LinkedHashMap<String,String> typeList = new LinkedHashMap<String,String>();
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs=null;
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ORGANISATION_TYPE_ID, DESCRIPTION ")
				.append("FROM ORGANISATION_TYPE ")
				.append("WHERE ORGANISATION_TYPE_ID <> -1 AND ")
				.append("DISPLAY_ORDER is not null ")
				.append("ORDER BY DISPLAY_ORDER ASC ");
			
			st = conn.createStatement();
			
			rs = st.executeQuery(sql.toString());
			while (rs != null && rs.next()) {
				String key = rs.getString("ORGANISATION_TYPE_ID");
				String val = rs.getString("DESCRIPTION");
				typeList.put(key, val);
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, st);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		return typeList;
	}
	
	public static UpdateOrganisationDetails getOrganisationDetails(String bodyId){
		LOGGER.info("getOrganisationDetails");
		UpdateOrganisationDetails orgDetails = new UpdateOrganisationDetails();
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs= null;

		try{
			conn = DBManager.openConnection();
			
			cs = conn.prepareCall("{ call PKG_CJOC_CUPADMIN_ORGANISATION.RETRIEVE_ORGANISATION_DETAILS(?,?,?,?,?) }");
			cs.registerOutParameter(1, Types.INTEGER); 
			cs.registerOutParameter(2, Types.VARCHAR);  
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.setString(5, bodyId);  		   	
			cs.execute();
			
			
			int ret = cs.getInt(1);
			
			if(ret==0){
				rs = (ResultSet) cs.getObject(3);
				if(rs != null && rs.next()){
					orgDetails.setType(rs.getString("TYPE"));
					orgDetails.setAthensId(rs.getString("ATHENS_ID"));
					orgDetails.setName(rs.getString("NAME"));
					orgDetails.setDisplayName(rs.getString("DISPLAY_NAME"));
					orgDetails.setDisplayMessage(rs.getString("MESSAGE_TEXT"));
					orgDetails.setCountry(rs.getString("COUNTRY"));
					String county = rs.getString("STATE_COUNTY"); 
					orgDetails.setCounty(county == null ? "0": county);
					orgDetails.setCity(rs.getString("ADDRESS3"));
					orgDetails.setPostCode(rs.getString("POST_ZIP_CODE"));
					orgDetails.setAddress1(rs.getString("ADDRESS1"));
					orgDetails.setAddress2(rs.getString("ADDRESS2"));
					
				}
			} else if (ret==100){
				throw new Exception(cs.getString(2));
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orgDetails;
	}
	
	public static LinkedHashMap<String, String> getCountyListById(String countryId ){
		LinkedHashMap<String, String> countyList = new LinkedHashMap<String, String>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = DBManager.openConnection();
			String sql = "select state_county_id, description from state_county where country = ? order by description asc";
			
			ps = conn.prepareStatement(sql);
		    ps.setString(1, countryId);
		    
		    rs = ps.executeQuery();
		    
		    //set for other
		    countyList.put("0", "Other");
		    while (rs.next()) {
				String key = rs.getString("state_county_id");
				String val = rs.getString("description");
				countyList.put(key, val);
			}
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return countyList;
	}

	public static void updateOrganisationDetails(HttpServletRequest request, String orgBodyId) {
		updateOrganisationTable(request,orgBodyId);
		updateAddressTable(request, orgBodyId);
	}
	
	
	
	private static void updateOrganisationTable(HttpServletRequest request, String orgBodyId) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			conn = DBManager.openConnection();
			StringBuilder sql = new StringBuilder();
			
			sql.append("update ORACJOC.ORGANISATION ")
				.append("set TYPE = ?, ATHENS_ID  = ?, ")
				.append("NAME = ?, DISPLAY_NAME = ?, ")
				.append("MESSAGE_TEXT = ? ")
				.append("where BODY_ID = ? ");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, request.getParameter("type"));
			ps.setString(2, request.getParameter("athensId"));
			ps.setString(3, request.getParameter("name"));
			ps.setString(4, request.getParameter("displayName"));
			ps.setString(5, request.getParameter("displayMessage"));
			ps.setString(6, orgBodyId);
			
			ps.executeUpdate();
			
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	private static void updateAddressTable(HttpServletRequest request, String orgBodyId){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ADDRESS FROM ORGANISATION where body_id = ?");
			
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, orgBodyId);
			
			rs = ps.executeQuery();
			
			int addressId = 0;
			while(rs.next()){
				addressId =Integer.parseInt( rs.getString("address"));
			}
			
			if(addressId>0){
				sql = new StringBuilder();
				sql.append("UPDATE ORACJOC.ADDRESS SET ")
					.append("ADDRESS1 = ?, ")
					.append("ADDRESS2 = ?, ")
					.append("ADDRESS3 = ?, ")
					.append("STATE_COUNTY = ?, ")
					.append("POST_ZIP_CODE = ?, ")
					.append("COUNTRY = ? ")
					.append("WHERE ADDRESS_ID = ?");
				ps = conn.prepareStatement(sql.toString());
				setPrepareStatment(ps,request,addressId);
				ps.executeUpdate();
			} else {
				addressId = getNextAddressId();
				
				sql	= new StringBuilder();
				sql.append("INSERT INTO ADDRESS (ADDRESS1, ")
					.append("ADDRESS2, ADDRESS3, ")
					.append("STATE_COUNTY, POST_ZIP_CODE, ")
					.append("COUNTRY, ADDRESS_ID) ")
					.append("VALUES ?, ?, ?, ?, ?, ?, ?");
				setPrepareStatment(ps,request,addressId);
				ps.executeUpdate();
				
				String updateOrgSql = "UPDATE ORGANISATION SET ADDRESS = ? WHERE BODY_ID = ?";
				ps = conn.prepareStatement(updateOrgSql);
				ps.setInt(1, addressId);
				ps.setString(2, orgBodyId);
				ps.executeUpdate();
				
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, ps,rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	private static void setPrepareStatment(PreparedStatement ps,
			HttpServletRequest request, int addressId) throws SQLException {
		
		ps.setString(1, request.getParameter("address1"));
		ps.setString(2, request.getParameter("address2"));
		ps.setString(3, request.getParameter("city"));
		ps.setString(4, request.getParameter("county"));
		ps.setString(5, request.getParameter("postCode"));
		ps.setString(6, request.getParameter("country"));
		ps.setInt(7, addressId);
		
	}

	private static int getNextAddressId() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nextVal = 0;
		
		conn = DBManager.openConnection();
		
		String sql = "SELECT ADDRESS_SEQ.NEXTVAL FROM DUAL";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		
		while(rs.next()){
			nextVal = rs.getInt(1);
		}
		return nextVal;
		
	}

}
