package org.cambridge.ebooks.orgadmin.administrator.counterreports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.ReportDBManager;

public class CounterReportsWorker {
	
	private static final Logger LOGGER = Logger.getLogger(CounterReportsWorker.class);
	
	private static final String TITLE_SUMMARY_ROW_1_2 = "Total for all titles";
	
	private static final String TITLE_SUMMARY_ROW_3_4 = "Total turnaways";
	
	private static final String TITLE_SUMMARY_ROW_5_6_SEARCHES = "Total searches";
	
	private static final String TITLE_SUMMARY_ROW_5_6_SESSIONS = "Total sessions";
	
	private static final String SEARCHES_RUN = "Searches run";
	
	private static final String SESSIONS = "Sessions";
	
	private static final String YES = "SEA";
	
	private static final String NO = "SES";
	
	private static final String[] MONTHS_LIST = new String[]{
		"jan",
		"feb",
		"mar",
		"apr",
		"may",
		"jun",
		"jul",
		"aug",
		"sep",
		"oct",
		"nov",
		"dec",
		};
	
	private static final String QUERY1 = 
		"select * from " +
		"(select d_load.Title, " +
		"'Cambridge University Press' as Publisher, " +
		"'Cambridge Books Online' as Platform, " +
		"d_load.ISBN, " +
		"'n/a' as ISSN, " +
		"nvl(jan_total,0) as jan, " +
		"nvl(feb_total,0) as feb, " +
		"nvl(mar_total,0) as mar, " +
		"nvl(apr_total,0) as apr, " +
		"nvl(may_total,0) as may, " +
		"nvl(jun_total,0) as jun, " +
		"nvl(jul_total,0) as jul, " +
		"nvl(aug_total,0) as aug, " +
		"nvl(sep_total,0) as sep, " +
		"nvl(oct_total,0) as oct, " +
		"nvl(nov_total,0) as nov, " +
		"nvl(dec_total,0) as dec, " +
		"nvl(total_ytd,0) as total_ytd " +
			"from ebook_access_agreement a_agr, ebook_order_line o_line, cbo_s_counter_book c_cnt, " +
			"ebook_isbn_data_load d_load " +
      			"where a_agr.body_id = ? " +
      			"and to_char(a_agr.access_confirmee_timestamp, 'YYYY') = ? " +
        		"and c_cnt.body_id = ? " +
        		"and c_cnt.year = ? " +
      			"and a_agr.order_id = o_line.order_id " +
      			"and d_load.isbn = o_line.product_id " +
        		"and d_load.isbn = c_cnt.isbn " +
      			"and d_load.online_flag = 'Y' " +
		"Union " +
		"select d_load.Title, " +
		"'Cambridge University Press' as Publisher, " +
		"'Cambridge Books Online' as Platform, " +
		"d_load.ISBN, " +
		"'n/a' as ISSN, " +
		"nvl(jan_total,0) as jan, " +
		"nvl(feb_total,0) as feb, " +
		"nvl(mar_total,0) as mar, " +
		"nvl(apr_total,0) as apr, " +
		"nvl(may_total,0) as may, " +
		"nvl(jun_total,0) as jun, " +
		"nvl(jul_total,0) as jul, " +
		"nvl(aug_total,0) as aug, " +
		"nvl(sep_total,0) as sep, " +
		"nvl(oct_total,0) as oct, " +
		"nvl(nov_total,0) as nov, " +
		"nvl(dec_total,0) as dec, " +
		"nvl(total_ytd,0) as total_ytd " +
    		"from ebook_access_agreement a_agr, ebook_order_line o_line, cbo_s_counter_book c_cnt, " +
         	"ebook_collection_list c_list, ebook_isbn_data_load d_load " +
      			"where a_agr.body_id = ? " +
            	"and to_char(a_agr.access_confirmee_timestamp, 'YYYY') = ? " +
             	"and c_cnt.body_id = ? " +
            	"and c_cnt.year = ? " +
      			"and a_agr.order_id = o_line.order_id " +
      			"and o_line.product_id = c_list.collection_id " +
      			"and c_list.isbn = d_load.isbn " +
            	"and d_load.isbn = c_cnt.isbn " +
      			"and d_load.online_flag = 'Y') " +
      	 "order by total_ytd desc";
		
      
	 private static final SelectInfoBean[] QUERY1_SELECT_LIST = 
	 		new SelectInfoBean[]{
		 			new SelectInfoBean(ReportsBean.TITLE, SelectInfoBean.STRING), 
		 			new SelectInfoBean(ReportsBean.PUBLISHER, SelectInfoBean.STRING),
		 			new SelectInfoBean(ReportsBean.PLATFORM, SelectInfoBean.STRING),
		 			new SelectInfoBean(ReportsBean.ISBN, SelectInfoBean.STRING),
		 			new SelectInfoBean(ReportsBean.ISSN, SelectInfoBean.STRING),
		 			new SelectInfoBean(ReportsBean.JAN, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.FEB, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.MAR, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.APR, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.MAY, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.JUN, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.JUL, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.AUG, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.SEP, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.OCT, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.NOV, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.DEC, SelectInfoBean.INT)
		 			};
	 
		private static final String QUERY2 = 
			"select i.Title, " +
		    "'Cambridge University Press' as Publisher, " +
		    "'Cambridge Books Online' as Platform, " +
		    "i.ISBN, " +
		    "'n/a' as ISSN, " +
		    "nvl(jan_total,0) as jan, " +
		    "nvl(feb_total,0) as feb, " +
		    "nvl(mar_total,0) as mar, " +
		    "nvl(apr_total,0) as apr, " +
		    "nvl(may_total,0) as may, " +
		    "nvl(jun_total,0) as jun, " +
		    "nvl(jul_total,0) as jul, " +
		    "nvl(aug_total,0) as aug, " +
		    "nvl(sep_total,0) as sep, " +
		    "nvl(oct_total,0) as oct, " +
		    "nvl(nov_total,0) as nov, " +
		    "nvl(dec_total,0) as dec, " +
		    "nvl(total_ytd,0) as total_ytd " +
		    "from (select * from ebook_isbn_data_load where online_flag = 'Y' ) i " +
		    "LEFT OUTER JOIN (select * from cbo_s_counter_book " +
		    "where body_id = ? " +
		    "and year = ? ) c " +
		    "on c.isbn = i.isbn " +
		    "order by title";	
		
		private static final String QUERY3 = 
			"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, " +
	        "sum(JAN) JAN, SUM(FEB) FEB, sum(MAR) MAR, SUM(APR) APR, " +
	        "sum(MAY) MAY, SUM(JUN) JUN, sum(JUL) JUL, SUM(AUG) AUG, " +
	        "sum(SEP) SEP, SUM(OCT) OCT ,sum(NOV) NOV, SUM(DEC) DEC " +
	        "from( " + 
			"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN," + 
	        	"nvl(decode(dte,'01', count), 0) JAN," + 
	        	"nvl(decode(dte,'02', count), 0) FEB," + 
	        	"nvl(decode(dte,'03', count), 0) MAR," + 
				"nvl(decode(dte,'04', count), 0) APR," + 
				"nvl(decode(dte,'05', count), 0) MAY," + 
				"nvl(decode(dte,'06', count), 0) JUN," +
	        	"nvl(decode(dte,'07', count), 0) JUL," +
	        	"nvl(decode(dte,'08', count), 0) AUG," +
	        	"nvl(decode(dte,'09', count), 0) SEP," +
	        	"nvl(decode(dte,'10', count), 0) OCT," +
	        	"nvl(decode(dte,'11', count), 0) NOV," +
	        	"nvl(decode(dte,'12', count), 0) DEC " +
			"from (" +
				"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, to_char(TIMESTAMP,'MM') dte, count(1) count " + 
	        	"from ORAEBOOKS.BOOK_REPORT3 " +
				"where to_char(TIMESTAMP, 'YY') = ? " +
	         		"and BODY_ID = ? " +
				"group by TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, to_char(TIMESTAMP,'MM') ) " +
		    "order by ISBN " +
		    ") group by TITLE, PUBLISHER, PLATFORM, ISBN, ISSN ";    	
		
		private static final String QUERY4 = 
			"select SERVICE, PUBLISHER, PLATFORM, " +
	        "sum(JAN) JAN, SUM(FEB) FEB, sum(MAR) MAR, SUM(APR) APR, " +
	        "sum(MAY) MAY, SUM(JUN) JUN, sum(JUL) JUL, SUM(AUG) AUG, " +
	        "sum(SEP) SEP, SUM(OCT) OCT ,sum(NOV) NOV, SUM(DEC) DEC " +
	        "from( " + 
			"select SERVICE, PUBLISHER, PLATFORM, " + 
	        	"nvl(decode(dte,'01', count), 0) JAN," + 
	        	"nvl(decode(dte,'02', count), 0) FEB," + 
	        	"nvl(decode(dte,'03', count), 0) MAR," + 
				"nvl(decode(dte,'04', count), 0) APR," + 
				"nvl(decode(dte,'05', count), 0) MAY," + 
				"nvl(decode(dte,'06', count), 0) JUN," +
	        	"nvl(decode(dte,'07', count), 0) JUL," +
	        	"nvl(decode(dte,'08', count), 0) AUG," +
	        	"nvl(decode(dte,'09', count), 0) SEP," +
	        	"nvl(decode(dte,'10', count), 0) OCT," +
	        	"nvl(decode(dte,'11', count), 0) NOV," +
	        	"nvl(decode(dte,'12', count), 0) DEC " +
			"from (" +
				"select SERVICE, PUBLISHER, PLATFORM, to_char(TIMESTAMP,'MM') dte, count(1) count " + 
	        	"from ORAEBOOKS.BOOK_REPORT4 " +
				"where to_char(TIMESTAMP, 'YY') = ? " +
	         		"and BODY_ID = ? " +
				"group by SERVICE, PUBLISHER, PLATFORM, to_char(TIMESTAMP,'MM') )" +
			"order by SERVICE " +
		    ") group by SERVICE, PUBLISHER, PLATFORM ";    	
		
		private static final SelectInfoBean[] QUERY4_SELECT_LIST = 
	 		new SelectInfoBean[]{
					new SelectInfoBean(ReportsBean.SERVICE, SelectInfoBean.STRING), 
		 			new SelectInfoBean(ReportsBean.PUBLISHER, SelectInfoBean.STRING),
		 			new SelectInfoBean(ReportsBean.PLATFORM, SelectInfoBean.STRING),		 			
		 			new SelectInfoBean(ReportsBean.JAN, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.FEB, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.MAR, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.APR, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.MAY, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.JUN, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.JUL, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.AUG, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.SEP, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.OCT, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.NOV, SelectInfoBean.INT),
		 			new SelectInfoBean(ReportsBean.DEC, SelectInfoBean.INT)
		 			};
		
		private static final String QUERY5 = 
			"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, " +
	        "sum(JAN) JAN, SUM(FEB) FEB, sum(MAR) MAR, SUM(APR) APR, " +
	        "sum(MAY) MAY, SUM(JUN) JUN, sum(JUL) JUL, SUM(AUG) AUG, " +
	        "sum(SEP) SEP, SUM(OCT) OCT ,sum(NOV) NOV, SUM(DEC) DEC " +
	        "from( " +
			"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, " + 
	        	"nvl(decode(dte,'01', count), 0) JAN," + 
	        	"nvl(decode(dte,'02', count), 0) FEB," + 
	        	"nvl(decode(dte,'03', count), 0) MAR," + 
				"nvl(decode(dte,'04', count), 0) APR," + 
				"nvl(decode(dte,'05', count), 0) MAY," + 
				"nvl(decode(dte,'06', count), 0) JUN," +
	        	"nvl(decode(dte,'07', count), 0) JUL," +
	        	"nvl(decode(dte,'08', count), 0) AUG," +
	        	"nvl(decode(dte,'09', count), 0) SEP," +
	        	"nvl(decode(dte,'10', count), 0) OCT," +
	        	"nvl(decode(dte,'11', count), 0) NOV," +
	        	"nvl(decode(dte,'12', count), 0) DEC " +
			"from (" +
				"select TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, to_char(TIMESTAMP,'MM') dte, count(1) count " + 
	        	"from ORAEBOOKS.BOOK_REPORT5 " +
				"where to_char(TIMESTAMP, 'YY') = ? " +
	         		"and BODY_ID = ? " +
	         		"and IS_SEARCH = ? " +
				"group by TITLE, PUBLISHER, PLATFORM, ISBN, ISSN, to_char(TIMESTAMP,'MM') ) " +
			"order by ISBN " +
		    ") group by TITLE, PUBLISHER, PLATFORM, ISBN, ISSN ";  
		
		private static final String QUERY6 = 
			"select 'Cambridge Books Online' as Service, "  +
			" 'Cambridge University Press' as Publisher, "  +
			" 'Cambridge Books Online' as Platform, "  +
			" count_type, "  +
			" nvl(jan_total,0) as jan, "  +
			" nvl(feb_total,0) as feb, "  +
			" nvl(mar_total,0) as mar, "  +
			" nvl(apr_total,0) as apr, "  +
			" nvl(may_total,0) as may, "  +
			" nvl(jun_total,0) as jun, "  +
			" nvl(jul_total,0) as jul, "  +
			" nvl(aug_total,0) as aug, "  +
			" nvl(sep_total,0) as sep, "  +
			" nvl(oct_total,0) as oct, "  +
			" nvl(nov_total,0) as nov, "  +
			" nvl(dec_total,0) as dec, "  +
			" nvl(total_ytd,0) as total_ytd "  +
			" from app_repcbo.cbo_s_counter_search "  +
			" where body_id = ? "  +
			" and year = ? "  +
			" and count_type = ?";
			
			
	/**
	 * Override to provide a different connection method
	 * @return
	 */
	protected static Connection getConnection() {
		Connection conn = null;
		try {
			conn = ReportDBManager.openConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return conn;
	}
	
	public static final ArrayList<String> getReport(String year, int bodyId, String query,
			int reportType, String isSearch) throws Exception {
		ArrayList<String> erbArr = getReportBeans(query, reportType, year, bodyId, isSearch);
		if(reportType != 6){
			addSummaryRow(erbArr, reportType, isSearch);
			if(reportType == 2){
				B6C6SummaryRowCheck(erbArr, reportType, isSearch);
			}
		}
		setSummaryColumn(erbArr);
		return erbArr;
	}
	
	/**
	 * get Report1 || Report2 || Report3 || Report4
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<String> getReportQuery(String year, int bodyId, int reportNum) throws Exception{
		String queryName = "";
		if(1 == reportNum){
			queryName = QUERY1;
		}else if (2 == reportNum){
			queryName = QUERY2;
		}else if (3 == reportNum){
			queryName = QUERY3;
		}else if (4 == reportNum){
			queryName = QUERY4;
		}
		return getReport(year, bodyId, queryName, reportNum, "");
	}
	
	/**
	 * get Report5
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<String> getReport5(String year, int bodyId) throws Exception{		 		
		ArrayList<String> erbArrSearch = getReport(year, bodyId, QUERY5, 5, YES);
		ArrayList<String> erbArrSession = getReport(year, bodyId, QUERY5, 5, NO);
		
		ArrayList erbArrReport5 = new ArrayList();
		erbArrReport5.add(erbArrSearch);
		erbArrReport5.add(erbArrSession);
		return erbArrReport5;
	}
	
	/**
	 * get Report6
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<String> getReport6(String year, int bodyId) throws Exception{		 		
		//BODY_ID 26137
		ArrayList<String> erbArrSearch = getReport(year, bodyId, QUERY6, 6, YES);
		ArrayList<String> erbArrSession = getReport(year, bodyId, QUERY6, 6, NO);
		
		ArrayList erbArrReport5 = new ArrayList();
		erbArrReport5.add(erbArrSearch);
		erbArrReport5.add(erbArrSession);
		return erbArrReport5;
	}
	
	/**
	 * places additional labels on the summary row
	 * @param erb
	 * @param reportType
	 * @param isSearch
	 */
	private static void doAfterSummaryRow(ReportsBean erb, int reportType, String isSearch ) {
		if(reportType == 1 || reportType == 2){
			erb.setTitle(TITLE_SUMMARY_ROW_1_2);			
		}else if(reportType == 3){
			erb.setTitle(TITLE_SUMMARY_ROW_3_4);
		}else if(reportType == 4){
			erb.setService(TITLE_SUMMARY_ROW_3_4);
		}else if(reportType == 5){
			setIsSearchValues(erb, isSearch, reportType);
			if(YES.equals(isSearch)){
				erb.setTitle(TITLE_SUMMARY_ROW_5_6_SEARCHES);
			}else if(NO.equals(isSearch)){
				erb.setTitle(TITLE_SUMMARY_ROW_5_6_SESSIONS);
			}
		}else if(reportType == 6){
			setIsSearchValues(erb, isSearch, reportType);
			if(YES.equals(isSearch)){
				erb.setService(TITLE_SUMMARY_ROW_5_6_SEARCHES);
			}else if(NO.equals(isSearch)){
				erb.setService(TITLE_SUMMARY_ROW_5_6_SESSIONS);
			}
		}
	}
	
	/**
	 * add a summary row
	 * @param erbArr
	 * @throws Exception
	 */
	private static void addSummaryRow(ArrayList erbArr, int reportType, String isSearch) throws Exception {
		int countEndI = MONTHS_LIST.length;
		int countEndJ = erbArr.size();
		ReportsBean erb = new ReportsBean();
		for(int i=0; i < countEndI ; i++){
			int countTotal = 0; 
			for(int j=0; j < countEndJ ; j++){				
				ReportsBean erbTemp = (ReportsBean)erbArr.get(j);
				
				//save platform
				String beanPropVal = BeanUtils.getProperty(erbTemp,MONTHS_LIST[i]);
				int count = Integer.parseInt(beanPropVal);
				countTotal = countTotal + count;
			}
			BeanUtils.setProperty(erb,MONTHS_LIST[i],new Integer(countTotal));			
		}
		
		//add Title
		doAfterSummaryRow(erb, reportType, isSearch);
		erbArr.add(0,erb);
		
		if(reportType == 2){
			B6C6SummaryRowCheck(erbArr, reportType, isSearch);
		}
	}
	
	/**
	 * B6 AND C6 CHECKING - erbArr should have the summary row already, in the first index(0).
	 * @param erbArr
	 * @throws Exception
	 */
	private static void B6C6SummaryRowCheck(ArrayList erbArr, int reportType, String isSearch) throws Exception {
		int countEndI = MONTHS_LIST.length;
		int countEndJ = erbArr.size();
		
		ReportsBean summaryRowBean = new ReportsBean();
		summaryRowBean = (ReportsBean) erbArr.get(0);
		
		Set publisherSet = new HashSet(); 
		Set platformSet = new HashSet(); 
		
		//start in the second item
		for(int i=0; i < countEndI ; i++){
			for(int j=1; j < countEndJ ; j++){				
				ReportsBean record = (ReportsBean)erbArr.get(j);
				publisherSet.add(record.getPublisher());
				platformSet.add(record.getPlatform());
			}
		}	
		

		if(publisherSet.size() == 1){
			summaryRowBean.setPublisher(publisherSet.toArray()[0].toString());
		}	

		if(platformSet.size() == 1){
			summaryRowBean.setPlatform(platformSet.toArray()[0].toString());
		}	
		
		erbArr.remove(0);
		erbArr.add(0,summaryRowBean);		
		
	}
	
	/**
	 * computes the total for each row
	 * @param erbArr
	 * @throws Exception
	 */
	private static void setSummaryColumn(ArrayList erbArr) throws Exception {
		int countEndI = erbArr.size();
		int countEndJ = MONTHS_LIST.length;		
		for(int i=0; i < countEndI ; i++){
			int countTotal = 0; 
			ReportsBean erb = (ReportsBean)erbArr.get(i);
			for(int j=0; j < countEndJ ; j++){
				String beanPropVal = BeanUtils.getProperty(erb,MONTHS_LIST[j]);
				int count = Integer.parseInt(beanPropVal);
				countTotal = countTotal + count;
			}
			BeanUtils.setProperty(erb,ReportsBean.YTD_BEAN, new Integer(countTotal));			
		}
		
	}
	
	/**
	 * sets the prepared statement
	 * @param query
	 * @param reportType
	 * @param ps
	 * @param year
	 * @param bodyId
	 * @throws SQLException
	 */
	private static void doPrepareStatement(String query, int reportType, PreparedStatement ps, 
			String year, int bodyId, String isSearch) throws SQLException{
		if(reportType == 1 || reportType == 2 || reportType == 3 || reportType == 4){
			
			try {
				if(reportType == 2){
					ps.setInt(1, bodyId);
					ps.setString(2, year);
				}else if(reportType == 1){
					ps.setInt(1, bodyId);
					ps.setString(2, year);
					ps.setInt(3, bodyId);
					ps.setString(4, year);
					ps.setInt(5, bodyId);
					ps.setString(6, year);
					ps.setInt(7, bodyId);
					ps.setString(8, year);
				}else{
					ps.setString(1, year.substring(2));
					ps.setInt(2, bodyId);
				}
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
				
		}else if(reportType == 5 || reportType == 6){
			ps.setInt(1, bodyId);
			ps.setString(2, year);
			ps.setString(3, isSearch);
		}
	}
	
	/**
	 * retrieves the max select list
	 * @param reportType
	 * @return
	 */
	private static int getSelectCount(int reportType){
		if(reportType == 1 || reportType == 2 || reportType == 3 || reportType == 5){
			return QUERY1_SELECT_LIST.length;
		}else if(reportType == 4 || reportType == 6){
			return QUERY4_SELECT_LIST.length;
		}
		return 0;
	}
	
	/**
	 * retrieves the select info bean 
	 * @param reportType
	 * @return
	 */
	private static SelectInfoBean[] getSelectInfoBean(int reportType){
		if(reportType == 1 || reportType == 2 || reportType == 3 || reportType == 5){
			return QUERY1_SELECT_LIST;
		}else if(reportType == 4 || reportType == 6){
			return QUERY4_SELECT_LIST;
		}
		return null;
	}
	
	/**
	 * sets the rs val
	 * @param sib
	 * @param rs
	 * @param erb
	 * @throws Exception
	 */
	private static void getRsVal(SelectInfoBean sib, ResultSet rs, ReportsBean erb) throws Exception{
		if(SelectInfoBean.STRING == sib.getDataType()){
			String strVal = rs.getString(sib.getSelect());
			String beanProp = (String) ReportsBean.HASHMAP.get(sib.getSelect());
			BeanUtils.setProperty(erb, beanProp, strVal);
		}else if(SelectInfoBean.INT == sib.getDataType()){
			Integer intVal = new Integer( rs.getInt(sib.getSelect()) );
			String beanProp = (String) ReportsBean.HASHMAP.get(sib.getSelect());
			BeanUtils.setProperty(erb, beanProp, intVal);
		}
	}
	
	/**
	 * loads a value into ebook report bean
	 * @param rs
	 * @param reportType
	 * @return
	 * @throws Exception
	 */
	private static ReportsBean generateReportBean(ResultSet rs, int reportType, String isSearch) throws Exception {
		ReportsBean erb = new ReportsBean();
		int selectCount = getSelectCount(reportType);
		SelectInfoBean[] sibArr = getSelectInfoBean(reportType);
		for(int i=0; i < selectCount; i++ ){
			SelectInfoBean sib = sibArr[i];
			getRsVal(sib, rs, erb);
		}
		setIsSearchValues(erb, isSearch, reportType);
		return erb;
	}
	
	/**
	 * method for placing values on isSearch (for report 5 and 6)
	 * @param erb
	 * @param isSearch
	 * @param reportType
	 */
	public static void setIsSearchValues(ReportsBean erb, String isSearch, int reportType) {
		//additional processing for report 5 or 6
		if(reportType == 5 || reportType == 6){
			if(YES.equals(isSearch)){
				erb.setIsSearch(SEARCHES_RUN);
			}else if(NO.equals(isSearch)){
				erb.setIsSearch(SESSIONS);
			}			
		}
		
	}
	
	/**
	 * returns an array of report Beans (sql table representation)
	 * @param query
	 * @param reportType
	 * @param year
	 * @param bodyId
	 * @return
	 * @throws Exception
	 */
	private static ArrayList<String> getReportBeans(String query, int reportType, String year, int bodyId, String isSearch) throws Exception{		
		
		Connection conn = getConnection();		
		ArrayList rbList = null;
		
		try{
			PreparedStatement ps = conn.prepareStatement(query);			
			try{
				doPrepareStatement(query, reportType, ps, year, bodyId, isSearch);
				ResultSet rs = ps.executeQuery();
				rbList = new ArrayList();
				try{					
					while( rs.next()){				
						ReportsBean erb = generateReportBean(rs, reportType, isSearch);
						rbList.add(erb);	
					}
				}finally{
					rs.close();
				}
			}finally{
				ps.close();
			}
		}finally{
			conn.close();
		}
		return rbList;
	
	}
	
	public CounterReportsBean getOtherReport(String startDate, String endDate, int bodyId, CounterReportsBean bean ) throws Exception{
		
		CounterReportsBean repVo = (CounterReportsBean) bean;
		Connection conn = getConnection();
		
		try{
			
			String sql = "SELECT site, " +
							"to_char(monthyear,'Month YYYY') as \"Month\", " +
							"to_char(monthyear,'YYYY MM') as \"Sorter\", " +
							"display_name as \"Member Organisation\", " +
							"body_id as \"Account ID\", " +
							"sum(login) as \"Session (Logins)\", " +
							"TO_CHAR(TO_DATE(MOD(SUM(SUBSTR(TOTAL_SESSION_TIME,1,2))*60*60 + SUM(SUBSTR(TOTAL_SESSION_TIME,4,2))*60 + SUM(SUBSTR(TOTAL_SESSION_TIME,7,2)), 86400),'SSSSS'), 'HH24:MI:SS') AS \"Total Session Time (hh:mm:ss)\", " +
							"TO_CHAR(TO_DATE(MOD(round(SUM(SUBSTR(AVERAGE_SESSION_TIME,1,2))*60*60/sum(login) + SUM(SUBSTR(AVERAGE_SESSION_TIME,4,2))*60/sum(login) + SUM(SUBSTR(AVERAGE_SESSION_TIME,7,2))/sum(login),0), 86400),'SSSSS'), 'HH24:MI:SS') AS \"Ave Session Time (hh:mm:ss)\", " +
							"round(SUM(AVERAGE_PAGES_PER_SESSION)/sum(login),0) AS \"Average Pages Per Session\", " +
							"sum(full_content_units_requested) as \"Full-Content Units Requested\", " +
							"sum(web_page_requested) as \"Web Pages Requested\", " +
							"sum(total_searches) as \"Queries (Searches)\", " +
							"sum(units_reached_from_browse) as \"Full Units Reached from Browse\", " +
							"sum(turnaways) as \"Turnaways\" " +
						"FROM cbo_improvement_reports " +
						"WHERE body_id = ? and trunc(monthyear) between ? and ? " +
						"GROUP BY " +
						"site, " +
						"to_char(monthyear,'Month YYYY'), " +
						"to_char(monthyear,'YYYY MM'), " +
						"body_id, " +
						"display_name " +
						"ORDER BY to_char(monthyear,'YYYY MM') desc ";
				
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, bodyId);
				pstmt.setString(2, startDate); 
				pstmt.setString(3, endDate);
			try {
				ResultSet rs = pstmt.executeQuery();
				ArrayList results = new ArrayList();
				
				try{
				
						while (rs.next()){
							CounterReportsBean reportVo = new CounterReportsBean();
							reportVo.setSite(rs.getString("site"));
							reportVo.setMonth(rs.getString("Month"));
							reportVo.setMem_org(rs.getString("Member Organisation"));
							reportVo.setAccnt_id(rs.getInt("Account ID"));
							reportVo.setLogins(rs.getInt("Session (Logins)"));
							reportVo.setTotal_session(rs.getString("Total Session Time (hh:mm:ss)"));
							reportVo.setAverage_session(rs.getString("Ave Session Time (hh:mm:ss)"));
							reportVo.setAverage_pages(rs.getInt("Average Pages Per Session"));
							reportVo.setFull_content_units_requested(rs.getInt("Full-Content Units Requested"));
							reportVo.setWeb_pages(rs.getInt("Web Pages Requested"));
							reportVo.setQueries(rs.getInt("Queries (Searches)"));
							reportVo.setUnits_reached_from_browse(rs.getString("Full Units Reached from Browse"));
							reportVo.setTurnways(rs.getString("Turnaways"));
							if(null != reportVo){
								results.add(reportVo);
							}
						}
						repVo.setSearchResults(results);
					
				} catch(SQLException se) {
					System.out.println("error="+se.getMessage());
				} finally {
					rs.close();
				}
			}
			finally{
				pstmt.close();	
			}
		}finally{
			conn.close();
		}
		return repVo;
	}
	
	public CounterReportsBean getTotalOtherReport(String startDate, String endDate, int bodyId, CounterReportsBean bean ) throws Exception{
		
		CounterReportsBean repVo = (CounterReportsBean) bean;
		Connection conn = getConnection();
		
		try{

			StringBuffer sql = new StringBuffer();
				sql.append("SELECT site, " +
								"display_name as \"Member Organisation\", " +
								"body_id as \"Account ID\", sum(login) as \"Session (Logins)\", " +
								"TO_CHAR(TO_DATE(MOD(SUM(SUBSTR(TOTAL_SESSION_TIME,1,2))*60*60 + SUM(SUBSTR(TOTAL_SESSION_TIME,4,2))*60 + SUM(SUBSTR(TOTAL_SESSION_TIME,7,2)), 86400),'SSSSS'), 'HH24:MI:SS') AS \"Total Session Time (hh:mm:ss)\", " +
								"TO_CHAR(TO_DATE(MOD(round(SUM(SUBSTR(AVERAGE_SESSION_TIME,1,2))*60*60/sum(login) + SUM(SUBSTR(AVERAGE_SESSION_TIME,4,2))*60/sum(login) + SUM(SUBSTR(AVERAGE_SESSION_TIME,7,2))/sum(login),0), 86400),'SSSSS'), 'HH24:MI:SS') AS \"Ave Session Time (hh:mm:ss)\", " +
								"round(SUM(AVERAGE_PAGES_PER_SESSION)/sum(login),0) AS \"Average Pages Per Session\", " +
								"sum(full_content_units_requested) as \"Full-Content Units Requested\", " +
								"sum(web_page_requested) as \"Web Pages Requested\", " +
								"sum(total_searches) as \"Queries (Searches)\", " +
								"sum(units_reached_from_browse) as \"Full Units Reached from Browse\", " +
								"sum(turnaways) as \"Turnaways\" " +
							"FROM cbo_improvement_reports " +
							"WHERE body_id = ? and trunc(monthyear) between ? and ? " +
							"group by site, display_name, body_id " );
				
				PreparedStatement pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, bodyId);
				pstmt.setString(2, startDate); 
				pstmt.setString(3, endDate);
			try {
				ResultSet rs = pstmt.executeQuery();
				
				ArrayList results = new ArrayList();
				
				try{
					while (rs.next()){
						CounterReportsBean reportVo = new CounterReportsBean();
						reportVo.setSite(rs.getString("site"));
						//reportVo.setMonth(rs.getString("Month"));
						reportVo.setMem_org(rs.getString("Member Organisation"));
						reportVo.setAccnt_id(rs.getInt("Account ID"));
						reportVo.setLogins(rs.getInt("Session (Logins)"));
						reportVo.setTotal_session(rs.getString("Total Session Time (hh:mm:ss)"));
						reportVo.setAverage_session(rs.getString("Ave Session Time (hh:mm:ss)"));
						reportVo.setAverage_pages(rs.getInt("Average Pages Per Session"));
						reportVo.setFull_content_units_requested(rs.getInt("Full-Content Units Requested"));
						reportVo.setWeb_pages(rs.getInt("Web Pages Requested"));
						reportVo.setQueries(rs.getInt("Queries (Searches)"));
						reportVo.setUnits_reached_from_browse(rs.getString("Full Units Reached from Browse"));
						reportVo.setTurnways(rs.getString("Turnaways"));
						if(null != (reportVo)){
							results.add(reportVo);
						}
					}
					repVo.setSearchResults(results);
				} catch(SQLException se) {
					System.out.println("error="+se.getMessage());
				} finally {
					rs.close();
				}
			}
			finally{
				pstmt.close();	
			}
		}finally{
			conn.close();
		}
		return repVo;
	}

}
