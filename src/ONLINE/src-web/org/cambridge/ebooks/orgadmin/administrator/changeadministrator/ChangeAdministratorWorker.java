package org.cambridge.ebooks.orgadmin.administrator.changeadministrator;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.login.UnixCrypt;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

public class ChangeAdministratorWorker {
	private static final Logger LOGGER = Logger.getLogger(ChangeAdministratorWorker.class);
	
	public static int updateAdministrator(HttpServletRequest request,
			String userName, String orgBodyId) {
		
		String encryptedPassword = getUserPassword(userName);
		int ret = 0;
		if(UnixCrypt.matches(encryptedPassword,request.getParameter("currentPassword")) ||
				encryptedPassword.equals(request.getParameter("currentPassword"))){
			
			String userId = getUsername(request.getParameter("username"));
			if (!("".equals(userId) || null == userId)){
				String encryptedNewPassword = getUserPassword(request.getParameter("username"));
				
				 if (UnixCrypt.matches(encryptedNewPassword, request.getParameter("newPassword")) || 
						 encryptedNewPassword.equals(request.getParameter("newPassword"))){
		                ret = updateAdmin(request.getParameter("username"), userName, orgBodyId);	                
		            }else{
		                // Invalid password for new admin.
		                ret = 400;
		            }
			} else {
                // Invalid new admin username.
                ret = 300;
            }
		} else {
			// Invalid password for current admin.
			ret = 200;
		}
		
		return ret;
	}

	private static int updateAdmin(String username, String adminuser,
			String orgBodyId) {
		
		Connection conn = null;
		CallableStatement cstmt = null;
		int ret = 0;
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("{ call PKG_CJOC_REGISTERED_USER.CHANGE_ADMIN(?,?,?,?,?,?) }");
			
			int i=1;
			cstmt = conn.prepareCall(sql.toString());
            cstmt.registerOutParameter(i++, Types.INTEGER);
            cstmt.registerOutParameter(i++, Types.VARCHAR);
            cstmt.registerOutParameter(i++, Types.VARCHAR);
		    cstmt.setString(i++, username);
		    cstmt.setString(i++, adminuser);
		    cstmt.setString(i++, orgBodyId);
			
		    cstmt.execute(); 

		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cstmt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return ret;
	}

	private static String getUsername(String username) {
		LOGGER.info("getUsername");
		
		Connection conn = null;
		PreparedStatement pstat = null;
		ResultSet rs = null;
		String userId = "";
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("select MEMBER_USERID ")
				.append("from BLADE_MEMBER ")
				.append("where MEMBER_USERID = ?");
			
			pstat = conn.prepareStatement(sql.toString());
			pstat.setString(1, username);
			
			rs = pstat.executeQuery();
			while(rs.next()){
				userId = rs.getString("MEMBER_USERID");
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, pstat, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return userId;
	}

	private static String getUserPassword(String userName) {
		LOGGER.info("getUserPassword");
		
		Connection conn = null;
		PreparedStatement pstat = null;
		ResultSet rs = null;
		String password = "";
		
		try{
			conn = DBManager.openConnection();
			
			StringBuilder sql = new StringBuilder();
			sql.append("select MEMBER_SPELL ")
				.append("from BLADE_MEMBER ")
				.append("where MEMBER_USERID = ?");
			
			pstat = conn.prepareStatement(sql.toString());
			pstat.setString(1, userName);
			
			rs = pstat.executeQuery();
			while(rs.next()){
				password = rs.getString("MEMBER_SPELL");
			}
		}catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, pstat, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return password;
	}



}
