package org.cambridge.ebooks.orgadmin.administrator.configureip;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author kmulingtapang
 */
public class ConfigureIpBean {

	private static final Logger LOGGER = Logger.getLogger(ConfigureIpBean.class);
	
	private List<Ip> ipList;
	private List<Ip> consIpList;
	private List<ConsortiumOrganisationOption> consOrgOptionList;
	private IpErrorMessage ipErrorMessage;
	private boolean hasError;
	
	public String getInitialize() {
		LOGGER.info("===getInitialize");
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {
			String userType = login.getUserType();
			
			if("AO".equals(userType)) {
				configureIpOrg(request);
			} else if("AC".equals(userType)) {
				configureIpCons(request);
			} else {
				HttpUtil.redirect(request.getContextPath() + "/home.jsf");
			}
		}	
		
		return "";
	}
	
	public String getValidateText() {		
		return "";
	}	
	
	public List<Ip> getIpList() {
		return ipList;
	}	
	
	public List<Ip> getConsIpList() {
		return consIpList;
	}

	public List<ConsortiumOrganisationOption> getConsOrgOptionList() {
		return consOrgOptionList;
	}

	public IpErrorMessage getIpErrorMessage() {
		return ipErrorMessage;
	}

	public boolean getHasError() {
		return hasError;
	}
	
	private void configureIpOrg(HttpServletRequest request) {
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		if(null != login) {
			String organisationId = login.getOrgBodyId();
			
			String updateParam = request.getParameter("Update");
			String type = "AO";
			
			if(StringUtils.isNotEmpty(updateParam)){
				deleteIp(request, type);
				excludeIp(request, organisationId);			
				insertIp(request, type, organisationId);
			}
			
			selectOrgIp(organisationId);
		}
	}
	
	private void deleteIp(HttpServletRequest request, String type) {
		String acceptParam = request.getParameter("accept");
		String[] deleteIpArr = request.getParameterValues("delete");	
		
		if("AC".equals(type)) {
			deleteIpArr = request.getParameterValues("deleteCons");
		}
		
		if(null != deleteIpArr && deleteIpArr.length > 0 && "Y".equals(acceptParam)) {
			ConfigureIpWorker.deleteIp(deleteIpArr, type);
		}	
	}
	
	private void excludeIp(HttpServletRequest request, String organisationId) {
		String[] excludeIpArr = request.getParameterValues("exclude");
		String[] includeIpArr = request.getParameterValues("include");
		String[] deleteIpArr = request.getParameterValues("delete");		
		if(null != excludeIpArr && excludeIpArr.length > 0) {
			ConfigureIpWorker.includeOrgIp(excludeIpArr, deleteIpArr, "N", organisationId);
		}	
		
		if(null != includeIpArr && includeIpArr.length > 0) {
			ConfigureIpWorker.includeOrgIp(includeIpArr, deleteIpArr, "Y", organisationId);
		}
	}
	
	private void excludeConsIp(HttpServletRequest request, String consortiaId) {
		String[] excludeIpArr = request.getParameterValues("excludeCons");
		String[] includeIpArr = request.getParameterValues("includeCons");
		String[] deleteIpArr = request.getParameterValues("deleteCons");		
		if(null != excludeIpArr && excludeIpArr.length > 0) {
			ConfigureIpWorker.includeConsIp(excludeIpArr, deleteIpArr, "N", consortiaId);
		}	
		
		if(null != includeIpArr && includeIpArr.length > 0) {
			ConfigureIpWorker.includeConsIp(includeIpArr, deleteIpArr, "Y", consortiaId);
		}
	}
	
	private void insertIp(HttpServletRequest request, String type, String bodyId) {	
		String newIpList = request.getParameter("newIpList");
		String newIpAddr = request.getParameter("newIpAddr");
		String newExclude = request.getParameter("newExclude");
		
		if("AC".equals(type)) {
			newIpList = request.getParameter("newIpListCons");
			newIpAddr = request.getParameter("newIpAddrCons");
			newExclude = request.getParameter("newExcludeCons");
		}

		List<Ip> ipList = new ArrayList<Ip>();
		
		if(StringUtils.isNotEmpty(newIpList)) {
			String[] newIpArr = newIpList.split("[,\\s]+"); 
			for(String newIp : newIpArr) {
				Ip ip = new Ip();
				ip.setAddress(newIp);
				ip.setIncludeFlag("Y");
				ipList.add(ip);
			}
		}
		
		if(StringUtils.isNotEmpty(newIpAddr)) {
			Ip ip = new Ip();
			ip.setAddress(newIpAddr);
			if(StringUtils.isNotEmpty(newExclude)) {
				ip.setIncludeFlag(newExclude);
			} else {
				ip.setIncludeFlag("Y");
			}
			ipList.add(ip);
		}
		
		if(!ipList.isEmpty()) {				
			IpValidation ipValidation = new IpValidation(ipList, bodyId);
			
			if(ipValidation.isValid()) {
				hasError = false;
				ConfigureIpWorker.insertIp(ipList, bodyId, type);
			} else if(ipValidation.hasDuplicates()) {
				hasError = true;
				ConfigureIpWorker.insertIp(ipValidation.getValidIpList(), bodyId, type);
				ipErrorMessage = ipValidation.getIpErrorMessage();
			} else {
				hasError = true;
				ipErrorMessage = ipValidation.getIpErrorMessage();
			}
		}
	}
	
	private void selectOrgIp(String organisationId) {
		this.ipList = ConfigureIpWorker.selectOrgIp(organisationId);	
	}
	
	private void selectConsIp(String consortiaId) {
		this.consIpList = ConfigureIpWorker.selectConsIp(consortiaId);	
	}
	
	private void selectOrganisationsOfConsortia(HttpServletRequest request, String consortiaId) {		
		this.consOrgOptionList = ConfigureIpWorker.selectConsOrgByConsId(consortiaId);	
	}
	
	private void configureIpCons(HttpServletRequest request) {
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
		if(null != login) {
			String bodyId = login.getOrgBodyId();
			String methodParam = request.getParameter("method");		
			
			if(StringUtils.isNotEmpty(methodParam)){
				if("updateCons".equals(methodParam)) {
					String type = "AC";
					deleteIp(request, type);
					excludeConsIp(request, bodyId);			
					insertIp(request, type, bodyId);
				} else if("updateOrg".equals(methodParam)) {
					String type = "AO";
					String organisationId = request.getParameter("orgOption");
					deleteIp(request, type);
					excludeIp(request, organisationId);			
					insertIp(request, type, organisationId);
				} 
			}		
			
			// Organisations
			selectOrganisationsOfConsortia(request, bodyId);			
			if(null != consOrgOptionList && consOrgOptionList.size() > 0) {
				String organisationId = request.getParameter("orgOption");
				if(StringUtils.isEmpty(organisationId)) {
					ConsortiumOrganisationOption consOrgOption = consOrgOptionList.get(0); // get 1st on the list
					consOrgOption.setSelected("Y");
					organisationId = consOrgOption.getConsortiumOrganisation().getOrganisation().getBodyId();
				} else {
					for(ConsortiumOrganisationOption consOrgOption : consOrgOptionList) {
						if(organisationId.equals(consOrgOption.getConsortiumOrganisation().getOrganisation().getBodyId())){
							consOrgOption.setSelected("Y");
							break;
						}
					}
				}
				selectOrgIp(organisationId);
			}
			
			// Consortia
			selectConsIp(bodyId);
		}
	}
}
