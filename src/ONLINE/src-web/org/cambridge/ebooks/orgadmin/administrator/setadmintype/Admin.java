package org.cambridge.ebooks.orgadmin.administrator.setadmintype;

public class Admin {
	private String cboAdmin;
	private String cjoAdmin;
	
	
	public void setCboAdmin(String cboAdmin) {
		this.cboAdmin = cboAdmin;
	}
	public String getCboAdmin() {
		return cboAdmin;
	}
	public void setCjoAdmin(String cjoAdmin) {
		this.cjoAdmin = cjoAdmin;
	}
	public String getCjoAdmin() {
		return cjoAdmin;
	}
	
	
}
