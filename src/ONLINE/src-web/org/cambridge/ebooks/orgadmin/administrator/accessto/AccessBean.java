package org.cambridge.ebooks.orgadmin.administrator.accessto;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.user.Login;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.organisation.OrganisationWorker;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author kmulingtapang
 */
public class AccessBean {

	private static final Logger LOGGER = Logger.getLogger(AccessBean.class);
	
	private String freeTrial;
	
	public String getInitialize() {
		LOGGER.info("===getInitialize");
		
		HttpServletRequest request = HttpUtil.getHttpServletRequest();
		OrgConLinkedMap orgConLinkedMap = OrgConLinkedMap.getFromSession(request);		
		Login login = orgConLinkedMap.getUserLogin();
				
		if(null == login) {
			HttpUtil.redirect(request.getContextPath() + "/home.jsf");
		} else {					
			String bodyIds = orgConLinkedMap.getAllBodyIds();
			
			String freeTrial = OrganisationWorker.checkFreeTrial(bodyIds);
			if(StringUtils.isNotEmpty(freeTrial)) {			
				this.freeTrial = "YES";
			} else {			
				this.freeTrial = "NO";
			}
		}	
		
		return "";
	}
		
	public String getFreeTrial() {
		return freeTrial;
	}
}
