package org.cambridge.ebooks.orgadmin.stream;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.HttpUtil;

/**
 * @author kmulingtapang
 */
public class ForLibrariansPageBean {

	private static final Logger LOGGER = Logger.getLogger(ForLibrariansPageBean.class);
	
	private static final String FOR_LIBRARIAN_PAGE_ID = "5048";
	
	private String content;
	private List<StreamPageLevel2> streamPageLevel2List;
	
	public String getInitialize() {
		LOGGER.info("===initialize");
		
		StreamPage streamPage = null;
		
		String pageId = HttpUtil.getHttpServletRequestParameter("pageId");		
		String level = HttpUtil.getHttpServletRequestParameter("level");		
		if(StringUtils.isNotEmpty(pageId) && StringUtils.isNotEmpty(level) && "2".equals(level)) {
			streamPage = StreamWorker.getStreamPageLevel2(pageId);
		} else {
			streamPage = StreamWorker.getStreamPage(FOR_LIBRARIAN_PAGE_ID);
		}		
		
		content = streamPage.getContent();
		streamPageLevel2List = streamPage.getStreamLevel2List();
		
		return "";
	}
	
	public String getContent() {
		return content;
	}
	
	public List<StreamPageLevel2> getStreamPageLevel2List() {
		return streamPageLevel2List;
	}
}
