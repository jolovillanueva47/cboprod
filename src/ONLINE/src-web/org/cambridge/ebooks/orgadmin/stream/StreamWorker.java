package org.cambridge.ebooks.orgadmin.stream;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Struct;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;
import oracle.sql.CLOB;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.orgadmin.db.DBManager;

/**
 * @author kmulingtapang
 */
public class StreamWorker {

	private static final Logger LOGGER = Logger.getLogger(StreamWorker.class);
	
	public static StreamPage getStreamPage(String pageId) {
		LOGGER.info("===getStreamPage");
		
		StreamPage stream = new StreamPage();
		
		String sql = "{call PKG_CJOC_STREAM.get_stream(?,?,?,?)}";
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;	
		CLOB clobData = null;
		try {
			conn = DBManager.openConnection();
			cs = conn.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.registerOutParameter(2, Types.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.setString(4, pageId);
			
			cs.execute();
			
			int ret = cs.getInt(1);
			
			switch(ret) {
				
				case 0:
					rs = (ResultSet)cs.getObject(3);
					String flag = null;
					while(rs.next()) {					
						flag = rs.getString("flag");
						stream.setPageName(rs.getString("page_name"));
						stream.setPageDesc(rs.getString("page_desc"));
						
						if("Y".equals(flag)){
							stream.setUrl(rs.getString("url2"));
						} else {
							stream.setUrl(rs.getString("url"));
						}	
						clobData= (CLOB)rs.getObject("content");
						
						if (null != clobData){
							long clobLenLong = clobData.length();
							int clobLenInt = (new Long(clobLenLong)).intValue();
							stream.setContent(clobData.getSubString(1, clobLenInt));
						}
						
						stream.setStreamLevel2List((getStreamPageLevel2List((Array)rs.getObject("level2_page"))));
					}				
				
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return stream;
	}
	
	public static StreamPage getStreamPageLevel2(String pageId) {
		LOGGER.info("===getStreamPageLevel2");
		
		StreamPage stream = new StreamPage();
		
		String sql = "{call PKG_CJOC_STREAM.get_Level2(?,?,?,?)}";
		
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;	
		CLOB clobData = null;
		try {
			conn = DBManager.openConnection();
			cs = conn.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.registerOutParameter(2, Types.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.setString(4, pageId);
			
			cs.execute();
			
			int ret = cs.getInt(1);
			
			switch(ret) {				
				case 0:
					rs = (ResultSet)cs.getObject(3);
					String flag = null;
					while(rs.next()) {					
						flag = rs.getString("flag");
						stream.setPageName(rs.getString("page_name"));
						stream.setParentPageId(rs.getString("parent_page_id"));
						stream.setParent(rs.getString("parent"));
						
						if("Y".equals(flag)){
							stream.setUrl(rs.getString("url2"));
						} else {
							stream.setUrl(rs.getString("url"));
						}	
						clobData= (CLOB)rs.getObject("content");
						
						if (null != clobData){
							long clobLenLong = clobData.length();
							int clobLenInt = (new Long(clobLenLong)).intValue();
							stream.setContent(clobData.getSubString(1, clobLenInt));
						}
						
						stream.setStreamLevel2List(getStreamPageLevel2List((Array)rs.getObject("other_level2")));
						stream.setStreamLevel3List(getStreamPageLevel3List((Array)rs.getObject("level3_page")));
					}				
				
			}
			
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		} finally {
			try {
				DBManager.closeConnection(conn, cs, rs);
			} catch (Exception e) {
				LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
			}
		}
		
		return stream;
	}
	
	private static List<StreamPageLevel2> getStreamPageLevel2List(Array streamArray) {	    
		List<StreamPageLevel2> streamLevel2Array = new ArrayList<StreamPageLevel2>();
		
		try {
			if (streamArray != null) {
				Object[] structs = (Object[]) streamArray.getArray();
				
				for(int i = 0; null != structs && i < structs.length; i++) {					
					Struct suppStruct = (Struct) structs[i];
					if (suppStruct != null) {
						Object[] attributes = suppStruct.getAttributes();
						StreamPageLevel2 streamPageLevel2 = new StreamPageLevel2();
						String datum = ""; 
						String url11 = ""; 
						String url21 = ""; 
						String flag1 = "";		
						
						for(int x = 0; x < attributes.length; x++) {							
							if(attributes[x] != null) {
								/* As defined in Oracle CJO_STREAM */
								switch (x) {
									case 0: // PAGE_ID
										datum = attributes[x].toString();
										streamPageLevel2.setPageId(datum);  
										break;
									case 1: // PAGE_NAME
										datum = attributes[x].toString();
										streamPageLevel2.setPageName(datum);  
										break;
									case 2: // URL
										datum = attributes[x].toString();
										url11 = datum;  
										break;
									case 3:
										datum = attributes[x].toString();
										url21 = datum;
										break;
									case 4:
										datum = attributes[x].toString();
										flag1 = datum;
										break;
									case 5: //level3 pages															
										streamPageLevel2.setStreamLevel3List(getStreamPageLevel3List((Array)attributes[x]));
										break;
								}	
							}
						}
						if("Y".equals(flag1)){
							streamPageLevel2.setUrl(url21);
						} else {
							streamPageLevel2.setUrl(url11);
						}						
						streamLevel2Array.add(streamPageLevel2);
					}
				} 
			} 
		} catch (Exception e) {
			LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		return streamLevel2Array;
	}
	
	private static List<StreamPageLevel3> getStreamPageLevel3List(Array array) throws Exception {
		List<StreamPageLevel3> streamLevel3Array = new ArrayList<StreamPageLevel3>();
		if(null != array) {
			Object[] struct2 = (Object[])array.getArray() ;
			Struct level3Struct = null;											
			for(int y = 0; null != struct2 && y < struct2.length; y++) {
				level3Struct = (Struct) struct2[y];
				StreamPageLevel3 streamPageLevel3 = new StreamPageLevel3();
				String url = "";
				String url2 = "";
				String flag = "";
				if(level3Struct != null) {
					Object[] attributes2 = level3Struct.getAttributes();
					String datum2 = null;
					for(int w = 0; w < attributes2.length; w++) {
						if(attributes2[w] != null) {
							datum2 = attributes2[w].toString();															
							switch (w) {
								case 0: // PAGE_ID
									streamPageLevel3.setPageId(datum2);  
									break;  
								case 1: // PAGE_NAME
									streamPageLevel3.setPageName(datum2);  
									break;
								case 2: // URL
									url = datum2; 
								case 3:
									url2 = datum2;
									break;
								case 4:
									flag = datum2;
							}	
						} 
					} 
				} 
				if("Y".equals(flag)){
					streamPageLevel3.setUrl(url2);
				} else {
					streamPageLevel3.setUrl(url);
				}												
				streamLevel3Array.add(streamPageLevel3);
			}
		}
		return streamLevel3Array;
	}
}
