package org.cambridge.ebooks.orgadmin.stream;

/**
 * @author kmulingtapang
 */
/**
 * @author kmulingtapang
 *
 */
public class StreamPageLevel3 {

	private String pageId;
	private String url;
	private String pageName;
	private String publish;
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}		
}
