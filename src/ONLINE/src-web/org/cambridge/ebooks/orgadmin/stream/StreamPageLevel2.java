package org.cambridge.ebooks.orgadmin.stream;

import java.util.List;

/**
 * @author kmulingtapang
 */
public class StreamPageLevel2 {

	private String pageId;
	private String url;
	private String pageName;
	private String publish;
	private List<StreamPageLevel3> streamLevel3List;
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}
	public List<StreamPageLevel3> getStreamLevel3List() {
		return streamLevel3List;
	}
	public void setStreamLevel3List(List<StreamPageLevel3> streamLevel3List) {
		this.streamLevel3List = streamLevel3List;
	}		
}
