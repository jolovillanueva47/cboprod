/**
 * 
 */
package org.cambridge.ebooks.orgadmin.stream;

import java.util.List;

/**
 * @author kmulingtapang
 *
 */
public class StreamPage {
	private String parentPageId;
	private String parent;
	private String pageName;
	private String pageDesc;
	private String url;
	private String content;
	private List<StreamPageLevel2> streamLevel2List;
	private List<StreamPageLevel3> streamLevel3List;
		
	public String getParentPageId() {
		return parentPageId;
	}
	public void setParentPageId(String parentPageId) {
		this.parentPageId = parentPageId;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageDesc() {
		return pageDesc;
	}
	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<StreamPageLevel2> getStreamLevel2List() {
		return streamLevel2List;
	}
	public void setStreamLevel2List(List<StreamPageLevel2> streamLevel2List) {
		this.streamLevel2List = streamLevel2List;
	}
	public List<StreamPageLevel3> getStreamLevel3List() {
		return streamLevel3List;
	}
	public void setStreamLevel3List(List<StreamPageLevel3> streamLevel3List) {
		this.streamLevel3List = streamLevel3List;
	}	
}
