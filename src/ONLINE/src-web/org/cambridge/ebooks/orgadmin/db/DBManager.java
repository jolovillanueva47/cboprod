package org.cambridge.ebooks.orgadmin.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * @author kmulingtapang
 */
public class DBManager {

	private static final Logger LOGGER = Logger.getLogger(DBManager.class);
	
	protected static final String CJO_DS = "java:/oracjoc";
	
	public static Connection openConnection() throws Exception {
		LOGGER.info("===getConnection");
		LOGGER.info("===cjoDS:" + CJO_DS);
		
		Connection conn = null;
		
		Context ctx = new InitialContext();
		DataSource ds = (DataSource)ctx.lookup(CJO_DS);
		
		conn = ds.getConnection();		
		
		return conn;
	}
	
	public static void closeConnection(Connection conn) throws Exception {
		if(null != conn) {
			conn.close();
		}
	}
	
	public static void closeConnection(Connection conn, Statement st) throws Exception {
		if(null != st) {
			st.close();
		}
		
		closeConnection(conn);
	}
	
	public static void closeConnection(Connection conn, Statement st, ResultSet rs) throws Exception {
		if(null != rs) {
			 rs.close();
		}
		closeConnection(conn, st);
	}
}
