package org.cambridge.ebooks.orgadmin.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class ReportDBManager {

private static final Logger LOGGER = Logger.getLogger(ReportDBManager.class);
	
	protected static final String Report_DS = "java:/cboreports_oracle_ds_report";
	
	public static Connection openConnection() throws Exception {
		LOGGER.info("===getConnection");
		LOGGER.info("===cboreports_oracle_ds_report:" + Report_DS);
		
		Connection conn = null;
		
		Context ctx = new InitialContext();
		DataSource ds = (DataSource)ctx.lookup(Report_DS);
		
		conn = ds.getConnection();		
		
		return conn;
	}
	
	public static void closeConnection(Connection conn) throws Exception {
		if(null != conn) {
			conn.close();
		}
	}
	
	public static void closeConnection(Connection conn, Statement st) throws Exception {
		if(null != st) {
			st.close();
		}
		
		closeConnection(conn);
	}
	
	public static void closeConnection(Connection conn, Statement st, ResultSet rs) throws Exception {
		if(null != rs) {
			 rs.close();
		}
		closeConnection(conn, st);
	}
}
