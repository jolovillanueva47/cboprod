package org.cambridge.ebooks.online.oracle.ip;

import java.util.ArrayList;



/**
 * taken from CJO's CheckAccessWorker.generateOrgIp
 *
 */
public class CheckAccessWorker {
	
	public static final int START_MARKER = 0;
	public static final int END_MARKER = 1;
	
	
//	private static boolean isEmpty(String input){
//		if(input == null || input.trim().length() == 0){
//			return true;
//		}
//		return false;
//	}
	
	public static long getStartIp(String ip){
		//return _getStartIp(ip);
		return getRange(ip,START_MARKER);
	}
	
	public static long _getStartIp(String ip){
		return -3;
	}
	
	public static long getEndIp(String ip){
		return getRange(ip,END_MARKER);
	}
	
	private static long getRange(String ip, int endStartMarker){		
		ip = ip.trim();
		try {
			if(ip.indexOf("/") > -1){
				if(START_MARKER == endStartMarker){
					return CidrToLong.getStart(ip);
					//return 1;
				}else{
					return CidrToLong.getEnd(ip);
					//return -1;
				}
			}else{
				return getNormalRange(ip, endStartMarker);
				//return -200;
			}
		} catch (Exception e) {		
			return -100;
		}
	}
	
	private static long getNormalRange(String ip, int endStartMarker){
		ArrayList ipset= ProcessIp.IpRange(ip);
		String _ip = (String) ipset.get(endStartMarker);
		return ConvertIp.ipToLong(_ip);
	}

	
//	public static void main(String[] args) {
//		System.out.println("start1");
//		long start = getStartIp("192.168.242.48/30");  
//		long end = getEndIp("192.168.242.48/30");
//		System.out.println("start2");
//		System.out.println("start = " + ConvertIp.longToIp(start));
//		System.out.println("end = " + ConvertIp.longToIp(end));
//		System.out.println("end");
//	}

}
