package org.cambridge.ebooks.online.oracle.ip;

public class IPAuthenticationOracle {	
	public static long getStart(String ip){
		return CheckAccessWorker.getStartIp(ip);
	}
	
	public static long getEnd(String ip){
		return CheckAccessWorker.getEndIp(ip);
	}
}
