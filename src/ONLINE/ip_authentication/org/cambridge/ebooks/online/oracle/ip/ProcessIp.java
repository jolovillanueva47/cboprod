package org.cambridge.ebooks.online.oracle.ip;

/*
 * Created on Oct 10, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */


import java.util.ArrayList;

/**
 * Copy pasted from CJO ProcessIp
 * 
 * @author mmanalo
 * 
 */
public class ProcessIp {
	
	private static final String[] ipRange = new String[]{
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
			"(\\d{1,3}\\.\\d{1,3}\\.\\*)",
			"(\\d{1,3}\\.\\d{1,3})",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})",
			"(\\d{1,3}\\.\\d{1,3}\\.\\*\\.\\*)",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\*)",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\*)",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})",
			"(\\d{1,3}\\.\\*)", "(\\d{1,3}\\.\\*\\.\\*)",
			"(\\d{1,3}\\.\\*\\.\\*\\.\\*)",
			"(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\-\\d{1,3})"
		};
	                           

	public ProcessIp() {

	}

	public static int getIpPattern(String ip) {
		int flag = 100;

		for (int i = 0; i < ipRange.length; i++) {
			if (ip.matches(ipRange[i])) {
				flag = i;
			}
		}
		return flag;
	}

	public static ArrayList IpRange(String ipaddr) {
		int flag=getIpPattern(ipaddr);
		ArrayList ipset = new ArrayList();
		String startIp="";
		String endIp="";
		switch (flag) {			
			case 0 : // '\0'  a.b.c.d 
				{					
					ipset.add(getIpLeft(ipaddr)+"."+getIpRight(ipaddr));
					ipset.add(getIpLeft(ipaddr)+"."+getIpRight(ipaddr));
					break;
				}
			case 1 : // '\001' 192.168.*
			case 4 : // '\004' 192.168.*.*
				{
					startIp=getIpLeft(ipaddr)+".0.0";
					endIp=getIpLeft(ipaddr)+".255.255";
					ipset.add(startIp);
					ipset.add(endIp);	
					break;
				}				
			case 2 : // '\002' 192.168
				{
					startIp=ipaddr+".0.0";
					endIp=ipaddr+".255.255";
					ipset.add(startIp);
					ipset.add(endIp);
					break;
				}			
			case 3 : // '\003'   192.168.128
				{
			        startIp=ipaddr+".0";
					endIp=ipaddr+".255";
					ipset.add(startIp);
					ipset.add(endIp);
				    break;
				}			
			case 5 : // '\005'   192.168.128.*
				{
					startIp=ipaddr.replaceAll("\\*", "0");
					endIp=ipaddr.replaceAll("\\*", "255");
					ipset.add(startIp);
					ipset.add(endIp);
					break;
				}			
			case 12:
			case 6 : // '\006'  192.168.10-15.*
				{	
					String ipright[] = getIpRight(ipaddr).split("\\.");
					String limits[] = ipright[0].split("-");
				    startIp=getIpLeft(ipaddr)+"."+limits[0]+".0";
					endIp=getIpLeft(ipaddr)+"."+limits[1]+".255";
					ipset.add(startIp);
					ipset.add(endIp);				
					break;
				}
			case 7 : // '\007'   192.168.1.1-20
				{
					String ipleft =ipaddr.substring(0,ipaddr.indexOf(".",ipaddr.indexOf(".", ipaddr.indexOf(".") + 1) + 1));
					String ipright[] = getIpRight(ipaddr).split("\\.");
					String limits[] = ipright[1].split("-");
					startIp=ipleft+"."+limits[0];
					endIp=ipleft+"."+limits[1];
					ipset.add(startIp);
					ipset.add(endIp);
					break;
				}
			case 8 : // '\008'
				{
					String ipleft = getIpLeft(ipaddr); //ipaddr.substring(0,ipaddr.indexOf(".",ipaddr.indexOf(".", ipaddr.indexOf(".") + 1) + 1));
					String ipright[] = getIpRight(ipaddr).split("\\.");
					String limits0[] = ipright[0].split("-");
					String limits1[] = ipright[1].split("-");
					startIp=ipleft+"."+limits0[0]+"."+limits1[0];
					endIp=ipleft+"."+limits0[1]+"."+limits1[1];
					ipset.add(startIp);
					ipset.add(endIp);					
					break;
				}
			case 9 : //  '\b'
			case 10 :
			case 11 :
				{
					startIp=ipaddr.substring(0, ipaddr.indexOf(".") + 1)+"0.0.0";
					endIp=ipaddr.substring(0, ipaddr.indexOf(".") + 1)+"255.255.255";
					ipset.add(startIp);
					ipset.add(endIp);
					break;
				}
			}
		
		return ipset;
	}

	private static String getIpLeft(String ip) {
		return ip.substring(0, ip.indexOf(".", ip.indexOf(".") + 1));
	}

	private static String getIpRight(String ip) {
		return ip.substring(ip.indexOf(".", ip.indexOf(".") + 1) + 1);
	}

}