package org.cambridge.ebooks.online.oracle.ip;



import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class IP implements Comparable, java.io.Serializable{
	
	private int a = -1;
	private int b = -1;
	private int c = -1;
	private int d = -1;
	
	/**
	 * Creates an IP based on the inputed String.
	 *
	 * @param ip a String representation of an IP
	 *           of the form XXX.XXX.XXX.XXX
	 *
	 * @exception UnknownHostException if <code> ip </code> is not in the
	 * correct form
	 */
	
	public IP(String ip) throws UnknownHostException {
		if(ip != null){
			parseIP(ip);
		}
	}
	
	public IP(int a, int b, int c, int d) throws UnknownHostException {
		buildIP(a,b,c,d);
	}
	
	private void parseIP(String ip) throws UnknownHostException {
		try{
			StringTokenizer toks = new StringTokenizer(ip, ".");
			buildIP(Integer.parseInt(toks.nextToken()),
					Integer.parseInt(toks.nextToken()),
					Integer.parseInt(toks.nextToken()),
					Integer.parseInt(toks.nextToken()));
		}catch(NullPointerException npe){
			throw new UnknownHostException
			("Invalid IP: " + ip);
		}catch(NumberFormatException nfe){
			throw new UnknownHostException
			("Invalid IP: " + ip);
		}catch(NoSuchElementException nsee){
			throw new UnknownHostException
			("Invalid IP: " + ip);
		}
	}
	
	private void buildIP(int a, int b, int c, int d) throws UnknownHostException{
		if(a >= 0 && a <=255){
			this.a = a;
		}
		else{
			throw new UnknownHostException
			("Invalid IP: " + a + "." + b + "." + c + "." + d);
		}
		if(b >= 0 && b <= 255){
			this.b = b;
		}
		else{
			throw new UnknownHostException
			("Invalid IP: " + a + "." + b + "." + c + "." + d);
		}
		if(c >= 0 && c <=255){
			this.c = c;
		}
		else{
			throw new UnknownHostException
			("Invalid IP: " + a + "." + b + "." + c + "." + d);
		}
		if(d >= 0 && d <=255){
			this.d = d;
		}
		else{
			throw new UnknownHostException
			("Invalid IP: " + a + "." + b + "." + c + "." + d);
		}
	}
	
	/**
	 * Returns a String representation of the IP in the form XXX.XXX.XXX.XXX
	 */
	
	public String toString(){
		return a + "." + b + "." + c + "." + d;
	}
	
	/**
	 * Sets this object"s IP.
	 *
	 * @param ip a String representing an IP in the form XXX.XXX.XXX.XXX
	 *
	 * @exception UnknownHostException if <code> ip </code> is not in the
	 * correct form.
	 */
	
	public void setIP(String ip) throws UnknownHostException{
		if(ip == null){
			throw new NullPointerException();
		}
		parseIP(ip);
	}
	
	/**
	 * Returns <code> true </code> if this IP has been set with an IP.
	 */
	
	public boolean set(){
		if(a != -1 && b != -1 && c != -1 && d != -1){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Compares this IP address against another, returning <code> true </code> if
	 * they are the same address
	 */
	
	public boolean equals(IP i){
		if(i == null){
			return false;
		}
		if(d != i.getD()){
			return false;
		}
		if(c != i.getC()){
			return false;
		}
		if(b != i.getB()){
			return false;
		}
		if(a != i.getA()){
			return false;
		}
		return true;
	}
	
	public int compareTo(Object input) throws ClassCastException{
		if(input == null){
			return 1;
		}
		return compareTo((IP)input);
	}
	
	public int compareTo(IP i){
		if(i == null){
			return 1;
		}
		
		int classA = i.getA();
		int classB = i.getB();
		int classC = i.getC();
		int classD = i.getD();
		
		if(a < classA){
			return -1;
		}
		else if(a > classA){
			return 1;
		}
		else{
			if(b < classB){
				return -1;
			}
			else if(b > classB){
				return 1;
			}
			else{
				if(c < classC){
					return -1;
				}
				else if(c > classC){
					return 1;
				}
				else{
					if(d < classD){
						return -1;
					}
					else if(d > classD){
						return 1;
					}
					else{
						return 0;
					}
				}
			}
		}
	}
	
	public int getA(){
		return a;
	}
	
	public IP getClassA(){
		try{
			return new IP(a,0,0,0);
		}catch(UnknownHostException uhe){
//			Will never happen.
			return null;
		}
	}
	
	public int getB(){
		return b;
	}
	
	public IP getClassB(){
		try{
			return new IP(a,b,0,0);
		}catch(UnknownHostException uhe){
//			Will never happen.
			return null;
		}
	}
	
	public int getC(){
		return c;
	}
	
	public IP getClassC(){
		try{
			return new IP(a,b,c,0);
		}catch(UnknownHostException uhe){
//			Will never happen.
			return null;
		}
	}
	
	public int getD(){
		return d;
	}
	
	public IP getClassD(){
		try{
			return new IP(a,b,c,d);
		}catch(UnknownHostException uhe){
//			Will never happen.
			return null;
		}
	}
}



