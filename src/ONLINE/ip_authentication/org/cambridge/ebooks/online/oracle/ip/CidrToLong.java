package org.cambridge.ebooks.online.oracle.ip;


public class CidrToLong {
	public static void main(String[] args) {
		getStart("127.1.1.1/30");		
		getEnd("127.1.1.1/30");
	}
	
	public static final int MAX_LENGTH = 32; 
	
	/**
	 * in getting the start value
	 * example, 127.1.1.1/8
	 * binary 				011111111 00000001 0000001 00000001
	 * 'and' with mask      011111111 00000000 0000000 00000000  -- copy the first /x values and pad zero, in this case 8
	 * resulting to 		011111111 00000000 0000000 00000000 -----> 127.0.0.0
	 * @param cidr
	 * @return
	 */
	public static long getStart(String cidr){
		String[] cidrArr = cidr.split("/");
		long ip = ConvertIp.ipToLong(cidrArr[0]);
		String ipBin = Long.toBinaryString(ip);
		//and the mask the cidr ip
		long _ip = Long.parseLong(ipBin, 2) & generateMask(cidrArr[1]);
		//System.out.println(ConvertIp.longToIp(_ip));
		return _ip;
	}
	
	
	/**
	 * in getting the end value
	 * example, 127.1.1.1./8
	 * binary						01111111 00000001 0000001 00000001
	 * get the /x values from left  01111111
	 * the pad the rest with 1		01111111 11111111 1111111 11111111 --> 127.255.255.255
	 * @param cidr
	 * @return
	 */
	public static long getEnd(String cidr){
		String[] cidrArr = cidr.split("/");		
		int ival = Integer.parseInt(cidrArr[1]);		
		StringBuffer sb = new StringBuffer();
		
		//the base end are the bits from left to right (starting from left most to the /x value)
		sb.append(getBaseEnd(cidrArr[0], ival));		
		
		//pad ones afterwards
		padOne(sb, ival);		
		return Long.parseLong(sb.toString(),2);
	}
	
	private static long generateMask(String value){
		int ival = Integer.parseInt(value);
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i <= ival; i++) {
			sb.append("1");
		}		
		for (int i = ival + 1; i <= MAX_LENGTH; i++) {
			sb.append("0");
		}	
		return Long.parseLong(sb.toString(),2);		
	}
	
	private static void padZero(StringBuffer sb){
		int length = sb.length();
		sb.reverse();
		for(int i = length; i < MAX_LENGTH; i++){
			sb.append("0");
		}
		sb.reverse();
	}
	
	private static void padOne(StringBuffer sb, int length){				
		for (int i = length + 1; i <= MAX_LENGTH; i++) {
			sb.append("1");
		}
	}
	
	private static String getBaseEnd(String cidr, int length){
		String ip = Long.toBinaryString(ConvertIp.ipToLong(cidr));
		StringBuffer sb = new StringBuffer();
		sb.append(ip);		
		padZero(sb);		
		return sb.toString().substring(0, length);
	}
	
	
}
