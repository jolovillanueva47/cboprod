<?xml version="1.0" encoding="utf-8"?>
<!-- 
	===============================	
	XSLT for converting vyre's subject tree.
	
	Note: Append <?xml version="1.0" encoding="utf-8"?><root> at the start and </root> at the end of the file to convert.
		  Find and replace all &amp; to & on the transformed file.
	
	-Cito
	===============================
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" />
	
	<xsl:template match="/">
		<div id="subjectTree" class="subjectTree">
		<ul class="level1">
		<xsl:for-each select="/root/h2">
			
			<li>
				<xsl:variable name="level1SubjectName" select="text()"/>
				<xsl:variable name="level1SubjectCode" select="substring($level1SubjectName, 0, 4)" />
				
				<a class="level1Link arrow" href="javascript:void(0)">
					<xsl:attribute name="id">
						<xsl:value-of select="$level1SubjectCode"/>
					</xsl:attribute>
					<img src="images/level_icon.gif" />
				</a>
				<a class="level1Link" href="javascript:void(0)">
					<xsl:attribute name="id">
						<xsl:value-of select="$level1SubjectCode"/>
					</xsl:attribute>
					<xsl:value-of select="$level1SubjectName" />
				</a>
				
				<xsl:for-each select="following-sibling::ul[position()=1]">
					<ul class="level2" style="display: none;">
						<xsl:for-each select="li">
							
							<xsl:variable name="level2subjectCode" select="substring-before(substring-after(text(),'(vista code: '),')')"/>
							<xsl:variable name="level2subjectName" select="substring-before(text(),' (vista code')"/>
							
							<li class="level2Li" style="display: none;">
								<xsl:attribute name="id">
									<xsl:value-of select="$level2subjectCode"/>
								</xsl:attribute>
								
								<xsl:choose>
									<xsl:when test="child::ul">
										
										<xsl:variable name="child" select="string(child::ul/child::li/text())" />
										<xsl:variable name="childSubjectCode" select="substring-before(substring-after($child,'(vista code: '),')')"></xsl:variable>
										
										<xsl:choose>
											
											<xsl:when test="string($childSubjectCode)">
												<a class="level2Link arrow" href="javascript:void(0)">
													<xsl:attribute name="id">
														<xsl:value-of select="$level2subjectCode"/>
													</xsl:attribute>
													<img src="images/level_icon.gif"/>
												</a>
												<a class="level2Link" href="javascript:void(0)">
													<xsl:attribute name="id">
														<xsl:value-of select="$level2subjectCode"/>
													</xsl:attribute>
													<xsl:value-of select="substring-before(text(),'(vista code')"/>
												</a><!--Art-->
												
												<ul class="level3" style="display: none;">
													<xsl:for-each select="child::ul/li">
														<xsl:variable name="subjectCode" select="substring-before(substring-after(text(),'(vista code: '),')')"/>
														<xsl:variable name="subjectName" select="substring-before(text(),' (vista code')"/>
														
														<li class="level3Li" style="display: none;">
															<xsl:attribute name="id">
																<xsl:value-of select="$subjectCode"/>
															</xsl:attribute>
															
															<img src="images/green_circle_icon.gif"/>
															<a class="level3Link">
																<xsl:attribute name="href">
																	<xsl:text>subject_landing.jsf?searchType=allTitles&amp;subjectCode=</xsl:text>
																	<xsl:value-of select="$subjectCode"/>
																	<xsl:text>&amp;</xsl:text>
																	<xsl:text>subjectName=</xsl:text>
																	<xsl:value-of select="$subjectName"/>
																</xsl:attribute>
																<xsl:value-of select="$subjectName"/>
															</a>
														</li><!--Applied arts-->
													</xsl:for-each>
												</ul>
												
											</xsl:when>
											
											<xsl:otherwise>
												<xsl:variable name="subjectCode" select="substring-before(substring-after(text(),'(vista code: '),')')"/>
												<xsl:variable name="subjectName" select="substring-before(text(),' (vista code')"/>
												
												
												<img src="images/green_circle_icon.gif"/>
												<a>
													<xsl:attribute name="href">
														<xsl:text>subject_landing.jsf?searchType=allTitles&amp;subjectCode=</xsl:text>
														<xsl:value-of select="$subjectCode"/>
														<xsl:text>&amp;</xsl:text>
														<xsl:text>subjectName=</xsl:text>
														<xsl:value-of select="$subjectName"/>
													</xsl:attribute>
													<xsl:value-of select="$subjectName"/>
												</a><!--Art-->
											</xsl:otherwise>
											
										</xsl:choose>
										
										
										
									</xsl:when>
									<xsl:otherwise>
										
										
										<img src="images/green_circle_icon.gif"/>
										<a class="level2Link circle">
											<xsl:attribute name="href">
												<xsl:text>subject_landing.jsf?searchType=allTitles&amp;subjectCode=</xsl:text>
												<xsl:value-of select="$level2subjectCode"/>
												<xsl:text>&amp;</xsl:text>
												<xsl:text>subjectName=</xsl:text>
												<xsl:value-of select="$level2subjectName"/>
											</xsl:attribute>
											<xsl:value-of select="$level2subjectName"/>
										</a><!--Art-->
									</xsl:otherwise>
								</xsl:choose>
							</li>
						</xsl:for-each>
					</ul>
				</xsl:for-each>
				
			</li><!--Humanities-->
			
		</xsl:for-each>
		</ul>
		</div>
	</xsl:template>
	
	
</xsl:stylesheet>