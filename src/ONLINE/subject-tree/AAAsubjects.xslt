<?xml version="1.0" encoding="utf-8"?>
<!-- 
	===============================	
	XSLT for converting vyre's subject tree.
	
	Note: Append <?xml version="1.0" encoding="utf-8"?><root> at the start and </root> at the end of the file to convert.
		  Find and replace all &amp; to & on the transformed file.
	
	-Cito
	===============================
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" />
	
	<xsl:template match="/">
		<ul id="red" class="treeview" style="display: none;">
		<xsl:for-each select="/root/h2">
			
			<li class="parentLink">
				<xsl:variable name="level1SubjectName" select="text()"/>
				<xsl:variable name="level1SubjectCode" select="substring($level1SubjectName, 0, 4)" />
				
				<span>
					<xsl:attribute name="id">
						<xsl:value-of select="$level1SubjectCode"/>
					</xsl:attribute>
					<xsl:value-of select="$level1SubjectName"/>
				</span>
				
				<xsl:for-each select="following-sibling::ul[position()=1]">
					<ul>
						<xsl:for-each select="li">
							
							<xsl:variable name="level2subjectCode" select="substring-before(substring-after(text(),'(vista code: '),')')"/>
							<xsl:variable name="level2subjectName" select="substring-before(text(),' (vista code')"/>
								
							<xsl:choose>
				
								<xsl:when test="child::ul">
									
									<xsl:variable name="child" select="string(child::ul/child::li/text())" />
									<xsl:variable name="childSubjectCode" select="substring-before(substring-after($child,'(vista code: '),')')"></xsl:variable>
									
									<xsl:choose>
										
										<xsl:when test="string($childSubjectCode)">
											
											<li class="parentLink">
												<xsl:attribute name="id">
													<xsl:value-of select="$level2subjectCode"/>
												</xsl:attribute>
												
												<span>
													<xsl:attribute name="id">
														<xsl:value-of select="$level2subjectCode"/>
													</xsl:attribute>
													<a href="javascript:void(0);">
														<xsl:value-of select="$level2subjectName"/>
													</a>
												</span>
											
												<ul>
													<xsl:for-each select="child::ul/li">
														<xsl:variable name="subjectCode" select="substring-before(substring-after(text(),'(vista code: '),')')"/>
														<xsl:variable name="subjectName" select="substring-before(text(),' (vista code')"/>
														
														<li>
															<xsl:attribute name="id">
																<xsl:value-of select="$subjectCode"/>
															</xsl:attribute>
															<span><a>
																<xsl:attribute name="href">
																	<xsl:text>subject_landing.jsf?searchType=allTitles&subjectCode=</xsl:text>
																	<xsl:value-of select="$subjectCode"/>
																	<xsl:text>&amp;</xsl:text>
																	<xsl:text>subjectName=</xsl:text>
																	<xsl:value-of select="$subjectName"/>
																</xsl:attribute>
																<xsl:value-of select="$subjectName"/>
															</a></span>
														</li><!--Applied arts-->
													</xsl:for-each>
												</ul>
											</li>
										</xsl:when>
										
										<xsl:otherwise>
											
											<li>
												<xsl:attribute name="id">
													<xsl:value-of select="$level2subjectCode"/>
												</xsl:attribute>
												<span ><a>
													<xsl:attribute name="href">
														<xsl:text>subject_landing.jsf?searchType=allTitles&subjectCode=</xsl:text>
														<xsl:value-of select="$level2subjectCode"/>
														<xsl:text>&amp;</xsl:text>
														<xsl:text>subjectName=</xsl:text>
														<xsl:value-of select="$level2subjectName"/>
													</xsl:attribute>
													<xsl:value-of select="$level2subjectName"/>
												</a></span><!--Art-->
												<ul></ul>
											</li>
										</xsl:otherwise>
										
									</xsl:choose>
									
									
									
								</xsl:when>
								<xsl:otherwise>
									
									
									<li class="seriesLink">
										<xsl:attribute name="id">
											<xsl:value-of select="$level2subjectCode"/>
										</xsl:attribute>
										<span class="seriesLink"><a href="javascript:void(0);">
											<xsl:attribute name="href">
												<xsl:text>subject_landing.jsf?searchType=allTitles&subjectCode=</xsl:text>
												<xsl:value-of select="$level2subjectCode"/>
												<xsl:text>&amp;</xsl:text>
												<xsl:text>subjectName=</xsl:text>
												<xsl:value-of select="$level2subjectName"/>
											</xsl:attribute>
											<xsl:value-of select="$level2subjectName"/>
										</a></span><!--Art-->
										<ul></ul>
									</li>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</ul>
				</xsl:for-each>
				
			</li><!--Humanities-->
			
		</xsl:for-each>
		</ul>
	</xsl:template>
	
	
</xsl:stylesheet>