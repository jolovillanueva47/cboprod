package org.cambridge.ebooks.online.internalReports.jpa;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table ( name = EventIpExcludeBean.TABLE_NAME )
public class EventIpExcludeBean {
	
	public static final String TABLE_NAME = "EVENT_IP_EXCLUDE";
	
	public static final String SEARCH_EXCLUDE = 
		"select * from EVENT_IP_EXCLUDE " +
		 "where exclude='Y' " +
		   "and trunc(sysdate) >= trunc(effective_from) " +  
		   "and (effective_to is null or trunc(sysdate) <= trunc(effective_to)) " +
		   "and IP_ADDRESS = ?1 ";
	
	
	
	@Id
	@Column(name = "IP_ADDRESS")
	private String ipAddress;
	
	@Column(name = "REASON")
	private String reason;
	
	@Column(name = "EFFECTIVE_FROM")
	private Date effectiveFrom;
	
	@Column(name = "EFFECTIVE_TO")
	private Date effectiveTo;
	
	@Column(name = "EXCLUDE")
	private String exclude;	
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	public Date getEffectiveTo() {
		return effectiveTo;
	}
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public String getExclude() {
		return exclude;
	}
	public void setExclude(String exclude) {
		this.exclude = exclude;
	}
	
	
}

