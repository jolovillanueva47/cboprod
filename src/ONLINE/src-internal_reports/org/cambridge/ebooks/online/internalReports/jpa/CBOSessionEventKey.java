package org.cambridge.ebooks.online.internalReports.jpa;

import java.io.Serializable;

import javax.persistence.Column;


@SuppressWarnings("serial")
public class CBOSessionEventKey implements Serializable{
	
	
	@Column( name = "SESSION_ID" )
	private String sessionId;	
	
	
	@Column( name = "SEQUENCE" )
	private long sequence;
	
	public CBOSessionEventKey(){
	
	}
	
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	
	@Override
	public int hashCode() {
	    return (int) sessionId.hashCode() + (int) sequence;
	}

	@Override
	public boolean equals(Object obj) {
//	    if (obj == this) return true;
//	    if (!(obj instanceof CBOSessionEventKey)) return false;
//	    if (obj == null) return false;
//	    
//	    CBOSessionEventKey pk = (CBOSessionEventKey) obj;
//	    return pk.sequence == sequence && pk.sessionId.equals(sessionId);
		boolean result; 
		if (obj == this) result = true;
		else if (!(obj instanceof CBOSessionEventKey)) result = false;
		else if (obj == null) result = false;
		else {
		    CBOSessionEventKey pk = (CBOSessionEventKey) obj;
		    result = pk.sequence == sequence && pk.sessionId.equals(sessionId);
		}
		return result;

	}

}
