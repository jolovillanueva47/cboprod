package org.cambridge.ebooks.online.internalReports.mdb;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.internalReports.jpa.CBOSessionEventBean;
import org.cambridge.ebooks.online.internalReports.jpa.EventIpExcludeBean;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

@MessageDriven(name = "InternalReportsMDB", activationConfig = {		
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/internal_reports/logger") })	

public class InternalReportsMDB implements MessageListener {
	
	private static final Logger logger = Logger.getLogger(InternalReportsMDB.class);
	
	private static final String[] properties = new String[]{
		"sessionId",			
		"ipAddress",
		"eventNo",
		"httpReferrer",
		"browser",
		"eventStr1",
		"eventStr2",
		"eventStr3",
		"athensId",
		"shibbolethId",
		"memberId",
		"userId",
		"debug",
		"ipOrgs",
		"directBodyId",
		"autologinId",
		"sequence"
	};

	public void onMessage(Message message) {
		Logger.getLogger( InternalReportsMDB.class ).info( "Report logger called!" );
		//UserTransaction ut = sc.getUserTransaction();
		try {
			if (message instanceof Message) {						
				try {			
					//ut.begin();	
					em.getTransaction().begin();
					CBOSessionEventBean report = generateEventBean(message);
					
					//check if in ip exclusion -- should not log if in ip exclusion
					if(checkIfInIpExclusion(report)){
						Logger.getLogger( InternalReportsMDB.class).info( "Message not logged -- ip:" + report.getIpAddress() + 
								" sessionId:" + report.getSessionId() + " url:" + report.getDebug() );						
					}else{
						
						//default sequence num to 0, because trigger will update the sequence number					
						//report.setSequence(((long)(Math.random()*10000000l)));
						//em.joinTransaction();
					    em.persist(report);
					    
					}
					
					em.getTransaction().commit();					
				} catch ( Exception e ) {
					logger.error("Message:" + ExceptionPrinter.getStackTraceAsString(e));
					em.getTransaction().rollback();
				} finally {
					
				}
			}
		} catch (Exception e) {
			logger.error("[JMSException]:Uploader EJB:"+ ExceptionPrinter.getStackTraceAsString(e));

		}

	}
	
	private boolean checkIfInIpExclusion(CBOSessionEventBean report){
		String ip = report.getIpAddress();			
		Query query = em.createNativeQuery(EventIpExcludeBean.SEARCH_EXCLUDE, EventIpExcludeBean.class);
		query.setParameter(1, ip);
		
		EventIpExcludeBean exclude;
		try {
			exclude = (EventIpExcludeBean) query.getSingleResult();
		} catch (NoResultException e) {			
			exclude = null;
		}
		
		if(exclude == null ){
			return false;
		}		
		return true;		
	}
	
	
	@SuppressWarnings("unused")
	private long getSequence(CBOSessionEventBean report){
		long seq;
		String sql =
			"select max(SEQUENCE) SEQUENCE " +
			"from CBO_SESSION_EVNT " +
			"where SESSION_ID = ?1 ";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, report.getSessionId());		
		try {
			seq = ((BigDecimal) query.getSingleResult()).longValue();
		} catch (Exception e) {			
			return 1;
		}		
		if(seq != 0){
			return seq + 1;
		}else{
			return 1;
		}	
				
	}
	
	private CBOSessionEventBean generateEventBean(Message message){
		
		CBOSessionEventBean bean = new CBOSessionEventBean();
		for(String prop : properties){
			String value = "";
			try {
				value = message.getStringProperty(prop);
			} catch (JMSException e) {
				continue;
			}
			try {				
				BeanUtils.setProperty(bean, prop, value);				
			} catch (Exception e) {			
				e.printStackTrace();
			}		
		}
		return bean;
	}
	

		
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("InternalReportsUnit");

	private EntityManager em;
	
	@PostConstruct
    void init() {
		Logger.getLogger( InternalReportsMDB.class ).info( " Initialize EJB " );		
		em = emf.createEntityManager();
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger( InternalReportsMDB.class ).info( " Destroy EJB " );
    	em.close();
    }
}
