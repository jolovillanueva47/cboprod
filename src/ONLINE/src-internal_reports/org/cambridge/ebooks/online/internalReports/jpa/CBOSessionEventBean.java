package org.cambridge.ebooks.online.internalReports.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@IdClass(CBOSessionEventKey.class)
@Table ( name = CBOSessionEventBean.TABLE_NAME )
public class CBOSessionEventBean implements Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "CBO_SESSION_EVNT";
	
	public CBOSessionEventBean(){};
	
	@Id
	@Column( name = "SESSION_ID" )
	private String sessionId;
	
	@Id	
	@Column( name = "SEQUENCE")
	private long sequence;
	
	@Column( name = "NOTIFIED_TIME" )
	private Timestamp notifiedTime = new Timestamp(new java.util.Date().getTime()); //init date
	
	@Column( name = "IP_ADDRESS" )
	private String ipAddress;
	
	@Column( name = "EVENT_NO" )
	private long eventNo;
	
	@Column( name = "HTTP_REFERRER" )
	private String httpReferrer;
	
	@Column( name = "BROWSER" )
	private String browser;
	
	@Column( name = "EVENT_STR1" )
	private String eventStr1;
	
	@Column( name = "EVENT_STR2" )
	private String eventStr2;
	
	@Column( name = "EVENT_STR3" )
	private String eventStr3;
	
	@Column( name = "ATHENS_ID" )
	private String athensId;
	
	@Column( name = "SHIBBOLETH_ID" )
	private String shibbolethId;
	
	@Column( name = "MEMBER_ID" )
	private String memberId;
	
	@Column( name = "DEBUG" )
	private String debug;
	
	@Column( name = "IP_ORGS" )
	private String ipOrgs;
	
	@Column( name = "AUTOLOGIN_ID" )
	private String autologinId;	
	


	@Transient
	private String directBodyId;
	
	
	public String getIpOrgs() {
		return ipOrgs;
	}
	public void setIpOrgs(String ipOrgs) {
		this.ipOrgs = ipOrgs;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	public Timestamp getNotifiedTime() {
		return notifiedTime;
	}
	public void setNotifiedTime(Timestamp notifiedTime) {
		this.notifiedTime = notifiedTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public long getEventNo() {
		return eventNo;
	}
	public void setEventNo(long eventNo) {
		this.eventNo = eventNo;
	}
	public String getHttpReferrer() {
		return httpReferrer;
	}
	public void setHttpReferrer(String httpReferrer) {
		this.httpReferrer = httpReferrer;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getEventStr1() {
		return eventStr1;
	}
	public void setEventStr1(String eventStr1) {
		this.eventStr1 = eventStr1;
	}
	public String getEventStr2() {
		return eventStr2;
	}
	public void setEventStr2(String eventStr2) {
		this.eventStr2 = eventStr2;
	}
	public String getEventStr3() {
		return eventStr3;
	}
	public void setEventStr3(String eventStr3) {
		this.eventStr3 = eventStr3;
	}
	public String getAthensId() {
		return athensId;
	}
	public void setAthensId(String athensId) {
		this.athensId = athensId;
	}
	public String getShibbolethId() {
		return shibbolethId;
	}
	public void setShibbolethId(String shibbolethId) {
		this.shibbolethId = shibbolethId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getDebug() {
		return debug;
	}
	public void setDebug(String url) {
		this.debug = url;
	}
	public String getDirectBodyId() {
		return directBodyId;
	}
	public void setDirectBodyId(String directBodyId) {
		this.directBodyId = directBodyId;
	}
	
	public String getAutologinId() {
		return autologinId;
	}
	public void setAutologinId(String autologinId) {
		this.autologinId = autologinId;
	}
}
