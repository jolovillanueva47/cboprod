package org.cambridge.ebooks.online.jpa.consortium;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 * 
 * @author mmanalo
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
			name = Consortium.GET_ALL_CONS,
			//2 for organisation
			query = 
				"select BODY_ID, NAME, DISPLAY_NAME " +
				"from ORACJOC.CONSORTIUM " +
				"where BODY_ID = ?1 ",
			resultClass =  Consortium.class
		),
	
	@NamedNativeQuery(
		name = Consortium.GET_CONS,
		//2 for organisation
		query = 
			"select BODY_ID, NAME, DISPLAY_NAME " +
			"from ORACJOC.CONSORTIUM " +
			"where BODY_ID = ?1 ",
		resultClass =  Consortium.class
	)
})


@Entity
@Table(name="ORACJOC.CONSORTIUM")
public class Consortium {
	
	public static final String GET_CONS = "GET_CONS";
	public static final String GET_ALL_CONS = "GET_CONS";
	
	@Id
	@Column(name="BODY_ID")
	private String bodyId;
	
	@Column(name="DISPLAY_NAME")
	private String displayName;

	@Column(name="IMAGE")
	private String image;
	
	
	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}	
}
