package org.cambridge.ebooks.online.jpa.faq;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




/**
 * @author Karlson A. Mulingtapang
 * FAQ.java - FAQ synonymous with CJO
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = Faq.SEARCH_ALL,
		query = "Select * from FAQ",
		resultSetMapping = Faq.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = Faq.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = Faq.class,
				fields = {
					@FieldResult(name = "faqId", column = "FAQ_ID"),
					@FieldResult(name = "question", column = "QUESTION"),
					@FieldResult(name = "answer", column = "ANSWER"),
					@FieldResult(name = "subjectAreaType", column = "SUBJECT_AREA_TYPE"),
					@FieldResult(name = "displayOrder", column = "DISPLAY_ORDER"),
					@FieldResult(name = "enable", column = "ENABLE"),
					@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
					@FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
					@FieldResult(name = "helpPage", column = "HELP_PAGE"),
					@FieldResult(name = "content", column = "CONTENT"),
					@FieldResult(name = "url", column = "URL")
				}
			)	
		}
	)
})

@Entity
@Table(name = "FAQ")
public class Faq {
	
	public static final String RESULT_SET_MAPPING = "FAQ.result";
	public static final String SEARCH_ALL = "FAQ.searchAll";
	
	@Id
	@Column(name = "FAQ_ID")
	private int faqId;
	
	@Column(name = "QUESTION")
	private String question;
	
	@Column(name = "ANSWER")
	private String answer;
	
	@Column(name = "SUBJECT_AREA_TYPE")
	private String subjectAreaType;
	
	@Column(name = "DISPLAY_ORDER")
	private int displayOrder;
	
	@Column(name = "ENABLE")
	private String enable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "HELP_PAGE")
	private String helpPage;
	
	@Column(name = "CONTENT")
	private String content;
	
	@Column(name = "URL")
	private String url;	
	
	/**
	 * @return the faqId
	 */
	public int getFaqId() {
		return faqId;
	}
	/**
	 * @param faqId the faqId to set
	 */
	public void setFaqId(int faqId) {
		this.faqId = faqId;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * @return the subjectAreaType
	 */
	public String getSubjectAreaType() {
		return subjectAreaType;
	}
	/**
	 * @param subjectAreaType the subjectAreaType to set
	 */
	public void setSubjectAreaType(String subjectAreaType) {
		this.subjectAreaType = subjectAreaType;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the helpPage
	 */
	public String getHelpPage() {
		return helpPage;
	}
	/**
	 * @param helpPage the helpPage to set
	 */
	public void setHelpPage(String helpPage) {
		this.helpPage = helpPage;
	}	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}	
}
