package org.cambridge.ebooks.online.jpa.organisation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.consortium.Consortium;




@NamedNativeQueries({
	@NamedNativeQuery(
		name = Organisation.GET_ORG,
		//2 for organisation
		query = 
			"select BODY_ID, NAME, DISPLAY_NAME, 2 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.ORGANISATION " +
			"where BODY_ID = ?1 " +
			"union " +
			"select BODY_ID, NAME, DISPLAY_NAME, 1 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.CONSORTIUM " +
			"where BODY_ID = ?1 ",
		resultClass = Organisation.class
	),
	
	@NamedNativeQuery(
		name = Organisation.GET_ALL_ORG,
		//2 for organisation
		query = 
			"select BODY_ID, NAME, DISPLAY_NAME, 2 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.ORGANISATION " +
			"where BODY_ID = ?1 " +
			"union " +
			"select BODY_ID, NAME, DISPLAY_NAME, 1 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.CONSORTIUM " +
			"where BODY_ID = ?1 ",
		resultClass = Organisation.class
	),
	
	@NamedNativeQuery(
		name = Organisation.GET_ORG_BY_ID,
		//2 for organisation
		query = 
			"select BODY_ID, NAME, DISPLAY_NAME, 2 TYPE, ADDRESS, MESSAGE_TEXT, IMAGE " +
			"from ORACJOC.ORGANISATION " +
			"where BODY_ID = ?1 ",
		resultClass = Organisation.class
	)
})



@Entity
@Table( name = "ORGANISATION" ) 
public class Organisation {	
	static Logger logger = Logger.getLogger(Organisation.class);
	
	public static final String GET_ORG = "GET_ORG";
	
	public static final String GET_ALL_ORG = "GET_ALL_ORG";
	
	public static final String DISPLAY_NAME = "displayName";
	
	public static final String MESSAGE_TEXT = "messageText";
	
	public static final String GET_ORG_BY_ID = "GET_ORG_BY_ID";
	
	public static final String IMAGE = "image";
	
	@Id
	@Column( name = "BODY_ID")
	private String bodyId;
	
	@Column( name = "NAME")
	private String name;
	
	@Column( name = "DISPLAY_NAME")
	private String displayName;
	
	@Column( name = "TYPE")
	private String type;
	
	@Column( name = "ADDRESS")
	private String address;
	
	@Column( name = "MESSAGE_TEXT")
	private String messageText;
	
	@Column( name = "IMAGE")
	private String hasImage;
	
	@Column( name = "TABLE_NAME")
	private String tableName; 
	
	@Column( name = "DIRECT")
	private String direct; 
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="BODY_ID", insertable=false, updatable=false )	
	private OrganisationBase organisation;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="BODY_ID", insertable=false, updatable=false )	
	private Consortium consortium;
	
	@Transient
	private boolean ipAuthenticated;
	
	@Transient
	private boolean athensOrg;
	
	@Transient
	private boolean shibbolethOrg;
	
	@Transient
	private boolean membershipOrg;
	
	@Transient
	private boolean autologin;
	
	/* public methods */
	
	public boolean isHasNoOrgNorConsortia(){
		if(this.organisation == null && this.consortium == null){
			return true;
		}
		return false;
	}
	
	
	/* getters / setters */

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getHasImage() {
		return hasImage;
	}
	
	public boolean hasLogo() { 
		return "Y".equals( hasImage );
	}

	public void setHasImage(String hasImage) {
		this.hasImage = hasImage;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public OrganisationBase getOrganisation() {
		return organisation;
	}

	public void setOrganisation(OrganisationBase organisation) {
		this.organisation = organisation;
	}

	public Consortium getConsortum() {
		return consortium;
	}

	public void setConsortium(Consortium consortium) {
		this.consortium = consortium;
	}

	public String getBaseProperty(String prop){
		Object bean = null;
		String value = null;
		if(this.organisation != null){
			bean = this.organisation;
		}else if(this.consortium != null){
			bean = this.consortium;
		}
		try {
			value = BeanUtils.getProperty(bean, prop);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return value; 
	}

	public Consortium getConsortium() {
		return consortium;
	}


	public boolean isIpAuthenticated() {
		return ipAuthenticated;
	}

	public void setIpAuthenticated(boolean ipAuthenticated) {
		this.ipAuthenticated = ipAuthenticated;
	}
	
	
	public boolean isAthensOrg() {
		return athensOrg;
	}


	public void setAthensOrg(boolean athensOrg) {
		this.athensOrg = athensOrg;
	}


	public boolean isShibbolethOrg() {
		return shibbolethOrg;
	}


	public void setShibbolethOrg(boolean shibbolethOrg) {
		this.shibbolethOrg = shibbolethOrg;
	}


	public boolean isMembershipOrg() {
		return membershipOrg;
	}


	public void setMembershipOrg(boolean membershipOrg) {
		this.membershipOrg = membershipOrg;
	}


	public boolean isAutologin() {
		return autologin;
	}


	public void setAutologin(boolean autologin) {
		this.autologin = autologin;
	}
}
