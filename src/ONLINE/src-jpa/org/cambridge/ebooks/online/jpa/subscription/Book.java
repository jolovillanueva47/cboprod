package org.cambridge.ebooks.online.jpa.subscription;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.PersistenceUtil;

@Entity
@Table(name = "EBOOK")
public class Book {

	private static final Logger LOGGER = Logger.getLogger(Book.class);
	
//	private static final String ACCESS_ORDER_TYPE = "'S','M','L'";
	
	public static final String ORDER_CONFIRMED = "4";

	@Id
	@Column(name = "BOOK_ID")
	private String bookId;

	@Column(name = "ISBN")
	private String isbn;

	@Column(name = "TITLE")
	private String title;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public static List<Book> getBooks(String directBodyId, String allBodyId) {
		String bodyIds[] = BookCollection
				.appendBodyIds(directBodyId, allBodyId).split(",");
		return PersistenceUtil.searchListNative(new Book(), findBooksQuery(
				directBodyId, allBodyId), bodyIds);
	}

	public static List<Book> getBooksUnderCollection(String collectionId) {
		return PersistenceUtil.searchListNative(new Book(),
				findBooksCollectionQuery(), collectionId);
	}

	private static String findBooksQuery(String directBodyId, String allBodyId) {
		LOGGER.info("directBodyId:" + directBodyId);
		LOGGER.info("allBodyId:" + allBodyId);
		
		String sql;
//		String sql1 =  
//			"select distinct eisbn, order_id, status, order_type " +
//			"from ebooks_access_list_final " +
//			"where collection_id is null " +
//			  "and body_id in (?) " + // -- direct body id is for access_agreement "
//			  "and order_type = 'P' " +
//			"union ";
//		
//		String sql2 = 
//			"select distinct eisbn, order_id, status, order_type " +
//			"from ebooks_access_list_final " +
//			"where collection_id is null " +
//			  "and body_id in (?) " + //-- all body ids
//			  "and ( " +
//			        "status = 0 or " +
//			        "ORDER_TYPE in (" + ACCESS_ORDER_TYPE + ") " +      
//			")";
		
//		StringBuilder baseSql = new StringBuilder();
//		baseSql.append("select distinct eisbn, order_id, status, order_type ");
//		baseSql.append("from ebooks_access_list_final ");
//		baseSql.append("where collection_id is null and (");
//		if(StringUtils.isNotEmpty(directBodyId)){
//			String directBodyIdSql = "(body_id in (?) and order_type = 'P') or ";
//			directBodyIdSql = BookCollection.arrangeQuery(directBodyIdSql, directBodyId, 0);			
//			baseSql.append(directBodyIdSql);
//			
//			String allBodyIdSql = "(body_id in (?) and (status = 0 or ORDER_TYPE in (" + ACCESS_ORDER_TYPE + "))))";
//			allBodyIdSql = BookCollection.arrangeQuery(allBodyIdSql, allBodyId, directBodyId.split(",").length);
//			baseSql.append(allBodyIdSql);
//		} else {
//			String allBodyIdSql = "(body_id in (?) and (status = 0 or ORDER_TYPE in (" + ACCESS_ORDER_TYPE + "))))";
//			allBodyIdSql = BookCollection.arrangeQuery(allBodyIdSql, allBodyId, 0);
//			baseSql.append(allBodyIdSql);
//		}		
		
		StringBuilder baseSql = new StringBuilder();
		baseSql.append("select distinct eisbn, order_id, status, order_type ");
		baseSql.append("from ebooks_access_list_final ");
		baseSql.append("where collection_id is null and ");		
		String directBodyIdSql = "body_id in (?)";
		directBodyIdSql = BookCollection.arrangeQuery(directBodyIdSql, directBodyId, 0);			
		baseSql.append(directBodyIdSql);		
		
		sql = 
			"select distinct eb.book_id, eb.isbn, eb.title from " + 
			"( " +
				baseSql.toString() +
			") al, " +
			"ebook eb " +
			"where al.eisbn = eb.isbn ";
		
		
//		if(StringUtils.isNotEmpty(directBodyId)){
//			sql1 = BookCollection.arrangeQuery(sql1, directBodyId, 0);
//			sql2 = BookCollection.arrangeQuery(sql2, allBodyId, directBodyId
//					.split(",").length);
//			/*sql = "select distinct eb.book_id, al.eisbn, eb.title from " + "( "
//					+ sql1 + sql2 + ") al, " + "ebook eb "
//					+ "where al.eisbn = eb.isbn ";*/
//			sql =
//				"select distinct eb.book_id, eb.isbn, eb.title from " + 
//					"( " +
//						sql1 + sql2 +
//					") al, " +
//				"ebook eb " +
//				"where al.eisbn = eb.isbn ";
//
//		} else {
//			sql2 = BookCollection.arrangeQuery(sql2, allBodyId, 0);
//			/*sql = "select distinct eb.book_id, al.eisbn, eb.title from " + "( "
//					+ sql2 + ") al, " + "ebook eb "
//					+ "where al.eisbn = eb.isbn ";*/
//			sql = 
//				"select distinct eb.book_id, eb.isbn, eb.title from " + 
//				"( " +
//					sql2 +
//				") al, " +
//				"ebook eb " +
//				"where al.eisbn = eb.isbn ";
//		}

		LOGGER.info("===findBooksQuery:" + sql);
		
		return sql;

	}

	private static String findBooksCollectionQuery() {
		String sql = "select BK.BOOK_ID, BK.ISBN, BK.TITLE "
				+ "from (select cl.collection_id, nvl(ci.ean_number, cl.isbn) isbn "
				+ "from ebook_collection_list cl, "
				+ "(select ci.ean13_of_set, ci.ean_number "
				+ "from core_isbn ci  " + "where ci.format_code = 'OC' ) ci "
				+ "where cl.isbn = ci.ean13_of_set (+)  ) c, " + "EBOOK BK "
				+ "where c.ISBN = BK.ISBN " + "and c.COLLECTION_ID = ?1 ";
		
		LOGGER.info("===findBooksCollectionQuery:" + sql);
		
		return sql;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		return true;
	}

}
