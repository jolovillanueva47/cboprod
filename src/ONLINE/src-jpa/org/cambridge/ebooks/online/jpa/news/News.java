package org.cambridge.ebooks.online.jpa.news;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Karlson A. Mulingtapang
 * News.java - NEWS Synonymous with CJOC NEWS
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = News.SEARCH_ALL,
		query = "Select * from NEWS",
		resultSetMapping = News.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = News.SEARCH_ALL_VALID,
		query = "Select * From NEWS " +
				"Where ((SYSDATE >= START_DATE " +
				" AND SYSDATE <= END_DATE) OR START_DATE = to_date('1-JAN-2020','DD-MON-YYYY'))" +
				" AND DISPLAY_ON_MAIN_PAGE = 'true' " +
				" AND APPLICATION_TYPE = 'B' ORDER BY PRIORITY DESC",
		resultSetMapping = News.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = News.SEARCH_BY_ID,
		query = "Select * from NEWS " + 
				"Where Message_Id = ?1",
		resultSetMapping = News.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = News.RESULT_SET_MAPPING,
		entities = {
			@EntityResult(
				entityClass = News.class,
				fields = {
					@FieldResult(name = "messageId", column = "MESSAGE_ID"),
					@FieldResult(name = "messageTitle", column = "MESSAGE_TITLE"),
					@FieldResult(name = "message", column = "MESSAGE"),
					@FieldResult(name = "startDate", column = "START_DATE"),
					@FieldResult(name = "endDate", column = "END_DATE"),
					@FieldResult(name = "priority", column = "PRIORITY"),
					@FieldResult(name = "link", column = "LINK"),
					@FieldResult(name = "archive", column = "ARCHIVE"),
					@FieldResult(name = "displayOnMainPage", column = "DISPLAY_ON_MAIN_PAGE"),
					@FieldResult(name = "journalId", column = "JOURNAL_ID"),
					@FieldResult(name = "applicationType", column = "APPLICATION_TYPE"),
					@FieldResult(name = "hits", column = "HITS")
				}
			)
		}
	)
})

@Entity
@Table(name = "NEWS")
public class News {
	public static final String RESULT_SET_MAPPING = "NEWS.result";
	public static final String SEARCH_ALL = "NEWS.searchAll";
	public static final String SEARCH_ALL_VALID = "NEWS.searchValidNews";
	public static final String SEARCH_BY_ID = "NEWS.searchById";
	
	@Id
	@Column(name = "MESSAGE_ID")
	private String messageId;
	
	@Column(name = "MESSAGE_TITLE")
	private String messageTitle;
	
	@Column(name = "MESSAGE")
	private String message;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END_DATE")
	private Date endDate;
	
	@Column(name = "PRIORITY")
	private String priority;
	
	@Column(name = "LINK")
	private String link;
	
	@Column(name = "ARCHIVE")
	private String archive;
	
	@Column(name = "DISPLAY_ON_MAIN_PAGE")
	private String displayOnMainPage;
	
	@Column(name = "JOURNAL_ID")
	private String journalId;
	
	@Column(name = "APPLICATION_TYPE")
	private String applicationType;
	
	@Column(name = "HITS")
	private int hits;

	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the messageTitle
	 */
	public String getMessageTitle() {
		return messageTitle;
	}

	/**
	 * @param messageTitle the messageTitle to set
	 */
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the archive
	 */
	public String getArchive() {
		return archive;
	}

	/**
	 * @param archive the archive to set
	 */
	public void setArchive(String archive) {
		this.archive = archive;
	}

	/**
	 * @return the displayOnMainPage
	 */
	public String getDisplayOnMainPage() {
		return displayOnMainPage;
	}

	/**
	 * @param displayOnMainPage the displayOnMainPage to set
	 */
	public void setDisplayOnMainPage(String displayOnMainPage) {
		this.displayOnMainPage = displayOnMainPage;
	}

	/**
	 * @return the journalId
	 */
	public String getJournalId() {
		return journalId;
	}

	/**
	 * @param journalId the journalId to set
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}

	/**
	 * @return the applicationType
	 */
	public String getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType the applicationType to set
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return the hits
	 */
	public int getHits() {
		return hits;
	}

	/**
	 * @param hits the hits to set
	 */
	public void setHits(int hits) {
		this.hits = hits;
	}
}
