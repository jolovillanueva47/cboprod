package org.cambridge.ebooks.online.jpa.subscription;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.util.PersistenceUtil;







@Entity
@Table(name = "EBOOK_COLLECTION_HEADER")
public class BookCollection {
	
	private static final Logger LOGGER = Logger.getLogger(BookCollection.class);
	
//	private static final String ACCESS_ORDER_TYPE = "'S','M','L'";


	@Id	
	@Column( name = "COLLECTION_ID")
	private String collectionId;
	
	@Column( name = "TITLE")
	private String title;
	
	@Column( name = "DESCRIPTION")
	private String description;
	
		
	public String getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<BookCollection> getCollection(String directBodyId, String allBodyId){	
		String bodyIds[] = appendBodyIds(directBodyId, allBodyId).split(",");
		return PersistenceUtil.searchListNative(new BookCollection(), generateQuery(directBodyId, allBodyId), bodyIds);
	}
		
	private static String generateQuery(String directBodyId, String allBodyId){
		LOGGER.info("directBodyId:" + directBodyId);
		LOGGER.info("allBodyId:" + allBodyId);
		
		String sql = "";		
//		String sql1 =			  
//				"select distinct collection_id, order_id, status, order_type " +
//				"from ebooks_access_list_final " +
//				"where collection_id is not null " +
//				  "and body_id in (?) " + // direct body id is for access_agreement " +
//				  "and ORDER_TYPE = 'P' " +
//				"union ";
//		
//		String sql2 = 
//				"select distinct collection_id, order_id, status, order_type " +
//				"from ebooks_access_list_final " +
//				"where collection_id is not null " +
//				  "and body_id in (?) " + //-- all body ids
//				  "and ( " +
//				        "status = 0 or  " +
//				        "ORDER_TYPE in (" + ACCESS_ORDER_TYPE + ") " +      
//				")";		
		
//		StringBuilder baseSql = new StringBuilder();
//		baseSql.append("select distinct collection_id, order_id, status, order_type ");
//		baseSql.append("from ebooks_access_list_final ");
//		baseSql.append("where collection_id is not null and (");
//		if(StringUtils.isNotEmpty(directBodyId)){
//			String directBodyIdSql = "(body_id in (?) and order_type = 'P') or ";
//			directBodyIdSql = BookCollection.arrangeQuery(directBodyIdSql, directBodyId, 0);			
//			baseSql.append(directBodyIdSql);
//			
//			String allBodyIdSql = "(body_id in (?) and (status = 0 or ORDER_TYPE in (" + ACCESS_ORDER_TYPE + "))))";
//			allBodyIdSql = BookCollection.arrangeQuery(allBodyIdSql, allBodyId, directBodyId.split(",").length);
//			baseSql.append(allBodyIdSql);
//		} else {
//			String allBodyIdSql = "(body_id in (?) and (status = 0 or ORDER_TYPE in (" + ACCESS_ORDER_TYPE + "))))";
//			allBodyIdSql = BookCollection.arrangeQuery(allBodyIdSql, allBodyId, 0);
//			baseSql.append(allBodyIdSql);
//		}	
		
		StringBuilder baseSql = new StringBuilder();
		baseSql.append("select distinct collection_id, order_id, status, order_type ");
		baseSql.append("from ebooks_access_list_final ");
		baseSql.append("where collection_id is not null and ");		
		String directBodyIdSql = "body_id in (?)";
		directBodyIdSql = BookCollection.arrangeQuery(directBodyIdSql, directBodyId, 0);			
		baseSql.append(directBodyIdSql);
			
			
		
		sql = 
			"select distinct al.collection_id, title, description from " +
			"( " +
				baseSql.toString() +
			") al, " + 
			"ebook_collection_header hdr " +
			"where al.collection_id = hdr.collection_id";
		
//		if(StringUtils.isNotEmpty(directBodyId)){			
//			sql1 = arrangeQuery(sql1, allBodyId, directBodyId.split(",").length);
//			sql2 = arrangeQuery(sql2, allBodyId, directBodyId.split(",").length);
//			sql = 
//				"select distinct al.collection_id, title, description from " +
//				"( " +
//					sql1 + sql2 +
//				") al, " + 
//				"ebook_collection_header hdr " +
//				"where al.collection_id = hdr.collection_id";			
//		}else{
//			sql2 = arrangeQuery(sql2, allBodyId, 0);
//			sql = 
//				"select distinct al.collection_id, title, description from " +
//				"( " +
//					sql2 +
//				") al, " + 
//				"ebook_collection_header hdr " +
//				"where al.collection_id = hdr.collection_id";
//		}
		
		LOGGER.info("===generateQuery:" + sql);
		
		return sql;		
	}
	
	public static String appendBodyIds(String directBodyId, String allBodyId){
		String all = "";
		if(StringUtils.isNotEmpty(directBodyId)){
			all = directBodyId + "," + allBodyId;
		}else{
			all = allBodyId;
		}
		return all;
	}
	
	public static String arrangeQuery(String sql, String bodyId, int offset){
		if(bodyId == null || StringUtils.isEmpty(bodyId)){
			//throw new IllegalArgumentException("Invalid. Body Id should note be empty. body id value =" + bodyId + "suffix");
			return sql;
		}
		
		String[] bodyIds = bodyId.split(",");
		
		return sql.replaceAll("\\?", generateReplaceString(bodyIds.length, offset));
	}
	
	private static String generateReplaceString(int count, int offset){
		int _offset = offset;
		StringBuffer sb = new StringBuffer();
		for(int i = ++_offset; i <= (count + offset); i++){
			if(i == (count + offset)){
				sb.append("?" + i);
			}else{
				sb.append("?" + i + ",");
			}
		}
		return sb.toString();
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((collectionId == null) ? 0 : collectionId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookCollection other = (BookCollection) obj;
		if (collectionId == null) {
			if (other.collectionId != null)
				return false;
		} else if (!collectionId.equals(other.collectionId))
			return false;
		return true;
	}
	
}
