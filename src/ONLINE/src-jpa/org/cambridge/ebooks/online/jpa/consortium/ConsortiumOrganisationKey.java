package org.cambridge.ebooks.online.jpa.consortium;

import java.io.Serializable;

import javax.persistence.Column;



@SuppressWarnings("serial")
public class ConsortiumOrganisationKey implements Serializable{

	
	
	@Column( name = "ORGANISATION_ID" )
	private String organisationId;	
	
	
	@Column( name = "CONSORTIUM_ID" )
	private String consortiumId;
	
	public ConsortiumOrganisationKey(){
	
	}
	
	
	public String getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}

	public String getConsortiumId() {
		return consortiumId;
	}

	public void setConsortiumId(String consortiumId) {
		this.consortiumId = consortiumId;
	}
	
	@Override
	public int hashCode() {
	    return (int) organisationId.hashCode() + (int) consortiumId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
//	    if (obj == this) return true;
//	    if (!(obj instanceof CBOSessionEventKey)) return false;
//	    if (obj == null) return false;
//	    
//	    CBOSessionEventKey pk = (CBOSessionEventKey) obj;
//	    return pk.sequence == sequence && pk.sessionId.equals(sessionId);
		boolean result; 
		if (obj == this) result = true;
		else if (!(obj instanceof ConsortiumOrganisationKey)) result = false;
		else if (obj == null) result = false;
		else {
			ConsortiumOrganisationKey pk = (ConsortiumOrganisationKey) obj;
		    result = pk.organisationId == organisationId && pk.consortiumId.equals(consortiumId);
		}
		return result;

	}
}
