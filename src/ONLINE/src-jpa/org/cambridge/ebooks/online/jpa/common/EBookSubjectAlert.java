package org.cambridge.ebooks.online.jpa.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EBOOK_SUBJECT_ALERT")
public class EBookSubjectAlert {
	
	@EmbeddedId
	private EBookSubjectAlertPK pk;
	
	@Column(name="MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column(name="MODIFIED_BY")
	private String modifiedBy;


	public EBookSubjectAlertPK getPk() {
		return pk;
	}

	public void setPk(EBookSubjectAlertPK pk) {
		this.pk = pk;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
