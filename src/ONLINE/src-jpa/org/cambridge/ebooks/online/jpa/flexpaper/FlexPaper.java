package org.cambridge.ebooks.online.jpa.flexpaper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@Entity
@Table(name = "EBOOK_FLEX_PAPER_TABLE")
@NamedNativeQueries({
	@NamedNativeQuery(
		name = FlexPaper.SEARCH_ALL,
		query = "SELECT * FROM EBOOK_FLEX_PAPER_TABLE WHERE ACCOUNT_ID like ?1 AND PDF_PATH LIKE ?2",
		resultSetMapping = FlexPaper.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = FlexPaper.GET_SELECTION,
		query = "SELECT * EBOOK_FLEX_PAPER_TABLE WHERE ID = ?",
		resultSetMapping = FlexPaper.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = FlexPaper.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = FlexPaper.class,
				fields = {
					@FieldResult(name = "flexPaperId", column = "ID"),
					@FieldResult(name = "selectionText", column = "SELECTION_TEXT"),
					@FieldResult(name = "hasSelection", column = "HAS_SELECTION"),
					@FieldResult(name = "color", column = "COLOR"),
					@FieldResult(name = "selectionInfo", column = "SELECTION_INFO"),
					@FieldResult(name = "note", column = "NOTE"),
					@FieldResult(name = "pageIndex", column = "PAGE_INDEX"),
					@FieldResult(name = "positionX", column = "POSITION_X"),
					@FieldResult(name = "positionY", column = "POSITION_Y"),
					@FieldResult(name = "width", column = "WIDTH"),
					@FieldResult(name = "height", column = "HEIGHT"),
					@FieldResult(name = "accountId", column = "ACCOUNT_ID"),
					@FieldResult(name = "pdfPath", column = "PDF_PATH")
				}
			)
		}
	)
})

public class FlexPaper {
	
	public static final String RESULT_SET_MAPPING = "ebooksFlexPaperResultSetMapping";
	public static final String SEARCH_ALL = "ebooksFlexPaperSearchAll";
	public static final String GET_SELECTION = "ebooksFlexPaperGetSelection";

	public FlexPaper() {}
	public FlexPaper(String flexPaperId) {
		super();
		this.flexPaperId = flexPaperId;
	}
	
	@Id
	@GeneratedValue(generator="EBOOK_FLEX_PAPER_ID_SEQ")
    @SequenceGenerator(name="EBOOK_FLEX_PAPER_ID_SEQ", sequenceName="EBOOK_FLEX_PAPER_ID_SEQ", allocationSize=1)
	@Column(name = "ID")
	private String flexPaperId;
	
	@Column(name = "SELECTION_TEXT")
	private String selectionText;
	@Column(name = "HAS_SELECTION")
	private String hasSelection;
	@Column(name = "COLOR")
	private String color;
	@Column(name = "SELECTION_INFO")
	private String selectionInfo;
	
	@Column(name = "NOTE")
	private String note;
	@Column(name = "PAGE_INDEX")
	private String pageIndex;
	@Column(name = "POSITION_X")
	private String positionX;
	@Column(name = "POSITION_Y")
	private String positionY;
	@Column(name = "WIDTH")
	private String width;
	@Column(name = "HEIGHT")
	private String height;
	
	@Column(name = "ACCOUNT_ID")
	private String accountId;
	@Column(name = "PDF_PATH")
	private String pdfPath;

	public String getFlexPaperId() {
		return flexPaperId;
	}
	public void setFlexPaperId(String flexPaperId) {
		this.flexPaperId = flexPaperId;
	}
	public String getSelectionText() {
		return selectionText;
	}
	public void setSelectionText(String selectionText) {
		this.selectionText = selectionText;
	}
	public String getHasSelection() {
		return hasSelection;
	}
	public void setHasSelection(String hasSelection) {
		this.hasSelection = hasSelection;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSelectionInfo() {
		return selectionInfo;
	}
	public void setSelectionInfo(String selectionInfo) {
		this.selectionInfo = selectionInfo;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}
	public String getPositionX() {
		return positionX;
	}
	public void setPositionX(String positionX) {
		this.positionX = positionX;
	}
	public String getPositionY() {
		return positionY;
	}
	public void setPositionY(String positionY) {
		this.positionY = positionY;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getPdfPath() {
		return pdfPath;
	}
	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
}
