package org.cambridge.ebooks.online.jpa.countrystates;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

/**
 * 
 * @author rvillamor
 *
 */

@NamedNativeQueries ({ 
	@NamedNativeQuery ( 
			name = State.GET_STATE_BY_COUNTRY_ID,
			query = " select state_county_id, description from state_county where country = ?1 order by description asc",
			resultSetMapping = "stateListResult"
	)
})

@SqlResultSetMapping ( 
	name = "stateListResult",
	entities = {
		@EntityResult (
			entityClass = State.class,
			fields = { 
				@FieldResult ( name = "stateId", 	column = "STATE_COUNTY_ID" ),
				@FieldResult ( name = "stateName", 	column = "DESCRIPTION" )
			}
		)	
	}
)

@Entity
@Table( name = "STATE_COUNTY" ) 
public class State {

	public static final String GET_STATE_BY_COUNTRY_ID = "getStateByCountryIdSql";
	
	@Id
	private String stateId ; 
	
	private String stateName;

	public boolean equals(Object o) { 
		if ( o instanceof State ) { 
			State c = (State) o;
			return c.getStateId().equals(this.getStateId());
		} else {
			return false;
		}
	}
	
	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
}
