package org.cambridge.ebooks.online.jpa.organisation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author mmanalo
 *
 */
@Entity
@Table(name="ORACJOC.ORGANISATION")
public class OrganisationBase {
	
	@Id
	@Column(name="BODY_ID")
	private String bodyId;
	
	@Column(name="DISPLAY_NAME")
	private String displayName;
	
	@Column( name = "MESSAGE_TEXT")
	private String messageText;
	
	@Column(name="IMAGE")
	private String image;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TYPE", insertable=false, updatable=false)	
	private OrganisationTypeBase organisationType;
	
	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public OrganisationTypeBase getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationType(OrganisationTypeBase organisationType) {
		this.organisationType = organisationType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}	
}
