package org.cambridge.ebooks.online.jpa.user;


/**
 * 
 * @author cmcastro
 *
 */

public class StringUtil {

	private static  final String REAL_NUMBER = "^[-+]?\\d+(\\.\\d+)?$";

	public static boolean isNumeric(String val) {
	   return isEmpty( val ) ? false : val.matches(REAL_NUMBER);
	}

	public static boolean isEmpty(Object str) { 
		return str == null || "".equals(str);
	}
	
	public static boolean isNotEmpty(Object str){ 
		return !isEmpty(str);
	}
	
	public static String nvl(String value, String replacement) { 
		return isEmpty(value) ? replacement : value;
	}
	
	public static String nvl(String value ) { 
		return isEmpty(value) ? "" : value;
	}
	
	public static <T> String nvl(T value) { 
		return ((T) (isEmpty(value) ? "" : value)).toString();
	}
	
	public static <T> String nvl(T value, T replacement) { 
		return ((T) (isEmpty(value) ? replacement : value)).toString();
	}
	
	public static boolean validateLength(Object obj, int minLength, int maxLength) { 
		boolean result = true;
		if ( obj  instanceof String ) { 
			String value = ( String ) obj;
			if ( isNotEmpty(value) ) { 
				result = value.length() >= minLength && value.length() <= maxLength;
			}
		}
		return result;
	}	
	
	public static String addPadding(final String value, final String padding ) {
		boolean ifValueIsLongerThanThePadding = nvl(value).length() > nvl(padding).length();
		if ( ifValueIsLongerThanThePadding ) { 
			return value;
		}
		String result = nvl(padding) + nvl(value);
		return result.substring( 0 + nvl(value).length(), result.length() );
	}
	
	public static String  join(String [] array, char separator) {
		if (array == null) {
            return null;
        }
        int arraySize = array.length;
        StringBuilder  result = new StringBuilder ();
        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                result.append(separator);
            }
            if (array[i] != null) {
                result.append(array[i]);
            }
        }
        return result.toString();
    }
	
	public static String[] split(String value){
		if(isEmpty(value)) return null;
		value = value.replace("[", "").replace("]", "");
		return value.split(",");
	}
	
	public static String removeHTMLTags(String input) {
		String delim1 = "&#.*?;.*?&#.*?;";
		String delim2 = "<.*?>";
		String delim3 = "&lt;.*?&gt;";
		String delim4 = "";
				
		String temp = input.replaceAll(delim1, "").replaceAll(delim2, "").replaceAll(delim3, "").replaceAll(delim4, " ").trim(); 

		return temp;
	}
}
