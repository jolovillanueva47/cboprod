package org.cambridge.ebooks.online.jpa.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table ( name = "MEMBER" ) 
public class MemberBody {

	@Id
	@Column( name = "BLADE_MEMBER_ID" )
	private String memberId;
	
	@Column( name = "BODY_ID" )
	private String bodyId;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private User userInfo;

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public User getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(User userInfo) {
		this.userInfo = userInfo;
	}
}
