package org.cambridge.ebooks.online.jpa.topbooks;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.apache.commons.lang.StringEscapeUtils;


@NamedNativeQueries({
	@NamedNativeQuery(
			name = EBookTopFeaturedTitles.SEARCH_TOP_FEATURED_TITLES,
			query = "SELECT TITLE, ISBN FROM featured_titles order by display_order",		
			resultSetMapping = EBookTopFeaturedTitles.TOP_FEATURED_RESULT_SET
		)	
})


@SqlResultSetMapping( 
	name = EBookTopFeaturedTitles.TOP_FEATURED_RESULT_SET,
	entities = {
		@EntityResult (
			entityClass = EBookTopFeaturedTitles.class,
			fields = {
				@FieldResult(name = "isbn",		column = "ISBN"),
				@FieldResult(name = "title",    column = "TITLE"),				
				@FieldResult(name = "displayOrder",   column = "DISPLAY_ORDER")
			}
		)
	}
 )



@Entity
@Table(name = "FEATURED_TITLES")
public class EBookTopFeaturedTitles {
	public static final String SEARCH_TOP_FEATURED_TITLES 	= "searchTopFeaturedTitles";
	public static final String TOP_FEATURED_RESULT_SET = "topFeaturedTitlesResult";

	
	@Id	
	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "TITLE")
	private String title;
	
	@Column( name="DISPLAY_ORDER")
	private int displayOrder;
	

	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getEscapeTitle() {
		return this.title != null ? StringEscapeUtils.escapeXml(this.title) : this.title;
		
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int display_order) {
		this.displayOrder = display_order;
	}
}
