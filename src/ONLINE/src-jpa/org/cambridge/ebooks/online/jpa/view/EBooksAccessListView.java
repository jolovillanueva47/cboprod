package org.cambridge.ebooks.online.jpa.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.subscription.AccessList;



@Entity
@Table(name = "EBOOKS_ACCESS_LIST_VIEW")
public class EBooksAccessListView {

	public static final String SEARCH_VIEW 	= "searchView";
	public static final String VIEW_RESULT_SET = "viewResult";
	
	
	
	@Id
	@Column( name = "ACCESS_LIST_ID")
	private int accessListId;

	@Column( name = "BODY_ID")
	private String bodyId;
	
	@Column( name = "EISBN")
	private String eisbn;
	
	@Column( name = "ORDER_ID")
	private String orderId;
	
	@Column( name = "STATUS")
	private String status;
	
	@Column( name = "ORDER_TYPE")
	private String orderType;
	
	@Column( name = "COLLECTION_ID")
	private String collectionId;
	
	@Column(name = "ACCESS_LIMIT")
	private int accessLimit;	
		
	@Transient
	private boolean isFreeTrial;
	
	@Transient
	private String concurrencyType;
	
	@Transient
	private boolean hasBookSlots;
	
	@Transient
	private boolean checkForConcurrency;

	public boolean hasBookSlots() {
		return hasBookSlots;
	}


	public void setHasBookSlots(boolean hasBookSlots) {
		this.hasBookSlots = hasBookSlots;
	}


	public String getConcurrencyType() {
		return concurrencyType;
	}


	public void setConcurrencyType(String concurrencyType) {
		this.concurrencyType = concurrencyType;
	}


	public boolean hasAccess(){
		boolean hasAccess = false;
		if(StringUtils.isNotEmpty(orderType) || isFreeTrial){			
			//check only concurrency
			if(hasConcurrency() && checkForConcurrency()){
				if(hasBookSlots()){
					hasAccess = true;
				} else {
					hasAccess = false;
				}
			} else {
				hasAccess = true;
			}
		}
		return hasAccess;
	}
	
	public boolean isConcurrencyError(){
		boolean isConcurrencyError = false;
		if(hasConcurrency() && checkForConcurrency()){
			if(hasBookSlots()){
				isConcurrencyError = false;
			} else {
				isConcurrencyError = true;
			}
		}
		
		return isConcurrencyError;
	}
	
	public String hasAccessDisplay(){		
		String accessType = this.getOrderType();
		
		if(null == accessType) {
			accessType = AccessList.NO_ACCESS;
		} else if(isFreeTrial) {
			accessType = AccessList.FREE_TRIAL;
		} 
		
//		if(AccessList.HAS_ACCESS.equals(_accessType)){
//			accessType = AccessList.HAS_ACCESS;
//		} else if(AccessList.SUBSCRIPTION.equals(_accessType) || AccessList.PATRON_DRIVEN.equals(_accessType)){
//			accessType = AccessList.SUBSCRIPTION;
//		} else if(AccessList.DEMO.equals(_accessType)) {
//			accessType = AccessList.DEMO;
//		} else if(isFreeTrial) {
//			accessType = AccessList.FREE_TRIAL;
//		}
		return accessType;
	}
	
	
	/* getters / setters */

	public int getAccessListId() {
		return accessListId;
	}

	public void setAccessListId(int accessListId) {
		this.accessListId = accessListId;
	}

	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getEisbn() {
		return eisbn;
	}

	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public boolean isFreeTrial() {
		return isFreeTrial;
	}

	public void setFreeTrial(boolean isFreeTrial) {
		this.isFreeTrial = isFreeTrial;
	}

	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public int getAccessLimit() {
		return accessLimit;
	}

	public void setAccessLimit(int accessLimit) {
		this.accessLimit = accessLimit;
	}
	
	public boolean checkForConcurrency() {
		return checkForConcurrency;
	}


	public void setCheckForConcurrency(boolean checkForConcurrency) {
		this.checkForConcurrency = checkForConcurrency;
	}


	public boolean hasConcurrency() {
		boolean isConcurrentSubscription = false;
		
		if(this.getAccessLimit() > 0) {
			isConcurrentSubscription = true;
		}
		
		return isConcurrentSubscription;
	}
}
