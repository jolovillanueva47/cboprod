package org.cambridge.ebooks.online.jpa.collections;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EBOOK_COLLECTION_LIST")
public class EbooksAutocompleteCollections {

	@Id
	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "COLLECTION_ID")
	private String collectionId;

	@Column( name="TITLE" )
	private String title;
	
	@Column( name="TITLE_ALPHASORT" )
	private String titleAlphaSort;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EbooksAutocompleteCollections other = (EbooksAutocompleteCollections) obj;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equalsIgnoreCase(other.isbn))
			return false;
		return true;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitleAlphaSort(String titleAlphaSort) {
		this.titleAlphaSort = titleAlphaSort;
	}

	public String getTitleAlphaSort() {
		return titleAlphaSort;
	}

}


