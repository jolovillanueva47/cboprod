package org.cambridge.ebooks.online.jpa.series;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = EBookSeriesLoad.SEARCH_SERIES_DESC,
		query = "SELECT * FROM EBOOK_SERIES_LOAD WHERE series_code = ?1",
		resultSetMapping = EBookSeriesLoad.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = EBookSeriesLoad.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = EBookSeriesLoad.class,
				fields = {
					@FieldResult(name = "seriesCode", column = "SERIES_CODE"),
					@FieldResult(name = "seriesLegend", column = "SERIES_LEGEND"),
					//@FieldResult(name = "seriesDesc", column = "SERIES_DESCRIPTION") for DGTIGER
					@FieldResult(name = "seriesDesc", column = "SERIES_BLURB") // for TIGER1T
				}
			)	
		}
	)
})

@Entity
@Table(name = "EBOOK_SERIES_LOAD")
public class EBookSeriesLoad {

	public static final String SEARCH_SERIES_DESC = "searchSeriesDescription";
	public static final String RESULT_SET_MAPPING = "findSeriesResult";
	
	@Id
	@Column( name = "SERIES_CODE")
	private String seriesCode;
	
	@Column( name = "SERIES_LEGEND")
	private String seriesLegend;
	
	@Column( name = "SERIES_BLURB")
	private String seriesDesc;

	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getSeriesLegend() {
		return seriesLegend;
	}

	public void setSeriesLegend(String seriesLegend) {
		this.seriesLegend = seriesLegend;
	}

	public String getSeriesDesc() {
		return seriesDesc;
	}

	public void setSeriesDesc(String seriesDesc) {
		this.seriesDesc = seriesDesc;
	}
}
