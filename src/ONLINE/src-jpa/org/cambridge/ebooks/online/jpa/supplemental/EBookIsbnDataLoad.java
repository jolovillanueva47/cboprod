package org.cambridge.ebooks.online.jpa.supplemental;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "EBOOK_ISBN_DATA_LOAD")
public class EBookIsbnDataLoad {

	@Id
	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "SUPPLEMENTAL_MATERIAL")
	private String supplementalMaterial;
	
	@Column( name = "SUPPLEMENTAL_MATERIAL_URL")
	private String supplementalMaterialUrl;
	
	@Column( name = "ANSWER_CODE")
	private String answerCode;
	
	@Column( name = "SALES_MODULE_FLAG")
	private String salesModuleFlag;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getSupplementalMaterial() {
		return supplementalMaterial;
	}

	public void setSupplementalMaterial(String supplementalMaterial) {
		this.supplementalMaterial = supplementalMaterial;
	}

	public String getSupplementalMaterialUrl() {
		return supplementalMaterialUrl;
	}

	public void setSupplementalMaterialUrl(String supplementalMaterialUrl) {
		this.supplementalMaterialUrl = supplementalMaterialUrl;
	}

	public String getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(String answerCode) {
		this.answerCode = answerCode;
	}

	public String getSalesModuleFlag() {
		return salesModuleFlag;
	}

	public void setSalesModuleFlag(String salesModuleFlag) {
		this.salesModuleFlag = salesModuleFlag;
	}
	
	
}
