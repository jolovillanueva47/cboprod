package org.cambridge.ebooks.online.jpa.esampling.loginrelated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@NamedNativeQueries({
	@NamedNativeQuery(
	        name = ChangePassword.UPDATE_PASSWORD, 
	        query = " UPDATE ESAMPLE_USER  SET   USER_PASSWORD = ?1 " +
    		"WHERE EMAIL_ADDRESS = ?2  " 
	),
	
	@NamedNativeQuery(
			name = ChangePassword.GET_OLD_PASSWORD,
			query = " SELECT  USER_PASSWORD, USER_ID " +
			" FROM   ESAMPLE_USER  WHERE  EMAIL_ADDRESS = ?1 " ,
			
			
			resultSetMapping = "getOldPassword"
		)
})
	


@SqlResultSetMapping(
    name = "getOldPassword", 
    entities = { 
        @EntityResult(
            entityClass = ChangePassword.class, 
            fields = {
            	
                @FieldResult(name = "memberId", 	column = "USER_ID"),
            	@FieldResult(name = "mem_password", 	column = "USER_PASSWORD"),
            	@FieldResult(name = "mem_user", 	column = "EMAIL_ADDRESS")

            }
        ) 
    }
)


@Entity
@Table(name = "ESAMPLE_USER")

public class ChangePassword {
	public static final String UPDATE_PASSWORD = "UPDATE_REGISTERED_PASSWORD";
	public static final String GET_OLD_PASSWORD = "GET_OLD_PASSWORD";
	
	
	@Id
	@Column( name = "USER_ID")
	private String memberId;
	
	
	@Column( name = "USER_PASSWORD")
	private String mem_password;
	
	@Column( name = "EMAIL_ADDRESS")
	private String mem_user;	


	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public String getMem_password() {
		return mem_password;
	}


	public void setMem_password(String mem_password) {
		this.mem_password = mem_password;
	}


	public String getMem_user() {
		return mem_user;
	}


	public void setMem_user(String mem_user) {
		this.mem_user = mem_user;
	}




	


	
	
}
