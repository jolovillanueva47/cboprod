package org.cambridge.ebooks.online.jpa.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table( name = "BLADE_PREF") 
public class UserPreference {
	
	@Id
	@Column( name = "MEMBER_ID")
	private String memberId;
	
	@Column( name = "PREF_COOKIE" ) 
	private String cookieEnabled;
	
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private User userInfo;
	
	public String getCookieEnabled() {
		return cookieEnabled;
	}
	
	public void setCookieEnabled(String cookieEnabled) {
		this.cookieEnabled = cookieEnabled;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public User getUserInfo() {
		return userInfo;
	}
	
	public void setUserInfo(User userInfo) {
		this.userInfo = userInfo;
	}

}

