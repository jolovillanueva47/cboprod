/**
 * 
 */
package org.cambridge.ebooks.online.jpa.esampling.ebook;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

/**
 * @author fmergano
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = ESamplingEBookLandingPage.ESAMPLING_CHAPTER_COUNTER, 
	        query = " INSERT INTO ESAMPLE_REQUEST_ACTIVITY (REQUEST_ID, CHAPTER_ID) " +
	        "VALUES (?1, ?2) "
	),
	@NamedNativeQuery(
		name = ESamplingEBookLandingPage.ESAMPLING_BOOKSHELF_ACCESS,
		query = "SELECT request_id " +
				"FROM esample_request " +
				"WHERE isbn = ?1 " +
				"AND user_id = ?2 " +
				"AND to_date(valid_from,'dd-Mon-yyyy') <= to_date(sysdate,'dd-Mon-yyyy') " +
				"AND to_date(valid_to,'dd-Mon-yyyy') >= to_date(sysdate,'dd-Mon-yyyy') ",
		resultSetMapping = ESamplingEBookLandingPage.ESAMPLING_BOOKSHELF_RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = ESamplingEBookLandingPage.ESAMPLING_BOOKSHELF_RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = ESamplingEBookLandingPage.class,
				fields = {
					@FieldResult(name = "requestId", column = "REQUEST_ID")
				}
			)	
		}
	)
})

@Entity
@Table(name = "ESAMPLING_BOOKSHELF")
public class ESamplingEBookLandingPage {
	
	public static final String ESAMPLING_BOOKSHELF_ACCESS = "checkPreviewAccess";
	public static final String ESAMPLING_CHAPTER_COUNTER = "updateChapterReport";
	public static final String ESAMPLING_BOOKSHELF_RESULT_SET_MAPPING = "previewAccessResult";
	
	@Id
	@Column( name = "requestId")
	private String requestId;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}







}
