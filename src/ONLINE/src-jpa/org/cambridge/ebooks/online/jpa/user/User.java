package org.cambridge.ebooks.online.jpa.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.cambridge.ebooks.online.countrystates.CountryStateMapWorker;

/**
 * 
 * @author rvillamor
 * 
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = User.SEARCH_BLADE_MEMBER_BY_USERNAME, 
	        query = " select mem.body_id, bm.member_title, bm.member_email_1, bm.member_country_id, bm.member_state_county, " +
	        		" bm.member_city, bm.member_post_code, bm.member_address_1, bm.member_address_2, bm.member_id, " +
	        		" bm.member_first_name, bm.member_surname, " +
	        		" bm.member_userid, bm.member_spell, country.currency, bm.member_telephone, pref.pref_cookie " +
	        		" from blade_member bm, member mem, country country, blade_pref pref " +
	        		" where bm.member_userid = ?1 and " +
	        		" mem.BLADE_MEMBER_ID = bm.MEMBER_ID and " + 
	        		" country.country_id = bm.member_country_id and " +
	        		" pref.MEMBER_ID = bm.member_id  " ,
	        resultSetMapping = "searchUserResult"
	), 
	
	@NamedNativeQuery(
	        name = User.SEARCH_USER_BY_BODY_ID, 
	        query = " select mem.body_id, bm.member_title, bm.member_email_1, bm.member_country_id, bm.member_state_county, " +   
	        		" bm.member_city, bm.member_post_code, bm.member_address_1, bm.member_address_2, bm.member_id,  " + 
	        		" bm.member_first_name, bm.member_surname,  " + 
	        		" bm.member_userid, bm.member_spell, country.currency, bm.member_telephone, pref.pref_cookie " +   
	        		" from blade_member bm, member mem, country country, blade_pref pref   " + 
	        		" where mem.body_id = ?1 and   " + 
	        		" mem.BLADE_MEMBER_ID = bm.MEMBER_ID and   " +  
	        		" country.country_id = bm.member_country_id and " +   
	        		" pref.MEMBER_ID = bm.member_id ", 
	        resultSetMapping = "searchUserResult"
	)
	
})

@SqlResultSetMapping(
    name = "searchUserResult", 
    entities = { 
        @EntityResult(
            entityClass = User.class, 
            fields = {
                @FieldResult(name = "bodyId", 	column = "BODY_ID"),
                @FieldResult(name = "title", 	column = "MEMBER_TITLE"), 
                @FieldResult(name = "email", 	column = "MEMBER_EMAIL_1"),
                @FieldResult(name = "countryId", column = "MEMBER_COUNTRY_ID"),
                @FieldResult(name = "stateId", 	column = "MEMBER_STATE_COUNTY"),
                @FieldResult(name = "townCity", column = "MEMBER_CITY"),
                @FieldResult(name = "postCode", column = "MEMBER_POST_CODE"),
                @FieldResult(name = "address1", column = "MEMBER_ADDRESS_1"),
                @FieldResult(name = "address2", column = "MEMBER_ADDRESS_2"),
                @FieldResult(name = "memberId", column = "MEMBER_ID"),
                @FieldResult(name = "firstName", column = "MEMBER_FIRST_NAME"),
                @FieldResult(name = "lastName", column = "MEMBER_SURNAME"),
                @FieldResult(name = "username", column = "MEMBER_USERID"),
                @FieldResult(name = "password", column = "MEMBER_SPELL"),
                @FieldResult(name = "currency", column = "CURRENCY"),
                @FieldResult(name = "telephone", column = "MEMBER_TELEPHONE")
            }
        ) 
    }
)

@Entity
@Table(name = "BLADE_MEMBER")
public class User {

	public static final String SEARCH_BLADE_MEMBER_BY_USERNAME = "searchBladeMemberByUsername";
	public static final String SEARCH_USER_BY_BODY_ID = "searchUserByMemberId";
	//public static final String STAHL_COMPONENT_ID = System.getProperty("stahl.componentid");
	
	@Id
	@Column( name = "MEMBER_ID")
	private String memberId;
	
	@OneToOne(mappedBy="userInfo")
	private MemberBody memberBody;
	

	@Column( name = "MEMBER_TITLE")
	private String title;

	@Column( name = "MEMBER_EMAIL_1")
	private String email;

	@Column( name = "MEMBER_COUNTRY_ID")
	private String countryId;

	@Column( name = "MEMBER_STATE_COUNTY")
	private String stateId;

	@Column( name = "MEMBER_CITY")
	private String townCity;

	@Column( name = "MEMBER_POST_CODE")
	private String postCode;

	@Column( name = "MEMBER_ADDRESS_1")
	private String address1;

	@Column( name = "MEMBER_ADDRESS_2")
	private String address2;

	@Column( name = "MEMBER_FIRST_NAME")
	private String firstName;

	@Column( name = "MEMBER_SURNAME")
	private String lastName;

	@Column( name = "MEMBER_USERID")
	private String username;

	@Column( name = "MEMBER_SPELL")
	private String password;
	
//	@Column( name = "CURRENCY")
//	private String currency; 
	
	@Column( name = "MEMBER_TELEPHONE")
	private String telephone;
	
//	@Column( name = "PREF_COOKIE")
//	private String cookieEnabled;
	
	@OneToOne(mappedBy="userInfo")
	private UserPreference preferences;
	
		
	public UserPreference getPreferences() {
		return preferences;
	}

	public void setPreferences(UserPreference preferences) {
		this.preferences = preferences;
	}

	public String getEncryptedDetails() {
		return DESHEXUtil.encrypt( getMemberId() );
	}
	
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCountryName() { 
		return CountryStateMapWorker.countryMap.get(countryId);
	}
	
	public String getStateName() { 
		return CountryStateMapWorker.getCountyStateMap(getCountryId()).get(getStateId());
	}
	
	public String getCurrency() {
		String currency = CountryStateMapWorker.countryCurrencyMap.get(countryId);
		return "usd".equals(currency) ? currency : "gbp";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String bodyId) {
		this.memberId = bodyId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String countyStateProvince) {
		this.stateId = countyStateProvince;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTownCity() {
		return townCity;
	}

	public void setTownCity(String townCity) {
		this.townCity = townCity;
	}

	public String getBodyId() {
		return memberBody.getBodyId();
	}
	
	public String getFullName(){
		return this.getFirstName() + " " + getLastName();
	}

}
