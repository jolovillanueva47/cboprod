package org.cambridge.ebooks.online.jpa.society.autologin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="ORACJOC.society_autologin_cbo")
public class AutoLoginCBO {	
	
	@Id
	@Column(name="LOGIN_ID")
	private String loginId;
	
	@Column(name="SOCIETY_ID")
	private String societyId;
	
	@Column(name="URL_PATH")
	private String urlPath;
	
	@Column(name="AUTHENTICATION_URL")
	private String authenticationUrl;	
	
	private String isbn;
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getUrlPath() {
		return urlPath;
	}
	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
	public String getAuthenticationUrl() {
		return authenticationUrl;
	}
	public void setAuthenticationUrl(String authenticationUrl) {
		this.authenticationUrl = authenticationUrl;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
		
}
