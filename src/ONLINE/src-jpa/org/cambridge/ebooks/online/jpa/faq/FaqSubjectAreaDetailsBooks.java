package org.cambridge.ebooks.online.jpa.faq;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author Karlson A. Mulingtapang
 * FAQSubjectAreaDetails.java - FAQ_SUBJECT_AREA_DETAILS synonymous with CJO
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = FaqSubjectAreaDetailsBooks.SEARCH_ALL,
		query = "Select * from FAQ_SUBJECT_AREA_DETAILS_BOOKS ORDER BY DISPLAY_ORDER",
		resultSetMapping = FaqSubjectAreaDetailsBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = FaqSubjectAreaDetailsBooks.SEARCH_BY_ID,
		query = "Select * from FAQ_SUBJECT_AREA_DETAILS_BOOKS where ID = ?1",
		resultSetMapping = FaqSubjectAreaDetailsBooks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = FaqSubjectAreaDetailsBooks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = FaqSubjectAreaDetailsBooks.class,
				fields = {
					@FieldResult(name = "id", column = "ID"),
					@FieldResult(name = "question", column = "QUESTION"),
					@FieldResult(name = "answer", column = "ANSWER"),
					@FieldResult(name = "displayOrder", column = "DISPLAY_ORDER"),
					@FieldResult(name = "enable", column = "ENABLE"),
					@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
					@FieldResult(name = "modifiedBy", column = "MODIFIED_BY")
				}
			)	
		}
	)
})

@Entity
@Table(name = "FAQ_SUBJECT_AREA_DETAILS_BOOKS")
public class FaqSubjectAreaDetailsBooks {
	
	public static final String RESULT_SET_MAPPING = "FAQ_SUBJECT_AREA_DETAILS_BOOKS.result";
	public static final String SEARCH_ALL = "FAQ_SUBJECT_AREA_DETAILS_BOOKS.searchAll";
	public static final String SEARCH_BY_ID = "FAQ_SUBJECT_AREA_DETAILS_BOOKS.searchById";
	
	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "QUESTION")
	private String question;
	
	@Column(name = "ANSWER")
	private String answer;
	
	@Column(name = "DISPLAY_ORDER")
	private int displayOrder;
	
	@Column(name = "ENABLE")
	private String enable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
		
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getEscapeQuestion(){
		return this.question != null ? StringEscapeUtils.escapeXml(this.question) : this.question;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getEscapeAnswer(){
		
		return this.answer != null ? StringEscapeUtils.escapeXml(this.answer) : this.answer;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}	
}
