package org.cambridge.ebooks.online.jpa.esampling.loginrelated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;




@NamedNativeQueries({
	@NamedNativeQuery(
	        name = UpdateTempPassword.UPDATE_TEMP_PASSWORD, 
	        query = " UPDATE ESAMPLE_USER  SET   USER_PASSWORD = ?1 " +
    		"WHERE EMAIL_ADDRESS = ?2 " 
	),
	
	@NamedNativeQuery(
			name = UpdateTempPassword.GET_TEMP_PASSWORD,
			query = " SELECT EMAIL_ADDRESS,USER_ID,FORENAME " +
			" FROM   ESAMPLE_USER  WHERE  EMAIL_ADDRESS = ?1 " ,
			
			resultSetMapping = "getTempPass"
		)
})
	


@SqlResultSetMapping(
    name = "getTempPass", 
    entities = { 
        @EntityResult(
            entityClass = UpdateTempPassword.class, 
            fields = {
            	
                @FieldResult(name = "max_cust_id", 	column = "USER_ID"),
            	@FieldResult(name = "email_address", 	column = "EMAIL_ADDRESS"),
                @FieldResult(name = "firstname", 	column = "FORENAME")

            }
        ) 
    }
)


@Entity
@Table(name = "ESAMPLE_USER")


public class UpdateTempPassword {
	public static final String UPDATE_TEMP_PASSWORD = "UPDATE_TEMP_PASSWORD";
	public static final String GET_TEMP_PASSWORD = "get_temp_password";
	
	
	@Id
	@Column( name = "MEMBER_ID")
	private String max_cust_id;
	
	
	@Column( name = "MEMBER_USERID")
	private String email_address;

	@Column( name = "MEMBER_EMAIL_1")
	private String firstname;

	public String getMax_cust_id() {
		return max_cust_id;
	}

	public void setMax_cust_id(String max_cust_id) {
		this.max_cust_id = max_cust_id;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	


}
