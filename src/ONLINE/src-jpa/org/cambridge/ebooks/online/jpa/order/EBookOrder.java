package org.cambridge.ebooks.online.jpa.order;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;



@Entity
@Table(name = "EBOOK_ORDER")
public class EBookOrder {
	
	public static final String UPDATE_ORDER = "UPDATE_ORDER"; 
	
	@Id
	@Column(name = "ORDER_ID")
	private String orderId;
		
	@Column(name = "ORDER_CONFIRMED_TIMESTAMP")	
	private Timestamp ebookOrderTimestamp;
	
	@Column(name = "ORDER_CONFIRMEE_EMAIL")
	private String orderConfirmeeEmail;
	
	@Column(name = "ORDER_STATUS")
	private String orderStatus;
	
	@Column(name = "ORDER_CONFIRMEE_NAME")
	private String orderConfirmeeName;
	
	@Column(name = "ORDER_CONFIRMEE_TITLE")
	private String orderConfirmeeTitle;
	
	@Transient
	private boolean error;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Timestamp getEbookOrderTimestamp() {
		return ebookOrderTimestamp;
	}

	public void setEbookOrderTimestamp(Timestamp ebookOrderTimestamp) {
		this.ebookOrderTimestamp = ebookOrderTimestamp;
	}

	public String getOrderConfirmeeEmail() {
		return orderConfirmeeEmail;
	}

	public void setOrderConfirmeeEmail(String orderConfirmeeEmail) {
		this.orderConfirmeeEmail = orderConfirmeeEmail;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderConfirmeeName() {
		return orderConfirmeeName;
	}

	public void setOrderConfirmeeName(String orderConfirmeeName) {
		this.orderConfirmeeName = orderConfirmeeName;
	}

	public String getOrderConfirmeeTitle() {
		return orderConfirmeeTitle;
	}

	public void setOrderConfirmeeTitle(String orderConfirmeeTitle) {
		this.orderConfirmeeTitle = orderConfirmeeTitle;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	
	
}
