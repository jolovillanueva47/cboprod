/**
 * 
 */
package org.cambridge.ebooks.online.jpa.concurrency;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author kmulingtapang
 */


@NamedNativeQueries({
	@NamedNativeQuery(
		name = EbookConcurrency.CHECK_BOOK_ACCESS,
		query = "select * from EBOOK_CONCURRENCY where order_id = ?1 and isbn = ?2",
		resultClass = EbookConcurrency.class
	),
	@NamedNativeQuery(
		name = EbookConcurrency.CHECK_BOOK_ACCESS_BY_THIS_SESSION,
		query = "select * from EBOOK_CONCURRENCY where order_id = ?1 and isbn = ?2 and session_id = ?3",
		resultClass = EbookConcurrency.class
	)
})

@Entity
@Table(name="EBOOK_CONCURRENCY")
public class EbookConcurrency {

	public static final String CHECK_BOOK_ACCESS = "EBOOK_CONCURRENCY.CHECK_BOOK_ACCESS";
	public static final String CHECK_BOOK_ACCESS_BY_THIS_SESSION = "EBOOK_CONCURRENCY.CHECK_BOOK_ACCESS_BY_THIS_SESSION";
	
	@Id
	@GeneratedValue(generator = "EBOOK_CONCURRENCY_SEQ")
	@SequenceGenerator(name = "EBOOK_CONCURRENCY_SEQ", sequenceName = "EBOOK_CONCURRENCY_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private String id;
	
	@Column(name = "ORDER_ID")
	private String orderId;
	
	@Column(name = "MAX_ACCESS")
	private int maxAccess;
		
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "ACCESS_TIME")
	private Timestamp accessTime;
	
	@Column(name = "SESSION_ID")
	private String sessionId;

	@Column(name = "SERVER_IP")
	private String serverIp;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public int getMaxAccess() {
		return maxAccess;
	}

	public void setMaxAccess(int maxAccess) {
		this.maxAccess = maxAccess;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public Timestamp getAccessTime() {
		return accessTime;
	}
	
	public void setAccessTime(Timestamp accessTime) {
		this.accessTime = accessTime;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}	
}
