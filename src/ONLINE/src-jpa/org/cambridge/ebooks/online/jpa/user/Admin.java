package org.cambridge.ebooks.online.jpa.user;



import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;


@NamedNativeQueries({
	@NamedNativeQuery(
			name = Admin.SEARCH_ADMIN_BY_TERMS_CONFIRM, 
		        query = "Select AD.ADMIN_TERMSAGREED, AD.BLADE_MEMBER_ID from ORACJOC.BLADE_MEMBER BM, ORACJOC.ADMIN AD " +
		        		"where BM.member_userid = ?1 " +
		        		"and AD.blade_member_id = BM.member_id",
		        resultClass = Admin.class
	),
	 
	@NamedNativeQuery(
			name = Admin.UPDATE_DATE_TERMS_AGREED, 
				 query = "update ORACJOC.BLADE_MEMBER BM SET BM.MEMBER_TERMSAGREED =  to_date(?1,'YYYY-MM-DD') " +
				        "where BM.MEMBER_UserID = ?2 and BM.MEMBER_TYPE = 'Administrator'",
				 resultClass = Admin.class
	)
})

@Entity
@Table(name = "ADMIN")
public class Admin {
	
	public static final String SEARCH_ADMIN_BY_TERMS_CONFIRM = "SEARCH_ADMIN_BY_TERMS_CONFIRM";

	public static final String UPDATE_DATE_TERMS_AGREED = "UPDATE_DATE_TERMS_AGREED";
	
	@Id
	@Column( name = "BLADE_MEMBER_ID")
	private String bladeMemberId;

	@Column( name = "ADMIN_TERMSAGREED")
	private Date dateTermsAgreed;
	
	public String getBladeMemberId() {
		return bladeMemberId;
	}

	public void setBladeMemberId(String bladeMemberId) {
		this.bladeMemberId = bladeMemberId;
	}
	
	public Date getDateTermsAgreed() {
		return dateTermsAgreed;
	}

	public void setDateTermsAgreed(Date dateTermsAgreed) {
		this.dateTermsAgreed = dateTermsAgreed;
	}

}
