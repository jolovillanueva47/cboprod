package org.cambridge.ebooks.online.jpa.user;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NoResultException;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.PersistenceUtil;


@NamedNativeQueries({
	@NamedNativeQuery(
		name = Login.LOGIN, 
		query = 
			"select BM.MEMBER_ID, BM.MEMBER_USERID, BM.MEMBER_SPELL, rownum LOGIN_ID " +
			"from ORACJOC.BLADE_MEMBER BM " +
			"where BM.MEMBER_USERID = ?1",			            
	    resultClass = Login.class
	 ),
	@NamedNativeQuery(
		name = Login.ADMIN, 
		query = 
			"select A.ORGANISATION_ID ORG_BODY_ID, B.BODY_TYPE, BM.MEMBER_USERID, BM.MEMBER_ID, A.DEFAULT_FLAG, rownum LOGIN_ID   " +
			"from ORACJOC.BLADE_MEMBER BM, ORACJOC.ADMIN A, ORACJOC.BODY B " +
			"where BM.MEMBER_ID = A.BLADE_MEMBER_ID " +			 
			  "and A.ORGANISATION_ID = B.BODY_ID " +
			  "and BM.MEMBER_USERID = ?1",			            
		resultClass = Login.class
	),
	@NamedNativeQuery(
		name = Login.MEMBERSHIP, 
		query = 
			"select wm_concat(org_id) ORGLIST, wm_concat(TYPE) TYPE_LIST, 'null' MEMBER_USERID, MEMBER_ID, rownum LOGIN_ID " +
			"from " +
			"( " +
			"select ORG_ID, TYPE " +
			"from ( " +
			/* get consortium from direct login -- indirect */
			  "select distinct consortium_id ORG_ID, 'C' TYPE " +
			  "from ORACJOC.consortium_organisation " + 
			  "where deleted='N'  " +
			    "and start_date <= sysdate and (end_date>=sysdate or end_date is null) " +
			    "and organisation_id = ?1 " +
			  "union " +			  
			  /* get consortium from remote user orgs -- indirect */ 
			  "select distinct consortium_id ORG_ID, 'C' TYPE " +
			  "from ORACJOC.consortium_organisation " + 
			  "where deleted='N'  " +
			    "and start_date <= sysdate and (end_date>=sysdate or end_date is null) " +
			    "and organisation_id in ( " +
			              "SELECT UNIQUE a.organisation_id " +
			              "FROM ORACJOC.membership a, ORACJOC.member b, ORACJOC.blade_member c " +
			              "WHERE a.BODY_ID = b.BODY_ID  " +
			                "and b.BLADE_MEMBER_ID = c.MEMBER_ID " + 
			                "AND a.status='Y'  " +
			                "and a.activation_date <= sysdate  " +
			                "and (a.expiration_date>= sysdate or a.expiration_date is null) " + 
			                "and c.member_userid = ?2 " + 
			    ")       " +
			  "union " +
			  /* get orgs from remote user */
			  "SELECT distinct a.organisation_id ORG_ID, 'O' TYPE " +
			  "FROM ORACJOC.membership a, ORACJOC.member b, ORACJOC.blade_member c " +
			  "WHERE a.BODY_ID = b.BODY_ID  " +
			    "and b.BLADE_MEMBER_ID = c.MEMBER_ID " + 
			    "and a.status='Y'  " +
			    "and a.activation_date <= sysdate  " +
			    "and (a.expiration_date>= sysdate or a.expiration_date is null) " + 
			    "and c.member_userid = ?2 " +
			  ")   " +
			"order by TYPE asc, ORG_ID desc " +
			") ",			            
		resultClass = Login.class
	),
	@NamedNativeQuery(
			name = Login.MEMBERSHIP_UNCONCAT, 
			query = 				
				"select ORG_ID BODY_ID, TYPE, DIRECT " +
				"from ( " +
				/* get consortium from direct login -- indirect */
				  "select distinct consortium_id ORG_ID, 'C' TYPE, 'N' DIRECT " +
				  "from ORACJOC.consortium_organisation " + 
				  "where deleted='N'  " +
				    "and start_date <= sysdate and (end_date>=sysdate or end_date is null) " +
				    "and organisation_id = ?1 " +
				  "union " +			  
				  /* get consortium from remote user orgs -- indirect */ 
				  "select distinct consortium_id ORG_ID, 'C' TYPE, 'N' DIRECT " +
				  "from ORACJOC.consortium_organisation " + 
				  "where deleted='N'  " +
				    "and start_date <= sysdate and (end_date>=sysdate or end_date is null) " +
				    "and organisation_id in ( " +
				              "SELECT UNIQUE a.organisation_id " +
				              "FROM ORACJOC.membership a, ORACJOC.member b, ORACJOC.blade_member c " +
				              "WHERE a.BODY_ID = b.BODY_ID  " +
				                "and b.BLADE_MEMBER_ID = c.MEMBER_ID " + 
				                "AND a.status='Y'  " +
				                "and a.activation_date <= sysdate  " +
				                "and (a.expiration_date>= sysdate or a.expiration_date is null) " + 
				                "and c.member_userid = ?2 " + 
				    ")       " +
				  "union " +
				  /* get orgs from remote user */
				  "SELECT distinct a.organisation_id ORG_ID, 'O' TYPE, 'Y' DIRECT " +
				  "FROM ORACJOC.membership a, ORACJOC.member b, ORACJOC.blade_member c " +
				  "WHERE a.BODY_ID = b.BODY_ID  " +
				    "and b.BLADE_MEMBER_ID = c.MEMBER_ID " + 
				    "and a.status='Y'  " +
				    "and a.activation_date <= sysdate  " +
				    "and (a.expiration_date>= sysdate or a.expiration_date is null) " + 
				    "and c.member_userid = ?2 " +
				  ")   " +
				"order by TYPE asc, ORG_ID desc ",			            
			resultClass = Organisation.class
		)
	 
})


@Entity
@Table(name = "BLADE_MEMBER")
public class Login {
		
	public static final String LOGIN = "LOGIN";
	
	public static final String ADMIN = "ADMIN";
	
	public static final String MEMBERSHIP = "MEMBERSHIP";
	
	public static final String MEMBERSHIP_UNCONCAT = "MEMBERSHIP_UNCONCAT";
	

	private static final Logger LOGGER = Logger.getLogger(Login.class);
	
	private static final String YES = "Y";
	
	private static final String NO = "N";
	
	private static final String GUA = "gua";
		
	
	@Id
	@Column( name = "LOGIN_ID")
	private String loginId;	
	
	@Column( name = "MEMBER_USERID")
	private String memberUserId;
	
	@Column( name = "MEMBER_ID")
	private String memberId;
	
	@Column( name = "MEMBER_SPELL")
	private String memberPassword;
	
	
	@Column( name = "ORG_BODY_ID")
	private String orgBodyId;
	
	@Column( name = "BODY_TYPE")
	private String bodyType;
		
	@Column( name = "ORGLIST")
	private String orgList;
		
	@Column( name = "TYPE_LIST")
	private String typeList;
	
	@Transient
	private String userType;
	
	@Column( name = "DEFAULT_FLAG")
	private String defaultFlag;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MEMBER_ID", insertable=false, updatable=false )
	private User user;
	
	@Transient
	private String checkUserAdminCount;
	
	@Transient
	private boolean isMultipleAdmin;
	
	public String getIsAdmin() {
		if(StringUtils.isNotEmpty(orgBodyId)){
			return YES;
		}
		return NO;
	}	
	
	public boolean isMultipleAdmin() {
		if(StringUtils.isEmpty(checkUserAdminCount)) {
			checkUserAdminCount = "execute";
			isMultipleAdmin = isMultipleAdmin(memberId);
		} 
		
		return isMultipleAdmin;
	}

	public String getMemberUserId() {
		return memberUserId;
	}

	public void setMemberUserId(String memberUserId) {
		this.memberUserId = memberUserId;
	}

	public String getMemberPassword() {
		return memberPassword;
	}

	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}

	public String getOrgBodyId() {
		return orgBodyId;
	}

	public void setOrgBodyId(String orgBodyId) {
		this.orgBodyId = orgBodyId;		
	}

	public String getBodyType() {
		return this.bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getOrgList() {
		return orgList;
	}

	public void setOrgList(String orgList) {
		this.orgList = orgList;
	}
	
	public String getUserType() {		
		this.userType = getNextPage(this, this.bodyType, GUA);		
		return this.userType; 
	}
	
	public void setUserType(String userType){
		this.userType = userType;
	}
		
	public String getTypeList() {
		return typeList;
	}

	public void setTypeList(String typeList) {
		this.typeList = typeList;
	}

	/**
	 * logic taken from CheckAccessWorker.getNextPage(LoginParseVo userVo,String curUserType)
	 * @param login
	 * @param curUserType
	 * @return
	 */
	public static String getNextPage(Login login, String bodyType, String curUserType){
		//check if the member is affiliated to an org
		String userType="";
		if (YES.equals(login.getIsAdmin())) {
			if ("2".equals(bodyType)) {
				//the user is organisation administrator (society or org)
				userType = "AO";
			}
			if ("3".equals(bodyType)) {
				//the user is consortia administrator
				userType = "AC";
			}
			if ("-1".equals(bodyType)) {
				//the user is society administrator
				userType = "AS";
			}
		} else if (StringUtils.isEmpty(login.getOrgList())) {
			if ("gua".equals(curUserType)) {
				//promote to RUI
				userType = "rui";
			} 
		} else if (StringUtils.isNotEmpty(login.getOrgList())) {
			if ("gua".equals(curUserType)) {
				//promote to RUI
				userType = "ruo";
			} else if ("guo".equals(curUserType)) {
				//promote to RUI
				userType = "ruo";
			} 

		}
		return userType;
	}
	
	/**
	 * logic taken from EBooksLoginAction.executeAsService
	 */	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		String userType = this.getUserType();
		boolean isAdministrator = YES.equals(this.getIsAdmin());
		
		String linebreak = System.getProperty("line.separator");
		
		sb.append("AUTHORIZED=" + Math.random());
		sb.append( linebreak );
		sb.append(userType);
		sb.append( linebreak );		
		String orgList = this.getOrgList();
		sb.append( "MEMBERSHIPS=" + (orgList == null ? "" : orgList) ) ;
		sb.append( linebreak );
		
		if ( isAdministrator ) 
		{ 
			sb.append("ADMINISTRATOR" );
			sb.append( linebreak );
			sb.append(this.getOrgBodyId());
			sb.append( linebreak );
			sb.append(this.getBodyType());
		}	
		return sb.toString();
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDefaultFlag() {
		return defaultFlag;
	}

	public void setDefaultFlag(String defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	
	private static boolean isMultipleAdmin(String memberId) {
		boolean isMultipleAdmin = false;
		
		EntityManager em = PersistenceUtil.emf.createEntityManager();
        Query query = em.createNativeQuery("select count(*) as ADMIN_COUNT from admin where blade_member_id = ?1");
        
        query.setParameter(1, memberId);
        
        try {
        	BigDecimal i = (BigDecimal)query.getSingleResult();
        	
        	if(i.intValue() > 1) {
        		isMultipleAdmin = true;
        	} 
        } catch (NoResultException e) {
        	LOGGER.error(e.getMessage());
        } catch (Exception e) {
        	LOGGER.error(ExceptionPrinter.getStackTraceAsString(e));
        } finally {
        	em.close();
        }
        
        return isMultipleAdmin;
	}
}

