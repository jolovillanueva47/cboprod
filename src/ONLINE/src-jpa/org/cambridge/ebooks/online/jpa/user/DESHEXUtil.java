package org.cambridge.ebooks.online.jpa.user;

import org.apache.commons.codec.binary.Hex;
import org.cambridge.util.DESEncrypter;

public class DESHEXUtil {
	
	public static String decrypt( final String decryptThis ) {
		String result = null;
		if ( StringUtil.isNotEmpty( decryptThis ) ) {
			DESEncrypter des = new DESEncrypter();
			try {
				char[] decodeThis = decryptThis.toCharArray();
				byte[] decodedBytes = Hex.decodeHex(decodeThis);
				String decrypted = des.decrypt(new String(decodedBytes));
				
				if ( StringUtil.isNotEmpty(decrypted) ) {
					result =  decrypted;
				}
			} catch (Exception ex) { 
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	public static String encrypt( String encrypThis ) {
		DESEncrypter des = new DESEncrypter();
		String encodedString = des.encrypt( encrypThis );
		return new String ( Hex.encodeHex(encodedString.getBytes()) ); 
	}
}
