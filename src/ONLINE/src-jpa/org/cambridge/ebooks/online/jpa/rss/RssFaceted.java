package org.cambridge.ebooks.online.jpa.rss;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.cambridge.ebooks.online.internal_reports.InternalReportsWorker;
import org.cambridge.ebooks.online.rss.search.faceted.RssServletFaceted;
import org.cambridge.ebooks.online.util.PersistenceUtil;



@Entity
@Table( name = "RSS_QUERY_FACETED" ) 
public class RssFaceted {
	
	public static final String SEARCH_TYPE = "searchType";
		
	public static final String QID = "qid";
	
	public static final String RSS = "rss";
	
	public static final String DELIMITER = "<<>>";
	
	public static final String EMPTY = "<<EmPtY>>";
	
	public static final String QUICK = "Q";
	
	public static final String ADVANCE = "A";
	
	public static final String PUBLISHER_CODE = "publisherCode";
	
	public static final String TARGET = "target";
	
	public static final String CONDITION = "condition";
	
	public static final String LOGICAL_OP = "logical_op";
	
	public static final String SEARCH_TEXT = "search_text";
	
	public static final String SEARCH_TEXT_2 = "search_text_2";
	
	public static final String SEARCH_WORD = "search_word";
	
	public static final String SEARCH_WORD_2 = "search_word_2";
	
	
	public RssFaceted(){
		
	}
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public RssFaceted(HttpServletRequest request) throws NullPointerException, ClassCastException {
		initRss(request);		
	}
	
	@Id
	@Column( name = "QID")
	@GeneratedValue(generator="RSS_QUERY_SEQ")
    @SequenceGenerator(name="RSS_QUERY_SEQ", sequenceName="RSS_QUERY_SEQ", allocationSize=1)
	private long qid;
	
	@Column( name = "SEARCH_TYPE")
	private String searchType;
	
	@Column( name = "PUBLISHER_CODE")
	private String publisherCode;
	
	@Column( name = "SEARCH_TEXT_QK")
	private String searchTextQk;
	
	@Column( name = "TARGET")
	private String target;
	
	@Column( name = "CONDITION")
	private String condition;
		
	@Column( name = "LOGICAL_OP")
	private String logicalOp;
	
	@Column( name = "SEARCH_TEXT")
	private String searchText;
	
	@Column( name = "SEARCH_TEXT_2")
	private String searchText2;
	
	
	public long getQid() {
		return qid;
	}



	public void setQid(long qid) {
		this.qid = qid;
	}



	public String getSearchType() {
		return searchType;
	}



	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}



	public String getPublisherCode() {
		return publisherCode;
	}



	public void setPublisherCode(String publisherCode) {
		this.publisherCode = publisherCode;
	}



	public String getSearchTextQk() {
		return searchTextQk;
	}



	public void setSearchTextQk(String searchTextQk) {
		this.searchTextQk = searchTextQk;
	}



	public String getTarget() {
		return target;
	}



	public void setTarget(String target) {
		this.target = target;
	}



	public String getCondition() {
		return condition;
	}



	public void setCondition(String condition) {
		this.condition = condition;
	}



	public String getLogicalOp() {
		return logicalOp;
	}



	public void setLogicalOp(String logicalOp) {
		this.logicalOp = logicalOp;
	}



	public String getSearchText() {
		return searchText;
	}



	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}



	public String getSearchText2() {
		return searchText2;
	}



	public void setSearchText2(String searchText2) {
		this.searchText2 = searchText2;
	}

	public static RssFaceted getRssFromDb(HttpServletRequest request){
		String qid = request.getParameter(QID);		
		EntityManager em = emf.createEntityManager();
		RssFaceted rss = null;
		try{
			rss = em.find(RssFaceted.class, Long.valueOf(qid));
		}catch (Exception e) {
			e.printStackTrace();					
		}finally{			
			em.close();			
		}
		
		return rss;
	}
	
	
	/* private methods */
	
	private void initRss(HttpServletRequest request){
		HttpSession session = request.getSession();
		String searchText = (String) session.getAttribute(RssServletFaceted.SEARCH_TEXT);
		if(StringUtils.isNotEmpty(searchText)){
			//quick search
			generateQuickRss(request);
		}else{
			//advance search
			generateAdvanceRss(request);
		}
		
		//set publisher code
		this.publisherCode = (String) session.getAttribute(PUBLISHER_CODE);
		
	}
	
	private void generateQuickRss(HttpServletRequest request){
		String searchText = (String) request.getSession().getAttribute(RssServletFaceted.SEARCH_TEXT);
		this.searchTextQk = searchText;
		this.searchType = QUICK;
	}
	
	@SuppressWarnings("unchecked")
	private void generateAdvanceRss(HttpServletRequest request){
		HttpSession session = request.getSession(); 
		Map<String, Object> param = (Map<String, Object>) session.getAttribute(
				InternalReportsWorker.ADVANCE_SEARCH_PARAM);
		
		String[] target = (String[]) param.get(TARGET);
		String[] condition = (String[]) param.get(CONDITION);
		String[] logical_op = (String[]) param.get(LOGICAL_OP);
		String[] search_text = (String[]) param.get(SEARCH_TEXT);
		String[] search_text_2 = (String[]) param.get(SEARCH_TEXT_2);
		
		this.target = processWithDelimiter(target);
		this.condition = processWithDelimiter(condition);
		this.logicalOp = processWithDelimiter(logical_op);
		this.searchText = processWithDelimiter(search_text);
		this.searchText2 = processWithDelimiter(search_text_2);
		
		this.searchType = ADVANCE;
	}
	
	private static String processWithDelimiter(String[] keywords){
		StringBuilder sb = new StringBuilder();
		int length = keywords.length;		
		if(length > 1){			
			for(int i=0; i < length ; i++){
				if(keywords[i].equals("")){
					sb.append(EMPTY);
				}else{
					sb.append(keywords[i]);
				}
				
				if(i < length - 1){				
					sb.append(DELIMITER);
				}
			}
		}else{
			try{
				sb.append(keywords[0]);
			}catch (ArrayIndexOutOfBoundsException e) {
				//do nothing
			}
			
		}
		return sb.toString();
	}
	
	public static String[] unprocessWithDelimiter(String keyword){
		if(keyword == null){
			return null;
		}
		String[] _keywords = keyword.split(DELIMITER);		
		for (int i = 0; i < _keywords.length; i++) {
			if(_keywords.equals(EMPTY)){
				_keywords[i] = "";
			}
		}
		return _keywords;
		
	}
	
	
	
	
}
