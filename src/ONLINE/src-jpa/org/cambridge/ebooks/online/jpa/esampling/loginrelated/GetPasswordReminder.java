package org.cambridge.ebooks.online.jpa.esampling.loginrelated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@NamedNativeQueries({
	@NamedNativeQuery(
	        name = GetPasswordReminder.GET_PASSWORD, 
	        query = " select EMAIL_ADDRESS,USER_PASSWORD,FORENAME,USER_ID,PASSWORD_REMINDER " +
	        		"from   ESAMPLE_USER " +
	        		"where EMAIL_ADDRESS = ?1 ",
	        		
	        resultSetMapping = "ianboolean"
	)
	
})

@SqlResultSetMapping(
    name = "ianboolean", 
    entities = { 
        @EntityResult(
            entityClass = GetPasswordReminder.class, 
            fields = {
            	
            	@FieldResult(name = "max_cust_id", 	column = "USER_ID"),
                @FieldResult(name = "email_address", 	column = "EMAIL_ADDRESS"),
                @FieldResult(name = "password", 	column = "USER_PASSWORD"), 
                @FieldResult(name = "firstname", column = "FORENAME"),
                @FieldResult(name = "passwordReminder", column = "PASSWORD_REMINDER"),

            }
        ) 
    }
)

@Entity
@Table(name = "ESAMPLE_USER")
public class GetPasswordReminder {


	public static final String GET_PASSWORD = "get_password";
	
	@Id
	@Column( name = "USER_ID")
	private String max_cust_id;
	
	
	@Column( name = "EMAIL_ADDRESS")
	private String email_address;
	
	@Column( name = "USER_PASSWORD")
	private String password;
	
	@Column( name = "FORENAME")
	private String firstname;
	
	@Column( name = "PASSWORD_REMINDER")
	private String passwordReminder;

	public String getMax_cust_id() {
		return max_cust_id;
	}

	public void setMax_cust_id(String max_cust_id) {
		this.max_cust_id = max_cust_id;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPasswordReminder() {
		return passwordReminder;
	}

	public void setPasswordReminder(String passwordReminder) {
		this.passwordReminder = passwordReminder;
	}


}
