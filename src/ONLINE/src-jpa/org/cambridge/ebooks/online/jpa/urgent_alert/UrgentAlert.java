package org.cambridge.ebooks.online.jpa.urgent_alert;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Joseph Mendiola
 * Based on News.java
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = UrgentAlert.SEARCH_ALL,
		query = "Select * from EMERGENCY_ANNOUNCE_CBO",
		resultSetMapping = UrgentAlert.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = UrgentAlert.RESULT_SET_MAPPING,
		entities = {
			@EntityResult(
				entityClass = UrgentAlert.class,
				fields = {
					@FieldResult(name = "title", column = "TITLE"),
					@FieldResult(name = "description", column = "DESCRIPTION"),
					@FieldResult(name = "display_online", column = "DISPLAY_ONLINE"),
					@FieldResult(name = "end_date", column = "END_DATE"),
					@FieldResult(name = "end_time", column = "END_TIME"),
					@FieldResult(name = "application_type", column = "APPLICATION_TYPE"),
					@FieldResult(name = "email", column = "EMAIL"),
					@FieldResult(name = "updated_date", column = "UPDATED_DATE"),
					@FieldResult(name = "updated_user", column = "UPDATED_USER"),
					@FieldResult(name = "email_status", column = "EMAIL_STATUS"),
					@FieldResult(name = "update_lib_newsfeed", column = "UPDATE_LIB_NEWSFEED"),
				}
			)
		}
	)
})

@Entity
@Table(name = "EMERGENCY_ANNOUNCE_CBO")
public class UrgentAlert {
	public static final String RESULT_SET_MAPPING = "URGENTALERT.result";
	public static final String SEARCH_ALL = "URGENTALERT.search";
	
	@Id
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "DISPLAY_ONLINE")
	private String display_online;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE")
	private Date end_date;
	
	@Column(name = "END_TIME")
	private String end_time;
	
	@Column(name = "APPLICATION_TYPE")
	private String application_type;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE")
	private Date updated_date;
	
	@Column(name = "UPDATED_USER")
	private String updated_user;
	
	@Column(name = "EMAIL_STATUS")
	private String email_status;
	
	@Column(name = "UPDATE_LIB_NEWSFEED")
	private String update_lib_newsfeed;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplay_online() {
		return display_online;
	}

	public void setDisplay_online(String display_online) {
		this.display_online = display_online;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getApplication_type() {
		return application_type;
	}

	public void setApplication_type(String application_type) {
		this.application_type = application_type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	public String getUpdated_user() {
		return updated_user;
	}

	public void setUpdated_user(String updated_user) {
		this.updated_user = updated_user;
	}

	public String getEmail_status() {
		return email_status;
	}

	public void setEmail_status(String email_status) {
		this.email_status = email_status;
	}

	public String getUpdate_lib_newsfeed() {
		return update_lib_newsfeed;
	}

	public void setUpdate_lib_newsfeed(String update_lib_newsfeed) {
		this.update_lib_newsfeed = update_lib_newsfeed;
	}

}
