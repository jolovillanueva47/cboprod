package org.cambridge.ebooks.online.jpa.countrystates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Query;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.jpa.organisation.Organisation;
import org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap;
import org.cambridge.ebooks.online.util.PersistenceUtil;

/**
 * 
 * @author rvillamor
 *
 */


@NamedNativeQueries ( { 
	@NamedNativeQuery(	
		name = Country.GET_COUNTRY_LIST,
		query = " select country_id, description, currency from country order by description asc",
		resultSetMapping = "countryListResult"
	),	
	@NamedNativeQuery(	
		name = Country.GET_COUNTRY_CODE,
		query = 
			"select C.COUNTRY_ID, decode(C.CURRENCY, 'usd', 'us', 'uk') DESCRIPTION " +
			"from ORACJOC.ADDRESS ADDR, ORACJOC.COUNTRY C, " +
			      "(select ADDRESS " +
			      "from ORACJOC.ORGANISATION O " +
			      "where O.BODY_ID = ?1 " + 
			      "union " +
			      "select ADDRESS " +
			      "from ORACJOC.CONSORTIUM C " +
			      "where C.BODY_ID = ?1) ORG " +
			"where ADDR.COUNTRY = C.COUNTRY_ID " +
			"and ORG.ADDRESS = ADDR.ADDRESS_ID",
		resultSetMapping = "countryListResult"
	)
})




@SqlResultSetMapping (
		name = "countryListResult",
		entities = {
			@EntityResult (
				entityClass = Country.class,
				fields = {
					@FieldResult ( name = "countryId", 		column = "COUNTRY_ID"),
					@FieldResult ( name = "countryName", 	column = "DESCRIPTION"),
					@FieldResult ( name = "currency", 		column = "CURRENCY")
				}
			) 
		}
)

@Entity
@Table(name = "COUNTRY")
public class Country {
	static Logger logger = Logger.getLogger(Country.class);
	
	public static final String UK = "17";
	public static final String US = "226";
	public static final String GET_COUNTRY_LIST = "getCountryListSQL"; 
	
	public static final String GET_COUNTRY_CODE = "getCountryCode";
												    
	@Id
	@Column( name = "COUNTRY_ID")
	private String countryId;
	
	@Column( name = "DESCRIPTION")
	private String countryName;
	
	@Column( name = "CURRENCY")
	private String currency;
	
	public boolean equals(Object o) { 
		if ( o instanceof Country ) { 
			Country c = (Country) o;
			return c.getCountryId().equals(this.getCountryId());
		} else {
			return false;
		}
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	private static EntityManagerFactory emf = PersistenceUtil.emf;
	
	public static final String US_FULL = "us";
	
	public static final String UK_FULL = "uk";
	
	
	/**
	 * gets the country (us, or uk only) given the orgBodyId
	 * @param request
	 * @return
	 */
	public static final String getCountry(HttpServletRequest request) {
		
		String bodyId = getOrgBodyId(request); 
		if(StringUtils.isEmpty(bodyId)){
			return US_FULL;
		}else{		
			EntityManager em = emf.createEntityManager();
		
			Country country = null;
			try {
				Query query = em.createNamedQuery(GET_COUNTRY_CODE);
				
				query.setParameter(1, bodyId);
				
				country = (Country) query.getSingleResult();
				
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			} finally {
				em.close();
			}
			
			return country.getCountryName();
		
		}
		
		
	}
	
	
	
	private static String getOrgBodyId(HttpServletRequest request){
		HttpSession session = request.getSession();
				
		Organisation org = OrgConLinkedMap.getFromSession(session).getFirst();
		
		if(org != null){
			return org.getBodyId();
		}
		return null;
		
	}
	
	

}


