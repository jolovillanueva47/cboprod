package org.cambridge.ebooks.online.jpa.url_resolver;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "URL_RESOLVER")
public class OpenUrlResolver {
	
	@Id
	@Column( name = "BODY_ID")
	private String bodyId;
	
	@Column( name = "URL_PATH") 
	private String urlResolverPath;
	
	
	public String getBodyId() {
		return bodyId;
	}

	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	public String getUrlResolverPath() {
		return urlResolverPath;
	}

	public void setUrlResolverPath(String urlResolver) {
		this.urlResolverPath = urlResolver;
	}
	
	
	

}
