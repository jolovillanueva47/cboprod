package org.cambridge.ebooks.online.jpa.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = BladeMember.BLADE_MEMBER_SELECT,
		query = "SELECT e.member_id, e.member_fullname, e.member_email_1 " +
				"FROM ADMIN ADMIN, BLADE_MEMBER e, ORGANISATION o " +
				"WHERE e.member_id = ADMIN.blade_member_id " +
					"AND ADMIN.ORGANISATION_ID = o.BODY_ID " +
					"AND ADMIN.ORGANISATION_ID IN (?1) " +
					"AND ADMIN.DISPLAY_ADMIN = 'true' " +
					"UNION ALL " +
					"SELECT e.member_id, e.member_fullname, e.member_email_1 " +
					"FROM ADMIN ADMIN, BLADE_MEMBER e, CONSORTIUM c " +
					"WHERE e.member_id = ADMIN.blade_member_id " +
					"AND ADMIN.ORGANISATION_ID = c.BODY_ID " +
					"AND ADMIN.ORGANISATION_ID IN (?2) " +
					"AND ADMIN.DISPLAY_ADMIN = 'true'",
		resultSetMapping = BladeMember.BLADE_MEMBER_RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = BladeMember.FIND_BLADE,
		query = "select MEMBER_ID, MEMBER_FULLNAME, MEMBER_EMAIL_1 from ORACJOC.BLADE_MEMBER where MEMBER_ID = ?1",
		resultSetMapping = BladeMember.BLADE_MEMBER_RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = BladeMember.BLADE_MEMBER_RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = BladeMember.class,
				fields = {
					@FieldResult(name = "memberId", column = "MEMBER_ID"),
					@FieldResult(name = "memberFullname", column = "MEMBER_FULLNAME"),
					@FieldResult(name = "memberEmail", column = "MEMBER_EMAIL_1")
				}
			)	
		}
	)
})

@Entity
@Table(name = "EBOOK_SERIES_LOAD")
public class BladeMember {

	public static final String BLADE_MEMBER_SELECT = "searchBladeMember";
	public static final String BLADE_MEMBER_RESULT_SET_MAPPING = "findBladeMemberResult";
	public static final String FIND_BLADE = "FIND_BLADE";
	
	@Id
	@Column( name = "MEMBER_ID")
	private String memberId;
	
	@Column( name = "MEMBER_FULLNAME")
	private String memberFullname;
	
	@Column( name = "MEMBER_EMAIL_1")
	private String memberEmail;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberFullname() {
		return memberFullname;
	}

	public void setMemberFullname(String memberFullname) {
		this.memberFullname = memberFullname;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	
}
