package org.cambridge.ebooks.online.jpa.faq;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 * @author Karlson A. Mulingtapang
 * FAQSubjectAreaType.java - FAQ_SUBJECT_AREA_TYPE synonymous with CJO
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = FaqSubjectAreaTypeBooks.SEARCH_ALL,
		query = "Select * from FAQ_SUBJECT_AREA_TYPE_BOOKS ORDER BY DISPLAY_ORDER",
		resultSetMapping = FaqSubjectAreaTypeBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = FaqSubjectAreaTypeBooks.SEARCH_BY_ID,
		query = "Select * from FAQ_SUBJECT_AREA_TYPE_BOOKS where ID = ?1",
		resultSetMapping = FaqSubjectAreaTypeBooks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = FaqSubjectAreaTypeBooks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = FaqSubjectAreaTypeBooks.class,
				fields = {
					@FieldResult(name = "id", column = "ID"),
					@FieldResult(name = "subjectAreaType", column = "SUBJECT_AREA_TYPE"),
					@FieldResult(name = "subjectAreaDescription", column = "SUBJECT_AREA_DESCRIPTION"),
					@FieldResult(name = "displayOrder", column = "DISPLAY_ORDER"),
					@FieldResult(name = "enable", column = "ENABLE"),
					@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
					@FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
					@FieldResult(name = "languageCode", column = "LANGUAGE_CODE")
					
				}
			)	
		}
	)
})

@Entity
@Table(name = "FAQ_SUBJECT_AREA_TYPE_BOOKS")
public class FaqSubjectAreaTypeBooks {
	static Logger logger = Logger.getLogger(FaqSubjectAreaTypeBooks.class);
	
	public static final String RESULT_SET_MAPPING = "FAQ_SUBJECT_AREA_TYPE_BOOKS.result";
	public static final String SEARCH_ALL = "FAQ_SUBJECT_AREA_TYPE_BOOKS.searchAll";
	public static final String SEARCH_BY_ID = "FAQ_SUBJECT_AREA_TYPE_BOOKS.searchById";
	
	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "SUBJECT_AREA_TYPE")
	private String subjectAreaType;
	
	@Column(name = "SUBJECT_AREA_DESCRIPTION")
	private String subjectAreaDescription;
	
	@Column(name = "DISPLAY_ORDER")
	private int displayOrder;
	
	@Column(name = "ENABLE")
	private String enable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "LANGUAGE_CODE")
	private int languageCode;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the subjectAreaType
	 */
	public String getSubjectAreaType() {
		return subjectAreaType;
	}
	/**
	 * @param subjectAreaType the subjectAreaType to set
	 */
	public void setSubjectAreaType(String subjectAreaType) {
		this.subjectAreaType = subjectAreaType;
	}
	/**
	 * @return the subjectAreaDescription
	 */
	public String getSubjectAreaDescription() {
		return subjectAreaDescription;
	}
	/**
	 * @param subjectAreaDescription the subjectAreaDescription to set
	 */
	public void setSubjectAreaDescription(String subjectAreaDescription) {
		this.subjectAreaDescription = subjectAreaDescription;
	}
	public String getEscapedSubjectAreaDescription() {
		
		return this.subjectAreaDescription != null ? StringEscapeUtils.escapeXml(this.subjectAreaDescription) : this.subjectAreaDescription;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setLanguageCode(int languageCode) {
		this.languageCode = languageCode;
	}
	public int getLanguageCode() {
		logger.info("=== Return the languageCode ===");
		return languageCode;
	}
	
}
