package org.cambridge.ebooks.online.jpa.consortium;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.cambridge.ebooks.online.jpa.organisation.OrganisationBase;


/**
 * 
 * @author cemanalo
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = ConsortiumOrganisation.GET_CONSORTIA,
		
		query = 
			"select ORGANISATION_ID ,CONSORTIUM_ID, OVERRIDE_CONSORTIA_LOGO, OVERRIDE_CONSORTIA_LOGO_NAME "+
			"from ORACJOC.CONSORTIUM_ORGANISATION where ORGANISATION_ID = ?1 "+
			"AND CONSORTIUM_ID = ?2",
		resultClass = ConsortiumOrganisation.class
	),
	@NamedNativeQuery(
		name = ConsortiumOrganisation.SELECT_BY_CONS_ID,		
		query = 
			"select * "+
			"from ORACJOC.CONSORTIUM_ORGANISATION where consortium_id = ?1 ",
		resultClass = ConsortiumOrganisation.class
	)
})

@Entity
@IdClass(ConsortiumOrganisationKey.class)
@Table(name="ORACJOC.CONSORTIUM_ORGANISATION")
public class ConsortiumOrganisation implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String GET_CONSORTIA = "GET_CONSORTIA";
	
	public static final String SELECT_BY_CONS_ID = "ConsortiumOrganisation.SELECT_BY_CONS_ID";
	
	@Id
	@Column(name="ORGANISATION_ID")
	private String organisationId;
	
	@Id
	@Column(name="CONSORTIUM_ID")
	private String consortiumId;
	
	@Column(name="OVERRIDE_CONSORTIA_LOGO")
	private String overrideConsortiaLogo;
	
	@Column(name="OVERRIDE_CONSORTIA_LOGO_NAME")
	private String overrideConsortiaLogoName;

	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ORGANISATION_ID", referencedColumnName="BODY_ID", insertable=false, updatable=false)	
	private OrganisationBase organisation;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CONSORTIUM_ID", referencedColumnName="BODY_ID", insertable=false, updatable=false)	
	private Consortium consortium;
	
	
	public String getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}

	public String getConsortiumId() {
		return consortiumId;
	}

	public void setConsortiumId(String consortiumId) {
		this.consortiumId = consortiumId;
	}

	public String getOverrideConsortiaLogo() {
		return overrideConsortiaLogo;
	}

	public void setOverrideConsortiaLogo(String overrideConsortiaLogo) {
		this.overrideConsortiaLogo = overrideConsortiaLogo;
	}
	
	public String getOverrideConsortiaLogoName() {
		return overrideConsortiaLogoName;
	}

	public void setOverrideConsortiaLogoName(String overrideConsortiaLogoName) {
		this.overrideConsortiaLogoName = overrideConsortiaLogoName;
	}

	public OrganisationBase getOrganisation() {
		return organisation;
	}

	public void setOrganisation(OrganisationBase organisation) {
		this.organisation = organisation;
	}

	public Consortium getConsortium() {
		return consortium;
	}

	public void setConsortium(Consortium consortium) {
		this.consortium = consortium;
	}
	
}


