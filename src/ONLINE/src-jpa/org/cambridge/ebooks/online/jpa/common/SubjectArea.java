package org.cambridge.ebooks.online.jpa.common;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "EBOOK_SUBJECT_HIERARCHY")
public class SubjectArea {
		
	public SubjectArea() {}
	public SubjectArea(String subjectId) {
		this.subjectId = subjectId;
	}
	
	@Id
	@Column(name = "SUBJECT_ID")
	private String subjectId;
	
	@Column(name = "SUBJECT_NAME")
	private String subjectName;

	@Column(name = "SUBJECT_LEVEL")
	private int subjectLevel;

	@Column(name = "PARENT_ID")
	private String parentId;

	@Column(name = "VISTA_SUBJECT_CODE")
	private String vistaSubjectCode;

	@Column(name = "FINAL_LEVEL")
	private String finalLevel;

	@Column(name = "TITLE_COUNT")
	private int titleCount;

	@Column(name = "ACTIVE_SUBJECT")
	private String activeSubject;

	@Column(name = "SERIES_COUNT")
	private int seriesCount;
	
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((subjectId == null) ? 0 : subjectId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectArea other = (SubjectArea) obj;
		if (subjectId == null) {
			if (other.subjectId != null)
				return false;
		} else if (!subjectId.equalsIgnoreCase(other.subjectId))
			return false;
		return true;
	}

	public int getSeriesCount() {
		return seriesCount;
	}
	public void setSeriesCount(int seriesCount) {
		this.seriesCount = seriesCount;
	}	

	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public int getSubjectLevel() {
		return subjectLevel;
	}
	public void setSubjectLevel(int subjectLevel) {
		this.subjectLevel = subjectLevel;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getVistaSubjectCode() {
		return vistaSubjectCode;
	}
	public void setVistaSubjectCode(String vistaSubjectCode) {
		this.vistaSubjectCode = vistaSubjectCode;
	}
	public String getFinalLevel() {
		return finalLevel;
	}
	public void setFinalLevel(String finalLevel) {
		this.finalLevel = finalLevel;
	}
	public int getTitleCount() {
		return titleCount;
	}
	public void setTitleCount(int titleCount) {
		this.titleCount = titleCount;
	}
	public String getActiveSubject() {
		return activeSubject;
	}
	public void setActiveSubject(String activeSubject) {
		this.activeSubject = activeSubject;
	}
	
	
}
