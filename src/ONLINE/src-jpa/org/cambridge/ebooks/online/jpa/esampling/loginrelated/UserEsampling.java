package org.cambridge.ebooks.online.jpa.esampling.loginrelated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

/**
 * 
 * @author ivreyes
 * 
 *
 */

@NamedNativeQueries({
	@NamedNativeQuery(
	        name = UserEsampling.SEARCH_MAXI_CONTACTS_BY_USERNAME, 
	        query = " select user_id, forename, surname, institution, " +
	        		" department, address_1, address_2, city, state, " +
	        		" country, post_code,  email_address , user_password" +
	        		" from ESAMPLE_USER " +
	        		" where email_address = ?1  " ,
	        		
	        resultSetMapping = "searchMaxiContactsResult"
	)
	
})

@SqlResultSetMapping(
    name = "searchMaxiContactsResult", 
    entities = { 
        @EntityResult( 
            entityClass = UserEsampling.class, 
            fields = {
                @FieldResult(name = "user_id", 	column = "USER_ID"),
                @FieldResult(name = "firstname", 	column = "FORENAME"),
                @FieldResult(name = "surname", column = "SURNAME"),
                @FieldResult(name = "institution", 	column = "INSTITUTION"),
                @FieldResult(name = "department", column = "DEPARTMENT"),
                @FieldResult(name = "address1", column = "ADDRESS_1"),
                @FieldResult(name = "address2", column = "ADDRESS_2"),
                @FieldResult(name = "city", column = "CITY"),
                @FieldResult(name = "state", column = "STATE"),
                @FieldResult(name = "country", column = "COUNTRY"),
                @FieldResult(name = "post_code", column = "POST_CODE"),
                @FieldResult(name = "email_address", column = "EMAIL_ADDRESS"),
                @FieldResult(name = "user_password", column = "USER_PASSWORD")
            }
        ) 
    }
)

@Entity
@Table(name = "ESAMPLE_USER")
public class UserEsampling {

	public static final String SEARCH_MAXI_CONTACTS_BY_USERNAME = "searchMaxiContactsByUsername";
	@Id
	@Column( name = "USER_ID")
	private String user_id;



	@Column( name = "FORENAME")
	private String firstname;

	@Column( name = "SURNAME")
	private String surname;

	@Column( name = "INSTITUTION")
	private String institution;

	@Column( name = "DEPARTMENT")
	private String department;

	@Column( name = "ADDRESS_1")
	private String address1;

	@Column( name = "ADDRESS_2")
	private String address2;

	@Column( name = "CITY")
	private String city;

	@Column( name = "STATE")
	private String state;

	@Column( name = "COUNTRY")
	private String country;

	@Column( name = "POST_CODE")
	private String postal;

	@Column( name = "EMAIL_ADDRESS")
	private String email_address;
	
	@Column( name = "USER_PASSWORD")
	private String user_password;
	
	




	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	

	

	

}
