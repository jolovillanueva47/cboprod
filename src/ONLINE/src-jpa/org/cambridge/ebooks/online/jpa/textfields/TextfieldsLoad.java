package org.cambridge.ebooks.online.jpa.textfields;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = TextfieldsLoad.SEARCH_BY_ISBN,
		query = "SELECT * FROM ebook_textfields_load WHERE isbn = ?1",
		resultSetMapping = TextfieldsLoad.RESULT_SET_MAPPING
	)
})
@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = TextfieldsLoad.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = TextfieldsLoad.class,
				fields = {
					@FieldResult(name = "titleCode", column = "TITLE_CODE"),
					@FieldResult(name = "isbn", column = "ISBN"),
					@FieldResult(name = "blurb", column = "BLURB"),
					@FieldResult(name = "review1", column = "REVIEW1"),
					@FieldResult(name = "review2", column = "REVIEW2"),
					@FieldResult(name = "review3", column = "REVIEW3")
				}
			)
		}
	)
})

@Entity
@Table(name = "EBOOK_TEXTFIELDS_LOAD")
public class TextfieldsLoad {
	
	public static final String RESULT_SET_MAPPING = "textfieldsLoadResultSetMapping";
	public static final String SEARCH_BY_ISBN = "textfieldsLoadSearchByIsbn";

	@Id
	@Column( name = "TITLE_CODE")
	private int titleCode;

	@Column( name = "ISBN")
	private String isbn;

	@Column( name = "BLURB")
	private String blurb;
	
	@Column( name = "REVIEW1")
	private String review1;
	
	@Column( name = "REVIEW2")
	private String review2;

	@Column( name = "REVIEW3")
	private String review3;

	public int getTitleCode() {
		return titleCode;
	}
	public void setTitleCode(int titleCode) {
		this.titleCode = titleCode;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getBlurb() {
		return blurb;
	}
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
	public String getReview1() {
		return review1;
	}
	public void setReview1(String review1) {
		this.review1 = review1;
	}
	public String getReview2() {
		return review2;
	}
	public void setReview2(String review2) {
		this.review2 = review2;
	}
	public String getReview3() {
		return review3;
	}
	public void setReview3(String review3) {
		this.review3 = review3;
	}
	
	
	
}
