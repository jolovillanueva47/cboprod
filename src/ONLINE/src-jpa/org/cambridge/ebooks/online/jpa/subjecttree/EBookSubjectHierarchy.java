package org.cambridge.ebooks.online.jpa.subjecttree;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = EBookSubjectHierarchy.SEARCH_ALL,
		query = "SELECT * FROM" +
					" (SELECT path, subject_id, COUNT(*)-1 AS \"SERIES_COUNT\" FROM" +
						" (SELECT" +
							" distinct SYS_CONNECT_BY_PATH(hie.subject_id,';') path," +
							" hie.subject_id," +
							" hie.subject_name," +
							" hie.subject_level," +
							" hie.parent_id," +
							" hie.vista_subject_code," +
							" hie.final_level," +
							" hie.title_count," +
							" hie.active_subject," +
							" load.series_code" +
						" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn" +
						" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook_isbn_data_load load" +
						" ON load.isbn = isbn.isbn" +
						" WHERE active_subject = 'Y'" +
						" CONNECT BY PRIOR hie.subject_id = hie.parent_id START WITH hie.subject_level = 1)" +
					" GROUP BY path, subject_id) res," +
					" ebook_subject_hierarchy esh" +
					" WHERE res.subject_id = esh.subject_id" +
					" ORDER BY esh.subject_level, esh.parent_id, esh.subject_id",
		resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = EBookSubjectHierarchy.SEARCH_BY_PARENT,
		query = "SELECT * FROM ebook_subject_hierarchy WHERE parent_id = ?1 AND active_subject = 'Y' ORDER BY subject_id",
		resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = EBookSubjectHierarchy.SERIES_SEARCH_BY_PARENT,
		query = "SELECT * FROM" +
					" (SELECT root_id AS \"SUBJECT_ID\", COUNT(*) AS \"SERIES_COUNT\" FROM" +
						" (SELECT" +
						" CONNECT_BY_ROOT hie.subject_id ROOT_ID," +
						" hie.subject_id," +
						" hie.subject_name," +
						" hie.subject_level," +
						" hie.parent_id," +
						" hie.vista_subject_code," +
						" hie.final_level," +
						" hie.title_count," +
						" hie.active_subject," +
						" isbn.isbn," +
						" eb.series_code" +
					" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn" +
					" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook eb" +
					" ON eb.isbn = isbn.isbn" +
					" CONNECT BY PRIOR hie.subject_id = hie.parent_id" +
					" START WITH hie.parent_id = ?1)" +
					" WHERE series_code IS NOT NULL" +
				" GROUP BY root_id) res," +
				" ebook_subject_hierarchy esh" +
				" WHERE res.subject_id = esh.subject_id",
		resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SUBJECT,
			query = //" SELECT DISTINCT * FROM" +
						"(SELECT DISTINCT vista_subject_code AS \"SUBJECT_ID\" FROM" +
							" (SELECT" +
							" CONNECT_BY_ROOT hie.subject_id ROOT_ID," +
							" hie.subject_id," +
							" hie.subject_name," +
							" hie.subject_level," +
							" hie.parent_id," +
							" hie.vista_subject_code," +
							" hie.final_level," +
							" hie.title_count," +
							" hie.active_subject," +
							" isbn.isbn," +
							" eb.series_code" +
						" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn" +
						" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook eb" +
						" ON eb.isbn = isbn.isbn" +
						" CONNECT BY PRIOR hie.subject_id = hie.parent_id" +
						" START WITH hie.parent_id = ?1)" +
						" WHERE vista_subject_code IS NOT NULL AND isbn IS NOT NULL)" +
						" ORDER BY vista_subject_code", 

		resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING_LEAF
	),
	@NamedNativeQuery(
		name = EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES,
			query = //" SELECT DISTINCT * FROM" +
						"(SELECT DISTINCT series_code AS \"SUBJECT_ID\" FROM" +
							" (SELECT" +
							" CONNECT_BY_ROOT hie.subject_id ROOT_ID," +
							" hie.subject_id," +
							" hie.subject_name," +
							" hie.subject_level," +
							" hie.parent_id," +
							" hie.vista_subject_code," +
							" hie.final_level," +
							" hie.title_count," +
							" hie.active_subject," +
							" isbn.isbn," +
							" eb.series_code" +
						" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn" +
						" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook eb" +
						" ON eb.isbn = isbn.isbn" +
						" CONNECT BY PRIOR hie.subject_id = hie.parent_id" +
						" START WITH hie.parent_id = ?1)" +
						" WHERE SERIES_CODE IS NOT NULL)" +
						" ORDER BY SERIES_CODE", 

		resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING_LEAF
	),
	@NamedNativeQuery(
			name = EBookSubjectHierarchy.SEARCH_ALL_LEAVES_SERIES_SUBLEVEL,
				query = //" SELECT DISTINCT * FROM" +
							"(SELECT DISTINCT series_code AS \"SUBJECT_ID\" FROM" +
								" (SELECT" +
								" CONNECT_BY_ROOT hie.subject_id ROOT_ID," +
								" hie.subject_id," +
								" hie.subject_name," +
								" hie.subject_level," +
								" hie.parent_id," +
								" hie.vista_subject_code," +
								" hie.final_level," +
								" hie.title_count," +
								" hie.active_subject," +
								" isbn.isbn," +
								" eb.series_code" +
							" FROM (ebook_subject_hierarchy hie LEFT JOIN ebook_subject_isbn isbn" +
							" ON hie.subject_id = isbn.subject_id) LEFT JOIN ebook eb" +
							" ON eb.isbn = isbn.isbn" +
							" CONNECT BY PRIOR hie.subject_id = hie.parent_id" +
							" START WITH hie.subject_id = ?1)" +
							" WHERE SERIES_CODE IS NOT NULL)" +
							" ORDER BY SERIES_CODE", 

			resultSetMapping = EBookSubjectHierarchy.RESULT_SET_MAPPING_LEAF
		)
})
@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = EBookSubjectHierarchy.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = EBookSubjectHierarchy.class,
				fields = {
					@FieldResult(name = "subjectId", column = "SUBJECT_ID"),
					@FieldResult(name = "subjectName", column = "SUBJECT_NAME"),
					@FieldResult(name = "subjectLevel", column = "SUBJECT_LEVEL"),
					@FieldResult(name = "parentId", column = "PARENT_ID"),
					@FieldResult(name = "vistaSubjectCode", column = "VISTA_SUBJECT_CODE"),
					@FieldResult(name = "finalLevel", column = "FINAL_LEVEL"),
					@FieldResult(name = "titleCount", column = "TITLE_COUNT"),
					@FieldResult(name = "activeSubject", column = "ACTIVE_SUBJECT"),
					@FieldResult(name = "path", column = "PATH"),
					@FieldResult(name = "seriesCount", column = "SERIES_COUNT")
				}
			)
		}
	),
	@SqlResultSetMapping(
			name = EBookSubjectHierarchy.RESULT_SET_MAPPING_LEAF,
			entities = {
				@EntityResult (
					entityClass = EBookSubjectHierarchy.class,
					fields = {
						@FieldResult(name = "subjectId", column = "SUBJECT_ID"),
					}
				)
			}
		)
})

@Entity
@Table(name = "EBOOK_SUBJECT_HIERARCHY")
public class EBookSubjectHierarchy {
	
	public static final String RESULT_SET_MAPPING = "eBookSubjectHierarchyResultSetMapping";
	public static final String RESULT_SET_MAPPING_LEAF = "eBookSubjectSearchAllLeaves";
	public static final String SEARCH_ALL = "eBookSubjectSearchAll";
	public static final String SEARCH_ALL_LEAVES_SUBJECT = "eBookSubjectSearchAllLeaves";
	public static final String SEARCH_ALL_LEAVES_SERIES = "eBookSeriesSearchAllLeaves";
	public static final String SEARCH_ALL_LEAVES_SERIES_SUBLEVEL = "eBookSeriesSearchSubLevels";
	public static final String SEARCH_BY_PARENT = "eBookSubjectSearchByParent";
	public static final String SERIES_SEARCH_BY_PARENT = "eBookSeriesSearchByParent";
	
	public EBookSubjectHierarchy() {}
	public EBookSubjectHierarchy(String subjectId) {
		super();
		this.subjectId = subjectId;
	}
	
	@Id
	@Column(name = "SUBJECT_ID")
	private String subjectId;
	
	@Column(name = "SUBJECT_NAME")
	private String subjectName;

	@Column(name = "SUBJECT_LEVEL")
	private int subjectLevel;

	@Column(name = "PARENT_ID")
	private String parentId;

	@Column(name = "VISTA_SUBJECT_CODE")
	private String vistaSubjectCode;

	@Column(name = "FINAL_LEVEL")
	private String finalLevel;

	@Column(name = "TITLE_COUNT")
	private int titleCount;

	@Column(name = "ACTIVE_SUBJECT")
	private String activeSubject;
	
	@Column(name = "PATH")
	private String path;
	
	@Column(name = "SERIES_COUNT")
	private int seriesCount;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((subjectId == null) ? 0 : subjectId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EBookSubjectHierarchy other = (EBookSubjectHierarchy) obj;
		if (subjectId == null) {
			if (other.subjectId != null)
				return false;
		} else if (!subjectId.equalsIgnoreCase(other.subjectId))
			return false;
		return true;
	}

	public int getSeriesCount() {
		return seriesCount;
	}
	public void setSeriesCount(int seriesCount) {
		this.seriesCount = seriesCount;
	}	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public int getSubjectLevel() {
		return subjectLevel;
	}
	public void setSubjectLevel(int subjectLevel) {
		this.subjectLevel = subjectLevel;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getVistaSubjectCode() {
		return vistaSubjectCode;
	}
	public void setVistaSubjectCode(String vistaSubjectCode) {
		this.vistaSubjectCode = vistaSubjectCode;
	}
	public String getFinalLevel() {
		return finalLevel;
	}
	public void setFinalLevel(String finalLevel) {
		this.finalLevel = finalLevel;
	}
	public int getTitleCount() {
		return titleCount;
	}
	public void setTitleCount(int titleCount) {
		this.titleCount = titleCount;
	}
	public String getActiveSubject() {
		return activeSubject;
	}
	public void setActiveSubject(String activeSubject) {
		this.activeSubject = activeSubject;
	}
	
	
	
	
}
