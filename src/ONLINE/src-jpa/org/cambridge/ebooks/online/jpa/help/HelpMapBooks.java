package org.cambridge.ebooks.online.jpa.help;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

/**
 * @author Karlson A. Mulingtapang
 * HelpMapBooks.java - HELP_MAP_BOOKS synonymous with CJOC 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = HelpMapBooks.SEARCH_ALL,
		query = "Select * from HELP_MAP_BOOKS",
		resultSetMapping = HelpMapBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = HelpMapBooks.SEARCH_BY_PAGE_ID,
		query = "Select * from HELP_MAP_BOOKS where page_id = ?1",
		resultSetMapping = HelpMapBooks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = HelpMapBooks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult(
				entityClass = HelpMapBooks.class,
				fields = {
					@FieldResult(name = "pageId", column = "PAGE_ID"),
					@FieldResult(name = "relatedPageId", column = "RELATED_PAGE_ID")
				}
			)
		}
	)
})

@Entity
@Table(name = "HELP_MAP_BOOKS")
public class HelpMapBooks {
	
	public static final String RESULT_SET_MAPPING = "HELP_MAP_BOOKS.result";
	public static final String SEARCH_ALL = "HELP_MAP_BOOKS.searchAll";
	public static final String SEARCH_BY_PAGE_ID = "HELP_MAP_BOOKS.searchByPageId";
	
	@Id
	@Column(name = "PAGE_ID")
	private int pageId;	
	
	@Column(name = "RELATED_PAGE_ID")
	private int relatedPageId;
	
	/**
	 * @return the pageId
	 */
	public int getPageId() {
		return pageId;
	}
	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	/**
	 * @return the relatedPageId
	 */
	public int getRelatedPageId() {
		return relatedPageId;
	}
	/**
	 * @param relatedPageId the relatedPageId to set
	 */
	public void setRelatedPageId(int relatedPageId) {
		this.relatedPageId = relatedPageId;
	}	
}
