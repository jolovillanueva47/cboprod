package org.cambridge.ebooks.online.jpa.order;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@IdClass(EBookAccessAgreementKey.class)
@Table ( name = EBookAccessAgreement.TABLE_NAME )
public class EBookAccessAgreement {
	
	public static final String TABLE_NAME = "EBOOK_ACCESS_AGREEMENT";
	
	@Id	
	@Column(name="ORDER_ID")
	private long orderId;
	 
	@Id
	@Column(name="BODY_ID")
	private long bodyId;
	 
	@Column(name="ACCESS_CONFIRMEE_TITLE")
	private String accessConfirmeeTitle;
	 
	@Column(name="ACCESS_CONFIRMEE_NAME")	 
	private String accessConfirmeeName;
	 
	@Column(name="ACCESS_CONFIRMEE_TIMESTAMP")
	private Timestamp accessConfirmeeTimestamp;	 
	
	@Column(name="ACCESS_CONFIRMEE_EMAIL")
	private String acccessConfirmeeEmail;	
	
	@Column(name="MEMBER_ID")
	private long memberId;
	
	
	 
	@Transient
	private boolean error;
	 

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getAccessConfirmeeTitle() {
		return accessConfirmeeTitle;
	}

	public void setAccessConfirmeeTitle(String accessConfirmeeTitle) {
		this.accessConfirmeeTitle = accessConfirmeeTitle;
	}

	public String getAccessConfirmeeName() {
		return accessConfirmeeName;
	}

	public void setAccessConfirmeeName(String accessConfirmeeName) {
		this.accessConfirmeeName = accessConfirmeeName;
	}

	public Timestamp getAccessConfirmeeTimestamp() {
		return accessConfirmeeTimestamp;
	}

	public void setAccessConfirmeeTimestamp(Timestamp accessConfirmeeTimestamp) {
		this.accessConfirmeeTimestamp = accessConfirmeeTimestamp;
	}
	
	public long getBodyId() {
		return bodyId;
	}

	public void setBodyId(long bodyId) {
		this.bodyId = bodyId;
	}

	public long getMemberId() {
		return memberId;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getAcccessConfirmeeEmail() {
		return acccessConfirmeeEmail;
	}

	public void setAcccessConfirmeeEmail(String acccessConfirmeeEmail) {
		this.acccessConfirmeeEmail = acccessConfirmeeEmail;
	}
	
	
	 
	 
}
