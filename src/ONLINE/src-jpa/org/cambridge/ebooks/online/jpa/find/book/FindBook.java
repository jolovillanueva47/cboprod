package org.cambridge.ebooks.online.jpa.find.book;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = FindBook.SEARCH_BOOK_LOCATION,
		query = "SELECT * FROM find_book WHERE body_id = ?1 AND eisbn = ?2",
		resultSetMapping = FindBook.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = FindBook.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = FindBook.class,
				fields = {
					@FieldResult(name = "findBookId", column = "FIND_BOOK_ID"),
					@FieldResult(name = "eisbn", column = "EISBN"),
					@FieldResult(name = "bodyId", column = "BODY_ID"),
					@FieldResult(name = "location", column = "LOCATION")
				}
			)	
		}
	)
})

@Entity
@Table(name = "FIND_BOOK")
public class FindBook {
	
	public static final String SEARCH_BOOK_LOCATION = "searchBookLocation";
	public static final String RESULT_SET_MAPPING = "findBookResult";
	
	@Id
	@GeneratedValue(generator="FIND_BOOK_SEQ")
    @SequenceGenerator(name="FIND_BOOK_SEQ", sequenceName="FIND_BOOK_SEQ", allocationSize=1)
	@Column( name = "FIND_BOOK_ID")
	private String findBookId;
	
	@Column( name = "BODY_ID")
	private String bodyId;
	
	@Column( name = "EISBN")
	private String eisbn;
	
	@Column( name = "LOCATION")
	private String location;
	
	public String getFindBookId() {
		return findBookId;
	}
	public void setFindBookId(String findBookId) {
		this.findBookId = findBookId;
	}
	public String getBodyId() {
		return bodyId;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
	public String getEisbn() {
		return eisbn;
	}
	public void setEisbn(String eisbn) {
		this.eisbn = eisbn;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
