package org.cambridge.ebooks.online.jpa.topbooks;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = EBookTopSeries.SEARCH_TOP_SERIES_TABLE,
			query = "SELECT SERIES_CODE, SERIES_LEGEND, ISBN, SERIES_NUMBER FROM top_series ORDER BY CNT desc",
			resultSetMapping = EBookTopSeries.TOP_SERIES_RESULT_SET
		),
	@NamedNativeQuery(
		name = EBookTopSeries.SEARCH_TOP_SERIES,
		query = "SELECT SERIES_CODE, SERIES_LEGEND, ISBN, CNT FROM top_series_view",
		resultSetMapping = EBookTopSeries.TOP_SERIES_RESULT_SET
	),
	@NamedNativeQuery(
		name = EBookTopSeries.SEARCH_TOP_SERIES_CLC,
		query =  "select d.series_code, sl.series_legend, null ISBN " +
				"from cbo_session_evnt e, ebook_isbn_data_load d, ebook_series_load sl " +
				"where e.event_str1 (+) = d.isbn " +
	    		"and d.cbo_product_group = ?1 " +
	    		"and d.series_code = sl.series_code " +
	    		"and e.event_no = 2 " +
				"group by d.series_code, sl.series_Legend " +  
				"order by count(1) desc",
		resultSetMapping = EBookTopSeries.TOP_SERIES_RESULT_SET
	),
	@NamedNativeQuery(
			name = EBookTopSeries.ALL_SERIES_CLC,
			query = "select distinct dl.series_code, sl.series_legend " +
					"from ebook_isbn_data_load dl, ebook_series_load sl, ebook eb " +
					"where cbo_product_group = ?1 " +
					  "and dl.series_code = sl.series_code " + 
					  "and eb.isbn = dl.isbn " +
					  "and eb.status = 7 " +
					  "and dl.online_flag = 'Y' ",
			resultSetMapping = EBookTopSeries.TOP_SERIES_RESULT_SET
		)		
})

@SqlResultSetMapping( 
	name = EBookTopSeries.TOP_SERIES_RESULT_SET,
	entities = {
		@EntityResult (
			entityClass = EBookTopSeries.class,
			fields = {
				@FieldResult(name = "seriesCode",   column = "SERIES_CODE"),
				@FieldResult(name = "seriesLegend",	column = "SERIES_LEGEND"),				
				@FieldResult(name = "isbn",      	column = "ISBN"),
				@FieldResult(name = "seriesNumber", column = "SERIES_NUMBER")
			}
		)
	}
 )

@Entity
@Table(name = "TOP_SERIES")
public class EBookTopSeries {

	public static final String SEARCH_TOP_SERIES 		= "searchTopSeries";	
	public static final String SEARCH_TOP_SERIES_TABLE  = "searchTopSeriesTable";
	public static final String TOP_SERIES_RESULT_SET 	= "topSeriesResult";
	
	public static final String SEARCH_TOP_SERIES_CLC 	= "searchTopSeriesCLC";
	public static final String ALL_SERIES_CLC 	= "ALL_SERIES_CLC";
	
	
	
	@Id
	@Column( name = "SERIES_CODE")
	private String seriesCode;

	@Column( name = "SERIES_LEGEND")
	private String seriesLegend;
	
	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "CNT")
	private int cnt;
	
	@Column( name = "SERIES_NUMBER")
	private String seriesNumber;
		
	public String getSeriesCode() {
		return seriesCode;
	}

	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}

	public String getSeriesLegend() {
		return seriesLegend;
	}

	public void setSeriesLegend(String seriesLegend) {
		this.seriesLegend = seriesLegend;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	
	
	
	
}
