package org.cambridge.ebooks.online.jpa.internal.content;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




@NamedNativeQueries({	
	@NamedNativeQuery (
			name = EBookContent.SEARCH_SELECTED_ADD_TITLE,
			query = " SELECT * FROM ebook_content " +		 
				 	" where type not in ('image_standard', 'image_thumb', 'xml' ) " +
				 	"   and TITLE is null ",
			resultClass = EBookContent.class
	)
})


@Entity
@Table(name = EBookContent.TABLE_NAME)
public class EBookContent {
	
	public static final String TABLE_NAME = "EBOOK_CONTENT";
	
	public static final String SEARCH_SELECTED_ADD_TITLE = "SEARCH_SELECTED_ADD_TITLE";
	

	@Id
	@Column(name = "CONTENT_ID")
	private String contentId;
	
	@Column(name = "FILENAME")
	private String fileName;
		
	@Column(name = "ISBN")
	private String isbn;
		
	@Column(name = "STATUS")
	private int status;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="EMBARGO_DATE")
    private Date embargoDate;
	
	@Column(name = "REMARKS")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "PAGE_START")
	private String pageStart;
	
	@Column(name = "PAGE_END")
	private String pageEnd;

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getEmbargoDate() {
		return embargoDate;
	}

	public void setEmbargoDate(Date embargoDate) {
		this.embargoDate = embargoDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPageStart() {
		return pageStart;
	}

	public void setPageStart(String pageStart) {
		this.pageStart = pageStart;
	}

	public String getPageEnd() {
		return pageEnd;
	}

	public void setPageEnd(String pageEnd) {
		this.pageEnd = pageEnd;
	}
	
	
	
	
}
