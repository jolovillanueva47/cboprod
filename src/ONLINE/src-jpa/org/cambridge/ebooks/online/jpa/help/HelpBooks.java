package org.cambridge.ebooks.online.jpa.help;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author Karlson A. Mulingtapang
 * HelpBooks.java - HELP_BOOKS synonymous with CJOC
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = HelpBooks.SEARCH_ALL,
		query = "Select * from HELP_BOOKS ORDER BY PAGE_NAME",
		resultSetMapping = HelpBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = HelpBooks.SEARCH_BY_ID,
		query = "Select * from HELP_BOOKS where page_id = ?1",
		resultSetMapping = HelpBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = HelpBooks.SEARCH_BY_LANGUAGE_CODE,
		query = "Select * from HELP_BOOKS where page_id = ?1 and language_code = ?2",
		resultSetMapping = HelpBooks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = HelpBooks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult(
				entityClass = HelpBooks.class,
				fields = {
					@FieldResult(name = "pageId", column = "PAGE_ID"),
					@FieldResult(name = "helpFile", column = "HELP_FILE"),
					@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
					@FieldResult(name = "modifiedBy", column = "MODIFIED_BY"),
					@FieldResult(name = "pageName", column = "PAGE_NAME"),
					@FieldResult(name = "notebookEventId", column = "NOTEBOOK_EVENT_ID"),
					@FieldResult(name = "content", column = "CONTENT"),
					@FieldResult(name = "url", column = "URL"),
					@FieldResult(name = "languageCode", column = "LANGUAGE_CODE"),
					@FieldResult(name = "chineseId", column = "CHINESE_ID"),
					@FieldResult(name = "espanolId", column = "ESPANOL_ID"),
					@FieldResult(name = "japaneseId", column = "JAPANESE_ID"),
					@FieldResult(name = "portuguesId", column = "PORTUGUES_ID")
				}
			)
		}
	)
})

@Entity
@Table(name = "HELP_BOOKS")
public class HelpBooks {
	
	public static final String RESULT_SET_MAPPING = "HELP_BOOKS.result";
	public static final String SEARCH_ALL = "HELP_BOOKS.searchAll";
	public static final String SEARCH_BY_ID = "HELP_BOOKS.searchById";
	public static final String SEARCH_BY_LANGUAGE_CODE = "HELP_BOOKS.searchByLanguageCode";
	
	
	@Id
	@Column(name = "PAGE_ID")
	private int pageId;
	
	@Column(name = "HELP_FILE")
	private String helpFile;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "PAGE_NAME")
	private String pageName;
	
	@Column(name = "NOTEBOOK_EVENT_ID")
	private int notebookEventId;
	
	@Column(name = "CONTENT")
	private String content;
	
	@Column(name = "URL")
	private String url;

	@Column(name = "LANGUAGE_CODE")
	private int languageCode;
	
	@Column(name = "CHINESE_ID")
	private int chineseId;
	
	@Column(name = "ESPANOL_ID")
	private int espanolId;
	
	@Column(name = "JAPANESE_ID")
	private int japaneseId;
	
	@Column(name = "PORTUGUES_ID")
	private int portuguesId;
	/**
	 * @return the pageId
	 */
	public int getPageId() {
		return pageId;
	}
	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	/**
	 * @return the helpFile
	 */
	public String getHelpFile() {
		return helpFile;
	}
	/**
	 * @param helpFile the helpFile to set
	 */
	public void setHelpFile(String helpFile) {
		this.helpFile = helpFile;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}
	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	
	public String getEscapedPageName() {
		return this.pageName != null ? StringEscapeUtils.escapeXml(this.pageName) : this.pageName;
	}
	/**
	 * @return the notebookEventId
	 */
	public int getNotebookEventId() {
		return notebookEventId;
	}
	/**
	 * @param notebookEventId the notebookEventId to set
	 */
	public void setNotebookEventId(int notebookEventId) {
		this.notebookEventId = notebookEventId;
	}	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	public void setLanguageCode(int languageCode) {
		this.languageCode = languageCode;
	}
	public int getLanguageCode() {
		return languageCode;
	}
	public void setChineseId(int chineseId) {
		this.chineseId = chineseId;
	}
	public int getChineseId() {
		return chineseId;
	}
	public void setEspanolId(int espanolId) {
		this.espanolId = espanolId;
	}
	public int getEspanolId() {
		return espanolId;
	}
	public void setJapaneseId(int japaneseId) {
		this.japaneseId = japaneseId;
	}
	public int getJapaneseId() {
		return japaneseId;
	}
	public void setPortuguesId(int portuguesId) {
		this.portuguesId = portuguesId;
	}
	public int getPortuguesId() {
		return portuguesId;
	}
	
}
