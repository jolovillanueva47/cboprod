package org.cambridge.ebooks.online.jpa.trackback;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Karlson A. Mulingtapang
 * Trackbacks.java - Trackbacks
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = Trackbacks.UPDATE,
		query = "MERGE INTO EBOOK_TRACKBACKS t USING DUAL ON (t.REFERENCE_ID = ?1 AND t.BLOG_URL = ?2)"
		    + " WHEN MATCHED THEN UPDATE SET t.title = ?3, t.blog_name = ?4, t.author_ip = ?5" 
		    + ", t.modified_date = SYSDATE, t.excerpt = ?6, t.agent = ?7"
		    + " WHEN NOT MATCHED THEN INSERT VALUES (?8, ?9, ?10, ?11, ?12, SYSDATE, ?13, ?14)"
	),
	
	@NamedNativeQuery(
		name = Trackbacks.SEARCH_BY_REFERENCE_ID,
		query = "Select * from EBOOK_TRACKBACKS where REFERENCE_ID = ?1",
		resultSetMapping = Trackbacks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = Trackbacks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult(
				entityClass = Trackbacks.class,
				fields = {
					@FieldResult(name = "referenceId", column = "REFERENCE_ID"),
					@FieldResult(name = "title", column = "TITLE"),
					@FieldResult(name = "blogName", column = "BLOG_NAME"),
					@FieldResult(name = "blogUrl", column = "BLOG_URL"),
					@FieldResult(name = "authorIp", column = "AUTHOR_IP"),
					@FieldResult(name = "modifiedDate", column = "MODIFIED_DATE"),
					@FieldResult(name = "excerpt", column = "EXCERPT"),
					@FieldResult(name = "agent", column = "AGENT")
				}
			)
		}
	)
})

@Entity
@Table(name = "EBOOK_TRACKBACKS")
public class Trackbacks {

	public static final String RESULT_SET_MAPPING = "EBOOK_TRACKBACKS.result";
	public static final String UPDATE = "EBOOK_TRACKBACKS.update";
	public static final String SEARCH_BY_REFERENCE_ID = "EBOOK_TRACKBACKS.searchByReferenceId";
	
	@Id
	@Column(name = "REFERENCE_ID")
	private String referenceId;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "BLOG_NAME")
	private String blogName;
	
	@Id 
	@Column(name = "BLOG_URL")
	private String blogUrl;
	
	@Column(name = "AUTHOR_IP")
	private String authorIp;

	@Temporal (TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "EXCERPT")
	private String excerpt;
	
	@Column(name = "AGENT")
	private String agent;
	
	/**
	 * @return the referenceId
	 */
	public String getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the blogName
	 */
	public String getBlogName() {
		return blogName;
	}
	/**
	 * @param blogName the blogName to set
	 */
	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}
	/**
	 * @return the blogUrl
	 */
	public String getBlogUrl() {
		return blogUrl;
	}
	/**
	 * @param blogUrl the blogUrl to set
	 */
	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}
	/**
	 * @return the authorIp
	 */
	public String getAuthorIp() {
		return authorIp;
	}
	/**
	 * @param authorIp the authorIp to set
	 */
	public void setAuthorIp(String authorIp) {
		this.authorIp = authorIp;
	}
	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the excerpt
	 */
	public String getExcerpt() {
		return excerpt;
	}
	/**
	 * @param excerpt the excerpt to set
	 */
	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}
	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}
	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}
}
