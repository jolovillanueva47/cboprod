package org.cambridge.ebooks.online.jpa.topbooks;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.apache.commons.lang.StringEscapeUtils;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = EBookTopBooks.SEARCH_TOP_BOOKS_TABLE,
			query = "SELECT TITLE, ISBN FROM top_books order by summer desc",		
			resultSetMapping = EBookTopBooks.TOP_BOOKS_RESULT_SET
		),
	@NamedNativeQuery(
		name = EBookTopBooks.SEARCH_TOP_BOOKS,
		query = "SELECT TITLE, ISBN, SUMMER FROM top_books_view",		
		resultSetMapping = EBookTopBooks.TOP_BOOKS_RESULT_SET
	)	
})


@SqlResultSetMapping( 
	name = EBookTopBooks.TOP_BOOKS_RESULT_SET,
	entities = {
		@EntityResult (
			entityClass = EBookTopBooks.class,
			fields = {
				@FieldResult(name = "isbn",		column = "ISBN"),
				@FieldResult(name = "title",    column = "TITLE"),				
				@FieldResult(name = "summer",   column = "SUMMER")
			}
		)
	}
 )



@Entity
@Table(name = "TOP_BOOKS")
public class EBookTopBooks {

	public static final String SEARCH_TOP_BOOKS 		= "searchTopBooks";
	public static final String SEARCH_TOP_BOOKS_TABLE 	= "searchTopBooksTable";
	public static final String TOP_BOOKS_RESULT_SET = "topBooksResult";
	
	@Id	
	@Column( name = "ISBN")
	private String isbn;
	
	@Column( name = "TITLE")
	private String title;
	
	@Column( name="SUMMER")
	private int summer;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getEscapedTitle() {
		return this.title != null ? StringEscapeUtils.escapeXml(this.title) : this.title;
	}

	public int getSummer() {
		return summer;
	}

	public void setSummer(int summer) {
		this.summer = summer;
	}

	
	
}
