package org.cambridge.ebooks.online.jpa.topbooks;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import org.apache.commons.lang.StringEscapeUtils;

@NamedNativeQueries({
	@NamedNativeQuery(
			name = EBookTopFeaturedCollections.SEARCH_TOP_COLLECTIONS,
			query = 
				  "SELECT FE.*, ECH.description AS description_header, ECH.creation_date " +
				  "FROM featured_collections FE " +
				  "LEFT OUTER JOIN ebook_collection_header ECH " +
				  "ON FE.collection_id = ECH.collection_id " +
				  "ORDER BY display_order",
			resultSetMapping = EBookTopFeaturedCollections.TOP_COLLECTIONS_RESULT_SET
		)
})

@SqlResultSetMapping( 
	name = EBookTopFeaturedCollections.TOP_COLLECTIONS_RESULT_SET,
	entities = {
		@EntityResult (
			entityClass = EBookTopFeaturedCollections.class,
			fields = {
				@FieldResult(name = "collectionId",		column = "COLLECTION_ID"),
				@FieldResult(name = "collectionName",    column = "COLLECTION_NAME")			
			}
		)
	}
 )

@Entity
public class EBookTopFeaturedCollections {

	public static final String SEARCH_TOP_COLLECTIONS 	= "searchTopCollections";
	public static final String TOP_COLLECTIONS_RESULT_SET = "topCollectionsResult";
	
	@Id	
	@Column( name = "COLLECTION_ID")
	private String collectionId;
	
	@Column( name = "COLLECTION_NAME")
	private String collectionName;
	
	@Column( name = "DESCRIPTION_HEADER")
	private String collectionDescription;
	
	@Column( name = "CREATION_DATE")
	private Date creationDate;
	
	@Column( name = "A")
	private String letterA;
	
	@Column( name = "B")
	private String letterB;
	
	@Column( name = "C")
	private String letterC;
	
	@Column( name = "D")
	private String letterD;
	
	@Column( name = "E")
	private String letterE;
	
	@Column( name = "F")
	private String letterF;
	
	@Column( name = "G")
	private String letterG;
	
	@Column( name = "H")
	private String letterH;
	
	@Column( name = "I")
	private String letterI;
	
	@Column( name = "J")
	private String letterJ;
	
	@Column( name = "K")
	private String letterK;
	
	@Column( name = "L")
	private String letterL;
	
	@Column( name = "M")
	private String letterM;
	
	@Column( name = "N")
	private String letterN;
	
	@Column( name = "O")
	private String letterO;
	
	@Column( name = "P")
	private String letterP;
	
	@Column( name = "Q")
	private String letterQ;
	
	@Column( name = "R")
	private String letterR;
	
	@Column( name = "S")
	private String letterS;
	
	@Column( name = "T")
	private String letterT;
	
	@Column( name = "U")
	private String letterU;
	
	@Column( name = "V")
	private String letterV;
	
	@Column( name = "W")
	private String letterW;
	
	@Column( name = "X")
	private String letterX;
	
	@Column( name = "Y")
	private String letterY;
	
	@Column( name = "Z")
	private String letterZ;
	
	@Column( name = "OTHERS")
	private String letterOthers;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((collectionId == null) ? 0 : collectionId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EBookTopFeaturedCollections other = (EBookTopFeaturedCollections) obj;
		if (collectionId == null) {
			if (other.collectionId != null)
				return false;
		} else if (!collectionId.equalsIgnoreCase(other.collectionId))
			return false;
		return true;
	}
	
	public EBookTopFeaturedCollections() {}
	public EBookTopFeaturedCollections(String collectionId) {
		super();
		this.collectionId = collectionId;
	}
	
	public String getEscapedTitle() {
		return this.collectionName != null ? StringEscapeUtils.escapeXml(this.collectionName) : this.collectionName;
	}

	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
	
	public String getLetterA() {
		return letterA;
	}

	public void setLetterA(String letterA) {
		this.letterA = letterA;
	}

	public String getLetterB() {
		return letterB;
	}

	public void setLetterB(String letterB) {
		this.letterB = letterB;
	}

	public String getLetterC() {
		return letterC;
	}

	public void setLetterC(String letterC) {
		this.letterC = letterC;
	}

	public String getLetterD() {
		return letterD;
	}

	public void setLetterD(String letterD) {
		this.letterD = letterD;
	}

	public String getLetterE() {
		return letterE;
	}

	public void setLetterE(String letterE) {
		this.letterE = letterE;
	}

	public String getLetterF() {
		return letterF;
	}

	public void setLetterF(String letterF) {
		this.letterF = letterF;
	}

	public String getLetterG() {
		return letterG;
	}

	public void setLetterG(String letterG) {
		this.letterG = letterG;
	}

	public String getLetterH() {
		return letterH;
	}

	public void setLetterH(String letterH) {
		this.letterH = letterH;
	}

	public String getLetterI() {
		return letterI;
	}

	public void setLetterI(String letterI) {
		this.letterI = letterI;
	}

	public String getLetterJ() {
		return letterJ;
	}

	public void setLetterJ(String letterJ) {
		this.letterJ = letterJ;
	}

	public String getLetterK() {
		return letterK;
	}

	public void setLetterK(String letterK) {
		this.letterK = letterK;
	}

	public String getLetterL() {
		return letterL;
	}

	public void setLetterL(String letterL) {
		this.letterL = letterL;
	}

	public String getLetterM() {
		return letterM;
	}

	public void setLetterM(String letterM) {
		this.letterM = letterM;
	}

	public String getLetterN() {
		return letterN;
	}

	public void setLetterN(String letterN) {
		this.letterN = letterN;
	}

	public String getLetterO() {
		return letterO;
	}

	public void setLetterO(String letterO) {
		this.letterO = letterO;
	}

	public String getLetterP() {
		return letterP;
	}

	public void setLetterP(String letterP) {
		this.letterP = letterP;
	}

	public String getLetterQ() {
		return letterQ;
	}

	public void setLetterQ(String letterQ) {
		this.letterQ = letterQ;
	}

	public String getLetterR() {
		return letterR;
	}

	public void setLetterR(String letterR) {
		this.letterR = letterR;
	}

	public String getLetterS() {
		return letterS;
	}

	public void setLetterS(String letterS) {
		this.letterS = letterS;
	}

	public String getLetterT() {
		return letterT;
	}

	public void setLetterT(String letterT) {
		this.letterT = letterT;
	}

	public String getLetterU() {
		return letterU;
	}

	public void setLetterU(String letterU) {
		this.letterU = letterU;
	}

	public String getLetterV() {
		return letterV;
	}

	public void setLetterV(String letterV) {
		this.letterV = letterV;
	}

	public String getLetterW() {
		return letterW;
	}

	public void setLetterW(String letterW) {
		this.letterW = letterW;
	}

	public String getLetterX() {
		return letterX;
	}

	public void setLetterX(String letterX) {
		this.letterX = letterX;
	}

	public String getLetterY() {
		return letterY;
	}

	public void setLetterY(String letterY) {
		this.letterY = letterY;
	}

	public String getLetterZ() {
		return letterZ;
	}

	public void setLetterZ(String letterZ) {
		this.letterZ = letterZ;
	}
	public void setLetterOthers(String letterOthers) {
		this.letterOthers = letterOthers;
	}
	public String getLetterOthers() {
		return letterOthers;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public String getFormattedCreationDate(String formatString){
		String s = "";
		if(null!=getCreationDate()){
			s = new SimpleDateFormat(formatString).format(getCreationDate());
		}
		return s;
	}
	public void setCollectionDescription(String collectionDescription) {
		this.collectionDescription = collectionDescription;
	}
	public String getCollectionDescription() {
		return collectionDescription;
	}

}

/*			query = "SELECT fe.collection_id, " +
					"ebh.title as COLLECTION_NAME, ebh.description as DESCRIPTION, " +
					"A, B, C, D, " +
					"E, F, G, H, " +
					"I, J, K, L, " +
					"M, N, O, P, " +
					"Q, R, S, T, " +
					"U, V, W, X, " +
					"Y, Z, OTHERS " +
					"FROM FEATURED_COLLECTIONS fe " +
						"left outer join " +
						"EBOOK_COLLECTION_HEADER ebh " +
						"ON ebh.collection_id = fe.collection_id ",
*/
