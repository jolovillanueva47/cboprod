package org.cambridge.ebooks.online.jpa.esampling.bookshelf;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = EsamplingBook.SEARCH_BOOKSHELF_BY_USERID,
		query = "SELECT * FROM ESAMPLE_REQUEST WHERE user_id = ?1 ",
		resultSetMapping = EsamplingBook.RESULT_SET_MAPPING
	),
	
	@NamedNativeQuery(
			name = EsamplingBook.UPDATE_VIEW_COUNT,
			query = " UPDATE ESAMPLE_REQUEST SET VIEW_COUNT = ?1 " +
    		"WHERE REQUEST_ID = ?2 " 
		),
	
	@NamedNativeQuery(
		name = EsamplingBook.SEARCH_BOOKSHELF_BY_ISBN,
		query = "SELECT * FROM ESAMPLE_REQUEST WHERE user_id = ?1 AND isbn = ?2 ",
		resultSetMapping = EsamplingBook.RESULT_SET_MAPPING
	),
	
	@NamedNativeQuery(
		name = EsamplingBook.SEARCH_BOOKSHELF_BY_REQUESTID,
		query = "SELECT * FROM ESAMPLE_REQUEST WHERE request_id = ?1",
		resultSetMapping = EsamplingBook.RESULT_SET_MAPPING
	)
	
})


@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = EsamplingBook.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = EsamplingBook.class,
				fields = {
					@FieldResult(name = "requestId", column = "REQUEST_ID"),
					@FieldResult(name = "userId", column = "USER_ID"),
					@FieldResult(name = "isbn", column = "ISBN"),
					@FieldResult(name = "validFrom", column = "VALID_FROM"),
					@FieldResult(name = "validTo", column = "VALID_TO"),
					@FieldResult(name = "viewCount", column = "VIEW_COUNT")
				}
			)	
		}
	)
})

@Entity
@Table(name = "ESAMPLE_REQUEST")
public class EsamplingBook {
	
	public static final String SEARCH_BOOKSHELF_BY_USERID = "searchBookshelf";
	public static final String SEARCH_BOOKSHELF_BY_ISBN = "searchBookshelfByBodyId";
	public static final String UPDATE_VIEW_COUNT = "updateViewCount";
	public static final String SEARCH_BOOKSHELF_BY_REQUESTID = "searchBookshelfByRequestId";
	
	public static final String RESULT_SET_MAPPING = "findBookshelfResult";
	
	
	@Id
	@Column( name = "REQUEST_ID")
	private int requestId;
	
	@Id
	@Column( name = "USER_ID")
	private int userId;
	
	@Id
	@Column( name = "ISBN")
	private String isbn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "VALID_FROM")
	private Date validFrom;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "VALID_TO")
	private Date validTo;
	
	@Column ( name= "VIEW_COUNT")
	private int viewCount;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	

	
}
