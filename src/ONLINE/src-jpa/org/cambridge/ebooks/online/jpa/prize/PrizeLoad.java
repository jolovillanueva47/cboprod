package org.cambridge.ebooks.online.jpa.prize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
		name = PrizeLoad.SEARCH_BY_ISBN,
		query = "SELECT * FROM ebook_prize_load WHERE isbn = ?1 ORDER BY sequence_number",
		resultSetMapping = PrizeLoad.RESULT_SET_MAPPING
	)
})
@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = PrizeLoad.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = PrizeLoad.class,
				fields = {
					@FieldResult(name = "isbn", column = "ISBN"),
					@FieldResult(name = "titleCode", column = "TITLE_CODE"),
					@FieldResult(name = "sequenceNumber", column = "SEQUENCE_NUMBER"),
					@FieldResult(name = "prizeText", column = "PRIZE_TEXT")
				}
			)	
		}
	)
})

@Entity
@Table(name = "EBOOK_PRIZE_LOAD")
public class PrizeLoad {
	
	public static final String RESULT_SET_MAPPING = "prizeLoadResultSetMapping";
	public static final String SEARCH_BY_ISBN = "prizeLoadSearchByIsbn";

	@Id
	@Column( name = "ISBN")
	private String isbn;

	@Id
	@Column( name = "TITLE_CODE")
	private int titleCode;

	@Id
	@Column( name = "SEQUENCE_NUMBER")
	private int sequenceNumber;

	@Column( name = "PRIZE_TEXT")
	private String prizeText;
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getTitleCode() {
		return titleCode;
	}
	public void setTitleCode(int titleCode) {
		this.titleCode = titleCode;
	}
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getPrizeText() {
		return prizeText;
	}
	public void setPrizeText(String prizeText) {
		this.prizeText = prizeText;
	}
	
	
}
