package org.cambridge.ebooks.online.jpa.citation.alert;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 * @author jgalang
 * 
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = CitationAlert.SEARCH_CITATION_ALERTS,
		query = "SELECT * FROM citation_alert WHERE body_id = ?1",
		resultSetMapping = CitationAlert.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = CitationAlert.SEARCH_CITATION_ALERTS_BY_IDS,
		query = "SELECT * FROM citation_alert WHERE body_id = ?1 AND book_id = ?2 AND content_id = ?3",
		resultSetMapping = CitationAlert.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = CitationAlert.SEARCH_ALL_DOI,
		query = "SELECT DISTINCT doi FROM citation_alert WHERE doi IS NOT NULL",
		resultSetMapping = CitationAlert.DOI_RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = CitationAlert.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = CitationAlert.class,
				fields = {
					@FieldResult(name = "citationAlertId", column = "CITATION_ALERT_ID"),
					@FieldResult(name = "bodyId", column = "BODY_ID"),
					@FieldResult(name = "bookId", column = "BOOK_ID"),
					@FieldResult(name = "bookTitle", column = "BOOK_TITLE"),
					@FieldResult(name = "author", column = "AUTHOR"),
					@FieldResult(name = "publishDate", column = "PUBLISH_DATE"),
					@FieldResult(name = "contentId", column = "CONTENT_ID"),
					@FieldResult(name = "contentTitle", column = "CONTENT_TITLE"),
					@FieldResult(name = "page", column = "PAGE"),
					@FieldResult(name = "doi", column = "DOI"),
					@FieldResult(name = "frequencyType", column = "FREQUENCY_TYPE"),
					@FieldResult(name = "emailAddress", column = "EMAIL_ADDRESS"),
					@FieldResult(name = "turnOffAlert", column = "TURNOFF_ALERT"),
					@FieldResult(name = "lastAlertDate", column = "LAST_ALERT_DATE")
				}
			)	
		}
	),
	@SqlResultSetMapping(
			name = CitationAlert.DOI_RESULT_SET_MAPPING,
			columns = { @ColumnResult(name = "DOI") }
		)
})

@Entity
@Table(name = "CITATION_ALERT")
public class CitationAlert {
	
	public static final String SEARCH_CITATION_ALERTS = "searchCitationAlerts";
	public static final String SEARCH_CITATION_ALERTS_BY_IDS = "searchCitationAlertsByIds";
	public static final String SEARCH_ALL_DOI = "searchAllDoi";
	public static final String RESULT_SET_MAPPING = "citationAlertsResult";
	public static final String DOI_RESULT_SET_MAPPING = "doiCitationAlertsResult"; 

	@Id
	@GeneratedValue(generator="CITATION_ALERT_SEQ")
    @SequenceGenerator(name="CITATION_ALERT_SEQ", sequenceName="CITATION_ALERT_SEQ", allocationSize=1)
	@Column( name = "CITATION_ALERT_ID")
	private String citationAlertId;

	@Column( name = "BODY_ID")
	private String bodyId;
	
	@Column( name = "BOOK_ID")
	private String bookId;

	@Column( name = "BOOK_TITLE")
	private String bookTitle;

	@Column( name = "AUTHOR")
	private String author;

	@Column( name = "PUBLISH_DATE")
	private String publishDate;

	@Column( name = "CONTENT_ID")
	private String contentId;

	@Column( name = "CONTENT_TITLE")
	private String contentTitle;

	@Column( name = "PAGE")
	private String page;

	@Column( name = "DOI")
	private String doi;

	@Column( name = "FREQUENCY_TYPE")
	private String frequencyType;

	@Column( name = "EMAIL_ADDRESS")
	private String emailAddress;

	@Column( name = "TURNOFF_ALERT")
	private String turnOffAlert;

	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "LAST_ALERT_DATE")
	private Date lastAlertDate;
	
	@Transient
	private boolean turnOff;

	@Transient
	private boolean delete;
	
	public boolean equals(Object o) { 
		boolean result = false;
		if (o instanceof CitationAlert) {
			CitationAlert b = (CitationAlert) o;
			if ( b.getCitationAlertId().equals(this.getCitationAlertId()) )  { 
				result = true;
			}
		}
		return result;
	}
	
	
	public String getCitationAlertId() {
		return citationAlertId;
	}
	public void setCitationAlertId(String citationAlertId) {
		this.citationAlertId = citationAlertId;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getBodyId() {
		return bodyId;
	}
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getContentTitle() {
		return contentTitle;
	}
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public String getFrequencyType() {
		return frequencyType;
	}
	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getTurnOffAlert() {
		return turnOffAlert;
	}
	public void setTurnOffAlert(String turnOffAlert) {
		this.turnOffAlert = turnOffAlert;
	}
	public Date getLastAlertDate() {
		return lastAlertDate;
	}
	public void setLastAlertDate(Date lastAlertDate) {
		this.lastAlertDate = lastAlertDate;
	}
	public boolean isTurnOff() {
		turnOff = turnOffAlert.equals("Y");
		return turnOff;
	}
	public void setTurnOff(boolean turnOff) {
		turnOffAlert = turnOff ? "Y" : "N";
		this.turnOff = turnOff;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
