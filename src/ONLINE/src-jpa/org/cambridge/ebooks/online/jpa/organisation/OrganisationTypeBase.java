package org.cambridge.ebooks.online.jpa.organisation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORACJOC.ORGANISATION_TYPE")
public class OrganisationTypeBase {
	
	@Id
	@Column(name="ORGANISATION_TYPE_ID")
	private int organisationTypeId;	
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public int getOrganisationTypeId() {
		return organisationTypeId;
	}
	public void setOrganisationTypeId(int organisationTypeId) {
		this.organisationTypeId = organisationTypeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isSociety(){
		if("Society".equals(this.description)){
			return true;
		}
		return false;
	}
	
	
	
}
