package org.cambridge.ebooks.online.jpa.faq;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

/**
 * @author Karlson A. Mulingtapang
 * FaqSubjectTypeDetailsMapBooks.java - FAQ_SUBJECT_TYPE_DETAILS_MAP_B synonymous with CJO
 */

@NamedNativeQueries({
	@NamedNativeQuery(
		name = FaqSubjectTypeDetailsMapBooks.SEARCH_ALL,
		query = "Select * from FAQ_SUBJECT_TYPE_DETAILS_MAP_B",
		resultSetMapping = FaqSubjectTypeDetailsMapBooks.RESULT_SET_MAPPING
	),
	@NamedNativeQuery(
		name = FaqSubjectTypeDetailsMapBooks.SEARCH_BY_SUBJECT_AREA_TYPE_ID,
		query = "Select * from FAQ_SUBJECT_TYPE_DETAILS_MAP_B where FAQ_SUBJECT_AREA_TYPE_ID = ?1",
		resultSetMapping = FaqSubjectTypeDetailsMapBooks.RESULT_SET_MAPPING
	)
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = FaqSubjectTypeDetailsMapBooks.RESULT_SET_MAPPING,
		entities = {
			@EntityResult (
				entityClass = FaqSubjectTypeDetailsMapBooks.class,
				fields = {
					@FieldResult(name = "faqSubjectAreaTypeId", column = "FAQ_SUBJECT_AREA_TYPE_ID"),
					@FieldResult(name = "faqSubjectAreaDetailsId", column = "FAQ_SUBJECT_AREA_DETAILS_ID")
				}
			)	
		}
	)
})

@Entity
@Table(name = "FAQ_SUBJECT_TYPE_DETAILS_MAP_B")
public class FaqSubjectTypeDetailsMapBooks {
	
	public static final String RESULT_SET_MAPPING = "FAQ_SUBJECT_TYPE_DETAILS_MAP_B.result";
	public static final String SEARCH_ALL = "FAQ_SUBJECT_TYPE_DETAILS_MAP_B.searchAll";
	public static final String SEARCH_BY_SUBJECT_AREA_TYPE_ID = 
		"FAQ_SUBJECT_TYPE_DETAILS_MAP_B.searchBySubjectAreaTypeId";
	
	@Id
	@Column(name = "FAQ_SUBJECT_AREA_TYPE_ID")
	private int faqSubjectAreaTypeId;
	
	@Id
	@Column(name = "FAQ_SUBJECT_AREA_DETAILS_ID")
	private int faqSubjectAreaDetailsId;		

	/**
	 * @return the faqSubjectAreaTypeId
	 */
	public int getFaqSubjectAreaTypeId() {
		return faqSubjectAreaTypeId;
	}

	/**
	 * @param faqSubjectAreaTypeId the faqSubjectAreaTypeId to set
	 */
	public void setFaqSubjectAreaTypeId(int faqSubjectAreaTypeId) {
		this.faqSubjectAreaTypeId = faqSubjectAreaTypeId;
	}

	/**
	 * @return the faqSubjectAreaDetailsId
	 */
	public int getFaqSubjectAreaDetailsId() {
		return faqSubjectAreaDetailsId;
	}

	/**
	 * @param faqSubjectAreaDetailsId the faqSubjectAreaDetailsId to set
	 */
	public void setFaqSubjectAreaDetailsId(int faqSubjectAreaDetailsId) {
		this.faqSubjectAreaDetailsId = faqSubjectAreaDetailsId;
	}	
}
