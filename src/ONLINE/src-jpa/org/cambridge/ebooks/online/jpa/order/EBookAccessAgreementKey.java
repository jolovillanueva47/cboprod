package org.cambridge.ebooks.online.jpa.order;

import java.io.Serializable;

import javax.persistence.Column;

public class EBookAccessAgreementKey implements Serializable{

	 
	@Column(name="ORDER_ID")
	private long orderId;
	
	 
	@Column(name="BODY_ID")
	private long bodyId;
	 
	public EBookAccessAgreementKey(){		 
	}
	
	

	public EBookAccessAgreementKey(long orderId, long bodyId) {	
		this.orderId = orderId;
		this.bodyId = bodyId;
	}



	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getBodyId() {
		return bodyId;
	}

	public void setBodyId(long bodyId) {
		this.bodyId = bodyId;
	}

	@Override
	public boolean equals(Object obj) {
//	    if (obj == this) return true;
//	    if (!(obj instanceof EBookAccessAgreementKey)) return false;
//	    if (obj == null) return false;
//	    
//	    EBookAccessAgreementKey pk = (EBookAccessAgreementKey) obj;
//	    return pk.orderId == orderId && pk.bodyId == bodyId;
		boolean result;
	    if (obj == this) result = true;
	    else if (!(obj instanceof EBookAccessAgreementKey)) result = false;
	    else if (obj == null) result = false;
	    else {	    
		    EBookAccessAgreementKey pk = (EBookAccessAgreementKey) obj;
		    result = pk.orderId == orderId && pk.bodyId == bodyId;
	    }
	    return result;

	}
	 
	 
}
