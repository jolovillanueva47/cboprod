package org.cambridge.ebooks.online.jpa.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class EBookSubjectAlertPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="MEMBER_ID", insertable=true, nullable=false)
	private int memberId;
	
	@Column(name="SUBJECT_ID", insertable=true, nullable=false)
	private String subjectId;

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	
}
