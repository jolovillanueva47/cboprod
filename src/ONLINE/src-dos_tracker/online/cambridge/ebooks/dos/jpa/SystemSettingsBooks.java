package online.cambridge.ebooks.dos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@SqlResultSetMapping(
	name = SystemSettingsBooks.RESULT_SET_MAPPING,
	entities = {
		@EntityResult(
			entityClass = SystemSettingsBooks.class,
			fields = {
				@FieldResult (name = "gracePeriod", column = "GRACE_PERIOD"),
				@FieldResult (name = "csrUkEmail", column = "CSR_UK_EMAIL"),
				@FieldResult (name = "csrUsEmail", column = "CSR_US_EMAIL"),
				@FieldResult (name = "csrUsEmailTechsupport", column = "CSR_US_EMAIL_TECHSUPPORT"),
				@FieldResult (name = "currentVolumeYear", column = "CURRENT_VOLUME_YEAR"),
				@FieldResult (name = "maxSaveSearch", column = "MAX_SAVE_SEARCH"),
				@FieldResult (name = "maxSaveBookmark", column = "MAX_SAVE_BOOKMARK"),
				@FieldResult (name = "defaulPpvHours", column = "DEFAULT_PPV_HOURS"),
				@FieldResult (name = "marketingEmail", column = "MARKETING_EMAIL"),
				@FieldResult (name = "withRobot", column = "WITH_ROBOT"),
				@FieldResult (name = "abuseMonitoring", column = "ABUSE_MONITORING")
			}
		)
	}
)

@Entity
@Table (name = "SYSTEM_SETTINGS_BOOKS")
public class SystemSettingsBooks {
	public static final String RESULT_SET_MAPPING = "oraebooks.system_settings_books";
	
	
	@Id
	private String dummyId;
	
	@Column(name = "GRACE_PERIOD")
	private String gracePeriod;
	
	@Column(name = "CSR_UK_EMAIL")
	private String csrUkEmail;
	
	@Column(name = "CSR_US_EMAIL")
	private String csrUsEmail;
	
	@Column(name = "CSR_US_EMAIL_TECHSUPPORT")
	private String csrUsEmailTechsupport;
	
	@Column(name = "CURRENT_VOLUME_YEAR")
	private String currentVolumeYear;
	
	@Column(name = "MAX_SAVE_SEARCH")
	private String maxSaveSearch;
	
	@Column(name = "MAX_SAVE_BOOKMARK")
	private String maxSaveBookmark;
	
	@Column(name = "DEFAULT_PPV_HOURS")
	private String defaultPpvHours;
	
	@Column(name = "MARKETING_EMAIL")
	private String marketingEmail;
	
	@Column(name = "WITH_ROBOT")
	private String withRobot;
	
	@Column(name = "ABUSE_MONITORING")
	private String abuseMonitoring;
	
	/**
	 * @return the gracePeriod
	 */
	public String getGracePeriod() {
		return gracePeriod;
	}
	/**
	 * @param gracePeriod the gracePeriod to set
	 */
	public void setGracePeriod(String gracePeriod) {
		this.gracePeriod = gracePeriod;
	}
	/**
	 * @return the csrUkEmail
	 */
	public String getCsrUkEmail() {
		return csrUkEmail;
	}
	/**
	 * @param csrUkEmail the csrUkEmail to set
	 */
	public void setCsrUkEmail(String csrUkEmail) {
		this.csrUkEmail = csrUkEmail;
	}
	/**
	 * @return the csrUsEmail
	 */
	public String getCsrUsEmail() {
		return csrUsEmail;
	}
	/**
	 * @param csrUsEmail the csrUsEmail to set
	 */
	public void setCsrUsEmail(String csrUsEmail) {
		this.csrUsEmail = csrUsEmail;
	}	
	/**
	 * @return the csrUsEmailTechsupport
	 */
	public String getCsrUsEmailTechsupport() {
		return csrUsEmailTechsupport;
	}
	/**
	 * @param csrUsEmailTechsupport the csrUsEmailTechsupport to set
	 */
	public void setCsrUsEmailTechsupport(String csrUsEmailTechsupport) {
		this.csrUsEmailTechsupport = csrUsEmailTechsupport;
	}
	/**
	 * @return the currentVolumeYear
	 */
	public String getCurrentVolumeYear() {
		return currentVolumeYear;
	}
	/**
	 * @param currentVolumeYear the currentVolumeYear to set
	 */
	public void setCurrentVolumeYear(String currentVolumeYear) {
		this.currentVolumeYear = currentVolumeYear;
	}
	/**
	 * @return the maxSaveSearch
	 */
	public String getMaxSaveSearch() {
		return maxSaveSearch;
	}
	/**
	 * @param maxSaveSearch the maxSaveSearch to set
	 */
	public void setMaxSaveSearch(String maxSaveSearch) {
		this.maxSaveSearch = maxSaveSearch;
	}
	/**
	 * @return the maxSaveBookmark
	 */
	public String getMaxSaveBookmark() {
		return maxSaveBookmark;
	}
	/**
	 * @param maxSaveBookmark the maxSaveBookmark to set
	 */
	public void setMaxSaveBookmark(String maxSaveBookmark) {
		this.maxSaveBookmark = maxSaveBookmark;
	}
	/**
	 * @return the defaultPpvHours
	 */
	public String getDefaultPpvHours() {
		return defaultPpvHours;
	}
	/**
	 * @param defaultPpvHours the defaultPpvHours to set
	 */
	public void setDefaultPpvHours(String defaultPpvHours) {
		this.defaultPpvHours = defaultPpvHours;
	}
	/**
	 * @return the marketingEmail
	 */
	public String getMarketingEmail() {
		return marketingEmail;
	}
	/**
	 * @param marketingEmail the marketingEmail to set
	 */
	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}
	/**
	 * @return the withRobot
	 */
	public String getWithRobot() {
		return withRobot;
	}
	/**
	 * @param withRobot the withRobot to set
	 */
	public void setWithRobot(String withRobot) {
		this.withRobot = withRobot;
	}
	/**
	 * @return the abuseMonitoring
	 */
	public String getAbuseMonitoring() {
		return abuseMonitoring;
	}
	/**
	 * @param abuseMonitoring the abuseMonitoring to set
	 */
	public void setAbuseMonitoring(String abuseMonitoring) {
		this.abuseMonitoring = abuseMonitoring;
	}
	/**
	 * @return the dummyId
	 */
	public String getDummyId() {
		return dummyId;
	}
	/**
	 * @param dummyId the dummyId to set
	 */
	public void setDummyId(String dummyId) {
		this.dummyId = dummyId;
	}		
}
