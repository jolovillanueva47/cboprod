package online.cambridge.ebooks.dos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@SqlResultSetMapping(
	name = DosTracking.RESULT_SET_MAPPING,
	entities = {
		@EntityResult (
			entityClass = DosTracking.class,
			fields = {
				@FieldResult (name = "dosId", column = "DOS_ID"),
				@FieldResult (name = "sessionId", column = "SESSION_ID"),
				@FieldResult (name = "bodyId", column = "BODY_ID"),
				@FieldResult (name = "startTime", column = "START_TIME"),
				@FieldResult (name = "pdfAccessCount", column = "PDF_ACCESS_COUNT"),
				@FieldResult (name = "ipType", column = "IP_TYPE"),
				@FieldResult (name = "ipAddress", column = "IP_ADDRESS"),
				@FieldResult (name = "endTime", column = "END_TIME"),
				@FieldResult (name = "duration", column = "DURATION"),
				@FieldResult (name = "logDate", column = "LOG_DATE")
			}
		)
	}
)

@Entity
@Table(name = "DOS_TRACKING")
public class DosTracking {
	
	public static final String RESULT_SET_MAPPING = "oraebooks.DOS_TRACKING";
	private static final String DOS_TRACKING_SEQ = "DOS_TRACKING_SEQ";

	@Id
	@GeneratedValue(generator = DOS_TRACKING_SEQ)
	@SequenceGenerator(name=DOS_TRACKING_SEQ, sequenceName=DOS_TRACKING_SEQ, allocationSize=1)
	@Column(name = "DOS_ID")
	private String dosId;
	
	@Column(name = "SESSION_ID")
	private String sessionId;
	
	@Column(name = "BODY_ID")
	private String bodyId;
	
	@Column(name = "START_TIME")
	private String startTime;
	
	@Column(name = "PDF_ACCESS_COUNT")
	private String pdfAccessCount;
	
	@Column(name = "IP_TYPE")
	private String ipType;
	
	@Column(name = "IP_ADDRESS")
	private String ipAddress;
	
	@Column(name = "END_TIME")
	private String endTime;
	
	@Column(name = "DURATION")
	private String duration;
	
	@Column(name = "LOG_DATE")
	private String logDate;

	/**
	 * @return the dosId
	 */
	public String getDosId() {
		return dosId;
	}

	/**
	 * @param dosId the dosId to set
	 */
	public void setDosId(String dosId) {
		this.dosId = dosId;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the bodyId
	 */
	public String getBodyId() {
		return bodyId;
	}

	/**
	 * @param bodyId the bodyId to set
	 */
	public void setBodyId(String bodyId) {
		this.bodyId = bodyId;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the pdfAccessCount
	 */
	public String getPdfAccessCount() {
		return pdfAccessCount;
	}

	/**
	 * @param pdfAccessCount the pdfAccessCount to set
	 */
	public void setPdfAccessCount(String pdfAccessCount) {
		this.pdfAccessCount = pdfAccessCount;
	}

	/**
	 * @return the ipType
	 */
	public String getIpType() {
		return ipType;
	}

	/**
	 * @param ipType the ipType to set
	 */
	public void setIpType(String ipType) {
		this.ipType = ipType;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return the logDate
	 */
	public String getLogDate() {
		return logDate;
	}

	/**
	 * @param logDate the logDate to set
	 */
	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}	
}
