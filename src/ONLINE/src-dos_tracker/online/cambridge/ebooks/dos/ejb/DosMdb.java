package online.cambridge.ebooks.dos.ejb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import online.cambridge.ebooks.dos.jpa.DosTracking;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.oracle.ip.ConvertIp;
import org.cambridge.ebooks.online.util.EBooksUtil;
import org.cambridge.ebooks.online.util.ExceptionPrinter;
import org.cambridge.ebooks.online.util.MailManager;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

@MessageDriven(name = "DosMdb", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/dos_tracker/logger")})

public class DosMdb implements MessageListener {
	
	private static final String[] ACCESS_VO_PROPERTIES = {		
		"pdfAccessCount",
		"ipAddress",
		"ipType",
		"sessionId",
		"startTime",
		"endTime",
		"duration",
		"logDate",
		"bodyId"
	}; 
	
	private static Logger logger = Logger.getLogger(DosMdb.class);
	
	private static DocumentBuilderFactory factory = null;
	
	private static final String TMP_DIR = System.getProperty("abuse.monitoring.tmp").trim();	
	private static final String PDF_ACCESS_LIMIT = System.getProperty("pdf.access.user.limit").trim();
	private static final String CONTENT_DIR = System.getProperty("content.dir").trim();
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("DosUnit");
	
	private EntityManager em;
	
	public void onMessage(Message message) {
		logger.info("onMessage...");	
		if(message instanceof Message) {
			em = emf.createEntityManager();
			try {				
				
				DosTracking dosTracking = createDosTracking(message);
				em.joinTransaction();
				em.persist(dosTracking);
				sendEmailNotification(dosTracking, em);				
			} catch (Exception e) {
				logger.error("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
			} finally {
				em.close();
			}
		}
	}
	
	private DosTracking createDosTracking(Message message) throws Exception {
		DosTracking dosTracking = new DosTracking();
		String value = "";
		for(String prop : ACCESS_VO_PROPERTIES) {
			try {
				value = message.getStringProperty(prop);
				logger.info("value: " + value);	
			} catch (Exception e) {
				logger.error("[Exception]" + ExceptionPrinter.getStackTraceAsString(e));
				continue;
			}
			BeanUtils.setProperty(dosTracking, prop, value);
		}
		return dosTracking;
	}
	
	private void sendEmailNotification(DosTracking dosTracking, EntityManager em)
		throws Exception {
		
		logger.info("sending email...");
		String sql = "SELECT ABUSE_MONITORING from ORACJOC.SYSTEM_SETTINGS_BOOKS";
		Query query = em.createNativeQuery(sql);
		String abuseMonitoringEmail = (String)query.getSingleResult();
        
		String templateDir = System.getProperty("templates.dir").trim();
		String outputDir = System.getProperty("mail.store.path").trim();
        String from = System.getProperty("email.cbo").trim();
        String[] to = abuseMonitoringEmail.split(",");
        String subject = System.getProperty("abuse.email.subject");
        String[] bcc = new String[]{System.getProperty("abuse.email.bcc")};
        String attachment = TMP_DIR + "/abuse_monitoring_" + EBooksUtil.getCurrentDate("yyyyMMddHHmmss") + ".csv";
        StringBuffer emailBody = new StringBuffer();	    
        
        // remove whitespace
        List<String> trimEmails = new ArrayList<String>(); 
        for(String s : to) {
        	if(StringUtils.isNotEmpty(s)) {
        		logger.info("email:" + s);
        		trimEmails.add(s);
        	}
        }
        
        // convert List to String[]
        to = trimEmails.toArray(new String[0]);
        
        // Type: User otherwise Proxy
        if ("User".equals(dosTracking.getIpType())){
        	emailBody.append(System.getProperty("abuse.email.body.user"));
        	emailBody.append("<br />");
        } else {
        	emailBody.append(System.getProperty("abuse.email.body.proxy"));
        	emailBody.append("<br />");        	
        }
        emailBody.append("<br />");
        emailBody.append(dosTracking.getIpAddress());
        emailBody.append("<br /><br />");
        emailBody.append(getIpDetails(dosTracking.getIpAddress()));
        
        addTimestamp(emailBody);
        
        if(createAttachmentFile(em, attachment, dosTracking)) {
        	// param: templateDir, templateFile, outputDir, from, to, cc, bcc, subject, attachment, messageBody
            MailManager.storeMail(
            		templateDir, 
            		System.getProperty("mail.export.citation.template").trim(), 
            		outputDir, 
            		from,
    				to, 
    				null,
    				bcc, 
    				subject,
    				attachment,
    				emailBody.toString()
    				);
        } else {            
            // param: templateDir, templateFile, outputDir, from, to, cc, bcc, subject, messageBody
            MailManager.storeMail(
            		templateDir, 
            		System.getProperty("mail.user.info.template").trim(), 
            		outputDir, 
            		from,
    				to, 
    				null,
    				bcc, 
    				subject,
    				emailBody.toString()
    				);
        }
        
	    logger.info("email sent!");
	}
	
	protected boolean createAttachmentFile(EntityManager em, String filePath, DosTracking dosTracking) throws Exception {		
		boolean hasAttachment = false;
		
		try {			
			DateFormat df = new SimpleDateFormat("MMM/dd/yyyy hh:mm a");
			
			StringBuilder sql = new StringBuilder();			
			sql.append("select e.isbn, e.title as BOOK_TITLE, cse.NOTIFIED_TIME, cse.IP_ADDRESS, ec.content_id ")
			  .append("from ORAEBOOKS.cbo_session_evnt cse, ORAEBOOKS.dos_tracking dt, ORAEBOOKS.ebook_content ec, ORAEBOOKS.ebook e ")
			  .append("where cse.ip_address = dt.ip_address ") 
			  .append("and cse.event_str1 = ec.content_id ")
			  .append("and ec.book_id = e.book_id ")
			  .append("and cse.session_id = dt.session_id ")
			  .append("and cse.event_no = 4 ") 
			  .append("and to_char(cse.notified_time, 'HH24:MI:SS') >= dt.start_time ")
			  .append("and rownum <= ").append(PDF_ACCESS_LIMIT).append(" ")
			  .append("and dt.session_id = '")			
			  .append(dosTracking.getSessionId())
			  .append("' order by cse.notified_time");			
			
			Query query = em.createNativeQuery(sql.toString());
			List<Object[]> results = query.getResultList();
			
			// create attachment if results is not 0
			if(null != results && results.size() > 0) {		
				hasAttachment = true;
				
				File file = new File(filePath);
				FileWriter writer = new FileWriter(file);
				writer.write("ISBN,BOOK_TITLE,CONTENT_TITLE,NOTIFIED_TIME,IP_ADDRESS\n");
				
				for(Object[] column : results) {
					StringBuilder row = new StringBuilder();
					
					// ISBN
					String isbn = "";
					if(null != isbn) {
						isbn = (String)column[0];
						row.append("=\"").append(isbn).append("\",");
					} else {
						row.append("null,");
					}
					
					// BOOK_TITLE
					if(null != column[1]) {
						row.append("\"").append((String)column[1]).append("\",");
					} else {
						row.append("null,");
					}
					
					// CONTENT_TITLE
					if(null != column[4]) {
						StringBuilder headerFile = new StringBuilder(CONTENT_DIR);
						headerFile.append(isbn).append("/").append(isbn).append(".xml"); 
						row.append("\"").append(getContentTitleByXpath(headerFile.toString(), (String)column[4])).append("\",");
					}
					
					// NOTIFIED_TIME
					if(null != column[2]) {
						row.append(df.format((Date)column[2])).append(",");					
					} else {
						row.append("null,");
					}
					
					// IP_ADDRESS
					if(null != column[3]) {
						row.append((String)column[3]);
					} else {
						row.append("null");
					}					
					
					row.append("\n");
					
					writer.write(row.toString());
				}		
				
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		
		return hasAttachment;
	}
	
	
	/* Sample output
	  	 
	   This IP address is associated with the following account(s) in the system:

	   Organisation Details
	   Body ID 1891296
	   Display Name Rabbit Rabbit (Testing Testing)
	   Body ID 1898352
	   Display Name Heather Brand New Test Org
		
	   Consortia Details
	   Body ID 1891300
	   Display Name Heather Test Consortium
	*/	 
	private String getIpDetails(String ipAddress) {		
		StringBuffer result = new StringBuffer();
		
		List<Object[]> ipDetails = getIpDetailsObjects(ipAddress);
		StringBuffer orgDetails = new StringBuffer("Organisation Details<br />");
		StringBuffer consortiaDetails = new StringBuffer("Consortia Details<br />");
		boolean hasOrgDetails = false;
		boolean hasConsortiaDetails = false;
		for(Object[] rows : ipDetails) {
			BigDecimal bodyId = (BigDecimal)rows[0];
			String name = (String)rows[1];
			String type = (String)rows[2];
			
			if("O".equals(type)) {
				orgDetails.append("Body ID: ");
				orgDetails.append(bodyId);
				orgDetails.append("<br />");
				orgDetails.append("Display Name: ");
				orgDetails.append(name);
				orgDetails.append("<br />");
				hasOrgDetails = true;
			} else {
				consortiaDetails.append("Body ID: ");
				consortiaDetails.append(bodyId);
				consortiaDetails.append("<br />");
				consortiaDetails.append("Display Name: ");
				consortiaDetails.append(name);
				consortiaDetails.append("<br />");
				hasConsortiaDetails = true;
			}
		}	
		
		if(hasOrgDetails) {
			result.append("This IP address is associated with the following account(s) in the system:<br /><br />");
			result.append(orgDetails.toString());
			result.append("<br />");
		}
		
		if(hasConsortiaDetails) {
			result.append(consortiaDetails.toString());
		}
		
		return result.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	private List<Object[]> getIpDetailsObjects(String ipAddress){
		long longIp = ConvertIp.ipToLong(ipAddress);
		
		StringBuffer sqlStatement =  new StringBuffer();
		sqlStatement.append("select distinct ");
		sqlStatement.append("decode(c.body_id, null, o.body_id, c.body_id) BODY_ID, ");
		sqlStatement.append("decode(c.name, null, o.name, c.name) NAME, ");
		sqlStatement.append("decode(c.body_id, null, 'O', 'C') TYPE ");
		sqlStatement.append("from ipaddress_identification_final ip, ");
		sqlStatement.append("oracjoc.organisation o, oracjoc.consortium c ");
		sqlStatement.append("where ");
		sqlStatement.append(longIp);
		sqlStatement.append(" between start_ip_long and end_ip_long ");
		sqlStatement.append("and ip.body_id = o.body_id (+) ");
		sqlStatement.append("and ip.body_id = c.body_id (+) ");
		sqlStatement.append("and INCLUDE = 'Y' ");
		sqlStatement.append("and ip.BODY_ID not in ");
		sqlStatement.append("(select BODY_ID from ipaddress_identification_final ");
		sqlStatement.append("where ");
		sqlStatement.append(longIp);
		sqlStatement.append(" between start_ip_long and end_ip_long ");
		sqlStatement.append("and INCLUDE = 'N') order by TYPE desc");	
		
		Query query;
		List<Object[]> ipDetails;
		try {
			query = em.createNativeQuery(sqlStatement.toString());
			ipDetails = query.getResultList();
		} catch (Exception e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
			ipDetails = new ArrayList<Object[]>();
		}
		
		return ipDetails;
	}
	
	private void addTimestamp(StringBuffer sb){		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM/dd/yyyy hh:mm a");
		sb.append("<br />");
		sb.append("The abuse occurred at " + sdf.format(date) + " UK time");		
		sb.append("<br />");
	}
	
	private String getContentTitleByXpath(String file, String contentId) {
		String title = "";
		String xpathQuery = "/book/content-items/descendant::content-item[attribute::id=\"" + contentId + "\"]/heading/title/text()";
		title = getValue(file, xpathQuery);
		
		return title;
	}
	
	private static String getValue(String inputFile, String expression) {		
		String value = "";
		Document dom = getXmlDom(inputFile);
		
		Node node = searchNode(dom, expression);
		
		if(null != node) {
			value = node.getNodeValue();
		}
		
		return value;
	}
	
	private static Document getXmlDom(String filename) { 
		Document doc = null;
		File file = new File(filename);
		try { 
			doc = factory.newDocumentBuilder().parse(file);	
		} catch (Exception e) { 
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		return doc;
	}
	
	private static Node searchNode(Node xmlDom, String xpathExpression) {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		Node node = null;
		try { 
			XPathExpression expr = xpath.compile(xpathExpression);			
			Object result = expr.evaluate(xmlDom, XPathConstants.NODE);	
			node = (Node) result;						
		} catch (Exception e) { 			
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
		return node;
	}
	
	@PostConstruct
	public void postInit() {
		logger.info("Initialize MDB...");	
		try {
			if (null == factory) {
				factory = DocumentBuilderFactory.newInstance();				
			}
		} catch (Exception e) {
			logger.error(ExceptionPrinter.getStackTraceAsString(e));
		}
	}
	
	@PreDestroy
	public void preDestroy() {
		logger.info("Destroying MDB...");	
	}
}