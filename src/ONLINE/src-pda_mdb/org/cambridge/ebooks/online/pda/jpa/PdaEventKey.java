package org.cambridge.ebooks.online.pda.jpa;

import java.io.Serializable;

import javax.persistence.Column;


@SuppressWarnings("serial")
public class PdaEventKey implements Serializable{
	
	public PdaEventKey(){
		
	}
	
	@Column(name="AGREEMENT_ID")
	private Integer agreementId;
	
	@Column(name="CONTENT_ID")
	private String contentId;
			
	public Integer getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(Integer agreementId) {
		this.agreementId = agreementId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@Override
	public int hashCode() {
	    return (int) contentId.hashCode() + (int) agreementId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result; 
		if (obj == this){
			result = true;
		}else if(!(obj instanceof PdaEventKey)){
			result = false;
		}else if(obj == null){
			result = false;
		}else{
			PdaEventKey pk = (PdaEventKey) obj;
		    result = pk.agreementId.equals(agreementId) && pk.contentId.equals(contentId);
		}
		return result;

	}

}

