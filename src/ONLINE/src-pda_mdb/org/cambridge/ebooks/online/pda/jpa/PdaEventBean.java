package org.cambridge.ebooks.online.pda.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@IdClass(PdaEventKey.class)
@Table ( name = "PDA_EVENT" )
public class PdaEventBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8007936651260261808L;

	@Id
	@Column(name="AGREEMENT_ID")
	private Integer agreementId;
	
	@Id
	@Column(name="CONTENT_ID")
	private String contentId;
	
	@Column(name="TIMESTAMP_CREATED")
	private Timestamp timestampCreated;
	
	@Column(name="IP_ADDRESS")
	private String ipAddress;
	
	@Column(name="REFERRER")
	private String referrer;
	
	@Column(name="SESSION_ID")
	private String sessionId;
	
	@Transient
	private String orderId;

	public Integer getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(Integer agreementId) {
		this.agreementId = agreementId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Timestamp getTimestampCreated() {
		return timestampCreated;
	}

	public void setTimestampCreated(Timestamp timestampCreated) {
		this.timestampCreated = timestampCreated;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	
	
}
