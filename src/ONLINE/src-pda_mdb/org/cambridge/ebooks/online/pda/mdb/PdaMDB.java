package org.cambridge.ebooks.online.pda.mdb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.pda.jpa.PdaEventBean;
import org.cambridge.ebooks.online.util.ExceptionPrinter;

@MessageDriven(name = "PdaMDB", activationConfig = {		
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/pda_mdb/logger") })	

public class PdaMDB implements MessageListener {
	
	private static final Logger logger = Logger.getLogger(PdaMDB.class);
	
	private static final String[] properties = new String[]{
		"contentId",
		"ipAddress",
		"orderId",
		"referrer",
		"sessionId"
	};

	public void onMessage(Message message) {
		logger.info( "PDA logger called!" );
		//UserTransaction ut = sc.getUserTransaction();
		try {
			if (message instanceof Message) {						
				try {			
					//ut.begin();	
					
					em.getTransaction().begin();
					PdaEventBean report = generateEventBean(message);
					
					Query q = em.createNativeQuery("{ call pkg_pda.insert_pda_event(?1,?2,?3,?4,?5) }");
					q.setParameter(1, report.getOrderId());
					q.setParameter(2, report.getContentId());
					q.setParameter(3, report.getIpAddress());
					q.setParameter(4, report.getReferrer());
					q.setParameter(5, report.getSessionId());					
					
					q.executeUpdate();
					em.getTransaction().commit();					
				} catch ( Exception e ) {
					logger.error("Message:" + ExceptionPrinter.getStackTraceAsString(e));
					em.getTransaction().rollback();
				} finally {
					
				}
			}
		} catch (Exception e) {
			logger.error("[JMSException]:Uploader EJB:"+ ExceptionPrinter.getStackTraceAsString(e));

		}

	}
	

	
	private PdaEventBean generateEventBean(Message message){
		
		PdaEventBean bean = new PdaEventBean();
		for(String prop : properties){
			String value = "";
			try {
				value = message.getStringProperty(prop);
			} catch (JMSException e) {
				continue;
			}
			try {				
				BeanUtils.setProperty(bean, prop, value);				
			} catch (Exception e) {			
				e.printStackTrace();
			}		
		}
		return bean;
	}
	

		
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("PdaUnit");

	private EntityManager em;
	
	@PostConstruct
    void init() {
		Logger.getLogger( PdaMDB.class ).info( " Initialize EJB " );		
		em = emf.createEntityManager();
    }

    @PreDestroy
    void cleanUp() {
    	Logger.getLogger( PdaMDB.class ).info( " Destroy EJB " );
    	em.close();
    }
}
