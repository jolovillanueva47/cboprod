
package org.cambridge.ebooks.alerts.util;

import static org.cambridge.ebooks.alerts.util.EBooksConfiguration.getProperty;

public interface EBooksProperties {

	public static final String CROSSREF_USERNAME = getProperty("crossref.username");
	public static final String CROSSREF_PASSWORD = getProperty("crossref.password");
	public static final String CROSSREF_FORWARD_URL = getProperty("crossref.forward.url");
	
}
