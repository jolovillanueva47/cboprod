package org.cambridge.ebooks.alerts.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author rvillamor
 *
 */

public class EBooksConfiguration {
	static Logger LOGGER = Logger.getLogger(EBooksConfiguration.class);

	public static String getProperty(String propertyName){
		String result = "";
		
		if (StringUtils.isEmpty( System.getProperty(propertyName)) )
		{			
			LOGGER.error("Property name: " + propertyName + " has not been set on property-service.xml");
		}
		else
		{
			result = System.getProperty(propertyName).trim();
		}
		
		return result;
		
	}
	
}
