package org.cambridge.ebooks.alerts.util;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

/**
 * 
 * @author cmcastro
 * @author C2
 *
 */

public class PersistenceUtil {
	static Logger logger = Logger.getLogger(PersistenceUtil.class);
	
	public enum PersistentUnits {
		EBOOKS("eBooksService");
		
		private PersistentUnits(String name ) {
			this.name = name;
		}
		
		private String name;
		
		public String toString() {
			return name;
		}
	}

	private static PersistentUnits BookService = PersistentUnits.EBOOKS;
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static <T> void updateEntity(Object obj) {
		EntityManager em = emf.createEntityManager();
		em.merge((T) obj);
		try {
			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Error in updating entity. " + e.getMessage());
		} finally {
			em.close();
		}
	}
	
	public static void updateEntity(String namedQuery, String... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Query query = em.createNamedQuery( namedQuery );
		
		if ( params != null && params.length > 0 ) { 
			int ctr = 1;
		    for ( String param : params ) { 
		    	query.setParameter( ctr++, param); 
		    }
		}
		
		query.executeUpdate();
		em.getTransaction().commit();
	
		em.close();				
	}
	
	public static void updateEntity(String namedQuery, Object... params) throws Exception {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Query query = em.createNamedQuery( namedQuery );
		
		if ( params != null && params.length > 0 ) { 
			int ctr = 1;
		    for ( Object param : params ) { 
		    	query.setParameter( ctr++, param); 
		    }
		}
		
		query.executeUpdate();
		em.getTransaction().commit();
	
		em.close();				
	}
	
	public static <T> ArrayList<T> searchList( T t, String namedQuery, String... params ) { 
		ArrayList<T> result = new ArrayList<T>();
		
		EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery( namedQuery );
        
        if ( params != null && params.length > 0 ) { 
        	int ctr = 1;
	        for ( String param : params ) { 
	        	query.setParameter( ctr++, param); 
	        }
        }
        
        try { 
        	result = (ArrayList<T>) query.getResultList();
        } catch (NoResultException nre ) { 
        	logger.error(" I did not get any result from: " + BookService.toString() + " named query is: " + namedQuery );
        } finally {
        	em.close();
        }
		return  result;
	}
	
	
	public static <T> T searchEntity(T t, String namedQuery, String... params ) { 
		T result = t;
		
		EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery( namedQuery );
        
        if ( params != null && params.length > 0 ) { 
        	int ctr = 1;
	        for ( String param : params ) { 
	        	query.setParameter( ctr++, param); 
	        }
        }
        
        try { 
        	result = (T) query.getSingleResult();
        } catch (NoResultException nre ) { 
        	result = null;
        	logger.error(" I did not get any result from: " + BookService.toString() + " named query is: " + namedQuery );
        } finally {
        	em.close();
        }
		return  result;
	}
	
	
	public static <T> void updateEntities( T t, String namedQuery, String... params ) { 
		
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
        Query query = em.createNamedQuery( namedQuery );
        
        
        if ( params != null && params.length > 0 ) { 
        	int ctr = 1;
	        for ( String param : params ) { 
	        	query.setParameter( ctr++, param); 
	        }
        }
        
        try { 
        	query.executeUpdate();
        	em.getTransaction().commit();
        } catch (NoResultException nre ) { 
        	logger.error("Update failed.. " + " named query is: " + namedQuery );
        } finally {
        	em.close();
        }
		
	}
	
	public static <T> ArrayList<T> dynamicSearchList( T t, String queryStr, String resultSetMapping, ArrayList<String> params ) { 
		ArrayList<T> result = new ArrayList<T>();
		
		EntityManager em = emf.createEntityManager();        
		Query query = em.createNativeQuery( queryStr, resultSetMapping );
		
		if ( params != null && params.size() > 0 ) { 
			int ctr = 1;
			for(String param : params){
				query.setParameter(ctr++, param);
			}
		}
		
        try { 
        	result = (ArrayList<T>) query.getResultList();
        } catch (NoResultException nre ) { 
        	logger.error(" I did not get any result from: " + BookService.toString() + " dynamic query is: " + queryStr );
        } finally {
        	em.close();
        }
		return  result;
	}
	
}
