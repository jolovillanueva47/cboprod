package org.cambridge.ebooks.alerts;

/**
 * 
 * @author jgalang
 * 
 */

public class CitedBy {

	private String doi;

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}
	
}
