package org.cambridge.ebooks.alerts.crossref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.cambridge.ebooks.alerts.CitedBy;
import org.cambridge.ebooks.alerts.util.EBooksProperties;
import org.w3c.dom.Document;

/**
 * 
 * @author jgalang
 * 
 */

public class CrossRefCitedByLinking implements EBooksProperties{
	static Logger logger = Logger.getLogger(CrossRefCitedByLinking.class);

	 private final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	 public static ArrayList<CitedBy> getCitedByLinks(ArrayList<String> doiList) {
		 ArrayList<CitedBy> citedBy = new ArrayList<CitedBy>();
		 
		 for (String doi : doiList) {
			 try {
				 DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
				 DocumentBuilder b = f.newDocumentBuilder();
				 
				 StringBuffer uri = new StringBuffer(CROSSREF_FORWARD_URL);
				 uri.append("?usr=");
				 uri.append(CROSSREF_USERNAME);
				 uri.append("&pwd=");
				 uri.append(CROSSREF_PASSWORD);
				 uri.append("&doi=");
				 uri.append(doi);
				 uri.append("&startDate=");
				 uri.append(format.format(new Date()));
				 
//				 System.out.println("petsa: " + format.format(new Date()));
				 
				 Document d = b.parse(uri.toString());
//				 System.out.println("stap!");
			 } catch (Exception e) {
				 logger.error(e.getMessage());
			 }
		 }
		 
		 
		 return citedBy;
	 }
}
