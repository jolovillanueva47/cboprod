package org.cambridge.ebooks.alerts;

import java.util.ArrayList;

import org.cambridge.ebooks.alerts.crossref.CrossRefCitedByLinking;
import org.cambridge.ebooks.alerts.util.PersistenceUtil;
import org.cambridge.ebooks.online.jpa.citation.alert.CitationAlert;

/**
 * 
 * @author jgalang
 * 
 */

public class EmailAlerts {

//	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistentUnits.EBOOKS.toString());
	
	public static void runAlerts() {
		ArrayList<String> doiList = PersistenceUtil.searchList(new String(), CitationAlert.SEARCH_ALL_DOI);
		System.out.println(doiList);
		
		ArrayList<CitedBy> citedByLinks =  CrossRefCitedByLinking.getCitedByLinks(doiList);
	}
}
