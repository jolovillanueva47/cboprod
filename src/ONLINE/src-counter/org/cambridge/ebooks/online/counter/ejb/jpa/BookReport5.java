package org.cambridge.ebooks.online.counter.ejb.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table ( name = BookReport5.TABLE_NAME )
public class BookReport5  implements Serializable {	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1168300678453021560L;

	public static final String TABLE_NAME = "BOOK_REPORT5";		
	
	@Id
	@GeneratedValue(generator=CounterBean.SEQUENCE)
    @SequenceGenerator(name=CounterBean.SEQUENCE, sequenceName=CounterBean.SEQUENCE, allocationSize=1)	
	@Column( name = CounterBean.LOG_ID )
	private int logId;

	@Column( name = CounterBean.PUBLISHER )
	private String publisher;
	
	@Column( name = CounterBean.PLATFORM )
	private String platform;
	
	@Column( name = CounterBean.SESSION_ID )
	private String sessionId;
	
	@Column( name = CounterBean.COMPONENT_ID )
	private String componentId;	
	
	@Column( name = CounterBean.BODY_ID)
	private int bodyId;
	
	@Column( name = CounterBean.TIMESTAMP )
	private Timestamp timestamp;

	@Column( name = CounterBean.ISBN)
	private String isbn;	
	
	@Column( name = CounterBean.ISSN)
	private String issn;
	
	@Column( name = CounterBean.TITLE)
	private String title;
	
	@Column( name = CounterBean.IS_SEARCH )
	private String isSearch;
	
	public String getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(String isSearch) {
		this.isSearch = isSearch;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLogId() {
		return logId;
	}

	public void setLogId(int logId) {
		this.logId = logId;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getComponentId() {
		return componentId;
	}


	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getBodyId() {
		return bodyId;
	}

	public void setBodyId(int bodyId) {
		this.bodyId = bodyId;
	}
}
