package org.cambridge.ebooks.online.counter.ejb.jpa;

import java.sql.Timestamp;

/**
 * 
 * @author mmanalo
 *
 */
public class CounterBean {
	
	public static final String LOG_ID = "LOG_ID";
	
	public static final String LOG_TYPE = "LOG_TYPE";
	
	public static final String SERVICE = "SERVICE";
	
	public static final String ISBN = "ISBN";
	
	public static final String ISSN = "ISSN";
	
	public static final String TITLE = "TITLE";
	
	public static final String PUBLISHER = "PUBLISHER";
	
	public static final String PLATFORM = "PLATFORM";
	
	public static final String SESSION_ID = "SESSION_ID";
	
	public static final String COMPONENT_ID = "COMPONENT_ID";
	
	public static final String BODY_ID = "BODY_ID";
	
	public static final String IS_SEARCH = "IS_SEARCH";
	
	public static final String TIMESTAMP = "TIMESTAMP";	
	
	public static final String SEQUENCE = "BOOK_REPORT_SEQ";
	
	public static final String CHECK_TIME = "CHECK_TIME";
	
	public static final int PDF = 30;
	
	public static final int HTML = 10;
	
	private int logId;	
	private int logType;
	private String service;
	private String isbn;
	private String issn;
	private String title;
	private String publisher;
	private String platform;
	private String sessionId;
	private String componentId;
	private String isSearch;
	private Timestamp timestamp;
	private int bodyId;
	private int checkTime;
	
	
	

	public int getCheckTime() {
		return checkTime;
	}

	public CounterBean setCheckTime(int checkTime) {
		this.checkTime = checkTime;
		return this;
	}

	public int getBodyId() {
		return bodyId;
	}

	public CounterBean setBodyId(int bodyId) {
		this.bodyId = bodyId;
		return this;
	}

	public int getLogId() {
		return logId;
	}

	public CounterBean setLogId(int logId) {
		this.logId = logId;
		return this;
	}

	public int getLogType() {
		return logType;
	}

	public CounterBean setLogType(int logType) {
		this.logType = logType;
		return this;
	}

	public String getService() {
		return service;
	}

	public CounterBean setService(String service) {
		this.service = service;
		return this;
	}

	public String getIsbn() {
		return isbn;
	}

	public CounterBean setIsbn(String isbn) {
		this.isbn = isbn;
		return this;
	}

	public String getIssn() {
		return issn;
	}

	public CounterBean setIssn(String issn) {
		this.issn = issn;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public CounterBean setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getPublisher() {
		return publisher;
	}

	public CounterBean setPublisher(String publisher) {
		this.publisher = publisher;
		return this;
	}

	public String getPlatform() {
		return platform;
	}

	public CounterBean setPlatform(String platform) {
		this.platform = platform;
		return this;
	}

	public String getSessionId() {
		return sessionId;
	}

	public CounterBean setSessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	public String getComponentId() {
		return componentId;
	}

	public CounterBean setComponentId(String componentId) {
		this.componentId = componentId;
		return this;
	}

	public String getIsSearch() {
		return isSearch;
	}

	public CounterBean setIsSearch(String isSearch) {
		this.isSearch = isSearch;
		return this;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public CounterBean setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
		return this;
	}
	

}
