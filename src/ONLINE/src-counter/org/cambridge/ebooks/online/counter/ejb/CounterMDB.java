package org.cambridge.ebooks.online.counter.ejb;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cambridge.ebooks.online.counter.ejb.jpa.CounterBean;
import org.cambridge.ebooks.online.counter.util.DateUtil;

@MessageDriven(name = "CounterMDB", activationConfig = {		
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/counter/logger") })	
@TransactionManagement(TransactionManagementType.BEAN)	
/**
 * @author mmanalo
 */
public class CounterMDB implements MessageListener {
	static Logger logger = Logger.getLogger(CounterMDB.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1852339708617688013L;

	public static final String BR1 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport1";
	
	public static final String BR2 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport2";
	
	public static final String BR3 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport3";
	
	public static final String BR4 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport4";
	
	public static final String BR5 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport5";
	
	public static final String BR6 = "org.cambridge.ebooks.online.counter.ejb.jpa.BookReport6";

	public static final String TIMESTAMP_BEAN = "timestamp";
	
	public static final String PERSISTENCE_NAME = "CounterReportService";
	
	@Resource
	private MessageDrivenContext mdc;
	
	@Resource
    private SessionContext sc;
	
	@PersistenceUnit( unitName = PERSISTENCE_NAME )	
	private EntityManagerFactory emf;

	private EntityManager em;	
	
	/**
	 * implemented method
	 */
	public void onMessage(Message payload) {
		logger.info( " CounterMDB called " );
		
		
		UserTransaction ut = sc.getUserTransaction();
		
		try {			
			ut.begin();			
			if(StringUtils.isNotEmpty(payload.getStringProperty(CounterBean.LOG_TYPE))){
				//recreate the message object
				CounterBean cb = createCounterBean(payload);				
				//logs the record (regards to business logic)
				logRecord(cb, ut);
			}			
		} catch (NumberFormatException e) {			
			logger.error( e.getMessage() );
			e.printStackTrace();
		} catch (JMSException e) {			
			logger.error( e.getMessage() );
			mdc.setRollbackOnly();
		} catch (ParseException e) {			
			logger.error( e.getMessage() );
		} catch (Exception e) {			
			logger.error( e.getMessage() );			
		} finally {
			
		}					
	}	

	/**
	 * recreates the message into an object
	 * @param payload
	 * @return
	 * @throws NumberFormatException
	 * @throws JMSException
	 * @throws ParseException
	 */
	private CounterBean createCounterBean(Message payload) throws NumberFormatException, JMSException, ParseException{
		CounterBean cb = new CounterBean();
		cb.setLogType(Integer.parseInt(payload
				.getStringProperty(CounterBean.LOG_TYPE)));
		cb.setService(payload.getStringProperty(CounterBean.SERVICE));
		cb.setIsbn(payload.getStringProperty(CounterBean.ISBN));
		cb.setIssn(payload.getStringProperty(CounterBean.ISSN));
		cb.setTitle(payload.getStringProperty(CounterBean.TITLE));
		cb.setPublisher(payload.getStringProperty(CounterBean.PUBLISHER));
		cb.setPlatform(payload.getStringProperty(CounterBean.PLATFORM));
		cb.setSessionId(payload.getStringProperty(CounterBean.SESSION_ID));
		cb.setComponentId(payload.getStringProperty(CounterBean.COMPONENT_ID));
		cb.setIsSearch(payload.getStringProperty(CounterBean.IS_SEARCH));
		cb.setTimestamp(DateUtil.convStrToTs(payload
				.getStringProperty(CounterBean.TIMESTAMP)));
		cb.setBodyId(Integer.parseInt(payload.getStringProperty(CounterBean.BODY_ID)));
		cb.setCheckTime(Integer.parseInt(payload.getStringProperty(CounterBean.CHECK_TIME)));
		System.out.println("payload ts = " + payload.getStringProperty(CounterBean.TIMESTAMP));
		
		return cb;
	}
	
	/**
	 * retrives the class path of the type of report
	 * @param cb
	 * @return
	 */
	private String getClassPath(CounterBean cb) {
		int logType = cb.getLogType();
		String classPath; 
		switch(logType) {
			case 1: classPath = BR1; break;
			case 2: classPath = BR2; break;
			case 3: classPath = BR3; break;
			case 4: classPath = BR4; break;
			case 5: classPath = BR5; break;
			case 6: classPath = BR6; break;
			default: classPath = null;
		}		
		return classPath;
	}
		
	/**
	 * business logic for logging the record
	 * @param cb
	 * @throws Exception
	 */
	private void logRecord(CounterBean cb, UserTransaction ut) throws Exception{		
		try{			
			BigDecimal logId = (BigDecimal) findMaxRecord(cb);
			logger.info("logId = " + logId);
			logger.info("table BR = " + cb.getLogType());
			if(logId == null){					
				insertRecordLogic(cb);
			}else{
				Object bookReport = em.find(newInstance(cb).getClass(), logId.intValue());
				String tsStr = BeanUtils.getProperty(bookReport, TIMESTAMP_BEAN);
				logger.info("ts Str = " + tsStr);
				Timestamp ts = DateUtil.convStrToTs(tsStr);				
				
				int logType = cb.getLogType();
				if(Math.abs((ts.getTime() - cb.getTimestamp().getTime())/1000) <= cb.getCheckTime()
						&& (logType == 2)){
					BeanUtils.setProperty(bookReport, TIMESTAMP_BEAN, cb.getTimestamp());
					em.joinTransaction();
					em.merge(bookReport);					
				}else{
					insertRecordLogic(cb);
				}
			}	
			ut.commit();
		}catch(Exception e){
			e.printStackTrace();
			ut.rollback();			
		}
	}
	
	/**
	 * inserts a new record into the DB
	 * @param cb
	 * @throws Exception
	 */
	private void insertRecordLogic(CounterBean cb)throws Exception {
		int logType = cb.getLogType();
		//System.out.println("dumaan");
		if(5 == logType || 6 == logType){
			//System.out.println("dumaan2");
			//if search == y log agad
			if(cb.getIsSearch().equals("Y")){
				insertNewRecord(cb);				
			}else{
				BigDecimal logId = (BigDecimal) sessionLog(cb);
				if(logId == null){
					//System.out.println("dumaan3");
					insertNewRecord(cb);
				}
			}
		}else{
			insertNewRecord(cb);
		}
	}
	
	/**
	 * inserts a new record into the DB 
	 * @param cb
	 * @throws Exception
	 */
	private void insertNewRecord(CounterBean cb) throws Exception {
		Object bean = newInstance(cb);
		BeanUtils.copyProperties(bean, cb);		
		em.joinTransaction();
		em.persist(bean);
	}
	
	
	/**
	 * checks so that no same session will be logged if is search == 'N'
	 * @param cb
	 * @return
	 * @throws Exception
	 */
	private Object sessionLog(CounterBean cb) throws Exception {
		if(cb.getLogType() == 5){
			String sql = 
				"select LOG_ID " +
				"from BOOK_REPORT" + cb.getLogType() + " " +
				"where SESSION_ID = ?1 " +
				"  and ISBN =  ?2 " + 
				"  and IS_SEARCH = 'N' " +
				"  and BODY_ID = ?3 ";
			Query query = em.createNativeQuery(sql);
			query.setParameter(1, cb.getSessionId());
			query.setParameter(2, cb.getIsbn());
			query.setParameter(3, cb.getBodyId());
			try {
				return query.getSingleResult();
			} catch (javax.persistence.NoResultException e) {			
				//e.printStackTrace();
			}
		}else{
			String sql = 
				"select LOG_ID " +
				"from BOOK_REPORT" + cb.getLogType() + " " +
				"where SESSION_ID = ?1 " +
				"  and SERVICE =  ?2 " + 
				"  and IS_SEARCH = 'N' " +
				"  and BODY_ID = ?3 ";
			Query query = em.createNativeQuery(sql);
			query.setParameter(1, cb.getSessionId());
			query.setParameter(2, cb.getService());
			query.setParameter(3, cb.getBodyId());
			try {
				return query.getSingleResult();
			} catch (javax.persistence.NoResultException e) {			
				//e.printStackTrace();
			}
			
		}
		return null;	
		
	}
	
	/**
	 * Create a new instance
	 * @param cb
	 * @return
	 * @throws Exception
	 */
	private Object newInstance(CounterBean cb) throws Exception {
		return Class.forName(getClassPath(cb)).newInstance();
	}
	
	/**
	 * finds the record to update if exists
	 * @param cb
	 * @return
	 */
	private Object findMaxRecord(CounterBean cb){
		int logType = cb.getLogType();
		Query query = null;
		
		//tables 5 and 6 have a distinct way to check max records
		//is search is required to find the proper max record
		if(logType == 5 || logType == 6){
			query = em.createNativeQuery(
					"select LOG_ID " +
					"from BOOK_REPORT" + cb.getLogType() + " a, " +
					    "(select max(TIMESTAMP) TIMESTAMP " + 
					    "from BOOK_REPORT" + cb.getLogType() + " " +
					    "where SESSION_ID = ?1  " +
					      "and IS_SEARCH = ?2 " +
					      "and BODY_ID = ?3) b " +
					"where a.TIMESTAMP = b.TIMESTAMP " +
					"and SESSION_ID = ?1 " +
					"and IS_SEARCH = ?2 " +
					"and BODY_ID = ?3"
					);
			query.setParameter(1, cb.getSessionId());
			query.setParameter(2, cb.getIsSearch());
			query.setParameter(3, cb.getBodyId());
		}else{
			query = em.createNativeQuery(
					"select LOG_ID " +
					"from BOOK_REPORT" + cb.getLogType() + " a, " +
					    "(select max(TIMESTAMP) TIMESTAMP " + 
					    "from BOOK_REPORT" + cb.getLogType() + " " +
					    "where SESSION_ID = ?1 " +
					    "and BODY_ID = ?2) b " +
					"where a.TIMESTAMP = b.TIMESTAMP " +
					"and SESSION_ID = ?1 " +
					"and BODY_ID = ?2" 
					);
			query.setParameter(1, cb.getSessionId());
			query.setParameter(2, cb.getBodyId());
		}
		try {
			return query.getSingleResult();
		} catch (javax.persistence.NoResultException e) {			
			//e.printStackTrace();
		}
		return null;
	}

	@PostConstruct
    void init() {
		logger.info( " Initialize EJB " );		
		em = emf.createEntityManager();
    }

    @PreDestroy
    void cleanUp() {
    	logger.info( " Destroy EJB " );
    	if(em.isOpen()){
    		em.close();
    	}
    }
}