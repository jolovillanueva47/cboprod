package org.cambridge.ebooks.online.counter.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

public class DateUtil {
	
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(TIMESTAMP_FORMAT);

	/**
	 * convert String to Timestamp
	 * @param str
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp convStrToTs(String str) throws ParseException{
		Logger.getLogger( DateUtil.class ).info( "String value of timestamp = " + str);
		return new Timestamp(SDF.parse(str).getTime());
	}
	
	/**
	 * Convert Timestamp to String
	 * @param ts
	 * @return
	 */
	public static String convTsToStr(Timestamp ts){
		return String.valueOf(ts);
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(convStrToTs(""));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
