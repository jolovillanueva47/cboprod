<?php
include('settings.php');
require_once 'lib/swift_required.php';

function my_exception_handler(Exception $ex) {
	error_log("Caught Exception ('{$ex->getMessage()}')\n", 3, $GLOBALS['logfilename']);
	rename(OUTBOX.$GLOBALS['file']['basename'], OUTBOX.$GLOBALS['file']['basename'].'.exception');
}

$logfilename = dirname(__FILE__).'/mail_'.date("Ymd").'.log';
$tmpfilename = dirname(__FILE__).'/mail.pid';
if (!($tmpfile = @fopen($tmpfilename,"w")))
	exit;
if (!@flock( $tmpfile, LOCK_EX | LOCK_NB, &$wouldblock) || $wouldblock) {
	@fclose($tmpfile);
	exit;
}

set_exception_handler("my_exception_handler");

$g = glob (OUTBOX.'*.mail');
if (count($g) <= 0) exit;	

$transport = Swift_SmtpTransport::newInstance(SMTP);
// TIMEOUT NOT DEFINED
$mailer = Swift_Mailer::newInstance($transport);

foreach ($g as $v) {
	$file = pathinfo($v);
	if ($file['extension'] == 'mail') {
		error_log('Processing '.OUTBOX.$file['basename'].'...', 3, $logfilename);
		$lines = file(OUTBOX.$file['basename']);
		foreach ($lines as $line_num => $line) {
			$segment = explode(':', $line, 2);
			if ($segment[0] == 'from') $from = $segment[1];
			elseif ($segment[0] == 'to') $tos = explode(',', $segment[1]);
			elseif ($segment[0] == 'cc') $ccs = explode(',', $segment[1]);
			elseif ($segment[0] == 'bcc') $bccs = explode(',', $segment[1]);
			elseif ($segment[0] == 'subject') $subject = '*** TEST *** '.$segment[1];
			elseif ($segment[0] == 'attachment') $attachments = explode(',', $segment[1]);
			elseif ($segment[0] == 'body') {
				$body = $segment[1];
				while (++$line_num < count($lines)) 
					$body .= $lines[$line_num];
				break;
			}
		}
		$message = Swift_Message::newInstance();
		$message->setSubject(trim($subject));
		$message->setFrom(trim($from));
		$message->setTo(array_map('trim', $tos));
		$message->setCc(array_map('trim', $ccs));
		$message->setBcc(array_map('trim', $bccs));
		$message->setBody(strip_tags($body));
		$message->addPart($body, 'text/html');
	   if (count($attachments) > 0) foreach ($attachments as $attachment) 
			if (strlen($attachment) > 0) $message->attach(Swift_Attachment::fromPath(trim($attachment)));
	   
	   
		if ($mailer->send($message)) {
			rename(OUTBOX.$file['basename'], ARCHIVE.$file['basename']);
			error_log("OK\n", 3, $logfilename);
		} else {
			error_log("ERROR\n", 3, $logfilename);
		}
		// delete temp attachment file
		if (count($attachments) > 0) foreach ($attachments as $attachment) {
			if (strlen($attachment) > 0) {
				unlink(trim($attachment));
			}
		}
	}
}
?>