package org.cambridge.ebooks.online.topbooks;

import java.util.Date;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.ResourceAdapter;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author mmanalo
 *
 */

@MessageDriven(
	activationConfig =
		{@ActivationConfigProperty(propertyName = "cronTrigger", propertyValue = "0 0 4 * * ?")
	}
)
@ResourceAdapter("quartz-ra.rar")
public class TopBooksMDB implements Job{
	
	
	private static final Logger logger = Logger.getLogger(TopBooksMDB.class);
	
	public TopBooksMDB(){
		logger.info("%%%%%^%&************************************************");
	}

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		long start = new Date().getTime();
		logger.info("Top books running once a day! Start");		
		logger.warn("Top books running once a day! Start");
		logger.error("Top books running once a day! Start");
		System.out.println("Top books running once a day! Start");
		TopBooksBean tb = new TopBooksBean();
		tb.getInitTopBooksSeries();
		long end = new Date().getTime();
		logger.info("Top books running once a day! End = " + (end - start)/1000d );
	}
	
}
