<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		<c:choose>
			<c:when test="${'usageStat' ne param.page}">
				<f:subview id="commonCssSubview">
					<c:import url="/components/common_css.jsp" />
				</f:subview>
				<link href="${pageContext.request.contextPath}/orgadmin/css/account_admin_styles.css" rel="stylesheet" type="text/css" />
			</c:when>
		</c:choose>

		<c:if test="${'access' eq param.page}">
		   <link href="${pageContext.request.contextPath}/orgadmin/css/jquery.treeview.css" rel="stylesheet" type="text/css" />
		</c:if>
		  
	</head>

	<body>	
	    <c:import url="/components/loader.jsp" />
	    
		<div id="main" style="display: none;">
	
		    <!-- bg Topline -->       
			<div id="top_container">					
				<c:remove var="publisherCode" scope="session"/>
	
				<!-- page-wrapper start -->
				
				<div id="page-wrapper">	
					<c:choose>
						<c:when test="${'usageStat' ne param.page}">
							<f:subview id="subviewIPLogo">
								<c:import url="/components/ip_logo.jsp" />	
							</f:subview>
						</c:when>
					</c:choose>
		
					<!-- Header -->
					<c:choose>
						<c:when test="${'usageStat' ne param.page}">
							<div id="header_container">	        	
								<f:subview id="top_menu">
									<c:import url="/components/top_menu.jsp" />
									<input type="hidden" value="${faqBean.cookieValue}" name="languageCookie"/>	
								</f:subview>	
								
								<f:subview id="search_container">
									<c:import url="/components/search_container.jsp" />	
								</f:subview>
								
					   	 	</div>	
					   	 </c:when>
			   	 	</c:choose>
			   	 	
		        	${administratorBean.initialize}
		        	
		        	<!-- Small Title -->
		        	<c:choose>
						<c:when test="${'usageStat' ne param.page}">
				        	<c:choose>
		        				<c:when test="${'configIp' eq param.page or 'configIpCons' eq param.page}">
		        					Configure IP Address
		        				</c:when>
		        				<c:when test="${'access' eq param.page}">
		        					Organisational Access Details
		        				</c:when>
		        				<c:when test="${'resolver' eq param.page}">
		        					Open URL Resolver
		        				</c:when>
		        				<c:when test="${'switchAccounts' eq param.page}">
		        					Switch Accounts
		        				</c:when>
		        				<c:when test="${'usageStat' eq param.page}">
		        					Switch Accounts
		        				</c:when>
		        				<c:when test="${'logo' eq param.page}">
        							Update Organisation Logo
        						</c:when>
        						<c:when test="${'remote' eq param.page}">
        							Remote User Access
        						</c:when>
        						<c:when test="${'admin' eq param.page}">
        							Change Administrator
        						</c:when>
				        	</c:choose>
				    	</c:when>
				    </c:choose>    	
		        	
		        	<!-- Crumbtrail -->
					<div style="display: none;">
						<f:subview id="crumbtrail">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>
					</div>
	
		        	
		        	<!-- Main Body Start -->
		       		<div id="main_container">		
			       		<c:choose>
							<c:when test="${'usageStat' ne param.page}">
								<f:subview id="menu">
									<c:import url="/orgadmin/components/administrator/menu.jsp" />	
								</f:subview>
		    				</c:when> 
						</c:choose>       			
			      
		       			<div id="adminContent" class="text_01">			        		 
			        		<c:choose>			
			        			<c:when test="${'configIpCons' eq param.page}">
		        					<f:subview id="configureIPDomainConsortia">
										<c:import url="/orgadmin/components/administrator/configure_ip_address_consortia.jsp" />	
									</f:subview>
		        				</c:when>
			        			<c:when test="${'access' eq param.page}">
		        					<f:subview id="subscriptionDetails">
										<c:import url="/orgadmin/components/administrator/subscription_details.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'remote' eq param.page}">
		        					<f:subview id="remoteUserAccess">
										<c:import url="/orgadmin/components/administrator/remote_user_access.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'usageStat' eq param.page}">
	        						<f:subview id="counterReports">
										<c:import url="/orgadmin/components/administrator/popups/counter_reports.jsp" />
									</f:subview>
	        					</c:when>
		        				<c:when test="${'resolver' eq param.page}">
		        					<f:subview id="openURLResolver">
										<c:import url="/orgadmin/components/administrator/open_url_resolver.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'orgDetails' eq param.page}">
		        					<f:subview id="updateOrganisationDetails">
										<c:import url="/orgadmin/components/administrator/update_organisation_details.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'hosting' eq param.page}">
		        					<f:subview id="organisationExpiryAlertDetail">
										<c:import url="/orgadmin/components/administrator/organisationExpiryAlertDetail.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'admin' eq param.page}">
		        					<f:subview id="changeAdministrator">
										<c:import url="/orgadmin/components/administrator/change_administrator.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'logo' eq param.page}">
		        					<f:subview id="updateLogo">
										<c:import url="/orgadmin/components/administrator/update_logo.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'pda' eq param.page}">
	        						<f:subview id="pda_batches">
										<c:import url="/orgadmin/components/administrator/pda_batches.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'email' eq param.page}">
 		        					<f:subview id="set_admin_type">
										<c:import url="/orgadmin/components/administrator/set_admin_type.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'switchAccounts' eq param.page}">
 		        					<f:subview id="set_admin_type">
										<c:import url="/orgadmin/components/administrator/switch_accounts.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'newMember' eq param.page}">
 		        					<f:subview id="new_member">
										<c:import url="/orgadmin/components/administrator/new_member.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:otherwise>
		        					<f:subview id="configure_ip_address">
										<c:import url="/orgadmin/components/administrator/configure_ip_address.jsp" />
									</f:subview>
		        				</c:otherwise>	        				
	        				</c:choose> 
		       			</div>		        	
					</div>
					
					<div class="clear"></div>
					<c:choose>			
			        	<c:when test="${'usageStat' ne param.page}">
							<f:subview id="footer_menuwrapper">
								<c:import url="/components/footer_menuwrapper.jsp" />
							</f:subview>
						</c:when>
					</c:choose>	        
				</div>
				<!-- page-wrapper end -->
			</div>
	    	<c:choose>			
				<c:when test="${'usageStat' ne param.page}">
					<f:subview id="footer_container">
						<c:import url="/components/footer_container.jsp" />
					</f:subview>
				</c:when>
			</c:choose>
	    </div>
	    
		<f:subview id="common_js">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		<f:subview id="account_administrator_js">
			<c:import url="/orgadmin/components/administrator/account_administrator_js.jsp" />
		</f:subview>
		<c:if test="${'access' eq param.page}">
	    	<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/jquery.treeview.js"></script>
	    	<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/subscription.js"></script>
	    </c:if>
		<f:subview id="google_analytics">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>		
	</body>
</f:view>
</html>