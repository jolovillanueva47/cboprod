var lt = /<<<(?!<)/;
var gt = />>>(?!>)/;
var imageLoc = "/images/";

var classCollTitle = "bookTitle"

function printBooks(data) {
	var line = data.split(gt);
}

function findBookCallback(data, id) {	
	var output = "";
	output = output + "<li class='" + id + "' style='display:block'>";
	output = output + "<ul class='subTree'>";
	var line = data.split(gt);
	for(var i=0; i < line.length; i++) {
		_line = $.trim(line[i]);
		if(_line.length > 0 ) {			
			var lineItem = _line.split(lt);		
			output = output + "<li><a href='" + "/ebook.jsf?bid=CBO" 
				+ lineItem[1] + "'>" + lineItem[1] + "</a>";
			output = output  + "<ul><li>" + lineItem[2] + "</li></ul>";	
		}
	}
	output = output + "</ul></li>";
	
	//gets the right element to insert after
	var elem = $("#" + id).parent();
	if(!elem.next().hasClass(classCollTitle)) {
		if(elem.next()[0] != undefined) {
			elem = elem.next();
		}
	}
	elem.after(output);
}


/**
	Method for finding the books under collections
**/
function findBooks(id) {
	var elem = $("." + id);
	if(elem[0] == undefined) {
		$("#" + id).children().attr("src",imageLoc + "circle_loading.gif");
		var currentDns = $("#current_dns").val();
		$.post(currentDns + "orgAccessDetails", {ACTION : "FIND_BOOKS", COLLECTION_ID : id, sortBy : sortValue},
			function(data) {
				findItemsCallback(findBookCallback, data, id);
				$("#" + id).children().attr("src",imageLoc + "icon_minus.jpg");
			}
		);
	} else if(elem.css("display") == "block") {
		elem.css("display", "none");
		$("#" + id).children().attr("src",imageLoc + "icon_plus.jpg");
	} else {
		elem.css("display", "block");
		$("#" + id).children().attr("src",imageLoc + "icon_minus.jpg");
	}
	return false;
}

function printCollections(data,divId) {
	var line = data.split(gt);
	var	elem = $("#" + divId);
	elem.html("");
	elem.css("display","block");
	var output = "";
	output = output + "<ul id='tree'>";
	for(var i=0; i < line.length; i++) {
		_line = $.trim(line[i]);
		if(_line.length > 0) {
			var lineItem = _line.split(lt);		
			if(lineItem[2] == "null") {
				output = output + "<li class='bookTitle'><a href='javascript:void(0);' id='" + lineItem[0] + "' onclick='findBooks(\"" + lineItem[0] + "\")'><img src='" + imageLoc + "icon_plus.jpg'></img></a>" + lineItem[1] + "</li>";
			} else {
				output = output + "<li class='bookTitle'><a href='javascript:void(0);' id='" + lineItem[0] + "' onclick='findBooks(\"" + lineItem[0] + "\")'><img src='" + imageLoc + "icon_plus.jpg'></img></a>" + lineItem[1] + "</li>";
				output = output + "<li >" + lineItem[2] + "</li>";
			}			
		}
	}
	output = output + "</ul>";
	elem.html(output);
}

function printIndividual(data, divId) {
	var	elem = $("#" + divId);
	elem.html("");
	elem.css("display","block");
	var output = "";
	output = output + "<ul id='tree'>";
	output = output + "<li>";
	output = output + "<ul class='subTree'>";

	var line = data.split(gt);
	for(var i=0; i < line.length; i++) {
		_line = $.trim(line[i]);
		if(_line.length > 0) {
			var lineItem = _line.split(lt);			
			output = output + "<li><a href='" + "/ebook.jsf?bid=" 
				+ lineItem[0] + "'>" + lineItem[1] + "</a>";
			output = output  + "<ul><li>" + lineItem[2] + "</li></ul>";	
		}
	}
	output = output + "</ul>";
	output = output + "<li>";
	output = output + "</ul>";
	
	elem.html(output);
}

function collOrgCallback(data, divId) {	
	printCollections(data,divId);
}

function collIndiCallback(data, divId) {
	printIndividual(data, divId);
}

/**
	Generic callback that seeks if empty then executes the proper callback
**/
function findItemsCallback(callback, data, divId) {
	var elem = $("#" + divId);
	if(data.indexOf("<<<Empty>>>") != -1) {
		elem.html("No items found.");
		elem.css("display","block");
	} else {
		callback(data,divId);
	}
}

function showLoaderItems(elem) {
	elem.prev().children().children().attr("src",imageLoc + "circle_loading.gif");
}

function removerLoaderItems(elem) {
	elem.prev().children().children().attr("src",imageLoc + "arrow07.gif");
}

function findItems(action, divId, sortVal) {
	var elem = $("#" + divId);
	if(elem.css("display") == "block"){
		elem.css("display","none");
	} else if($.trim(elem.html()).length > 1 && elem.css("display") == "none") {
		elem.css("display","block");
	} else {
		showLoaderItems(elem);	
		var currentDns = $("#current_dns").val();
		$.post(currentDns + "orgAccessDetails", {ACTION : action, sortBy : sortVal},
			function (data) {				
				if(action == "COLL_ORG") {
					findItemsCallback(collOrgCallback, data, divId);
				} else if(action == "INDI_ORG") {
					findItemsCallback(collIndiCallback, data, divId);
				} else if(action == "INDI_DEMO") {
					findItemsCallback(collIndiCallback, data, divId);
				} else if(action == "COLL_DEMO") {
					findItemsCallback(collOrgCallback, data, divId);
				} else if(action == "INDI_SUBS") {
					findItemsCallback(collIndiCallback, data, divId);
				} else if(action == "COLL_SUBS") {
					findItemsCallback(collOrgCallback, data, divId);
				}
				removerLoaderItems(elem);
			}
		);
	}
}


/**
	Main menu items
**/
var tabsMain = ["Organisational,tab1","Demo,tab2","Free,tab3","Subscription,tab4","Patron,tab5"];

function disbleSiblings(element) {
	element.parent().parent().children().each(function() {
		$(this).children().removeClass("current");
	});
}

function enableMainTab(html){
	for(var i=0; i<tabsMain.length; i++ ) {			
		var keyVal = tabsMain[i].split(",");		
		if(html.indexOf(keyVal[0]) != -1) {
			//alert(2);			
			var tab = keyVal[1];			
			$("#tabcontentcontainer").children().css("display","none");
			$("#" + tab).css("display", "block");						
		}
	}
}

$(function() {
	var currentDns = $("#current_dns").val();
	var sortBy = getUrlParam("sortBy");
	
	$("ul#tablist li a").each(function () {			
		$(this).click(function() {
			//alert(1);
			var element = $(this);
			disbleSiblings(element);
			element.attr("class","current");
			enableMainTab(element.html());
			return false;
		})
	});
	
	if(2 == sortBy) {
		$("#sortBy option[value='2']").attr("selected", true);
	} else {
		$("#sortBy option[value='1']").attr("selected", true);
	}
			
	$("#sortBy").change(function(){		
		if(1 == $(this).val()){						
			window.parent.location = currentDns + "orgadmin/account_administrator.jsf?type=ao&page=access&sortBy=1";					
		} else {
			window.parent.location = currentDns + "orgadmin/account_administrator.jsf?type=ao&page=access&sortBy=2";					
		}
	});

	$("#rootNode").treeview({
		collapsed: true
	});

	$(".journal").click(function (event) {			
		//current li element
		var element = $($(event.currentTarget));
		
		//alert(element.html());
		
		//get the value of first ul
		var ul = $(element.children("ul")[0]);
		
		//get if display css prop
		var display = ul.css("display");
		
		if(display == "block"){
			//call ajax method
			getCollectionItems(element, ul);
		}
		//alert(ul.css("display"));	
	});		
});

function getCollectionItems(element, ul){	
	var contents = $.trim(element.html());
	//var newSublist = $("<li>testing natin</li>").appendTo($(ul));
	var id = getCollectionId(contents);
	id = element.children("input").attr("value");
	//alert(id);
	getHTMLItems(id, ul, element);
}

function getCollectionId(contents) {
	//alert(contents);
	var reg = new RegExp("</div>","gi");
	return contents.split(reg)[1].split(" ")[0];
}

function getHTMLItems(id, ul, element) {
	var currentDns = $("#current_dns").val();
	$.get(currentDns + "action/ebooks/getCollectionItems",
		{ID : id},
		function (collectionItems) {
			var newSublist = ul.html(collectionItems);
			$("#rootNode").treeview({
				add: newSublist				
			});
			//element.css("background-repeat","repeat-y");
			//alert(2);
		}
	);
}