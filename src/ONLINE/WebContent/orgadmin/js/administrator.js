$(function(){
	$(".topmenu_admin").attr("class", "topmenu_active");	
	
	$("#" + $("#page").val()).children().attr("class","menu_current");
	
	// Configure IP Domain -- START
	$("#newIpAddr").blur(function(){
		var trimed = $.trim($(this).val());
		$(this).val(trimed); // trim trailing spaces
	});
	$("#newIpList").blur(function(){
		var trimed = $.trim($(this).val());
		$(this).val(trimed); // trim trailing spaces
	});	
	$("#newIpAddrCons").blur(function(){
		var trimed = $.trim($(this).val());
		$(this).val(trimed); // trim trailing spaces
	});
	$("#newIpListCons").blur(function(){
		var trimed = $.trim($(this).val());
		$(this).val(trimed); // trim trailing spaces
	});	
	$("#confIpUpdate").click(function(){
		var isDeleteExists = $(".delete").size() > 0 ? true : false; 
		if(isDeleteExists) {
			if($(".delete:checked").length > 0) {
				showDeleteConfirmation("ao", "");
			} else {
				submitUpdate("ao", "");
			}
		} else {
			submitUpdate("ao", "");
		}
	});
	$(".confIpConsUpdate").click(function(){
		var isDeleteExists = $(".delete").size() > 0 ? true : false; 
		var method = $(this).attr("id");
		if(isDeleteExists) {
			if($(".delete:checked").length > 0) {
				showDeleteConfirmation("ac", method);
			} else {
				submitUpdate("ac", method);
			}
		} else {
			submitUpdate("ac", method);
		}
	});
	$(".exclude").change(function(){
		var val = $(this).val();
		if($(this).attr("checked")) {			
			$(".include[value='" + val + "']").attr("checked", false);
		} else {
			$(".include[value='" + val + "']").attr("checked", true);
		}
	});
	// Configure IP Domain -- END
	
	// Open URL Resolver -- Start
	$("#openURLUpdate").click(function(){
		submitOpenURLUpdate();
	});
	
	$("#openURLDelete").click(function(){
		submitOpenURLDelete();
	});
	// Open URL Resolver -- END
	
	//Update org logo
	$("#uploadLogo").click(function(){
		createHiddenThenSubmit('uploadLogoRequest','Upload Logo');
	});
	
	$("#removeLogo").click(function(){
		createHiddenThenSubmit('uploadLogoRequest','Remove Logo');
	});
	
	$("#doneLogo").click(function(){
		createHiddenThenSubmit('uploadLogoRequest','Done');
	});
	//Update org logo -- end
	
	//new remote user -- start
	$("#newMemberForm").validate({
		rules:{
			firstName: "required",
			surName: "required",
			country: "required",
			userName: "required",
			passWord: "required",
			password2: {
				required: true,
				equalTo: "#passWord"
			},
			email: {
				required: true,
				email: true
			},
			email2: {
				required: true,
				email: true,
				equalTo: "#email"
			}
		}
	});
	
	$("#country").change(function(){
		getCountyList();
	});
	//new remote user - end
	
	$("#organisationDetailsForm").validate({
		rules:{
			type: "required",
			name: "required",
			displayName: "required",
			country: "required",
			city: "required",
			postCode: "required",
			address1: "required"
		}
	});
	
});

function submitUpdate(type, method) {
	var currentDns = $("#current_dns").val();
	if("ao" == type) {
		$("#configureIpDomainForm").attr("action", currentDns + "orgadmin/account_administrator.jsf?type=ao&page=configIp&accept=Y&Update=1");
		$("#configureIpDomainForm").submit();
	} else if("ac" == type) {
		$("#configureIpConsForm").attr("action", currentDns + "orgadmin/account_administrator.jsf?type=ac&page=configIpCons&accept=Y&method=" + method);
		$("#configureIpConsForm").submit();
	}
}

function showDeleteConfirmation(type, method) {
	var currentDns = $("#current_dns").val();
	MM_openBrWindow(currentDns + "orgadmin/components/administrator/popups/configure_ip_confirmation_popup.jsf?type=" + type + "&method=" + method, "confirmation","status=yes,scrollbars=yes,width=350,height=300");
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
	window.open(theURL,winName,features);
	return false;
}
//Open url resolver methods -- start
function submitOpenURLUpdate(){
	if(!$("#newURLResolver").val()){
		alert("URL Resolver is required.");
	} else {
		var currentDns = $("#current_dns").val();
		$("#openURLResolverForm").attr("action", currentDns + "orgadmin/account_administrator.jsf?type=ao&page=resolver&UpdateURL=1");
		$("#openURLResolverForm").submit();
	}
}

function submitOpenURLDelete(){
	var currentDns = $("#current_dns").val();
	$("#openURLResolverForm").attr("action", currentDns + "orgadmin/account_administrator.jsf?type=ao&page=resolver&DeleteURL=1");
	$("#openURLResolverForm").submit();
}
//Open url resolver methods -- end

//Switch accounts methods --start
function doBeforeSubmit(id, cmdValue){
	jQuery("#"+id).after("<input type='hidden' name='Update' value='" + cmdValue + "'>");
	$("#switchAccountForm").submit();
	//document.getElementById(id).form.submit();
}
//Switch accounts methods --end

//Update Logo methods --start
function createHiddenThenSubmit(name,value){
	
	jQuery("#uploadLogo").after("<input type='hidden' name='"+name+"' value='"+value+"' >");
	$("#updateLogoForm").attr("action","/updateLogo");
	$("#updateLogoForm").submit();
}

//Update Logo methods --end

//remoteUseraccess-- start
function gotoNewMember(){
	$("#remoteUserForm").attr('action', '/orgadmin/account_administrator.jsf?type=ao&page=newMember');
	$("#remoteUserForm").submit();
}

function getCountyList(){
	var cId = $("#country").val();
	
	$("#county option").remove();
	$.ajax({
		type: "POST",
		url: "/updateCountyList?countryId="+cId,
		contentType: "application/json",
		dataType: "json",
		success: function(msg){
			jQuery.each(msg, function(key, val){
				if(key==0){
					$("#county").append("<option value='" + key+ "' selected='selected'>" + val + "</option>");
				} else {
					$("#county").append("<option value='" + key+ "'>" + val + "</option>");
				}
			});
		}
	});
}		
//remoteUseraccess-- end