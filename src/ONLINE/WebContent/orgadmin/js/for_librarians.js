$(function(){	
	
	$(".topmenu_librarian").attr("class", "topmenu_active");		
	
	// select all widget code on textarea
	$("#widget_code_textarea").click(function(){
		this.select();
	});
	
	// select all widget code on select all button
	$("#widget_code_button").click(function(){
		$("#widget_code_textarea").focus();
		$("#widget_code_textarea").select();
	});
	
	$("#browser_toolbar").click(function(){
		var currentDns = $("#current_dns").val();
		if (window.external && ("AddSearchProvider" in window.external)) {
			// Firefox 2 and IE 7, OpenSearch
			window.external.AddSearchProvider(currentDns + "widget?cmd=openSearch&xml=cboQuickSearch.xml&servlet=" + currentDns + "search?");
		} else if (window.sidebar && ("addSearchEngine" in window.sidebar)) {			
			// Firefox <= 1.5, Sherlock
			window.sidebar.addSearchEngine(currentDns + "widget?cmd=openSearch&xml=cboQuickSearch.xml&servlet=" + currentDns + "search?", currentDns + "favicon.ico", "cboQuickSearch", "Books"); 
		} else {			
			// No search engine support (IE 6, Opera, etc).
			alert("No search engine support");
		}
	});
	
	// MARC Records
	// init options
	if("mr" == $("#page").val()){
		$.ajax({
			url: $("#cjo_domain").val() + 'action/librariansUpdates/getRSSFileExist?callback=?',
			dataType: 'jsonp',
			success: function(data){
				if(data.rssExists != 'Y')
					$("div[id='rss_marcrecord']").hide();
			}
		});
		showCalendar();
		$("#ui-datepicker-div").css("display", "none");//hide the white line of the calendar
		showCriteria();
	}
	
	$("#marcRecordsOption").change(function(){
		showCriteria();
	});
	
	/*$("#dateFrom").change(function(){
		updateDateToList();
	})	*/

});

// MARC Records
function clearErrorMessage() {
	$("#errorMessage").html("");	
}

function showCalendar(){
	var dates = $( "#dateFrom, #dateTo" ).datepicker({
		dateFormat: 'dd-M-y',
		changeMonth: true,
		onSelect: function( selectedDate ) {
			var option = this.id == "dateFrom" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
			if(this.id=="dateFrom"){
				$("#dateTo").val($("#dateFrom").val());
			}
			
			dates.not( this ).datepicker( "option", option, date );
			
		}
	});
}

function showCriteria(){
	
	var marcRecordsOption = $("#marcRecordsOption").val();	
	$.post("/filterMarcRecords",{bodyId: marcRecordsOption, filterType: "byOrderId"}, 
		function(plain){
	
			var orderId = plain;				
			orderId = plain.replace(']','');//remove the opening square bracket
			orderId = orderId.replace('[','');//remove the ending square bracket
			orderIds = orderId.split(','); // turns into an array
			$("#orderIds option").remove();
			$("#orderIds").append(new Option("",""));
			jQuery.each(orderIds, function(){				
				$("#orderIds").append("<option value='" + this+ "'>" + this + "</option>");
 
			});
		}
	);
		/*$.post("/filterMarcRecords",{bodyId: marcRecordsOption, filterType: "byDate"},
		function(plain){
			var date = plain;
			date = plain.replace('\[',''); //remove the opening square bracket
			date = date.replace('\]',''); //remove the ending square bracket			
			dates = date.split(','); // turns into an array
			$("#dateFrom option").remove();
			$("#dateTo option").remove();
			$("#dateFrom").append(new Option("",""));
			jQuery.each(dates, function(){				
				$("#dateFrom").append("<option value='" + this + "'>" + this + "</option>");
			});
		}
	);*/
}

/*
function updateDateToList(){
	if("" == $("#dateFrom").val()){
		$("#dateTo option").remove();
	} else {		
		var startDateTo = $("#dateFrom option:selected").index();
		var dateFromLength = $("#dateFrom option").size();
		$("#dateTo option").remove();

		var dates = $("#dateFrom option").slice(startDateTo, dateFromLength);
		jQuery.each(dates,function(){
			$("#dateTo").append("<option value='" + $(this).val() + "'>" + $(this).val() + "</option>");
		});
	}
}*/