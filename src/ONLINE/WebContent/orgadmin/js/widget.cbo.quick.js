/*
 * google apis -> http://code.google.com/apis/libraries/devguide.html
 * jquer-ui themes -> http://www.stemkoski.com/jquery-ui-1-7-2-themes-list-at-google-code/
 * main code widget -> http://alexmarandon.com/articles/web_widget_jquery/
 * core javascript -> http://www.comptechdoc.org/independent/web/cgi/javamanual/javalocation.html
 * 
 * */

/*
 *-jquery script can appear anywhere in the document
 * -cant get any ids in the starting codes
 * -when two widget is added
 * 
 * */

function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = "";
};

(function() {

	// Localize jQuery variable
	var jQuery;

	/******** Load jQuery if not present *********/
	if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
		var script_tag = document.createElement('script');
	    script_tag.setAttribute("type","text/javascript");
	    script_tag.setAttribute("src", "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
	    
	    var noOnload = false;
		if (window.navigator.userAgent.indexOf('Opera') > -1) {
			version = navigator.userAgent.substring(navigator.userAgent.indexOf('v'));
			if(version.indexOf('9.64') > -1){
				noOnload = true;
			}
		}  
		
		if(noOnload){
			//alert("no onload");
		}else{
			script_tag.onload = scriptLoadHandler;
		}

	    script_tag.onreadystatechange = function () { // Same thing but for IE
	        if (this.readyState == 'complete' || this.readyState == 'loaded') {
	        	scriptLoadHandler();
	        }
	    };
	    // Try to find the head, otherwise default to the documentElement
	    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
 
	} else {
	    // The jQuery version on the window is the one we want to use
	    jQuery = window.jQuery;
	    main();
	}

	/******** Called once jQuery has loaded ******/
	function scriptLoadHandler() {
	    // Restore $ and window.jQuery to their previous values and store the
	    // new jQuery in our local jQuery variable
		//alert("calling function");
		//loadUi();
		jQuery = window.jQuery.noConflict(true);
	    // Call our main function
	    main(); 
	}
	
	function loadUi(){
		//alert("im in ui");
	   var script_tag_2 = document.createElement('script');
	   script_tag_2.setAttribute("type","text/javascript");
	   script_tag_2.setAttribute("src", "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js");
	   
	   var css_tag_2 = document.createElement('link');
	   css_tag_2.setAttribute("type","text/css");
	   css_tag_2.setAttribute("rel","stylesheet");
	   css_tag_2.setAttribute("href","http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css");

	   (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(css_tag_2);
	   (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag_2);
	}

	function getContextPath(){
		return location.protocol + "//" + location.host;
	};	  

	/******** Our main function ********/
	function main() { 
	    jQuery(document).ready(function($) { 
	    	
	    	/******* Load CSS *******/
	       // var css_link = $("<link>", { 
	         //   rel: "stylesheet", 
	           // type: "text/css", 
	           // href: "style.css" 
	       // });
	       // css_link.appendTo('head');          

	        //load the ui
	    	//alert("jquery="+window.jQuery.fn.jquery);
	    	
	    	//if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
	    		
	    		
	    	//}
	    	
	    	
	    
	    	//var jquery_ui = $("<script>", { 
	          //  type: "text/javascript", 
	           // src: "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js" 
	        //});
	    	//jquery_ui.appendTo('head');   
	    	
	        /******* Load HTML *******/
	        //var jsonp_url = "http://alpage.org/cgi-bin/webwidget_tutorial.py?callback=?";
	        //$.getJSON(jsonp_url, function(data) {
	          //$('#example-widget-container').html("This data comes from another server: " + data.html);
	       // });
	    	
	    	/******** process validation ********/
    		$("#searchFormWidget_quick").bind("keypress", function(e) {
    			if(e.keyCode==13){
    				e.preventDefault();
    	        	$("#searchLink_cboWidget_quick").click();
            	}
            });
    		$("#searchLink_cboWidget_quick").click(function(event){
    			event.preventDefault();
    			var ebooksContextPath = $("#resultContext_js").val();
    			
    			var valid = false;
    			var term = jQuery.trim($("#searchWords_cboWidget_quick").val());
    			
    			if(term === "Please enter a keyword") {
    				term = "";
    			}
    			if (term != null && term.length > 0) {
    				valid = true;
    			}
    			
    			if (valid) {
    				if(term.length > 1){
        				var searchForm = document.getElementById("search_form");
        				$("#searchFormWidget_quick").attr("action", ebooksContextPath + "search?searchType=quick&searchText=" + term);
        				$("#searchFormWidget_quick").attr("target", "cbo");
        				$("#searchFormWidget_quick").submit();
    				}
    			} else {
    				alert("Invalid search.");
    			}
    		});//end validate quick search widget input
	    });
	    /******** End jQuery(document).ready(function ********/
	}
	/******** End Our main function ********/
})(); // We call our anonymous function immediately

