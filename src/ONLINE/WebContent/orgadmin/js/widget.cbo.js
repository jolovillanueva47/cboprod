function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = "";
};

(function() {

	// Localize jQuery variable
	var jQuery;

	/******** Load jQuery if not present *********/
	if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
	    var script_tag = document.createElement('script');
	    script_tag.setAttribute("type","text/javascript");
	    script_tag.setAttribute("src",
	    		getContextPath() + "/js/jquery-ui/js/jquery-1.4.2.min.js");
	    script_tag.onload = scriptLoadHandler;
	    script_tag.onreadystatechange = function () { // Same thing but for IE
	        if (this.readyState == 'complete' || this.readyState == 'loaded') {
	            scriptLoadHandler();
	        }
	    };
	    // Try to find the head, otherwise default to the documentElement
	    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
	} else {
	    // The jQuery version on the window is the one we want to use
	    jQuery = window.jQuery;
	    main();
	}

	/******** Called once jQuery has loaded ******/
	function scriptLoadHandler() {
	    // Restore $ and window.jQuery to their previous values and store the
	    // new jQuery in our local jQuery variable
	    jQuery = window.jQuery.noConflict(true);
	    // Call our main function
	    main(); 
	}

	/******** Our main function ********/
	function main() { 
	    jQuery(document).ready(function($) { 
	    	
	    	/******* Load CSS *******/
	        var css_link = $("<link>", { 
	           rel: "stylesheet", 
	           type: "text/css", 
	            href: getContextPath() + "/css/jquery-ui/css/smoothness/jquery-ui-1.8.2.custom.css" 
	        });
	        css_link.appendTo('head');    

	    	/******** Autocomplete function ********/
	    	
	    		var collection_id = {};
	    		var isbn_id = {};
	    		var searchWords = "";
	    		var source_main = "/ebooks/widget";

	    		var cache_coll = {};

	    		doPreProcessing();
	    		
	    		//for coll autocomplete input textbox
	    		$( "#collWidgetCbo" ).autocomplete({
	    			minLength: 2,
	    			source: function(request, response) {
	    				if ( request.term in cache_coll ) {
//	    					if(cache_coll[ request.term ].length == 0 )
//	    						alert("No items found.");
	    					response( cache_coll[ request.term ] );
	    					return;
	    				}
	    				
	    				$.ajax({
	    					url: getSource("coll"),
	    					dataType: "jsonp",
	    					data: request,
	    					success: function( data ) {
	    						var data_length = data.items.length;
	    						if(data_length == 0){
	    							cache_coll[ request.term ] = "";
	    							alert("No items found.");
	    						}else{
	    							cache_coll[ request.term ] = data.items;
	    							response($.map(data.items, function(item) {
		    							return{
		    								label: item.label,
		    								value: item.value,
		    								id: item.id
		    							}
		    						}));//END RESPONSE
	    						}//end if else
	    					}//END SUCCESS
	    				});//END AJAX
	    			},//END SOURCE
	    			
	    			select: function(event, ui) {	
	    				$("#collWidget_collection_id").attr("value", ui.item.id);
	    				$("#bookWidgetCbo").attr("value", "");
	    				

	    			}//END SELECT
	    		})//END AUTOCOMPLETE
	    		.data( "autocomplete" )._renderItem = function( ul, item ) {
    				var term_tmp = this.term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1");
    			    // Join the terms with a pipe as an 'OR'
    				term_tmp = term_tmp.split(' ').join('|');
    			    var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term_tmp + ")(?![^<>]*>)(?![^&;]+;)", "gi");
    				var t = item.label.replace(re,"<span style='font-weight:bold;'>$1</span>"); 				
   	    			return $( "<li></li>" )
	    				.data( "item.autocomplete", item )
	    				.append( "<a>" + t + "</a>" )
	    				.appendTo( ul );
    			};//END DATA FOR AUTOCOMPLETE
	    		

	    		//utils
	    		function getSource(type){
	    			var url = getContextPath() + source_main + "?type=coll";
	    			return url;
	    		};	   
	    		
	    		function getContextPath(){
	    			return $("#resultContext_js").val();
	    		};	  

	    		//this is for book input box to have the correct css.
	    		function doPreProcessing(){
	    			//for css issues
	    			//$("#collWidgetCbo").attr("class", "ui-autocomplete-input");
	    			$("#bookWidgetCbo").attr("class", "ui-autocomplete-input");
	    			
	    		};
	    		
	    		//for validation of input textbox
	    		$("#searchLink_cboWidget").click(function(){
	    			var valid = false;
	    			var collection_id = $("#collWidget_collection_id").val();
	    			var bookInputBox = $("#bookWidgetCbo").val();
	    			
	    			
	    			if (collection_id != null && collection_id.length > 0 && (bookInputBox != null && bookInputBox.length > 0)) {
	    				valid = true;
	    			}
	    			
	    			if (valid) {
	    				$("#searchFormWidget").submit();
	    			} else {
	    				alert("Invalid search.");
	    				$("#collWidget_collection_id").val("");
						$("#collWidgetCbo").val("");
						$("#bookWidgetCbo").val("");
	    			}
	    		});
	    		$("#searchFormWidget").bind("keypress", function(e) {
	    	        if(e.keyCode==13){
	    	        	if($("#collWidget_collection_id").val() && $("#bookWidgetCbo").val()){
	    	        		$("#searchLink_cboWidget").click();
	    	        	}
	    	        	
	            	}
	            });
	    		
	    		//to avoid unnecessary form submission
	    		$("#searchFormWidget").submit(function() {     
	    			 if ($("#collWidget_collection_id").val() && $("#bookWidgetCbo").val()) {
	                   return true;
	                 }     
	                 return false;
	            });
	    		
	    	
	    	/******** End Autocomplete function ********/
	    });
	    /******** End jQuery(document).ready(function ********/
	}
	/******** End Our main function ********/
})(); // We call our anonymous function immediately

