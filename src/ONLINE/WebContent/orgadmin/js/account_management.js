$(function(){
	//register
	$("#registerForm").validate({
		messages: {
			acceptTerms: {
	            required:function(){
	            	alert('You must accept the Terms of Use to register')
	            }
	        }
	    },
		rules:{
			organisation: "required",
			firstName: "required",
			surName: "required",
			country: "required",
			city: "required",
			postCode: "required",
			address: "required",
			userName: {
				required: true,
	    		minlength: 6,
	    		maxlength: 24
			},
			passWord: {
				required: true,
	    		minlength: 6,
	    		maxlength: 24
			},
			passWord2: {
				required: true,
				equalTo: "#passWord"
			},
			email: {
				required:true,
				email:true,
				maxlength: 80
			},
			email2:{
				required: true,
				email:true,
				equalTo: "#email"
			},
			question: "required",
			acceptTerms: "required"
			
		}
	});
	
	
	
	$("#updateRegistrationForm").validate({
		messages: {
			acceptTerms: {
	            required:function(){
	            	alert('You must accept the Terms of Use to register')
	            }
	        }
	    },
		rules:{
			firstName: "required",
			surName: "required",
			country: "required",
			city: "required",
			postCode: "required",
			address: "required",
			userName: {
				required: true,
	    		minlength: 6,
	    		maxlength: 24
			},
			oldPassWord: {
				required: true,
	    		minlength: 6,
	    		maxlength: 24
			},
			passWord: {
				equalTo: "#passWord"
			},
			passWord2: {
				equalTo: "#passWord"
			},
			email: {
				required:true,
				email:true,
				maxlength: 80
			},
			email2:{
				required: true,
				email:true,
				equalTo: "#email"
			},
			question: "required",
			acceptTerms: "required"
			
		}
	});
	
	$("#country").change(function(){
		getCountyList();
	});
	//end register
});

function getCountyList(){
	var cId = $("#country").val();
	
	$("#county option").remove();
	$.ajax({
		type: "POST",
		url: "/updateCountyList?countryId="+cId,
		contentType: "application/json",
		dataType: "json",
		success: function(msg){
			jQuery.each(msg, function(key, val){
				if(key==0){
					$("#county").append("<option value='" + key+ "' selected='selected'>" + val + "</option>");
				} else {
					$("#county").append("<option value='" + key+ "'>" + val + "</option>");
				}
			});
		}
	});
}