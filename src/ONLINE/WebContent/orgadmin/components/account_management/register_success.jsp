<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>

<h1 class="titlepage_heading" >Registration</h1>

<div id="searchresults">
<c:if test="${invalidOfferCode ne null and invalidOfferCode.subscribedActive ne null}">
	<p class="alert" style="color: red">
		Offer code ${activateSubscriptionsForm.subscribedActive} has already been activated. Please double-check your offer code and enter it again.
	</p>
	<c:set var="withError" scope="request" value="Y"/> 
</c:if>

<c:if test="${invalidOfferCode ne null and invalidOfferCode.subscribedInvalid ne null}">
	<p class="alert" style="color: red">Offer code ${activateSubscriptionsForm.subscribedInvalid} is invalid.</p>
	<c:set var="withError" scope="request" value="Y"/> 
</c:if>

<c:if test="${empty errorInAdminregister}">
	<h4>Registration Successful!</h4>
	<ul>
		<li>${firstName} ${surName}</li>
		<li>${email}</li>
	</ul>
	
	<p>Thank you for registering with Cambridge Books Online.</p>
	
	<c:if test="${userType eq 'AO'}">
		<p>You are now the administrator for the following organisation:</p>
		<h4>${orgnamemo}</h4>
		<p>Use the "Account Administrator" link on the upper right to view and manage your organisation's preferences.</p>
	</c:if>
</c:if>

<c:if test="${!empty orgNameForIpEntry }">
	<h1>Administrator Registration</h1>
	
	<form action="/registrationAdmin" method="post" id="registrationAdminForm">
		<c:if test="${not empty param.SIGNUP}" >
	  		<input type="hidden" name="SIGNUP" value="Y" />
		</c:if>
		<input type="hidden" name="register" value="Submit"/>
	<table cellspacing="0">
		<tr>
			<td>You have checked the option to become  Account Administrator for your organisation. 
	        Please <a href="<%=System.getProperty("ebooks.server.link") %>">click here</a> if you are not the administrator.</td>
		</tr>
		
		<c:if test="${!empty errorInAdminregister}">
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					<div class="validate">${errorInAdminregister}</div>
				<td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		</c:if>
		<tr>
			<td>Enter your organisation's IP ranges here: <input type="text" class="input_field" name="newIpAddr" /></td>
		</tr>
	</table>
	<table cellspacing="0" class="register_buttons">
		<tr>
			<td>
				<c:choose>
					<c:when test="${not empty param.SIGNUP}" >							
						<span class="button">
							<input type="reset" class="button" value="Reset" />
						</span>
						<span class="button">
							<input type="submit" class="button" value="Sign-up for a Trial" onclick="return validateRegistration()" name="registerButton" />																
						</span>							
					</c:when>
					<c:otherwise>
						<span class="button">
							<input type="reset" class="button" value="Reset" />
						</span>
						<span class="button">
							<input type="submit" class="button" value="Submit" onclick="return validateRegistration()" name="registerButton" />								
						</span>
					</c:otherwise>
				</c:choose>	
			</td>
		</tr>
	</table>
		<input type="hidden" name="orgNameForIpEntry" value="${orgNameForIpEntry}" />
		<input type="hidden" name="memberId" value="${memberId}" />
	</form>

</c:if>

</div>


<img src="http://cup.msgfocus.com/v/?tag=newuser">