<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>

<style type="text/css" media="screen">
	label.error {color:#f00;}
</style>
${updateRegisterBean.initialize}
<c:set var="userDetails" value="${updateRegisterBean.userDetails }" />
<h1>Change Registration Details</h1>

<div id="register">
	<c:if test="${not empty error}" >
		<div class="validate">
			${error }
		</div>
	</c:if>
	
	<p>
		Use the form below to update your registration if any of your details have changed or if you would like to change your password. <br /> 
		Take note that if you are also using Cambridge Journals Online, any changes you make here will be implemented to Cambridge Journals, and vice versa. 
		For example: if you change your password on Cambridge Books Online, you'll need to use your new password when logging in on Cambridge Journals Online.
	</p>
	
	<form action="/updateRegistration" method="post" id="updateRegistrationForm">
		<div id="body">
			<table cellspacing="0">
				<tr class="border">
					<td colspan="2">* Required Information</td>
				</tr>
				<tr class="border">
					<th><label for="title">Title</label></th>
					<td class="fields"><input type="text" name="title" value="${userDetails.title }" class="title_field" id="title" maxlength="48" /></td>
				</tr>
				<tr>
					<th><label for="firstName">First Name*</label></th>    
					<td><input type="text" name="firstName" value="${userDetails.firstName }"  class="register_field" id="firstName" maxlength="48" /></td>
				</tr>
				<tr>
					<th><label for="surName">Last Name*</label></th>
					<td><input type="text" name="surName" class="register_field" value="${userDetails.surName }" id="surName" maxlength="48" /></td>
				</tr>

				<tr class="border">
					<th><a name="country" ></a><label for="country">Country*</label></th>
					<td>
						<select name="country" id="country">
							<option value="">Select Country</option>
<!-- 							<option value="17">United Kingdom</option> -->
<!-- 							<option value="226" selected="selected">United States</option> -->
							<c:forEach var="country" items="${updateRegisterBean.countryList}" >
<%-- 								<c:if test="${country.key ne '17' or country.key eq '226' }" > --%>
									<c:choose>
										<c:when test="${country.key eq userDetails.country }">
											<option value="${country.key }" selected="selected">${country.value}</option>
										</c:when>
										<c:otherwise>
											<option value="${country.key }">${country.value}</option>
										</c:otherwise>
									</c:choose>
<%-- 								</c:if> --%>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th><label for="state">County / State / Province </label></th>
					<td>
						<select name="county" name="county" id="county">
							<c:forEach var="county" items="${updateRegisterBean.countyList}" >
								<c:choose>
									<c:when test="${county.key eq userDetails.state }">
										<option value="${county.key }" selected="selected">${county.value}</option>
									</c:when>
									<c:otherwise>
										<option value="${county.key }">${county.value}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>    
				</tr>
				<tr>
					<th><label for="city">Town / City*</label></th>
					<td><input type="text" name="city" value="${userDetails.city }"  class="register_field" id="city" maxlength="48" /></td>
				</tr>
				<tr>
					<th><label for="postCode">Post / Zip Code*</label></th>
					<td><input type="text" name="postCode" value="${userDetails.postCode }" class="register_field" id="postCode" maxlength="48" /></td>
				</tr>
				<tr>
					<th><label for="address">Address*</label></th>
					<td><input type="text" name="address" value="${userDetails.address }" class="register_field" id="address" maxlength="80" /></td>
				</tr>
				<tr>
					<th><label for="address2">Address 2</label></th>
					<td><input type="text" name="address2" value="${userDetails.address2 }" class="register_field" id="address2" maxlength="80" /></td>
				</tr>
				<tr>
					<th><label for="telephone">Telephone</label></th>
					<td><input type="text" name="telephone" value="${userDetails.telephone }" class="register_field" id="telephone" maxlength="48" /></td>
				</tr>
				<tr>
					<th>Fax&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" name="fax" class="register_field" value="${userDetails.fax }" id="telephone" maxlength="48" /></td>
				</tr>
				<tr>
					<th><label for="mobilePhone">Mobile Phone</label></th>
					<td><input type="text" name="mobilePhone" value="${userDetails.mobilePhone }" class="register_field" id="mobilePhone" maxlength="48" /></td>
				</tr>
				<tr>
					<th>Affiliation</th>
					<td><input type="text" name="affiliation" value="${userDetails.affiliation }" class="register_field" id="org" maxlength="80" /></td>
				</tr>
<!-- 				<tr> -->
<!-- 					<td colspan="2" class="check"> -->
<%-- 						<c:choose> --%>
<%-- 							<c:when test="${userDetails.smsAlert }" > --%>
<!-- 								<input type="checkbox" name="smsAlert" value="on" checked/>	 -->
<%-- 							</c:when> --%>
<%-- 							<c:otherwise> --%>
<!-- 								<input type="checkbox" name="smsAlert" value="on"/> -->
<%-- 							</c:otherwise> --%>
<%-- 						</c:choose> --%>
<!-- 					&nbsp;Please keep me informed by SMS/text message about relevant Cambridge publishing, news and special offers&nbsp;</td> -->
					
<!-- 				</tr>			 -->
				<tr class="border">
					<th><label for="userNameTemp">Username*</label></th>
					<td><input type="text" name="userNameTemp" class="register_field" value="${userDetails.userName }" id="userNameTemp" onkeypress="blur();" onfocus="blur();"  />
					<input type="hidden" name="userName" value="${userDetails.userName }" /></td>
				</tr>
				<tr>
					<th><label for="oldPassWord">Password*</label></th>
					<td>
					<input type="password" name="oldPassWord" class="register_field" id="oldPassWord" maxlength="24" />
					</td>
				</tr>
				<tr>
					<th><label for="passWord">New Password</label></th>
					<td>
					<input type="password" name="passWord" class="register_field" id="passWord" maxlength="24" value="" />
					<p class="note">case sensitive, 6 to 24 characters only</p></td>
				</tr>
				<tr>
					<th><label for="passWord2">Confirm New Password</label></th>
					<td><input type="password" name="passWord2" class="register_field" id="passWord2" maxlength="24" value="" /></td>
				</tr>
				<tr>
					<th><label for="email">Email*</label></th>
					<td><input type="text"  value="${userDetails.email }" name="email" class="register_field" id="email" maxlength="80" /></td>
				</tr>
				<tr>
					<th><label for="email2">Confirm Email*</label></th>
					<td><input type="text"  value="${userDetails.email }" name="email2" class="register_field" id="email2" maxlength="80" /></td>
				</tr>
				<tr class="border">
					<td colspan="2">Please enter a question or sentence to help remind you of your password. If you forget your password, this reminder will be emailed to you.</td>
				</tr>
				<tr>
					<th><label for="question">Question*</label></th>
					<td><input type="text" name="question"  value="${userDetails.question }" class="register_field" id="question" maxlength="80" /></td>
				</tr>
				<tr class="border">
					<td colspan="2">If you have an Offer Code, please enter it here:</td>
				</tr>
				<tr>
					<th><label for="offerCode">Offer Code</label></th>
					<td><input type="text" name="offerCode" class="register_field" id="question" maxlength="16" /></td>
				</tr>
			</table>


			<table cellspacing="0" >
				<tr class="border">
					<td colspan="1">&nbsp;</td>
					<td class="check" >
						<input type="checkbox" name="acceptTerms" value="on" id="acceptTerms" checked/>
						<label for="acceptTerms">You must accept the <a href="#" onclick="javascript:openWindow('<%= System.getProperty("ebooks.server.link") %>/templates/terms_of_use_popup.jsf','popup','status=yes,scrollbars=yes');">Terms of use</a> to register.*</label>
					</td>
				</tr>	
				<tr class="border">
					<td colspan="1">&nbsp;</td>
					<td class="check">
						<c:choose>
							<c:when test="${userDetails.emailList }">
								<input type="checkbox" name="emailList" id="emailList" value="on" checked/>
							</c:when>
							<c:otherwise>
								<input type="checkbox" name="emailList" id="emailList" value="on" />					
							</c:otherwise>
						</c:choose>
						<label for="emailList">Please keep me informed by email about relevant Cambridge publishing, news and special offers.</label>
					</td>
				</tr >
				<tr class="border">
					<td colspan="1" >&nbsp;</td>
					<td class="check">
						<c:choose>
							<c:when test="${userDetails.thirdParty }">
								<input type="checkbox" value="on" name="thirdParty" id="thirdParty" checked/>
							</c:when>
							<c:otherwise>
								<input type="checkbox" value="on" name="thirdParty" id="thirdParty" />					
							</c:otherwise>
						</c:choose>
						
						<label for="thirdParty">From time to time we may share customer information with other, similar organisations. Please check this box if you do not want us to pass your details on.</label>
					</td>
				</tr>
			</table>
			<input type="hidden" name="mailMe" />
			<input type="hidden" name="cookie" />
		
			<input type="hidden" name="org" />
			<input type="hidden" name="admin" />
			<input type="hidden" name="address3" />
			<input type="hidden" name="update" value="update" />
			
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td colspan="2">
						<div class="form_button">
							<span><input type="reset"  value="Reset" /></span>
							<span><input type="submit" value="Update" name="registerButton" /></span>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>



