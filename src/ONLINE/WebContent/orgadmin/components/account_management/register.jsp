<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>

<style type="text/css" media="screen">
	label.error {color:#f00;}
</style>

<c:choose>
	<c:when test="${not empty param.SIGNUP}">
		<h1 class="titlepage_heading" >Sign-up for a Trial</h1>		
	</c:when>
	<c:otherwise>
		<h1 class="titlepage_heading" >Registration</h1>
	</c:otherwise>
</c:choose>
${registerBean.initialize }
<form action="/register" method="post" id="registerForm">
	<input type="hidden" name="register" value="reg" />
	<c:if test="${not empty param.SIGNUP}" >
		<input type="hidden" name="SIGNUP" value="Y"/>
	</c:if>

	<c:if test="${null != page && page == 'contentAlert'}">
		<div id="body">
			<div class="notice">
				<p>Would you like to add a Table Of Contents alert for ${journalTitle}?</p>
				<ul class="personalize">
					<li>
						<input type="checkbox" id="issueAlert" />
						<%--<c:if test="${registrationForm.issuealert == 'Y'}"> checked</c:if> --%>
						Issue Alert?
					</li>
				<li>
					<input type="checkbox" id="firstViewAlert" />
					<%-- <c:if test="${registrationForm.Forthcomingalert == 'Y'}"> checked</c:if> --%>
					First View Alert?
				</li>
				</ul>
			</div>
		</div>
	</c:if>
	
	<div id="register">
		<c:if test="${null != page && page != 'displayAbstract'}">
			<p class="alert">To continue this action you will need to login to CJO with your username or password.  If you are a new visitor please register here.</p>
		</c:if>
		
		<c:choose>
				<c:when test="${not empty errorExist}" >
					<div class="validate">
						<h4>Field below already exists in our system, please try another one:</h4>
						<ul><li>${errorExist}</li></ul>
					</div>
				</c:when>
			<c:otherwise>
				<c:if test="${not empty param.SIGNUP}">
					<p>Trials to Cambridge Books Online are available to prospective institutional subscribers worldwide. Librarians or electronic resources co-ordinators may use the form below to request a trial. We will then confirm your trial access via e-mail. If you would like to speak directly to a Cambridge representative about setting up a trial please contact <a href="mailto:online@cambridge.org" class="link03" >online@cambridge.org</a> if you are in the Americas, or <a href="mailto:academicsales@cambridge.org" class="link03">academicsales@cambridge.org</a> if you are in the United Kingdom or the rest of the world.</p>
				</c:if>
				<p>Please take a minute to register. You will only have to do this once for yourself or your institution.</p>  
				<p>Note:</p>
				<p>* The username and password you select must each have a minimum of four characters, are case-sensitive and can be any combination of letters and numbers (e.g. 1xE24tY)</p>
				<p>* If you already have a username for Cambridge Journals Online, you can log in to Cambridge Books Online using your existing CJO username.</p>
				<p>* Required Information(*)</p>
			</c:otherwise>
		</c:choose>
		
		<table cellspacing="0" width="100%" border="0">
			<tr>
		  		<td class="border" colspan="2"></td>	
		  	</tr>
		  	<tr>
			    <th align="right"><label for="firstName">First Name*</label></th>    
			    <td><input type="text" name="firstName" class="register_field" id="firstName" maxlength="40" /></td>
			</tr>
			<tr>
			    <th align="right"><label for="surName">Last Name*</label></th>    
			    <td><input type="text" name="surName" class="register_field" id="surName" maxlength="40" /></td>
			</tr>
			<tr>
			    <th align="right"><label for="organisation">Organisation</label></th>    
			    <td><input type="text" name="organisation" class="register_field" id="organisation" maxlength="40" /></td>
			</tr>
		  	<tr>  
				<td class="check" colspan="2"><input type="checkbox" name="admin" id="admin" />
				<label for="admin">I want to be the Account Administrator for my organisation. Only librarians can create organisational accounts. </label></td>
		  	</tr>
			<tr>
				<td class="border" colspan="2"></td>
			</tr>
		  
		  
		  	<tr class="border">
			    <th align="right"><a name="country"></a><label for="countries">Country*</label></th>
			    <td class="fields" >
			    	<select name="country" class="register_field" id="country" >
						<option value="17">United Kingdom</option>
		    				<option value="226" selected="selected">United States</option>
							<c:forEach var="country" items="${registerBean.countryList}" >
								<c:if test="${country.key ne '17' or country.key eq '226' }" >
									<option value="${country.key }">${country.value}</option>
								</c:if>
							</c:forEach>
					</select>
			    </td>
		 	</tr>
		 	<tr>
			    <th align="right"><label for="state">County / State / Province 	</label></th>
			    <td>
			 		<select name="state" id="county" class="register_field">
						<c:forEach var="county" items="${registerBean.countyList}" >
							<c:choose>
								<c:when test="${county.key eq 0 }">
									<option value="${county.key }" selected="selected">${county.value}</option>
								</c:when>
								<c:otherwise>
									<option value="${county.key }">${county.value}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>   
			    </td>    
		  	</tr>
		  	<tr>
			    <th align="right"><label for="city">Town / City*</label></th>
			    <td><input type="text" class="register_field" name="city" id="city" maxlength="48" /></td>
			</tr>
			<tr>
				<th align="right"><label for="postCode">Post / ZIP Code*</label></th>
				<td><input type="text" class="register_field" name="postCode" id="postCode" maxlength="48" /></td>
			</tr>
			<tr>
			    <th align="right"><label for="address">Address*</label></th>
			    <td><input type="text" class="register_field" name="address" id="address" maxlength="80" /></td>
			</tr>
			<tr>
			    <th align="right"><label for="address">Address 2</label></th>
			    <td><input type="text" class="register_field" name="address2" id="address2" maxlength="80" class="register_field"/></td>
			</tr>
			<tr>
				<th align="right"><label for="telephone">Telephone&nbsp;&nbsp;&nbsp;</label></th>
				<td><input id="telephone" class="register_field" type="text" value="" maxlength="80" name="telephone" ></td>
			</tr>
		  	<tr>
			<th align="right"><label for="fax">Fax&nbsp;&nbsp;&nbsp;</label></th>
			<td><input id="telephone" class="register_field" type="text" value="" maxlength="48" name="fax"></td>
			</tr>
			<tr>
				<th align="right"><label for="affiliation">Affiliation</label></th>
				<td><input id="org" class="register_field" type="text" value="" maxlength="80" name="affiliation"></td>
			</tr>
			<tr>
				<td class="border" colspan="2"></td>
			</tr>
			
			
			
			<tr class="border">
				<th align="right"><label for="username">Username*</label></th>
				<td>
					<p class="note">
						<input id="userName" class="register_field" type="text" value="" maxlength="24" name="userName">
						case-sensitive, 6 to 24 characters only
					</p>
				</td>
			</tr>
			<tr>
				<th align="right"><label for="passWord">Password*</label></th>
				<td>
				<p class="note">
					<input id="passWord" class="register_field" type="password" value="" maxlength="24" name="passWord">
					case-sensitive, 6 to 24 characters only
					</p>
				</td>
			</tr>
			<tr>
				<th align="right"><label for="password2">Confirm Password*</label></th>
				<td><input id="passWord2" class="register_field" type="password" value="" maxlength="24" name="passWord2"></td>
			</tr>
			<tr>
				<th align="right"><label for="email">Email*</label></th>
				<td><input id="email" class="register_field" type="text" value="" maxlength="80" name="email" ></td>
			</tr>
			<tr>
				<th align="right"><label for="email2">Confirm Email*</label></th>
			<td><input id="email2" class="register_field" type="text" value="" maxlength="80" name="email2" ></td>
			</tr>
			<tr>
				<td class="border" colspan="2"></td>
			</tr>
			
			
			
			<tr class="border">
				<td colspan="2">Please enter a question or sentence to help remind you of your password. If you forget your password, this reminder will be emailed to you.</td>
			</tr>
			<tr>
				<th align="right"><label for="question">Question*</label></th>
				<td class="fields"><input id="question" class="register_field" type="text" value="" maxlength="80" name="question" ></td>
			</tr>
			<tr>
			<td class="border" colspan="2"></td>
			</tr>
			
			<tr class="border">
				<td colspan="1">&nbsp;</td>
				<td class="check"><input id="acceptTerms" type="checkbox" value="on" name="acceptTerms">
					<label for="acceptTerms">You must accept the
						<a onclick="javascript:openWindow('/templates/terms_of_use_popup.jsf','popup','status=yes,scrollbars=yes');" href="#">Terms of Use</a>
						to register.*
					</label>
				</td>
			</tr>
			<tr>
				<td colspan="1">&nbsp;</td>
				<td class="check">
					<input id="emailList" type="checkbox" value="on" name="emailList">
					<label for="emailList">Please keep me informed by email about relevant Cambridge publishing, news and special offers.</label>
				</td>
			</tr>
			<tr>
				<td colspan="1">&nbsp;</td>
				<td class="check">
					<input id="thirdParty" type="checkbox" value="on" name="thirdParty">
					<label for="thirdParty">
						From time to time we may share customer information with other, similar organisations.
						<br>
						Please check this box if you do not want us to pass your details on.
					</label>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="border" colspan="2"></td>
			</tr>
		</table>
		
		<table cellspacing="0" class="registered_buttons">
				<tr>
					<td colspan="2">
						<!-- Form button start -->
						<div class="form_button">
							<ul>
								<li><span><input type="reset" name="Reset" value="Reset" accesskey="R" /></span></li>
								<li><span><input type="submit" value="Submit" name="submit" accesskey="U" /></span></li>
							</ul>
						</div>
						<!-- Form button end -->
					</td>
				</tr>
			</table>
		
	</div>
</form>