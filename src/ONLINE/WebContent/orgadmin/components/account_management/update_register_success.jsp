<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>

<h1 class="titlepage_heading">Change Registration Details</h1>

<c:if test="${invalidSubscription ne null or not empty invalidSubscription}">
	<p class="alert" style="color: red">Offer code ${invalidSubscription} is invalid.</p>
	<c:set var="withError" scope="request" value="Y"/> 
</c:if>

<div id="searchresults">
	<h4>Update Successful!</h4>
	
	<c:if test="${!empty registrationForm.updatedFields}">
		<p>You have updated the following details:</p>
		<ul>
			<c:forEach var="updatedFields" items="${registrationForm.updatedFields}">
				<li>${updatedFields}</li>
			</c:forEach>
		</ul>
	
	
	</c:if>

</div>