<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>



${contentAlertBean.initialize}
<c:set var="setting" value="${contentAlertBean.subjectSetting }" />
<h1>Manage Content Alerts</h1>



<form  id="manageContentAlertsForm" name="manageContentAlertsForm" action="/manageContentAlerts" method="post">
	<div id="registered">
		<h2>Set Alert Preferences</h2>
		
		<div id="body">
			<table cellspacing="0" class="terms">
				<tbody>
					<tr>
						<td class="check">
							<c:choose>
								<c:when test="${setting.alertType eq 1 }">
									<input type="radio" name="type" value="1" checked/>
								</c:when>
								<c:otherwise>
									<input type="radio" name="type" value="1" />
								</c:otherwise>
							</c:choose>
						</td>
						<th class="fields">
							<label for="1">Send me an email alert whenever any books in the selected subject areas below are published online.</label>
						</th>
					</tr>			
					<tr>
						<td class="check">
							<c:choose>
								<c:when test="${setting.alertType eq 0 }">
									<input type="radio" name="type" value="0" checked/>
								</c:when>
								<c:otherwise>
									<input type="radio" name="type" value="0" />
								</c:otherwise>
							</c:choose>
						</td>
						<th class="fields">
							<label for="0">Unsubscribe from all email alerts for all subject areas. <br/> (If you wish to unsubscribe only from alerts for some subject areas, please un-tick the boxes below for individual subject areas as needed.)</label>	
						</th>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="emailFormat" value="${setting.emailFormat }" />
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
						<input type="reset" class="button" value="Reset" />
						<input type="submit" class="button" name="UpdatePreferences" value="Update"/>
					</td>
				</tr>
			</table>
		</div>		
	</div>
	<br/>
	
	<div id="registered">
		<div id="searchresults">
			<a name="top"></a>
				<table cellspacing="0">
					<tr class="shaded">
					  <th class="title" scope="col">Select Subject Area(s)</th>
					  <th scope="col">&nbsp;</th>
					  <th scope="col">&nbsp;</th>
					</tr>
				</table>
			<c:set var="count" value="0"/>
			<table cellspacing="0">
				<c:forEach var="subject" items="${contentAlertBean.subjectHierarchy}" varStatus="index">
					<c:choose>
						<c:when test="${subject.subjectLevel eq 1}">
							<tr class="shaded">
						  		<th class="title" scope="col">${subject.subjectName}</th>
	 	  					    <th scope="col">
						  			<c:if test="${subject.withAlert eq 'Y'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" checked="checked" />
									</c:if>		 	  					    
						  			<c:if test="${subject.withAlert eq 'N'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" />
									</c:if>	
	 	  					    </th>
							</tr>
						</c:when>
						<c:when test="${(subject.subjectLevel eq 2)}">
							<tr class="shaded">
						  		<td class="title">
						  			<c:forEach begin="1" end="${subject.subjectLevel}">
										&nbsp;
									</c:forEach>
									<b>${subject.subjectName}</b>
						  		</td>
						  		<td align="right">
						  			<c:if test="${subject.withAlert eq 'Y'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" checked="checked"  />
									</c:if>					  		
						  			<c:if test="${subject.withAlert eq 'N'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}"  />
									</c:if>							  		
						  		</td>
							</tr>
							<c:if test="${(subject.subjectLevel < contentAlertBean.subjectHierarchy[index.count-2].subjectLevel) and (subject.subjectLevel eq contentAlertBean.subjectHierarchy[index.count].subjectLevel)}">
								<tr>
									<td align="right">&nbsp;</td>
									<td align="right"><a href="#top">back to top</a></td>
								</tr>
							</c:if>
							<c:if test="${(contentAlertBean.subjectHierarchy[index.count].subjectLevel eq 1)}">
								<tr>
									<td align="right">&nbsp;</td>
									<td align="right"><a href="#top">back to top</a></td>
								</tr>
							</c:if>
						</c:when>
						<c:when test="${(subject.subjectLevel gt 2) and (subject.subjectLevel < contentAlertBean.subjectHierarchy[index.count].subjectLevel)}">
							<tr class="shaded">
						  		<td class="title">
						  			<c:forEach begin="1" end="${subject.subjectLevel + 4}">
										&nbsp;
									</c:forEach>
									<b>${subject.subjectName}</b>
						  		</td>
						  		<td align="right">
						  			<c:if test="${subject.withAlert eq 'Y'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" checked="checked" />
									</c:if>
						  			<c:if test="${subject.withAlert eq 'N'}">
										<input id="${subject.subjectId}:${subject.subjectLevel}" type="checkbox" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" />
									</c:if>					  		
						  		</td>
							</tr>
						</c:when>					
						<c:when test="${subject.subjectLevel > manageContentAlertsForm.subjectHierarchy[index.count].subjectLevel}">
							<tr>
								<td align="right">
									<c:forEach begin="1" end="${subject.subjectLevel + 7}">
										&nbsp;
									</c:forEach>
									${subject.subjectName} 
								</td>
								<td align="right">
									<c:if test="${subject.withAlert eq 'Y'}">
										<input type="checkbox" id="${subject.subjectId}:${subject.subjectLevel}" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" checked="checked" />								
									</c:if>
									<c:if test="${subject.withAlert eq 'N'}">
										<input type="checkbox" id="${subject.subjectId}:${subject.subjectLevel}" name="parentSubject" value="${subject.subjectId}" title="${subject.path}"/>								
									</c:if>
								</td>
							</tr>						
							<tr>
								<td align="right">&nbsp;</td>
								<td align="right"><a href="#top">back to top</a></td>
							</tr>
						</c:when>		
						<c:otherwise>
							<tr>
								<td align="right">
									<c:forEach begin="1" end="${subject.subjectLevel + 7}">
										&nbsp;
									</c:forEach>
									${subject.subjectName}
								</td>
								<td align="right">
									<c:if test="${subject.withAlert eq 'Y'}">
										<input type="checkbox" id="${subject.subjectId}:${subject.subjectLevel}" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" checked="checked" />
										
									</c:if>
									<c:if test="${subject.withAlert eq 'N'}">
										<input type="checkbox" id="${subject.subjectId}:${subject.subjectLevel}" name="parentSubject" value="${subject.subjectId}" title="${subject.path}" />
									</c:if>
								</td>
							</tr>					
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<tr>
					<td align="right">&nbsp;</td>
					<td align="right"><a href="#top">back to top</a></td>
				</tr>
			</table>
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
						<input type="reset" class="button" value="Reset" />
						<input type="submit" class="button" name="UpdateSubjectAlert" value="Update"  />
					</td>
				</tr>
			</table>
		</div>
	</div>	
</form>
<script language="JavaScript" type="text/Javascript">




	function removeFromArray(array, item){
		var arr = array;
		var removeItem = item; // item to remove from array
		
		return jQuery.grep(arr, function(value) {
			return value != removeItem;
		});
	}

	function toggle(checkbox){	
		var checked = checkbox.attr('checked') == true ? true : false;
		var id = checkbox.attr('id').split(':')[0];
		var level = checkbox.attr('id').split(':')[1];
		var parent = checkbox.attr('title').split(':')[level-1];


		for(x=0; x<(document.manageContentAlertsForm.getElementsByTagName('input').length); x++)
		{
		
			var currentPath = document.manageContentAlertsForm.getElementsByTagName('input')[x].getAttribute('title');
			if(currentPath != null)
				if(currentPath.split(':')[level] == id)
					document.manageContentAlertsForm.getElementsByTagName('input')[x].checked = checked;
					
		}
		
		var parentIds = checkbox.attr('title').split(':');
		if(checked == true)
		{
			for(p=parentIds.length-2; p>0; p--)
			{
			
				var lengthOfChildCheckboxes = $("input[title*=':" + parentIds[p] + ":']").length;
				var lengthOfSelectedChildCheckboxes = $("input[title*=':" + parentIds[p] + ":']:checked").length
				if(lengthOfChildCheckboxes == lengthOfSelectedChildCheckboxes)
				{
					$("input[id^='" + parentIds[p] + ":']").attr('checked', true);
				}
			}			
		}
		else
		{
			for(p=1; p<parentIds.length-1; p++)
				$("input[id^='" + parentIds[p] + ":']").attr('checked', false);
		}	
	}


//-->
</script>

