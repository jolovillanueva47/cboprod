<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page import="java.util.Calendar" %>

<link href="${pageContext.request.contextPath}/css/popup.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/orgadmin/css/print.css" rel="stylesheet" type="text/css" />

	<div class="camLogo">
		<a href="http://www.cambridge.org">
			<img src="${pageContext.request.contextPath}/orgadmin/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" />
		</a>
	</div>
	
	<div class="clear_bottom"></div>
		
		<div class="noBordercontent">
			<div>
				<ul>
					<li class="home">
			   			<b>Usage Reporting Policy and Parameters</b>
			   			<a href="${pageContext.request.contextPath}/orgadmin/components/administrator/popups/read_policy.jsp">Read Policy</a>
					</li>
				</ul>
				<div class="clear"></div>
				<ul>
					<li class="home">
			   			<b>Organisation</b>
			   			<a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=usageStat">Usage Reports</a>
					</li>
				</ul>
	   		</div>
			<!-- start admin content -->
			
				<h2>${administratorBean.orgName} - COUNTER Reports</h2>

				<div id="body_admin">			
								
					<form name="counterStatForm" action="<%= request.getContextPath() %>/ebooks/genEbooksCountRep" method="get">
				
						<table cellspacing="0" >			
							<tr class="shaded">
								<th colspan="4">Selection criteria</th>
							</tr>			
							<tr>
								<td width="15%">Year</td>
								<td width="85%" colspan="3">
									<select name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.YEAR %>">
									<% 
										int startYear = 2009;  
										Calendar cal = Calendar.getInstance();
										int currentYear = cal.get(Calendar.YEAR);
										while ( startYear <= currentYear ) {
									%>
											<option value="<%= currentYear %>"><%= currentYear-- %>&nbsp;</option>
									<%
										}	
									%>						
									</select> 
							</td>
							</tr>					
							<tr class="border">
								<td>Report</td>
								<td colspan="3">
									<%-- <input type="radio" id="report1" name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.REPORT %>" value="1" checked />&nbsp;<label for="report2">Book Report 1: Number of Subscribed Titles by Month and Title</label><br/> --%>
									<input type="radio" id="report2" name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.REPORT %>" value="2" checked />&nbsp;<label for="report2">Book Report 2: Number of Successful Section Requests by Month and Title</label><br/>
									<%-- <input type="radio" id="report3" name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.REPORT %>" value="3" /><label for="report3">Book Report 3: Turnaways by Month and Title</label><br/> --%>
									<%-- <input type="radio" id="report5" name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.REPORT %>" value="5" />&nbsp;<label for="report5">Book Report 5: Total Searches and Sessions by Month and Title</label> --%> 
									<input type="radio" id="report6" name="<%=org.cambridge.ebooks.orgadmin.administrator.counterreports.CounterReportsServlet.REPORT %>" value="6" />&nbsp;<label for="report5">Book Report 6: Total Searches and Sessions by Service</label>
								</td> 
							</tr>	
							<%-- 
							<tr class="border">	
								<td>Body Id</td>
								<td colspan="3"><input type="text" name="BODY_ID" /></td>
							</tr>
							--%>			
							
							<tr class="border">
								<td>Report type</td>
								<td colspan="3">
									<input type="radio" value="excel" checked />&nbsp;Excel		
								</td>			
								
							</tr>
				
						    <tr class="border">
						    	<td colspan="4">
						    		<p>Note:</p>
						    		<p><b>Usage statistics are unavailable for user sessions taking place between April 13 - May 1, 2011, and also for user sessions taking place between September 4-11, 2010. The data from May 1, 2011 to June 13, 2011, which was previously unavailable, has been recovered and is available. Other usage data is unaffected; the data for the periods mentioned above was unfortunately lost. We apologize for any inconvenience and will endeavor to prevent this from happening in the future. If you have any questions or comments on this unavailability, please write to us at <a href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a></b></p>
						    		<p>Only usage data for Cambridge Books Online products are available through this site.</p>
						    		<p>If you require usage statistics for Cambridge Companions Online, Historical Statistics of the United States, Orlando, Econometric Society Monographs online, Lectrix - Complete Texts, Lectrix - Greek Texts, Lectrix - Latin Texts, or Shakespeare Survey Online, please download them from <a href="http://subs.sams.cup.semcs.net/">http://subs.sams.cup.semcs.net/</a>.</p>
						    	</td> 
						    </tr>
						</table>
						
						<table cellspacing="0">
							<tr><td>
								<div class="form_button">
	            					<ul>
		                				<li><span><input name="cmd" type="submit" class="button" value="Run report"></span></li>                                            
	               					</ul>                
             					</div>
							</td></tr>
						</table>
						
						
				<h2>${administratorBean.orgName} - Other Reports</h2>
				
						<table cellspacing="0" >
							<tr class="shaded">
								<th colspan="4">Monthly Breakdown Report</th>
							</tr>			
							<tr class="shaded">
								<th colspan="4">Selection criteria</th>
							</tr>			
							<tr>
								<td width="30%">Period commencing: dd-Mon-yyyy</td>
								<td width="85%" colspan="3">
										<input type="text" id="datepicker1" name="datepicker1" onchange="javascript:validDate1();"/>
								</td>
							</tr>	
							<tr>
								<td width="30%">Period ending: dd-Mon-yyyy</td>
								<td width="85%" colspan="3">
									<input id="datepicker2" name="datepicker2" type="text" onchange="javascript:validDate2();"/>			
								</td>
							</tr>				
							<tr>
								<td width="30%">Report type:</td>
								<td colspan="3">
									<input type="radio" id="otherRepType" name="otherRepType" value="1" checked />&nbsp;<label>HTML</label><br/>
									<input type="radio" id="otherRepType" name="otherRepType" value="2" />&nbsp;<label>CSV</label><br/>
									<input type="radio" id="otherRepType" name="otherRepType" value="3" />&nbsp;<label>Excel</label>
								</td>
							</tr>
						</table>
									
								
						<table cellspacing="0">
							<tr><td>
								<div class="form_button">
	            					<ul>
		                				<li><span><input name="cmd"  type="submit" class="button" value="Run monthly breakdown report"></span></li>                                            
	               					</ul>                
             					</div>
							</td></tr>
						</table>
					</form>
				</div>	
			
			<!-- end admin content -->
		</div>

		<div class="clear">&nbsp;</div>
		
		<%-- footer links 
		<c:import url="/jsp/ebooks/footerPopup.jsp"></c:import>--%>



<style type="text/css">
	<c:import url="${pageContext.request.contextPath}/orgadmin/css/jquery/redmond/jquery-ui-1.8.13.custom.css"></c:import>
</style>

<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/jquery/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/jquery/jquery-ui-1.8.9.custom.min.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$("#datepicker1").datepicker({dateFormat: 'dd-M-yy'});
		//$("#datepicker1").datepicker({ minDate: new Date(2010, 1 - 1, 1) });
		//getter
		//var minDate = $( "#datepicker1" ).datepicker( "option", "minDate" );
		//setter
		//$( "#datepicker1" ).datepicker( "option", "minDate", new Date(2010, 1 - 1, 1) );
		
		$("#datepicker2").datepicker({dateFormat: 'dd-M-yy'});
	});
	
	function validDate2(){
	var date1 = $("#datepicker1").datepicker("getDate");
	var date2 = $("#datepicker2").datepicker("getDate");

		if(date1 > date2){
			alert("Please enter a date that is greater than the start date.");
			$( "#datepicker2" ).datepicker("setDate" , null);
		}
	}
	
	function validDate1(){
	var date1 = $("#datepicker1").datepicker("getDate");
	var date2 = $("#datepicker2").datepicker("getDate");

		if(date1 > date2 && date2 != null){
			alert("Please enter a date that will not exceed in period ending.");
			$( "#datepicker1" ).datepicker("setDate" , null);
		}
	}
	
</script>