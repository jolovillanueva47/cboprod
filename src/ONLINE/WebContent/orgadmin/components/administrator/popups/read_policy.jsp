<%@ page import="java.util.Calendar" %>
<%@ page isELIgnored="false"%>

<link href="${pageContext.request.contextPath}/orgadmin/css/popup.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/orgadmin/css/print.css" rel="stylesheet" type="text/css" />

<div class="camLogo">
		<a href="http://www.cambridge.org">
			<img src="${pageContext.request.contextPath}/orgadmin/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" />
		</a>
	</div>
	
	<div class="clear_bottom"></div>
		
		<div class="noBordercontent">
			<div>
				<ul>
					<li class="home">
			   			<b>Usage Reporting Policy and Parameters</b>
			   			<a href="${pageContext.request.contextPath}/orgadmin/components/administrator/popups/read_policy.jsp">Read Policy</a>
					</li>
				</ul>
				<div class="clear"></div>
				<ul>
					<li class="home">
			   			<b>Organisation</b>
			   			<a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=usageStat">Usage Reports</a>
					</li>
				</ul>
	   		</div>

			<h1>Cambridge Books Online Reports</h1>
			
			
			<div id="body_admin">			
				<ul style="list-style-type: disc; padding-left: 20px" class="menu">
					<li>Are available as both COUNTER-compliant and ICOLC-style reports</li>
					<li>Are collated from an event tracking system which records both the accessed pages and the IP address of the machine from which they have been accessed</li>
					<li>Retain usage generated from IPs only for the months when those IPs were in full use. If an IP was deleted by an organisation, usage from it is not counted in the month of deletion or after the month of deletion</li>
					<li>Are developed and administered in-house by Cambridge University Press to retain accountability and ensure accuracy</li>
				</ul>
			</div>
		</div>