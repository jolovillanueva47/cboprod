<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Configure IP Domain - Confirmation</title>
		<link href="${pageContext.request.contextPath}/css/popup.css" rel="stylesheet" type="text/css" />
	</head>

	<body>	
	    <form id="configureForm">
			<div class="content">               							
				<label>Are you sure you want to delete the IPs?</label>																
				<br />
				<br />
				<!-- Form button start -->
				<div class="form_button">
            		<ul>
                		<li><span><input type="button" name="acceptConditon" id="acceptConditonY" value="Yes" /></span></li>                                            
                    	<li><span><input type="button" name="acceptConditon" id="acceptConditonN" value="No" /></span></li>
               		</ul>                
             	</div>
        	 	<!-- Form button end -->
			</div>
		</form>		
		<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {					
				var type = "${param.type}";
				var method = "${param.method}";
				$("#acceptConditonY").click(function(){	
					if("ao" == type) {
						$('#configureIpDomainForm', window.opener.document).attr("action","${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=configIp&accept=Y&Update=1");
						$('#configureIpDomainForm', window.opener.document).submit();
					} else if("ac" == type) {
						$('#configureIpConsForm', window.opener.document).attr("action","${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=configIpCons&accept=Y&method=" + method);
						$('#configureIpConsForm', window.opener.document).submit();						
					}
				 	window.close();				
				});
					
				$("#acceptConditonN").click(function(){		
					window.close();				
				});
			
			});		
		</script>	
	</body>
</f:view>
</html>