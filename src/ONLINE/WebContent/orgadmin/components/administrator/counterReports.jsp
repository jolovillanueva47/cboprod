<%@ page import="java.util.Calendar" %>



<%-- 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page import="java.util.Calendar" %>
<%@ page isELIgnored="false"%>

<jsp:include flush="true" page="/jsp/ebooks/ebooksLayoutLogic.jsp" />


<h1>${orgName} - COUNTER Reports</h1>
--%>


<!-- Titlepage Start -->
<div class="titlepage_heading">
	<h1>${orgName} - COUNTER Reports</h1>
</div>
<!-- Titlepage End -->

<br />


<div id="body_admin">			
				
	<form name="counterStatForm" action="<%= request.getContextPath() %>/ebooks/genEbooksCountRep" method="get">
		<table cellspacing="0" >			
			<tr class="shaded">
				<th colspan="4">Selection criteria</th>
			</tr>			
			<tr>
				<td width="15%">Year</td>
				<td width="85%" colspan="3">
					<select name="<%=org.cambridge.ebooks.orgadmin.administrator.EbooksGenerateCounterReportServlet.YEAR %>">
					<% 
						int startYear = 2009;  
						Calendar cal = Calendar.getInstance();
						int currentYear = cal.get(Calendar.YEAR);
						while ( startYear <= currentYear ) {
					%>
							<option value="<%= startYear %>"><%= startYear++ %>&nbsp;</option>
					<%
						}	
					%>						
					</select> 
				</td>
			</tr>					
			<tr class="border">
				<td>Report</td>
				<td colspan="3">
					<input type="radio" id="report2" name="<%=org.cambridge.ebooks.orgadmin.administrator.EbooksGenerateCounterReportServlet.REPORT %>" value="2" checked />&nbsp;<label for="report2">Book Report 2: Number of Successful Section Requests by Month and Title</label><br/>
					<!-- <input type="radio" id="report3" name="<%=org.cambridge.ebooks.orgadmin.administrator.EbooksGenerateCounterReportServlet.REPORT %>" value="3" /><label for="report3">Book Report 3: Turnaways by Month and Title</label><br/> -->
					<!-- <input type="radio" id="report5" name="<%=org.cambridge.ebooks.orgadmin.administrator.EbooksGenerateCounterReportServlet.REPORT %>" value="5" />&nbsp;<label for="report5">Book Report 5: Total Searches and Sessions by Month and Title</label> --> 
					<input type="radio" id="report6" name="<%=org.cambridge.ebooks.orgadmin.administrator.EbooksGenerateCounterReportServlet.REPORT %>" value="6" />&nbsp;<label for="report5">Book Report 6: Total Searches and Sessions by Service</label>
				</td> 
			</tr>	
			<%-- 
			<tr class="border">	
				<td>Body Id</td>
				<td colspan="3"><input type="text" name="BODY_ID" /></td>
			</tr>
			--%>			
			
			<tr class="border">
				<td>Report type</td>
				<td colspan="3">
					<input type="radio" value="excel" checked />&nbsp;Excel		
				</td>			
				
			</tr>

		    <tr class="border">
		    	<td colspan="4">
		    		<p>Note:<p>
		    		<p><b>Usage statistics are unavailable for user sessions taking place between September 4-11. The other days of the month remain unaffected. We apologize for any inconvenience and will endeavor to prevent this from happening in the future.</b><p>
		    		<p>Only usage data for Cambridge Books Online products are available through this site.</p>
		    		<p>If you require usage statistics for Cambridge Companions Online, Historical Statistics of the United States, Orlando, Econometric Society Monographs online, Lectrix - Complete Texts, Lectrix - Greek Texts, Lectrix - Latin Texts, or Shakespeare Survey Online, please download them from <a href="http://subs.sams.cup.semcs.net/">http://subs.sams.cup.semcs.net/</a>.</p>
		    	</td> 
		    </tr>
		</table>
							
		<table cellspacing="0">
			<tr>
				<td colspan="2">
					<!-- Form button start -->
					<div class="form_button">
						<ul>
							<li><span><input type="submit" value="Run report" name="cmd"  /></span></li>
						</ul>
					</div>
					<!-- Form button end -->
				</td>
			</tr>
		</table>
	</form>
</div>