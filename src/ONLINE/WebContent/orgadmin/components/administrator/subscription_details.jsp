<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Organisational Access Details" scope="session" />

${accessBean.initialize}

<div class="titlepage_heading">
	<h1>Organisational Access Details</h1>
</div>
<br />	
<div id="subscribedlist">

	<p>Below is a list of Cambridge Books Online titles to which your organisation has access.</p>	
	
	<div id="body">
	
		<c:set var="chkOrg" value=""/> 
		<c:set var="count" value="0"/>
		<c:set var="chkOrg" value=""/> 
	
		Sort By:&nbsp;
		<select name="sortBy" id="sortBy" class="select_02">		
			<option value="1">Title</option>
			<option value="2">ISBN</option>
		</select>
		
		<br />
		<br />

		<ul id="tablist">
			<li><a class="current" id="tab1-header" href="">Organisational Access</a></li>
			<li><a id="tab2-header" href="">Demo Access</a></li>
			<li><a id="tab3-header" href="">Free Trial</a></li>
			<li><a id="tab4-header" href="">Subscription Access</a></li>
			<li><a id="tab5-header" href="">Patron Driven Acquisition</a></li>
		</ul>
		 
		<div id="tabcontentcontainer">
			<div id="tab1" class="tabcontent" style="display:block">
				<div id="adminTab">
					<a class="link03" onclick="findItems('INDI_ORG','div_01',${param.sortBy});" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Individual Titles</h1>
					</a>
					<div id="div_01" style="display:none;">
					</div>
					<a class="link03" onclick="findItems('COLL_ORG','div_02',${param.sortBy});" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Collections</h1>
					</a>
					<div id="div_02" style="display:none;">
					</div>			
				</div>
			</div>

			<div id="tab2" class="tabcontent" style="display:none">
				<div id="adminTab">
					<a class="link03" onclick="findItems('INDI_DEMO','div_03',${param.sortBy});" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Individual Titles</h1>
					</a>
					<div id="div_03" style="display:none;">
					</div>			
					<a class="link03" onclick="findItems('COLL_DEMO','div_04','${param.sortBy}');" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Collections</h1>
					</a>
					<div id="div_04" style="display:none;">
					</div>		
				</div>
			</div>

			<div id="tab3" class="tabcontent" style="display:none">
				<c:choose>
					<c:when test="${'YES' eq accessBean.freeTrial}">
						Free Trial Active
					</c:when>
					<c:otherwise>
						Free Trial Inactive
					</c:otherwise>
				</c:choose>
			</div>	

			<div id="tab4" class="tabcontent" style="display:none">
				<div id="adminTab">
					<a class="link03" onclick="findItems('INDI_SUBS','div_05','${param.sortBy}');" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Individual Titles</h1>
					</a>
					<div id="div_05" style="display:none;">
					</div>			
					<a class="link03" onclick="findItems('COLL_SUBS','div_06','${param.sortBy}');" href="javascript:void(0);">
						<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Collections</h1>
					</a>
					<div id="div_06" style="display:none;">
					</div>			
				</div>
			</div>

			<div id="tab5" class="tabcontent" style="display:none;">	
				<%-- 		
				<jsp:include page="pda_access_popup.jsp"></jsp:include>
				--%>
			</div>			
		</div> <!--  end tabcontentcontainer -->	
	</div> <!--  end body -->
</div> 