<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="New Remote User" scope="session" />


<%-- 
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>

<html:xhtml/>

--%>
<style type="text/css" media="screen">
	label.error {color:#f00;}
/* 	input.error {border: 1px dotted #f00;} */
</style>
<c:if test="${fromCJO}">
	<h1>New Remote User</h1>
</c:if>

<%-- <c:if test="${fromStahl}"> --%>
	<h1>New Remote User</h1>
<%-- </c:if> --%>

<%-- 
<c:if test="${fromCJO}">
	<div id="floated_col">
		<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
	</div>
</c:if>


<div id="body">
	<div id="registered">
--%>
${newMemberBean.initialize}
<form action="/newMember"  method="post" id="newMemberForm">
	<c:set var="showContact" value="N"/>
	<div class="validate" id="errorDiv" style="display:none">
		<h4>Below is a list of errors. Please correct them before updating the administrator:</h4>
		<label id="errorLabel"></label>
	</div>
	<c:choose>
		<c:when test="${not empty existError}" >
			<div class="validate">
				${existError}
			</div>
			<c:if test="${showContact eq 'Y'}">
				<p>If you wish to add this user, please contact Customer Services.</p>
				<p><a href="mailto:${requestScope['settings'].csr_us_email}">North America Customer Services (USA,Canada and Mexico)</a></p>
				<p><a href="mailto:${requestScope['settings'].csr_uk_email}">UK Customer Services (rest of the world)</a></p>
			</c:if>
		</c:when>
		<c:otherwise>
			<p>Please enter the user details for your new remote user, then press Update. Note that the username and password need to be a minimum of four characters. These can be a combination of letters and numbers and are case-sensitive.</p>
		</c:otherwise>
	</c:choose>

	<table cellspacing="0">
		<tr class="border">
			<td colspan="2">* Required Information</td>
		</tr>
		<tr class="border">
			<th><label for="title">Title</label></th>
			<td class="fields"><input type="text" name="title" maxlength="48" accesskey="T"/></td>
		</tr>
		<tr>
		    <th><label for="firstName">First Name*</label></th>    
		    <td class="fields"><input type="text" id="firstName" name="firstName" maxlength="48" accesskey="F"/></td>
		</tr>
		<tr>
			<th><label for="surName">Last Name*</label></th>
			<td><input type="text" id="surName" name="surName" maxlength="48" accesskey="L"/></td>
		</tr>
		<tr>			
			<th><a name="countryName" id="countryName"></a><label for="country">Country*</label></th>
			<td class="fields">
			    <select id="country" name="country">
			    	<option value="17">United Kingdom</option>
			    	<option value="226" selected="selected">United States</option>
					<c:forEach var="country" items="${newMemberBean.countryList}" >
						<c:if test="${country.key ne '17' or country.key eq '226' }" >
							<option value="${country.key }">${country.value}</option>
						</c:if>
					</c:forEach>
				</select>
			</td>
		</tr> 
		<tr class="border">
			<th><label for="userName">Username*</label></th>
			<td>
				<p class="note">
					<input type="text" id="userName" name="userName" maxlength="24" accesskey="U"/>
					case sensitive, 4 to 24 characters only
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="passWord">Password*</label></th>
			<td>
				<p class="note">
					<input type="password" id="passWord" name="passWord" maxlength="24" accesskey="P"/>
					case sensitive, 4 to 24 characters only
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="passWord2">Confirm Password*</label></th>
			<td><input type="password" id="passWord2" name="passWord2" maxlength="24" accesskey="W"/></td>
		</tr>
		<tr>
			<th><label for="email">Email*</label></th>
			<td><input type="text" id="email" name="email"  maxlength="80" accesskey="E"/></td>
		</tr>
		<tr>
			<th><label for="email2">Confirm Email*</label></th>
			<td><input type="text" id="email2" name="email2" maxlength="80" accesskey="C"/></td>
		</tr>
		<tr class="border">
			<th><label for="activation">Activation Date*</label></th>
			<td>
				<p class="note">
					<input type="text" id="activation" name="activation" accesskey="A" value="${newMemberBean.activation}"/>
					DDMonYYYY (e.g. 01Jan2006)
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="expiration">Expiration Date</label></th>
			<td>
				<p class="note">
					<input type="text" id="expiration" name="expiration" accesskey="D"/>
					DDMonYYYY (e.g. 01Jan2006)
				</p>
			</td>
		</tr>
	</table>

	<table cellspacing="0" class="registered_buttons">
		<tr>
			<td>
				<c:choose>
					<c:when test="${fromEBooks}">
						<span class="button">
							<input name="Cancel" type="submit" class="button" value="<bean:message key='button.cancel'/>" onclick="javascript:bCancel=true;" accesskey="C" />
						</span>
				   		<span class="button">
							<input name="reset" type="reset" class="button" value="<bean:message key='button.reset'/>" accesskey="R" />
						</span>
						<span class="button">
							<input name="saveNewMember" type="submit" class="button" value="<bean:message key='button.save'/>" onclick="return checkData();" accesskey="S" />
						</span>
					</c:when>
					<c:otherwise>
					<div class="form_button">
						
					<span class="button">	<input name="Cancel" type="submit" class="button" value="Cancel" onclick="javascript:bCancel=true;" accesskey="C" />&nbsp;&nbsp; </span>
					<span class="button">	<input name="reset" type="reset" class="button" value="Reset" accesskey="R" />&nbsp;&nbsp;</span>
					<span class="button">	<input name="saveNewMember" type="button" class="button" value="Save" onclick="checkData();" accesskey="S" /></span>
					
					</div>
					
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</table>
</form>
<%-- 
	</div>
</div>
--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/javascripts.js"></script>

<script language="JavaScript" type="text/Javascript">
<!--
function empty(elem) {
	//alert('empty');
	return (elem.value == null) || (trim(elem.value).length == 0);
}

function checkData(){
	if($("#newMemberForm").valid()){
		var fields = new Array();
		var i = 0;
		var validDates= true;
		
		if(!$("#activation").val()){
			fields[i++] = "Activation Date is required.";
		}
		if(!validateInputDate($("#activation").val())){
			fields[i++] = "Activation Date is not valid.";
			validDates = false;
		}
		
		if (!validateInputDate($("#expiration").val())){
			fields[i++] = "Expiration Date is not valid.";
			validDates = false;
		}
		
		if(validDates){
			var actDate = $("#activation").val();
			actDate = convertMonthToInt(actDate.substring(2,5))+"/"+actDate.substring(0,2)+"/"+actDate.substring(5,9);
			var expDate = $("#expiration").val();
			expDate = convertMonthToInt(expDate.substring(2,5))+"/"+expDate.substring(0,2)+"/"+expDate.substring(5,9);
			
		 	var activationDate = new Date(actDate);
			var expirationDate = new Date(expDate);
			
			if(activationDate > expirationDate){
				alert('Expiration date comes earlier than Activation date.');
			}
			
		 	if (fields.length > 0){
				alert(fields.join('\n'));
				return false;
			}else{
				if(confirm('Are you sure you want to add this remote user?')){
					$("#newMemberForm").append("<input type='hidden' name='save' value='save' >");
					$("#newMemberForm").submit();
				}
			}
		}
	}
}


//-->
</script>
