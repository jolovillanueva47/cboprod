<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%-- <%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"--%> 
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />



<!-- Titlepage Start -->
<div class="titlepage_heading">
	<h1>Remote user access</h1>
</div>
<!-- Titlepage End -->

<br />

<div id="searchresults">
	<p>To give a user remote access to materials to which your organisation has access, click "Add Remote User". The list below shows all your remote users and their current status. To update any of their details or to activate or de-activate any of them, click on the user's name.</p>
	<p><b>Note:</b> if one of your library's patrons already has a username on Cambridge Books Online or on Cambridge Journals Online, you may want to grant remote user access to that existing username, instead of creating a new one. To do this, please email <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a> if you are in the Americas, or <a href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a> if you are in the United Kingdom or the rest of the world. Please provide the patron's first name, last name, email address, and existing CBO/CJO username (if you know it).</p>

	<div id="body">
		<form action="${stahlUrl}remoteUserAccess" method="post">

			<table cellspacing="0">
				<tr>
					<td colspan="4">
						<!-- Form button start -->
						<div class="form_button">
							<span>
								<c:choose>
									<c:when test="${fromEBooks}">
										<input type="submit" value="Add Remote User" class="button" name="newMember">
									</c:when>
									<c:otherwise>
										<input type="submit" value="New Remote User" class="button" name="newMember">
									</c:otherwise>
								</c:choose>
							</span>
						</div>
		        		<!-- Form button end -->
					</td>
				</tr>
			</table>
	
			<c:choose>
				<c:when test="${fromEBooks}">
					<jsp:include page="/orgadmin/components/sortingWithPaging.jsp">
						<jsp:param name="formName" value="remoteUserAccessForm" />
					</jsp:include> 
				</c:when>
				<c:otherwise>
					<jsp:include page="/orgadmin/components/sortingWithPaging.jsp">
						<jsp:param name="formName" value="remoteUserAccessForm" />
					</jsp:include> 
				</c:otherwise>
			</c:choose>
	
			<table cellspacing="0">
				<tr class="shaded">
					<th>Name</th>
					<th>Username</th>
					<th>Expiration</th>
					<th>Activated</th>
					<th>Activation Date</th>
				</tr>
				
				<c:forEach var="user" items="${remoteUserAccessForm.userList}" >
					<tr>
						<td><a href="memberDetails?memberId=${user.memberId}">${user.firstName} ${user.surName}</a></td>
						<td>${user.userName}</td>
						<c:choose>
							<c:when test="${null ne user.expirationDate}">
								<td><fmt:formatDate value="${user.expirationDate}" pattern="dd-MMM-yyyy"/></td>
							</c:when>
							<c:otherwise>
								<td>&nbsp;</td>
							</c:otherwise>
						</c:choose>
					    <td>${user.status}</td>
						<c:choose>
							<c:when test="${null ne user.activationDate}">
								<td><fmt:formatDate value="${user.activationDate}" pattern="dd-MMM-yyyy"/></td>
							</c:when>
							<c:otherwise>
								<td>&nbsp;</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
				
				
				<%-- sample data1 start--%>
					<tr>
						<td><a href="memberDetails?memberId=${user.memberId}">[user.firstName] [user.surName]</a></td>
						<td>[user.userName]</td>
						<td>[dd-MMM-yyyy]</td>
					    <td>[user.status]</td>
						<td>[dd-MMM-yyyy]</td>
					</tr>
				<%-- sample data1 end--%>
			</table>

			<c:choose>
				<c:when test="${fromEBooks}">
					<jsp:include page="/orgadmin/components/sortingWithPaging.jsp">
						<jsp:param name="formName" value="remoteUserAccessForm" />
					</jsp:include> 
				</c:when>
				<c:otherwise>
					<jsp:include page="/orgadmin/components/sortingWithPaging.jsp">
						<jsp:param name="formName" value="remoteUserAccessForm" />
					</jsp:include> 
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</div>




