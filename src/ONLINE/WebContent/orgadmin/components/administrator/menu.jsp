<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<c:set var="isMultipleAdmin" value="false" />
<o:isMultipleAdmin>
	<c:set var="isMultipleAdmin" value="true" />
</o:isMultipleAdmin>
                            	
<c:choose>
	<c:when test="${'AC' eq orgConLinkedMap.userLogin.userType}">
		<div class="menu">
			<h2>Account Administrator:</h2>
			<h2>${administratorBean.orgName}</h2>	
			<ul id="adminMenu">
				<li id="configIpCons"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=configIpCons">Configure IP Address Consortia</a></li>
				<c:if test="${isMultipleAdmin}">
					<li id="switchAccounts"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=switchAccounts">Switch Accounts</a></li>
				</c:if>
				<li id="usageStatistics"><span class="icons_img arrow01"></span><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=usageStat','popup','status=yes,scrollbars=yes,width=750,height=700');return false;">Usage Statistics</a></li>
		       	<li id="resolver"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=resolver">Open URL Resolver</a></li>
		 		<%--
		 		<li id="details"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/action/ebooks/updateConsortiumDetails">Update Consortium Details</a></li>
		 		 --%>
		 		<li id="details"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=orgDetails">Update Consortium Details</a></li>
		 		<%--
		 		<li id="hosting"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/action/ebooks/organisationExpiryAlertsDetail">Expiry Alerts</a></li>
		 		 --%>
		 		<li id="hosting"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=hosting">Hosting Fee Reminder</a></li>
		        <%--
		        <li id="changeAdmin"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/action/ebooks/changeAdministrator">Change Administrator</a></li>
		         --%>
		        <li id="changeAdmin"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=admin">Change Administrator</a></li>
		        <%--
		        <li id="logo"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/action/ebooks/updateConsortiaLogo">Update Consortium Logo</a></li>
		         --%>
		        <li id="logo"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=logo">Update Consortium Logo</a></li>
		  	</ul>
		</div>	
	</c:when>
	<c:when test="${!fn:contains(orgConLinkedMap.userLogin.userType, 'ru')}">
		<div class="menu text_01">
			<h2>Account Administrator:</h2>
			<h2>${administratorBean.orgName}</h2>	

			<ul id="adminMenu">			
				<li id="configIp"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=configIp">Configure IP Address</a></li>
				<li id="access"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=access&sortBy=1">Access to</a></li>
				<li id="remote"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=remote">Remote User Access</a></li>
				<c:if test="${isMultipleAdmin}">
					<li id="switchAccounts"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=switchAccounts">Switch Accounts</a></li>
				</c:if>
				<%-- this should be a pop-up page
				<li><span class="icons_img arrow01"></span><a href="javascript:openUsageStatistics()">Usage Statistics</a></li>
 				--%>
 				<li id="usageStatistics"><span class="icons_img arrow01"></span><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=usageStat','popup','status=yes,scrollbars=yes,width=750,height=700');return false;">Usage Statistics</a></li> 				
				<li id="resolver"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=resolver">Open URL Resolver</a></li>
		        <li id="details"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=orgDetails">Update Organisation Details</a></li>		 		
		        <li id="hosting"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=hosting">Hosting Fee Reminder</a></li>		 		
		        <li id="changeAdmin"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=admin">Change Administrator</a></li>
		        <li id="logo"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=logo">Update Organisation Logo</a></li>
				<%--
				<li id="pdaBatches"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=pda">PDA Batches</a></li>
				 --%>
				<li id="emailPref"><span class="icons_img arrow01"></span><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=email">Email Preferences</a></li>
		  	</ul>
		</div>
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>