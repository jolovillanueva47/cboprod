<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="New Remote User" scope="session" />


<%-- 
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>

<html:xhtml/>

--%>


<c:if test="${fromCJO}">
	<h1>New Remote User</h1>
</c:if>

<c:if test="${fromStahl}">
	<h1>New Remote User</h1>
</c:if>

<%-- 
<c:if test="${fromCJO}">
	<div id="floated_col">
		<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
	</div>
</c:if>


<div id="body">
	<div id="registered">
--%>

<form action="${stahlUrl}newMember"  method="post" >
	<c:set var="showContact" value="N"/>
	<c:choose>
		<c:when test="${remoteUserAccessForm.withError}" >
			<c:choose>
				<c:when test="${fromEBooks}">
					<div class="validate">
						${existError}
					</div>
				</c:when>
				<c:otherwise>
					<logic:messagesPresent>	
						<div class="validate">
							<html:messages header="errors.header.required" footer="errors.footer" property="requiredError" id="requiredError"><li>${requiredError}</li></html:messages>
							<html:messages header="errors.header.minimum" footer="errors.footer" property="minimumError" id="minimumError"><li>${minimumError}</li></html:messages>
							<html:messages header="errors.header.exist" footer="errors.footer" property="existError" id="existError">
								<li>${existError}</li>
								<c:set var="showContact" value="Y"/>
							</html:messages>
							<html:messages header="errors.header.general" footer="errors.footer" property="generalError" id="generalError"><li>${generalError}</li></html:messages>
						</div>
					</logic:messagesPresent>	
				</c:otherwise>
			</c:choose>
			<c:if test="${showContact eq 'Y'}">
				<p>If you wish to add this user, please contact Customer Services.</p>
				<p><a href="mailto:${requestScope['settings'].csr_us_email}">North America Customer Services (USA,Canada and Mexico)</a></p>
				<p><a href="mailto:${requestScope['settings'].csr_uk_email}">UK Customer Services (rest of the world)</a></p>
			</c:if>
		</c:when>
		<c:otherwise>
			<p>Please enter the user details for your new remote user, then press Update. Note that the username and password need to be a minimum of four characters. These can be a combination of letters and numbers and are case-sensitive.</p>
		</c:otherwise>
	</c:choose>

	<table cellspacing="0">
		<tr class="border">
			<td colspan="2">* <bean:message key='member.label.required'/></td>
		</tr>
		<tr class="border">
			<th><label for="title"><bean:message key='register.label.title'/></label></th>
			<td class="fields"><input type="text" name="title" styleClass="title_field" styleId="title" maxlength="48" accesskey="T"/></td>
		</tr>
		<tr>
		    <th><label for="firstName"><bean:message key='register.label.firstname'/>*</label></th>    
		    <td class="fields"><input type="text" name="firstName" styleClass="register_field" styleId="firstName" maxlength="48" accesskey="F"/></td>
		</tr>
		<tr>
			<th><label for="surName"><bean:message key='register.label.lastname'/>*</label></th>
			<td><input type="text" name="surName" styleClass="register_field" styleId="surName" maxlength="48" accesskey="L"/></td>
		</tr>
		<tr>
			<c:if test="${null == page}">
				<c:if test="${fromStahl}">
		    		<th><a name="country" id="country"></a><label for="country"><bean:message key='member.label.country'/>*</label></th>
			    	<td class="fields">
			    		<html:select property="country" styleId="country" value="226">
							<html:optionsCollection property="countryList" label="value" value="key"/>
						</html:select>
		    		</td>
		    	</c:if>
				<c:if test="${fromCJO}">
					<th><a name="country" id="country"></a><label for="country"><bean:message key='member.label.country'/>*</label></th>
				    <td class="fields">
			    		<html:select property="country" styleId="country">
							<!--<html:option value="">Select Country</html:option>-->
							<html:optionsCollection property="countryList" label="value" value="key"/>
						</html:select>
					</td>
		    	</c:if>
				<c:if test="${fromEBooks}">
			    	<th><a name="country" id="country"></a><label for="country"><bean:message key='member.label.country'/>*</label></th>
				    <td class="fields">
			    		<html:select property="country" styleId="country" value="226">
							<!--<html:option value="">Select Country</html:option>-->
							<html:optionsCollection property="countryList" label="value" value="key"/>
						</html:select>
					</td>
				</c:if>
			</c:if>
		</tr> 
		<tr class="border">
			<th><label for="userName"><bean:message key='member.label.username'/>*</label></th>
			<td>
				<p class="note">
					<input type="text" name="userName" styleClass="register_field" styleId="userName" maxlength="24" accesskey="U"/>
					<bean:message key='register.label.case.char'/>
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="passWord"><bean:message key='member.label.password'/>*</label></th>
			<td>
				<p class="note">
					<html:password property="passWord" styleClass="register_field" styleId="passWord" maxlength="24" accesskey="P"/>
					<bean:message key='register.label.case.char'/>
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="passWord2"><bean:message key='register.label.confirm.password'/>*</label></th>
			<td><html:password property="passWord2" styleClass="register_field" styleId="passWord2" maxlength="24" accesskey="W"/></td>
		</tr>
		<tr>
			<th><label for="email"><bean:message key='member.label.email'/>*</label></th>
			<td><input type="text" name="email" styleClass="register_field" styleId="email" maxlength="80" accesskey="E"/></td>
		</tr>
		<tr>
			<th><label for="email2"><bean:message key='register.label.confirm.email'/>*</label></th>
			<td><input type="text" name="email2" styleClass="register_field" styleId="email2" maxlength="80" accesskey="C"/></td>
		</tr>
		<tr class="border">
			<th><label for="activation"><bean:message key='member.label.activation'/>*</label></th>
			<td>
				<p class="note">
					<input type="text" name="activation" styleClass="register_field" styleId="activation" accesskey="A"/>
					<bean:message key='register.label.format.date'/>
				</p>
			</td>
		</tr>
		<tr>
			<th><label for="expiration"><bean:message key='member.label.expiration'/></label></th>
			<td>
				<p class="note">
					<input type="text" name="expiration" styleClass="register_field" styleId="expiration" accesskey="D"/>
					<bean:message key='register.label.format.date'/>
				</p>
			</td>
		</tr>
	</table>

	<table cellspacing="0" class="registered_buttons">
		<tr>
			<td>
				<c:choose>
					<c:when test="${fromEBooks}">
						<span class="button">
							<input name="Cancel" type="submit" class="button" value="<bean:message key='button.cancel'/>" onclick="javascript:bCancel=true;" accesskey="C" />
						</span>
				   		<span class="button">
							<input name="reset" type="reset" class="button" value="<bean:message key='button.reset'/>" accesskey="R" />
						</span>
						<span class="button">
							<input name="saveNewMember" type="submit" class="button" value="<bean:message key='button.save'/>" onclick="return checkData();" accesskey="S" />
						</span>
					</c:when>
					<c:otherwise>	    
						<input name="Cancel" type="submit" class="button" value="<bean:message key='button.cancel'/>" onclick="javascript:bCancel=true;" accesskey="C" />
						<input name="reset" type="reset" class="button" value="<bean:message key='button.reset'/>" accesskey="R" />
						<input name="saveNewMember" type="submit" class="button" value="<bean:message key='button.save'/>" onclick="return checkData();" accesskey="S" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</table>
</form>
<%-- 
	</div>
</div>
--%>

<c:if test="${fromEBooks}">
	<script language="JavaScript" type="text/Javascript" src="/js/javascripts.js"></script>
</c:if>


<script language="JavaScript" type="text/Javascript">
<!--
function empty(elem) {
	//alert('empty');
	return (elem.value == null) || (trim(elem.value).length == 0);
}

function checkData()
{
var fields = new Array();
var i = 0;
var validDates= true;


if (empty(document.remoteUserAccessForm.activation)) 
{
  fields[i++] = "Activation Date is required.";
  document.remoteUserAccessForm.activation.focus();
}

if (!validateInputDate(document.remoteUserAccessForm.activation))
{
  fields[i++] = "Activation Date is not valid.";
  document.remoteUserAccessForm.activation.focus();
  validDates = false;
}

if (!validateInputDate(document.remoteUserAccessForm.expiration) )
{
  fields[i++] = "Expiration Date is not valid.";
  document.remoteUserAccessForm.expiration.focus();
  validDates = false;
}
if(validDates){
	var actDate = document.remoteUserAccessForm.activation.value;
	actDate = convertMonthToInt(actDate.substring(2,5))+"/"+actDate.substring(0,2)+"/"+actDate.substring(5,9);
	var expDate = document.remoteUserAccessForm.expiration.value;
	expDate = convertMonthToInt(expDate.substring(2,5))+"/"+expDate.substring(0,2)+"/"+expDate.substring(5,9);
	var activationDate = new Date(actDate);
	var expirationDate = new Date(expDate); 
	if(activationDate > expirationDate){
		alert('Expiration date comes earlier than Activation date.');
	}
}   
   if (fields.length > 0){
		alert(fields.join('\n'));
		return false;
	}else{
		return confirm('Are you sure you want to add this remote user?');
	}
}
//-->
</script>
