<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

${configureIpBean.initialize}

<form id="configureIpConsForm" action="" method="post">
	<div class="titlepage_heading">
		<h1>Configure IP Address</h1>
	</div>	
	<p>To give members of your organisation access to the material to which your organisation has access rights, you need to supply the IP addresses that identify the eligible machines on your network. You can also use this page to make changes to your IP configuration.</p>
	<p>To add a single IP address or range, enter it into the first text box below.  To add multiple IPs/ranges you can copy and paste comma-delimited IPs/ranges into the larger text box. If you want to exclude IP addresses that are part of a range, enter the IPs you wish to exclude in the first textbox and check the Exclude box.</p>
	<p>To delete a complete IP range, check the Delete box next to it. To save your changes, click the Update button.</p>
	<p><b>Note:</b> If your organisation also has access to Cambridge Journals Online, any updates you make here will be made in the Cambridge Journals Online system as well. If you have any questions about how your access for Cambridge Journals Online is configured, please contact your regional support team for Cambridge Journals Online. If you're in the US, Canada, or Mexico, please email <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a>. If you're in the UK, Europe, or the rest of the world, please email <a href="mailto:cjosupport@cambridge.org">cjosupport@cambridge.org</a>.</p>
	
	<c:if test="${configureIpBean.hasError}">
		<div class="validate">
			<c:forEach var="error" items="${configureIpBean.ipErrorMessage.map}">
				<c:choose>
					<c:when test="${'pattern' eq error.key}">
						<h4>The following have invalid IP Patterns:</h4>
					</c:when>		
					<c:when test="${'range' eq error.key}">
						<h4>The following are out of legal IP range:</h4>
					</c:when>	
					<c:when test="${'duplicate' eq error.key}">
						<h4>The following are already in the database:</h4>
					</c:when>	
					<c:when test="${'overlap' eq error.key}">
						<h4>The following overlaps with existing organisation IPs:</h4>
					</c:when>		
				</c:choose>
				<ul>
					<li>${error.value}</li>
	   			</ul>
	  		</c:forEach>
		</div>
	</c:if>
	<%-- Consortia - Org List --%>
	<c:if test="${not empty configureIpBean.consOrgOptionList}">
		<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="shaded">
					<th>Consortia - Organisation IP Address Configuration</th>
			    </tr>	
			</tbody>
		</table>
		<table class="config_ip_container">
		    <tr>
		    	<td>
		    		<span>Select organisation</span>			    		
		    	</td>
		    	<td>
		    		<select id="orgOption" name="orgOption">	
						<c:forEach var="consOrgOption" items="${configureIpBean.consOrgOptionList}">
							<c:choose>
								<c:when test="${'Y' eq consOrgOption.selected}">	
									<option value="${consOrgOption.consortiumOrganisation.organisation.bodyId}" selected>${consOrgOption.consortiumOrganisation.organisation.displayName}</option>
								</c:when>
								<c:otherwise>
									<option value="${consOrgOption.consortiumOrganisation.organisation.bodyId}">${consOrgOption.consortiumOrganisation.organisation.displayName}</option>
								</c:otherwise>
							</c:choose>														
						</c:forEach>				
					</select>
		    	</td>
		    	<td>
		    		<div class="form_button">			
						<span>
				    		<input class="confIpConsUpdate" id="changeSelectedOrg" type="button" value="Go" name="changeSelectedOrg"/>
				    	</span>
					</div>
		    	</td>
		    </tr>
		</table>
	</c:if>
	<%-- Organisation - Configure IP --%>
	<table border="0" cellpadding="3" cellspacing="1">
		<tbody>
			<tr class="shaded">
				<th>Internet Address</th>
		       	<th class="inputs">Exclude</th>
		       	<th class="inputs">Delete</th>
		    </tr>		
			<c:forEach var="ip" items="${configureIpBean.ipList}">
				<input id="debugIncludeFlag" type="hidden" value="${ip.includeFlag}" />
				<tr>
					<td>${ip.address}</td>			
					<td class="inputs">
						<c:choose>
							<c:when test="${'N' eq ip.includeFlag}">
								<input class="exclude" name="exclude" type="checkbox" value="${ip.addressId}" checked />
								<input style="display:none;" class="include" name="include" type="checkbox" value="${ip.addressId}" />
							</c:when>
							<c:otherwise>
								<input class="exclude" name="exclude" type="checkbox" value="${ip.addressId}" />
								<input style="display:none;" class="include" name="include" type="checkbox" value="${ip.addressId}" checked />
							</c:otherwise>
						</c:choose>
					</td>			
					<td class="inputs">
			       		<input class="delete" name="delete" type="checkbox" value="${ip.addressId}" />
			       	</td>
				</tr>
			</c:forEach>		
			<tr>
				<td>
					<input id="newIpAddr" type="text" name="newIpAddr" class="registerField" />
				</td>
				<td class="inputs">
					<input id="newExclude" type="checkbox" name="newExclude" value="N" />
				</td>
				<td class="inputs">&nbsp;</td>
			</tr>
			<tr class="border">
				<td colspan="3">
					<label>To enter a list of IP addresses at one time, please enter the IP addresses, delimited by comma (,) in the text area provided below.</label> <br />
					Please make sure that all the IPs inserted follow the standard valid <a href="#" onclick="MM_openBrWindow('${pageContext.request.contextPath}/popups/faq.jsf#IP_FORMAT_FAQ','help','toolbar=yes,status=yes,scrollbars=yes,resizable=yes,menubar=yes,directories=yes,location=yes,resizable=yes,width=820,height=640')">IP format</a>.				
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<textarea id="newIpList" class="message" name="newIpList" cols="1" rows="4"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="form_button">
						<ul>
							<li>							
								<span>
						    		<input type="reset" value="Reset" name="Reset"/>
						    	</span>
							</li>
							<li>							
						    	<span>
						        	<input class="confIpConsUpdate" id="updateOrg" type="button" value="Update Organisation" name="update"/>
					        	</span>
							</li>
						</ul>
					</div>
		       	</td>
			</tr>
		</tbody>
	</table>
	
	<%-- Consortia - Configure IP --%>
	<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr class="shaded">
				<th>Consortia IP Address Configuration</th>
		    </tr>		
		</tbody>
	</table>
	<table border="0" cellpadding="3" cellspacing="1">
		<tbody>
			<tr class="shaded">
				<th>Internet Address</th>
		       	<th class="inputs">Exclude</th>
		       	<th class="inputs">Delete</th>
		    </tr>		
			<c:forEach var="ip" items="${configureIpBean.consIpList}">
				<input id="debugIncludeFlagCons" type="hidden" value="${ip.includeFlag}" />
				<tr>
					<td>${ip.address}</td>			
					<td class="inputs">
						<c:choose>
							<c:when test="${'N' eq ip.includeFlag}">
								<input class="exclude" name="excludeCons" type="checkbox" value="${ip.addressId}" checked />
								<input style="display:none;" class="include" name="includeCons" type="checkbox" value="${ip.addressId}" />
							</c:when>
							<c:otherwise>
								<input class="exclude" name="excludeCons" type="checkbox" value="${ip.addressId}" />
								<input style="display:none;" class="include" name="includeCons" type="checkbox" value="${ip.addressId}" checked/>
							</c:otherwise>
						</c:choose>
					</td>			
					<td class="inputs">
			       		<input class="delete" name="deleteCons" type="checkbox" value="${ip.addressId}" />
			       	</td>
				</tr>
			</c:forEach>		
			<tr>
				<td>
					<input id="newIpAddrCons" type="text" name="newIpAddrCons" class="registerField" />
				</td>
				<td class="inputs">
					<input id="newExcludeCons" type="checkbox" name="newExcludeCons" value="N" />
				</td>
				<td class="inputs">&nbsp;</td>
			</tr>
			<tr class="border">
				<td colspan="3">
					<label>To enter a list of IP addresses at one time, please enter the IP addresses, delimited by comma (,) in the text area provided below.</label> <br />
					Please make sure that all the IPs inserted follow the standard valid <a href="#" onclick="MM_openBrWindow('${pageContext.request.contextPath}/popups/faq.jsf#IP_FORMAT_FAQ','help','toolbar=yes,status=yes,scrollbars=yes,resizable=yes,menubar=yes,directories=yes,location=yes,resizable=yes,width=820,height=640')">IP format</a>.				
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<textarea id="newIpListCons" class="message" name="newIpListCons" cols="1" rows="4"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="form_button">
						<ul>
							<li>							
								<span>
						    		<input type="reset" value="Reset" name="Reset"/>
						    	</span>
							</li>
							<li>							
						    	<span>
						        	<input class="confIpConsUpdate" id="updateCons" type="button" value="Update" name="update"/>
					        	</span>
							</li>
						</ul>
					</div>
		       	</td>
			</tr>
		</tbody>
	</table>
	
	<p>Please note that if you are part of a consortia agreement with us you will need to contact us or your Consortium Administrator to set up or amend your IPs.</p>  
	<p>To contact us: if you are in North America or South America, e-mail <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a>; if you are in the UK, Europe, or the rest of the world, email <a href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a></p>
</form>