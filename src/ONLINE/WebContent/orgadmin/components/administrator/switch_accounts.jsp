<%-- <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%> --%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page isELIgnored="false"%>

<script type="text/javascript">

	$(document).ready(function() {
//   		Handler for .ready() called.
		var lengthOfSelectedDefault = $('input[name=defaultAccountRadio]:checked').length
		if(lengthOfSelectedDefault < 1)
			$('input[name=defaultAccountRadio]:nth(0)').attr('checked', true);
	});


// 	function doBeforeSwitch(id,def1,def2,def3,def4,def5){
// 		jQuery("#"+id).after("<input type='hidden' name='Update' value='Switch'>");
// 		jQuery("#"+id).after("<input type='hidden' name='def1' value='" + def1 + "'>");
// 		jQuery("#"+id).after("<input type='hidden' name='def2' value='" + def2 + "'>");
// 		jQuery("#"+id).after("<input type='hidden' name='def3' value='" + def3 + "'>");
// 		jQuery("#"+id).after("<input type='hidden' name='def4' value='" + def4 + "'>");
// 		jQuery("#"+id).after("<input type='hidden' name='def5' value='" + def5 + "'>");
// 		document.getElementById(id).form.submit();
// 	}
	
</script>
${switchAccountsBean.initialize}

<div class="page-header">
<div class="ph-left">
<h2>Switch Accounts</h2>
</div>
</div>

<div class="description-box">
<p class="instructions"><span class="bold">Click on an account name to
administer that account.</span></p>
<p class="instructions">Click the <span class="bold">Default Account</span>
button to change the account selected by default when you log on.</p>
</div>

 <form action="#" method="post" id="switchAccountForm">
	<div id="registered">

	<table cellspacing="0">
		<tr class="shaded">
			<th class="title">Account</th>
			<th class="title">Default Account</th>
		</tr>
		
		
 		<c:forEach var="account" items="${switchAccountsBean.accountsList}">  
			<tr> 
 				<c:choose> 
 					<c:when test="${eBookLogoBean.orgConMap.currentlyLoggedBodyId eq account.bodyId}">
 						
 						<td><p>${account.displayName}</p></td>
					</c:when>
					<c:otherwise>
						<td><a href="#" title="Click here to administrate this account." 
						onclick="doBeforeSwitch('Update','${account.bodyId}','${account.bodyType}','${account.configureIPDisabled}','${account.displayName}','${account.memberBodyId}')">${account.displayName}</a></td>
					</c:otherwise>
				</c:choose>			
				<td><input type="radio" name="defaultAccountRadio" value="${account.bodyId}" <c:if test="${account.defaultAccount eq 'true'}">checked="checked"</c:if> /></td>
			</tr>
		</c:forEach>
		</table>
	</div>
	<div class="clear"></div>
	
	<div id="bottomButtons">
  		<span class="button">
			<input id="Reset" name="Reset" type="reset" class="button" value="Reset" accesskey="R" />
		</span>
  		<span class="button">
			<input id="Update" name="Update" type="button" class="button" value="Update" accesskey="U" onclick="doBeforeSubmit('Update','Update')"/>
		</span>
	</div>	

</form> 