<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

<%-- 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn" %>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>

<html:xhtml/>
--%>


<!-- Titlepage Start -->
<div class="titlepage_heading">
	<h1>Configure IP address</h1>
</div>
<!-- Titlepage End -->

<br />



<%-- 
	<c:if test="${empty fromEBooks}">
		<div id="floated_col">
			<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
		</div>
	</c:if>
--%>

	
		<form action="${stahlUrl}configureIPDomainConsortia" method="post">

			<p>To give members of your organisation access to the material to which your organisation has access rights, you need to supply the IP addresses that identify the eligible machines on your network. You can also use this page to make changes to your IP configuration.</p>
			<p>To add a single IP address or range, enter it into the first text box below.  To add multiple IPs/ranges you can copy and paste comma-delimited IPs/ranges into the larger text box. If you want to exclude IP addresses that are part of a range, enter the IPs you wish to exclude in the first textbox and check the Exclude box.</p>
			<p>To delete a complete IP range, check the Delete box next to it. To save your changes, click the Update button.</p>
			<p><b>Note:</b> If your organisation also has access to Cambridge Journals Online, any updates you make here will be made in the Cambridge Journals Online system as well. If you have any questions about how your access for Cambridge Journals Online is configured, please contact your regional support team for Cambridge Journals Online. If you're in the US, Canada, or Mexico, please email <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a>. If you're in the UK, Europe, or the rest of the world, please email <a href="mailto:cjosupport@cambridge.org">cjosupport@cambridge.org</a>.</p>					

			<%-- 
			<logic:messagesPresent message="true">
				<div class="validate">
					<html:messages message="true" header="errors.header.ip.pattern" footer="errors.footer" property="pattern" id="pattern"><li>${pattern}</li></html:messages>
					<html:messages message="true" header="errors.header.ip.range" footer="errors.footer" property="range" id="range"><li>${range}</li></html:messages>
					<html:messages message="true" header="errors.header.ip.duplicate" footer="errors.footer" property="duplicate" id="duplicate"><li>${duplicate}</li></html:messages>
					<html:messages message="true" header="errors.header.ip.overlap" footer="errors.footer" property="overlap" id="overlap"><li>${fn:substringBefore(overlap, '<a')}</li></html:messages>
				</div>
			</logic:messagesPresent>
			<logic:messagesPresent>
			<p class="alert">
				<html:messages id="errors">${errors}</html:messages>
			</p>
			</logic:messagesPresent>
			--%>

			
			<%-- 	DONT FORGET TO UNCOMMENT ME!
			<c:if test="${not empty consortiaIPForm.orgNameIDList}">
			--%>
			<a id="organisation"></a>
		
			<h2>Consortia - Organisation IP Address Configuration</h2>
		
			<input type="hidden" name="orgIdPrevSelected" value="${consortiaIPForm.orgIdPrevSelected}" />	
			
			<div class="select"> Select organisation
				<%-- 
				<html:select property="orgIDSelected">
					<html:optionsCollection property="orgNameIDList" label="orgName" value="orgID"/>
		  		</html:select>
		  		--%>
		  		<select>
		  			<c:forEach var="org" items="${orgNameIDList}" >
		  				<option value="xxx.orgID">sameple orgName</option>
				  	</c:forEach>
		  		</select>
		  		
		  		
				<div class="form_button">
					<span class="button">
		    			<input type="submit" value="Go" class="button" />
		    		</span>
				</div>
		  	</div>
		  			 	
			<table cellspacing="0">
		  		<tr class="shaded">
					<th><a href="#" onclick="MM_sort_consortiaOrgIP('consortiaIPForm', 'address', '${consortiaIPForm.orgSortOrder}'); return false;">Internet Address</a></th>
		       		<th class="inputs">Exclude</th>
		       		<th class="inputs">Delete</th>
		    	</tr>
				<c:forEach var="sub" items="${consortiaIPForm.orgIPList}">
					<tr>
						<td>${sub.address}&nbsp;</td>
						<td class="inputs"><input type="checkbox" name="includeOrgIPList" value="${sub.addressID}" /></td>
						<td class="inputs"><input type="checkbox" name="deleteOrgList" value="${sub.addressID}" /></td>
					</tr>
				</c:forEach>
		    	<tr>
					<td><input type="text" name="newOrgIPAddr" class="register_field" onblur="this.value=removeSpaces(this.value);"/></td>
					<td class="inputs"><input type="checkbox" name="newOrgExclude" /></td>
					<td class="inputs">&nbsp;</td>
				</tr>
		    </table>
		 
			<table cellspacing="0">
		  			<tr class="border">
						<td><label for="newOrgIPList">To enter a list of IP addresses at one time, please enter the IP addresses, delimited by comma (,) in the text area provided below.</label><br />
							Please make sure that all the IPs inserted follows the standard valid <a href="#" onclick="MM_openBrWindow('faq/answer?selectedTopicID=40&amp;selectedAnswerID=99','help','<bean:message key='pop.up.window.feature'/><bean:message key='pop.up.window.feature.size'/>')">IP format FAQ</a>.
						</td>
					</tr>	
					<tr>
						<td><textarea name="newOrgIPList" class="message" onblur="this.value=removeSpaces(this.value);" id="newOrgIPList" rows="4" cols="1"></textarea></td>
					</tr>
			</table>

			
			<%-- 				    
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<c:choose>
						<c:when test="${fromEBooks}">
							<td>
								<div class="form_button">
									<span class="button">
							    		<input type="reset" value="Reset" class="button" name="Reset" accesskey="R" />
							    	</span>
							    	<span class="button">
							        	<input type="submit" value="Update organisation" class="button" name="cmdUpdate2" accesskey="S" />
						        	</span>
								</div>
							</td>	
						</c:when>
						<c:otherwise>
						<td class="next">
							<input name="Reset" type="reset" class="button" value="Reset" accesskey="R" />
							<input name="cmdUpdate2" type="submit" class="button" value="Update organisation" />
						</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
			--%>			
			<table>
				<tr>
					<td>
						<div class="form_button">
							<span class="button">
					    		<input type="reset" value="Reset" class="button" name="Reset" accesskey="R" />
					    	</span>
					    	<span class="button">
					        	<input type="submit" value="Update organisation" class="button" name="cmdUpdate2" accesskey="S" />
				        	</span>
						</div>
					</td>
				</tr>
			</table>


			<c:if test="${not empty consortiaIPForm.organisationsIP}">
				<c:choose>
					<c:when test="${fromEBooks}">
						<h2>These IP addresses have also been registered by the following organisations for access to their own organisational purchases.</h2>
					</c:when>
					<c:otherwise>
						<h2>These IP addresses have also been registered by the following organisations for access to their own organisational subscriptions</h2>
					</c:otherwise>
				</c:choose>
				
				<table cellspacing="0">	
				<c:set var="prevId" value=""/> 
					<c:forEach var="sub" items="${consortiaIPForm.organisationsIP}">
						<c:if test="${sub.orgName != prevId}">
							<c:set var="prevId" value="${sub.orgName}"/> 	
							<tr class="shaded">
								<th>${sub.orgName}</th>
							</tr>
						</c:if>
						<tr>
							<td>${sub.address}</td>
						</tr>
					</c:forEach>
						<tr>
							<td>&nbsp;</td>
						</tr>	
				</table>
			</c:if>
		<%--  DONT FORGET TO UNCOMMENT ME!
		</c:if>
		--%>
	
			<h2>Consortia IP Address Configuration</h2>
			<table cellspacing="0">
			  <tr class="shaded">
					<th><a href="#" onclick="javascript:MM_sort_consortiaOwnIP('consortiaIPForm', 'address', '${consortiaIPForm.ownSortOrder}'); return false;">Internet Address</a></th>
			       	<th class="inputs">Exclude</th>
			       	<th class="inputs">Delete</th>
			    </tr>
				<c:forEach var="sub" items="${consortiaIPForm.ownIPList}">
				 <tr>
					<td>${sub.address}&nbsp;</td>
					<td class="inputs""><input type="checkbox" name="includeOwnIPList" value="${sub.addressID}" /></td>
			       	<td class="inputs"><input type="checkbox" name="deleteOwnList" value="${sub.addressID}" /></td>
				 </tr>
				</c:forEach>
				<tr>
					<td><input type="text" name="newOwnIPAddr" onblur="this.value=removeSpaces(this.value);" style="register_field" /></td>
					<td class="inputs"><input type="checkbox" name="newOwnExclude" /></td>
					<td class="inputs">&nbsp;</td>
				</tr>
			</table>
			
			<table cellspacing="0">
			  	<tr class="border">
					<td><label for="newOwnIPList">To enter a list of IP addresses at one time, please enter the IP addresses, delimited by comma (,) in the text area provided below.</label><br />
						Please make sure that all the IPs inserted follows the standard valid <a href="#" onclick="MM_openBrWindow('faq/answer?selectedTopicID=40&amp;selectedAnswerID=99','help','<bean:message key='pop.up.window.feature'/><bean:message key='pop.up.window.feature.size'/>')">IP format FAQ</a>.
					</td>
				</tr>	
				<tr>
					<td><textarea name="newOwnIPList" onblur="this.value=removeSpaces(this.value);" class="message" id="newOwnIPList" rows="4" cols="1"></textarea> </td>
				</tr>
			</table>


			<%--
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<c:choose>
						<c:when test="${fromEBooks}">
							<td>
								<span class="button">
							    	<input name="Reset" type="reset"  class="button" value="Reset" />
							    </span>
							    <span class="button">
							        <input name="cmdUpdate" type="submit" class="button" value="Update"  />
						        </span>
					       	</td>
						</c:when>
						<c:otherwise>
							<td>
								<input name="Reset" type="reset" class="button" value="Reset" />
								<input name="cmdUpdate" type="submit" class="button" value="<bean:message key='button.update'/>" />
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
			--%>
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
						<div class="form_button">
							<span class="button">
					    		<input type="reset" value="Reset" class="button" name="Reset" accesskey="R" />
					    	</span>
					    	<span class="button">
					        	<input type="submit" value="Update organisation" class="button" name="cmdUpdate2" accesskey="S" />
				        	</span>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="bodyID" value="${consortiaIPForm.bodyID}" />	
			<input type="hidden" name="ownSortBy"/>
			<input type="hidden" name="ownSortOrder"/>
			<input type="hidden" name="orgSortBy"/>
			<input type="hidden" name="orgSortOrder"/>
		</form>
	


<%
session.setAttribute("eventNo","114000");
%>

<script language="javascript" type="text/javascript">
	function removeSpaces(string) {
		var tstring = "";
		string = '' + string;
		splitstring = string.split(" ");
		for(i = 0; i < splitstring.length; i++)
		tstring += splitstring[i];
		return tstring;
	}
</script>


