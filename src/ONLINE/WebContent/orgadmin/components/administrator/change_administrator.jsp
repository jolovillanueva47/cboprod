<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />



<form action="/changeAdministrator" method="post">
	<!-- Titlepage Start -->
	<div class="titlepage_heading">
	  <h1>Change Administrator</h1>
	</div>
	<!-- Titlepage End -->
	<br />
	<c:if test="${not empty error}">
		<div class="validate">
			<h4>Below is a list of errors. Please correct them before updating the administrator:</h4>
			${error}
		</div>
		<c:if test="${changeAdministratorForm.errorType eq '500'}">
			<p>This is not allowed by our system, please contact Customer Services.</p>
			<p><a href="mailto:${requestScope['settings'].csr_us_email}">North America Customer Services (USA,Canada and Mexico)</a></p>
			<p><a href="mailto:${requestScope['settings'].csr_uk_email}">UK Customer Services (rest of the world)</a></p>
		</c:if>
	</c:if>	


   	<p>
   		Please enter the current account administrator's password and then the username and password 
     	for the new account administrator. Please note that if you have not already registered the username 
     	you would like for the new account administrator, you must first do so.
	</p>
    	
	<p>
		After you click "Update," the process of setting the new administrator may take a moment. 
		Please wait for this page to refresh.
	</p>

	<p>
		If you have any problems changing the administrator, or if you would like 
		to set up	multiple account administrator usernames, 
		please <a href="/contact.jsf" class="link01">contact us</a>.
	</p>
      
      
      
	<table cellspacing="0">
		<tr class="border">
			<th width="27%">Current administrator password</th>
			<td><input type="password" maxlength="24" class="registerField" accesskey="C" name="currentPassword" /></td>
		</tr>
		<tr>
			<th>New administrator username</th>
			<td><input type="text" maxlength="24" class="registerField" accesskey="U" name="username" /></td>
		</tr>
		<tr>
			<th>New administrator password</th>
			<td><input type="password" maxlength="24" class="registerField" accesskey="P" name="newPassword" /></td>
		</tr>
		<tr>
			<th>Confirm password</th>
			<td><input type="password" maxlength="24" class="registerField" accesskey="W" name="newConfirmPassword" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- Form button start -->
				<div class="form_button">
					<ul>
						<li><span><input type="reset" name="Reset" value="Reset" accesskey="R" /></span></li>
						<li><span><input type="submit" value="Update" name="updateAdmin" accesskey="U" /></span></li>
					</ul>
				</div>
				<!-- Form button end -->
			</td>
		</tr>
	</table>
</form>


<%--<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.Globals.TRANSACTION_TOKEN_KEY)%>"> --%>



        
        
        
        
        
        
        
<%-- from cjo 11-2 (struts) modified for ebooks use only


<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>

<div id="registered">
	<div id="body">
		<html:form action="${stahlUrl}changeAdministrator" method="post">
		
			<c:if test="${changeAdministratorForm.withError}">
				<div class="validate">
					<h4>Below is a list of errors. Please correct them before updating the administrator:</h4>
					<html:errors />
				</div>
				<c:if test="${changeAdministratorForm.errorType eq '500'}">
					<p>This is not allowed by our system, please contact Customer Services.</p>
					<p><a href="mailto:${requestScope['settings'].csr_us_email}">North America Customer Services (USA,Canada and Mexico)</a></p>
					<p><a href="mailto:${requestScope['settings'].csr_uk_email}">UK Customer Services (rest of the world)</a></p>
				</c:if>
			</c:if>	
		
		
			<p>Please enter the current account administrator's password and then the username and password for the new account administrator. Please note that if you have not already registered the username you would like for the new account administrator, you must first do so.</p>
			<p>After you click "Update," the process of setting the new administrator may take a moment. Please wait for this page to refresh.</p>
			<p>If you have any problems changing the administrator, or if you would like to set up multiple account administrator usernames, please <a href="<%=System.getProperty("ebooks.server.link") %>/templates/contact.jsf" class="link01">contact us</a>.</p>
		
		
			<table cellspacing="0">
			  <tr class="border">
				<th width="27%" nowrap="nowrap"><label for="currentPassword">Current Administrator Password</label></th>
			    <td class="fields"><html:password property="currentPassword" styleClass="register_field" styleId="currentPassword" maxlength="24" accesskey="C"/></td>
			  </tr>
			  <tr>
			    <th><label for="username">New Administrator Username</label></th>
			    <td><html:text property="username" styleClass="register_field" styleId="username" maxlength="24" accesskey="U"/></td>
			  </tr>
			  <tr>
			    <th><label for="newPassword">New Administrator Password</label></th>
			    <td><html:password property="newPassword" styleClass="register_field" styleId="newPassword" maxlength="24" accesskey="P"/></td>
			  </tr>
			  <tr>
			    <th><label for="newConfirmPassword">Confirm Password</label></th>
			    <td><html:password property="newConfirmPassword" styleClass="register_field" styleId="newConfirmPassword" maxlength="24" accesskey="W"/></td>
			  </tr>
			</table>
		
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
						<div id="bottomButtons">
				  			<span class="button">
								<input name="Reset" type="reset" class="button" value="Reset" accesskey="R" />
							</span>
				  			<span class="button">
								<input name="updateAdmin" type="submit" class="button" value="Update" accesskey="U" />
				  			</span>
					  	</div>
					</td>
				</tr>
			</table>

		
			<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.Globals.TRANSACTION_TOKEN_KEY)%>">


		</html:form>
	</div>
</div>

--%>
        