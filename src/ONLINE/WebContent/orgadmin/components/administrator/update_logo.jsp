<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

<div class="titlepage_heading">
	<h1>Update organisation logo</h1>
</div>	
<br />
${updateLogoBean.initialize}
<%-- 
       	<logic:messagesPresent>
			<p class="alert">
				<c:choose>
					<c:when test="${fromEBooks}" >
						<html:messages id="errors"><font color="red">${errors}</font></html:messages>
					</c:when>
					<c:otherwise>
					     <html:messages id="errors">${errors}</html:messages>
					</c:otherwise>
				</c:choose>
			</p>
		</logic:messagesPresent>
--%>       
   <font color="red">${param.errors}</font>     
   <form action="" method="post" enctype="multipart/form-data" id="updateLogoForm">     
		<p>As the Account Administrator, you can upload an image that will be displayed to all members 
		of your organisation whenever they visit Cambridge Books Online. 
		We suggest that you use your organisation's logo.</p>
		
		<p><strong>Note:</strong> In order for your image to be displayed on Cambridge Books Online, 
		it must be no larger than 85 pixels wide x 35 pixels tall, and no larger than 15 kb.</p>

        
<%-- 	<p>Image will be displayed for: <span class="strong">${updateLogoForm.name}</span></p>--%>
        <%-- <p>Image will be displayed for: <span class="strong">Faculty of Oriental Studies - Library</span></p> html--%>
		<p>Image will be displayed for: <span class="strong">${updateLogoBean.name}</span></p>


<%--	<p>Browse for and select an image and then click upload button. <bean:message key='label.clickRefresh'/></p>--%>
        <p>Browse for and select an image and then click upload button. Click refresh button or F5 to apply changes.</p>



<%-- 	<p><label for="logo">Image file: </label><html:file property="logo" accesskey="I" styleId="logo" /></p>--%>
		<p><label for="logo">Image file: </label><input type="file" name="logo" accesskey="I" value="" id="logo" /></p>
		<!-- Form button start -->
        <div class="form_button">
        	<span><input type="button" value="Upload logo" accesskey="U" id="uploadLogo" /></span>
		</div>
		<br>
		<div class="form_button">
			<c:if test="${updateLogoBean.image}">
				<p class="border_top">Your organisation is using the image displayed below.</p>
				<img src="${pageContext.request.contextPath}/organisation/logo/org${updateLogoBean.bodyId}" alt="Organisation Logo" styleClass="org_logo"/>
				<br><br>
				<ul>
					<li><span><input type="button" name="Remove" value="Remove" id="removeLogo" ></span></li>
					<li><span><input type="button" name="Done" value="Done" id="doneLogo" ></span></li>
				</ul>
			</c:if>
		</div>
</form>









<%--
        <h:form id="uploadForm" enctype="multipart/form-data" >
	        <p>
	        	<label for="logo">Image file: </label>
	        	
		        	<t:inputFileUpload accesskey="I" value="#{updateLogoBean.logo}" id="logo" storage="memory" required="true" />
	        	
	        </p>
	        
	        <!-- Form button start -->
	        <div class="form_button">
	        	<span><input type="reset" value="Upload logo" /></span>
			</div>
		</h:form>
--%>




<%-- if org has a logo, it will be displayed, with the option of buttons: "Remove Logo" or "Done" 		
		<c:if test="${updateLogoForm.image}">
			<p class="border_top">Your organisation is using the image displayed below.</p>
		
			<html:img src="${pageContext.request.contextPath}/images/organisations/org${updateLogoForm.bodyId}" alt="Organisation Logo" styleClass="org_logo"/>
				
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td>
			  			<span class="button">
						  	<input name="removeLogo" type="submit" class="button" value="Remove Logo" accesskey="R"/>
						</span>
						<span class="button">
							<input name="Done" type="submit" class="button" value="Done" accesskey="D"/>
						</span>
				  	</td>
				</tr>
			</table>
		</c:if>
--%>












<%-- CJO
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"  />

<html:xhtml/>

<c:if test="${fromCJO}">
	<c:choose>
		<c:when test="${userType eq 'AO'}">
			<h1>Update Organisation Logo</h1>
		</c:when>
		<c:otherwise>
			<h1>Update Society Logo</h1>
		</c:otherwise>
	</c:choose>
</c:if>

<c:if test="${fromStahl}">
	<h1>Update organization Logo</h1>
</c:if>

<c:if test="${fromEBooks}">
	<h1>Update Organisation Logo</h1>
</c:if>

<div id="registered">
	
	<c:if test="${fromCJO}">
		<div id="floated_col">
			<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
		</div>
	</c:if>
	
	<div id="body">
		<html:form action="${stahlUrl}updateLogo" method="post" enctype="multipart/form-data">
			<logic:messagesPresent>
				<p class="alert">
					<c:choose>
						<c:when test="${fromEBooks}" >
							<html:messages id="errors"><font color="red">${errors}</font></html:messages>
						</c:when>
						<c:otherwise>
						     <html:messages id="errors">${errors}</html:messages>
						</c:otherwise>
					</c:choose>
				</p>
			</logic:messagesPresent>
		
			<c:if test="${fromCJO}">
				<p>As the Account Administrator, you can upload an image that will be displayed to all members of your organisation 
				   whenever they visit CJO. We suggest that you use your organisation's logo. In order for your image be displayed 
				   on CJO, it must be be no larger than 85 x 35 pixels.
				</p>
			</c:if>
		
			<c:if test="${fromStahl}">
				<p>As the Account Administrator, you can upload an image that will be displayed to all members of your organization 
				   whenever they visit Stahl Online. We suggest that you use your organization's logo. In order for your image be displayed 
				   on Stahl Online, it must be be no larger than 85 x 35 pixels.
				</p>
			</c:if>
		
			<c:if test="${fromEBooks}">
				<p>As the Account Administrator, you can upload an image that will be displayed to all members of your organisation 
				   whenever they visit Cambridge Books Online. We suggest that you use your organisation's logo.</p> 
				<p><b>Note:</b> In order for your image to be displayed on Cambridge Books Online, it must be no larger than 
				   85 pixels wide x 35 pixels tall, and no larger than 15 kb.
				</p>
			</c:if>
		
			<c:if test="${fromStahl}">
				<p>Image will be displayed for: <span class="strong">${updateLogoForm.name}</span></p>
			</c:if>
		
			<c:if test="${fromEBooks}">
				<p>Image will be displayed for: <span class="strong">${updateLogoForm.name}</span></p>
			</c:if>
		
			<p>Browse for and select an image and then click upload button. <bean:message key='label.clickRefresh'/></p>
		
			<p><label for="logo">Image file: </label><html:file property="logo" accesskey="I" styleId="logo" /></p>
		
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td class="next">
						<c:choose>
							<c:when test="${fromEBooks}" >
								<div id="bottomButtons">
						  			<span class="button">
									  	<input name="uploadLogo" type="submit" class="button" value="Upload Logo" accesskey="U" onclick="return validateLogo('updateLogoForm')"/>
									</span>
							  	</div>
							</c:when>
							<c:otherwise>
								<div id="bottom_buttons">
								  	<input name="uploadLogo" type="submit" class="button" value="Upload Logo" accesskey="U" onclick="return validateLogo('updateLogoForm')"/>
							  	</div>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</table>
		
			<c:if test="${updateLogoForm.image}">
				<p class="border_top">Your organisation is using the image displayed below.</p>
			
				<html:img src="${pageContext.request.contextPath}/images/organisations/org${updateLogoForm.bodyId}" alt="Organisation Logo" styleClass="org_logo"/>
				
				<table cellspacing="0" class="registered_buttons">
					<tr>
					<c:choose>
						<c:when test="${fromEBooks}" >
							<td>
					  			<span class="button">
								  	<input name="removeLogo" type="submit" class="button" value="Remove Logo" accesskey="R"/>
								</span>
								<span class="button">
									<input name="Done" type="submit" class="button" value="Done" accesskey="D"/>
								</span>
						  	</td>
						</c:when>
						<c:otherwise>
						    <td class="next">
						      <input name="removeLogo" type="submit" class="button" value="Remove Logo" accesskey="R"/>
						      <input name="Done" type="submit" class="button" value="Done" accesskey="D"/>
						    </td>
						</c:otherwise>
					</c:choose>
					</tr>
				</table>
			</c:if>
		</html:form>
	</div>
</div>

--%>


