<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />


<!-- Titlepage Start -->
<div class="titlepage_heading">
	<h1>Update organisation details</h1>
</div>
<!-- Titlepage End -->

<div id="registered">
  <div id="body">
		<form name="organisationDetailsForm" method="post" action="/action/ebooks/updateOrganisationDetails?" onsubmit="javascript:return confirm('Are you sure you want to make these changes?');">
			<input name="updates" values="u" type="hidden">
			
			<c:if test="${organisationDetailsForm.withError}">
				<logic:messagesPresent>	
					<div class="validate">
						<html:messages header="errors.header.required" footer="errors.footer" name="requiredError" id="requiredError"><li>${requiredError}</li></html:messages>
						<html:messages header="errors.header.maximum" footer="errors.footer" name="maximumError" id="maximumError"><li>${maximumError}</li></html:messages>
						<html:messages header="errors.header" footer="errors.footer" name="existError" id="existError"><li>${existError}</li></html:messages>
						<html:messages header="errors.header.general" footer="errors.footer" name="generalError" id="generalError"><li>${generalError}</li></html:messages>
					</div>
				</logic:messagesPresent>	
			</c:if>	

			<table cellspacing="0">
				<tbody>
					<tr class="border">
						<td colspan="2">* Required Information</td>
					</tr>

					<tr class="border">
						<th><label for="type">Type*</label></th>
						<td>
							<select name="type" id="type">
								<option value="0" selected="selected">Institution</option>
								<option value="3">Company</option>
								<option value="5">Library within Institution</option>
								<option value="6">Department within Institution</option>
								<option value="4">Campus within Institution</option>
								<option value="7">Library within Company</option>
								<option value="8">Department within Company</option>
								<option value="9">Public Library</option>
								<option value="2">Other</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
    					<td><p class="note">Please select the most appropriate category for the body that you represent. Being as specific as possible here enables us to give you the best possible service.</p></td>
					</tr>
					<tr>
						<th><label for="athensId">Athens ID</label></th>
						<td><input type="text" name="athensIdList" styleClass="register_field" styleId="athensId" maxlength="300"/></td>
					</tr>
					<tr>
						<th><label for="name">Organisation Name*</label></th>
						<td><input type="text" name="name" styleClass="register_field" styleId="name"  maxlength="80" accesskey="O"/></td>  
					</tr>  
					<tr>
						<th><label for="displayName">Display Name*</label></th>
						<td><input type="text" name="displayName" styleClass="register_field" styleId="displayName" maxlength="80" accesskey="D"/></td>  
					</tr>    	
					<tr>
						<th>&nbsp;</th>
						<td><p class="note">Enter an appropriate display name for the body you represent. This is the name your users will see at the top of each Cambridge Books Online page. e.g. <em>Department of Physics, University Of Cambridge, University of Michigan</em></p></td>
					</tr>
				
					<%-- verify this tag's Location! --%>
					<input type="hidden" name="displayName" />

					<tr>
						<th>&nbsp;</th>
						<td><input type="hidden" name="siteLicense" /></td>
					</tr>
					<tr class="border">
						<th>
							<c:choose>
								<c:when test="${fromEBooks}" >
									<%-- no a name=country tag --%>
								</c:when>
								<c:otherwise>
								    <a name="country" id="country"></a>
								</c:otherwise>
							</c:choose>
							<label for="country">Country*</label>
						</th>
						<td>
							<select name="country" onchange="javascript:evalCountry('organisationDetailsForm', this.value);" id="country">
								<option value="">Select Country</option>
								<optionCollection name="countryList" label="value" value="key" />
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="county">County / State / Province</label></th>
						<td>
							<select name="county" styleId="county">
							<!--<html:option value="">Select</html:option>-->
								<optionsCollection name="stateCountyList" label="value" value="key"/>
							</select>
						</td> 
					</tr>
					<tr>
						<th><label for="city">Town / City*</label></th>
						<td><input type="text" name="city" styleClass="register_field" styleId="city" maxlength="30" accesskey="T"/></td>
					</tr>
					<tr>
						<th><label for="postCode">Post / Zip Code*</label></th>
						<td><input type="text" name="postCode" styleClass="register_field" styleId="postCode"  maxlength="100" accesskey="P"/></td>
					</tr>
					<tr>
						<th><label for="address1">Address*</label></th>
						<td><input type="text" name="address1" styleClass="register_field" styleId="address1" maxlength="30" accesskey="A"/></td>
					</tr>
					<tr>
						<th><label for="address2">Address 2</label></th>
						<td><input type="text" name="address2" styleClass="register_field" styleId="address2" maxlength="30" accesskey="E"/></td>
					</tr>
				</tbody>
			</table>
			
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td colspan="2">
						<!-- Form button start -->
						<div class="form_button">
							<ul>
								<li><span><input type="reset" name="Reset" value="Reset" accesskey="R" /></span></li>
								<li><span><input type="submit" value="Update" name="updates" accesskey="U" /></span></li>
							</ul>
						</div>
						<!-- Form button end -->
					</td>
				</tr>
			</table>
		
		<input type="hidden" name="addressId" />
		<input type="hidden" name="adminBodyId"/>
		<input type="hidden" name="image"/>
		<input type="hidden" name="urlResolver"/>

		</form>

	<script language="JavaScript" type="text/JavaScript">
	<!--
	function evalCountry(formName,countryId) {
		var form = document.forms[formName];
		var action = form.action;
		form.action = action + "?submitCountry=" + countryId + "#country";
		form.submit();
	}
	//-->
	</script>
		
	</div> <!-- end body -->
</div><!-- end registered-->






        