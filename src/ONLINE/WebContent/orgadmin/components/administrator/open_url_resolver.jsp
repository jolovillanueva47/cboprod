<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

${openURLResolverBean.initialize}

<%-- 
<script type="text/javascript" 
	src="<c:url value='/jsp/cjo/common/staticJavascript.jsp'/>" >
</script>
<html:javascript formName="openURLResolverForm" staticJavascript="false" />
--%>


<%--
<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>
 --%>


<form id="openURLResolverForm" action="" method="post" onsubmit="return on_submit();">
<!-- Titlepage Start -->
	<div class="titlepage_heading">
		<h1>Open URL Resolver</h1>
	</div>
<!-- Titlepage End -->
        
        
	<!--<c:if test="${!empty openURLResolverBean.currentURLResolver && openURLResolverBean.currentURLResolver != 'null'}">
		${openURLResolverBean.currentURLResolver}
	</c:if> -->
        
	<table cellspacing="0">
		<tr>
			<th width="20%" nowrap="nowrap">Current URL Resolver</th>
		<%-- html mock up             
			<td>http://metalib.lib.ic.ac.uk:9003/sfx_local</td> 
		--%>
			<td>
				<c:if test="${!empty openURLResolverBean.currentURLResolver && openURLResolverBean.currentURLResolver != 'null'}">
					${openURLResolverBean.currentURLResolver}
				</c:if>
			</td>
			
			<%-- is this ok (from cjo struts) 
			<input type="hidden" name="currentURLResolver" />
			--%>
		</tr>

		<tr>
			<th>New URL Resolver</th>
            <td>
            	<input type="text" name="newURLResolver" id="newURLResolver"  class="register_field" id="newURLResolver" maxlength="100" accesskey="N" />
            </td>
		</tr>

		<tr>
			<td colspan="2">
				<!-- Form button start -->
				<div class="form_button">
					<ul>
						<li><span><input type="reset" name="Reset" value="Reset" accesskey="R" /></span></li>
						<li><span><input id="openURLDelete" type="button" value="Delete" name="Delete"/></span></li>
						<!-- <li><span><input type="submit" name="DeleteURL" value="Delete" accesskey="D" onclick="javascript:bCancel=true;" /></span></li>  -->
						<li><span><input id="openURLUpdate" type="button" value="Update" name="Update"/></span></li>
						
					</ul>
				</div>
				<!-- Form button end -->
			</td>
		</tr>
	</table>
</form>





<%--         
<html:form action="${stahlUrl}openURLResolver" method="post" onsubmit="return on_submit(this);">
	<table cellspacing="0">
		<tr>
			<c:choose>
				<c:when test="${fromEBooks}">
					<th width="20%" nowrap="nowrap">Current URL Resolver</th>
				</c:when>
				<c:otherwise>	    
					<th nowrap="nowrap">Current URL Resolver</th>
				</c:otherwise>
			</c:choose>
			
			<td class="fields">
				<c:if test="${!empty openURLResolverForm.currentURLResolver && openURLResolverForm.currentURLResolver != 'null'}">
					${openURLResolverForm.currentURLResolver}
				</c:if>
			</td>
	    	<html:hidden property="currentURLResolver"/>
		</tr>
		<tr>
			<th><label for="newURLResolver">New URL Resolver</label></th> 
			<td><html:text property="newURLResolver" styleClass="register_field" styleId="newURLResolver" maxlength="100" accesskey="N"></html:text></td>
		</tr>
	</table>
	
	<table cellspacing="0" class="registered_buttons">
		<tr>
			<td>
				<c:choose>
					<c:when test="${fromEBooks}">
						<span class="button">	
							<input name="Reset" type="reset" class="button" value="Reset" accesskey="R" />
						</span>
			  			<span class="button">
							<input name="DeleteURL" type="submit" class="button" value="Delete" accesskey="D" onclick="javascript:bCancel=true;" />	
						</span>
					  	<span class="button">
					  		<input name="UpdateURL" type="submit" class="button" value="Update" accesskey="U" />
					  	</span>
					</c:when>
					<c:otherwise>
						  <input name="Reset" type="reset" class="button" value="Reset" accesskey="R" />
						  <input name="DeleteURL" type="submit" class="button" value="Delete" accesskey="D" onclick="javascript:bCancel=true;" />
						  <input name="UpdateURL" type="submit" class="button" value="Update" accesskey="U" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</table>

</html:form>

--%>