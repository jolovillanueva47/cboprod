<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Consortia Subscriptions" scope="session" />


<%-- 
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>

<html:xhtml/>
--%>


<c:if test="${fromCJO}">
<c:choose>
	<c:when test="${userType eq 'AC'}">
		<h1>Activate Consortia Subscriptions</h1>
	</c:when>
	<c:otherwise>
		<h1>Consortia Subscriptions</h1>
	</c:otherwise>
</c:choose>
</c:if>

<c:if test="${fromStahl}">
	<h1>Activate organizational subscriptions</h1>	
</c:if>

<div id="registered">

	<%-- 
	<div id="floated_col">
		<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
	</div>
	--%>
	
	<form action="${stahlUrl}activateOrganisationSubscriptions" method="post">
		
		<c:if test="${fromCJO}"> 
			<p>To activate your organisational subscriptions, enter your subscriber number in the text box below and click 'Activate'. <a href="#" onclick="MM_openBrWindow('faq/answer?selectedTopicID=40&amp;selectedAnswerID=39','faq','<bean:message key='pop.up.window.feature'/><bean:message key='pop.up.window.feature.size'/>'); return false;">FAQ</a></p>
		</c:if>
		
		
		<c:if test="${fromStahl}">
			<p>To activate your organizational subscriptions, enter your subscriber number in the text box below and click 'Activate'. <a href="#" onclick="MM_openBrWindow('<%= System.getProperty("stahl.server.link") %>/components/faq_link.jsf?faq_page=account_admin.html#account_admin_5','faq','<bean:message key='pop.up.window.feature'/><bean:message key='pop.up.window.feature.size'/>'); return false;">FAQ</a></p>	
		</c:if>
		
		
		<div id="body">
		<table cellspacing="0">
		  <tr class="headers">
			<th>Subscriber information</th>
			<th>Subscriber information fields </th>
		  </tr>
		  <tr class="border">
			<th>Subscriber Name</th>
			<td class="fields">${activateSubscriptionsForm.societyName}</td>
		  </tr>
		  <c:forEach var="count" begin="1" end="1" step="1">
			  <tr>
				<th nowrap="nowrap"><label for="subscriptionsNumber">Subscriber Number</label></th>
				<td><input type="text" name="subscriptionsNumber" class="subnum_field" maxlength="10" accesskey="S" id="subscriptionsNumber" /></td>
			  </tr>	
		  </c:forEach>
		</table>
	
		<table cellspacing="0" class="registered_buttons">
			<tr>
				<td colspan="2">
					<!-- Form button start -->
					<div class="form_button">
						<ul>
							<li><span><input type="submit" value="Activate" name="Activate" accesskey="A" onclick="javascript:return validateSubno('subscriptionsNumber','activateSubscriptionsForm','Subscriber Number is required')" /></span></li>
						</ul>
					</div>
					<!-- Form button end -->
				</td>
			</tr>
		</table>
		
		<table cellspacing="0">
		  	<tr class="border">
				<td>
				
					<c:if test="${fromCJO}">
						<label for="multipleSubNumber">Use this box to activate multiple organisational subscriber numbers.</label> Separate each number
				  		with a comma (e.g. 164380,23400,234561).
			  		</c:if>
			  		
			  		<c:if test="${fromStahl}">
						<label for="multipleSubNumber">Use this box to activate multiple organizational subscriber numbers.</label> Separate each number
			  			with a comma (e.g. 164380,23400,234561).
					</c:if>
			  		
			  	</td>
			</tr>
		  	<tr>
				<td nowrap="nowrap"><textarea name="multipleSubNumber" cols="1" rows="4" class="message" id="multipleSubNumber"/></textarea></td>
			</tr>
		</table>
		
		<table cellspacing="0" class="registered_buttons">
			<tr>
				<td colspan="2">
					<!-- Form button start -->
					<div class="form_button">
						<ul>
							<li><span><input type="submit" value="Activate multiple" name="ActivateMultiple" accesskey="M" onclick="javascript:return validateMultipleEntry('multipleSubNumber','activateSubscriptionsForm','Subscriber Number is required')" /></span></li>
						</ul>
					</div>
					<!-- Form button end -->
				</td>
			</tr>
		</table>
		</div>
	</form>
</div>