<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

<div class="titlepage_heading">
	<h1>Set Admin Type</h1>
</div>	
<br />

<form action="" method="post">
	${EBooksSetAdminTypeBean.initialize}
	<p>I would like to receive automated emails related to the below sites: </p>

	<ul>
		<li>
			<c:set var="cboAdmin" value="${EBooksSetAdminTypeBean.cboAdmin eq 'Y' ? 'checked=\"checked\"' : ''}" scope="page" />
			<input type="checkbox" name="cboAdmin" value="Y" ${cboAdmin} id="cboAdmin" />&nbsp;<label for="cboAdmin">Cambridge Books Online</label>			
		</li>
		<li>
			<c:set var="cjoAdmin" value="${EBooksSetAdminTypeBean.cjoAdmin eq 'Y' ? 'checked=\"checked\"' : ''}" scope="page" />
			<input type="checkbox" name="cjoAdmin" value="Y" ${cjoAdmin} id="cjoAdmin" />&nbsp;<label for="cjoAdmin">Cambridge Journals Online</label>			
		</li>
	</ul>
	<br/>
	<div>

	</div>
	
	<c:set var="bladeMemberId" value="${EBooksSetAdminTypeBean.bladeMemberId}" scope="page" />
	<input type="hidden" name="bladeMemberId" value="${bladeMemberId}" />
	<c:set var="orgId" value="${EBooksSetAdminTypeBean.bodyId}" scope="page" />
	<input type="hidden" name="orgId" value="${orgId}" />
	
	<div id="bottomButtons">  			
		<div class="form_button">
			<input type="hidden" name="action" value="update" />
        	<span><input type="submit" name=updates value="Update" /></span>
		</div>
	</div>
</form>