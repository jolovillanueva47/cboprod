<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Subscriptions" scope="session" />


<%--
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/jsp/stahl/stahlUrl.jsp"/>
 --%>
 
<c:choose>
	<c:when test="${userType eq 'AC'}">
		<h1>Consortia Subscriptions</h1>
	</c:when>
	<c:otherwise>
		<h1>Organisational Subscriptions</h1>
	</c:otherwise>
</c:choose>


<form action="${stahlUrl}activateOrganisationSubscriptions" method="post">
	<div id="registered">
		
		<%-- 
		<div id="floated_col">
			<jsp:include page="/jsp/cjo/common/m_banner_right.jsp"/>
		</div>
		--%>
		<div id="body">
		
			<c:set var="withError" scope="request" value="N"/> 
		 	
			<%-- 
			<c:if test="${!empty activateSubscriptionsForm.subscribedJournals}">
			--%>
				<p>The following organisation subscriptions have been activated. </p>
				<table cellspacing="0">
					<c:set var="orgName" value=""/>
					<%-- 
					<c:forEach var="journals" items="${activateSubscriptionsForm.subscribedJournals}" >
						<c:if test="${journals.orgName ne orgName}">
					--%>
							<tr class="border">
								<th>Subscriber Name </th>
							<%--<td class="fields">${journals.orgName}</td>--%>
								<td class="fields">[sample: journals.orgName]</td>
							</tr>
						  <c:set var="orgName" value="${journals.orgName}"/>
					<%-- 	
						</c:if>
					--%>
						<tr>
							<th nowrap="nowrap">Journal Subscription</th>
						<%--<td><a href="displayJournal?jid=${journals.journalId}">${journals.journalTitle}</a>, Vol.${journals.startVolume}, Issue ${journals.startIssue}</td>--%>
							<td><a href="#">[sample: journals.journalTitle]</a>, Vol. [journals.startVolume], Issue [journals.startIssue]</td>
						</tr>
					<%-- 
					</c:forEach>
					--%>
				</table>
			<%-- 
			</c:if>
			--%>
			
		<%--	error msgs / externalize msgs	
			<c:if test="${activateSubscriptionsForm.subscribedActive ne null}">
				<div class="validate">
					<h4>The following subscriber number has already been activated by your account. To view the activated subscriptions, select Subscribed to under the Browse Journals panel.
					If you would like to activate another subscriber number, please select &#8216;Activate more&#8216;. </h4>
					<ul>
						<c:forTokens var="subsNum" items="${activateSubscriptionsForm.subscribedActive}" delims=",">
							<li>${subsNum}</li>				
						</c:forTokens>
					</ul>
				</div>
				<c:set var="withError" scope="request" value="Y"/>
			</c:if>
		
			<c:if test="${activateSubscriptionsForm.subscribedInvalid ne null}">
				<div class="validate">
					<h4>The following subscriber numbers are invalid.  Please select &#8216;Activate more&#8217; to re-enter your subscriber number. </h4>
					<ul>
						<c:forTokens var="subsNum" items="${activateSubscriptionsForm.subscribedInvalid}" delims=",">
							<li>${subsNum}</li>				
						</c:forTokens>
					</ul>
				</div>
				<c:set var="withError" scope="request" value="Y"/> 
			</c:if>
		--%>		
			
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td colspan="2">
						<!-- Form button start -->
						<div class="form_button">
							<ul>
								<li><span><input type="submit" name="Activate more" value="Activate more" /></span></li>
								<li><span><input type="submit" value="Done" name="Done" /></span></li>
							</ul>
						</div>
						<!-- Form button end -->
					</td>
				</tr>
			</table>
			
			<%--
			<c:if test="${activateSubscriptionsForm.newUsertype ne null}">
			--%>
				<p>You are now the Account Administrator for the following organisation:</p>
				<%-- <h4>${activateSubscriptionsForm.societyName}</h4>--%>
				<h4>[sample: activateSubscriptionsForm.societyName]</h4>
				<p>Use the "Account Administrator" menu on the left of the page to view and manage your organisation preferences.</p>
			<%--
			</c:if>	
			--%>
			
			<%--
			<c:if test="${withError eq 'Y'}">
			--%>
				<p><a href="mailto:${requestScope['settings'].csr_us_email}">North America Customer Services (USA,Canada and Mexico)</a></p>
				<!--<p><a href="mailto:${requestScope['settings'].csr_us_email_tech}">North America Customer Services Technical Support (USA,Canada and Mexico)</a></p>-->
				<p><a href="mailto:${requestScope['settings'].csr_uk_email}">UK Customer Services (rest of the world)</a></p>
				<p><a href="#" onclick="MM_openBrWindow('faq/answer?selectedTopicID=40&amp;selectedAnswerID=39','faq','<bean:message key='pop.up.window.feature'/><bean:message key='pop.up.window.feature.size'/>')">FAQ</a></p>
			<%--
			</c:if>
			--%>
			
		</div> <!-- end body -->

		
		<div id="subscribedlist">
			<c:set var="chkOrg" value=""/>
			<c:set var="count" value="0"/>
	
	<%--  		
			<c:forEach var="journals" items="${activateSubscriptionsForm.accessedJournals}"> 
	--%>
				<c:if test="${journals.orgName ne chkOrg && count ne 0}" >
					</table>
				</c:if>
			
				<c:if test="${journals.orgName ne chkOrg}">
					<a name="${journals.orgName}" id="${journals.orgName}"></a>
					<h2>Organisational subscriptions for the ${journals.orgName}</h2>
					<table cellspacing="0">
						<tr class="headers">
							<th>Journals / Subscription year</th>
						</tr>
						<c:set var="chkOrg" value="${journals.orgName}" />	
						<c:set var="count" value="${count + 1}" />
				</c:if>
			
				<tr>
					<%-- 
					<td class="journal"><h3><a href="displayJournal?jid=${journals.journalId}">${journals.journalTitle}</a></h3></td>
					--%>
					<td class="journal"><h3><a href="displayJournal?jid=${journals.journalId}">[sample: journals.journalTitle]</a></h3></td>
				</tr>
				<tr class="year">
					<%-- 
					<td class="journal"><h4><i>${journals.withSubscription}</i></h4></td>
					--%>
					<td class="journal"><h4><i>[sample: journals.withSubscription]</i></h4></td>
				</tr>
	<%-- 
			</c:forEach> 
	--%>
			</table>
			
				
		</div> <!--  end subscribedlist -->
	</div> <!-- end registered -->
</form>
