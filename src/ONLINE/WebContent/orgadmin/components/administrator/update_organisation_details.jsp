<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />


<style type="text/css" media="screen">
	label.error {color:#f00;}
</style>
	
<div class="validate">
	<c:if test="${not empty param.success }">
	Update successful.
	</c:if>
</div>
<!-- Titlepage Start -->
<div class="titlepage_heading">
	<h1>Update organisation details</h1>
</div>
<!-- Titlepage End -->
${updateOrganisationDetailsBean.initialize}
<c:set var="orgDetails" value="${updateOrganisationDetailsBean.orgDetails }" />
<div id="registered">
  <div id="body">
		<form id="organisationDetailsForm" name="organisationDetailsForm" method="post" action="/updateOrganisationDetails" onsubmit="javascript:return confirm('Are you sure you want to make these changes?');">
			<input name="updates" value="u" type="hidden">
			<p>Use this form to keep your organisation's details up-to-date.</p>
			<table cellspacing="0">
				<tbody>
					<tr class="border">
						<td colspan="2">* Required Information</td>
					</tr>
					<tr class="border">
						<th><label for="type">Type*</label></th>
						<td>
							<select name="type" id="type">
								<c:forEach var="type" items="${updateOrganisationDetailsBean.typeList }" >
									<c:choose>
										<c:when test="${type.key eq orgDetails.type }">
											<option value="${type.key }" selected="selected">${type.value}</option>
										</c:when>
										<c:otherwise>
											<option value="${type.key }">${type.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
    					<td><p class="note">Please select the most appropriate category for the body that you represent. Being as specific as possible here enables us to give you the best possible service.</p></td>
					</tr>
					<tr>
						<th><label for="athensId">Athens ID</label></th>
						<td><input type="text" value="${orgDetails.athensId }" id="athensId" maxlength="300"/></td>
					</tr>
					<tr>
						<th><label for="name">Organisation Name*</label></th>
						<td><input type="text" value="${orgDetails.name}" name="name" id="name"  maxlength="80" accesskey="O"/></td>  
					</tr>  
					<tr>
						<th><label for="displayName">Display Name*</label></th>
						<td><input type="text" value="${orgDetails.displayName}" name="displayName" id="displayName" maxlength="80" accesskey="D"/></td>  
					</tr>    	
					<tr>
					<tr>
	    				<td><label for="displayMessage">Display Message</label></td>
	    				<td><textarea id="displayMessage"  rows="4" cols="50">${orgDetails.displayMessage }</textarea></td>
	  				</tr>
	  				<tr>
	  				<th>&nbsp;</th>
						<td><p class="note">Enter an appropriate display name for the body you represent. This is the name your users will see at the top of each Cambridge Books Online page. e.g. <em>Department of Physics, University Of Cambridge, University of Michigan</em></p></td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td><input type="hidden" name="siteLicense" /></td>
					</tr>
					<tr class="border">
						<th>
							<label for="country">Country*</label>
						</th>
						<td>
							<select name="country" id="country">
								<option value="">Select Country</option>
								<option value="17">United Kingdom</option>
			    				<option value="226" selected="selected">United States</option>
								<c:forEach var="country" items="${updateOrganisationDetailsBean.countryList}" >
									<c:if test="${country.key ne '17' or country.key eq '226' }" >
										<c:choose>
											<c:when test="${country.key eq orgDetails.country }">
												<option value="${country.key }" selected="selected">${country.value}</option>
											</c:when>
											<c:otherwise>
												<option value="${country.key }">${country.value}</option>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="county">County / State / Province</label></th>
						<td>
							<select name="county" name="county" id="county">
								<c:forEach var="county" items="${updateOrganisationDetailsBean.countyList}" >
									<c:choose>
										<c:when test="${county.key eq orgDetails.county }">
											<option value="${county.key }" selected="selected">${county.value}</option>
										</c:when>
										<c:otherwise>
											<option value="${county.key }">${county.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td> 
					</tr>
					<tr>
						<th><label for="city">Town / City*</label></th>
						<td><input type="text" value="${orgDetails.city}" id="city" name="city" maxlength="30" accesskey="T"/></td>
					</tr>
					<tr>
						<th><label for="postCode">Post / Zip Code*</label></th>
						<td><input type="text" value="${orgDetails.postCode}" id="postCode" name="postCode"  maxlength="100" accesskey="P"/></td>
					</tr>
					<tr>
						<th><label for="address1">Address*</label></th>
						<td><input type="text" value="${orgDetails.address1}" name="address1" id="address1" maxlength="30" accesskey="A"/></td>
					</tr>
					<tr>
						<th><label for="address2">Address 2</label></th>
						<td><input type="text" value="${orgDetails.address2}" id="address2" name="address2" id="address2" maxlength="30" accesskey="E"/></td>
					</tr>
				</tbody>
			</table>
			
			<table cellspacing="0" class="registered_buttons">
				<tr>
					<td colspan="2">
						<!-- Form button start -->
						<div class="form_button">
							<ul>
								<li><span><input type="reset" name="Reset" value="Reset" accesskey="R" /></span></li>
								<li><span><input type="submit" value="Update" name="updates" accesskey="U" /></span></li>
							</ul>
						</div>
						<!-- Form button end -->
					</td>
				</tr>
			</table>
		
		<input type="hidden" name="addressId" />
		<input type="hidden" name="adminBodyId"/>
		<input type="hidden" name="image"/>
		<input type="hidden" name="urlResolver"/>

		</form>


		
	</div> <!-- end body -->
</div><!-- end registered-->