<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="Configure IP addresses" scope="session" />

<!-- Titlepage Start -->
<div class="titlepage_heading">
  <h1>Hosting Fee Reminder</h1>
</div>
<!-- Titlepage End -->
<br />


<script language="javascript">
function validateExpiryAlert() {
	var elem = document.getElementById('emailAddress');
	if (isEmpty(elem)) {
		alert('Email Address is required.');
		elem.focus();
		return false;
	} else if (!checkEmail(elem.value)) {
		alert('Email Address is invalid.');
		elem.focus();
		return false;
	}
	
	elem = document.getElementById('emailAddress2');
	if (!isEmpty(elem)) {
		
		var separator = ",";
		var tokens = new Array();

		if (elem.value.substring(elem.value.length - 1, elem.value.length) == ',') {
			alert('Please enter a valid email address after each comma.');
			elem.focus();
			return false;
		}

		tokens = tokenize(elem.value, separator);
		for (i=0; i<tokens.length; i++) {
			if (!checkEmail(tokens[i])) {
				alert('Email Address is invalid.');
				elem.focus();
				return false;
			}
		}
	}
	return confirm('Are you sure you want to make these changes?');	
}

function checkEmailLength(elem){
	var length = elem.value.length;
	if(length == 100){
		alert("This field has a limit of 100 characters");
	}
}
</script>

<%--
<html:xhtml/>
 <h1>Expiry Alerts</h1> 
--%>



	
<form action="/ebooks/organisationExpiryAlertsDetail" method="post">

	<input type="hidden" name="bodyId" value="${ebooksOrganisationExpiryAlertsForm.subscriptionDetail.bodyId}" />
	
	<c:set value="${ebooksOrganisationExpiryAlertsForm.subscriptionDetail.expirationDate}" var="expiryDate" scope="request" />
			<%
				String expiryDate = (String) request.getAttribute("expiryDate"); 
				String dateDisplay = "";
				if(expiryDate != null && expiryDate.trim().length() > 0){
					java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
					java.util.Date date = sdf.parse(expiryDate);
					java.text.SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("ddMMMyyyy");
					dateDisplay = sdf2.format(date);
				}
				request.setAttribute("expiryDate", dateDisplay);
			%>

<%-- for testing purposes
	<c:choose>	
		<c:when test="${not empty expiryDate}">
--%>			
			<p>As the account administrator, you will receive an email reminder before the hosting fee on your institution's access is due. Our system will send an initial email 60 days before, a follow-up 30 days before, and another reminder 7 days before the hosting fee due date. If someone else at your institution should receive these reminders in addition to you, you can enter that person's email address below. </p>
			<table cellspacing="0">	
				<tr class="border">
					<th width="25%"><label for="title">Hosting Fee Due Date</label></th>
					<td class="fields">${expiryDate}</td>
				</tr>
				<tr class="border">
					<th width="25%"><label for="title">Copy to Email Address</label></th>
					<td class="fields">		
						<input type="text" name="emailAddress2" size="50" id="emailAddress2" value="${ebooksOrganisationExpiryAlertsForm.subscriptionDetail.emailAddress2}" maxlength="100" onkeyup="checkEmailLength(this)" />
						<br />
						If you would like this reminder to be sent to more than one email address please enter in the space provided. If you are entering multiple addresses they need to be separated by commas.
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<!-- Form button start -->
						<div class="form_button">
							<ul>
								<li><span><input type="submit" value="Update" name="cmd" accesskey="U" onclick="javascript:return validateExpiryAlert();" /></span></li>
							</ul>
						</div>
						<!-- Form button end -->
					</td>
				</tr>
			</table>
<%--
		</c:when>	
		<c:otherwise>
 --%>
			<p>Your organisation does not have any confirmed orders yet.  Hence, there is no need to remind you of your hosting fee. </p>
<%-- 
		</c:otherwise>
	</c:choose>
--%>	

</form>