<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>
<o:url var="current_dns" option="current_dns" />

<input type="hidden" id=current_dns value="${current_dns}" />
<input id="cjo_domain" type="hidden" value='<%=System.getProperty("cjo.domain") %>' />
<input type="hidden" id="page" value="${param.page}" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/for_librarians.js"></script>
