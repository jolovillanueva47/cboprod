<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>

<div class="titlepage_admin">
	<h1>Add <em>Cambridge Books Online</em> Widget to Your Website</h1>
</div>	
<div class="titlepage_admin">
	<h1>Cambridge Books Online Quick Search</h1>
</div>

<jsp:include page="/orgadmin/components/librarians/widget_search_box.jsp" />

<br/>

<c:import url="/orgadmin/components/librarians/widget_code_inline.jsp" var="cimprt_quick" />

<c:set var="trim_qsearch" value="${fn:trim(cimprt_quick)}"/>
		
<table cellspacing="0" cellpadding="0" style="width: 100px;">
	<tbody>
		<tr>
			<td>
				<p>Copy the code from the text below:</p>
				<textarea cols="44" rows="5" name="widgetCode_qsearch" id="widget_code_textarea" readonly="readonly">${trim_qsearch}</textarea>
			</td>
		</tr>
		<tr>
			<td>
				<div class="form_button fr">
					<span>
						<a href="javascript:void(0);" title="Select All" id="widget_code_button">Select All</a>
					</span>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<div class="titlepage_admin">
	<h1>The <em>Cambridge Books Online</em> widget is also available on widgetbox.com</h1>
</div>

<a href="http://www.widgetbox.com/widget/cambridge-books-online-quicksearch" target="_blank">You can view the <em>Cambridge Books Online</em> Quick Search widget on widgetbox.com here.</a>
<script type="text/javascript" src="${pageContext.request.contextPath}/orgadmin/js/widget.cbo.quick.js" ></script>