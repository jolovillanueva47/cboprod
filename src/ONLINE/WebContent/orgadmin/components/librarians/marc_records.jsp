<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<div class="titlepage_admin">
	<h1>Retrieve MARC Records</h1>	
</div>

<i>Cambridge Books Online</i> provides MARC records free of charge for
all titles in the collections. You may download a MARC21 format file (.mrc)
containing records of all titles available on the platform, or you may
download a MARC21 format file (.mrc) containing only those titles to which
your institution has purchased access.

<div id="rss_marcrecord">
	<p>
		<span class="icons_img rss_on">&nbsp;</span>To sign up for an RSS feed to be alerted to news related to Cambridge Books Online MARC records, 
		<a target="_blank" href="<%=System.getProperty("cjo.domain") %>data/rss/cbo_marcrecord_feed_rss.xml">click here</a>.
	</p>
</div>

<p>
	MARC record reader software such as MarcEdit will be required to add these
	files to your library system.
</p>

<div id="adminContent">
	<div id="body_admin">	
		<form name="counterStatForm" action="${pageContext.request.contextPath}/downloadMarcRecords" method="post">												
			<div>
			<!-- displays the orgList -->
			${marcRecords.initBean}				
				Download Records for: <select id="marcRecordsOption" name="option" class="select_02">	
					<option value="-1">All Published Books</option>
					<c:forEach var="orgMap" items="${marcRecords.orgList}">							
						<option value="${orgMap.bodyId}">${orgMap.name}</option>
					</c:forEach>				
				</select>
			</div><br>
		
			Order ID:&nbsp;<select id="orderIds" name="orderIds" style="width: 100px" class="select_02">
			</select>
		
			<!-- <div id="showOrderDate"> -->
			&nbsp;&nbsp;Order Date From:&nbsp;<input type="text" id="dateFrom" name="dateFrom" style="width:100px" /><!-- select id="dateFrom" name="dateFrom" style="width: 100px">
			</select>  -->
			&nbsp;To:&nbsp;<input type="text" id="dateTo" name="dateTo" style="width:100px"/><!-- <select id="dateTo" name="dateTo" style="width: 100px">
			</select> -->
			<br /><br />
			<div class="form_button">
				<span>
					<input name="cmd" type="submit" class="button" value="Download MARC Records" onclick="clearErrorMessage();" >
				</span>
			</div>
			<br />
			<c:if test="${param.error eq 'noMarcRecords'}">	
				<p id="errorMessage" style="color: red; padding-left: 5px; padding-top: 5px;">
					${param.errorMessage}
				</p>
			</c:if>
		</form>
	</div>
</div>
<div class="clear"></div>