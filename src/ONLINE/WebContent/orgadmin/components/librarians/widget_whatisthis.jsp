<div class="titlepage_admin">
	<h1>What Is This?</h1>
</div>
<p>The <a target="_blank" href="/"><em>Cambridge Books Online</em></a> Widget provides a single interaction point with <a target="_blank" href="/"><em>Cambridge Books Online</em></a> directly from your blog, "start page," or a social networking site.</p>
<p>After placing the widget in your blog or start page, or your preferred social networking site, you will be able to run quick search from that page on the content of all <a target="_blank" href="/"><em>Cambridge Books Online</em></a>. The results will display in a new window or tab of your browser.</p>
<p>To get the widget, please click on the link <a target="_blank" href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf?page=wadd" >Add <em>Cambridge Books Online</em> widget to your Website</a>. Copy and paste the code and embed it to your page.</p>
<p>You may input a search term in the search box and press the button 'Search'. A Quick Search will be run on the term on all <a target="_blank" href="/"><em>Cambridge Books Online</em></a> and the results will display in a new window or tab of your browser.</p>