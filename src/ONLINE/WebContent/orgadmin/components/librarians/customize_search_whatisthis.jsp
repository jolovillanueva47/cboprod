<div class="titlepage_admin">
	<h1>What Is This?</h1>
</div>				
<p>If you click the "Add Cambridge Books Online as a search option in your browser toolbar" link, Cambridge Books Online is added as a search option in your browser's dropdown search box, which is located in the upper right corner of your browser window.</p>
To add <em>Cambridge Books Online</em> as a search option in your browser toolbar (for IE7/Firefox):
<ul>
	<li>Click on the "Add Cambridge Books Online as a search option in your browser toolbar" link.</li>
	<li>Click on the Add Provider button.</li>
	<li>You should now see <em>Cambridge Books Online</em> in the list of search providers in the dropdown search box at the top right of the screen. To select it, use the dropdown arrow and click on <em>Cambridge Books Online</em>.</li>
	<li>Enter a search term and press Enter or click the magnifying glass icon to initiate your search.</li>
</ul>				
<br />
To remove <em>Cambridge Books Online</em> from the list of search options:
<br />			
For IE7:
<ul>
	<li>Click on the dropdown arrow next to the search bar's magnifying glass icon</li>
	<li>Select "Change Search Defaults."</li>
	<li>Select Cambridge Books Online.</li>
	<li>Click Remove, then click OK.</li>
</ul>
<br />
For Firefox:
<ul>
	<li>Click on the dropdown arrow next to the icon of the current search engine.</li>
	<li>Select "Manage Search Engines."</li>
	<li>Select Cambridge Books Online.</li>
	<li>Click Remove, then click OK.</li>
</ul>			