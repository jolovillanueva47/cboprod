<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>

<div class="menu">		
	<h2 class="titlepage_heading">Information for Librarians:</h2>
	<ul>
		<li>
			<a class="" href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf?page=mr"><span class="icons_img arrow01"></span>Retrieve MARC Records</a>	
		</li>		
		<c:forEach var="level2" items="${forLibrariansPageBean.streamPageLevel2List}">	
			<li>
				<span class="icons_img arrow01"></span>
				<a class="" href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf${fn:substringAfter(level2.url, 'action/stream')}&pageTitle=${level2.pageName}">${level2.pageName}</a>
				<ul>			
					<c:forEach var="level3" items="${level2.streamLevel3List}">		   			
						<li>
							<span class="icons_img arrow01"></span>
							<a href="">${level3.pageName}</a>
						</li>
					</c:forEach>
				</ul>
   			</li>
   		</c:forEach>
	</ul>		
	<h2>Cambridge Books Online Widget:</h2>
	<ul>
		<li>
			<a href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf?page=wwit">
				<span class="icons_img arrow01"></span>What Is This?
			</a>	
		</li>		
		<li>
			<a href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf?page=wadd">
				<span class="icons_img arrow01"></span>Add Cambridge Books Online Widget to Your Website
			</a>	
		</li>		
	</ul>
	<h2>Customize Your Search Toolbar:</h2>
	<ul>
		<li>
			<a href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf?page=cswit">
				<span class="icons_img arrow01"></span>What Is This?
			</a>
		</li>
		<li>
			<a href="javascript:void(0);" id="browser_toolbar">
				<span class="icons_img arrow01"></span>Add Cambridge Books Online as a Search Option in Your Browser Toolbar
			</a>
		</li>
	</ul>
</div>