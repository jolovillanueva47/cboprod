<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="helpType" value="child" scope="session" />
<c:set var="pageId" value="1712" scope="session" />
<c:set var="pageName" value="sorting with paging" scope="session" />

<%-- 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
--%>

<%--
	Notes:
	1. paging.jsp should be called within an HTML form. 
	2. The HTML form action should be able to generate the resultset. 
	3. The HTML form action should also be able to create an instance
	of the org.cambridge.cjo.lib.util.PagingUtil which contains paging information.
	4. The calling JSP should pass a "formName" parameter whose value is equal to the name 
	of the HTML form.
--%>

<jsp:useBean id="_pageBean" class="org.cambridge.ebooks.online.util.PagingUtil" scope="request"/>
<table class="search_navigation" cellspacing="0" border="0">

	<tr>
		<td colspan="1">
			<span>Sort by</span>	
			<select name="_pageBean" name="sortBy" id="sortBy" onchange="javascript:sort(this.options[this.selectedIndex].value);">
				<c:forEach var="collection" items="${_pageBean.sortCollection}" >
					<c:choose>
						<c:when test="${not empty param._sortBy}">
							<c:choose>
								<c:when test="${param._sortBy eq collection.key}">
									<option value="${collection.key}" selected="selected">${collection.value}</option>
								</c:when>
								<c:otherwise>
									<option value="${collection.key}">${collection.value}</option>
								</c:otherwise>
							</c:choose>						
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${'1' eq collection.key}">
									<option value="${collection.key}" selected="selected">${collection.value}</option>
								</c:when>
								<c:otherwise>
								<option value="${collection.key}">${collection.value}</option>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>				
		</td>

		<td>
			<span>Results per page</span>
    		<select name="_pageBean" name="pageSize" id="pageSize" onchange="javascript:jump2(1,this.options[this.selectedIndex].value);">
				<c:forEach var="i" begin="10" end="50" step="10">
					<c:choose>
					<c:when test="${not empty param._pageSize and param._pageSize eq i}">
						<option value="${i}" selected="selected">${i}</option>
					</c:when>
					<c:otherwise>
						<option value="${i}">${i}</option>
					</c:otherwise>
					</c:choose>
				</c:forEach>

			</select>
			<input type="hidden" name="sortBy2" value="SORTBYRELEVANCY"/>			
			&nbsp;
		</td>
		
		<td>
			<span>Page ${_pageBean.currentPage} of ${_pageBean.pageCount} | Go to page</span>
    		<select name="_pageBean" name="currentPage" id="currentPage" onchange="javascript:jump(this.options[this.selectedIndex].value)">
				<c:forEach var="i" begin="1" end="${_pageBean.pageCount}" >
					<c:choose>
						<c:when test="${_pageBean.currentPage eq i}">
							<option value="${i}" selected="selected">${i}</option>
						</c:when>
						<c:otherwise>
							<option value="${i}">${i}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			&nbsp;
		</td>
		
		<td colspan="2">
			<span>Go to:&nbsp;&nbsp;<a href="#" onclick="javascript:jump(1)">First</a> | <a href="#" onclick="javascript:jump(${_pageBean.previousPage})">Previous</a> | <a href="#" onclick="javascript:jump(${_pageBean.nextPage})">Next</a> | <a href="#" onclick="javascript:jump(${_pageBean.pageCount})">Last</a></span>
		</td>
		
	</tr>
</table>

<script type="text/javascript" language="Javascript">
//<!-- Begin
var form = document.forms["${param.formName}"];


function numeric(alphane)
{
	var numaric = alphane;
	for(var j=0; j<numaric.length; j++)
	{
		  var alphaa = numaric.charAt(j);
		  var hh = alphaa.charCodeAt(0);
		  if(!(hh > 47 && hh<58))
			 return false;
	}
		
	return true;
}
	
function validateAndGo (page) {		
	if (numeric(page)) {
		if (page > 0 && page <= ${_pageBean.pageCount}) {
			jump(page);	
		} else {
			alert('Please enter a number between 1 and ${_pageBean.pageCount}');
		}
	} else {
		alert('Please enter a valid number!');
	}
	
	return false;
}

function goToPage(page, e)
{
	var key;      
    if(window.event)
   		key = window.event.keyCode; //IE
    else
    	key = e.which; //firefox      

	if (key == 13) {
		validateAndGo(page);
		return false;
	} else if (key > 31 && (key < 48 || key > 57)) {
        return false;
	} else {
		return true;
	}
}

function jump(page) {
	var pagesize = $("#pageSize").val();
	var sortBy = $("#sortBy").val();
	var action = $("#remoteUserForm").attr('action');
	
	action += "&_currentPage=" + page + "&_pageSize=" + pagesize 
	+ "&_sortBy=" + sortBy + "&searchType=sort";
	
	$("#remoteUserForm").attr('action',action);
	$("#remoteUserForm").submit();	
}

function jump2(page,size) {
	
	var pagesize = size;
	var sortBy = $("#sortBy").val();
	var action = $("#remoteUserForm").attr('action');

	action += "&_currentPage=" + page + "&_pageSize=" + pagesize 
	+ "&_sortBy=" + sortBy + "&searchType=sort";
	
	$("#remoteUserForm").attr('action',action);
	$("#remoteUserForm").submit();	

}	

function sort(sortBy) {
	//var action = form.action;
	var pagesize = $("#pageSize").val();
	var page = $("#currentPage").val();
	var action = $("#remoteUserForm").attr('action');
	
	action += "&_currentPage=" + page + "&_pageSize=" + pagesize 
	+ "&_sortBy=" + sortBy + "&searchType=sort";
	
	$("#remoteUserForm").attr('action',action);
	$("#remoteUserForm").submit();	
}

function sort2(sortBy2) {
	var action = form.action;
	var pagesize = form.pageSize[0].options[form.pageSize[0].selectedIndex].value;
	var page = form.currentPage.value;
	var sortBy = form.sortBy[0].options[form.sortBy[0].selectedIndex].value;
	
	if (action.indexOf("?") > 0) {
		form.action = action + "&_currentPage=" + page + "&_pageSize=" + pagesize 
			+ "&_sortBy=" + sortBy + "&_sortBy2=" + sortBy2;
	} else {
		form.action = action + "?_currentPage=" + page + "&_pageSize=" + pagesize 
			+ "&_sortBy=" + sortBy + "&_sortBy2=" + sortBy2;
	}
	
	form.action += "&searchType=sort";
	
	var submitForm = true;		
	if(sortBy == "SORTBYACCESS") {
		var foo =window.confirm("<bean:message key='sortByAccess.confirm.message'/>");
		if(!foo) {		
			submitForm = false;
		}		
	} 
	if(submitForm) {	
		form.submit();
	}		
}

//End -->	
</script>			
