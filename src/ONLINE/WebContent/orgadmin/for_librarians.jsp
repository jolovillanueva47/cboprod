<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>
	    <link href="${pageContext.request.contextPath}/orgadmin/css/account_admin_styles.css" rel="stylesheet" type="text/css" />
	    <link href="${pageContext.request.contextPath}/css/jquery/datepicker/redmond/jquery-ui-1.7.2.redmond.css" rel="stylesheet" type="text/css" />
	</head>

	<body>	
	    <c:import url="/components/loader.jsp" />
	    
		<div id="main" style="display: none;">
	
		    <!-- bg Topline -->       
			<div id="top_container">	
				<c:set var="helpType" value="child" scope="session" />
				<c:set var="pageId" value="1744" scope="session" />
				<c:set var="pageName" value="Information for Librarians" scope="session" />
				<c:remove var="publisherCode" scope="session"/>
	
				<!-- page-wrapper start -->
				<div id="page-wrapper">					
					
					<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>
		
					<!-- Header -->
					<div id="header_container">	        	
						<f:subview id="top_menu">
							<c:import url="/components/top_menu.jsp" />
							<input type="hidden" value="${faqBean.cookieValue}" name="languageCookie"/>	
						</f:subview>	
						
						<f:subview id="search_container">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
			   	 	</div>	 
		        	
		        	<!-- Crumbtrail -->
					<div style="display: none;">
						<f:subview id="crumbtrail">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>
					</div>
		        	
		        	<!-- Main Body Start -->
		       		<div id="main_container">
		       			${forLibrariansPageBean.initialize}	
		       			
		       			<f:subview id="menu">
							<c:import url="/orgadmin/components/librarians/menu.jsp" />	
						</f:subview>
		       			
		       			<div id="adminContent">
		       				<div class="titlepage_heading">
			        			<h1>For Librarians</h1>
			        		</div>	
			        		
			        		<c:if test="${not empty param.pageTitle}">
			        			<div class="titlepage_admin">
		        					<h1>${param.pageTitle}</h1>	
		        				</div>
		        			</c:if>	
			        		
			        		<c:choose>
		        				<c:when test="${'mr' eq param.page}">
		        					<f:subview id="marc_records">
										<c:import url="/orgadmin/components/librarians/marc_records.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'wwit' eq param.page}">
		        					<f:subview id="widget_whatisthis">
										<c:import url="/orgadmin/components/librarians/widget_whatisthis.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'wadd' eq param.page}">
		        					<f:subview id="add_widget">
										<c:import url="/orgadmin/components/librarians/add_widget.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:when test="${'cswit' eq param.page}">
		        					<f:subview id="customize_search_whatisthis">
										<c:import url="/orgadmin/components/librarians/customize_search_whatisthis.jsp" />	
									</f:subview>
		        				</c:when>
		        				<c:otherwise>
		        					<p>
		        						${forLibrariansPageBean.content}
		        					</p>	     					
		        				</c:otherwise>	        				
	        				</c:choose>		
		       			</div>		        	
					</div>
					
					<div class="clear"></div>
					
					<f:subview id="footer_menuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>	        
				</div>
				<!-- page-wrapper end -->
			</div>
	    
			<f:subview id="footer_container">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>
	    </div>
	    
		<f:subview id="common_js">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		<f:subview id="for_librarians_js">
			<c:import url="/orgadmin/components/librarians/for_librarians_js.jsp" />
		</f:subview>
		<f:subview id="google_analytics">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
	</body>
</f:view>
</html>