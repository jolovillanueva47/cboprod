<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Cambridge Books Online - Register</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>
	</head>
	<body>	
		<c:import url="/components/loader.jsp" />
		
	    <div id="main" style="display: none;">
	    	<div id="top_container">
	    		<c:remove var="publisherCode" scope="session"/>
	    		
	    		<!-- page-wrapper start -->
				<div id="page-wrapper">
					
					<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>	
	    		
	    			<!-- Header -->
					<div id="header_container">	        	
						<f:subview id="top_menu">
							<c:import url="/components/top_menu.jsp" />
							<input type="hidden" value="${faqBean.cookieValue}" name="languageCookie"/>	
						</f:subview>	
						
						<f:subview id="search_container">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
			   	 	</div>
			   	 	<!-- Crumbtrail -->
<!-- 					<div style="display: none;"> -->
<%-- 						<f:subview id="crumbtrail"> --%>
<%-- 							<c:import url="/components/crumbtrail.jsp" />	 --%>
<%-- 						</f:subview> --%>
<!-- 					</div> -->
			   	 	<!-- Main Body Start -->
		       		<div id="main_container">
		       			<c:choose>
		       				<c:when test="${'updateReg' eq param.page}">
								<f:subview id="update_reg">
									<c:import url="/orgadmin/components/account_management/update_register.jsp" />	
								</f:subview>
		    				</c:when> 
		       				<c:when test="${'contentAlerts' eq param.page}">
								<f:subview id="content_alert">
									<c:import url="/orgadmin/components/account_management/manage_content_alerts.jsp" />	
								</f:subview>
		    				</c:when> 
		       				<c:when test="${'reg_success' eq param.page}">
								<f:subview id="reg_success">
									<c:import url="/orgadmin/components/account_management/register_success.jsp" />	
								</f:subview>
		    				</c:when>
		    				<c:when test="${'up_success' eq param.page}">
								<f:subview id="reg_success">
									<c:import url="/orgadmin/components/account_management/update_register_success.jsp" />	
								</f:subview>
		    				</c:when>
							<c:when test="${'register' eq param.page}">
								<f:subview id="register">
									<c:import url="/orgadmin/components/account_management/register.jsp" />	
								</f:subview>
		    				</c:when> 
		    				<c:otherwise>
		    					<f:subview id="register">
									<c:import url="/orgadmin/components/account_management/register.jsp" />	
								</f:subview>
		    				</c:otherwise>
						</c:choose>  
		       		
		       		</div>
		       		
			   	 	<div class="clear"></div>
	    		
	    			<f:subview id="footer_menuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>	        
				</div>
				<!-- page-wrapper end -->
	    	</div>
	    	
	    	<f:subview id="footer_container">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>
		</div>
		<f:subview id="common_js">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		<f:subview id="account_management_js">
			<c:import url="/orgadmin/components/account_management/account_management_js.jsp" />
		</f:subview>
		<f:subview id="google_analytics">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
	</body>
</f:view>
</html>