<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<c:set var="httpHost" value="http://${header.host}" />

<link rel="stylesheet" type="text/css" media="screen, print" href="${httpHost}css/popup_styles.css" />
<link rel="stylesheet" type="text/css" media="screen" href="${httpHost}css/styles.css" />
<link rel="stylesheet" href="${httpHost}css/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="print" href="${httpHost}css/print.css" />
