<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>

 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambridge Books Online - Cambridge University Press</title>
<f:subview id="commonCssSubview">
	<c:import url="common_css.jsp" />
</f:subview>
</head>
<body>

<f:subview id="headerSubview">
	<c:import url="../components/header.jsp" />
</f:subview>

<div id="crumbtrail">
  <ul>
    <li class="last">Terms of Use</li>
  </ul>
</div><!-- end crumbtrail -->

<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="../components/ip_logo.jsp" />
	</f:subview>

	<f:subview id="subviewLoginContainer">
		<c:import url="../components/login_container.jsp" />	
	</f:subview>

	<f:subview id="recentlyUpdatedSeriesSubview">
		<c:import url="../components/recently_updated_series.jsp" />
	</f:subview>

</div><!-- end right nav -->



<div id="content">

  <h1>Terms of Use</h1>
	
    <p>This legal notice is specific to Cambridge Books Online and overrides any other legal notice appearing elsewhere on Cambridge University Press websites.</p>
    
    <p>By requesting a trial or registering to access Cambridge Books Online (hereinafter CBO) you are indicating that you accept the terms and conditions set out below.</p>
    
    <p>Tables of Content and extracts of chapters may be accessed free of charge by all users of CBO.</p>
    
    <p>The full texts of chapters (referred to hereinafter as 'Content') may be accessed only by Authorised Users. 'Authorised User' is defined as:</p>
    
<ul class="terms">
    	<li>an individual who is authorised to access CBO Content through the secure network at a purchasing institution, via his/her affiliation with a purchasing institution as a current student, faculty member, library patron, or employee</li>
        <li>an individual who is authorised to access CBO Content through a valid payment</li>
        <li>an individual who is a member of a society that has arranged for access to CBO for its current members</li>
    </ul>
    
  	<h5>Fees and Payment</h5>
	
    <p>Your annual fee as set out by Cambridge University Press must be fully paid before you will be provided with access to CBO.</p>
    
    <p>You agree to pay all fees and charges incurred in connection with your access, including but not limited to applicable taxes and communications or access charges, at the rates in effect when the charges were incurred.</p>
    
    <p>Prior to expiration of your existing access or upon your annual maintenance fee becoming due, Cambridge University Press will notify you of the rates for the forthcoming year, which may differ from the previous year. Payment of the new rates will be taken as an indication of your acceptance of them and your wish to continue to access CBO. If no payment is received from you by the required date your access will expire at the end of your agreed access period.</p>
    
    <h5>Institutional Access</h5>
    
    <p>An appropriate IP address range(s) to that institution's site must be registered to activate each electronic purchase. Responsibility for the allocation of the IP addresses to the institutional access lies with the registrant (not the publisher or service provider).</p><br />

	<h5>Consortia and Custom Licences</h5>
    
    <p>Site licence fees are charged on a sliding scale depending on the size of the institution, such institutions to include small public libraries, colleges of further education, high sCBOols, multi-campus institutions, large public libraries and consortia.</p>
    
    <p>Please contact your local representative for more information:</p>
    
    <h6>Outside North America:</h6>
    
    <p>Please email: <a href="mailto:hperrett@cambridge.org">Hannah Perrett</a></p>
    
    <h6>For US, Canada & Mexico:</h6>
    
    <p>Please email: <a href="mailto:aroseman@cambridge.org">Alan Roseman</a></p>
    
    <h5>Obligations of the Institutions/Consortia</h5>
    
    <p>The institutions/consortia shall use best endeavours to monitor compliance and immediately upon becoming aware of any unauthorised use or other breach by one of its users, inform Cambridge University Press and take all reasonable and appropriate steps, including disciplinary action, both to ensure that such activity ceases and to prevent any recurrence.</p>
    
    <p>The institutions/consortia shall use best endeavours to ensure that all of its users are appropriately notified of the importance of respecting the intellectual property rights in the Content.</p>
    
    <p>The institutions/consortia shall be responsible for ensuring that adequate security is in place in order that only such users can gain access to CBO.</p>
    
    <h5>Individual Access</h5>
    
    <p>Individuals with valid access to CBO qualify as Authorised Users following registration and specification of username and password.</p>
    
    <p>Members of selected societies may obtain individual access via their society membership. Access to full CBO Content for society members is controlled by a referrer URL, whereby members authenticate on the society's web site and are given access via a link on that site to CBO. For questions about this type of authentication, please contact <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a> (North America & Mexico) or onlinepublications@cambridge.org (Outside North America).</p>
    
    <h5>Terms and Conditions of Use</h5>
    
    <p>Users acknowledge that all rights relating to CBO are the sole and exclusive property of Cambridge University Press and that this Agreement does not convey any right, title or interest therein except the right to use CBO in accordance with the terms and conditions of this Agreement.</p>
    
    <p>Users undertake to ensure that the intellectual property rights of the copyright holder and the software owners and the moral rights of the authors of the Content are not infringed.</p>
    
    <p>Users may access, search and view individual chapters for personal use only. Print, copy and download permissions may vary from collection to collection depending on the sensitivity and rights available for the Content. Unless otherwise stated below, users may make copies, printed or otherwise, of one chapter or up to 5% of the pages from each CBO title, whichever is the greater.</p>
    
    <p>Hypertext links to other Web locations are for the convenience of users and do not constitute any endorsement or authorisation by Cambridge University Press.</p>
    
    <p>Authorised Users are not permitted to:</p>
    
    <ul class="terms">
    	<li>Engage in systematic copying or downloading of the Content or transmit any part of the Content by any means to any unauthorised user</li>
    	<li>Allow copies to be stored or accessed by an unauthorised user</li>
    	<li>Mount or distribute any part of the Content on any electronic network, including without limitation the Internet and the World Wide Web, other than as specified in these terms and conditions</li>
    	<li>Make the Content available in any other form or medium or create derivative works without the written permission of Cambridge University Press. For permissions information, please follow the 'Rights and permissions' quick-link on the Cambridge website for your region, via http://www.cambridge.org</li>
    	<li>Alter, amend, modify or change the Content</li>
    	<li>Reverse engineer, decompile, disassemble or otherwise alter software</li>
    	<li>Maintain downloaded Content after expiration of valid access</li>
	</ul>
    
	<p>Cambridge University Press's explicit written permission must be obtained in order to:</p>
	
    <ul class="terms">
    	<li>Use all or any part of the Content for any Commercial Use</li>
    	<li>Distribute the whole or any part of the Content to anyone other than Authorised Users</li>
    	<li>Publish, distribute or make available the Content, works based on the Content or works which combine the Content with any other Content, other than as permitted in these terms and conditions;</li>
    </ul>
    
    <p>Cambridge University Press reserves the right to withdraw access to the Content in the event of deliberate and/or systematic breach of these terms and conditions by any Authorised User.</p>
    
    <h5>Intellectual Property Rights</h5>
    
    <p>The Content in this form is copyright &copy; Cambridge University Press or published under exclusive license from the copyright holder by Cambridge University Press. </p>
    
    <h5>Disclaimers Regarding Services and Materials</h5>
    
    <p>Cambridge University Press does not warrant that CBO will be usefully accessible in every hardware/software environment. Cambridge University Press does not warrant the accuracy or completeness of any information contained in CBO, or its merchantability or fitness for a particular purpose.</p>
    
    <p>Cambridge University Press will have no liability to any person for any loss or damage arising out of use of, or inability to use, CBO. The CBO service is supplied on an 'as is' and 'as available' basis. We exclude all liability whatever, to the fullest extent permitted by law, in respect of any loss or damage resulting or arising from any non-availability or use of this website or of any other website linked to it, or from reliance on the Contents of this website or any material or Content accessed through it.</p>
    
    <h5>Fair Dealing</h5>
    
    <p>Nothing in this Agreement shall limit your rights to make 'fair dealing' of CBO Content as that term is defined under the Copyright, Design and Patent Act 1988.</p>
    
    <h5>Renewals and Termination</h5>
    
    <p>This Agreement shall commence on your acceptance of these terms of use, such acceptance being indicated by checking the box at the bottom of the registration/trial form. The Agreement shall remain in full force and effect for the duration of the access period or until a) you notify Cambridge University Press of your decision to terminate your access prior to the time of renewal, or b) Cambridge University Press terminates your access in accordance with this Agreement.</p>
    
    <p>In the event that you commit a material breach of this Agreement Cambridge University Press may, at its discretion, terminate this Agreement, and/or exercise all rights and remedies which may be available to it in law or equity.</p>
    
    <p>Cambridge University Press may terminate this Agreement at any time. In the event that Cambridge University Press terminates this Agreement for reasons other than your breach of this Agreement, no refunds will be applicable without any prior agreement with Cambridge University Press.</p>
    
    <p>Upon termination of this Agreement, you agree to continue to adhere to the provisions of this Agreement relating to any Cambridge University Press Intellectual Property.</p>

	<h5>Changes to Agreement</h5>
    
    <p>Cambridge University Press may change, add or remove portions of this Agreement, at any time. Such changes shall (i) be highlighted on the CBO home page with a link to the new terms of use; and (ii) sent via email or postal mail to your preferred address. Your continued use of CBO shall be deemed to constitute your consent to such changed terms.</p>
    
    <h5>Privacy Policy</h5>
    
    <p>If you supply personal details to Cambridge University Press through this website then you consent to our maintaining, recording, holding and using such personal data in accordance with our <a href="privacy_policy.html">Privacy Policy</a>.</p>
    
    <h5>No Waiver</h5>
    
    <p>Either party's waiver, or failure to require performance by the other, of any provisions of this Agreement will not affect its full right to require such performance at any subsequent time, or be construed to be a waiver of the provision itself.</p>

</div><!-- end content -->

<div class="clear">&nbsp;</div>

<f:subview id="footerSubview">
	<c:import url="../components/footer.jsp" />
</f:subview>
<f:subview id="commonJsSubview">
	<c:import url="../components/common_js.jsp" />
</f:subview>

</body>
</f:view>
</html>