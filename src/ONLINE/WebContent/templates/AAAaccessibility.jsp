<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>

 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambridge Books Online - Cambridge University Press</title>
<f:subview id="commonCssSubview">
	<c:import url="common_css.jsp" />
</f:subview>
</head>
<body>

<f:subview id="headerSubview">
	<c:import url="../components/header.jsp" />
</f:subview>


<div id="crumbtrail">
  <ul>
    <li class="last">Accessibility</li>
  </ul>
</div><!-- end crumbtrail -->



<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="../components/ip_logo.jsp" />
	</f:subview>

	<f:subview id="subviewLoginContainer">
		<c:import url="../components/login_container.jsp" />	
	</f:subview>

	<f:subview id="recentlyUpdatedSeriesSubview">
		<c:import url="../components/recently_updated_series.jsp" />
	</f:subview>

</div><!-- end right nav -->



<div id="content">

  <h1>Accessibility</h1>
	
    <p>Cambridge Books Online recognizes the importance of making its web services available to the largest possible audience and has attempted to design and develop this website to be accessible by all users. Where possible this web site has been coded to comply with the World Wide Web Consortium (W3C) Web Accessibility Guidelines Priority Levels 1, 2 and 3 (Conformance Level "AAA").</p>
    
    <p>Cambridge Books Online will continue to test future releases of this site and remain committed to maintaining its compliance with appropriate accessibility guidelines and serving the widest possible audience for our services.</p>
    
    <p>For questions about our continuing efforts to make web-based information accessible to all users, or to report an accessibility problem on any of our pages, <a href="contact.jsf">contact us</a>.</p>
    
  <h5>Browser / Platform Support</h5>
	
    <p>The following is a list of browsers / platforms that are supported on the Cambridge Books Online website: </p>	
	
	<ul>
		<li class="title">Windows (XP, Vista):</li>
		<li>Internet Explorer 6.0</li>
		<li>Internet Explorer 7.0</li>
		<li>Internet Explorer 8.0</li>
		<li>Mozilla Firefox 2.0</li>
		<li>Mozilla Firefox 3.0.x</li>
		<li>Mozilla Firefox 3.5.x</li>
		<li>Safari 4.28.17</li>
		<li>Opera 9.63</li>
		<li>Chrome 2.0</li>
    </ul>

    <ul>
    	<li class="title">Mac (OS X):</li>
    	<li>Safari 2.0.4</li>
		<li>Safari 3.1.2</li>
		<li>Mozilla Firefox 3.5.x</li>
		<li>Opera 9.64</li>
    </ul>


</div><!-- end content -->

<div class="clear">&nbsp;</div>

<f:subview id="footerSubview">
	<c:import url="../components/footer.jsp" />
</f:subview>
<f:subview id="commonJsSubview">
	<c:import url="../components/common_js.jsp" />
</f:subview>

</body>
</f:view>
</html>