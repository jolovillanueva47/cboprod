<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>

 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambridge Books Online - Cambridge University Press</title>
<f:subview id="commonCssSubview">
	<c:import url="common_css.jsp" />
</f:subview>
</head>
<body>

<f:subview id="headerSubview">
	<c:import url="../components/header.jsp" />
</f:subview>


<div id="crumbtrail">
  <ul>
    <li class="last">Contact us</li>
  </ul>
</div><!-- end crumbtrail -->



<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="../components/ip_logo.jsp" />
	</f:subview>

	<f:subview id="subviewLoginContainer">
		<c:import url="../components/login_container.jsp" />	
	</f:subview>

	<f:subview id="recentlyUpdatedSeriesSubview">
		<c:import url="../components/recently_updated_series.jsp" />
	</f:subview>

</div><!-- end right nav -->



<div id="content">

  <h1>Contact us</h1>
	
    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
    
    <h5>UK and the Rest of the World</h5>
	
    <p>For Customer Services and other queries:</p>
    
	<ul class="contacts">
        <li>Cambridge University Press</li>
        <li>The Edinburgh Building</li>
        <li>Shaftesbury Road</li>
        <li>Cambridge</li>
        <li>CB2 8RU</li>
    </ul>
    
    <ul class="contacts">
    	<li>By phone: +44 (0)1223 326070</li>
        <li>By fax: +44 (0)1223 325150</li>
        <li>Email: <a href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a></li>
    </ul>

</div><!-- end content -->

<div class="clear">&nbsp;</div>

<f:subview id="footerSubview">
	<c:import url="../components/footer.jsp" />
</f:subview>
<f:subview id="commonJsSubview">
	<c:import url="../components/common_js.jsp" />
</f:subview>

</body>
</f:view>
</html>