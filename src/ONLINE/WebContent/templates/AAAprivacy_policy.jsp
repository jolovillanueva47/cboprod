<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>

 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambridge Books Online - Cambridge University Press</title>
<f:subview id="commonCssSubview">
	<c:import url="common_css.jsp" />
</f:subview>
</head>
<body>

<f:subview id="headerSubview">
	<c:import url="../components/header.jsp" />
</f:subview>


<div id="crumbtrail">
  <ul>
    <li class="last">Privacy Policy</li>
  </ul>
</div><!-- end crumbtrail -->



<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="../components/ip_logo.jsp" />
	</f:subview>

	<f:subview id="subviewLoginContainer">
		<c:import url="../components/login_container.jsp" />	
	</f:subview>

	<f:subview id="recentlyUpdatedSeriesSubview">
		<c:import url="../components/recently_updated_series.jsp" />
	</f:subview>

</div><!-- end right nav -->



<div id="content">

  <h1>Privacy Policy</h1>
	
    <p>This privacy policy is specific to Cambridge Histories Online (hereafter CHO) and overrides any other privacy policy or legal notice appearing elsewhere on Cambridge University Press websites.</p>
    
    <p>Cambridge University Press (hereafter Cambridge) is committed to protecting your privacy online. If you have any questions about our privacy policy, please <a href="contact.html">contact us</a>.</p>
    
    <p>By using the CHO website, you are accepting the practices described in this statement.</p>
    
    <h5>Collection and Use of Information</h5>
    
    <p>When you register for a free trial, ask for further information about CHO or purchase a subscription to a product within CHO, we ask for your name, e-mail address, postal address, and other relevant personal information. Personal information submitted in this way is added to our access control database, hosted by a third party hosting company, and to customer databases at Cambridge. This information will only be updated if you inform us of any changes to your personal details. </p>
    
    <p>When registering for a free trial or purchasing a subscription to a product within CHO you are also able to opt-in to any or all of the following opportunities:</p>
       
	<ul class="terms">
    	<li>to receive regular notifications on Cambridge Histories Online via email</li>
        <li>to receive regular notifications on new titles from Cambridge University Press via email</li>
        <li>to receive regular notifications on new titles from Cambridge University Press via post</li>
    </ul>
    
    <p>Cambridge also requests your consent to be able to:</p>
    
    <ul class="terms">
    	<li>pass your details to one of our overseas branches; or</li>
        <li>pass your details to a third party</li>
    </ul>
	
    <p>Where you have explicitly consented by way of "opt-in" to any or all of the above we may send you information about us and products of ours that we think may be of interest to you. Where you have consented to your information being passed to our overseas branches and/or third parties, you may receive information about them and their products that they think may be of interest to you.</p>
    
    <p>You may notify us at any time either in writing or by telephone, fax or e-mail that you object to being contacted in a particular stated way and we will amend our records accordingly.</p>
    
    <p>When you contact us with order queries, technical problems, feedback on CHO, etc, we will also request personal information (such as your name, e-mail address and other contact details) from you so that we can respond to your query appropriately. Statistics regarding the types of queries submitted to us may be collated by us in aggregate form so that we can effectively monitor the site and improve levels of service. No personal information will be released as part of these statistics without your prior permission.</p>
    
    <h5>Sharing information</h5>
    
    <p>Cambridge will not sell your personal information to others or release it to others without your prior consent.</p>
    
    <p>The CHO access control database is hosted by third party hosting companies who undertake to keep your information secure and not use it for any other purpose.</p>
    
    <p>If we believe that your use of the site is unlawful or damaging to others, we reserve the right to disclose the information we have obtained through the site about you to the extent that it is reasonably necessary in our opinion to prevent, remedy or take action in relation to such conduct.</p>

	<h5>Cookies</h5>
    
    <p>We use cookies on this web site. Cookies are small amounts of information that we transfer to your computer's hard drive through your web browser. They tell us when you have visited our site and where you have been. They do not identify you personally, just the presence of your browser. Cookies make it easier for you to log on and use the site during future visits. They also enable us to provide you with a more personalised service. Should you wish to do so, your browser's help section should be able to warn you before accepting cookies and how to filter, delete or disable them, although in the latter case, you may not be able to use certain features on our site as a result. </p>
    
    <p>Subscribers may sign in with or without using cookies.</p>
    
    <p>Please Note: Institutions with referring URL access to CHO need to sign in with cookies.</p>
    
    <h5>Security</h5>
    
    <p>Your personal information is stored at a data centre controlled by our third party hosting company and on the customer databases at Cambridge. The data centre controlled by our third party hosting company is specifically designed to be physically secure and to admit authorised personnel only, who are contractually bound to keep all of our data confidential.</p>
    
    <p>Access to our databases at Cambridge is only given to those of our employees who need such access in order to carry out necessary processing of your account.</p>
    
    <p>All personal information submitted by you and entered by Cambridge into the CHO access control system is encoded using 128-bit SSL encryption before being sent over the Internet.</p>
    
    <h5>Changes to our privacy policy</h5>
    
    <p>Any changes to our privacy policy in the future will be posted to this site, and where appropriate, sent via e-mail notification to your email address.</p>
    
    
</div><!-- end content -->

<div class="clear">&nbsp;</div>

<f:subview id="footerSubview">
	<c:import url="../components/footer.jsp" />
</f:subview>
<f:subview id="commonJsSubview">
	<c:import url="../components/common_js.jsp" />
</f:subview>

</body>
</f:view>
</html>