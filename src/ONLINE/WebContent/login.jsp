<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />
<o:url var="contextRootSsl" option="current_dns_to_https" />
 
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />

	<title>Cambridge Books Online - Cambridge University Press</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/popup.css" />
	
	<!--[if IE 8]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 6]>
        <link href="css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]--> 
    
</head>

<% session.setAttribute("termsDate", "2011-03-21"); %>

<body >

<form id="loginForm" method="post">

	<div class="content">

		<c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
	
		<ul class="logIn">
			<li>
				<span style="font-weight: bold;">Welcome</span>
				
				<span style="position:absolute; right: 20px; top: 21px;" >
					<span id="loginLoadingId" style="display: none;">Logging in. Please wait...
						<img src="${pageContext.request.contextPath}/images/circle_loading.gif"></img>
					</span>
				</span>
			</li>

			<li>${pageScope.isLoggedIn ? userInfo.username : 'Guest'}</li>
	
			<li>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr style="${pageScope.isLoggedIn ? 'font-weight: bold; color: gray;' : ''}">
						<td width="40%"><label for="userName">Username</label></td>
						<td><label for="passWord">Password</label></td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${not pageScope.isLoggedIn}">
								<td><input name="username" class="login_field" id="username" size="24" maxlength="24" type="text" autocomplete="off"/></td>
								<td><input name="password" class="login_field" id="password" size="24" maxlength="24" type="password" /></td>
							</c:when>
							<c:otherwise>
								<td><input class="login_field" disabled="disabled" /></td>
								<td><input class="login_field" disabled="disabled" /></td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${not pageScope.isLoggedIn}">
								<td colspan="2"><div class="form_button"><span><a href="javascript:void(0);" title="Login" id="submitUsrnamePassword">Login</a></span></div></td>
							</c:when>
							<c:otherwise>
								<td colspan="2"><div class="form_button"><span><a class="bot_login_disabled">Logout</a></span></div></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</table>
			</li>

			<li>
				<c:choose>
					<c:when test="${not pageScope.isLoggedIn}"><a href="javascript:toPage('forgottenPassword');" >Forgotten your password?</a></c:when>
					<c:otherwise><span style="font-weight: bold; color: gray;">Forgot your password?</span></c:otherwise>
				</c:choose>
			</li>
	
			<li style="${pageScope.isLoggedIn ? 'font-weight: bold; color: gray;' : ''}">
				Note: <br />* Usernames and passwords are case-sensitive. <br />* If you already have a username for Cambridge Journals Online, you can log in to Cambridge Books Online using your existing CJO username.
			</li>
			
			<li class="error"><div id="result" /></li>
	        
			<%--li>
				<c:choose>
					<c:when test="${empty athensId}"><a id="athensLoginLink" href="javascript:void(0);">Athens Login</a> |</c:when>
					<c:otherwise><span style="font-weight: bold; color: gray;">Athens Login</span> |</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${empty shibbId}"><a id="shibbolethLoginLink" href="javascript:void(0);">Shibboleth Login</a> |</c:when>
					<c:otherwise><span style="font-weight: bold; color: gray;">Shibboleth Login</span> |</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not pageScope.isLoggedIn}"><a id="registrationLink" href="javascript:void(0);">Register</a></c:when>
					<c:otherwise><span style="font-weight: bold; color: gray;">Register</span></c:otherwise>
				</c:choose>
			</li--%>
	    </ul>
	    <!-- 
    	<img style="display:none" id="cjoLogin" alt="" src="" />
    	 -->
	</div><!-- end content -->
</form>

<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/common.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function(){
		setTimeout("$(\"#username\").focus();", 100);
		
		$("#athensLoginLink").click(function(){
			self.parent.tb_remove();
			window.parent.location = '<%= System.getProperty("openathensLink") %>';
		});

		$("#shibbolethLoginLink").click(function(){
			self.parent.tb_remove();
			window.parent.location = '<%= System.getProperty("shibbolethLink") %>';
		});

		$("#registrationLink").click(function(){
			self.parent.tb_remove();
			window.parent.location = '${contextRootCjo}registration?displayname=${referalName}';
		});

		$("#submitUsrnamePassword").click(function(){
			$("#loginLoadingId").show();
			$username = document.getElementById("username").value;
			$password = document.getElementById("password").value;
			var randVal = Math.random();
			
     		$.post("${contextRootSsl}login", {username:$username, password:$password, rand: randVal}, function(xml) {         		
     		//$.post("http://localhost:8080/login", {username:$username, password:$password, rand: randVal}, function(xml) {
     		//$.post("login", {username:$username, password:$password, rand: randVal}, function(xml) {
				//var resText = $("response", xml).text();         		
				var resText = xml;

           		if(resText.indexOf("success") > -1) {
               		/**    
           			$("#loginLoadingId").hide();
           			self.parent.tb_remove();		
           			**/

           			
           			//var locationUrl = window.parent.location.href; 
           			var locationUrl = getParentLocation(window.location);
            		locationUrl = locationUrl.replace("?lo=y&", "?");
            		locationUrl = locationUrl.replace("?lo=y", "");
					locationUrl = locationUrl.replace("&lo=y", "");				

					if(/#$/.test(locationUrl)) {
						locationUrl = locationUrl.replace("#", "");
					}
					
					window.parent.location=locationUrl;
					
					//window.parent.location="http://localhost:8080/home.jsf";
					
					/*
					$("#cjoLogin").load(function(){
						//on load of image							
						window.parent.location=locationUrl;
					});
					
					$("#cjoLogin").attr("src",getCjoLogin(resText) + "&RETURN_IMAGE=Y");					
					*/
					
            		
            	} else if ($("response", xml).text()=='redirect') {
            		toPage('forgottenPassword');
            	} else {
            		$("#loginLoadingId").hide();
   					$("#result").html(resText);
   					$("#username").focus();
   				}         
     		});
     	});

		$("#loginForm").bind("keypress", function(e) {
	        if(e.keyCode==13){
	        	$("#submitUsrnamePassword").click();
	        	return false;
	        }
		});
   	});

	function toPage(pageName){
		window.parent.location='${contextRootCjo}' + pageName + '?displayname=${referalName}';
		self.parent.tb_remove();
	}

	function getParentLocation(url){
		var _url = new String(url);
		var newUrl = _url.split("LOCATION10556=")[1];

		if (newUrl.indexOf("%3F") > -1){
			newUrl = newUrl.split("&")[0] + "&fromLogin=Y";
		}
		else {
			newUrl = newUrl.split("&")[0] + "?1fromLogin=Y";
		}
		newUrl = window.decodeURIComponent(newUrl);
		
		return newUrl;                                 		
	}

	function getCjoLogin(text){
		var id = text.split("--->")[1];		
		return "${contextRootCjo}accmanagement?id=" + id + "&topage=stream";
	}
	
	
	
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>

</html>