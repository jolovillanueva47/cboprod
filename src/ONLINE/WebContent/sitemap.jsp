<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cambridge Books Online - Cambridge University Press</title>
	
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>
</head>

<body>
	
    <c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">

    <!-- bg Topline -->       
	<div id="top_container">

		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="0000" scope="session" />
		<c:set var="pageName" value="Site Map" scope="session" />

		<div id="page-wrapper">					
				
			<f:subview id="subviewIPLogo">
				<c:import url="/components/ip_logo.jsp" />	
			</f:subview>
	
			<!-- Header Start -->
			<div id="header_container">	        	
				<f:subview id="subviewTopMenu">
					<c:import url="/components/top_menu.jsp" />	
				</f:subview>	
				
				<f:subview id="subviewSearchContainer">
					<c:import url="/components/search_container.jsp" />	
				</f:subview>
		    </div>
		    <!-- Header End -->
			
	        <!-- Main Body Start --> 
	        <div id="main_container">
		    	        
	        	<!-- Titlepage Start -->
	            <div class="titlepage_heading"><h1>Site Map</h1></div>
	            <!-- Titlepage End -->
	            
				<!-- Breadcrumbs Start -->
				<f:subview id="crumbtrailContainer">
					<c:import url="/components/crumbtrail.jsp" />	
				</f:subview>
				<!-- Breadcrumbs End -->
	        
				<div class="clear"></div>
				
				<!-- Sitemap Start --> 
                <div class="sitemap">
                
                	<!-- variable start -->
		            <c:set var="lib_page_id" value='<%= System.getProperty("librarian.pageId") %>' /> 
		            <c:set var="popup_online_product_url" value="MM_openWindow('http://www.cambridge.org/online/','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" />
		            <c:set var="popup_academic_and_professional_url" value="MM_openWindow('http://cambridge.org/gb/knowledge/home/item2273191/','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" />
		            <c:set var="popup_diagnostics_url" value="MM_openWindow('${pageContext.request.contextPath}/popups/view_access.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" />
		            <c:set var="popup_faq_url" value="MM_openWindow('${pageContext.request.contextPath}/popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" />
		            <c:set var="popup_help_url" value="MM_openWindow('${pageContext.request.contextPath}/popups/help_index.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" />
		            <c:set var="popup_usage_stat_url" value="usageStatWindow('${contextRootCjo}','${contextRootCjo}counter','adminLink');return false;" />
		            <!-- variable end -->
                
                	<!-- url start -->                    	
                	<c:url value="${pageContext.request.contextPath}/home.jsf" var="home_url" />
                	<c:url value="${pageContext.request.contextPath}/aaa/home.jsf" var="accessible_version_url" />
                	<c:url value="${pageContext.request.contextPath}/subject_tree.jsf" var="subject_tree_url" />
                	<c:url value="${pageContext.request.contextPath}/advance_search.jsf" var="advance_search_url" />
                	<c:url value="${pageContext.request.contextPath}/about.jsf" var="about_url" />     
                	<c:url value="${pageContext.request.contextPath}/contact.jsf" var="contact_url" />       
                	<c:url value="${pageContext.request.contextPath}/accessibility.jsf" var="accessibility_url" />
                	<c:url value="${pageContext.request.contextPath}/terms_of_use.jsf" var="terms_url" />   
                	<c:url value="${pageContext.request.contextPath}/privacy_policy.jsf" var="privacy_url" />
                	<c:url value="${pageContext.request.contextPath}/rights_and_permissions.jsf" var="rights_and_permission_url" />
                	<c:url value="${pageContext.request.contextPath}/ebooks/subscriptionServlet" var="access_to_url" >
                		<c:param name="ACTION" value="ALL" />
                		<c:param name="sortBy" value="1" />
                	</c:url>                         	
		            <c:url value="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf" var="librarians_url" />
		            <c:url value="${contextRootCjo}registration" var="registration_url">
		            	<c:param name="displayname" value="${referalName}" />       		
		            </c:url>	
		            <c:url value="accmanagement" var="account_management_url">
		            	<c:param name="topage" value="updateRegistration" />       		
		            </c:url>	
		            <c:url value="configureIPDomain" var="configure_ip_domain_url" />
		            <c:url value="subscriptionDetails" var="access_to_admin_url" />
		            <c:url value="remoteUserAccess" var="remote_user_url" />
		            <c:url value="openURLResolver" var="resolver_url" />
		            <c:url value="updateOrganisationDetails" var="org_details_url" />
		            <c:url value="organisationExpiryAlertsDetail" var="hosting_fee_url" />
		            <c:url value="changeAdministrator" var="change_admin_url" />
		            <c:url value="updateLogo" var="update_org_url" />
		            <c:url value="manageContentAlerts" var="content_alerts_url" />
		            <c:url value="switchAccounts" var="multiple_accounts_url" />
		            
		            <c:url value="${contextRootCjo}forgottenPassword" var="forgotten_password_url">
		            	<c:param name="displayname" value="${referalName}" />          		
		            </c:url>            
		            <!-- url end -->
                	
                	<!-- Start Sitemap Left Column -->
                    <div class="sitemap_leftcolumn">
                    	<ul>
                            <li><strong>Accessing Content</strong>
                                <ul><!-- Start Level 1 -->
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${home_url}">Featured Titles</a></li>      
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${home_url}">Featured Collections</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${subject_tree_url}">Browse by Subject</a></li>                                    
                                    <li><a href="#" class="icons_img bullet02"></a>Book Landing Page
                                        <ul><!-- Start Level 2 -->
                                            <li><span class="icons_img bullet02">&nbsp;</span>Book Description</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>Table of Contents</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>References</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>Full-text PDF</li>
                                        </ul>
                                    </li> 
                                    <li><a href="#" class="icons_img bullet02"></a>Chapter Landing Page
                                        <ul><!-- Start Level 2 -->
                                            <li><span class="icons_img bullet02">&nbsp;</span>Chapter Extract</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>Table of Contents</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>References</li>
                                            <li><span class="icons_img bullet02">&nbsp;</span>Full-text PDF</li>
                                        </ul>
                                    </li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Email Link to This Book</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>How to Cite</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Export Citation</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Alert Me When This Book is Cited</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Cited By</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Link to This Book</li>
                                    <li><span class="icons_img bullet02">&nbsp;</span>Recommend This to a Librarian</li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><strong>Searching Content</strong>
                                <ul><!-- Start Level 1 -->
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${home_url}">Quick Search</a></li>    
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${advance_search_url}">Advanced Search</a></li> 
                                    <li><span class="icons_img bullet02">&nbsp;</span>Search Results</li> 
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><strong>User Registration</strong>
                                <ul><!-- Start Level 1 -->
                                	<c:choose>
										<c:when test="${eBookLogoBean.userLoggedIn}">
											<li><span class="icons_img bullet02">&nbsp;</span>Registration</li>
										</c:when>
										<c:otherwise>
											<li><a href="#" class="icons_img bullet02"></a><a href="${registration_url}">Registration</a></li>
										</c:otherwise>
									</c:choose>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- End Sitemap Left Column -->
                    
                    <!-- Start Sitemap Right Column -->
                    <div class="sitemap_rightcolumn">
                    	<ul>
                            <li><strong>General Information</strong>
                                <ul><!-- Start Level 1 -->
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${home_url}">Home</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${about_url}">About CBO</a></li>    
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${home_url}">News and Events</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${librarians_url}">For Librarians</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="#" onclick="${popup_online_product_url}">Other Online Products from Cambridge</a></li> 
                                    <li><a href="#" class="icons_img bullet02"></a><a href="#" onclick="${popup_academic_and_professional_url}">Academic and Professional Books</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${contact_url}">Contact Us</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${accessibility_url}">Accessibility</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${terms_url}">Terms of Use</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${privacy_url}">Privacy Policy</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${rights_and_permission_url}">Rights and Permissions</a></li> 
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><strong>Account Personalization</strong>
                                <ul><!-- Start Level 1 -->                                
                                	<c:choose>
										<c:when test="${eBookLogoBean.userLoggedIn}">
											<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${account_management_url}', 'librarianLink');">Change Registration Details</a></li>   
											<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${content_alerts_url}', 'librarianLink');">My Content Alerts</a></li> 	
										</c:when>
										<c:otherwise>
											<li><span class="icons_img bullet02">&nbsp;</span>Change Registration Details</li>
											<li><span class="icons_img bullet02">&nbsp;</span>My Content Alerts</li>
										</c:otherwise>
									</c:choose>                                     
                                </ul>
                            </li>
                        </ul> 
                        <ul>
                            <li><strong>Account Administrator</strong>
                            	<c:set var="is_admin" value="false" />
                            	<o:isAdmin>
                            		<c:set var="is_admin" value="true" />
                            	</o:isAdmin>
                            	
                            	<c:set var="is_multiple_admin" value="false" />
                            	<o:isMultipleAdmin>
                            		<c:set var="is_multiple_admin" value="true" />
                            	</o:isMultipleAdmin>
                                <ul><!-- Start Level 1 -->
                              		<c:choose>		        		
							        	<c:when test="${is_admin}" >
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${configure_ip_domain_url}', 'adminLink');">Configure IP Address</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${access_to_admin_url}', 'adminLink');">Access to</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${remote_user_url}', 'adminLink');">Remote User Access</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="${popup_usage_stat_url}">Usage Statistics</a></li> 		
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${resolver_url}', 'adminLink');">Open URL Resolver</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${org_details_url}', 'adminLink');">Update Organisation Details</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${hosting_fee_url}', 'adminLink');">Hosting Fee Reminder</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${change_admin_url}', 'adminLink');">Change Administrator</a></li>
							        		<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${update_org_url}', 'adminLink');">Update Organisation Logo</a></li>							        		
							           	</c:when>
							           	<c:otherwise>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Configure IP Address</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Access to</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Remote User Access</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Usage Statistics</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Open URL Resolver</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Update Organisation Details</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Hosting Fee Reminder</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Change Administrator</li>
							           		<li><span class="icons_img bullet02">&nbsp;</span>Update Organisation Logo</li>
							           	</c:otherwise>
						        	</c:choose>			
						        	<c:choose>                   
	                               		<c:when test="${is_multiple_admin}"> <!-- need to check if 2 or more admin -->
											<li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="CJOLoginLink('${contextRootCjo}','${multiple_accounts_url}', 'adminLink');">Switch Accounts</a></li> 		
										</c:when>
										<c:otherwise>
											<li><span class="icons_img bullet02">&nbsp;</span>Switch Accounts</li>
										</c:otherwise>
									</c:choose>									
                                </ul>
                            </li>
                        </ul> 
                        <ul>
                            <li><strong>Help Features</strong>
                                <ul><!-- Start Level 1 -->
                                    <li><a href="#" class="icons_img bullet02"></a><a href="${accessible_version_url}">Accessible Version</a></li> 
                                    <c:choose>
										<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
											<li><a href="#" class="icons_img bullet02"></a><a href="${access_to_url}">Access To</a></li>
										</c:when>
										<c:otherwise>
											<li><span class="icons_img bullet02">&nbsp;</span>Access To</li>
										</c:otherwise>
									</c:choose>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="${popup_diagnostics_url}">Diagnostics</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="${popup_faq_url}">FAQs</a></li>
                                    <li><a href="#" class="icons_img bullet02"></a><a href="javascript:void(0);" onclick="${popup_help_url}">Help Topic Index</a>
                                        <ul><!-- Start Level 2 -->
                                            <li><span class="icons_img bullet02">&nbsp;</span>Content-sensitive Help Topic</li>     
                                        </ul>
                                    </li>
                                    <c:choose>
	                                    <c:when test="${eBookLogoBean.userLoggedIn}">
											<li><span class="icons_img bullet02">&nbsp;</span>Forgotten Password</li> 	
										</c:when>
										<c:otherwise>                                    
	                                    	<li><a href="#" class="icons_img bullet02"></a><a href="${forgotten_password_url}">Forgotten Password</a></li>
										</c:otherwise>
									</c:choose>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- End Sitemap Right Column -->    
                
                </div>
                <!-- Sitemap End -->
				
	        </div>  
	        <!-- Main Body End --> 
	
			<div class="clear"></div>
			
			<!-- Footermenu Start -->
			<f:subview id="subviewFooterMenuwrapper">
				<c:import url="/components/footer_menuwrapper.jsp" />
			</f:subview>
	        <!-- Footermenu End -->
	        
		</div>
	
	</div>
	
    <!-- Footer Start -->
	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	<!-- Footer End -->
	
    </div>
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/sitemap.js"></script>
	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>

</f:view>

</html>