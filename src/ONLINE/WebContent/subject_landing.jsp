<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>	
	${subjectBean.initAlphaMap}
	${subjectBean.initPage}
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>	
	    
	</head>

	<body>
		
		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="1540" scope="session" />
		<c:set var="pageName" value="Subject Landing" scope="session" />
		
		<c:import url="/components/loader.jsp" />	
		<div id="main" style="display: none;"> 
			<div id="top_container">
				<div id="page-wrapper">
		
			    	<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>
		
					<!-- Header Start -->
					<div id="header_container">		
			        	
						<f:subview id="subviewTopMenu">
							<c:import url="/components/top_menu.jsp" />	
						</f:subview>
				           
						<f:subview id="subviewSearchContainer">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
			
			        </div> 
			        <!-- Header End -->
        			
        			<!-- Main Body Start -->        
					<div id="main_container">        
						<!-- Titlepage Start -->
			            <div class="titlepage_heading"><h1>Subject</h1></div>
			            <!-- Titlepage End -->
            
						<!-- Breadcrumbs Start -->
						<f:subview id="crumbtrailContainer">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>
						<!-- Breadcrumbs End -->  
												
						<!-- Access Information Start -->
						<div class="access_information">
							<strong>Access Information:</strong>
							<ul>
								<li><span class="icons_img purchase"><img src="${pageContext.request.contextPath}/images/icon_purchased.png" alt="Purchased Access" width="19" height="19" /></span>Purchased Access</li>
							</ul>
						</div>
						<!-- Access Information End -->
						
						<div class="clear"></div>

			            <div>
			            	<!-- Pager Menu Start -->
							<form id="browse_sub_subject_form" action="" method="post">
			                	<f:subview id="subviewPagerMenuContainer">
									<c:import url="/components/pager.jsp" />
								</f:subview>
							</form>
							<!-- Pager Menu End -->
			            </div>
			            
			           	<div class="searchtitle_heading">
			           		<h2><c:out value="${subjectBean.subject}" /></h2>
			           	</div>
												
						<div id="content_wrapper01"> 
			                       
							<!-- Border Line01 Start -->
			                <div id="border01_container">  
			                    <!-- Alpha List Start -->
				                <f:subview id="subviewAlphaListContainer">
									<c:import url="/components/subject_alphalist.jsp" />
								</f:subview>
								<!-- Alpha List End -->
			                </div>  
			                <!-- Border Line01 End -->
			                
			                <!-- Letter Heading Start -->
							<div id="content_wrapper09"> 
				           		<h1><c:out value="${subjectBean.firstLetter}" /></h1> 
							</div>
							<!-- Letter Heading End -->
							
							<!-- Search Count Start -->
							<c:choose>
								<c:when test="${subjectBean.resultsFound != 1}">
									<div id="content_wrapper03">Your search returned <b>${subjectBean.resultsFound}</b> results.</div>
								</c:when>
								<c:otherwise>
									<div id="content_wrapper03">Your search returned <b>${subjectBean.resultsFound}</b> result.</div>
								</c:otherwise>
							</c:choose>
							<!-- Search Count End -->
			
							<c:choose>
								<c:when test="${subjectBean.resultsFound > 0}">
									<c:forEach var="book" items="${subjectBean.bookMetaDataList}" varStatus="status">
			
										<c:choose>
											<c:when test="${status.count eq 1}">
												<c:set var="contentWrapperId" value="content_wrapper04" />
											</c:when>
											<c:otherwise>
												<c:set var="contentWrapperId" value="content_wrapper05" />
											</c:otherwise>
										</c:choose>
										
										<div class="${contentWrapperId}">
			
											<!-- Icon Access -->	
											<o:isPurchasedAccess accessType="${book.accessType}">
												<div class="icons_img purchase" title="You have access."><img src="${pageContext.request.contextPath}/images/icon_purchased.png" alt="Purchased Access" width="19" height="19" /></div>
											</o:isPurchasedAccess>
			
											<!-- Sub Books Start -->
											<div class="div_search_info">			
												<span class="search_info_item"><a href="${pageContext.request.contextPath}/ebook.jsf?bid=${book.id}"><c:out value="${book.title}" escapeXml="false" /></a></span><br/>			
												<c:set var="hasComma" value="false"/>
												<c:if test="${not empty book.volumeNumber and '0' ne book.volumeNumber}">
													<b>Volume <c:out value="${book.volumeNumber}" escapeXml="false" /></b>
													<c:set var="hasComma" value="true"/>
												</c:if>
												<c:if test="${not empty book.volumeTitle}">
													<b><c:if test="${hasComma}">, </c:if><c:out value="${book.volumeTitle}" escapeXml="false" /></b>
													<c:set var="hasComma" value="true"/>
							       				</c:if>		
							       				<c:if test="${not empty book.partNumber and '0' ne book.partNumber}">
													<b><c:if test="${hasComma}">, </c:if>Part <c:out value="${book.partNumber}" escapeXml="false" /></b>
													<c:set var="hasComma" value="true"/>
												</c:if>
												<c:if test="${not empty book.partTitle}">
													<b><c:if test="${hasComma}">, </c:if><c:out value="${book.partTitle}" escapeXml="false" /></b> 
													<c:set var="hasComma" value="true"/>
												</c:if>				       			
								       			<c:if test="${not empty book.edition}">
													<b><c:if test="${hasComma}">, </c:if><c:out value="${book.edition}" escapeXml="false" /></b>
													<c:set var="hasComma" value="true"/>
								       			</c:if>
								       			<c:if test="${not empty book.subtitle && 'null' ne book.subtitle}">
										    		<b><c:if test="${hasComma}">, </c:if><c:out value="${book.subtitle}" escapeXml="false" /></b>
										    		<c:set var="hasComma" value="true"/>
										    	</c:if>			
										    	<c:if test="${not empty book.authorSingleline && 'null' ne book.authorSingleline}">
										    		<c:if test="${hasComma}"><br /></c:if>
										    		<c:out value="${book.authorSingleline}" escapeXml="false" />
													<br/>
										    	</c:if>	
							        			
												<c:if test="${not empty book.series && 'null' ne book.series}">
													<c:url var="series_landing_url" value="series_landing.jsf">
														<c:param name="seriesCode" value="${book.seriesCode}" />
														<c:param name="seriesTitle" value="${book.series}" />
														<c:param name="sort" value="series_number" />
													</c:url>
								        			<p>
														<a class="link08" href='<c:out value="${series_landing_url}" />'>
															<c:out value="${book.series}" escapeXml="false" /></a>										
														<c:if test="${not empty book.seriesNumber}">
															<c:out value=" (No. ${book.seriesNumber})" escapeXml="false" />
														</c:if>
													</p>
												</c:if>
												
												<p>
													<c:if test="${not empty book.printDate}">
														<b>Print Publication Year:</b>&nbsp;<c:out value="${book.printDate}" escapeXml="false" /><br/>
													</c:if>
													<c:if test="${not empty book.printIsbn}">
														<b>Print ISBN:</b>&nbsp;<c:out value="${book.printIsbn}" />
													</c:if>
												</p>
			
												<p>
													<c:if test="${not empty book.onlineDate}">
														<b>Online Publication Date:</b>&nbsp;<c:out value="${book.onlineDate}" escapeXml="false" /><br/>
													</c:if>
													<c:if test="${not empty book.onlineIsbn}">
														<b>Online ISBN:</b>&nbsp;<c:out value="${book.onlineIsbn}" />
													</c:if>
													<%--b>Print ISBN:</b> 9780511495410--%>
												</p>
			
												<p>
													<c:if test="${not empty book.doi}">
														<b>Book DOI:</b>&nbsp;<a href="http://dx.doi.org/${book.doi}">http://dx.doi.org/${book.doi}</a>
													</c:if>
												</p>
												
												<p>
													<span class="Z3988" title="${book.coinString}">&nbsp;</span>
												</p>
			
												<p>
													<c:out value="${book.blurb}" escapeXml="false" />
												</p>
											</div>
											<!-- Sub Books End -->
			
					              		</div>
			
									</c:forEach>    	
								</c:when>
								<c:otherwise>
									<div class="content_wrapper04">
										<p><h2>No results found.</h2></p>
									</div>
								</c:otherwise>
							</c:choose>
			                
						</div>   
					</div>  
					<!-- Main Body End -->
        
					<f:subview id="subviewFooterMenuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>        
				</div> 
			</div>
			<f:subview id="subviewFooterContainer">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>	
		</div>
	
		<f:subview id="commonJsSubview">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/subject.js"></script>
		<f:subview id="googleAnalyticsSubview">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
	</body>
</f:view>
</html>