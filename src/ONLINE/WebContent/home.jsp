<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="org.cambridge.ebooks.online.organisation.OrganisationWorker"%>
<%@page import="org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap"%>

<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>
	    <link href="${pageContext.request.contextPath}/css/smoothDivScroll.css" rel="stylesheet" type="text/css" />
	   
		<!-- loadstorm-27593 -->
	</head>

	<body>	
	    <c:import url="/components/loader.jsp" />
	    
		<div id="main" style="display: none;">
	
		    <!-- bg Topline -->       
			<div id="top_container">	
				<c:set var="helpType" value="child" scope="session" />
				<c:set var="pageId" value="1537" scope="session" />
				<c:set var="pageName" value="Home" scope="session" />
				<c:remove var="publisherCode" scope="session"/>
	
				<!-- page-wrapper start -->
				<div id="page-wrapper">					
					
					<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>
		
					<!-- Header -->
					<div id="header_container">	        	
						<f:subview id="top_menu">
							<c:import url="/components/top_menu.jsp" />
							<input type="hidden" value="${faqBean.cookieValue}" name="languageCookie"/>	
						</f:subview>	
						
						<f:subview id="search_container">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
			   	 	</div>	    
		        
		        	<!-- Crumbtrail -->
					<div style="display: none;">
						<f:subview id="crumbtrail">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>
					</div>
		        
		        	<!-- logo -->
		        
		       		<!-- Main Body Start -->
		       		<div id="main_container">
		        
		        		<!-- Title/Description -->
		          		<f:subview id="description">
							<c:import url="/components/home/description.jsp" />	
						</f:subview>
	
						<f:subview id="urgent_alert">
							<c:import url="/components/home/urgent_alert.jsp" />	
						</f:subview>
			            
		        		<!-- Banner Start --> 
		            	<div id="banner_container">
		            	
			                <!-- Title Banner Start -->
			                <div class="titlebanner_homepage">
			                	<ul>
			                    	<li>Featured Titles</li>
			                        <li>News and Events</li>
			                        <li>Browse by Subject</li>
		                   		</ul>
			                </div>
		                	
		                	<!-- 1st Column -->
		                	<div class="banner_wrapper01">
			                	<!-- Featured Titles -->
			                	<f:subview id="featured_titles">
									<c:import url="/components/home/featured_titles.jsp" />	
								</f:subview>
								<br />
								<!-- Featured Collections -->
			                	<f:subview id="featured_collections">
									<c:import url="/components/home/featured_collections.jsp" />	
								</f:subview>
			                </div>
			                
			                <!-- 2nd Column -->
			                <div class="banner_wrapper01">	
			                	<!-- News And Events -->
			                	<f:subview id="news_and_events">
									<c:import url="/components/home/news_and_events.jsp" />	
								</f:subview>
			                	 
			                	<!-- Available Books -->
			                	<f:subview id="available_books">
									<c:import url="/components/home/available_books.jsp" />	
								</f:subview>
			                </div>
			                
			                <!-- 3rd Column -->
			                <div class="banner_wrapper01">
								<!-- Browse by Subject -->
								<f:subview id="browse_by_subject">
									<c:import url="/components/home/browse_by_subject.jsp" />	
								</f:subview>
								
								<!-- Signup for a Trial Button -->
								<f:subview id="signup_for_a_trial">
									<c:import url="/components/home/signup_for_a_trial.jsp" />	
								</f:subview>
			                </div>
		            	</div> 
		            	<!-- Banner End -->
		            	
		            	<div class="icons_img banner_bottom"></div>
	
						<div class="clear"></div>
	
						<!-- Homepage Scroller -->
						<f:subview id="home_scroller">
							<c:import url="/components/home/home_scroller.jsp" />	
						</f:subview>
	            	
		        	</div>  
		        	<!-- Main Body End --> 
		
					<f:subview id="footer_menuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>	        
				</div>
				<!-- page-wrapper end -->
			</div>
	    
			<f:subview id="footer_container">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>
	    </div>
	    
		<f:subview id="common_js">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		
		<script type="text/javascript">
			$(".topmenu_home").attr("class", "topmenu_active rounded");
			$(document).ready(function(){
				$("#closeUrgentAlert").click(function(){
					$("div.urgent_alert").slideToggle();
				});
				
				$.post("downloads/file_size", 
					{filename:"/app/ebooks/content/titlelist_xls.zip"},
					function(data) {
						$("#title_list_xls").html("Microsoft Excel Format (" + data + " MB)");
					}
				);	
				$.post("downloads/file_size", 
					{filename:"/app/ebooks/content/titlelist_csv.zip"},
					function(data) {
						$("#title_list_csv").html("CSV Format - Recommended for Mac users (" + data + " MB)");
					}
				);			
			});
		</script>
		<script src="${pageContext.request.contextPath}/js/jquery/jquery.scroll-min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/jquery/jquery.scroll-controller.js" type="text/javascript"></script>
		<f:subview id="google_analytics">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
	</body>
</f:view>
</html>