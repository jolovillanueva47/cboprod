<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.sql.*"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>	
		<title>Internal Reports Checker</title>
	</head>
	<body> 
		<% 
			String driverName = "oracle.jdbc.driver.OracleDriver";
			String user = "oraebooks";
			String pass = "oraebooks";
			String connUrl = System.getProperty("conn.url");
			
			DateFormat f = new SimpleDateFormat("dd MMM yyyy");			
			out.write("Today:" + f.format(new Date()) + "<br/>");
			
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection(connUrl, user, pass);
			try {
				PreparedStatement pStatement = null;
				ResultSet rs = null;
				
				String sql = "select count(*) as count, event_no from CBO_SESSION_EVNT where event_no in (1,2,3,4) and to_char(notified_time, 'mm/dd/yyyy') = to_char(sysdate, 'mm/dd/yyyy') group by event_no";
				pStatement = conn.prepareStatement(sql);	
				
				StringBuilder existingEventsBuilder = new StringBuilder();
				StringBuilder errorEventsBuilder = new StringBuilder();
				rs =  pStatement.executeQuery();
				while (rs != null && rs.next()){					
					int eventNo = rs.getInt("event_no");			
					int count = rs.getInt("count");
					if(count < 1) {
						errorEventsBuilder.append(eventNo).append(" ");
					}
					existingEventsBuilder.append(eventNo).append(" ");
					switch (eventNo) {
						case 1: {
							out.write("event 1:View Home page ---- count:" + rs.getInt("count") + "<br/>");	
							break;
						}
						case 2: {
							out.write("event 2:View Book Landing ---- count:" + rs.getInt("count") + "<br/>");								
							break;
						}
						case 3: {
							out.write("event 3:View Chapter Landing ---- count:" + rs.getInt("count") + "<br/>");								
							break;
						}
						case 4: {
							out.write("event 4:Download PDF ---- count:" + rs.getInt("count") + "<br/>");								
							break;
						}
						default: {
							out.write("event:" + eventNo + "<br/>");								
							break;
						}
					}								
				}
				if(existingEventsBuilder.toString().indexOf("1") > -1 &&
					existingEventsBuilder.toString().indexOf("2") > -1 &&
					existingEventsBuilder.toString().indexOf("3") > -1 &&
					existingEventsBuilder.toString().indexOf("4") > -1) {
					out.write("Status:No Errors");					
				} else {
					if(errorEventsBuilder.length() > 0) {
						out.write("Status:Error; affected events are " + errorEventsBuilder.toString());
					} else {
						out.write("Status:Error; existing events are " + existingEventsBuilder.toString() + "; events 1 2 3 4 should exist");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if(null != conn) {
					try {
						conn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		%>
	</body>
</html>		
		