<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
</head>

	

	<!-- body (holds country classes) -->
	<body > 
			
	<!-- bg Topline -->       
	<div id="top_container">
	
	<div id="page-wrapper">
		<f:subview id="subviewIPLogo">
			<c:import url="/components/ip_logo.jsp" />	
		</f:subview>
	
	<!-- Header Start -->
	<div id="header_container">
	    <f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		
           
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
	</div>
	
	<div id="main_container">
		<div class="titlepage_registration" ></div>
						<h:panelGroup rendered="#{param.type eq 'sa'}">
							<p class="alert" style="color: red">
								Offer code <%--${param.msg}--%> has already been activated. Please double-check your offer code and enter it again.
							</p>
						</h:panelGroup>
						
						<h:panelGroup rendered="#{param.type eq 'si'}">
							<p class="alert" style="color: red">Offer code <%--${param.msg}--%> is invalid.</p>
						</h:panelGroup>
					
					
						<h4>Registration Successful!</h4>
						<ul>
							<li><h:outputText value="#{orgConLinkedMap.userLogin.user.firstName}"/>&nbsp;<h:outputText value="#{orgConLinkedMap.userLogin.user.lastName}"/></li>
							<li><h:outputText value="#{orgConLinkedMap.userLogin.user.email}"/> </li>
						</ul>
						<p>Thank you for registering with Cambridge Books Online.</p>
						
						<%-- 
						<p>Use the menu on the left of the page to view and manage your preferences.</p>
						--%>
					
						<h:panelGroup rendered="#{orgConLinkedMap.userLogin ne null && orgConLinkedMap.userLogin.userType eq 'AO'}">
							<p>You are now the administrator for the following organisation:</p>
							<h4><h:outputText value="#{orgConLinkedMap.currentlyLoggedDisplayName}"/></h4>
							<br/>
							<p>Use the "Account Administrator" link on the upper right to view and manage your organisation's preferences.</p>
						</h:panelGroup>
						
						<!-- for succesful reloging to cjo -->
						<c:if test="${not empty param.lid}">
							<img style="display: none" src="${contextRootCjo}cbo/cboImageLogout?RAND=<%=Math.random()%>"  />
						</c:if>								
						
		</div>
		
		<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
		</f:subview>
	</div>
	
	</div>
		
	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>

  	
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
	</body>
</f:view>
</html>		
		