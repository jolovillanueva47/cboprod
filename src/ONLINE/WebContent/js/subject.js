var URL_SERVLET = $("#context_root").val() + "subject_landing.jsf";
var resultsPerPage = 50;
var currentPage = 1;
var sortType = "title_alphasort";
var subjectCode = "";
var subjectId = "";
var subject = "";
var firstLetter = "";
var searchType = "";
var isViewAll = $("#isViewAll").val()	
$(function(){	
	if("" != getUrlParam("subjectCode")) {
		subjectCode = getUrlParam("subjectCode");
	} 
	
	if("" != getUrlParam("subjectId")) {
		subjectId = getUrlParam("subjectId");
	} 

	if("" != getUrlParam("subjectName")) {
		subject = getUrlParam("subjectName");
	} 

	if("" != getUrlParam("firstLetter")) {
		firstLetter = getUrlParam("firstLetter");
	} 

	if("" != getUrlParam("searchType")) {
		searchType = getUrlParam("searchType");
	} 

	if("" != getUrlParam("page")) {
		currentPage = getUrlParam("page");
	}
	
	if("" != getUrlParam("results")) {
		resultsPerPage = getUrlParam("results");
		$(".results_per_page option").removeAttr("selected");
		if("50" == resultsPerPage) {
			$(".results_per_page option[value='50']").attr("selected", true);
		} else if("75" == resultsPerPage) {
			$(".results_per_page option[value='75']").attr("selected", true);
		} else if("100" == resultsPerPage) {
			$(".results_per_page option[value='100']").attr("selected", true);
		}
	} else {
		$(".results_per_page option[value='50']").attr("selected", true);
	}
	
	if("" != getUrlParam("sort")) {
		sortType = getUrlParam("sort");
		$(".sort_type option").removeAttr("selected");
		if("title_alphasort" == sortType) {
			$(".sort_type option[value='title_alphasort']").attr("selected", true);
		} else if("author_name_alphasort" == sortType) {
			$(".sort_type option[value='author_name_alphasort']").attr("selected", true);
		} else if("print_date" == sortType) {
			$(".sort_type option[value='print_date']").attr("selected", true);
		} else if("online_date" == sortType) {
			$(".sort_type option[value='online_date']").attr("selected", true);
		}
	} else {
		$(".sort_type option[value='title_alphasort']").attr("selected", true);
	}

	// results per page			
	$(".results_per_page").change(function(){	
		var subjectParam = "subjectCode";
		var code = subjectCode;
		if ('true' == isViewAll) {
			subjectParam = "subjectId";
			code = subjectId;
		}
		
		$("#browse_sub_subject_form").attr("action", URL_SERVLET + "?" + subjectParam + "=" + code 
				+ "&subjectName=" + subject 
				+ "&firstLetter=" + firstLetter 
				+ "&searchType=" + searchType 
				+ "&results=" + $(this).val());
		$("#browse_sub_subject_form").submit();
	});

	// goto page
	$(".go_to_page").val(currentPage);
	$(".bot_go").click(function(){
		var subjectParam = "subjectCode";
		var code = subjectCode;
		if ('true' == isViewAll) {
			subjectParam = "subjectId";
			code = subjectId;
		}
		
		$("#browse_sub_subject_form").attr("action", URL_SERVLET + "?" + subjectParam + "=" + code 
				+ "&subjectName=" + subject 
				+ "&firstLetter=" + firstLetter 
				+ "&searchType=page&page=" + $("#txt_go_to_page").val());
		$("#browse_sub_subject_form").submit();
	});

	$("#browse_sub_subject_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$(".bot_go").click();
        }
	});
	
	// pager navigation
	$(".page_nav").click(function(){
		var subjectParam = "subjectCode";
		var code = subjectCode;
		if ('true' == isViewAll) {
			subjectParam = "subjectId";
			code = subjectId;
		}

		$("#browse_sub_subject_form").attr("action", URL_SERVLET + "?" + subjectParam + "=" + code 
				+ "&subjectName=" + subject 
				+ "&firstLetter=" + firstLetter 
				+ "&searchType=page&page=" + $(this).attr("name"));
		$("#browse_sub_subject_form").submit();
	});

	// sort
	$(".sort_type").change(function(){
		var subjectParam = "subjectCode";
		var code = subjectCode;
		if ('true' == isViewAll) {
			subjectParam = "subjectId";
			code = subjectId;
		}
		
		$("#browse_sub_subject_form").attr("action", URL_SERVLET + "?" + subjectParam + "=" + code 
				+ "&subjectName=" + subject 
				+ "&firstLetter=" + firstLetter 
				+ "&searchType=" + searchType 
				+ "&results=" + resultsPerPage
				+ "&sort=" + $(this).val());
		$("#browse_sub_subject_form").submit();
	});

});
			