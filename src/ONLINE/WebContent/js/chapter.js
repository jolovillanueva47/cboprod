$(function(){
	$('#anchorImageExtract').click(function(){
		$('#anchorImageExtract').hide();
		$('#imageShown').show();
		$('#divHtmlExtract').fadeOut('fast', function() {
			$('#divImageExtract').fadeIn('fast');	
		});			
	});

	$('#anchorHtmlExtract').click(function(){
		$('#imageShown').hide();
		$('#anchorImageExtract').show();
		$('#divImageExtract').fadeOut('fast', function() {
			$('#divHtmlExtract').fadeIn('fast');	
		});
	});	
});