var URL_SERVLET = $("#context_root").val() + "search";
var resultsPerPage = "50";
var currentPage = "1";
var sortType = "score";

$(function(){	
	$("#toggle_search").hide();
	$("#toggle_search_link").toggle(
		function(){		
			$("#toggle_search").fadeIn("fast");
			$("#toggle_search_link").html("Hide Search");
		},
		function(){
			$("#toggle_search").fadeOut("fast");
			$("#toggle_search_link").html("Show Search");
		}
	);
	
	// req params
	if("" != getUrlParam("resultsPerPage")) {
		resultsPerPage = getUrlParam("resultsPerPage");
		$(".results_per_page option").removeAttr("selected");
		if("50" == resultsPerPage) {
			$(".results_per_page option[value='50']").attr("selected", true);
		} else if("75" == resultsPerPage) {
			$(".results_per_page option[value='75']").attr("selected", true);
		} else if("100" == resultsPerPage) {
			$(".results_per_page option[value='100']").attr("selected", true);
		}
	} else {
		$(".results_per_page option[value='50']").attr("selected", true);
	}

	if("" != getUrlParam("currentPage")) {
		currentPage = getUrlParam("currentPage");
	}

	if("" != getUrlParam("sortType")) {
		sortType = getUrlParam("sortType");
		$(".sort_type option").removeAttr("selected");
		if("score" == sortType) {
			$(".sort_type option[value='score']").attr("selected", true);
		} else if("main_title_alphasort" == sortType) {
			$(".sort_type option[value='main_title_alphasort']").attr("selected", true);
		} else if("author_alphasort" == sortType) {
			$(".sort_type option[value='author_alphasort']").attr("selected", true);
		} else if("print_date_range" == sortType) {
			$(".sort_type option[value='print_date_range']").attr("selected", true);
		} else if("online_date_range" == sortType) {
			$(".sort_type option[value='online_date_range']").attr("selected", true);
		}
	} else {
		$(".sort_type option[value='score']").attr("selected", true);
	}
	
	// results per page			
	$(".results_per_page").change(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=page&resultsPerPage=" + $(this).val());
		$("#search_result_form").submit();
	});
	
	// go to page			
	$(".go_to_page").val(currentPage);
	$("#go_btn_top").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=page&resultsPerPage=" 
				+ resultsPerPage + "&currentPage=" + $("#goToPageTop").val() + "&sortType=" + sortType);
		$("#search_result_form").submit();
	});
	$("#go_btn_bottom").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=page&resultsPerPage=" 
				+ resultsPerPage + "&currentPage=" + $("#goToPageBottom").val() + "&sortType=" + sortType);
		$("#search_result_form").submit();
	});
	$("#goToPageTop").keyup(function(event){
		if(event.keyCode == 13){
			$("#go_btn_top").click();
		}
	});
	$("#goToPageBottom").live('keyup', function(event) {
		  if(event.keyCode == 13){
		    $("#go_btn_bottom").click();
		  }
	});
	
	// pager navigation
	$(".page_nav").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=page&resultsPerPage=" 
				+ resultsPerPage + "&currentPage=" + $(this).attr("name") + "&sortType=" + sortType);
		$("#search_result_form").submit();
	});

	// sort
	$(".sort_type").change(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=page&resultsPerPage=" 
				+ resultsPerPage + "&sortType=" + $(this).val());
		$("#search_result_form").submit();
	});

	// filters
	$(".filter_content_type").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=content_type&filterValue=" + $(this).attr("id"));
		
				
		$("#search_result_form").submit();
	});

	$(".filter_subject").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=subject_facet&filterValue=" + $(this).attr("id"));
		
				
		$("#search_result_form").submit();
	});

	$(".filter_author").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=author_name_facet&filterValue=" + $(this).attr("id"));
				
		
		$("#search_result_form").submit();
	});

	$(".filter_year").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=print_date&filterValue=" + $(this).attr("id"));
		
		
		$("#search_result_form").submit();
	});

	$(".filter_top_book").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=book_title_facet&filterValue=" + $(this).attr("id"));
		
				
		$("#search_result_form").submit();
	});

	$(".filter_top_journal").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=journal_title_facet&filterValue=" + $(this).attr("id"));
				
		$("#search_result_form").submit();
	});

	$(".filter_series").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=series_facet&filterValue=" + $(this).attr("id"));
		
		$("#search_result_form").submit();
	});

	$(".remove_filter").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=remove&filterValue=" + $(this).attr("id"));
				
		$("#search_result_form").submit();
	});

	$("#reset_filter").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage=" 
				+ resultsPerPage + "&filterType=reset");
				
		$("#search_result_form").submit();
	});
	
	$(".bot_reset").click(function(){		
		$("input.filter").attr("checked", false);		
	});
	
	$(".bot_select_all").click(function(){
		$("input.filter").attr("checked", true);
	});
	
	$(".bot_submit").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=filter&resultsPerPage="
				+ resultsPerPage + "&filterType=multiple");
		$("#search_result_form").submit();	
	});
	
	// show/hide details
	$(".link_view").toggle( 
		function(){
			var cid = $(this).attr("name");		
			$(this).html('View less<img src="images/icon_arrow_less_info.gif" alt="" width="13" height="10" />');
			$(this).attr("title", "View less");
			$("." + cid + "_hide").fadeIn("fast");
		}, function() {
			var cid = $(this).attr("name");
			$(this).html('View more<img src="images/icon_arrow_more_info.gif" alt="" width="13" height="10" />');
			$(this).attr("title", "View more");
			$("." + cid + "_hide").fadeOut("fast");
		}
	);
	
	// journal
	$(".preview").toggle(
		function(){		
			var divPrevAbstract = $(this).parent().parent().next(".preview_abstract");
			divPrevAbstract.attr("style", "display: block;");
			var p = divPrevAbstract.find(".journal_preview");
			var img = $("img[name='image_" + p.attr("id") + "']");
			//p.removeClass("hide_preview");
			//p.addClass("journal_preview");
			p.fadeIn("fast");
			if(!p.hasClass("isLoaded")) {				
				$.ajax({url: "cjoc/content" + p.attr("id"), data: ({dummy: Math.random()}),
					success: function(data){
						var parsedData = "";
						parsedData = data.replace(/<link[^<]*<\/\s*link\s*>/i, "");
						parsedData = parsedData.replace(/<style[^<]*<\/\s*style\s*>/i, "");
						parsedData =  parsedData.replace(/<script[^<]*<\/\s*script\s*>/i, "");
						p.append(parsedData);
						p.addClass("isLoaded");
						img.attr("style", "display: none;");
					}, error: function() {
						p.append("Error loading data.");
						p.addClass("isLoaded");
						img.attr("style", "display: none;");
					}
				});
			}
		},
		function(){
			var divPrevAbstract = $(this).parent().parent().next(".preview_abstract");
			divPrevAbstract.attr("style", "display: none;");
			var p = divPrevAbstract.find(".journal_preview");
			p.fadeOut("fast");
		}
	);
	
	// spell check
	$("#spell_check").click(function(){
		$("#search_result_form").attr("action", URL_SERVLET + "?searchType=quick&searchText=" 
				+ $(this).html());
		$("#search_result_form").submit();			
	});
	
	$("#goToPageTop").numeric();
});