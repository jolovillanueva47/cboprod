var onSeries = false;
var delimiterMajor = "<";
var delimiterMinor = ">";
var levelCheck = 0;
var stop = false;
var contextRoot = $("#context_root").val();

$(document).ready(function(){
	$('div.subjectTree li.search a.tree_item').click(function(){search(this);});
	onSeries = $('div.subjectTree').hasClass('onSeries');
	initClick();
});

//modify by reg
function search(element) {
	startLoading(element);
		
	var searchOpt = onSeries ? 'seriesSearch' : 'subjectSearch';
//	var servlet = searchOpt + '?subjectId=' + $(element).attr('id');
	var servlet = contextRoot + searchOpt + '?path=' + $(element).attr('id');
//	var feeds = $('#feedIds').val();


	$.getJSON(servlet,
			function(data){


				var append = '<ul style="display: none">';

				$.each(data, function(i, item) {
					if (item.finalLevel == 'Y' && onSeries) {
						append = append + '<li class="search"><a id="' + item.vistaSubjectCode + '" href="javascript:void(0);" class="icons_img tree_plus seriesSearch tree_item"></a><a id="' + item.vistaSubjectCode + '" href="javascript:void(0);" class="tree_item seriesSearch">' + item.subjectName + '</a>'
						+ ' [<a href="' + contextRoot + 'all_series.jsf?searchType=allSeriesSubLevel&seriesCode=' +item.path.substring(item.path.lastIndexOf(';')+1)+ '&firstLetter=' + item.subjectName + '&seriesName='+item.subjectName+'">View All</a>]</li>' 
						//+ ((feeds.indexOf(item.subjectId) != -1) ?  '<a href="/feeds/rss/feed_' + item.subjectId + '_rss_2.0.xml" target="_blank"><img src="../images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>' : '') + '</li>';
					} else if (item.finalLevel == 'Y' && !onSeries) {
						append = append + '<li><a href="' + contextRoot + 'subject_landing.jsf?searchType=allTitles&subjectCode=' + item.vistaSubjectCode + '&subjectName=' + item.subjectName + '" class="icons_img bullet02"></a><a href="/subject_landing.jsf?searchType=allTitles&subjectCode=' + item.vistaSubjectCode + '&subjectName=' + item.subjectName + '">' + item.subjectName + '</a></li>'
						//+ ((feeds.indexOf(item.subjectId) != -1) ?  '<a href="/feeds/rss/feed_' + item.subjectId + '_rss_2.0.xml" target="_blank"><img src="../images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>' : '') + '</li>';
					} else if (!onSeries){
						append = append + '<li class="search"><a id="' + item.path + '" href="javascript:void(0);" class="icons_img tree_plus subjectSearch tree_item"></a><a id="' + item.path + '" href="javascript:void(0);" class="tree_item  subjectSearch">' + item.subjectName + '</a>'
						+ ' [<a href="' + contextRoot + 'subject_landing.jsf?searchType=allSubjectBook&subjectId=' +item.path.substring(item.path.lastIndexOf(';')+1)+ '&subjectName=' + item.subjectName + '">View All </a>]</li>' 
						//+ ((feeds.indexOf(item.subjectId) != -1) ?  '<a href="/feeds/rss/feed_' + item.subjectId + '_rss_2.0.xml" target="_blank"><img src="../images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>' : '') + '</li>';
					} else {
						append = append + '<li class="search"><a id="' + item.path + '" href="javascript:void(0);" class="icons_img tree_plus subjectSearch tree_item"></a><a id="' + item.path + '" href="javascript:void(0);" class="tree_item  subjectSearch">' + item.subjectName + '</a>'
						+ ' [<a href="' + contextRoot + 'all_series.jsf?searchType=allSubjectBook&seriesCode=' +item.path.substring(item.path.lastIndexOf(';')+1)+ '&firstLetter=' + item.subjectName + '&seriesName='+item.subjectName+'">View All</a>]</li>'
						//+ ((feeds.indexOf(item.subjectId) != -1) ?  '<a href="/feeds/rss/feed_' + item.subjectId + '_rss_2.0.xml" target="_blank"><img src="../images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>' : '') + '</li>';
					}
				});

				append = append + '</ul>';
				$(element).parent().append(append);

				$(element).parent().children('ul').children('li.search').children('a.subjectSearch').click(function(){search(this);});
				$(element).parent().children('ul').children('li.search').children('a.seriesSearch').click(function(){seriesSearch(this);});

				stopLoading(element);

				//for crumbtrail
				checkForNewItems();				

				//for initializing the clicks		
				if(levelCheck > 0 && stop == false){										
					initClick();
				}
			}
	);
}

function seriesSearch(element) {
	startLoading(element);
	$.getJSON(contextRoot + 'seriesSearch?getSeries=1&subjectCode=' + $(element).attr('id'),
			function(data){
		
				var append = '<ul style="display: none">';
				$.each(data, function(i, item) {
					var sort = 'print_date';
					if(item.seriesNumber != null 
						&& jQuery.trim(item.seriesNumber) != ''
						&& item.seriesNumber != 'null') {

						sort = 'series_number';
					}

					
					append = append + '<li><a id="' + item.seriesCode + '" class="icons_img bullet02"></a><a onclick="modifyLink(\'' + item.seriesCode + '\')" href=\"' + contextRoot + 'series_landing.jsf?seriesCode=' + item.seriesCode + '&seriesTitle=' + item.seriesName + '&sort=' + sort + '\" href="javascript:void(0);">' + item.seriesName + '</a></li>'
				});
				append = append + '</ul>';
				$(element).parent().append(append);
				
				stopLoading(element);
			}
	);
}

function startLoading(element) {
	$(element).parent().children('a.tree_item').unbind('click');
	$(element).parent().children('a:first-child').removeClass('icons_img');
	$(element).parent().children('a:first-child').removeClass('tree_plus');
	$(element).parent().children('a:first-child').addClass('treeloader');
}

function stopLoading(element) {
	$(element).parent().removeClass('search');
	$(element).parent().addClass('collapse');
	$(element).parent().children('ul').slideToggle(300);

	$(element).parent().children('a.tree_item').click(function(){collapse(this)});
	$(element).parent().children('a:first-child').removeClass('treeloader');
	$(element).parent().children('a:first-child').addClass('tree_minus');
	$(element).parent().children('a:first-child').addClass('icons_img');
}

function collapse(element) {
	$(element).parent().children('a.tree_item').unbind('click');
	$(element).parent().children('a:first-child').removeClass('tree_minus');
	$(element).parent().children('a:first-child').addClass('tree_plus');
	
	$(element).parent().removeClass('collapse');
	$(element).parent().addClass('expand');
	$(element).parent().children('ul').slideToggle(300);
	$(element).parent().children('a.tree_item').click(function(){expand(this)});
}

function expand(element) {
	$(element).parent().children('a.tree_item').unbind('click');
	$(element).parent().children('a:first-child').removeClass('tree_plus');
	$(element).parent().children('a:first-child').addClass('tree_minus');
	
	$(element).parent().removeClass('expand');
	$(element).parent().addClass('collapse');
	$(element).parent().children('ul').slideToggle(300);
	$(element).parent().children('a.tree_item').click(function(){collapse(this)});
}

function clickElem(id){	
	var elem = $('#'+id);
	if(elem.hasClass('tree_plus')){
		elem.click();
	}
}


function initClick(){
	var loc =  window.location.href;
	var idx = loc.indexOf('SUBJECT_CODE=');
	var idxCrumb = loc.indexOf('CRUMB=');
	//coming from homepage
	if ( idx > -1 && idxCrumb == -1) {		
		var id = loc.substring(idx+'SUBJECT_CODE='.length,loc.length);
		clickElem(id);		
	}else{		
		//coming from crumbtrail
		var id = loc.substring(idx+'SUBJECT_CODE='.length,idxCrumb-1);
		var fullCrumb = loc.substring(idxCrumb+'CRUMB='.length, loc.length);
		fullCrumb = window.decodeURIComponent(fullCrumb);
		var fullCrumbArr = fullCrumb.split(delimiterMajor)		
		var item = fullCrumbArr[levelCheck];		
		var items = item.split(delimiterMinor);
		if(id == items[1]){
			stop = true;			
		}				
		clickElem(items[1]);
		levelCheck++;		
	}
}

function checkForNewItems(){
	var loc =  window.location.href;	
	var idx = loc.indexOf('tree.jsf');	
	if ( idx > -1 ) {		
		$(".subjectTree a").each(assignClick);		
	}
}

/**
 * modified to accept view all as well
 * @return
 */
function assignClick() {
	var link = $(this);
	var href = link.attr("href");
	//javascript is string href="javascript:"	
	if(href.indexOf("javascript") == -1 || link.html == "View All"){
		//remove crumbtrail
//		link.click(function(){			
//			var crumb = getTrail(link);
//			
//			link.attr("href",href + "&CRUMB=" + encodeURIComponent(crumb) );
//			return true;
//		});
	}	
}

function modifyLink(element){
	//new crumbtrail hence remove
	//Parent subject	
//	var link = $("#" + element).next();
//	var href = link.attr("href");
//	var parentSubjectCode = link.parent().parent().parent().children().attr("id");			
//	link.attr("href",href + "&parentSubjectCode=" + parentSubjectCode);
	//alert(link.attr("href")); 
//	return true;
}


/**
 * finds the path from the clicked element to the trail --- modified to find view all
 * @param element
 * @return
 */
function getTrail(element){	
	if(element.html() == "View All"){
		var link = element.prev();		
	}else{
		var link = element.parent().parent().parent().children().next();
	}
	
	var id = $.trim(link.attr("id"));
	var linkName = $.trim(link.html());
	
	if(endTrail(linkName)){
		return linkName + delimiterMinor + id;
	}else{
		return getTrail(link) + delimiterMajor + linkName + delimiterMinor + id;
	}		
}

/**
 * checks if this is the end of the getTrail recursion
 */
var endTrailArr = ["Humanities", "Social Sciences", "Science and Engineering", "Medicine", "English Language Teaching"];
function endTrail(linkName){
	for(var i=0; i<endTrailArr.length; i++ ){
		if(endTrailArr[i] == linkName){
			return true;
		}
	}
	return false; 
}