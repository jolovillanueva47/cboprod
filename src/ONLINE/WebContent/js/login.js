$(document).ready(function(){	
		$('#logintoggle').click(function () {
			$('#top-login .loginbox').slideToggle(300, function(){
				$('#logintoggle').toggleClass('up');
			});
		});
		
		$('#top-login .box').focus(function () {
			if($(this).val() == 'Login...') $(this).val('');
		});
		$('#top-login .box').blur(function () {
			if($(this).val() == '') $(this).val('Login...');
		});
	});