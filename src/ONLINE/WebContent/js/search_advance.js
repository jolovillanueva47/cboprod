var DEFAULT_QUERY_COUNT = 2;
	
var OPT_TXT_ALL = "Anything";
var OPT_TXT_AUTHOR = "Author / Editor / Contributor";		
var OPT_TXT_TITLE = "Title / Volume";
var OPT_TXT_SUBJECT = "Subject";
var OPT_TXT_PRINT_DATE = "Print Publication Year";
var OPT_TXT_ONLINE_DATE = "Online Publication Date";
var OPT_TXT_ISBN = "ISBN";
var OPT_TXT_SERIES = "Series Name";
var OPT_TXT_DOI = "Digital Object Identifier";
var OPT_TXT_KEYWORD = "Keyword";
var OPT_TXT_AUTH_AFF = "Author Affiliation";

var OPT_VAL_ALL = "all";
var OPT_VAL_AUTHOR = "author";		
var OPT_VAL_TITLE = "title";
var OPT_VAL_SUBJECT = "subject";
var OPT_VAL_PRINT_DATE = "print_date";
var OPT_VAL_ONLINE_DATE = "online_date";
var OPT_VAL_ISBN = "isbn_issn";
var OPT_VAL_SERIES = "series";
var OPT_VAL_DOI = "doi";
var OPT_VAL_KEYWORD = "keyword";
var OPT_VAL_AUTH_AFF = "auth_aff";

var OPT_TXT_CONTAINS = "Contains";
var OPT_TXT_DOES_NOT_CONTAIN = "Does not contain";
var OPT_TXT_IS_EXACTLY = "Is exactly"; 
var OPT_TXT_IS_BETWEEN = "Is between";
var OPT_TXT_IS_FROM = "Is from";

var OPT_VAL_CONTAINS = "contain";
var OPT_VAL_DOES_NOT_CONTAIN = "not_contain";
var OPT_VAL_IS_EXACTLY = "exactly"; 
var OPT_VAL_IS_BETWEEN = "between";
var OPT_VAL_IS_FROM = "from";

var OPT_TXT_AND = "AND";
var OPT_TXT_OR = "OR";

var OPT_VAL_AND = "and";
var OPT_VAL_OR = "or";

var URL_ADVANCE_SEARCH = "advance_search.jsf";
var URL_SEARCH_RESULTS = "search";
var queryCount = 0;

var FORMAT_PRINT_DATE = "yyyy";
var FORMAT_ONLINE_DATE = "yyyyMMdd";

var URL_SERVLET = "search";

function addDateInputBox(fieldName, dateFormat){
	var elem = $(fieldName); 	
	elem.val(dateFormat);
	elem.click(function(){
		var value = elem.val();
		if(value == dateFormat){			
			elem.val("");
		}
	});
	elem.blur(function(){
		var value = elem.val();
		if($.trim(value) == ""){			
			elem.val(dateFormat);
		}
	});	
}

function getCurrentPage() {
	var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf("/") + 1);
	return filename;
}

function initialize(querySize) {
	if(URL_ADVANCE_SEARCH == getCurrentPage()) {
		queryCount = 0;
		//logicalOps = new Array();	
		for (i = 0; i < querySize; i++) {
			addQuery();
		}		
	} else if (URL_SEARCH_RESULTS == getCurrentPage()) {
		var conditionArr = $.makeArray($(".condition"));
		var logicalOpArr = $.makeArray($(".logical_op"));
		var searchTextArr = $.makeArray($(".search_text"));
		var searchText2Arr = $.makeArray($(".search_text_2"));
		var sFilterArr = $.makeArray($(".s_filter"));
		
		$.each($(".search_filter"), function(index, domElem){
			var filter = $(this).val();
			$.each($(".s_filter"), function(index, domElem){
				if ($(this).val() == filter) {
					$(this).attr("checked", true);
				}
			});
		});
		
		$.each($(".target"), function(index, domElem){
			if (index > 0) { 
				addQueryFromSession(domElem.value, conditionArr[index].value, 
						searchTextArr[index].value, searchText2Arr[index].value, 
						logicalOpArr[index - 1].value);
			} else {
				addQueryFromSession(domElem.value, conditionArr[index].value, 
						searchTextArr[index].value, searchText2Arr[index].value, 
						"");
			}
		});
	}

	if(queryCount <= 1) {
		disableButtonViaId("removeButton");
	}
}

function addQuery() {
	var idNum = ++queryCount;
	$("#advance_query_option").append(
		$("<tr />")
			.attr("id", idNum)
	);

	if (idNum > 1) { 
		createComboBox("logical_op", idNum - 1); 
	}
	createComboBox("target", idNum);
	createComboBox("condition", idNum);
	createTextBox("search_word", idNum);
	createTextBox("search_word_2", idNum);
}

function addQueryFromSession(targetStart, 
		conditionStart, searchText, searchText2, logicStart) {
	
	var idNum = ++queryCount;
	$("#advance_query_option").append(
		$("<tr />")
			.attr("id", idNum)
	);
	if (idNum > 1) { 
		createComboBox("logical_op", idNum - 1); 
		$("#logical_op_" + (idNum - 1) + " option").each(function(index) {			
			if($(this).val() == logicStart) {
				$(this).attr("selected", true);
			}
		}); 
	}
	createComboBox("target", idNum);
	createComboBox("condition", idNum);
	createTextBox("search_word", idNum);
	createTextBox("search_word_2", idNum);	
	
	$("#target_" + idNum + " option").each(function(index) {
		if($(this).val() == targetStart) {
			$(this).attr("selected", true);
			$(this).change();
		}
	}); 
	
	$("#condition_" + idNum + " option").each(function(index) {
		if($(this).val() == conditionStart) {
			$(this).attr("selected", true);
			$(this).change();
		}
	}); 
	
	$("#search_word_" + idNum).each(function(index) {
		$(this).val(searchText);
	}); 
	
	$("#search_word_2_" + idNum).each(function(index) {
		$(this).val(searchText2);
	}); 
}

function createComboBox(id, idNum) {
	if("target" == id) {
		$("#" + idNum).append(
			$("<td />").append(
				$("<select />")
				.attr("id", id + "_" + idNum)
				.attr("name", id)
				.addClass("select_02")
				.append(
					$("<option />")					
						.val(OPT_VAL_ALL)
						.text(OPT_TXT_ALL)		
				)
				.append(
					$("<option />")
						.val(OPT_VAL_AUTHOR)
						.text(OPT_TXT_AUTHOR)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_TITLE)
						.text(OPT_TXT_TITLE)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_SUBJECT)
						.text(OPT_TXT_SUBJECT)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_PRINT_DATE)
						.text(OPT_TXT_PRINT_DATE)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_ONLINE_DATE)
						.text(OPT_TXT_ONLINE_DATE)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_ISBN)
						.text(OPT_TXT_ISBN)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_SERIES)
						.text(OPT_TXT_SERIES)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_DOI)
						.text(OPT_TXT_DOI)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_KEYWORD)
						.text(OPT_TXT_KEYWORD)
				)
				.append(
					$("<option />")
						.val(OPT_VAL_AUTH_AFF)
						.text(OPT_TXT_AUTH_AFF)
				)
				.change(function(){
					if (OPT_VAL_PRINT_DATE == $(this).val() 
							|| OPT_VAL_ONLINE_DATE == $(this).val() 
							|| OPT_VAL_ISBN == $(this).val()) {						
						
						$(".has_second_option_" + idNum).remove();
						
						$("#between_span_" + idNum).hide(0);
						$("#from_span_" + idNum).hide(0);
						$("#search_word_2_" + idNum).hide(0);	
						
						$("#condition_" + idNum)
						.append(
							$("<option />")
								.addClass("has_second_option_" + idNum)
								.val(OPT_VAL_IS_BETWEEN)
								.text(OPT_TXT_IS_BETWEEN)										
						)
						.append(
							$("<option />")
								.addClass("has_second_option_" + idNum)
								.val(OPT_VAL_IS_FROM)
								.text(OPT_TXT_IS_FROM)
						)
					} else {
						$(".has_second_option_" + idNum).remove();
						
						$("#between_span_" + idNum).hide(0);
						$("#from_span_" + idNum).hide(0);
						$("#search_word_2_" + idNum).hide(0);
					}
				})
			)
		);		
	} else if("condition" == id) {
		$("#" + idNum).append(
			$("<td />").append(
				$("<select />")
				.attr("id", id + "_" + idNum)
				.attr("name", id)
				.addClass("select_02")
				.append(
					$("<option />")
						.val(OPT_VAL_CONTAINS)
						.text(OPT_TXT_CONTAINS)			
				)
				.append(
					$("<option />")
						.val(OPT_VAL_DOES_NOT_CONTAIN)
						.text(OPT_TXT_DOES_NOT_CONTAIN)		
				)
				.append(
					$("<option />")
						.val(OPT_VAL_IS_EXACTLY)
						.text(OPT_TXT_IS_EXACTLY)	
				)
				.change(function(){
					var target = jQuery.trim($("#target_" + idNum).val());
					
					if(OPT_VAL_PRINT_DATE == target 
							|| OPT_VAL_ONLINE_DATE == target 
							&& (OPT_VAL_IS_BETWEEN == $(this).val() 
									|| OPT_VAL_IS_FROM == $(this).val())) {		
						
						if(OPT_VAL_ONLINE_DATE == target) {
							addDateInputBox("#search_word_" + idNum, FORMAT_ONLINE_DATE);
							addDateInputBox("#search_word_2_" + idNum, FORMAT_ONLINE_DATE);
						} else {
							addDateInputBox("#search_word_" + idNum, FORMAT_PRINT_DATE);
							addDateInputBox("#search_word_2_" + idNum, FORMAT_PRINT_DATE);
						}
					} 
					
					if (OPT_VAL_IS_BETWEEN == $(this).val()) {
						$("#between_span_" + idNum).show(0);
						$("#from_span_" + idNum).hide(0);
						$("#search_word_2_" + idNum).show(0);
					} else if (OPT_VAL_IS_FROM == $(this).val()) {
						$("#from_span_" + idNum).show(0);
						$("#between_span_" + idNum).hide(0);
						$("#search_word_2_" + idNum).show(0);
					} else {
						$("#between_span_" + idNum).hide(0);
						$("#from_span_" + idNum).hide(0);
						$("#search_word_2_" + idNum).hide(0);
					}
				})
			)
		);
	} else if("logical_op" == id) {
		$("#" + idNum).append(
			$("<td>").append(						
				$("<select />")
				.attr("id", id + "_" + idNum)
				.attr("name", id)
				.addClass("options")
				.append(
					$("<option />")
						.val(OPT_VAL_AND)
						.text(OPT_TXT_AND)	
				)
				.append(
					$("<option />")
						.val(OPT_VAL_OR)
						.text(OPT_TXT_OR)	
				)
			)	
		);
	}	
}

function createTextBox(id, idNum) {
	if("search_word" == id) {
		$("#" + idNum).append(
			$("<td />").append(
				$("<input />")
					.attr("id", id + "_" + idNum)
					.attr("name", id)
					.attr("type", "text")
					.addClass("inputField")
					.addClass("input_01")
			) 
		);
	} else {
		$("#" + idNum).append(
			$("<td />").append(
				$("<label />")
				.attr("style", "display: none;")
				.attr("id", "between_span_" + idNum)
				.text(" and ")
			).append(
				$("<label />")
				.attr("style", "display: none;")
				.attr("id", "from_span_" + idNum)
				.text(" to ")
			).append(
				$("<input />")
				.attr("style", "display: none;")
				.attr("id", id + "_" + idNum)
				.attr("name", id)
				.attr("type", "text")
				.addClass("inputField")
				.addClass("input_01")
			) 
		);
	}
}

function disableButtonViaId(id) {
	if("removeButton" == id) {
		$("#" + id).attr("class", "button_disabled");
	} else if("addButton" == id) {
		$("#" + id).attr("class", "button_disabled");		
	}
	//$("#" + id).attr("disabled", "disabled");
}

function enableButtonViaId(id) {
	if("removeButton" == id) {
		$("#" + id).attr("class", "");
	} else if("addButton" == id) {
		$("#" + id).attr("class", "");		
	}
	$("#" + id).removeAttr("disabled");
}

function addEvents() {
	var ebooksContextPath = $("#context_root").val();
	
	$("#removeButton").click(function(){
		if(queryCount > 1) {
			$("#" + queryCount--).remove();
			$("#logical_op_" + queryCount).parent().remove();
			enableButtonViaId("addButton");
		}
		if(queryCount <= 1){
			disableButtonViaId("removeButton");
		}
	});
	
	$("#clearAllButton").click(function(){
		$("#advance_query_option").empty();
		initialize(1);
		enableButtonViaId("addButton");
		disableButtonViaId("removeButton");
	});
	
	$("#addButton").click(function(){		
		if(queryCount < 8) {
			addQuery();	
			enableButtonViaId("removeButton");
		}
		if(queryCount >= 8) {
			disableButtonViaId("addButton");
		}
	});
	
	$("#searchButton").click(function(){
		var valid = false;
		var valid2 = false;
		for(i = 1; i <= queryCount; i++) {
			var term = jQuery.trim($("#search_word_" + i).val());
			var term2 = jQuery.trim($("#search_word_2_" + i).val());
			var condition = jQuery.trim($("#condition_" + i).val());
			var target = jQuery.trim($("#target_" + i).val());
			
			if((OPT_VAL_PRINT_DATE == target
					|| OPT_VAL_ONLINE_DATE == target) &&					
					(OPT_VAL_IS_BETWEEN == condition || OPT_VAL_IS_FROM == condition)) {
				
				if (null != term2 && term2.length > 0) {
					valid2 = true;
				}

				//check if valid date format (print_date)				
				if(OPT_VAL_PRINT_DATE == target && !Date.parseExact(term2, "yyyy")){					
					alert("Invalid date format.");
					return false;
				} else if(OPT_VAL_ONLINE_DATE == target && !Date.parseExact(term2, "yyyyMMdd")) {
					alert("Invalid date format.");
					return false;
				}
			}
			
			if (term != null && term.length > 0) {
				valid = true;
				if((OPT_VAL_PRINT_DATE == target
						|| OPT_VAL_ONLINE_DATE == target) &&					
						(OPT_VAL_IS_BETWEEN == condition || OPT_VAL_IS_FROM == condition)) {
					
					//check if valid date format (print_date)				
					if(OPT_VAL_PRINT_DATE == target && !Date.parseExact(term, "yyyy")){					
						alert("Invalid date format.");
						return false;
					} else if(OPT_VAL_ONLINE_DATE == target && !Date.parseExact(term, "yyyyMMdd")) {
						alert("Invalid date format.");
						return false;
					}
				}					
			} 
			
		}
		if (valid) {
			if(OPT_VAL_IS_BETWEEN == condition || OPT_VAL_IS_FROM == condition) {
				if (valid2) {
					$("#advance_search_form").attr("action", ebooksContextPath + URL_SERVLET + "?searchType=advance");
					$("#advance_search_form").submit();
				} else {
					alert("Invalid search.");
				}
			} else {
				$("#advance_search_form").attr("action", ebooksContextPath + URL_SERVLET + "?searchType=advance");
				$("#advance_search_form").submit();
			}
		} 
	});
	
	$("#advance_search_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$("#searchButton").click();
        }
	});	
}
		
$(function(){
	initialize(DEFAULT_QUERY_COUNT);
	addEvents();
});