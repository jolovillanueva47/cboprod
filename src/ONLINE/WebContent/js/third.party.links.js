var contextRoot = $("#context_root").val();

//used by AAA ebook references
function openUrl(params) {
    var loc = contextRoot + 'openUrlResolver.jsf?' + params
    window.open(loc, 'openUrl');
}

function googleScholar(params) {
	
	var loc = 'http://scholar.google.com/scholar?q=';
	var author = getParam(params, 'author');
	author = jQuery.trim(author);
	
	
	var title = getParam(params, 'title');
	title = jQuery.trim(title);
	
	var q = getParam(params, 'q');
	q = jQuery.trim(q);
	
	if (author != null &&  author.length > 0) {
		author = '%22author%3A' + author.replace(/ /g,' author%3A');
		loc += author + ' ';
	}
	
	if (title != null && title.length > 0) {	
		loc += '%22' + title;
	}
	
	if (q != null && q.length > 0) {
		//alert(q);
		loc += q;
	}
	
	loc = loc.replace(/ /g, '+') + '%22';
	window.open(loc, 'googleScholar');
}

function googleBooks(params) {
	var loc = 'http://books.google.com/books?lr=&as_brr=0&q=';
	var title = getParam(params, 'title');
	loc += title;
	//loc += title + params.indexOf(title, title.length);
	window.open(loc, 'googleBooks');
}

function getParam(s, paramName) {
	var result;
	var i = s.indexOf('&' + paramName + '=');
	if(i==-1){
		i = s.indexOf(paramName + '=');
	}
	if (i > -1) {
		i += paramName.length+2;
		var j = s.indexOf('&', i+1) > 0 ? s.indexOf('&', i+1) : s.length;
		result = stripTags(s.substring(i, j));
	} else {
		result = null;
	}
	return result;

}

function stripTags(text) {
	if(text == null || text == ""){
		return text;
	}

	return text.replace(/<i>/g, "").
	replace(/<\/i>/, "").
	replace(/<b>/g, "").
	replace(/<\/b>/g, "").
	replace(/<blockquote>/g, "").
	replace(/<\/blockquote>/g, "").
	replace(/<u>/g, "").
	replace(/<\/u>/g, "").
	replace(/<span style=\'font-variant:small-caps\'>/g, "").
	replace(/<\/span>/g, "").
	replace(/<sup>/g, "").
	replace(/<\/sup>/g, "").
	replace(/<sub>/g, "").
	replace(/<\/sub>/g, "");
}