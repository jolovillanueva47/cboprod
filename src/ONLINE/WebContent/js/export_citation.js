$(function(){
	$("#exportButtonDiv").hide();
	
	$("#exportButton").click(function(){		
		var textareaVal = $("#ImportData").val(); 	
		var form = $("#exportCitationForm");
		form.attr("action", "http://www.refworks.com/express/expressimport.asp?vendor=Cambridge%20Books%20Online&database=RWCambridgeUPressDB&filter=RefWorks%20Tagged%20Format&WNCLang=false" + textareaVal);
		form.attr("target", "RefWorksMain");
		form.attr("method", "POST");
		
		self.parent.tb_remove();		
		
		form.submit();
	});	
	
	$("#downloadButton").click(function(){
		var contextPath = $("#context_root").val();
		var includeAbstract = $("#abstractYes").is(":checked");
		var fileFormat = $(".format:checked").val();
		var emailAdd =  $("#emailText").val();
		
		var action = contextPath + "export_citation?type=download&includeAbstract=" + includeAbstract 
			+ "&fileFormat=" + fileFormat + "&emailAdd=" + emailAdd 
			+ "&isbn=" + getUrlParam("isbn") + "&id=" + getUrlParam("id") + "&author=" + getUrlParam("author")
			+ "&bookTitle=" + getUrlParam("bookTitle") + "&contentTitle=" + getUrlParam("contentTitle")
			+ "&printDate=" + getUrlParam("printDate") + "&doi=" + getUrlParam("doi") 
			+ "&bookId=" + getUrlParam("bookId") + "&contentId=" + getUrlParam("contentId")
			+ "&contributor=" + getUrlParam("contributor");
		
		var form = $("#exportCitationForm");		
		form.attr("action", action);
		form.submit();
	});
	
	$("#emailButton").click(function(){
		var contextPath = $("#context_root").val();
		var includeAbstract = $("#abstractYes").is(":checked");
		var fileFormat = $(".format:checked").val();
		var emailAdd =  $("#emailText").val();
		
		var action = contextPath + "export_citation?type=email&includeAbstract=" + includeAbstract 
			+ "&fileFormat=" + fileFormat + "&emailAdd=" + emailAdd
			+ "&isbn=" + getUrlParam("isbn") + "&id=" + getUrlParam("id") + "&author=" + getUrlParam("author")
			+ "&bookTitle=" + getUrlParam("bookTitle") + "&contentTitle=" + getUrlParam("contentTitle")
			+ "&printDate=" + getUrlParam("printDate") + "&doi=" + getUrlParam("doi") 
			+ "&bookId=" + getUrlParam("bookId") + "&contentId=" + getUrlParam("contentId")
			+ "&contributor=" + getUrlParam("contributor");
		
		var form = $("#exportCitationForm");		
		form.attr("action", action);
		form.submit();
	});
	
	$("#resetButton").click(function(){
		$("#exportCitationForm").each(function(){
			this.reset();
		});
		
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#DirExpRefWorks").click(function(){
		$("#emailButtonDiv").hide();
		$("#downloadButtonDiv").hide();
		$("#exportButtonDiv").show();
	});
	
	$("#ASCII").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#Biblioscape").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#BibTex").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#CSV").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#EndNote").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#HTML").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#Medlars").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#Papyrus").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#ProCite").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#ReferenceManager").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#RefWorks").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
	
	$("#RIS").click(function(){
		$("#emailButtonDiv").show();
		$("#downloadButtonDiv").show();
		$("#exportButtonDiv").hide();
	});
});