function usageStatWindow(contextPath, link, elemId) {
	var orgIdSet = $("#orgAllBodyIds").val();
	
	// login to cjo
	$.get(contextPath + "loginCJO?RAND=" + Math.random(), function(response){		
		if(response.indexOf("LOGGED_EBOOKS") > -1){
			$("#" + elemId).after(generateImage(contextPath, response));			
			$("#cjoLogin").load(function(){
				$.get(contextPath + "/loginCJO", {RAND:Math.random(),UPDATE_CJO_LOGGED:"Y"}, function(response){					
					MM_openWindow(link,'popup','status=yes,scrollbars=yes,width=750,height=700');
				});												
			});					
		}else{
			//when accmanagement
			if(link.indexOf("accmanagement") > -1){
				if(orgIdSet=="null"){
					MM_openWindow(link,'popup','status=yes,scrollbars=yes,width=750,height=700');
				}else{
					MM_openWindow(link + "&ORG_ID_SET=" + orgIdSet,'popup','status=yes,scrollbars=yes,width=750,height=700');
				}				
			}else{
				MM_openWindow(link,'popup','status=yes,scrollbars=yes,width=750,height=700');
			}
		}
	});
}