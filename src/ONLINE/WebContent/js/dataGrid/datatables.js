

function dg_initDataTables(cssId, className, serverSideFilterSort, multipleSelect, ajaxSource, dg_addHttpData, dg_addInitData){	
	$(document).ready(function(){		
		var initData = 
				{
				bProcessing: true,
				sAjaxSource: ajaxSource,
				bServerSide: true,	
				bJQueryUI: serverSideFilterSort,
				sPaginationType: 'full_numbers',
				bDestroy: true,
				fnDrawCallback: 
					function(oSettings){						
						if(multipleSelect == "true" || multipleSelect == "one"){
							dg_enableMultipleSelect(oSettings, cssId, multipleSelect);
						}
					},
				fnServerData: function (sSource, aoData, fnCallback) {						
						dg_addServerData(sSource, aoData, fnCallback, className, cssId, dg_addHttpData);
					},	
				fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){ 
					$(nRow, 'tr').addClass('gradeA'); return nRow; 
					}
				};		
		
		if(dg_addInitData){		
			dg_addInitData(initData);		
		}	
		
		$('#' + cssId).dataTable(initData);
		
		//alert('loaded data tables');
	});			
			
}

/*
 * front end select of items, no server side unlike dg_postItems
 * return rows of selected records usable with
 * var aData = oTable.fnGetData(value);
 * where value is the row element--tr	
 *
 */
function dg_select(callback){
	var dt = $("#pdasubject").dataTable();
	var rows = dg_fnGetSelected(dt);
	
	if(rows.length > 0){
		if(callback){
			callback(rows);
		}
	}else{
		alert("No Records Selected")
	}						
}



function dg_addServerData ( sSource, aoData, fnCallback, className, cssId, dg_addHttpData ) {
	
	/* Add some extra data to the sender */
	aoData.push( { "name": "className", "value": className } );
	aoData.push( { "name": "cssId", "value": cssId } );
	aoData.push( { "name": "rand", "value": Math.random() } );
	
	if(dg_addHttpData){
		dg_addHttpData(aoData);
	}	
	
	$.getJSON( sSource, aoData, function(json){ 
		/* Do whatever additional processing you want on the callback, then tell DataTables */
		fnCallback(json)
	}); 
}


function dg_enableMultipleSelect(oSettings, cssId, multipleSelect){	
	var doNot = false;
	
	//should not highlight if a link is clicked
	$('#' + cssId + ' tbody tr a').click( function(){
		doNot = true;		
	});	
	
	$('#' + cssId + ' tbody tr').click( function(){		
		if(doNot == false){
			if ( $(this).hasClass('row_selected') ){
				$(this).removeClass('row_selected');
			}else{
				if(multipleSelect == "one"){
					$(this).parent().children().each(function (){
						$(this).removeClass('row_selected');
					});
				}
				$(this).addClass('row_selected');
			}
		}
		//restart do not
		doNot = false;
	});
	
}

function dg_postItems(url, gridId, className, action, callback, before, httpPost){
	if(before){
		if(before() == false){
			return;
		}
	}
	//gets all rows that are selected
	var rows = dg_fnGetSelected($("#" + gridId).dataTable());		
	if(rows.length > 0){
		var cols = 0;
		var json = "{"; 
		
		$.each(
			rows,
			function(index, value){
				var i = 0;				
				$(value).children("td").each(function(){
					var _value = dg_getValueItems(this);			
					json = json + "\"aaData" + index + "_" + i + "\":" + "\"" + _value + "\"" + ",";			
					i++;		
				});			
				i = 0;
			}	
		);	
		
		json = json + "\"rows\":\"" + rows.length + "\"}";
		//json = json + "\"rand\":" + "\"" + Math.random() + "\"" + ",\"action\":\"" + action + "\",\"className\":\"" + className+"\",\"rows\":\"" + rows.length + "\"}";		
		//generate the json object repesenting all the query item
		
		var jsonObj = $.parseJSON(json);
		
		jsonObj.rand = Math.random();
		jsonObj.action = action;
		jsonObj.className = className;	
		
		if(httpPost){
			httpPost(jsonObj);
		}
				
		$.post(url, jsonObj, function (xml){		
			$("#" + gridId).dataTable().fnDraw();		
			if(callback){		
				callback(xml);
			}				
		});
	}else{
		alert("No Records Selected");
	}
}

function dg_getValueItems(element){	
	var td = $(element).html();
	td = new String(td);	
	
	var output = null;
	
	//in case of href	
	if(td.indexOf("<a") > -1 || td.indexOf("<A") > -1){		
		output = $('' + td).html();		
	}else{		
		output = td;
	}	
	return output;
}

function dg_postBean(url, enclosingDiv, className, action, callback, before){
	
	if(before){		
		if(before() == false){		
			return;
		}
	}
	
	var div = $('#' + enclosingDiv);
	
	//will hold all the key value pairs
	var keyValSet = [];
	
	//will traverse all bean items
	div.find('[type="text"], [type="hidden"], textarea, select').each(function(){
		elem = $(this);		
		var key = elem.attr('name');
		if(key){
			var val = elem.attr('value');
			
			//key val pair
			var keyVal = [key, val]
			keyValSet.push(keyVal)       
		}
	});
	
	var json = {};
	json["action"] = action;
	json["postBean"] = "Y";
	json["className"] = className;
	
	//will create the json post object	
	for(var i = 0; i < keyValSet.length; i++ ){
		var keyVal = keyValSet[i]
		json[keyVal[0]] = keyVal[1];
	}
	
	$.post(url, json, function(xml){
		if(callback){
			callback(xml)
		}		
	});
	
}



/*
 * I don't actually use this here, but it is provided as it might be useful and demonstrates
 * getting the TR nodes from DataTables
 */
function dg_fnGetSelected( oTableLocal ){	
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )	{
		if ( $(aTrs[i]).hasClass('row_selected') ){
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
