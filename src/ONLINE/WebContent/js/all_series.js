var URL_SERVLET = $("#context_root").val() + "all_series.jsf";
var resultsPerPage = 50;
var currentPage = 1;
var firstLetter = "";
var searchType = "";
var seriesCode = "";
var seriesName = "";
$(function(){	
	if("" != getUrlParam("firstLetter")) {
		firstLetter = getUrlParam("firstLetter");
	} 

	if("" != getUrlParam("searchType")) {
		searchType = getUrlParam("searchType");
	}
	
	if("" != getUrlParam("seriesCode")) {
		seriesCode = getUrlParam("seriesCode");
	}
	
	if("" != getUrlParam("seriesName")) {
		seriesName = getUrlParam("seriesName");
	}

	if("" != getUrlParam("page")) {
		currentPage = getUrlParam("page");
	}
	
	
	if("" != getUrlParam("results")) {
		resultsPerPage = getUrlParam("results");
		$(".results_per_page option").removeAttr("selected");
		if("50" == resultsPerPage) {
			$(".results_per_page option[value='50']").attr("selected", true);
		} else if("75" == resultsPerPage) {
			$(".results_per_page option[value='75']").attr("selected", true);
		} else if("100" == resultsPerPage) {
			$(".results_per_page option[value='100']").attr("selected", true);
		}
	} else {
		$(".results_per_page option[value='50']").attr("selected", true);
	}
	
	// results per page			
	$(".results_per_page").change(function(){			
		$("#browse_all_series_form").attr("action", URL_SERVLET + "?firstLetter=" + firstLetter 
				+ "&searchType=page&results=" + $(this).val());
		$("#browse_all_series_form").submit();
	});
	
	// goto page
	$(".go_to_page").val(currentPage);
	$(".bot_go").click(function(){		
		$("#browse_all_series_form").attr("action", URL_SERVLET + "?firstLetter=" + firstLetter 
				+ "&searchType=page&results=" + resultsPerPage + "&page=" + $("#txt_go_to_page").val());
		$("#browse_all_series_form").submit();
	});

	$("#browse_all_series_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$(".bot_go").click();
        }
	});
	
	// pager navigation
	$(".page_nav").click(function(){
		$("#browse_all_series_form").attr("action", URL_SERVLET + "?firstLetter=" + firstLetter 
				+ "&searchType=" + searchType + "&seriesCode=" + seriesCode + "&seriesName=" 
				+ seriesName + "&page=" + $(this).attr("name"));
		$("#browse_all_series_form").submit();
	});

});
			