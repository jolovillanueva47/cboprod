var lt = /<<<(?!<)/;
var gt = />>>(?!>)/;
var imageLoc = "../images/";





var classCollTitle = "bookTitle"

function printBooks(data) {
	var line = data.split(gt);
}

function findBookCallback(data, id) {	
	//$("#debug").attr("data",data);
	var output = "";
	output = output + "<li class='" + id + "' style='display:block'>";
	output = output + "<ul class='subTree'>";
	var line = data.split(gt);
	for(var i=0; i < line.length; i++){
		_line = $.trim(line[i]);
		if(_line.length > 0 ){
			var lineItem = _line.split(lt);		
			output = output + "<li><a href=\"" + contextPath + "ebook.jsf?bid=CBO" 
				+ lineItem[1] + "\">" + lineItem[1] + "</a>";
			
			output = output  + "<ul><li><a href=\"" + contextPath + "ebook.jsf?bid=CBO" 
				+ lineItem[1] + "\">" + lineItem[2] + "</a>" + "</li></ul>";	
		}
	}
	output = output + "</ul></li>";
	
	var elem = $("#" + id).parent();
	if(!elem.next().hasClass(classCollTitle)){
		if(elem.next()[0] != undefined){
			elem = elem.next();
		}
	}
	
	elem.after(output);
}


/**
	Method for finding the books under collections
**/

function findSort(){
	return $("#sortBy option:selected").val();
}


function findBooks(id, sortVal) {
	//alert(sortVal);
	var idTrim = id.substring(0, id.indexOf("_"));
	var elem = $("." + id);
	if(elem[0] == undefined){
		var path = contextPath;
		$("#" + id).children().attr("src",path + "images/circle_loading.gif")
		$.post(contextPath + "ebooks/subscriptionServlet", {ACTION : "FIND_BOOKS", COLLECTION_ID : idTrim, sortBy : sortVal},
			function (data) {
				findItemsCallback(findBookCallback, data, id);
				$("#" + id).children().attr("src",path +imageLoc + "icon_minus.jpg")
			}
		);
	}else if(elem.css("display") == "block"){
		elem.css("display", "none");
		$("#" + id).children().attr("src",imageLoc + "icon_plus.jpg")
	}else{
		elem.css("display", "block");
		$("#" + id).children().attr("src",imageLoc + "icon_minus.jpg")
	}
	return false;
}

function printCollections(data,divId, action) {
	var line = data.split(gt);
	var	elem = $("#" + divId);
	var suffix = "";
	if((action == "COLL_ORG") || (action == "INDI_ORG") || (action =="INDI") || (action == "COLL")  ){
		suffix = "_o";
	} else {
		suffix = "_d"
	}
	
	elem.html("");
	elem.css("display","block");
	var output = "";
	output = output + "<ul id='tree'>";
	for(var i=0; i < line.length; i++){
		_line = $.trim(line[i]);
		if(_line.length > 0 ){
			var lineItem = _line.split(lt);		
			if(lineItem[2] == "null"){
				output = output + "<li class='bookTitle'><a href='javascript:void(0);' id='" + lineItem[0] + suffix + i +"' onclick='findBooks(\"" + lineItem[0] + suffix + i +"\", findSort())'><img src='" + imageLoc + "icon_plus.jpg'></img></a>" + lineItem[1] + "</li>" ;
			}else{
				output = output + "<li class='bookTitle'><a href='javascript:void(0);' id='" + lineItem[0] + suffix + i +"' onclick='findBooks(\"" + lineItem[0] + suffix + i +"\", findSort())'><img src='" + imageLoc + "icon_plus.jpg'></img></a>" + lineItem[1] + "</li>";
				output = output + "<li >" + lineItem[2] + "</li>";
			}			
		}
	}

	output = output + "</ul>";
	elem.html(output);
}

function printIndividual(data, divId, action) {
	var	elem = $("#" + divId);
	elem.html("");
	elem.css("display","block");
	var output = "";
	output = output + "<ul id='tree'>";
	output = output + "<li>";
	output = output + "<ul class='subTree'>";

	var line = data.split(gt);
	for(var i=0; i < line.length; i++){
		_line = $.trim(line[i]);
		if(_line.length > 0 ){
			var lineItem = _line.split(lt);		
			output = output + "<li><a href='" + contextPath + "ebook.jsf?bid=" 
				+ lineItem[0] + "'>" + lineItem[1] + "</a>";
			output = output + "<ul><li><a href='" + contextPath + "ebook.jsf?bid=CBO" 
				+ lineItem[1] + "'>" + lineItem[2] + "</a>" + "</li></ul>"	
		}
	}
	output = output + "</ul>";
	output = output + "<li>";
	output = output + "</ul>";
	
	elem.html(output);
}

function collOrgCallback(data, divId, action) {
	printCollections(data,divId, action);
}

function collIndiCallback(data, divId, action) {
	printIndividual(data, divId, action);
}

/**
	Generic callback that seeks if empty then executes the proper callback
**/
function findItemsCallback(callback, data, divId, action) {
	var elem = $("#" + divId)
	if(data.indexOf("<<<Empty>>>") != -1) {
		elem.html("No items found.");
		elem.css("display","block");
	}else{
		callback(data,divId, action);
	}
}

function showLoaderItems(elem) {
	elem.prev().children().children().attr("src",imageLoc + "circle_loading.gif")
}

function removerLoaderItems(elem) {
	elem.prev().children().children().attr("src",imageLoc + "arrow07.gif")
}

function findItems(action, divId, orgId) {
	var elem = $("#" + divId);
	if(elem.css("display") == "block"){
		elem.css("display","none");
	}else if($.trim(elem.html()).length > 1 && elem.css("display") == "none"){
		elem.css("display","block");
	}else{
		
//		showLoaderItems(elem);	
//		//var selectedOrg is from access_details.jsp, global var
//		//contextPath is a global var
//	$.post(contextPath + "/ebooks/subscriptionServlet", {ACTION : action, ORG_ID : orgId, SELECTED_ORG : selectedOrg},
//			function (data) {
////				//apirante:0066145:
////				//if(action == "COLL_ORG"){
////				//	findItemsCallback(collOrgCallback, data, divId, action);
////				//}else if(action == "INDI_ORG"){
////				//	findItemsCallback(collIndiCallback, data, divId, action);
////				//}else if(action == "INDI_DEMO"){
////				//	findItemsCallback(collIndiCallback, data, divId, action);
////				//}else if(action == "COLL_DEMO"){
////				//	findItemsCallback(collOrgCallback, data, divId, action);
////				//}else 
////				
////				if(action == "INDI"){
////					findItemsCallback(collIndiCallback, data, divId, action);
////				}else if(action == "COLL"){
////					findItemsCallback(collOrgCallback, data, divId, action);
////				}
//				removerLoaderItems(elem);
//			}
//		);
	}
}



/**
	Main menu items
**/

var tabsMain = ["Organisational,tab1","Demo,tab2","Free,tab3"];

function disbleSiblings(element) {
	element.parent().parent().children().each(function() {
		$(this).children().removeClass("current");
	});
}

function enableMainTab(html){
	for(var i=0; i<tabsMain.length; i++ ){			
		var keyVal = tabsMain[i].split(",");		
		if(html.indexOf(keyVal[0]) != -1){			
			var tab = keyVal[1];
			$("#tabcontentcontainer").children().css("display","none");
			$("#" + tab).css("display", "block");
		}
	}
}

$(document).ready(function() {
	$("ul#tablist li a").each(function () {			
		$(this).click(function() {
			var element = $(this);
			disbleSiblings(element);
			element.attr("class","current");
			enableMainTab(element.html());
			return false;
		})
	});
});



/**
 * Org List
 */

/**
 * not used, done in server side
 */
function setSelectedOrg(){	
	var selectedOrg = $("selectedOrg").val();
	$("#orgList option").each(function(){
		if(selectedOrg == $(this).val()) {
			$(this).attr("selected", true);
			return;
		} 
	});
}

function getSelectedOrg(){
	return $("#orgList option:selected").val();	
}

var contextPath = $("#context_root").val();

function setLocation(){	
	//window.location = contextPath + "/access_details.jsf?SELECTED_ORG=" + getSelectedOrg(); 
	var sortBy = $("#sortBy option:selected").val();
	if(1 == sortBy) {
		window.location = contextPath + "ebooks/subscriptionServlet?ACTION=ALL&SELECTED_ORG=" + getSelectedOrg() + "&sortBy=1"; 
	} else {
		window.location = contextPath + "ebooks/subscriptionServlet?ACTION=ALL&SELECTED_ORG=" + getSelectedOrg() + "&sortBy=2"; 
	}	
}

$(document).ready(function() {
	$("#orgList").change(function(){
		setLocation();
	});
	
	//alert("first")
	//var elem = $("#div_05");
	//alert(elem.children().length);
	
	
	
	/*
	 * open sections by default
	 */
	$("#indivTab").click();
	$("#collectionsTab").click();
	
	
});



