var ebooksContextPath = $("#context_root").val();

$(function(){	
	$("#search_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$("#search_link").click();
        }
	});
	
	$("#search_link").click(function(){
		var valid = false;
		var term = jQuery.trim($("#search_text").val());
		if(term == "Please enter a keyword") {
			term = "";
		}
		if (term != null && term.length > 0) {
			valid = true;
		}
		
		if (valid) {
			$("#search_form").attr("action", ebooksContextPath + "search?searchType=quick&searchText=" + term);
			$("#search_form").submit();
		} else {
			alert("Invalid search.");
		}
	});	
});