$(function(){
	if ($('#tabSelect').attr('value') != null && $('#tabSelect').attr('value') == 'toc') {
		$('#tabsContainer li a').removeClass('current');
		$('div.contentWrapper').hide();
		$('#contentTocTab').addClass('current');
		$('#contentTocWrapper').show();
	}
	
	$('#tabsContainer li a').click(function(){
		if (!$(this).hasClass('current')) {
			var current = 'div.contentWrapper:eq(' + $('#tabsContainer li a').index(this) + ')';

			var tabToFadeOut = "";
			
			if($("#extractTab").hasClass("current")) {
				tabToFadeOut = "#extractWrapper";
			} else if($("#bookTocTab").hasClass("current")) {
				tabToFadeOut = "#bookTocWrapper";
			} else if($("#contentTocTab").hasClass("current")){
				tabToFadeOut = "#contentTocWrapper";
			} else {
				tabToFadeOut = "#referencesWrapper";
			}
			
			$('#tabsContainer li a').removeClass('current');
			$(this).addClass('current');
			if ($(this).attr('id') != 'referencesTab') {
				$(tabToFadeOut).fadeOut('fast', function(){
					$(current).fadeIn('fast');
				});
			} else {
				$(tabToFadeOut).fadeOut('fast', function() {
					$("#loadingImg").show();
					$('#referencesWrapper').load($('#references').attr('value'),function(){
						$('#referencesWrapper').fadeIn('fast', function() {								
							$("#loadingImg").hide();
							if ($('#referencesWrapper').html().length == 0) {
								$('#referencesWrapper').append(
									'No references available.'
								);
							}							
						});							
					 });
				});					
			}
		}
	});
	
	$(".tocItem").toggle(		
		function(){			
			var cid = $(this).attr("id");			
			$("." + cid).removeClass("hide");
			$(this).html("-");
		}, function() {			
			var cid = $(this).attr("id");			
			$("." + cid).addClass("hide");
			$(this).html("+");
		}
	);
	
	//ie7 fix, if adding hide via jsp, you can't remove the hide anymore --> it is removed but it does not work (weird)
	//the solution is to add hide via js as well hence this line
	$(".forHide").addClass("hide");
	
	$("#hithighlight").toggle(
		function(){
			$("span.searchWord").attr("class", "searchWord_off");
			$(this).html("Turn On Hit Highlighting");
			$("#hid_hithighlight").val("off");
		}, function() {
			$("span.searchWord_off").attr("class", "searchWord");
			$(this).html("Turn Off Hit Highlighting");
			$("#hid_hithighlight").val("on");
		}
	);
	
});

rightmenu.init({
	headerclass: "submenuheader", 
	contentclass: "submenu", 
	revealtype: "click", 
	mouseoverdelay: 200, 
	collapseprev: true, 
	defaultexpanded: [], 
	animatedefault: false, 
	persiststate: true, 
	toggleclass: ["", ""], 
	animatespeed: "fast", 
	oninit:function(headers, expandedindices){ 
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ 
		//do nothing
	}
})

function showPdfFromLandingPage(bookId, chapterId, viewer) {
	var hithighlight = $("#hid_hithighlight").val();
	if("on" == hithighlight) {
		showPdfHighlight(bookId, chapterId, viewer);
	} else {
		showPdf(bookId, chapterId, viewer);
	}
}

function showContentTocPdfFromLandingPage(bookId, chapterId, pageStart, pageBase, viewer) {
	var hithighlight = $("#hid_hithighlight").val();
	if("on" == hithighlight) {
		showContentTOCPdfHighlight(bookId, chapterId, pageStart, pageBase, viewer);
	} else {
		showContentTOCPdf(bookId, chapterId, pageStart, pageBase, viewer);
	}
}

function anchor(url) {
	window.open(url, "_blank");
}

function MM_openHowToCite(theURL, winName, features) {
	
	var help_win_width = 750;
	var help_win_height = 250;
	
	if (screen.availWidth > help_win_width){
		var help_win_top = parseInt((screen.availHeight/2) - (help_win_height/2));
	}else{
		help_win_top = 0;
	}
	if (screen.availWidth > help_win_width){
		var help_win_left = parseInt((screen.availWidth/2) - (help_win_width/2));
	}else{
		var help_win_left = 0;
	}

	var features = features + ",width=" + help_win_width + ",height=" + help_win_height + ",top=" + help_win_top + ",left=" + help_win_left;
	
	var h = window.open(theURL,winName,features);
	h.focus();
}
