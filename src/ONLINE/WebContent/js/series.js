var URL_SERVLET = $("#context_root").val() + "series_landing.jsf";
var resultsPerPage = 50;
var currentPage = 1;
var firstLetter = "";
var searchType = "";
var seriesCode = "";
var series = "";
var sort="";
$(function(){	
	if("" != getUrlParam("firstLetter")) {
		firstLetter = getUrlParam("firstLetter");
	} 

	if("" != getUrlParam("searchType")) {
		searchType = getUrlParam("searchType");
	} 

	if("" != getUrlParam("page")) {
		currentPage = getUrlParam("page");
	}
	
	if("" != getUrlParam("seriesCode")) {
		seriesCode = getUrlParam("seriesCode");
	}
	
	if("" != getUrlParam("seriesTitle")) {
		series = getUrlParam("seriesTitle");
	}
	
	if("" != getUrlParam("sort")) {
		sort = getUrlParam("sort");
	}
	
	if("" != getUrlParam("results")) {
		resultsPerPage = getUrlParam("results");
		$(".results_per_page option").removeAttr("selected");
		if("50" == resultsPerPage) {
			$(".results_per_page option[value='50']").attr("selected", true);
		} else if("75" == resultsPerPage) {
			$(".results_per_page option[value='75']").attr("selected", true);
		} else if("100" == resultsPerPage) {
			$(".results_per_page option[value='100']").attr("selected", true);
		}
	} else {
		$(".results_per_page option[value='50']").attr("selected", true);
	}
	
	// results per page			
	$(".results_per_page").change(function(){			
		$("#browse_series_form").attr("action", URL_SERVLET + "?seriesCode=" + seriesCode 
				+ "&seriesTitle=" + series + "&sort=" + sort + "&firstLetter=" + firstLetter 
				+ "&searchType=page&results=" + $(this).val());
		$("#browse_series_form").submit();
	});

	// sort
	$(".sort_link").each(function(){
		if(sort == $(this).attr("id")) {
			$(this).html("<strong>" + $(this).html() + "</strong>");
		}		
	});
	
	// goto page
	$(".go_to_page").val(currentPage);
	$(".bot_go").click(function(){		
		$("#browse_series_form").attr("action", URL_SERVLET + "?seriesCode=" + seriesCode 
				+ "&seriesTitle=" + series + "&sort=" + sort + "&firstLetter=" + firstLetter 
				+ "&searchType=page&results=" + resultsPerPage + "&page=" + $("#txt_go_to_page").val());
		$("#browse_series_form").submit();
	});

	$("#browse_series_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$(".bot_go").click();
        }
	});
	
	// pager navigation
	$(".page_nav").click(function(){
		$("#browse_series_form").attr("action", URL_SERVLET + "?seriesCode=" + seriesCode 
				+ "&seriesTitle=" + series + "&sort=" + sort + "&firstLetter=" + firstLetter 
				+ "&searchType=page&page=" + $(this).attr("name"));
		$("#browse_series_form").submit();
	});

});
			