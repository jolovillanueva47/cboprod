/* 
-----------------------------------------
	Template JS
	Modified by: John Ryan Acoba
    GUI Designer
-----------------------------------------
*/


$(document).ready(function() {	


  //Get all the LI from the #tabMenu UL
  $('.tabMenu > li').click(function(){
      
	 // cbo team, for Reference tab
	 if ($(this).hasClass("reference")) {
		 var bid = 'ebook'==$('#pageType').attr('value') ? $('#idHidden').attr('value') : $('#bookIdHidden').attr('value'); 
		 var loc = $('#pageType').attr('value') + '.jsf?bid=' + bid;
		 loc = ('chapter'==$('#pageType').attr('value')) ? (loc+'&cid='+$('#idHidden').attr('value')) : loc;
		 loc += '&ref=1';
		 window.location = loc;
		 return true;
	 }
	  
	  
    //remove the selected class from all LI    
    $('.tabMenu > li').removeClass('selected');
    
    //Reassign the LI
    $(this).addClass('selected');
    
    //Hide all the DIV in .boxBody
    $('.boxBody div.tabContent').slideUp('1500');
    
    //Look for the right DIV in boxBody according to the Navigation UL index, therefore, the arrangement is very important.
    $('.boxBody div.tabContent:eq(' + $('.tabMenu > li').index(this) + ')').slideDown('1500');  
	
	
	
  }).mouseover(function() {

    //Add and remove class, Personally I dont think this is the right way to do it, anyone please suggest 	
    $(this).addClass('mouseover');
    $(this).removeClass('mouseout');	
    
  }).mouseout(function() {
    
    //Add and remove class
    $(this).addClass('mouseout');
    $(this).removeClass('mouseover');   
    
  });

  //Mouseover with animate Effect for right menu list
  $('.boxBody .tabbing li').mouseover(function() {

    //Change background color and animate the padding
    $(this).css('backgroundColor','#02668e');
    $(this).children().animate({paddingLeft:"20px"}, {queue:false, duration:300});
  }).mouseout(function() {
    
    //Change background color and animate the padding
    $(this).css('backgroundColor','');
    $(this).children().animate({paddingLeft:"0"}, {queue:false, duration:300});
  });  
	
  //Mouseover effect for right menu list.
  //$('.boxBody li').click(function(){
  //  window.location = $(this).find("a").attr("href");
  //}).mouseover(function() {
  //  $(this).css('backgroundColor','#fff');
  //}).mouseout(function() {
  //  $(this).css('backgroundColor','');
  //});  	
  
  //hide reference image
  hideImage();	
});

// cbo team
var tabPrevExtract;
var tabPrevNav;
var tabReset;
function hideImage(){			
	var lielement = $(".reference");	
	if(lielement.hasClass("selected")){		
		tabPrevExtract = $("div#divImageExtract").css("display");
		tabPrevNav = $("span.chapter_nav").css("display");
		$("div#divImageExtract").css("display","none");
		$("span.chapter_nav").css("display","none");		

		//onclick of tabmenu		
		tabReset = true;
		$(".tabMenu").children().children().click(function(){			
			setTimeout("resetImage()",1000);
		});
	}
}

function resetImage(){
	if(tabPrevExtract && tabReset){		
		$("div#divImageExtract").css("display",tabPrevExtract);
		$("span.chapter_nav").css("display",tabPrevNav);	
		tabReset = false;
	}
}