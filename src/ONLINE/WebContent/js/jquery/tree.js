function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = ""
}

$(document).ready(function(){
		
	// first example
	$("#navigation").treeview({
		collapsed: true,
		unique: true,
		persist: "location"
	});

	
	// second example
	$("#browser").treeview({
		animated:"normal",
		persist: "cookie"
	});

	$("#samplebutton").click(function(){
		var branches = $("<li><span class='folder'>_new</span><ul>" + 
			"<li><span class='file'>_</span></li>" + 
			"<li><span class='file'>_</span></li></ul></li>").appendTo("#browser");
		$("#browser").treeview({
			add: branches
		});
	});


	// third example
	$("#red").treeview({
		animated: "fast",
		collapsed: true
	});
	
	$("#red li.parentLink").click(function(){

		var link = $(this);
		var id = link.attr("id");
		var li = link.children("ul").children("li");
		var subjectCodes = "";
		li.each(function (i) {
			var id = $(this).attr("id");
			subjectCodes += id + ",";
		});
		var searchType = $("#red").hasClass("seriesSearch") ? "seriesSearch" : "subjectSearch";
		
		$.getJSON(searchType + '?subjectCodes=' + subjectCodes,
				function(data) {
					$.each(data, function(i, item) {
						$("#red li#"+item).addClass("flagged");
					});
					li.each(function (i) {
						if (!$(this).hasClass("flagged")) {
							$(this).remove();
						}
					});
					
					if (searchType == "seriesSearch") {
						link.children("ul").children("li:last").removeClass("expandable");
						link.children("ul").children("li:last").addClass("lastExpandable");
						link.children("ul").children("li:last").children("div").addClass("lastExpandable-hitarea");
					} else {
						link.children("ul").children("li:last").addClass("last");
					}
				}
		);

	});
	
	$("#red li.seriesLink").click(function(){

		$(this).unbind("click");
		var link = $(this).find("a");
		var subjectCode = link.attr("id");

		$.getJSON('seriesSearch?subjectCode=' + subjectCode,
				function(data){
					var append = "";

					$.each(data, function(i, item) {
						append = append + "<li><span><a href=\"series_landing.jsf?seriesCode=" + item.seriesCode + "&seriesTitle=" + item.seriesName + "\">" + item.seriesName + "</a></span></li>\n";
					});

					if (data.length == 0) {
						append = append + "<li><em>No series for this subject is available yet.</em></li>";
					}

					var bago = $(append).appendTo(link.parent().next("ul"));
					$("#red").treeview({
						add: bago
					});

				}
		);
	});

	$("#red").show();
	
	var loc =  window.location.href;
	var idx = loc.indexOf('#');
	if ( idx > -1 ) {
		window.location = loc;
		$(loc.substring(idx,loc.length)).click();
	}
	

});