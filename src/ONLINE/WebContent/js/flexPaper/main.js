    /*
     * http://www.devaldi.com/?p=212 
     * http://www.learningjquery.com/2007/08/what-is-this
     * http://www.rgagnon.com/javadetails/java-0014.html
     * http://www.java-forums.org/java-tips/3296-how-execute-external-program-through-java-program.html
     * http://forums.techarena.in/software-development/1061176.htm
     * pdf2swf.exe D:\ancientanger.pdf -o D:\ancientanger.swf -T 9 -f
     * http://www.roseindia.net/java/javascript-array/javascript-array-remove-duplicat.shtml
     * */       

			var docViewer;

            var swfVersionStr = "10.0.0";
           
            var xiSwfUrlStr = "playerProductInstall.swf";
            var swfFile = getPdf();//'../swf/ancientanger.swf'; //'../swf/testPdf.swf';//

            var ObjectList = "empty";
            
            var database_local = "empty";
            var account_id_global = '1903980';//getAccountId(); //cbo admin 1903980 // test id = 123456789
            var id_array = "empty";//may not be used
            var ANNOTATIONS_SERV = getContextPath() + "annotations";
            
            var UpdateList = "empty";
            
            
            var display_console = true;
            
            var DB_FLAG = 0 ;
            var DB_MAX = -1;
            var tb_pathToImage = getContextPath()+"images/loadingAnimation.gif";
        	imgLoader = new Image();// preload image
        	imgLoader.src = tb_pathToImage;
            
            var flashvars = { 
                  SwfFile : escape(swfFile),
				  Scale : 0.6, 
				  ZoomTransition : "easeOut",
				  ZoomTime : 0.5,
  				  ZoomInterval : 0.2,
  				  FitPageOnLoad : true,
  				  FitWidthOnLoad : false,
  				  PrintEnabled : true,
  				  FullScreenAsMaxWindow : false,
  				  ProgressiveLoading : true,
  				  
  				  PrintToolsVisible : true,
  				  ViewModeToolsVisible : true,
  				  ZoomToolsVisible : true,
  				  FullScreenVisible : true,
  				  NavToolsVisible : true,
  				  CursorToolsVisible : true,
  				  SearchToolsVisible : true,
  				  
  				  localeChain: "en_US"
				  };
				  
			 var params = {
				
			    }
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "sameDomain";
            params.allowfullscreen = "true";
            var attributes = {};
            attributes.id = "FlexPaperViewer_Annotations";
            attributes.name = "FlexPaperViewer_Annotations";
            swfobject.embedSWF(
                "../swf/FlexPaperViewer_Annotations.swf", "flashContent", 
                "100%", "750",
                
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
			swfobject.createCSS("#flashContent", "display:block;text-align:left;");

			function storeList() {
				ObjectList = new Array();
				ObjectList = getDocViewer().getMarkList();				
				manipulate();

			}
			//"700", "750", - width heidht
			//ap codeg
			function displayInConsole(name,OBJECT_){
				if(display_console==false)return;
				if(window.console && window.console.firebug){}else{return};
				console.log("-----------------------------");
				if(OBJECT_ === "empty" || OBJECT_ == null || OBJECT_ == "" || OBJECT_ == undefined){
					console.log(name);
					if(OBJECT_ != undefined){
						console.log("is empty");
					}
				}else{
					console.log(name);
					for(var a in OBJECT_){
						var val = OBJECT_[a];
						if(val === "empty" || val == null || val == "" || val == undefined){
							console.log("key="+a+"; "+ val);   //OBJECT_[a].toSource());	
						}else{
							console.log("key="+a+"; "+ val.toSource());   //OBJECT_[a].toSource());	
						}
					}
				}

				console.log("-----------------------------");
			}
			//ap codeg

			function displayImportantObjects(){
				displayInConsole("database_local",database_local);
				displayInConsole("ObjectList",ObjectList);
				displayInConsole("getDocViewer().getMarkList()",getDocViewer().getMarkList());
			}
			
			function manipulate(){
				
				displayImportantObjects();
				UpdateList = new Array();
				
				var newItems_index;
				var deleteItems_index;
				var updateItems_index;
				
				var arr_temp = new Array();
				
				var dbInsert;
				var dbDelete;
				var dbUpdate;
				
				/**get items for insertion, deletion, and update**/
				newItems_index = getNewItems_returnArrayIndex(database_local, getDocViewer().getMarkList());
				deleteItems_index = getItemsForDeletion_returnArrayIndex(getDocViewer().getMarkList(), database_local);
				updateItems_index = getItemsForUpdate_returnArrayId(getDocViewer().getMarkList(),database_local);
				
				/**display items for debugging**/
				displayInConsole("newItems_index(list of index pointing to current markings)",newItems_index);
				displayInConsole("deleteItems_index(list of index pointing to database local) - local db items that are for update/delete",deleteItems_index);
				displayInConsole("updateItems_index(list of id to be updated in the database)",updateItems_index);
								
				//displayInConsole("UpdateList",UpdateList);
				
				/**process insert, update, and delete***/
				dbInsert = doDbInsertion(newItems_index,getDocViewer().getMarkList(),account_id_global,swfFile);
				dbDelete = doDbDeletion(deleteItems_index,database_local);
				dbUpdate = doDbUpdate(UpdateList);
				
				//FOR LOADING ISSUE
				arr_temp.push(dbInsert);
				arr_temp.push(dbDelete);
				arr_temp.push(dbUpdate);
				setDbMax(arr_temp);
				displayInConsole("DB_MAX="+DB_MAX);
				
				getDocViewer().clearMarks();
				
				/**refresh items when no transaction should be made**/
				if(dbInsert==null && dbDelete==null && dbUpdate==null){
					getDocViewer().clearMarks();
					getDocViewer().addMarks(database_local);
					ObjectList = database_local;
				}
				restoreAnnotations();
			}//end manipulate
						
			/*****MAIN DB PROCESSES*******/
			/*****INSERT******/
			function doDbInsertion(array_index,array_object,account_id,pdf_path){
				if(array_index == null || array_index == undefined || array_index == "empty" || array_index == ""){
					displayInConsole(">>>no items to be inserted");
					return null;
				}
				var params = "";
				for(var x in array_index){
					var a = array_index[x];
					if(array_object[a] != null && array_object[a].note != null){					
						params = params + 
						"pageIndex:"  +array_object[a].pageIndex + 
						", height:"   +array_object[a].height + 
						", width:"    +array_object[a].width +
						", positionX:"+array_object[a].positionX  + 
						", positionY:"+array_object[a].positionY + 
						", note:"     +escape(array_object[a].note) +
						", account_id:"  + account_id_global +
						", pdf_path:"  + swfFile +
						";";
					} else if (array_object[a] != null && array_object[a].selection_info != null){
						params = params + 
						"has_selection:"     +array_object[a].has_selection + 
						", color:"           +array_object[a].color + 
						", selection_text:"  +escape(array_object[a].selection_text) + 
						", selection_info:"  +escape(array_object[a].selection_info) +
						", account_id:"  + account_id_global +
						", pdf_path:"  + swfFile +
						";";
					}	
				}
				
				var obj = new Object();
				obj.objectList = params;
				doPostToServlet(obj);
				
				
				if(database_local == undefined || database_local == null || database_local == "" || database_local == "empty"){
					ObjectList = "empty";
					database_local = "empty";
					//restoreAnnotations();
				}else{
					ObjectList = getDocViewer().getMarkList();
					database_local = ObjectList;
				}
				
				displayInConsole(">>>insertion end="+params);
				return "sucess";
			}//end insert
			/*****DELETE*******/
			function doDbDeletion(array_index,array_object){
				if(array_index == null || array_index == undefined || array_index == "empty" || array_index == ""){
					displayInConsole(">>>no items to be deleted");
					return null;
				}
				var array_ids = getIdsForDeletion(array_index,array_object);
				var params = "";
				for(var x in array_ids){
					params = params + array_ids[x] + ";";
				}
				
				var obj = new Object();
				obj.deleteItems = params;
				doPostToServlet(obj);
				
				ObjectList = getDocViewer().getMarkList();
				database_local = ObjectList;
				displayInConsole(">>>deletion end="+params);
				return "success";
			}//end delete
			/*****UPDATE*******/
			function doDbUpdate(array_index){
				if(array_index == null || array_index == undefined || array_index == "empty" || array_index == ""){
					displayInConsole(">>>no items for update");
					return;
				}
				//displayInConsole("array_index",array_index);
				var params = "";
				for(var x in array_index){
					var a = array_index[x];
					
					if (a != null && a.note != null){
						params = params + 
						"category:"  +a.category + 
						", pageIndex:"  +a.pageIndex + 
						", height:"   +a.height + 
						", width:"    +a.width +
						", positionX:"+a.positionX  + 
						", positionY:"+a.positionY + 
						", note:"     +escape(a.note) +
						", account_id:"  + account_id_global +
						", pdf_path:"  + swfFile +
						";";
					}	
				}
				
				var obj = new Object();
				obj.updateItems = params;
				doPostToServlet(obj);
				
				ObjectList = getDocViewer().getMarkList();
				database_local = ObjectList;
				displayInConsole(">>>update end="+params);
				return "success";
			}//end update
			
			/**MAIN POST PROCESS**/
			//{objectList:params}  - INSERT	//{updateItems:params} - UPDATED //{deleteItems:params} - DELETE 
			function doPostToServlet(params){
				showLoading();
				$.post(ANNOTATIONS_SERV, 
						params, 
						function(xml){
							doAfterLoading(xml);
						});
			}//end main post process
			/**POST CALLBACK**/
			function doAfterLoading(xml){
				DB_FLAG++;
				if(DB_MAX == -1){
					removeLoading();
					//restoreAnnotations();
				}else{
					if(DB_FLAG == DB_MAX){
						DB_FLAG = 0;
						removeLoading();
						//restoreAnnotations();
					}
				}
			}// end do after post process
			/**SET DB MAX**/
			function setDbMax(temp){
				var count = 0;
				for(var a in temp){
					var val = temp[a];
					if(val === "empty" || val == null || val == "" || val == undefined){
						
					}else{
						count++;
					}
				}
				if(count != 0){
					DB_MAX = count;
				}else{
					DB_MAX = -1;
				}
			}//end set db max
			function getViewerForced(){
				return getDocViewer();
			}
			
			/**MAIN GET PROCESS**/
			//params - {account_id:account_id_global, pdf_path:swfFile}
			function doGetToServlet(params){
				//showLoading();
				$.get( ANNOTATIONS_SERV,
						params, 
						function(xml){
							doAfterGet(xml);
						}
				);	
				//alert("params get="+params.toSource());
			}//end main get process
			/**GET CALLBACK**/
			function doAfterGet(xml){
				//removeLoading();
				var fullText = xml;
				fullText = removeSingleQuote(fullText);
				recreateAnnotations(fullText);
				ObjectList = new Array();
				ObjectList = getDocViewer().getMarkList();
				database_local = ObjectList;
			}//end do after get process
			
			/****UTILS*****/
			/****GET NEW ITEMS*****/
			function getNewItems_returnArrayIndex(ORIG_,CURRENT_){			
				var results = new Array();
				var index = -1;	
				if(database_local == undefined || database_local == null || database_local == "" || database_local == "empty"){
					
					for(var a in CURRENT_){
						index++
						results.push(index);
					}
					
					return results;
				}
				
				for(var a in CURRENT_){
					var isNotSame = true;
					index++;
					if(ORIG_ === "empty"){
						var f = CURRENT_[a];
						if(f.selection_text && f.selection_text != ''){
							results.push(index);
						}else if(f.category){
							results.push(index);
						}
					}else{
						for(var b in ORIG_){
							
							if(objectsAreSame(CURRENT_[a],ORIG_[b])){//CURRENT_[a].toSource() == ORIG_[b].toSource()){
								isNotSame = false;
								break;
							}
						}
						if(isNotSame){
							//don't insert existiong objecst
							//don't insert when exist in db
							var f = CURRENT_[a];
							//notes
							if(f.category){
								if(!isExistInDb(f.category)){
									results.push(index);
								}else{
									//selection is not updatable so no need for this to selection
									UpdateList.push(CURRENT_[a]);
								}
							}else{
								//selection highlight
								//don't insert when selection text is empty
								if(f.selection_text != ''){
									results.push(index);
								}
							}
						}
					}
				}
				return results;
			}//end get new items
			
			
			function getItemFromDbLocal(category,database_local){
				var results = "empty";
				for(var x in database_local){
					var f = database_local[x];
					if(f.category){
						if(f.category == category){
							results = f;
						}
					}
				}
				return results;
			}
			
			function getItemsForUpdate_returnArrayId(current_markings, database_local){
				if(database_local == undefined || database_local == null || database_local == "" || database_local == "empty"){
					return null;
				}
				var results = new Array();
				for(var x in current_markings){
					var f = current_markings[x];
					if(f.category){
						var t = getItemFromDbLocal(f.category,database_local);
						if(t!="empty"){
							if(objectsAreSame(f,t)){
							
							}else{
								results.push(t.category);
							}	
						}		
					}
				}
				return results;
			}
			
			function objectsAreSame(x, y) {
				   var objectsAreSame = true;
				   for(var propertyName in x) {
				      if(x[propertyName] !== y[propertyName]) {
				         objectsAreSame = false;
				         break;
				      }
				   }
				   return objectsAreSame;
			}

			function getIdsForDeletion(arrayIndex,db_local){
				var results = new Array();
				for(var x in arrayIndex){
					var index = arrayIndex[x];
					var item_fordelete = db_local[index];
					var id = id_array[index];
					results.push(id);
				}
				return results;
			}//END getIdsForDeletion

			//current markings against database_local
			function getItemsForDeletion_returnArrayIndex(ORIG_,CURRENT_){
				if(database_local == undefined || database_local == null || database_local == "" || database_local == "empty"){
					return null;
				}
				var results = new Array();
				var index = -1;			
				for(var a in CURRENT_){
					var isNotSame = true;
					index++;
					for(var b in ORIG_){
						if(objectsAreSame(CURRENT_[a],ORIG_[b])){//CURRENT_[a].toSource() == ORIG_[b].toSource()){
							isNotSame = false;
							break;
						}
					}
					if(isNotSame){
						var f = CURRENT_[a];
						if(f.category){
							
							if(!isExistInDb(f.category)){
								results.push(index);
							}else if(!isExistInUpdateList(f.category)){
								results.push(index);
							}
						}else{
							results.push(index);
						}
					}
				}
				return results;
			}//END getItemsForDeletion_returnArrayIndex

			
			function isExistInDb(cat){
				var results = true;
				var t = getItemFromDbLocal(cat,database_local);
				if(t=="empty"){
					return false;
				}
				return results;
			}
			
			function isExistInUpdateList(cat){
				var results = true;
				var t = getItemFromDbLocal(cat,UpdateList);
				if(t=="empty"){
					return false;
				}
				return results;
			}

			function showLoading(){
				//$("#loading").show();
				//$("#main").hide();			
			}
			function removeLoading(){
				//$("#loading").hide();
				//$("#main").show();
			}			
			
			function restoreAnnotations(){	
				if (ObjectList == "empty" || ObjectList == null){
					var obj = new Object();
					obj.account_id = account_id_global;
					obj.pdf_path = swfFile;
					doGetToServlet(obj);
				}
				else {
					getDocViewer().clearMarks();
					getDocViewer().addMarks(ObjectList);
					
				}
			}
			
			function recreateAnnotations(fullText) {
				//alert("fulltext="+fullText);
				var AnnotationList = new Array();
				AnnotationList = fullText.split(";");
				id_array = new Array();
				for(var i in AnnotationList){
					var objectText = AnnotationList[i];
					
					var m_tempId = null ;
					if(objectText.indexOf("id") > -1){
						var startIndex = objectText.indexOf(":", objectText.indexOf("id")) + 1;
						var endIndex = objectText.indexOf(",", startIndex);
						var temp_id = objectText.substring(startIndex, endIndex);
						m_tempId = temp_id;
						id_array.push(temp_id);
					}
					
					if(objectText.indexOf("has_selection") != -1){
						
						var newSelection = new Object();
						
						var startIndex = objectText.indexOf(":", objectText.indexOf("has_selection")) + 1;
						var endIndex = objectText.indexOf(",", startIndex);
						newSelection.has_selection = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("color")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newSelection.color = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("selection_text")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newSelection.selection_text = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("selection_info")) + 1;
						newSelection.selection_info = unescape(objectText.substring(startIndex));
						
						getDocViewer().addMark(newSelection);
						
					} else if (objectText.indexOf("note") != -1){
						var newNote = new Object();
						
						var startIndex = objectText.indexOf(":", objectText.indexOf("pageIndex")) + 1;
						var endIndex = objectText.indexOf(",", startIndex);
						newNote.pageIndex = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("height")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newNote.height = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("width")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newNote.width = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("positionX")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newNote.positionX = objectText.substring(startIndex, endIndex);
						
						startIndex = objectText.indexOf(":", objectText.indexOf("positionY")) + 1;
						endIndex = objectText.indexOf(",", startIndex);
						newNote.positionY = unescape(objectText.substring(startIndex, endIndex));
						
						startIndex = objectText.indexOf(":", objectText.indexOf("note")) + 1;
						newNote.note = unescape(objectText.substring(startIndex));	
						//added ap - for test only 
						newNote.category = m_tempId;

						getDocViewer().addMark(newNote);

					} else {
						//do nothing
						//alert("error @_@");
					}
				}
			}
			
			function test(){
				var s = "has_selection:true, color:#fffc15, selection_text:null, selection_info:1%3B0%3B47";
				alert(s.indexOf("selection_info") );
			}
			
			function removeSingleQuote(fullText){
				return fullText.replace("'", "");
			}

			
			function clearAll(){
				getDocViewer().clearMarks();
				//ObjectList = "empty";
				//database_local = "empty";
			}
			
			
			
			function getDocViewer(){
			    if(docViewer)
			        return docViewer;
			    else if(window.FlexPaperViewer_Annotations)
			        return window.FlexPaperViewer_Annotations;
			    else if(document.FlexPaperViewer_Annotations)
				return document.FlexPaperViewer_Annotations;
			    else 
				return null;
			}

			function appendLog(val){
				$("#txt_eventlog").val(val + '\n' + $("#txt_eventlog").val());
			}

			function onMarkClicked(mark){
				appendLog('onMarkClicked:' + mark.selection_info);
			}


			function onExternalLinkClicked(link){
				$("#txt_eventlog").val('onExternalLinkClicked' + '\n' + $("#txt_eventlog").val());	
			}


			function onProgress(loadedBytes,totalBytes){
				$("#txt_progress").val('onProgress:' + loadedBytes + '/' + totalBytes + '\n');	
			}


			function onDocumentLoading(){
				$("#txt_eventlog").val('onDocumentLoading' + '\n' + $("#txt_eventlog").val());
				//restoreAnnotations();
			}


			function onCurrentPageChanged(pagenum){
				$("#txt_eventlog").val('onCurrentPageChanged:' + pagenum + '\n' + $("#txt_eventlog").val());
			}


			function onDocumentLoaded(totalPages){
				$("#txt_eventlog").val('onDocumentLoaded:' + totalPages + '\n' + $("#txt_eventlog").val());
				//restoreAnnotations();
				//$("#btnload").click();
			}


			function onDocumentLoadedError(errMessage){
				$("#txt_eventlog").val('onDocumentLoadedError:' + errMessage + '\n' + $("#txt_eventlog").val());
			}
			