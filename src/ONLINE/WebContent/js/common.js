function showHelp(contextPath, helpType, pageId, pageName, langCode) {	
	var url = "";
	if(helpType == "root") {
		url = "popups/help_index.jsf";
	} else {
		url = "popups/help.jsf?pageId=" + pageId + "&pageName=" + pageName + "&languageCode=" +langCode + "&fromTop=Y";
	}
	MM_openWindow(contextPath + url, 'popup','status=yes,scrollbars=yes,width=750,height=700');
}

function MM_logout() {	
   	$.get("logout?"+Math.random(), null, function(){location='home.jsf?lo=y'});   	  
}

function logout() { 	
//	if (window.notSaved === true) {
//		isOpen();
//		notSaved = false;
//	}
//	var locationUrl = window.parent.location.href; 
//	
//	if(/#$/.test(locationUrl)) {
//		locationUrl = locationUrl.replace("#", "");
//	}
//	
//	var locationUrlArr = locationUrl.split("?");
//	
//	if(locationUrlArr.length == 1) {
//		locationUrl = locationUrlArr[0] + "?lo=y";
//	} else {
//		locationUrl = locationUrlArr[0] + "?lo=y&" + locationUrlArr[1];
//	}	
//	
//	if(locationUrl.indexOf("home.jsf") > -1 || locationUrl == ebooksContextPath) {
//			window.parent.location="home.jsf?lo=y";
//	} else {			
//		window.parent.location = locationUrl;
//	}
		//if(locationUrl.indexOf("subject_tree.jsf") > -1 
	    //		|| locationUrl.indexOf("series_tree.jsf") > -1) {
		//	window.parent.location.reload();
		//}
	window.parent.location = $("#context_root").val() + "home.jsf?lo=y";
	return true;
}


function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function openWindow(url) { //v2.0		
	var h = window.open(url,'','location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes,scrollbars=yes,width=700,height=440');
	h.focus();
}

function MM_windowOpener(url){
	if(window.opener != null){
		window.opener.location=url;
		window.close();
	}else {
		window.location=url;
	}
}

function MM_openWindow(theURL, winName, features) {

	var help_win_width = 850;
	var help_win_height = 800;
	
	if (screen.availWidth > help_win_width){
		var help_win_top = parseInt((screen.availHeight/2) - (help_win_height/2));
	}else{
		help_win_top = 0;
	}
	if (screen.availWidth > help_win_width){
		var help_win_left = parseInt((screen.availWidth/2) - (help_win_width/2));
	} else {
		var help_win_left = 0;
	}

	var features = features + ",width=" + help_win_width + ",height=" + help_win_height + ",top=" + help_win_top + ",left=" + help_win_left;
	var h = window.open(theURL,winName,features);
	h.focus();
	
	return h;
}

function MM_openWindow_socialNetworking(theURL,winName, features, socialnetUrl) {
	var finUrl = socialnetUrl + "?u=" + escape(theURL);
	MM_openWindow(finUrl,winName, features) 
}

function openPdf_flexPaper(bookId, contentId){
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	var serv = "popups/swf_viewer.jsf";
	MM_openWindow(serv, winName,"status=yes,scrollbars=yes,width=800,height=600");
	
	googleTrackPDFView(serv);
}

function showPdf(bookId, contentId, viewer) {
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	var contextRoot = $("#context_root").val();
	var url;
	if("goodreader" == viewer) {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&viewer=goodreader";
	} else {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId;
	}
	
	var features = "location=0,titlebar=0,toolbar=0,menubar=0,status=1,scrollbars=1,width=800,height=600";	
	MM_openWindow(url, winName, features);	
	
	googleTrackPDFView(url);
}

function showPdfHighlight(bookId, contentId, viewer) {
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	var contextRoot = $("#context_root").val();
	var url;
	if("goodreader" == viewer) {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&viewer=goodreader&hithighlight=on";
	} else {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&hithighlight=on";
	}
	
	var features = "location=0,titlebar=0,toolbar=0,menubar=0,status=1,scrollbars=1,width=800,height=600";		
	MM_openWindow(url, winName, features);
	
	googleTrackPDFView(url);
}

function showContentTOCPdf(bookId, contentId, startPage, basePage, viewer) {
	basePage = basePage.replace(/pp. /g, "");
	basePage = basePage.replace(/-\d+/g, "");
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	startPage = (startPage - basePage) + 1;
	
	var contextRoot = $("#context_root").val();
	var url;
	if("goodreader" == viewer) {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&startPage=" + startPage + "&viewer=goodreader";
	} else {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&startPage=" + startPage;
	}
	
	var features = "location=0,titlebar=0,toolbar=0,menubar=0,status=1,scrollbars=1,width=800,height=600";			
	
	MM_openWindow(url, winName, features);
	
	googleTrackPDFView(url);
}

function showContentTOCPdfHighlight(bookId, contentId, startPage, basePage) {
	basePage = basePage.replace(/pp. /g, "");
	basePage = basePage.replace(/-\d+/g, "");
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	startPage = (startPage - basePage) + 1;
	
	var contextRoot = $("#context_root").val();
	var url;
	if("goodreader" == viewer) {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&startPage=" + startPage + "&viewer=goodreader&hithighlight=on";
	} else {
		url = contextRoot + "popups/pdf_viewer.jsf?cid=" + contentId + "&startPage=" + startPage + "&hithighlight=on";
	}
	
	var features = "location=0,titlebar=0,toolbar=0,menubar=0,status=1,scrollbars=1,width=800,height=600";	
	MM_openWindow(url, winName, features);
	
	googleTrackPDFView(url);
}

function googleTrackPDFView(url) {
	try {
		var pageTracker = _gat._getTracker("UA-31379-39");
		pageTracker._trackPageview(url);
	} catch(err) {}
}

function hasAdobeAcrobat() {
	
	var isInstalled = false;  
	var version = null;  
	
	if (window.ActiveXObject) {  
		var control = null;  
	 	try {  
	    	// AcroPDF.PDF is used by version 7 and later  
			control = new ActiveXObject('AcroPDF.PDF');  
	 	} catch (e) {  
	    	// Do nothing  
	 	}  
	 	if (!control) {  
	    	try {  
	        // PDF.PdfCtrl is used by version 6 and earlier  
	        	control = new ActiveXObject('PDF.PdfCtrl');  
	     	} catch (e) {  
	         	  
	    	}  
	 	}  
	    if (control) {  
	    	isInstalled = true;  
	        version = control.GetVersions().split(',');  
	        version = version[0].split('=');  
	        version = parseFloat(version[1]);  
	    }  
	} else {  
		// Check navigator.plugins for "Adobe Acrobat" or 
		// "Adobe PDF Plug-In for Firefox and Netscape"      	
		if(navigator.plugins["Adobe PDF Plug-In for Firefox and Netscape"]) { // Adobe Acrobat 8
			isInstalled = true;
		} else if(navigator.plugins["Adobe Acrobat"]) {
			isInstalled = true;
		}
	}  
	
	return isInstalled;
}

//function showPdf(eisbn, pdf, bookTitle, publisher) {
//	MM_openWindow("content/" + eisbn + "/" + pdf,"popup","status=yes,scrollbars=yes,width=750,height=700");
	//if(hasAdobeAcrobat()) {
	//	openWindow('show_pdf?pdfFileUrl=content/' + eisbn 
	//			+ '/' + pdf + '&eisbn=' + eisbn + '&title=' + bookTitle + '&publisher=' + publisher);
	//} else {
	//	alert("Please get Adobe Acrobat Reader at http://get.adobe.com/reader/");
	//}
//}

function getUrlParam(name) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if(results == null) {
		return "";
	} else {
		return results[1];
	}
}

//--- IP LOGO START ---
$(document).ready(function(){	
	$("#orgLogoImg").load(function(){	
		var width = $("#orgLogoImg").width();
		var height = $("#orgLogoImg").height();	

		if($("#orgLogoId").val() == "") {
			width = 0;
			height = 0;
		}
		
		$("#orgLogoImg").attr("style", "width: " + width + "px; height: " + height + "px;");
	});
	//$("#orgLogoImg").attr("src", $("#orgLogoImg").attr("src"));
});	
//--- IP LOGO END ---

function loginFunction(){
	$("#loginLink").click();
}

function callParentLoginFunction(){
	window.opener.loginFunction();
	window.close();
}

function clearDefault(el) {
	if (el.defaultValue==el.value) el.value = ""
}

//--- LOGIN START ---
$(document).ready(function(){	
	$('#logintoggle').click(function () {
		$('#top-login .loginbox').slideToggle(300, function(){
			$('#logintoggle').toggleClass('up');
		});
	});
	
	$('#top-login .box').focus(function () {
		if($(this).val() == 'Login...') $(this).val('');
	});
	$('#top-login .box').blur(function () {
		if($(this).val() == '') $(this).val('Login...');
	});
});
//--- LOGIN END ---

//--- LOGIN PANEL START ---
$(document).ready(function() {
	$("div.panel_button").click(function(){
		$("div#panel").animate({
			height: "128px"
		})
		.animate({
			height: "128px"
		}, "fast");
		$("div.panel_button").toggle();
	
	});	
	
   $("div#hide_button").click(function(){
		$("div#panel").animate({
			height: "0px"
		}, "fast");
		
	
   });	
	
});
//--- LOGIN PANEL END ---

//--- LOADER START ---
$(document).ready(function() {
	$("#loading").hide();
	$("#main").show();
});
//--- LOADER END ---


//--- CJO LOGIN START ---
function getUrl(cjoContextPath, id){
	var orgAllBodyIds = $("#orgAllBodyIds").val();
	
	//cjoUrl is a global var set on the jsp that imports this script
	if(orgAllBodyIds=="null"){
		return cjoContextPath + "accmanagement?id=" + id + "&RETURN_IMAGE=Y";
	}
	cjoContextPath =  cjoContextPath + "accmanagement?id=" + id + "&RETURN_IMAGE=Y&ORG_ID_SET=" + orgAllBodyIds;
	return cjoContextPath + "&RAND=" + Math.random(); 
}

function generateImage(cjoContextPath, response){	
	return "<img src='' style='display:block; position: absolute; left: -999999px' id='cjoLogin' />"
}


function CJOLoginLink(cjoContextPath, link, elemId){	
	var contextRoot = $("#context_root").val();
	var orgAllBodyIds = $("#orgAllBodyIds").val();
	$.get(contextRoot + "loginCJO?RAND=" + Math.random(), function(response){		
		if(response.indexOf("LOGGED_EBOOKS") > -1){
			$("#" + elemId).after(generateImage(cjoContextPath, response));			
			$("#cjoLogin").load(function(){
				$.get(contextRoot + "loginCJO", {RAND:Math.random(),UPDATE_CJO_LOGGED:"Y"}, function(response){					
					location=cjoContextPath + link;
				});												
			});		
			var id = response.split("-->")[1];	
			var url = getUrl(cjoContextPath, id);	
			$("#cjoLogin").attr("src", url);
		}else{
			//when accmanagement
			if(link.indexOf("accmanagement") > -1){
				if(orgAllBodyIds=="null"){
					location= cjoContextPath + link;
				}else{
					location= cjoContextPath + link + "&ORG_ID_SET=" + orgAllBodyIds;
				}				
			}else{
				location= cjoContextPath + link;
			}
		}
	});
	return false;	
}
//--- CJO LOGIN END ---


// IP LOGO START needed for https login
//this function modifies the url so as to pass as paramter the parent iframe location
//since window.parent.location is disallowed when http: (parent) and https: (child) are used
//also instead of just normally appending, i placed this as the first attribute because the jquery tool used only gets the first param
function makeHttpsLink(link){
	var loginLink = $(link);
	var href = loginLink.attr("href");	
	if(href != undefined){
		var arrayUrl = href.split("?");
		var append = "&LOCATION10556=" + encodeURIComponent(window.location) + "&";	
		loginLink.attr("href", arrayUrl[0] + "?" + append + arrayUrl[1]);
	}
}
$(document).ready(function() {	
	makeHttpsLink("#loginLink");
	makeHttpsLink(".loginLink");
});



// IP LOGO END

//for ie6 ap test
var isIe6 = $.browser.msie && jQuery.browser.version.substr(0,3)=="6.0" ? true : false;
$(document).ready(function() {	
	if(isIe6){
		$("a").click(function(e){
			var ref = jQuery.trim($(this).attr('href'));
			var onclik = $(this).attr('onclick');
			if(ref == 'javascript:void(0);' || ref == '' || onclik != null ){
				e.preventDefault();
			}
		});	
	}
});
//end for ie6 ap test