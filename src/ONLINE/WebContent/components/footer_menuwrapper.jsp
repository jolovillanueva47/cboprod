<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%
	java.util.Date date = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
	String currentYear = sdf.format(date);
%>
<!-- Footermenu Start -->
<div class="footer_menuwrapper">
	<ul>
		<li><a href="http://www.cambridge.org/online/" target="oth">Other Online Products from Cambridge</a>&nbsp;|</li>
		<li><a href="http://journals.cambridge.org/" target="cjo">Cambridge Journals Online</a>&nbsp;|</li>
		<li><a href="${pageContext.request.contextPath}/clc/home.jsf" >Cambridge Library Collection</a>&nbsp;|</li>
		<li><a href="http://cambridge.org/uk/browse/default.asp?subjectid=1000001" target="apb">Academic and Professional Books</a>&nbsp;|</li> 
		<li><a href="http://www.cambridge.org/learning/" target="clo">Cambridge Learning</a>&nbsp;|</li>                
		<li><a href="http://www.cambridge.org/uk/bibles/" target="bib">Bibles</a></li>
	</ul>
	<ul>
		<li>&copy; Cambridge University Press <%=currentYear %>.</li>
		<li><a href="${pageContext.request.contextPath}/about.jsf">About Cambridge Books Online</a>&nbsp;|</li>  
		<li><a href="${pageContext.request.contextPath}/contact.jsf">Contact Us</a>&nbsp;|</li>  
		<li><a href="${pageContext.request.contextPath}/accessibility.jsf">Accessibility </a>&nbsp;|</li>  
		<li><a href="${pageContext.request.contextPath}/terms_of_use.jsf">Terms of Use</a>&nbsp;|</li>
		<li><a href="${pageContext.request.contextPath}/privacy_policy.jsf">Privacy Policy</a>&nbsp;|</li>
		<li><a href="${pageContext.request.contextPath}/rights_and_permissions.jsf">Rights &amp; Permissions</a>&nbsp;|</li>
		<li><a href="${pageContext.request.contextPath}/sitemap.jsf">Site Map</a></li>
	</ul>
</div>
<!-- Footermenu End -->