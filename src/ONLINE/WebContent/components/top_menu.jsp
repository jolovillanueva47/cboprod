<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@page import="org.cambridge.ebooks.online.organisation.OrganisationWorker"%>
<%@page import="org.cambridge.ebooks.online.user.UserWorker"%>
<%@page import="org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap"%>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRoot" option="context_root" />
<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

<!-- Topmenu Start -->
<div class="nav">
	<input type="hidden" value="${orgConLinkedMap.userLogin.userType}" name="hiddenUserType"/>
	<input type="hidden" value="<%=OrgConLinkedMap.getAllBodyIds(request.getSession())%>" id="orgAllBodyIds"/>
	<ul>	
    	<li class="topmenu_home rounded"><a href="${pageContext.request.contextPath}/home.jsf" title="Home"><span>Home</span></a></li>
		<li class="topmenu_about"><a href="${pageContext.request.contextPath}/about.jsf" title="About Us"><span>About Us</span></a></li>
        <li><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" title="FAQ"><span>FAQ</span></a></li>
        <li><a href="#" onclick="showHelp('${pageContext.request.contextPath}/', '${sessionScope.helpType}', '${sessionScope.pageId}', '${sessionScope.pageName}','1');return false;" title="Help"><span>Help</span></a></li>
       	<%--
        <li>
            <c:set var="pageId" value='<%= System.getProperty("librarian.pageId") %>' />                	
            <c:url value="accmanagement" var="forLibrariansUrl">
            	<c:param name="topage" value="stream" />
               	<c:param name="pageId" value="${pageScope.pageId}" />                		
            </c:url>
            <a href="#" onclick="return CJOLoginLink('${contextRootCjo}', '<c:out value="${forLibrariansUrl}" escapeXml="true" />','librarianLink')" id="librarianLink" title="For Librarians"><span>For Librarians</span></a>
        </li>   
         --%>       
        <li class="topmenu_librarian"><a href="${pageContext.request.contextPath}/orgadmin/for_librarians.jsf" title="For Librarians"><span>For Librarians</span></a></li>      
        <c:if test="${null ne orgConLinkedMap.userLogin.userType}" >    
	    	<c:choose>		        		
	        	<c:when test="${'AO' eq orgConLinkedMap.userLogin.userType}" >
	        		<%-- 
	        		<li><a href="#" onclick="return CJOLoginLink('${contextRootCjo}', 'accmanagement?topage=configureIPDomain','adminLink')" id="adminLink" title="Account Administrator"><span>Account Administrator</span></a></li>
	        		--%>
	        		<li class="topmenu_admin"><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ao&page=configIp" id="adminLink" title="Account Administrator"><span>Account Administrator</span></a></li>
	           	</c:when>
	           	<c:when test="${'AC' eq orgConLinkedMap.userLogin.userType}" >
	           		<li class="topmenu_admin"><a href="${pageContext.request.contextPath}/orgadmin/account_administrator.jsf?type=ac&page=configIpCons" id="adminLink" title="Account Administrator"><span>Account Administrator</span></a></li>
	           		<%--
	           		<li><a href="#" onclick="return CJOLoginLink('${contextRootCjo}', 'accmanagement?topage=configureIPConsortiaDispatcher','adminLink')" id="adminLink" title="Account Administrator"><span>Account Administrator</span></a></li>
	           		 --%>
	           	</c:when>
	        </c:choose>
		</c:if>
	</ul>
</div>
     
<script type="text/javascript">
	function loginFunction(){
		$("#loginLink").click();
	}
	
	function callParentLoginFunction(){
		window.opener.loginFunction();
		window.close();
	}
</script>
               
<script type="text/javascript">
	var cjoUrl = "${contextRootCjo}";
    var contextPath = "${contextRoot}";
</script>      
<!-- Topmenu End -->
