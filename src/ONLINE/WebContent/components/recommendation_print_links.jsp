<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>			
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:set var="hasAccess" value="${bookBean.bookContentItem.isFreeContent or bookBean.hasAccess}" scope="page" />
<c:url value='/popups/recommendToLibrarian.jsf' var="urlRecommend">
	<c:param name="Title" value="${bookBean.bookMetaData.title}" />
	<c:param name="Copyright" value="${bookBean.bookMetaData.copyrightStatement}" />
	<c:param name="PrintpubDate" value="${bookBean.bookMetaData.printDate}" />
	<c:param name="OnlinepubDate" value="${bookBean.bookMetaData.onlineDate}" />
	<c:param name="OnlineIsbn" value="${bookBean.bookMetaData.onlineIsbn}" />
	<c:param name="HardbackIsbn" value="${bookBean.bookMetaData.hardbackIsbn}" />
	<c:param name="Doi" value="${bookBean.bookMetaData.doi}" />
	<c:param name="bid" value="${bookBean.bookMetaData.id}" />
	<c:param name="cid" value="${bookBean.bookContentItem.id}" />
	<c:param name="contentType" value="${bookBean.contentType}" />
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="620" />
	<c:param name="height" value="460" />
</c:url>

<% String userAgent = request.getHeader("User-Agent"); %>
<!-- Recommendation/Print Links Start -->
<div class="rightmenu_recommend">
	<ul>		
		<c:if test="${not hasAccess}">
			<li>
				<!-- See book_info_links.jsp for session setup of authorList -->					
			 	<c:choose>
				 	<c:when test="${supplemental.salesModuleFlag != 'N'}">	
						<span class="icons_img recommend">&nbsp;</span><a href="<c:out value='${urlRecommend}' escapeXml='true' />" class="thickbox">Recommend This to a Librarian</a>	
				 	</c:when>
			 	</c:choose>
			</li>            
		</c:if>		
		<li><span class="icons_img print">&nbsp;</span><a href="#" onclick="javascript:window.print(); return false;">Print This Page</a></li>
		<c:if test="${not empty sessionScope.hithighlight}">
			<li><span class="icons_img highlight_text">&nbsp;</span><a id="hithighlight" href="javascript:void(0);">Turn Off Hit Highlighting</a></li>
		</c:if>
		<c:if test='<%=userAgent.indexOf("iPad") != -1 %>'>
			<li><span class="icons_img choose_reader">&nbsp;</span><a href="${pageContext.request.contextPath}/ipad_Reader.jsf?TB_iframe=true&amp;width=620&amp;height=450>" class="thickbox" id="readPDF">Choose your PDF Reader</a></li>
		</c:if>
	</ul>
</div>
<!-- Recommendation/Print Links End -->