<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<% 
	String userAgent = request.getHeader("User-Agent");
	session.setAttribute("prevPath",request.getRequestURL());
	session.setAttribute("sType",request.getParameter("searchType"));
	session.setAttribute("sText",request.getParameter("searchText"));
%>
<c:set var="enablePdf" value="false" />
<p>
	<strong>pp ${param.pageRange}</strong> |
	<c:choose>
		<c:when test="${param.access}">
			<c:set var="enablePdf" value="true" />
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${enablePdf}">                                                    							                                                        					                                                    				
			<c:choose>
				<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'> 
					<c:choose>
						<c:when test="${bookBean.cookieValue eq 2}"> 
							<img src="${pageContext.request.contextPath}/images/icon_pdf.gif" alt="" width="21" height="14" /><a href="javascript:void(0);" onclick="showPdfHighlight('${param.bookId}', '${param.chapterId}', 'goodreader','<%=session.getAttribute("refList")%>');">Read PDF</a>							
						</c:when>
						<c:otherwise>
							<img src="${pageContext.request.contextPath}/images/icon_pdf.gif" alt="" width="21" height="14" /><a href="javascript:void(0);" onclick="showPdfHighlight('${param.bookId}', '${param.chapterId}', '','<%=session.getAttribute("refList")%>');">Read PDF</a>							
						</c:otherwise> 
					</c:choose>
				</c:when>
				<c:otherwise>
					<img src="${pageContext.request.contextPath}/images/icon_pdf.gif" alt="" width="21" height="14" /><a href="javascript:void(0);" onclick="showPdfHighlight('${param.bookId}', '${param.chapterId}', '','<%=session.getAttribute("refList")%>');">Read PDF</a>					
				</c:otherwise>
			</c:choose>	
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${not empty userInfo and not empty userInfo.username}">
					<c:choose>
						<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'> 
							<c:choose>
								<c:when test="${bookBean.cookieValue eq 2}"> 
									<img src="${pageContext.request.contextPath}/images/icon_pdf_goodreader_disabled.gif" alt="" width="21" height="14" />Read PDF
								</c:when>
								<c:otherwise>
									<img src="${pageContext.request.contextPath}/images/icon_pdf_disabled.gif" alt="" width="21" height="14" />Read PDF
								</c:otherwise> 
							</c:choose>
						</c:when>
						<c:otherwise>
							<img src="${pageContext.request.contextPath}/images/icon_pdf_disabled.gif" alt="" width="21" height="14" />Read PDF							
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'> 
							<c:choose>
								<c:when test="${bookBean.cookieValue eq 2}"> 
									<img height="14" width="21" alt="" src="${pageContext.request.contextPath}/images/icon_pdf_goodreader_disabled.gif"><a class="thickbox link01 loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a>
								</c:when>
								<c:otherwise>
									<img src="${pageContext.request.contextPath}/images/icon_pdf_disabled.gif" alt="" width="21" height="14" /><a class="thickbox link01 loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a>
								</c:otherwise> 
							</c:choose>
						</c:when>
						<c:otherwise>
							<img src="${pageContext.request.contextPath}/images/icon_pdf_disabled.gif" alt="" width="21" height="14" /><a class="thickbox link01 loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a>																				
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</p>