<form id="advance_search_form" action="" method="post">
	<p><b>Please input search parameters in search fields.</b></p>
	<p><b>*</b> Empty search fields will not be included in the search.</p>
	<table id="searchFilterTable">
		<tr>
			<td class="advanced_search_checkbox">
				<ul>
					<li><input name="sfilter" type="checkbox" value="book" />&nbsp;<label>Cambridge Books Online</label></li>
					<li><input name="sfilter" type="checkbox" value="clc" />&nbsp;<label>Cambridge Library Collection</label></li>
					<li><input name="sfilter" type="checkbox" value="journal" />&nbsp;<label>Cambridge Journals Online</label></li>
				</ul>
			</td>
		</tr>
	</table>
	<table id="advance_query_option" class="queryOpt">	
	<tr><td></td></tr>				
	</table>
	<div class="form_button">
		<ul>                 
			<li id="searchButton"><span><a href="javascript:void(0);" class="bot_searching" title="Search">Search</a></span></li>
			<li id="clearAllButton"><span><a href="javascript:void(0);" class="bot_clear" title="Clear All">Clear All</a></span></li>
			<li id="removeButton"><span><a href="javascript:void(0);" class="bot_fewerchoices" title="Fewer Choices">Fewer Choices</a></span></li>
			<li id="addButton"><span><a href="javascript:void(0);" class="bot_morechoices" title="More Choices">More Choices</a></span></li>
		</ul>
	</div>		             
</form> 