<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<div id="content_wrapper03">
	<c:choose>
		<c:when test="${seriesBean.resultsFound > 1}">
			There are a total of 
			<b><c:out value="${seriesBean.resultsFound}" /></b> series.
		</c:when>
		<c:when test="${seriesBean.resultsFound == 1}">
			There is only 
			<b><c:out value="${seriesBean.resultsFound}" /></b> series.
		</c:when>
		<c:otherwise>
			There are no series available.					
		</c:otherwise>
	</c:choose>			
</div>
