<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" />

<input type="hidden" id="context_root" value="${pageContext.request.contextPath}/" />

<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery/thickbox.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/common.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/search_common.js" type="text/javascript"></script>

<%
	Object logoutAthens = session.getAttribute("logoutAthens");
	Object logoutShibb = session.getAttribute("logoutShibb");
	
	if ( logoutAthens instanceof Boolean ) 
	{ 
		session.removeAttribute("logoutAthens");
%>	
	<script language="JavaScript" >
		
		window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		
	</script>
<%
	}
	else if ( logoutShibb instanceof Boolean )
	{
		session.removeAttribute("logoutShibb");
%>	
	<script language="JavaScript" >
		
		window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		
	</script>
<%
	}
%>

<!-- this is for logging out on cjo -->
<c:if test="${not empty param.JSESS}">
	<img style="display: none" src="${contextRootCjo}cbo/cboImageLogout?RAND=<%=Math.random()%>"  />
</c:if>