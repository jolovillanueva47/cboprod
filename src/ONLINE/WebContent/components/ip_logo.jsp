<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.util.Set"%>
<%@page import="java.io.File"%>
<%@ page import="java.util.Date" %>
<%@page import="org.cambridge.ebooks.online.login.logo.EBookLogoBean"%>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRoot" option="current_dns" />
<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />
<o:url var="contextRootSsl" option="current_dns_to_https" />

<input id="memberType" type="hidden" value="${orgConLinkedMap.userLogin.userType}" />

<%--
	<c:set var="acceptTerms1" value="${confirmationBean.updateTermsDate}" />
	<c:set var="acceptTerms" value="${confirmationBean.updateTermsDate}" />
	EBookLogoBean lb = new EBookLogoBean(request);
	request.setAttribute("eBookLogoBean", lb);
--%>
	<!-- Login Start -->
	<input type="hidden" name="IP_LOGO_TYPEX" value="1" />
	<div id="page_container"> 
		<div id="toppanel">  
			<div class="login_container">
			<ul>
				<c:choose>
					<c:when test="${eBookLogoBean.userLoggedIn}">
						<li><b>Welcome ${eBookLogoBean.orgConMap.userLogin.user.firstName}&nbsp;${eBookLogoBean.orgConMap.userLogin.user.lastName}</b> &nbsp;|&nbsp;</li>
					</c:when>
					<c:when test="${not empty eBookLogoBean.orgName}">						
						<li>
							<c:choose>
								<c:when test="${fn:length(eBookLogoBean.orgName) > 18 }">
									<b>Welcome ${fn:substring(eBookLogoBean.orgName, 0, 18)}...</b> &nbsp;|&nbsp;
								</c:when>
								<c:otherwise>
									<b>Welcome ${eBookLogoBean.orgName}</b> &nbsp;|&nbsp;
								</c:otherwise>
							</c:choose>							
						</li>
					</c:when>
					<c:otherwise>
						<li><b>Welcome Guest</b> &nbsp;|&nbsp;</li>
					</c:otherwise>
				</c:choose>
			
				<li><a href="${pageContext.request.contextPath}/aaa/home.jsf" >Accessible Version</a> &nbsp;|&nbsp;</li>
				
				<!-- Toggle login/logouts -->	
				<c:choose>
					<c:when test="${not eBookLogoBean.userLoggedIn}">
						<li><a class="thickbox" id="loginLink" href="${contextRootSsl}login.jsf?${productPathParam}keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> &nbsp;|&nbsp;</li>
					</c:when>
					<c:otherwise>
						<li><a href="#" onclick="logout(); return false;">Logout</a> &nbsp;|&nbsp;</li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not eBookLogoBean.athensLoggedIn}">
						<li><a href="<%= System.getProperty("openathensLink") %>">Athens Login</a> &nbsp;|&nbsp;</li>
					</c:when>
					<c:otherwise>
						<li><a href="#" onclick="athensLogout(); return false;">Athens Logout</a> &nbsp;|&nbsp;</li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not eBookLogoBean.shibbolethLoggedIn}">
						<li></li><a href="<%= System.getProperty("shibbolethLink") %>">Shibboleth Login</a> &nbsp;|&nbsp;</li>
					</c:when>
					<c:otherwise>
						<li><a href="#" onclick="shibbolethLogout(); return false;">Shibboleth Logout</a> &nbsp;|&nbsp;</li>
					</c:otherwise>
				</c:choose>		
		
				<c:choose>
					<c:when test="${eBookLogoBean.userLoggedIn}">			
						<li><a href="#" onclick="MM_windowOpener('${pageContext.request.contextPath}/orgadmin/account_management.jsf?page=updateReg');" id="accountLink">My Account</a> &nbsp;|&nbsp;</li>
						<li><a href="#" onclick="MM_windowOpener('${pageContext.request.contextPath}/orgadmin/account_management.jsf?page=contentAlerts'); return false;" id="contentAlertsLink">My Content Alerts</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="#" onclick="MM_windowOpener('${pageContext.request.contextPath}/orgadmin/account_management.jsf?page=contentAlerts'); return false;" id="contentAlertsLink">My Content Alerts</a> &nbsp;|&nbsp;</li>
						<li><a href="#" onclick="MM_windowOpener('${pageContext.request.contextPath}/orgadmin/account_management.jsf?page=register'); return false;">Register</a></li>
					</c:otherwise>
				</c:choose>
				
				<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">	   
					&nbsp;|&nbsp;<li class="access_to"><a href="${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=ALL&amp;sortBy=1" >Access to</a></li>			
				</c:if>
			</ul>
			</div>
			<div class="clear"></div>
	   
			<!-- Org Info Start -->   
			<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
	    		<div id="org_container">
	    			<div id="org_content">
	    				<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
	    				<c:if test="${eBookLogoBean.hasLogo}" >
	    					<img id="orgLogoImg" src='${contextRoot}organisation/logo/org${eBookLogoBean.orgLogoId}' width="85" height="35" alt=""/>	
	    				</c:if>		    				
	    				<p class="orgname_container">${eBookLogoBean.showLogoNamesUnderMax}</p>	    				
	    				<p>
	    					<c:out value="${fn:replace(eBookLogoBean.textMessage,'null,','')}" escapeXml="false" />	    				
	    					<c:if test="${eBookLogoBean.logoNameCount gt 3 or not empty eBookLogoBean.textMessage}" >
	    						&nbsp;<a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">more ...</a>
	    					</c:if>	 				
    					</p>
	    			</div>
	    		</div>    
			</c:if>
			<!-- Org Info End --> 
			
			<!-- CUP Link Start -->
    		<a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"><img src="${pageContext.request.contextPath}/images/cup_logo.jpg" alt="Cambridge University Press" border="0" width="130" height="37" /></a>	
    		<!-- CUP Link End -->
    		
    		<!-- CBO Logo Link Start -->
			<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"><img src="${pageContext.request.contextPath}/images/cbo_pagebranding.png" alt="Cambridge Books Online" border="0" width="452" height="42" /></a>
			<!-- CBO Logo Link End -->
		</div>
    </div>	
	<!-- Login End -->



<script type="text/javascript">

function athensLogout() {
	window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = "${pageContext.request.contextPath}/home.jsf?alo=y";
	return true;
}

function shibbolethLogout() {
	window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = "${pageContext.request.contextPath}/home.jsf?slo=y";
	return true;
}

	//termsOfUseAgreement();
	
	function termsOfUseAgreement(){
		var memType = document.getElementById("memberType").value;
		
		if(getDateTerms() != getDbDateTerms()){
			if("AO"==memType || "AC"==memType){
				window.open('${pageContext.request.contextPath}/popups/terms_use_confirmation.jsf','popup','status=yes,scrollbars=yes,width=550,height=500');return false;				  
			}
		}
	}
		
	function getDateTerms(){
		dateTerms ='<%=session.getAttribute("termsDate") %>';
		return dateTerms;
	}
	
	function getDbDateTerms(){
		dbDateTerms ='<%=session.getAttribute("dateUpdatedTerms") %>';
		return dbDateTerms;
	}
</script>