<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<div class="series_alphalist_container">
	${seriesBean.initAlphaMap}
	<ul>
		<c:forEach var="alphaMap" items="${seriesBean.alphaMap}">    	
			<c:choose>
		    	<c:when test="${'link' eq alphaMap.value}">
		    		<li><a href="${pageContext.request.contextPath}/all_series.jsf?firstLetter=${alphaMap.key}">${alphaMap.key}</a></li>
		    	</c:when>
		    	<c:otherwise>
		    		<li>${alphaMap.key}</li>
		    	</c:otherwise>					
		    </c:choose>			   
		</c:forEach>
	</ul>
</div>