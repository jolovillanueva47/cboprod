<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

${subscriptionBean.initSubscription}
<c:if test="${subscriptionBean.noActiveSubscriptions}">
	<div class="signup_button">
		<span>
			<c:choose>
				<c:when test="${not empty userInfo.username}">
					<a title="Sign up for a Trial" href="#" onclick="return CJOLoginLink('${contextRootCjo}','updateRegistration?SIGNUP_ADMIN=Y&ORG=${orgConLinkedMap.currentlyLoggedBodyId}&USER=${accessDetailsBean.bodyId}','signupLink')" id="signupLink">
						Sign up for a Trial
					</a>
				</c:when>             	
				<c:otherwise>			 
					<a title="Sign up for a Trial" href="#" onclick="return CJOLoginLink('${contextRootCjo}','registration?SIGNUP=Y','adminLink')" id="adminLink">
						Sign up for a Trial
					</a>
				</c:otherwise>		                    	
			</c:choose>
		</span>
	</div>
</c:if>			 