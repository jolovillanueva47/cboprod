<p><strong>Download list of available books</strong></p>
			                        
<ul>
	<li>
		<span class="icons_img download_excel">&nbsp;</span>
		<a id="title_list_xls" href="${pageContext.request.contextPath}/downloads/titlelist.xls">Microsoft Excel Format</a>
	</li>
	<li>
		<span class="icons_img download_csv">&nbsp;</span>
		<a id="title_list_csv" href="${pageContext.request.contextPath}/downloads/titlelist.csv">CSV Format - Recommended for Mac users</a>
	</li>
</ul>