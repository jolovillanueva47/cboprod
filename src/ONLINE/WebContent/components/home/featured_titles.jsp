<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<ol>			
	<c:if test="${not empty topBooksBean.topFeaturedTitleList}">			
		<c:forEach var="book" items="${topBooksBean.topFeaturedTitleList}">				
			<li>						
				<a href="${pageContext.request.contextPath}/ebook.jsf?bid=CBO${book.isbn}"><c:out value="${book.title}" escapeXml="false"/></a>
			</li>			
		</c:forEach>			
	</c:if>			 
</ol>
