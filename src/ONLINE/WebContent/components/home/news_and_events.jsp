<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

${newsBean.initialize}
<ul>	                    	
	<c:forEach var="news" items="${newsBean.newsList}">
		<li><span class="icons_img bullet01">&nbsp;</span><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/news.jsf?messageId=${news.messageId}','popup','status=yes,scrollbars=yes,width=750,height=700'); return false;">${news.messageTitle}</a></li>
	</c:forEach>                        	   	        	
</ul>          