<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>	
<div class="titlebanner_topsearch">Featured Collections</div>
<div class="clear"></div>
<ol>
	<c:if test="${not empty topBooksBean.topFeaturedCollectionsList}">
		<c:forEach var="coll" items="${topBooksBean.topFeaturedCollectionsList}">
			<c:url value="/collection_landing.jsf" var="collection">
				<c:param name="results" value="50" />
				<c:param name="sort" value="title_alphasort" />
				<c:param name="collectionId" value="${coll.collectionId}" />
				<c:param name="collectionName" value="${coll.collectionName}" />
			</c:url>			
			<li><a href="<c:out value='${collection}'/>">${empty coll.collectionName ? coll.collectionId : coll.collectionName }</a></li>
		</c:forEach>
	</c:if>
</ol>