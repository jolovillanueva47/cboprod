<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

${urgentAlertBean.initialize}
<c:if test="${urgentAlertBean.display_online == 'true'}">
	<div class="urgent_alert">
		<span id="closeUrgentAlert" class="icon_close_urgent_alert"><a href="#">Close</a></span>
		<p>${urgentAlertBean.description}</p>
	</div>
</c:if>