<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:choose>
	<c:when test="${fn:contains(pageContext.request.requestURL, 'subject_landing.jsp')}">
		<c:set var="bean" value="${subjectBean}" />
	</c:when>
	<c:otherwise>
		<c:set var="bean" value="${seriesBean}" />
	</c:otherwise>
</c:choose>

<c:if test="${bean.resultsFound > 0}">
	<div class="page_controller">
		<div class="border_divider01">
			<label class="label_01">Results per page:</label>
			<select class="results_per_page select_01">
				<option value="50">50</option>   
				<option value="75">75</option>
				<option value="100">100</option>
			</select>
		</div>
		<div class="border_divider02">
			<label class="label_01">Page ${bean.currentPage} of ${bean.pages} | Go to page:</label>
			<input type="text" size="3" id="txt_go_to_page" class="go_to_page"/>
		</div>
		<div id="go_button_container">
			<div class="form_button"><span><a href="javascript:void(0);" class="bot_go" title="Click here to go!">Go</a></span></div>
		</div>
		
		<c:if test="${accBean.pageName eq 'subject_landing.jsf?'}">
			<div class="border_divider02">
				<label class="label_01">Sort by:</label>
	        	<select class="sort_type" >
	            	<option value="title_alphasort">Title</option>
	            	<option value="author_name_alphasort">Author</option>
	            	<option value="print_date">Print Publication Year</option>
	            	<option value="online_date">Online Publication Date</option>
	            </select>
			</div>
		</c:if>
	
		<div class="border_divider03">
			<label>Go to:</label>  
			<c:choose>
				<c:when test="${bean.hasFirst}">
					<a href="javascript:void(0);" name="1" class="page_nav">First</a>
				</c:when>
				<c:otherwise>
					First
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${bean.hasPrevious}">
					<a href="javascript:void(0);" name="${bean.currentPage-1}" class="page_nav">Previous</a>
				</c:when>
				<c:otherwise>
					Previous
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${bean.hasNext}">
					<a href="javascript:void(0);" name="${bean.currentPage+1}" class="page_nav">Next</a>
				</c:when>
				<c:otherwise>
					Next
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${bean.hasLast}">
					<a href="javascript:void(0);" name="${bean.pages}" class="page_nav">Last</a>
				</c:when>
				<c:otherwise>
					Last
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="clear"></div>
</c:if>