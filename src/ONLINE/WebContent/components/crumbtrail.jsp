<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>
<div class="breadcrumbs_container">
	<div class="icons_img breadcrumbs_divider"></div>
	<o:crumbtrail propertyFile="org.cambridge.ebooks.online.crumbtrail.crumb"					
		lastElementStyle="last" 
		userCrumb="org.cambridge.ebooks.online.crumbtrail.UserCrumb" >
	</o:crumbtrail>							
</div>
