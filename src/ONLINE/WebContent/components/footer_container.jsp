<!-- Footer Start -->
<div class="footer_container">
	<div class="footer_wrapper">
		<a class="icons_img crossref" href="http://www.crossref.org/" target="_blank" title="Crossref Member"><img src="${pageContext.request.contextPath}/images/icon_crossref.png" alt="Crossref Member" border="0" width="85" height="31" /></a>
		<a class="icons_img acap" href="http://the-acap.org/acap-enabled.php" target="_blank" title="ACAP Enabled"><img src="${pageContext.request.contextPath}/images/icon_acap.png" alt="ACAP Enabled" width="35" height="31" /></a>
		<div class="icons_img cup_logo"><img src="${pageContext.request.contextPath}/images/cup_shield.png" alt="Cambridge University Press" width="144" height="32" /></div>
	</div>
</div>
<!-- Footer End -->
