<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<div id="bordergrid_container">  
	<div id="alphalist_horizontal">
		${collectionBean.initAlphaMap}
		<ul>
			<c:forEach var="alphaMap" items="${collectionBean.alphaMap}">    	
				<c:choose>
			    	<c:when test="${'link' eq alphaMap.value}">
					    	<c:url value="/collection_landing.jsf" var="collectionUrl">
								<c:param name="results" value="50" />
								<c:param name="sort" value="title_alphasort" />
								<c:param name="collectionId" value="${collectionBean.collectionId}" />
								<c:param name="collectionName" value="${collectionBean.collectionName}" />
								<c:param name="firstLetter" value="${alphaMap.key}" />
							</c:url>
			    		<li><a title="${alphaMap.key}" href="<c:out value='${collectionUrl}'/>">${alphaMap.key}</a></li>
			    	</c:when>
			    	<c:otherwise>
			    		<li>${alphaMap.key}</li>
			    	</c:otherwise>					
			    </c:choose>			   
			</c:forEach>
			<c:url value="/collection_landing.jsf" var="collectionUrl_ALL">
				<c:param name="results" value="50" />
				<c:param name="sort" value="title_alphasort" />
				<c:param name="collectionId" value="${collectionBean.collectionId}" />
				<c:param name="collectionName" value="${collectionBean.collectionName}" />
				<c:param name="firstLetter" value="allTitles" />
			</c:url>
			<li><a class="link03" title="All Titles" href="<c:out value='${collectionUrl_ALL}'/>">All Titles</a></li>
		</ul>
	</div>
</div>

<c:choose>
	<c:when test="${empty collectionBean.firstLetter or collectionBean.firstLetter eq 'allTitles' }">
		<div id="content_wrapper09"><h1>All Titles</h1></div>
	</c:when>
	<c:otherwise>
		<div id="content_wrapper09"><h1>${collectionBean.firstLetter}</h1></div>
	</c:otherwise>
</c:choose>
