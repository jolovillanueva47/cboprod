<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen" />
<!--[if IE 8]>
<link href="${pageContext.request.contextPath}/css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 7]>
<link href="${pageContext.request.contextPath}/css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 6]>
<link href="${pageContext.request.contextPath}/css/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/thickbox.css" type="text/css" media="screen" />
<% 
String userAgent = request.getHeader("User-Agent");
if(userAgent.indexOf("Opera") > -1) { %>	
<style type="text/css">
	span.button button, del.button span, span.button input {
		_behavior:expression(
			(function(el){
	
				if( typeof( behavior_onMouseEnter) == 'undefined'){				
					behavior_onMouseEnter = function(el){					
						var dEl = this.parentNode;					
						var sClass = dEl.className ;
						dEl.__defaultClassName = sClass ;
						dEl.className = sClass + ' button-behavior-hover';	
						this.setCapture();
					};
	
					behavior_onMouseLeave = function(el) {
						var dEl = this.parentNode;
						dEl.className = dEl.__defaultClassName ;
						dEl.__defaultClassName = undefined;
						this.releaseCapture();
					};
	
				};			
				
				el.runtimeStyle.behavior = 'none';
				el.onmouseenter = behavior_onMouseEnter;
				el.onmouseleave = behavior_onMouseLeave;			
				
			})(this));
	
	}
</style>	
<%}%>

<!-- open search -->
<link rel="search" href="${pageContext.request.contextPath}/widget?cmd=openSearch&amp;xml=cboQuickSearch.xml&amp;servlet=${pageContext.request.contextPath}/search?" 
     type="application/opensearchdescription+xml" 
     title="Cambridge Books Online" />
     