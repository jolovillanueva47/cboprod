<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="org.cambridge.ebooks.online.jpa.countrystates.Country"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:set var="country" value="${bookBean.country}" scope="page" />	
<c:set var="bookId" value="${bookBean.bookMetaData.id}" scope="page" />			
<c:set var="bookTitle" value="${bookBean.bookMetaData.title}" scope="page" />
<c:set var="bookTitleSession" value="${bookTitle}" scope="session" />
<c:set var="chapterId" value="${bookBean.bookContentItem.id}" scope="page" />	
<c:set var="chapterTitle" value="${bookBean.bookContentItem.title}" scope="page" />
<c:set var="chapterPageStart" value="${bookBean.bookContentItem.pageStart}" scope="page" />
<c:set var="chapterPageEnd" value="${bookBean.bookContentItem.pageEnd}" scope="page" />
<c:set var="copyright" value="${bookBean.bookMetaData.copyrightStatement}" scope="session" />
<c:set var="printDate" value="${bookBean.bookMetaData.printDate}" scope="page" />
<c:set var="printDateSession" value="${printDate}" scope="session" />
<c:set var="onlineDate" value="${bookBean.bookMetaData.onlineDate}" scope="page" />
<c:set var="onlineDateSession" value="${onlineDate}" scope="session" />
<c:set var="onlineIsbn" value="${bookBean.bookMetaData.onlineIsbn}" scope="page" />
<c:set var="onlineIsbnSession" value="${onlineIsbn}" scope="session" />
<c:set var="printIsbn" value="${bookBean.bookMetaData.printIsbn}" scope="page"/>
<c:set var="hardbackIsbn" value="${bookBean.bookMetaData.hardbackIsbn}" scope="page" />
<c:set var="doi" value="${bookBean.bookMetaData.doi}" scope="page" />
<c:set var="doiSession" value="${doi}" scope="session" />
<c:set var="chapterDoi" value="${bookBean.bookContentItem.doi}" scope="page" />
<c:set var="chapterDoiSession" value="${chapterDoi}" scope="session" />
<c:set var="authorNames" value="${bookBean.bookMetaData.authorNames}" scope="page" />
<c:set var="authorNamesSession" value="${authorNames}" scope="session" />
<c:set var="authorNamesLf" value="${bookBean.bookMetaData.authorNamesLf}" scope="page" />
<c:set var="contributorNames" value="${bookBean.bookContentItem.contributorNames}" scope="page" />
<c:set var="currentIsbn" value="${bookBean.bookMetaData.onlineIsbn}" scope="request" />
<c:set var="volume" value="${bookBean.bookMetaData.volumeNumber}" scope="request" />
<c:set var="publisherName" value="${bookBean.bookMetaData.publisherName}" scope="request" />
<c:set var="publisherLoc" value="${bookBean.bookMetaData.publisherLoc}" scope="request" />
<c:set var="contentType" value="${bookBean.contentType}" scope="request" />


<c:set var="worldCatLink" value="${bookBean.worldCatLink}" scope="page" />

<c:remove var="authorList" scope="session" />
<c:remove var="authorRoleList" scope="session" />
<c:remove var="authorAffiliationList" scope="session" />
<c:set var="authorList" value="${bookBean.bookMetaData.authorNameList}" scope="session"/>
<c:set var="authorRoleList" value="${bookBean.bookMetaData.authorRoleTitleList}" scope="session"/>
<c:set var="authorAffiliationList" value="${bookBean.bookMetaData.authorAffiliationList}" scope="session"/>		

<c:set var="ebookUrl" value="${fn:replace(pageContext.request.requestURL, '.jsp', '.jsf')}?${pageContext.request.queryString}" scope="page" />
<c:set var="exactURLSession" value="${ebookUrl}" scope="session" />


<c:choose>
	<c:when test="${fn:contains(pageContext.request.requestURL, 'chapter.jsp')}">		
		<c:url value="/popups/how_to_cite.jsf" var="urlHowToCite">
			<c:param name="bookId" value="${bookId}" />
			<c:param name="bid" value="${bookId}" />
			<c:param name="cid" value="${chapterId}" />			
			<c:param name="bookTitle" value="${bookTitle}" />
			<c:param name="contentId" value="${chapterId}" />
			<c:param name="contentTitle" value="${chapterTitle}" />
			<c:param name="page" value="pp. ${chapterPageStart}-${chapterPageEnd}" />
			<c:param name="author" value="${authorNamesLf}" />
			<c:param name="printDate" value="${printDate}" />
			<c:param name="doi" value="${chapterDoi}" />		
	 	</c:url>
	 	
	 	<!-- start cited by crossref-->
		<c:if test="${not empty chapterDoi}">
			<c:url value="/citedBy" var="urlCitedByCrossref">
				<c:param name="doi" value="${chapterDoi}" />
				<c:param name="TB_iframe" value="true" />
				<c:param name="width" value="620" />
				<c:param name="height" value="480" />
		 	</c:url>
	 	</c:if>
	 	<!-- end cited by crossref -->
	 	
	 	<c:choose>
			<c:when test="${not empty userInfo}">
				<c:url value="/popups/alert_me.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />
					<c:param name="doi" value="${chapterDoi}" />
					<c:param name="author" value="${authorNames}" />
					<c:param name="bookId" value="${bookId}" />
					<c:param name="bookTitle" value="${bookTitle}" />
					<c:param name="contentId" value="${chapterId}" />
					<c:param name="contentTitle" value="${chapterTitle}" />
					<c:param name="page" value="pp. ${chapterPageStart}-${chapterPageEnd}" />
					<c:param name="publishDate" value="${onlineDate}" />
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="455" />
				</c:url>
			</c:when>
			<c:otherwise>			
				<c:url value="/popups/alert_me_not.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />								
					<c:param name="LOCATION10556" value="${ebookUrl}"/>
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="100" />
				</c:url>
			</c:otherwise>
		</c:choose>	
	</c:when>
	<c:otherwise>						
		<c:url value="/popups/how_to_cite.jsf" var="urlHowToCite">
			<c:param name="bookId" value="${bookId}" />
			<c:param name="bid" value="${bookId}" />
			<c:param name="bookTitle" value="${bookTitle}" />
			<c:param name="author" value="${authorNamesLf}" />
			<c:param name="printDate" value="${printDate}" />
			<c:param name="doi" value="${doi}" />		
	 	</c:url>
				 	
		<!-- start cited by crossref-->
		<c:if test="${not empty doi}">
			<c:url value="/citedBy" var="urlCitedByCrossref">
				<c:param name="doi" value="${doi}" />
				<c:param name="TB_iframe" value="true" />
				<c:param name="width" value="620" />
				<c:param name="height" value="480" />
		 	</c:url>
	 	</c:if>
	 	<!-- end cited by crossref -->
				 			
		<c:choose>
			<c:when test="${not empty userInfo}">
				<c:url value="/popups/alert_me.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />
					<c:param name="doi" value="${doi}" />
					<c:param name="author" value="${authorNames}" />
					<c:param name="bookId" value="${bookId}" />
					<c:param name="bookTitle" value="${bookTitle}" />
					<c:param name="publishDate" value="${onlineDate}" />
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="455" />
				</c:url>
			</c:when>
			<c:otherwise>			
				<c:url value="/popups/alert_me_not.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />								
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="100" />
				</c:url>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

<!-- Shared URL Start -->
<c:url value="/openUrlResolver.jsf" var="openUrlResolver">
 	<c:param name="keepThis" value="true" />  		
	<c:param name="title" value="${bookTitle}" />
	<c:param name="TB_iframe" value="true" />  		
	<c:param name="width" value="730" />  
	<c:param name="height" value="435" /> 
</c:url>

<c:url value="/popups/export_citation.jsf" var="urlExportCitation">
	<c:param name="isbn" value="${onlineIsbn}" />
	<c:param name="bookId" value="${bookId}" />
	<c:param name="id" value="${bookId}" />
	<c:param name="contentId" value="${chapterId}" />
	<c:param name="bookTitle" value="${bookTitle}" />						
	<c:param name="contentTitle" value="${chapterTitle}" />						
	<c:param name="printDate" value="${printDate}" />						
	<c:param name="doi" value="${doi}" />								
	<c:param name="page" value="${page}" />
	<c:param name="author" value="${authorNames}" />
	<c:param name="contributor" value="${contributorNames}" />
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="850" />
	<c:param name="height" value="455" />
	<c:param name="volume" value="${volume}" />
	<c:param name="publisherName" value="${publisherName}" />
	<c:param name="publisherLoc" value="${publisherLocation}" />
	<c:param name="pageStart" value="${chapterPageStart}" />

</c:url>
				 	
<c:url value='/popups/email_link.jsf' var="emailLink">			
  	<c:param name="keepThis" value="true"/>
  	<c:param name="Title" value="${bookTitle}" />
	<c:param name="Copyright" value="${copyright}" />
	<c:param name="PrintpubDate" value="${printDate}" />
	<c:param name="OnlinepubDate" value="${onlineDate}" />
	<c:param name="OnlineIsbn" value="${onlineIsbn}" />
	<c:param name="HardbackIsbn" value="${hardbackIsbn}" />
	<c:param name="Doi" value="${doi}" />	
	<c:param name="ChapterDoi" value="${chapterDoi}" />		
	<c:param name="exactURL" value="${ebookUrl}" />	
	<c:param name="bid" value="${bookId}" />
	<c:param name="cid" value="${chapterId}" />
	<c:param name="contentType" value="${contentType}" />			 
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="620" />
	<c:param name="height" value="490" />
</c:url>	

<c:url value="http://delicious.com/save" var="urlDelicious">
	<c:param name="url" value="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.connotea.org/addpopup" var="urlConnotea">
	<c:param name="uri" value="${ebookUrl}" />
	<c:param name="usertitle" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.bibsonomy.org/ShowBookmarkEntry" var="urlBibsonomy">
	<c:param name="url" value="${ebookUrl}" />
	<c:param name="description" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://digg.com/submit" var="urlDigg">
	<c:param name="url" value="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>			

<c:url value="http://reddit.com/submit" var="urlReddit">
	<c:param name="url" value="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.facebook.com/sharer.php" var="urlFacebook">
	<c:param name="u" value="${ebookUrl}" />
</c:url>
		
<c:url value="http://addthis.com/bookmark.php" var="urlAddThis">
	<c:param name="url" value="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<!-- Google Scholar --> 
<c:set var="firstAuthorChop" value="${fn:split(authorList[0], ' ')}" />
<c:set var="firstAuthorLastName" value="${firstAuthorChop[fn:length(firstAuthorChop) - 1]}" />
<c:url value="http://scholar.google.com/scholar" var="googleScholarUrl">
	<c:param name="q" value="allintitle:\"${fn:replace(bookTitle, '%26%2338%3B', '%26')}\" author:${firstAuthorLastName} site:books.google.com" />	
</c:url>
<!-- Shared URL End -->
                    
<!-- Right menu Start -->       
<div class="rightmenu">
	<a href="<c:out value="${openUrlResolver}" escapeXml="true" />" class="thickbox menuitem submenuheader">Open URL Link Resolver</a>
    <div class="submenu"></div>
    
    <!-- buy the book start -->		
    <c:choose>
    	<c:when test="${'uk' eq country}">
    		<a class="menuitem submenuheader" href="javascript:void(0);" onclick="anchor('http://www.cambridge.org/catalogue/catalogue.asp?isbn=${printIsbn}');">Buy the Book</a>
    	</c:when>
    	<c:otherwise>
    		<a class="menuitem submenuheader" href="javascript:void(0);" onclick="anchor('http://www.cambridge.org/catalogue/catalogue.asp?isbn=${printIsbn}');">Buy the Book</a>
    	</c:otherwise>
    </c:choose>	
    
	<!-- buy the book end -->
    <div class="submenu"></div>
      <%-- temporarily disabled 
      <a class="menuitem submenuheader" href="#">Trackback Links</a>
         <div class="submenu"></div>
	  --%>
	<!-- find this book in a library -->			
	<a class="menuitem submenuheader" onclick="anchor('${worldCatLink}');" href="javascript:void(0);">Find This Book in a Library</a>
	
	<!-- find this book in a library -->
    <div class="submenu"></div>
      
    <!-- Email Link to This Book --> 
 	<a class="menuitem submenuheader thickbox" href="<c:out value='${emailLink}' escapeXml='true' />">Email Link to This Book</a>
 	<div class="submenu"></div>
    <a class="menuitem submenuheader" href="#">Citation Tools</a>
   	<div class="submenu">
   		<ul>
	       	<li>    			 	
				<a href="#" onclick="MM_openHowToCite('<c:out value="${urlHowToCite}"/>','popup','status=no,scrollbars=no,width=750,height=250'); return false;">
				How to Cite</a>               
			</li>
      		<li>	
      			<a href="<c:out value='${urlExportCitation}' escapeXml='true' />" class="thickbox">Export Citation</a>
      		</li>
      		<li>          			
				<a href="<c:out value="${urlAlertMe}" escapeXml="true" />" class="thickbox">Alert Me When This Book Is Cited</a>
            </li>
            <!-- cited by crossref -->
			<c:if test="${not empty urlCitedByCrossref}" >
				<li>
					<a href="<c:out value='${urlCitedByCrossref}' escapeXml='true' />" class="thickbox">Cited By (CrossRef)</a>
				</li>
			</c:if>
			<!-- cited by crossref end-->

			<!-- cited by google scholar (for release4) -->
            <li>
				<a href="<c:out value='${googleScholarUrl}' escapeXml='true' />" target="_blank">Cited By (Google Scholar)</a>
            </li>
     	</ul>
    </div>  
    <a class="menuitem submenuheader" href="#">Link to This Book</a>
   	<div class="submenu">
       	<ul>        
       		<li>
				<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlDelicious}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					<span class="icons_img delicious"></span>Del.icio.us
				</a>
			</li>
            <li>               		
				<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlConnotea}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					<span class="icons_img connotea">&nbsp;</span>Connotea.org
				</a>
			</li>
            <li>                	
               	<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlBibsonomy}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					<span class="icons_img bibsonomy">&nbsp;</span>Bibsonomy.org
				</a>
			</li>
            <li>
               	<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlDigg}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					<span class="icons_img digg">&nbsp;</span>Digg.com
				</a>
			</li>
            <li>
               	<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlReddit}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					<span class="icons_img reddit">&nbsp;</span>Reddit.com
				</a>
           	</li>
           
            <li>
                <a href="#" onclick="javascript:MM_openWindow_socialNetworking('<c:out value='${ebookUrl}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700', '<c:out value='${urlFacebook}' escapeXml='true' />');">
					 <span class="icons_img facebook">&nbsp;</span>Facebook.com
				</a>
            </li>
          
            <li>
               	<a href="#" onclick="javascript:MM_openWindow('<c:out value='${urlAddThis}' escapeXml='true' />','popup','status=yes,scrollbars=yes,width=850,height=700');">
					 <span class="icons_img addthis">&nbsp;</span>AddThis.com
				</a>
            </li>
    	</ul>
    </div>
   	<c:if test="${'Y' eq supplemental.supplementalMaterial}">    	
    	<a href="#" onclick="anchor('${supplemental.supplementalMaterialUrl}'); return false;" class="menuitem submenuheader">Resources and Solutions</a>
    </c:if>                           
</div>
<!-- Right menu End --> 
