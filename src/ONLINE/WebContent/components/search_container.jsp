<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>			
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<!-- Search Start -->
<form id="search_form" action="" method="post">		
	<!-- show only in ebook and chapter landing page -->
	<c:choose>
		<c:when test="${param.bid ne null}">
			<input type="hidden" name="pageType" value="book" />
			<input type="hidden" name="bid" value="${param.bid}" />  
		</c:when>
		<c:otherwise>             				
			<input type="hidden" name="pageType" value="other" />
		</c:otherwise>
	</c:choose>		
	<table id="search_container" cellspacing="0" cellpadding="0">
		<tr>
			<td><input id="search_text" type="text" value="Please enter a keyword" onfocus="clearDefault(this);" class="text_search" /></td>
			<td><div class="form_button"><span><a id="search_link" href="javascript:void(0);" title="Click to Search">Search</a></span></div></td>
		</tr>
		<tr>
			<td colspan="2"> 
				<c:choose>             			
					<c:when test='${fn:contains(pageContext.request.requestURL, "ebook.jsp") or fn:contains(pageContext.request.requestURL, "chapter.jsp")}'>
						<ul>
							<li class="fr"><span class="icons_img arrow01">&nbsp;</span><a href="${pageContext.request.contextPath}/advance_search.jsf">Advanced Search</a></li>
							<li class="fl"><span class="input_checkbox"><input type="checkbox" name="searchWithinContent"></input></span><label>Search within This Book</label></li>
						</ul>  
					</c:when>
					<c:otherwise>                   					                  			         						      
						<ul>
							<li class="fr"><span class="icons_img arrow01">&nbsp;</span><a href="${pageContext.request.contextPath}/advance_search.jsf">Advanced Search</a></li>
						</ul>  
					</c:otherwise>
				</c:choose>			
			</td>
		</tr>
	</table>
</form>
<!-- Search End -->