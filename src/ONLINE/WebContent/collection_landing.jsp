<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>
	${collectionBean.initPage}
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>	
	    
	</head>

	<body>
		
		<c:import url="/components/loader.jsp" />	
		
		<div id="main" style="display: none;"> 
			<div id="top_container">
				<div id="page-wrapper">	   	           
					<f:subview id="subviewIPLogo">
						<c:import url="${pageContext.request.contextPath}/components/ip_logo.jsp" />	
					</f:subview>
	    	
					<!-- Header Start -->
					<div id="header_container">        	
						<f:subview id="subviewTopMenu">
							<c:import url="/components/top_menu.jsp" />	
						</f:subview>    
						
						<f:subview id="subviewSearchContainer">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
				    </div> 
				    <!-- Header End -->
	        		
			        <!-- Main Body Start -->
			        <div id="main_container">			
			        
						<!-- Titlepage Start -->
					<%--<span class="titlepage_heading"><h1>Collection</h1></span>  --%>
						<div class="titlepage_heading"><h1>Collection</h1></div>
						<!-- Titlepage End -->
	            
						<!-- Breadcrumbs Start -->
						<f:subview id="crumbtrailContainer">
							
							<c:import url="/components/crumbtrail.jsp" />
									
						</f:subview>
						<!-- Breadcrumbs End -->  
	            		
	            		<div class="clear"></div>
	            		
						<!-- Access Information Start -->
						<div class="access_information">
							<strong>Access Information:</strong>
							<ul>
								<li><span class="icons_img purchase"><img src="${pageContext.request.contextPath}/images/icon_purchased.png" alt="Purchased Access" width="19" height="19" /></span>Purchased Access</li>
							</ul>
						</div>
						<!-- Access Information End -->
	                
	            		<div class="clear"></div>
						
						<div class="searchtitle_heading">
							<h2><c:out value="${collectionBean.collectionName}" /></h2>
							<c:out value="${collectionBean.collectionDesc}" escapeXml="false" /><br/>
							<c:if test='${not empty collectionBean.collectionCreationDate}' >
								<c:out value="<b>Creation Date: </b>${collectionBean.collectionCreationDate}" escapeXml="false" />
							</c:if>
						</div>
						
						<div id="content_wrapper01">
						
							<f:subview id="subviewAlphaMap">
								<c:import url="/components/collection_alphalist_main.jsp" />	
							</f:subview> 

							<!-- Border Line01 End -->
	                 
							<div id="content_wrapper03">
								<c:url var="urlTitle" value="${pageContext.request.contextPath}/collection_landing.jsf">
									<c:param name="collectionId" value="${collectionBean.collectionId}" />
									<c:param name="collectionName" value="${collectionBean.collectionName}" />
									<c:param name="searchType" value="page" />
									<c:param name="sort" value="title_alphasort" />
									<c:param name="results" value="${param.results}" />
									<c:param name="firstLetter" value="${param.firstLetter}" />
								</c:url>
								<c:url var="urlAuthor" value="${pageContext.request.contextPath}/collection_landing.jsf">
									<c:param name="collectionId" value="${collectionBean.collectionId}" />
									<c:param name="collectionName" value="${collectionBean.collectionName}" />
									<c:param name="searchType" value="page" />
									<c:param name="sort" value="author_name_alphasort" />
									<c:param name="results" value="${param.results}" />
									<c:param name="firstLetter" value="${param.firstLetter}" />
								</c:url>								
								<b>Sort by:</b>	
								<a href="<c:out value='${urlTitle}' escapeXml='true' />" id="title_alphasort" class="sort_link">Book Title</a> |
								<a href="<c:out value='${urlAuthor}' escapeXml='true' />" id="author_name_alphasort" class="sort_link">Author Last Name</a>
								
								Your search returned <b>${collectionBean.resultsFound}</b> ${collectionBean.resultsFound > 1 ? 'results.' : 'result.'} 			
							</div>

							<c:choose>
								<c:when test="${collectionBean.resultsFound > 0}">
									<c:out value="${collectionBean.bookMeta}" escapeXml="false"></c:out>  
								</c:when>
								<c:otherwise>
									<div class="content_wrapper04">
										<p><h2>No results found.</h2></p>
									</div>
									
								</c:otherwise>
							</c:choose>                
						</div>
						
						<c:if test="${collectionBean.resultsFound > 0}" >
								<span id="loadingImg" style="display: none;">Please wait...
									<img src="${pageContext.request.contextPath}/images/circle_loading.gif"></img>
								</span>
						
								<ul class="show_all" id="show_all">
									<li><a title="View more" href="javascript:void(0);" id="viewmore_coll" class="link_view">View more<img height="10" width="13" alt="" src="${pageContext.request.contextPath}/images/icon_arrow_more_info.gif" /></a></li>
								 </ul>
								 <%--
								 <ul class="show_all" id="sim">
									<li><a title="View more" href="javascript:void(0);" id="sim" class="link_view">Simulate simultaneous Click<img height="10" width="13" alt="" src="${pageContext.request.contextPath}/images/icon_arrow_more_info.gif" /></a></li>
								 </ul>
								  --%>
						</c:if>

					</div>  
					<!-- Main Body End -->
	        
					<f:subview id="subviewFooterMenuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>	        
				</div> 
			</div>
			<f:subview id="subviewFooterContainer">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>	
		</div>
	
		<f:subview id="commonJsSubview">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/series.js"></script>		
		<f:subview id="googleAnalyticsSubview">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
		
		
		<script type="text/javascript">
			$(document).ready(function(){
				var PAGE_SIZE = {};
				var sortBy = {};
				var start = {};
				var stop = {};
				var temp = {};
				var isbnList = new Array();
				var collection_id = '${collectionBean.collectionId}';
				
				temp = '${collectionBean.isbnlist_isbnOnly}';
				isbnList = temp.split(",");
				PAGE_SIZE = parseInt('<%=request.getParameter("results")%>');
				sortBy = '<%=request.getParameter("sort")%>';
				
				start = PAGE_SIZE;
				
				if((isbnList.length - PAGE_SIZE) < PAGE_SIZE){
					stop = start + (isbnList.length % PAGE_SIZE);
				}else{
					stop = start + PAGE_SIZE;
				}
				mega_counter = start;
		
				$('#viewmore_coll').click(function(event) {
					event.preventDefault();
					var sb = "";
					
					for(i = start ; i < stop ; i++){
						var isbn = isbnList[i];
						if(isbn != undefined){
							isbn = isbn.replace("[","");
							isbn = isbn.replace("]","");
							isbn = jQuery.trim(isbn);

							sb+=isbn;
							
							if(i!=stop-1){
								sb+=","
							}
						}
					}

					if(stop == isbnList.length ){
						$('#viewmore_coll').remove();
					}

					start+=PAGE_SIZE;
					stop+=PAGE_SIZE;

					if(stop > isbnList.length ){
						stop = isbnList.length
					} 
					doposting(sb);
				});


				function doposting(sb){
					$('#loadingImg').attr("style","display: block;");
					$('#show_all').attr("style","display: none;");
					//this will go to a servlet
					//$.post( "collection",{ paramIsbn: sb, pageSize: PAGE_SIZE, paramSortBy: sortBy },function(xml){ 
							//doSuccessMethod(xml);});
					//this will go to a dojsprocess
					$.post( "${pageContext.request.contextPath}/components/viewmore.jsf",{ paramIsbn: sb, pageSize: PAGE_SIZE, paramSortBy: sortBy },function(xml){ 
						doSuccessMethod(xml);});
				}

				function doSuccessMethod(xml){
					$('#content_wrapper01').append(xml);
					$('#loadingImg').attr("style","display: none;");
					$('#show_all').attr("style","display: block;");	
				}
				
				$('#sim').click(function(event) {
					event.preventDefault();

					var len = isbnList.length - PAGE_SIZE ;
					var rem = len % PAGE_SIZE ;
					var quo = (len - rem ) / PAGE_SIZE;
					if(rem!=0){
						quo++;
					}
					quo--;
					
					for(var i = 0 ; i < quo ; i++){
						$('#viewmore_coll').click();
					}

				});

				if(isbnList.length <= PAGE_SIZE){
					$('#viewmore_coll').remove();
				}

				// sort
				$(".sort_link").each(function(){
					if(sort == $(this).attr("id")) {
						$(this).html("<strong>" + $(this).html() + "</strong>");
					}		
				});				

			});
		</script>
		
		
		
		
		
	</body>

</f:view>

</html>