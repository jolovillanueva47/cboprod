<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>
</head>

<body>
	<c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">
	
	<div id="top_container">
	<div id="page-wrapper">
    	
    <f:subview id="subviewIPLogo">
		<c:import url="/components/ip_logo.jsp" />	
	</f:subview>
	
	<!-- Header Start -->
	<div id="header_container">	
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
	<!-- Header End -->
	<!-- Main Body Start -->
	<div id="main_container">
	
		<!-- Titlepage Start -->
		<div class="titlepage_heading"><h1>About <em>Cambridge Books Online</em></h1></div>
		<!-- Titlepage End -->
		
		<p>Cambridge University Press is one of the largest and most prestigious academic publishers in the world and we are widely respected as a world leader in publishing for subjects as diverse as astronomy, Shakespeare studies, economics, mathematics and politics.</p>
		
		<p><em>Cambridge Books Online</em> offers predefined or bespoke collections of content within a richly functional, fully searchable online environment.</p>
		
		<p>Access to <em>Cambridge Books Online</em> content is via institutional purchase, allowing unlimited concurrent users, meaning anyone who wants to may access the resource 24 hours a day.</p>
		
		<p>With online access to unique, indispensable and extensive scholarly content, <em>Cambridge Books Online</em> will offer all levels of user a new dimension of access and usability to support and enhance research.</p>  
			
		<!-- Titlepage Start -->
		<div class="titlepage_heading"><h1>Key Features</h1></div>
		<!-- Titlepage End -->		
		<ul>
			<li><span class="icons_img bullet01">&nbsp;</span>Thousands of front &amp; backlist titles</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Dynamic content and feature set, with frequent addition of new titles being and regular functionality enhancements</li>	
            <li><span class="icons_img bullet01">&nbsp;</span>Titles from across all Cambridge world renowned subject areas</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Flexible purchase plans and custom packages</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Powerful quick search, advanced search and browse capabilities</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Comprehensive library support tools including downloadable MARC records, usage reports and access &amp; authentication methods</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Compliance with all major industry standards and initiatives</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Extensive user functionality including hyperlinked references &amp; personalisation features</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Dedicated customer support teams</li>
            <li><span class="icons_img bullet01">&nbsp;</span>Enhanced discoverability tools</li>
        </ul>
        
		<!-- Titlepage Start -->
		<div class="titlepage_heading"><h1>Further Information</h1></div>
		<!-- Titlepage End -->		
		<ul>
			<li><span class="icons_img bullet01">&nbsp;</span><a href="#" onclick="showHowToPurchase(); return false;">How to purchase</a></li>
			<c:url var="registration_url" value="${contextRootCjo}registration">
				<c:param name="SIGNUP" value="Y" />
			</c:url>
			<li><span class="icons_img bullet01">&nbsp;</span><a href="${registration_url}" >Sign up for a free trial</a></li>
			<%--<li><a href="#" onclick="showViewGuidedTour(); return false;">View our guided tour</a></li> --%>										
			<li><span class="icons_img bullet01">&nbsp;</span><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;">Frequently Asked Questions (FAQs)</a></li>
			<li><span class="icons_img bullet01">&nbsp;</span><a href="${pageContext.request.contextPath}/contact.jsf" >Contact Us</a></li>
			<li><span class="icons_img bullet01">&nbsp;</span><a href="http://www.cambridge.org/online/"  target="_blank">Other online products from Cambridge</a></li>		
        </ul>
	</div>
	<!-- Main Body End -->
	
	<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	</div>
	</div>

	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	</div>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>

	<script type="text/javascript">
		$(".topmenu_about").attr("class", "topmenu_active");
		
		function showHowToPurchase() {
			var messageId = 1556;
			if(window.location.href.indexOf("ebooks.cambridge.org") > -1 || window.location.href.indexOf("ebooks-test.cup.cam.ac.uk") > -1) {
				messageId = 2364;
			}
			MM_openWindow('${pageContext.request.contextPath}/popups/news.jsf?messageId=' + messageId,'popup','status=yes,scrollbars=yes,width=750,height=700');
		}

		function showViewGuidedTour() {
			var messageId = 1560;
			if(window.location.href.indexOf("ebooks.cambridge.org") > -1) {
				messageId = 2368;
			}
			MM_openWindow('${pageContext.request.contextPath}/popups/news.jsf?messageId=' + messageId,'popup','status=yes,scrollbars=yes,width=750,height=700');
		}
	</script>
	
</body>
</f:view>
</html>