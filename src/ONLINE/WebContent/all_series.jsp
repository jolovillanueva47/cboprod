<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>	
	${seriesBean.initAlphaMap}
	${seriesBean.initAllSeriesPage}
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>	
	    
	</head>

	<body>
		
		<c:set var="helpType" value="child" />
		<c:set var="pageId" value="1541" />
		<c:set var="pageName" value="All Series" />
		
		<c:import url="/components/loader.jsp" />
		<div id="main" style="display: none;"> 
			<div id="top_container">
				<div id="page-wrapper">	
				
					<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>
			    	
					<!-- Header Start -->
					<div id="header_container">				        
						<f:subview id="subviewTopMenu">
							<c:import url="/components/top_menu.jsp" />	
						</f:subview>						
				           
						<f:subview id="subviewSearchContainer">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
				    </div> 
				    <!-- Header End -->
	       			
			        <!-- Main Body Start -->
					<div id="main_container">
						
			        	<!-- Titlepage Start -->
			            <span class="titlepage_heading"><h1>All Series</h1></span>
			            <!-- Titlepage End -->
			            
			            <!-- Breadcrumbs Start -->
						<f:subview id="crumbtrailContainer">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>						
						<!-- Breadcrumbs End -->  
						
		            	<form id="browse_all_series_form" action="" method="post">
			            	<!-- Pager Menu Start -->
			                <f:subview id="subviewPagerMenuContainer">
								<c:import url="/components/pager.jsp" />
							</f:subview>
							<!-- Pager Menu End -->
						</form>
			            			            
			            <!--<p><b>Select your series by clicking on a letter.</b></p>-->            
					
						<div id="content_wrapper01"> 
						
				            <!-- Border Line01 Start -->
				            <div id="border01_container">  
				            	<!-- Alpha List Start -->
				                <f:subview id="subviewAlphaListContainer">
									<c:import url="/components/series_alphalist_sub.jsp" />
								</f:subview>
								<!-- Alpha List End -->	                
							</div>  
				        	<!-- Border Line01 End -->   
							
							<!-- Letter Heading Start -->
							<div id="content_wrapper09"> 
				           		<h1><c:out value="${seriesBean.firstLetter}" /></h1> 
							</div>
							<!-- Letter Heading End -->
							
							<!-- Series Count Start -->
							<f:subview id="subviewSeriesCountContainer">
								<c:import url="/components/series_count.jsp" />
							</f:subview>
							<!-- Series Count End -->
							
							<div class="clear"></div>
							
							<!-- Series Titles Start -->
							<c:choose>
								<c:when test="${seriesBean.resultsFound > 0}">
									<c:forEach var="book" items="${seriesBean.bookMetaDataList}">							
										<div id="content_wrapper10">
				           					<p>	
												<c:choose>
													<c:when test="${not empty book.seriesNumber && 'null' ne book.seriesNumber}">
														<c:url var="series_landing_url" value="${pageContext.request.contextPath}/series_landing.jsf">
															<c:param name="seriesCode" value="${book.seriesCode}" />
															<c:param name="seriesTitle" value="${book.series}" />
															<c:param name="sort" value="series_number" />
														</c:url>														
													</c:when>
													<c:otherwise>
														<c:url var="series_landing_url" value="${pageContext.request.contextPath}/series_landing.jsf">
															<c:param name="seriesCode" value="${book.seriesCode}" />
															<c:param name="seriesTitle" value="${book.series}" />
															<c:param name="sort" value="print_date" />
														</c:url>														
													</c:otherwise>
												</c:choose>
												<a href='<c:out value="${series_landing_url}" />'><c:out value="${book.series}" escapeXml="false" /></a>
				           	    			</p>	                          
										</div>  											
									</c:forEach>
								</c:when>
								<c:otherwise>
									<p>
										<h2>No results found.</h2>
									</p>
								</c:otherwise>
							</c:choose>
							<!-- Series Titles End -->
						</div>
					</div>  
					<!-- Main Body End -->
	        
			        <!-- Footermenu Start -->
					<f:subview id="subviewFooterMenuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>
			        <!-- Footermenu End -->
				</div> 
			</div>
			<div class="clear"></div>
		
			<!-- Footer Start -->
			<f:subview id="subviewFooterContainer">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>
			<!-- Footer End -->
		
		</div>
		
		<f:subview id="commonJsSubview">
			<c:import url="/components/common_js.jsp" />
		</f:subview>	
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/all_series.js"></script>
		<f:subview id="googleAnalyticsSubview">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
	</body>

</f:view>

</html>