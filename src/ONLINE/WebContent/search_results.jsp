<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ page import="org.cambridge.ebooks.online.jpa.rss.RssFaceted"%>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootSsl" option="current_dns_to_https" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
</head>

<body>	
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="86" scope="session" />
	<c:set var="pageName" value="Search Results" scope="session" />

	<c:set var="wrongFormat" value="&<span class='searchWord'>#" />
	<c:set var="rightFormat" value="<span class='searchWord'>&#" />
		
	<c:import url="/components/loader.jsp" />	
           		
	<div id="main" style="display: none;"> 
	
		<!-- bg Topline -->       
		<div id="top_container">       
			<div id="page-wrapper">
			
      			<f:subview id="subviewIPLogo">
					<c:import url="/components/ip_logo.jsp" />	
				</f:subview>
				
      			<!-- Header Start -->
				<div id="header_container">        	
					<f:subview id="subviewTopMenu">
						<c:import url="/components/top_menu.jsp" />	
					</f:subview>
			
					
					<f:subview id="subviewSearchContainer">
						<c:import url="/components/search_container.jsp" />	
					</f:subview>
			    </div> 
			    <!-- Header End -->
           		            	
            	<!-- Main Body Start -->
        		<div id="main_container">   	
			                   	    	
					<!-- Titlepage Start -->
					<div class="titlepage_heading"><h1>Search Results</h1></div>
					<!-- Titlepage End -->  
           		
	           		<!-- Breadcrumbs Start -->
					<f:subview id="crumbtrailContainer">
						<c:import url="/components/crumbtrail.jsp" />	
					</f:subview>
	           		<!-- Breadcrumbs End -->
           		
			        <!-- Print Header Start -->
			        <div id="print_header">Cambridge Books Online</div>
			        <!-- Print Header End -->
			        
			        <!-- Access Information Start -->
	                <div class="access_information">
	                	<strong>Access Information:</strong>
                        <ul>
                            <li><span class="icons_img purchase"><img src="${pageContext.request.contextPath}/images/icon_purchased.png" alt="Purchased Access" width="19" height="19" /></span>Purchased Access</li>
                        </ul>
	            	</div>
	                <!-- Access Information End -->
            		
            		<div class="clear"></div>
            		
            		<!-- show/hide search start -->    
            		<c:choose>
            			<c:when test="${'advance' eq param.searchType or not empty sessionScope.advanceSearchParam}">
            				<c:forEach var="target" items="${sessionScope.advanceSearchParam['target']}">
            					<input class="target" type="hidden" value="${target}" />
            				</c:forEach>
            				<c:forEach var="condition" items="${sessionScope.advanceSearchParam['condition']}">
            					<input class="condition" type="hidden" value="${condition}" />
            				</c:forEach>
            				<c:forEach var="logical_op" items="${sessionScope.advanceSearchParam['logical_op']}">
            					<input class="logical_op" type="hidden" value="${logical_op}" />
            				</c:forEach>
            				<c:forEach var="search_text" items="${sessionScope.advanceSearchParam['search_text']}">
            					<input class="search_text" type="hidden" value="${fn:replace(search_text, '"', '&quot;')}" />
            				</c:forEach>
            				<c:forEach var="search_text_2" items="${sessionScope.advanceSearchParam['search_text_2']}">
            					<input class="search_text_2" type="hidden" value="${fn:replace(search_text_2, '"', '&quot;')}" />
            				</c:forEach>
            				<c:forEach var="search_filter" items="${sessionScope.advanceSearchParam['search_filter']}">
            					<input class="search_filter" type="hidden" value="${search_filter}" />
            				</c:forEach>
            			</c:when>
            			<c:when test="${'quick' eq param.searchType or not empty sessionScope.searchText}">
            				<input class="target" type="hidden" value="all" />
            				<input class="condition" type="hidden" value="contain" />
            				<input class="logical_op" type="hidden" value="and" />      
            				<input class="search_text" type="hidden" value="${fn:replace(sessionScope.searchText, '"', '&quot;')}" />
            				<input class="search_text_2" type="hidden" value="" />
            			</c:when>
            		</c:choose>
           			<form id="advance_search_form" action="" method="post">
           				<div id="toggle_search">
		            		<table id="searchFilterTable">
					            <tr>
					            	<td class="advanced_search_checkbox">
					            		<ul>
							            	<li><input class="s_filter" name="sfilter" type="checkbox" value="book" />&nbsp;<label>Cambridge Books Online</label></li>
							            	<li><input class="s_filter" name="sfilter" type="checkbox" value="clc" />&nbsp;<label>Cambridge Library Collection</label></li>
							            	<li><input class="s_filter" name="sfilter" type="checkbox" value="journal" />&nbsp;<label>Cambridge Journals Online</label></li>
						            	</ul>
					            	</td>
					            </tr>
				           	</table>
				            <table id="advance_query_option" class="queryOpt">	
				            	<tr><td></td></tr>				
							</table>
							<div class="form_button">
								<ul>                 
				                    <li id="searchButton"><span><a href="javascript:void(0);" class="bot_searching" title="Search">Search</a></span></li>
				                    <li id="clearAllButton"><span><a href="javascript:void(0);" class="bot_clear" title="Clear All">Clear All</a></span></li>
				                    <li id="removeButton"><span><a href="javascript:void(0);" class="bot_fewerchoices" title="Fewer Choices">Fewer Choices</a></span></li>
				                    <li id="addButton"><span><a href="javascript:void(0);" class="bot_morechoices" title="More Choices">More Choices</a></span></li>
				                </ul>				                
						    </div>							    
					    </div>
				    </form>
				    
			    	<div class="clear"></div>
			    	
			    	<div class="form_button toggle_search_link">
			    		<span><a href="javascript:void(0);" id="toggle_search_link">Show Search</a></span>
			    	</div>			    	
			    	<!-- show/hide search end -->			    	
			    	
			    	<div class="clear"></div>
			    	
			    	<!-- search results found start -->
            		<c:choose>
            			<c:when test="${searchResultBean.resultsFound eq 1}">
            				Your search returned <b>${searchResultBean.resultsFound}</b> result.
            			</c:when>
            			<c:otherwise>
            				Your search returned <b>${searchResultBean.resultsFound}</b> results.
            			</c:otherwise>
            		</c:choose>
            		<!-- search results found end -->
            		
            		<div id="content_wrapper01">                                
              
	                	<!-- Box Tab Start -->
	                  	<div id="border_container">                  
	                    	<div class="box">                                                                                
                            	<form id="search_result_form" method="post">
		                            <!-- Faceted Content Start --> 
		                            <div class="boxBody_faceted">                               
	                            	
	                                	<!-- Left Content Start -->
	                                	<div class="left_content_container">	                                		
	                                		<c:choose>
	                                			<c:when test="${searchResultBean.resultsFound gt 0 or 'multiple' eq param.filterType}">
	                                				<!-- Left Control Start -->
				                                    <div class="left_control">	
				                                    	<div class="form_button">
					                                        <ul>
					                                            <li><span><a href="javascript:void(0);" class="bot_submit" title="Submit">Submit</a></span></li>                                            
					                                            <li><span><a href="javascript:void(0);" class="bot_reset" title="Reset">Reset</a></span></li>
					                                            <li><span><a href="javascript:void(0);" class="bot_select_all" title="Select All">Select All</a></span></li>
					                                        </ul>	
				                                        </div>
				                                    </div>
				                                    <!-- Left Control End -->
	                                			</c:when>
	                                			<c:otherwise>
	                                				<!-- Left Control Start -->
				                                    <div class="left_control">	
				                                        <ul>
				                                            <li></li>
				                                        </ul>	
				                                    </div>
				                                    <!-- Left Control End -->
	                                			</c:otherwise>
	                                		</c:choose>
	                                    	<% 
												String userAgent = request.getHeader("User-Agent");
							                    session.setAttribute("prevPath",request.getRequestURL());
							                    session.setAttribute("sType",request.getParameter("searchType"));
							                    session.setAttribute("sText",request.getParameter("searchText"));
											%>
		                                    <div class="left_content">
		                                    	<!-- 
		                                    	<div class="partner_button">
		                                    		<c:url var="oso_url" value="http://www.oxfordscholarship.com/search/query">
		                                    			<c:param name="quickSearchText" value="${sessionScope.searchTerms}" />
		                                    		</c:url>
													<a title="Search on Oxford Scholarship Online" target="oso" href="${oso_url}">
														<img height="40" width="199" alt="Search on Oxford Scholarship Online" src="images/button_oso_partner.jpg">
													</a>
												</div>
		                                    	 -->
		                                    	<!-- Did you mean search generator start -->		                                    	
	                                        	<c:if test="${not empty searchResultBean.spellCheckTerm and 'null' ne searchResultBean.spellCheckTerm}">
			                                        <div class="did_you_mean_container">
			                                            <h1>Did you mean: <a id="spell_check" href="javascript:void(0);">${searchResultBean.spellCheckTerm}</a></h1>
			                                        </div>	                                        
	                                            </c:if>
		                                        <!-- Did you mean search generator end -->
		                                        
		                                        <%--
	                                            <!-- Search related generator start -->
	                                            <div class="search_related_container">
	                                                <h2>Searches related to <a href="#">China</a></h2>
	                                                <ul>
	                                                    <li><a href="#">facts about <span>china</span></a></li>
	                                                    <li><a href="#">history of <span>china</span></a></li>
	                                                    <li><a href="#">facts about <span>china</span></a></li>
	                                                    <li><a href="#"><span>china</span> map</a></li>
	                                                    <li><a href="#">the great wall of <span>china</span></a></li>
	                                                    <li><a href="#"><span>china</span> chow</a></li>
	                                                    <li><a href="#"><span>china</span> economy</a></li>
	                                                    <li><a href="#"><span>china</span> olympics</a></li>
	                                                    <li><a href="#"><span>china</span> earthquake</a></li>
	                                                </ul>
	                                            </div>
	                                            <!-- Search related generator end --> --%>  
	                                              
		                                    	<c:set var="filterDisplayMap" value="${facet.filterDisplayMap}" />
		                                    	<c:if test="${not empty filterDisplayMap}">		                                    		
			                                    	<ul class="remove_bot">
			                                    		<li><strong>Search Filters:</strong></li>
			                                            <c:forEach var="filters" items="${filterDisplayMap}" varStatus="status">
			                                            	<li><c:out value="${filters.value}" escapeXml="false" /> <a href="javascript:void(0);" class="remove_filter" id="${filters.key}">Remove</a></li>
			                                            </c:forEach>
			                                            <li><a href="javascript:void(0);" id="reset_filter">Remove All</a></li>
			                                        </ul> 
		                                        </c:if>
		                                        <c:set var="contentTypeList" value="${facet.contentTypeList}" />
		                                    	<c:if test="${not empty contentTypeList and fn:length(contentTypeList) > 0 and not empty contentTypeList[0]}">
			                                        <ul>
			                                            <li><strong>Content Type:</strong></li>
			                                            <c:forEach var="contentType" items="${contentTypeList}" varStatus="status">
			                                            	<c:if test="${'chapter' eq contentType.name or 'article' eq contentType.name}">
			                                            		<c:set var="filterId" value="content_type=COL=${contentType.name}" />
			                                            		<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_content_type" href="javascript:void(0);">${contentType.display} (${contentType.count})</a></li>
			                                            	</c:if>			                                            		                                 	
			                                            </c:forEach>
			                                        </ul> 
		                                        	<div class="clear"></div>
		                                        </c:if>		                                        
		                                        <c:set var="subjectFacetList" value="${facet.subjectFacetList}" />
		                                        <c:if test="${not empty subjectFacetList and fn:length(subjectFacetList) > 0}">
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Subject:</strong></li>
			                                            <c:forEach var="subject" items="${subjectFacetList}" varStatus="status">		
			                                            	<c:set var="filterId" value="subject_facet=COL=${subject.escapeName}" />                                            	
		                                           			<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_subject" href="javascript:void(0);"><c:out value="${subject.name}" escapeXml="false" /> (${subject.count})</a></li>
			                                            </c:forEach>                                    
			                                        </ul>  
		                                        	<div class="clear"></div> 
		                                        </c:if>
		                                        <c:set var="seriesFacetList" value="${facet.seriesFacetList}" />
		                                        <c:if test="${not empty seriesFacetList and fn:length(seriesFacetList) > 0}">
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Series:</strong></li>
			                                            <c:forEach var="series" items="${seriesFacetList}" varStatus="status">		
			                                            	<c:set var="filterId" value="series_facet=COL=${series.escapeName}" />                                              	
		                                           			<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_series" href="javascript:void(0);"><c:out value="${series.name}" escapeXml="false" /> (${series.count})</a></li>
			                                            </c:forEach>                                        
			                                        </ul>  
		                                        	<div class="clear"></div> 
		                                        </c:if>
		                                        <c:set var="authorNameFacetList" value="${facet.authorNameFacetList}" />
		                                        <c:if test="${not empty authorNameFacetList and fn:length(authorNameFacetList) > 0}">		                                        	
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Author:</strong></li>
			                                            <c:forEach var="author" items="${authorNameFacetList}" varStatus="status">	 
			                                            	<c:set var="filterId" value="author_name_facet=COL=${author.escapeName}" />                                            	
		                                           			<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_author" href="javascript:void(0);"><c:out value="${author.name}" escapeXml="false" /> (${author.count})</a></li>
			                                            </c:forEach>
			                                            <%--<li class="arrow_view_more"><a href="#"><strong>View more</strong></a></li>--%>                                             
			                                        </ul>  
			                                        <div class="clear"></div>
		                                        </c:if>
		                                        <c:if test="${(searchResultBean.resultsFound gt 0 and not empty facet.facetQueryList) or 'multiple' eq param.filterType}">
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Year:</strong></li>
			                                            <c:forEach var="printDate" items="${facet.facetQueryList}" varStatus="status">		
			                                            	<c:choose>
			                                            		<c:when test="${status.count > 6}">
			                                            			<c:set var="hideClass" value="hide_list" />
			                                            		</c:when>
			                                            		<c:otherwise>
			                                            			<c:set var="hideClass" value="" />
			                                            		</c:otherwise>
			                                            	</c:choose>	                         
		                                           			<c:set var="parsedDate1" value="${fn:replace(printDate, 'print_date:[', '')}" />
		                                           			<c:set var="parsedDate2" value="${fn:replace(parsedDate1, 'TO', '-')}" />   
		                                           			<c:set var="parsedDate3" value="${fn:replace(parsedDate2, ']', '')}" />                               			                          	
		                                           			<li class="${hideClass}"><input name="filter" type="checkbox" value="${fn:replace(printDate, ':', '=COL=')}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[fn:replace(printDate, ":", "=COL=")]}'>checked="checked"</c:if> /><a id="${printDate}" class="filter_year" href="javascript:void(0);">${parsedDate3} (${facet.facetQueryMap[printDate]})</a></li>		                                           			
			                                            </c:forEach>
			                                            <%--<li class="arrow_view_more"><a href="#"><strong>View more</strong></a></li>--%>                              
			                                        </ul>      
			                                        <div class="clear"></div>
		                                        </c:if>
		                                        <c:set var="bookTitleFacetList" value="${facet.bookTitleFacetList}" />
		                                        <c:if test="${not empty bookTitleFacetList and fn:length(bookTitleFacetList) > 0}">
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Top Books:</strong></li>
			                                            <c:forEach var="topBook" items="${bookTitleFacetList}" varStatus="status">				                                            	
			                                            	<c:set var="filterId" value="book_title_facet=COL=${topBook.escapeName}" />  
		                                           			<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_top_book" href="javascript:void(0);"><c:out value="${topBook.name}" escapeXml="false" /> (${topBook.count})</a></li>
			                                            </c:forEach>
			                                        </ul>   
			                                        <div class="clear"></div>
		                                        </c:if>
		                                        <c:set var="journalTitleFacetList" value="${facet.journalTitleFacetList}" />
		                                        <c:if test="${not empty journalTitleFacetList and fn:length(journalTitleFacetList) > 0}">
			                                        <ul class="leftmenu_form">
			                                            <li><strong>Top Journals:</strong></li>
			                                            <c:forEach var="topJournal" items="${journalTitleFacetList}" varStatus="status">	
			                                            	<c:set var="filterId" value="journal_title_facet=COL=${topJournal.escapeName}" />  		          
		                                           			<li><input name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${"checked" eq facet.categorySelectionMap[filterId]}'>checked="checked"</c:if> /><a id="${filterId}" class="filter_top_journal" href="javascript:void(0);"><c:out value="${topJournal.name}" escapeXml="false" /> (${topJournal.count})</a></li>
			                                            </c:forEach>
			                                        </ul>  
		                                        </c:if>
		                                        <div class="clear"></div>
		                                    </div>   
	                                                                    
	                                       	<c:choose>
	                                			<c:when test="${searchResultBean.resultsFound gt 0 or 'multiple' eq param.filterType}">
	                                				<!-- Left Control Start -->
				                                    <div class="left_control">	
				                                        <ul>
				                                            <li><a href="javascript:void(0);" class="bot_submit" title="Submit"></a></li>                                            
				                                            <li><a href="javascript:void(0);" class="bot_reset" title="Reset"></a></li>
				                                            <li><a href="javascript:void(0);" class="bot_select_all" title="Select All"></a></li>
				                                        </ul>	
				                                    </div>
				                                    <!-- Left Control End -->
	                                			</c:when>
	                                			<c:otherwise>
	                                				<!-- Left Control Start -->
				                                    <div class="left_control">	
				                                        <ul>
				                                            <li></li>
				                                        </ul>	
				                                    </div>
				                                    <!-- Left Control End -->
	                                			</c:otherwise>
	                                		</c:choose>
	                                	</div>       
	                                	<!-- Left Content End -->  
	                                	<c:set var="reader" value="${bookBean.cookieValue}"/>                
	                                	<div class="other_control">              
	                                        <c:if test="${searchResultBean.resultsFound gt 0}" >
	                                            <ul class="other_control_single">
													<li><span class="icons_img rss_on">&nbsp;</span><a href="${pageContext.request.contextPath}/popups/view_rss.jsf?keepThis=true&amp;<%=RssFaceted.SEARCH_TYPE%>=<%=RssFaceted.RSS%>&amp;TB_iframe=true&amp;width=650&amp;height=200" class="thickbox">Save Query as RSS Feed</a></li>
													<c:if test='<%=userAgent.indexOf("iPad") != -1 %>'> 
														<li><span class="icons_img choose_reader">&nbsp;</span><a href="<%=request.getContextPath() %>/ipad_Reader.jsf?TB_iframe=true&amp;width=620&amp;height=450>" class="thickbox" id="readPDF">Choose your PDF Reader</a></li>
													</c:if>
													
 												</ul>
	                                        </c:if>
                               	 		</div>
                                		<c:choose>
                                			<c:when test="${searchResultBean.resultsFound gt 0}">
			                               		<!-- Right Content Start -->	                               		
			                                	<div class="right_content_container">      
				                                	<!-- Right Control Start -->
				                                    <div class="right_control">
				                                    	<ul>
				                                        	<li class="control_divider01">Go to page <input size="5" id="goToPageTop" class="go_to_page" type="text" value="${searchResultBean.currentPage}"> of ${searchResultBean.pages}</li>
				                                            <li class="control_divider02"><a href="javascript:void(0);" id="go_btn_top" class="bot_go_faceted" title="Click here to go!"></a></li>
				                                            <li class="control_divider01">Results per page: 
																<select class="results_per_page">
				                                  					<option value="50">50</option>   
				                                  					<option value="75">75</option>
				                                  					<option value="100">100</option>
				                              					</select>
				                                            </li>
				                                            <!-- 
				                                            <li class="control_divider01">Sort by:
				                                            	<select class="sort_type" >
				                                                	<option value="score" selected="selected">Relevancy</option>
				                                                	<option value="main_title_alphasort">Title</option>
				                                                	<option value="author_alphasort">Author</option>
				                                                	<option value="print_date_range">Print Publication Year</option>
				                                                	<option value="online_date_range">Online Publication Date</option>
				                                                </select>
				                                            </li>
				                                             -->
				                                            <li class="control_divider03">
				                                            	<c:choose>
																	<c:when test="${searchResultBean.hasFirst}">
																		<a href="javascript:void(0);" class="page_nav" name="1">First</a>
																	</c:when>
																	<c:otherwise>
																		First
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasPrevious}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.currentPage-1}">Previous</a>
																	</c:when>
																	<c:otherwise>
																		Previous
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasNext}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.currentPage+1}">Next</a>
																	</c:when>
																	<c:otherwise>
																		Next
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasLast}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.pages}">Last</a>
																	</c:when>
																	<c:otherwise>
																		Last
																	</c:otherwise>
																</c:choose>		                                            	
				                                            </li>
				                                        </ul>
				                                    </div>
			                                    	<!-- Right Control End -->
			                                    
				                                    <!-- Right Content Start -->
				                                    <div class="right_content">
				                                    	
				                                    	<c:forEach var="result" items="${searchResultBean.searchResultList}" varStatus="status">
				                                    		<c:choose>
				                                    			<c:when test="${'chapter' eq fn:toLowerCase(result.contentType)}">
				                                       				<!-- Ebooks Content Info Start -->
				                                    				<div class="ebook_list">
				                                    					<input id="debugAccess" type="hidden" value="${result.accessType}" />
				                                    					<o:isPurchasedAccess accessType="${result.accessType}">
				                                    						<div class="icons_img purchase_faceted "><img src="${pageContext.request.contextPath}/images/icon_purchased.png" alt="Purchased Access" width="19" height="19" /></div>
				                                    					</o:isPurchasedAccess>                                        
						                                    			<ul>
						                                                    <li class="list_type">
						                                                		<a href="${pageContext.request.contextPath}/ebook.jsf?bid=${result.bookId}"><c:out value="${fn:replace(result.bookTitle, wrongFormat, rightFormat)}" escapeXml="false" /></a>
						                                                		<c:choose>
						                                                			<c:when test="${publisherIdCbo eq result.bookMetaData.publisherId}">						                                                				
						                                                        		<img src="${pageContext.request.contextPath}/images/icon_book.gif" alt="Book" width="32" height="13" />
						                                                			</c:when>
						                                                			<c:when test="${publisherIdClc eq result.bookMetaData.publisherId}">						                                                				
						                                                        		<img src="${pageContext.request.contextPath}/images/icon_clc.gif" alt="CLC" width="32" height="13" />
						                                                			</c:when>
						                                                		</c:choose>
						                                                    </li>						                         
						                                                    <c:set var="hasComma" value="false"/>  
						                                                    <li>
						                                                    	<b>
								                                                    <c:if test="${not empty result.bookVolumeNumber and 'null' ne result.bookVolumeNumber and '0' ne result.bookVolumeNumber}">
																						Volume <c:out value="${result.bookVolumeNumber}" escapeXml="false" />
																						<c:set var="hasComma" value="true"/>
																					</c:if>                         
																					<c:if test="${not empty result.bookVolumeTitle}">
																						<c:if test="${hasComma}">, </c:if><c:out value="${result.bookVolumeTitle}" escapeXml="false"/>
																						<c:set var="hasComma" value="true"/>
																					</c:if>
																					<c:if test="${not empty result.bookPartNumber and 'null' ne result.bookPartNumber and '0' ne result.bookPartNumber}">
																						<c:if test="${hasComma}">, </c:if>Part <c:out value="${result.bookPartNumber}" escapeXml="false" />
																						<c:set var="hasComma" value="true"/>
																					</c:if>
																					<c:if test="${not empty result.bookPartTitle}">
																						<c:if test="${hasComma}">, </c:if><c:out value="${result.bookPartTitle}" escapeXml="false"/>
																						<c:set var="hasComma" value="true"/>
																					</c:if>
																					<c:if test="${not empty result.bookEdition and 'null' ne result.bookEdition}">
																						<c:if test="${hasComma}">, </c:if><c:out value="${result.bookEdition}" escapeXml="false" />
																						<c:set var="hasComma" value="true"/>
																					</c:if>
									                                                <c:if test="${not empty result.bookSubtitle and 'null' ne result.bookSubtitle}">
									                                                	<c:if test="${hasComma}">, </c:if><c:out value="${result.bookSubtitle}" escapeXml="false" />
									                                                	<c:set var="hasComma" value="true"/>
									                                                </c:if>
								                                                </b>
																			</li>
																			<li><c:out value="${result.authorSingleline}" escapeXml="false"/></li>
					                                            		</ul>
						                                                <div class="ebook_info">
							                                                <ul class="${result.id}_hide" style="display: none;">
								                                                <li><strong>Print Publication Year:</strong> ${result.bookPrintDate}</li>
								                                                <li><strong>Online Publication Date:</strong> ${result.bookOnlineDate}</li>								                                               
							                                                </ul>
							                                                <ul class="${result.id}_hide" style="display: none;">
								                                                <li><strong>Online ISBN:</strong> ${result.bookHighlightedOnlineIsbn}</li>
								                                                <li><strong>Print ISBN:</strong> ${result.bookPrintIsbn}</li>
							                                                </ul>							                                                
						                                               	 	<div class="clear"></div>
						                                                </div>
						                                                <c:if test="${not empty result.bookSeries and 'null' ne result.bookSeries}">
						                                                	<p class="${result.id}_hide" style="display: none;">
						                                                		<strong>Series:</strong>
						                                                		<c:choose>
																					<c:when test="${not empty result.bookSeriesNumber and 'null' ne result.bookSeriesNumber and '0' ne result.bookSeriesNumber}">
																						<a href="${pageContext.request.contextPath}/series_landing.jsf?seriesCode=${result.bookSeriesCode}&amp;seriesTitle=${result.bookSeries}&amp;sort=series_number" class="link03"><c:out value=" ${result.bookHighlightedSeries}" escapeXml="false" /></a><c:out value=" (No. ${result.bookSeriesNumber})" />
																					</c:when>
																					<c:otherwise>
																						<a href="${pageContext.request.contextPath}/series_landing.jsf?seriesCode=${result.bookSeriesCode}&amp;seriesTitle=${result.bookSeries}&amp;sort=print_date" class="link03"><c:out value=" ${result.bookHighlightedSeries}" escapeXml="false" /></a>
																					</c:otherwise>
																				</c:choose>
						                                                	</p>
						                                                </c:if>	
						                                            	<ul class="ebook_chapterlist">
							                                            	<li class="chapter_name">
							                                                	<a href="${pageContext.request.contextPath}/chapter.jsf?bid=${result.bookId}&amp;cid=${result.id}"><c:if test="${not empty result.chapterLabel}">${result.chapterLabel} - </c:if><c:out value="${result.chapterTitle}" escapeXml="false" /></a><br>
							                                                	<c:if test="${not empty result.chapterSubtitle and 'null' ne result.chapterSubtitle}">
							                                                		<c:out value="${result.chapterSubtitle}" escapeXml="false"/><br />
							                                                	</c:if>
							                                                	<c:if test="${not empty result.contributor and 'null' ne result.contributor}">
							                                                		Contributed by <c:out value="${result.contributor}" escapeXml="false"/><br />
							                                                	</c:if>
							                                                    <strong>Chapter DOI:</strong> <a href="http://dx.doi.org/${result.chapterDoi}">http://dx.doi.org/${result.chapterDoi}</a><br/><br/>
							                                                    <span class="Z3988" title="${result.bookMetaData.coinString}">&nbsp;</span>
							                                    			</li>
							                                                <li class="${result.id}_hide" style="display: none;"><c:out value="${result.chapterFulltext}" escapeXml="false" /></li>
							                                                
							                                                <li class="chapter_name">                                     		
						                                                   		<c:forEach var="chapters" items="${result.collapsedChapterList}" varStatus="status">
						                                                   			<c:choose>
						                                                   				<c:when test="${status.count eq 1}">						                                                   					
						                                                   					Other chapters with results of your search:<br />
						                                                   					<a href="chapter.jsf?bid=${result.bookId}&amp;cid=${chapters.id}"><c:if test="${not empty chapters.label}">${chapters.label} </c:if><c:out value="${chapters.title}" escapeXml="false"/></a><c:if test="${not status.last}">, </c:if>
						                                                   					
						                                                   				</c:when>						                                                   			
						                                                   				<c:otherwise>						                                                   					
						                                                   					<a href="chapter.jsf?bid=${result.bookId}&amp;cid=${chapters.id}"><c:if test="${not empty chapters.label}">${chapters.label} </c:if><c:out value="${chapters.title}" escapeXml="false"/></a><c:if test="${not status.last}">, </c:if>
						                                                   					
						                                                   				</c:otherwise>
						                                                   				 
						                                                   			</c:choose>	                                                   			
						                                                   		</c:forEach>
							                                                </li>
							                                            </ul>    
							                                            <!-- Show all details start -->
							                                          
							                                            <div class="show_all_container">
							                                            	<c:import url="/components/search/read_pdf.jsp">
							                                            		<c:param name="pageRange" value="${result.chapterPageRange}" />
						                                                   		<c:param name="access" value="${result.hasAccess}" />
						                                                   		<c:param name="bookId" value="${result.bookId}" />
						                                                   		<c:param name="chapterId" value="${result.id}" />
						                                                    </c:import>	
							                                                <ul class="show_all">
							                                           	  		<li><a class="link_view" name="${result.id}" href="javascript:void(0);" title="View more">View more<img src="${pageContext.request.contextPath}/images/icon_arrow_more_info.gif" alt="" width="13" height="10" /></a></li>
							                                            	</ul>							                                                
							                                                <div class="clear"></div>
							                                            </div>
							                                            <!-- Show all details end -->
							                                            
							                                            <div class="clear"></div>            
						                                    	 	</div>
				                                        			<!-- Ebooks Content Info End -->
				                                    			</c:when>
				                                    			<c:when test="${'article' eq fn:toLowerCase(result.contentType)}">
				                                    				<!-- Journals Content Info Start -->
							                                        <div class="journal_list">
							                                        	<ul>
						                                                	<li class="list_type">
						                                                		<c:url var="url_abstract" value="${domainCjo}action/displayAbstract">
						                                                			<c:param name="fromPage" value="online" />
						                                                			<c:param name="aid" value="${result.id}" />
						                                                			<c:param name="fulltextType" value="${result.articleType}" />
						                                                			<c:param name="fileId" value="${result.articleFileId}" />
						                                                		</c:url>
						                                                		<a target="cjo_abstract" href="${url_abstract}"><c:out value="${fn:replace(result.articleHighlightedTitle, wrongFormat, rightFormat)}" escapeXml="false" /></a>
						                                                        <img src="${pageContext.request.contextPath}/images/icon_journal.gif" alt="Journal" width="40" height="13" />
						                                                    </li>
							                                                <li><c:out value="${result.author}" escapeXml="false" /></li>
							                                                <c:set var="lowerCasedIssueNum" value="${fn:toLowerCase(result.articleIssueNumber)}" />
							                                                <c:choose>
							                                                	<c:when test="${(fn:startsWith(lowerCasedIssueNum, 's') or fn:startsWith(lowerCasedIssueNum, 'i')) and 'IAU' ne result.journalId}">
							                                                		<c:set var="issueWordings" value="Supplement" />
							                                                	</c:when>
							                                                	<c:otherwise>
							                                                		<c:set var="issueWordings" value="Issue" />
							                                                	</c:otherwise>
							                                                </c:choose>
							                                                <c:url var="url_journal" value="${domainCjo}action/displayJournal">
							                                                	<c:param name="jid" value="${result.journalId}" />
							                                                </c:url>
							                                                <li><a target="cjo_journal" href="${url_journal}"><c:out value="${result.journalTitle}" escapeXml="false"/></a>,
							                                                <c:choose>
							                                                	<c:when test="${'-1' eq result.articleVolumeNumber and '-1' eq result.articleIssueNumber}">
							                                                		<c:url var="url_firstview" value="${domainCjo}action/displayIssue">
							                                                			<c:param name="jid" value="PHN" />
							                                                			<c:param name="volumeId" value="-1" />
							                                                			<c:param name="seriesId" value="0" />
							                                                			<c:param name="issueId" value="-1" />
							                                                		</c:url>
							                                                		<a target="cjo_firstview" href="${url_firstview}"><i>FirstView Articles</i></a>, 
							                                                	</c:when>
							                                                	<c:otherwise>
							                                                		<c:url var="url_issue" value="${domainCjo}action/displayIssue">
							                                                			<c:param name="jid" value="${result.journalId}" />
							                                                			<c:param name="volumeId" value="${result.articleVolumeNumber}" />
							                                                			<c:param name="seriesId" value="0" />
							                                                			<c:param name="issueId" value="${result.articleIssueNumber}" />
							                                                		</c:url>
							                                                		<c:url var="url_volume" value="${domainCjo}action/displayBackIssues">
							                                                			<c:param name="jid" value="${result.journalId}" />
							                                                			<c:param name="volumeId" value="${result.articleVolumeNumber}" />
							                                                		</c:url>
							                                                		<a target="cjo_volume" href="${url_volume}">Volume ${result.articleVolumeNumber}</a>, <c:if test="${'-1' ne result.articleIssueNumber}"><a target="cjo_issue" href="${url_issue}">${issueWordings} ${result.articleIssueNumber}</a>, </c:if>
							                                                	</c:otherwise>
							                                                </c:choose>
							                                                ${result.articlePrintDate}<c:if test="${not empty result.articlePageStart or 'null' ne  result.articlePageStart}">, pp ${result.articlePageStart}<c:if test="${not empty result.articlePageEnd or 'null' ne result.articlePageEnd}">-${result.articlePageEnd}</c:if></c:if></li>							                                               
							                                                <li><strong>DOI:</strong> <a href="http://dx.doi.org/${result.articleDoi}">http://dx.doi.org/${result.articleDoi}</a> <a target="cjo_about_doi" href="${domainCjo}action/stream?pageId=3624&amp;level=2#30">(About doi)</a>, Available on CJO ${result.articleOnlineDate}</li>
							                                                <li>
							                                                	<div class="journal_preview">
							                                                        <ul>
							                                                            <li><a href="javascript:void(0);" class="preview">Preview</a></li>
							                                                            <li><a target="cjo_abstract" href="${url_abstract}">Abstract</a></li>
							                                                            <%--<li><a target="cjo_errata" href="${domainCjo}action/displayErrata?cupCode=1&amp;type=4&amp;jid=${result.journalId}&amp;volumeId=${result.articleVolumeNumber}&amp;issueId=${result.articleIssueNumber}&amp;aid=${result.id}">Errata</a></li>
							                                                        	<li><a target="cjo_related_articles" href="${domainCjo}action/displayRelatedArticles?cupCode=1&amp;type=4&amp;jid=${result.journalId}&amp;volumeId=${result.articleVolumeNumber}&amp;issueId=${result.articleIssueNumber}&amp;aid=${result.id}">Related Articles</a></li> 
							                                                        	<li><a target="cjo_supplementary_data" href="${domainCjo}action/displaySuppMaterial?cupCode=1&amp;type=4&amp;jid=${result.journalId}&amp;volumeId=${result.articleVolumeNumber}&amp;issueId=${result.articleIssueNumber}&amp;aid=${result.id}">Supplementary Data</a></li>--%>  
							                                                        	<c:set var="rightTitle" value="${fn:replace(result.articleTitle, '', '&quot;')}" />		
																						<c:set var="rightTitle" value="${fn:replace(rightTitle, '\"', '')}" />
																						<c:set var="rightau" value='${fn:replace(result.author, "\'", "&rsquo;")}' />
							                                                        	<c:url var="urlReqPermission" value="https://s100.copyright.com/AppDispatchServlet">
							                                                        		<c:param name="publisherName" value="CUP" />
							                                                        		<c:param name="publication" value="${result.journalId}" />
							                                                        		<c:param name="title" value="${rightTitle}" />
							                                                        		<c:param name="publicationDate" value="${result.articleOnlineDate}" />
							                                                        		<c:param name="author" value="${rightau}" />
							                                                        		<c:param name="copyright" value="Cambridge Journals" />
							                                                        		<c:param name="contentID" value="${result.articleDoi}" />
							                                                        		<c:param name="startPage" value="${result.articlePageStart}" />
							                                                        		<c:param name="endPage" value="${result.articlePageEnd}" />
							                                                        		<c:param name="orderBeanReset" value="True" />
							                                                        		<c:param name="volumeNum" value="${result.articleVolumeNumber}" />
							                                                        		<c:param name="issueNum" value="${result.articleIssueNumber}" />
							                                                        	</c:url>
							                                                        	<li><a href="javascript:void(0);" onclick="openWindow('${urlReqPermission}');">Request Permissions</a></li>
							                                                        </ul>
							                                                        <c:set var="abstractFilename" value="${fn:replace(result.articleAbstractFilename, 'w.htm', 'h.htm')}" />
							                                                        <c:set var="abstractFilename" value="${fn:replace(abstractFilename, 'r.htm', 'h.htm')}" />
							                                                        <c:set var="abstractFilename" value="${fn:replace(abstractFilename, 's.htm', 'h.htm')}" />
							                                                        <div class="preview_abstract" style="display: none;">
								                                                        <p class="journal_preview" style="display: none;" id="${abstractFilename}"> <strong>Abstract</strong><img name="image_${abstractFilename}" src="${pageContext.request.contextPath}/images/circle_loading.gif" style="display: block;" /><br /> 
								                                                        </p>
							                                                        </div>
							                                                    </div>
							                                                </li>
							                                            </ul>
							                                        </div>
							                                        <!-- Journals Content Info End -->
				                                    			</c:when>
				                                    		</c:choose>	                                    		
				                                    	</c:forEach>                                        
			                                    	</div>
			                                    	<!-- Right Content End -->
			                                    
				                                    <!-- Right Control Start -->
				                                    <div class="right_control">
				                                    	<ul>
				                                        	<li class="control_divider01">Go to page <input size="3" id="goToPageBottom" class="go_to_page" type="text" value="${searchResultBean.currentPage}"> of ${searchResultBean.pages}</li>
				                                            <li class="control_divider02"><a href="javascript:void(0);" id="go_btn_bottom" class="bot_go_faceted" title="Click here to go!"></a></li>
				                                            <li class="control_divider01">Results per page: 
																<select class="results_per_page">
				                                  					<option value="50">50</option>   
				                                  					<option value="75">75</option>
				                                  					<option value="100">100</option>
				                              					</select>
				                                            </li>
				                                            <!-- 
				                                            <li class="control_divider01">Sort by:
				                                            	<select class="sort_type">
				                                                	<option value="score" selected="selected">Relevancy</option>
				                                                	<option value="main_title_alphasort">Title</option>
				                                                	<option value="author_alphasort">Author</option>
				                                                	<option value="print_date_range">Print Publication Year</option>
				                                                	<option value="online_date_range">Online Publication Date</option>
				                                                </select>
				                                            </li>
				                                             -->
				                                            <li class="control_divider03">
				                                            	<c:choose>
																	<c:when test="${searchResultBean.hasFirst}">
																		<a href="javascript:void(0);" class="page_nav" name="1">First</a>
																	</c:when>
																	<c:otherwise>
																		First
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasPrevious}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.currentPage-1}">Previous</a>
																	</c:when>
																	<c:otherwise>
																		Previous
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasNext}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.currentPage+1}">Next</a>
																	</c:when>
																	<c:otherwise>
																		Next
																	</c:otherwise>
																</c:choose>
																 | 
																<c:choose>
																	<c:when test="${searchResultBean.hasLast}">
																		<a href="javascript:void(0);" class="page_nav" name="${searchResultBean.pages}">Last</a>
																	</c:when>
																	<c:otherwise>
																		Last
																	</c:otherwise>
																</c:choose>		                                            	
				                                            </li>
				                                        </ul>
				                                    </div>
				                                    <!-- Right Control Start -->				                                    
				                                </div>
				                                <!-- Right Content End -->     
	                                    	</c:when>
                                			<c:otherwise>
												<br />
												<div class="alert_msg">No results found.</div>
												<br />
                                			</c:otherwise>
                                		</c:choose>         
	                            		<div class="clear"></div>
	                            	</div>
	                            	<!-- Faceted Content End -->
                            	</form>                            
                        	</div>
                    	</div> 
                    	<!-- Box Tab End -->
          			</div>  
    			</div>  
        		<!-- Main Body End -->     
        
		    	<!-- Footermenu Start -->
		        <f:subview id="subviewFooterMenuwrapper">
					<c:import url="/components/footer_menuwrapper.jsp" />
				</f:subview>
		        <!-- Footermenu End -->
        
			</div> 
		</div>
    
    	<div class="clear"></div>    
    
	    <!-- Footer Start -->
	   	<f:subview id="subviewFooterContainer">
			<c:import url="/components/footer_container.jsp" />
		</f:subview>	
	    <!-- Footer End -->    
   	</div>
   	
   	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js" ></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.alphanumeric.js"></script>	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/search_results.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/search_advance.js"></script>	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>	
</body>

</f:view>
</html>