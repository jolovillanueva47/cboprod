<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	
	<style type="text/css" media="screen">
		label.error {color:#f00;}
		input.error {border: 1px dotted #f00;}
	</style>
</head>
<body>
	<div id="page-wrapper">
    	
	<!-- Header Start -->
	<div id="header_container">
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>

		<f:subview id="subviewLoginContainer">
			<c:import url="/components/login_container.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
        
        <!-- Header End -->
        <!-- Main Body Start -->

	<div id="main_container">
		<div class="titlepage_about" ></div>
		
		<form action="" id="signup">
			<div id="register">
				<p>Please take a minute to register. You will only have to do this once for yourself or your institution.</p>  
				<p>Note:</p>
				<p>* The username and password you select must each have a minimum of four characters, are case-sensitive and can be any combination of letters and numbers (e.g. 1xE24tY)</p>
				<p>* If you already have a username for Cambridge Journals Online, you can log in to Cambridge Books Online using your existing CJO username.</p>
				<p>* Required Information(*)</p>
				
				<table cellspacing="0" width="100%" border="0">
					  <tr>
  						<td class="border" colspan="2"></td>	
  					  </tr>
  					  <tr>
  					  	<th align="left" colspan="2">Organisational Information</th>
  					  </tr>
  					  <tr>
					    <th align="right"><label for="org">Organisation*</label></th>
					    <td><input type="text" name="org" class="register_field" id="org" maxlength="80" /></td>
					  </tr>  
					  <tr class="border">
					    <th align="right"><label for="country">Country*</label></th>
					    <td  >
					    	<input type="text" name="country" class="register_field"/>
					    </td>
					  </tr>
					  <tr class="border">
					    <th align="right"><label for="country">County / State / Province*</label></th>
					    <td  >
					    	<input type="text" name="county" class="register_field"/>
					    </td>
					  </tr>
					  <tr>
					    <th align="right"><label for="city">City*</label></th>
					    <td><input type="text" name="city" class="register_field" id="city" maxlength="48" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="address">Address*</label></th>
					    <td><input type="text" name="address" class="register_field" id="address" maxlength="80" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="address2">Address 2</label></th>
					    <td><input type="text" name="address2" class="register_field" id="address2" maxlength="80" /></td>
					  </tr>
					  <tr>
  						<td class="border" colspan="2"></td>	
  					  </tr>
  					   <tr>
  					  	<th align="left" colspan="2">Organistion Administrator Information</th>
  					  </tr>
  					  <tr>  					  
					    <th align="right"><label for="firstName">First Name*</label></th>    
					    <td><input type="text" name="firstName" class="register_field" id="firstName" maxlength="40" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="surName">Last Name*</label></th>
					    <td><input type="text" name="surName" class="register_field" id="surName" maxlength="40" /></td>
					  </tr>	
					  <tr>
					  	<td  class="border" colspan="2"></td>
					  </tr>
					 
					  <tr class="border">
					    <th align="right"><label for="userName_reg">User Name*</label></th>
					    <td><p class="note">
					    <input type="text" name="userName" class="register_field" id="userName_reg" maxlength="24" />
		  					case-sensitive, 4 to 24 characters only
		  				</p></td>
		  			  <tr>
					    <th align="right"><label for="passWord_reg">Password*</label></th>
					    <td><p class="note">
						    <input type="password" name="passWord" class="register_field" id="passWord_reg" maxlength="24" />
				  	case-sensitive, 4 to 24 characters only
				  		</p></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="passWord2">Confirm Password*</label></th>
					    <td><input type="password" name="passWord2" class="register_field" id="passWord2" maxlength="24" /></td>
					  </tr>
					  
					  <tr>
					    <th align="right"><label for="email">Email*</label></th>
					    <td><input type="text" name="email" class="register_field" id="email" maxlength="80" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="email2">Confirm Email*</label></th>
					    <td><input type="text" name="email2" class="register_field" id="email2" maxlength="80" /></td>
					  </tr>
					  <tr>
					  <tr>
					  	<td  class="border" colspan="2"></td>
					  </tr>
					 			
					  <tr class="border">
					    <th align="right"><label for="country">Country*</label></th>
					    <td  >
					    	<input type="text" name="countryUser" class="register_field"/>
					    </td>
					  </tr>
					  <!-- 
					  <tr class="border">
					    <th align="right"><label for="country">County / State / Province*</label></th>
					    <td  >
					    	<input type="text" name="countyUser" class="register_field"/>
					    </td>
					  </tr>
					  <tr>
					    <th align="right"><label for="city">City*</label></th>
					    <td><input type="text" name="cityUser" class="register_field" id="cityUser" maxlength="48" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="address">Address*</label></th>
					    <td><input type="text" name="addressUser" class="register_field" id="addressUser" maxlength="80" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="address2">Address 2</label></th>
					    <td><input type="text" name="address2" class="register_field" id="address2" maxlength="80" /></td>
					  </tr>
					  -->
					  <tr>
					    <th align="right"><label for="telephone">Telephone</label>&nbsp;&nbsp;&nbsp;</th>
					    <td><input type="text" name="telephone" class="register_field" id="telephone" maxlength="48" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="fax">Fax&nbsp;&nbsp;&nbsp;</label></th>
					    <td><input type="text" name="fax" class="register_field" id="telephone" maxlength="48" /></td>
					  </tr>
					  <tr>
					    <th align="right"><label for="mobilePhone">Mobile Phone</label>&nbsp;&nbsp;&nbsp;</th>
					    <td><input type="text" name="mobilePhone" class="register_field" id="telephone" maxlength="48" />&nbsp;&nbsp;&nbsp;</td>
					  </tr>
					  <tr>
					    <th align="right"><label for="affiliation">Affiliation</label></th>
					    <td><input type="text" name="affiliation" class="register_field" id="org" maxlength="80" /></td>
					  </tr>					  		
					  <tr>
					  	<td colspan="2"></td>
					  </tr>
					  <tr class="border">						
						<td  class="check" colspan="2"><input type="checkbox" name="acceptTerms" id="acceptTerms" />
						<label for="acceptTerms">You must accept the <a href="#" onclick="javascript:openWindow('${pageContext.request.contextPath}/popups/terms_of_use_popup.jsf','popup','status=yes,scrollbars=yes');">Terms of Use</a> to register.*</label>
						<label class="error" for="acceptTerms" style="display:none">You must accept to proceed.</label></td>
					  </tr>						 
					  
				</table>
				<table cellspacing="0" class="register_buttons">
				  <tr>
					<td>
						<input type="reset" class="button" value="Reset" />
						<input type="submit" class="button" value="Signup for Trial" onclick="return validateRegistration()" name="registerButton" />
					</td>
				  </tr>
				</table>
			</div>
		</form>  
			
		</div>
		
		<!-- end content -->
		 <div class="clear_list"></div>
	
	
	<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	
	
	</div>


	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){	
			$.validator.addMethod("acceptTerms", 
					 function(value, element) {						
						var isChecked = $(element).attr("checked")
						if(!isChecked){
							alert("You must accept terms.");
						}
						return true;
					}, "" //nothing, this function alerts
				);
					
			$("#signup").validate({
				rules: {
				  org : "required",
				  country: "required",
				  county: "required",
				  city: "required",
				  address: "required",
				  firstName: "required",
				  surName: "required",
				  userName: "required",
				  passWord: "required",
				  passWord2: "required",
				  email: "required",
				  email2: "required",
				  //countryUser: "required",
				  //countyUser: "required",
				  //cityUser: "required",
				  //addressUser: "required",
				  acceptTerms: "acceptTerms"				  
				}
			});				
		});

		
						
	</script>
	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>
</f:view>
</html>