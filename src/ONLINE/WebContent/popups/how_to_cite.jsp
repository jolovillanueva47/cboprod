<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page import="java.net.URLEncoder"%>
<%@page import="org.cambridge.ebooks.online.util.EBooksUtil"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />

</head>

<body onload="javascript:initZero();">

<div class="camLogo"><a href="http://www.cambrige.org"><img src="${pageContext.request.contextPath}/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" /></a></div>

<div class="content">
	<ul>
		<li class="header"><div class="headerTools"><a href="javascript:window.close();"></a></div>Copy and paste this citation</li>
		<li id="citebody"><c:out value="${fn:trim(howToCiteBean.bookInfo.authorNameLfListAsString)}"/>. <c:out value="${fn:trim(howToCiteBean.bookInfo.title)}"/>. 
			Cambridge University Press, ${fn:trim(howToCiteBean.bookInfo.printDateDisplay)}. Cambridge Books Online. Cambridge University Press. <%= EBooksUtil.getCurrentDate("dd MMMMM yyyy") %>
			<c:choose>
				<c:when test="${not empty param.cid}"><a target="_blank" href="http://dx.doi.org/${howToCiteBean.chapterInfo.doi}">http://dx.doi.org/${howToCiteBean.chapterInfo.doi}</a></c:when>
				<c:otherwise><a target="_blank" href="http://dx.doi.org/${howToCiteBean.bookInfo.doi}">http://dx.doi.org/${howToCiteBean.bookInfo.doi}</a></c:otherwise>
			</c:choose> 
		</li>		
		<%-- <li><a onclick="javascript:window.parent.askMyParent();" href="javascript:void(0);">Copy</a></li> --%>
    </ul>
    <div id="d_clip_container" style="position:relative">
		<br/><div id="d_clip_button"><a href="javascript:ieClick();">Copy Text to Clipboard</a></div>
		<textarea id="holdtext" style="display:none;"></textarea>
	</div>
</div><%-- end content --%>

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ZeroClipboard.js"></script>
<script language="JavaScript">

		function ieClick() {
			holdtext.innerText = document.getElementById('citebody').innerText;
			Copied = holdtext.createTextRange();
			Copied.execCommand("Copy");
			alert('Copied citation.');
		}

		var clip = null;

		function initZero() {
			ZeroClipboard.setMoviePath("${pageContext.request.contextPath}/swf/ZeroClipboard.swf");

	        clip = new ZeroClipboard.Client();
	        clip.setHandCursor( true );
	        clip.addEventListener('mouseOver', my_mouse_over);
	        clip.addEventListener('onmouseup',my_mouse_up);
	        clip.glue( 'd_clip_button' );
	        document.onmouseover=clipReposition;
		}
		
        function my_mouse_over(client) {
        	
        	if (navigator.userAgent.match(/MSIE/)) {
        		clip.setText( document.getElementById('citebody').innerText);
        	}else{
        		clip.setText( document.getElementById('citebody').textContent);		
        	}	
        }
        
        function my_mouse_up(){
        	alert('Copied citation.');
        	if (navigator.userAgent.match(/MSIE/)){	
        		top.document.title = "Cambridge Books Online - Cambridge University Press";
        	}
                	
        }

        function clipReposition() {
        	clip.reposition();
        }
        	        

               
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>