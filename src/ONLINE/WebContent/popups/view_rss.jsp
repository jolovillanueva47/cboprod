<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.cambridge.ebooks.online.rss.search.faceted.RssServletFaceted"%>

<head>

	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />

</head>
<body>
<%
	String[] urlTitle = RssServletFaceted.getRssUrlTitle(request);
	String url = urlTitle[0]; 
	String title = urlTitle[1];
%>

<div class="content" style="height: 150px;"> 
	<ul>		 
    	<li class="header">Save Query as RSS Feed</li>
    	<li>You may copy this link to your RSS reader:</li>
       	<li class="bookLink" ><a id="bookLink" href="<%=url%>" target="_blank"><%=url %></a></li>
       	<li>&nbsp;</li>
       	<li>       		
       		Feed Title: <input type="text" id="feedTitle" value="<%=title %>" style="width:450px;" />
       	</li>
       	<!-- use the -->
       	<!-- 
       	<li>&nbsp;</li>    	    	
       	<li>You can also subscribe to this feed using:
       		<ul>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('bloglines')" alt="bloglines" src="${pageContext.request.contextPath}/images/bloglines.gif" />
       			</li>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('yahoo')" alt="yahoo" src="${pageContext.request.contextPath}/images/yahoo.gif" />
       			</li>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('google')" alt="google" src="${pageContext.request.contextPath}/images/google.gif" />
       			</li>
       		</ul>
       	</li>
       	 -->       	     	
	</ul>	
	<div>
		<span class="button">
	    	<input type="button" value="Update Title" onclick="updateTitle()"/>			
	    </span>
	</div>   		     	
</div>

	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
	<script type="text/javascript">
		<%--			
			String urlEncoded1 = URLEncoder.encode(url, "UTF-8");
			String urlEncoded2 = URLEncoder.encode(urlEncoded1, "UTF-8");
		--%>
		function saveRss(value){			
			var url = $(document.getElementById("bookLink")).attr("href");					
			//var urlEnc = encodeURIComponent(url);
			var urlEnc = encodeURIComponent(url);
			var newUrl = "";
			if(value == "bloglines") {
				newUrl = "http://www.bloglines.com/sub/" + url;																
			}else if(value == "google") {				 
				//custom replace all is required to be read by google and yahoo correctly
				//newUrl ="http://www.google.com/ig/add?feedurl="  + urlEnc.replace(/amp%3B/g,"");				
				newUrl ="http://www.google.com/ig/add?feedurl="  + urlEnc;
			}else if(value == "yahoo") {
				//newUrl = "http://add.my.yahoo.com/rss?url=" + urlEnc.replace(/amp%3B/g,"");					
				newUrl = "http://add.my.yahoo.com/rss?url=" + urlEnc;
			}
			MM_openWindow(newUrl,'newWindow','scrollbars=yes');
		}
		
		function updateTitle(){
			var bookLinkJQ = $(document.getElementById("bookLink"));
			var bookLinkStr = bookLinkJQ.attr("href");			
			var newUrl = removeTitle(bookLinkStr);
			var newTitle = document.getElementById("feedTitle").value;
			newUrl = newUrl + "<%=RssServletFaceted.FEED_TITLE_PARAM %>=" + newTitle;
			
			bookLinkJQ.html(newUrl);
			bookLinkJQ.attr("href", newUrl);
		}

		function removeTitle(url){			
			var newUrl = url.split("<%=RssServletFaceted.FEED_TITLE_PARAM %>")[0];
			return newUrl;
		} 
	</script>
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>