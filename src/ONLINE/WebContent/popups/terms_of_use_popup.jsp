<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambridge Books Online - Cambridge University Press</title>
<f:subview id="commonCssSubview">
	<c:import url="common_css.jsp" />
</f:subview>
</head>
<body>
<div class="camLogo"><a href="http://www.cambridge.org"><img src="${pageContext.request.contextPath}/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" /></a></div>
<br />
<br />
<br />
<%-- 
<f:subview id="headerSubview">
	<c:import url="/components/header.jsp" />
</f:subview>

<div id="crumbtrail">
  <ul>
    <li class="last">Terms of use</li>
  </ul>
</div><!-- end crumbtrail -->

<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="/components/ip_logo.jsp" />
	</f:subview>

	<f:subview id="recentlyUpdatedSeriesSubview">
		<c:import url="/components/recently_updated_series.jsp" />
	</f:subview>

</div><!-- end right nav -->
--%>

<div class="noBordercontent">
	<h1>Terms of Use</h1>
</div>

<div id="content">

    		<h2>Cambridge Books Online ('CBO')</h2>
			<h2>Access rights</h2>
			
				<p>The full texts of chapters available through a specific collection within CBO (referred to below as 'Content') may be accessed only by Authorised Users. An 'Authorised User' is defined as:</p>
				
				<ul class="normal_list">
				    <li>an individual who is affiliated with a purchasing institution as a current student, faculty member, library patron, or employee and who can access CBO through the secure network of a purchasing institution or from such other places where Authorised Users work or study (including but not limited to Authorised Users' offices and homes, halls of residence and student dormitories)</li>
				    <li>an individual who is permitted to use the computer terminals within the purchasing institution's library premises and as a result may access CBO on a temporary basis</li>
				    <li>an individual who is a member of a society that has arranged for access to CBO for its current members</li>
				</ul>
				
				<p>An Authorised User will be provided with a password or other authentication by its purchasing institution in order to access CBO.</p>
				
				<p>Tables of contents and extracts from chapters may be accessed free of charge by anyone accessing CBO.</p>
			
			<h2>Permitted uses</h2>
			
				<p>Authorised Users may access, search and view individual chapters for personal use only. Print, copy and download permissions may vary from collection to collection depending on the sensitivity and rights available for the Content.</p>
				
				<p>Unless otherwise stated, Authorised Users may make printed copies of either one chapter per title in the collection in question or up to 20% of the pages from the total collection, whichever is the greater, for personal use only, during any given four-week period.</p>
				
				<p>Unless otherwise stated, Authorised Users may copy and paste one chapter of each title in the collection in question, or up to 5% of the pages from the total collection, whichever is the greater, for personal use only, during any given four-week period.</p>
				
				<p>Authorised Users may download the Content to a maximum of 5 hand held devices for their personal use only.  However whilst Cambridge University Press permits the use of the Content on hand held devices, Cambridge University Press makes no warranty as to the Content's suitability for or use on such hand held devices and expressly excludes all liability towards Authorised Users in the event that the Content does not function properly or at all on such hand held devices.</p>
				
				<p>In the event that Authorised Users at an institution open more than 100 pdfs forming part of the Content in any one hour period, an email shall be sent automatically to Cambridge University Press' administrators.  Cambridge's administrators reserve the right to contact the Authorised User's administrators on receipt of such email to ask your purchasing institution to investigate such usage and ensure it complies with these Terms of Use.  In the event that such usage does not comply with these Terms of Use, it will be considered a material breach and Cambridge University Press shall be entitled to terminate these Terms of Use as detailed below.</p>
				
				<h2>Prohibited uses</h2>
				
				<p>Authorised Users are not permitted to:</p>
				
				<ul class="normal_list">
				    <li>Remove or alter the authors' names or Cambridge University Press' copyright notices or other means of identification or disclaimers as they appear in the Content</li>
				    <li>Engage in systematic copying or downloading of the Content or transmit any part of the Content by any means to anyone who is not an Authorised User</li>
				    <li>Allow copies to be stored or accessed by anyone who is not an Authorised User</li>
				    <li>Mount or distribute any part of the Content on any electronic network, including without limitation the Internet and the World Wide Web, other than as specified in these Terms and Conditions</li>
				    <li>Make the Content available in any other form or medium or create derivative works without the written permission of Cambridge University Press. For permissions information, please follow the 'Rights and permissions' quick-link on the Cambridge website for your region, via <a href="http://www.cambridge.org" class="link03">http://www.cambridge.org</a></li>
				    <li>Alter, amend, modify or change the Content</li>
				    <li>Reverse engineer, decompile, disassemble or otherwise alter software</li>
				    <li>Maintain downloaded Content after expiration of your institution's access period</li>			    
				</ul>
				
				<p>Cambridge University Press's explicit written permission must be obtained in order to:</p>
				
				<ul class="normal_list">
					<li>Use all or any part of the Content for any commercial use</li>
					<li>Distribute the whole or any part of the Content to anyone other than Authorised Users</li>
					<li>Publish, distribute or make available the Content, works based on the Content or works which combine the Content with any other content, other than as permitted in these Terms and Conditions</li>
				</ul>
			
			<h2>Intellectual property rights</h2>
			
				<p>Users acknowledge that all rights relating to CBO are the sole and exclusive property of Cambridge University Press and that these Terms and Conditions do not convey any right, title or interest therein except the right to use CBO in accordance with the terms and conditions of the Access Agreement accepted by your institution.</p>
				
				<p>Users undertake to ensure that the intellectual property rights of the copyright holder and the software owners and the moral rights of the authors of the Content are not infringed.</p>
				
				<p>The Content in the form published on CBO is copyright � Cambridge University Press or published under exclusive license from the copyright holder by Cambridge University Press.</p>
				
				<p>Nothing in these Terms and Conditions shall in any way exclude, modify or affect any of your statutory rights under applicable copyright law.</p>
										
				<h2>Disclaimers regarding services and materials</h2>
				
				<p>Owing to the nature of the Internet we cannot guarantee that CBO or the websites to which it is linked will always be available to users. You should ensure that you have appropriate protection against viruses and other security arrangements in place whilst using the Internet.</p>
				
				<p>Although every reasonable effort has been made to ensure that the information on CBO was accurate at the time of publication, it is subject to variation at any time without notice and we do not give any warranty that any such information will be accurate or complete at any particular time or at all.</p>
				
				<p>CBO and any information or other material contained in it are made available strictly on the basis that you accept it on an 'as is' and 'as available' basis. Where you rely on any information or other material contained in it, you do so entirely at your own risk and you accept that all warranties, conditions and undertakings, express or implied, whether by common law, statute, trade usage, course of dealings or otherwise in respect of this website are excluded to the fullest extent permitted by law.</p>
				
				<p>We exclude all liability whatever, to the fullest extent permitted by law, in respect of any loss or damage resulting or arising from any non-availability or use of CBO or of any other website linked to it, or from reliance on the contents of CBO or any material or Content accessed through it.</p>
			
			<h2>Term and termination</h2>
			
				<p>These Terms and Conditions shall remain in full force and effect for the duration of your institution's access period or until such time as Cambridge University Press or your institution may terminate their agreement for access to CBO.</p>
				
				<p>In the event that an Authorised User commits a material breach of these Terms and Conditions Cambridge University Press reserves the right at its sole discretion to either (i) withdraw access on a temporary basis, or (ii) terminate access on a permanent basis, to the Content in the event of a deliberate and/or systematic breach of these Terms and Conditions by the Authorised User.  Cambridge University Press further reserves the right to exercise all rights and remedies which may be available to it in law or equity.</p>
				
			<h2>Changes to Terms and Conditions</h2>
				
				<p>Cambridge University Press may change, add or remove portions of these Terms and Conditions at any time. Such changes shall be highlighted on the CBO home page. Your continued use of CBO shall be deemed to constitute your acknowledgment of any such changes.</p>
				
			<h2>Privacy Policy</h2>
				
				<p>If you supply personal details to Cambridge University Press through CBO then you consent to our maintaining, recording, holding and using such personal data in accordance with our <a href="${pageContext.request.contextPath}/privacy_policy.jsf" class="link03">Privacy Policy</a>.</p>
			
			<h2>Links</h2>
			
				<p>Links from CBO are provided for information and convenience only and we have no control over and cannot therefore accept responsibility or liability for the content of any linked third party website.  We do not endorse any linked website.</p>
			<%-- 
			<h2>Jurisdiction</h2>
			
			<p>This website has been designed and these Terms and Conditions shall be governed by and construed in accordance with English law.  Any dispute arising out of the accessing or use of this website shall be subject to the jurisdiction of the English courts.</p>
			--%>
			<h2>Contact us</h2>
			
				<p>Cambridge University Press is a syndicate of the University of Cambridge and our principal place of business is at the Edinburgh Building, Shaftesbury Road, Cambridge, CB2 8RU, UK.</p>
			
				<p>In the event of any comments or questions concerning these terms of use, please contact us by e-mailing The Legal Services Director at <a href="mailto:legalservices@cambridge.org" class="link03">legalservices@cambridge.org</a> or writing to The Legal Services Director at The Edinburgh Building, Shaftesbury Road, Cambridge, CB2 8RU, UK.</p>
				   

</div><!-- end content -->

<div class="clear">&nbsp;</div>

<%-- 
<f:subview id="footerSubview">
	<c:import url="/components/footer.jsp" />
</f:subview>
--%>

<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>

</body>
</f:view>
</html>