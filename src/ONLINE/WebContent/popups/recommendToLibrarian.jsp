<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="org.cambridge.ebooks.online.email.RecommendToLibrarianBean"%>
<%@page import="org.cambridge.ebooks.online.email.Email"%>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>

<body>

<h:form id="recommendForm" prependId="false"> 

<div class="content" style="height: 430px;">	
	<input type="hidden" name="bid" value="${param.bid}" />
	
	<h:panelGroup rendered="#{!recommendToLibrarian.displayDetails}">
	<ul>
    	<li class="header">Recommend This to a Librarian</li>
    	<li><font color="#FF0000"><h:messages layout="list" /></font></li>
    	<li><h:commandLink value="Go Back" action="#{recommendToLibrarian.goBack}" /></li>   	
	</ul>
	</h:panelGroup>
	<h:panelGroup rendered="#{recommendToLibrarian.displayDetails}">
	<ul>
    	<li class="header">Recommend This to a Librarian</li>
    	<li>Email your librarian to recommend adding this book to the library's collection. Either send your message to the library administrator listed below (we use your IP address to fill these fields in) or enter a new email address in the Librarian email address box. You can also add a message.</li>
	</ul> 		
	
   	<font color="#FF0000"><h:messages layout="list" /></font>
   	
	<input name ="exactURL" value='${param.exactURL}' type="hidden" />
   	
    <table cellspacing="0"> 
		<tr>
            <td><label for="name">Your name:</label></td>
            <td class="fields">
            	<h:inputText id="userName" styleClass="register_field"
            		value="#{recommendToLibrarian.userName}" />
            </td>
        </tr>
        
        <tr class="border">
            <td><label for="userEmail">Your email address*:</label></td>
            <td class="fields">
	            <h:inputText id="userEmail" styleClass="register_field" 
						value="#{recommendToLibrarian.userEmail}" required="true" 
						validator="#{recommendToLibrarian.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Email address is required"/>
				</h:inputText>
           </td>
        </tr>
        
        <!-- addition ko -->
        <%--
        <tr class="border">
            <td><label for="userOrgs">Organisations*:</label></td>
            <td class="fields">
            	<select name="organisation" id="organisations" onchange="getEmail();">
            		<option>Select an Organisation</option>>
  					<c:forEach var="organisation" items="${accessDetailsBean.organisationInSession}">
						<option value="${organisation.key}" >${organisation.value}</option>>
					</c:forEach>	
				</select>
           </td>
        </tr>
		 --%>	 
        <!-- addition ko END-->
        
        <tr class="border">
            <td><label for="recipientName">Librarian name:</label></td>

            <td>
				<h:inputText id="recipientName" styleClass="register_field" 
					value="#{recommendToLibrarian.recipientName}" />
			</td>
			<!--
			<td class="fields">
                <input name="recipientName" value="" class="register_field" id="name" type="text" />
            </td>
        	-->
        </tr>
        
		<tr class="border">
            <td><label for="recipientEmail">Librarian email address*:</label></td>
            <td>
				<h:inputText id="recipientEmail" styleClass="register_field" 
					value="#{recommendToLibrarian.recipientEmail}" required="true"
					validator="#{recommendToLibrarian.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Recipient email address is required"/>
				</h:inputText>
			</td>
        </tr>
        
        <tr class="border">
            <td><label for="subjectEmail">Subject:</label></td>
            <td>
				<h:inputText id="subjectEmail" styleClass="register_field"  value="#{recommendToLibrarian.subject}" readonly="true" size="60"/>
			</td>
        </tr>
        
        <tr class="border">
            <td><label for="content">Message</label></td>
            <td>
            	
				<h:inputTextarea id="body" styleClass="problem" value="#{recommendToLibrarian.mailmessage}" cols="23" rows="5" required="true" >				
					<f:attribute name="requiredMessage" value="Message is required"/>
				</h:inputTextarea >				
			</td>
			
		<!--<td><textarea name="content" cols="1" rows="5" class="email" id="content"></textarea></td>-->     
        </tr>
        
	</table>

	<div id="bottomButtons">		 
		<a href="javascript:doReset();" title="Reset"><img src="${pageContext.request.contextPath}/images/bot_reset.gif" /></a>       	
       	<a href="javascript:doEmail();" title="Email"><img src="${pageContext.request.contextPath}/images/bot_email.gif" /></a>       
       	<h:commandButton id="resetBtn" type="reset" style="display: none;" />
       	<h:commandButton id="email_send" action="#{recommendToLibrarian.sendEmail}" style="display: none;" />      	
		<h:message for="email_send"/>
    </div>
	</h:panelGroup>
</div><!-- end content -->
</h:form>
<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<script type="text/javascript">
	function doReset() {
		$("#resetBtn").click();
	}
	
	function doEmail() {
		$("#email_send").click();
	}

	function send(formName){
		 var form = document.forms[formName];
		 form.action = form.action + "#country";
		 form.submit();
	} 
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>