<!-- end header -->
<div class="camLogo"><a href="http://www.cambridge.org/" target="_blank"><img src="${pageContext.request.contextPath}/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" /></a></div>

<div id="navigation">
	
	<ul>
		<li class="home"><a href="#" onclick="showHelp('${pageContext.request.contextPath}/','root', '<%= session.getAttribute("pageId") %>', '<%= session.getAttribute("pageName") %>'); return false;">Help Topic Index</a></li>
		<li><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/faq.jsf','popup','status=yes,scrollbars=yes'); return false;">FAQs</a></li>
		<li><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">Diagnostics</a></li>
		<li><a href="#" onclick="window.close();">Exit</a></li>
	</ul>
	
	<div class="clear">&nbsp;</div>
</div>
<!-- end header -->

