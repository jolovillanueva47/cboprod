<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.online.util.UrlUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="org.cambridge.ebooks.online.util.SolrUtil"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>
<body>	
	<form id="exportCitationForm" action="" method="post">

		<div class="content">
			<ul>
			   	<li class="header">Export Citation</li>
			   	<li>You can download citations to your desktop or you can email them to a colleague.</li> 
			   	<%
					if(request.getParameter("download") != null && request.getParameter("download").equals("none")) {
						out.write("<li></li>");
						out.write("<li style='color: #FF0000;'>No citation to be downloaded.</li>");
					} else if(request.getParameter("email") != null && request.getParameter("email").equals("success")) {
						out.write("<li></li>");
						out.write("<li style='color: #FF0000;'>Your message was sent.</li>");
					} else if(request.getParameter("email") != null && request.getParameter("email").equals("fail")) {
						out.write("<li></li>");
						out.write("<li style='color: #FF0000;'>Your message was not sent. Please try again.</li>");
					}
				%>  	
			</ul>

			<table cellspacing="0" cellpadding="0">	
				<tr class="shaded">
					<th colspan="2">Include extract for this citation: </th>
				</tr>	
				<tr class="border">
					<td width="10%">Yes</td>
					<td class="fields">
						<input id="abstractYes" name="displayAbstract" value="Yes" type="radio" />
					</td>
				</tr>
				<tr>
					<td>No</td>
					<td>
						<input name="displayAbstract" value="No" checked="checked" type="radio" />
					</td>
				</tr>
				
				<tr class="shaded">
					<th colspan="2">Choose file format:</th>
				</tr>
				
				<tr class="border">
					<td><label for="ASCII">ASCII</label></td>
					<td><input class="format" name="format" value="ASCII" id="ASCII" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="Bibliscape">Biblioscape</label></td>
					<td><input class="format" name="format" value="Biblioscape" id="Biblioscape" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="BibTex">BibTex</label></td>
					<td><input class="format" name="format" value="BibTex" id="BibTex" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="CSV">CSV</label></td>
					<td><input class="format" name="format" value="CSV" id="CSV" checked="checked" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="Endnote">EndNote</label></td>
					<td><input class="format" name="format" value="Endnote" id="EndNote" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="HTML">HTML</label></td>
					<td><input class="format" name="format" value="HTML" id="HTML" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="Medlars">Medlars</label></td>
					<td><input class="format" name="format" value="Medlars" id="Medlars" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="Papyrus">Papyrus</label></td>
					<td><input class="format" name="format" value="Papyrus" id="Papyrus" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="ProCite">ProCite</label></td>
					<td><input class="format" name="format" value="ProCite" id="ProCite" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="ReferenceManager">Reference Manager</label></td>
					<td><input class="format" name="format" value="ReferenceManager" id="ReferenceManager" type="radio" /></td>
				</tr>
				
				<tr>
					<td><label for="RefWorks">RefWorks</label></td>
					<td><input class="format" name="format" value="RefWorks" id="RefWorks" type="radio" /></td>
				</tr>
				
				<tr> 
					<td><label for="RIS">RIS</label></td>
					<td class="fields">
						<input class="format" name="format" value="RIS" id="RIS" type="radio" />
					</td>
				</tr>
				
				<tr class="shaded">
					<th colspan="2">Export citations directly into your citation software.</th>
				</tr>
				<tr> 
					<td><label for="DirExpRefWorks">RefWorks (Direct Export)</label></td>
					<td><input class="format" name="format" value="DirExpRefWorks" id="DirExpRefWorks" type="radio"/></td>
				</tr>
				<tr>
					<td>
					<div class="form_button">
						<span id="exportButtonDiv">
							<a href="javascript:void(0);" id="exportButton" title="Export">Export</a>
						</span>
					</div>
				</td>
				</tr>
				<tr class="shaded">
					<th colspan="2">To email citation to a colleague, enter email address below:</th>
				</tr>
				<tr class="border">
					<td><label for="emailId">Email</label></td> 
					<td>			
						<input type="text" id="emailText" class="register_field" value="" />
					</td>
				</tr>
			</table>

<textarea id="ImportData" name="ImportData" style="display:none;" rows=30 cols=70 >
RT Book, Whole
SR Print(0)
ID 
A1 ${sessionScope.authorNamesSession}
T1 ${sessionScope.bookTitleSession}
YR ${param.printDate}
VO ${param.volume}
AB <%= SolrUtil.getAbstractByIsbn(request.getParameter("isbn"))%>
PB <%= SolrUtil.getPublisherNameByIsbn(request.getParameter("isbn"))%>
PP <%= SolrUtil.getPublisherLocationByIsbn(request.getParameter("isbn"))%>
SN ${param.isbn}
LA English
LK http://dx.doi.org/${param.doi}
DO http://dx.doi.org/${param.doi}
DB 
UL <%= UrlUtil.getClientDns(request) %>ebook.jsf?bid=${param.id}
WT Cambridge Books Online
OL English(30)
</textarea>
			<div class="form_button">
				<ul>
					<li><span id="resetFormDiv"><a href="javascript:void(0);" id="resetButton" title="Reset">Reset</a></span></li>                                            
                    <li><span id="downloadButtonDiv"><a href="javascript:void(0);" id="downloadButton" title="Download">Download</a></span></li>
                    <li><span id="emailButtonDiv"><a href="javascript:void(0);" id="emailButton" title="Email">Email</a></span></li>
                </ul>
              	<div class="clear"></div>
			</div>
		</div><!-- end content -->
	</form>
	
	<input id="context_root" type="hidden" value="${pageContext.request.contextPath}/" />
	
	<f:subview id="commonJsSubview">
		<c:import url="/popups/common_js.jsp" />
	</f:subview>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/export_citation.js"></script>
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>
</f:view>
</html>