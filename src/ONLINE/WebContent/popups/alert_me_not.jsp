<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />
<o:url var="contextRootSsl" option="current_dns_to_https" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>
</head>

<body>

<div class="content">
<script type="text/javascript">
	var utut = '${param.LOCATION10556}';
	utut = window.encodeURIComponent(utut);
	var url = "${contextRootSsl}/login.jsf?keepThis=true&amp;LOCATION10556=" + utut +"&amp;TB_iframe=true&amp;width=460&amp;height=290";
	
</script>
<h:form id="citationAlertsForm" prependId="false">
	<ul>
    	<li class="header">Citation Alert</li>
    	<li>You will receive an e-mail when the books or chapters below are cited by other publications.</li>
    	
		<li>You need to <a onclick="window.parent.location='${contextRootCjo}registration?displayname=${referalName}'; self.parent.tb_remove();" href="javascript:void(0);">register</a>
			  or <%--<a class="thickbox link10" id="loginLink" onclick="tb_remove(); self.parent.tb_show(null, '${contextRootSsl}/login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290', null);" href="#">login</a>  --%>
			  <a class="thickbox" id="loginLink" onclick="self.parent.tb_show(null, url, null);" href="#">login</a>
		to use this feature.</li>
		<%--li>You need to register or login to use this feature.</li--%>		
    </ul>
    
    <input type="hidden" id="loc" value="<c:out value="${fn:replace(param.LOCATION10556, '&', '&amp;')}" escapeXml="true"/>"/>
</h:form>
</div><!-- end content -->

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>