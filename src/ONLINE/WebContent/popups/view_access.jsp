<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>

<body>

<f:subview id="headerSubview">
	<c:import url="/popups/header.jsp" />
</f:subview>

<h:form>
<div class="noBordercontent">

  <h1>Diagnostics</h1>
	
  <p>Below is a summary of your user details. You may use this information in order to work with your local administrator to troubleshoot any access issues you have, or you can contact Cambridge University Press Customer Services by filling in the form below and submitting it.</p>
  <table cellspacing="0">
	<tr class="shaded">
		<th colspan="2" class="title">User Details </th>
	</tr>
	
    <tr class="border">
		<th><label for="browser">Browser</label> &amp; OS</th>
		<td>${header["user-agent"]}</td>
	</tr>
	
    <tr>
		<th><label for="ipAddress">IP Address</label></th>
		<c:choose>
			<c:when test="${empty header['X-Cluster-Client-Ip']}">
				<td>${pageContext.request.remoteAddr}</td>
			</c:when>
			<c:otherwise>
				<td>${header["X-Cluster-Client-Ip"]}</td>
			</c:otherwise>
		</c:choose>
	</tr>
	
    <tr>
		<th><label for="domainName">Domain Name</label></th>
		<td>${pageContext.request.remoteHost}</td>
	</tr>
	
    <tr>
		<th><label for="node">Node</label></th>
		<td>
			<%= java.net.InetAddress.getLocalHost().getHostName() 
				//System.getProperty("java.rmi.server.codebase")
			%>
		</td>
	</tr>
	
    <tr>
		<th><label for="userId">Username</label></th>
		<td><c:out value="${accessDetailsBean.userId}" /></td>
	</tr>

	<tr>
		<th><label for="bodyId">Body ID</label></th>
		<td><c:out value="${accessDetailsBean.bodyId}" /></td>
	</tr>
	
    <tr>
		<th><label for="sessionId">Session ID</label></th>
		<td><c:out value="${accessDetailsBean.sessionId}" /></td>
	</tr>
	
    <tr>
		<th><label for="memberName">User&#8217;s Full Name</label></th>
		<td><c:out value="${accessDetailsBean.fullName}" /></td>
	</tr>
	
    <tr>
		<th><label for="emailAddress">Email Address</label></th>
		<td><c:out value="${accessDetailsBean.emailAddress}" /></td>
	</tr>
	
    <tr class="shaded">			
		<th colspan="2" class="title" scope="rowgroup"><a name="organisations" id="organisations"></a>Organisation Details</th>
	</tr>
	
	<c:forEach var="organisation" items="${accessDetailsBean.organisationInSession}">
		<tr class="border">
			<th>Body ID</th>
			<td><c:out value="${organisation.bodyId}" /></td>
		</tr>
		<tr class="border">
			<th>Display Name</th>
			<td><c:out value="${organisation.displayName}" /></td>
		</tr>
		<c:if test="${not empty organisation.messageText}">
			<tr class="border">
				<th>Message</th>
				<td><c:out value="${organisation.messageText}" /></td>
			</tr>
		</c:if>
	</c:forEach>
			
	<tr class="shaded">			
		<th colspan="2" class="title" scope="rowgroup">Consortia Details </th>
	</tr>
		
	<c:forEach var="consortia" items="${accessDetailsBean.consortiaInSession}">
		<tr class="border">
			<th>Body ID</th>
			<td><c:out value="${consortia.key}" /></td>
		</tr>
		<tr class="border">
			<th>Display Name</th>
			<td><c:out value="${consortia.value}" /></td>
		</tr>
	</c:forEach>
	
	<tr class="shaded">			
		<th colspan="2" class="title" scope="rowgroup">Society Details </th>
	</tr>
		
	<c:forEach var="society" items="${accessDetailsBean.societyInSession}">
		<tr class="border">
			<th>Body ID</th>
			<td><c:out value="${society.key}" /></td>
		</tr>
		<tr class="border">
			<th>Display Name</th>
			<td><c:out value="${society.value}" /></td>
		</tr>
	</c:forEach>
		
	<tr class="shaded">
		<th colspan="2" class="title" scope="rowgroup">Offer Details </th>
	</tr>
		  
	<c:forEach var="offer" items="${accessDetailsBean.offerInSession}">
		<tr class="border">
			<th>Body ID</th>
			<td><c:out value="${offer.key}" /></td>
		</tr>
		<tr class="border">
			<th>Display Name</th>
			<td><c:out value="${offer.value}" /></td>
		</tr>
	</c:forEach>
		
	<tr class="shaded">
		<th colspan="2" class="title" scope="rowgroup">Problem  Details</th>
	</tr>

	<tr class="border">
		<th><label for="emailAd">User Email Address</label></th>
		<td>
			<h:inputText id="emailAd" styleClass="register_field" 
				value="#{accessDetailsBean.userEmailAddress}" required="true" 
				validator="#{accessDetailsBean.validateEmail}" maxlength="80" >
				
				<f:attribute name="requiredMessage" value="Email address is required"/>
			</h:inputText>
			&nbsp;<h:message for="emailAd" style="color:#f00"></h:message>
		</td>		
	</tr>
	
    <tr>
		<th><label for="subject">Email Subject</label></th>
		<td>
			<h:inputText id="subject" styleClass="register_field" 
				value="#{accessDetailsBean.emailSubject}" />
		</td>		
	</tr>
	
    <tr>
		<th><label for="problem">Type of Problem</label><span class="note">*</span></th>
		<td>
			<h:selectOneMenu id="problem" value="#{accessDetailsBean.typeOfProblem}">
				<f:selectItem itemLabel="Select type of problem ..." itemValue="" />
				<f:selectItem itemLabel="Account Administrator" itemValue="Account Administrator" />
				<f:selectItem itemLabel="Access to Content" itemValue="Access to Content" />
				<%--<f:selectItem itemLabel="Article Purchase" itemValue="Article Purchase" />--%>
				<f:selectItem itemLabel="Error Message" itemValue="Error Message" />
				<f:selectItem itemLabel="IP Recognition" itemValue="IP Recognition" />
				<f:selectItem itemLabel="Login" itemValue="Login" />
				<f:selectItem itemLabel="Password" itemValue="Password" />
				<f:selectItem itemLabel="Registration" itemValue="Registration" />
				<f:selectItem itemLabel="Remote Access" itemValue="Remote Access" />
				<f:selectItem itemLabel="Searching" itemValue="Searching" />
				<%--<f:selectItem itemLabel="Society Member Access" itemValue="Society Member Access" />
				<f:selectItem itemLabel="Subscription Activation" itemValue="Subscription Activation" />--%>			
				<f:selectItem itemLabel="Usage Statistics" itemValue="Usage Statistics" />
				<f:selectItem itemLabel="OTHER" itemValue="Other" />
			</h:selectOneMenu>
		</td>
	</tr>
	
    <tr>
		<th><label for="body">Problem Description</label><span class="note">*</span></th>
		<td>
			<h:inputTextarea id="body" styleClass="problem" value="#{accessDetailsBean.problemDescription}" 
				cols="23" rows="5" required="true" >				
				<f:attribute name="requiredMessage" value="Problem description is required"/>
			</h:inputTextarea> 
			&nbsp;<h:message for="body" style="color:#f00; vert-alignment: middle"></h:message>
		</td>		
	</tr>
	
    <tr>
		<th><label for="locations">Your Location</label></th>
		<td>			
			<h:selectOneMenu id="locations" value="#{accessDetailsBean.location}">				
				<f:selectItem itemLabel="The Americas" 
					itemValue="#{accessDetailsBean.usEmail}:The Americas" />       
				<f:selectItem itemLabel="United Kingdom and Rest of World" 
					itemValue="#{accessDetailsBean.ukEmail}:United Kingdom and Rest of World" />         
			</h:selectOneMenu>
		</td>
	</tr>
    
	<tr>
        <td colspan="2"><span class="note">*</span> required</td>
	</tr>

	</table>
    
    <div id="bottomButtons">
        <span class="button"><h:commandButton value="Reset" type="reset" />&nbsp;</span>
		<span class="button"><h:commandButton value="Email" id="email_send" action="#{accessDetailsBean.sendEmail}" />&nbsp;</span>
		<h:message for="email_send"/>
    </div>
    
</div><!-- end content -->
</h:form>

<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>