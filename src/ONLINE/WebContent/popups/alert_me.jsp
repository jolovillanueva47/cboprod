<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>

<body>

<h:inputHidden id="initThickBox" value="#{citationAlertsBean.initThickBox}" />
<div class="content">
<h:form id="citationAlertsForm" prependId="false">

	<ul>
    	<li class="header">Citation Alert</li>
    	<li>You will receive an e-mail when the books or chapters below are cited by other publications.</li>    	
    </ul>

    <div id="topButtons">
    	<h:commandLink immediate="true"><img src="${pageContext.request.contextPath}/images/bot_reset.gif" /></h:commandLink>
		&nbsp;<h:commandLink actionListener="#{citationAlertsBean.updateCitationAlerts}"><img src="${pageContext.request.contextPath}/images/bot_update.gif" /></h:commandLink>
    </div>
    
    
	<table cellspacing="1">
		<tr class="shaded">
			<th class="alertMeHeader">Book Title</th>
			<th width="15%" class="alertMeHeader">Alert Frequency</th>
			<th width="20%" class="alertMeHeader">Email Address</th>
			<th width="10%" class="alertMeHeader"><input id="toggleAllTurnOff" type="checkbox" /><br />Turn Off Alert</th>
			<th width="10%" align="center" style="vertical-align: middle !important;"><input id="toggleAllDelete" type="checkbox" style="display: block; margin-bottom: 10px;"/>&nbsp;Delete</th>
		</tr>		
	</table>		
	
	<h:dataTable style="height: 215px;" columnClasses=",fifteen,twenty,ten,ten" cellspacing="1" value="#{citationAlertsBean.citationAlerts}" var="alert">
		<h:column>	
			<div class="bookResultItem">
				<h3><span class="bookTitle"><a href="javascript:void(0);" onclick="window.parent.location='${pageContext.request.contextPath}/ebook.jsf?bid=<h:outputText value="#{alert.bookId}"/>'; self.parent.tb_remove();">
					<h:outputText value="#{alert.bookTitle}" escape="false"/>
				</a></span>by&nbsp;<span class="author"><h:outputText value="#{alert.author}"/></span></h3>
				<ul>
					<li class="publication">Published Online: <h:outputText value="#{alert.publishDate}" /></li>

					<h:panelGroup rendered="#{not empty alert.contentTitle}">
						<li><span class="chapterTitle"><a href="javascript:void(0);" onclick="window.parent.location='${pageContext.request.contextPath}/chapter.jsf?bid=<h:outputText value="#{alert.bookId}"/>&cid=<h:outputText value="#{alert.contentId}"/>'; self.parent.tb_remove();">
							<h:outputText value="#{alert.contentTitle}" />
						</a></span></li>
						<li class="page"><h:outputText value="#{alert.page}" /></li>
						<li class="doi"><span class="title">DOI:</span> <a href="javascript:void(0);" onclick="window.parent.location='http://dx.doi.org/<h:outputText value="#{alert.doi}" />'; self.parent.tb_remove();">
							http://dx.doi.org/<h:outputText value="#{alert.doi}" />
						</a></li>
					</h:panelGroup>
					
					<h:panelGroup rendered="#{empty alert.contentTitle}">
						<li class="doi"><span class="title">DOI:</span> <a href="javascript:void(0);" onclick="window.parent.location='http://dx.doi.org/<h:outputText value="#{alert.doi}" />'; self.parent.tb_remove();">
							http://dx.doi.org/<h:outputText value="#{alert.doi}" />
						</a></li>
					</h:panelGroup>

				</ul>
			</div>
		</h:column>

		<h:column>
			<h:selectOneMenu value="#{alert.frequencyType}">
				<f:selectItem itemValue="0" itemLabel="Weekly" />
				<f:selectItem itemValue="1" itemLabel="Monthly" />
			</h:selectOneMenu>	
		</h:column>

		<h:column>
			<h:inputText maxlength="100" value="#{alert.emailAddress}"/>
		</h:column>

		<h:column>
			<h:selectBooleanCheckbox styleClass="turnOffCheckBox checkBox" value="#{alert.turnOff}"/>
		</h:column>

		<h:column>
			<h:selectBooleanCheckbox styleClass="deleteCheckBox checkBox" value="#{alert.delete}"/>
		</h:column>
	</h:dataTable>
   
    <div id="bottomButtons">
    	<h:commandLink immediate="true"><img src="${pageContext.request.contextPath}/images/bot_reset.gif" /></h:commandLink>
		&nbsp;<h:commandLink actionListener="#{citationAlertsBean.updateCitationAlerts}"><img src="${pageContext.request.contextPath}/images/bot_update.gif" /></h:commandLink>
    </div>

</h:form>
</div><!-- end content -->

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>

<script type="text/javascript">
	$(document).ready(function(){
		
		$('#toggleAllTurnOff').attr('checked', false);
		$('#toggleAllDelete').attr('checked', false);
		
		$('#toggleAllTurnOff').click(function(){
			var checked = $(this).attr('checked');
			$('#toggleAllTurnOff, .turnOffCheckBox').attr('checked', checked ? true : false);
		});
		$('#toggleAllDelete').click(function(){
			var checked = $(this).attr('checked');
			$('#toggleAllDelete, .deleteCheckBox').attr('checked', checked ? true : false);
		});
	});
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>