<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup_styles.css" />
</head>

<body>

<div class="content">
	<h:form id="findBookForm" prependId="false">
	<h:inputHidden id="initBookLocation" value="#{findBookBean.initBookLocation}" />
	<h:inputHidden id="bodyIdHidden" value="#{findBookBean.bodyId}" />
	<h:inputHidden id="eisbnHidden" value="#{findBookBean.eisbn}" />
	<%
	if((response.encodeURL(request.getParameter("setLocation")) == null)){
	%>
		<ul>
			<li class="header">Find This Book in a Library</li>
			<li>EISBN: <h:outputText value="#{findBookBean.eisbn}" /> </li>
			<li>Location: <h:outputText value="#{findBookBean.location}"/></li>
		</ul>
	<%}else{%>
		<ul>		
			<li class="header">Set the location of this Book.</li>
			<li>EISBN: <h:outputText value="#{findBookBean.eisbn}" /> </li>
			<li>Location: <h:inputTextarea value="#{findBookBean.location}"/></li>
		</ul>
		<div id="bottomButtonsNoBorder">
		<h:commandButton actionListener="#{findBookBean.updateBookLocation}" value="Update" />
    	</div>
		
	<%} %>
	</h:form>
</div><!-- end content -->

<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>