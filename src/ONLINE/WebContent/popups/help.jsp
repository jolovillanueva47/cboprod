<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
	
</head>

<body>

${helpBean.initializeHelp}
	
<f:subview id="headerSubview">
	<c:import url="/popups/header.jsp" />
</f:subview>

<div class="noBordercontent">
	<h1>Help Topic: ${helpBean.helpBooksBean.pageName}</h1>
	<c:choose>
		<c:when test="${helpBean.helpBooksBean.helpFile ne null && helpBean.helpBooksBean.helpFile ne ''}">				
			<div>	
				<f:subview id="helpPageSubview">
					<c:import url="${helpBean.helpBooksBean.helpFile}" charEncoding="utf8"/>
				</f:subview>	
			</div>				 
		</c:when>		
		<c:otherwise>
			<div class="answer">
				No help text.
			</div>
		</c:otherwise>
	</c:choose>
    
</div><!-- end content -->
<div class="clear"></div>
<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>
