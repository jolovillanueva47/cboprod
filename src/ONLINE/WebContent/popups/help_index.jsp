<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>
<body>

${helpBean.initializeHelpIndex}

<f:subview id="headerSubview">
	<c:import url="/popups/header.jsp" />
</f:subview>

<div class="noBordercontent">
	<%--<div class="leftmenu">
	<h1>Choose your language</h1>        
        <ul>
        	<h:form>
	        	<li><h:commandLink action="#{faqBean.getCookieValue}" value="English">			<f:param name="languageCode" value="1"/></h:commandLink></li>
	        	<li><h:commandLink action="#{faqBean.getCookieValue}" value="中文 (Chinese)">	<f:param name="languageCode" value="2"/></h:commandLink></li>
	        	<li><h:commandLink action="#{faqBean.getCookieValue}" value="Español">			<f:param name="languageCode" value="3"/></h:commandLink></li>
	        	<li><h:commandLink action="#{faqBean.getCookieValue}" value="日本語 (Japanese)">	<f:param name="languageCode" value="4"/></h:commandLink></li>
	        	<li><h:commandLink action="#{faqBean.getCookieValue}" value="Português">		<f:param name="languageCode" value="5"/></h:commandLink></li>
        	</h:form>
		</ul>
    </div>--%>
    
	<h1>Help Topic Index</h1>
	<ul class="index">
		<c:forEach var="helpBooksBean" items="${helpBean.helpBooksBeans}">
			<c:if test="${helpBooksBean.languageCode == faqBean.cookieValue || helpBooksBean.languageCode eq 0}">		
				<li><a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/help.jsf?pageId=${helpBooksBean.pageId}&amp;pageName=${helpBooksBean.pageName}&amp;languageCode=${faqBean.cookieValue}','popup','status=yes,scrollbars=yes'); return false;"><c:out value="${helpBooksBean.pageName}" escapeXml="false"/></a></li>
			</c:if>	
		</c:forEach>
	</ul>
	<!-- ul class="index">
		<li><a href="javascript:MM_openWindow('${pageContext.request.contextPath}/popups/help.jsf','popup','status=yes,scrollbars=yes');">Help topic one</a></li>
		<li><a href="#">Help topic two</a></li>
		<li><a href="#">Help topic three</a></li>
		<li><a href="#">Help topic four</a></li>
		<li><a href="#">Help topic five</a></li>
		<li><a href="#">Help topic six</a></li>
		<li><a href="#">Help topic seven</a>s</li>
		<li><a href="#">Help topic eight</a></li>
		<li><a href="#">Help topic nine</a></li>
		<li><a href="#">Help topic ten</a></li>
	</ul -->    
    
</div><!-- end content -->
<div class="clear"></div>
<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>
<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>