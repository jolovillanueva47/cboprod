<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>
<body>

<f:subview id="headerSubview">
	<c:import url="/popups/header.jsp" />
</f:subview>

<div class="clear"></div>

<div class="noBordercontent">
  	${faqBean.initialize}
  	${faqBean.populateSubjectAreaDetails}
	<div class="leftmenu">
       
    	<h1>FAQ topics</h1>
      	<ul>
	        <c:forEach var="faqSubjectAreaTypeBooksBean" items="${faqBean.faqSubjectAreaTypeBooksBeans}">				
				<c:if test="${not empty faqSubjectAreaTypeBooksBean.subjectAreaDescription}">
					<c:if test="${faqSubjectAreaTypeBooksBean.languageCode == faqBean.cookieValue || empty faqSubjectAreaTypeBooksBean.languageCode || faqSubjectAreaTypeBooksBean.languageCode eq null || faqSubjectAreaTypeBooksBean.languageCode eq ''}">			
						<li><a href="${pageContext.request.contextPath}/popups/faq.jsf?languageCode=${faqBean.cookieValue}&amp;#${faqSubjectAreaTypeBooksBean.id}">${faqSubjectAreaTypeBooksBean.subjectAreaDescription}</a></li>
					</c:if>
				</c:if>				
			</c:forEach>	
		</ul>
    </div>
	
	
  	<h1><a name="top">Frequently Asked Questions</a></h1>  
  	<!-- 
  	<c:forEach var="faqSubjectAreaDetailsBooksBean" items="${faqBean.faqSubjectAreaDetailsBooksBeans}">
		<h2><c:out value="${faqSubjectAreaDetailsBooksBean.question}" escapeXml="false" /></h2>
		<p class="answer"><c:out value="${faqSubjectAreaDetailsBooksBean.answer}" escapeXml="false" /></p>		
	</c:forEach>
  	-->
  	<!-- 
  	<c:forEach var="faq" items="${faqBean.faqs}">
		<h2><c:out value="${faq.question}" escapeXml="false" /></h2>
		<p class="answer"><c:out value="${faq.answer}" escapeXml="false" /></p>		
	</c:forEach>
  	-->
  	<h2>Question</h2>
  	<ul class="index">
		<li><c:out value="${faqBean.faqSubjectAreaDetailsBooksBean.question}" escapeXml="false" /></li>				
	</ul>
	<h2>Answer</h2>
	<ul class="index">
		<li><c:out value="${faqBean.faqSubjectAreaDetailsBooksBean.answer}" escapeXml="false" /></li>				
	</ul>
	<c:forEach var="faqSubjectAreaTypeBooksBean" items="${faqBean.faqSubjectAreaTypeBookByIdBeans}">
		<c:if test="${not empty faqSubjectAreaTypeBooksBean.subjectAreaDescription}">
		<c:if test="${faqSubjectAreaTypeBooksBean.languageCode eq 0 || faqSubjectAreaTypeBooksBean.languageCode == faqBean.cookieValue}">					
			<h2><a name="${faqSubjectAreaTypeBooksBean.id}"><c:out value="${faqSubjectAreaTypeBooksBean.subjectAreaDescription}" escapeXml="false" /></a></h2>
		</c:if>
		</c:if>
		
		<c:forEach var="faqSubjectAreaDetailsBooksBean" items="${faqSubjectAreaTypeBooksBean.faqSubjectAreaDetailsBooksBeans}">
			<c:if test="${not empty faqSubjectAreaDetailsBooksBean.question}">
			<c:if test="${faqSubjectAreaTypeBooksBean.languageCode eq 0 || faqSubjectAreaTypeBooksBean.languageCode == faqBean.cookieValue}">		
				<ul class="index">
					<li><a href="${pageContext.request.contextPath}/popups/faq_answer.jsf?typeId=${faqSubjectAreaTypeBooksBean.id}&detailsId=${faqSubjectAreaDetailsBooksBean.id}&amp;languageCode=${faqBean.cookieValue}"><c:out value="${faqSubjectAreaDetailsBooksBean.question}" escapeXml="false" /></a></li>				
				</ul>		
			</c:if>
			</c:if>
		</c:forEach>
	</c:forEach>
</div><!-- end content -->
<div class="clear"></div>
<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>