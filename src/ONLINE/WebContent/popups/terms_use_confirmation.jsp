<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />

	<title>Cambridge Books Online - Cambridge University Press</title>
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>
<body>
	<div class="content">
		<input type="checkbox" name="confirmCheckbox" id="confirmCheckbox"/>                  							
		<label for="acceptTerms">You must accept the changes made in <a href="${pageContext.request.contextPath}/popups/terms_of_use_popup.jsf">Terms of Use</a>.</label>
														
		<br /><br /> 
			
		 <input type="button" name="termsAccepted" id="termsAccepted" value="Accept"/>
	</div>
	
<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {				
		$("#termsAccepted").click(function(){
			 var isChecked = ($("#confirmCheckbox").attr("checked"));
			 //alert(isChecked);
			 if(!isChecked){
				 alert("You must accept terms.");
			 }else{
				 $.post("${pageContext.request.contextPath}/login?confirm=Y");
				 window.close();
			 }
		});
	});	
</script>

</body>
</html>


