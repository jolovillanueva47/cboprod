<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="createXml" option="current_dns" append="/create_xml" />
<o:url var="sslContextRoot" option="current_dns_to_https" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${param.cid}</title>
	<jsp:include page="/popups/common_css.jsp" />
</head>
<body>
<%-- 
	<div id="loading" style="margin-top: 250px; text-align: center;">
		<b>Please wait, page is loading...</b><br /><img width="208" height="13" src="/images/loadingAnimation.gif"  alt="Loading..." />
	</div>
	--%>
	<div id="main" style="display: block;">
	
		 <c:if test="${not empty accessDetailsBean.bodyId}" >
		     <div id="topButtons">
		      <div class="fr">
		          <span class="button"><input name="reset" value="Save" type="submit" onclick="storeList();">&nbsp;</span>
		          <span class="button"><input id="btnload" name="reset" value="Load" type="submit" onclick="restoreAnnotations();return false;">&nbsp;</span>
		          <span class="button"><input name="reset" value="Clear" type="submit" onclick="clearAll();return false;">&nbsp;</span>
		      </div>
		      <div class="clear"></div>
		    </div>
		 </c:if>   
	     <div id="flashContent"> 
	        	<p> 
		        	To view this page ensure that Adobe Flash Player version 
					10.0.0 or greater is installed. 
				</p> 
				<script type="text/javascript"> 
					var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
					document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
									+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
				</script> 
	 	</div>
	</div>
	
	
	<c:set var="httpHost" value="http://${header.host}" />
	<c:choose>
		<c:when test="${not empty param.startPage and 'null' ne param.startPage}">
			<c:choose>
				<c:when test="${'on' eq param.hithighlight}">
					<c:url var="xmlHighlight" value="${createXml}">
						<c:param name="cid" value="${param.cid}" />
					</c:url>
					<c:url var="urlPdf" value="/open_pdf/${param.cid}#page=${param.startPage}#xml=${xmlHighlight}" />
				</c:when>
				<c:otherwise>
					<c:url var="urlPdf" value="/open_pdf/${param.cid}#page=${param.startPage}" />
				</c:otherwise>
			</c:choose>		
			<iframe src="${urlPdf}" class="content">
			</iframe>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${'on' eq param.hithighlight}">
					<c:set var="xmlHighlight" value="${createXml}?cid=${param.cid}" />
					<c:url var="urlPdf" value="/open_pdf/${param.cid}#xml=${xmlHighlight}" />
				</c:when>
				<c:otherwise>
					<c:url var="urlPdf" value="/open_pdf/${param.cid}" />
				</c:otherwise>
			</c:choose>	
			
		</c:otherwise>
	</c:choose>
	<script type="text/javascript"> 
		function getAccountId(){
			var id = '${accessDetailsBean.bodyId}';
			//alert("id is="+id);
			return id;
		}
		function getPdf(){
			return '${pageContext.request.contextPath}/popups/swf/testPdf.swf';
			//var answer = confirm ("Would like to load this? > ../swf/ancientanger.swf  else ../swf/testPdf.swf");
			//if(answer){
				//return '../swf/ancientanger.swf';
			//}else{
				//return '../swf/testPdf.swf';
			//}
		}
    </script> 

  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/flexPaper/swfobject.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js"></script>
<!--	<script type="text/javascript" src="../js/flexPaper/flexpaper_annotations_flash_debug.js"></script>-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/flexPaper/main.js"></script>
	
	<script type="text/javascript"> 
		$(document).ready(function(){
			$("#loading").hide();
			$("#main").show();			
		});		
    </script> 
</body>

</html>


<%-- 
	<div style="position:absolute;left:750px;top:10px;font-family:Verdana;font-size:9pt;">
        <table border="0" width="230">
	        <tr><th colspan=3>Operations</th></tr>
	        <tr><td>loadSwf</td><td><input type=text style="width:70px;" id="txt_swffile" value="Paper.swf"></td><td><input type=submit value="Invoke" onclick="getDocViewer().loadSwf($('#txt_swffile').val())"></td></tr>
	        <!-- 
			<tr><td>fitWidth</td><td></td><td><input type=submit value="Invoke" onclick="getDocViewer().fitWidth()"></td></tr>
	        <tr><td>fitHeight</td><td></td><td><input type=submit value="Invoke" onclick="getDocViewer().fitHeight()"></td></tr>
	        <tr><td>gotoPage</td><td><input type=text style="width:70px;" id="txt_pagenum" value="3"></td><td><input type=submit value="Invoke" onclick="getDocViewer().gotoPage($('#txt_pagenum').val())"></td></tr>
	        <tr><td>getCurrPage</td><td></td><td><input type=submit value="Invoke" onclick="alert('Current page:' + getDocViewer().getCurrPage())"></td></tr>
	        <tr><td>nextPage</td><td></td><td><input type=submit value="Invoke" onclick="getDocViewer().nextPage()"></td></tr>
	        <tr><td>prevPage</td><td></td><td><input type=submit value="Invoke" onclick="getDocViewer().prevPage()"></td></tr>
	        <tr><td>setZoom</td><td><input type=text style="width:70px;" id="txt_zoomfactor" value="1.30"></td><td><input type=submit value="Invoke" onclick="getDocViewer().setZoom($('#txt_zoomfactor').val())"></td></tr>
	        <tr><td>searchText</td><td><input type=text style="width:70px;" id="txt_searchtext" value="text"></td><td><input type=submit value="Invoke" onclick="getDocViewer().searchText($('#txt_searchtext').val())"></td></tr>
	        <tr><td>switchMode</td><td><input type=text style="width:70px;" id="txt_viewmode" value="Tile"></td><td><input type=submit value="Invoke" onclick="getDocViewer().switchMode($('#txt_viewmode').val())"></td></tr>
	        <tr><td>printPaper</td><td></td><td><input type=submit value="Invoke" onclick="getDocViewer().printPaper()"></td></tr>
			-->
	    </table><br>
	    <table border="0" width="230">
	    	<tr><th colspan=3>Annotations Plugin</th></tr>    
	    	<tr><td>createMark</td><td></td><td><input type=submit readonly value="Invoke" onclick="appendLog('createMark:'+getDocViewer().createMark('#fffc15').selection_info)"></td></tr>
			<tr><td>View selection</td><td></td><td><input type=submit readonly value="Invoke" onclick="alert(getDocViewer().selection_info)"></td></tr>
	    	<tr><td>addMark</td><td><input type=text style="width:70px;" id="txt_addmark" value="1;0;47"></td><td><input type=submit value="Invoke" onclick="getDocViewer().addMark({selection_info: $('#txt_addmark').val(),has_selection:false,color:'#fffc15'})"></td></tr>
	    	<tr><td>addMark(note)</td><td><input type=text style="width:70px;" id="txt_addmark_note" value="a note"></td><td><input type=submit value="Invoke" onclick="getDocViewer().addMark({note: $('#txt_addmark_note').val(),positionX:200,positionY:200,width:200,height:300,pageIndex:1})"></td></tr>
			<tr><td>addMark page 2(note)</td><td><input type=text style="width:70px;" id="txt_addmark_note" value="a note"></td><td><input type=submit value="Invoke" onclick="getDocViewer().addMark({note: $('#txt_addmark_note').val(),positionX:200,positionY:200,width:200,height:300,pageIndex:2})"></td></tr>
			<tr><td>addMark page 5(note)</td><td><input type=text style="width:40px;" id="txt_addmark_note" value="a note"></td><td><input type=submit value="Invoke" onclick="getDocViewer().addMark({note: $('#txt_addmark_note').val(),positionX:100,positionY:50,width:100,height:400,pageIndex:5})"></td></tr>
	    	<tr><td>removeMark</td><td><input type=text style="width:70px;" id="txt_removemark" value="1;0;47"></td><td><input type=submit value="Invoke" onclick="getDocViewer().removeMark({selection_info: $('#txt_removemark').val(),has_selection:false,color:'#fffc15'})"></td></tr>
	    	<tr><td>scrollToMark</td><td><input type=text style="width:70px;" id="txt_scrolltomark" value="1;0;47"></td><td><input type=submit value="Invoke" onclick="getDocViewer().scrollToMark({selection_info: $('#txt_scrolltomark').val(),has_selection:false,color:'#fffc15'})"></td></tr>
	    	<tr><td>clearMarks</td><td></td><td><input type=submit readonly value="Invoke" onclick="getDocViewer().clearMarks();"></td></tr>
	    	<tr><td>getMarkList</td><td></td><td><input type=submit readonly value="Invoke" onclick="alert(getDocViewer().getMarkList());"></td></tr>
			<tr><td>save/remove annotations</td><td></td><td><input type=submit readonly value="Invoke" onclick="storeList();"></td></tr>
			<tr><td>Load annotations</td><td></td><td><input type=submit readonly value="Invoke" onclick="restoreAnnotations();"></td></tr>
			<tr><td>Debug</td><td></td><td><input type=submit readonly value="Invoke" onclick="test();"></td></tr>
        </table>
        <br/>
        <table border="0" width="230">
        	<tr><th>Event Log</th></tr>
        	<tr><td><textarea rows=6 cols=28 id="txt_eventlog" style="width:215px;"></textarea></td></tr>
        	<tr><td><input type=text style="width:215px;" id="txt_progress" value=""></td></tr>
	    </table>
        </div>
   --%>