<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.net.URL"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="createXml" option="current_dns" append="create_xml" />
<o:url var="contextRootSsl" option="current_dns_to_https" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${param.cid}</title>
	
	<% String userAgent = request.getHeader("User-Agent"); %>
	
	<c:choose>
		<c:when test='<%=userAgent.indexOf("iPad") > -1 || userAgent.indexOf("iPhone") > -1 %>'>
			<meta content="yes" name="apple-mobile-web-app-capable" />
			<meta name="viewport" content="user-scalable=yes, width=100%" />
			<link href="${pageContext.request.contextPath}/css/mobile.css" rel="stylesheet" type="text/css" />
		</c:when>
		<c:otherwise>
			<style type="text/css">
		   		html, body, div, iframe { margin:0; padding:0; height:100%; }
		   		iframe { display:block; width:100%; border:none; }
	  		</style>
		</c:otherwise>
	</c:choose>
</head>
<body>
	<c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">
		<c:choose>
			<c:when test="${not empty param.startPage and 'null' ne param.startPage}">
				<c:choose>
					<c:when test="${'on' eq param.hithighlight}">
						<c:url var="xmlHighlight" value="${createXml}">
							<c:param name="cid" value="${param.cid}" />
						</c:url>
						<c:choose>
							<c:when test="${not empty param.viewer and 'goodreader' eq param.viewer}">
								<c:url var="urlPdf" value="g${contextRootSsl}open_pdf/${param.cid}#page=${param.startPage}#xml=${xmlHighlight}" />
							</c:when>						
							<c:otherwise>
								<c:url var="urlPdf" value="/open_pdf/${param.cid}#page=${param.startPage}#xml=${xmlHighlight}" />							
							</c:otherwise>
						</c:choose>					
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${not empty param.viewer and 'goodreader' eq param.viewer}">
								<c:url var="urlPdf" value="g${contextRootSsl}open_pdf/${param.cid}#page=${param.startPage}" />
							</c:when>						
							<c:otherwise>
								<c:url var="urlPdf" value="/open_pdf/${param.cid}#page=${param.startPage}" />
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>		
				<iframe id="pdf_frame" src="">
				</iframe>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${'on' eq param.hithighlight}">
						<c:url var="xmlHighlight" value="${createXml}">
							<c:param name="cid" value="${param.cid}" />
						</c:url>
						<c:choose>
							<c:when test="${not empty param.viewer and 'goodreader' eq param.viewer}">
								<c:url var="urlPdf" value="g${contextRootSsl}open_pdf/${param.cid}#xml=${xmlHighlight}" />
							</c:when>						
							<c:otherwise>
								<c:url var="urlPdf" value="/open_pdf/${param.cid}#xml=${xmlHighlight}" />
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${not empty param.viewer and 'goodreader' eq param.viewer}">
								<c:url var="urlPdf" value="g${contextRootSsl}open_pdf/${param.cid}" />
							</c:when>						
							<c:otherwise>
								<c:url var="urlPdf" value="/open_pdf/${param.cid}" />
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>	
				<iframe id="pdf_frame" src="">
				</iframe>
			</c:otherwise>
		</c:choose>
	</div>
	<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){			
			$.ajax({
				type: "POST",
			  	url: "${pageContext.request.contextPath}/pdf_info/${param.cid}",
			  	success: function(data) {
					$("#loading").hide();
					$("#main").show();
					
					var UA = "${header['User-Agent']}";
					var OSindex = UA.indexOf("OS");
					var OSver = UA.substr(OSindex, 6 );		
					var ipad2ver = "OS 4_3";
					var isIpad2 = ipad2ver <= OSver;				
			  		if(("${header['User-Agent']}".indexOf("iPad") > -1 && isIpad2) ||
						("${header['User-Agent']}".indexOf("iPhone") > -1)){
							var dimension = data.split(",");
							dimension[0] = parseFloat(dimension[0]) + 16;	  			
							$("#pdf_frame").attr("style", "width:" + dimension[0] + "px; height:" + dimension[1] + "px;");
			  		} 			  		
					$("#pdf_frame").attr("src", "${urlPdf}");					
			  	}
			});
		});	
	
		setInterval(function(){
			$.post("${pageContext.request.contextPath}/pingBook", {dummy : Math.random(), cid : "${param.cid}"});
		}, 180000);
	</script>
</body>
</html>