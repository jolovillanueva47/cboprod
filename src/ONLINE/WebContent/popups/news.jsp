<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>
<body>

<!-- Header -->
<div class="camLogo"><a href="http://www.cambridge.org/"><img src="${pageContext.request.contextPath}/images/cup_logo.jpg" width="130" height="37" title="Cambridge University Press" alt="Cambridge University Press" /></a></div>
<div id="navigation" style="text-align: right; padding-right: 15px;">	
	<a href="javascript:window.close()">Exit</a>
	<div class="clear">&nbsp;</div>
</div>
<!-- Header -->
 
<div class="noBordercontent">
  	<h1>News And Events</h1>  
  	${newsBean.populateNews}
  	<h2><a target="_blank" href="${newsBean.link}"><c:out value="${newsBean.messageTitle}" escapeXml="false" /></a></h2>
  	<p class="answer"><c:out value="${newsBean.message}" escapeXml="false" /></p>	
</div><!-- end content -->

<f:subview id="footerSubview">
	<c:import url="/popups/footer.jsp" />
</f:subview>

<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>