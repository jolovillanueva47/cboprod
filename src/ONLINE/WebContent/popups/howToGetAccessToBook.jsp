<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>	
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
</head>

<body>

<h:form prependId="false"> 
 
<div class="content" style="height: 420px;">
	<h:panelGroup rendered="#{!howToAccess.displayDetails}">
	<ul>
    	<li class="header">How to Get Access to This Book</li>
    	<li></li>
    	<li><font color="#FF0000"><h:messages layout="list" /></font></li>
    	<li></li>
    	<li><h:commandLink value="Go Back" action="#{howToAccess.goBack}" /></li>   	
	</ul>
	</h:panelGroup>
	
	<h:panelGroup rendered="#{howToAccess.displayDetails}">
	<ul>
    	<li class="header">How to Get Access to This Book</li>
    	<li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</li>
	</ul>
 	
 	<font color="#FF0000"><h:messages layout="list" /></font>
 	
	<input name ="exactURL" value='${param.exactURL}' type="hidden" />
    
    <table cellspacing="0"> 
		<tr>
            <td><label for="name">Your name:</label></td>
            <td class="fields">
                <h:inputText id="userName" styleClass="register_field"
            		value="#{howToAccess.userName}" />
            </td>
        </tr>
        
        <tr class="border">
            <td><label for="userEmail">Your email address*:</label></td>
            <td class="fields">
	            <h:inputText id="userEmail" styleClass="register_field" 
						value="#{howToAccess.userEmail}" required="true" 
						validator="#{howToAccess.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Your Email address is required"/>
				</h:inputText>
           </td>
        </tr>
        
        <tr class="border">
            <td><label for="recipientName">Administrators name:</label></td>

            <td>
				<h:inputText id="recipientName" styleClass="register_field" 
					value="#{howToAccess.recipientName}" />
			</td>
			<!--
			<td class="fields">
                <input name="recipientName" value="" class="register_field" id="name" type="text" />
            </td>
        	-->
        </tr>
        
        <tr class="border">
            <td><label for="recipientEmail">Administrators email address*:</label></td>
            <td>
				<h:inputText id="recipientEmail" styleClass="register_field" 
					value="#{howToAccess.recipientEmail}" />
			</td>
			
            <%--
            <td class="fields">
            	<input name="email" value="" class="register_field" id="email" type="text" />
            	<h:inputText id="recipientEmail" styleClass="register_field" 
					value="#{howToAccess.recipientEmail}" required="true" 
					validator="#{howToAccess.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Recipient Email address is required"/>
				</h:inputText>
			</td>--%>
        </tr>
        
        <tr class="border">
            <td><label for="subjectEmail">Subject:</label></td>
            <td>
				<h:inputText id="subjectEmail" styleClass="register_field"  value="#{howToAccess.subject}" readonly="true" size="64"/>
			</td>
        </tr>
        
        <tr class="border">
            <td><label for="content">Message</label></td>
            <td>
            	
				<h:inputTextarea id="body" styleClass="problem" value="#{howToAccess.mailmessage}" cols="23" rows="5" required="true" >				
					<f:attribute name="requiredMessage" value="Message is required"/>
				</h:inputTextarea > 
				
			</td>
			
		<!--<td><textarea name="content" cols="1" rows="5" class="email" id="content"></textarea></td>-->     
        </tr>
        
	</table>

	<div id="bottomButtons">
        <h:commandButton value="Reset" type="reset" />
        <h:commandButton value="Email" id="email_send" action="#{howToAccess.sendEmail}" />
		<h:message for="email_send"/>
    </div>
	</h:panelGroup>
</div><!-- end content -->

</h:form>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>