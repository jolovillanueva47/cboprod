<div id="footer">
<div class="copyright">
  <ul>
        	<li>&copy; Cambridge University Press <script type="text/javascript" language="JavaScript"><!--
var today = new Date();
document.write(today.getFullYear());
//--></script>.</li>
			<li><a href="${pageContext.request.contextPath}/about.jsf" target="_blank">About Cambridge Books Online</a></li>
            <li><a href="${pageContext.request.contextPath}/contact.jsf" target="_blank">Contact Us</a></li>
            <li><a href="${pageContext.request.contextPath}/accessibility.jsf" target="_blank">Accessibility</a></li>
            <li><a href="${pageContext.request.contextPath}/terms_of_use.jsf" target="_blank">Terms of Use</a></li>
            <li><a href="${pageContext.request.contextPath}/privacy_policy.jsf" target="_blank">Privacy Policy</a></li>
            <li><a href="${pageContext.request.contextPath}/rights_and_permissions.jsf" target="_blank">Rights &amp; Permission</a></li>
            <li><a href="${pageContext.request.contextPath}/sitemap.jsf" target="_blank">Site Map</a></li>
        </ul>
    </div>
</div>