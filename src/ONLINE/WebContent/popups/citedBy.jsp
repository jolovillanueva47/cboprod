<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Cited-by Crossref - Cambridge Books Online - Cambridge University Press</title>
	<link href="${pageContext.request.contextPath}/css/popup.css" rel="stylesheet" type="text/css" />	
	<style type="text/css">
		table {
			margin: 0 !important;
			text-align: left !important;
		}
	</style>
</head>

<body>
	<div class="content content_custom">
		<ul>
		   	<li class="header">Cited By (CrossRef)</li>
		</ul>
		<br />
		<table cellspacing="0" cellpadding="0">	
			<c:choose>
				<c:when test="${not empty result}">
					<!-- 
					<tr class="shaded"> 
						<th colspan="2">CrossRef Entries Found:</th>
					</tr>
					 -->
					<c:forEach var="citationVO" items="${result}" varStatus="status">
						<c:choose>
							<c:when test="${1 == status.count}">
								<c:set var="tr_class" value="" />
							</c:when>
							<c:otherwise>
								<c:set var="tr_class" value="border" />
							</c:otherwise>
						</c:choose>
						<tr class="${tr_class}">
							<td>
								<c:forEach var="cont" items="${citationVO.contributors}" varStatus="status">
								${cont}	
									<c:choose>
										<c:when test="${status.last}">.&nbsp;</c:when>
										<c:otherwise>,&nbsp;</c:otherwise>
									</c:choose> 
								</c:forEach>
								&nbsp;&nbsp;
						  		${citationVO.articleTitle}. <i>${citationVO.journalTitle}</i> ${citationVO.year}; 
						  		<c:if test="${not empty citationVO.volume}">
						  			<strong>${citationVO.volume}</strong>: ${citationVO.page}.
						  		</c:if>
						  		&nbsp;&nbsp;
							</td>
							<td>
								<a href="http://dx.doi.org/${citationVO.doi}" target="_blank" title="opens in new window">[CrossRef]</a>
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<!-- 
				<c:otherwise>
					<tr class="shaded">
						<th colspan="2">No CrossRef Entries Found.</th>
					</tr>
				</c:otherwise>
				 -->
			</c:choose>
		</table>
		<c:if test="${empty result}">
			No CrossRef Entries Found.
		</c:if>
	</div><!-- end content -->

	<f:subview id="commonJsSubview">
		<c:import url="/popups/common_js.jsp" />
	</f:subview>

	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>

</f:view>

</html>
