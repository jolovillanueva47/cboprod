<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.online.email.EmailBooklinkBean"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>	
	<f:subview id="commonCssSubview">
		<c:import url="/popups/common_css.jsp" />
	</f:subview>	
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>

<body>

<h:form prependId="false"> 
 
<div class="content" style="height: 460px;">	
    <input type="hidden" name="bid" value="${param.bid}" />
    <input type="hidden" name="cid" value="${param.cid}" />
    <input type="hidden" name="contentType" value="${param.contentType}" />
    
	<h:panelGroup rendered="#{!emailBooklink.displayDetails}">
	<ul>
    	<li class="header">Email Link to This Book</li>
    	<li></li>
    	<li><font color="#FF0000"><h:messages layout="list" /></font></li>
    	<li></li>
    	<li><h:commandLink value="Go Back" action="#{emailBooklink.goBack}" /></li>   	
	</ul>
	</h:panelGroup>
	
	<h:panelGroup rendered="#{emailBooklink.displayDetails}">
	<ul>
    	<li class="header">Email Link to This Book</li>
    	<li>To send a link to this book to a colleague, enter your contact information and your colleague&#8217;s name and email address in the boxes below. Use the "Message" box to add your own message.</li>
       	<li class="bookLink"><a target="book_link" href="${emailBooklink.bookLink}" >${emailBooklink.bookLink}</a></li>       	
	</ul>
	
	<font color="#FF0000"><h:messages layout="list" /></font> 	
	
    <table cellspacing="0"> 
		<tr>
            <td><label for="name">Your name:</label></td>
            <td class="fields">
            	<h:inputText id="userName" styleClass="register_field"
            		value="#{emailBooklink.userName}" />
            </td>
        </tr>
        
        <tr class="border">
            <td><label for="userEmail">Your email address*:</label></td>
            
            <td class="fields">
	            <h:inputText id="userEmail" styleClass="register_field" 
						value="#{emailBooklink.userEmail}" required="true" 
						validator="#{emailBooklink.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Your email address is required"/>
				</h:inputText>
           </td>
        </tr>
        
        <tr class="border">
            <td><label for="recipientName">Recipient's name:</label></td>

            <td>
				<h:inputText id="recipientName" styleClass="register_field" 
					value="#{emailBooklink.recipientName}" />
			</td>
			<%--
			<td class="fields">
                <input name="recipientName" value="" class="register_field" id="name" type="text" />
            </td>
        	--%>
        </tr>
        
        <tr class="border">
            <td><label for="recipientEmail">Recipient's email address*:</label></td>
            <td>
				<h:inputText id="recipientEmail" styleClass="register_field" 
					value="#{emailBooklink.recipientEmail}" required="true"
					validator="#{emailBooklink.validateEmail}" maxlength="80" >
					<f:attribute name="requiredMessage" value="Recipient email address is required"/>
				</h:inputText>
			</td>
        </tr>
        
		<tr class="border">
            <td><label for="subjectEmail">Subject:</label></td>
            <td>
				<h:inputText id="subjectEmail" styleClass="register_field"  value="#{emailBooklink.subject}" readonly="true" size="65"/>
			</td>
        </tr>
        
        <tr class="border">
            <td><label for="content">Message</label></td>
            <td>
            	<c:choose>						
						<c:when test="${fn:contains(pageContext.request.requestURL, 'chapter.jsp')}">	
							${bookBean.bookContentItem.title}							
						</c:when>					
						<c:otherwise>
							${bookBean.bookMetaData.title}
						</c:otherwise>
				</c:choose>
				<h:inputTextarea id="body" styleClass="problem" value="#{emailBooklink.mailmessage}" 
					cols="23" rows="5" required="true" >				
					<f:attribute name="requiredMessage" value="Message is required"/>
				</h:inputTextarea> 
			</td>
			
		<%--<td><textarea name="content" cols="1" rows="5" class="email" id="content"></textarea></td>--%>     
        </tr>
        
	</table>

	<div id="bottomButtons">
		<a href="javascript:doReset();" title="Reset"><img src="${pageContext.request.contextPath}/images/bot_reset.gif" /></a>       	
       	<a href="javascript:doEmail();" title="Email"><img src="${pageContext.request.contextPath}/images/bot_email.gif" /></a>
        <h:commandButton value="Reset" id="resetBtn" type="reset" style="display: none;" />
        <h:commandButton value="Email" id="email_send" style="display: none;" action="#{emailBooklink.sendEmail}" />
		<h:message for="email_send"/>
    </div>
	</h:panelGroup>
</div><%-- end content --%>
</h:form>
<f:subview id="commonJsSubview">
	<c:import url="/popups/common_js.jsp" />
</f:subview>
<script type="text/javascript">
	function doReset() {
		$("#resetBtn").click();
	}
	
	function doEmail() {
		$("#email_send").click();
	}
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</f:view>
</html>