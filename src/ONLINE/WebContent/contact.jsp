<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	
</head>
<body>
	<c:import url="/components/loader.jsp" />
	
	<div id="main" style="display: none;">
	<div id="top_container">
	<div id="page-wrapper">
	
	<f:subview id="subviewIPLogo">
		<c:import url="/components/ip_logo.jsp" />	
	</f:subview>
    	
	<!-- Header Start -->
	<div id="header_container">	
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
	<!-- Header End -->
	<!-- Main Body Start -->
	<div id="main_container">

		<!-- Titlepage Start -->
		<h1 class="titlepage_heading">Contact Us</h1>
		<!-- Titlepage End -->

		<p>Please use the contact details provided below if you would have a query/comment to make about <em>Cambridge Books Online</em> or if you have experienced problems using the site. Please ensure you have checked through our <a class="link03" href="javascript:void(0);" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;">Frequently Asked Questions</a> and <a class="link03" href="#" onclick="showHelp('${pageContext.request.contextPath}/', '<%= session.getAttribute("helpType") %>', '<%= session.getAttribute("pageId") %>', '<%= session.getAttribute("pageName") %>');return false;">Help</a> sections before submitting your query.</p>
		<br/><p>If you would like to purchase access to <em>Cambridge Books Online</em>, please go to <a class="link03" href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/news.jsf?messageId=2364','popup','status=yes,scrollbars=yes,width=750,height=700'); return false;">How to Purchase</a>. To register for a free trial (institutions only), please fill out our <a class="link03" href="${contextRootCjo}registration?SIGNUP=Y" title="Click here to signup!">trial registration form</a>. For information about other online products, please go to <a class="link03" href="http://www.cambridge.org/online/" target="cam_ol">Cambridge Online</a>.</p>
		<br/><p>For all other queries, please refer to your nearest regional office:</p>

		<br/>
		<div>
		<h2>The Americas</h2>	
		For price quotes and trial requests contact: <a class="link03" href="mailto:online@cambridge.org">online@cambridge.org</a>		
		</div>
		
		<br/>
		
		<p>
			For Customer Services and other queries:
			<br/>Cambridge University Press
			<br/>100 Brook Hill Drive
			<br/>West Nyack, NY 10994-2133
		</p>

		<br/>
		
		<p>
			Toll-free: 800-872-7423
			<br/>Toll-free (Mexico): 95-800-010-0200
			<br/>Phone: (845) 353-7500
			<br/>Fax: (845) 353-4141
			<br/>www.cambridge.org/us
		</p>

		<br/>
		<p>
			For technical support: 
			E-mail: <a class="link03" href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a>
		</p> 

		<br/>
		
		<div>
		<h2>United Kingdom and Rest of World</h2>
		For price quotes and trial requests contact: <a class="link03" href="mailto:academicsales@cambridge.org">academicsales@cambridge.org</a>
		<br/>For consortia and multi-site licence pricing contact: <a class="link03" href="mailto:hperrett@cambridge.org">hperrett@cambridge.org</a>		
		</div>
		
		<br/>
		
		<p>
			For Customer Services and other queries:
			<br/><strong>Cambridge University Press</strong>
			<br/>The Edinburgh Building
			<br/>Shaftesbury Road
			<br/>Cambridge
			<br/>CB2 8RU
		</p>
		
		<br/>
		
		<p>
			By phone: +44 (0) 1223 326098
			<br/>By fax: +44 (0) 1223 325152
			<br/>Email: <a class="link03" href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a>
		</p>


	</div>  
	<!-- Main Body End -->

	<f:subview id="subviewFooterMenuwrapper">
		<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	</div>
	</div>

	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	</div>
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>


</body>
</f:view>
</html>