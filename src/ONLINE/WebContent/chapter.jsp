<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRootSsl" option="current_dns_to_https" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	${bookBean.initPage}
	<head>
		<fb:share-button class="meta">
			<meta name="medium" content="mult"/>
			<meta name="title" content="Cambridge Books Online - ${bookBean.bookMetaData.title}"/>
			<meta name="description" content="${bookBean.bookMetaData.blurb}"/>
			<link rel="image_src" href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" />		
		</fb:share-button>  
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<title>
			<c:if test="${not empty bookBean.bookContentItem.label}">
				${bookBean.bookContentItem.label} -
			</c:if> 
			<c:out value="${bookBean.bookContentItem.title}" escapeXml="false" /> - Cambridge Books Online - Cambridge University Press
		</title>
		
		<meta name="keyword" content="${bookBean.bookKeywordMeta},${bookBean.chapterKeywordMeta}" />
	<!-- google scholar -->
			<meta name="citation_title" content="${fn:replace(bookBean.bookMetaData.title, '"', '&quot;')}: ${fn:replace(bookBean.bookContentItem.title, '"', '&quot;')}" />		<!-- title (required)-->
		<c:forEach items="${bookBean.bookMetaData.authorNameLfList}" var="author">	
			<meta name="citation_author" content="${author}" />								<!-- author (required)-->
		</c:forEach>
		<c:if test="${not empty bookBean.bookMetaData.printDate}">
			<meta name="citation_date" content="${bookBean.bookMetaData.printDate}" />		<!-- publication date yyyy or yyyy/mm/dd (required)-->
		</c:if>
		<c:if test="${not empty bookBean.bookMetaData.onlineIsbn}">
			<meta name="citation_isbn" content="${bookBean.bookMetaData.onlineIsbn}" />		<!-- online isbn -->
		</c:if>
		<c:if test="${not empty bookBean.bookMetaData.doi}">
			<meta name="citation_doi" content="${bookBean.bookContentItem.doi}" />				<!-- doi -->
		</c:if>
			<meta name="citation_keywords" content="${bookBean.bookKeywordMeta}, ${bookBean.chapterKeywordMeta}" />
			
			<meta name="citation_abstract_html_url" content="${pageContext.request.contextPath}/chapter.jsf?bid=${bookBean.bookMetaData.id}&cid=${bookBean.bookContentItem.id}" />
	<!-- google scholar -->
	
		<f:subview id="commonCssSubview">
			<c:import url="/components/common_css.jsp" />
		</f:subview>	
	    <link rel="stylesheet" type="text/css" media="print" href="${pageContext.request.contextPath}/css/print.css" />
	   
	   	<!-- Asia Exact Target tracking -->
	   	<%--
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/exact_landing.js"></script>
		 --%>
	</head>
	
	<body>		
		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="1532" scope="session" />
		<c:set var="pageName" value="Chapter Landing" scope="session" />
	
		<c:import url="/components/loader.jsp" />
		<div id="main" style="display: none;"> 
			<div id="top_container">
				<div id="page-wrapper">

					<input type="hidden" id="pageLoader" value=""/>
					<input type="hidden" id="references" value="${bookBean.bookContentItem.referenceFile}"/>
					<input type="hidden" id="tabSelect" value="${tab}"/>
					<input type="hidden" id="isbnHidden" value="${bookBean.bookMetaData.onlineIsbn}" />
					<input type="hidden" id="bookIdHidden" value="${bookBean.bookMetaData.id}" />
					<input type="hidden" id="idHidden" value="${bookBean.bookContentItem.id}" />
					<input type="hidden" id="pageType" value="chapter" />

					<!-- for references -->
					<c:set var="refSelected" value="${param.ref}" />  
					  	
					<!-- for hithighlighting -->
					<c:choose>
						<c:when test="${not empty sessionScope.hithighlight}">
							<input type="hidden" id="hid_hithighlight" value="on" />
						</c:when>
						<c:otherwise>
							<input type="hidden" id="hid_hithighlight" value="off" />
						</c:otherwise>
					</c:choose>
					<c:set var="reader" value="${bookBean.cookieValue}"/>
					<c:set var="bookTitlePdf" value="${bookBean.bookMetaData.title}" scope="request" />
	
					<f:subview id="subviewIPLogo">
						<c:import url="/components/ip_logo.jsp" />	
					</f:subview>
		
					<!-- Header Start -->
					<div id="header_container">
				        	
						<f:subview id="subviewTopMenu">
							<c:import url="/components/top_menu.jsp" />	
						</f:subview> 
						
						<f:subview id="subviewSearchContainer">
							<c:import url="/components/search_container.jsp" />	
						</f:subview>
					</div> 
				    <!-- Header End -->
	    					            			
					<!-- Main Body Start -->
					<div id="main_container">
			            
						<!-- Breadcrumbs Start -->
						<f:subview id="crumbtrailContainer">
							<c:import url="/components/crumbtrail.jsp" />	
						</f:subview>
						<!-- Breadcrumbs End -->
			                                
						<div id="content_wrapper01">
              
							<div id="content_wrapper07">              
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>	                  
										<!-- Book Thumbnail Start -->
										<c:if test="${not empty bookBean.bookMetaData.thumbnailImageFilename}">
											<td class="thumb_landing">
						                    	<a href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox"><img src="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.thumbnailImageFilename}" alt="thumbnail"/></a>
						                        <ul>
						                          <li><span class="icons_img enlarge">&nbsp;</span><a href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox">Enlarge Image</a></li>
						                        </ul>
						                    </td>
										</c:if>
										<!-- Book Thumbnail End -->
		                    
										<!-- Book Info Start -->
										<td class="div_book_info">
											<h3>								
											<b>
												<c:if test="${not empty bookBean.bookContentItem.label}">
													${bookBean.bookContentItem.label} -
												</c:if>
												${bookBean.bookContentItem.title}&nbsp;
											</b> 
											pp. ${bookBean.bookContentItem.pageStart}-${bookBean.bookContentItem.pageEnd}</h3>
											<c:if test="${not empty bookBean.bookContentItem.contributorNames}">
												By <c:out value="${bookBean.bookContentItem.contributorNames}" escapeXml="false" />
											</c:if>		
											<% 
												String userAgent = request.getHeader("User-Agent"); 
												session.setAttribute("prevPath",request.getRequestURL());
												session.setAttribute("bdoi", request.getParameter("bid"));
												session.setAttribute("cdoi", request.getParameter("cid"));
												session.setAttribute("pId", request.getParameter("p"));
											%>		
																					
											<c:choose>
												<c:when test="${bookBean.bookContentItem.isFreeContent or bookBean.hasAccess}">
				                           			<ul><!-- pdf enabled -->
			                           					<li class="pdf_link">
			                           						<c:choose>
																<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																<c:choose>
																	<c:when test="${bookBean.cookieValue eq 2}">
																		<span class="icons_img pdf_goodreader">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showPdfFromLandingPage('${bookBean.bookMetaData.id}', '${bookBean.bookContentItem.id}', 'goodreader');return false;"><strong>View chapter as PDF</strong></a>																		
																	</c:when>
																	<c:otherwise>
																		<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showPdfFromLandingPage('${bookBean.bookMetaData.id}', '${bookBean.bookContentItem.id}', '');return false;"><strong>View chapter as PDF</strong></a>
																	</c:otherwise>
																</c:choose>
																</c:when>
																<c:otherwise>
			                           								<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="showPdfFromLandingPage('${bookBean.bookMetaData.id}', '${bookBean.bookContentItem.id}', '');return false;"><strong>View chapter as PDF</strong></a>
			                           							</c:otherwise>
															</c:choose>	
			                           					</li>
			                        				</ul>
												</c:when>
												<c:otherwise>
													<ul><!-- pdf disabled -->
														<li class="disabled_link">
															<c:choose>
																<c:when test="${not empty userInfo and not empty userInfo.username}">
																	<c:choose>
																		<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																			<c:choose>
																				<c:when test="${bookBean.cookieValue eq 2}">
																					<span class="icons_img pdf_goodreader_disabled">&nbsp;</span><strong>View chapter as PDF</strong>
																				</c:when>
																				<c:otherwise>
																					<span class="icons_img pdf_off">&nbsp;</span><strong>View chapter as PDF</strong>
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<span class="icons_img pdf_off">&nbsp;</span><strong>View chapter as PDF</strong>
																		</c:otherwise>
																	</c:choose>													
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																			<c:choose>
																				<c:when test="${bookBean.cookieValue eq 2}">
																					<span class="icons_img pdf_goodreader_disabled">&nbsp;</span><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290"><strong>View chapter as PDF</strong></a>
																				</c:when>
																				<c:otherwise>
																					<span class="icons_img pdf_off">&nbsp;</span><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290"><strong>View chapter as PDF</strong></a>
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<span class="icons_img pdf_off">&nbsp;</span><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290"><strong>View chapter as PDF</strong></a>
																		</c:otherwise>
																	</c:choose>			
																</c:otherwise>
															</c:choose>
														</li>
													</ul>
												</c:otherwise>
											</c:choose>
											<!-- Icon PDF START -->
													
											<p>									
												<strong><a href="ebook.jsf?bid=${bookBean.bookMetaData.id}">${bookBean.bookMetaData.title}</a></strong><br />
												<c:if test="${not empty bookBean.bookMetaData.subtitle}">									
													<strong><c:out value="${bookBean.bookMetaData.subtitle}" escapeXml="false" /></strong><br/>
												</c:if>	
												<c:if test="${not empty bookBean.bookMetaData.volumeNumber and '0' ne bookBean.bookMetaData.volumeNumber}">
													Volume <c:out value="${bookBean.bookMetaData.volumeNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.volumeTitle)? '<br/>': ',' }" escapeXml="false"/> 
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.volumeTitle}">
													<c:out value="${bookBean.bookMetaData.volumeTitle}" escapeXml="false" /><br/> 
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.partNumber and '0' ne bookBean.bookMetaData.partNumber}">
													Part <c:out value="${bookBean.bookMetaData.partNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.partTitle)? '<br/>': ',' }" escapeXml="false"/>
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.partTitle}">
													<c:out value="${bookBean.bookMetaData.partTitle}" escapeXml="false" /><br/> 
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.edition}">
													<c:out value="${bookBean.bookMetaData.edition}" escapeXml="false" /><br />
												</c:if>				
												
												<c:set var="authorRoleTitle" value="${bookBean.bookMetaData.authorRoleTitleList}" />
												<c:set var="authorAffiliation" value="${bookBean.bookMetaData.authorAffiliationList}" />
												<c:forEach items="${bookBean.bookMetaData.authorNameList}" var="author" varStatus="status">												
													<c:out value="${authorRoleTitle[status.count - 1]}" /> <c:out value="${author}" escapeXml="false" /><br />
													<c:if test="${not empty authorAffiliation and not empty authorAffiliation[status.count - 1] and 'none' ne authorAffiliation[status.count - 1]}">														
														<i><c:out value="${authorAffiliation[status.count - 1]}" escapeXml="false" /></i><br />	
													</c:if>															
													<c:if test="${not status.last}">
														<br />
													</c:if>
												</c:forEach>													
											</p>
											
											<p>
												<c:if test="${not empty bookBean.bookMetaData.series}">
													<c:url value="/series_landing.jsf" var="urlSeries">
														<c:param name="seriesCode" value="${bookBean.bookMetaData.seriesCode}" />
														<c:param name="seriesTitle" value="${bookBean.bookMetaData.series}" />
														<c:choose>
															<c:when test="${not empty bookBean.bookMetaData.seriesNumber}">
																<c:param name="sort" value="series_number" />
															</c:when>
															<c:otherwise>
																<c:param name="sort" value="print_date" />
															</c:otherwise>
														</c:choose>
													</c:url>
									            	<strong><a href="<c:out value="${urlSeries}" escapeXml="true" />"><c:out value="${bookBean.bookMetaData.series}" escapeXml="false" /></a></strong>
													<c:if test="${not empty bookBean.bookMetaData.seriesNumber}">
														<c:out value=" (No. ${bookBean.bookMetaData.seriesNumber})" escapeXml="false" />
													</c:if>
													<br/>
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.printDate}">
													<b>Print Publication Year:</b> ${bookBean.bookMetaData.printDate}
												</c:if>									
												<c:if test="${not empty bookBean.bookMetaData.onlineDate}">
													<br/><b>Online Publication Date:</b> ${bookBean.bookMetaData.onlineDate}
												</c:if>									
											</p>
						
											<p>
												<c:if test="${not empty bookBean.bookMetaData.onlineIsbn}">
													<b>Online ISBN:</b> ${bookBean.bookMetaData.onlineIsbn}
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.hardbackIsbn}">
												 	<br/><b>Hardback ISBN:</b> ${bookBean.bookMetaData.hardbackIsbn}
												</c:if>
												<c:if test="${not empty bookBean.bookMetaData.paperbackIsbn}">
												 	<br/><b>Paperback ISBN:</b> ${bookBean.bookMetaData.paperbackIsbn}
												</c:if>
											</p>
				
											<p>
												<c:if test="${not empty bookBean.bookContentItem.doi}">
												 	<b>Chapter DOI:</b> <a href="http://dx.doi.org/${bookBean.bookContentItem.doi}">http://dx.doi.org/${bookBean.bookContentItem.doi}</a>
												</c:if>
											</p>
											
											<p>
												<span class="Z3988" title="${bookBean.coinString}">&nbsp;</span>
											</p>
											
											<p>
												<b>Subjects:</b>
												<c:forEach items="${bookBean.bookMetaData.subjectList}" var="subject" varStatus="status">
													<c:url value="/subject_landing.jsf" var="subjectLandingUrl">
														<c:param name="searchType" value="allTitles" />
														<c:param name="subjectCode" value="${bookBean.bookMetaData.subjectCodeList[status.count - 1]}" />
														<c:param name="subjectName" value="${subject}" />
													</c:url>
													
													<a href="<c:out value="${subjectLandingUrl}" escapeXml="true" />">${subject}</a><c:if test="${not status.last}">,</c:if>
												</c:forEach> 
											</p>
										</td>
										<!-- Book Info End -->
										<td id="rightmenu_wrapper">
					                    	<f:subview id="subviewBookInfoLinks">
												<c:import url="/components/book_info_links.jsp" />
											</f:subview>  
											<f:subview id="subviewRecommendationPrint">
												<c:import url="/components/recommendation_print_links.jsp" />
											</f:subview>
										</td>
										<c:if test="${not bookBean.hasAccess}">
												<c:if test="${supplemental.salesModuleFlag eq 'N'}">
													<blockquote>This title has been withdrawn from sale on Cambridge Books Online. Institutions who purchased this title previous to removal will continue to have access via Cambridge Books Online but this title can no longer be purchased.</blockquote>	
												</c:if>	
										</c:if>
									</tr>
								</table>					
								<div class="clear"></div>
							</div>
	
							<!-- Box Tab Start -->
							<div id="border_container">                  
								<div class="box">  
									<!-- Tab Menu Start -->
									<div class="boxTop">
										<ul class="tabMenu">		
											<c:choose>
												<c:when test="${not empty bookBean.bookContentItem.abstractText}">
													<li class="tab_rounded ${empty refSelected ? 'chapter_extract selected' : 'chapter_extract'}"><a title="Chapter Extract">Chapter Extract</a></li>	
													<c:set var="toc_tab_divider" value="tab_divider"/>	
												</c:when>
												<c:otherwise>
													<c:set var="toc_tab_divider" value="tab_rounded"/>	
												</c:otherwise>
											</c:choose>	
											<c:set var="tocTabClass" value="${not empty bookBean.bookContentItem.abstractText ? 'tableofcontents' : 'toc_page'}"/>
											<li class="${toc_tab_divider} ${tocTabClass} ${empty refSelected and empty bookBean.bookContentItem.abstractText ? 'selected' : ''}"><a title="Table of Contents">Table of Contents</a></li>
											<c:if test="${not empty bookBean.bookMetaData.volumeNumber}">
												<li class="tab_divider ${3 eq refTabSelected ? 'volumes selected' : 'volumes'}"><a title="Volume">Volumes</a></li>
											</c:if>	
											<c:choose>
												<c:when test="${not empty bookBean.bookMetaData.referenceList}">
													<c:set var="refList" value="true" scope="session" />
													<li class="tab_divider ${1 eq refSelected ? 'reference selected' : 'reference'}">
														<a title="References">References</a>
													</li>
												</c:when>
												<c:otherwise>
													<c:set var="refList" value="false" scope="session" />
												</c:otherwise>
											</c:choose>	
										</ul>
									</div>
									<!-- Tab Menu End -->
				
									<div class="boxBody">
										<c:if test="${not empty bookBean.bookContentItem.abstractText}">
											<!-- 1st Tab Content Start -->
											<div class="${empty refSelected ? 'show tabContent' : 'tabContent'}">
				
												<p class="title_container">
													<c:if test="${not empty bookBean.bookContentItem.abstractFilename}">	    			
					    								<c:if test="${'none' eq bookBean.bookContentItem.abstractProblem or empty bookBean.bookContentItem.abstractProblem}">
															<a id="anchorImageExtract" href="javascript:void(0);">Image View</a>
															
															<span style="display: none;" id="imageShown">
																<a id="anchorHtmlExtract" href="javascript:void(0);">Text View</a>
																| <a target="extract" href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}">Enlarge Image</a>
															</span>
														</c:if>
														
														<c:if test="${bookBean.currChapterPosition ne -1 }">
															<span class="chapter_nav" >
																<c:set var="chapNav" value="${bookBean.chapNav}"></c:set>
																<c:set var="currIndex" value="${bookBean.currChapterPosition}"></c:set>
																<c:set var="chapNavLen" value="${fn:length(chapNav)-1}" />
																<c:set var="prevPos" value="${currIndex eq 0 ? chapNavLen : currIndex-1}" />
																<c:set var="nextPos" value="${currIndex eq chapNavLen ? 0 : currIndex+1}" />
																
																<c:set var="prevNavStyle" value="${currIndex eq 0 ? 'background: #bdccd3; border: 1px solid #87949a;':''}"/>
																<c:set var="nextNavStyle" value="${currIndex eq chapNavLen ? 'background: #bdccd3; border: 1px solid #87949a;':''}"/>
																
																<c:set var="prevNavHref" value="${pageContext.request.contextPath}/chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[prevPos].id}" />
																<c:set var="nextNavHref" value="${pageContext.request.contextPath}/chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[nextPos].id}" />
																
																<!-- previous -->
																<c:choose>
																	<c:when test="${currIndex eq 0}">
																		<a href="#" style="${prevNavStyle}" title="No Previous Chapter"> &lsaquo;<span class="hide_text"> Previous Chapter</span></a>
																	</c:when>
																	<c:otherwise>
																		<a  href="${prevNavHref}" title="Previous Chapter"> &lsaquo;<span class="hide_text"> Previous Chapter</span></a>
																	</c:otherwise>
																</c:choose>
		
																<!-- next -->
																<c:choose>
																	<c:when test="${currIndex eq chapNavLen}">
																		<a href="#" style="${nextNavStyle}" title="No Next Chapter"> &rsaquo;<span class="hide_text"> Previous Chapter</span></a>
																	</c:when>
																	<c:otherwise>
																		<a  href="${nextNavHref}" title="Next Chapter"> &rsaquo;<span class="hide_text">Next Chapter</span></a>
																	</c:otherwise>
																</c:choose>
															</span>														
														</c:if>
													</c:if>
												</p>
				
												<c:choose>
													<c:when test="${'none' eq bookBean.bookContentItem.abstractProblem or empty bookBean.bookContentItem.abstractProblem}">			
														<div id="divHtmlExtract" style="display: block;">
													    	<c:choose>
													    		<c:when test="${not empty bookBean.bookContentItem.abstractText}">										    			
													    			<c:out value="${bookBean.bookContentItem.highlightedAbstractText}" escapeXml="false"/>
													    		</c:when>
																<c:otherwise>No extract available.</c:otherwise>
															</c:choose>		
														</div>
														<div id="divImageExtract" style="display: none;">
															<span class="extractImage">
																<a href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" target="extract">
																	<img src="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" title="${bookBean.bookContentItem.title}" alt="${bookBean.bookContentItem.title}" />
																</a>
															</span>
														</div>
				
													</c:when>
													<c:otherwise>
														<p>
															<c:choose>			
																<c:when test="${not empty bookBean.bookContentItem.abstractText}">
																	<div style="display: block;" id="divImageExtract">
																		<span class="extractImage">														
																			<a href="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" target="extract">
																				<img src="${pageContext.request.contextPath}/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" title="${bookBean.bookContentItem.title}" alt="${bookBean.bookContentItem.title}" />
																			</a>
																		</span>
																	</div>
																</c:when>				
																<c:otherwise>No extract available.</c:otherwise>				
															</c:choose>
														</p>
													</c:otherwise>
												</c:choose>
											</div>  
											<!-- 1st Tab Content End -->
										</c:if>
			
										<!-- 2nd Tab Content Start -->
										<div class="${empty refSelected and empty bookBean.bookContentItem.abstractText ? 'show tabContent' : 'tabContent'}">					                                
											<!-- TOC List Start -->
											<table cellspacing="0" cellpadding="0">
												<c:forEach items="${bookBean.bookTocItemList}" var="bookTocItem" varStatus="status">
													<tr>
														<c:choose>
															<c:when test="${'root' ne bookBean.bookTocItemLevelMap[bookTocItem.id]}">
																<c:set var="indent" value="text_indent_lev01" />
																<c:choose>
																<c:when test="${1 ne bookBean.bookTocItemLevelMap[bookTocItem.id]}">
																	<c:set var="padding_px" value="${bookBean.bookTocItemLevelMap[bookTocItem.id] * 15}" />
																	<c:set var="style_level" value="padding: 8px 0 8px ${padding_px}px;" />
																</c:when>
																<c:otherwise>
																	<c:set var="padding_px" value="" />
																	<c:set var="style_level" value="" />
																</c:otherwise>
															</c:choose>				
															</c:when>
															<c:otherwise>
																<c:set var="indent" value="" />
															</c:otherwise>
														</c:choose>														
														<td class="${indent}" style="${style_level}">				
															<span class="fl">	
																<c:choose>
																	<c:when test="${'Y' eq bookTocItem.displayFlag}">
																		<c:if test="${not empty bookBean.tocItemMap[bookTocItem.id]}">
																			<a id="${bookTocItem.id}" class="tocItem" href="javascript:void(0);">+</a>
																		</c:if>
																		<a href="chapter.jsf?bid=${bookTocItem.bookId}&amp;cid=${bookTocItem.id}">
																		<c:if test="${not empty bookTocItem.label}">
																			${bookTocItem.label} -
																		</c:if>
																		<c:out value="${bookTocItem.title}" escapeXml="false" /><span class="double_quote">:</span></a>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${not empty bookTocItem.label}">
																			${bookTocItem.label} -
																		</c:if>
																		<c:out value="${bookTocItem.title}" escapeXml="false" />
																	</c:otherwise>
																</c:choose>																
															</span>	
															<c:if test="${'Y' eq bookTocItem.displayFlag}">
																<c:choose>
																	<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">
																		<ul class="pdf_container">
																			<li>
																				<c:choose>
																					<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																					<c:choose>
																						<c:when test="${bookBean.cookieValue eq 2}">
																							<span class="icons_img pdf_goodreader">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', 'goodreader');return false;">Read PDF</a>																						
																						</c:when>
																						<c:otherwise>
																							<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '');return false;">Read PDF</a>																						
																						</c:otherwise>
																					</c:choose>
																					</c:when>
																					<c:otherwise>
																						<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="showPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '');return false;">Read PDF</a>																	
																					</c:otherwise>
																				</c:choose>
																			</li>
																		</ul>	
																	</c:when>
																	<c:otherwise>
																		<c:choose>
																			<c:when test="${not empty userInfo and not empty userInfo.username}">
																				<ul class="pdf_container">
																					<li class="disabled_link">
																						<c:choose>
																							<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																								<c:choose>
																									<c:when test="${bookBean.cookieValue eq 2}">
																										<span class="icons_img pdf_goodreader_disabled">&nbsp;</span>Read PDF
																									</c:when>
																									<c:otherwise>
																										<span class="icons_img pdf_off">&nbsp;</span>Read PDF
																									</c:otherwise>
																								</c:choose>
																							</c:when>
																							<c:otherwise>
																								<span class="icons_img pdf_off">&nbsp;</span>Read PDF
																							</c:otherwise>
																						</c:choose>
																					</li>
																				</ul>
																			</c:when>
																			<c:otherwise>
																				<ul class="pdf_container">
																					<li>
																						<c:choose>
																							<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																								<c:choose>
																									<c:when test="${bookBean.cookieValue eq 2}">
																										<span class="icons_img pdf_goodreader_disabled">&nbsp;</span><span class="disabled_link"><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a></span>
																									</c:when>
																									<c:otherwise>
																										<span class="icons_img pdf_off">&nbsp;</span><span class="disabled_link"><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a></span>
																									</c:otherwise>
																								</c:choose>
																							</c:when>
																							<c:otherwise>
																								<span class="icons_img pdf_off">&nbsp;</span><span class="disabled_link"><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a></span>																		
																							</c:otherwise>
																						</c:choose>
																					</li>
																				</ul>
																			</c:otherwise>
																		</c:choose>														
																	</c:otherwise>
																</c:choose>																														
																<c:if test="${not empty bookTocItem.contributorNames}">												
																	<span class="contributor">By <c:out value="${bookTocItem.contributorNames}" escapeXml="false" /></span>
																</c:if>
															</c:if>	
														</td>
														<td class="toc_number">
															<c:if test="${'Y' eq bookTocItem.displayFlag}">
																pp. ${bookTocItem.pageStart}<c:if test="${not empty bookTocItem.pageEnd}">-${bookTocItem.pageEnd}</c:if>
															</c:if>
														</td>
													</tr>
													<c:if test="${not empty bookBean.tocItemMap[bookTocItem.id]}">
														<c:forEach items="${bookBean.tocItemMap[bookTocItem.id]}" var="tocItems">
															<tr class="${bookTocItem.id} forHide">																
																<c:set var="toc_indent" value="text_indent_lev01" />
																<c:choose>
																	<c:when test="${'root' ne bookBean.tocItemLevelMap[tocItems.id]}">
																		<c:set var="toc_padding_px" value="${(bookBean.tocItemLevelMap[tocItems.id] + 2) * 15}" />
																		<c:set var="toc_style_level" value="padding: 8px 0 8px ${toc_padding_px}px;" />
																	</c:when>
																	<c:otherwise>
																		<c:set var="toc_style_level" value="padding: 8px 0 8px 30px;" />
																	</c:otherwise>
																</c:choose>	
																<td class="${toc_indent}" style="${toc_style_level}">
																	<span class="fl">
																		<c:out value="${tocItems.text}" escapeXml="false" />&nbsp;
																	</span>														
																	<c:choose>
																		<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">
																			<ul class="pdf_container">	
																				<li>
																					<c:choose>
																						<c:when test='<%=userAgent.indexOf("iPad") > -1 %>'>
																							<c:choose>
																								<c:when test="${bookBean.cookieValue eq 2}">
																									<span class="icons_img pdf_goodreader">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showContentTocPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '${tocItems.pageStart}', '${bookTocItem.pageStart}', 'goodreader');return false;">Read PDF</a>																																															
																								</c:when>
																								<c:otherwise>
																									<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showContentTocPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '${tocItems.pageStart}', '${bookTocItem.pageStart}', '');return false;">Read PDF</a>																																														
																								</c:otherwise>
																							</c:choose>
																						</c:when>
																						<c:otherwise>
																							<span class="icons_img pdf_on">&nbsp;</span><a href="#" onclick="${bookBean.cookieValue};showContentTocPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '${tocItems.pageStart}', '${bookTocItem.pageStart}', '');return false;">Read PDF</a>																																								
																						</c:otherwise>
																					</c:choose>
																				</li>																				
																			</ul>														
																		</c:when>
																		<c:otherwise>
																			<c:choose>
																				<c:when test="${not empty userInfo and not empty userInfo.username}">
																					<ul class="pdf_container">
																						<li class="disabled_link"><span class="icons_img pdf_off">&nbsp;</span>Read PDF</li>
																					</ul>
																				</c:when>
																				<c:otherwise>	
																					<ul class="pdf_container">
																						<li><span class="icons_img pdf_off">&nbsp;</span><span class="disabled_link"><a class="thickbox loginLink" href="${contextRootSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a></span></li>																						
																					</ul>	
																				</c:otherwise>
																			</c:choose>
																		</c:otherwise>
																	</c:choose>
																</td>
																<td class="toc_number">${tocItems.pageStart}</td>	
															</tr>
														</c:forEach>
													</c:if>	
												</c:forEach>
											</table>
											<!-- TOC List End-->	
										</div>
										<!-- 2nd Tab Content End -->
										
										<!-- volume Tab Content Start -->
										 <div id="volumeWrapper" class="${3 eq refTabSelected ? 'show tabConten arrow_05' : 'tabContent arrow_05'}">
										 	<c:forEach items="${bookBean.volumeList}" var="BookMetaData" varStatus="status">
									 			<dl class="volume_list">                                    
				                                	<!-- Volume item start -->
				                                    <dl>
				                                    	<h3><a href="ebook.jsf?bid=${BookMetaData.id}">${BookMetaData.title}</a> Volume ${BookMetaData.volumeNumber}</h3>
				                                    	<p><c:out value="${BookMetaData.authorSingleline}" /></p>

				                                        <dt>
				                                            <p><strong>Print Publication Year:</strong> ${BookMetaData.printDate}</p>
				                                            <p><strong>Online Publication Date:</strong> ${BookMetaData.onlineDate}</p>
				                                            <p><strong>Online ISBN:</strong> ${BookMetaData.onlineIsbn}</p>
				                                        </dt>  
				                                        <dt>
				                                            <p><strong>Paperback ISBN:</strong> ${BookMetaData.paperbackIsbn}</p>
				                                            <p><strong>Book DOI:</strong> <a href="http://dx.doi.org/${BookMetaData.doi}">http://dx.doi.org/${BookMetaData.doi}</a></p>
				                                        </dt> 
				                                    </dl>
				                                    <!-- Volume item end -->   
                               					</dl>
										 	</c:forEach>
										 	<dl>&nbsp;</dl>
										 </div>
										<!-- volume Tab Content End -->
										
										<!-- 3rd Tab Content Start -->
			                            <div id="referencesWrapper" class="${1 eq refSelected ? 'show tabContent arrow_05' : 'tabContent arrow_05'}">
											<c:choose>
												<c:when test="${1 eq refSelected and refList}">
													<c:forEach items="${bookBean.bookMetaData.referenceList}" var="referenceItem">
														<input type="hidden" value="${referenceItem.file}" />														
														<f:subview id="reference">
															<c:import url="${contextRootSsl}content/${referenceItem.file}"></c:import>
														</f:subview>
													</c:forEach>
												</c:when>
												<c:otherwise><p>No references available.</p></c:otherwise>
											</c:choose>
										</div>
										<!-- 3rd Tab Content End -->
										<div id="loadingImg" style="display: none;"><img style="margin-left: 3px;" src="${pageContext.request.contextPath}/images/circle_loading.gif" alt="Loading..." /></div>
										<br/><br/>
										<a id="refOpenURLLink" class="thickbox"></a>			
									</div>			
									<div class="boxBottom"></div>			
								</div>
							</div>
							<!-- Box Tab End -->
						</div> 
			    	</div>
			    	<!-- Main Body End -->

					<f:subview id="subviewFooterMenuwrapper">
						<c:import url="/components/footer_menuwrapper.jsp" />
					</f:subview>

				</div>
			</div>
			<f:subview id="subviewFooterContainer">
				<c:import url="/components/footer_container.jsp" />
			</f:subview>
		</div>	
		<f:subview id="commonJsSubview">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/tabmenu.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/tab.js"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/rightmenu.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/book.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/chapter.js"></script>
		<f:subview id="googleAnalyticsSubview">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
		
		<!-- Asia Exact Target tracking START -->	
		<%--
		<script type="text/javascript">
			var convid = "${bookBean.bookMetaData.onlineIsbn}";
			var displayorder = "1";
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/exact_conversion.js"></script>
		 --%>
		<!-- Asia Exact Target tracking END -->	
	</body>

</f:view>

</html>