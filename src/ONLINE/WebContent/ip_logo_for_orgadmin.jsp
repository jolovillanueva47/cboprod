<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.util.Set"%>
<%@page import="java.io.File"%>
<%@ page import="java.util.Date" %>
<%@page import="org.cambridge.ebooks.online.login.logo.EBookLogoBean"%>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRoot" option="current_dns" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<f:view>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=7" />
		
		<title>Cambridge Books Online - Cambridge University Press</title>
		
		<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/style_org_details.css">
	</head>

	<body>			
		<!-- Org Info Start -->   
		<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
	  		<div id="org_container">
    			<div id="org_content">
    				<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
    				<c:if test="${eBookLogoBean.hasLogo}" >
    					<img id="orgLogoImg" src='${contextRoot}organisation/logo/org${eBookLogoBean.orgLogoId}' width="85" height="35" alt=""/>	
    				</c:if>		    				
    				<p class="orgname_container">${eBookLogoBean.showLogoNamesUnderMax}</p>	    				
    				<p>
    					<c:out value="${fn:replace(eBookLogoBean.textMessage,'null,','')}" escapeXml="false" />	    				
    					<c:if test="${eBookLogoBean.logoNameCount gt 3 or not empty eBookLogoBean.textMessage}" >
    						&nbsp;<a href="#" onclick="MM_openWindow('${pageContext.request.contextPath}/popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">more ...</a>
    					</c:if>	 				
   					</p>
    			</div>
    		</div>    
		</c:if>
		<!-- Org Info End --> 
		
		<f:subview id="common_js">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
	</body>
</f:view>
</html>