<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.cambridge.ebooks.online.subscription.SubscriptionServlet"%>


<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	
	<style type="text/css">
	<!--	
	@import url("${pageContext.request.contextPath}/orgadmin/css/account_admin_styles.css");	
	-->
  	</style>
</head>

	<!-- body (holds country classes) -->
	<body > 
			
		<!-- hidden fields -->
		<input id="selectedOrg" type="hidden" value="${param.SELECTED_ORG}"/>
			
		<!-- bg Topline -->       
		<div id="top_container">
		
			<!-- Page Wrapper Start -->
			<div id="page-wrapper">
				<f:subview id="subviewIPLogo">
					<c:import url="/components/ip_logo.jsp" />	
				</f:subview>
				<div class="clear"></div>
				
				<!-- Header Start -->
				<div id="header_container">
				    <f:subview id="subviewTopMenu">
						<c:import url="/components/top_menu.jsp" />	
					</f:subview>
					
					
			           
					<f:subview id="subviewSearchContainer">
						<c:import url="/components/search_container.jsp" />	
					</f:subview>
				</div>
				
					
				<!-- Main Container Start -->
				<div id="main_container">
					<div class="titlepage_heading"><h1>Organisational Access Details</h1></div>
					
					<!-- bread crumb hidden -->
					<f:subview id="crumbtrailContainer">
						<c:set value="crumbStyle" var="display: none;" />
						<c:import url="/components/crumbtrail.jsp" />	
					</f:subview>
				    <!-- bread crumb hidden -->
				            
					<div class="clear"></div>      
					<p>Below is a list of Cambridge Books Online titles to which your organisation has access.</p>
				
					<!-- Admin Content Start -->
					<div id="adminContent" class="text_01">
					
					<!-- displays the orgList -->
					${accessDetails.initBean}
					<c:if test="${not empty accessDetails.orgList}" >
						<select class="select_02" name="target_1" id="orgList"  >            
							<c:forEach items="${accessDetails.orgList}" var="org">
								<c:choose>
									<c:when test="${org.bodyId eq param.SELECTED_ORG}">
										<option value="${org.bodyId}" selected="selected">${org.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${org.bodyId}">${org.name}</option>
									</c:otherwise>					
								</c:choose>
							</c:forEach>
						</select>	
					</c:if>
					
					&nbsp;Sort By:&nbsp;<select name="sortBy" id="sortBy" class="select_02">						
						<option value="1" selected="selected">Title</option>
						<option value="2">ISBN</option>
					</select>					
					
					<!-- Tab Container Start -->
					<div id="tabcontentcontainer">
				    
				    	<!-- Tab Content Start -->
				        <div id="tab1" class="tabcontent" style="display:block">					
							<!-- Admin Tab Start -->
							<div id="adminTab">
								<!-- Individual Titles Start -->
								<c:if test="${not empty indi_servlet}">	
								<a id="indivTab" class="link03" title="Click here for Individual Collection" onclick="findItems('<%=SubscriptionServlet.INDI %>','div_05')" href="javascript:void(0);">
									<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Individual Titles</h1>
								</a>
								<div id="div_05" style="display:none">	
									<ul id="tree">
   										<li>
       										<ul class="subTree">
       										<c:forEach var="indi_" items="${indi_servlet}">
           										<li><a href="${pageContext.request.contextPath}/ebook.jsf?bid=${indi_.bookId}"><c:out value="${indi_.isbn}" escapeXml="false" /></a>
           											<ul>
           												<li><a href="${pageContext.request.contextPath}/ebook.jsf?bid=${indi_.bookId}"><c:out value="${indi_.title}" escapeXml="false" /></a></li>
           											</ul>
           										</li>
           										</c:forEach>
           									</ul>
           								</li>
									</ul>
								</div>	
								</c:if>
								<!-- Individual Titles End -->
								
								<!-- Collections Start -->
								<c:if test="${not empty coll_servlet}">	
									<a id="collectionsTab" class="link03" title="Click here for Collection" onclick="findItems('<%=SubscriptionServlet.COLL%>','div_06')" href="javascript:void(0);">
										<h1><img src="${pageContext.request.contextPath}/images/arrow07.gif"/>Collections</h1>
									</a>
									<div id="div_06" style="display:none">
									 	<ul id="tree">
									    	<c:forEach var="coll" items="${coll_servlet}" varStatus="x">
									   			<c:set var="id" value="${coll.collectionId}_o${x.count - 1}" scope="page"/>
									    		<li class="bookTitle">
	      												<a onclick='findBooks("${id}", "${param.sortBy}")' id="${id}" href="javascript:void(0);" name="${id}">
	      												<img src="${pageContext.request.contextPath}/images/icon_plus.jpg" /></a><c:out value="${coll.title}" escapeXml="false" />
	      										</li>
	      										<li>${coll.description}</li>
									    	</c:forEach>
									  	</ul> 
									</div>	
								</c:if>
								<!-- Collections End -->		
							</div>
							<!-- Admin Tab End -->
							
						                	
				            </div>
				            <!-- Tab Content End -->	
				       	
					       	<!-- Free Trial Start -->
					        <div id="tab2" class="tabcontent" style="display:block">
						        <c:choose>
									<c:when test="${accessDetails.freeTrial}">
										Free Trial Active
									</c:when>
									<c:otherwise>
										Free Trial Inactive
									</c:otherwise>
								</c:choose>
					        </div>
					        <!-- Free Trial Start -->
				        </div>
				        <!-- Tab Container End -->
				    </div>
				    <!-- Admin Content End -->
				</div>
				<!-- Main Container End -->									
							
				<div class="clear">&nbsp;</div>		
								
				<f:subview id="subviewFooterMenuwrapper">
					<c:import url="/components/footer_menuwrapper.jsp" />
				</f:subview>
				
			</div>
			<!-- Page Wrapper End -->		
		
		</div>
		<!--  Top Container -->	
		
		<div id="debug" style="display:none">		
		</div>
			
		<f:subview id="subviewFooterContainer">
				<c:import url="/components/footer_container.jsp" />
		</f:subview>
	
	  	
		<f:subview id="commonJsSubview">
			<c:import url="/components/common_js.jsp" />
		</f:subview>
		
		<f:subview id="googleAnalyticsSubview">
			<c:import url="/components/google_analytics.jsp" />
		</f:subview>
			
		<script type="text/javascript">					
			$(function(){
				if(2 == ${param.sortBy}) {
					$("#sortBy option[value='2']").attr("selected", true);
				} else {
					$("#sortBy option[value='1']").attr("selected", true);
				}
				$("#sortBy").change(function(){
					if(1 == $(this).val()){						
						window.parent.location = "${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=ALL&SELECTED_ORG=${param.SELECTED_ORG}&sortBy=1";					
					} else {
						window.parent.location = "${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=ALL&SELECTED_ORG=${param.SELECTED_ORG}&sortBy=2";
					}
				});
			});
		</script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/subscription.js" ></script>
	</body>
</f:view>
</html>		
		