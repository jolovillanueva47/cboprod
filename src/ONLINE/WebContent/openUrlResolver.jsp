<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>

<body> 
<div class="content">
<!-- content area -->

	<ul>
    	<li class="header">OpenURL Resolver</li>
    </ul>

	<%--openURLresolverList - session attribute --%>

	<c:if test="${not empty openUrlResolverBean.urlResolvers}">

		<p>Below is/are the open URL resolver(s) which were detected from your IP:</p>

		<c:forEach items="${openUrlResolverBean.urlResolvers}" var="openUrl">
			<a href="<c:url value='${fn:trim(openUrl.urlResolverPath)}' >
						<c:param name="genre" value="book" />
						<c:param name="isbn" value="${param.isbn}" />
						
					 </c:url>" title='${openUrl}' target='_blank'>Open URL Resolver ${openUrl.urlResolverPath}
			</a><br />
		</c:forEach>

		<p>
			Click the above link to try to find this resource through your library, using your library's URL resolver. If the link doesn't work, please follow the instructions below.
		</p>
	</c:if>
	
	<br/>

	<ul>
    	<li><b>Reference - OpenURL Query String</b></li>
	</ul>
	
	<%--p>&nbsp;<c:out value="genre=book&isbn=${param.isbn}&title=${param.title}" /></p--%>
	<p>&nbsp;<%--
		--%><c:if test="${empty param.genre}"><c:out value="genre=book" /></c:if><%--
		--%><c:if test="${not empty param.genre}"><c:out value="genre=${param.genre}" /></c:if><%--
		--%><c:if test="${not empty param.isbn}"><c:out value="&isbn=${param.isbn}" /></c:if><%--
		--%><c:if test="${not empty sessionScope.bookTitle}"><c:out value="&title=${sessionScope.bookTitle}" /></c:if><%--
		--%><c:if test="${not empty param.spage}"><c:out value="&spage=${param.spage}" /></c:if><%--
		--%><c:if test="${not empty param.volume}"><c:out value="&volume=${param.volume}" /></c:if>
	</p>

	<br/>
	<ul>
		<li><b>Use this query data to:</b></li>
		<li>Query your local OpenURL Resolver</li>
		<li>Query a public OpenURL resolver</li>
	</ul>
	
	<br/>
<p>Instructions: </p>
<ol>
	<li>Highlight the query with your mouse</li>
	<li>Press CTRL-c to copy</li>
	<li>Move to your query interface and select the input box or equivalent</li>
	<li>Press CTRL-v to paste the query</li>
</ol>

</div>

<div class="clear">&nbsp;</div>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>
</html>