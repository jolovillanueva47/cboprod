<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRoot" option="context_root" />

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Cambridge Books Online - Cambridge University Press</title>


  <style type="text/css">
	<!--
	@import url("${pageContext.request.contextPath}/css/styles.css");
	@import url("${pageContext.request.contextPath}/css/collection_manager_styles.css");
	-->
	
  </style>
  
  <script src="${pageContext.request.contextPath}/js/urchin.js" type="text/javascript"></script>
  <script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
  <script src="${pageContext.request.contextPath}/js/search_common.js" type="text/javascript"></script>
</head>
<body>

<input type="hidden" id="helpTypeHidden" value="child" />
<input type="hidden" id="pageNameHidden" value="Collection Manager" />
<input type="hidden" id="pageIdHidden" value="1527" />

<f:subview id="headerSubview">
	<c:import url="/components/header.jsp" />
</f:subview>

<!-- Breadcrumbs Start -->
<f:subview id="crumbtrailContainer">
	<c:import url="/components/crumbtrail.jsp" />	
</f:subview>
<!-- Breadcrumbs End -->



<div id="navigationColumn">

	<f:subview id="ipLogoSubview">
		<c:import url="/components/ip_logo.jsp" />
	</f:subview>

</div><!-- end right nav -->

<div id="collectionManager">
  <form id="collectionManagerForm" action="" method="post">
  <h1>Collection Manager</h1>
	
    <p>Collection Manager allows you to retrieve information from the Cambridge Journals Online database either to display on screen or to download in a convenient format for use with other applications. You can include only those journals that are accessible to the computer you are working on. To do this, check the 'Available to this IP only' box. To select or unselect more than one item from a list, hold down the Ctrl key (PC) or Apple key (Macintosh) as you click each item to select it. To select all items in a list, hold down the Shift key and click on the first and last item. </p>
    
	<table cellspacing="0">
		<tr class="border">
			<td><label for="subject">Subjects</label></td>
			<td>
				<h:selectManyListbox id="subjects" size="5">
					<f:selectItem id="s1" itemLabel="African Studies" itemValue="African Studies" />
					<f:selectItem id="s2" itemLabel="Agriculture" itemValue="Agriculture" />
					<f:selectItem id="s3" itemLabel="American Studies" itemValue="American Studies" />
					<f:selectItem id="s4" itemLabel="Animal Science" itemValue="Animal Science" />
					<f:selectItem id="s5" itemLabel="Biology" itemValue="Biology" />
					<f:selectItem id="s6" itemLabel="Classical Studies" itemValue="Classical Studies" />
				</h:selectManyListbox>
			</td>
		</tr>	
		
		<tr>
			<td><label for="series">Series</label></td>
			<td>
				<h:selectManyListbox id="series" size="5">
					<f:selectItem id="se1" itemLabel="African Studies" itemValue="African Studies" />
					<f:selectItem id="se2" itemLabel="Agriculture" itemValue="Agriculture" />
					<f:selectItem id="se3" itemLabel="American Studies" itemValue="American Studies" />
					<f:selectItem id="se4" itemLabel="Animal Science" itemValue="Animal Science" />
					<f:selectItem id="se5" itemLabel="Biology" itemValue="Biology" />
					<f:selectItem id="se6" itemLabel="Classical Studies" itemValue="Classical Studies" />
				</h:selectManyListbox>
			</td>
		</tr>
	
		<tr>
			<td><label for="titles">Titles</label></td>
			<td>
				<h:selectManyListbox id="titles" size="5">
					<f:selectItem id="t1" itemLabel="African Studies" itemValue="African Studies" />
					<f:selectItem id="t2" itemLabel="Agriculture" itemValue="Agriculture" />
					<f:selectItem id="t3" itemLabel="American Studies" itemValue="American Studies" />
					<f:selectItem id="t4" itemLabel="Animal Science" itemValue="Animal Science" />
					<f:selectItem id="t5" itemLabel="Biology" itemValue="Biology" />
					<f:selectItem id="t6" itemLabel="Classical Studies" itemValue="Classical Studies" />
				</h:selectManyListbox>
			</td>
		</tr>
		
		<tr class="border">
			<td><label for="contents">Contents</label></td>
			<td class="fields">
				<h:selectManyListbox id="contents" size="4">
					<f:selectItem id="c1" itemLabel="All content available to you" itemValue="African Studies" />
					<f:selectItem id="c2" itemLabel="All content not available to you" itemValue="Agriculture" />
					<f:selectItem id="c3" itemLabel="Your organisational subscriptions" itemValue="American Studies" />
					<f:selectItem id="c4" itemLabel="Your free trials" itemValue="Animal Science" />
					<f:selectItem id="c5" itemLabel="Books free to all" itemValue="Biology" />
				</h:selectManyListbox>
			</td>
		</tr>	
	
		<tr>
			<td><label for="fieldList">Fields to Display</label></td>
			<td>
				<h:selectManyListbox id="fieldList" size="5">
					<f:selectItem id="f1" itemLabel="All Fields" itemValue="African Studies" />
					<f:selectItem id="f2" itemLabel="Electronic ISSN" itemValue="Agriculture" />
					<f:selectItem id="f3" itemLabel="First View Articles" itemValue="American Studies" />
					<f:selectItem id="f4" itemLabel="ISSN" itemValue="Animal Science" />
					<f:selectItem id="f5" itemLabel="Journal mnemonic" itemValue="Biology" />
					<f:selectItem id="f6" itemLabel="Publisher" itemValue="Classical Studies" />
				</h:selectManyListbox>
			</td>
		</tr>	
	
		<tr>
			<td><label for="fileList">Output File</label></td>
			<td>
				<h:selectManyListbox id="fileList" size="5">
					<f:selectItem id="o1" itemLabel="Display on Screen" itemValue="African Studies" />
					<f:selectItem id="o2" itemLabel="Tab Separated" itemValue="Agriculture" />
					<f:selectItem id="o3" itemLabel="Comma Separated" itemValue="American Studies" />
					<f:selectItem id="o4" itemLabel="Excel Spreedsheet" itemValue="Animal Science" />
				</h:selectManyListbox>
			</td>
		</tr>	
	
		<tr class="border">
	        <td colspan="2">
	        	<label for="check">Available to this IP only</label>
	          	<input type="checkbox" name="check" value="on" />
	        </td>
      	</tr>
	</table>

	<!-- end advanced_search fields -->
    <table cellspacing="0" class="search_buttons">
	    <tr>
		    <td>
		    <input type="reset" class="button" value="Reset" />
		    <input name="Find" type="submit" class="button" value="Find titles" onclick="return false"/>
		    </td>
	    </tr>
    </table>

    <div class="clear">&nbsp;</div>
    </form>
</div><!-- end content -->

<f:subview id="footerSubview">
	<c:import url="/components/footer.jsp" />
</f:subview>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>

</f:view>

</html>