<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>OpenURL Resolver - Cambridge Books Online - Cambridge University Press</title>  
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>

<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">                                         
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
              	<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               
 		
 		<a id="maincontent" name="maincontent" ></a>
 			              
		<!-- Start Body Content -->
		<div class="content_container link_02">

 			<div class="clear_bottom"></div>
        	
        	<!-- Titlepage Start -->
            <h2>OpenURL Resolver</h2>   
            <!-- Titlepage End -->
            
            <div class="clear_bottom"></div>
             
            	<table class="global_forms input_text buttons" cellspacing="0">
                	<tr>
                    	<td colspan="2">
                    		<c:if test="${not empty openURLresolverList}">
 								<p>Below is/are the open URL resolver(s) which were detected from your IP:</p>
			
								<c:forEach items="${openURLresolverList}" var="openUrl">
										<a href="<c:url value='${fn:trim(openUrl)}' >
											<c:param name="genre" value="book" />
											<c:param name="isbn" value="${param.isbn}" />
											<c:param name="title" value="${param.title}" />
								 		</c:url>" title='${openUrl}' 
								 				target='_blank'>Open URL Resolver ${openUrl}
										</a><br />
								</c:forEach>
			
								<p>Click the above link to try to find this resource through your library, using your library's URL resolver. If the link doesn't work, please follow the instructions below.</p>                   		
                    		</c:if>
                    		
                        	<p>Reference - OpenURL Query String</p>
                        	
                        	<p>&nbsp;<%--
								--%><c:if test="${empty param.genre}"><c:out value="genre=book" /></c:if><%--
								--%><c:if test="${not empty param.genre}"><c:out value="genre=${param.genre}" /></c:if><%--
								--%><c:if test="${not empty param.isbn}"><c:out value="&isbn=${param.isbn}" /></c:if><%--
								--%><c:if test="${not empty param.title}"><c:out value="&title=${param.title}" /></c:if><%--
								--%><c:if test="${not empty param.spage}"><c:out value="&spage=${param.spage}" /></c:if><%--
								--%><c:if test="${not empty param.volume}"><c:out value="&volume=${param.volume}" /></c:if>
							</p>
                        	
                            <p>
                            	<strong>Use this query data to:</strong><br/>
                                    Query your local OpenURL Resolver<br/>
                                    Query a public OpenURL resolver<br/>                                    
                            </p>
                            <p>Instructions:</p>
                            <ol>
                            	<li>Highlight the query with your mouse</li>
                            	<li>Press CTRL-c to copy</li>
                                <li>Move to your query interface and select the input box or equivalent</li>
                                <li>Press CTRL-v to paste the query</li>
                            </ol>
                        </td>
                	</tr>                               
               	</table>            	


            <div class="clear_bottom"></div>
		
            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
      
</body>

</html>		
