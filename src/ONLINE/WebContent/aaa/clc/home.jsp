<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cambridge Library Collection - Cambridge University Press</title>    
    
   	<jsp:include page="../../aaa/clc/components/acc_bar.jsp"></jsp:include>

</head>
<body>

<%
	session.setAttribute("publisherCode", "CLC");
	session.setAttribute("clcPageName", "home");
%>
<div id="page-wrapper">    
   	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
					<jsp:include page="../../aaa/clc/components/acc_links.jsp" /> 
              	 </div>
              
              	<div class="clear"></div>
            
			  	<div class="middleheader_divider">
                     <jsp:include page="../../aaa/clc/components/org_info.jsp"></jsp:include>           
              	</div>     
                 
			  	<div class="clear"></div>
            
                <!-- Start Topmenu -->
                <div class="topmenu">
                   	<jsp:include page="../../aaa/clc/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/clc/components/ip_logo.jsp"></jsp:include>                   
     			</div>
                <!-- End Topmenu -->
            </div>
        </div>     
        <!-- End Header -->
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/clc/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
            
            <!-- bread crumb hidden -->
	    	<div class="breadcrumbs_container" style="display: none;">
	    		<span class="titlepage_hidden">You are here:</span> 
			</div>
	    	<!-- bread crumb hidden -->
        </div>
        <!-- End Middle Content -->             
		
		<a id="maincontent" name="maincontent" ></a>	
        
        <!-- Start Body Content -->
		<div class="content_container">
        
        	<div class="clear"></div>
            
            <!-- Start page branding -->
            <div class="page_branding"><h2>Cambridge Library Collection - Books of enduring scholarly value</h2></div>
            <!-- End page branding -->
            
            	<!-- Start Banner -->
                <div class="border-div link_01">
                <input type="hidden" value="${newsBean.initialize}" />
                	<div class="border_divider04">
                    
                    	<!-- Subtitle page Start -->
                        <h3>Download List of Available Books</h3>
                        <!-- Subtitle page End -->
                        
                        <div class="clear"></div>
                        
                        <c:catch var="eXls">
                        	<c:import url="/downloads/titlelist_clc.xls" varReader="ignore" />
                        </c:catch>
                        <c:catch var="eCsv">
                        	<c:import url="http://ebooks.cambridge.org/downloads/titlelist.csv" varReader="ignore"/>
                        </c:catch>
                        
                        <!-- Download book format start -->
                        <ul>
                            <li><span class="icons_img download_excel${not empty eXls ? '_disabled' : '' }">&nbsp;</span><a href="${empty eXls ? 'http://ebooks.cambridge.org/downloads/titlelist.xls' : '#' }">Microsoft Excel Format</a></li>
                            <li><span class="icons_img download_csv${not empty eCsv ? '_disabled' : '' }">&nbsp;</span><a href="${empty eCsv ? 'http://ebooks.cambridge.org/downloads/titlelist.csv' : '#' }">CSV Format</a></li>
                        </ul>
                        <!-- Download book format end -->

                    </div>
                    
                    <div class="border_divider05">
                    	<!-- Subtitle page Start -->
                        <h3>Books</h3>
                        <!-- Subtitle page End -->   										
                       	<ul>
	                    	<c:if test="${not empty topBooksBean.topSeriesListCLC}">
								<c:forEach var="series" items="${topBooksBean.topSeriesListCLC}">					
									<c:url value="/series_landing.jsf" var="seriesSortByVolume">
										<c:param name="seriesCode" value="${series.seriesCode}" />
										<c:param name="seriesTitle" value="${series.seriesLegend}" />
										<c:param name="sort" value="series_number" />
									</c:url>				
									<li><span class="icons_img arrow_03">&nbsp;</span><a href="<c:out value="${seriesSortByVolume}"/>">${series.seriesLegend}</a></li>								
								</c:forEach>
							</c:if>					
						</ul>	
					
                    </div>
                
                	<div class="clear"></div>
                    
                </div>
            	<!-- End Banner -->
            
            <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/clc/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/clc/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 

      
</body>

</html>