<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@page import="java.util.Set"%>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%
User user = (User) session.getAttribute("userInfo");
System.out.println("user: "+user);
System.out.println("orgBodyId: "+session.getAttribute("orgBodyId"));

String orgnames = (String) session.getAttribute("orgNames");
if(orgnames != null && orgnames.trim().length() > 0){
	String[] orgArr = orgnames.split(",");
	
	if(orgArr.length > 3) {
		String orgNamesLimited = orgArr[0] + ", " + orgArr[1] + ", " + orgArr[2];
		request.setAttribute("orgNamesLimited", orgNamesLimited);
		request.setAttribute("orgNamesLenght", orgArr.length);
		request.setAttribute("orgName", orgArr[0]);
	} else {
		request.setAttribute("orgNamesLimited", orgnames);
		request.setAttribute("orgNamesLenght", orgArr.length);
		request.setAttribute("orgName", orgArr[0]);
	}
	
}

Object orgIdObj = session.getAttribute("orgIdSet");
Set<String> orgIdSet = null;		
if(orgIdObj instanceof Set) {
	orgIdSet = (Set<String>)orgIdObj;
}
if(orgIdSet != null) {
	for(String id : orgIdSet) {
		if(new File("/app/cjo_logo/org" + id).exists()) {
			request.setAttribute("orgLogoId", "org" + id);
			break;
		}
	}
} else {
	request.setAttribute("orgLogoId", "");
}

%>

<%
	String orgBodyId = (String) session.getAttribute("orgBodyId");					
	String[] ipMembership = (String[])session.getAttribute("IPmembership");
%>

<c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
<c:set var="athensId" value='<%= session.getAttribute("athensId") %>' />

                <!-- User login start -->
				<div class="frontpage_login_leftdivider">
					
				
                  	  <span class="icons_img icon_login">&nbsp;</span>
                  	  <h2> 
                  	  	<c:choose>
                  	  		<c:when test="${isLoggedIn}">
                  	  			<a href="javascript:void(0);" onclick="clclogout(); return false;" >Logout</a>
                  	  		</c:when>
                  	  		<c:otherwise>Login:</c:otherwise>
                  	  	</c:choose>
                  	  </h2>
                  	  <c:choose>
						<c:when test="${not empty orgBodyId or isLoggedIn}">
					  		<c:choose>
								<c:when test="${isLoggedIn}">
									&nbsp;Welcome <strong>${userInfo.username}</strong> 
								</c:when>
								<c:otherwise>
									&nbsp;Welcome <strong>${orgName}</strong> 
								</c:otherwise>
							</c:choose>
                  	  	</c:when>
                  	  	<c:when test="${isLoggedIn}">
                  	  		&nbsp;Welcome <strong>${userInfo.username}</strong>
                  	  	</c:when>
                  	  	<c:when test="${not empty IPmembership}">
                  	  		&nbsp;<strong>Welcome ${orgName}</strong>
                  	  	</c:when>
                  	  	<c:otherwise>
                  	  		&nbsp;<strong>Welcome Guest</strong>
                  	  	</c:otherwise>
                  	  </c:choose>
                  	  
                  	  
                      <ol>
                      	<strong>Note:</strong>
                        <li>Username and password are case-sensitive.</li>
                      	<li>If you already have a username for Cambridge Books Online, you can log in to Cambridge Library Collection using CBO username.</li>
                      </ol>
                      
                   <c:if test="${not isLoggedIn}">
                   <input type="hidden" value="true" name="clcLogin_aaa" >
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                          <td colspan="2">
                              <label>Username:</label>
                              <input name="username" id="username" type="text" maxlength="24" style="width: 275px;" />                    
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                              <label>Password:</label>
                              <input name="password" id="password" type="password" maxlength="24" style="width: 275px;" />
                          </td>
                        </tr>
                        <tr>
                          <td>
                          	<div id="result"></div>                      
                          </td>
                        </tr>                                       
                        <tr>                  
                          <td width="70%"><a href="javascript:toPage('forgottenPassword');" title="Forgotten your password?">&raquo;&nbsp;Forgotten your password?</a></td>
                          <td width="30%" class="button"><input name="submitUsrnamePassword" id="submitUsrnamePassword" type="button" value="Login" /></td>
                        </tr>
                    </table> 
                   </c:if>
                                     
                </div>
                <div class="frontpage_login_rightdivider">
                	<ul>

							<c:choose>
								<c:when test="${empty athensId}">
									<li><a href="<%= System.getProperty("openathensLink") %>" >&raquo;&nbsp;Athens Login</a></li> 
								</c:when>
								<c:otherwise>
									<li><a href="javascript:void(0);" onclick="athensLogout(); return false;" title="Athens Login">&raquo;&nbsp;Athens Logout</a></li> 
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${empty shibbId}">
									<li><a href="<%= System.getProperty("shibbolethLink") %>">&raquo;&nbsp;Shibboleth Login</a></li> 
								</c:when>
								<c:otherwise>
									<li><a href="javascript:void(0);" onclick="shibbolethLogout(); return false;" title="Shibboleth Logout">&raquo;&nbsp;Shibboleth Logout</a></li> 
								</c:otherwise>
							</c:choose>	
							
							<c:choose>
								<c:when test="${isLoggedIn}">
									<li><a href="javascript:void(0);" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=updateRegistration','accountLink')" id="accountLink">&raquo;&nbsp;My Account</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="javascript:void(0);" onclick="MM_windowOpener('<%= System.getProperty("cjo.url") %>registration?displayname=${referalName}'); return false;" >&raquo;&nbsp;Register</a></li>
								</c:otherwise>
							</c:choose>
				
                    </ul>
                    
                    
                    
                    
                </div>
                
                  <!-- User login end -->

