<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	
<% 
	String userAgent = request.getHeader("User-Agent"); 
	String s = "";
	float userAgentVersion = -1;
	if(userAgent.indexOf("MSIE") > -1) {
		s =  userAgent.substring(userAgent.indexOf("MSIE") + 4);
		s = s.replace(" ", "");
		userAgentVersion = Float.parseFloat(s.substring(0, s.indexOf(";")));
	}
%>

<!-- Search Start -->
<form id="searchForm" action="<%=request.getContextPath()%>/search_aaa?searchType=quick" method="post">
	<!-- show only in ebook and chapter landing page -->
	<c:choose>
    	<c:when test="${param.bid ne null}">
       		<input type="hidden" name="pageType" value="book" />
       		<input type="hidden" name="bid" value="${param.bid}" />  
       	</c:when>
       	<c:otherwise>             				
       		<input type="hidden" name="pageType" value="other" />
       	</c:otherwise>
   </c:choose>		
	<div class="search_container link_01">
 		<input type="text" class="text_search" title="Enter keyword to search" id="searchText" name="searchText" />
		<input type="submit" value="Search" name="search" class="input_buttons" />
		<span class="icons_img arrow_01">&nbsp;</span><a href="${pageContext.request.contextPath}/aaa/advance_search.jsf">Advanced Search</a>
	</div>     			
	<c:if test='${fn:contains(pageContext.request.requestURL, "ebook.jsp") or fn:contains(pageContext.request.requestURL, "chapter.jsp")}'>
		<div class="search_container arrow_01">
			<c:choose>
				<c:when test='<%= userAgent.indexOf("Opera") > -1 %>'>	             					             	         	               					                  			         						      
					<input type="checkbox" name="searchWithinContent" id="searchWithinContent" style="margin: 0 4px;"></input>	
		        </c:when>
		        <c:when test='<%= userAgent.indexOf("MSIE") > -1 && userAgentVersion > -1.0 && userAgentVersion <= 7.0 %>'>
					<input type="checkbox" name="searchWithinContent" id="searchWithinContent" style="margin: 0 4px;"></input>
		        </c:when>
		        <c:otherwise>   	             						             	         	                       					                  			         						    
					<input type="checkbox" name="searchWithinContent" id="searchWithinContent" style="margin: 0 4px;"></input>
		        </c:otherwise>
		    </c:choose>	             								
			<label for="searchWithinContent">Search within This Book</label>
		</div>						              	        	         	
	</c:if>		
</form>
<!-- Search End --> 
