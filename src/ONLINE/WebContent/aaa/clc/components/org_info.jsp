<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@page import="java.net.*" %>
<%@page import="java.util.Set" %>
<%@page import="java.io.File" %>

<h1><a href="<%= System.getProperty("ebooks.context.path")%>aaa/clc/home.jsf" class="logo_ebooks" title="Cambridge Library Collection">Cambridge Library Collection</a></h1>

	<!-- Start Organization Info -->
	<c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
	<input id="ipLogoContextPath" type="hidden" value="<%= System.getProperty("ebooks.context.path")%>" />

	<c:choose>
		<c:when test="${not empty orgBodyId or pageScope.isLoggedIn}">
			
			<div class="org_wrapper">
				<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
			    <div class="org_container link_03">
				    	<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
						<c:choose>
							<c:when test="${eBookLogoBean.hasLogo}">
		    					<img alt="" id="orgLogoImg" src="<%= System.getProperty("org.logo.url") %>${eBookLogoBean.orgLogoId}" width="85" height="35"/>	
		    				</c:when>
		    				<c:otherwise>
		    					<img id="orgLogoImg" src="<%= System.getProperty("ebooks.context.path") %>aaa/images/logos/__utm2.GIF" width="0px" height="0px" title="" alt="" />
		    				</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 3}">
								<strong>Member Organisations: </strong>${eBookLogoBean.showLogoNamesUnderMax},&nbsp;
								<a href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/view_access.jsf">more details</a>
							</c:when>
							<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 1}">
								<strong>Member Organisations: </strong>${eBookLogoBean.showLogoNamesUnderMax}
							</c:when>
							<c:otherwise>
								<strong>Member Organisation: </strong>${eBookLogoBean.showLogoNamesUnderMax}
							</c:otherwise>
						</c:choose>
		    	</div>
		    	</c:if>
	    	</div>
	    	<div class="clear"></div>
			<div class="orgname_container">
			<c:choose>
				<c:when test="${pageScope.isLoggedIn}">
					<strong>Welcome ${userInfo.firstName}&nbsp;${userInfo.lastName}</strong> 
				</c:when>
				<c:otherwise>
					<strong>Welcome ${orgName}</strong>
				</c:otherwise>
			</c:choose>
			</div>
		</c:when>
		
		<c:when test="${pageScope.isLoggedIn}">
			<div class="clear"></div>
			<div class="orgname_container">
				Welcome ${userInfo.firstName}&nbsp;${userInfo.lastName}
			</div> 
		</c:when>
		
		<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
			<div class="org_wrapper">
				<%-- <c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}"> --%>
			    <div class="org_container link_03">
				    	<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
						<c:choose>
							<c:when test="${eBookLogoBean.hasLogo}" >
		    					<img id="orgLogoImg" src="<%= System.getProperty("org.logo.url") %>${eBookLogoBean.orgLogoId}" width="85" height="35" alt=""/>	
		    				</c:when>
		    				<c:otherwise>
		    					<img id="orgLogoImg" src="<%= System.getProperty("ebooks.context.path") %>aaa/images/logos/__utm2.GIF" width="0px" height="0px" title="" alt="" />
		    				</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 3}">
								<strong>Member Organisations: </strong>${eBookLogoBean.showLogoNamesUnderMax},&nbsp;
								<a href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/view_access.jsf">more details</a>
							</c:when>
							<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 1}">
								<strong>Member Organisations: </strong>${eBookLogoBean.showLogoNamesUnderMax}
							</c:when>
							<c:otherwise>
								<strong>Member Organisation: </strong>${eBookLogoBean.showLogoNamesUnderMax}
							</c:otherwise>
						</c:choose>
		    	</div>
		    	<%--</c:if> --%>
	    	</div>
    		<div class="clear"></div>       
			<div class="orgname_container">
				<b>Welcome ${eBookLogoBean.orgName}</b>
				<%--  <c:when test="${not empty eBookLogoBean.orgName}">
					<b>Welcome ${eBookLogoBean.orgName}</b> |
				</c:when> --%> 
			</div> 
		</c:when>
		
		<c:otherwise>
			<div class="clear"></div>       
			<div class="orgname_container">
				Welcome Guest
			</div>
		</c:otherwise>
	</c:choose>