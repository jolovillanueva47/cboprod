<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="ContextPath" value='<%= System.getProperty("ebooks.context.path") %>'></c:set>

<!-- Start Logo Block -->
<div class="cup_logo"><a href="#" title="Cambridge University Press"><img src="${ContextPath}/aaa/clc/images/cup_logo.jpg" alt="Cambridge University Press" /></a></div>
<div class="access_menu">

	<c:set var="name" value="${accBean.pageName}" />
	<c:set var="isAccessible" value="${accBean.accessible}" scope="session"/>
	
	<c:set var="acc_01" value="${sessionStyle == 'default_style' || sessionStyle == null ? 'Default Font Selected' : 'Change to Default Font' }" scope="page"/>
	<c:set var="acc_02" value="${sessionStyle == 'medium_style' ? 'Large Font Selected' : 'Change to Large Font' }" scope="page"/>
	<c:set var="acc_03" value="${sessionStyle == 'large_style' ? 'Largest Font Selected' : 'Change to Largest Font' }" scope="page"/>
	<c:set var="acc_04" value="${sessionStyle == 'contrast_style' ? 'High Contrast Active' : 'Change Contrast' }" scope="page"/>
	
	<c:set var="view_01" value="${sessionStyle == 'default_style' || sessionStyle == null ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_02" value="${sessionStyle == 'medium_style' ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_03" value="${sessionStyle == 'large_style' ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_04" value="${sessionStyle == 'contrast_style' ? 'current_view' : '' }" scope="page"/>

	<c:set var="hrefLink" value="nothing" scope="page"></c:set>
	
	<c:set var="hrefSkipId" value="${param.skipId}" scope="page"></c:set>
	<c:set var="ContextPath" value='<%= System.getProperty("ebooks.context.path") %>'></c:set>
	
	<c:choose>
		<c:otherwise>
			<c:set var="hrefLink" value="${accBean.pagePath}${accBean.queryString}"></c:set>
		</c:otherwise>
	</c:choose>

	<ul>
    	<li><a href="#maincontent" title="Skip to Content"><img src="${ContextPath}aaa/images/bot_skip.gif" alt="Skip to Content" width="192" height="30" /></a></li>
    	<li class="${view_01}"><a href="<c:out value="${hrefLink}stylez=default_style#${hrefSkipId}" escapeXml="true"  />" title="${acc_01}" class="small_font"><img src="${ContextPath}aaa/images/bot_font_small.gif" alt="${acc_01}" width="34" height="30" /></a></li>
        <li class="${view_02}"><a href="<c:out value="${hrefLink}stylez=medium_style#${hrefSkipId}"  escapeXml="true"  />" title="${acc_02}" class="medium_font"><img src="${ContextPath}aaa/images/bot_font_large.gif" alt="${acc_02}" width="34" height="30" /></a></li>
        <li class="${view_03}"><a href="<c:out value="${hrefLink}stylez=large_style#${hrefSkipId}"  escapeXml="true" />" title="${acc_03}" class="large_font"><img src="${ContextPath}aaa/images/bot_font_largest.gif" alt="${acc_03}" width="34" height="30" /></a></li>                            
        <li class="${view_04}"><a href="<c:out value="${hrefLink}stylez=contrast_style#${hrefSkipId}"  escapeXml="true"  />" title="${acc_04}" class="change_contrast"><img src="${ContextPath}aaa/images/bot_contrast.gif" alt="${acc_04}" width="34" height="30" /></a></li>
	</ul>
	<!-- end ul -->
		
</div>



	

