<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Contacts - Cambridge Books Online - Cambridge University Press</title>   
    
	<jsp:include page="../../../aaa/clc/components/acc_bar.jsp"></jsp:include>
    
</head>
<body>

<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../../aaa/clc/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../../aaa/clc/components/org_info.jsp"></jsp:include>
				</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../../aaa/clc/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../../aaa/clc/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
			<%--
	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
	   			<gui:crumbtrail propertyFile="org.cambridge.ebooks.online.crumbtrail.crumb"
	  				lastElementStyle="last"
	  				userCrumb="org.cambridge.ebooks.online.crumbtrail.UserCrumb" >
	  			</gui:crumbtrail>
			</div>
			<!-- Breadcrumbs End -->
			 --%>
 		</div>
        <!-- End Middle Content -->               
  		
  		<a id="maincontent" name="maincontent" ></a>
  		                
		<!-- Start Body Content -->
		<div class="content_container link_02">
		
			<div class="clear"></div> 
			<h2>Contact Us</h2>
			<div class="clear"></div> 
			
			<p>Please use the contact details provided below if you would have a query/comment to make about <em>Cambridge Books Online</em> or if you have experienced problems using the site. Please ensure you have checked through our <a class="link03" href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/faq.jsf">Frequently Asked Questions</a> and <a class="link03" href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/help.jsf?pageId=1537&pageName=<%= session.getAttribute("pageName") %>">Help</a> sections before submitting your query.</p>
			<br/><p>If you would like to purchase access to <em>Cambridge Books Online</em>, please go to <a class="link03" href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/news.jsf?messageId=1556">How to Purchase</a>. To register for a free trial (institutions only), please fill out our <a class="link03" href="<%=System.getProperty("cjo.url") %>registration?SIGNUP=Y" title="Click here to signup!">trial registration form</a>. For information about other online products, please go to <a class="link03" href="http://www.cambridge.org/online/" target="cam_ol">Cambridge Online</a>.</p>
			<br/><p>For all other queries, please refer to your nearest regional office:</p>
			<br/>
			<div>
			<h3>The Americas</h3>	
			For price quotes and trial requests contact: <a class="link03" href="mailto:online@cambridge.org">online@cambridge.org</a>
			<br/>For consortia and multi-site licence pricing contact: <a class="link03" href="mailto:aroseman@cambridge.org">aroseman@cambridge.org</a>
			</div>
			<br/>
			<p>
				For Customer Services and other queries:
				<br/>Cambridge University Press
				<br/>100 Brook Hill Drive
				<br/>West Nyack, NY 10994-2133
			</p>
			<br/>
			<p>
				Toll-free: 800-872-7423
				<br/>Toll-free (Mexico): 95-800-010-0200
				<br/>Phone: (845) 353-7500
				<br/>Fax: (845) 353-4141
				<br/>www.cambridge.org/us
			</p>
			<br/>
			<p>
				For technical support: 
				E-mail: <a class="link03" href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a>
			</p> 
			<br/>
			<div>
			<h3>United Kingdom and Rest of World</h3>
			For price quotes and trial requests contact: <a class="link03" href="mailto:academicsales@cambridge.org">academicsales@cambridge.org</a>
			<br/>For consortia and multi-site licence pricing contact: <a class="link03" href="mailto:hperrett@cambridge.org">hperrett@cambridge.org</a>		
			</div>
			<br/>
			<p>
				For Customer Services and other queries:
				<br/><strong>Cambridge University Press</strong>
				<br/>The Edinburgh Building
				<br/>Shaftesbury Road
				<br/>Cambridge
				<br/>CB2 8RU
			</p>
			<br/>
			<p>
				By phone: +44 (0) 1223 326098
				<br/>By fax: +44 (0) 1223 325152
				<br/>Email: <a class="link03" href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a>
			</p>   
			<br/>
			<%-- 
			<p>
				<a href="http://spo-test.cup.cam.ac.uk/direct_stahl.jsp?displayname=BigayNiLord" >Stahl Full Access (Staging)</a>
			</p>
			--%>
			
			<!-- Footermenu Start -->
	        <jsp:include page="../../../aaa/clc/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
	        <!-- Footermenu End -->
					
        </div>        
        <!-- End Body Content -->
        
        <div class="clear"></div>
    
	    <!-- FooterContainer Start -->
	  	  <jsp:include page="../../../aaa/clc/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
       
</div>         
</body>
</html>