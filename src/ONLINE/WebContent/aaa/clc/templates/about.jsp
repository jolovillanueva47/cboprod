<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@page import="org.cambridge.ebooks.online.subjecttree.SubjectWorker_aaa"%>


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cambridge Library Collection - Cambridge University Press</title>    
    
   	<jsp:include page="../../../aaa/clc/components/acc_bar.jsp"></jsp:include>

</head>

<body>

		
<div id="page-wrapper">    	    
     <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../../../aaa/clc/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../../../aaa/clc/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../../../aaa/clc/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../../aaa/clc/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      
        
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../../aaa/clc/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
            

        </div>
        <!-- End Middle Content -->             
		
		<a id="maincontent" name="maincontent" ></a>
		               
		<!-- Start Body Content -->
		<div class="content_container">   
      
        	<!-- Titlepage Start -->
            <h2>About <em>Cambridge Library Collection</em></h2>   
            <!-- Titlepage End -->
           
            <p>Originating in a unique collaboration between the world's oldest publisher and the renowned Cambridge University Library, the Cambridge Library Collection makes important historical works accessible in new ways. The combination of state-of-the-art scanning technology and the Press's commitment to quality gives today's readers access to the content of books that until recently would have been available only in specialist libraries.</p>

			<p>Already a pioneer in the re-publishing of titles from its own backlist, Cambridge University Press has extended its reach to include other books which are still of interest to researchers, students and the general reader. The Press's collaboration with Cambridge University Library and other partner libraries allows access to a vast range of out-of-copyright works, from which titles are selected with advice from leading specialists.</p>

			<p>With subjects ranging from anthropology to zoology, the Cambridge Library Collection allows readers to own books they would otherwise find it hard to obtain, including the monumental Library Edition of the Works of John Ruskin edited by Cook and Wedderburn, the complete Naval Chronicle documenting warfare at sea in the Napoleonic period, accounts of the exploration of the Americas, Australia and China, and scientific writings by Darwin and his circle.</p>

			<p>Each page is scanned and the resulting files undergo a rigorous process of cleaning, in which any blemishes are removed to obtain a crisp and legible text. Each book has a new cover design and a specially written blurb which highlights the relevance of the book to today's readers. The latest print-on-demand technology then ensures that the content of these rare and sometimes fragile books will be made available worldwide. The Collection will shortly become available, in searchable form, as part of Cambridge Books Online.</p>	
            
            <!-- Footermenu Start -->
        	<jsp:include page="../../../aaa/clc/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../../../aaa/clc/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
      
</body>

</html>