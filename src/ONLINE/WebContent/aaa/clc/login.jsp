<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" 
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 


<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    
	<title>Login - Cambridge Books Online - Cambridge University Press</title>    
	
	<jsp:include page="../../aaa/clc/components/acc_bar.jsp"></jsp:include>
</head>

<body>
	<div id="page-wrapper">
 		<!-- Start Header -->
        <div class="header">
            <div class="header_container">
				<div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="../../aaa/clc/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
				</div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/clc/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/clc/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/clc/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/clc/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
<%--
	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
	   			<gui:crumbtrail propertyFile="org.cambridge.ebooks.online.crumbtrail.crumb"
	  				lastElementStyle="last"
	  				userCrumb="org.cambridge.ebooks.online.crumbtrail.UserCrumb"
	  				hide="false" >
	  			</gui:crumbtrail>
			</div>
			<!-- Breadcrumbs End -->
			
			--%>
 		</div>
        <!-- End Middle Content -->  
        
        <a id="maincontent" name="maincontent" ></a>
          
		<div class="content_container link_02">
         	
         	<div class="clear_bottom"></div>
      		<!-- Titlepage Start -->
            <h2>Login</h2>   
            <!-- Titlepage End -->		
			
            <div class="clear_bottom"></div>
            <c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
			<form id="loginForm" method="post" action="../../aaa/login_aaa">
				<!--  Resending the param when from search_results.jsf for retaining the search results-->
				<c:if test="${not empty param.currentPage}">
					<input type="hidden" name="currentPage" value="${param.currentPage}"/>
					<input type="hidden" name="resultsPerPage" value="${param.resultsPerPage}"/>
					<input type="hidden" name="sortType" value="${param.sortType}"/>
				</c:if>

			<c:if test="${errorMessage != null && not pageScope.isLoggedIn}">
				<div class="validate">
					<h3>Login Failed</h3>
					<p>Unable to Login, the following error was encountered:</p>
					${ errorMessage }
				</div>
				<div class="clear_bottom"></div>
			</c:if>
	
		<table class="global_forms input_text buttons" cellspacing="0">
<!--          <c:if test="${errorMessage != null && not pageScope.isLoggedIn}">-->
<!--          <tr>-->
<!--            <td colspan="2" class="error_msg">${ errorMessage }</td>-->
<!--          </tr>-->
<!--          <tr><td colspan="2"><hr /></td></tr>-->
<!--          </c:if>-->
          
          <tr>
            <td colspan="2">
           <!--  <span class="titlepage_hidden"><h3>Login Form</h3></span> -->
            <div class="hide_titlepage"><h3>Login Form</h3></div><!-- Hidden Titlepage --> 
            	<div class="${pageScope.isLoggedIn == false ? 'show': 'hide' }">
	            	<a href="<%= System.getProperty("cjo.url") %>registration_aaa?displayname=${referalName}&stylez=${sessionStyle == null ? 'default_style' : sessionStyle}">Register now</a>
            	</div>
            </td>
          </tr>
          <tr><td colspan="2"><hr /></td></tr>
          <tr>
            <td colspan="2">${pageScope.isLoggedIn ? userInfo.username : 'Guest'}</td>
          </tr>
          <tr><td colspan="2"><hr /></td></tr>
          <tr>
            <td align="right"><label for="userName">Username or Email Address</label></td>
            <%-- <td><input name="title" maxlength="48" value="" type="text"></td>--%>
        	<c:choose>
				<c:when test="${not pageScope.isLoggedIn}">
					<td><input name="username" class="input_text" id="username" size="24" maxlength="24" type="text" /></td>
				</c:when>
				<c:otherwise>
					<td><input id="userName" class="input_text" disabled="disabled" /></td>
				</c:otherwise>
			</c:choose>
          </tr>
          <tr>
            <td align="right"><label for="passWord">Password</label></td>    
            <%-- <td><input name="firstName" maxlength="48" value="" type="password"></td>--%>
				<c:choose>
					<c:when test="${not pageScope.isLoggedIn}">
						<td><input name="password" class="input_text" id="password" size="24" maxlength="24" type="password" /></td>
					</c:when>
					<c:otherwise>
						<td><input id="passWord" class="input_text" disabled="disabled" /></td>
					</c:otherwise>
				</c:choose>
          </tr>
          <tr><td colspan="2"><hr /></td></tr>
          <tr>
              <td>&nbsp;</td>
              <td>
				<c:choose>
					<c:when test="${not pageScope.isLoggedIn}">
						<input type="submit" value="Login" class="input_buttons"/>
					</c:when>
					<c:otherwise>
						<input type="submit" value="Login" disabled="disabled" class="input_buttons"/>
					</c:otherwise>
				</c:choose>
              </td>
            </tr> 
          <tr>
            <td colspan="2">
				<c:choose>
					<c:when test="${not pageScope.isLoggedIn}">
						<c:set var="cjo_url" value='<%=System.getProperty("cjo.url")%>'/>
						<a href="${cjo_url}forgottenPassword_aaa?displayName=${referalName}  " >Forgotten your password?</a>
					</c:when>
					<c:otherwise>
						<span style="font-weight: bold; color: gray;">Forgot your password?</span>
					</c:otherwise>
				</c:choose>
			</td>
          </tr>

          <tr><td colspan="2"><hr /></td></tr>
                    
          <tr>
            <td colspan="2">
           		<c:choose>
					<c:when test="${empty athensId}">
						<a target="_blank" href="<%= System.getProperty("openathensLink") %>" title="Athens Login">Athens Login</a>
					</c:when>
					<c:otherwise>
						<a target="_blank" href="${pageContext.request.contextPath}/aaa/home.jsf?alo=y" title="Athens Logout">Athens Logout</a>	
					</c:otherwise>
				</c:choose>
            	&nbsp;|&nbsp;
           		<c:choose>
					<c:when test="${empty shibbId}">
						<a target="_blank" href="<%= System.getProperty("shibbolethLink") %>" title="Shibboleth Login">Shibboleth Login</a>
					</c:when>
					<c:otherwise>
						<a target="_blank" href="${pageContext.request.contextPath}/aaa/home.jsf?slo=y" title="Shibboleth Logout">Shibboleth Logout</a>
					</c:otherwise>
				</c:choose>
            	
            </td>
          </tr>                                 
		</table>
			
			<ul class="logIn">

		
				<li style="${pageScope.isLoggedIn ? 'font-weight: bold; color: gray;' : ''}">
					Note: <br />* Usernames and passwords are case-sensitive. <br />* If you already have a username for Cambridge Journals Online, you can log in to Cambridge Books Online using your existing CJO username.
				</li>
		    </ul>
	    
		
	</form>
	    
	    <!-- Footermenu Start -->
        <jsp:include page="../../aaa/clc/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        <!-- Footermenu End -->
        
	</div>
        <!-- FooterContainer Start -->
	  	<jsp:include page="../../aaa/clc/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
</div>
</body>

</html>