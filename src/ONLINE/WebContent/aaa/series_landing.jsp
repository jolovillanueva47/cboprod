<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%@page import="org.cambridge.ebooks.online.landingpage.BookMetaData"%><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	${seriesBean.initSeriesPage}
	${seriesBean.initSeriesPage_aaa}
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Book Series Category - Cambridge Books Online - Cambridge University Press</title>  
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>
    
</head>

<body>
		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="78" scope="session" />
		<c:set var="pageName" value="Series Landing" scope="session" />
		
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               

		<a id="maincontent" name="maincontent" ></a>
		                   
		<!-- Start Body Content -->
		<div class="content_container input_text buttons">
			
			<div class="clear_bottom"/>
			<h2>Series</h2>
            <div class="clear_bottom"/>
            
            <!-- Access Information Start -->
            <p><strong>Access Information:</strong></p>
			<div class="icon_access">
            	<ul>
            		<li><img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_f_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="" width="30" height="30" /></li>
            		<li class="icon_free" title="Purchased Access">Purchased Access</li>
            	</ul>
            </div>
            <!-- Access Information End -->
                     
            <div class="clear_bottom"></div> 
			
			<!-- Search Result Query Start -->
			<h3><c:out value="${seriesBean.series}" /></h3>
			<c:if test="${not empty seriesBean.seriesDesc}">
				<p><c:out value="${seriesBean.seriesDesc}" escapeXml="false" /></p>
			</c:if>

            <div class="link_02 fr">
				<c:url var="urlVolume" value="series_landing.jsf">
					<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
					<c:param name="seriesTitle" value="${seriesBean.series}" />
					<c:param name="searchType" value="page" />
					<c:param name="sort" value="series_number" />
				</c:url>
				<c:url var="urlTitle" value="series_landing.jsf">
					<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
					<c:param name="seriesTitle" value="${seriesBean.series}" />
					<c:param name="searchType" value="page" />
					<c:param name="sort" value="title_alphasort" />
				</c:url>
				<c:url var="urlAuthor" value="series_landing.jsf">
					<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
					<c:param name="seriesTitle" value="${seriesBean.series}" />
					<c:param name="searchType" value="page" />
					<c:param name="sort" value="author_name_alphasort" />
				</c:url>
				<c:url var="urlPrintDate" value="series_landing.jsf">
					<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
					<c:param name="seriesTitle" value="${seriesBean.series}" />
					<c:param name="searchType" value="page" />
					<c:param name="sort" value="print_date" />
				</c:url>
				<c:url var="urlOnlineDate" value="series_landing.jsf">
					<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
					<c:param name="seriesTitle" value="${seriesBean.series}" />
					<c:param name="searchType" value="page" />
					<c:param name="sort" value="online_date" />
				</c:url>
				<strong>Sort by:</strong>											
				<c:if test="${'Yes' eq seriesBean.showVolumeSort}">						
					<a href="<c:out value="${urlVolume}" />" id="series_number" class="link03">Volume</a> |
				</c:if>
				<a href="<c:out value="${urlTitle}" />" id="title_alphasort" class="link03">Title</a> |
				<a href="<c:out value="${urlAuthor}" />" id="author_name_alphasort" class="link03">Author</a> |
				<a href="<c:out value="${urlPrintDate}" />" id="print_date" class="link03">Print Publication Year</a> |
				<a href="<c:out value="${urlOnlineDate}" />" id="online_date" class="link03">Online Publication Date</a>					
					
			</div> 
			<!-- Search Result Query End -->
			
			<div class="clear_bottom"></div>
			
			<c:url var="urlSeriesLanding" value="series_landing.jsf">
				<c:param name="seriesCode" value="${seriesBean.seriesCode}" />
				<c:param name="seriesTitle" value="${seriesBean.series}" />
				<c:param name="searchType" value="page" />
			</c:url>
                
            <!-- Border Start -->
            <div class="border-div">			
				<div class="bar_container link_02">
			    	<!-- Pager Menu Start -->
					<form action="<c:out value="${urlSeriesLanding}"/>" method="post">
						<jsp:include page="../aaa/components/pager.jsp" flush="true"></jsp:include>
					</form>
					<!-- Pager Menu End -->
		        </div>
	        	
				<c:choose>
					<c:when test="${seriesBean.resultsFound > 0}">
						<c:forEach var="book" items="${seriesBean.bookMetaDataList}" varStatus="status">										
							<c:choose>
								<c:when test="${status.count eq 1}">
									<c:set var="contentWrapperId" value="content_wrapper04" />
								</c:when>
								<c:otherwise>
									<c:set var="contentWrapperId" value="content_wrapper05" />
								</c:otherwise>
							</c:choose>
							
							<div class="${contentWrapperId}">

								<!-- Icon Access -->
								<c:set var="accessClass" value="" />
								<c:set var="accessTitle" value="You don't have access." />
								<gui:isPurchasedAccess accessType="${book.accessType}">
									<c:set var="accessClass" value="icons_img purchase" />
									<c:set var="accessTitle" value="You have access." />
								</gui:isPurchasedAccess>
								<div class="${accessClass}" title="${accessTitle}"></div>			

								<!-- Sub Books Start -->
								<div class="div_search_info link_02">
								
									<div class="fl">
										<h4><a href="ebook.jsf?bid=${book.id}" class="link05">
											<c:out value="${book.title}" escapeXml="false" />
										</a></h4><br/>								
										<c:if test="${not empty book.volumeTitle}">
											<strong><c:out value="${book.volumeTitle}" escapeXml="false" />,</strong>
										</c:if>
										<c:if test="${not empty book.edition}">
											<strong><c:out value="${book.edition}" escapeXml="false" />,</strong>
										</c:if>
										<c:if test="${not empty book.subtitle && 'null' ne book.subtitle}">
											<strong><c:out value="${book.subtitle}" escapeXml="false" /></strong>
											<br/>
										</c:if>
										<c:set var="authorRoleTitle" value="${book.authorRoleTitleList}" />
										<c:set var="authorAffiliation" value="${book.authorAffiliationList}" />
										<c:forEach items="${book.authorNameList}" var="author" varStatus="status">
											<c:out value="${authorRoleTitle[status.count - 1]}" /> <c:out value="${author}" escapeXml="false" /><br />
											<c:if test="${not empty authorAffiliation and not empty authorAffiliation[status.count - 1] and 'none' ne authorAffiliation[status.count - 1]}">														
												<em><c:out value="${authorAffiliation[status.count - 1]}" escapeXml="false" /></em><br />	
											</c:if>													
											<c:if test="${not status.last}">
												<br />
											</c:if>
										</c:forEach>
									</div>
									
									<div class="clear_bottom"></div>
									
										 <!-- START CHOOSE APPROPRIATE ICON ACCESS -->
										<c:choose>						
											<c:when test="${series.freeContent}">
												<div class="icon_access_on">
													<img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_f_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Free Access Enabled" width="30" height="30" />
												</div>
											</c:when>
											<c:when test="${series.hasAccess}">
												<div class="icon_access_on">
													<img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_s_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Subscription Access Enabled" width="30" height="30" />
												</div>
											</c:when>							
											<c:otherwise>																
												<div class="icon_access_off" title="You don't have access."></div>
											</c:otherwise>
										</c:choose>
										<!-- END CHOOSE APPROPRIATE ICON ACCESS -->
										
										<div class="clear"></div>  
										
									
										<c:if test="${not empty book.series && 'null' ne book.series}">
											<p><c:out value="${book.series}" escapeXml="false" />
											<c:if test="${not empty book.seriesNumber}">
												<c:out value=" (No. ${book.seriesNumber})" escapeXml="false" />
											</c:if>										
											<br/></p>
										</c:if>
									
									
										<div class="list_divider01">
											<c:if test="${not empty book.printDate}">
												<strong>Print Publication Year:</strong>&nbsp;<c:out value="${book.printDate}" escapeXml="false" /><br/>
											</c:if>							
											<c:if test="${not empty book.printIsbn}">
												<strong>Print ISBN:</strong>&nbsp;<c:out value="${book.printIsbn}" />
											</c:if>
										</div>
			
										<div class="list_divider01">
											<c:if test="${not empty book.onlineDate}">
												<strong>Online Publication Date:</strong>&nbsp;<c:out value="${book.onlineDate}" escapeXml="false" /><br/>
											</c:if>	
											<c:if test="${not empty book.onlineIsbn}">
												<strong>Online ISBN:</strong>&nbsp;<c:out value="${book.onlineIsbn}" />
											</c:if>	
										</div>
										
										<div class="list_divider01">
											<c:if test="${not empty book.doi}">
												<strong>Book DOI:</strong>&nbsp;<c:out value="${book.doi}" />
											</c:if>	
										</div>

									<div class="clear_bottom"></div>
									
									<p><c:out value="${book.blurb}" escapeXml="false" /></p>

								</div>
								<!-- Sub Books End -->
					  
							</div>
		
						</c:forEach>    	
					</c:when>
					<c:otherwise>
						<div class="content_wrapper04">
							<h2>No results found.</h2>
						</div>
					</c:otherwise>
				</c:choose>                
     	
	        	<c:if test="${seriesBean.resultsFound > 0}">
					<div class="bar_container link_02">
			           	<!-- Pager Menu Start -->
						<form action="<c:out value="${urlSeriesLanding}"/>" method="post">
							<jsp:include page="../aaa/components/pager.jsp" flush="true">
								<jsp:param name="bottom" value="bottom"></jsp:param>
							</jsp:include>
						</form>
						<!-- Pager Menu End -->
		            </div>
		         </c:if>
 			</div>
 			<!-- Border End -->
			        
       		<!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
     
        </div>         
        <!-- End Body Content -->     
 
	
</div> 
	   	<!-- FooterContainer Start -->
	  		<jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
</body>
</html>