<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="ContextPath" value='<%= System.getProperty("ebooks.context.path") %>'></c:set>

<div class="icons_img cup_logo"><a href="http://www.cambridge.org/" title="Cambridge University Press"><img src="${ContextPath}aaa/images/cup_logo.jpg" alt="www.cambridge.org" /></a></div>

<div class="access_menu">

	<c:set var="name" value="${accBean.pageName}" />
	<c:set var="isAccessible" value="${accBean.accessible}" scope="session"/>
	
	<c:set var="acc_01" value="${sessionStyle == 'default_style' || sessionStyle == null ? 'Default Font Selected' : 'Change to Default Font' }" scope="page"/>
	<c:set var="acc_02" value="${sessionStyle == 'medium_style' ? 'Large Font Selected' : 'Change to Large Font' }" scope="page"/>
	<c:set var="acc_03" value="${sessionStyle == 'large_style' ? 'Largest Font Selected' : 'Change to Largest Font' }" scope="page"/>
	<c:set var="acc_04" value="${sessionStyle == 'contrast_style' ? 'High Contrast Active' : 'Change Contrast' }" scope="page"/>
	
	<c:set var="view_01" value="${sessionStyle == 'default_style' || sessionStyle == null ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_02" value="${sessionStyle == 'medium_style' ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_03" value="${sessionStyle == 'large_style' ? 'current_view' : '' }" scope="page"/>
	<c:set var="view_04" value="${sessionStyle == 'contrast_style' ? 'current_view' : '' }" scope="page"/>

	<c:set var="hrefLink" value="nothing" scope="page"></c:set>
	<%-- 
	<c:set var="hrefSkipId" value="<%=request.getParameter("skipId")%>" scope="page"></c:set>
	<c:set var="ContextPath" value="<%= System.getProperty("ebooks.context.path") %>"></c:set>
	--%>
	<c:set var="hrefSkipId" value="${param.skipId}" scope="page"></c:set>
	
	
	<c:choose>
		<c:when test="${name == 'search_results.jsf?'}">
			<c:set var="hrefLink" value="${domainCbo}search_aaa?searchType=page&currentPage=${searchResultBean.currentPage}&"></c:set>
		</c:when>
		<c:when test="${name == 'email_link.jsf?'}">
			<c:url value="${accBean.pagePath}" var="emailLinkUrl">
				<c:param name="emailType" value="BL" />
				<c:param name="Title" value="${param.Title}"/>
				<c:param name="Copyright" value="${param.Copyright}"/>
				<c:param name="PrintpubDate" value="${param.PrintpubDate}"/>
				<c:param name="OnlinepubDate" value="${param.OnlinepubDate}"/>
				<c:param name="OnlineIsbn" value="${param.OnlineIsbn}"/>
				<c:param name="HardbackIsbn" value="${param.HardbackIsbn}"/>
				<c:param name="Doi" value="${param.Doi}"/>
				<c:param name="exactURL" value="${param.exactURL}"/>
				<c:param name="successBookLink" value="${emailBooklink.bookLink}" />
				
				<c:param name="userName" value="${param.reset == 'all' ? '' : param.userName}" />
				<c:param name="userEmail" value="${param.reset == 'all' ? '' : param.userEmail}" />
				<c:param name="recipientName" value="${param.reset == 'all' ? '' : param.recipientName}" />
				<c:param name="recipientEmail" value="${param.reset == 'all' ? '' : param.recipientEmail}" />
				<c:param name="subject" value="${param.submit != null  ? param.subject : emailBooklink.subject }" /> 
				<c:param name="mailmessage" value="${param.submit !=null  ? param.mailmessage : emailBooklink.mailmessage }" />
				
				<c:if test="${param.submit !=null}">
					<c:param name="submit" value="${param.submit}" />
				</c:if>
				
				
		  	</c:url>
			<c:set var="hrefLink" value="${emailLinkUrl}"></c:set>
		</c:when>
		<c:when test="${name == 'recommendToLibrarian.jsf?'}">
			<c:url value="${accBean.pagePath}" var="recommendUrl">			
				<c:if test="${param.recommendType == 'firstType'}">
					<c:param name="Title" value="${param.Title}" />
					<c:param name="Copyright" value="${param.Copyright}" />
					<c:param name="PrintpubDate" value="${param.PrintpubDate}" />
					<c:param name="OnlinepubDate" value="${param.OnlinepubDate}" />
					<c:param name="OnlineIsbn" value="${param.OnlineIsbn}" />
					<c:param name="HardbackIsbn" value="${param.HardbackIsbn}" />
					<c:param name="Doi" value="${param.Doi}" />
				</c:if>
				<c:if test="${param.recommendType == 'secondType'}">
					<c:param name="Title" value="${param.Title}" />
					<c:param name="Author" value="${param.Author}" />
					<c:param name="Affiliation" value="${param.Affiliation}" />
					<c:param name="Copyright" value="${param.Copyright}" />
					<c:param name="PrintpubDate" value="${param.PrintpubDate}" />
					<c:param name="OnlinepubDate" value="${param.OnlinepubDate}" />
					<c:param name="OnlineIsbn" value="${param.OnlineIsbn}" />
					<c:param name="HardbackIsbn" value="${param.HardbackIsbn}" />
					<c:param name="Doi" value="${param.Doi}" />
				</c:if>
				<c:param name="bookId" value="${param.bookId}" />
				<c:param name="recommendType" value="${param.recommendType}" />
				
				<c:param name="userName" value="${param.submit != null  ? param.userName : recommendToLibrarian.userName}" />
				<c:param name="userEmail" value="${param.submit != null ? param.userEmail : recommendToLibrarian.userEmail}" />
				<c:param name="recipientName" value="${param.submit != null ? param.recipientName : recommendToLibrarian.recipientName}" />
				<c:param name="recipientEmail" value="${param.submit != null ? param.recipientEmail : recommendToLibrarian.recipientEmail}" />

				<c:param name="mailmessage" value="${param.submit != null  ? param.mailmessage : recommendToLibrarian.mailmessage}" />
				
				<c:if test="${param.submit !=null}">
					<c:param name="submit" value="${param.submit}" />
				</c:if>
				
		  	</c:url>
			<c:set var="hrefLink" value="${recommendUrl}"></c:set>
		</c:when>
		<c:when test="${name == 'view_access.jsf?'}">
			<c:url value="${accBean.pagePath}" var="viewAccessUrl" >
				<c:param name="userEmailAddress" value="${param.email != null ? param.userEmailAddress : accessDetailsBean.userEmailAddress}" />
				<c:param name="subject" value="${param.email != null  ? param.subject : accessDetailsBean.emailSubject }" />
				<c:param name="typeOfProblem" value="${param.email != null  ? param.typeOfProblem : '' }" />
				<c:param name="problemDescription" value="${param.email != null  ? param.problemDescription : '' }" />
				<c:param name="location" value="${param.location}" />
				
				<c:if test="${param.email !=null}">
					<c:param name="email" value="${param.email}" />
				</c:if>
						
			</c:url>
			<c:set var="hrefLink" value="${viewAccessUrl}${accBean.queryString}"></c:set>
		</c:when>
		<c:otherwise>
			<c:set var="hrefLink" value="${accBean.pagePath}${accBean.queryString}"></c:set>
		</c:otherwise>
	</c:choose>

	<ul>
    	<li><a href="#maincontent" title="Skip to Content"><img src="${ContextPath}aaa/images/bot_skip.gif" alt="Skip to Content" width="192" height="30" /></a></li>
    	<li class="${view_01}"><a href="<c:out value="${hrefLink}stylez=default_style#${hrefSkipId}" escapeXml="true"  />" title="${acc_01}" class="small_font"><img src="${ContextPath}aaa/images/bot_font_small.gif" alt="${acc_01}" width="34" height="30" /></a></li>
        <li class="${view_02}"><a href="<c:out value="${hrefLink}stylez=medium_style#${hrefSkipId}"  escapeXml="true"  />" title="${acc_02}" class="medium_font"><img src="${ContextPath}aaa/images/bot_font_large.gif" alt="${acc_02}" width="34" height="30" /></a></li>
        <li class="${view_03}"><a href="<c:out value="${hrefLink}stylez=large_style#${hrefSkipId}"  escapeXml="true" />" title="${acc_03}" class="large_font"><img src="${ContextPath}aaa/images/bot_font_largest.gif" alt="${acc_03}" width="34" height="30" /></a></li>                            
        <li class="${view_04}"><a href="<c:out value="${hrefLink}stylez=contrast_style#${hrefSkipId}"  escapeXml="true"  />" title="${acc_04}" class="change_contrast"><img src="${ContextPath}aaa/images/bot_contrast.gif" alt="${acc_04}" width="34" height="30" /></a></li>
	</ul>
	<!-- end ul -->
	
</div>



	

