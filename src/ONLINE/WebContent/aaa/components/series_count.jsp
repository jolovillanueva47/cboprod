<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

	<c:choose>
		<c:when test="${seriesSubjectSearcherBean_aaa.seriesCount > 1}">
			There are a total of 
			<strong><c:out value="${seriesSubjectSearcherBean_aaa.seriesCount}" /></strong> series.
		</c:when>
		<c:when test="${seriesSubjectSearcherBean_aaa.seriesCount == 1}">
			There is only 
			<strong><c:out value="${seriesSubjectSearcherBean_aaa.seriesCount}" /></strong> series.
		</c:when>
		<c:otherwise>
			There are no series available.					
		</c:otherwise>
	</c:choose>			


