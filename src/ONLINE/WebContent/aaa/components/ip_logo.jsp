<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!--<span><h2>Login Options</h2></span>-->
<div class="hide_titlepage"><h2>Login Options</h2></div><!-- Hidden Titlepage -->
<ul class="fr">
<c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
<%-- 	<c:if test="${accBean.pagePath eq '/aaa/home.jsf?'}">--%>
		<li><a href="${pageContext.request.contextPath}/home.jsf" class="link10">Standard Version</a></li>
<%--	</c:if>--%>
	<!-- If the current page is search_results.jsf -->
	<c:url var="urlLogin" value="${pageContext.request.contextPath}/aaa/login.jsf">
		<c:if test="${fn:contains(accBean.pagePath,'/aaa/search_results.jsf')}">
			<c:param name="currentPage" value="${param.currentPage_aaa}"/>
			<c:param name="resultsPerPage" value="${param.resultsPerPage_aaa}"/>
			<c:param name="sortType" value="${param.sortType_aaa}"/>
		</c:if>
	</c:url>
	<c:choose>
		<c:when test="${not pageScope.isLoggedIn}">
			<li><a href="${pageContext.request.contextPath}/aaa/login.jsf" title="Login">Login</a>
				<c:choose>
					<c:when test="${! (fn:contains(accBean.pagePath,'/aaa/login.jsf?')|| fn:contains(accBean.queryString,'lo=y&')) }">
						<c:set var="lastUrl" value="${accBean.pagePath}${accBean.queryString}" scope="session"/>
						<c:if test="${fn:contains(accBean.pagePath,'/aaa/popups/alert_me_not.jsf?')}">
							<c:set var="lastUrl" value="/aaa/popups/alert_me.jsf?${accBean.queryString}" scope="session"/>
						</c:if>
					</c:when>
					<c:when test="${fn:contains(accBean.queryString,'lo=y&')}">
						<c:set var="lastUrl" value="${accBean.pagePath}" scope="session"/>
					</c:when>
				</c:choose>
				</li>
		</c:when>
		<c:otherwise>
			<li><a href="${pageContext.request.contextPath}/aaa/home.jsf?lo=y" title="Logout">Logout</a></li>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${empty athensId}">
			<li><a href="<%= System.getProperty("openathensLink_aaa") %>" title="Athens Login">Athens Login</a></li>
		</c:when>
		<c:otherwise>
			<li><a href="${pageContext.request.contextPath}/aaa/home.jsf?alo=y" title="Athens Logout">Athens Logout</a></li>	
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${empty shibbId}">
			<li><a href="<%= System.getProperty("shibbolethLink") %>/aaa/home.jsf" title="Shibboleth Login">Shibboleth Login</a></li>
		</c:when>
		<c:otherwise>
			<li><a href="${pageContext.request.contextPath}/aaa/home.jsf?slo=y" title="Shibboleth Logout">Shibboleth Logout</a></li>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${pageScope.isLoggedIn}">
			<li><a href="<%= System.getProperty("cjo.url")%>accmanagement?id=${userInfo.encryptedDetails}&amp;topage=updateRegistration_aaa&amp;stylez=${sessionStyle == null ? 'default_style' : sessionStyle}">My Account</a></li>
		</c:when>
		<c:otherwise>
			<li><a href="<%= System.getProperty("cjo.url") %>registration_aaa?displayname=${referalName}&amp;stylez=${sessionStyle == null ? 'default_style' : sessionStyle}" title="Register">Register</a></li>
		</c:otherwise>
	</c:choose>
	
	<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">	   
		<li><a href="${pageContext.request.contextPath}/aaa/ebooks/subscriptionServlet_aaa?ACTION=ALL&amp;sortBy=1" class="link10">Access to</a></li>			
	</c:if>
</ul>
<div class="clear"></div>