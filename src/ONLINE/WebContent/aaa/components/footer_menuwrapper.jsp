<!-- Footermenu Start -->
<div class="footer">
<!--<span><h2>Footer Links</h2></span>-->
<div class="hide_titlepage"><h2>Footer Links</h2></div><!-- Hidden Titlepage -->
	<a href="http://www.cambridge.org/online/" target="_blank">Other Online Products from Cambridge</a> | 
	<a href="http://journals.cambridge.org/" target="_blank">Cambridge Journals Online</a> |  
	<a href="/clc/home.jsf" class="link01" target="cjo">Cambridge Library Collection</a>|
	<a href="http://cambridge.org/uk/browse/default.asp?subjectid=1000001" target="_blank">Academic and Professional Books</a> |  
	<a href="http://www.cambridge.org/learning/" target="_blank">Cambridge Learning</a> |
	<a href="http://www.cambridge.org/uk/bibles/" target="_blank">Bibles</a>
	<br />
	&copy; Cambridge University Press 2010.
	<a href="${pageContext.request.contextPath}/aaa/templates/about.jsf" >About Cambridge Books Online</a> |  
	<a href="${pageContext.request.contextPath}/aaa/templates/contact.jsf" >Contact</a> |  
	<a href="${pageContext.request.contextPath}/aaa/templates/accessibility.jsf" >Accessibility </a> |  
	<a href="${pageContext.request.contextPath}/aaa/templates/terms_of_use.jsf" >Terms of use</a> |  
	<a href="${pageContext.request.contextPath}/aaa/templates/privacy_policy.jsf" >Privacy Policy</a> |
	<a href="${pageContext.request.contextPath}/aaa/templates/rights_and_permissions.jsf" >Rights &amp; Permissions</a> |
	<a href="${pageContext.request.contextPath}/aaa/sitemap.jsf" >Site Map</a><%-- |
	<a href="<%= System.getProperty("ebooks.context.path") %>home.jsf" ><strong>Standard Version</strong></a>--%> 
</div>
<!-- Footermenu End -->