<%@page import="org.cambridge.ebooks.online.jpa.countrystates.Country"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:set var="country" value="${bookBean.country}" scope="page" />	
<c:set var="bookId" value="${bookBean.bookMetaData.id}" scope="page" />
<c:set var="bookTitleSession" value="${bookBean.bookMetaData.title}" scope="session" />			
<c:set var="bookTitle" value="${bookBean.bookMetaData.title}" scope="page" />
<c:set var="chapterId" value="${bookBean.bookContentItem.id}" scope="page" />	
<c:set var="chapterTitle" value="${bookBean.bookContentItem.title}" scope="page" />
<c:set var="chapterPageStart" value="${bookBean.bookContentItem.pageStart}" scope="page" />
<c:set var="chapterPageEnd" value="${bookBean.bookContentItem.pageEnd}" scope="page" />
<c:set var="copyright" value="${bookBean.bookMetaData.copyrightStatement}" scope="page" />
<c:set var="printDate" value="${bookBean.bookMetaData.printDate}" scope="page" />
<c:set var="onlineDate" value="${bookBean.bookMetaData.onlineDate}" scope="page" />
<c:set var="onlineIsbn" value="${bookBean.bookMetaData.onlineIsbn}" scope="page" />
<c:set var="printIsbn" value="${bookBean.bookMetaData.printIsbn}" scope="page"/>
<c:set var="hardbackIsbn" value="${bookBean.bookMetaData.hardbackIsbn}" scope="page" />
<c:set var="doi" value="${bookBean.bookMetaData.doi}" scope="page" />
<c:set var="chapterDoi" value="${bookBean.bookContentItem.doi}" scope="page" />
<c:set var="authorNames" value="${bookBean.bookMetaData.authorNames}" scope="page" />
<c:set var="authorNamesSession" value="${bookBean.bookMetaData.authorNames}" scope="session" />
<c:set var="authorNamesLf" value="${bookBean.bookMetaData.authorNamesLf}" scope="page" />
<c:set var="contributorNames" value="${bookBean.bookContentItem.contributorNames}" scope="page" />
<c:set var="currentIsbn" value="${bookBean.bookMetaData.onlineIsbn}" scope="request" />

<c:set var="worldCatLink" value="${bookBean.worldCatLink}" scope="page" />

<c:remove var="authorList" scope="session" />
<c:remove var="authorRoleList" scope="session" />
<c:remove var="authorAffiliationList" scope="session" />
<c:set var="authorList" value="${bookBean.bookMetaData.authorNameList}" scope="session"/>
<c:set var="authorRoleList" value="${bookBean.bookMetaData.authorRoleTitleList}" scope="session"/>
<c:set var="authorAffiliationList" value="${bookBean.bookMetaData.authorAffiliationList}" scope="session"/>		

<c:set var="ebookUrl" value="${fn:replace(pageContext.request.requestURL, '.jsp', '.jsf')}?${pageContext.request.queryString}" scope="page" />
<c:set var="googleScholar" value="${fn:trim(googleScholarSrc)}ebook.jsf?${pageContext.request.queryString}" />

<c:choose>
	<c:when test="${fn:contains(pageContext.request.requestURL, 'chapter.jsp')}">
		
		<c:url value="../aaa/popups/how_to_cite.jsf" var="urlHowToCite">
			<c:param name="bookId" value="${bookId}" />
			<c:param name="bookTitle" value="${bookTitle}" />
			<c:param name="contentId" value="${chapterId}" />
			<c:param name="contentTitle" value="${chapterTitle}" />
			<c:param name="page" value="pp. ${chapterPageStart}-${chapterPageEnd}" />
			<c:param name="author" value="${authorNamesLf}" />
			<c:param name="printDate" value="${printDate}" />
			<c:param name="doi" value="${chapterDoi}" />		
	 	</c:url>
	 	
	 	<!-- start cited by crossref-->
		<c:if test="${not empty chapterDoi}">
			<c:url value="/aaa/popups/citedBy_aaa" var="urlCitedByCrossref">
				<c:param name="doi" value="${chapterDoi}" />
		 	</c:url>
	 	</c:if>
	 	<!-- end cited by crossref -->
	 	
	 	<c:choose>
			<c:when test="${not empty userInfo}">
				<c:url value="../aaa/popups/alert_me.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />
					<c:param name="doi" value="${chapterDoi}" />
					<c:param name="author" value="${authorNames}" />
					<c:param name="bookId" value="${bookId}" />
					<c:param name="bookTitle" value="${bookTitle}" />
					<c:param name="contentId" value="${chapterId}" />
					<c:param name="contentTitle" value="${chapterTitle}" />
					<c:param name="page" value="pp. ${chapterPageStart}-${chapterPageEnd}" />
					<c:param name="publishDate" value="${onlineDate}" />
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="455" />
				</c:url>
			</c:when>
			<c:otherwise>			
				<c:url value="../aaa/popups/alert_me_not.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />								
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="100" />
				</c:url>
			</c:otherwise>
		</c:choose>	
	</c:when>
	<c:otherwise>						
		<c:url value="../aaa/popups/how_to_cite.jsf" var="urlHowToCite">
			<c:param name="bookId" value="${bookId}" />
			<c:param name="bookTitle" value="${bookTitle}" />
			<c:param name="author" value="${authorNamesLf}" />
			<c:param name="printDate" value="${printDate}" />
			<c:param name="doi" value="${doi}" />		
	 	</c:url>
				 	
		<!-- start cited by crossref-->
		<c:if test="${not empty doi}">
			<c:url value="/aaa/popups/citedBy_aaa" var="urlCitedByCrossref">
				<c:param name="doi" value="${doi}" />
		 	</c:url>
	 	</c:if>
	 	<!-- end cited by crossref -->
				 			
		<c:choose>
			<c:when test="${not empty userInfo}">
				<c:url value="../aaa/popups/alert_me.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />
					<c:param name="doi" value="${doi}" />
					<c:param name="author" value="${authorNames}" />
					<c:param name="bookId" value="${bookId}" />
					<c:param name="bookTitle" value="${bookTitle}" />
					<c:param name="publishDate" value="${onlineDate}" />
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="455" />
				</c:url>
			</c:when>
			<c:otherwise>			
				<c:url value="../aaa/popups/alert_me_not.jsf" var="urlAlertMe">
					<c:param name="keepThis" value="true" />								
					<c:param name="TB_iframe" value="true" />
					<c:param name="width" value="850" />
					<c:param name="height" value="100" />
				</c:url>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

<!-- Shared URL Start -->
<c:url value="../aaa/openUrlResolver.jsf" var="openUrlResolver">
 	<c:param name="keepThis" value="true" />  		
	<c:param name="title" value="${bookTitle}" />
	<c:param name="TB_iframe" value="true" />  		
	<c:param name="width" value="730" />  
	<c:param name="height" value="435" /> 
</c:url>

<c:url value="../aaa/popups/export_citation.jsf" var="urlExportCitation">
	<c:param name="isbn" value="${onlineIsbn}" />
	<c:param name="bookId" value="${bookId}" />
	<c:param name="id" value="${bookId}" />
	<c:param name="bookTitle" value="${bookTitle}" />
	<c:param name="contentId" value="${chapterId}" />						
	<c:param name="contentTitle" value="${chapterTitle}" />						
	<c:param name="printDate" value="${printDate}" />						
	<c:param name="doi" value="${doi}" />								
	<c:param name="page" value="${page}" />
	<c:param name="author" value="${authorNames}" />
	<c:param name="contributor" value="${contributorNames}" />
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="850" />
	<c:param name="height" value="455" />
</c:url>
				 	
<c:url value='../aaa/popups/email_link.jsf' var="emailLink">			
  	<c:param name="keepThis" value="true"/>
  	<c:param name="Title" value="${bookTitle}" />
	<c:param name="Copyright" value="${copyright}" />
	<c:param name="PrintpubDate" value="${printDate}" />
	<c:param name="OnlinepubDate" value="${onlineDate}" />
	<c:param name="OnlineIsbn" value="${onlineIsbn}" />
	<c:param name="HardbackIsbn" value="${hardbackIsbn}" />
	<c:param name="Doi" value="${doi}" />		
	<c:param name="exactURL" value="${ebookUrl}" />				 
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="620" />
	<c:param name="height" value="480" />
</c:url>					

<c:url value="http://delicious.com/save?" var="urlDelicious">
	<c:param name="url" value ="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.connotea.org/addpopup" var="urlConnotea">
	<c:param name="uri" value ="${ebookUrl}" />
	<c:param name="usertitle" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.bibsonomy.org/ShowBookmarkEntry" var="urlBibsonomy">
	<c:param name="url" value ="${ebookUrl}" />
	<c:param name="description" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://digg.com/submit?" var="urlDigg">
	<c:param name="url" value ="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>			

<c:url value="http://reddit.com/submit" var="urlReddit">
	<c:param name="url" value ="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<c:url value="http://www.facebook.com/sharer.php" var="urlFacebook">
	<c:param name="u" value ="${ebookUrl}" />
</c:url>
		
<c:url value="http://addthis.com/bookmark.php" var="urlAddThis">
	<c:param name="url" value ="${ebookUrl}" />
	<c:param name="title" value="Cambridge Books Online - ${fn:replace(bookTitle, '%26%2338%3B', '%26')}" />
</c:url>

<!-- Shared URL End -->
                    
<!-- Right menu Start -->       
<div class="nav_right">
	
  	<ul class="nav_right_bar">
  	
  		<!-- Open URL resolver start -->
		<li><a href="<c:out value="${openUrlResolver}" escapeXml="true" />" >Open URL Link Resolver</a></li>
 		
		<!-- buy the book start -->		
	    <c:choose>
	    		<li><a href="http://www.cambridge.org/${printIsbn}" target="_blank" title="Link opens in new Window" >Buy the Book</a></li>
	    </c:choose>	
	
		<!-- buy the book end -->
	
	
      <%-- temporarily disabled 
      <a class="menuitem submenuheader" href="#">Trackback Links</a>
         <div class="submenu"></div>
	  --%>
	  
		<!-- find this book in a library -->		
		<li><a href="${worldCatLink}" target="_blank" title="www.worldcat.org opens in new window">Find This Book in a Library</a></li>

	    <!-- Email Link to This Book --> 
	    <li><a href="<c:out value='${emailLink}' escapeXml='true' />">Email Link to This Book</a></li>

		<!-- Citation Tools Start -->
	 	<li><p>Citation Tools</p></li>
	       	<li>    			 	
				<a href="<c:out value="${urlHowToCite}" />">
				How to Cite</a>               
			</li>
      		<li>	
      			<a href="<c:out value='${urlExportCitation}' escapeXml='true' />" >Export Citation</a>
      		</li>
      		<li>          			
				<a href="<c:out value="${urlAlertMe}" escapeXml="true" />" >Alert Me When This Book Is Cited</a>
            </li>
            <!-- cited by crossref -->
			<c:if test="${not empty urlCitedByCrossref}" >
				<li>
					<a href="${urlCitedByCrossref}" >Cited By (CrossRef)</a>
				</li>
			</c:if>
			<!-- cited by crossref end-->

			<%-- cited by google scholar 	
            <li>
				<a href="http://scholar.google.com/scholar?q=link:${googleScholar}" target="_blank" title="opens in new tab/window">Cited-by (Google Scholar)</a>
            </li>
            --%>
		<!-- Citation Tools Start -->

		<!-- Link to this Book Start -->
	    <li><p>Link to this Book</p></li>
	    <%-- <li><a href="#"><span class="icons_img delicious">&nbsp;</span>Del.icio.us</a></li>
 			<li><a href="#"><span class="icons_img connotea">&nbsp;</span>Connotea.org</a></li>
 			<li><a href="#"><span class="icons_img bibsonomy">&nbsp;</span>Bibsonomy.org</a></li>
			<li><a href="#"><span class="icons_img digg">&nbsp;</span>Digg.com</a></li>
 			<li><a href="#"><span class="icons_img reddit">&nbsp;</span>Reddit.com</a></li>
			<li><a href="#"><span class="icons_img facebook">&nbsp;</span>Facebook.com</a></li>
			<li><a href="#"><span class="icons_img addthis">&nbsp;</span>AddThis.com</a></li> --%>
       	
            <li>
				<a href="<c:out value='${urlDelicious}' escapeXml='true' />" target="_blank" title="Link to delicious.com"><span class="icons_img delicious">&nbsp;</span>
					Del.icio.us
				</a>
			</li>		
            <li>
				<a href="<c:out value='${urlConnotea}' escapeXml='true' />" target="_blank" title="Link to connotea.org"><span class="icons_img connotea">&nbsp;</span>
					Connotea.org
				</a>               		
			</li>			
            <li>
            	<a href="<c:out value='${urlBibsonomy}' escapeXml='true' />" target="_blank" title="Link to bibsonomy.org"><span class="icons_img bibsonomy">&nbsp;</span>
					Bibsonomy.org
				</a>                   	
			</li>			
            <li>
                <a href="<c:out value='${urlDigg}' escapeXml='true' />" target="_blank" title="Link to digg.com"><span class="icons_img digg">&nbsp;</span>
					Digg.com
				</a>
			</li>		
            <li>
                <a href="<c:out value='${urlReddit}' escapeXml='true' />" target="_blank" title="Link to reddit.com"><span class="icons_img reddit">&nbsp;</span>
					Reddit.com
				</a>
           	</li>            
            <li>
                <a href="<c:out value='${urlFacebook}' escapeXml='true' />" target="_blank" title="Link to facebook.com"><span class="icons_img facebook">&nbsp;</span>
					Facebook.com
				</a>            
            </li>           
            <li>
                <a href="<c:out value='${urlAddThis}' escapeXml='true' />" target="_blank" title="Link to facebook.com"><span class="icons_img addthis">&nbsp;</span>
					AddThis.com
				</a>             
            </li>
        
       	<c:if test="${supplemental.supplementalMaterial eq 'Y'}">    	
		   	<c:url var="urlResourcesAndSolutions" value="${supplemental.supplementalMaterialUrl}"/>
		   	<li><a href="<c:out value='${urlResourcesAndSolutions}' />" >Resources and Solutions</a></li>
		</c:if>
		<%--   
	  	<c:if test="${'Y' eq supplemental.supplementalMaterial}">    	
	    	<a href="#" onclick="anchor('${supplemental.supplementalMaterialUrl}'); return false;" class="menuitem submenuheader">Resources and Solutions</a>
	    </c:if>  
	    --%>
	   
</ul>           
 	<jsp:include flush="true" page="../../aaa/components/recommendation_print_links.jsp" />              
</div>
<!-- Right menu End --> 

