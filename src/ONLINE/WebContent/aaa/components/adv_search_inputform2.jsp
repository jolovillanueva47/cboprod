<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<jsp:useBean id="advSearchBean" class="org.cambridge.ebooks.online.search.AdvanceSearchBean" scope="request"/>
			
<!-- Search Form Start -->	
	   
			    <c:if test="${param.search != 'fail'}">
					<c:set var="query" value="${searcherMap['queryBuilder'].queries}"></c:set>
				</c:if>

					<c:forEach begin="1" end="8" var="index">
						<c:set var="index_02" value="${index-1}"/>
								<c:choose>
									<c:when test="${index == 7 || index == 8 }">
										<!-- Target Start -->
			         					<select id="target_${index}" name="target_${index}" title="Criteria one" >
				            				<c:forEach var="adv_search_option" items="${advSearchBean.targetArray_2}">
				            					<c:choose>
													<c:when test="${query[index_02].target eq adv_search_option}">
														<option value="${adv_search_option}" selected="selected">${adv_search_option}</option>
													</c:when>
													<c:otherwise>
												
														<c:choose>
															<c:when test="">
																<option value="${adv_search_option}" selected="selected">${adv_search_option}</option>
															</c:when>
															<c:otherwise>
																<option value="${adv_search_option}">${adv_search_option}</option>
															</c:otherwise>
														</c:choose>
															

														
													</c:otherwise>
												</c:choose>
				            				</c:forEach>
				         				</select>
				         				<!-- Target End -->         						
	 
	 			         				<!-- Condition Start -->
				         				<select id="condition_${index}" name="condition_${index}" title="Criteria two">
				            				<c:forEach var="adv_search_option" items="${advSearchBean.conditionArray_2}">
				            					<c:choose>
													<c:when test="${query[index_02].condition eq adv_search_option}">
														<option value="${adv_search_option}" selected="selected">${adv_search_option}</option>
													</c:when>
													<c:otherwise>
														<option value="${adv_search_option}">${adv_search_option}</option>
													</c:otherwise>
												</c:choose>
				            				</c:forEach>
				         				</select>
				         				<!-- Condition End -->
		         				
				         				<!-- SearchWord Start -->
				         				<input id="search_word_${index}" name="search_word_${index}" type="text" value="${query[index_02].searchWord1}" title="Search word one" class="input_text"/>
										<label id="betweenSpan_${index}" for="search_word2_${index}" >and/to&nbsp;</label>							
										<input id="search_word2_${index}" name="search_word2_${index}" type="text" value="${query[index_02].searchWord2}" title="Search word two" class="input_text"/>
										<!-- SearchWord End -->         										
         							</c:when>
         							<c:otherwise>
										<!-- Target Start -->
			         					<select id="target_${index}" name="target_${index}" title="Criteria one" >
				            				<c:forEach var="adv_search_option" items="${advSearchBean.targetArray}">
				            					<c:choose>
													<c:when test="${query[index_02].target eq adv_search_option}">
														<option value="${adv_search_option}" selected="selected">${adv_search_option}</option>
													</c:when>
													<c:otherwise>
														<option value="${adv_search_option}">${adv_search_option}</option>
													</c:otherwise>
												</c:choose>
				            				</c:forEach>
				         				</select>
				         				<!-- Target End -->
	         				
					         			<!-- Condition Start -->
					         			<select id="condition_${index}" name="condition_${index}" title="Criteria two">
					            			<c:forEach var="adv_search_option" items="${advSearchBean.conditionArray}">
					            				<c:choose>
													<c:when test="${query[index_02].condition eq adv_search_option}">
														<option value="${adv_search_option}" selected="selected">${adv_search_option}</option>
													</c:when>
													<c:otherwise>
														<option value="${adv_search_option}">${adv_search_option}</option>
													</c:otherwise>
												</c:choose>
					            			</c:forEach>
					         			</select>
				         				<!-- Condition End -->
	         				
					         			<!-- SearchWord Start -->
					         			<input id="search_word_${index}" name="search_word_${index}" type="text" value="${query[index_02].searchWord1}" title="Search word one" class="input_text"/>
										<!-- <label id="betweenSpan_${index}" >and/to&nbsp;</label> -->							
										<input id="search_word2_${index}" name="search_word2_${index}" type="hidden"" value="${query[index_02].searchWord2}" title="Search word two"/>
										<!-- SearchWord End -->         						
         							</c:otherwise>
								</c:choose>

         						<!-- LogicalOp Start -->
		         				<c:if test="${index < 8 }" >
			         				<select id="logical_op_${index}" name="logical_op_${index}" title="Logical Operator" >
			            				<c:forEach var="adv_search_option" items="${advSearchBean.logicalOpArray}">
			            							<option value="${adv_search_option}">${adv_search_option}</option>
										<!--<c:out value="${query[index_02].logicalOp}"></c:out>-->
			            				</c:forEach>
			         				</select>
		         				</c:if>
								<!-- LogicalOp End -->         					

  						<div class="clear_bottom"></div>  

	         		</c:forEach>
		    		
<!-- Search Form End -->		
