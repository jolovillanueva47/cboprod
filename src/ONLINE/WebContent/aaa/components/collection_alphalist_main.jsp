<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
${collectionBean.initAlphaMap}
<div id="border01_container"> 
<div class="alphalist02">
	
	<ul>
		<c:forEach var="alphaMap" items="${collectionBean.alphaMap}">    	
				<c:choose>
			    	<c:when test="${'link' eq alphaMap.value}">
					    	<c:url value="collection_landing.jsf" var="collectionUrl">
								<c:param name="results" value="50" />
								<c:param name="sort" value="print_date" />
								<c:param name="collectionId" value="${collectionBean.collectionId}" />
								<c:param name="collectionName" value="${collectionBean.collectionName}" />
								<c:param name="firstLetter" value="${alphaMap.key}" />
							</c:url>
			    		<li><a class="link02" title="${alphaMap.key}" href="${collectionUrl}">${alphaMap.key}</a></li>
			    	</c:when>
			    	<c:otherwise>
			    		<li>${alphaMap.key}</li>
			    	</c:otherwise>					
			    </c:choose>			   
			</c:forEach>
		<c:url value="collection_landing.jsf" var="collectionUrl_ALL">
			<c:param name="results" value="50" />
			<c:param name="sort" value="print_date" />
			<c:param name="collectionId" value="${collectionBean.collectionId}" />
			<c:param name="collectionName" value="${collectionBean.collectionName}" />
		</c:url>
		<li><a class="link02" title="All Titles" href="${collectionUrl_ALL}">All Titles</a></li> 
	</ul>
	<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<p>Your search returned <strong>${collectionBean.resultsFound}</strong> result.</p>


