<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<div class="alphalist01">
	${seriesBean.initAlphaMap}
	<ul>
		<c:forEach var="alphaMap" items="${seriesBean.alphaMap}">    	
			<c:choose>
		    	<c:when test="${'link' eq alphaMap.value}">
		    		<li><a href="all_series.jsf?firstLetter=${alphaMap.key}">${alphaMap.key}</a></li>
		    	</c:when>
		    	<c:otherwise>
		    		<li>${alphaMap.key}</li>
		    	</c:otherwise>					
		    </c:choose>			   
		</c:forEach>
	</ul>
	<div class="clear"></div>
</div>
