<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<div id="alphalist02">
	<c:set var="subjectCode" value="${fn:substring(param.subjectCode,0,1)}" />
	<c:set var="subjectId" value="${fn:substring(param.subjectId,0,1)}" />
	<c:set var="isViewAll" value="${subjectId eq 'A' or subjectId eq 'B' or subjectId eq 'C' or subjectId eq 'D' or subjectId eq 'E'}"/>
	<input type="hidden" id="isViewAll" value="${isViewAll}"/>
	
	<ul>
	    <c:forEach var="alphabetMap" items="${subjectBean.alphaMap}">
		    <c:choose>
	    		<c:when test="${alphabetMap.value eq 'link'}">
	    			<c:url var="subject_alpha_url" value="subject_landing.jsf">
	    				<c:choose>
				    	 	<c:when test="${isViewAll}">
				    	 		<c:param name="subjectId" value="${subjectBean.subjectId}" />
				    	 	</c:when>
				    	 	<c:otherwise>
				    	 		<c:param name="subjectCode" value="${subjectBean.subjectCode}" />
			    			</c:otherwise>
	    				</c:choose>		    				
	    	 			<c:param name="subjectName" value="${subjectBean.subject}" />
	    	 			<c:param name="firstLetter" value="${alphabetMap.key}" />
	    			</c:url>		    	
	    			<li><a href="<c:out value="${subject_alpha_url}" />" class="link02">${alphabetMap.key}</a></li>
	    		</c:when>
	    		<c:otherwise>
	    			<li>${alphabetMap.key}</li>
	    		</c:otherwise>
	    	</c:choose>
	    </c:forEach>
	    <c:url var="view_all_url" value="subject_landing.jsf">
			<c:choose>
				<c:when test="${isViewAll}">
					<c:param name="searchType" value="allSubjectBook" />
    	 			<c:param name="subjectId" value="${subjectBean.subjectId}" />
				</c:when>
				<c:otherwise>
					<c:param name="searchType" value="allTitles" />
    	 			<c:param name="subjectCode" value="${subjectBean.subjectCode}" />
				</c:otherwise>
			</c:choose>
			<c:param name="subjectName" value="${subjectBean.subject}"/>
			<c:param name="sa" value="yes"/>
		</c:url>  
	    <li><a href='<c:out value="${view_all_url}" />' class="link02">All titles</a></li>
    </ul>
</div>