<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!--  <span><h2>Site Navigation</h2></span> -->
<div class="hide_titlepage"><h2>Site Navigation</h2></div><!-- Hidden Titlepage -->
<ul class="fl">
    <li><a href="${pageContext.request.contextPath}/aaa/home.jsf" title="Home">Home</a></li>
	<li><a href="${pageContext.request.contextPath}/aaa/templates/about.jsf" title="About">About</a></li>
	<li><a href="${pageContext.request.contextPath}/aaa/popups/faq.jsf?stylez=${sessionScope.sessionStyle}" title="FAQ">FAQ</a></li>
	<c:choose>
		<c:when test="${(pageId != '') || (pageName != null)}">
			<li><a href="${pageContext.request.contextPath}/aaa/popups/help.jsf?pageId=${pageId}&amp;pageName=${fn:replace(pageName,' ','%20')}" title="Help">Help</a></li>
		</c:when>
		<c:otherwise>
			<li><a href="${pageContext.request.contextPath}/aaa/popups/help_index.jsf" title="Help">Help</a></li>
		</c:otherwise>
	</c:choose>
	<%-- <li><a href="<%= System.getProperty("ebooks.context.path") %>/for_librarians.jsf" target="_blank" title="For Librarians">For Librarians</a></li>--%>
<%-- ORIGINAL FOR LIBRARIANS --%>
		<li>
			<c:set var="cjoUrl" value='<%= System.getProperty("cjo.url")%>' />
			<c:set var="pageId" value='<%= System.getProperty("librarian.pageId") %>' />
			<c:set var="athensId" value='<%= session.getAttribute("athensId") %>' />
			<c:url value="${pageScope.cjoUrl}accmanagement" var="forLibrariansUrl">
				<c:param name="id" value="${userInfo.encryptedDetails}" />
				<c:param name="topage" value="stream" />
				<c:param name="pageId" value="${pageScope.pageId}" />
				<c:param name="ebooksAthensId" value="${pageScope.athensId}" />
				<c:param name="ebooksShibbId" value="${shibbId}" />
				<c:param name="IPmembership" value="${IPmembership}" />
				<c:param name="hasIPMembershipLogo" value="${hasIPMembershipLogo}" />
				<c:param name="IPmembershipLogo" value="${IPmembershipLogo}" />
				<c:param name="IPorgNames" value="${orgNames}" />
			</c:url>
			<a href="<c:out value="${forLibrariansUrl}" escapeXml="true" />" target="_blank" >For Librarians</a>
		</li>
		
		<!-- 
		<c:if test="${null ne orgConLinkedMap.userLogin.userType}">
			<c:choose>
				<c:when test="${'AO' eq orgConLinkedMap.userLogin.userType}">
					<c:url value="${pageContext.request.contextPath}/cjoc/action/ebooks/accmanagement" var="accountAdminUrl">
						<c:param name="id" value="${userInfo.encryptedDetails}" />
						<c:param name="topage" value="configureIPDomain" />
						<c:param name="pageId" value="${pageScope.pageId}" />
						<c:param name="ebooksAthensId" value="${pageScope.athensId}" />
						<c:param name="ebooksShibbId" value="${shibbId}" />
					</c:url>
				</c:when>
				<c:when test="${'AC' eq orgConLinkedMap.userLogin.userType}">
					<c:url value="${pageContext.request.contextPath}/cjoc/action/ebooks/accmanagement" var="accountAdminUrl">
						<c:param name="id" value="${userInfo.encryptedDetails}" />
						<c:param name="topage" value="configureIPConsortiaDispatcher" />
						<c:param name="pageId" value="${pageScope.pageId}" />
						<c:param name="ebooksAthensId" value="${pageScope.athensId}" />
						<c:param name="ebooksShibbId" value="${shibbId}" />
					</c:url>
				</c:when>
			</c:choose>
			
			<li>
				<a href="<c:out value="${accountAdminUrl}" escapeXml="true" />" target="_blank">Account Administrator</a>
			</li>
		</c:if>	
		-->
</ul>
