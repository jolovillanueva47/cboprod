<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:choose>
	<c:when test="${fn:contains(pageContext.request.requestURL, 'subject_landing.jsp')}">
		<c:set var="bean" value="${subjectBean}" />
	</c:when>
	<c:otherwise>
		<c:set var="bean" value="${seriesBean}" />
	</c:otherwise>
</c:choose>

<c:if test="${bean.resultsFound > 0}">
	<label for="results${param.bottom}">Results per page:</label>	
	<select id="results${param.bottom}" name="results" >
          	<c:choose><c:when test="${bean.resultsPerPage eq 50}"><option value="50" selected="selected">50</option></c:when>
          	<c:otherwise><option value="50">50</option></c:otherwise></c:choose>
          	<c:choose><c:when test="${bean.resultsPerPage eq 75}"><option value="75" selected="selected">75</option></c:when>
          	<c:otherwise><option value="75">75</option></c:otherwise></c:choose>
          	<c:choose><c:when test="${bean.resultsPerPage eq 100}"><option value="100" selected="selected">100</option></c:when>
          	<c:otherwise><option value="100">100</option></c:otherwise></c:choose>
	</select>
	<input type="submit" value="Apply" name="resultsPerPage" class="input_buttons"/>

	<label for="page${param.bottom}">Page ${bean.currentPage} of ${bean.pages} | Go to page:</label>

<%-- 
		<c:if test="${accBean.pageName eq 'subject_landing.jsf?'}">
		<div class="border_divider02">
			<label class="label_01">Sort by:</label>
        	<select class="sort_type" >
            	<option value="title_alphasort">Title</option>
            	<option value="author_name_alphasort">Author</option>
            	<option value="print_date">Print Publication Year</option>
            	<option value="online_date">Online Publication Date</option>
            </select>
		</div>
	</c:if>
--%>
	<c:choose>
		<c:when test="${accBean.pageName == 'all_series.jsf?'}">
			<c:url var="urlPagerAllSeries" value="all_series.jsf">
				<c:param name="firstLetter" value="${bean.firstLetter}" />
			</c:url>
		</c:when>
		<c:when test="${accBean.pageName == 'series_landing.jsf?'}">
			<c:url var="urlPagerSeriesLanding" value="series_landing.jsf">
				<c:param name="seriesCode" value="${bean.seriesCode}" />
				<c:param name="seriesTitle" value="${bean.series}" />
			</c:url>
		</c:when>
		<c:when test="${accBean.pageName == 'subject_landing.jsf?'}">
			<c:url var="urlPagerSubjectLanding" value="subject_landing.jsf">
				<c:choose>
					<c:when test="${not empty subjectBean.subjectCode}">
						<c:param name="subjectCode" value="${subjectBean.subjectCode}" />
					</c:when>
					<c:otherwise>
						<c:param name="subjectId" value="${subjectBean.subjectId}" />
					</c:otherwise>
				</c:choose>
				<c:param name="subjectName" value="${bean.subject}" />
			</c:url>
		</c:when>
	</c:choose>

    <input type="text" name="page" id="page${param.bottom}" size="3" class="input_text"/>   
	<input type="submit" value="Go" name="gotopage" class="input_buttons"/>
	Go to:  
	<c:choose>
		<c:when test="${bean.hasFirst}">
			<c:choose>
				<c:when test="${accBean.pageName == 'all_series.jsf?'}">
					<a href="<c:out value="${urlPagerAllSeries}" />&amp;searchType=page&amp;page=1"  >First</a>
				</c:when>
				<c:when test="${accBean.pageName == 'series_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSeriesLanding}" />&amp;searchType=page&amp;page=1" >First</a>
				</c:when>
				<c:when test="${accBean.pageName == 'subject_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSubjectLanding}" />&amp;searchType=page&amp;page=1" >First</a>
				</c:when>
			</c:choose>
		</c:when>
		<c:otherwise>
			No First Page
		</c:otherwise>
	</c:choose> | 
	<c:choose>
		<c:when test="${bean.hasPrevious}">
			<c:choose>
				<c:when test="${accBean.pageName == 'all_series.jsf?'}">
					<a href="<c:out value="${urlPagerAllSeries}" />&amp;searchType=page&amp;page=${bean.currentPage-1}">Previous</a>
				</c:when>
				<c:when test="${accBean.pageName == 'series_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSeriesLanding}" />&amp;searchType=page&amp;page=${bean.currentPage-1}">Previous</a>
				</c:when>
				<c:when test="${accBean.pageName == 'subject_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSubjectLanding}" />&amp;searchType=page&amp;page=${bean.currentPage-1}" >Previous</a>						
				</c:when>	
			</c:choose>				
		</c:when>
		<c:otherwise>
			No Previous Page
		</c:otherwise>
	</c:choose> | 
	<c:choose>
		<c:when test="${bean.hasNext}">
			<c:choose>
				<c:when test="${accBean.pageName == 'all_series.jsf?'}">
					<a href="<c:out value="${urlPagerAllSeries}" />&amp;searchType=page&amp;page=${bean.currentPage+1}" >Next</a>
				</c:when>
				<c:when test="${accBean.pageName == 'series_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSeriesLanding}" />&amp;searchType=page&amp;page=${bean.currentPage+1}">Next</a>
				</c:when>
				<c:when test="${accBean.pageName == 'subject_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSubjectLanding}" />&amp;searchType=page&amp;page=${bean.currentPage+1}">Next</a>						
				</c:when>	
			</c:choose>				
		</c:when>
		<c:otherwise>
			No Next Page
		</c:otherwise>
	</c:choose> | 
	<c:choose>
		<c:when test="${bean.hasLast}">
			<c:choose>
				<c:when test="${accBean.pageName == 'all_series.jsf?'}">
					<a href="<c:out value="${urlPagerAllSeries}" />&amp;searchType=page&amp;page=${bean.pages}" >Last</a>					
				</c:when>
				<c:when test="${accBean.pageName == 'series_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSeriesLanding}" />&amp;searchType=page&amp;page=${bean.pages}" >Last</a>
				</c:when>
				<c:when test="${accBean.pageName == 'subject_landing.jsf?'}">
					<a href="<c:out value="${urlPagerSubjectLanding}" />&amp;searchType=page&amp;page=${bean.pages}" >Last</a>						
				</c:when>	
			</c:choose>								
		</c:when>
		<c:otherwise>
			No Last Page
		</c:otherwise>
	</c:choose>	
</c:if>

