<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	

<c:choose>
	<c:when test="${not empty param.goto}">
		<c:set var="bean" value="${searchResultBean}" />
	
		<c:choose>
			<c:when test="${param.goto eq 'yes'}">
				<strong>Page ${bean.currentPage} of ${bean.pages}:</strong>
			</c:when>
			<c:when test="${param.goto eq 'displaynoresults'}">
				<strong>No Results Found:</strong>
			</c:when>
		</c:choose>
		
		<c:url var="urlGoto" value="${domainCbo}search_aaa">
			<c:param name="searchType" value="page"/>
			<c:param name="resultsPerPage" value="${bean.resultsPerPage}"/>
			<c:param name="sortType" value="${not empty sorttype_aaa ? sorttype_aaa : 'score' }" />
		</c:url>
		
		<c:if test="${bean.resultsFound > 0}">	
			<div class="goto_container">
				<ul>
					<li><strong>Go to:</strong></li>
					<li><c:choose><c:when test="${bean.hasFirst}"><a title="First" href="<c:out value='${urlGoto}'/>&amp;currentPage=1">First</a></c:when>
						<c:otherwise>First</c:otherwise></c:choose>&nbsp;|&nbsp;</li>
					<li><c:choose><c:when test="${bean.hasPrevious}"><a title="Previous" href="<c:out value='${urlGoto}'/>&amp;currentPage=${bean.currentPage - 1}">Previous</a></c:when>
						<c:otherwise>Previous</c:otherwise></c:choose>&nbsp;|&nbsp;</li>
					<li><c:choose><c:when test="${bean.hasNext}"><a title="Next" href="<c:out value='${urlGoto}'/>&amp;currentPage=${bean.currentPage + 1}">Next</a></c:when>
						<c:otherwise>Next</c:otherwise></c:choose>&nbsp;|&nbsp;</li>
					<li><c:choose><c:when test="${bean.hasLast}"><a title="Last" href="<c:out value='${urlGoto}'/>&amp;currentPage=${bean.pages}">Last</a></c:when>
						<c:otherwise>Last</c:otherwise></c:choose></li>
				</ul>
			</div>     
			<div class="clear"></div>
		</c:if>  		
		
	</c:when>
	<c:otherwise>
		<form action="${domainCbo}search_aaa?searchType=page" method="post">
			<div class="search_results_control">
				<input type="hidden" name="searchType" value="page" />
				<c:set var="bean" value="${searchResultBean}" />
				<c:if test="${bean.resultsFound > 0}">
					<ul>
						<li><label for="page${param.bottom}">Go to page:</label></li>
					    <li class="input_text"><input type="text" name="currentPage" id="page${param.bottom}" style="width: 25px;"/></li>   
					    <li>of ${bean.pages}</li>
						<li class="input_button"><input type="submit" value="Go" /></li>
					</ul>
					<ul>
						<li><label for="results${param.bottom}">Results per page:</label></li>	
						<li class="input_select">
							<select id="results${param.bottom}" name="resultsPerPage" >
					          	<c:choose><c:when test="${bean.resultsPerPage eq 50}"><option value="50" selected="selected">50</option></c:when>
					          	<c:otherwise><option value="50">50</option></c:otherwise></c:choose>
					          	<c:choose><c:when test="${bean.resultsPerPage eq 75}"><option value="75" selected="selected">75</option></c:when>
					          	<c:otherwise><option value="75">75</option></c:otherwise></c:choose>
					          	<c:choose><c:when test="${bean.resultsPerPage eq 100}"><option value="100" selected="selected">100</option></c:when>
					          	<c:otherwise><option value="100">100</option></c:otherwise></c:choose>
							</select>
						</li>
						<li class="input_button"><input type="submit" value="Apply" /></li>
					</ul>
					<ul>
						<li><label for="sort${param.bottom}">Sort by:</label></li>
	     				
	     				<c:choose>
	     					<c:when test="${not empty param.sortType}">
	     						<c:set var="sorttype_aaa" value="${param.sortType}" scope="request" />
	     					</c:when>
	     					<c:otherwise>
	     						<c:set var="sorttype_aaa" value="score" scope="request" />
	     					</c:otherwise>
	     				</c:choose>
	
	     				<li class="input_select">
	     					<select id="sort${param.bottom}" name="sortType">
	     						<c:choose>
	     							<c:when test="${not empty requestScope.sorttype_aaa}" >
		     							<option <c:if test='${requestScope.sorttype_aaa eq "score"}'>selected="selected"</c:if> value="score">Relevancy</option>
		     							<option <c:if test='${requestScope.sorttype_aaa eq "main_title_alphasort"}'>selected="selected"</c:if> value="main_title_alphasort">Title</option>
		     							<option <c:if test='${requestScope.sorttype_aaa eq "author_alphasort"}'>selected="selected"</c:if> value="author_alphasort">Author</option>
		     							<option <c:if test='${requestScope.sorttype_aaa eq "print_date"}'>selected="selected"</c:if> value="print_date">Copyright</option>
		     							<option <c:if test='${requestScope.sorttype_aaa eq "online_date"}'>selected="selected"</c:if> value="online_date">Online Publication Date</option>	
	     							</c:when>
	     							<c:otherwise>
	     								<option selected="selected" value="score">Relevant</option>
			     						<option value="main_title_alphasort">Title</option>
			     						<option value="author_alphasort">Author</option>
			     						<option value="print_date">Copyright</option>
			     						<option value="online_date">Online Publication Date</option>
	     							</c:otherwise>
	     						</c:choose>
	         				</select>
	        			</li>
	        			<li class="input_button"><input type="submit" value="Sort" /></li>
					</ul>				
				</c:if>	
				<div class="clear"></div>  
			</div>
		</form>
	</c:otherwise>
</c:choose>



