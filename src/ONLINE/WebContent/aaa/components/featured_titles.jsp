<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<h3>Featured Titles</h3>
<ol>			
	<c:if test="${not empty topBooksBean.topFeaturedTitleList}">			
		<c:forEach var="book" items="${topBooksBean.topFeaturedTitleList}">				
			<li>						
				<a href="${pageContext.request.contextPath}/aaa/ebook.jsf?bid=CBO${book.isbn}">${book.title}</a>
			</li>				
		</c:forEach>			
	</c:if>			 
</ol>

