<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@page import="java.net.*" %>
<%@page import="java.util.Set" %>
<%@page import="java.io.File" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="o" %>

<o:url var="contextRoot" option="current_dns" />
<o:url var="contextRootCjo" option="context_root_cjo" append="action/ebooks/" />
<o:url var="contextRootSsl" option="current_dns_to_https" />

<h1><a href="${pageContext.request.contextPath}/aaa/home.jsf" class="logo_ebooks" title="Cambridge Books Online">Cambridge Books Online</a></h1>

	<!-- Start Organization Info -->
	<c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />
	<input id="ipLogoContextPath" type="hidden" value="${pageContext.request.contextPath}/" />
	
	<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">			
	    <div class="org_container link_03">
	    	<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
			<c:if test="${eBookLogoBean.hasLogo}">
				<img id="orgLogoImg" src='${contextRoot}organisation/logo/org${eBookLogoBean.orgLogoId}' width="85" height="35" alt=""/>	
			</c:if>
  			<ul>
  				<li class="orgname_container">
    				<c:choose>
						<c:when test="${eBookLogoBean.logoNameCount gt 1}">
							<strong>Member Organisations: </strong>
						</c:when>
						<c:otherwise>
							<strong>Member Organisation: </strong>
						</c:otherwise>
					</c:choose>
    				${eBookLogoBean.showLogoNamesUnderMax} 
   				</li>
   				<li>
   					<c:out value="${fn:replace(eBookLogoBean.textMessage,'null,','')}" escapeXml="false" /> 
    				<c:if test="${eBookLogoBean.logoNameCount gt 3 or not empty eBookLogoBean.textMessage}" >
    					<a href="${pageContext.request.contextPath}/aaa/popups/view_access.jsf">more details</a>
    				</c:if>  
   				</li>
  			</ul>
    	</div>
	</c:if>	
	<div class="clear"></div> 
	<div class="orgname_container">
		<c:choose>
			<c:when test="${eBookLogoBean.userLoggedIn}">
				<b>Welcome ${eBookLogoBean.orgConMap.userLogin.user.firstName}&nbsp;${eBookLogoBean.orgConMap.userLogin.user.lastName}</b>
			</c:when>
			<c:when test="${not empty eBookLogoBean.orgName}">	
				<c:choose>
					<c:when test="${fn:length(eBookLogoBean.orgName) > 18 }">
						<b>Welcome ${fn:substring(eBookLogoBean.orgName, 0, 18)}...</b>
					</c:when>
					<c:otherwise>
						<b>Welcome ${eBookLogoBean.orgName}</b>
					</c:otherwise>
				</c:choose>	
			</c:when>
			<c:otherwise>
				Welcome Guest
			</c:otherwise>
		</c:choose>
	</div>