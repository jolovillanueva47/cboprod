<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %> 

<ul>     
	<c:forEach var="nodes" items="${nodes_request}">
		
		<c:url var="urlViewAll" value="subject_landing.jsf"  >
			<c:param name="searchType" value="allSubjectBook"/>
			<c:param name="subjectId" value="${nodes.key.subjectId}"/>
			<c:param name="subjectName" value="${nodes.key.subjectName}"/>
		</c:url>
		<c:url var="urlRssFeeds" value="rsstree_aaa"  scope="page">
			<c:param name="rss_tree" value="true"/>
			<c:param name="xml" value="feed_${nodes.key.subjectId}_rss_2.0.xml"/>
		</c:url>
		<c:url var="urlSubjectLanding" value="subject_landing.jsf">
			<c:param name="searchType" value="allTitles"/>
			<c:param name="subjectCode" value="${nodes.key.vistaSubjectCode}"/>
			<c:param name="subjectName" value="${nodes.key.subjectName}"/>
			<c:param name="paths" value="${nodes.key.path}"/>
		</c:url>	
		
		<li>
			<c:set var="subjectLevel" value="${nodes.key.subjectLevel}" scope="page"/>
			<c:set var="subjectId" value="${nodes.key.subjectId}" scope="page"/>
			<%Integer subjectLevel = (Integer)pageContext.getAttribute("subjectLevel") + 1;
			  String subjectId = (String)pageContext.getAttribute("subjectId");
			%>
			<%out.println("<h"+subjectLevel+" id =\""+subjectId+"\">"); %>
			<c:choose>
				<c:when test="${nodes.key.finalLevel == 'N'}">
					${nodes.key.subjectName} - <a href="<c:out value='${urlViewAll}' escapeXml='true'/>">View All</a>
				</c:when>
				<c:when test="${nodes.key.finalLevel == 'Y'}">
					<a href="<c:out value='${urlSubjectLanding}' escapeXml='true'/>" ><c:out value="${nodes.key.subjectName}" escapeXml="true" /></a>
				</c:when>
			</c:choose>
			
			<%out.println("</h"+subjectLevel+">"); %> 
			 
			<c:set var="nodes_request" value="${nodes.value}" scope="request"/>	
			<c:set var= "feedsBean" value="${feedsBean}" scope="request"/>
			<c:if test="${nodes.key.finalLevel != 'Y'}">
				<c:catch>
					<jsp:include page="../../aaa/components/subject_tree_node.jsp"/>
				</c:catch>
			</c:if>
		</li>
	</c:forEach>
</ul>  
  

  