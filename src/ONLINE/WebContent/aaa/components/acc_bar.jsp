<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<% 
	String userAgent = request.getHeader("User-Agent"); 
	String s = "";
	float userAgentVersion = -1;
	if(userAgent.indexOf("MSIE") > -1) {
		s =  userAgent.substring(userAgent.indexOf("MSIE") + 4);
		s = s.replace(" ", "");
		userAgentVersion = Float.parseFloat(s.substring(0, s.indexOf(";")));
	}
	
%>

<c:if test="${not empty param.stylez}">
	<c:set var="sessionStyle" scope="session" value="${param.stylez}" />
</c:if>
<c:if test="${not empty param.JSESS}">
	<img style="display: none" src="<%= System.getProperty("cjo.alias") %>cbo/cboImageLogout" />
</c:if>
<c:choose>
	<c:when test="${sessionStyle == null}">
		<link href="<%= System.getProperty("ebooks.context.path") %>aaa/css/default_style.css" rel="stylesheet" type="text/css" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" type="text/css" href="<%= System.getProperty("ebooks.context.path") %>aaa/css/${sessionStyle}.css" />
	</c:otherwise>
</c:choose>
<c:if test='<%= userAgent.indexOf("MSIE") > -1 && userAgentVersion > -1.0 && userAgentVersion <= 9.0 %>'>
	<link href="<%= System.getProperty("ebooks.context.path") %>aaa/css/ie.css" rel="stylesheet" type="text/css" />
</c:if>



<!-- open search -->
<link rel="search" href="<%= System.getProperty("ebooks.context.path") %>widget?cmd=openSearch&amp;xml=aaa/cboQuickSearch_aaa.xml&amp;servlet=<%= System.getProperty("ebooks.context.path") %>search_aaa?" 
     type="application/opensearchdescription+xml" 
     title="(Accessible Version) Cambridge Books Online" />
