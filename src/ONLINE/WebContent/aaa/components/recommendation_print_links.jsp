<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c"%>			
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	


<c:set var="hasAccess" value="${'F' eq bookBean.accessType or 'S' eq bookBean.accessType}" scope="page" />
<c:url value='popups/recommendToLibrarian.jsf' var="urlRecommend">
	<c:param name="Title" value="${bookBean.bookMetaData.title}" />
	<c:param name="Copyright" value="${bookBean.bookMetaData.copyrightStatement}" />
	<c:param name="PrintpubDate" value="${bookBean.bookMetaData.printDate}" />
	<c:param name="OnlinepubDate" value="${bookBean.bookMetaData.onlineDate}" />
	<c:param name="OnlineIsbn" value="${bookBean.bookMetaData.onlineIsbn}" />
	<c:param name="HardbackIsbn" value="${bookBean.bookMetaData.hardbackIsbn}" />
	<c:param name="Doi" value="${bookBean.bookMetaData.doi}" />
	<c:param name="TB_iframe" value="true" />
	<c:param name="width" value="620" />
	<c:param name="height" value="450" />
	<c:param name="bookId" value="${bookBean.bookMetaData.id}" />
</c:url>

<!-- Recommendation/Print Links Start -->
<ul class="link_02">		
	<c:if test="${not hasAccess}">
		<li>
			<!-- See book_info_links.jsp for session setup of authorList -->					
		 	<c:choose>
			 	<c:when test="${supplemental.salesModuleFlag != 'N'}">	
			 		<a href="<c:out value='${urlRecommend}' escapeXml='true' />" ><span class="icons_img recommend">&nbsp;</span>
						<strong>Recommend This to a Librarian</strong>
					</a>														 										 									
			 	</c:when>
		 	</c:choose>
		</li>            
	</c:if>	
	<li style="list-style: none; display: inline"><div class="clear"></div></li>
	<!-- <span class="icons_img print">&nbsp;</span> -->  	
	<li><a href="#" onclick="javascript:window.print(); return false;"><span class="icons_img print">&nbsp;</span><strong>Print This Page</strong></a></li>


	<c:choose>
		<c:when test="${not empty sessionScope.hithighlight}">
			<c:choose>
				<c:when test="${param.hithighlight_aaa eq 'on'}">
					<c:set var="hithighlight" value="on" scope="session"/>
				</c:when>
				<c:when test="${param.hithighlight_aaa eq 'off'}">
					<c:remove var="hithighlight"scope="session"/>
					<c:set var="hithighlight" value="off" scope="session"/>	
				</c:when>
			</c:choose>

			

			<c:choose>
				<c:when test="${sessionScope.hithighlight eq 'on'}">
					<c:set var="accBean_param" value="&hithighlight_aaa=off#contentTab" scope="request" />
					<li><a id="hithighlight" href="${accBean.pagePath}${accBean.queryStringNoKeyword}"><span class="icons_img highlight_text">&nbsp;</span><strong>Turn Off Hit Highlight</strong></a></li>
				</c:when>
				<c:when test="${sessionScope.hithighlight eq 'off'}">
					<c:set var="accBean_param" value="&hithighlight_aaa=on#contentTab" scope="request" />
					<li><a id="hithighlight" href="${accBean.pagePath}${accBean.queryStringNoKeyword}"><span class="icons_img highlight_text">&nbsp;</span><strong>Turn On Hit Highlight</strong></a></li>
				</c:when>
			</c:choose>
		</c:when>
	</c:choose>

</ul>
<!-- Recommendation/Print Links End -->