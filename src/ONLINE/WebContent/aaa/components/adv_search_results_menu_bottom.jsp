<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.rss.RssServlet"%>



<!-- Search results menu start -->	
<div class="bar_container link_02">
	
	<!-- Form Start -->
	<form action="search_aaa?searchType=page&lastpage=${searcherMap['pager'].lastPage}" method="post">	
	
		<!-- Results per page start -->
	    <label for="results_bottom"> Results per page:</label>		
		<select id="results_bottom" name="results" >
			<c:choose><c:when test="${searcherMap['pager'].resultsPerPage eq 50}"><option value="50" selected="selected">50</option></c:when>
			<c:otherwise><option value="50">50</option></c:otherwise></c:choose>
			<c:choose><c:when test="${searcherMap['pager'].resultsPerPage eq 75}"><option value="75" selected="selected">75</option></c:when>
			<c:otherwise><option value="75">75</option></c:otherwise></c:choose>
			<c:choose><c:when test="${searcherMap['pager'].resultsPerPage eq 100}"><option value="100" selected="selected">100</option></c:when>
			<c:otherwise><option value="100">100</option></c:otherwise></c:choose>
		</select>
			
		<input type="submit" value="Apply" id="resultsperpage" name="resultsperpage" class="input_buttons"/>
		<!-- Results per page end -->
	
		<!--  Goto page Start -->
			<label for="page_bottom" >Page ${searcherMap['pager'].caption} | Go to page:</label>					        
			<input type="text" id="page_bottom" name="page" size="3" class="input_text"/>
			<input type="submit" value="Go" id="gotopage" name="gotopage" class="input_buttons"/>
		<!--  Goto page End -->
			
		<!-- Sort by start -->
		<label for="sort_bottom">Sort by:</label>
			<select id="sort_bottom" name="sort" class="select_sort_by">
				<c:choose><c:when test="${searcherMap['sort'] eq 'Relevancy'}"><option selected="selected">Relevancy</option></c:when>
				<c:otherwise><option>Relevancy</option></c:otherwise>
				</c:choose>
				<c:choose><c:when test="${searcherMap['sort'] eq 'Title'}"><option selected="selected">Title</option></c:when>
				<c:otherwise><option>Title</option></c:otherwise>
				</c:choose>
				<c:choose><c:when test="${searcherMap['sort'] eq 'Author'}"><option selected="selected">Author</option></c:when>
				<c:otherwise><option>Author</option></c:otherwise>
				</c:choose>
				<c:choose><c:when test="${searcherMap['sort'] eq 'Print Publication Year'}"><option selected="selected">Print Publication Year</option></c:when>
				<c:otherwise><option>Print Publication Year</option></c:otherwise>
				</c:choose>
				<c:choose><c:when test="${searcherMap['sort'] eq 'Online Publication Date'}"><option selected="selected">Online Publication Date</option></c:when>
				<c:otherwise><option>Online Publication Date</option></c:otherwise>
				</c:choose>
		    </select>
		<input type="submit" value="Sort Results" id="sortresults" name="sortresults" class="input_buttons"/>
		<!-- Sort by end -->
		
		<!-- Pager start -->	
		<c:if test="${searcherMap['pager'].resultSize > 0}">		                                	
			Go to:
			<c:choose>
				<c:when test="${searcherMap['pager'].hasFirst}">
					<a href="search_aaa?searchType=page&page=1&gotoLink=yes" name="gotoLink" class="goToPage link03">First</a>
				</c:when>
				<c:otherwise>
					First
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${searcherMap['pager'].hasPrevious}">
					<a href="search_aaa?searchType=page&page=${searcherMap['pager'].currentPage-1}&gotoLink=yes" name="gotoLink" class="goToPage link03">Previous</a>
				</c:when>
				<c:otherwise>
					Previous
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${searcherMap['pager'].hasNext}">
					<a href="search_aaa?searchType=page&page=${searcherMap['pager'].currentPage+1}&gotoLink=yes" name="gotoLink" class="goToPage link03">Next</a>
				</c:when>
				<c:otherwise>
					Next
				</c:otherwise>
			</c:choose> | 
			<c:choose>
				<c:when test="${searcherMap['pager'].hasLast}">
					<a href="search_aaa?searchType=page&page=${searcherMap['pager'].lastPage}&gotoLink=yes" name="gotoLink" class="goToPage link03">Last</a>
				</c:when>
				<c:otherwise>
					Last
				</c:otherwise>
			</c:choose>
		</c:if>
		<!-- Pager end -->		

	</form>
	<!-- Form End -->
	
</div>					
<!-- Search results menu end -->	
	