<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %> 


<%@page import="org.cambridge.ebooks.online.subjecttree.SeriesWorker_aaa"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty nodes_request}">
	<ul>     
		<c:forEach var="nodes" items="${nodes_request}">
			<c:set var="subjectLevel" value="${nodes.key.subjectLevel}" scope="page"/>
			<c:set var="subjectId" value="${nodes.key.subjectId}" scope="page"/>
			<c:url var="urlViewAll" value="subject_landing.jsf"  >
				<c:param name="searchType" value="allSubjectBook"/>
				<c:param name="subjectId" value="${nodes.key.subjectId}"/>
				<c:param name="subjectName" value="${nodes.key.subjectName}"/>
			</c:url>
			<c:url var="urlViewAll_series" value="all_series.jsf"  >
				<c:param name="searchType" value="allSeriesSubLevel"/>
				<c:param name="seriesCode" value="${nodes.key.subjectId}"/>
				<c:param name="firstLetter" value="${nodes.key.subjectName}"/>
				<c:param name="seriesName" value="${nodes.key.subjectName}"/>
				
			</c:url>
			<c:url var="urlSubjectLanding" value="subject_landing.jsf">
				<c:param name="searchType" value="allTitles"/>
				<c:param name="subjectCode" value="${nodes.key.vistaSubjectCode}"/>
				<c:param name="subjectName" value="${nodes.key.subjectName}"/>
				<c:param name="paths" value="${nodes.key.path}"/>
			</c:url>

			<%
				Integer subjectLevel = (Integer)pageContext.getAttribute("subjectLevel") + 1;
				String subjectId = (String)pageContext.getAttribute("subjectId");
			%>
				
			<c:choose>
				<c:when test="${nodes.key.finalLevel == 'N'}" >
					<li>
						<%out.println("<h"+subjectLevel+" id =\""+subjectId+"\">"); %>
							${nodes.key.subjectName} - <a href="<c:out value='${urlViewAll}' escapeXml='true'/>">View All</a>
						<%out.println("</h"+subjectLevel+">"); %>	
						
								<c:set var="nodes_request" value="${nodes.value}" scope="request"/>	
								<c:set var="nodes_request" value="${nodes.value}" scope="request"/>	
								
								<c:catch>
									<jsp:include page="../../aaa/components/series_tree_node.jsp"/>
								</c:catch>
						
					</li>
				</c:when>
				<c:when test="${nodes.key.finalLevel == 'Y'}">
					<c:set var="seriesVistaSubjectCode" value="${nodes.key.vistaSubjectCode}" scope="page"/>	
					<%
						String seriesVistaSubjectCode = (String)pageContext.getAttribute("seriesVistaSubjectCode");
					%>
					<c:set var="seriesObj_aaa" value="<%=SeriesWorker_aaa.getSeriesObjects_fromStaticVariable(seriesVistaSubjectCode) %>" scope="page"/>
					
					<c:if test="${seriesObj_aaa!=null && not empty seriesObj_aaa }">
						<li>
							<%out.println("<h"+subjectLevel+" id =\""+subjectId+"\">"); %>
								${nodes.key.subjectName} - <a href="<c:out value='${urlViewAll_series}' escapeXml='true'/>">View All</a>								
							<%out.println("</h"+subjectLevel+">"); %>	
									<ul>
										<c:forEach var="seriesObject" items="${seriesObj_aaa}" >
											<c:url var="urlSeriesLanding" value="../aaa/series_landing.jsf">
												<c:param name="seriesCode" value="${seriesObject.seriesCode}"/>
												<c:param name="seriesTitle" value="${seriesObject.seriesName}"/>
												<c:param name="paths" value="${nodes.key.path}"/>
											</c:url>	
											<li><a href="<c:out value='${urlSeriesLanding}' escapeXml='true'/>">${seriesObject.seriesName}</a></li>
										</c:forEach>
									</ul>
						</li>
					</c:if>
				</c:when>
			</c:choose>
		</c:forEach>
	</ul>  
</c:if>