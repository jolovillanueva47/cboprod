<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

	<ul>
    	<c:forEach var="alphaMap" items="${seriesBean.alphaMap}">    	
	    	<c:choose>
	    		<c:when test="${alphaMap.value eq 'link'}">
	    			<c:url var="urlAlphaMap" value="${all_series.jsf}" >
	    				<c:param name="firstLetter" value="${alphaMap.key}"/>
	    				<c:param name="seriesCode" value="${seriesBean.seriesCode}"/>
	    				<c:param name="seriesName" value="${seriesBean.series}"/>
	    			</c:url>
	    			<li><a href="<c:out value="${urlAlphaMap}" />" class="link02">${alphaMap.key}</a></li>
	    		</c:when>
	    		<c:otherwise>
	    			<li>${alphaMap.key}</li>
	    		</c:otherwise>
	    	</c:choose>
		</c:forEach>
		<li>
			<c:set var="seriesCode" value="${fn:substring(seriesBean.seriesCode,0,1)}" />
	    	<c:choose>
	    	 	<c:when test="${seriesCode eq 'A' or seriesCode eq 'B' or seriesCode eq 'C' or seriesCode eq 'D'}">
	    	 		<c:set var="seriesName" value="${seriesBean.series}"/>
	    	 		<c:set var="seriesNameCut" value="${fn:substring(seriesName,0,8)}..."/>
	    	 		<c:set var="seriesName" value="${fn:length(seriesName) > 11 ? seriesNameCut : seriesName}"/>
	    	 		<c:url var="urlAllSeriesAlphalist" value="all_series.jsf">
	    	 			<c:param name="searchType" value="allSubjectBook"/>
	    	 			<c:param name="seriesCode" value="${seriesBean.seriesCode}"/>
	    	 			<c:param name="firstLetter" value="${seriesBean.series}"/>
	    	 			<c:param name="seriesName" value="${seriesBean.series}"/>
	    	 		</c:url>
	    	 		
	    	 		<a href="<c:out value="${urlAllSeriesAlphalist}" />" class="link02">All ${seriesName}</a>
	    	 	</c:when>	
				<c:otherwise>
					<a href="all_series.jsf?searchType=allSeries" class="link02">All Series</a>
				</c:otherwise>
			</c:choose>
		</li>   
	</ul>