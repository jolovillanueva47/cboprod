<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<h3>Top Accessed Books</h3>
<c:if test="${topBooksBean.initTopBooksSeries != null}"/>
	<ol>
		<c:if test="${topBooksBean.topBooksList != null}">
			<c:forEach var="book" items="${topBooksBean.topBooksList}">
				<li><a href="../aaa/ebook.jsf?bid=CBO${book.isbn}">${book.title}</a></li>
			</c:forEach>
		</c:if>
	</ol>

	<br/>
	
	<h4>Featured Collections</h4>
	<c:if test="${not empty topBooksBean.topFeaturedCollectionsList}">
		<ol>
			<c:forEach var="coll" items="${topBooksBean.topFeaturedCollectionsList}">
					<c:url value="../aaa/collection_landing.jsf" var="collection">
						<c:param name="results" value="50" />
						<c:param name="sort" value="title_alphasort" />
						<c:param name="collectionId" value="${coll.collectionId}" />
						<c:param name="collectionName" value="${coll.collectionName}" />
					</c:url>			
					<li><a href="<c:out value='${collection}'/>">${empty coll.collectionName ? coll.collectionId : coll.collectionName }</a></li>
			</c:forEach>
		</ol>
	</c:if>
<%-- 
	<!-- Title Banner Start -->
	<h4>Top Searched Series</h4>
   <!--  <div class="titlebanner_topsearch"></div> -->
    <!-- Title Banner End -->
	    <ol>
			<c:if test="${not empty topBooksBean.topSeriesList}">
				<c:forEach var="series" items="${topBooksBean.topSeriesList}">				
					<c:choose>
					<c:when test="${not empty series.seriesNumber && series.seriesNumber ne '0'}">	
						<c:url value="../aaa/series_landing.jsf" var="seriesSortByVolume">
							<c:param name="seriesCode" value="${series.seriesCode}" />
							<c:param name="seriesTitle" value="${series.seriesLegend}" />
							<c:param name="sort" value="series_number" />
						</c:url>				
						<li><a href="<c:out value="${seriesSortByVolume}"/>">${series.seriesLegend}</a></li>					
					</c:when>
					<c:otherwise>
						<c:url value="../aaa/series_landing.jsf" var="seriesSortByPrintDate">
							<c:param name="seriesCode" value="${series.seriesCode}" />
							<c:param name="seriesTitle" value="${series.seriesLegend}" />
							<c:param name="sort" value="print_date" />
						</c:url>
						<li><a href="<c:out value="${seriesSortByPrintDate}"/>">${series.seriesLegend}</a></li>					
					</c:otherwise>
					</c:choose>		
					
				</c:forEach>
			</c:if>
	    </ol>
    
    <div class="fl"><span class="icons_img arrow_02">&nbsp;</span><a href="series.jsf">Browse All Series</a></div>
    --%>