<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<jsp:useBean id="advSearchBean" class="org.cambridge.ebooks.online.search.AdvanceSearchBean" scope="request"/>  

<c:if test="${not empty param.newSearch}">
	<c:set var="param_target" value="${paramValues.target}" scope="session"/>
	<c:set var="param_condition" value="${paramValues.condition}" scope="session"/>
	<c:set var="param_search_word" value="${not empty param.searchText ? param.searchText : paramValues.search_word}" scope="session"/>
	<c:set var="param_search_word_2" value="${paramValues.search_word_2}" scope="session"/>
	<c:set var="param_logical_op" value="${paramValues.logical_op}" scope="session"/>
	<c:set var="param_sfilter" value="${paramValues.sfilter}" scope="session"/>
</c:if>

<c:set var="param_target" value="${sessionScope.param_target}" scope="request"/>
<c:set var="param_condition" value="${sesssionScope.param_condition}" scope="request"/>
<c:set var="param_search_word" value="${sesssionScope.param_search_word}" scope="request"/>
<c:set var="param_search_word_2" value="${sesssionScope.param_search_word_2}" scope="request"/>
<c:set var="param_logical_op" value="${sesssionScope.param_logical_op}" scope="request"/>
<c:set var="param_sfilter" value="${fn:join(sessionScope.param_sfilter,',')}" scope="request"/>

<input type="hidden" name="newSearch" value="newSearch"/>

<c:forEach var="option" items="${advSearchBean.mapSFilter}">
	<input id="sfilter${option.key}" name="sfilter" type="checkbox" value="${option.value}" <c:if test="${fn:contains(param_sfilter, option.value)}" >checked="checked"</c:if> /><label for="sfilter${option.key}">${option.key}</label>
</c:forEach>

<div class="clear_bottom"></div>

<c:forEach begin="1" end="8" varStatus="status">
	<c:set var="index" value="${status.count}" />
	<c:set var="array_index" value="${status.count-1}" />
	
	<c:choose>
		<c:when test="${index > 6}">
			<select id="target_${index}" name="target" title="Search criteria one" >
				<c:forEach var="option" items="${advSearchBean.mapTarget2}">
					<option <c:if test='${param_target[array_index] eq option.value}'>selected="selected"</c:if> value="${option.value}">${option.key}</option>
				</c:forEach>
			</select>
			<select id="condition_${index}" name="condition" title="Search criteria two">
				<c:forEach var="option" items="${advSearchBean.mapCondition2}">
					<option <c:if test='${param_condition[array_index] eq option.value}'>selected="selected"</c:if> value="${option.value}" >${option.key}</option>
				</c:forEach>
			</select>
		</c:when>
		<c:otherwise>
			<select id="target_${index}" name="target" title="Search criteria one" >
				<c:forEach var="option" items="${advSearchBean.mapTarget}">
					<option <c:if test='${param_target[array_index] eq option.value}'>selected="selected"</c:if> value="${option.value}">${option.key}</option>
				</c:forEach>
			</select>
			
			<select id="condition_${index}" name="condition" title="Search criteria two">
				<c:forEach var="option" items="${advSearchBean.mapCondition}">
					<option <c:if test='${param_condition[array_index] eq option.value}'>selected="selected"</c:if> value="${option.value}">${option.key}</option>
				</c:forEach>
			</select>
		</c:otherwise>
	</c:choose>

	<input name="search_word" type="text" title="Search word one" class="input_text" value="${fn:length(param_search_word) ne 8 ? index eq 1 ? param_search_word : '' : param_search_word[array_index]}" />
	
	<c:choose>
		<c:when test="${index eq 7 || index eq 8}">
			<label for="search_word_2_${index}">and/to&nbsp;</label>
			<input id="search_word_2_${index}" name="search_word_2" type="text" title="Search word two" class="input_text"  value="${param_search_word_2[array_index]}"/>
		</c:when>
		<c:otherwise>
			<input id="search_word_2_${index}" name="search_word_2" type="hidden" title="Search word two" class="input_text" value="${param_search_word_2[array_index]}" />
		</c:otherwise>
	</c:choose>
	
	<c:if test="${index ne 8}">
		<select id="logical_op_${index}" name="logical_op" title="Logical Operator">
			<c:forEach var="option" items="${advSearchBean.mapAndOr}">
				<option <c:if test='${param_logical_op[array_index] eq option.value}'>selected="selected"</c:if> value="${option.value}">${option.key}</option>
			</c:forEach>
		</select>
	</c:if>
	<div class="clear_bottom"></div>
</c:forEach>


