<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page import="org.cambridge.ebooks.online.subscription.SubscriptionServlet"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Organisational Access - Cambridge Books Online - Cambridge University Press</title>
	
	<jsp:include page="/aaa/components/acc_bar.jsp"></jsp:include>
</head>

<body>
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="/aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="/aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="/aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="/aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="/aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
 	    	
	    	<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
	      	
        </div>
        <!-- End Middle Content --> 
                    
 		<a id="maincontent" name="maincontent" ></a> 
 		             
		<!-- Start Body Content -->
		<div class="content_container link_02">
		     
        	${accessDetails.initBean}

			<!-- Titlepage Start -->
			<h2>Organisational Access Details</h2>   
			<!-- Titlepage End -->
			
			<div class="clear"></div>
           
         	<p>Below is a list of Cambridge Books Online titles to which your organisation has access.</p>
            
       		<table cellspacing="0" width="100%" class="global_forms input_text buttons">
                <tbody>
                   	<!-- displays the orgList -->
					<c:if test="${not empty accessDetails.orgList}" >
						<tr>
							<td>
								<form action="${pageContext.request.contextPath}/aaa/ebooks/subscriptionServlet_aaa" method="get">
									<input type="hidden" name="ACTION" value="ALL"/>
									<label for="orgList">Organisational name:</label>
									<select class="select_02" name="SELECTED_ORG" >            
										<c:forEach items="${accessDetails.orgList}" var="org">
											<c:choose>
												<c:when test="${org.bodyId eq param.SELECTED_ORG}">
													<option value="${org.bodyId}" selected="selected">${org.name}</option>
												</c:when>
												<c:otherwise>
													<option value="${org.bodyId}">${org.name}</option>
												</c:otherwise>					
											</c:choose>
										</c:forEach>
									</select>
									&nbsp;
									<label for="sortList">Sort By:</label>
									&nbsp;<select name="sortBy" id="sortBy" class="select_02">						
												<option value="1" selected="selected">Title</option>
												<option value="2">ISBN</option>
											  </select>
									&nbsp;
									<input type="submit" value="Submit" class="input_buttons" name="submit_org"/>
								</form>
							</td>
						</tr>
	
					</c:if>
					<!-- displays the orgList -->
					
					<c:set var="indi_servlet" value="${not empty requestScope.indi_servlet or not empty param.submit_org ? requestScope.indi_servlet : sessionScope.indi_servlet }" scope="page"/>
					<c:set var="coll_servlet" value="${not empty requestScope.coll_servlet or not empty param.submit_org ? requestScope.coll_servlet : sessionScope.coll_servlet }" scope="page"/>	
					
                  	<tr>
	                  	<td>
							<!-- Individual Titles Start -->
							<c:if test="${not empty indi_servlet}">
								<c:set var="indi_servlet" value="${indi_servlet}" scope="session"/>
								
								<h2>�&nbsp;Individual Titles</h2>       	
								<ul>
									<c:forEach var="indi_" items="${indi_servlet}">
										<li><a href="${pageContext.request.contextPath}/aaa/ebook.jsf?bid=${indi_.bookId}"><c:out value="${fn:replace(indi_.bookId, 'CBO', '')}"/></a>
											<ul>
												<li><a href="${pageContext.request.contextPath}/aaa/ebook.jsf?bid=${indi_.bookId}">${indi_.title}</a></li>
											</ul>
										</li>
									</c:forEach>
								</ul>
							</c:if>
							<!-- Individual Titles End -->    
							
							<!-- Collections Start -->
							<c:if test="${not empty coll_servlet}">
								<c:set var="coll_servlet" value="${coll_servlet}" scope="session"/>
								
								<h2>�&nbsp;Collections</h2>
								<ul>	
								   	<c:forEach var="coll" items="${coll_servlet}" varStatus="x">
										<c:set var="id" value="${coll.collectionId}_o${x.count - 1}" scope="page"/>
										
										<li>	
											<strong>${coll.title}</strong>
											<br/>${coll.description}
											<ul>
												<c:import url="${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=FIND_BOOKS&sortBy=${param.sortBy}&COLLECTION_ID=${coll.collectionId}" scope="page" charEncoding="UTF-8" var="importedItems">
													<c:param name="aaa_import"></c:param>
												</c:import>
												<c:forEach items="${fn:split(importedItems, ';')}" var="i" >
													<li>
														<c:forEach items="${fn:split(i, ',')}" var="bookItem" varStatus="c">
															<c:if test="${c.count eq 1}">
																<c:set var="bid_aaa" value="${bookItem}"/>
															</c:if>
															<c:if test="${c.count eq 2}">
																<a href="${pageContext.request.contextPath}/aaa/ebook.jsf?bid=${bid_aaa}">${bookItem}</a>
															</c:if>
															<c:if test="${c.count eq 3}">
																<ul>
																	<li><a href="${pageContext.request.contextPath}/aaa/ebook.jsf?bid=${bid_aaa}">${bookItem}</a></li>
																</ul>
															</c:if>
														</c:forEach>
													</li>
												</c:forEach>
											</ul>		
										</li>
									</c:forEach>
								</ul> 
							</c:if>
							<!-- Collections End -->								
								
                          	<c:choose>
								<c:when test="${accessDetails.freeTrial}">
									<h2>Free Trial Active</h2>
								</c:when>
								<c:otherwise>
									<h2>Free Trial Inactive</h2>
								</c:otherwise>
							</c:choose>
	                	</td>
                  	</tr>  
                  	
				</tbody>
			</table>
                  
			<div class="clear_bottom"></div>
   
            <!-- Footermenu Start -->
        	<jsp:include page="/aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="/aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery/jquery-1.3.2.min.js"></script>
	<script type="text/javascript">		
		var selectedOrg = "${param.SELECTED_ORG}";
			
		$(function(){
			
			if(2 == ${param.sortBy}) {
				$("#sortBy option[value='2']").attr("selected", true);
			} else {
				$("#sortBy option[value='1']").attr("selected", true);
			}
		});
	</script>
</body>

</html>