<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<%@page import="org.cambridge.ebooks.online.subjecttree.SubjectWorker_aaa"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Browse By Subject - Cambridge Books Online - Cambridge University Press</title>    
    
	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include> 
</head>

<body>
		
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1539" scope="session" />
	<c:set var="pageName" value="Browse By Subject" scope="session" />

	<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
            	<div class="topheader_divider">                                                    
                	<!-- Start Accessibility Menu -->
                    <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                    <!-- End Accessibility Menu -->            
              	</div>
              	              
              	<div class="clear"></div>
            
			  	<div class="middleheader_divider">             
              		<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              	</div>
                 
			  	<div class="clear"></div>
            
                <!-- Start Topmenu -->
                <div class="topmenu">
                	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include>                  
     			</div>
                <!-- End Topmenu -->                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               
		
		<a id="maincontent" name="maincontent" ></a>
		         
		<!-- Start Body Content -->
		<div class="content_container">			
			<div class="clear_bottom"></div>
		    
		    <!-- Titlepage Start -->
            <h1>Subject</h1>   
            <!-- Titlepage End -->
            
           	<div class="clear_bottom"></div>
                        
          	<c:set  var="nodes" value="<%= SubjectWorker_aaa.parseAllSubjects()  %>" ></c:set>

			<c:if test="${nodes == null}">
				<h2>No Subjects Available</h2>
			</c:if>


			<c:if test="${nodes != null}">
			
           		<!-- Nav anchor Start -->
           		<div class="nav_anchor">
            		<p>On this page:</p>
                	<ul class="nav_anchor_bar">
						<c:forEach var="node" items="${nodes}" varStatus = "status">
							<li>
								<c:choose>
									<c:when test="${status.count == 1}">
										<a href="#">${node.key.subjectName}</a>
									</c:when>
									<c:otherwise>
										<a href="#${node.key.subjectId}">${node.key.subjectName}</a>
									</c:otherwise>
								</c:choose>
							</li>	
						</c:forEach>	
					</ul>
           		</div>
           		<!-- Nav anchor End -->
           
            	<div class="clear"></div>
 			
 				<div class="treelist">
		            <ul>
						<c:forEach var="node" items="${nodes}" varStatus = "status">
							<c:url var="urlViewAll" value="subject_landing.jsf"  >
								<c:param name="searchType" value="allSubjectBook"/>
								<c:param name="subjectId" value="${node.key.subjectId}"/>
								<c:param name="subjectName" value="${node.key.subjectName}"/>
							</c:url>
							<li>
								<span class="icons_img bullet_01">&nbsp;</span>
								<h2 id="${node.key.subjectId}">${node.key.subjectName} - <a href="<c:out value='${urlViewAll}' escapeXml='true'/>">View All</a>
								</h2>
									<c:if test="${status.count > 1}">
										<div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
									</c:if>
								<c:set var= "nodes_request" value="${node.value}" scope="request"/>
								<c:set var= "feedsBean" value="${feedsBean}" scope="request"/>
								<jsp:include page="../aaa/components/subject_tree_node.jsp"/>
							</li>		
						</c:forEach>	
		            </ul>
	        	</div> 
			</c:if>   
			 
           	<div class="clear"></div>
                   
            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
               
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	<jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->        
	</div>    
</body>
</html>