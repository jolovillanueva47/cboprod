<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Rights &amp; Permissions - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>	
	
</head>
<body>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		         <div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		         </div>
		         <div class="clear"></div>
			<div class="middleheader_divider">
				<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
			</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->
		  
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

			<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
            
 		</div>
        <!-- End Middle Content --> 
        
 		<a id="maincontent" name="maincontent" ></a>
 		   
 		<!-- Start Body Content -->
		<div class="content_container link_02 ul_content">
        
         	<div class="clear"></div>
         	 
        	<!-- Titlepage Start -->
            <h3>Rights &amp; Permissions</h3>   
            <!-- Titlepage End -->
            
            <div class="clear"></div> 
            
           <p>To apply for permission to reproduce parts of Cambridge Books Online content in ways not already allowed under the Terms of Use, follow the directions on the below pages:</p>
          	<ul>
          		<li><a href="http://www.cambridge.org/us/information/rights/">North America</a></li>
     			<li><a href="http://www.cambridge.org/aus/information/rights_permissions.htm">Australia &amp; New Zealand</a></li>
     			<li><a href="http://www.cambridge.org/rights/">Anywhere else in the world</a></li>
     		</ul>
                  
                <div class="clear_bottom"></div>
            
            <!-- Footermenu Start -->
	        <jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
            
        </div>        
        <!-- End Body Content -->     
        
        <!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
         	   
</div>	   
</body>
</html>