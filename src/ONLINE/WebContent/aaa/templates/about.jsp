<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>About Us - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>	
	
</head>
<body>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		         <div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		         </div>
		         <div class="clear"></div>
			<div class="middleheader_divider">
				<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
			</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->
		  
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

			<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
            
 		</div>
        <!-- End Middle Content --> 
        
 		<a id="maincontent" name="maincontent" ></a>
 		   
 		<!-- Start Body Content -->
		<div class="content_container link_02 ul_content">
        
         	<div class="clear"></div>
         	 
        	<!-- Titlepage Start -->
            <h2>About <em>Cambridge Books Online</em></h2>   
            <!-- Titlepage End -->
            
            <div class="clear"></div> 
            
            <p>Cambridge University Press is one of the largest and most prestigious academic publishers in the world and we are widely respected as a world leader in publishing for subjects as diverse as astronomy, Shakespeare studies, economics, mathematics and politics.</p>
		
			<p><em>Cambridge Books Online</em> offers predefined or bespoke collections of content within a richly functional, fully searchable online environment.</p>
		
			<p>Access to <em>Cambridge Books Online</em> content is via institutional purchase, allowing unlimited concurrent users, meaning anyone who wants to may access the resource 24 hours a day.</p>
		
			<p>With online access to unique, indispensable and extensive scholarly content, <em>Cambridge Books Online</em> will offer all levels of user a new dimension of access and usability to support and enhance research.</p>  
            
            	<div class="titlepage_heading"><h3>Key Features</h3></div>            
				<ul>
					<li>Thousands of front &amp; backlist titles</li>
		            <li>Dynamic content and feature set, with frequent addition of new titles being and regular functionality enhancements</li>	
		            <li>Titles from across all Cambridge world renowned subject areas</li>
	                <li>Flexible purchase plans and custom packages</li>
	                <li>Powerful quick search, advanced search and browse capabilities</li>
	                <li>Comprehensive library support tools including downloadable MARC records, usage reports and access &amp; authentication methods</li>
	                <li>Compliance with all major industry standards and initiatives</li>
	                <li>Extensive user functionality including hyperlinked references &amp; personalisation features</li>
	                <li>Dedicated customer support teams</li>
	                <li>Enhanced discoverability tools</li>
	            </ul>
                
                	<div class="titlepage_heading"><h3>Further Information</h3></div>    
                    <ul>
                        <c:choose>
                         	<c:when test="${fn:contains(pageContext.request.requestURL,'localhost') or fn:contains(pageContext.request.requestURL,'192.168.242.136') or fn:contains(pageContext.request.requestURL,'web-manila')}">
                         		<li><a href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/news.jsf?messageId=1556">How to purchase</a></li> 
                         	</c:when>
                         	<c:otherwise>
                         		<li><a href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/news.jsf?messageId=2364">How to purchase</a></li> 
                         	</c:otherwise>
                         </c:choose>
                        <li><a href="<%=System.getProperty("cjo.url") %>registration_aaa?SIGNUP=Y">Sign up for a free trial</a></li>
<!--                        <li><a href="#" onclick="showViewGuidedTour(); return false;">View our guided tour</a></li>										-->
                        <li><a href="<%= System.getProperty("ebooks.context.path") %>aaa/popups/faq.jsf">Frequently Asked Questions (FAQs)</a></li>
                        <li><a href="<%= System.getProperty("ebooks.context.path") %>aaa/templates/contact.jsf">Contact Us</a></li>
                        <li><a href="http://www.cambridge.org/online/" target="_blank">Other online products from Cambridge</a></li>		
                    </ul>
                  
                <div class="clear_bottom"></div>
            
            <!-- Footermenu Start -->
	        <jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
            
        </div>        
        <!-- End Body Content -->     
        
        <!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
         	   
</div>	   
</body>
</html>