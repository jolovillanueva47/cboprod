<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Accessibility - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>
<body>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               
 		
 		<a id="maincontent" name="maincontent" ></a>
 		                 
		<!-- Start Body Content -->
		<div class="content_container ul_content link_02">	
			
			<div class="clear"></div>
			
        	<!-- Titlepage Start -->
            <h2>Accessibility</h2>   
            <!-- Titlepage End -->
            
            <div class="clear"></div>
            
            <p>Cambridge Books Online recognizes the importance of making its web services available to the largest possible audience and has attempted to design and develop this website to be accessible by all users. Where possible this web site has been coded to comply with the World Wide Web Consortium (W3C) Web Accessibility Guidelines Priority Levels 1, 2 and 3 (Conformance Level "AAA").</p>
            <p>Cambridge Books Online will continue to test future releases of this site and remain committed to maintaining its compliance with appropriate accessibility guidelines and serving the widest possible audience for our services.</p>
            <p>For questions about our continuing efforts to make web-based information accessible to all users, or to report an accessibility problem on any of our pages, <a href="../../aaa/templates/contact.jsf">contact us</a>.</p>
            
            <p><h3>Browser / Platform Support</h3></p>
            
            <p>The following is a list of browsers / platforms that are supported on the Cambridge Books Online website:</p>
           
            <ul>
				<li>Internet Explorer 7.0+</li>
				<li>Firefox (Windows and Mac) 3.0+</li>
				<li>Safari (Windows and Mac) 4.0+</li>
				<li>Chrome (Windows) 4.0+</li>
				<li>Opera (Windows) 9.0+</li>
			</ul>
          
                  
                <div class="clear_bottom"></div>
			
			<!-- Footermenu Start -->
	        <jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
	        <!-- Footermenu End -->
			
		</div>        
        <!-- End Body Content -->
  
	    <!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
       
</div>      
</body>
</html>