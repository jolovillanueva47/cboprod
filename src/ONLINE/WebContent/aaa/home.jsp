<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="org.cambridge.ebooks.online.subjecttree.SubjectWorker_aaa"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cambridge Books Online - Cambridge University Press</title>    
    
   	<jsp:include page="${pageContext.request.contextPath}/aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1537" scope="session" />
	<c:set var="pageName" value="Home" scope="session" />
	<c:remove var="publisherCode" scope="session"/>
		
	<div id="page-wrapper">    	    
        
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
            	<div class="topheader_divider">                                                    
	                <!-- Start Accessibility Menu -->
	                <jsp:include page="${pageContext.request.contextPath}/aaa/components/acc_links.jsp"></jsp:include>
	                <!-- End Accessibility Menu -->            
              	</div>
              
             	<div class="clear"></div>
            
			  	<div class="middleheader_divider">             
              		<jsp:include page="${pageContext.request.contextPath}/aaa/components/org_info.jsp"></jsp:include>
              	</div>
              	
			  	<div class="clear"></div>
            
                <!-- Start Topmenu -->
                <div class="topmenu">
               		<jsp:include page="${pageContext.request.contextPath}/aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="${pageContext.request.contextPath}/aaa/components/ip_logo.jsp"></jsp:include>                  
     			</div>
                <!-- End Topmenu -->                 
            </div>
        </div>     
        <!-- End Header -->
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="${pageContext.request.contextPath}/aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
            
            <!-- bread crumb hidden -->
	    	<div class="breadcrumbs_container" style="display: none;">
	    		<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="${pageContext.request.contextPath}/components/crumbtrail.jsp" flush="true" />
			</div>
	    	<!-- bread crumb hidden -->
        </div>
        <!-- End Middle Content -->             
		
		<a id="maincontent" name="maincontent" ></a>
		               
		<!-- Start Body Content -->
		<div class="content_container">   
      
        	<!-- Titlepage Start -->
            <h2>Welcome to <em>Cambridge Books Online</em></h2>   
            <!-- Titlepage End -->
                 	
        	<p><em>Cambridge Books Online</em> offers access to eBooks from our world-renowned publishing programme, covering subjects from all disciplines across science, technology and medicine, as well as humanities and social sciences.</p>
            <p>Building on our outstanding print programme and on our extensive portfolio of high quality, dynamic and innovative online resources, <em>Cambridge Books Online</em> offers all levels of user a new dimension of access and usability to our extensive scholarly content, supporting and enhancing all aspects of research.</p>
            <p><em>Cambridge Books Online</em> is available to libraries worldwide under a number of attractive and flexible models, ensuring instant access to the best research available.</p>
            <p class="link_02">Visit the News and Events section below for the latest updates, or <a href="${pageContext.request.contextPath}/aaa/templates/contact.jsf">contact us</a> for more information.</p>
            
            	<!-- Start Banner -->
            	<input type="hidden" value="${newsBean.initialize}" />
                
                <div class="border-div link_01">
                	<div class="border_divider01">
                    	<!-- Featured Titles -->
                   		<jsp:include page="${pageContext.request.contextPath}/aaa/components/featured_titles.jsp"></jsp:include>
                   		
                   		<!-- Featured Collections -->
                   		<jsp:include page="${pageContext.request.contextPath}/aaa/components/featured_collections.jsp"></jsp:include>
						
                    </div>
                    
                    <div class="border_divider02">
                    	<!-- Subtitle page Start -->
                        <h3>News and Events</h3>
                        <!-- Subtitle page End -->                     
                       	<ul>
                            <c:forEach var="news" items="${newsBean.newsList}">
                        		<li><span class="icons_img bullet_01">&nbsp;</span><a href="${pageContext.request.contextPath}/aaa/popups/news.jsf?messageId=${news.messageId}">${news.messageTitle}</a></li>
                        	</c:forEach>
                        </ul>
                        
                        <div class="clear"></div>
 						
 						<p><strong>Download list of available books:</strong></p>
 						<ul>
                        	<!-- <strong>Download list of available books:</strong> -->
                            <li><span class="icons_img download_excel">&nbsp;</span><a href="${pageContext.request.contextPath}/downloads/titlelist.xls">Microsoft Excel Format</a></li>
                            <li><span class="icons_img download_csv">&nbsp;</span><a href="${pageContext.request.contextPath}/downloads/titlelist.csv">CSV Format - Recommended for Mac users</a></li>
                        </ul> 						
                    </div>
                    
                    <div class="border_divider03">
                    	<!-- Subtitle page Start -->
                        <h3>Browse by Subject</h3>
                        <!-- Subtitle page End -->
	                   	<c:set var="nodes" value="<%= SubjectWorker_aaa.parseAllSubjects()%>" ></c:set>
	             
	                   	 <c:if test="${nodes != null}">
	                   	 	<ul>
								<c:forEach var="node" items="${nodes}" varStatus = "status">
									<c:choose>
										<c:when test="${status.count == 1}">
											<li><span class="icons_img arrow_03">&nbsp;</span>
												<a href="${pageContext.request.contextPath}/subject_tree.jsf?skipId=${node.key.subjectId}">${node.key.subjectName}</a>
											</li>	
										</c:when>
										<c:otherwise>
											<li><span class="icons_img arrow_03">&nbsp;</span>
												<a href="${pageContext.request.contextPath}/subject_tree.jsf?skipId=${node.key.subjectId}#${node.key.subjectId}">${node.key.subjectName}</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
	                   	 </c:if>

                        <form action="<%= System.getProperty("cjo.url")%>registration_aaa?SIGNUP=Y&amp;stylez=${sessionStyle == null ? 'default_style' : sessionStyle}">
							<input type="submit" value="Sign up for a Trial" name="signup" class="bot_signup" />
						</form>     
                    </div>
                
                	<div class="clear"></div>
                    
                </div>
            	<!-- End Banner -->
            
            <!-- Footermenu Start -->
        	<jsp:include page="${pageContext.request.contextPath}/aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	<jsp:include page="${pageContext.request.contextPath}/aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->        
	</div>       
</body>

</html>