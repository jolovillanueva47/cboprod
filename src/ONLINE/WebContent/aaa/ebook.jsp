<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn"%>	
<%@page import="java.net.*" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	${bookBean.initPage}
	<fb:share-button class="meta">
		<meta name="medium" content="mult"/>
		<meta name="title" content="Cambridge Books Online - ${bookBean.bookMetaData.title}"/>
		<meta name="description" content="${bookBean.bookMetaData.blurb}"/>
		<link rel="image_src" href="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" />	
	</fb:share-button>  
		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title><c:out value="${bookBean.bookMetaData.title}" escapeXml="false" /> - Cambridge Books Online - Cambridge University Press</title>
	
	<meta name="keyword" content="${bookBean.bookKeywordMeta}" />
	
	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>
	    
</head>

<body>
		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="1536" scope="session" />
		<c:set var="pageName" value="Book Landing" scope="session" />
		
		

<!-- Start page wrapper -->
	<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
              	<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
			  
			  		<!-- for tab.js -->
					<input type="hidden" id="isbnHidden" value="${bookBean.bookMetaData.onlineIsbn}" />
					<input type="hidden" id="idHidden" value="${bookBean.bookMetaData.id}" />
					
					<input type="hidden" id="pageType" value="ebook" />
					
					<!-- for references -->
					<c:set var="refTabSelected" value="${ref}" />
					
					<c:set var="bookTitlePdf" value="${bookBean.bookMetaData.title}" scope="request" />
	
			  		<!-- for hithighlighting -->
					<c:choose>
						<c:when test="${not empty sessionScope.hithighlight}">
							<input type="hidden" id="hid_hithighlight" value="on" />
						</c:when>
						<c:otherwise>
							<input type="hidden" id="hid_hithighlight" value="off" />
						</c:otherwise>
					</c:choose>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	         
	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
			
			
 		</div>
        <!-- End Middle Content -->               
        
		<a id="maincontent" name="maincontent" ></a>
		              
		<!-- Start Body Content -->
		<div class="content_container">

			<c:set var="pageTab" value="${param.pageTab == null ? 'bd' : param.pageTab}"/>

			<div class="clear_bottom"></div>
		
			<!-- Nav Start -->
			<jsp:include flush="true" page="../aaa/components/book_info_links.jsp" />
			<!-- Nav End -->
			
			<!-- Landing Container Start -->
			<div class="landing_container link_02">
				<!-- Book Thumbnail Start -->
				<c:if test="${not bookBean.hasAccess}">
					<c:if test="${supplemental.salesModuleFlag eq 'N'}">
						<blockquote>This title has been withdrawn from sale on Cambridge Books Online. Institutions who purchased this title previous to removal will continue to have access via Cambridge Books Online but this title can no longer be purchased.</blockquote>	
					</c:if>	
				</c:if>
				
				<div class="icon_enlarge">
					<c:choose>
						<c:when test="${not empty bookBean.bookMetaData.thumbnailImageFilename}">

	                  		<a href="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="${bookBean.bookMetaData.title} cover image">
	                  			<img src="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.thumbnailImageFilename}" alt="${bookBean.bookMetaData.title} cover image" />
	                   		</a>
							<%-- 
	                  		<a href="http://ebooks.cambridge.org/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="${bookBean.bookMetaData.title} cover image">
	                  			<img src="http://ebooks.cambridge.org/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.thumbnailImageFilename}" alt="${bookBean.bookMetaData.title} cover image" />
	                   		</a>
	                   		--%>
	                       	<ul>
	                        	<li>
	                        		<span class="icons_img enlarge">&nbsp;</span><a href="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="link opens new window">Enlarge Image</a>
	                        	</li>
	                       	</ul>	                    	
						</c:when>
						<c:otherwise>
							<ul>
								<li><img src="../../aaa/images/thumbs/thumbnail_placeholder.jpg" alt="No cover image available"/></li>
							</ul>
						</c:otherwise>
					</c:choose>
	            </div>
	            <!-- Book Thumbnail End -->			
			
				<!-- Book Search Info Start -->
				<div class="search_info">
					<ul>
						<li><h2><c:out value="${bookBean.bookMetaData.title}" escapeXml="false" /></h2></li>
							<c:if test="${not empty bookBean.bookMetaData.subtitle}">									
								<li><c:out value="${bookBean.bookMetaData.subtitle}" escapeXml="false" /></li>
							</c:if>	
							<c:if test="${not empty bookBean.bookMetaData.volumeNumber}">
								<li>Volume <c:out value="${bookBean.bookMetaData.volumeNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.volumeTitle)? '</li>': ',' }" escapeXml="false"/> 
							</c:if>
							<c:if test="${not empty bookBean.bookMetaData.volumeTitle}">
								<c:out value="${bookBean.bookMetaData.volumeTitle}" escapeXml="false" /></li>
							</c:if>
							<c:if test="${not empty bookBean.bookMetaData.partNumber}">
								<li>Part <c:out value="${bookBean.bookMetaData.partNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.partTitle)? '</li>': ',' }" escapeXml="false"/>
							</c:if>
							<c:if test="${not empty bookBean.bookMetaData.partTitle}">
								<c:out value="${bookBean.bookMetaData.partTitle}" escapeXml="false" /></li>
							</c:if>
							<c:if test="${not empty bookBean.bookMetaData.edition}">
								<li><c:out value="${bookBean.bookMetaData.edition}" escapeXml="false" /></li>
							</c:if>
						<c:set var="authorRoleTitle" value="${bookBean.bookMetaData.authorRoleTitleList}" />
						<c:set var="authorAffiliation" value="${bookBean.bookMetaData.authorAffiliationList}" />
						<c:forEach items="${bookBean.bookMetaData.authorNameList}" var="author" varStatus="status">	
							<li><c:out value="${authorRoleTitle[status.count - 1]}" /> <c:out value="${author}" escapeXml="false" /></li>
							<c:if test="${not empty authorAffiliation and not empty authorAffiliation[status.count - 1] and 'none' ne authorAffiliation[status.count - 1]}">
								<li><em><c:out value="${authorAffiliation[status.count - 1]}" escapeXml="false" /></em></li>
							</c:if>															
							<c:if test="${not status.last}">
								<li style="list-style: none; display: inline"><br /></li>
							</c:if>
						</c:forEach>				
					</ul>
					<c:if test="${not empty bookBean.bookMetaData.series}">
						<ul>
							<c:url value="series_landing.jsf" var="urlSeries">
								<c:param name="seriesCode" value="${bookBean.bookMetaData.seriesCode}" />
								<c:param name="seriesTitle" value="${bookBean.bookMetaData.series}" />
								<c:choose>
									<c:when test="${not empty bookBean.bookMetaData.seriesNumber}">
										<c:param name="sort" value="series_number" />
									</c:when>
									<c:otherwise>
										<c:param name="sort" value="print_date" />
									</c:otherwise>
								</c:choose>
							</c:url>
							<li>										
				            	<a href="<c:out value="${urlSeries}" />" class="link08">
				            		<c:out value="${bookBean.bookMetaData.series}" escapeXml="false" /></a>
								<c:if test="${not empty bookBean.bookMetaData.seriesNumber}">
									<c:out value=" (No. ${bookBean.bookMetaData.seriesNumber})" escapeXml="false" />
								</c:if>
							</li>
							<li style="list-style: none; display: inline"><br /></li>
						</ul>
					</c:if>
					
					<ul>
						<c:if test="${not empty bookBean.bookMetaData.printDate}">
							<li><strong>Print Publication Year:</strong> ${bookBean.bookMetaData.printDate}</li> 
						</c:if>									
						<c:if test="${not empty bookBean.bookMetaData.onlineDate}">
							<li><strong>Online Publication Date:</strong> ${bookBean.bookMetaData.onlineDate}</li>
						</c:if>	
						<c:if test="${not empty bookBean.bookMetaData.onlineIsbn}">
							<li><strong>Online ISBN:</strong> ${bookBean.bookMetaData.onlineIsbn}</li>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.hardbackIsbn}">
							<li><strong>Hardback ISBN:</strong> ${bookBean.bookMetaData.hardbackIsbn}</li>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.paperbackIsbn}">
							<li><strong>Paperback ISBN:</strong> ${bookBean.bookMetaData.paperbackIsbn}</li>
						</c:if>
					</ul>
				
					<ul>
						<c:if test="${not empty bookBean.bookMetaData.doi}">
							<li><strong>Book DOI:</strong> ${bookBean.bookMetaData.doi}</li>
						</c:if>
					</ul>
				
					<div class="list_divider01">
						<span class="Z3988" title="${bookBean.coinString}">&nbsp;</span>
					</div>
					
					<div class=" clear_bottom"></div>
					
					<p>
						<strong>Subjects:</strong>
						<c:forEach items="${bookBean.bookMetaData.subjectList}" var="subject" varStatus="status">
							<c:url value="subject_landing.jsf" var="subjectLandingUrl">
							<c:param name="searchType" value="allTitles" />
							<c:param name="subjectCode" value="${bookBean.bookMetaData.subjectCodeList[status.count - 1]}" />
							<c:param name="subjectName" value="${subject}" />
							</c:url>
							
							<a href="<c:out value="${subjectLandingUrl}" escapeXml="true" />" class="link07">${subject}</a><c:if test="${not status.last}">,</c:if>
						</c:forEach> 
					</p>
				</div>
				<!-- Book Search Info End --> 
			</div>
			<!-- Landing Container End -->   
			
			<div class="clear_bottom"></div>
				
			<!-- Border Start -->
			<div class="border-landing">
				<!-- Start Topmenu -->
				<div class="topmenu">
					<a name="contentTab"></a>
					<!--<span><h3>Book Details Navigation</h3></span>-->
					<div class="hide_titlepage"><h2>Book Details Navigation</h2></div><!-- Hidden Titlepage --> 
					<c:url var="urlMangerPath_AAA" value="${domainCbo}aaa/ebook.jsf?${accBean.queryStringNoKeyword}"/>
					
					<ul>
						<c:choose>
							<c:when test="${pageScope.pageTab == 'bd' }">
								<li class="top_current"><h4>Book Description</h4></li>		
							</c:when>
							<c:otherwise>
								<li><h4><a id="ebookBd" href="<c:out value="${urlMangerPath_AAA}" />pageTab=bd&amp;#contentTab">Book Description</a></h4></li>
							</c:otherwise> 
						</c:choose>
					
						<c:choose>
							<c:when test="${pageScope.pageTab == 'toc' }">
								<li class="top_current"><h4>Table of Contents</h4></li>
							</c:when>
							<c:otherwise>
								<li><h4><a id="ebookToc" href="<c:out value="${urlMangerPath_AAA}" />pageTab=toc&amp;#contentTab">Table of Contents</a></h4></li>
							</c:otherwise>
						</c:choose>
					
						<c:choose>
							<c:when test="${pageScope.pageTab == 'vol' }">
								<li class="top_current"><h4>Volumes</h4></li>
							</c:when>
							<c:otherwise>
							<c:if test="${not empty bookBean.bookMetaData.volumeNumber}">
								<li><h4><a id="ebookVol" href="<c:out value="${urlMangerPath_AAA}" />pageTab=vol&amp;#contentTab">Volumes</a></h4></li>
							</c:if>
							</c:otherwise>
						</c:choose>
					
						<c:choose>
							<c:when test="${pageScope.pageTab == 'ref' }">
								<li class="top_current"><h4>References</h4></li>
							</c:when>
							<c:otherwise>
							<c:if test="${not empty bookBean.bookMetaData.referenceList}">
								<li><h4><a id="ebookRef" href="<c:out value="${urlMangerPath_AAA}" />pageTab=ref&amp;#contentTab">References</a></h4></li>
							</c:if>
							</c:otherwise>
						</c:choose>
					</ul>
					<div class="clear"></div>                      
				</div>
				<!-- End Topmenu -->
			   
				<div class="clear"></div>		

				<!-- 1st Tab Content Start -->
				<div class="${pageScope.pageTab == 'bd' ? 'show' : 'hide'}">
					<!-- Hidden Titlepage Start -->
					<c:if test="${pageScope.pageTab == 'bd'}">
						<!--  <span class="titlepage_hidden"><h3>Book Description Section</h3></span>-->
						<div class="hide_titlepage"><h3>Book Description Section</h3></div><!-- Hidden Titlepage -->
					</c:if>
					<!-- Hidden Titlepage End -->    
									  
					<dl>
						<c:choose>
							<c:when test="${not empty sessionScope.hithighlight}">
								<c:choose>
									<c:when test="${sessionScope.hithighlight eq 'on'}">
										<c:set var="blurb_aaa" value="${fn:replace(bookBean.bookMetaData.highlightedBlurb,'<span class=\\'searchWord\\'>','<strong>' )}" />
										<c:set var="blurb_aaa" value="${fn:replace(blurb_aaa,'</span>' , '</strong>')}"/>
									</c:when>
									<c:when test="${sessionScope.hithighlight eq 'off'}">
										<c:set var="blurb_aaa" value="${fn:replace(bookBean.bookMetaData.highlightedBlurb,'<span class=\\'searchWord\\'>','' )}" />
										<c:set var="blurb_aaa" value="${fn:replace(blurb_aaa,'</span>' , '')}"/>
									</c:when>
								</c:choose>
								<dt><c:out value="${blurb_aaa}" escapeXml="false" /></dt>
							</c:when>
							<c:otherwise>
								<dt><c:out value="${bookBean.bookMetaData.highlightedBlurb}" escapeXml="false" /></dt>
							</c:otherwise>
						</c:choose>
					</dl>
					
					<c:if test="${not empty bookBean.textfieldsLoad}">												
						<!-- Start Reviews -->
						<c:if test="${not (empty bookBean.textfieldsLoad.review1 and empty bookBean.textfieldsLoad.review2 and empty bookBean.textfieldsLoad.review3)}">													
							<p class="comment_title"><span class="icons_img comment">&nbsp;</span>Reviews:</p>
							<dl class="comment_list">											
								<c:if test="${not empty bookBean.textfieldsLoad.review1}">
									<dt><c:out value="${bookBean.textfieldsLoad.review1}" escapeXml="false" /></dt>
								</c:if>
								<c:if test="${not empty bookBean.textfieldsLoad.review2}">
									<dt><c:out value="${bookBean.textfieldsLoad.review2}" escapeXml="false" /></dt>
								</c:if>
								<c:if test="${not empty bookBean.textfieldsLoad.review3}">
									<dt><c:out value="${bookBean.textfieldsLoad.review3}" escapeXml="false" /></dt>
								</c:if>
							</dl>
						</c:if>
						<!-- End Reviews -->												
					</c:if>							
					
					<!-- Start Prizes -->
					<c:if test="${not empty bookBean.prizeLoadList}">
						<p class="comment_title"><span class="icons_img winner">&nbsp;</span>Prizes:</p>											
						<dl class="comment_list">
							<c:forEach var="prizeLoad" items="${bookBean.prizeLoadList}">                                 	
								<dt><c:out value="${prizeLoad.prizeText}" escapeXml="false"/></dt>
							</c:forEach>
						</dl>
					</c:if>
					<!-- End Prizes -->
				</div>		                              							
				<!-- 1st Tab Content End -->					

				<!-- 2nd Tab Content Start -->
				<div class="${pageScope.pageTab == 'toc' ? 'show' : 'hide'}">  
				
					<!-- Hidden Titlepage Start -->
					<c:if test="${pageScope.pageTab == 'toc'}">
						<!-- <span class="titlepage_hidden"><h3>Table of Contents Section</h3></span> -->
						<div class="hide_titlepage"><h3>Table of Contents Section</h3></div><!-- Hidden Titlepage -->
					</c:if>
					<!-- Hidden Titlepage End -->
						
					<!-- Set hithighlight for readpdf -->
					
					<c:choose>
						<c:when test="${not empty sessionScope.hithighlight}">
							<c:choose>
								<c:when test="${sessionScope.hithighlight eq 'on'}">
									<c:set var="pdfHitHighlight" value="&amp;hithighlight=on" scope="page"/>
								</c:when>
								<c:when test="${sessionScope.hithighlight eq 'off'}">
									<c:set var="pdfHitHighlight" value="&amp;hithighlight=off" scope="page" />
								</c:when>
							</c:choose>
						</c:when>
					</c:choose>
					
					<!-- TOC List Start -->
					<table  class="book_landing link_01" summary="Table of Contents">
						<c:forEach items="${bookBean.bookTocItemList}" var="bookTocItem" varStatus="status">
							<tr>
								<c:choose>
									<c:when test="${'root' ne bookBean.bookTocItemLevelMap[bookTocItem.id]}">
										<c:set var="indent" value="indent01" />			
									</c:when>
									<c:otherwise>
										<c:set var="indent" value="" />
									</c:otherwise>
								</c:choose>														
								<td class="${indent}">				
									<ul>
										<%-- 
										<c:if test="${not empty bookBean.tocItemMap[bookTocItem.id]}">
											<li><a id="${bookTocItem.id}" class="tocItem" href="javascript:void(0);">+</a></li>
										</c:if>
										--%>
										<li>
											<a href="chapter.jsf?bid=${bookTocItem.bookId}&amp;cid=${bookTocItem.id}&amp;p=${status.count-1}&amp;pageTab=ce">
												<c:if test="${not empty bookTocItem.label}">
													<strong>${bookTocItem.label} -</strong>
												</c:if>
												<strong><c:out value="${bookTocItem.title}" escapeXml="false" />:</strong></a>
										</li>
										<c:if test="${not empty bookTocItem.contributorNames}">												
											<li> By <c:out value="${bookTocItem.contributorNames}" escapeXml="false" /></li>
										</c:if>
										
										<li><input type="hidden" value="${bookTocItem.accessType}" name="accessTypeHidden" /></li>
										<% 
											String userAgent = request.getHeader("User-Agent"); 
										%>
										<c:choose>
											<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">												
												<li class="icons_img pdf_on">&nbsp;</li>
												<li>
													<a href="javascript:void(0);" onclick="showPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '');">Read PDF</a>
												</li>																										
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${not empty userInfo and not empty userInfo.username}">
														<li class="icons_img pdf_off">&nbsp;</li>	
														<li>No PDF Available</li>
													</c:when>
													<c:otherwise>
														<c:if test="${supplemental.salesModuleFlag eq 'N'}">
															<li class="icons_img pdf_off">&nbsp;</li>	
															<li>PDF is No Longer Available for Sale on This Platform</li>
														</c:if>
														<c:if test="${supplemental.salesModuleFlag eq 'Y'}">
															<li class="icons_img pdf_off">&nbsp;</li>	
															<li><a href="${pageContext.request.contextPath}/aaa/login.jsf?">Log-in to Read PDF</a></li>
														</c:if>
													</c:otherwise>
												</c:choose>														
											</c:otherwise>
										</c:choose>															
									</ul>
								</td>
								<td class="toc_num">pp. ${bookTocItem.pageStart}<c:if test="${not empty bookTocItem.pageEnd}">-${bookTocItem.pageEnd}</c:if></td>
							</tr>
							<c:if test="${not empty bookBean.tocItemMap[bookTocItem.id]}">
								<c:forEach items="${bookBean.tocItemMap[bookTocItem.id]}" var="tocItems">
									<tr>																
										<c:set var="toc_indent" value="indent01" />
										<td class="${toc_indent}">
											<ul>
												<li><c:out value="${tocItems.text}" escapeXml="false" />&nbsp;</li>							
												<c:choose>
													<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">
														<li class="icons_img pdf_on">&nbsp;</li>
														<li>
															<a href="#" class="link03" onclick="showContentTocPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '${tocItems.pageStart}', '${bookTocItem.pageStart}', '');return false;">Read PDF</a>															
														</li>
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${not empty userInfo and not empty userInfo.username}">
																<li class="icons_img pdf_off">&nbsp;</li>	
																<li>No PDF Available</li>
															</c:when>
															<c:otherwise>	
																<c:if test="${supplemental.salesModuleFlag eq 'N'}">
																		<li class="icons_img pdf_off">&nbsp;</li>	
																		<li>PDF is No Longer Available for Sale on This Platform</li>
																	</c:if>
																	<c:if test="${supplemental.salesModuleFlag eq 'Y'}">
																		<li class="icons_img pdf_off">&nbsp;</li>	
																		<li><a href="${pageContext.request.contextPath}/aaa/login.jsf?">Log-in to Read PDF</a></li>
																	</c:if>
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</ul>		
										</td>
										<td class="toc_num">${tocItems.pageStart}</td>	
									</tr>
								</c:forEach>
							</c:if>	
						</c:forEach>
					</table>
					<!-- TOC List End-->
				</div>
				<!-- 2nd Tab Content End -->
				
				<!-- volume Tab Content Start -->
				<div class="${pageScope.pageTab == 'vol' ? 'show link_01' : 'hide'}">
			
					<!-- Hidden Titlepage Start -->
					<span class="titlepage_hidden">Volumes</span>
					<!-- Hidden Titlepage End -->

					<c:forEach items="${bookBean.volumeList}" var="BookMetaData" varStatus="status">
						<dl class="volume_list">                                    
							<!-- Volume item start -->
							<dl>
								<h3><a href="ebook.jsf?bid=${BookMetaData.id}">${BookMetaData.title}</a> Volume ${BookMetaData.volumeNumber}</h3>
								<p><c:out value="${BookMetaData.authorSingleline}" /></p>
								
								<dt>
									<p><strong>Print Publication Year:</strong> ${BookMetaData.printDate}</p>
									<p><strong>Online Publication Date:</strong> ${BookMetaData.onlineDate}</p>
									<p><strong>Online ISBN:</strong> ${BookMetaData.onlineIsbn}</p>
								</dt>  
								<dt>
									<p><strong>Paperback ISBN:</strong> ${BookMetaData.paperbackIsbn}</p>
									<p><strong>Book DOI:</strong> ${BookMetaData.doi}</p>
								</dt> 
							</dl>
							<!-- Volume item end -->   
						</dl>
					</c:forEach>
					<dl>&nbsp;</dl>				
				</div>
				<!-- volume Tab Content End -->

				<c:set var="refTabSelected" value="${pageScope.pageTab == 'ref'}" />

				<!-- 3rd Tab Content Start -->
				<div id="referencesWrapper" class="${refTabSelected ? 'arrow_05 show' : 'arrow_05 hide'}">
					<!-- Hidden Titlepage Start -->
						<c:if test="${pageScope.pageTab == 'ref'}">
							<!-- <span class="titlepage_hidden"><h3>References Section</h3></span> -->
							<div class="hide_titlepage"><h3>References Section</h3></div><!-- Hidden Titlepage -->
						</c:if>
					<!-- Hidden Titlepage End -->  
					<c:choose>
						<c:when test="${refTabSelected and not empty bookBean.bookMetaData.referenceList}">
							<p>Clicking the following links will forward you to other websites.</p>
							<p>These websites provide other information regarding the selected reference.</p>
							<div class="show bullet_01">
								<ul>
									<li>Open URL</li>
									<li>Google Scholar</li>
									<li>Google Books</li>
								</ul>
							</div>
							<c:forEach items="${bookBean.bookMetaData.referenceList}" var="referenceItem">
								<p><b><c:out value="${referenceItem.contentLabel}" escapeXml="false" /></b></p>
								
																		
								<c:import url="${urlContent}${referenceItem.file}"></c:import>
																			
							</c:forEach>
						</c:when>
						<c:otherwise><p>No references available.</p></c:otherwise>
					</c:choose>
				</div><br/><br/>
				<!-- 3rd Tab Content End -->  
			   
				<div class="boxBottom"></div>
				<a id="refOpenURLLink" class="thickbox"></a>									
							
			</div>
			<!-- Border End -->	

			<div class="clear"></div>
			
			<!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
	    
	   
		<jsp:include page="/components/common_js.jsp" flush="true" />
	    <script type="text/javascript" src="/js/book.js"></script>
	    <script type="text/javascript" src="/js/rightmenu.js"></script>
	    <script type="text/javascript" src="js/references.js"></script>
	      
</div> 		
<!-- End page wrapper -->		
	
</body>
</html>