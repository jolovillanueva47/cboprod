<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<%@page import="org.cambridge.ebooks.online.jpa.rss.Rss"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Advanced Search - Cambridge Books Online - Cambridge University Press</title>    
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
	<% 
		session.setAttribute("helpType", "child");
		session.setAttribute("pageId", "1527");
		session.setAttribute("pageName", "Advance Search");
	%>
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">                                         
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
              	<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               
        
        <a id="maincontent" name="maincontent" ></a>
            
		<!-- Start Body Content -->
		<div class="content_container buttons">
			
			<div class="clear_bottom"></div>     
        	<!-- Titlepage Start -->
            <h2><em>Advanced Search</em></h2>   
            <!-- Titlepage End -->
        	<div class="clear_bottom"></div>
        	
         	<!-- Search Again Start -->       
            <form id="advanceSearchForm" action="<%=request.getContextPath()%>/search_aaa?queryCount=8&searchType=advance" method="post">
            	
            	<p><strong>Please input search parameters in search fields.</strong></p>
            	<p><strong>*</strong> Empty search fields will not be included in the search.</p>

          		
          	
				<jsp:include page="../aaa/components/adv_search_inputform1.jsp"></jsp:include>


		    	<input type="submit" value="Search" name="search" class="input_buttons" />
                <input type="reset" value="Clear All" name="clear" class="input_buttons"/>
                      
			</form>
		    <!-- Search Again End -->
            
            <div class="clear_bottom"></div>
            
            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
      
</body>

</html>