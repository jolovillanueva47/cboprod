<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	${seriesBean.initAlphaMap}
	${seriesBean.initAllSeriesPage}
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>All Series - Cambridge Books Online - Cambridge University Press</title>    
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
		<c:set var="helpType" value="child" />
		<c:set var="pageId" value="1541" />
		<c:set var="pageName" value="All Series" />
		
<div id="page-wrapper"> 
	<!-- Start Header -->
	<div class="header">
		<div class="header_container">
		
			 <div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
			 </div>
			 <div class="clear"></div>
		<div class="middleheader_divider">
			<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
		</div>
		
		<div class="clear"></div>
		
			<!-- Start Topmenu -->
			<div class="topmenu">
				<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
				<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
			</div>
			<!-- End Topmenu -->     
		</div>
	</div>  
	<!-- End Header -->	

	<!-- Start Middle Content -->
    <div class="breadcrumbs_search_container">
    	<!-- Search Start -->
    	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
    	<!-- Search End --> 

        <!-- Breadcrumbs Start -->
		<div class="breadcrumbs_container">
			<span class="titlepage_hidden">You are here:</span> 
			<jsp:include page="/components/crumbtrail.jsp" flush="true" />
		</div>
		<!-- Breadcrumbs End -->
	</div>
    <!-- End Middle Content -->               
	
	<a id="maincontent" name="maincontent" ></a>
		
	<!-- Start Body Content -->
	<div class="content_container input_text buttons">	
       	
       	<div class="clear_bottom"></div>	
       	<!-- Titlepage Start -->
       		<h2>All Series</h2>   
        <!-- Titlepage End -->
        <div class="clear_bottom"></div>
        
  		<div class="clear_bottom"></div>
  		
  		<c:url var="urlAllSeries" value="all_series.jsf" >
  			<c:param name="firstLetter" value="${seriesBean.firstLetter}" />
  			<c:param name="searchType" value="page" />
  		</c:url>

  		<!-- Pager Menu Start -->
		<!-- Bar Container Start -->
		<div class="bar_container link_02">
			<form action="<c:out value="${urlAllSeries}" />" method="post">
				<jsp:include page="../aaa/components/pager.jsp"></jsp:include>
			</form>
		</div>  
        <!-- Bar Container End -->
		<!-- Pager Menu End --> 
      
        <!-- Series Count Start -->
		<p>
		<c:choose>
			<c:when test="${seriesBean.resultsFound > 1}">
				There are a total of 
				<strong><c:out value="${seriesBean.resultsFound}" /></strong> series.
			</c:when>
			<c:when test="${seriesBean.resultsFound == 1}">
				There is only 
				<strong><c:out value="${seriesBean.resultsFound}" /></strong> series.
			</c:when>
			<c:otherwise>
				There are no series available.					
			</c:otherwise>
		</c:choose>	
		</p>			
		<!-- Series Count End -->
		
	    <p><strong>Select your series by clicking on a letter.</strong></p>	

		<!-- Alpha List Start -->
        <div class="alphalist02">
           	<jsp:include page="../aaa/components/series_alphalist_sub.jsp" flush="true"></jsp:include>
            <div class="clear"></div>
        </div>
		<!-- Alpha List End -->	                

        <div class="clear"></div>
        
		<!-- Letter Heading Start -->
		<div id="content_wrapper09"> 
			<c:choose>
				<c:when test="${fn:containsIgnoreCase(seriesBean.firstLetter,'All')}">
					<c:set value="All Series Starting With Any Letters" var="firstLetter" scope="page"></c:set>
				</c:when>
				<c:otherwise>
					<c:set value="All Series Starting With The Letter ${seriesBean.firstLetter}" var="firstLetter" scope="page"></c:set>
				</c:otherwise>
			</c:choose>
			
			<h2><c:out value="${pageScope.firstLetter}" /></h2> 
		</div>
		<!-- Letter Heading End -->			
		
		<div class="clear"></div>
							
		<!-- Series Titles Start -->
		<div class="list01">
			<ul>
				<c:choose>				
					<c:when test="${seriesBean.resultsFound > 0}">			
						<c:forEach var="book" items="${seriesBean.bookMetaDataList}">							
							<c:url var="urlSeriesLanding" value="series_landing.jsf">
								<c:param name="seriesCode" value="${book.seriesCode}" />
								<c:param name="seriesTitle" value="${book.series}" />
								
								<c:choose>
									<c:when test="${not empty book.seriesNumber && 'null' ne book.seriesNumber}">
										<c:param name="sort" value="series_number" />
									</c:when>
									<c:otherwise>
										<c:param name="sort" value="print_date" />
									</c:otherwise>
								</c:choose>	
							</c:url>
							<li><a href="<c:out value="${urlSeriesLanding}" />" class="link08"><c:out value="${book.series}" escapeXml="false" /></a></li>										
						</c:forEach>			
					</c:when>
					<c:otherwise>
						<li>
							<h2>No results found.</h2>
						</li>
					</c:otherwise>			
				</c:choose>
			</ul>
		</div>
		<!-- Series Titles End -->
		
		<!-- Pager Menu Start -->
		<!-- Bar Container Start -->
		<div class="bar_container link_02">
			<form action="<c:out value="${urlAllSeries}" />" method="post">
				<jsp:include page="../aaa/components/pager.jsp">
					<jsp:param name="bottom" value="bottom"></jsp:param>
				</jsp:include>
			</form>
		</div>  
        <!-- Bar Container End -->
		<!-- Pager Menu End --> 
						
		<!-- Footermenu Start -->
		<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
		<!-- Footermenu End -->
		
	</div>        
	<!-- End Body Content -->
	
	<div class="clear"></div>
	
	<!-- FooterContainer Start -->
	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	<!-- FooterContainer End -->
       
</div>     		
</body>
</html>