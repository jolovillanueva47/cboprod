<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="org.cambridge.ebooks.online.util.EBooksUtil"%>


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >


<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cited By CrossRef - Cambridge Books Online - Cambridge University Press</title>
	
	<jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>

<body>
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
 	    	
	    	<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<!-- used standard crumbtrail.jsp--> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
	      	
        </div>
        <!-- End Middle Content --> 
                    
 		<a id="maincontent" name="maincontent" ></a> 
 		             
		<!-- Start Body Content -->
		<div class="content_container link_02">
		     
        	<div class="clear_bottom"></div>          
            
              <!-- Border Start -->
              <div class="border-landing">
              
              <!-- Titlepage Start -->
              <h2>Cited By (CrossRef)</h2>   
              <!-- Titlepage End -->
    
                
                    <!-- Content Start -->                      
         	       
         	        <c:if test="${not empty result  }">
						<table cellspacing="0" cellpadding="0">	
							
							<c:forEach var="citationVO" items="${result}">	
							
							
								<c:catch var="e">
									<p>
										<c:forEach var="cont" items="${citationVO.contributors}" varStatus="status">
											${cont}	
											<c:choose>
												<c:when test="${status.last}">.&nbsp;</c:when>
												<c:otherwise>,&nbsp;</c:otherwise>
											</c:choose> 
											
										</c:forEach>
										&nbsp;
										${citationVO.articleTitle}. <em>${citationVO.journalTitle}</em> ${citationVO.year}; 
										<c:if test="${not empty citationVO.volume_title}">
											<strong>${citationVO.volume_title} </strong>
										</c:if>		
												  		
								  		<c:if test="${not empty citationVO.volume}">
								  			<strong>${citationVO.volume}</strong>: ${citationVO.page}.&nbsp;
								 		</c:if>
									
										<a href="http://dx.doi.org/${citationVO.doi}" target="_blank" title="opens in new tab/window">[CrossRef]</a>
									</p>
								</c:catch>
							
							
								
								
								<c:if test="${not empty e}">error</c:if>
								
								
								
								
								
								
																		
										<%-- 
										
										<%= 
											java.net.URLDecoder.decode("", "iso-8859-1")
										%>
										--%>
								
								
								
								
								
								
							</c:forEach>
						</table>
					</c:if>
					
					<c:if test="${empty result}">
						<p>No CrossRef Entries Found.</p>
					</c:if>
         	        <!-- Content End -->             
                
            </div>
           	<!-- Border End -->
                
			<div class="clear_bottom"></div>

                  
            <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
</body>

</html>