<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>News and Events - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
 </head>
<body>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		         <div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		         </div>
		         <div class="clear"></div>
			<div class="middleheader_divider">
				
				<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
			</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
  		<div class="clear_bottom"></div>
	
		<!-- Search Start -->
			<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
		<!-- Search End --> 

	    <!-- Breadcrumbs Start -->
		<div class="breadcrumbs_container">
			<span class="titlepage_hidden">You are here:</span> 
			<jsp:include page="/components/crumbtrail.jsp" flush="true" />
		</div>
		<!-- Breadcrumbs End -->
		  
		<a id="maincontent" name="maincontent" ></a>
		       
		<!-- Start Body Content -->
		<div class="content_container">
			<div class="noBordercontent link_02">
				<div class="clear_bottom"></div>
				<h1>News And Events</h1>
				<div class="clear_bottom"></div>  
				<input type="hidden" value="${newsBean.populateNews}" />
				<h2><a target="_blank" href="${newsBean.link}"><c:out value="${newsBean.messageTitle}" escapeXml="false" /></a></h2>
				<p class="answer"><c:out value="${newsBean.message}" escapeXml="false" /></p>	
			</div><!-- end content -->	
			
			
			<div class="clear"></div>
			
			<!-- Footermenu Start -->
	        <jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
	        <!-- Footermenu End -->
			
		</div>        
        <!-- End Body Content -->
        
	</div>   
    
    <!-- FooterContainer Start -->
  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    <!-- FooterContainer End -->

</body>
</html>