<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Help - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>

<body>
${helpBean.initializeHelp}
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
	    	<div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
	    	</div>
		</div>  
		<!-- End Header -->
	    
  		<div class="clear_bottom"></div>

        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
            
            <!-- bread crumb hidden -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
	    	<!-- bread crumb hidden -->
        </div>
        <!-- End Middle Content --> 
		  
 		<a id="maincontent" name="maincontent" ></a>
 		                 
		<!-- Start Body Content -->
		<div class="content_container link_02">
		<div class="clear_bottom"></div>
			<!-- Border Start -->
			<div class="border-landing input_text buttons">
				
				<div class="topmenu">
					<a name="contentTab"></a>
						<ul>
						<li><h3><a id="faqHti" href="help_index.jsf?#contentTab">Help Topic Index</a></h3></li>
						<li><h3><a id="faqFaq" href="faq.jsf?#contentTab">FAQs</a></h3></li>		
						<li><h3><a id="faqVad" href="view_access.jsf?#contentTab">Diagnostics</a></h3></li>
						</ul> 
					<div class="clear"></div>                      
				</div>
				<!-- End Topmenu -->   
				
		<div class="content_container">
			<h1>Help Topic: ${helpBean.helpBooksBean.pageName}</h1>			 
			<c:choose>
				<c:when test="${helpBean.helpBooksBean.helpFile ne null && helpBean.helpBooksBean.helpFile ne ''}">	
					<div>	
						<c:import url="${helpBean.helpBooksBean.helpFile}" charEncoding="utf8"/>
					</div>	 
				</c:when>
				
				<c:otherwise>
					<div class="answer">
						No help text.
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- End Body Content -->
		
		</div>
       </div>
        <div class="clear"></div>

        <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        <!-- Footermenu End -->
       
    	<!-- FooterContainer Start -->
  	  		<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    	<!-- FooterContainer End -->

</div>	
</body>
</html>