<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Email link to this book - Cambridge Books Online - Cambridge University Press</title>
	
	<jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
	
</head>

<body>

<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
 	    	
	    	<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End --> 
	      	
        </div>
        <!-- End Middle Content -->             
        
        <a id="maincontent" name="maincontent" ></a>
             
		<!-- Start Body Content -->
		<div class="content_container link_02">

      		<!-- Titlepage Start -->
            <h1>Message Sent.</h1>   
            <!-- Titlepage End -->
            
            <div class="clear_bottom"></div>
            
				<div class="validate">
					<p>Your email message was sent.</p>
				</div>
				
			<c:choose>
				<c:when test="${vadEmail != null}">
					<a href="../../aaa/popups/view_access.jsf">Go back to Diagnostics.</a> <br/>
				</c:when>
				<c:otherwise>
					<a href="${param.successBookLink}">Go back to ${param.Title} book details.</a> <br/>
				</c:otherwise>
			</c:choose>	
				
				
			
       
			<div class="clear_bottom"></div>

            <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 

</body>

</html>