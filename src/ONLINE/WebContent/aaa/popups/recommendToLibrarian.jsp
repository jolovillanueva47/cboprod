<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<%@page import="org.cambridge.ebooks.online.email.RecommendToLibrarianBean"%>
<%@page import="org.cambridge.ebooks.online.email.Email"%>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Recommend To Librarian - Cambridge Books Online - Cambridge University Press</title>        
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
    
</head>

<body>

	<div id="page-wrapper"> 
	    <!-- Start Header -->
        <div class="header">
            <div class="header_container">
				<div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
				</div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->    
		
	<a id="maincontent" name="maincontent" ></a>

	<!-- Start Body Content -->
	<div class="content_container link_02">
		
		<div class="clear"/>
		
		<h2>Recommend This to a Librarian</h2>
		<div class="clear_bottom"></div>
		
		<c:set var="errorMessage_LIB_request" value="${errorMessage_LIB}" scope="request" />
		
		<c:if test="${errorMessage_LIB_request != null}">
			<div class="validate">
   				<h3>Email Error Message:</h3>
				<p>Unable to send email. The following errors found.</p>
				<ol>
					<c:forEach var="error" items="${errorMessage_LIB_request}">
						<li><a href="#${error.key}">${error.value}</a></li>
					</c:forEach>
				</ol>
			</div>
			<div class="clear_bottom"></div>
		</c:if>
		
		<span class="titlepage_hidden"><h3>Email Form</h3></span>
		
		<form action="email_aaa" method="post">
	
			<input type="hidden" name="emailType" value="LIB" />
		
			<input type="hidden" name="recommendType" value="${param.recommendType}" />
			<input type="hidden" name="bookId" value="${param.bookId}" />
			<input type="hidden" name="successBookLink" value="<%=System.getProperty("ebooks.context.path")%>aaa/ebook.jsf?bid=${param.bookId}" />
			<input type="hidden" name="Title" value="${param.Title}" />
			
			
		
		<c:if test="${!recommendToLibrarian.displayDetails}">
		<ul>
	    	<li class="header">Recommend This to a Librarian</li>
	    	<li></li>
	    	<li><%-- <font color="#FF0000"><h:messages layout="list" /></font>--%>&nbsp;</li>
	    	<li></li>
	    	<li><%--  <h:commandLink value="Go Back" action="${recommendToLibrarian.goBack}" />--%>
	    		<input type="submit" name="update" value="Update" class="input_buttons"/>
	    	</li>   	
		</ul>
		</c:if>
		
		<c:if test="${recommendToLibrarian.displayDetails}">
		
	   
	   	<p>Email your librarian to recommend adding this book to the library's collection. Either send your message to the library administrator listed below (we use your IP address to fill these fields in) or enter a new email address in the Librarian email address box. You can also add a message.</p>
		 		
		<div class="clear_bottom"></div>
	   	<%-- <font color="#FF0000"><h:messages layout="list" /></font>--%>
	   	
		<input name ="exactURL" value='${param.exactURL}' type="hidden" />
	   	
	    <table class="global_forms input_text buttons" cellspacing="0">
			<tr>
	            <td align="right"><label for="name">Your name:</label></td>
	            <td><input type="text" name="userName" id="userName" value="${param.submit != null  ? param.userName : recommendToLibrarian.userName}" class="input_text" />
	            </td>
	        </tr>
	        
	        <tr>
	            <td align="right"><label for="userEmail">Your email address*:</label></td>
	            <td>
		            <%--<input type="text" id="userEmail" class="register_field" 
							value="${recommendToLibrarian.userEmail}"  required="true" 
							validator="${recommendToLibrarian.validateEmail}" maxlength="80" >
						 <f:attribute name="requiredMessage" value="Email address is required"/>--%>
					<input type="text" name="userEmail" id="userEmail" maxlength="80" value="${param.submit != null ? param.userEmail : recommendToLibrarian.userEmail}" class="input_text"></input>
	           </td>
	        </tr>
	        
	        <tr>
	            <td align="right"><label for="recipientName">Librarian name:</label></td>
	            <td>
					<input type="text" name="recipientName" id="recipientName" value="${param.submit != null ? param.recipientName : recommendToLibrarian.recipientName}" class="input_text" />
				</td>
	        </tr>
	        
			<tr>
	            <td align="right"><label for="recipientEmail">Librarian email address*:</label></td>
	            <td>
					<%-- <h:inputText id="recipientEmail" styleClass="register_field" 
						value="${recommendToLibrarian.recipientEmail}" required="true"
						validator="${recommendToLibrarian.validateEmail}" maxlength="80" >
						<f:attribute name="requiredMessage" value="Recipient email address is required"/>
					</h:inputText>--%>
					<input type="text" name="recipientEmail" id="recipientEmail" value="${param.submit != null ? param.recipientEmail : recommendToLibrarian.recipientEmail}" class="input_text"></input>
				</td>
	        </tr>
	        
	        <tr>
	            <td align="right"><label for="subject">Subject:</label></td>
	            <td>
					<input type="text" name="subject" id="subject" value="${recommendToLibrarian.subject}" size="50" readonly="readonly" class="input_text"/>
				</td>  
	        </tr>
	        
	        <tr>
	            <td align="right"><label for="content">Message</label></td>
	            <td>
	            	
					<textarea  name="mailmessage" id="mailmessage" title="Scroll-up to view System Generated Message" onfocus="doFocusCursorTop('mailmessage');" onmouseover="removeTitle('mailmessage');"class="problem"  cols="23" rows="5" <%-- required="true"--%> >${param.submit != null  ? param.mailmessage : recommendToLibrarian.mailmessage}</textarea>
					<%-- <f:attribute name="requiredMessage" value="Message is required"/> --%>				
				</td>
				
			<!--<td><textarea name="content" cols="1" rows="5" class="email" id="content"></textarea></td>-->     
	        </tr>
	        <tr>
	        	<td colspan="2"><hr/></td>
	        </tr>
	        <tr><td>
	        <input type="submit" name="submit" id="email_send" value="Send" class="input_buttons"/>
			&nbsp;<input type="submit" id="resetBtn" name="reset_aaa" value="Reset" class="input_buttons"/>
	        </td></tr>
		</table>
	
		<div id="bottomButtons">
			<%--a href="javascript: void(0);" onclick="doReset();" title="Reset"><img src="${pageContext.request.contextPath}/images/bot_reset.gif" /></a--%>       	
	       	<%--a href="javascript: void(0);" onclick="doEmail();" title="Email"><img src="${pageContext.request.contextPath}/images/bot_email.gif" /></a--%>
	       	<%--  <h:commandButton id="resetBtn" action="${recommendToLibrarian.resetRecommendationFields}" image="../images/bot_reset.gif"/>
	       	&nbsp;<h:commandButton id="email_send" action="${recommendToLibrarian.sendEmail}" image="../images/bot_email.gif"/>       	
	       	<h:message for="email_send"/>--%>
			
			
	    </div>
		</c:if>
	<!--</div> end content -->
	</form>
			<!-- Footermenu Start -->
			<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
			<!-- Footermenu End -->	
		
        <!-- End Body Content -->
    	<%-- </div><!-- end content -->--%>    
        <div class="clear"></div>	
</div>
<%-- 
<script type="text/javascript">
	function doReset() {
		document.getElementById("resetBtn").onclick();
	}
	
	function doEmail() {
		document.getElementById("email_send").onclick();
	}

	function send(formName){
		 var form = document.forms[formName];
		 form.action = form.action + "#country";
		 form.submit();
	} 
</script>
--%>
	<c:import url="${pageContext.request.contextPath}/components/google_analytics.jsp" />
         

</div>
    <!-- FooterContainer Start -->
	<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    <!-- FooterContainer End -->
    
<script type="text/javascript">
	function doFocusCursorTop(areaId) {
		var txtarea = document.getElementById(areaId); 
 
		var strPos = 0; 
		

		setCaretPosition(txtarea, strPos);

		var current = doGetCaretPosition(txtarea);
		var last = txtarea.value.length;
		
		setSelection(txtarea, current, last)
		
		
	}
	
	function setCaretPosition(ctrl, pos){
		if(ctrl.setSelectionRange)
		{
			ctrl.focus();
			ctrl.setSelectionRange(pos,pos);
		}
		else if (ctrl.createTextRange) {
			var range = ctrl.createTextRange();
			range.collapse(true);
			range.moveEnd('character', pos);
			range.moveStart('character', pos);
			range.select();
		}
	}
	function doGetCaretPosition (ctrl) {
		var CaretPos = 0;	// IE Support
		if (document.selection) {
		ctrl.focus ();
			var Sel = document.selection.createRange ();
			Sel.moveStart ('character', -ctrl.value.length);
			CaretPos = Sel.text.length;
		}
		// Firefox support
		else if (ctrl.selectionStart || ctrl.selectionStart == '0')
			CaretPos = ctrl.selectionStart;
		return (CaretPos);
	}
		
	function setSelection(ctrl, start, stop){

		if(ctrl.setSelectionRange)
		{
			ctrl.focus();
			ctrl.setSelectionRange(start,stop);
		}
		else if (ctrl.createTextRange) {
			var range = ctrl.createTextRange();
			range.collapse(true);
			range.moveEnd('character', start);
			range.moveStart('character', stop);
			range.select();
		}

	}
	function removeTitle(id){
		var txtarea = document.getElementById(id); 
		txtarea.setAttribute("title","");
		txtarea.onmouseover = hideTooltip;
		
	}
</script>    
    
    
    
</body>
</html>
