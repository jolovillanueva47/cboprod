<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Citation Alert - Cambridge Books Online - Cambridge University Press</title>        
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
    
</head>

<body>

	<jsp:useBean id="citationAlertsBean" class="org.cambridge.ebooks.online.citation.alerts.CitationAlertsBean" scope="session"/>
	<!--<jsp:getProperty property="initThickBox" name="citationAlertsBean"/>-->
	
	<div id="page-wrapper"> 
	    <!-- Start Header -->
        <div class="header">
            <div class="header_container">
				<div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
				</div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->    
		
	<a id="maincontent" name="maincontent" ></a>
	       
		<!-- Start Body Content -->
	<div class="content_container link_02">
		
		<div class="clear_bottom"></div>
		
		<!-- Titlepage Start -->
		<h2>Citation Alert</h2>   
		<!-- Titlepage End -->
		
		<div class="clear_bottom"></div>
   
		<form method="post" action="citationAlertsBean_aaa" >
		
			<input type="hidden" name="bookId" value="${param.bookId}"></input>
			<input type="hidden" name="doi" value="${param.doi}"></input>
			<input type="hidden" name="contentId" value="${param.contentId}"></input>
			
			<table class="global_forms input_text buttons" cellspacing="0">
				<tr>
					<td colspan="2">You will receive an e-mail when the articles below are cited by other publications.</td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<tr>
					<td colspan="2"><input type="reset" name="reset" value="Reset" class="input_buttons"/> <input type="submit" name="update" value="Update" class="input_buttons"/></td>
				</tr>
			          <tr><td colspan="2"><hr/></td></tr>
			          <tr>
			            <td colspan="2"><table class="landing" summary="Citation Alert">
			              <tbody>
			                <tr>
			                  <td><strong>Book Title</strong></td>
			                  <td><strong>Alert Frequency</strong></td>
			                  <td><strong>Email Address</strong></td>
			                  <td><%--<input type="submit" name="Turn Off Alert" value="Save and Turn Off Alerts" class="input_buttons"/><input type="checkbox" />&nbsp;--%><strong>Turn Off Alert</strong> </td>
			                  <td><%--<input type="submit" name="Delete" value="Delete" class="input_buttons"/><input type="checkbox" />&nbsp;--%><strong>Delete</strong></td>
			</tr>
			<c:set var="index" value="${0}"></c:set>
			<c:forEach var="alert" items="${citationAlertsBean.citationAlerts}">
			
			<tr>
			  <td>
			    <a href="/aaa/ebook.jsf?bid=${alert.bookId}">${alert.bookTitle }</a> by ${alert.author } <br/>
			Published Online: ${alert.publishDate } <br/>
			
			<c:choose>
			<c:when test="${not empty alert.contentTitle}">
			<a href="/aaa/chapter.jsf?bid=${alert.bookId}&cid=${alert.contentId}&pageTab=ce">${alert.contentTitle }</a> by ${alert.author } <br/>
			${alert.page} <br/>
			DOI: <a href="/aaa/chapter.jsf?bid=${alert.bookId}&cid=${alert.contentId}&pageTab=ce">${alert.doi }</a>
			</c:when>
			
			<c:otherwise>
			DOI: <a href="/aaa/ebook.jsf?bid=${alert.bookId}">${alert.doi }</a>
			</c:otherwise>
			</c:choose>
			  
			</td>
			<td>
					
			    <select name="frequencyType${index}" size="1" >
			  <option value="0" ${alert.frequencyType == '0' ? 'selected="selected"' : ''}>Weekly</option>
			  <option value="1" ${alert.frequencyType == '1' ? 'selected="selected"' : ''}>Monthly</option>
			</select>
			<!--{alert.frequencyType} = ${alert.frequencyType} -->
			</td>
			<td><input name="emailAddress${index}" type="text" maxlength="100" value="${alert.emailAddress }" class="input_text"/></td>
			<td>
				<input type="checkbox" name="turnOff${index }" value="${alert.turnOff}" ${alert.turnOff ? 'checked="checked"' : ''}" class="input_checkbox"/>
			<!--{alert.turnOff} == ${alert.turnOff} -->
			</td>
			<td>
				<input type="checkbox" name="delete${index}" value="${alert.delete}" class="input_checkbox"/>
			<!-- {alert.delete} == ${alert.delete} -->
			</td>
			            </tr>
			            <c:set var="index" value="${index + 1 }"></c:set>
			</c:forEach>
			              </tbody>
			            </table></td>
			          </tr>
			          <tr><td colspan="2"><hr/></td></tr>
			          <tr>
			            <td colspan="2"><input type="reset" name="reset" value="Reset" class="input_buttons"/> <input type="submit" name="update" value="Update" class="input_buttons"/></td>
			          </tr>
			</table>
			</form>
	
		
		
			<!-- Footermenu Start -->
			<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
			<!-- Footermenu End -->	
		
        <!-- End Body Content -->
    	</div><!-- end content -->    
        <div class="clear"></div>
 

        
    <!-- FooterContainer Start -->
	<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    <!-- FooterContainer End -->
</div>
</body>
</html>