<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>FAQ - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>
<body>

<div id="page-wrapper"> 
	<!-- Start Header -->
	<div class="header">
           <div class="header_container">
			<div class="topheader_divider">
				<!-- Start Accessibility Menu -->
				<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
				<!-- End Accessibility Menu --> 
			</div>
	        <div class="clear"></div>
			<div class="middleheader_divider">
				<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
			</div>
		
			<div class="clear"></div>
		
			<!-- Start Topmenu -->
			<div class="topmenu">
				<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
				<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
			</div>
			<!-- End Topmenu -->     
	    </div>
	</div>  
	<!-- End Header -->	
		
	<!-- Start Middle Content -->
    <div class="breadcrumbs_search_container">
    	<!-- Search Start -->
       	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
       	<!-- Search End --> 

		<!-- Breadcrumbs Start -->
		<div class="breadcrumbs_container">
			<span class="titlepage_hidden">You are here:</span>
			<jsp:include page="/components/crumbtrail.jsp" flush="true" />
		</div>
		<!-- Breadcrumbs End -->

	</div>
    <!-- End Middle Content -->

	<!-- Start Body Content -->
	<%--	<div class="content_container link_02">
        <div class="nav_anchor">
          	<p>Choose your language:</p>
              <ul class="nav_anchor_bar">
                <li><a id="faqFaq" href="faq.jsf?languageCode=1&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="English" escapeXml="false"/></a></li>
				<li><a id="faqFaq" href="faq.jsf?languageCode=2&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="中文 (Chinese)" escapeXml="false"/></a></li>		
				<li><a id="faqFaq" href="faq.jsf?languageCode=3&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="Español" escapeXml="false"/></a></li>									
				<li><a id="faqFaq" href="faq.jsf?languageCode=4&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="日本語 (Japanese)" escapeXml="false"/></a></li>					
				<li><a id="faqFaq" href="faq.jsf?languageCode=5&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="Português" escapeXml="false"/></a></li>
              </ul>
         </div>	--%>
            <div class="clear_bottom"></div>
            
                <!-- Border Start -->
                <div class="border-landing input_text buttons">
                 	<a id="maincontent" name="maincontent" ></a>
                      <!-- Start Topmenu -->
                      <div class="topmenu">
                      	<a name="contentTab"></a>
                      	<ul>
                      		<li><div class="hide_titlepage"><h2>Page Navigation</h2></div></li>															
							<li><h3><a id="faqHti" href="help_index.jsf?stylez=${sessionScope.sessionStyle}#contentTab">Help Topic Index</a></h3></li>
							<li class="top_current"><h3>FAQs</h3></li>		 
							<li><h3><a id="faqVad" href="view_access.jsf?stylez=${sessionScope.sessionStyle}#contentTab">Diagnostics</a></h3></li>						
                         </ul> 
                         <div class="clear"></div>                      
                      </div>
                      <!-- End Topmenu -->    
                      
                      
				<div class="content_container">
					<h2>Frequently Asked Questions</h2>
					<input type="hidden" value="${faqBean.initialize}" />
					<table class="landing" summary="Frequently Asked Questions">
                          <tbody>
                    
								<c:forEach var="faqSubjectAreaTypeBooksBean" items="${faqBean.faqSubjectAreaTypeBooksBeans}">
								<c:if test="${faqSubjectAreaTypeBooksBean.languageCode eq 0 || faqSubjectAreaTypeBooksBean.languageCode == faqBean.cookieValue}">	
									<c:if test="${not empty faqSubjectAreaTypeBooksBean.subjectAreaDescription}">			
									<tr>	
										<td><h3><c:out value="${faqSubjectAreaTypeBooksBean.subjectAreaDescription}" escapeXml="false" /></h3></td>
									</tr>
									</c:if>
									<tr><td>
									<c:forEach var="faqSubjectAreaDetailsBooksBean" items="${faqSubjectAreaTypeBooksBean.faqSubjectAreaDetailsBooksBeans}">
									<c:if test="${faqSubjectAreaTypeBooksBean.languageCode eq 0 || faqSubjectAreaTypeBooksBean.languageCode == faqBean.cookieValue}">	
										<c:if test="${not empty faqSubjectAreaDetailsBooksBean.question}">
											<h4><c:out value="${faqSubjectAreaDetailsBooksBean.question}" escapeXml="false" /></h4>
										</c:if>	
										
										<c:if test="${not empty faqSubjectAreaDetailsBooksBean.answer}">	
											<c:choose>
												<c:when test="${faqSubjectAreaDetailsBooksBean.question eq 'How do I purchase access to Cambridge Books Online?'}">
													<div class="answer"><p><c:out value="Only the designated account administrator can purchase access on behalf of an organisation. To find out more about pricing or how to purchase, please " escapeXml="false" /><a href="../../aaa/templates/contact.jsf">contact us</a> .</p></div>
												</c:when>
												<c:otherwise>
													<div class="answer"><p><c:out value="${faqSubjectAreaDetailsBooksBean.answer}" escapeXml="false" /></p></div>
												</c:otherwise>
											</c:choose>	
										</c:if>			
									</c:if>
									</c:forEach>
									</td></tr>
									</c:if>
								</c:forEach>
							
						</tbody>
					</table>	
				</div>        
				<!-- End Body Content -->
       
       			</div>
                <!-- Border End -->
                  
				<div class="clear_bottom"></div>

		       <!-- Footermenu Start -->
		       <jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
		       <!-- Footermenu End -->
       
    
	</div>
	
	<!-- FooterContainer Start -->
  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    <!-- FooterContainer End -->
</div>         
</body>
</html>