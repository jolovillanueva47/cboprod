<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>View Access Details - Cambridge Books Online - Cambridge University Press</title>
    
    <jsp:include page="${pageContext.request.contextPath}/aaa/components/acc_bar.jsp"></jsp:include>
</head>
<body>
<div id="page-wrapper"> 
<!-- Start Header -->
        <div class="header">
            <div class="header_container">
				<div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="${pageContext.request.contextPath}/aaa/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
				</div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="${pageContext.request.contextPath}/aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="${pageContext.request.contextPath}/aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="${pageContext.request.contextPath}/aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="${pageContext.request.contextPath}/aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->    
		
		<a id="maincontent" name="maincontent" ></a>
		
		<!-- Content Start -->
		<div class="content_container link_02">
			
			<div class="clear_bottom"></div>
			
			<!-- Border Start <div class="border-landing input_text buttons">-->
			<div class="border-landing input_text buttons">
				
				<div class="topmenu">
					<a name="contentTab"></a>
						<ul>
							<li><div class="hide_titlepage"><h2>Page Navigation</h2></div></li>
							<li><h3><a id="faqHti" href="help_index.jsf?stylez=${sessionScope.sessionStyle}#contentTab">Help Topic Index</a></h3></li>
							<li><h3><a id="faqFaq" href="faq.jsf?stylez=${sessionScope.sessionStyle}#contentTab">FAQs</a></h3></li>		
							<li class="top_current"><h3>Diagnostics</h3></li>
						</ul> 
					<div class="clear"></div>                      
				</div>
				<!-- End Topmenu -->   
				<div class="clear"></div>   
				
				<div class="show">
			 		 <h2>Diagnostics</h2>
			  		 
			  		 <div class="clear_bottom"></div>
			  		 
			  		 <c:if test="${errorMessage_VAD != null}">
						<div class="validate">
   							<h3>Email Error Message:</h3>
							<p>Unable to send email. The following errors found.</p>
							<ol>
								<c:forEach var="error" items="${errorMessage_VAD}">
									<li><a href="#${error.key}">${error.value}</a></li>
								</c:forEach>
							</ol>
						</div>
						<div class="clear_bottom"></div>
					 </c:if>
		
					<h3><span class="titlepage_hidden">Email Form</span></h3>
		 		  
					<jsp:useBean id="accessDetailsBean" class="org.cambridge.ebooks.online.help.ViewAccessDetailsBean" scope="request"/>
		  
					<form action="email_aaa" method="post">	
					
						<%-- This determines what type of Email_aaa to instantiate--%>
						<input type="hidden" name="emailType" value="VAD" />
<!--						<c:set var="organisationInSessiontest" value="${accessDetailsBean.organisationInSession}" scope="request" ></c:set>	-->
						<input type="hidden" name="organisationInSession" value="${accessDetailsBean.organisationInSession}" />
						<input type="hidden" name="consortiaInSession" value="${accessDetailsBean.consortiaInSession}" />
						<input type="hidden" name="societyInSession" value="${accessDetailsBean.societyInSession}" />
						<input type="hidden" name="offerInSession" value="${accessDetailsBean.offerInSession}" />
														
						  <table class="landing" summary="User Access Details">
						  	<tbody>
								 <tr>
							       <%--<td colspan="3"><p>Below is a summary of user details. You may use this information in order to contact your local administrator or you can contact Cambridge University Press Customer service by filling in the form below and submitting.</p> </td>--%>
							       
							       <td colspan="3"><p>Below is a summary of your user details. You may use this information in order to work with your local administrator to troubleshoot any access issues you have, or you can contact Cambridge University Press Customer Services by filling in the form below and submitting it.</p> </td>
							     </tr>
								
								<tr>
									<td colspan="3"><h3>User Details </h3></td>
								</tr>
								
							    <tr>
									<td>Browser &amp; OS</td>
									<td colspan="2"><input type="hidden" name="browser" value="${header['user-agent']}" />	${header["user-agent"]}</td>
									
								</tr>
								
							    <tr>
									<td>IP Address</td>
									<c:choose>
										<c:when test="${empty header['X-Cluster-Client-Ip']}">
											<td colspan="2">${pageContext.request.remoteAddr}<input type="hidden" name="ipAddress" value="${pageContext.request.remoteAddr}" /></td>
										</c:when>
										<c:otherwise>
											<td  colspan="2" >${header["X-Cluster-Client-Ip"]}<input type="hidden" name="ipAddress" value="${header['X-Cluster-Client-Ip']}" /></td>
										</c:otherwise>
									</c:choose>
								</tr>
								
							    <tr>
									<td>Domain Name</td>
									<td  colspan="2"><input type="hidden" name="domainName" value="${pageContext.request.remoteHost}" />${pageContext.request.remoteHost}</td>
								</tr>
								
							    <tr>
									<td>Node</td>
									<td  colspan="2">
										<input type="hidden" name="node" value="<%= java.net.InetAddress.getLocalHost().getHostName()%>" />
										<%= java.net.InetAddress.getLocalHost().getHostName() 
											//System.getProperty("java.rmi.server.codebase")
										%>
									</td>
								</tr>
								
							    <tr>
									<td>Username</td>
									<td><c:out value="${accessDetailsBean.userId}"  /></td>
									<td><input type="hidden" name="userId" value="${accessDetailsBean.userId}" /></td>
								</tr>
							
								<tr>
									<td>Body ID</td>
									<td><c:out value="${accessDetailsBean.bodyId}" /></td>
									<td><input type="hidden" name="bodyId" value="${accessDetailsBean.bodyId}" /></td>
								</tr>
								
							    <tr>
									<td>Session ID</td>
									<td><c:out value="${accessDetailsBean.sessionId}" /></td>
									<td><input type="hidden" name="sessionId" value="${accessDetailsBean.sessionId}" /></td>
								</tr>
								
							    <tr>
									<td>User&#8217;s Full Name</td>
									<td><c:out value="${accessDetailsBean.fullName}" /></td>
									<td><input type="hidden" name="fullName" value="${accessDetailsBean.fullName}" /></td>
								</tr>
								
							    <tr>
									<td>Email Address</td>
									<td><c:out value="${accessDetailsBean.emailAddress}" /></td>
									<td><input type="hidden" name="emailAddress" value="${accessDetailsBean.emailAddress}" /></td>
								</tr>
								
							    <tr>			
									<td colspan="3" class="title" scope="rowgroup"><a name="organisations" id="organisations"></a><h3>Organisation Details</h3></td>
								</tr>
								
								<c:forEach var="organisation" items="${accessDetailsBean.organisationInSession}">
									<tr>
										<td>Body ID</td>
										<td  colspan="2"><c:out value="${organisation.bodyId}" /></td>
									</tr>
									<tr>
										<td>Display Name</td>
										<td colspan="2"><c:out value="${organisation.displayName}" /></td>
									</tr>
									<c:if test="${not empty organisation.messageText}">
										<tr>
											<td>Message</td>
											<td colspan="2"><c:out value="${organisation.messageText}" /></td>
										</tr>
									</c:if>
								</c:forEach>
									
								<tr>			
									<td colspan="3" class="title" scope="rowgroup"><h3>Consortia Details</h3> </td>
								</tr>
									
								<c:forEach var="consortia" items="${accessDetailsBean.consortiaInSession}">
									<tr>
										<td>Body ID</td>
										<td colspan="2"><c:out value="${consortia.key}" /></td>
									</tr>
									<tr>
										<td>Display Name</td>
										<td colspan="2"><c:out value="${consortia.value}" /></td>
									</tr>
								</c:forEach>
								
								<tr>			
									<td colspan="3" class="title" scope="rowgroup"><h3>Society Details</h3> </td>
								</tr>
									
								<c:forEach var="society" items="${accessDetailsBean.societyInSession}">
									<tr>
										<td>Body ID</td>
										<td colspan="2"><c:out value="${society.key}" /></td>
									</tr>
									<tr>
										<td>Display Name</td>
										<td colspan="2"><c:out value="${society.value}" /></td>
									</tr>
								</c:forEach>
								
								<tr>
									<td colspan="3" class="title" scope="rowgroup"><h3>Offer Details</h3> </td>
								</tr>
									  
								<c:forEach var="offer" items="${accessDetailsBean.offerInSession}">
									<tr>
										<td>Body ID</td>
										<td colspan="2"><c:out value="${offer.key}" /></td>
									</tr>
									<tr>
										<td>Display Name</td>
										<td colspan="2"><c:out value="${offer.value}" /></td>
									</tr>
								</c:forEach>
									
								<tr>
									<td colspan="3" class="title" scope="rowgroup"><h3>Problem  Details</h3></td>
								</tr>
							
								<tr>
									<td align="right"><label for="userEmailAddress">User email address: *</label></td>
									<td colspan="2"><input id="userEmailAddress" type="text" name="userEmailAddress" maxlength="80" value="${param.email != null ? param.userEmailAddress : accessDetailsBean.userEmailAddress}" class="input_text"/></td>		
								</tr>
								
							    <tr>
									<td align="right"><label for="subject">Email subject:</label></td>
									<td colspan="2"><input id="subject" type="text" name="subject" value="${param.email != null  ? param.subject : accessDetailsBean.emailSubject }" class="input_text"/></td>		
								</tr>
								<tr>
									<td align="right"><label for="typeOfProblem">Type of problem: *</label></td>
									<td colspan="2">
									<select name="typeOfProblem" id="typeOfProblem">
										<option value="">Select type of problem ...</option>
											<%
												String[] stringArray = {
													"Account Administrator",
													"Access to Content", 
													//"Article Purchase", 
													"Error Message", 
													"IP Recognition",
													"Login", 
													"Password",
													"Registration", 
													"Remote Access", 
													"Searching", 
													//"Society Member Access", 
													//"Subscription Activation", 
													"Usage Statistics", 
													"Other" 
												};
											String[] locationNames = {
													//"uk:UK, Europe and the rest of the world",
													//"us:US, Mexico and Canada"
													"us:The Americas",
													"uk:United Kingdom and Rest of World"
												};
										
										String paramLocation = request.getParameter("location");
										%>	
										<c:choose>
											<c:when test="${not empty param.reset}">
												<c:forEach var="problemName" items="<%=stringArray%>">
													<option value="${problemName}">${problemName}</option>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<c:forEach var="problemName" items="<%=stringArray%>">
													<c:choose>
														<c:when test="${param.typeOfProblem == problemName}">
															<option selected="selected" value="${problemName}">${problemName}</option>
														</c:when>
														<c:otherwise>
															<option value="${problemName}">${problemName}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
										
									</select>
									</td>
								</tr>
							    				
							    <tr>
									<td align="right"><label for="problemDescription">Problem description: *</label></td>
									<td colspan="2"><textarea id="problemDescription" name="problemDescription" cols="1" rows="5">${param.email != null  ? param.problemDescription : '' }</textarea></td>	
								</tr>
								
							    <tr>
									<td align="right"><label for="locations">Your location:</label></td>
									<td colspan="2">
											
										<select name="location" id="locations">
											<c:forEach var="locationName" items="<%=locationNames%>">
												<c:choose>
													<c:when test="${fn:contains(param.location,locationName)}">
														<option selected="selected" value="${locationName}">${fn:substringAfter(locationName, ":")}</option>
													</c:when>
													<c:otherwise>
														<option value="${locationName}">${fn:substringAfter(locationName, ":")}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>											
										</select>
									</td>
								</tr>
							    
								<tr>
							        <td colspan="3" ><span class="note">*</span> required</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td colspan="2"><input type="submit" name="reset_aaa" value="Reset" class="input_buttons"/> <input type="submit" name="email" value="Email" class="input_buttons"/></td>
								</tr>
							</tbody>
						</table>
				
					</form>	
				
				<%-- 	    
			    <div id="bottomButtons">
			        <%--<h:commandButton value="Reset" type="reset" />
					<h:commandButton value="Email" id="email_send" action="#{accessDetailsBean.sendEmail}" />
					<h:message for="email_send"/>
			    </div>
			    --%>
			    
			</div>
			<!-- end div show -->
		</div>
		
		<div class="clear_bottom"></div>
	   
	   <!-- Footermenu Start -->
        	<jsp:include page="${pageContext.request.contextPath}/aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        <!-- Footermenu End -->
 
	</div>
    <!-- Content End -->

    	<!-- FooterContainer Start -->
  	  		<jsp:include page="${pageContext.request.contextPath}/aaa/components/footer_container.jsp" flush="true"></jsp:include>
    	<!-- FooterContainer End -->
</div>
</body>
</html>