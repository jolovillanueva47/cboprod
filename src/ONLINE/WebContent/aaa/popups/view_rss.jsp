<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@page import="org.cambridge.ebooks.online.rss.RssServlet"%>
<%@page import="org.cambridge.ebooks.online.rss.RssServlet_aaa"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.cambridge.ebooks.online.jpa.rss.Rss"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 <meta http-equiv="X-UA-Compatible" content="IE=7" />
	 
<!--	<title>Rss Feeds</title>    -->

	<title>Save Query as RSS Feed - Cambridge Books Online - Cambridge University Press</title>
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>

<body>
<%
	//String[] urlTitle = RssServlet.getRssUrlTitle(request);
	String[] urlTitle = RssServlet_aaa.getRssUrlTitle(request);
	String url = urlTitle[0]; 
	String title = urlTitle[1];
%>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
	    	<div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
	    	</div>
		</div>  
		<!-- End Header -->

		<!-- Start Middle Content -->
		<div class="breadcrumbs_search_container">
			<!-- Search Start -->
			<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
			<!-- Search End --> 
			
			<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
	   			<gui:crumbtrail propertyFile="org.cambridge.ebooks.online.crumbtrail.crumb"
	  				lastElementStyle="last"
	  				userCrumb="org.cambridge.ebooks.online.crumbtrail.UserCrumb" >
	  			</gui:crumbtrail>
			</div>
			<!-- Breadcrumbs End -->
		</div>
       <!-- End Middle Content -->
		  
 		<a id="maincontent" name="maincontent" ></a>
 		                 
		<!-- Start Body Content -->
		<div class="content_container link_02">
			
			<div class="clear_bottom"></div>
			
			<!-- Titlepage Start -->
            <h1>Save Query as RSS Feed</h1>   
            <!-- Titlepage End -->
            
          	<div class="clear_bottom"></div>
<form action="view_rss.jsf?" >           
            <table class="global_forms input_text buttons" cellspacing="0">
               <tr>
               		<td>You may copy this link to your RSS reader:</td>
               </tr>
               <tr><td><hr /></td></tr>
               <tr>
               		<td>
                    	<a href="<%=url%>" ><%=url%></a>
                    	<input type="hidden" value="<%=url%>" name="rssUrl" id="rssUrl"/>
                    </td>
               </tr>
               <tr><td><hr /></td></tr>

               	
             
               	<tr>
               		<td><label for="feedTitle">Feed Title:</label> <input id="feedTitle"  name="FEED_TITLE" type="text" style="width: 450px;" value="<%=title %>" class="input_text"/></td>
              	</tr>
                <tr>
                	<td></td>
                </tr>
                	<tr><td><hr /></td></tr>
                 <tr>
                 	<td><input type="submit" class="input_buttons" value="Update Title" name="updateTitle"/></td>
                 </tr>          
                                            
                                      
			</table>	
  	</form>		
			
			<div class="clear_bottom"></div>
	       
	        <!-- Footermenu Start -->
	        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
	        <!-- Footermenu End -->
		
		</div>        
		<!-- End Body Content -->
       
    	<!-- FooterContainer Start -->
  	  		<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    	<!-- FooterContainer End -->

</div>	
</body>
</html>


<%-- 
<div class="content" style="height: 150px;"> 
	<ul>		 
    	<li class="header">Save Query as RSS Feed</li>
    	<li>You may copy this link to your RSS reader:</li>
       	<li class="bookLink" ><a id="bookLink" href="<%=url%>" target="_blank"><%=url %></a></li>
       	<li>&nbsp;</li>
       	<li>       		
       		Feed Title: <input type="text" id="feedTitle" value="<%=title %>" style="width:450px;" />
       	</li>
       	<!-- use the -->
       	<!-- 
       	<li>&nbsp;</li>    	    	
       	<li>You can also subscribe to this feed using:
       		<ul>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('bloglines')" alt="bloglines" src="${pageContext.request.contextPath}/images/bloglines.gif" />
       			</li>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('yahoo')" alt="yahoo" src="${pageContext.request.contextPath}/images/yahoo.gif" />
       			</li>
       			<li>
       				<img style="cursor: pointer;" onclick="saveRss('google')" alt="google" src="${pageContext.request.contextPath}/images/google.gif" />
       			</li>
       		</ul>
       	</li>
       	 -->       	     	
	</ul>	
	<div>
		<span class="button">
	    	<input type="button" value="Update Title" onclick="updateTitle()"/>			
	    </span>
	</div>   		     	
</div>
	<script type="text/javascript">
		<%--			
			String urlEncoded1 = URLEncoder.encode(url, "UTF-8");
			String urlEncoded2 = URLEncoder.encode(urlEncoded1, "UTF-8");
		--%>
		
<%-- 		
		function saveRss(value){			
			var url = $(document.getElementById("bookLink")).attr("href");					
			//var urlEnc = encodeURIComponent(url);
			var urlEnc = encodeURIComponent(url);
			var newUrl = "";
			if(value == "bloglines") {
				newUrl = "http://www.bloglines.com/sub/" + url;																
			}else if(value == "google") {				 
				//custom replace all is required to be read by google and yahoo correctly
				//newUrl ="http://www.google.com/ig/add?feedurl="  + urlEnc.replace(/amp%3B/g,"");				
				newUrl ="http://www.google.com/ig/add?feedurl="  + urlEnc;
			}else if(value == "yahoo") {
				//newUrl = "http://add.my.yahoo.com/rss?url=" + urlEnc.replace(/amp%3B/g,"");					
				newUrl = "http://add.my.yahoo.com/rss?url=" + urlEnc;
			}
			MM_openWindow(newUrl,'newWindow','scrollbars=yes');
		}
		
		function updateTitle(){
			var bookLinkJQ = $(document.getElementById("bookLink"));
			var bookLinkStr = bookLinkJQ.attr("href");			
			var newUrl = removeTitle(bookLinkStr);
			var newTitle = document.getElementById("feedTitle").value;
			newUrl = newUrl + "<%=RssServlet.FEED_TITLE_PARAM %>=" + newTitle;
			
			bookLinkJQ.html(newUrl);
			bookLinkJQ.attr("href", newUrl);
		}

		function removeTitle(url){			
			var newUrl = url.split("<%=RssServlet.FEED_TITLE_PARAM %>")[0];
			return newUrl;
		} 
	</script>

</body>--%>