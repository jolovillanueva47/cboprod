<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="org.cambridge.ebooks.online.email.EmailBooklinkBean"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Email link to this book - Cambridge Books Online - Cambridge University Press</title>
	
	<jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
	
</head>

<body>





<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      
        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
 	    	
	    	<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
	      	
        </div>
        <!-- End Middle Content -->             
        
        <a id="maincontent" name="maincontent" ></a>
             
		<!-- Start Body Content -->
		<div class="content_container link_02">

			<div class="clear"/>
			
      		<!-- Titlepage Start -->
            <h2>Email link to this book</h2>   
            <!-- Titlepage End -->
            
            <div class="clear_bottom"></div>
 
 			<jsp:useBean id="emailBooklink" class="org.cambridge.ebooks.online.email.EmailBooklinkBean" scope="request"></jsp:useBean>
   			
   			
   		
   			<c:choose>
   				<c:when test="${errorMessage_BL_request == null }">
   					<c:set var="errorMessage_BL_request" value="${errorMessage_BL}" scope="request" />
   				</c:when>
   				<c:when test="">
   					
   				</c:when>
   				<c:otherwise>
   					
   				</c:otherwise>
   			</c:choose>

   			<c:if test="${errorMessage_BL_request != null}">
   				<div class="validate">
   					<h3>Email Error Message:</h3>
					<p>Unable to send email. The following errors found.</p>
					<ol>
						<c:forEach var="error" items="${errorMessage_BL_request}">
							<li><a href="#${error.key}" >${error.value}</a></li>
						</c:forEach>
					</ol>
				</div>
				<div class="clear_bottom"></div>
          	</c:if>
   			
    		<c:if test="${emailBooklink.displayDetails && empty param.success}">    
    			<form action="email_aaa" method="post" >     
    				<%-- This determines what type of Email_aaa to instantiate--%>
					<input type="hidden" name="emailType" value="BL" />


					<input type="hidden" name="Title" value="${param.Title}"/>
					<input type="hidden" name="Copyright" value="${param.Copyright}"/>
					<input type="hidden" name="PrintpubDate" value="${param.PrintpubDate}"/>
					<input type="hidden" name="OnlinepubDate" value="${param.OnlinepubDate}"/>
					<input type="hidden" name="OnlineIsbn" value="${param.OnlineIsbn}"/>
					<input type="hidden" name="HardbackIsbn" value="${param.HardbackIsbn}"/>
					<input type="hidden" name="Doi" value="${param.Doi}"/>
					<input type="hidden" name="exactURL" value="${param.exactURL}"/>
					<input type="hidden" name="successBookLink" value="${emailBooklink.bookLink}" />
				 
					
					<span class="titlepage_hidden"><h3>Email Form</h3></span>
					
		            <table class="global_forms input_text buttons" cellspacing="0">
		                      <tr>
		                        <td colspan="2">To send this book link to a colleague, enter your contact information and their name and their email address in the boxes below. Use the "Message" box to add your own message.</td>
		                      </tr>
		                      <tr><td colspan="2"><hr /></td></tr>
		                      <tr>
		                        <td colspan="2"><a href="${emailBooklink.bookLink}" id="testing">${emailBooklink.bookLink}</a></td>
		                      </tr>
		                      <tr><td colspan="2"><hr /></td></tr>
		                      <tr>
		                            <td width="20%" align="right"><label for="userName">Your name*:</label></td>
		                            <td width="80%"><input class="input_text"id="userName" name="userName" value="${param.reset == 'all' ? emailBooklink.userName : not empty param.userName ? param.userName : emailBooklink.userName }" type="text" size="40"/></td>
		                      </tr>
		                       <tr>
		                            <td align="right"><label for="userEmail">Your e-mail address*:</label></td>
		                            <td><input class="input_text" id="userEmail" name="userEmail" value="${param.reset == 'all' ? emailBooklink.userEmail : not empty param.userEmail ? param.userEmail : emailBooklink.userEmail }" type="text" size="40"/></td>
		                      </tr>
		                      <tr>
		                            <td align="right"><label for="recipientName">Recipient's name*:</label></td>
		                            <td><input class="input_text" id="recipientName" name="recipientName" value="${param.reset == 'all' ? '' : param.recipientName}" type="text" size="40"/></td>
		                      </tr>
		                      <tr>
		                            <td align="right"><label for="recipientEmail">Recipient's Email Address*:</label></td>
		                            <td><input class="input_text" id="recipientEmail" name="recipientEmail" value="${param.reset == 'all' ? '' : param.recipientEmail}" type="text" size="40"/></td>
		                      </tr>
		                      <tr>
                            		<td align="right"><label for="subject">Subject:</label></td>
                            		<td><input class="input_text" id="subject" name="subject" value="${param.submit != null  ? param.subject : emailBooklink.subject }" type="text" size="40"/></td>
                      		  </tr>
		                      <tr>
		                            <td align="right"><label for="mailmessage">Message:</label></td>
		                            <td>
		                            	<c:choose>	
											<c:when test="${not empty eBookLandingPageBean.bookTitle}" >
												${eBookLandingPageBean.bookTitle}
											</c:when>
					
											<c:otherwise>
												${chapterLandingPageBean.bookTitle}
											</c:otherwise>
										</c:choose>
		                            	<textarea id="mailmessage" title="Scroll-up to view System Generated Message" name="mailmessage" cols="1" rows="5" onfocus="doFocusCursorTop('mailmessage');" onmouseover="removeTitle('mailmessage');">${param.submit != null  ? param.mailmessage : emailBooklink.mailmessage }</textarea></td>
		                      </tr>
		                      <tr>
		                        <td colspan="2"></td>
		                      </tr>
		                      <tr><td colspan="2"><hr /></td></tr>
		                      <tr>
		                        <%--<td colspan="2"><input type="reset" class="input_buttons" name="reset" value="Reset" /> <input type="submit" class="input_buttons" name="submit" value="Submit" /></td>--%>
		                        <td colspan="2"><input type="submit" class="input_buttons" name="reset_aaa" value="Reset" /> <input type="submit" class="input_buttons" name="submit" value="Submit" /></td>
		                      </tr>                                
		                    </table>
		    	</form>
         	</c:if>  
         	
			<div class="clear_bottom"></div>

            <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		
        
	</div> 
		
		<!-- FooterContainer Start -->
	  	 <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->

<script type="text/javascript">
	function doFocusCursorTop(areaId) {
		var txtarea = document.getElementById(areaId); 
 
		var strPos = 0; 
		

		setCaretPosition(txtarea, strPos);

		var current = doGetCaretPosition(txtarea);
		var last = txtarea.value.length;
		
		setSelection(txtarea, current, last)
		
		
	}
	
	function setCaretPosition(ctrl, pos){
		if(ctrl.setSelectionRange)
		{
			ctrl.focus();
			ctrl.setSelectionRange(pos,pos);
		}
		else if (ctrl.createTextRange) {
			var range = ctrl.createTextRange();
			range.collapse(true);
			range.moveEnd('character', pos);
			range.moveStart('character', pos);
			range.select();
		}
	}
	function doGetCaretPosition (ctrl) {
		var CaretPos = 0;	// IE Support
		if (document.selection) {
		ctrl.focus ();
			var Sel = document.selection.createRange ();
			Sel.moveStart ('character', -ctrl.value.length);
			CaretPos = Sel.text.length;
		}
		// Firefox support
		else if (ctrl.selectionStart || ctrl.selectionStart == '0')
			CaretPos = ctrl.selectionStart;
		return (CaretPos);
	}
		
	function setSelection(ctrl, start, stop){

		if(ctrl.setSelectionRange)
		{
			ctrl.focus();
			ctrl.setSelectionRange(start,stop);
		}
		else if (ctrl.createTextRange) {
			var range = ctrl.createTextRange();
			range.collapse(true);
			range.moveEnd('character', start);
			range.moveStart('character', stop);
			range.select();
		}

	}
	function removeTitle(id){
		var txtarea = document.getElementById(id); 
		txtarea.setAttribute("title","");
		txtarea.onmouseover = hideTooltip;
		
	}
</script>


</body>

</html>