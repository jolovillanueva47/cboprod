<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Help Topic Index - Cambridge Books Online - Cambridge University Press</title>    
    
    <link href="css/aaa_style.css" rel="stylesheet" type="text/css" />
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
    
</head>
<body>
${helpBean.initializeHelpIndex}
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
	    	<div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
	    	</div>
		</div>  
		<!-- End Header -->
	    
  		<div class="clear_bottom"></div>
 
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->   
		<div class="content_container link_02">
	<%--	<div class="nav_anchor">
          	<p>Choose your language:</p>
              <ul class="nav_anchor_bar">
                <li><a id="faqHti" href="help_index.jsf?languageCode=1&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="English" escapeXml="false"/></a></li>
				<li><a id="faqHti" href="help_index.jsf?languageCode=2&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="中文 (Chinese)" escapeXml="false"/></a></li>		
				<li><a id="faqHti" href="help_index.jsf?languageCode=3&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="Español" escapeXml="false"/></a></li>									
				<li><a id="faqHti" href="help_index.jsf?languageCode=4&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="日本語 (Japanese)" escapeXml="false"/></a></li>					
				<li><a id="faqHti" href="help_index.jsf?languageCode=5&amp;stylez=${sessionScope.sessionStyle}#contentTab" onclick="${faqBean.cookieValue}"><c:out value="Português" escapeXml="false"/></a></li>
              </ul>
         </div>	--%>	          
		<!-- Content Start -->
		
			<div class="clear_bottom"></div>
			<!-- Border Start -->
			<div class="border-landing input_text buttons">
			<a id="maincontent" name="maincontent" ></a>	
				<!-- Start Topmenu -->   
				<div class="topmenu">
					<a name="contentTab"></a>
						<ul>
							<li><div class="hide_titlepage"><h2>Page Navigation</h2></div></li>
							<li class="top_current"><h3>Help Topic Index</h3></li>
							<li><h3><a id="faqFaq" href="faq.jsf?stylez=${sessionScope.sessionStyle}#contentTab">FAQs</a></h3></li>		
							<li><h3><a id="faqVad" href="view_access.jsf?stylez=${sessionScope.sessionStyle}#contentTab">Diagnostics</a></h3></li>
						</ul> 
					<div class="clear"></div>                      
				</div>
				<!-- End Topmenu -->   
	            
	            <div class="clear"></div>
	                        
				<!-- Help Topic Start -->
				<div class="show">
				
					<!-- Titlepage Start -->
						<h2>Help Topic Index</h2> 
					<!-- Titlepage End --> 
					<div class="help_topics">
						<ul>
							<c:forEach var="helpBooksBean" items="${helpBean.helpBooksBeans}">	
							<c:if test="${helpBooksBean.languageCode == faqBean.cookieValue || helpBooksBean.languageCode eq 0}">				
								<li><span class="icons_img bullet_01">&nbsp;</span>
									<a href="${pageContext.request.contextPath}/aaa/popups/help.jsf?pageId=${helpBooksBean.pageId}&amp;pageName=${helpBooksBean.pageName}" >
										<c:out value="${helpBooksBean.pageName}" />
									</a>
								</li>
							</c:if>
							</c:forEach>
						</ul>
					</div>
				</div>        
				<!-- End Body Content -->
				
			</div>
			<!-- Border End -->
	       
	        <div class="clear"></div>
 		
 		<!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        <!-- Footermenu End -->
       
		</div>

    	<!-- FooterContainer Start -->
  	  		<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    	<!-- FooterContainer End -->
       
</div>        
</body>
</html>