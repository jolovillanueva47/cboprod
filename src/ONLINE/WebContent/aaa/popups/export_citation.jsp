<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@page import="org.cambridge.ebooks.online.util.SolrUtil"%>
<%@page import="org.cambridge.ebooks.online.util.UrlUtil"%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	<title>Export Citation - Cambridge Books Online - Cambridge University Press</title>
	<jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
</head>
<body>
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->      

		<a id="maincontent" name="maincontent" ></a>
		        
        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
 	    	
	    	<!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->  
	      	
        </div>
        <!-- End Middle Content -->             
               
		<!-- Start Body Content -->
		<div class="content_container link_02">

        	<div class="clear_bottom"></div>
        	
      		<!-- Titlepage Start -->
            <h2>Export Citation</h2>   
            <!-- Titlepage End -->
            
            <div class="clear_bottom"></div>
            
            <c:if test="${errorMessage_EC != null}">
			<div class="validate">
   				<h3>Email Error Message:</h3>
				<p>Unable to send email. The following errors found.</p>
				<ol>
					<c:forEach var="error" items="${errorMessage_EC}">
						<li><a href="#${error.key}">${error.value}</a></li>
					</c:forEach>
				</ol>
			</div>
			<div class="clear_bottom"></div>
		</c:if>
            
           
            

<!--<form id="exportCitationForm" action="email_aaa?isbn=${param.isbn}&id=${param.id}&author=${param.author}&bookTitle=${param.bookTitle}&contentTitle=${param.contentTitle}&printDate=${param.printDate}&doi=${param.doi}&bookId=${param.bookId}&contentId=${param.contentId}&contributor=${param.contributor}" method="post">-->
       		<%-- This determines what type of Email_aaa to instantiate
			<input type="hidden" name="emailType" value="EC" />
			<input type="hidden" name="successBookLink" value="<%=System.getProperty("ebooks.context.path")%>aaa/ebook.jsf?bid=${param.id}" />
			<input type="hidden" name="Title" value="${param.bookTitle}" />
            
            <span class="titlepage_hidden"><h3>Email Form</h3></span>--%>
 
 <form id="exportCitationForm" action="/export_citation_aaa?isbn=${param.isbn}&id=${param.id}&author=${param.author}&bookTitle=${param.bookTitle}&contentTitle=${param.contentTitle}&printDate=${param.printDate}&doi=${param.doi}&bookId=${param.bookId}&contentId=${param.contentId}&contributor=${param.contributor}" method="post">                           

            <table class="global_forms input_text buttons" cellspacing="0">
                      <tr>
                        <td colspan="2">You can download citations to your desktop or you can email them to a colleague.</td>
                      </tr>
                      <tr><td colspan="2"><hr></td></tr>
                      <tr>
                      	    <%
								if(request.getParameter("download") != null && request.getParameter("download").equals("none")) {
									out.write("<td></td>");
									out.write("<td style='color: #FF0000;'>No citation to be downloaded.</td>");
											
								} else if(request.getParameter("email") != null && request.getParameter("email").equals("success")) {
									out.write("<td></td>");
									out.write("<td style='color: #FF0000;'>Your message was sent.</td>");
								} else if(request.getParameter("email") != null && request.getParameter("email").equals("fail")) {
									out.write("<td></td>");
									out.write("<td style='color: #FF0000;'>Your message was not sent. Please try again.</td>");
								}
							%>                       
                      </tr>
                      <tr>                      
                        <td colspan="2"><table class="landing" summary="Citation Alert">
                          <tbody>
                            <tr>
                              <td><strong>Include extract for this citation:</strong></td>
                            </tr>
                            <c:choose>
                            	<c:when test="${param.includeAbstract eq 'true' && param.email != 'reset' }">
	                            	<tr>
		                              <td>
		                              	<input id="yes" name="includeAbstract" value="true" checked="checked" type="radio" /><label for="yes">Yes</label>
		                              </td>
		                            </tr>
		                            <tr>
		                              <td>                              	
		                              	<input id="no" name="includeAbstract" value="false" type="radio" /><label for="no">No</label>
		                              </td>
		                            </tr>
                            	</c:when>
                            	<c:otherwise>
                            		<tr>
		                              <td>
		                              	<input id="yes" name="includeAbstract" value="true" type="radio" /><label for="yes">Yes</label>
		                              </td>
		                            </tr>
		                            <tr>
		                              <td>                              	
		                              	<input id="no" name="includeAbstract" value="false" checked="checked" type="radio" /><label for="no">No</label>
		                              </td>
		                            </tr>
                            	</c:otherwise>
                            </c:choose>
                            <tr>
                              <td><strong>Choose file format:</strong></td>
                            </tr>
                            <%
                            
                            	String[] fileformats ={
                            		"ASCII",
                            		"Bibliscape",
                            		"BibTex",
                            		"CSV",
                            		"Endnote",
                            		"HTML",
                            		"Medlars",
                            		"Papyrus",
                            		"ProCite",
                            		"ReferenceManager",
                            		"RefWorks",
                            		"RIS"
                            		};
                            
                            	String pFileFormat = request.getParameter("fileFormat");
                            	boolean isReset = request.getParameter("email") != null && request.getParameter("email").equals("reset") ? true : false ; 
                            	boolean isNullFormat = pFileFormat == null ? true : false;
                            	

                            	for(String item : fileformats ){
                            		out.print("<tr>");
                            		out.print("<td>");
                            		out.print("<input id=\"" + item + "\"");
                            		out.print(" name=\"fileFormat\" value=\"" + item + "\"");
                            		if(isNullFormat && item.equals("CSV")){
                            			out.print(" checked=\"checked\"");
                            		}else if(!isNullFormat){
                            			boolean isSame = pFileFormat.equals(item);
                                		if(isSame && !isReset){
                                			out.print(" checked=\"checked\"");
                                		}else if(isReset && item.equals("CSV")){
                                			out.print(" checked=\"checked\"");
                                		}
                            		}
                            		
                            		out.print(" type=\"radio\" />");
                            		out.print("<label for=\""+item+"\">" + item +"</label>");
                            		out.print("</td>");
                            		out.print("</tr>");
                            		
                            		
                            	}
                            
                            %>
							<tr>
								<td><strong>Export citations directly into your citation software.</strong></td>
							</tr>
							<tr> 
								<td><label for="DirExpRefWorks">RefWorks (Direct Export)</label> <input name="format" value="DirExpRefWorks" id="DirExpRefWorks" type="radio"/></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="submit" name="export_aaa" class="input_buttons" value="Export" onclick="execute();"/>
								</td>
							</tr>
							
                            <tr>
                              <td><strong>To email citation to a colleague, enter email address below:</strong></td>
                            </tr>
                            <tr>
                              <td><label for="emailId">Email:</label> <input id="emailId" name="emailAdd" type="text" class="input_text" value="${param.email eq 'reset' ? '' : param.emailAdd }"/></td>
                            </tr>
                          </tbody>
                        </table></td>
                      </tr>
                      <tr><td colspan="2"><hr></td></tr>
                      <tr>
                        <td colspan="2">
							<input type="submit" name="reset_aaa" class="input_buttons" value="Reset"/>
                            <input type="submit" name="type" value="download" class="input_buttons" />
                            <input type="submit" name="type" value="email" class="input_buttons" />
                        </td>
                      </tr>                                
                    </table>

<textarea id="ImportData" name="ImportData" style="display:none;" rows=30 cols=70>
RT Book, Whole
SR Print(0)
ID 
A1 ${sessionScope.authorNamesSession}
T1 ${sessionScope.bookTitleSession} 
YR ${param.printDate}
VO ${param.volume}
AB <%= SolrUtil.getAbstractByIsbn(request.getParameter("isbn"))%>
PB <%= SolrUtil.getPublisherNameByIsbn(request.getParameter("isbn"))%>
PP <%= SolrUtil.getPublisherLocationByIsbn(request.getParameter("isbn"))%>
SN ${param.isbn}
LA English
LK ${param.doi}
DO ${param.doi}
DB 
UL <%= UrlUtil.getClientDns(request) %>ebook.jsf?bid=${param.id}
WT Cambridge Books Online
OL English(30)
</textarea>
                  
				<div class="clear_bottom"></div>
</form>

            <!-- Footermenu Start -->
        	<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 

<script language="javascript">	
		
		function execute(){
			var textareaVal = document.getElementById("ImportData").value; 	
			var form1= document.forms['exportCitationForm'];
			form1.action="http://www.refworks.com/express/expressimport.asp?vendor=Cambridge%20Books%20Online&database=RWCambridgeUPressDB&filter=RefWorks%20Tagged%20Format&WNCLang=false"+textareaVal;
			form1.target="RefWorksMain";
			form1.method="POST";
			//self.parent.tb_remove();

			document.getElementById("exportCitationForm").submit();
		}
	</script>

</body>

</html>