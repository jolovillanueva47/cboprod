<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Citation Alert - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../../aaa/components/acc_bar.jsp"></jsp:include>
    
</head>

<body>
${eBookLandingPageBean.initPage}
	<% 
		session.setAttribute("helpType", "child");
		session.setAttribute("pageId", "1532");
		session.setAttribute("pageName", "Chapter Landing");
	%>
	
<div id="page-wrapper"> 
	<input type="hidden" id="pageLoader" value="${chapterLandingPageBean.initPage}"/>
	<input type="hidden" id="references" value="${chapterLandingPageBean.references}"/>
	<input type="hidden" id="tabSelect" value="${chapterLandingPageBean.tabSelect}"/>
	
	<input type="hidden" id="isbnHidden" value="${chapterLandingPageBean.onlineIsbn}" />
	<input type="hidden" id="bookIdHidden" value="${chapterLandingPageBean.bookId}" />
	<input type="hidden" id="idHidden" value="${chapterLandingPageBean.contentId}" />
	<input type="hidden" id="pageType" value="chapter" />
	
	<c:set var="refSelected" value="${not empty param.ref}" />
	<c:set var="pageTab" value="${param.pageTab == null ? 'toc' : param.pageTab}"/>	 
			
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
				<div class="topheader_divider">
					<!-- Start Accessibility Menu -->
					<jsp:include page="../../aaa/components/acc_links.jsp"></jsp:include>
					<!-- End Accessibility Menu --> 
				</div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
				<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->	
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->    
		
		<a id="maincontent" name="maincontent" ></a>
		
		<div class="content_container link_02">
        
        	<div class="clear_bottom"></div>
        
      		<!-- Titlepage Start -->
            <h2>Citation Alert</h2>   
            <!-- Titlepage End -->
            
            <div class="clear_bottom"></div>
            
	        <table class="global_forms input_text buttons" cellspacing="0"> 
	            <tr>
			    	<td>You will receive an e-mail when the books or chapters below are cited by other publications.</td>
				</tr>
				
				<tr>
					<td><hr /></td>
				</tr>
				
				<tr>
					<%-- <td>You need to <a href="../../aaa/register.jsf">register</a>--%>
					<td>You need to <a href="<%= System.getProperty("cjo.url") %>registration_aaa?displayname=${referalName}&stylez=${sessionStyle == null ? 'default_style' : sessionStyle}" >register</a>
						 or <a href="../../aaa/login.jsf">login</a> to use this feature.</td>
			
					<%--td>You need to register or login to use this feature.</td--%>		
			    </tr>
			</table>
		
		
			<!-- Footermenu Start -->
			<jsp:include page="../../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
			<!-- Footermenu End -->	
		</div>        
        <!-- End Body Content -->
        
        <div class="clear"></div>
 

        
    <!-- FooterContainer Start -->
	<jsp:include page="../../aaa/components/footer_container.jsp" flush="true"></jsp:include>
    <!-- FooterContainer End -->
</div>
</body>
</html>