<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Sitemap - Cambridge Books Online - Cambridge University Press</title>    
    
    <jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>
</head>
<body>
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
						<jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
				<div class="middleheader_divider">
					<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->
		
		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	
			<span class="titlepage_hidden">You are here:</span>
			<jsp:include page="../components/crumbtrail.jsp" />
 		</div>
        <!-- End Middle Content -->               
 		
 		<a id="maincontent" name="maincontent" ></a>
 		                 
		<!-- Start Body Content -->
		<div class="content_container">	
			
			<!-- Titlepage Start -->
			<h2>Site Map</h2>   
			<!-- Titlepage End -->
			
			<div class="clear_bottom"></div>
			
			<!-- Nav anchor Start -->
			<div class="nav_anchor">
				<p>On this page:</p>
				<ul class="nav_anchor_bar">
					<li><a href="#parent-1">Accessing Content</a></li>
					<li><a href="#parent-2">Searching Content</a></li>
					<li><a href="#parent-3">User Registration</a></li>
					<li><a href="#parent-4">General Information</a></li>
					<li><a href="#parent-5">Account Personalization</a></li>
					<li><a href="#parent-6">Account Administrator</a></li>
					<li><a href="#parent-7">Help Features</a></li>
				</ul>
			</div>
			<!-- Nav anchor End -->
			
			<div class="clear"></div>
			
			<c:set var="isLogIn" value="${eBookLogoBean.userLoggedIn}" />			
			<c:set var="isAdmin" value="${'AO' eq orgConLinkedMap.userLogin.userType or 'AC' eq orgConLinkedMap.userLogin.userType}"/>
			
			<!-- Treelist Start -->
			<div class="treelist">
				<ul><!-- Start Level 1 --> 
					<li id="parent-1"><span class="icons_img bullet_01">&nbsp;</span><h3>Accessing Content</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">Featured Titles</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">Featured Collections</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/subject_tree.jsf">Browse by Subject</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Book Landing Page</h4> 
								<ul><!-- Start Level 3 -->
									<li>Book Description</li>
									<li>Table of Contents</li>
									<li>References</li>
									<li>Full-text PDF</li>
								</ul>
							</li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Chapter Landing Page</h4> 
								<ul><!-- Start Level 3 -->
									<li>Chapter Extract</li>
									<li>Table of Contents</li>
									<li>References</li>
									<li>Full-text PDF</li>
								</ul>
							</li>                            
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Email Link to This Book</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>How to Cite</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Export Citation</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Alert Me When This Book is Cited</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Cited By</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Link to This Book</h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Recommend This to a Librarian</h4></li>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-2"><span class="icons_img bullet_01">&nbsp;</span><h3>Searching Content</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">Quick Search</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/advance_search.jsf">Advanced Search</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4>Search Results</h4></li>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-3"><span class="icons_img bullet_01">&nbsp;</span><h3>User Registration</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<c:choose>
								<c:when test="${eBookLogoBean.userLoggedIn}">
									<li><span class="icons_img bullet_01">&nbsp;</span><h4>Registration</h4></li>
								</c:when>
								<c:otherwise>
									<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/registration?displayname=">Registration</a></h4></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-4"><span class="icons_img bullet_01">&nbsp;</span><h3>General Information</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">Home</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/templates/about.jsf">About CBO</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">News and Events</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/accmanagement?topage=stream&amp;pageId=5048&amp;ORG_ID_SET=">For Librarians</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="http://www.cambridge.org/online/">Other Online Products from Cambridge</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="http://cambridge.org/gb/knowledge/home/item2273191/">Academic and Professional Books</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/templates/contact.jsf">Contact Us</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/templates/accessibility.jsf">Accessibility</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/templates/terms_of_use.jsf">Terms of Use</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/templates/privacy_policy.jsf">Privacy Policy</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/templates/rights_and_permissions.jsf">Rights and Permissions</a></h4></li>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-5"><span class="icons_img bullet_01">&nbsp;</span><h3>Account Personalization</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<c:choose>
								<c:when test="${eBookLogoBean.userLoggedIn}">
									<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/accmanagement?topage=updateRegistration">Change Registration Details</a></h4></li>
									<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/manageContentAlerts">My Content Alerts</a></h4></li>
								</c:when>
								<c:otherwise>
									<li><span class="icons_img bullet_01">&nbsp;</span><h4>Change Registration Details</h4></li>
									<li><span class="icons_img bullet_01">&nbsp;</span><h4>My Content Alerts</h4></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-6"><span class="icons_img bullet_01">&nbsp;</span><h3>Account Administrator</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
						
						<c:choose>
							<c:when test="${isAdmin}">
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/configureIPDomain">Configure IP Address</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/subscriptionDetails">Access to</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/remoteUserAccess">Remote User Access</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/counter">Usage Statistics</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/openURLResolver">Open URL Resolver</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/updateOrganisationDetails">Update Organisation Details</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/organisationExpiryAlertsDetail">Hosting Fee Reminder</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/changeAdministrator ">Change Administrator</a></h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/updateLogo">Update Organisation Logo</a></h4></li>								
							</c:when>
							<c:otherwise>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Configure IP Address</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Access to</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Remote User Access</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Usage Statistics</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Open URL Resolver</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Update Organisation Details</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Hosting Fee Reminder</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Change Administrator</h4></li>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Update Organisation Logo</h4></li>								
							</c:otherwise>
						</c:choose>
						<!-- Switch Accounts: check if 2 or more admins -->
						<c:choose>
							<c:when test="${isAdmin}">
								<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/switchAccounts">Switch Accounts</a></h4></li>
							</c:when>
							<c:otherwise>
								<li><span class="icons_img bullet_01">&nbsp;</span><h4>Switch Accounts</h4></li>
							</c:otherwise>
						</c:choose>
						</ul>
					</li>
					<!-- Start Level 1 --> 
					<li id="parent-7"><span class="icons_img bullet_01">&nbsp;</span><h3>Help Features</h3> <div class="link_01 fr"><span class="icons_img arrow_04">&nbsp;</span><a href="#">Back to Top</a></div>
						<ul><!-- Start Level 2 -->
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/home.jsf">Accessible Version</a></h4></li>
					        <c:choose>
								<c:when test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
									<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=ALL&sortBy=1">Access To</a></h4></li>
								</c:when>
								<c:otherwise>
									<li><span class="icons_img bullet_01">&nbsp;</span><h4>Access To</h4></li>
								</c:otherwise>
							</c:choose>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/popups/view_access.jsf">Diagnostics</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/popups/faq.jsf">FAQs</a></h4></li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/aaa/popups/help_index.jsf">Help Topic Index</a></h4> 
								<ul><!-- Start Level 3 -->
									<li>Content-sensitive Help Topic</li>
								</ul>
							</li>
							<li><span class="icons_img bullet_01">&nbsp;</span><h4><a href="${pageContext.request.contextPath}/cjoc/action/ebooks/forgottenPassword?displayname=">Forgotten Password</a></h4></li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- Treelist End -->
  
             <div class="clear_bottom"></div>
			
			<!-- Footermenu Start -->
	        <jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
	        <!-- Footermenu End -->
			
		</div>        
        <!-- End Body Content -->
  
	    <!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
       
</div>      
</body>
</html>