<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.net.*" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	${bookBean.initPage}
	<fb:share-button class="meta">
		<meta name="medium" content="mult"/>
		<meta name="title" content="Cambridge Books Online - ${bookBean.bookMetaData.title}"/>
		<meta name="description" content="${bookBean.bookMetaData.blurb}"/>
		<link rel="image_src" href="${pageContent.request.contextPath}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" />		
	</fb:share-button>  
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>
		<c:if test="${not empty bookBean.bookContentItem.label}">
			${bookBean.bookContentItem.label} -
		</c:if> 
		${bookBean.bookContentItem.title} - Cambridge Books Online - Cambridge University Press
	</title>
	
	<meta name="keyword" content="${bookBean.chapterKeywordMeta}" />
	
	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>
	
</head>
<body>
		<c:set var="helpType" value="child" scope="session" />
		<c:set var="pageId" value="1532" scope="session" />
		<c:set var="pageName" value="Chapter Landing" scope="session" />

<!-- Start page wrapper -->
	<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
              	<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
			  
			  		<!-- for tab.js -->
					<input type="hidden" id="isbnHidden" value="${bookBean.bookMetaData.onlineIsbn}" />
					<input type="hidden" id="idHidden" value="${bookBean.bookMetaData.id}" />
					
					<input type="hidden" id="pageType" value="ebook" />
					
					<!-- for references -->
					<c:set var="refTabSelected" value="${ref}" />
					
					<c:set var="bookTitlePdf" value="${bookBean.bookMetaData.title}" scope="request" />
	
					<!-- for hithighlighting -->
					<c:choose>
						<c:when test="${not empty sessionScope.hithighlight}">
							<input type="hidden" id="hid_hithighlight" value="on" />
						</c:when>
						<c:otherwise>
							<input type="hidden" id="hid_hithighlight" value="off" />
						</c:otherwise>
					</c:choose>
	
					
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
	   			<jsp:include page="/components/crumbtrail.jsp"></jsp:include>
			</div>
			<!-- Breadcrumbs End -->
			
			
 		</div>
        <!-- End Middle Content -->               
        
		<a id="maincontent" name="maincontent" ></a>
		              
		<!-- Start Body Content -->
		<div class="content_container">
		
			<c:set var="pageTab" value="${param.pageTab == null ? 'bd' : param.pageTab}"/>

			<div class="clear_bottom"></div>
		
			<!-- Nav Start -->
			<jsp:include flush="true" page="../aaa/components/book_info_links.jsp" />
			<!-- Nav End -->
		
		    <!-- Landing Container Start --> 
            <div class="landing_container link_02">
            	<!-- Book Thumbnail Start -->
            	<c:if test="${not bookBean.hasAccess}">
					<c:if test="${supplemental.salesModuleFlag eq 'N'}">
						<blockquote>This title has been withdrawn from sale on Cambridge Books Online. Institutions who purchased this title previous to removal will continue to have access via Cambridge Books Online but this title can no longer be purchased.</blockquote>	
					</c:if>	
				</c:if>
				<div class="icon_enlarge">
					<c:choose>
						<c:when test="${not empty bookBean.bookMetaData.thumbnailImageFilename}">
	                  	
	                  		<a href="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="${bookBean.bookMetaData.title} cover image">
	                  			<img src="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.thumbnailImageFilename}" alt="${bookBean.bookMetaData.title} cover image" />
	                   		</a>
	                   	<%--
	                  		<a href="http://ebooks.cambridge.org/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="${bookBean.bookMetaData.title} cover image">
	                  			<img src="http://ebooks.cambridge.org/content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.thumbnailImageFilename}" alt="${bookBean.bookMetaData.title} cover image" />
	                   		</a>	--%>
	                       	<ul>
	                        	<li>
	                        		<span class="icons_img enlarge">&nbsp;</span><a href="${domainCbo}content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookMetaData.standardImageFilename}" class="thickbox" title="link opens new window">Enlarge Image</a>
	                        	</li>
	                       	</ul>	                    	
						</c:when>
						<c:otherwise>
							<ul>
								<li><img src="../../aaa/images/thumbs/thumbnail_placeholder.jpg" alt="No cover image available"/></li>
							</ul>
						</c:otherwise>
					</c:choose>
				</div>
				<!-- Book Thumbnail End -->
				
				<!-- Book Info Start -->
				<div class="search_info">
					
					<ul>
						<li><h2>
							<c:if test="${not empty bookBean.bookContentItem.label}">
								${bookBean.bookContentItem.label} -
							</c:if>
							${bookBean.bookContentItem.title}&nbsp;
							
							pp. ${bookBean.bookContentItem.pageStart}-${bookBean.bookContentItem.pageEnd}</h2>
							<c:if test="${not empty bookBean.bookContentItem.contributorNames}">
								<li>By <c:out value="${bookBean.bookContentItem.contributorNames}" escapeXml="false" /></li>
							</c:if>		
						</li>
						<!-- Icon PDF START -->
						<li>
						<% 
							String userAgent = request.getHeader("User-Agent"); 
						%>
							<c:choose>
								<c:when test="${bookBean.bookContentItem.isFreeContent or bookBean.hasAccess}">
									<div id="icon_pdf_on">
										<ul>
											<li><a href="#" onclick="showPdfFromLandingPage('${bookBean.bookMetaData.id}', '${bookBean.bookContentItem.id}', '');return false;">View chapter as PDF</a></li>
										</ul>
									</div>
								</c:when>
								<c:otherwise>
									<div id="icon_pdf_off">
										<c:choose>
											<c:when test="${not empty userInfo and not empty userInfo.username}">
												<ul>
													<li><strong>PDF Not Available</strong></li>
												</ul>												
											</c:when>
											<c:otherwise>
												<ul>
													<li><a href="${pageContext.request.contextPath}/aaa/login.jsf?"><strong>Log-in to View chapter as PDF</strong></a></li>
												</ul>	
											</c:otherwise>
										</c:choose>
									</div>
								</c:otherwise>
							</c:choose>
						</li>
						<!-- Icon PDF End -->
					</ul>
						
					<div class="clear"></div>
					
					<ul>
						<li>
							<h3><a href="ebook.jsf?bid=${bookBean.bookMetaData.id}" class="linkBookTitle">${bookBean.bookMetaData.title}</a></h3>
						</li>
						<c:if test="${not empty bookBean.bookMetaData.subtitle}">									
							<strong><c:out value="${bookBean.bookMetaData.subtitle}" escapeXml="false" /></strong>
						</c:if>	
						<c:if test="${not empty bookBean.bookMetaData.volumeNumber}">
							<li>Volume <c:out value="${bookBean.bookMetaData.volumeNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.volumeTitle)? '<br/>': ',' }" escapeXml="false"/> 
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.volumeTitle}">
							<c:out value="${bookBean.bookMetaData.volumeTitle}" escapeXml="false" /></li>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.partNumber}">
							<li>Part <c:out value="${bookBean.bookMetaData.partNumber}" escapeXml="false" /><c:out value="${(empty bookBean.bookMetaData.partTitle)? '<br/>': ',' }" escapeXml="false"/>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.partTitle}">
							<c:out value="${bookBean.bookMetaData.partTitle}" escapeXml="false" /></li>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.edition}">
							<li><c:out value="${bookBean.bookMetaData.edition}" escapeXml="false" /></li>
						</c:if>	
						
						<c:set var="authorRoleTitle" value="${bookBean.bookMetaData.authorRoleTitleList}" />
						<c:set var="authorAffiliation" value="${bookBean.bookMetaData.authorAffiliationList}" />
						<c:forEach items="${bookBean.bookMetaData.authorNameList}" var="author" varStatus="status">												
							<li><c:out value="${authorRoleTitle[status.count - 1]}" /> <c:out value="${author}" escapeXml="false" /></li>
							<c:if test="${not empty authorAffiliation and not empty authorAffiliation[status.count - 1]}">														
								<li><em><c:out value="${authorAffiliation[status.count - 1]}" escapeXml="false" /></em></li>	
							</c:if>															
							<c:if test="${not status.last}">
								<br />
							</c:if>
						</c:forEach>				
					</ul>
					
											
					<div class="list_divider01">
						<c:if test="${not empty bookBean.bookMetaData.series}">
							<c:url value="series_landing.jsf" var="urlSeries">
								<c:param name="seriesCode" value="${bookBean.bookMetaData.seriesCode}" />
								<c:param name="seriesTitle" value="${bookBean.bookMetaData.series}" />
								<c:choose>
									<c:when test="${not empty bookBean.bookMetaData.seriesNumber}">
										<c:param name="sort" value="series_number" />
									</c:when>
									<c:otherwise>
										<c:param name="sort" value="print_date" />
									</c:otherwise>
								</c:choose>
							</c:url>
			            	<a href="<c:out value="${urlSeries}" escapeXml="true" />" class="link08">
			            		<c:out value="${bookBean.bookMetaData.series}" escapeXml="false" /></a>
							<c:if test="${not empty bookBean.bookMetaData.seriesNumber}">
								<c:out value=" (No. ${bookBean.bookMetaData.seriesNumber})" escapeXml="false" />
							</c:if>
							<br/>
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.printDate}">
							<strong>Print Publication Year:</strong> ${bookBean.bookMetaData.printDate}
						</c:if>									
						<c:if test="${not empty bookBean.bookMetaData.onlineDate}">
							<br/><strong>Online Publication Date:</strong> ${bookBean.bookMetaData.onlineDate}
						</c:if>									
					</div>
					
		
					<div class="list_divider01">
						<c:if test="${not empty bookBean.bookMetaData.onlineIsbn}">
							<strong>Online ISBN:</strong> ${bookBean.bookMetaData.onlineIsbn}
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.hardbackIsbn}">
						 	<br/><strong>Hardback ISBN:</strong> ${bookBean.bookMetaData.hardbackIsbn}
						</c:if>
						<c:if test="${not empty bookBean.bookMetaData.paperbackIsbn}">
						 	<br/><strong>Paperback ISBN:</strong> ${bookBean.bookMetaData.paperbackIsbn}
						</c:if>
					</div>
				
					<div class="list_divider01">
						<c:if test="${not empty bookBean.bookContentItem.doi}">
						 	<strong>Chapter DOI:</strong> ${bookBean.bookContentItem.doi}
						</c:if>
					</div>
											
					<div class="list_divider01">
						<span class="Z3988" title="${bookBean.coinString}">&nbsp;</span>
					</div>
											
					<div class="clear_bottom"></div>
					
					<p>
						<strong>Subjects:</strong>
						<c:forEach items="${bookBean.bookMetaData.subjectList}" var="subject" varStatus="status">
							<c:url value="subject_landing.jsf" var="subjectLandingUrl">
								<c:param name="searchType" value="allTitles" />
								<c:param name="subjectCode" value="${bookBean.bookMetaData.subjectCodeList[status.count - 1]}" />
								<c:param name="subjectName" value="${subject}" />
							</c:url>
							
							<a href="<c:out value="${subjectLandingUrl}" escapeXml="true" />" class="link07">${subject}</a><c:if test="${not status.last}">,</c:if>
						</c:forEach> 
					</p>
					<div class="clear_bottom"></div>								
				</div>
				<!-- Book Info End -->
			</div>
			<!-- Landing Container End -->
			
			<div class="clear_bottom"></div>
						
			<!-- Box Tab Start -->
			<div class="border-landing">            
				<!-- Start Box -->
				<div class="box">
				  	<!-- Tab Menu Start -->
					<div class="topmenu">
						<a name="contentTab" id="contentTab"></a>
						<!--  <span><h3>Chapter Details Navigation</h3></span> -->
						<div class="hide_titlepage"><h3>Chapter Details Navigation</h3></div><!-- Hidden Titlepage -->
						<ul>	
							
							<c:set var="ceExist" value="false"></c:set>
							<c:if test="${not empty bookBean.bookContentItem.abstractText}">
								<c:choose>
									<%--<c:when test="${(pageScope.pageTab == 'ce' || pageScope.pageTab == 'img') && pageScope.ceExist == 'true' }">--%>
									<c:when test="${(pageScope.pageTab == 'ce' || pageScope.pageTab == 'img')  == 'true' }">
										<li class="top_current"><h4>Chapter Extract</h4></li>											
										<c:set var="ceExist" value="true"></c:set>
									</c:when>
									<c:otherwise>
										<li><h4><a id="chapCE" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=ce&amp;#contentTab">Chapter Extract</a></h4></li>											
										<c:set var="ceExist" value="true"></c:set>
									</c:otherwise>
								</c:choose>						
							</c:if>
							
							<c:choose>
								<c:when test="${pageScope.pageTab == 'toc' || pageScope.ceExist == 'false' }">
									<li class="top_current"><h4>Table of Contents</h4></li>
								</c:when>
								<c:otherwise>
									<li><h4><a id="chapToc" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=toc&amp;#contentTab">Table of Contents</a></h4></li>
								</c:otherwise>
							</c:choose>
					
							<c:choose>
								<c:when test="${pageScope.pageTab == 'vol' }">
									<li class="top_current"><h4>Volumes</h4></li>
								</c:when>
								<c:otherwise>
								<c:if test="${not empty bookBean.bookMetaData.volumeNumber}">
									<li><h4><a id="ebookVol" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=vol&amp;#contentTab">Volumes</a></h4></li>
								</c:if>
								</c:otherwise>
							</c:choose>
							
							<c:if test="${not empty bookBean.bookContentItem.referenceFile}">
								<c:choose>
									<c:when test="${pageScope.pageTab == 'ref' }">
										<li class="top_current"><h4>References</h4></li>
									</c:when>
									<c:otherwise>
										<li><h4><a id="chapRef" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=ref&amp;#contentTab">References</a></h4></li>
									</c:otherwise>
								</c:choose>
							</c:if>
						</ul>
						<div class="clear"></div>
					</div>
					<!-- Tab Menu End -->
					
					<div class="clear"></div>
					
					<!-- 1st Tab Content Start -->
					<c:if test="${not empty bookBean.bookContentItem.abstractText}">
						<div class="${(pageScope.pageTab == 'ce' || pageScope.pageTab == 'img') && pageScope.ceExist == 'true' ? 'link_02 show' : 'hide'}">
							<!-- Hidden Titlepage Start -->
	         				<c:if test="${(pageScope.pageTab == 'ce' || pageScope.pageTab == 'img') && pageScope.ceExist == 'true'}">
	         					<span class="titlepage_hidden"><h3>Chapter Extract Section</h3></span>
	         				</c:if>
	        				<!-- Hidden Titlepage End -->
							<div class="title_container">
								<c:if test="${not empty bookBean.bookContentItem.abstractFilename}">	
								
									<c:choose>
										<c:when test="${'none' eq bookBean.bookContentItem.abstractProblem or empty bookBean.bookContentItem.abstractProblem}">
											<span style="${pageScope.pageTab == 'ce'? 'display: block;' : 'display: none;'}" id="imageShown">
												<span class="extract_nav">
													<strong>Extract:</strong>
													<a id="anchorImageExtract" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=img&amp;#contentTab" >Image View</a>
												</span>
											</span>
	
											<span style="${pageScope.pageTab == 'img'? 'display: block;' : 'display: none;'}" id="imageShown">
												<a id="anchorHtmlExtract" href="${accBean.pagePath}${accBean.queryStringNoKeyword}pageTab=ce&amp;#contentTab" >Text View</a>
												| <a href="enlarge.jsf?${accBean.queryString}">Enlarge Image</a>
											</span>										
										</c:when>
										<c:otherwise>
												No Text View for This Chapter.
											<br/>
										</c:otherwise>
									</c:choose>
									
									<c:set var="chapNav" value="${bookBean.chapNav}"></c:set>
									<c:set var="currIndex" value="${bookBean.currChapterPosition}"></c:set>
									<c:set var="chapNavLen" value="${fn:length(chapNav)-1}" />
									<c:set var="prevPos" value="${currIndex eq 0 ? chapNavLen : currIndex-1}" />
									<c:set var="nextPos" value="${currIndex eq chapNavLen ? 0 : currIndex+1}" />
									
									<c:set var="prevNavStyle" value="${currIndex eq 0 ? 'background: #bdccd3; border: 1px solid #87949a;':''}"/>
									<c:set var="nextNavStyle" value="${currIndex eq chapNavLen ? 'background: #bdccd3; border: 1px solid #87949a;':''}"/>
									
									<c:set var="prevNavHref" value="chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[prevPos].id}" />
									<c:set var="nextNavHref" value="chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[nextPos].id}" />

									<div class="chapter_nav">
	                                	<ul>
	                                    	<li>
	                                    		<c:choose>
	                                    			<c:when test="${currIndex eq 0}">
	                                    				<a href="#contentTab" title="No Previous Chapter Available">No Previous Chapter For This Page</a>
	                                    			</c:when>
	                                    			<c:otherwise>
	                                    				<!-- previous -->
	                                    				<!--<div class="hide_text">Previous Chapter</div>--><a href="chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[prevPos].id}&amp;pageTab=${param.pageTab}#contentTab" title="Previous Chapter">Previous Chapter</a>
	                                    			</c:otherwise>
	                                    		</c:choose>	
	                                    	</li>
	                                   	    <li>
	                                   	    	<c:choose>
	                                   	    		<c:when test="${currIndex eq chapNavLen}">
	                                   	    			<a href="#contentTab" title="No Next Chapter Availble">No Next Chapter For This Page</a>
	                                   	    		</c:when>
	                                   	    		<c:otherwise>
	                                   	    			<!-- next -->
	                                   	    			<!-- <div class="hide_text">Next Chapter</div>--><a href="chapter.jsf?bid=${bookBean.bookMetaData.id}&amp;cid=${chapNav[nextPos].id}&amp;pageTab=${param.pageTab}#contentTab" title="Next Chapter">Next Chapter</a>
	                                   	    		</c:otherwise>
	                                   	    	</c:choose>
	                                   	    </li>
	                                    </ul>
	                            	</div>
	                                <div class="clear"></div>                                    			
								</c:if>
							</div>
							<c:choose>
								<c:when test="${'none' eq bookBean.bookContentItem.abstractProblem or empty bookBean.bookContentItem.abstractProblem}">			
									<div class="${pageScope.pageTab == 'ce'? 'show' : 'hide'}">
										<p>	    	
									    	<c:choose>
									    		<c:when test="${not empty bookBean.bookContentItem.abstractText}">										    			
													<c:choose>
														<c:when test="${not empty sessionScope.hithighlight}">
															<c:choose>
																<c:when test="${sessionScope.hithighlight eq 'on'}">
																	<c:set var="abstracttext_aaa" value="${fn:replace(bookBean.bookContentItem.highlightedAbstractText,'<span class=\\'searchWord\\'>','<strong>' )}" />
																	<c:set var="abstracttext_aaa" value="${fn:replace(abstracttext_aaa,'</span>' , '</strong>')}"/>
																</c:when>
																<c:when test="${sessionScope.hithighlight eq 'off'}">
																	<c:set var="abstracttext_aaa" value="${fn:replace(bookBean.bookContentItem.highlightedAbstractText,'<span class=\\'searchWord\\'>','' )}" />
																	<c:set var="abstracttext_aaa" value="${fn:replace(abstracttext_aaa,'</span>' , '')}"/>
																</c:when>
															</c:choose>
															<c:out value="${abstracttext_aaa}" escapeXml="false"/>
														</c:when>
														<c:otherwise>
															<c:out value="${bookBean.bookContentItem.highlightedAbstractText}" escapeXml="false"/>
														</c:otherwise>
													</c:choose>
									    		</c:when>
												<c:otherwise>No extract available.</c:otherwise>
											</c:choose>
										</p>					
									</div>
									<div class="${pageScope.pageTab == 'img'? 'show' : 'hide'}">
										<p class="extractImage" style="padding-left: 0;">
											<a href="enlarge.jsf?${accBean.queryString}" >
												<img src="../content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" title="${bookBean.bookContentItem.title}" alt="${bookBean.bookContentItem.title}" />
											</a>
										</p>
									</div>
								</c:when>
								<c:otherwise>
									<p>
										<c:choose>			
											<c:when test="${not empty bookBean.bookContentItem.abstractText}">
												<div style="display: block;" id="divImageExtract">
													<p class="extractImage" style="padding-left: 0;">														
														<a href="enlarge.jsf?${accBean.queryString}">
															<img src="../content/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.abstractFilename}" title="${bookBean.bookContentItem.title}" alt="${bookBean.bookContentItem.title}" />
														</a>
													</p>
												</div>
											</c:when>				
											<c:otherwise>No extract available.</c:otherwise>				
										</c:choose>
									</p>
								</c:otherwise>
							</c:choose>
						</div> 	
					</c:if>
					<!-- 1st Tab Content End -->	

					<!-- 2nd Tab Content Start -->
					<div class="${pageScope.pageTab == 'toc' || pageScope.ceExist == 'false' ? 'link_02 show' : 'hide'}">
						<!-- Hidden Titlepage Start -->
          				<c:if test="${pageScope.pageTab == 'toc' || pageScope.ceExist == 'false'}">
          					<!--  <span class="titlepage_hidden"><h3>Table of Contents Section</h3></span> -->
          					<div class="hide_titlepage"><h3>Table of Contents Section</h3></div><!-- Hidden Titlepage -->
          				</c:if>
         				<!-- Hidden Titlepage End -->
         				<!-- Set hithighlight for readpdf -->
						<c:choose>
							<c:when test="${not empty sessionScope.hithighlight}">
								<c:choose>
									<c:when test="${sessionScope.hithighlight eq 'on'}">
										<c:set var="pdfHitHighlight" value="&amp;hithighlight=on" scope="page"/>
									</c:when>
									<c:when test="${sessionScope.hithighlight eq 'off'}">
										<c:set var="pdfHitHighlight" value="&amp;hithighlight=off" scope="page" />
									</c:when>
								</c:choose>
							</c:when>
						</c:choose>
              				
              			<!-- TOC List Start -->
						<table class="book_landing link_01" summary="Table of Contents">
	                     	<tbody>		                                
								<c:forEach items="${bookBean.bookTocItemList}" var="bookTocItem" varStatus="status">
									<tr>
										<c:choose>
											<c:when test="${'root' ne bookBean.bookTocItemLevelMap[bookTocItem.id]}">
												<c:set var="indent" value="indent01" />			
											</c:when>
											<c:otherwise>
												<c:set var="indent" value="" />
											</c:otherwise>
										</c:choose>														
										<td class="${indent}" >				
											<ul>
												<li><strong>
												<%-- 	<a href="chapter.jsf?bid=${bookTocItem.bookId}&amp;cid=${bookTocItem.id}&amp;p=${status.count-1}">--%>
													<a href="chapter.jsf?bid=${bookTocItem.bookId}&amp;cid=${bookTocItem.id}&amp;p=${bookTocItem.position -1}&amp;pageTab=ce">
													<c:if test="${not empty bookTocItem.label}">
														${bookTocItem.label} -
													</c:if>
													<c:out value="${bookTocItem.title}" escapeXml="false" />:</a></strong>
													<c:if test="${not empty bookTocItem.contributorNames}">												
														<span class="contributor">By <c:out value="${bookTocItem.contributorNames}" escapeXml="false" /></span>
													</c:if>
												</li>
												<li><input type="hidden" value="${bookTocItem.accessType}" name="accessTypeHidden" /></li>
												
												<c:choose>
													<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">
														<li class="icons_img pdf_on">&nbsp;</li>													
													 	<li><a href="#" onclick="showPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '');return false;">Read PDF</a></li>	
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${not empty userInfo and not empty userInfo.username}">
																<li class="icons_img pdf_off">&nbsp;</li>	
																<li>No PDF Available</li>
															</c:when>
															<c:otherwise>
																<%-- 
																<span class="disabled_link"><a class="thickbox loginLink" href="${urlSsl}login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290">Read PDF</a></span>
																--%>
																<li class="icons_img pdf_off">&nbsp;</li>	
																<li><a href="${pageContext.request.contextPath}/aaa/login.jsf?">Log-in to Read PDF</a></li>
															</c:otherwise>
														</c:choose>														
													</c:otherwise>
												</c:choose>															
	
											</ul>
										</td>
										<td class="toc_num">pp. ${bookTocItem.pageStart}<c:if test="${not empty bookTocItem.pageEnd}">-${bookTocItem.pageEnd}</c:if></td>
									</tr>
									<c:if test="${not empty bookBean.tocItemMap[bookTocItem.id]}">
										<c:forEach items="${bookBean.tocItemMap[bookTocItem.id]}" var="tocItems">
											<tr>																
												<c:set var="toc_indent" value="indent01" />
												<c:choose>
													<c:when test="${'root' ne bookBean.tocItemLevelMap[tocItems.id]}">
														<c:set var="toc_padding_px" value="${(bookBean.tocItemLevelMap[tocItems.id] + 2) * 15}" />
														<c:set var="toc_style_level" value="padding: 8px 0 8px ${toc_padding_px}px;" />
													</c:when>
													<c:otherwise>
														<c:set var="toc_style_level" value="padding: 8px 0 8px 30px;" />
													</c:otherwise>
												</c:choose>	
												<td class="${toc_indent}" >
													<ul>
														<li>
															<c:out value="${tocItems.text}" escapeXml="false" />&nbsp;
														</li>											
													<c:choose>
														<c:when test="${bookTocItem.isFreeContent or bookBean.hasAccess}">															
															<li class="icons_img pdf_on">&nbsp;</li>
															<li>
																<a href="#" class="link03" onclick="showContentTocPdfFromLandingPage('${bookTocItem.bookId}', '${bookTocItem.id}', '${tocItems.pageStart}', '${bookTocItem.pageStart}', '');return false;">Read PDF</a>	
															</li>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${not empty userInfo and not empty userInfo.username}">
																	<li class="icons_img pdf_off">&nbsp;</li>	
																	<li>No PDF Available</li>
																</c:when>
																<c:otherwise>																
																	<li class="icons_img pdf_off">&nbsp;</li>																		
																	<li><a href="${pageContext.request.contextPath}/aaa/login.jsf?">Log-in to Read PDF</a></li>
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
													</ul>
												</td>
												<td class="toc_num">${tocItems.pageStart}</td>	
											</tr>
										</c:forEach>
									</c:if>	
								</c:forEach>
							</tbody>
						</table>
						<!-- TOC List End-->	
					</div>
					<!-- 2nd Tab Content End -->
					
					<!-- volume Tab Content Start -->
					<div class="${pageScope.pageTab == 'vol' ? 'show link_01' : 'hide'}">
				
						<!-- Hidden Titlepage Start -->
						<span class="titlepage_hidden">Volumes</span>
						<!-- Hidden Titlepage End -->
	
						<c:forEach items="${bookBean.volumeList}" var="BookMetaData" varStatus="status">
							<dl class="volume_list">                                    
								<!-- Volume item start -->
								<dl>
									<h3><a href="ebook.jsf?bid=${BookMetaData.id}">${BookMetaData.title}</a> Volume ${BookMetaData.volumeNumber}</h3>
									<p><c:out value="${BookMetaData.authorSingleline}" /></p>
									
									<dt>
										<p><strong>Print Publication Year:</strong> ${BookMetaData.printDate}</p>
										<p><strong>Online Publication Date:</strong> ${BookMetaData.onlineDate}</p>
										<p><strong>Online ISBN:</strong> ${BookMetaData.onlineIsbn}</p>
									</dt>  
									<dt>
										<p><strong>Paperback ISBN:</strong> ${BookMetaData.paperbackIsbn}</p>
										<p><strong>Book DOI:</strong> ${BookMetaData.doi}</p>
									</dt> 
								</dl>
								<!-- Volume item end -->   
							</dl>
						</c:forEach>
						<dl>&nbsp;</dl>				
					</div>
					<!-- volume Tab Content End -->
					
					<!-- 3rd Tab Content Start -->
                    <div id="referencesWrapper" class="${pageScope.pageTab == 'ref' ? 'link_02 show' : 'hide'}">
	                    <!-- Hidden Titlepage Start -->
	     				<c:if test="${pageScope.pageTab == 'ref'}">
	     					<!--  <span class="titlepage_hidden"><h3>References Section</h3></span> -->
	     					<div class="hide_titlepage"><h3>References Section</h3></div><!-- Hidden Titlepage -->
	     				</c:if>
	    				<!-- Hidden Titlepage End -->
						<c:choose>
							<c:when test="${refSelected and not empty bookBean.bookContentItem.referenceFile}">
								<c:import url="${urlContent}/${bookBean.bookMetaData.onlineIsbn}/${bookBean.bookContentItem.referenceFile}"></c:import>
							</c:when>
							<c:otherwise><p>No references available.</p></c:otherwise>
						</c:choose>
					</div>
					<!-- 3rd Tab Content End -->					
					
					<a id="refOpenURLLink" class="thickbox"></a>

					<div class="boxBottom"></div>
					
				</div>
				<!-- End Box -->

			</div>
			<!-- Box Tab End -->
		
			<div class="clear"></div>
			
			<!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
	    
	    <jsp:include page="/components/common_js.jsp" flush="true" />
	    <script type="text/javascript" src="/js/book.js"></script>
	    <script type="text/javascript" src="/js/chapter.js"></script>
	    <script type="text/javascript" src="/js/rightmenu.js"></script>
	    <script type="text/javascript" src="js/references.js"></script>
	      
</div> 		
<!-- End page wrapper -->

</body>
</html>