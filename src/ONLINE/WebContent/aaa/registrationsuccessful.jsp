<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<%@page import="org.cambridge.ebooks.online.jpa.rss.Rss"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Cambridge Books Online - Cambridge University Press</title>    
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
	
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">                                         
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
              	<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <%--<!-- Breadcrumbs Start -->
			<div class="breadcrumbs">
	   			<gui:crumbtrail propertyFile="org.cambridge.ebooks.online.crumbtrail.crumb"
	  				lastElementStyle="last"
	  				userCrumb="org.cambridge.ebooks.online.crumbtrail.UserCrumb" >
	  			</gui:crumbtrail>
			</div>
			<!-- Breadcrumbs End -->--%>
 		</div>
        <!-- End Middle Content -->               

		<a id="maincontent" name="maincontent" ></a>               

		<!-- Start Body Content -->
		<div class="content_container link_02">
        
        	<!-- Titlepage Start -->
            <h1>Registration</h1>   
            <!-- Titlepage End -->
            
            <div class="clear"></div>
	
	<div id="main_container">
		<div class="titlepage_registration" ></div>
						<%-- <h:panelGroup rendered="#{param.type eq 'sa'}">
							<p class="alert" style="color: red">
								Offer code <%--${param.msg}--- -- %> has already been activated. Please double-check your offer code and enter it again.
							</p>
						</h:panelGroup>
						
						<h:panelGroup rendered="#{param.type eq 'si'}">
							<p class="alert" style="color: red">Offer code <%--${param.msg}-- %> is invalid.</p>
						</h:panelGroup>
					
					
						<h4>Registration Successful!</h4>
						<ul>
							<li><h:outputText value="#{userInfo.firstName}"/>&nbsp;<h:outputText value="#{userInfo.lastName}"/></li>
							<li><h:outputText value="#{userInfo.email}"/> </li>
						</ul>
						<p>Thank you for registering with Cambridge Books Online.</p>
						
						<%-- 
						<p>Use the menu on the left of the page to view and manage your preferences.</p>
						-- %>
					
						<h:panelGroup rendered="#{orgName ne null && userType eq 'AO'}">
							<p>You are now the administrator for the following organisation:</p>
							<h4><h:outputText value="#{orgName}"/></h4>
							<br/>
							<p>Use the "Account Administrator" link on the upper right to view and manage your organisation's preferences.</p>
						</h:panelGroup>		
						--%>
						
						<h4>Registration Successful!</h4>
						<ul>
							<li>${userInfo.firstName}&nbsp;${userInfo.lastName}</li>
							<li>${userInfo.email}</li>
						</ul>
						<p>Thank you for registering with Cambridge Books Online.</p>
		</div>
		
		<div class="clear_bottom"></div>
		
            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
	
	
	</body>

</html>		
		