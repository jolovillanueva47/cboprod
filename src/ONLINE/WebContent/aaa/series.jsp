<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>
<%@page import="org.cambridge.ebooks.online.subjecttree.SeriesWorker_aaa"%>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />

	<title>Series - Cambridge Books Online - Cambridge University Press</title>

    <jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1804" scope="session" />
	<c:set var="pageName" value="Browse By Series" scope="session" />
	
<div id="page-wrapper"> 
		<!-- Start Header -->
		<div class="header">
		    <div class="header_container">
		    	<div class="topheader_divider">
		         		<!-- Start Accessibility Menu -->
							<jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
					  	<!-- End Accessibility Menu --> 
		        </div>
		        <div class="clear"></div>
	
				<div class="middleheader_divider">
					<jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
				</div>
			
			<div class="clear"></div>
			
				<!-- Start Topmenu -->
				<div class="topmenu">
					<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
				</div>
				<!-- End Topmenu -->     
		    </div>
		</div>  
		<!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

			 
	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span>
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
			
 		</div>
        <!-- End Middle Content -->               

		<a id="maincontent" name="maincontent" ></a>
		     
		<!-- Start Body Content -->
		<div class="content_container">
			<div class="clear_bottom"></div>
			<!-- Titlepage Start -->
				<h2>Series</h2>
			<!-- Titlepage Start -->				
			<div class="clear_bottom"></div>		
		
			<!-- Start Banner -->
			<div class="border-div link_01">
			
				<div class="border_divider04 link_01">
				 
					<!-- Subtitle page Start -->
					<h2>Series</h2>
					<!-- Subtitle page End -->
					<p>This site lists all our series in Academic, Education and English Language Teaching areas, and includes new and forthcoming series, as well as those that have now come to an end. Click on one of the options below to browse our series by A-Z listing, or search for your series using our 'quick search' box.</p>
					<p><strong>Select your series by clicking on a letter.</strong></p>
										
					<!-- Alpha List Start -->
					<jsp:include page="../aaa/components/series_alphalist_main.jsp" />	
					<!-- Alpha List End -->
						
				</div>
				

				<div class="border_divider05">
            
                	<!-- Subtitle page Start -->
                	<h3>Series by Subject</h3>
                	<!-- Subtitle page End -->
                	
                	
                	
               		<c:set var="nodes" value="<%= SeriesWorker_aaa.parseAllSubjects()%>" ></c:set>
	             
	                   	 <c:if test="${nodes != null}">
	                   	 	<ul>
								<c:forEach var="node" items="${nodes}" varStatus = "status">
									<c:choose>
										<c:when test="${status.count == 1}">
											<li><span class="icons_img arrow_03">&nbsp;</span>
												<a href="series_tree.jsf?skipId=${node.key.subjectId}">${node.key.subjectName}</a>
											</li>	
										</c:when>
										<c:otherwise>
											<li><span class="icons_img arrow_03">&nbsp;</span>
												<a href="series_tree.jsf?skipId=${node.key.subjectId}#${node.key.subjectId}">${node.key.subjectName}</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
	                   	 </c:if>
                	
<%--                 	
                	<ul>
                        <li><span class="icons_img arrow_03"></span><a href="series_tree.jsf?skipId=A">Humanities</a></li>
                        <li><span class="icons_img arrow_03"></span><a href="series_tree.jsf?skipId=B#B">Social Sciences</a></li>
                        <li><span class="icons_img arrow_03"></span><a href="series_tree.jsf?skipId=C#C">Science &amp; Engineering</a></li>
<!--                        <li><span class="icons_img arrow_03"></span><a href="series_tree.jsf?#D">Law</a></li>-->
						<li><span class="icons_img arrow_03"></span><a href="series_tree.jsf?skipId=D#D">Medicine</a></li>
                    </ul>
--%>                    
                </div>
				
				<div class="clear"></div>
				 
			</div>
			<!-- End Banner -->	
				
			<!-- Footermenu Start -->
				<jsp:include page="../aaa/components/footer_menuwrapper.jsp"></jsp:include>
			<!-- Footermenu End -->		
		
		</div>        
        <!-- End Body Content -->
             
	    <!-- FooterContainer Start -->
			<jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
       
</div>     
</body>
</html>





