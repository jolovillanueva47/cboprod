<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<%@page import="java.net.URLEncoder"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	<title>Cambridge Books Online - Cambridge University Press</title>
  
  	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>
  	
</head>

<body>	
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="86" scope="session" />
	<c:set var="pageName" value="Search Results" scope="session" />
		
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp">
						<jsp:param name="currentPage_aaa" value="${searchResultBean.currentPage}"/>
						<jsp:param name="resultsPerPage_aaa" value="${searchResultBean.resultsPerPage}"/>
						<jsp:param name="sortType_aaa" value="${not empty param.sortType ? param.sortType : 'score'}"/>	
					</jsp:include> 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

        <!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 
            
            <!-- bread crumb hidden -->
	    	<div class="breadcrumbs_container" >
	    		<span class="titlepage_hidden">You are here:</span> 
	    		<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
	    	<!-- bread crumb hidden -->
        </div>
        <!-- End Middle Content -->             
		

		               
		<!-- Start Body Content -->
		<div class="content_container input_text buttons">      
			
			<div class="clear"></div>
			<!-- Print Header Start -->
			<h2>Search Results</h2>
			<!-- Print Header End -->
            <div class="clear"></div>
            
			<!-- Access Information Start -->
			<p><strong>Access Information:</strong></p>
			<div class="icon_access">
			    <ul>
			        <li><img src="../../aaa/images/icon_p_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Purchased Access" width="30" height="30" /></li>
			        <li class="icon_free" title="Free Access">Purchased Access</li>
			    </ul>
			</div>
			<!-- Access Information End -->
			<div class="clear_bottom"></div> 
			
			<!-- Advance Search Start -->       
            <form id="advanceSearchForm" action="<%=request.getContextPath()%>/search_aaa?searchType=advance" method="post" style="${param.showSearch eq 'show' ? 'display:block' : 'display:none'}">				
				<jsp:include page="../aaa/components/adv_search_inputform1.jsp"></jsp:include>                  
                <input type="submit" class="input_buttons" value="Search" name="search" />
                <input type="reset" class="input_buttons" value="Clear All" name="clear_aaa_" />
			</form>
			<!-- Advance Search End -->
			
			<div class="clear_bottom"></div>
			
			<!-- Start Hide/Show -->
			<form action="${domainCbo}search_aaa?searchType=page" method="post">
				<input type="hidden" name="currentPage" value="${searchResultBean.currentPage}"/>
				<input type="hidden" name="resultsPerPage" value="${searchResultBean.resultsPerPage}"/>
				<input type="hidden" name="sortType" value="${not empty param.sortType ? param.sortType : 'score'}"/>
								
				<c:choose>
					<c:when test="${not empty param.showSearch and param.showSearch eq 'show' }">
						<input type="hidden" name="showSearch" value="hide" />
						<input type="submit" class="input_buttons" value="Hide Search Details" name="more" />
					</c:when>
					<c:otherwise>
						<input type="hidden" name="showSearch" value="show" />
						<input type="submit" class="input_buttons" value="Show Search Details" name="more" />
					</c:otherwise>
				</c:choose>	
			</form>
			
			<div class="clear_bottom"></div>
			
  			<!-- search results found start -->
       		<c:choose>
       			<c:when test="${searchResultBean.resultsFound eq 1}">
       				Your search returned <b>${searchResultBean.resultsFound}</b> result.
       			</c:when>
       			<c:otherwise>
       				Your search returned <b>${searchResultBean.resultsFound}</b> results.
       			</c:otherwise>
       		</c:choose>
       		<!-- search results found end -->
       		
			<div class="clear_bottom"></div>
			
            <!-- Start Faceted search container -->
            <div class="faceted_search_container">
            	
			<c:url var="urlSearchResults_aaa" value="${domainCbo}search_aaa" >
				<c:param name="searchType" value="filter" />
				<c:param name="resultsPerPage" value="50" />
				<c:param name="filterType" value="multiple" />
			</c:url>

   				<!-- Filter container start -->
   				<form action="<c:out value='${urlSearchResults_aaa}' />" method="post" >
   					
	   				<div class="search_filter_container">
	   				
	   					<jsp:include page="../aaa/components/filter_control_search_results.jsp" >
	   						<jsp:param value="${searchResultBean.currentPage}" name="currentPage"/>
	   					</jsp:include>	   
	   										
	   					<div class="did_you_mean_container">                    	
	                    	<div class="search_related_container">
								<!-- Did you mean search generator start -->		                                    	
								<c:if test="${not empty searchResultBean.spellCheckTerm and 'null' ne searchResultBean.spellCheckTerm}">
									<div class="did_you_mean_container">
									    <h3><strong>Did you mean: </strong><a href="#">${searchResultBean.spellCheckTerm}</a></h3>
									</div>	                                        
								 </c:if>
								<!-- Did you mean search generator end -->

	                    		<%-- 
	                    		<h4>Searches related to <a href="#">China</a></h4>
	                        	<ul>
	                            	<li><a href="#">facts about <strong>china</strong></a></li>
	                                <li><a href="#">history of <strong>china</strong></a></li>
	                                <li><a href="#">facts about <strong>china</strong></a></li>
	                                <li><a href="#"><strong>china</strong> map</a></li>
	                                <li><a href="#">the great wall of <strong>china</strong></a></li>
	                                <li><a href="#"><strong>china</strong> chow</a></li>
	                                <li><a href="#"><strong>china</strong> economy</a></li>
	                                <li><a href="#"><strong>china</strong> olympics</a></li>
	                                <li><a href="#"><strong>china</strong> earthquake</a></li>
	                            </ul>
	                            --%>
	                       </div>

	                    </div> 
	                    <div class="hide_titlepage"><h3>Search Filters</h3></div><!-- Hidden Titlepage -->
	                    <div class="search_filter_selection_container">	                    
							<c:set var="filterDisplayMap" value="${facet.filterDisplayMap}" />
	                       	<c:if test="${not empty filterDisplayMap}">
	                       		<h4>Selected Search Filters:</h4>
	                        	<div class="search_filter_level_container">
		                        	<ul>
		                                <c:forEach var="filters" items="${filterDisplayMap}" varStatus="status">
		                                	<li><c:out value="${filters.value}" escapeXml="fasle" /><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=remove&amp;filterValue=${filters.key}">Remove</a></li>
		                                </c:forEach>
		                                <li><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=reset" >Remove All</a></li>
		                            </ul>
	                            </div>
	                        </c:if>
	                    	
	                    	<c:set var="contentTypeList" value="${facet.contentTypeList}" />
	                    	<c:if test="${not empty contentTypeList and fn:length(contentTypeList) > 0 and not empty contentTypeList[0]}">
	                            <h4>Content Type:</h4>
	                            <ul>
	                                <c:forEach var="contentType" items="${contentTypeList}" varStatus="status">
	                                	<c:if test="${'chapter' eq contentType.name or 'article' eq contentType.name}">
	                                		<c:set var="filterId" value="content_type=COL=${contentType.name}" />
	                                		<li>
	                                			<input id="contentType${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
	                                			<label for="contentType${status.count}" ><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=content_type&amp;filterValue=${filterId}" title="go to facet">${contentType.display} (${contentType.count})</a></label>
	                                		</li>
	                                	</c:if>			                                            		                                 	
	                                </c:forEach>
	                            </ul> 
	                           	<div class="clear"></div>
	                        </c:if>
	                        
							<c:set var="subjectFacetList" value="${facet.subjectFacetList}" />
							<c:if test="${not empty subjectFacetList and fn:length(subjectFacetList) > 0}">
								<h4>Subject:</h4>
								<ul>
								    <c:forEach var="subject" items="${subjectFacetList}" varStatus="status">		
										<c:set var="filterId" value="subject=COL=${subject.escapeName}" />                                            	
										<li>
											<input id="subject${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
											<c:set var="EncodeUtil_var" value="${filterId}" scope="request"/>
											<label for="subject${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=subject&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${subject.noHtmlName} (${subject.count})</a></label>
										</li>
									</c:forEach>                                    
								</ul>  
								<div class="clear"></div> 
							</c:if>
	                  
							<c:set var="seriesFacetList" value="${facet.seriesFacetList}" />
							<c:if test="${not empty seriesFacetList and fn:length(seriesFacetList) > 0}">
								<h4>Series:</h4>
								<ul>
								    <c:forEach var="series" items="${seriesFacetList}" varStatus="status">		
										<c:set var="filterId" value="series=COL=${series.escapeName}" />
										<c:set var="EncodeUtil_var" value="${filterId}" scope="request"/>                                             	
										<li><input id="series${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
										<label for="series${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=series&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${series.noHtmlName} (${series.count})</a></label>
										</li>
									</c:forEach>                                        
								</ul>  
								<div class="clear"></div> 
							</c:if>
	                 
							<c:set var="authorNameFacetList" value="${facet.authorNameFacetList}" />
							<c:if test="${not empty authorNameFacetList and fn:length(authorNameFacetList) > 0}">		                                        	
								<h4>Author:</h4>
								<ul>
								    <c:forEach var="author" items="${authorNameFacetList}" varStatus="status">	 
								   		<c:set var="filterId" value="author_name=COL=${author.escapeName}" />
								   		<c:set var="EncodeUtil_var" value="${filterId}" scope="request"/>                                            	
								 		<li><input id="author${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
								 		<label for="author${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=author_name&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${author.noHtmlName} (${author.count})</a></label>
								 		</li>
								 		<%--<label for="${filterId}"><c:out value="${author.display}" escapeXml="false" /> (${author.count})</label></li> --%>
								   </c:forEach>                                             
								</ul>  
								<div class="clear"></div>
							</c:if>
	                  
							<c:if test="${(searchResultBean.resultsFound gt 0 and not empty facet.facetQueryList) or 'multiple' eq param.filterType}">
								<h4>Year:</h4>
								<ul>
								    <c:forEach var="printDate" items="${facet.facetQueryList}" varStatus="status">		
										<c:choose>
											<c:when test="${status.count > 6}">
												<c:set var="hideClass" value="hide_list" />
											</c:when>
											<c:otherwise>
												<c:set var="hideClass" value="" />
											</c:otherwise>
										</c:choose>	                         
										<c:set var="parsedDate1" value="${fn:replace(printDate, 'print_date:[', '')}" />
										<c:set var="parsedDate2" value="${fn:replace(parsedDate1, 'TO', '-')}" />   
										<c:set var="parsedDate3" value="${fn:replace(parsedDate2, ']', '')}" />                               			                          	
										<li class="${hideClass}"><input id="year_${status.count}" name="filter" type="checkbox" value="${fn:replace(printDate, ':', '=COL=')}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[fn:replace(printDate, ":", "=COL=")] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
										<c:set var="EncodeUtil_var" value="${printDate}" scope="request"/> 
										<label for="year_${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=print_date&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${parsedDate3} (${facet.facetQueryMap[printDate]})</a></label>
										</li>                                 			
									</c:forEach>                              
								</ul>      
								<div class="clear"></div>
							</c:if>
							
							<c:set var="bookTitleFacetList" value="${facet.bookTitleFacetList}" />
							<c:if test="${not empty bookTitleFacetList and fn:length(bookTitleFacetList) > 0}">
								<h4>Top Books:</h4>
								<ul>
								    <c:forEach var="topBook" items="${bookTitleFacetList}" varStatus="status">				                                            	
										<c:set var="filterId" value="main_title_b=COL=${topBook.escapeName}" />
										<c:set var="EncodeUtil_var" value="${filterId}" scope="request"/>  
										<li><input id="topBook${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
										<label for="topBook${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=main_title_b&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${topBook.noHtmlName} (${topBook.count})</a></label>
										</li>
									</c:forEach>
								</ul>   
								<div class="clear"></div>
							</c:if>
							
							<c:set var="journalTitleFacetList" value="${facet.journalTitleFacetList}" />
							<c:if test="${not empty journalTitleFacetList and fn:length(journalTitleFacetList) > 0}">
								<h4>Top Journals:</h4>
								<ul>
								    <c:forEach var="topJournal" items="${journalTitleFacetList}" varStatus="status">	
										<c:set var="filterId" value="main_title_j=COL=${topJournal.escapeName}" />
										<c:set var="EncodeUtil_var" value="${filterId}" scope="request"/>  		          
										<li><input id="topJournal${status.count}" name="filter" type="checkbox" value="${filterId}" class="filter" <c:if test='${("checked" eq facet.categorySelectionMap[filterId] or param.selectAll_aaa eq "Select All") and param.selectAll_aaa ne "Reset"}'>checked="checked"</c:if> />
										<label for="topJournal${status.count}"><a href="search_aaa?searchType=filter&amp;resultsPerPage=50&amp;filterType=main_title_j&amp;filterValue=<%= EncodeUtil(request) %>" title="go to facet">${topJournal.noHtmlName} (${topJournal.count})</a></label>
										</li>
										<%--<label for="${filterId}"><c:out value="${topJournal.display}" escapeXml="false" /> (${topJournal.count})</label></li> --%>
									</c:forEach>
								</ul>  
							</c:if>
	                   
	                    </div>
	   				
	   					<jsp:include page="../aaa/components/filter_control_search_results.jsp" >
	   						<jsp:param value="${searchResultBean.currentPage}" name="currentPage"/>
	   					</jsp:include>
	   					
	   				</div>
	   			</form>
	   			<!-- Filter container end -->
   				<a id="maincontent" name="maincontent" ></a>	
            	<!-- Search results start -->
                <div class="search_results_container">
                                	
 					<!-- Search results control start -->
					<jsp:include page="../aaa/components/pager_search_results.jsp" />
					<!-- Search results control end -->               	
                	
                	<!-- Search results start -->
                	<div class="search_results_content_container">
                		<h3>Results</h3>
						<!-- goto links -->
						
							<c:choose>
								<c:when test="${searchResultBean.resultsFound > 0}">
									<jsp:include page="../aaa/components/pager_search_results.jsp">
										<jsp:param name="goto" value="yes"></jsp:param>
									</jsp:include>
								</c:when>
								<c:otherwise>
									<jsp:include page="../aaa/components/pager_search_results.jsp">
										<jsp:param name="goto" value="dontdisplay"></jsp:param>
									</jsp:include>
								</c:otherwise>
							</c:choose>
						
						<!-- goto links -->     
             
						<c:forEach var="result" items="${searchResultBean.searchResultList}" varStatus="status">
							<c:choose>
								<c:when test="${'chapter' eq fn:toLowerCase(result.contentType)}">

									<!-- Ebooks Content Info Start -->
									<div class="ebook_list">
										<ul>				
											<li>
												<h4><a href="${domainCbo}aaa/ebook.jsf?bid=${result.bookId}"><c:out value="${result.bookTitle}" escapeXml="false" /> by <c:out value="${result.author}" escapeXml="false"/></a>
												<img height="21" width="55" alt="Book" src="aaa/images/icon_book.gif" />	
												</h4>
												<div class="icon_access_faceted">
													<gui:isPurchasedAccess accessType="${result.accessType}">
														<img height="30" width="30" alt="Purchased Access" src="../../aaa/images/icon_p_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" />
													</gui:isPurchasedAccess>
												</div>
											</li>
											<c:if test="${not empty result.bookSubtitle and 'null' ne result.bookSubtitle}">
												<li><c:out value="${result.bookSubtitle}" escapeXml="false" /></li>
											</c:if>
											<c:if test="${not empty result.bookVolumeNumber and 'null' ne result.bookVolumeNumber}">
												<li>Volume <c:out value="${result.bookVolumeNumber}" escapeXml="false" /></li>
											</c:if>
											<c:if test="${not empty result.bookVolumeTitle and 'null' ne result.bookVolumeTitle}">
												<li><c:out value="${result.bookVolumeTitle}" escapeXml="false" /></li>
											</c:if>
											<c:if test="${not empty result.bookPartNumber and 'null' ne result.bookPartNumber}">
												<li>Part <c:out value="${result.bookPartNumber}" escapeXml="false" /></li>
											</c:if>
											<c:if test="${not empty result.bookPartTitle and 'null' ne result.bookPartTitle}">
												<li><c:out value="${result.bookPartTitle}" escapeXml="false" /></li>
											</c:if>
											<c:if test="${not empty result.bookEdition and 'null' ne result.bookEdition}">
												<li><c:out value="${result.bookEdition}" escapeXml="false" /></li>
											</c:if>
										</ul>
										
										<div class="search_results_info">
											<ul >
												<li><strong>Print Publication Year:</strong> ${result.bookPrintDate}</li>
												<li><strong>Online Publication Date:</strong> ${result.bookOnlineDate}</li>
												<c:if test="${not empty result.bookSeries and 'null' ne result.bookSeries}">
	                                               	<li><strong>Series:</strong>
	                                               		<c:choose>
															<c:when test="${not empty result.bookSeriesNumber and 'null' ne result.bookSeriesNumber}">
																<a href="${domainCbo}aaa/series_landing.jsf?seriesCode=${result.bookSeriesCode}&amp;seriesTitle=${result.bookSeries}&amp;sort=series_number" class="link03"><c:out value=" ${result.bookHighlightedSeries}" escapeXml="false" /></a><c:out value=" (No. ${result.bookSeriesNumber})" />
															</c:when>
															<c:otherwise>
																<a href="${domainCbo}aaa/series_landing.jsf?seriesCode=${result.bookSeriesCode}&amp;seriesTitle=${result.bookSeries}&amp;sort=print_date" class="link03"><c:out value=" ${result.bookHighlightedSeries}" escapeXml="false" /></a>
															</c:otherwise>
														</c:choose>
	                                               	</li>
						                        </c:if>
											</ul>
											<ul>
												<li><strong>Online ISBN:</strong> ${result.bookOnlineIsbn}</li>
												<li><strong>Print ISBN:</strong> ${result.bookPrintIsbn}</li>
											</ul>
										</div>
										<div class="clear"></div>
											
										<ul>
											<li>
												<h5><a href="${domainCbo}aaa/chapter.jsf?bid=${result.bookId}&amp;cid=${result.id}"><c:if test="${not empty result.chapterLabel}">${result.chapterLabel} - </c:if><c:out value="${result.chapterTitle}" escapeXml="false" /></a></h5><br/>
											</li>
												<c:if test="${not empty result.chapterSubtitle and 'null' ne result.chapterSubtitle}">
													<li><c:out value="${result.chapterSubtitle}" escapeXml="false"/></li><li style="list-style: none"><br/></li>
												</c:if>
												<c:if test="${not empty result.contributor and 'null' ne result.contributor}">
													<li>Contributed by <c:out value="${result.contributor}" escapeXml="false"/></li><li style="list-style: none"><br/></li>
												</c:if>
												<li><strong>Chapter DOI:</strong> ${result.chapterDoi}</li>
										</ul>
										
										<p><c:out value="${result.chapterFulltext}" escapeXml="false" /></p>
										
																						
										<h5>Other chapters with results of your search:</h5>
																		                                                                                        
										<c:forEach var="chapters" items="${result.collapsedChapterList}" varStatus="status">
											<c:choose>
												<c:when test="${status.count eq 1}">
													<a href="${domainCbo}aaa/chapter.jsf?bid=${result.bookId}&amp;cid=${chapters.id}&amp;pageTab=ce#contentTab"><c:if test="${not empty chapters.label}">${chapters.label} </c:if>
														<c:set var="chapterTitle_aaa" value="${fn:replace(chapters.title, '<i>', '<em>')}" scope="page"/>
														<c:set var="chapterTitle_aaa" value="${fn:replace(pageScope.chapterTitle_aaa, '</i>', '</em>')}" scope="page"/>	
														<c:out value="${chapterTitle_aaa}" escapeXml="false"/>
													</a>
													<c:if test="${not status.last}">, </c:if>
												</c:when>
												<c:otherwise>
													<a href="${domainCbo}aaa/chapter.jsf?bid=${result.bookId}&amp;cid=${chapters.id}&amp;pageTab=ce#contentTab"><c:if test="${not empty chapters.label}">${chapters.label} </c:if>
														<c:set var="chapterTitle_aaa" value="${fn:replace(chapters.title, '<i>', '<em>')}" scope="page"/>
														<c:set var="chapterTitle_aaa" value="${fn:replace(pageScope.chapterTitle_aaa, '</i>', '</em>')}" scope="page"/>	
														<c:out value="${chapterTitle_aaa}" escapeXml="false"/>
													</a>
													<c:if test="${not status.last}">, </c:if>
												</c:otherwise>
											</c:choose>			                                                   			
										</c:forEach>
									
										<div class="download_container">
											<ul>
												<li><strong>pp ${result.chapterPageRange}</strong> |</li> 
												<c:choose>
													<c:when test="${result.hasAccess}">
														<li><span class="icon_search_result_pdf"><a href="javascript:void(0);" onclick="showPdfHighlight('${result.bookId}', '${result.id}', '');">Read PDF</a></span></li>
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${not empty userInfo and not empty userInfo.username}">
																<li><span class="icon_search_result_pdf_disabled">No PDF Available</span></li>
															</c:when>
															<c:otherwise>
																<li><span class="icon_search_result_pdf_disabled"><a href="${pageContext.request.contextPath}/aaa/login.jsf?">Log-in to Read PDF</a></span></li>																									
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose> 							                                            	
											</ul>       
											<div class="clear"></div>
										</div>
										<div class="clear"></div>            
									</div>
									<!-- Ebooks Content Info End -->
								</c:when>
								<c:when test="${'article' eq fn:toLowerCase(result.contentType)}">
									<c:url var="url_abstract" value="${domainCjo}action/displayAbstract">
										<c:param name="fromPage" value="online" />
										<c:param name="aid" value="${result.id}" />
										<c:param name="fulltextType" value="${result.articleType}" />
										<c:param name="fileId" value="${result.articleFileId}" />
									</c:url>
									<!-- Journals Content Info Start -->
									<div class="journal_list">
										<ul>
											<li><h4><a target="cjo_abstract" href="${url_abstract}"><c:out value="${fn:replace(result.articleHighlightedTitle, wrongFormat, rightFormat)}" escapeXml="false" /></a>
											<img height="21" width="72" alt="Journal" src="aaa/images/icon_journal.gif" />
											</h4></li>
											<li><c:out value="${result.author}" escapeXml="false"/></li>
											<li><a target="cjo_journal" href="${domainCjo}action/displayJournal?jid=${result.journalId}"><c:out value="${result.journalTitle}" escapeXml="false"/></a>, Volume ${result.articleVolumeNumber}, Issue ${result.articleIssueNumber}, ${result.articlePrintDate}<c:if test="${not empty result.articlePageStart or 'null' ne  result.articlePageStart}">, pp ${result.articlePageStart}<c:if test="${not empty result.articlePageEnd or 'null' ne result.articlePageEnd}">-${result.articlePageEnd}</c:if></c:if></li>
											<li><strong>DOI:</strong> ${result.articleDoi} <a target="cjo_about_doi" href="${domainCjo}action/stream?pageId=3624&level=2#30">(About doi)</a>, Published online by Cambridge University Press ${result.articleOnlineDate}</li>
											
										</ul>
										
										<h5>Preview Abstract:</h5>
										
										<c:set var="abstractFilename" value="${fn:replace(result.articleAbstractFilename, 'w.htm', 'h.htm')}" />
										<c:set var="abstractFilename" value="${fn:replace(abstractFilename, 'r.htm', 'h.htm')}" />
										<c:set var="abstractFilename" value="${fn:replace(abstractFilename, 's.htm', 'h.htm')}" />
					
											<c:set var="random_aaa" value="<%=Math.random() %>" />
											<c:set var="journal_url" value="${domainCbo}cjoc/content${abstractFilename}?dummy=${random_aaa}"></c:set>
												<%
													try {
												%>
													<c:import url="${journal_url}" var="finString" charEncoding="UTF-8"/>	
												<% 	
													}catch (Exception e){
												%>
														<c:set value="Abstract Preview Not Available." var="finString"></c:set>
												<%	
													}
												%>
										<c:if test="${not empty finString}" >
											<p><c:out value="${finString}" escapeXml="false"/></p>
										</c:if>
										
										<div class="other_journal_links">
											<ul>
												<li><a title="Link to journal abstract" target="cjo_abstract" href="${domainCjo}action/displayAbstract?fromPage=online&amp;aid=${result.id}&amp;fulltextType=${result.articleType}&amp;fileId=${result.articleFileId}">Abstract</a></li>
												<c:set var="rightTitle" value="${fn:replace(result.articleTitle, '', '&quot;')}" />		
												<c:set var="rightTitle" value="${fn:replace(rightTitle, '\"', '')}" />
												<c:set var="rightau" value='${fn:replace(result.author, "\'", "&rsquo;")}' />
												<c:url var="urlReqPermission" value="https://s100.copyright.com/AppDispatchServlet">
													<c:param name="publisherName" value="CUP" />
													<c:param name="publication" value="${result.journalId}" />
													<c:param name="title" value="${rightTitle}" />
													<c:param name="publicationDate" value="${result.articleOnlineDate}" />
													<c:param name="author" value="${rightau}" />
													<c:param name="copyright" value="Cambridge Journals" />
													<c:param name="contentID" value="${result.articleDoi}" />
													<c:param name="startPage" value="${result.articlePageStart}" />
													<c:param name="endPage" value="${result.articlePageEnd}" />
													<c:param name="orderBeanReset" value="True" />
													<c:param name="volumeNum" value="${result.articleVolumeNumber}" />
													<c:param name="issueNum" value="${result.articleIssueNumber}" />
												</c:url>
												<li><a title="Link to journal request permissions" href="<c:out value="${urlReqPermission}" />">Request Permissions</a></li>
											</ul>
										</div>
										<div class="clear"></div>
									</div>
									<!-- Journals Content Info End -->
								</c:when>
							</c:choose>	                                    		
						</c:forEach>             
						
						<!-- goto links -->
						
							<c:choose>
								<c:when test="${searchResultBean.resultsFound > 0}">
									<jsp:include page="../aaa/components/pager_search_results.jsp">
										<jsp:param name="goto" value="yes"></jsp:param>
									</jsp:include>
								</c:when>
								<c:otherwise>
									<jsp:include page="../aaa/components/pager_search_results.jsp">
										<jsp:param name="goto" value="displaynoresults"></jsp:param>
									</jsp:include>
								</c:otherwise>
							</c:choose>
						
						<!-- goto links -->       
						    
                	</div>
                	<!-- Search results end -->
                 	
                	<!-- Search results control start -->
					<jsp:include page="../aaa/components/pager_search_results.jsp">
						<jsp:param name="bottom" value="bottom"></jsp:param>
					</jsp:include>
					<!-- Search results control end -->    
					
                </div>
            	<!-- Search results end -->
            	<div class="clear"></div>
           		
            </div>
            <!-- End Faceted search container -->
            
            <%!
	            public String EncodeUtil(HttpServletRequest request)
	            {
	            	String reqatt = (String)request.getAttribute("EncodeUtil_var");
					String[] snt = reqatt.split("=");
					String item = snt[snt.length-1];
					String enc = "";
					try{
						enc = (String)URLEncoder.encode(item,"UTF-8");
					}catch(Exception e){
						
					}
					
					String results = "";
					for(int i = 0 ; i < snt.length-1 ; i ++ ){
						results += snt[i] + "=";
					}
					results += enc;
					request.setAttribute("EncodeUtil_var",results); 
            		return results;
	            };
            %>

            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
	    
        <jsp:include page="/components/common_js.jsp" flush="true" />
</div> 
      
</body>
</html>
