<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>
<%@ taglib uri="/WEB-INF/lib/online.tld" prefix="gui" %>

<%@page import="org.cambridge.ebooks.online.jpa.rss.Rss"%>

<%@page import="java.net.URLEncoder"%>
<%@page import="org.cambridge.ebooks.online.rss.RssServlet"%>
<%@page import="org.cambridge.ebooks.online.search.SearchResult"%>
<%@page import="org.cambridge.ebooks.online.search.SearcherBean"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
	
	<title>Search Results - Cambridge Books Online - Cambridge University Press</title>   
    
   	<jsp:include page="../aaa/components/acc_bar.jsp"></jsp:include>

</head>

<body>
    <% 
		session.setAttribute("helpType", "child");
		session.setAttribute("pageId", "86");
		session.setAttribute("pageName", "Search Results");
	%>
<div id="page-wrapper">    	    
        <!-- Start Header -->
        <div class="header">
            <div class="header_container">
                 <div class="topheader_divider">
                                                    
                      <!-- Start Accessibility Menu -->
                      <jsp:include page="../aaa/components/acc_links.jsp"></jsp:include>
                      <!-- End Accessibility Menu -->            
              	</div>
              
              <div class="clear"></div>
            
			  <div class="middleheader_divider">
             
              <jsp:include page="../aaa/components/org_info.jsp"></jsp:include>
              </div>
                 
			  <div class="clear"></div>
            
                 <!-- Start Topmenu -->
                 <div class="topmenu">
                  	<jsp:include page="../aaa/components/top_menu.jsp"></jsp:include>
					<jsp:include page="../aaa/components/ip_logo.jsp"></jsp:include> 
                 
     			 </div>
                 <!-- End Topmenu -->
                 
            </div>
        </div>     
        <!-- End Header -->

		<!-- Start Middle Content -->
        <div class="breadcrumbs_search_container">
        	<!-- Search Start -->
        	<jsp:include page="../aaa/components/search_container.jsp"></jsp:include>
        	<!-- Search End --> 

	        <!-- Breadcrumbs Start -->
			<div class="breadcrumbs_container">
				<span class="titlepage_hidden">You are here:</span> 
				<jsp:include page="/components/crumbtrail.jsp" flush="true" />
			</div>
			<!-- Breadcrumbs End -->
 		</div>
        <!-- End Middle Content -->               

		<a id="maincontent" name="maincontent" ></a>
		            
		<!-- Start Body Content -->
		<div class="content_container buttons">
				
			<div class="clear"></div>
					
		  	<!-- Titlepage Start -->
            <h2>Search Results</h2>   
            <!-- Titlepage End -->
            
            <div class="clear"></div>

<jsp:useBean id="searcherBean" class="org.cambridge.ebooks.online.search.SearcherBean" scope="session" ></jsp:useBean>
            <!--  Check if empty search Start -->
			<c:choose>
				<c:when test="${param.search == 'fail'}">
					<div class="border-div">
						<div id="content_wrapper04">
							<h3>No search term provided.</h3>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<input type="hidden" value="${searcherBean.initializeSearchString}" />
				</c:otherwise>
			</c:choose>
			<!--  Check if empty search End -->

			<!--  Check if zero results Start -->
            <c:if test="${searcherMap['pager'].resultSize == 0 && param.search != 'fail'}">
            		<div class="border-div">
						<div id="content_wrapper04">
							<h3>Your search returned <strong>0</strong> results.</h3>    
						</div>
					</div>
            </c:if>
            <!--  Check if zero results End -->
                
            <c:if test="${param.search != 'fail' && searcherMap['pager'].resultSize > 0}">
           		<!-- Access Information Start -->
            	<p><strong>Access Information:</strong></p>
            	<div class="icon_access">
	                <ul>
	                	<!-- 
    	                <li><img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_s_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Subscription Access" width="30" height="30" /></li>
        	            <li class="icon_access" title="Subscription Access">Subscription Access</li>
        	             -->
            	        <li><img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_f_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Free Access" width="30" height="30" /></li>
                	    <li class="icon_free" title="Free Access">Free Access</li>
                	</ul>
            	</div>
            	<!-- Access Information End -->
            </c:if>
            
            <div class="clear_bottom"></div>            
            
            <!-- Search Form Start -->
			<c:if test="${param.showSearch == 'show' }">
				<form id="advanceSearchForm" action="search_aaa?queryCount=8&searchType=advance" method="post"> 
					<c:choose>
						<c:when test="${param.clear == 'all'}">
							<jsp:include page="../aaa/components/adv_search_inputform1.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
							<jsp:include page="../aaa/components/adv_search_inputform2.jsp" flush="true"></jsp:include>
						</c:otherwise>
					</c:choose>
				
					<input type="submit" value="Search" name="search" class="input_buttons"/>
			   	 	<input type="submit"" value="Clear All" name="clearAll_aaa" class="input_buttons" />
				
				</form>	
				
			</c:if>
			
			<div class="clear_bottom"></div>   
			
			<!-- Start Hide/Show -->
			<form action="search_aaa?searchType=page&page=${searcherMap['pager'].currentPage}" method="post">
				<c:if test="${param.showSearch == 'show' }">
					<input type="hidden" name="showSearch" value="hide" />
					<input type="submit" name="hideSearchDetails" value="Hide Search Details" class="input_buttons"/>
				</c:if>
				<c:if test="${param.showSearch == 'hide' || empty param.showSearch}">
					<input type="hidden" name="showSearch" value="show" />
					<input type="submit" name="showSearchDetails" value="Show Search Details" class="input_buttons"/>
				</c:if>
			</form>
			<!-- End Hide/Show -->
			
 			<!-- Search Form Start -->	
                
            <div class="clear_bottom"></div>
         	
         	<!-- Test outofrange start -->   
         	<c:if test="${param.search != 'fail' && searcherMap['pager'].resultSize > 0 }">   
         	            
	            <!-- Search Result Query Start -->
	            <div class="fl">
	            	<h3>Book Chapters:</h3>
	            	<p>Your search returned <strong>${searcherMap['pager'].resultSize}</strong> results.</p>    
	            </div>   
	            
	            <div class="rss_container link_02">
	            	<div class="fr">&nbsp;|&nbsp;Search in <a href="http://journals.cambridge.org/action/quickSearch?search=${searcherBean.searchString}">Cambridge Journals</a></div>
		     
	                <%if(RssServlet.displayRss(request)){ %>
		        		<ul class="fr">
							<li><span class="icons_img rss_on"></span><a href="<%= System.getProperty("ebooks.context.path")%>aaa/popups/view_rss.jsf?keepThis=true&<%=Rss.SEARCH_TYPE%>=<%=Rss.RSS%>&TB_iframe=true&width=650&height=200"" class="thickbox" >Save Query as RSS Feed</a></li>
						</ul>  			
					<%}else{ %>
		        		<ul class="fr">
							<li><span class="icons_img rss_off"></span><a href="${pageContext.request.contextPath}/aaa/login.jsf?searchType=${param.searchType}">Log-in to Save Query as RSS Feed</a></li>
							
						</ul>
					<%}%>       

	            </div> 
	            <!-- Search Result Query End -->
	                  
	            <div class="clear_bottom"></div>
	                
	            <!-- Border Start -->
	            <div class="border-div">
	
	                  <!-- Bar Container Start -->
					 	<jsp:include page="../aaa/components/adv_search_results_menu.jsp" flush="true"></jsp:include>
	                  <!-- Bar Container End -->
	                                  
	                    <!-- Loop Search Info Start -->                                
	                    <c:forEach var="result" items="${searcherBean.searchResults}" varStatus="status">
	                    	                   	    	
	                    	<!-- Search Info Start -->  
	                    	<div class="div_search_info link_02">
	                    		
	                    		<div class="fl">
	                    			<h4><a href="aaa/ebook.jsf?bid=${result.bookId}" class="link05">
										<c:out value="${fn:trim(result.bookTitleDisplay)}" escapeXml="false" />
									</a></h4>
									
									<c:choose>
										<c:when test="${not empty result.authors}">
											<c:out value="by ${result.authors}" escapeXml="false" />
										</c:when>
										<c:otherwise>
											<c:out value="edited by ${result.editors}" escapeXml="false" />
										</c:otherwise>
									</c:choose>	
								</div>          	
									
								<!-- START CHOOSE APPROPRIATE ICON ACCESS -->
								<c:choose>						
									<c:when test="${result.hasAccess eq 'F'}">
										<div class="icon_access_on">
											<img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_f_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Free Access Enabled" width="30" height="30" />
										</div>
									</c:when>
									<c:when test="${result.hasAccess eq 'S'}">
										<div class="icon_access_on">
											<img src="<%= System.getProperty("ebooks.context.path") %>aaa/images/icon_s_access${sessionStyle == 'contrast_style' ? '_contrast' : '' }.gif" alt="Subscription Access Enabled" width="30" height="30" />
										</div>
									</c:when>							
									<c:otherwise>																
										<div class="icon_access_off" title="You don't have access."></div>
									</c:otherwise>
								</c:choose>
								<!-- END CHOOSE APPROPRIATE ICON ACCESS -->
								<div class="clear"></div>																		
									<c:if test="${not empty result.bookSubTitle}">
										<strong><c:out value="${result.bookSubTitle}" escapeXml="false" /></strong>
									</c:if>		
									<c:if test="${not empty result.volume}">									
										<c:out value="${result.volume}" escapeXml="false" /><br/>
									</c:if>	
									<c:if test="${not empty result.edition}">
										<c:out value="${result.edition}" escapeXml="false" /><br/>
									</c:if>	
									<c:if test="${not empty result.printDate}">
										Print Publication Year: <c:out value="${result.printDate}" escapeXml="false"/><br/>
									</c:if>	
									<c:choose>										
										<c:when test="${not empty result.liveDate}">
											Online Publication Date: <c:out value="${result.liveDate}" escapeXml="false"/>
										</c:when>
									</c:choose>
	                    	
	                    	<input type="hidden" value="${result.contentId}" />
							
										<c:if test="${not empty result.forewords}">
											Foreword&nbsp;by&nbsp;<strong><c:out value="${result.forewords}" escapeXml="false"/></strong>
											<br />
										</c:if>
										<c:if test="${not empty result.afterwords}">
											Afterword&nbsp;by&nbsp;<strong><c:out value="${result.afterwords}" escapeXml="false"/></strong>
											<br />
										</c:if>
										<c:if test="${not empty result.translators}">
											Translated&nbsp;by&nbsp;<strong><c:out value="${result.translators}" escapeXml="false"/></strong>
											<br />
										</c:if>									
										<c:if test="${not empty result.seriesName}">
											<c:choose>
												<c:when test="${not empty result.seriesNumber && result.seriesNumber ne 'null'}">
													<strong>Series:</strong> 
													<a href="aaa/series_landing.jsf?seriesCode=${result.seriesCode}&amp;seriesTitle=${result.seriesName}&amp;sort=Volume" class="link03">
													<c:out value="${result.seriesNameDisplay}" escapeXml="false" /></a><c:out value=" (No. ${result.seriesNumber})" escapeXml="false" />
												</c:when>
												<c:otherwise>
													<strong>Series:</strong> 
													<a href="aaa/series_landing.jsf?seriesCode=${result.seriesCode}&amp;seriesTitle=${result.seriesName}&amp;sort=Print+Date" class="link03">
													<c:out value="${result.seriesNameDisplay}" escapeXml="false" />
													</a>
												</c:otherwise>
											</c:choose>
											<br/>
										</c:if>
	                    	
									<br/>
									<strong>Online ISBN:</strong> <c:out value="${result.eisbnDisplay}" escapeXml="false"/>
									<br/><strong>Print ISBN:</strong> <c:out value="${result.pisbnDisplay}" escapeXml="false"/>
	                    	
	                    	<br/><br/>
	                    		<h5>
		                    		<a href="aaa/chapter.jsf?bid=${result.bookId}&amp;cid=${result.contentId}&pageTab=ce" >
										<c:if test="${not empty result.contentLabel}">	
											<c:out value="${result.contentLabel}" escapeXml="false"/> -
										</c:if>
										<c:out value="${result.contentTitle}" escapeXml="false" />
									</a>
								</h5>
								<c:if test="${not empty result.contributors}">
									<br />
									Contributed&nbsp;by&nbsp;<c:out value="${result.contributors}" escapeXml="false"/>
								</c:if>
								<c:if test="${not empty result.doi}">	
									<br/>						
									<strong>Chapter DOI:</strong> <c:out value="${result.doi}" escapeXml="false"/>
								</c:if>
								<p><c:out value="${result.snippet}" escapeXml="false" />...</p>
								<p><strong><c:out value="${result.page}"/></strong></p>	
								
								
								
								<!-- START PDF -->
								<c:choose>
									<c:when test="${result.hasAccess eq 'F' or result.hasAccess eq 'S' }">
										<c:set var="bookTitlePdf" value="${result.bookTitle}" scope="request" />
										<input type="hidden" id="showPdfBookTitle_${status.count}" value="<%=URLEncoder.encode((String)request.getAttribute("bookTitlePdf"), "UTF-8")%>" />		
										<ul>
											<li><span class="icons_img pdf_on" ></span>
												
													<c:url value="${pageContext.request.contextPath}/counterEbooks" var="counterEbooksURL">
														<c:param name="eisbn" value="${result.eisbn}" />
														<c:param name="title" value="${chapterLandingPageBean.chapterDoi}" />
														<c:param name="publisher" value="${chapterLandingPageBean.authorDisplay}" />
														
													</c:url>
													<%-- <a href="<c:out value="${counterEbooksURL}" escapeXml="true" />">Read PDF ${result.pdfSize}</a>
													showPdf('${result.eisbn}', '${result.pdf}', '${result.bookId}', 'showPdfBookTitle_${status.count}', '${result.contentId}', '${result.publisher}
													<a href="stamp_drm?eisbn=${eBookLandingPageBean.onlineIsbn}&pdf=${content.pdf}&bookTitle=${eBookLandingPageBean.bookTitle}" class="toc_pdf" >Read PDF &nbsp;${content.pdfSize}</a>
													--%>
													<%-- <a href="stamp_drm?eisbn=${result.eisbn}&pdf=${result.pdf}&bookTitle=${result.bookTitle}">Read PDF ${result.pdfSize}</a>--%>
													<%-- <a href="stamp_drm?eisbn=${result.eisbn}&pdf=${result.pdf}&bookTitle=<%=URLEncoder.encode((String)request.getAttribute("bookTitlePdf"), "UTF-8")%>">Read PDF ${result.pdfSize}</a>--%>
													<a href="/popups/pdf_viewer.jsp?cid=${result.contentId}" target="_blank" >Read PDF ${result.pdfSize}</a>
													
													
									
													
													
													<%-- 
													<a href="#"
														onclick="showPdf('${result.eisbn}', 
														'${result.pdf}', '${result.bookId}', 
														'showPdfBookTitle_${status.count}', 
														'${result.contentId}', 
														'${result.publisher}', 
														'${result.doi}');return false;" >
														Read PDF ${result.pdfSize}
													</a> 
													--%>
												
											</li>
											<%-- %><li class="icon_view_extract">&nbsp;|&nbsp;<a href="#">View Extract</a></li>--%>
										</ul>
									</c:when>
									<c:otherwise>
									<ul>
										<li><span class="icons_img pdf_off"></span>
											
												<c:choose>
													<c:when test="${not empty userInfo and not empty userInfo.username}">
														No Access to Read PDF ${result.pdfSize}
													</c:when>
													<c:otherwise>													
														<a href="${pageContext.request.contextPath}/aaa/login.jsf?searchType=${param.searchType}">Log-in to Read PDF ${result.pdfSize}</a>
													</c:otherwise>
												</c:choose>
											
										</li>								
										<%-- %><li class="icon_view_extract">&nbsp;|&nbsp;<a href="#">View Extract</a></li>--%>
									</ul>
									</c:otherwise>							
								</c:choose>
								<!-- End PDF -->
									
									
												                   
	                    	</div>
	                    	<!-- Search Info End -->
	               </c:forEach>
	               <!-- Loop Search Info End -->      
        	
        			<!-- Bar Container Start -->
					<jsp:include page="../aaa/components/adv_search_results_menu_bottom.jsp" flush="true"></jsp:include>
	                <!-- Bar Container End -->
        	
        		</div>
				<!-- Border End -->
				
			</c:if>
            <!-- Test outofrange end -->   
		    
            <!-- Footermenu Start -->
        	<jsp:include page="../aaa/components/footer_menuwrapper.jsp" flush="true"></jsp:include>
        	<!-- Footermenu End -->
        
        </div>        
        <!-- End Body Content -->     
        
   		<!-- FooterContainer Start -->
	  	  <jsp:include page="../aaa/components/footer_container.jsp" flush="true"></jsp:include>
	    <!-- FooterContainer End -->
        
</div> 
      
</body>

</html>