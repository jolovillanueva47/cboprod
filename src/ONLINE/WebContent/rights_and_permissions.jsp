<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	<!--<link rel="stylesheet" type="text/css" media="print" href="${pageContext.request.contextPath}/css/print.css" />
-->
</head>
<body>
	<c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">
	<div id="top_container">
	<div id="page-wrapper">
    	
    <f:subview id="subviewIPLogo">
		<c:import url="/components/ip_logo.jsp" />	
	</f:subview>
	
	<!-- Header Start -->
	<div id="header_container">	
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
	<!-- Header End -->
	<!-- Main Body Start -->
	<div id="main_container">

			<!-- Titlepage Start -->
			<h1 class="titlepage_heading">Rights &amp; Permissions</h1>
			<!-- Titlepage End -->
			
			<p>To apply for permission to reproduce parts of Cambridge Books Online content in ways not already allowed under the Terms of Use, follow the directions on the below pages:</p>
			
			<ul>
				<li><span class="icons_img bullet01">&nbsp;</span><a href="http://www.cambridge.org/us/information/rights/">North America</a></li>
				<li><span class="icons_img bullet01">&nbsp;</span><a href="http://www.cambridge.org/aus/information/rights_permissions.htm">Australia &amp; New Zealand</a></li>
				<li><span class="icons_img bullet01">&nbsp;</span><a href="http://www.cambridge.org/rights/">Anywhere else in the world</a></li>
			</ul>
			
	</div>  
    <!-- Main Body End -->
        
	<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	</div>
	</div>

	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	
	</div>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	

</body>
</f:view>
</html>