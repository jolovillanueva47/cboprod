<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/fn.tld" prefix="fn" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>
	<link href="${pageContext.request.contextPath}/css/treelist.css" rel="stylesheet" type="text/css" />

</head>

<body>	
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1539" scope="session" />
	<c:set var="pageName" value="Browse By Subject" scope="session" />
	
	<c:import url="/components/loader.jsp" />	
	
	<div id="main" style="display: none;"> 
	<div id="top_container">
	<div id="page-wrapper">	

	    <f:subview id="subviewIPLogo">
			<c:import url="/components/ip_logo.jsp" />	
		</f:subview>

	<!-- Header Start -->
	<div id="header_container">	        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>	

		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
    </div> 
    <!-- Header End -->
        <!-- Main Body Start -->
        <div id="main_container">

			<!-- Titlepage Start -->
			<%-- <span class="titlepage_heading"><h1>Subject</h1></span> --%>
			<div class="titlepage_heading"><h1>Subject</h1></div>
			
			<!-- Titlepage End -->


			<!-- Breadcrumbs Start -->
			<f:subview id="crumbtrailContainer">
				<c:import url="/components/crumbtrail.jsp" />	
			</f:subview>
			<!-- Breadcrumbs End -->
			<div class="clear"></div>
			<!-- Start Treelist --> 
			<div class="subjectTree">                
				<ul>
					<li class="search"><a id="A" class="icons_img tree_plus tree_item" href="javascript:void(0);"></a><a id="A" class="tree_item" href="javascript:void(0);">Humanities</a> [<a href="${pageContext.request.contextPath}/subject_landing.jsf?searchType=allSubjectBook&amp;subjectId=A&amp;subjectName=Humanities">View All</a>]
					</li>
					<li class="search"><a id="B" class="icons_img tree_plus tree_item" href="javascript:void(0);"></a><a id="B" class="tree_item" href="javascript:void(0);">Social Sciences</a> [<a href="${pageContext.request.contextPath}/subject_landing.jsf?searchType=allSubjectBook&amp;subjectId=B&amp;subjectName=Social Sciences">View All</a>]	
					</li>
					<li class="search"><a id="C" class="icons_img tree_plus tree_item" href="javascript:void(0);"></a><a id="C" class="tree_item" href="javascript:void(0);">Science and Engineering</a> [<a href="${pageContext.request.contextPath}/subject_landing.jsf?searchType=allSubjectBook&amp;subjectId=C&amp;subjectName=Science and Engineering">View All</a>]			
					</li>
					<li class="search"><a id="D" class="icons_img tree_plus tree_item" href="javascript:void(0);"></a><a id="D" class="tree_item" href="javascript:void(0);">Medicine</a> [<a href="${pageContext.request.contextPath}/subject_landing.jsf?searchType=allSubjectBook&amp;subjectId=D&amp;subjectName=Medicine">View All</a>]			
					</li>
					<li class="search"><a id="E" class="icons_img tree_plus tree_item" href="javascript:void(0);"></a><a id="E" class="tree_item" href="javascript:void(0);">English Language Teaching</a> [<a href="${pageContext.request.contextPath}/subject_landing.jsf?searchType=allSubjectBook&amp;subjectId=E&amp;subjectName=English Language Teaching">View All</a>]
					</li>
				</ul>
			</div> 
			<!-- End Treelist -->   

    	</div>  
        <!-- Main Body End -->

		<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
		</f:subview>

	</div> 
    </div>
	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>

	</div>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/subject.tree.js"></script>
    <f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>

</f:view>

</html>