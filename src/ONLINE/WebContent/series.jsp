<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Cambridge Books Online - Cambridge University Press</title>
	
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
    
</head>

<body>
    
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1804" scope="session" />
	<c:set var="pageName" value="Browse By Series" scope="session" />
	
   	<c:import url="/components/loader.jsp" />	
	<div id="main" style="display: none;"> 
	<div id="top_container">
	<div id="page-wrapper">
	
		<f:subview id="subviewIPLogo">
			<c:import url="/components/ip_logo.jsp" />	
		</f:subview>
    	    
	<!-- Header Start -->
	<div id="header_container">        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>   
	
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>  
     </div> 
     <!-- Header End -->
        <!-- Main Body Start -->
        <div id="main_container">        
             
        	<!-- Titlepage Start -->
            <span class="titlepage_heading"><h1>Series</h1></span>
            <!-- Titlepage End -->
            
            <!-- Breadcrumbs Start -->
			<f:subview id="crumbtrailContainer">
				<c:import url="/components/crumbtrail.jsp" />	
			</f:subview>
			<!-- Breadcrumbs End --> 
            <%--p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p--%>
            
        	<!-- Banner Start --> 
            <div id="banner_series_container">
            	
                <!-- Title Banner Start -->
                <div class="titlebanner_series">
                	<ul>
                    	<li>Series</li>
                        <li>Series by Subject</li>
                    </ul>
                </div>
                <!-- Title Banner End -->
                
           	  	<div class="banner_wrapper04">
                	<p>This site lists all our series in Academic, Education and English Language Teaching areas, and includes new and forthcoming series, as well as those that have now come to an end. Click on one of the options below to browse our series by A-Z listing, or search for your series using our 'quick search' box.</p>
                    <p><strong>Select your series by clicking on a letter.</strong></p>
                                        
                    <!-- Alpha List Start -->
	                <f:subview id="subviewAlphaListContainer">
						<c:import url="/components/series_alphalist_main.jsp" />
					</f:subview>
					<!-- Alpha List End -->
                    
                    <%--div id="arrow03"><ul>
                        <li><a href="all_series.jsf?searchType=allSeries">All Series</a></li>
                    </ul></div--%>
                </div>
                
                <%-- 
                <div id="banner_wrapper01">
                    <div id="bullet01">
						<ul>
							<c:if test="${termsBean.titleListHidden != null}">
							</c:if>
							<c:if test="${termsBean.bookMap != null}">
							<c:forEach var="book" items="${termsBean.bookMap}">								
								<li><a href="javascript:MM_windowOpener('${pageContext.request.contextPath}/series_landing.jsf?seriesCode=${book.key}&seriesTitle=${book.value}');">${book.value}</a></li>
							</c:forEach>
							</c:if>
						</ul>
					</div>
                </div>
                --%>
                <div class="banner_wrapper05">
                	<ul>
						<li><span class="icons_img arrow02">&nbsp;</span><a href="${pageContext.request.contextPath}/series_tree.jsf?SUBJECT_CODE=A">Humanities</a></li>
						<li><span class="icons_img arrow02">&nbsp;</span><a href="${pageContext.request.contextPath}/series_tree.jsf?SUBJECT_CODE=B">Social Sciences</a></li>
						<li><span class="icons_img arrow02">&nbsp;</span><a href="${pageContext.request.contextPath}/series_tree.jsf?SUBJECT_CODE=C">Science &amp; Engineering</a></li>
						<li><span class="icons_img arrow02">&nbsp;</span><a href="${pageContext.request.contextPath}/series_tree.jsf?SUBJECT_CODE=D">Medicine</a></li>	
					</ul>
                </div>
                
            </div>
			<div class="icons_img banner_bottom"></div>
            <!-- Banner End -->           
            
        </div>  
        <!-- Main Body End -->
        
		<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
		</f:subview>
        
	</div> 
	</div>
	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>

	</div>
	
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>

</f:view>

</html>