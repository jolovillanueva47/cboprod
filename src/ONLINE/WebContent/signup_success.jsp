<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
</head>

	

<!-- body (holds country classes) -->
<body> 
			
	<!-- bg Topline -->       
	<div id="top_container">
	
		<div id="page-wrapper">
			<f:subview id="subviewIPLogo">
				<c:import url="/components/ip_logo.jsp" />	
			</f:subview>
		
			<!-- Header Start -->
			<div id="header_container">
			    <f:subview id="subviewTopMenu">
					<c:import url="/components/top_menu.jsp" />	
				</f:subview>
				
				
		           
				<f:subview id="subviewSearchContainer">
					<c:import url="/components/search_container.jsp" />	
				</f:subview>
			</div>
			<div id="main_container">
				
				
				<h1 class="titlepage_heading" >Sign-up for a Trial</h1>
				<p>You have successfully signed up for a trial. An email was sent to your regional sales representatives, who will review your trial request.</p>		
												
				<f:subview id="subviewFooterMenuwrapper">
					<c:import url="/components/footer_menuwrapper.jsp" />
				</f:subview>
			</div>
		
		</div>
	</div>
		
	<f:subview id="subviewFooterContainer">
			<c:import url="/components/footer_container.jsp" />
	</f:subview>

  	
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>
</f:view>
</html>		
		