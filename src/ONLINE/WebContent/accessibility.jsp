<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	
</head>
<body>
	<c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">
	<div id="top_container">
	<div id="page-wrapper">
    	
    <f:subview id="subviewIPLogo">
		<c:import url="/components/ip_logo.jsp" />	
	</f:subview>
	
	<!-- Header Start -->
	<div id="header_container">	
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
	<!-- Header End -->
	<!-- Main Body Start -->
	<div id="main_container">

		<!-- Titlepage Start -->
		<h1 class="titlepage_heading">Accessibility</h1>
		<!-- Titlepage End -->

		<p>Cambridge Books Online recognizes the importance of making its web services available to the largest possible audience and has attempted to design and develop this website to be accessible by all users. Where possible this web site has been coded to comply with the World Wide Web Consortium (W3C) Web Accessibility Guidelines Priority Levels 1, 2 and 3 (Conformance Level "AAA").</p>
	
	    
	    <p>Cambridge Books Online will continue to test future releases of this site and remain committed to maintaining its compliance with appropriate accessibility guidelines and serving the widest possible audience for our services.</p>
	    
	    <p>For questions about our continuing efforts to make web-based information accessible to all users, or to report an accessibility problem on any of our pages, <a href="${pageContext.request.contextPath}/contact.jsf" class="link03">contact us</a>.</p>
	   	<br />
	   		    
		<h2>Browser / Platform Support</h2>
	
		<p>The following is a list of browsers / platforms that are supported on the Cambridge Books Online website:</p>
	
		<ul class="normal_list">
			<li>Internet Explorer 7.0+</li>
			<li>Firefox (Windows and Mac) 3.0+</li>
			<li>Safari (Windows and Mac) 4.0+</li>
			<li>Chrome (Windows) 4.0+</li>
			<li>Opera (Windows) 9.0+</li>
		</ul>

	</div>
	<!-- Main Body End -->
	
	<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	</div>	
	</div>

	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	</div>
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>


</body>
</f:view>
</html>