<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 


<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>

<body >

<div class="content">	
	<ul class="logIn">
		<c:choose>
			<c:when test="${not empty userInfo and not empty userInfo.username}">
				<li><a href="javascript:logout();" class="link10">Logout</a></li>
			</c:when>
			<c:otherwise><li><span style="font-weight: bold; color: gray;">Logout</span></li></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${eBookLogoBean.athensLoggedIn}">
				<li><a href="javascript:athensLogout();" class="link10">Athens Logout</a></li>
			</c:when>
			<c:otherwise><li><span style="font-weight: bold; color: gray;">Athens Logout</span></li></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${eBookLogoBean.shibbolethLoggedIn}">
				<li><a href="javascript:shibbolethLogout();" class="link10">Shibboleth Logout</a></li>
			</c:when>
			<c:otherwise><li><span style="font-weight: bold; color: gray;">Shibboleth Logout</span></li></c:otherwise>
		</c:choose>
    </ul>
   
</div><!-- end content -->

<script src="${pageContext.request.contextPath}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/common.js" type="text/javascript"></script>
<script language="JavaScript" >
	function athensLogout() {
		window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		window.parent.location = "${pageContext.request.contextPath}/home.jsf?alo=y";
		return true;
	}

	function shibbolethLogout() {
		window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		window.parent.location = "${pageContext.request.contextPath}/home.jsf?slo=y";
		return true;
	}
</script>

<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>

</html>