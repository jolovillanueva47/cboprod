<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%@page import="org.cambridge.ebooks.online.jpa.rss.RssFaceted"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Cambridge Books Online - Cambridge University Press</title>
	
	<f:subview id="common_css">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
    
    <style type="text/css">
		<!--	
		@import url("${pageContext.request.contextPath}/css/jquery/datepicker/redmond/jquery-ui-1.7.2.redmond.css");
		-->
  	</style>
</head>

<body>
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1527" scope="session" />
	<c:set var="pageName" value="Advance Search" scope="session" />
	
	<c:import url="/components/loader.jsp" />
	
	<div id="main" style="display: none;">    
		<div id="top_container">
			<div id="page-wrapper">	
		
				<f:subview id="subviewIPLogo">
					<c:import url="/components/ip_logo.jsp" />	
				</f:subview>
	    	
				<!-- Header Start -->
				<div id="header_container">        	
					<f:subview id="top_menu">
						<c:import url="/components/top_menu.jsp" />	
					</f:subview>
			           
					<f:subview id="search_container">
						<c:import url="/components/search_container.jsp" />	
					</f:subview>
			    </div> 
			    <!-- Header End -->
			    
       			<!-- Main Body Start -->
        		<div id="main_container">
        		
		        	<!-- Titlepage Start -->
		            <div class="titlepage_advancedsearch"></div>
		            <!-- Titlepage End -->
            
            
					<!-- Breadcrumbs Start -->
					<f:subview id="crumbtrail">
						<c:import url="/components/crumbtrail.jsp" />	
					</f:subview>
					<!-- Breadcrumbs End -->
					
		            <!-- Search Again Start -->       
		            <f:subview id="advance_search_form">
						<c:import url="/components/search/advance_search_form.jsp" />	
					</f:subview>
				    <!-- Search Again End -->
		        </div>
		        <!-- Main Body End -->   
        
        		<div class="clear_list"></div>    
        
				<f:subview id="footer_menuwrapper">
					<c:import url="/components/footer_menuwrapper.jsp" />
				</f:subview>        
			</div> 
		</div>
		
		<f:subview id="footer_container">
			<c:import url="/components/footer_container.jsp" />
		</f:subview>
	
	</div>
	
	<f:subview id="common_js">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
		 	
	<script type="text/javascript" src="js/date.js"></script>
    <script type="text/javascript" src="js/jquery/jquery_collapse.js"></script>
	<script type="text/javascript" src="js/jquery/jquery.validate.min.js" ></script>
	<script type="text/javascript" src="js/search_advance.js"></script>
	<script type="text/javascript" >
		//<![CDATA[		
		function showRss(element) {			
			element.href = "popups/view_rss.jsf?keepThis=true&<%=RssFaceted.SEARCH_TYPE%>=<%=RssFaceted.RSS%>" +  
				  "&TB_iframe=true&width=650&height=330";		
		}
		//]]>
	</script>
	<f:subview id="google_analytics">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
</body>

</f:view>

</html>