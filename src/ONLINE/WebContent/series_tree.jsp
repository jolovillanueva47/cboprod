<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>
	<link href="${pageContext.request.contextPath}/css/treelist.css" rel="stylesheet" type="text/css" />


</head>

<body>
	<input id="feedIds" type="hidden" value="${feedsBean.rssFeedList}"/>
	
	<c:set var="helpType" value="child" scope="session" />
	<c:set var="pageId" value="1700" scope="session" />
	<c:set var="pageName" value="Browse Series By Subject" scope="session" />

	<c:import url="/components/loader.jsp" />	
	<div id="main" style="display: none;"> 
	<div id="top_container">
	<div id="page-wrapper">    	

		<f:subview id="subviewIPLogo">
			<c:import url="/components/ip_logo.jsp" />	
		</f:subview>	

	<!-- Header Start -->
	<div id="header_container">        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>

		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
    </div> 
    <!-- Header End -->
        <!-- Main Body Start -->
        <div id="main_container">

        	<!-- Titlepage Start -->
        	<span class="titlepage_heading"><h1>Series</h1></span>
            <!-- Titlepage End -->

          <!-- Breadcrumbs Start -->
			<f:subview id="crumbtrailContainer">
				<c:import url="/components/crumbtrail.jsp" />	
			</f:subview>
			<!-- Breadcrumbs End -->  
            <!-- <p><b>Select your series by clicking on a letter.</b></p> -->            
            <!-- <div id="content_wrapper01"> --> 
                <%--       
                <!-- Border Line01 Start -->
                <div id="border01_container">  
                    <!-- Alpha List Start -->
	                <f:subview id="subviewAlphaListContainer">
						<c:import url="/components/series_alphalist_sub.jsp" />
					</f:subview>
					<!-- Alpha List End -->
                </div>  
                <!-- Border Line01 End -->

              	<!-- Series Count Start -->              	
				${seriesSubjectSearcherBean.initializeSeriesCount}
				<f:subview id="subviewSeriesCountContainer">
					<c:import url="/components/series_count.jsp" />
				</f:subview>
				<!-- Series Count End -->
				--%>

				<!-- Start Treelist --> 
				<div class="subjectTree onSeries">                
					<ul>
						<li class="search"><a id="A" class="icons_img tree_item tree_plus level1" href="javascript:void(0);"></a><a id="A" class="tree_item" href="javascript:void(0);">Humanities</a> [<a href="${pageContext.request.contextPath}/all_series.jsf?searchType=allSubjectBook&seriesCode=A&firstLetter=Humanities&seriesName=Humanities">View All</a>]
							<c:if test="${feedsBean.contains['A'] == 'A'}">
								<a href="${pageContext.request.contextPath}/feeds/rss/feed_A_rss_2.0.xml" target="_blank"><img src="${pageContext.request.contextPath}/images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>
							</c:if>							
						</li>
						<li class="search"><a id="B" class="icons_img tree_item tree_plus level1" href="javascript:void(0);"></a><a id="B" class="tree_item" href="javascript:void(0);">Social Sciences</a> [<a href="${pageContext.request.contextPath}/all_series.jsf?searchType=allSubjectBook&seriesCode=B&firstLetter=Social Sciences&seriesName=Social Sciences">View All</a>]
							<c:if test="${feedsBean.contains['B'] == 'B'}">
								<a href="${pageContext.request.contextPath}/feeds/rss/feed_B_rss_2.0.xml" target="_blank"><img src="${pageContext.request.contextPath}/images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>
							</c:if>							
						</li>
						<li class="search"><a id="C" class="icons_img tree_item tree_plus level1" href="javascript:void(0);"></a><a id="C" class="tree_item" href="javascript:void(0);">Science and Engineering</a> [<a href="${pageContext.request.contextPath}/all_series.jsf?searchType=allSubjectBook&seriesCode=C&firstLetter=Science and Engineering&seriesName=Science and Engineering">View All</a>]
							<c:if test="${feedsBean.contains['C'] == 'C'}">
								<a href="${pageContext.request.contextPath}/feeds/rss/feed_C_rss_2.0.xml" target="_blank"><img src="${pageContext.request.contextPath}/images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>
							</c:if>							
						</li>
						<li class="search"><a id="D" class="icons_img tree_item tree_plus level1" href="javascript:void(0);"></a><a id="D" class="tree_item" href="javascript:void(0);">Medicine</a> [<a href="${pageContext.request.contextPath}/all_series.jsf?searchType=allSubjectBook&seriesCode=D&firstLetter=Medicine&seriesName=Medicine">View All</a>]
							<c:if test="${feedsBean.contains['D'] == 'D'}">
								<a href="${pageContext.request.contextPath}/feeds/rss/feed_D_rss_2.0.xml" target="_blank"><img src="${pageContext.request.contextPath}/images/icon_rss.gif" title="RSS Feeds" alt="RSS Feeds" /></a>
							</c:if>							
						</li>
					</ul>
				</div>

				<!-- End Treelist -->  

            	<!-- </div> -->    

    	</div>  
        <!-- Main Body End -->

		<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
		</f:subview>

	</div> 
    </div>
	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>

	</div>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/subject.tree.js"></script>
    <f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>


</body>

</f:view>

</html>
	