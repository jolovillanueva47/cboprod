<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	<link rel="stylesheet" type="text/css" media="print" href="${pageContext.request.contextPath}/css/print.css" />
</head>
<body>
	<c:import url="/components/loader.jsp" />
	<div id="main" style="display: none;">
	<div id="top_container">
	<div id="page-wrapper">
    	
    <f:subview id="subviewIPLogo">
		<c:import url="/components/ip_logo.jsp" />	
	</f:subview>
	
	<!-- Header Start -->
	<div id="header_container">	
        	
		<f:subview id="subviewTopMenu">
			<c:import url="/components/top_menu.jsp" />	
		</f:subview>
		
		<f:subview id="subviewSearchContainer">
			<c:import url="/components/search_container.jsp" />	
		</f:subview>
           
    </div>
	<!-- Header End -->
	<!-- Main Body Start -->
	<div id="main_container">

			<!-- Titlepage Start -->
			<h1 class="titlepage_heading">Privacy Policy</h1>
			<!-- Titlepage End -->
			
			<p>This website is operated by Cambridge University Press, which is a Syndicate of the University of Cambridge. Our principal place of business is at the Edinburgh Building, Shaftesbury Road, Cambridge, CB2 8RU. Cambridge University Press, the University of Cambridge and its associated companies will use the information we collect from you in accordance with this policy. A list of our associated companies is available on request to <a class="link03" href="mailto:legalservices@cambridge.org">legalservices@cambridge.org</a>.</p>
			
			<p>This privacy policy is specific to Cambridge Books Online (hereafter CBO) and overrides any other privacy policy or legal notice appearing elsewhere on Cambridge University Press websites.</p>

			<p>Cambridge University Press (hereafter Cambridge) is committed to protecting your privacy online. If you have any questions about our privacy policy, please <a href="${pageContext.request.contextPath}/contact.jsf" class="link03">contact us</a>.</p>

			<p>By using the CBO website, you are accepting the practices described in this statement.</p><br />

			<h2>Collection and Use of Information</h2>

			<p>When you apply for a free trial, ask for further information about CBO or purchase access to content within CBO, we ask for your name, e-mail address, postal address, and other relevant personal information. Personal information submitted in this way is added to our access control database and to customer databases at Cambridge and will be used to enable us to provide you with access to CBO and to carry out administration, statistical analysis and market research. If you are purchasing access to content within CBO then we will use your information to supply such access, to bill you and to contact you where necessary concerning your order.  Your information will only be updated if you inform us of any changes to your personal details. </p>
		
			<p>Where you have consented, we may contact you on occasion by post, telephone, fax and email about products and services we think might be of interest to you. If you decide in the future that you do not want to receive such information, please email <a class="link03" href="mailto:mlist@cambridge.org">mlist@cambridge.org.</a></p>
			
			<p>Where you have consented, we may make your details available to other reputable organisations whose product or services we think might be of interest to you. If you decide in the future that you do not want us to make your information available in this way please email <a class="link03" href="mailto:mlist@cambridge.org">mlist@cambridge.org</a>.</p>

			<p>We may transfer your personal details to other regions in which we have offices throughout the world, namely Europe, the Middle East and Africa; the Americas; and Asia-Pacific. You should be aware that some of our branches are in countries which do not have data protection laws. If you do not want us to make the information available in this way email <a class="link03" href="mailto:mlist@cambridge.org">mlist@cambridge.org</a>.</p>
			
			<p>You may notify us at any time either in writing or by telephone, fax or e-mail that you object to being contacted in a particular stated way and we will amend our records accordingly.</p>
			
			<p>When you contact us with order queries, technical problems, feedback on CBO, etc, we will also request personal information (such as your name, e-mail address and other contact details) from you so that we can respond to your query appropriately. </p>
			
			<p>We may disclose your information to our agents and service providers for the purposes set out above.</p>
			
			<p>All personal information received from you is subject to processing centrally in the Europe branch of Cambridge University Press, England. Access to our databases at Cambridge is only given to those of our employees who need such access in order to carry out necessary processing of your account.  We will retain your information for a reasonable period or as long as the law requires.</p>
			
			<br/>
			<h2>Cookies</h2>
			
			<p>We use cookies on this web site. Cookies are small amounts of information that we transfer to your computer's hard drive through your web browser. They tell us when you have visited our site and where you have been. They do not identify you personally, just the presence of your browser. Cookies make it easier for you to log on and use the site during future visits. They also enable us to provide you with a more personalised service. Should you wish to do so, your browser's help section should be able to warn you before accepting cookies and how to filter, delete or disable them, although in the latter case, you may not be able to use certain features on our site as a result. </p>
			
			<p>Subscribers may sign in with or without using cookies.</p>

			<br/>
			<h2>Changes to our privacy policy</h2>

			<p>Any changes to our privacy policy in the future will be posted to this site, and where appropriate, sent via e-mail notification to your email address.</p>
			
			<br/>
			<h2>Accessing and updating</h2>
			
			<p>You are entitled to see the information held about you and you may ask us to make any necessary changes to ensure that it is accurate and kept up to date. If you wish to do this, please contact us at <a href="mailto:legalservices@cambridge.org" class="link03">legalservices@cambridge.org</a>. We are entitled by law to charge a fee of �10 to meet our costs in providing you with details of the information we hold about you.</p>
			
			<br/>
			<h2>Contact</h2>
			
			<p>If you have any requests in relation to the use we make of your information, please email <a href="mailto:mlist@cambridge.org" class="link03">mlist@cambridge.org.</a></p>
			
			<p>All other comments and queries relating to the administration of your relationship with us, our use of your information and this Privacy Policy are welcomed and should be addressed to The Legal Services Director, Cambridge University Press, The Edinburgh Building, Shaftesbury Road, Cambridge, CB2 8RU, England (<a href="mailto:legalservices@cambridge.org" class="link03">legalservices@cambridge.org</a>).</p>

        </div>  
        <!-- Main Body End -->
	
	<f:subview id="subviewFooterMenuwrapper">
			<c:import url="/components/footer_menuwrapper.jsp" />
	</f:subview>
	</div>
	</div>

	<f:subview id="subviewFooterContainer">
		<c:import url="/components/footer_container.jsp" />
	</f:subview>
	
	</div>

	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>


</body>
</f:view>
</html>