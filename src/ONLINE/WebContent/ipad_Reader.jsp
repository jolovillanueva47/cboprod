<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>
	
	<link rel="stylesheet" type="text/css" media="screen, print" href="${pageContext.request.contextPath}/css/popup.css" />
</head>


<body >

	<div class="content">

	<ul>
    	<li class="header"><div class="headerTools"></div>Choose your PDF Reader</li>
    	<li><span class="icons_img icon_adobereader">&nbsp;</span><a id="adobeReader" title="Reader" href="#" onclick="${bookBean.cookieValue}">Adobe Reader</a></li>
        <li><span class="icons_img icon_goodreader">&nbsp;</span><a id="goodReader" title="Reader" href="#" onclick="${bookBean.cookieValue}">GoodReader</a></li>        
    </ul>
    
	</div>

<c:import url="/components/common_js.jsp" />
<script type="text/javascript">
$(document).ready(function(){
	$("#adobeReader").click(function(){

		var x = '1';
	//	alert(x);
		toPage(x);
		
	});
	$("#goodReader").click(function(){

		var x = '2';
	//	alert(x);
		toPage(x);
		
	});
});
		
function toPage(pageName){
	
	var bid = 'bid=' + '<%=session.getAttribute("bdoi")%>';
	var cid = '&cid=' + '<%=session.getAttribute("cdoi")%>';
	var pId = '&p=' + '<%=session.getAttribute("pId")%>';
	var reader = '&reader='+pageName;
	var sType = 'searchType=' + '<%=session.getAttribute("sType")%>';
	var sText = '&searchText=' + '<%=session.getAttribute("sText")%>';
	
	var prevUrl = '<%=session.getAttribute("prevPath")%>';

	if(prevUrl.indexOf('chapter.jsp') > -1){
		var url = '${pageContext.request.contextPath}/chapter.jsf?' + bid + cid + pId + reader;
	} else if(prevUrl.indexOf('search_results.jsp') > -1){
		var url = '${pageContext.request.contextPath}/search?' + sType + sText + reader;
	} else if(prevUrl.indexOf('ebook.jsp') > -1){
		var url = '${pageContext.request.contextPath}/ebook.jsf?' + bid + reader;
	} else{
		alert(prevUrl);
	}
	window.parent.location=  url ;
	self.parent.tb_remove();
}

</script>

</body>

</html>