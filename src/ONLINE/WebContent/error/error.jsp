<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head><title>500 Internal Server Error</title></head><body>
<h1>Internal Server Error</h1>
<p>The server encountered an internal error or misconfiguration and was unable to complete your request.</p>
<p>This might have happened because you were attempting to access a piece of content that is not currently available.</p>
<p>Please contact your regional support team by emailing <a href="mailto:techsupp@cambridge.org">techsupp@cambridge.org</a> if you are in the Americas, or <a href="mailto:onlinepublications@cambridge.org">onlinepublications@cambridge.org</a> if you are in the United Kingdom or the rest of the world. Please let them know the time the error occurred, along with details about the page you were viewing on the site and any actions you took on the site immediately before the error occurred.</p>
<%--p>Additionally, a 404 Not Found error was encountered while trying to use an ErrorDocument to handle the request.</p> --%>
</body></html>