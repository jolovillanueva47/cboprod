<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<head>
	<title>This Book is already in use.</title>
</head>
<body>
	<h1>This Book is already in use.</h1>
	<p>This book is not currently available to you because it is being used by someone else at your institution.</p>
	<p>Please try again later. If you are seeing this message, your institution has purchased limited-concurrency access to this title - for more about how limited concurrency works, please read our <a href="#" onclick="MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" title="FAQ">FAQs</a>.</p>
		
	<script type="text/javascript">
		function MM_openWindow(theURL, winName, features) {
			
			var help_win_width = 850;
			var help_win_height = 800;
			
			if (screen.availWidth > help_win_width){
				var help_win_top = parseInt((screen.availHeight/2) - (help_win_height/2));
			}else{
				help_win_top = 0;
			}
			if (screen.availWidth > help_win_width){
				var help_win_left = parseInt((screen.availWidth/2) - (help_win_width/2));
			} else {
				var help_win_left = 0;
			}
	
			var features = features + ",width=" + help_win_width + ",height=" + help_win_height + ",top=" + help_win_top + ",left=" + help_win_left;
			var h = window.open(theURL,winName,features);
			h.focus();
			
			return h;
		}
	</script>
</body>
</html>