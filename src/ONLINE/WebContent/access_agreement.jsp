<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambridge Books Online - Cambridge University Press</title>

	<f:subview id="commonCssSubview">
		<c:import url="/components/common_css.jsp" />
	</f:subview>	
	
	<style type="text/css">
	<!--	
	@import url("${pageContext.request.contextPath}/css/users_administrator_CBO.css");	
	@import url("${pageContext.request.contextPath}/css/validation.css");
	-->
  	</style>
</head>
	
	<body >		
  	<!-- Header page information -->
	




<div id="strip">

	<h1>Cambridge Journals Online</h1>

  <img src="${pageContext.request.contextPath}/images/cbo_logo.gif" width="130" height="37" title="Cambridge Books Online" alt="Cambridge Books Online" />
</div>


<div id="content">

<h1>Order Management: Access Agreement</h1>

<div id="iFrame">
  <iframe src="<%=System.getProperty("order.pdf.ack.path") %>" name="orderPDF" scrolling="yes"></iframe>
</div>


<div id="agreement">
	
	<form action="accessAgreement" id="acceptanceForm" method="post">	
		<input type="hidden" name="memberId" value="${param.memberId}" />		
		<input type="hidden" name="orderId" value="${param.orderId}" />
		<input type="hidden" name="bodyId" value="${param.bodyId}" />
	
	<c:choose>
  			<c:when test="${empty ACCESS_AGREEMENT}">			
  	<table cellpadding="0" cellspacing="0" width="100%">
  		  			  		  
    	<tr>
        	<td class="title" width="10%">Name:</td>
            <td class="fields"><input name="name" type="text" autocomplete="off"/></td>
        </tr>
        
        <tr>
        	<td class="title">Title:</td>
            <td class="fields"><input name="title" type="text" autocomplete="off"/></td>
        </tr>
        
  		<tr class="borderTop">
        	<td colspan="2">
        		<input name="confirm" type="checkbox" value="" id="confirmId"/>&nbsp;<label for="confirmId" >I accept the terms of this Access Agreement and I confirm that I am authorised to do so.</label>
        		<label for="confirm" class="error" style="display:none">Your confirmation is required.</label>
        	</td>
        </tr>
        
        <tr class="borderTop">
        	<td colspan="2"><input name="" type="submit" value="Accept" class="button" /></td>
        </tr>
        	
    </table>
    		</c:when>
        	<c:otherwise>        	        		
        		<div class="message">
        			<c:choose>
        				<c:when test="${ACCESS_AGREEMENT.error}">
        					<p><img src="${pageContext.request.contextPath}/images/error.jpg" />Error: Access Agreement already acknowledged.</p>
        				</c:when>        				
        				<c:otherwise>
        					<p><img src="${pageContext.request.contextPath}/images/info.jpg" />Access agreement acknowledgement successful. The Cambridge Books Online link will follow via email.</p>
        				</c:otherwise>
        			</c:choose>
        		</div>
        		<table cellpadding="0" cellspacing="0" width="100%">
	        		<tr>
	        			<td class="title" colspan="2">Access Agreement Acknowledgement Details</td>
	        		</tr>
	        		<tr class="borderTop">
	        			<td class="title" width="15%">Name:</td>
	        			<td class="fields"><c:out value="${ACCESS_AGREEMENT.accessConfirmeeName}"></c:out></td>
	        		</tr>
	        		<tr>
	        			<td class="title">Title:</td>
	        			<td class="fields"><c:out value="${ACCESS_AGREEMENT.accessConfirmeeTitle}"></c:out></td>
	        		</tr>
	        		<tr>
	        			<td class="title">Timestamp:</td>
	        			<td class="fields"><c:out value="${ACCESS_AGREEMENT.accessConfirmeeTimestamp}"></c:out></td>
	        		</tr> 
	        	</table>      			
        	</c:otherwise>
        </c:choose>
    
    
	</form>
</div>

</div>

	<!-- Footer -->
	



<div id="footer">
<ul>
	<li>&copy; Cambridge University Press <script type="text/javascript" language="JavaScript"><!--
var today = new Date();
document.write(today.getFullYear());
//--></script>.</li>

</ul>
</div>  	
	<f:subview id="commonJsSubview">
		<c:import url="/components/common_js.jsp" />
	</f:subview>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.min.js" ></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#acceptanceForm").validate({			
			rules: {
				name : "required",
				title : "required",
				confirm :	"required"
			}
		});
	}); 
	</script>
	
	<f:subview id="googleAnalyticsSubview">
		<c:import url="/components/google_analytics.jsp" />
	</f:subview>
		
	<script type="text/javascript">
		var selectedOrg = "${param.SELECTED_ORG}";
	</script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/subscription.js" ></script>
	</body>
</f:view>	
</html>		
	