<link rel="stylesheet" href="${pageContext.request.contextPath}/clc/style/style.css" type="text/css" media="screen" />
<link href="${pageContext.request.contextPath}/clc/style/smoothDivScroll.css" rel="stylesheet" type="text/css" /> 
<!--[if IE 8]>
<link href="${pageContext.request.contextPath}/clc/style/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 7]>
<link href="${pageContext.request.contextPath}/clc/style/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 6]>
<link href="${pageContext.request.contextPath}/clc/style/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
<% 
String userAgent = request.getHeader("User-Agent");
if(userAgent.indexOf("Opera") > -1) { %>	
<style type="text/css">
	span.button button, del.button span, span.button input {
		_behavior:expression(
			(function(el){
	
				if( typeof( behavior_onMouseEnter) == 'undefined'){				
					behavior_onMouseEnter = function(el){					
						var dEl = this.parentNode;					
						var sClass = dEl.className ;
						dEl.__defaultClassName = sClass ;
						dEl.className = sClass + ' button-behavior-hover';	
						this.setCapture();
					};
	
					behavior_onMouseLeave = function(el) {
						var dEl = this.parentNode;
						dEl.className = dEl.__defaultClassName ;
						dEl.__defaultClassName = undefined;
						this.releaseCapture();
					};
	
				};			
				
				el.runtimeStyle.behavior = 'none';
				el.onmouseenter = behavior_onMouseEnter;
				el.onmouseleave = behavior_onMouseLeave;			
				
			})(this));
	
	}
</style>	
<%}%>