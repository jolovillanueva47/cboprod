<%@page import="org.cambridge.ebooks.online.organisation.OrganisationWorker"%>

<div><input id="ebooksContextPath" type="hidden" value="${pageContext.request.contextPath}/" /></div>

<script src="${pageContext.request.contextPath}/clc/js/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clc/js/jquery/thickbox.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clc/js/common.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clc/js/search_common.js" type="text/javascript"></script>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%
	Object logoutAthens = session.getAttribute("logoutAthens");
	Object logoutShibb = session.getAttribute("logoutShibb");
	
	if ( logoutAthens instanceof Boolean ) 
	{ 
		session.removeAttribute("logoutAthens");
%>	
	<script language="JavaScript" >
		
		window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		
	</script>
<%
	}
	else if ( logoutShibb instanceof Boolean )
	{
		session.removeAttribute("logoutShibb");
%>	
	<script language="JavaScript" >
		
		window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
		
	</script>
<%
	}
%>

<script type="text/javascript">

	function getCjoLogin(text){
		var id = text.split("--->")[1];		
		return "<%=System.getProperty("cjo.url")%>accmanagement?id=" + id + "&topage=stream";
	}
	
	$(document).ready(function() {

		$("#submitUsrnamePassword").click(function(){
			//$("#loginLoadingId").show();
			$username = document.getElementById("username").value;
			$password = document.getElementById("password").value;
			var randVal = Math.random();

	
     		$.post("<%= System.getProperty("ebooks.context.path") %>login", {username:$username, password:$password, rand: randVal}, function(xml) {         		
     		//$.post("http://localhost:8080/login", {username:$username, password:$password, rand: randVal}, function(xml) {
     		//$.post("login", {username:$username, password:$password, rand: randVal}, function(xml) {
				//var resText = $("response", xml).text();         		
				var resText = xml;

           		if(resText.indexOf("success") > -1) {
               		/**    
           			$("#loginLoadingId").hide();
           			self.parent.tb_remove();		
           			**/

           			var locationUrl = window.parent.location.href; 

           			
            		locationUrl = locationUrl.replace("?lo=y&", "?");
            		locationUrl = locationUrl.replace("?lo=y", "");
					locationUrl = locationUrl.replace("&lo=y", "");				

					if(/#$/.test(locationUrl)) {
						locationUrl = locationUrl.replace("#", "");
					}

					window.parent.location=locationUrl;
					
					//window.parent.location="http://localhost:8080/home.jsf";

            		
            	} else if ($("response", xml).text()=='redirect') {
            		toPage('forgottenPassword');
            	} else {
            		//$("#loginLoadingId").hide();
   					$("#result").html(resText);
   				}         
     		});
     	});

		$("#mainForm").bind("keypress", function(e) {
	        if(e.keyCode==13){
	        	$("#submitUsrnamePassword").click();
	        	return false;
	        }
		});


		$("#search_text").bind("keypress", function(e) {
	        if(e.keyCode==13){
	        	$("#search_text_go").click();
	        	return false;
	        }
		});


		$("#search_text_go").click(function() {
	        	var valid = false;
				var term = jQuery.trim($("#search_text").val());
				if(term == "Please enter a keyword") {
					term = "";
				}
				if (term != null && term.length > 0) {
					valid = true;
				}
				
				if (valid) {
					var mainForm = document.getElementById("mainForm");
					$("#mainForm").attr("action", ebooksContextPath + "search?searchType=quick&searchText=" + term);
					$("#mainForm").submit();
				} else {
					alert("Invalid search.");
				} 
		});

   	});

	function toPage(pageName){
		window.parent.location='<%= System.getProperty("cjo.url") %>' + pageName + '?displayname=${referalName}';
		self.parent.tb_remove();
	}

</script>


<script type="text/javascript">
function athensLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "clc/home.jsf?alo=y";
	return true;
}

function shibbolethLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "clc/home.jsf?slo=y";
	return true;
}

function clclogout() { 
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.parent.location = ebooksContextPath + "clc/home.jsf?lo=y";
	return true;
}
</script>

<!-- this is for logging out on cjo -->
<c:if test="${not empty param.JSESS}">
	<img style="display: none" src="<%= System.getProperty("cjo.alias") %>cbo/cboImageLogout" />
</c:if>



