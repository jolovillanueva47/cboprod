<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.login.authentication.OrgConLinkedMap"%>


<script type="text/javascript">
	var cjoUrl = "<%=System.getProperty("cjo.url")%>";
  	var contextPath = "<%=request.getContextPath()%>";
	var orgIdSet = "<%=OrgConLinkedMap.getAllBodyIds(request.getSession())%>";
</script>

          <!-- Topmenu Start -->
          <div class="nav">
              <ul>
                    <li class="nav_home_current"><a href="${pageContext.request.contextPath}/clc/home.jsf" title="Home"></a></li>
                    <li class="nav_about"><a href="${pageContext.request.contextPath}/clc/templates/about.jsf" title="About"></a></li>
                    <li class="nav_faq"><a href="#" onclick="MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');return false;" title="FAQ"></a></li>
                    <li class="nav_help"><a href="#" onclick="showHelp('<%= System.getProperty("ebooks.context.path") %>', '<%= session.getAttribute("helpType") %>', '<%= session.getAttribute("pageId") %>', '<%= session.getAttribute("pageName") %>');return false;" title="Help"></a></li>                   
                    <li class="nav_forlibrarians">
                		<c:set var="cjoUrl" value='<%= System.getProperty("cjo.url")%>' />
                		<c:set var="pageId" value='<%= System.getProperty("librarian.pageId") %>' />                	
                		<c:url value="${pageScope.cjoUrl}accmanagement" var="forLibrariansUrl">
                			<c:param name="topage" value="stream" />
                			<c:param name="pageId" value="${pageScope.pageId}" />                		
                		</c:url>
                      	<a href="#" onclick="return CJOLoginLink('<c:out value="${forLibrariansUrl}" escapeXml="true" />','librarianLink')" id="librarianLink" title="For Librarians"></a>
                    </li>
                    
                	<c:if test="${orgConLinkedMap.userLogin.userType ne null}" >    
		        	<c:choose>		        		
		        		<c:when test="${orgConLinkedMap.userLogin.userType eq 'AO'}" >
		        			<li class="nav_account"><a href="#" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=configureIPDomain','adminLink')" id="adminLink"></a></li>
		           		</c:when>
		           		<c:otherwise>
		           			<li class="nav_account"><a href="#" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=configureIPConsortiaDispatcher','adminLink')" id="adminLink"></a></li>
		           		</c:otherwise>
		           	</c:choose>
					</c:if>
					
					
              </ul>
          </div>
          <!-- Topmenu End -->  
          
          
                   	    <!-- Search Start -->
            	<table id="search_container" cellspacing="0" cellpadding="0">
            	  <tr>
            	    <td><input name="search_text" id="search_text" type="text" value="Please enter a keyword" onfocus="clearDefault(this)" class="text_search" /></td>
            	    <td><a name="search_text_go" id="search_text_go" href="#" class="icons_img bot_search" title="Click to Search"></a></td>
          	      </tr>
            	  <tr>
            	  <td colspan="2">
                      <ul>
                          <li><span class="icons_img arrow01">&nbsp;</span><a href="${pageContext.request.contextPath}/advance_search.jsf">Advanced search</a></li>
                      </ul>
                  </td>
          	      </tr>
          	    </table>
            	<!-- Search End -->
            	
            	
 
            	
     