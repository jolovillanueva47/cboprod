<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
User user = (User) session.getAttribute("userInfo");
System.out.println("user: "+user);
System.out.println("orgBodyId: "+session.getAttribute("orgBodyId"));

String orgnames = (String) session.getAttribute("orgNames");
if(orgnames != null && orgnames.trim().length() > 0){
	String[] orgArr = orgnames.split(",");
	
	if(orgArr.length > 3) {
		String orgNamesLimited = orgArr[0] + ", " + orgArr[1] + ", " + orgArr[2];
		request.setAttribute("orgNamesLimited", orgNamesLimited);
		request.setAttribute("orgNamesLenght", orgArr.length);
		request.setAttribute("orgName", orgArr[0]);
	} else {
		request.setAttribute("orgNamesLimited", orgnames);
		request.setAttribute("orgNamesLenght", orgArr.length);
		request.setAttribute("orgName", orgArr[0]);
	}
	
}

Object orgIdObj = session.getAttribute("orgIdSet");
Set<String> orgIdSet = null;		
if(orgIdObj instanceof Set) {
	orgIdSet = (Set<String>)orgIdObj;
}
if(orgIdSet != null) {
	for(String id : orgIdSet) {
		if(new File("/app/cjo_logo/org" + id).exists()) {
			request.setAttribute("orgLogoId", "org" + id);
			break;
		}
	}
} else {
	request.setAttribute("orgLogoId", "");
}

%>

<%
	String orgBodyId = (String) session.getAttribute("orgBodyId");					
	String[] ipMembership = (String[])session.getAttribute("IPmembership");
%>


<%@page import="java.util.Set"%>
<%@page import="java.io.File"%><c:set var="isLoggedIn" value="${not empty userInfo and not empty userInfo.username}" />

<input id="ipLogoContextPath" type="hidden" value="<%= System.getProperty("ebooks.context.path")%>" />
<c:choose>
<c:when test="${not empty orgBodyId or pageScope.isLoggedIn}">
	<!-- Login Start -->

	<input type="hidden" name="IP_LOGO_TYPE" value="1" />
	<div id="page_container"> 
	<div id="toppanel">  
	<div id="login_container">
		<c:choose>
		<c:when test="${pageScope.isLoggedIn}">
			<b>Welcome ${userInfo.firstName}&nbsp;${userInfo.lastName}</b> |
		</c:when>
		<c:otherwise>
			<b>Welcome ${orgName}</b> |
		</c:otherwise>
		</c:choose>
				
		<!-- Toggle login/logouts -->
		<c:choose>
			<c:when test="${not pageScope.isLoggedIn}">
				<!-- <a class="thickbox link10" id="loginLink" href="${pageContext.request.contextPath}/login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> | -->
				<a class="thickbox link10" id="loginLink" href="<%= System.getProperty("path.ssl") %>login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="logout(); return false;" class="link10">Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty athensId}">
				<a href="<%= System.getProperty("openathensLink") %>" class="link10">Athens Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="athensLogout(); return false;" class="link10">Athens Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty shibbId}">
				<a href="<%= System.getProperty("shibbolethLink") %>" class="link10">Shibboleth Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="shibbolethLogout(); return false;" class="link10">Shibboleth Logout</a> |
			</c:otherwise>
		</c:choose>		
		
		<c:choose>
			<c:when test="${pageScope.isLoggedIn}">
				<!-- 
					<a href="#" onclick="MM_windowOpener('<%= System.getProperty("cjo.url")%>accmanagement?id=${userInfo.encryptedDetails}&amp;topage=updateRegistration'); return false;" class="link10">My Account</a>
				 -->				
				<a href="#" class="link10" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=updateRegistration','accountLink')" id="accountLink">My Account</a>
			</c:when>
			<c:otherwise>
				<a href="#" onclick="MM_windowOpener('<%= System.getProperty("cjo.url") %>registration?displayname=${referalName}'); return false;" class="link10">Register</a>
			</c:otherwise>
		</c:choose>
		
		<c:if test="${orgNamesLenght gt 0}">
	   <%-- | <a href="<%= System.getProperty("ebooks.context.path")%>access_details.jsf" class="link10">Access to</a>--%>
			| <a href="${pageContext.request.contextPath}/ebooks/subscriptionServlet?ACTION=ALL" class="link10">Access to</a>
			
		</c:if>
		
	</div>
	<div class="clear"></div>   
	<!-- Org Info Start -->   
	<c:if test="${not empty orgNamesLimited}">
    <div id="org_container">
    	<div id="org_content">
    		<input id="orgLogoId" type="hidden" value="${orgLogoId}" />
			<img id="orgLogoImg" src="<%= System.getProperty("ebooks.context.path") %>organisation/logo/${orgLogoId}" width="85" height="35" title="${orgName}" alt="${orgName}"/>
			<c:choose>
				<c:when test="${orgNamesLenght gt 3}">
					<p>${orgNamesLimited},&nbsp;
					<a href="#" onclick="MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">more</a>
					</p>
				</c:when>
				<c:otherwise>
					<p>${orgNamesLimited}</p>
				</c:otherwise>
			</c:choose>
    	</div>
    </div>    
	</c:if>
    <a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"></a>	
	<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"></a>
	
    </div>
    </div>
    <!-- Org Info End --> 
	
	<!-- Login End -->

</c:when>

<c:when test="${pageScope.isLoggedIn}">
	<input type="hidden" name="IP_LOGO_TYPE" value="2" />
	<div id="page_container">   		
	<div id="toppanel">  
	<div id="login_container">
		<b>Welcome ${userInfo.firstName}&nbsp;${userInfo.lastName}</b> |
	
		<!-- Toggle login/logouts -->
		<c:choose>
			<c:when test="${not pageScope.isLoggedIn}">
				<!-- <a class="thickbox link10" id="loginLink" href="${pageContext.request.contextPath}/login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> | -->
				<a class="thickbox link10" id="loginLink" href="<%= System.getProperty("path.ssl") %>login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="logout(); return false;" class="link10">Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty athensId}">
				<a href="<%= System.getProperty("openathensLink") %>" class="link10">Athens Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="athensLogout(); return false;" class="link10">Athens Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty shibbId}">
				<a href="<%= System.getProperty("shibbolethLink") %>" class="link10">Shibboleth Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="shibbolethLogout(); return false;" class="link10">Shibboleth Logout</a> |
			</c:otherwise>
		</c:choose>

		<!-- <a href="#" onclick="MM_windowOpener('<%= System.getProperty("cjo.url")%>accmanagement?id=${userInfo.encryptedDetails}&amp;topage=updateRegistration'); return false;" class="link10">My Account</a> | -->		
		<a href="#" class="link10" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=updateRegistration','accountLink')" id="accountLink">My Account</a>
		
		
		<c:if test="${orgNamesLenght gt 0}">
	   <%-- | <a href="<%= System.getProperty("ebooks.context.path")%>access_details.jsf" class="link10">Access to</a>--%>
			| <a href="<%= System.getProperty("ebooks.context.path")%>ebooks/subscriptionServlet?ACTION=ALL" class="link10">Access to</a>
		</c:if>
		
	</div>
	<div class="clear"></div>   
	<a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"></a>	
	<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"></a>
	</div>
	</div>	
</c:when>

<c:when test="${not empty IPmembership}">
	<input type="hidden" name="IP_LOGO_TYPE" value="3" />
	<!-- Login Start -->
       <!-- 
	<div class="panel_button"><a href="#"><img src="<%=request.getContextPath() %>/images/myorg_expand.gif"></a></div>
	<div class="panel_button" id="hide_button" style="display: none;"><a href="#"><img src="<%=request.getContextPath() %>/images/myorg_collapse.gif"></a></div>
       -->
    <div id="page_container">   		
	<div id="toppanel">  
	<div id="login_container">
		<b>Welcome ${orgName}</b> |
		 	
		<!-- Toggle login/logouts -->
		<c:choose>
			<c:when test="${not pageScope.isLoggedIn}">
				<!-- <a class="thickbox link10" id="loginLink" href="${pageContext.request.contextPath}/login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> | -->
				<a class="thickbox link10" id="loginLink" href="<%= System.getProperty("path.ssl") %>login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |
			</c:when>
			<c:otherwise>
				<a href="" onclick="logout(); return false;" class="link10">Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty athensId}">
				<a href="<%= System.getProperty("openathensLink") %>" class="link10">Athens Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="athensLogout(); return false;" class="link10">Athens Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty shibbId}">
				<a href="<%= System.getProperty("shibbolethLink") %>" class="link10">Shibboleth Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="shibbolethLogout(); return false;" class="link10">Shibboleth Logout</a> |
			</c:otherwise>
		</c:choose>

		<a href="#" onclick="MM_windowOpener('<%= System.getProperty("cjo.url") %>registration?displayname=${referalName}'); return false;" class="link10">Register</a>
		
		
		<c:if test="${orgNamesLenght gt 0}">
	   <%-- | <a href="<%= System.getProperty("ebooks.context.path")%>access_details.jsf" class="link10">Access to</a>--%>
			| <a href="<%= System.getProperty("ebooks.context.path")%>ebooks/subscriptionServlet?ACTION=ALL" class="link10">Access to</a>
		</c:if>
		
	</div>		        
   	<div class="clear"></div>       
	<!-- Org Info Start -->   
    <div id="org_container">
    	<div id="org_content">    	
    		<input id="orgLogoId" type="hidden" value="${orgLogoId}" />
			<img id="orgLogoImg" src="<%= System.getProperty("ebooks.context.path") %>organisation/logo/${orgLogoId}" width="85" height="35" title="${orgNamesLimited}" alt="${orgNamesLimited}" />			
			<c:choose>
				<c:when test="${orgNamesLenght gt 3}">
					<p>${orgNamesLimited},&nbsp;
					<a href="#" onclick="MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">more</a>
					</p>
				</c:when>
				<c:otherwise>
					<p>${orgNamesLimited}</p>
				</c:otherwise>
			</c:choose>
    	</div>
    </div>
   	<a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"></a>	
	<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"></a>
    </div>
    </div>
    <!-- Org Info End --> 
	
	<!-- Login End -->

</c:when>
<c:otherwise>
	<input type="hidden" name="IP_LOGO_TYPE" value="4" />
	<div id="page_container"> 		
	<div id="toppanel"> 
	<!-- Login Start -->
	<div id="login_container">
		<b>Welcome Guest</b> | 
				
		<!-- Toggle login/logouts -->
		<c:choose>
			<c:when test="${not pageScope.isLoggedIn}">
				<!-- <a class="thickbox link10" id="loginLink" href="${pageContext.request.contextPath}/login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |  -->
				<a class="thickbox link10" id="loginLink" href="<%= System.getProperty("path.ssl") %>login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="logout(); return false;" class="link10">Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty athensId}">
				<a href="<%= System.getProperty("openathensLink") %>" class="link10">Athens Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="athensLogout(); return false;" class="link10">Athens Logout</a> |
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${empty shibbId}">
				<a href="<%= System.getProperty("shibbolethLink") %>" class="link10">Shibboleth Login</a> |
			</c:when>
			<c:otherwise>
				<a href="#" onclick="shibbolethLogout();return false;" class="link10">Shibboleth Logout</a> |
			</c:otherwise>
		</c:choose>

		<a href="#" onclick="javascript:MM_windowOpener('<%= System.getProperty("cjo.url") %>registration?displayname=${referalName}'); return false;" class="link10">Register</a>

 		<c:if test="${orgNamesLenght gt 0}">
			<%-- | <a href="<%= System.getProperty("ebooks.context.path")%>access_details.jsf" class="link10">Access to</a>--%>
			| <a href="<%= System.getProperty("ebooks.context.path")%>ebooks/subscriptionServlet?ACTION=ALL" class="link10">Access to</a>
		</c:if>
		
	</div>
	<div class="clear"></div>   	
	<a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"></a>	
	<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"></a>
	</div>
	</div>
	<!-- Login End -->

</c:otherwise>

</c:choose>

<script type="text/javascript">
function athensLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "home.jsf?alo=y";
	return true;
}

function shibbolethLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "home.jsf?slo=y";
	return true;
}


</script>