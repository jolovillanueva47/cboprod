    
    
    <!-- Footer Start -->
    <div id="footer_container">
    	
        <div id="footer_wrapper">
          <ul>
          	<li>&copy; Cambridge Library Collection 2010.</li>
          	<li><a href="${pageContext.request.contextPath}/home.jsf" target="_blank">Cambridge Books Online</a>|</li>
            <li><a href="${pageContext.request.contextPath}/templates/terms_of_use.jsf" target="_blank">Terms of use</a>|</li>
            <li><a href="${pageContext.request.contextPath}/templates/privacy_policy.jsf" target="_blank">Privacy Policy</a>|</li>
            <li><a href="${pageContext.request.contextPath}/templates/rights_and_permissions.jsf" target="_blank">Rights &amp; Permissions</a>|</li>            
            <li><a href="${pageContext.request.contextPath}/templates/contact.jsf" target="_blank">Contact Us</a></li>
          </ul>          
      	</div>        
        
    </div>    
    <!-- Footer End -->