<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.util.Set"%>
<%@page import="java.io.File"%>


<input id="ipLogoContextPath" type="hidden" value="<%= System.getProperty("ebooks.context.path")%>" />


	<!-- Login Start -->
	<input type="hidden" name="IP_LOGO_TYPEX" value="1" />
	<div id="page_container"> 
		<div id="toppanel">  
			<div id="login_container">
				<c:choose>
					<c:when test="${eBookLogoBean.userLoggedIn}">
						<b>Welcome ${eBookLogoBean.orgConMap.userLogin.user.firstName}&nbsp;${eBookLogoBean.orgConMap.userLogin.user.lastName}</b> |
					</c:when>
					<c:when test="${not empty eBookLogoBean.orgName}">
						<b>Welcome ${eBookLogoBean.orgName}</b> |
					</c:when>
					<c:otherwise>
						<b>Welcome Guest</b> |
					</c:otherwise>
				</c:choose>
			
				<a class="link10" href="<%= System.getProperty("ebooks.context.path") %>aaa/clc/home.jsf" >Accessible Version</a> |
		
				<!-- Toggle login/logouts -->
				<c:choose>
					<c:when test="${not eBookLogoBean.userLoggedIn}">				
						<a class="thickbox link10" id="loginLink" href="<%= System.getProperty("path.ssl") %>login.jsf?keepThis=true&amp;TB_iframe=true&amp;width=460&amp;height=290" >Login</a> |
					</c:when>
					<c:otherwise>
						<a href="#" onclick="clclogout(); return false;" class="link10">Logout</a> |
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not eBookLogoBean.athensLoggedIn}">
						<a href="<%= System.getProperty("openathensLink") %>" class="link10">Athens Login</a> |
					</c:when>
					<c:otherwise>
						<a href="#" onclick="athensLogout(); return false;" class="link10">Athens Logout</a> |
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not eBookLogoBean.shibbolethLoggedIn}">
						<a href="<%= System.getProperty("shibbolethLink") %>" class="link10">Shibboleth Login</a> |
					</c:when>
					<c:otherwise>
						<a href="#" onclick="shibbolethLogout(); return false;" class="link10">Shibboleth Logout</a> |
					</c:otherwise>
				</c:choose>		
		
				<c:choose>
					<c:when test="${eBookLogoBean.userLoggedIn}">			
						<a href="#" class="link10" onclick="return CJOLoginLink('<%= System.getProperty("cjo.url")%>accmanagement?topage=updateRegistration','accountLink')" id="accountLink">My Account</a>
					</c:when>
					<c:otherwise>
						<a href="#" onclick="MM_windowOpener('<%= System.getProperty("cjo.url") %>registration?displayname=${referalName}'); return false;" class="link10">Register</a>
					</c:otherwise>
				</c:choose>
		
				<!-- 
				<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">	   
					| <a href="<%= System.getProperty("ebooks.context.path")%>ebooks/subscriptionServlet?ACTION=ALL" class="link10">Access to</a>			
				</c:if>
				 -->
				 
			</div>
			<div class="clear"></div>
	   
	<!-- Org Info Start 
			<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 0}">
	    		<div id="org_container">
	    			<div id="org_content">
	    				<input id="orgLogoId" type="hidden" value="${eBookLogoBean.orgLogoId}" />
	    				<c:if test="${eBookLogoBean.hasLogo}" >
	    					<img id="orgLogoImg" src="<%= System.getProperty("org.logo.url") %>${eBookLogoBean.orgLogoId}" width="85" height="35" alt=""/>	
	    				</c:if>	
	    				<p>${eBookLogoBean.showLogoNamesUnderMax}
	    					<c:if test="${fn:length(eBookLogoBean.orgConMap.map) gt 3}" >
	    						,&nbsp;<a href="#" onclick="MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/view_access.jsf','popup','status=yes,scrollbars=yes'); return false;">more ...</a>
	    					</c:if>	    				
	    				</p>						
	    			</div>
	    		</div>    
			</c:if>
	 
    		<a id="cup_logo" title="Cambridge University Press" href="http://www.cambridge.org/" target="_blank"></a>	
			<a id="cbo_logo" title="Cambridge Books Online" href="${pageContext.request.contextPath}/home.jsf"></a>
	--> 
		</div>
    </div>
    
    
	
	<!-- Login End -->



<script type="text/javascript">

function athensLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("athensLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "home.jsf?alo=y";
	return true;
}

function shibbolethLogout() {
	var ebooksContextPath = $("#ebooksContextPath").val();
	window.open('<%= System.getProperty("shibbolethLinkLogout") %>','','scrollbars=no,menubar=no,height=400,width=600,resizable=yes,toolbar=no,location=no,status=no');
	window.parent.location = ebooksContextPath + "home.jsf?slo=y";
	return true;
}

</script>