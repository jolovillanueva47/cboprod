var ebooksContextPath = $("#ebooksContextPath").val();

$(function(){	
	$("#search_form").bind("keypress", function(e) {
        if(e.keyCode==13){
        	$("#search_link").click();
        }
	});
	
	$("#search_link").click(function(){
		var valid = false;
		var term = jQuery.trim($("#search_text").val());
		if(term == "Please enter a keyword") {
			term = "";
		}
		if (term != null && term.length > 0) {
			valid = true;
		}
		
		if (valid) {
			var searchForm = document.getElementById("search_form");
			$("#search_form").attr("action", ebooksContextPath + "search?pubCode=CLC&amp;searchType=quick&amp;searchText=" + term);
			$("#search_form").submit();
		} else {
			alert("Invalid search.");
		}
	});	
})