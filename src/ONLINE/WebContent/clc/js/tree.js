/* 
-----------------------------------------
	Template JS
	Modified by: John Ryan Acoba
    GUI Designer
-----------------------------------------
*/

$(document).ready(function(){
		
	// first example
	$("#navigation").treeview({
		collapsed: true,
		unique: true,
		persist: "location"
	});

	
	// second example
	$("#browser").treeview({
		animated:"normal",
		persist: "cookie"
	});

	$("#samplebutton").click(function(){
		var branches = $("<li><span class='folder'>_new</span><ul>" + 
			"<li><span class='file'>_</span></li>" + 
			"<li><span class='file'>_</span></li></ul></li>").appendTo("#browser");
		$("#browser").treeview({
			add: branches
		});
	});


	// third example
	$("#red").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol"
	});


});