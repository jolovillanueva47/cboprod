function showHelp(contextPath, helpType, pageId, pageName) {	
	var url = "";
	
	if(helpType == "root") {
		url = "popups/help_index.jsf";
	} else {
		url = "popups/help.jsf?pageId=" + pageId + "&amp;pageName=" + pageName;
	}

	MM_openWindow(contextPath + url, 'popup','status=yes,scrollbars=yes,width=750,height=700');
}

function MM_logout() {	
   	$.get("logout?"+Math.random(), null, function(){location='home.jsf?lo=y'});   	  
}

function logout() { 
	var ebooksContextPath = $("#ebooksContextPath").val();
	
	window.parent.location = ebooksContextPath + "home.jsf?lo=y";
	return true;
}


function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function openWindow(url) { //v2.0		
	var h = window.open(url,'','location=yes,toolbar=yes,menubar=yes,resizable=yes,status=yes,scrollbars=yes,width=700,height=440');
	h.focus();
}

function MM_windowOpener(url){
	if(window.opener != null){
		window.opener.location=url;
		window.close();
	}else {
		window.location=url;
	}
}

function MM_openWindow(theURL,winName, features) {
		
	var help_win_width = 850;
	var help_win_height = 800;
	
	if (screen.availWidth > help_win_width){
		var help_win_top = parseInt((screen.availHeight/2) - (help_win_height/2));
	}else{
		help_win_top = 0;
	}
	if (screen.availWidth > help_win_width){
		var help_win_left = parseInt((screen.availWidth/2) - (help_win_width/2));
	} else {
		var help_win_left = 0;
	}

	var features = features + ",width=" + help_win_width + ",height=" + help_win_height + ",top=" + help_win_top + ",left=" + help_win_left;
	
	var h = window.open(theURL,winName,features);
	h.focus();
}

function showPdf(bookId, contentId) {
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
	
	//add counter logs
//	var url = "counterEbooks";	
//	$.post(url, {RAND:Math.random(), bid:bookId},
//			function () {
//				//do nothing
//			}
//		);
	
	var serv = "popups/pdf_viewer.jsf?cid=" + contentId;
	MM_openWindow(serv, winName,"status=yes,scrollbars=yes,width=800,height=600");
	
	googleTrackPDFView(serv);
}

function showContentTOCPdf(bookId, contentId, startPage, basePage) {
	basePage = basePage.replace(/pp. /g, "");
	basePage = basePage.replace(/-\d+/g, "");
	var winName = "";
	if(contentId != null || contentId != "") {
		winName = contentId;
	} else {
		winName = bookId;
	}
		
	//add counter logs
//	var url = "counterEbooks";	
//	$.post(url, {RAND:Math.random(), bid:bookId},
//			function () {
//				//do nothing
//			}
//		);
	
	startPage = (startPage - basePage) + 1;
	
	var serv = "popups/pdf_viewer.jsf?cid=" + contentId + "&amp;startPage=" + startPage;
	MM_openWindow(serv, winName,"status=yes,scrollbars=yes,width=800,height=600");
	
	googleTrackPDFView(serv);
}

function googleTrackPDFView(url) {
	try {
		var pageTracker = _gat._getTracker("UA-31379-39");
		pageTracker._trackPageview(url);
	} catch(err) {}
}

function hasAdobeAcrobat() {
	
	var isInstalled = false;  
	var version = null;  
	
	if (window.ActiveXObject) {  
		var control = null;  
	 	try {  
	    	// AcroPDF.PDF is used by version 7 and later  
			control = new ActiveXObject('AcroPDF.PDF');  
	 	} catch (e) {  
	    	// Do nothing  
	 	}  
	 	if (!control) {  
	    	try {  
	        // PDF.PdfCtrl is used by version 6 and earlier  
	        	control = new ActiveXObject('PDF.PdfCtrl');  
	     	} catch (e) {  
	         	  
	    	}  
	 	}  
	    if (control) {  
	    	isInstalled = true;  
	        version = control.GetVersions().split(',');  
	        version = version[0].split('=');  
	        version = parseFloat(version[1]);  
	    }  
	} else {  
		// Check navigator.plugins for "Adobe Acrobat" or 
		// "Adobe PDF Plug-In for Firefox and Netscape"      	
		if(navigator.plugins["Adobe PDF Plug-In for Firefox and Netscape"]) { // Adobe Acrobat 8
			isInstalled = true;
		} else if(navigator.plugins["Adobe Acrobat"]) {
			isInstalled = true;
		}
	}  
	
	return isInstalled;
}

//function showPdf(eisbn, pdf, bookTitle, publisher) {
//	MM_openWindow("content/" + eisbn + "/" + pdf,"popup","status=yes,scrollbars=yes,width=750,height=700");
	//if(hasAdobeAcrobat()) {
	//	openWindow('show_pdf?pdfFileUrl=content/' + eisbn 
	//			+ '/' + pdf + '&eisbn=' + eisbn + '&title=' + bookTitle + '&publisher=' + publisher);
	//} else {
	//	alert("Please get Adobe Acrobat Reader at http://get.adobe.com/reader/");
	//}
//}

// --- EXPORT CITATION START ---

function execute(commandType, contextPath) {
	var includeAbstract = document.getElementsByName("displayAbstract")[0].checked;
	var fileFormat = "";
	for(var i = 0; i < document.getElementsByName("format").length; i++) {
		if(document.getElementsByName("format")[i].checked == true) {
			fileFormat = document.getElementsByName("format")[i].value;
			break;
		}
	}
	var emailAdd = document.getElementById("emailText").value;

	if(commandType.title == "Download") {
		document.getElementById("exportCitationForm").action 
			= contextPath + "export_citation?type=download&amp;includeAbstract=" + includeAbstract 
			+ "&amp;fileFormat=" + fileFormat + "&amp;emailAdd=" + emailAdd 
			+ "&amp;isbn=" + getUrlParam("isbn") + "&amp;id=" + getUrlParam("id") + "&amp;author=" + getUrlParam("author")
			+ "&amp;bookTitle=" + getUrlParam("bookTitle") + "&amp;contentTitle=" + getUrlParam("contentTitle")
			+ "&amp;printDate=" + getUrlParam("printDate") + "&amp;doi=" + getUrlParam("doi") 
			+ "&amp;bookId=" + getUrlParam("bookId") + "&amp;contentId=" + getUrlParam("contentId")
			+ "&amp;contributor=" + getUrlParam("contributor");
		
	} else if(commandType.title == "Email") {
		document.getElementById("exportCitationForm").action 
			= contextPath + "export_citation?type=email&amp;includeAbstract=" + includeAbstract 
			+ "&amp;fileFormat=" + fileFormat + "&amp;emailAdd=" + emailAdd
			+ "&amp;isbn=" + getUrlParam("isbn") + "&amp;id=" + getUrlParam("id") + "&amp;author=" + getUrlParam("author")
			+ "&amp;bookTitle=" + getUrlParam("bookTitle") + "&amp;contentTitle=" + getUrlParam("contentTitle")
			+ "&amp;printDate=" + getUrlParam("printDate") + "&amp;doi=" + getUrlParam("doi") 
			+ "&amp;bookId=" + getUrlParam("bookId") + "&amp;contentId=" + getUrlParam("contentId")
			+ "&amp;contributor=" + getUrlParam("contributor");
	}

	document.getElementById("exportCitationForm").submit();
}
function getUrlParam(name) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if(results == null) {
		return "";
	} else {
		return results[1];
	}
}

function resetForm(formName) {	
	document.getElementById(formName).reset();
}

//--- EXPORT CITATION END ---


//--- IP LOGO START ---

$(document).ready(function(){	
	$("#orgLogoImg").load(function(){	
		var width = $("#orgLogoImg").width();
		var height = $("#orgLogoImg").height();	

		if($("#orgLogoId").val() == "") {
			width = 0;
			height = 0;
		}
		
		$("#orgLogoImg").attr("style", "width: " + width + "px; height: " + height + "px;");
	});
	$("#orgLogoImg").attr("src", $("#orgLogoImg").attr("src"));
});	
//--- IP LOGO END ---

function loginFunction(){
	$("#loginLink").click();
}

function callParentLoginFunction(){
	window.opener.loginFunction();
	window.close();
}

function clearDefault(el) {
	if (el.defaultValue==el.value) el.value = ""
}

//--- LOGIN START ---
$(document).ready(function(){	
	$('#logintoggle').click(function () {
		$('#top-login .loginbox').slideToggle(300, function(){
			$('#logintoggle').toggleClass('up');
		});
	});
	
	$('#top-login .box').focus(function () {
		if($(this).val() == 'Login...') $(this).val('');
	});
	$('#top-login .box').blur(function () {
		if($(this).val() == '') $(this).val('Login...');
	});
});
//--- LOGIN END ---

//--- LOGIN PANEL START ---
$(document).ready(function() {
	$("div.panel_button").click(function(){
		$("div#panel").animate({
			height: "128px"
		})
		.animate({
			height: "128px"
		}, "fast");
		$("div.panel_button").toggle();
	
	});	
	
   $("div#hide_button").click(function(){
		$("div#panel").animate({
			height: "0px"
		}, "fast");
		
	
   });	
	
});
//--- LOGIN PANEL END ---

//--- LOADER START ---
$(document).ready(function() {
	$("#loading").hide();
	$("#main").show();
});
//--- LOADER END ---


//--- CJO LOGIN START ---
function getUrl(id){
	//cjoUrl is a global var set on the jsp that imports this script
	if(orgIdSet=="null"){
		return cjoUrl + "accmanagement?id=" + id + "&amp;RETURN_IMAGE=Y";
	}
	return cjoUrl + "accmanagement?id=" + id + "&amp;RETURN_IMAGE=Y&amp;ORG_ID_SET=" + orgIdSet;	
}

function generateImage(response){
	var id = response.split("-->")[1];	
	var url = getUrl(id);	
	return "<img src='" + getUrl(id) + "' style='display:none' id='cjoLogin' />"
}


function CJOLoginLink(link, elemId){	
	$.get(contextPath + "/loginCJO?RAND=" + Math.random(), function(response){		
		if(response.indexOf("LOGGED_EBOOKS") > -1){
			$("#" + elemId).after(generateImage(response));			
			$("#cjoLogin").load(function(){
				$.get(contextPath + "/loginCJO", {RAND:Math.random(),UPDATE_CJO_LOGGED:"Y"}, function(response){					
					location=link;
				});												
			});					
		}else{
			//when accmanagement
			if(link.indexOf("accmanagement") > -1){
				if(orgIdSet=="null"){
					location=link;
				}else{
					location=link + "&amp;ORG_ID_SET=" + orgIdSet;
				}				
			}else{
				location=link;
			}
		}
	});
	return false;	
}
//--- CJO LOGIN END ---


// IP LOGO START needed for https login
//this function modifies the url so as to pass as paramter the parent iframe location
//since window.parent.location is disallowed when http: (parent) and https: (child) are used
//also instead of just normally appending, i placed this as the first attribute because the jquery tool used only gets the first param
function makeHttpsLink(link){
	var loginLink = $(link);
	var href = loginLink.attr("href");	
	if(href != undefined){
		var arrayUrl = href.split("?");
		var append = "&amp;LOCATION10556=" + encodeURIComponent(window.location) + "&";	
		loginLink.attr("href", arrayUrl[0] + "?" + append + arrayUrl[1]);
	}
}
$(document).ready(function() {	
	makeHttpsLink("#loginLink");
	makeHttpsLink(".loginLink");
});



// IP LOGO END

//for ie6 ap test
var isIe6 = $.browser.msie && jQuery.browser.version.substr(0,3)=="6.0" ? true : false;
$(document).ready(function() {	
	if(isIe6){
		$("a").click(function(e){
			var ref = $(this).attr('href');
			if(ref == 'javascript:void(0);'){
				e.preventDefault();
			}
		});	
	}
});
//end for ie6 ap test
