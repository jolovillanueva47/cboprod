<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="org.cambridge.ebooks.online.jpa.user.User"%>
<%@page import="org.cambridge.ebooks.online.organisation.OrganisationWorker"%>
<%@page import="java.util.Set"%>
<%@page import="java.io.File"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%
	session.setAttribute("clcPageName", "about");
%>

<html>

<f:view>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta name="description" content="" />
    <meta name="keyword" content="" />
	
	<title>Cambridge Library Collection</title>    
    
   	<f:subview id="commonCssSubview">
		<c:import url="/clc/components/common_css.jsp" />
	</f:subview>
	
    <link href="/clc/style/style.css" rel="stylesheet" type="text/css" />
    <link href="/clc/style/smoothDivScroll.css" rel="stylesheet" type="text/css" /> 
    
    <script type="text/javascript" src="/clc/js/jquery.min.js"></script>    
     
    <script type="text/javascript" src="/clc/js/jquery.scroll-min.js"></script>

</head>

<body>
	<form id="mainForm" method="post" action="#">
	
<!-- bg Topline -->       
<div id="top_container">

    <div id="page-wrapper">  
    
          <!-- Login Container Start -->
          <div id="page_container">
            <div id="toppanel">
            
              <!-- Login Start -->
              <div id="login_container">
              	<!-- Topmenu start -->
			  	<f:subview id="clcIpLogo">
					<c:import url="${pageContext.request.contextPath}/clc/components/clc_ip_logo.jsp" />
			  	</f:subview>
              	<!-- Topmenu end -->  
              </div>
     	      <!-- Login End -->
              
              <div class="clear"></div>
              
              <!-- CUP Link Start -->
          	  <a href="#" title="Cambridge University Press" id="cup_logo">&nbsp;</a>
              <!-- CUP Link End -->
          
              <!-- CBO Logo Link Start -->
              <a href="#" title="Cambridge Library Collection Online" id="cbo_logo">&nbsp;</a>
              <!-- CBO Logo Link End -->
          
            </div>
          </div>
          <!-- Login Container End -->
      
      <!-- Header Start -->
      <div id="header_container">
        	
          <!-- Topmenu start -->
		  <f:subview id="clcTopMenu">
			<c:import url="${pageContext.request.contextPath}/clc/components/top_menu.jsp" />
		  </f:subview>
          <!-- Topmenu end --> 
          

        </div> 
        <!-- Header End -->
        
            <div class="breadcrumbs">
            	<ul>
                	<li><a href="${pageContext.request.contextPath}/clc/home.jsf">Home</a>&nbsp;&rsaquo;&nbsp;About Cambridge Library Collection</li>
                </ul>
            </div>
        
        <div class="clear_bottom"></div>
        
        <!-- Main Body Start -->
        <div id="main_container">
        
        	<!-- Titlepage Start -->
            <h1 class="titlepage_heading">About <em>Cambridge Library Collection</em></h1>
            <!-- Titlepage End -->
            
                <p>Originating in a unique collaboration between the world's oldest publisher and the renowned Cambridge University Library, the Cambridge Library Collection makes important historical works accessible in new ways. The combination of state-of-the-art scanning technology and the Press's commitment to quality gives today's readers access to the content of books that until recently would have been available only in specialist libraries.</p>

				<p>Already a pioneer in the re-publishing of titles from its own backlist, Cambridge University Press has extended its reach to include other books which are still of interest to researchers, students and the general reader. The Press's collaboration with Cambridge University Library and other partner libraries allows access to a vast range of out-of-copyright works, from which titles are selected with advice from leading specialists.</p>

				<p>With subjects ranging from anthropology to zoology, the Cambridge Library Collection allows readers to own books they would otherwise find it hard to obtain, including the monumental Library Edition of the <em>Works of John Ruskin</em> edited by Cook and Wedderburn, the complete <em>Naval Chronicle</em> documenting warfare at sea in the Napoleonic period, accounts of the exploration of the Americas, Australia and China, and scientific writings by Darwin and his circle.</p>

				<p>Each page is scanned and the resulting files undergo a rigorous process of cleaning, in which any blemishes are removed to obtain a crisp and legible text. Each book has a new cover design and a specially written blurb which highlights the relevance of the book to today's readers. The latest print-on-demand technology then ensures that the content of these rare and sometimes fragile books will be made available worldwide. The Collection is now also available, in searchable form, as part of Cambridge Books Online.</p>          
           
        <!-- Titlepage Start 
		<h1 class="titlepage_heading">Key Features</h1>-->
		<!-- Titlepage End 
	 		
				<ul>
					<li><span class="icons_img bullet01">&nbsp;</span>Thousands of front &amp; backlist titles</li>
		            <li><span class="icons_img bullet01">&nbsp;</span>Dynamic content and feature set, with frequent addition of new titles being and regular functionality enhancements</li>	
		            <li><span class="icons_img bullet01">&nbsp;</span>Titles from across all Cambridge world renowned subject areas</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Flexible purchase plans and custom packages</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Powerful quick search, advanced search and browse capabilities</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Comprehensive library support tools including downloadable MARC records, usage reports and access &amp; authentication methods</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Compliance with all major industry standards and initiatives</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Extensive user functionality including hyperlinked references &amp; personalisation features</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Dedicated customer support teams</li>
	                <li><span class="icons_img bullet01">&nbsp;</span>Enhanced discoverability tools</li>
	            </ul>
        -->	
        <div class="clear_bottom"></div>
			
		<!-- Titlepage Start
		<h1 class="titlepage_heading">Further Information</h1> -->
		<!-- Titlepage End 
	 		
	 			<ul>	
					<li><span class="icons_img bullet01"></span><a href="#" onclick="showHowToPurchase(); return false;">How to purchase</a></li>
					<li><span class="icons_img bullet01"></span><a href="<%=System.getProperty("cjo.url") %>registration?SIGNUP=Y" >Sign up for a free trial</a></li>										
					<li><span class="icons_img bullet01"></span><a href="javascript:MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/faq.jsf','popup','status=yes,scrollbars=yes,width=750,height=700');" >Frequently Asked Questions (FAQs)</a></li>
					<li><span class="icons_img bullet01"></span><a href="<%= System.getProperty("ebooks.context.path") %>templates/contact.jsf" >Contact Us</a></li>
					<li><span class="icons_img bullet01"></span><a href="http://www.cambridge.org/online/"  target="_blank">Other online products from Cambridge</a></li>
							
	            </ul>
          -->  
            
        </div>  
        <!-- Main Body End --> 
        
	</div> 
</div>
    
    <!-- Start footer content -->
	<f:subview id="clcFooterMenu">
		<c:import url="/clc/components/footer_menu.jsp" />
	</f:subview>    
    <!-- End footer content -->
</form>
    
<f:subview id="commonJsSubview">
	<c:import url="/clc/components/common_js.jsp" />
</f:subview>


	<script type="text/javascript">
		$(".nav_about").attr("class", "nav_about_current");
		
		function showHowToPurchase() {
			var messageId = 1556;
			if(window.location.href.indexOf("ebooks.cambridge.org") > -1) {
				messageId = 2364;
			}
			MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/news.jsf?messageId=' + messageId,'popup','status=yes,scrollbars=yes,width=750,height=700');
		}

		function showViewGuidedTour() {
			var messageId = 1560;
			if(window.location.href.indexOf("ebooks.cambridge.org") > -1) {
				messageId = 2368;
			}
			MM_openWindow('<%= System.getProperty("ebooks.context.path") %>popups/news.jsf?messageId=' + messageId,'popup','status=yes,scrollbars=yes,width=750,height=700');
		}
	</script>
	
            
</body>
</f:view>
</html>