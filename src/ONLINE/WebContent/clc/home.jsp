<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="/WEB-INF/lib/c.tld" prefix="c" %>

<%
	session.setAttribute("publisherCode", "CLC");
	session.setAttribute("clcPageName", "home");
%>

<html>
<f:view>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta name="description" content="CLC Home Page" />
    <meta name="keyword" content="" />
    
	<title>Cambridge Library Collection - Cambridge University Press</title>    

	<f:subview id="commonCssSubview">
		<c:import url="/clc/components/common_css.jsp" />
	</f:subview>
</head>
<body>
<c:import url="/components/loader.jsp" />
<form id="mainForm" method="post" action="#">
		
<!-- bg Topline -->       
<div id="top_container">

    <div id="page-wrapper">  
    
          <!-- Login Container Start -->
          <div id="page_container">
            <div id="toppanel">
            
              <!-- Login Start -->
              <!-- 
          	  <div id="login_container"> 
                  <b>Welcome Lorem ipsum dolor sit amet</b> |
                  <a href="#" class="link10">Accessibility Site</a> |
                  <a href="#" class="link10">Login</a> | 
                  <a href="#" class="link10">Athens Login</a> |
                  <a href="#" class="link10">Shibboleth Login</a> |    
                  <a href="#" class="link10">Register</a> |
                  <a href="#" class="link10">Access Details</a>                           
              </div>
               -->
              <div id="login_container">
              	<!-- Topmenu start -->
			  	<f:subview id="clcIpLogo">
					<c:import url="/clc/components/clc_ip_logo.jsp" />
			  	</f:subview>
              	<!-- Topmenu end -->  
              </div>
     	      <!-- Login End -->
              
              <div class="clear"></div>
              
              <!-- CUP Link Start -->
          	  <a href="#" title="Cambridge University Press" id="cup_logo">&nbsp;</a>
              <!-- CUP Link End -->
          
              <!-- CBO Logo Link Start -->
              <a href="#" title="Cambridge Library Collection Online" id="cbo_logo">&nbsp;</a>
              <!-- CBO Logo Link End -->
          
            </div>
          </div>
          <!-- Login Container End -->
      
      <!-- Header Start -->
      <div id="header_container">
        	
          <!-- Topmenu start -->
		  <f:subview id="clcTopMenu">
			<c:import url="/clc/components/top_menu.jsp" />
		  </f:subview>
          <!-- Topmenu end -->            
          
 

       </div> 
       <!-- Header End -->
        
        <div class="clear_bottom"></div>
        
        <!-- Main Body Start -->
        <div id="main_container">        
        	
            <!-- Page branding start -->
            <div class="page_branding_frontpage">
            	<a href="#" title="Cambridge Library Collection Online"><img src="${pageContext.request.contextPath}/clc/images/bg_branding.jpg" alt="" width="900" height="321" /></a>
            </div>
          <!-- Page branding end -->
            
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, ang pogi ni cito consectetur adipisicing elit. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> -->
            
        	
            <!-- Banner Start --> 
            <div id="banner_series_container">
            	
                <!-- Title Banner Start -->
              <div class="titlebanner_homepage"></div>
                <!-- Title Banner End -->
                
           	  	<div class="banner_wrapper04">
                	<!-- Download book format start -->
                    <ul>
                    	<li><span class="icons_img download_excel">&nbsp;</span><a id="title_list_xls" href="/downloads/titlelist_clc.xls">Microsoft Excel Format (0.71 MB)</a></li>
                        <li><span class="icons_img download_csv">&nbsp;</span><a id="title_list_csv" href="/downloads/titlelist_clc.csv">CSV Format (0.6 MB) - Recommended for Mac Users</a></li>
                    </ul>
                    <!-- Download book format end -->
                    <!-- Download book format start
                    <ul>
                    	<li class="disabled_link"><span class="icons_img download_excel_disabled">&nbsp;</span><a href="#">Microsoft Excel Format (0.71 MB)</a></li>
                        <li class="disabled_link"><span class="icons_img download_csv_disabled">&nbsp;</span><a href="#">CSV Format (0.6 MB) - Recommended for Mac Users</a></li>
                    </ul>
                    Download book format end -->   
                    
                </div>
  
                <div class="banner_wrapper05">
                	<c:if test="${not empty topBooksBean.topSeriesListCLC}">
                    	<ul>                    	
							<c:forEach var="series" items="${topBooksBean.topSeriesListCLC}">					
								<c:url value="/series_landing.jsf" var="seriesSortByVolume">
									<c:param name="seriesCode" value="${series.seriesCode}" />
									<c:param name="seriesTitle" value="${series.seriesLegend}" />
									<c:param name="sort" value="series_number" />
								</c:url>				
								<li><span class="icons_img arrow02">&nbsp;</span><a href="<c:out value="${seriesSortByVolume}"/>">${series.seriesLegend}</a></li>								
							</c:forEach>
											
						</ul>
					 	<ul class="fr">
	        				<li><span class="icons_img arrow03">&nbsp;</span><a href="/series.jsf">Browse All Series</a></li>
	    				</ul>
	    			</c:if>
                </div>
            
          </div> 
            <div class="icons_img banner_bottom"></div>
            <!-- Banner End --> 
            
            <div class="clear"></div>                  
            
	  </div>  
        <!-- Main Body End --> 
        
	</div> 
</div>   
    <!-- End body content -->
    
    <!-- Start footer content -->
	<f:subview id="clcFooterMenu">
		<c:import url="/clc/components/footer_menu.jsp" />
	</f:subview>  
    <!-- End footer content -->	
</form>


<f:subview id="commonJsSubview">
	<c:import url="/clc/components/common_js.jsp" />
</f:subview>	
	    
<script type="text/javascript" src="${pageContext.request.contextPath}/clc/js/jquery.scroll-min.js"></script>
	
<script type="text/javascript">
		$(document).ready(function(){
			$.post("/downloads/file_size", 
				{filename:"/app/ebooks/content/CLC_titlelist_xls.zip"},
				function(data) {
					$("#title_list_xls").html("Microsoft Excel Format (" + data + " MB)");
				}
			);	
			$.post("/downloads/file_size", 
				{filename:"/app/ebooks/content/CLC_titlelist_csv.zip"},
				function(data) {
					$("#title_list_csv").html("CSV Format (" + data + " MB) - Recommended for Mac Users");
				}
			);			
		});
</script>

<f:subview id="googleAnalyticsSubview">
	<c:import url="/components/google_analytics.jsp" />
</f:subview>
</body>

</f:view>
</html>