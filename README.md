## Synopsis

This project serves as a proof of concept for  legacy apps containerization under Academic Digital Non-Core Team.

## Requirements

Should have installed the following in your local machine.
 - Docker CE
 - git
 - jdk version 1.6.0_45
 - Apache Ant 1.9.11
 
## Installation

1. Add System Variables to your system environment variable 
	ex.
		APP_ENV uat
		ANT_HOME C:\Program Files\Apache\apache-ant-1.9.11
		JAVA_HOME C:\Program Files\Java\jdk1.6.0_45
	    PATH %JAVA_HOME%\bin;%ANT_HOME%\bin

2. Using your computer's shell, go to any directory you prefer and clone the project using the command.
	- git clone https://gitlab.service.adnc.cambridge.org/adnc/cboproduction.git
	
3. Go inside the clr folder
	- cd <Working directory>/cboproduction/build
	
4. Build using Ant
    - ant -buildfile production-build.xml
    
5. Build docker image
	- docker build --build-arg ENVIRONMENT=${APP_ENV} -t cboprod:v1 .
	
6. Run the container using the image.
	- docker container run -p 8080:8080 cboprod:v1
	