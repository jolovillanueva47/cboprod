#!groovy

@Library ('adnc-lib@master')

def pipeline = new org.cambridge.adnc.Pipeline()
def proj_name = "cboproduction"


node {

    gitlab_url = "https://registry.gitlab.service.adnc.cambridge.org"
    gitlab_repo = "registry.gitlab.service.adnc.cambridge.org/adnc/${proj_name}"
    gitlab_creds = "gitlab-credentials"
    KUBE_AUTH = credentials("kubeAuth")
    home_dir = '/var/lib/jenkins'
    pwd = pwd()
    chart_dir = "${pwd}/${proj_name}"

    if (env.BRANCH_NAME == 'master') {
        ENVIRONMENT = prod
    } else {
        ENVIRONMENT = "${env.BRANCH_NAME}"
    }

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */
        
        checkout scm

    }

    stage('ANT Build') {
        sh """
        
        
        ant -buildfile build/production-build.xml
        
        
        """ 
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
        * docker build on the command line */
        docker.withRegistry("${gitlab_url}", "${gitlab_creds}") {
            app = docker.build("${gitlab_repo}", "--no-cache --build-arg ENV=${ENVIRONMENT} .")
        }    
    }

    stage('Test image') {
        /* Ideally, we would run a test framework against our image.
        * For this example, we're using a Volkswagen-type approach ;-) */
        app.inside {
            sh 'echo "No test features yet - TBD"'
        }
    }

    stage('Push image') {
        /* Finally, we'll push the image with two tags:
        * First, the incremental build number from Jenkins
        * Second, the 'latest' tag.
        * Pushing multiple tags is cheap, as all the layers are reused. */
        docker.withRegistry("${gitlab_url}", "${gitlab_creds}") {
            app.push("${ENVIRONMENT}-${env.BUILD_NUMBER}")
            app.push("latest")
        }
    }

    stage('login to K8 cluster') {
        if (env.BRANCH_NAME == 'uat' || env.BRANCH_NAME == 'stg' ) {
            k8_env = "dev"
        } else {
            k8_env = "prod"
        }
        try {
            timeout(time: 5, unit: 'SECONDS') {
                println "Logging in to k8.${k8_env}.adnc.cambridge.org using kops"
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:"adncAwsKeys${k8_env}", usernameVariable: 'AWS_ACCESS', passwordVariable: 'AWS_SECRET']]) {
                    sh """
                    export AWS_ACCESS_KEY_ID=${AWS_ACCESS}
                    export AWS_SECRET_ACCESS_KEY=${AWS_SECRET}
                    export AWS_REGION='eu-west-1' 
                    /usr/local/bin/kops export kubecfg k8.${k8_env}.adnc.cambridge.org --state=s3://clusters.${k8_env}.adnc.cambridge.org
                    """
                    println "Successfully logged in to k8.${k8_env}.adnc.cambridge.org using kops"
               }
            }
        } catch(err) {
            println "Failed login using KOPS"
            println "Logging in to k8.${k8_env}.adnc.cambridge.org using osprey"
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:'kubeAuth', usernameVariable: 'KUBE_AUTH_USR', passwordVariable: 'KUBE_AUTH_PSW']]) {
                pipeline.ospreyLogin(
                    ldapuser      : "${KUBE_AUTH_USR}",
                    ldappass      : "${KUBE_AUTH_PSW}",
                    k8env         : k8_env,
                    homedir       : home_dir
                )      
            }
        }
    }

    stage('Update chart & tag version') {
        sh """
        sed -i s/'0.1.0'/${env.BUILD_NUMBER}/g ${chart_dir}/Chart.yaml
        sed -i s/'latest'/${ENVIRONMENT}-${env.BUILD_NUMBER}/g ${chart_dir}/${ENVIRONMENT}-values.yaml
        """
    }

    // deploy only the uat branch auto deployment
    if (env.BRANCH_NAME == 'uat') {

        stage ('deploy to UAT') {

            // run helm chart linter
            // helmLint(chart_dir)

            // Deploy using Helm chart
            pipeline.helmDeploy(
                dry_run       : false,
                name          : "${proj_name}-${env.BRANCH_NAME}",
                namespace     : "${ENVIRONMENT}",
                chart_dir     : chart_dir,
                tag           : "${ENVIRONMENT}-${env.BUILD_NUMBER}"
            )
        }
    }

    if (env.BRANCH_NAME == 'stg') {
        stage ('Approval?') {
        
            input "Do you approve deployment in ${ENVIRONMENT} env?"
        
        }

        stage ('deploy in Staging') {
            // run helm chart linter
            // helmLint(chart_dir)

            // Deploy using Helm chart
            pipeline.helmDeploy(
                dry_run       : false,
                name          : "${proj_name}-${ENVIRONMENT}",
                namespace     : "${ENVIRONMENT}",
                chart_dir     : chart_dir,
                tag           : "${ENVIRONMENT}-${env.BUILD_NUMBER}"
            )
        }
    }

    // deploy only the master branch with manual triggering
    if (env.BRANCH_NAME == 'master') {
        stage ('Approval?') {
        
            input "Do you approve deployment in ${ENVIRONMENT} env?"
        
        }
        
        stage ('deploy in Production') {
            // run helm chart linter
            // helmLint(chart_dir)

            // Deploy using Helm chart
            pipeline.helmDeploy(
                dry_run       : false,
                name          : "${proj_name}-${ENVIRONMENT}",
                namespace     : "${ENVIRONMENT}",
                chart_dir     : chart_dir,
                tag           : "${ENVIRONMENT}-${env.BUILD_NUMBER}"
            )
        } 
    }
}