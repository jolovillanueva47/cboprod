<?php
	/* Author: Karlson Mulingtapang
	   About: ISBN-13 Parser. 
	 		  Divides ISBN-13 to 5 parts
	 		  	EAN(1-3 digit position), 
				GROUP(4-5 digit position),
				PUBLISHER(6-9 digit position),
				TITLE(10-12 digit position),
				CHECK DIGIT(13 position)
	*/
	
	
	// check if argv[1] is empty - it is important that this has a value
	if(isset($argv[1])){	
		$contentDir = "/app/ebooks/content";
		$archiveDir = "/app/ebooks/archive";
		
		$uploadFile = $argv[1];				
		$pathParts = pathinfo($uploadFile);
	
		try {
			// get only zip files
			if("zip" == $pathParts["extension"]) {
				$isbn13 = $pathParts["filename"];
				$zipFile = $pathParts['basename'];
				
				// divide isbn13 to 5 parts - EAN, GROUP, PUBLISHER, TITLE, CHECK DIGIT
				if(false !== strpos($isbn13, "_")) {
					$isbn13 = substr($isbn13, 0, strpos($isbn13, "_"));
				} 
				$ean = substr($isbn13, 0, 3) ;
				$group = substr($isbn13, 3, 2);
				$publisher = substr($isbn13, 5, 4);
				$title = substr($isbn13, 9, 3);
				$checkDigit = substr($isbn13, 12, 1);
				
				
				$finalPath = "$contentDir/$ean/$group/$publisher/$title/$checkDigit";
				
				// check if the given path exists; create dir if not existing
				if(!file_exists($finalPath)) {
				
					// create nested directory
					mkdir($finalPath, 0755, true);
				}					
				
				// extract zip
				echo "Processing $uploadFile ...\n";
				
				exec("unzip -o -d $finalPath $uploadFile");
				
				// copy files to new directory
				foreach(glob("$finalPath/$isbn13/*") as $fileEntry) {
					$filename = pathinfo($fileEntry, PATHINFO_BASENAME);			
					
					if(!copy($fileEntry, "$finalPath/$filename")) {
						echo "Failed to copy $fileEntry\n";
					} else {
						// removes file
						if(!unlink($fileEntry)) {
							echo "Failed to delete file: $fileEntry\n";
						}
					}
				}
				
				// removes dir
				if(!rmdir("$finalPath/$isbn13")) {
					echo "Failed to delete directory: $dirEntry\n";
				}
				
				/*
				echo "Done processing $uploadDir/$zipFile. Moved contents to $finalPath\n";				
				
				
				// copy zip file to archive dir
				if(!copy("$uploadDir/$zipFile", "$archiveDir/$zipFile")) {
					echo "Failed to copy $uploadDir/$zipFile\n";
				} else {
					// remove file
					if(!unlink("$uploadDir/$zipFile")) {
						echo "Failed to delete $uploadDir/$zipFile\n";
					}
				}
				*/
			} 
		} catch (Exception $e) {
			echo "Caught exception: " . $e->getMessage() . "\n";
		}
	} 
	
	
?>