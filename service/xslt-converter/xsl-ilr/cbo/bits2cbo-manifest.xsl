<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">


<!-- 
    DEPENDENCY: valid CUP-BITS file
    ACTS on: BITS file
    PURPOSE: creates  CBO manifest - initially for ILR:
    INPUT PARAMETERS: 
    USES FUNCTIONS:
    COMMENTS: 
    PIPELINE: 
    DECSIONS:

    -->
    
    <xsl:output method="xml" indent="yes" doctype-public="-//CUP//DTD Manifest DTD//EN" doctype-system="http://dtd.cambridge.org/schemas/2009/01/manifest.dtd"/>
    <xsl:param name="e-isbn"/>
    <xsl:variable name="header-fnm" select="concat($e-isbn, '.xml')"/>
    <xsl:variable name="header" select="document($header-fnm, .)"/>
    <!--<xsl:variable name="isbn" select="book/book-meta/book-id"/>-->
   
   <xsl:template match="/">
       <package id="{$e-isbn}">
           <file filename="{$e-isbn}.xml" datatype="pdf_metadata"/>
           <xsl:apply-templates select="$header//pdf[not(@filename = preceding::pdf/@filename)]"/>
           <file filename="Images/" datatype="png"/> 
           <xsl:apply-templates/>
       </package>
   </xsl:template> 
    
   <xsl:template match="book">
       <xsl:apply-templates select="descendant::graphic | descendant::inline-graphic"/>
   </xsl:template>
    
    <xsl:template match="graphic | inline-graphic">
        <file filename="{concat('Images/',@xlink:href, '.', @mime-subtype)}" datatype="{@mime-subtype}"/>
    </xsl:template>
    
    <xsl:template match="pdf">
        <file filename="{@filename}" datatype="pdf"/>
    </xsl:template>
</xsl:stylesheet>
