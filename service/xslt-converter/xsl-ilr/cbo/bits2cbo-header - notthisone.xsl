<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">

<!--dependencies - valid CUP-BITS file -->
<!--         -->
<!-- 
    DEPENDENCY: valid CUP-BITS file processed by add-filenames.xsl
                which adds @chunk  and @filename to every element that is to be the root of an individual html file
                and @filename to every element with attribute id or rid
    PURPOSE: creates  CBO header file - initially for ILR:
    the decision as to what is to be a separate html file was made at the add-filenames.xsl stage
    INPUT PARAMETERS: 
    USES FUNCTIONS:
    COMMENTS: 
    PIPELINE: precede by add-filenames.xsl
    DECSIONS:
        one html file per case report in ILR
        for now links are created using the filename attribute in the amended BITS file
    WARNINGS : INITIALLY THIS ONLY WORKS FOR ILR
    -->
    
    <xsl:output method="xml" indent="yes" doctype-public="-//CUP//DTD eBook Header DTD//EN" doctype-system="http://dtd.cambridge.org/schemas/2014/06/header.dtd"/>
    <xsl:param name="e-isbn"/>
    <xsl:variable name="isbn" select="book/book-meta/book-id"/>
    
   <xsl:template match="/">
       <xsl:apply-templates/>
   </xsl:template> 
    
    <xsl:template match="book">
        <book>
            <xsl:attribute name="id"><xsl:value-of select="concat('CBO', $e-isbn)"/></xsl:attribute>
            <metadata>
                <xsl:apply-templates select="." mode="metadata"/>
            </metadata>
            <content-items id="{concat('CBO', $e-isbn, 'A002')}">
                <xsl:apply-templates select="front-matter/* |book-body/* | book-back/* | front-matter/processing-instruction('anchor')"/> <!--for ILR-->
            </content-items>
        </book>
    </xsl:template>
    
<!--    metadata   -->
    <xsl:template match="book" mode="metadata">
        <isbn><xsl:value-of select="book-meta/isbn"/></isbn>
            <!--[TOM TO DO]-->
    </xsl:template>
    
    <xsl:template match="*" mode="subtitle">
        <subtitle><xsl:apply-templates/></subtitle>
    </xsl:template>
  
<!--  content   -->    
    
    <xsl:template match="front-matter/* | book-back/*">
        <xsl:variable name="id" select="string(@id)"></xsl:variable>
        <xsl:choose>
            <xsl:when test="preceding::processing-instruction('anchor')[string(.)=$id]"></xsl:when>
            <xsl:otherwise>
                <content-item>
                    <xsl:apply-templates select="." mode="attributes1"/>
                    <xsl:apply-templates select="." mode="attributes2"/>
                    <pdf filename="{concat(substring-before(@filename,'.html'),'.pdf')}"/>
                    <html filename="{@filename}" rid="{@id}"/>
                    
                    <xsl:apply-templates select="." mode="heading"/>
                </content-item>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="book-part">
        <xsl:variable name="id" select="string(@id)"></xsl:variable>
        <xsl:choose>
            <xsl:when test="preceding::processing-instruction('anchor')[string(.)=$id]"></xsl:when>
            <xsl:otherwise>
                <content-item>
                    <xsl:apply-templates select="." mode="attributes1"/>
                    <xsl:apply-templates select="." mode="attributes2"/>
                    <pdf filename="{concat(substring-before(@filename,'.html'),'.pdf')}"/>
                    <html filename="{@filename}" rid="{@id}"/>
                    <nav-file filename="nav_{@filename}" rid="{@id}"/> 
                    <xsl:apply-templates select="." mode="heading"/>
                </content-item>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="front-matter/processing-instruction('anchor')">
        <xsl:variable name="val" select="string(.)"/>
        <xsl:variable name="content" select="following::*[string(@id)=$val]"></xsl:variable>
        <content-item>
            <xsl:apply-templates select="$content" mode="attributes1"/>
            <xsl:apply-templates select="." mode="attributes2"/>
            <pdf filename="{concat(substring-before(@filename,'.html'),'.pdf')}"/>
            <html filename="{$content/@filename}" rid="{$content/@id}"/>
            <xsl:apply-templates select="$content" mode="heading"/>
        </content-item>
    </xsl:template>
    
    <xsl:template match="*" mode="attributes1">
        <xsl:attribute name="page-start" select="descendant::named-content[@content-type='page'][1]"/>
        <xsl:attribute name="page-end" select="descendant::named-content[@content-type='page'][last()]"/>
        <xsl:attribute name="type" select="if (@book-part-type) then (string(@book-part-type)) else (name())"/>
    </xsl:template>
    
    <xsl:template match="*" mode="attributes2">
        <xsl:variable name="num">
                    <xsl:value-of select="count(preceding::*[parent::front-matter[parent::book] or parent::book-body or parent::book-back ]) 
                        +count(preceding::processing-instruction('anchor')[parent::front-matter])
                        +1"/>
        </xsl:variable>
        <xsl:variable name="num2" select="$num +2"/>
        <xsl:variable name="num-len" select="string-length(string($num2))"/>
        <xsl:variable name="zeros">
            <xsl:choose>
                <xsl:when test="$num-len=1">A00</xsl:when>
                <xsl:when test="$num-len=2">A0</xsl:when>
                <xsl:when test="$num-len=3">A</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:attribute name="id" select="concat('CBO', $e-isbn, $zeros, $num2)"/>
        <xsl:attribute name="position" select="$num"/>
    </xsl:template>
    
    <xsl:template match="processing-instruction()" mode="attributes2">
        <xsl:variable name="num">
            <xsl:value-of select="count(preceding::*[parent::front-matter[parent::book] or parent::book-body or parent::book-back ]) 
                +count(preceding::processing-instruction('anchor')[parent::front-matter])
                +1"/>
        </xsl:variable>
        <xsl:variable name="num2" select="$num +2"/>
        <xsl:variable name="num-len" select="string-length(string($num2))"/>
        <xsl:variable name="zeros">
            <xsl:choose>
                <xsl:when test="$num-len=1">A00</xsl:when>
                <xsl:when test="$num-len=2">A0</xsl:when>
                <xsl:when test="$num-len=3">A</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:attribute name="id" select="concat('CBO', $e-isbn, $zeros, $num2)"/>
        <xsl:attribute name="position" select="$num"/>
    </xsl:template>
    
    <xsl:template match="*" mode="heading">
        <xsl:variable name="lookup">
            <names>
                <elem name="toc" type="" heading="Contents"/>
                <elem name="" type="" heading=""/>
            </names>
        </xsl:variable>
       <heading>
          <title> 
            <xsl:choose>
            <xsl:when test="book-part-meta/title-group/title">
                <xsl:attribute name="alphasort" select="book-part-meta/title-group/title/text()"/><!--change to apply templates--> 
                <xsl:value-of select="book-part-meta/title-group/title/text()"/> 
            </xsl:when>
            <xsl:when test="book-part-meta/title-group/alt-title">
                <xsl:attribute name="alphasort" select="book-part-meta/title-group/alt-title[@alt-title-type='right-running']"/>
                <xsl:value-of select="book-part-meta/title-group/alt-title[@alt-title-type='right-running']"/> 
            </xsl:when>
                <xsl:when test="title-group/title">
                    <xsl:attribute name="alphasort" select="title-group/title/text()"/>
                    <xsl:value-of select="title-group/title/text()"/> 
                </xsl:when>
                <xsl:when test="self::front-matter-part[@book-part-type='half-title-page']">
                    <xsl:attribute name="alphasort" select="'Half title page'"/>
                    <xsl:value-of select="'Half title page'"/> 
                </xsl:when>
                <xsl:when test="self::front-matter-part[@book-part-type='series-page']">
                    <xsl:attribute name="alphasort" select="'Series page'"/>
                    <xsl:value-of select="'Series page'"/> 
                </xsl:when>
                <xsl:when test="self::front-matter-part[@book-part-type='title-page']">
                    <xsl:attribute name="alphasort" select="'Title page'"/>
                    <xsl:value-of select="'Title page'"/> 
                </xsl:when>
                <xsl:when test="self::front-matter-part[@book-part-type='imprint-page']">
                    <xsl:attribute name="alphasort" select="'Imprints page'"/>
                    <xsl:value-of select="'Imprints page'"/> 
                </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="alphasort" select="name()"/>
                <xsl:value-of select="name()"/>
            </xsl:otherwise>
        </xsl:choose>
        </title>
      </heading> 
    </xsl:template>
  
</xsl:stylesheet>
