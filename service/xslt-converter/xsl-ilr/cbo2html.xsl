<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">

<!--dependencies - valid CUP-BITS file and valid CBO header file
        acts on CBO header file
    currently imports amended BITS file that has been processed by add-filenames.xsl
    
    -->
<!-- acts on: header file        -->

    <!-- Import/include templates for HTML conversion. Precedence is as follows:
            * Anything in this stylesheet overrides anything imported/included
            * Anything in ILR.xsl will override everything else imported
            * Then bits2html.xsl
            * Then elements.xsl
            * Finally default.xsl -->
    <xsl:import href="html/bits2html.xsl"/>
    <xsl:include href="html/include/ILR.xsl"/>
    
    <xsl:output method="html" indent="no"/>
    
    
    <xsl:param name="isbn"/>
    <xsl:param name="image-path" select="'Images/'"/>
   <!-- <xsl:variable name="isbn" select="/book/metadata/isbn"/>-->
    <xsl:variable name="content" select="concat($isbn, '_BITS.xml')"/>
    <xsl:variable name="content-file" select="document($content, .)"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:template match="book">
        <xsl:apply-templates select="descendant::html"/>
            <xsl:apply-templates select="." mode="vol-nav"/><!--one navigation file for the whole volume-->
    </xsl:template>

    <xsl:template match="html">
        <xsl:variable name="rid" select="@rid"/>
        <xsl:variable name="filename" select="@filename"/>
        <xsl:variable name="contentNode" select="$content-file//*[@id=$rid]"/>
        <xsl:result-document href="{@filename}">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta charset="UTF-8"/>
                        <link href="{concat($css-path, 'cup-bits.css')}" rel="stylesheet" type="text/css"></link> 
                        <title><xsl:value-of select="parent::content-item/heading/title"/></title>
                    </head>
                    <body>
                        <!-- Build the HTML for the current content-item, by applying templates to the corresponding 
                             element in the BITS file -->
                        <xsl:apply-templates select="$contentNode" mode="cbo"/>
                        
                        <!-- Do the same for any siblings in the BITS file with the same @filename (which add-filenames.xsl
                             does for special cases, such as consecutive ILR stubs) -->
                        <xsl:apply-templates select="$contentNode/following-sibling::*[@filename=$filename]" mode="cbo"/>
                        
                        <xsl:apply-templates select="$contentNode" mode="footnotes"/> 
                        <xsl:apply-templates select="$contentNode" mode="pages"/>
                    </body>
                </html>
            </xsl:result-document>
    </xsl:template>
    
  
</xsl:stylesheet>
