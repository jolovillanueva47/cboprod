<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">

<!--    acts on a bits file that has had the filenames of the individual html files added as att filename-->
<!--    overrides the math for epub-->
    
    <!--  include the default epub xslt  -->
    <xsl:import href="bits2epub.xsl"/>

    <xsl:template match="alternatives[child::mml:math]" mode="kernel.body" priority="2.0">
        <xsl:apply-templates mode="kernel.body" select="graphic | inline-graphic | textual-form"/>
    </xsl:template>
    
<!--    kindlegen doesn't like id on body (fails to resolve hyperlink) -->
    <xsl:template match="/child::*[1]" mode="kernel.body" priority="20">
        <body>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <a><xsl:apply-templates mode="kernel.body" select="@*"/></a>
            <xsl:apply-templates select="book-part-meta | title-group | title | label" mode="kernel.header"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </body>
    </xsl:template>
 
    <!--    kindlegen doesn't like  header tag (forcefully closes it); use div class=header instead -->
      <xsl:template match="title-group | /child::*[1][label or title]" mode="kernel.header" priority="2">
        <xsl:variable name="elem-with-id" select="ancestor-or-self::*[@id][1]"> </xsl:variable>
        <xsl:variable name="style" select="substring-before($elem-with-id/@id, '-')"/>
        <div class="header">
            <h1>
                <xsl:attribute name="class" select="$style"/>
                <xsl:apply-templates mode="do-heading" select="label | title"/>
            </h1>
        </div>
        <xsl:apply-templates mode="do-heading" select="subtitle">
            <xsl:with-param name="type" select="div"/>
        </xsl:apply-templates>
    </xsl:template>
    
<!--    these three templates have been added here to solve a problem of character encoding in the mobi file
    not entirely sure which of the changes in these three templates are the critical ones
    kindle seems not to like html5-->
    <xsl:template match="/" mode="kernel.head" priority="2">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when
                        test="
                        if (descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)]) 
                        then true()
                        else false()">
                        <xsl:apply-templates mode="kernel.head"
                            select="(descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)])[1]"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'BITS2EPUB'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <meta charset="UTF-8"/>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><!--added this-->
            <meta content="bits2epub v.{$bits2epub-version}" name="generator"/>
            <link href="{concat($path.css, 'cup-bits-epub3.css')}" media="all" rel="stylesheet" type="text/css"/>
            <xsl:apply-templates mode="mathjax" select="descendant::mml:math[1]"/>
        </head>
    </xsl:template>
    
    <xsl:template match="/" mode="kernel" priority="2">
        <html xmlns="http://www.w3.org/1999/xhtml"><!--added namespace-->
            <xsl:apply-templates mode="kernel.head" select="."/>
            <xsl:apply-templates mode="kernel.body"/>
        </html>
    </xsl:template>
    
    <xsl:template match="/">
        <xsl:for-each select="descendant::*[@chunk][@filename]">
            <xsl:variable as="document-node()" name="document">
                <xsl:document>
                    <!--<xsl:copy-of select="."/>-->
                    <xsl:apply-templates mode="document" select="."/>
                </xsl:document>
            </xsl:variable>
            <xsl:result-document href="{@filename}"><!--don't add html doctype-->
                <xsl:apply-templates mode="kernel" select="$document"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>
