<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" 
    xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns:mml="http://www.w3.org/1998/Math/MathML" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes="cup xlink xs">
   
<!--   
    REQUIREMENTS FOR HEADINGS
    
    when @chunk='yes'
    <header><h1 class="CT"><span class="label">label text</span> title text</h1><div class="subtitle">subtitle</div></header>
    where CT is the first part of the element ID
    
    where the heading may be one of:
    
        book-part/book-part-meta/title-group
        dedication/book-part-meta/title-group
        foreword/book-part-meta/title-group
        front-matter-part/book-part-meta/title-group
        preface/book-part-meta/title-group
        
        index/title-group
        index-div/title-group
        index-group/title-group
        toc/title-group
        toc-div/title-group
        toc-group/title-group
        
        ack/(label|title)
        app/(label|title)
        app-group/(label|title)
        glossary/(label|title)
        ref-list/(label|title)
        
    otherwise
        when the heading is part of the heading hierarchy:
        <h1 class="A"><span class="label">label text</span> title text: <span class="subtitle">subtitle</div></h1>
        
        where the heading may be one of:
        ack[label|title]
        app[label|title]
        app-group[label|title]
        glossary[label|title]
        ref-list[label|title]
        sec[label|title]
        
        otherwise (ie not part of the hierarchy)
        <div class="disp-quote title"><span class="label">label text</span> title text: <span class="subtitle">subtitle</div>
    -->
    
<!--    first the header-->
    <xsl:template match="/child::*[1]" mode="kernel.body" priority="20">
        <body>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates select="book-part-meta | title-group | title | label" mode="kernel.header"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </body>
    </xsl:template>
    
    <xsl:template match="book-part-meta" mode="kernel.header" priority="2">
        <xsl:apply-templates select="title-group" mode="kernel.header"/>
        <xsl:apply-templates select="contrib-group" mode="kernel.body"/>
    </xsl:template>
    
<!--    those with a title group do the title group-->
    <xsl:template match="title-group | /child::*[1][label or title]" mode="kernel.header" priority="2">
        <xsl:variable name="elem-with-id" select="ancestor::*[@id][1]">
        </xsl:variable>
        <xsl:variable name="style" select="substring-before($elem-with-id/@id, '-')"/>
        <header>
            <h1>
                <xsl:attribute name="class" select="$style"></xsl:attribute>
                <xsl:apply-templates select="label | title" mode="kernel.header"/>
            </h1>
        </header>
        <xsl:apply-templates select="subtitle" mode="kernel.header">
            <xsl:with-param name="type" select="div"/>
        </xsl:apply-templates>
    </xsl:template>
    
<!--    those without a title group do the parent as for title group-->
    <xsl:template match="/child::*[1]/label | /child::*[1]/title[not(preceding-sibling::label)]" mode="kernel.header" priority="2">
        <xsl:apply-templates select="parent::*" mode="kernel.header"/>
    </xsl:template>
    
<!--    then those in the hierarchy-->
    
    <xsl:template match="ack/label | ack/title[not(preceding-sibling::label)] | app/label | app/title[not(preceding-sibling::label)]
        | app-group/label | app-group/title[not(preceding-sibling::label)] | glossary/label | glossary/title[not(preceding-sibling::label)]
        | ref-list/label | ref-list/title[not(preceding-sibling::label)] | sec/label | sec/title[not(preceding-sibling::label)] |
        def-list/label |
        disp-quote/label |
        fn-group/label |
        statement/label |
        verse-group/label |
        list/label |
        answer/label |
        question/label |
        def-list/title[not(preceding-sibling::label)] |
        disp-quote/title[not(preceding-sibling::label)] |
        fn-group/title[not(preceding-sibling::label)] |
        statement/title[not(preceding-sibling::label)] |
        verse-group/title[not(preceding-sibling::label)] |
        list/title[not(preceding-sibling::label)] |
        answer/title[not(preceding-sibling::label)] |
        question/title[not(preceding-sibling::label)]" mode="kernel.body" priority="2">
        <xsl:apply-templates select="parent::*" mode="kernel.header"/>
    </xsl:template>
    
    <xsl:template match="ack |  app | app-group  | glossary | ref-list  | sec" mode="kernel.header" priority="2">
        <xsl:variable name="style" select="substring-before(@id, '-')"/>
        <xsl:variable  name="head.level" select="string(count(ancestor-or-self::*[label or title]))"/>
        <xsl:element name="{concat('h',$head.level)}">
                <xsl:attribute name="class" select="$style"/>
                <xsl:apply-templates select="label | title" mode="kernel.header"/>
                <xsl:apply-templates select="subtitle" mode="kernel.header">
                    <xsl:with-param name="type" select="span"/>
                </xsl:apply-templates>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="def-list | disp-quote | fn-group | statement | verse-group | list | answer | question" mode="kernel.header" priority="2">
        <xsl:variable name="style" select="substring-before(substring-before(@id, '-'), '-')"/>
        <xsl:variable  name="head.level" select="string(count(ancestor-or-self::*[label or title]))"/>
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates select="label | title" mode="kernel.header"/>
            <xsl:apply-templates select="subtitle" mode="kernel.header">
                <xsl:with-param name="type" select="span"/>
            </xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="label" mode="kernel.header" priority="2">
        <span class="label"><xsl:apply-templates/></span>
        <xsl:if test="following-sibling::title"><xsl:text> </xsl:text></xsl:if>
    </xsl:template>
    
    <xsl:template match="title" mode="kernel.header" priority="2">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="subtitle" mode="kernel.header" priority="2">
        <xsl:param name="type"/>
        <xsl:text> </xsl:text>
        <xsl:element name="{$type}">
            <xsl:attribute name="class" select="'subtitle'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    
</xsl:stylesheet>