<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs cup xlink" version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--
        CUP-BITS baseline HTML kernel
        
        Do not use this stylesheet on its own.
        It is intended to be imported by an implementation-specific stylesheet,
        and expects to be applied to a result tree fragment of type document-node().
        Note that the result tree fragment *must* be a single document node.
        
        The input document-node is expected to have had Unicode normalization form 
        NFD applied to all textual content prior to being passed to any code herein.
        Input that has not been subject to Unicode normalization, or has been
        subject to a normalization form other than NFD may cause unexpected output
        or errors.
        
        All templates are given an explicit priority; generally this is 1.0. Overriding
        templates in implementations must also use explicit priorities with a
        value greater than the kernel template to avoid unintended behaviour.
        
        Mode 'kernel' is used only as an entry point; 'kernel.head' (not to be confused
        with 'kernel.header'!) deals with the HTML head element; 'kernel.body' deals 
        with the HTML body element. Any BITS content that is unwanted in body content 
        by implementations may be excluded by overriding its template in 'kernel.body'. 
        Mode 'kernel.tbl-out' is a generic template for outputting a plain XHTML table; 
        note that the template for text nodes exists in all four modes, to ensure that text is
        subject to Unicode NFD.
        
        A reference implementation is provided in ../ref_impl/example.xsl
        -->

    <xsl:output byte-order-mark="no" encoding="UTF-8" indent="no"/>

    <xsl:param name="path.img" as="xs:string" required="no" select="'Images/'"/>
    <xsl:param name="path.css" as="xs:string" required="no" select="'Styles/'"/>

    <xsl:variable as="xs:string" name="regex.titles" select="'(\-)?title$'"/>
    <xsl:variable as="xs:string" name="regex.types" select="'\-type$'"/>
    <xsl:variable as="xs:string" name="regex.meta" select="'\-meta$'"/>

    <xsl:template match="/" mode="kernel" priority="1.0">
        <html>
            <xsl:apply-templates mode="kernel.head" select="."/>
            <xsl:apply-templates mode="kernel.body"/>
        </html>
    </xsl:template>

    <xsl:template match="/" mode="kernel.head" priority="1.0">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when
                        test="
                        if (descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)]) 
                        then true()
                        else false()">
                        <xsl:apply-templates mode="kernel.head"
                            select="(descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)])[1]"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('CUP-BITS baseline HTML: ',current-date())"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <meta charset="UTF-8"/>
            <meta content="cup-bits2html_baseline" name="generator"/>
            <link href="baseline.css" media="all" rel="stylesheet" type="text/css"/>
        </head>
    </xsl:template>

    <xsl:template match="*[matches(local-name(),$regex.titles)]" mode="kernel.head" priority="1.0">
        <xsl:apply-templates mode="kernel.head"/>
    </xsl:template>
    
    <!-- =================== MODE KERNEL.BODY: UNPROCESSED ================== -->

    <xsl:template match="*" mode="kernel.body" priority="-1.0">
        <div class="unprocessed" style="border:1px red solid;color:green;">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

<!-- =================== MODE KERNEL.BODY: ATTRIBUTES ================== -->

    <xsl:template match="@id" mode="kernel.body" priority="1.0">
        <xsl:copy copy-namespaces="yes"/>
    </xsl:template>
    
    <xsl:template match="@xml:lang" mode="kernel.body" priority="1.0">
        <xsl:copy copy-namespaces="yes"/>
    </xsl:template>
    
    <xsl:template match="@xlink:href[matches(parent::*/local-name(),'(\-)?graphic$')]" mode="kernel.body" priority="1.0">
        <xsl:attribute name="src" select="concat($path.img,string(),'.',parent::*/@mime-subtype/string())"/>
    </xsl:template>
    
    <xsl:template match="@xlink:href[parent::ext-link]" mode="kernel.body" priority="1.0">
        <xsl:attribute name="href" select="concat('#',string())"/>
    </xsl:template>
    
    <xsl:template match="@rid" mode="kernel.body" priority="1.0">
        <xsl:attribute name="href" select="concat('#',string())"/>
    </xsl:template>

    <xsl:template match="@*[not(local-name() eq 'id')]" mode="kernel.body" priority="-1.0"/>

    <xsl:template match="@*[matches(local-name(),$regex.types)]" mode="kernel.body" priority="1.0"/>

    <!-- ======================= MODE KERNEL.BODY: BODY =============== -->
    <xsl:template match="/child::*[1]" mode="kernel.body" priority="2.0">
        <body>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:sequence select="cup:headerConstructor(.,0)"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </body>
    </xsl:template>
    
<!--    ======================== MODE KERNEL.BODY:  ELEMENTS ================= -->

    <xsl:template match="abbrev" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="abstract" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="ack" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="addr-line" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="address" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="aff" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="aff-alternatives" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="alt-text" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="alt-title" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="alternatives" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="annotation" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="answer" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*[not(local-name() eq 'rid')]"/>
            <xsl:if test="child::label">
                <a>
                    <xsl:apply-templates mode="kernel.body" select="@*"/>
                    <xsl:apply-templates mode="kernel.body" select="label"/>
                </a>
            </xsl:if>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
        </div>
    </xsl:template>

    <xsl:template match="answer-set" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="app" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="app-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="array" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::tbody or self::attrib or self::permissions)]"/>
            <xsl:if test="child::tbody">
                <table>
                    <xsl:sequence select="cup:classConstructor(.)"/>
                    <caption>
                        <xsl:value-of select="local-name()"/>
                    </caption>
                    <colgroup>
                        <xsl:for-each select="max(for $r in child::tr return (count($r/child::*)))">
                            <col/>
                        </xsl:for-each>
                    </colgroup>
                    <xsl:apply-templates mode="kernel.body" select="tbody"/>
                </table>
            </xsl:if>
            <xsl:apply-templates mode="kernel.body" select="attrib | permissions"/>
        </div>
    </xsl:template>

    <xsl:template match="article-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="attrib" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="award-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="back" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="bio" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="body" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="bold" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--obsolete?-->
    <!--<xsl:template match="*[matches(local-name(),$regex.meta)]" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <caption>
                <xsl:value-of select="local-name()"/>
            </caption>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tbody>
                <xsl:for-each select="node()">
                    <xsl:apply-templates mode="kernel.tbl-out" select="."/>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>-->

    <xsl:template match="book" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-back" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-body" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-id" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-meta" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <caption>
                <xsl:value-of select="local-name()"/>
            </caption>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tbody>
                <xsl:for-each select="node()">
                    <xsl:apply-templates mode="kernel.tbl-out" select="."/>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="book-part" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:sequence select="cup:headerConstructor(.,count(./ancestor-or-self::*[child::*[matches(local-name(),$regex.titles)]]))"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-part-id" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-part-meta" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <caption>
                <xsl:value-of select="local-name()"/>
            </caption>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tbody>
                <xsl:for-each select="node()">
                    <xsl:apply-templates mode="kernel.tbl-out" select="."/>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="book-part-wrapper" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="book-title-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-volume-id" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="book-volume-number" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="boxed-text" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="break" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="caption" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="chapter-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="chem-struct" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="chem-struct-wrap" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="citation-alternatives" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="code" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="col" mode="kernel.body" priority="1.0">
        <col>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </col>
    </xsl:template>

    <xsl:template match="colgroup" mode="kernel.body" priority="1.0">
        <colgroup>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </colgroup>
    </xsl:template>

    <xsl:template match="collab" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="collab-alternatives" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="collection-id" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="collection-meta" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <caption>
                <xsl:value-of select="local-name()"/>
            </caption>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tbody>
                <xsl:for-each select="node()">
                    <xsl:apply-templates mode="kernel.tbl-out" select="."/>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="comment" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="compound-kwd" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="compound-kwd-part" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="compound-subject" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="compound-subject-part" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-acronym" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-date" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-loc" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-name" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-num" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-sponsor" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conf-theme" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="conference" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="contrib" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="contrib-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="contrib-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="copyright-holder" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="copyright-statement" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="copyright-year" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="corresp" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="country" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="custom-meta" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="custom-meta-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="date" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="date-in-citation" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="day" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="dedication" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="def" mode="kernel.body" priority="1.0">
        <dd>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </dd>
    </xsl:template>
    
    <xsl:template match="def-head" mode="kernel.body" priority="1.0">
        <dd>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </dd>
    </xsl:template>
    
    <xsl:template match="def-item" mode="kernel.body" priority="1.0">
        <xsl:apply-templates mode="kernel.body"/>
    </xsl:template>

    <xsl:template match="def-list" mode="kernel.body" priority="1.0">
        <xsl:apply-templates select="child::label | child::title" mode="kernel.body"/>
        <dl>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)][not(self::title)]"/>
        </dl>
    </xsl:template>
    
    <xsl:template match="degrees" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="disp-formula" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="disp-formula-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="disp-quote" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="edition" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="elocation-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="email" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="etal" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="event" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="explanation" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="ext-link" mode="kernel.body" priority="1.0">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </a>
    </xsl:template>
    
    <xsl:template match="fig" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="fig-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="fixed-case" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="floats-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="fn" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="fn-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="foreword" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="fpage" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="front-matter" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="front-matter-part" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="funding-source" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="given-names" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="glossary" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="graphic" mode="kernel.body" priority="1.0">
        <img>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </img>
    </xsl:template>
    
    <xsl:template match="hr" mode="kernel.body" priority="1.0">
        <hr/>
    </xsl:template>
    
    <xsl:template match="index" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="index-div" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="index-entry" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="index-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="index-term-range-end" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="inline-formula" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="inline-graphic" mode="kernel.body" priority="1.0">
        <img>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </img>
    </xsl:template>
    
    <xsl:template match="inline-supplementary-material" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="institution" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="isbn" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="issn" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="issn-l" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="issue" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="issue-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="issue-part" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="issue-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="italic" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="journal-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="kwd" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="kwd-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="label" mode="kernel.body" priority="1.0">
            <span>
                <xsl:sequence select="cup:classConstructor(.)"/>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </span>       
    </xsl:template>
    
<!--    <xsl:template match="label" mode="kernel.label-before" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>-->
    
    <xsl:template match="list" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="list-item" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="long-desc" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="lpage" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="mml:*" mode="kernel.body" priority="1.0">
        <xsl:copy copy-namespaces="yes">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="media" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="meta-name" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="meta-value" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="milestone-end" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="milestone-start" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="mixed-citation" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="monospace" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="month" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="name" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="name-alternatives" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="named-book-part-body" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="named-content" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="named-content[@content-type eq 'page']" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="nav-pointer" mode="kernel.body" priority="1.0">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </a>
    </xsl:template>
    
    <xsl:template match="nav-pointer-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="nested-kwd" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="object-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="on-behalf-of" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="overline" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="p" mode="kernel.body" priority="1.0">
        <xsl:variable as="attribute(class)" select="cup:classConstructor(.)" name="class.p"/>
        <div>
            
            <xsl:choose>
                <xsl:when test=".[preceding-sibling::*[1][self::p]]">
                    <xsl:sequence select="$class.p"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="{$class.p/local-name()}" select="concat($class.p/string(),' noindent')"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
<!--            <xsl:apply-templates select="preceding-sibling::*[1][self::label]" mode="kernel.label-before"/>-->
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="page-range" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="part-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="permissions" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="person-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="preface" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="prefix" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="preformat" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="private-char" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="pub-date" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="pub-history" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
        
    <xsl:template match="pub-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="publisher" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="publisher-loc" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="publisher-name" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="question" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="question-wrap" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="rb" mode="kernel.body" priority="1.0">
        <rb>
            <xsl:copy>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </xsl:copy>
        </rb>
    </xsl:template>

    <xsl:template match="ref" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="ref-list" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="related-object" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="role" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="roman" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="rp" mode="kernel.body" priority="1.0">
        <rp>
            <xsl:copy>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </xsl:copy>
        </rp>
    </xsl:template>
    
    <xsl:template match="rt" mode="kernel.body" priority="1.0">
        <rt>
            <xsl:copy>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </xsl:copy>
        </rt>
    </xsl:template>
    
    <xsl:template match="ruby" mode="kernel.body" priority="1.0">
        <ruby>
            <xsl:copy>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </xsl:copy>
        </ruby>
    </xsl:template>
    
    <xsl:template match="sans-serif" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="sc" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="sec" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="sec-meta" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <caption>
                <xsl:value-of select="local-name()"/>
            </caption>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tbody>
                <xsl:for-each select="node()">
                    <xsl:apply-templates mode="kernel.tbl-out" select="."/>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
    
    <xsl:template match="see-also-entry" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="see-entry" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="series" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="sig" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="sig-block" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="source" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="speaker" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="speech" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="statement" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="std" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="std-organization" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="strike" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="string-conf" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="string-date" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="string-name" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="sub" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="subj-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="subject" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="suffix" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="sup" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="supplement" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="supplementary-material" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="surname" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    


    <xsl:template match="table" mode="kernel.body" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </table>
    </xsl:template>

    <xsl:template match="table-wrap" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="table-wrap-foot" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="table-wrap-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="target" mode="kernel.body" priority="1.0">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </a>
    </xsl:template>
    
    <xsl:template match="tbody" mode="kernel.body" priority="1.0">
        <tbody>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </tbody>
    </xsl:template>
    
    <xsl:template match="td" mode="kernel.body" priority="1.0">
        <td>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </td>
    </xsl:template>
    
    <xsl:template match="term" mode="kernel.body" priority="1.0">
        <dt>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:if test="parent::def-item[@id] and not(.[@id])"><xsl:apply-templates select="parent::def-item/@id" mode="kernel.body"/></xsl:if>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </dt>
    </xsl:template>
    
    <xsl:template match="term-head" mode="kernel.body" priority="1.0">
        <dt>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:if test="parent::def-item[@id] and not(.[@id])"><xsl:apply-templates select="parent::def-item/@id" mode="kernel.body"/></xsl:if>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </dt>
    </xsl:template>
    
    <xsl:template match="tex-math" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:text>++TeX MATH IS NOT HANDLED BY THE KERNEL TRANSFORMATION++</xsl:text>
        </div>
    </xsl:template>
    
    <xsl:template match="textual-form" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="tfoot" mode="kernel.body" priority="1.0">
        <tfoot>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </tfoot>
    </xsl:template>
    
    <xsl:template match="th" mode="kernel.body" priority="1.0">
        <th>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </th>
    </xsl:template>
    
    <xsl:template match="thead" mode="kernel.body" priority="1.0">
        <thead>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </thead>
    </xsl:template>

    <xsl:template match="title[not(ancestor::toc)]" mode="kernel.body" priority="1.0">
        <xsl:variable as="xs:integer" name="head.level" select="count(ancestor-or-self::*[child::*[matches(local-name(),$regex.titles)]])"/>
        <xsl:element name="{concat('h',$head.level)}">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </xsl:element>
    </xsl:template>


    <xsl:template match="title[ancestor::toc]" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="title-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="text()[string-length(.) gt 0]" mode="kernel.tbl-out kernel.body kernel.head kernel.header" priority="1.0">
        <xsl:value-of select="normalize-unicode(.,'NFC')"/>
    </xsl:template>
    
    <xsl:template match="toc" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="toc-div" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="toc-entry" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="toc-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="tr" mode="kernel.body" priority="1.0">
        <tr>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </tr>
    </xsl:template>
    
    <xsl:template match="trans-abstract" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="trans-source" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="trans-subtitle" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="trans-title" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="trans-title-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="underline" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="unstructured-kwd-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="uri" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="verse-group" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="verse-line" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="volume" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="volume-id" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="volume-in-collection" mode="kernel.body" priority="1.0">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <xsl:template match="volume-number" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="volume-series" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="volume-title" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="x" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <xsl:template match="xref" mode="kernel.body" priority="1.0">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </a>
    </xsl:template>

    <xsl:template match="year" mode="kernel.body" priority="1.0">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
<!--    ======================= MODE KERNEL.TBL-OUT: METADATA =================== -->

    <xsl:template match="*[matches(parent::*/local-name(),$regex.meta)]" mode="kernel.tbl-out" priority="1.0">
        <tr>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <td rowspan="1">
                <xsl:value-of select="local-name()"/>
            </td>
            <td>
                <xsl:apply-templates mode="kernel.tbl-out"/>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="*[not(matches(parent::*/local-name(),$regex.meta))]" mode="kernel.tbl-out" priority="1.0">
        <table>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <colgroup>
                <col/>
                <col/>
            </colgroup>
            <tr>
                <td rowspan="{if (count(child::element()) gt 1) then count(child::element()) else 1}">
                    <xsl:value-of select="local-name()"/>
                </td>
                <td>
                    <xsl:apply-templates mode="kernel.tbl-out"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    
    <!--    =============== MODE HEADER: LABELS AND TITLES ================ -->

    <xsl:template match="element()" mode="kernel.header" priority="0.5">
        <xsl:apply-templates mode="kernel.header" select="child::node()[not(self::text())]"/>
    </xsl:template>


    <xsl:template match="label" mode="kernel.header" priority="1.0">
        <h>
            <xsl:apply-templates mode="kernel.body" select="child::node()"/>
        </h>
    </xsl:template>

    <xsl:template match="title | book-title" mode="kernel.header" priority="1.0">
        <h>
            <xsl:apply-templates mode="kernel.body" select="child::node()"/>
        </h>
    </xsl:template>

    <xsl:template match="subtitle" mode="kernel.header" priority="1.0">
        <st>
            <xsl:apply-templates mode="kernel.body" select="child::node()"/>
        </st>
    </xsl:template>
    
<!--    ===================== FUNCTIONS ================= -->

<!-- =============== ATTRIBUTE CLASS ===============-->
    
    <xsl:function as="attribute()" name="cup:classConstructor">
        <xsl:param as="element()" name="elem"/>
        <xsl:choose>
            <xsl:when test="$elem[@*[matches(local-name(),'\-type$')]]">
                <xsl:attribute name="class"
                    select="concat($elem/local-name(),for $a in $elem/@*[matches(local-name(),'\-type$')] return concat(' ',$a/string()))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="class" select="$elem/local-name()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
<!-- ================= HEADER ================== -->

    <xsl:function as="element()+" name="cup:headerConstructor">
        <xsl:param as="element()" name="elem"/>
        <xsl:param as="xs:integer" name="head.level"/>
        <xsl:variable as="element()+" name="header.src">
            <xsl:choose>
                <xsl:when test="$elem[self::book][child::book-meta[book-title-group[book-title or label or subtitle]]]">
                    <xsl:copy-of select="$elem/book-meta/book-title-group"/>
                </xsl:when>
                <xsl:when test="$elem[child::book-part-meta[title-group[title or label or subtitle]]]">
                    <xsl:copy-of select="$elem/book-part-meta/title-group"/>
                </xsl:when>
                <xsl:when test="$elem[title-group[title or label or subtitle]]">
                    <xsl:copy-of select="$elem/title-group"/>
                </xsl:when>
                <xsl:when test="$elem[label or title or subtitle]">
                    <xsl:copy-of select="for $e in ($elem/label | $elem/title | $elem/subtitle) return $e"/>
                </xsl:when>
                <xsl:otherwise>
                    <title class="unprocessed" xmlns="">++NO CANDIDATE FOR TITLE FOUND++</title>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!--<xsl:message><xsl:value-of select="$header.src"></xsl:value-of></xsl:message>-->
        <xsl:variable as="element()+" name="header.raw">
            <xsl:choose>
                <xsl:when test="not($header.src/*)"><title class="unprocessed" xmlns="">++NO CANDIDATE FOR TITLE FOUND++</title></xsl:when>
                <xsl:otherwise><xsl:apply-templates mode="kernel.header" select="$header.src"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <header>
            <xsl:element name="{concat('h',$head.level + (if (matches(local-name($elem),$regex.titles)) then 0 else 1))}">
                <xsl:for-each select="$header.raw[self::h]/node()">
                    <xsl:copy-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:element>
            <xsl:if test="$elem[st]">
                <div class="subtitle">
                    <xsl:copy-of select="$header.raw[self::st]/node()"/>
                </div>
            </xsl:if>
        </header>
    </xsl:function>

    <cup:permitted>
        <!--for reference: elements allowed in CUP-BITS-->
        <element>abbrev</element>
        <element>abstract</element>
        <element>ack</element>
        <element>addr-line</element>
        <element>address</element>
        <element>aff</element>
        <element>aff-alternatives</element>
        <element>alt-text</element>
        <element>alt-title</element>
        <element>alternatives</element>
        <element>annotation</element>
        <element>answer</element>
        <element>answer-set</element>
        <element>app</element>
        <element>app-group</element>
        <element>array</element>
        <element>article-title</element>
        <element>attrib</element>
        <element>award-id</element>
        <element>back</element>
        <element>bio</element>
        <element>body</element>
        <element>bold</element>
        <element>book</element>
        <element>book-back</element>
        <element>book-body</element>
        <element>book-id</element>
        <element>book-meta</element>
        <element>book-part</element>
        <element>book-part-id</element>
        <element>book-part-meta</element>
        <element>book-part-wrapper</element>
        <element>book-title</element>
        <element>book-title-group</element>
        <element>book-volume-id</element>
        <element>book-volume-number</element>
        <element>boxed-text</element>
        <element>break</element>
        <element>caption</element>
        <element>chapter-title</element>
        <element>chem-struct</element>
        <element>chem-struct-wrap</element>
        <element>citation-alternatives</element>
        <element>code</element>
        <element>col</element>
        <element>colgroup</element>
        <element>collab</element>
        <element>collab-alternatives</element>
        <element>collection-id</element>
        <element>collection-meta</element>
        <element>comment</element>
        <element>compound-kwd</element>
        <element>compound-kwd-part</element>
        <element>compound-subject</element>
        <element>compound-subject-part</element>
        <element>conf-acronym</element>
        <element>conf-date</element>
        <element>conf-loc</element>
        <element>conf-name</element>
        <element>conf-num</element>
        <element>conf-sponsor</element>
        <element>conf-theme</element>
        <element>conference</element>
        <element>contrib</element>
        <element>contrib-group</element>
        <element>contrib-id</element>
        <element>copyright-holder</element>
        <element>copyright-statement</element>
        <element>copyright-year</element>
        <element>corresp</element>
        <element>country</element>
        <element>custom-meta</element>
        <element>custom-meta-group</element>
        <element>date</element>
        <element>date-in-citation</element>
        <element>day</element>
        <element>dedication</element>
        <element>def</element>
        <element>def-head</element>
        <element>def-item</element>
        <element>def-list</element>
        <element>degrees</element>
        <element>disp-formula</element>
        <element>disp-formula-group</element>
        <element>disp-quote</element>
        <element>edition</element>
        <element>elocation-id</element>
        <element>email</element>
        <element>etal</element>
        <element>event</element>
        <element>explanation</element>
        <element>ext-link</element>
        <element>fig</element>
        <element>fig-group</element>
        <element>fixed-case</element>
        <element>floats-group</element>
        <element>fn</element>
        <element>fn-group</element>
        <element>foreword</element>
        <element>fpage</element>
        <element>front-matter</element>
        <element>front-matter-part</element>
        <element>funding-source</element>
        <element>given-names</element>
        <element>glossary</element>
        <element>graphic</element>
        <element>hr</element>
        <element>index</element>
        <element>index-div</element>
        <element>index-entry</element>
        <element>index-group</element>
        <element>index-term-range-end</element>
        <element>inline-formula</element>
        <element>inline-graphic</element>
        <element>inline-supplementary-material</element>
        <element>institution</element>
        <element>isbn</element>
        <element>issn</element>
        <element>issn-l</element>
        <element>issue</element>
        <element>issue-id</element>
        <element>issue-part</element>
        <element>issue-title</element>
        <element>italic</element>
        <element>journal-id</element>
        <element>kwd</element>
        <element>kwd-group</element>
        <element>label</element>
        <element>list</element>
        <element>list-item</element>
        <element>long-desc</element>
        <element>lpage</element>
        <element>mml:math</element>
        <element>media</element>
        <element>meta-name</element>
        <element>meta-value</element>
        <element>milestone-end</element>
        <element>milestone-start</element>
        <element>mixed-citation</element>
        <element>monospace</element>
        <element>month</element>
        <element>name</element>
        <element>name-alternatives</element>
        <element>named-book-part-body</element>
        <element>named-content</element>
        <element>nav-pointer</element>
        <element>nav-pointer-group</element>
        <element>nested-kwd</element>
        <element>object-id</element>
        <element>on-behalf-of</element>
        <element>overline</element>
        <element>p</element>
        <element>page-range</element>
        <element>part-title</element>
        <element>permissions</element>
        <element>person-group</element>
        <element>preface</element>
        <element>prefix</element>
        <element>preformat</element>
        <element>private-char</element>
        <element>pub-date</element>
        <element>pub-history</element>
        <element>pub-id</element>
        <element>publisher</element>
        <element>publisher-loc</element>
        <element>publisher-name</element>
        <element>question</element>
        <element>question-wrap</element>
        <element>rb</element>
        <element>ref</element>
        <element>ref-list</element>
        <element>related-object</element>
        <element>role</element>
        <element>roman</element>
        <element>rp</element>
        <element>rt</element>
        <element>ruby</element>
        <element>sans-serif</element>
        <element>sc</element>
        <element>sec</element>
        <element>sec-meta</element>
        <element>see-also-entry</element>
        <element>see-entry</element>
        <element>series</element>
        <element>sig</element>
        <element>sig-block</element>
        <element>source</element>
        <element>speaker</element>
        <element>speech</element>
        <element>statement</element>
        <element>std</element>
        <element>std-organization</element>
        <element>strike</element>
        <element>string-conf</element>
        <element>string-date</element>
        <element>string-name</element>
        <element>sub</element>
        <element>subj-group</element>
        <element>subject</element>
        <element>subtitle</element>
        <element>suffix</element>
        <element>sup</element>
        <element>supplement</element>
        <element>supplementary-material</element>
        <element>surname</element>
        <element>table</element>
        <element>table-wrap</element>
        <element>table-wrap-foot</element>
        <element>table-wrap-group</element>
        <element>target</element>
        <element>tbody</element>
        <element>td</element>
        <element>term</element>
        <element>term-head</element>
        <element>tex-math</element>
        <element>textual-form</element>
        <element>tfoot</element>
        <element>th</element>
        <element>thead</element>
        <element>title</element>
        <element>title-group</element>
        <element>toc</element>
        <element>toc-div</element>
        <element>toc-entry</element>
        <element>toc-group</element>
        <element>tr</element>
        <element>trans-abstract</element>
        <element>trans-source</element>
        <element>trans-subtitle</element>
        <element>trans-title</element>
        <element>trans-title-group</element>
        <element>underline</element>
        <element>unstructured-kwd-group</element>
        <element>uri</element>
        <element>verse-group</element>
        <element>verse-line</element>
        <element>volume</element>
        <element>volume-id</element>
        <element>volume-in-collection</element>
        <element>volume-number</element>
        <element>volume-series</element>
        <element>volume-title</element>
        <element>x</element>
        <element>xref</element>
        <element>year</element>
    </cup:permitted>

    <cup:disallowed>
        <!--for reference: elements disallowed in CUP-BITS-->
        <element>anonymous</element>
        <element>author-comment</element>
        <element>author-notes</element>
        <element>award-group</element>
        <element>count</element>
        <element>counts</element>
        <element>element-citation</element>
        <element>equation-count</element>
        <element>era</element>
        <element>event-desc</element>
        <element>fax</element>
        <element>fig-count</element>
        <element>funding-group</element>
        <element>funding-statement</element>
        <element>glyph-data</element>
        <element>glyph-ref</element>
        <element>gov</element>
        <element>index-term</element>
        <element>institution-id</element>
        <element>institution-wrap</element>
        <element>license</element>
        <element>license-p</element>
        <element>note</element>
        <element>notes</element>
        <element>open-access</element>
        <element>overline-end</element>
        <element>overline-start</element>
        <element>page-count</element>
        <element>patent</element>
        <element>phone</element>
        <element>price</element>
        <element>principal-award-recipient</element>
        <element>principal-investigator</element>
        <element>product</element>
        <element>ref-count</element>
        <element>related-article</element>
        <element>season</element>
        <element>see</element>
        <element>see-also</element>
        <element>self-uri</element>
        <element>size</element>
        <element>styled-content</element>
        <element>table-count</element>
        <element>underline-end</element>
        <element>underline-start</element>
        <element>word-count</element>
    </cup:disallowed>

</xsl:stylesheet>
