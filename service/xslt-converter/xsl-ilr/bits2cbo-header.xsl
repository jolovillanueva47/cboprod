<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cup xlink xs java" extension-element-prefixes="sql" version="2.0"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:java="http://saxon.sf.net/java-type" xmlns:sql="http://saxon.sf.net/sql"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- 
    DEPENDENCIES:
      * CUP-BITS file processed by add-filenames.xsl
      * Saxon-EE on classpath
      * Saxon SQL extension on classpath
      * Oracle JDBC bridge on classpath
      * Saxon config file read on run
    PURPOSE: creates  CBO header file - initially for ILR:
    the decision as to what is to be a separate html file was made at the add-filenames.xsl stage
    INPUT PARAMETERS: e-isbn
    USES FUNCTIONS:
    COMMENTS: 
    PIPELINE: precede by add-filenames.xsl
    DECSIONS:
        one html file per case report in ILR
        for now links are created using the filename attribute in the amended BITS file
    WARNINGS : INITIALLY THIS ONLY WORKS FOR ILR
    -->
    
    <xsl:import href="functions/pages.xsl"/>
    
    <xsl:variable as="xs:string+" name="elems.with-seq-id" select="('content-items','author','content-item')"/>
    <!--contains local-names of elements to have CBO IDs for use in mode add.ids-->

    <xsl:variable as="xs:string" name="ora.select"
        select="'
        cihb.EAN_NUMBER AS hb_isbn,
        cioc.SERIES_CODE AS s_code,
        srtr.SERIES_LEGEND AS series,
        cipb.EAN_NUMBER AS pb_isbn,
        cioc.EAN_NUMBER AS oc_isbn,
        cihb.TITLE_CODE AS title_code,
        cioc.PRODUCT_GROUP_CODE AS prod_group,
        ttl.TITLE as title,
        ttl.SUBTITLE as subtitle,
        ttl.VOLUME_NUMBER AS vol,
        ttl.PART_NUMBER AS pt_num,
        ttl.PART_TITLE AS pt_ttl,
        ttl.EDITION_NUMBER AS ed_num,
        ttl.VOLUME_TITLE AS vl_ttl,
        SUBSTR(ttl.SUBJECT2_CODE,0,2) AS subj1,
        ttl.SUBJECT2_CODE AS subj2,
        ttl.SUBJECT3_CODE AS subj3,
        (SELECT sb.SUBJECT_LEGEND
        FROM oraliv.sr_subject_vista sb
        WHERE sb.SUBJECT_CODE = SUBSTR(ttl.SUBJECT2_CODE,0,2)) AS subject1_legend,
        (SELECT sb.SUBJECT_LEGEND
        FROM oraliv.sr_subject_vista sb
        WHERE sb.SUBJECT_CODE = ttl.SUBJECT2_CODE) AS subject2_legend,
        (SELECT sb.SUBJECT_LEGEND
        FROM oraliv.sr_subject_vista sb
        WHERE sb.SUBJECT_CODE = ttl.SUBJECT3_CODE) AS subject3_legend,
        ptf.BLURB150 AS blurb,
        cihb.year_first_published AS pubdate,
        cihb.page_extent AS pages'"/>
    <!--select clause for main Oracle query-->

    <xsl:variable as="xs:string" name="ora.from"
        select="'
        oraliv.core_isbn cioc
        LEFT OUTER JOIN oraliv.core_isbn cihb ON (cioc.TITLE_CODE=cihb.TITLE_CODE and cihb.FORMAT_CODE = ''HB'') 
        LEFT OUTER JOIN oraliv.core_isbn cipb ON (cioc.TITLE_CODE=cipb.TITLE_CODE and cipb.FORMAT_CODE = ''PB'') 
        LEFT OUTER JOIN oraliv.core_title ttl ON (cihb.TITLE_CODE=ttl.TITLE_CODE)
        LEFT OUTER JOIN oraliv.product_textfields ptf ON (cioc.TITLE_CODE=ptf.TITLE_CODE)
        LEFT OUTER JOIN oraliv.sr_series_trs srtr ON (cihb.SERIES_CODE=srtr.SERIES_CODE)'"/>
    <!--from clause for main Oracle query-->

    <xsl:variable as="xs:string" name="ora.where"
        select="concat('cioc.FORMAT_CODE = ''OC'' AND ''',replace(base-uri(.),'^(.*)((\d{13})(_BITS)?(\.xml))$','$3'),
        ''' IN (cihb.EAN_NUMBER, cihb.ISBN, cipb.EAN_NUMBER, cipb.ISBN, cioc.EAN_NUMBER)')"/>
    <!--where clause for main Oracle query-->

    <xsl:variable as="java:java.sql.Connection" name="connection">
        <sql:connect auto-commit="no" database="jdbc:oracle:thin:csmanila/csmanila@//131.111.154.51:1521/inpu.cup.cam.ac.uk"
            driver="oracle.jdbc.driver.OracleDriver" password="csmanila" user="csmanila" xsl:extension-element-prefixes="sql"
        > </sql:connect>
    </xsl:variable>

    <xsl:variable name="query.results">
        <sql:query column="{$ora.select}" connection="$connection" row-tag="BOOK" table="{$ora.from}" where="{$ora.where}"/>
        <xsl:message select="''"/>
    </xsl:variable>

    <xsl:variable name="author.query">
        <AUTHORS>
            <sql:query column="sequence_number, forename, surname, affiliation, authorship_role, orcid" connection="$connection"
                row-tag="AUTHOR" table="oraliv.core_ip_title_code" where="{concat('title_code = ',$db.title_code)}"/>
        </AUTHORS>
    </xsl:variable>

    <xsl:output doctype-public="-//CUP//DTD eBook Header DTD//EN" doctype-system="http://dtd.cambridge.org/schemas/2014/08/header.dtd"
        indent="yes" method="xml"/>
    <xsl:param name="e-isbn" required="no" select="$query.results/BOOK[1]/OC_ISBN"/>
    <xsl:variable as="xs:string" name="db.title_code" select="$query.results/BOOK[1]/TITLE_CODE"/>
    
    
    <xsl:variable name="bookType" select="/book/@book-type"/>
    <xsl:variable name="volume" as="xs:double" select="
        number(
            tokenize(
                string-join(/book/collection-meta/volume-in-collection/volume-number//text(), '')
                , '\D+')[.!=''][1])"/>
    <xsl:variable name="is-icsid" as="xs:boolean" select="
        $bookType = 'ilr' 
        and matches(
            normalize-space(
                string-join(/book/collection-meta/title-group//text(), '')
            ),
            '(ICSID|International\s*Centre\s*for\s*Settlement\s*of\s*Investment\s*Disputes)', 
            'is'
        )"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="book">
        <xsl:if test="count($query.results/descendant::BOOK) gt 1">
            <xsl:message select="'[WARN] Multiple matches found in database: check metadata carefully!'"/>
        </xsl:if>
        <xsl:variable as="document-node()" name="book-no-ids">
            <xsl:document>
                <book group="{$query.results/BOOK[1]/PROD_GROUP}" id="{concat('CBO',$e-isbn)}">
                    <metadata>
                        <xsl:apply-templates mode="db.meta" select="."/>
                    </metadata>
                    <content-items xml-dtd="bits">
                        <nav-file filename="nav_{$e-isbn}.html" rid="{front-matter/*[1]/@id}"/>
                        <!--    fix for missing index   -->
                        <xsl:apply-templates select="front-matter | book-body//book-part[@chunk='yes'] | book-back//book-part[@chunk='yes'] | book-back//app | book-back//index"/>
                    </content-items>
                </book>
            </xsl:document>
        </xsl:variable>
        <xsl:apply-templates mode="add.ids" select="$book-no-ids"/>
    </xsl:template>

    <!--    metadata   -->

    <!--
        ++README++
        mode meta:      Processes metadata from the BITS source doc
                        (all calls made while in mode db.meta)
        mode db.meta:   Processes metadata from Oracle; note that it pipes text nodes
                        through a fallback template to normalise space AFTER applying Unicode NFC
                        (sanity check for damaged text from DB, most likely in blurb), so when 
                        in db.meta, *apply-templates*, don't copy-of or value-of
                        Note also that db-sourced elements are named in FULL-CAPS as a
                        convention (this is specified, not inherent to the db output)
                        -->

    <xsl:template match="book" mode="db.meta">
        <!--<xsl:copy-of select="$author.query"/>-->
        <!--<xsl:copy-of select="$query.results"/>-->
        <!--for debugging-->
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/TITLE[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/SUBTITLE[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/ED_NUM[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$author.query"/>
        <doi><xsl:value-of select="concat('10.1014/', $e-isbn)"/></doi>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/OC_ISBN[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/*[self::PB_ISBN or self::HB_ISBN][not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/VOL[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/VL_TTL[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/PT_NUM[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/PT_TTL[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/SERIES[not(string-length() eq 0)]"/>
        <xsl:apply-templates mode="meta" select="descendant::book-meta[1]/publisher/publisher-name"/>
        <xsl:apply-templates mode="meta" select="descendant::book-meta[1]/publisher/publisher-loc"/>
        <pub-dates>
            <print-date>
                <xsl:apply-templates mode="meta" select="descendant::book-meta[1]/pub-date[@date-type eq 'first-published']/year"/>
            </print-date>
            <online-date>
                <xsl:value-of select="substring(string(current-date()),1,4)"/>
            </online-date>
        </pub-dates>
        <xsl:apply-templates mode="meta" select="descendant::book-meta[1]/permissions"/>
        <subject-group><xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/*[matches(local-name(),'^SUBJ\d$')]"/></subject-group>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/BLURB"/>
        <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/PAGES"/>
        <cover-image filename="{$e-isbn}i.jpg" type="standard"/>
        <cover-image filename="{$e-isbn}t.jpg" type="thumb"/>
    </xsl:template>

    <xsl:template match="OC_ISBN" mode="db.meta">
        <xsl:element name="isbn">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="PB_ISBN | HB_ISBN" mode="db.meta">
        <xsl:element name="alt-isbn">
            <xsl:attribute name="type" select="if (self::PB_ISBN) then 'paperback' else 'hardback'"/>
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="TITLE" mode="db.meta">
        <xsl:element name="main-title">
            <xsl:attribute name="alphasort" select="cup:alphaSortString(normalize-space(.))"/>
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="BLURB" mode="db.meta">
        <xsl:element name="blurb">
            <xsl:element name="p">
                <xsl:apply-templates mode="db.meta"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template match="PAGES" mode="db.meta">
        <xsl:element name="pages">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[matches(local-name(),'^SUBJ\d$')]" mode="db.meta">
        <xsl:choose>
            <xsl:when test="matches(local-name(),'1$')">
                <subject code="{.}" level="1">
                    <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/SUBJECT1_LEGEND"/>
                </subject>
            </xsl:when>
            <xsl:when test="matches(local-name(),'2$')">
                <subject code="{.}" level="2">
                    <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/SUBJECT2_LEGEND"/>
                </subject>
            </xsl:when>
            <xsl:otherwise>
                <subject code="{.}" level="3">
                    <xsl:apply-templates mode="db.meta" select="$query.results/BOOK[1]/SUBJECT3_LEGEND"/>
                </subject>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="SUBTITLE" mode="db.meta">
        <subtitle>
            <xsl:apply-templates mode="db.meta"/>
        </subtitle>
    </xsl:template>

    <xsl:template match="AUTHORS[child::AUTHOR]" mode="db.meta">
        <xsl:for-each select="AUTHOR">
            <xsl:sort select="AUTHOR/SEQUENCE_NUMBER/number()"/>
            <xsl:element name="author">
                <xsl:attribute name="alphasort" select="concat(./SURNAME,', ',./FORENAME)"/>
                <xsl:attribute name="position" select="./SEQUENCE_NUMBER"/>
                <xsl:attribute name="role" select="./AUTHORSHIP_ROLE"/>
                <xsl:element name="name">
                    <xsl:element name="forenames">
                        <xsl:apply-templates mode="db.meta" select="FORENAME"/>
                    </xsl:element>
                    <xsl:element name="surname">
                        <xsl:apply-templates mode="db.meta" select="SURNAME"/>
                    </xsl:element>
                </xsl:element>
                <xsl:if test="string-length(./AFFILIATION) gt 0">
                    <xsl:element name="affiliation">
                        <xsl:apply-templates mode="db.meta" select="AFFILIATION"/>
                    </xsl:element>
                </xsl:if>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="VOL" mode="db.meta">
        <xsl:element name="volume-number">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="VL_TTL" mode="db.meta">
        <xsl:element name="volume-title">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="PT_NUM" mode="db.meta">
        <xsl:element name="part-number">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="PT_TTL" mode="db.meta">
        <xsl:element name="part-title">
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="SERIES" mode="db.meta">
        <xsl:element name="series">
            <xsl:attribute name="alphasort" select="cup:alphaSortString(normalize-space(.))"/>
            <xsl:attribute name="code" select="normalize-space(normalize-unicode($query.results/BOOK[1]/S_CODE,'NFC'))"/>
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ED_NUM" mode="db.meta">
        <xsl:element name="edition">
            <xsl:attribute name="number" select="normalize-space(normalize-unicode(.,'NFC'))"/>
            <xsl:apply-templates mode="db.meta"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*" mode="db.meta">
        <xsl:apply-templates mode="db.meta"/>
    </xsl:template>

    <xsl:template match="text()" mode="db.meta">
        <!--apply templates to DB output, don't value-of or copy-of (in case of bad text)-->
        <xsl:copy-of select="normalize-space(normalize-unicode(.,'NFC'))"/>
    </xsl:template>

    <xsl:template match="publisher-name | publisher-loc" mode="meta">
        <xsl:copy-of copy-namespaces="no" select="."/>
    </xsl:template>

    <xsl:template match="copyright-year" mode="meta">
        <xsl:element name="copyright-date">
            <xsl:value-of select="normalize-space(normalize-unicode(string(.),'NFC'))"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="permissions" mode="meta">
        <copyright-statement>&#x00A9; <xsl:copy-of copy-namespaces="no" select="copyright-holder"/><xsl:apply-templates mode="meta"
                select="copyright-year"/></copyright-statement>
    </xsl:template>

    <!--  content   -->

    <xsl:template match="node()[not(local-name() = $elems.with-seq-id)]" mode="add.ids">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="add.ids" select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="node()[local-name() = $elems.with-seq-id]" mode="add.ids">
        <xsl:variable as="xs:string" name="seq.padded"
            select="concat('000',(count(./ancestor-or-self::*[local-name() = $elems.with-seq-id]) + count(./preceding::node()[local-name() = $elems.with-seq-id])))"/>
        <xsl:copy>
            <xsl:attribute name="id" select="concat('CBO',$e-isbn,'A',substring($seq.padded,string-length($seq.padded)-2,3))"/>
            <xsl:if test="self::content-item">
                <xsl:attribute name="position" select="count(ancestor-or-self::content-item) + count(preceding::content-item)"/>
            </xsl:if>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="add.ids" select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!--  content   -->    
    
<!--    fix for missing index   -->
    <xsl:template match="front-matter/* | book-back//*[@chunk = 'yes' and not(self::app-group)] | app | index">
        <xsl:variable name="id" select="string(@id)"></xsl:variable>
        <xsl:choose>
            <xsl:when test="preceding::processing-instruction('anchor')[string(.)=$id]"></xsl:when>
            <xsl:otherwise>
                <content-item>
                    <xsl:apply-templates select="." mode="page-num-stuff"/>
                    <html filename="{@filename}" rid="{@id}"/>
                    <xsl:apply-templates select="." mode="heading"/>
                </content-item>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="app-group">
        <xsl:apply-templates select="app"/>
    </xsl:template>
    
    <xsl:template match="book-part[@chunk='yes'] | app" priority="10">
        <xsl:variable name="id" select="string(@id)"/>
        <xsl:variable name="book-parts.padded" select="concat(
            '000',
            (
                count(ancestor-or-self::book-part[@chunk='yes']) 
                + count(preceding::book-part[@chunk='yes']) 
                + count(ancestor-or-self::app[@chunk='yes']) 
                + count(preceding::app[@chunk='yes'])
            )
        )"/>
        <xsl:variable name="items.num" select="substring($book-parts.padded,string-length($book-parts.padded)-2,3)"/>
        <xsl:choose>
            <xsl:when test="preceding::processing-instruction('anchor')[string(.)=$id]"></xsl:when>
            <xsl:otherwise>
                <content-item>
                    <xsl:apply-templates select="." mode="page-num-stuff"/> 
                    <html filename="{@filename}" rid="{@id}"/>
                    <xsl:if test="not(@book-part-type='case-report-xref')">
                        <xsl:if test="not(self::app) and not(@book-part-type = 'backmatter')">
                            <xsl:if test="not(@book-part-type = 'chapter' and $bookType = 'ilr')">
                                <nav-file filename="nav_{@filename}" rid="{@id}"/>
                            </xsl:if>
                        </xsl:if>
                        <doi><xsl:value-of select="concat('10.1017/CBO',$e-isbn, '.', $items.num )"/></doi>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="heading"/>
                </content-item>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="book-part">
        <xsl:variable name="id" select="string(@id)"/>
        <xsl:choose>
            <xsl:when test="preceding::processing-instruction('anchor')[string(.)=$id]"></xsl:when>
            <xsl:otherwise>
                <content-item>
                    <xsl:apply-templates select="." mode="page-num-stuff"/>
                    <html filename="{@filename}" rid="{@id}"/>
                    <nav-file filename="nav_{@filename}" rid="{@id}"/> 
                    <xsl:apply-templates select="." mode="heading"/>
                </content-item>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="front-matter/processing-instruction('anchor')">
        <xsl:variable name="val" select="string(.)"/>
        <xsl:variable name="content" select="following::*[string(@id)=$val]"></xsl:variable>
        <content-item>
            <xsl:apply-templates select="$content" mode="page-num-stuff"/>
            <html filename="{$content/@filename}" rid="{$content/@id}"/>
            <xsl:apply-templates select="$content" mode="heading"/>
        </content-item>
    </xsl:template>
    

    <xsl:template match="*" mode="page-num-stuff">
        <xsl:variable name="sp" as="xs:string" select="cup:firstPage(.)"/>
        <xsl:variable name="ep" as="xs:string">
            <xsl:choose>
                <xsl:when test="@book-part-type='case-report-xref' and following-sibling::book-part[@book-part-type='case-report-xref']">
                    <!-- Consecutive sibling stubs are to be grouped into single chunk, so find the page on which the last stub ends -->
                    <xsl:value-of select="cup:lastPage(following-sibling::book-part[@book-part-type='case-report-xref'][last()])"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="cup:lastPage(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:attribute name="page-start" select="$sp"/>
        <xsl:attribute name="page-end" select="$ep"/>
        <xsl:attribute name="type" select="if (@book-part-type) then (string(@book-part-type)) else (name())"/>
        <xsl:variable name="page-range" as="xs:string">
            <xsl:choose>
                <xsl:when test="$sp = $ep">
                    <xsl:value-of select="$sp"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($sp, '-',  $ep)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <!--<xsl:when test="@book-part-type='case-report-xref'">
                <!-\- No PDF for stubs? -\->
            </xsl:when>-->
            <xsl:when test="$is-icsid">
                <pdf filename="{concat('ICSID', $volume, '_', $page-range, '.pdf')}"/>
            </xsl:when>
            <xsl:when test="$bookType = 'ilr'">
                <pdf filename="{concat('ILR', $volume, '_', $page-range, '.pdf')}"/>
            </xsl:when>
            <xsl:otherwise>
                <pdf filename="{concat(substring-before(@filename,'.html'),'.pdf')}"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*" mode="heading">
        <xsl:variable name="lookup">
            <names>
                <elem heading="Contents" name="toc" type=""/>
                <elem heading="" name="" type=""/>
            </names>
        </xsl:variable>
        <xsl:variable name="volume" select="ancestor::book/collection-meta/volume-in-collection/volume-number"></xsl:variable>
        <heading>
            <title>
                <xsl:variable name="chunkTitle" as="xs:string*">
                    <xsl:choose>
                        <xsl:when test="book-part-meta/title-group/title">
                            <xsl:value-of select="book-part-meta/title-group/title//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]"/>
                            <xsl:if test="local-name() = 'book-part'">
                                <xsl:value-of select="book-part-meta/custom-meta-group/custom-meta[meta-name = 'case-citation']/meta-value"/>
                            </xsl:if>
                        </xsl:when>
                        <xsl:when test="book-part-meta/title-group/alt-title">
                            <!--
		                      CBT-731 – change the XSLT so that the HTML display uses the first right-hand running head in any case as the page head in the online service. In some cases the right-hand running head changes through the case
		                      2016-02-04 JBA
                            -->
                            <xsl:value-of select="
                                if 
                                    (number(substring-after($volume, ' ')) &gt; 156) 
                                then
                                    book-part-meta/title-group/alt-title[@alt-title-type = 'right-running']//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]
                                else
                                    book-part-meta/title-group/alt-title[1]//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]
                            "/>
                        </xsl:when> 
                        <xsl:when test="title-group/title">
                            <xsl:value-of select="title-group/title//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]"/>
                        </xsl:when>
                        <xsl:when test="self::front-matter-part[@book-part-type='half-title-page']">
                            <xsl:value-of select="'Half title page'"/>
                        </xsl:when>
                        <xsl:when test="self::front-matter-part[@book-part-type='series-page']">
                            <xsl:value-of select="'Series page'"/>
                        </xsl:when>
                        <xsl:when test="self::front-matter-part[@book-part-type='title-page']">
                            <xsl:value-of select="'Title page'"/>
                        </xsl:when>
                        <xsl:when test="self::front-matter-part[@book-part-type='imprint-page']">
                            <xsl:value-of select="'Imprints page'"/>
                        </xsl:when>
                        <xsl:when test="self::index[@content-type='case-list-complex']">
                            <xsl:value-of select="'Consolidated table of cases'"/>
                        </xsl:when>
                        <xsl:when test="self::book-part[@book-part-type='case-report-xref']">
                            <xsl:text>See also</xsl:text>
                        </xsl:when>
                        <xsl:when test="title">
                            <xsl:value-of select="title//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]"/>
                        </xsl:when>
                        <xsl:when test="sec/title">
                            <xsl:value-of select="sec[1]/title[1]//text()[not(ancestor::named-content[@content-type='page'] or ancestor::xref)]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="name()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <xsl:attribute name="alphasort" select="cup:alphaSortString(string-join($chunkTitle,' '))"/>
                <xsl:value-of select="$chunkTitle"/>
                
                <!-- For ILR/ICSID case reports, check if there are any silbing cases contested by exactly the same two parties 
                     (although the role of claimant and dedendant can be reversed), and if there are add the court or stage-name
                     to the content-item's title. This minimizes the chance of multiple identically-named links in CLR's volume 
                     navigation. -->
                <xsl:if test="$bookType = 'ilr' and @book-part-type='case-report'">
                    <xsl:variable name="parties" as="xs:string*" select="(body/sec[@sec-type='casehead'][1]//named-content[@content-type='claimant' or @content-type='defendant']/string-join(.//text()[not(ancestor::xref) or ancestor::named-content[@content-type='page']], ''))[position() le 2]"/>
                    <xsl:variable name="siblingCases" as="element(book-part)*" select="(preceding-sibling::book-part[@book-part-type='case-report'], following-sibling::book-part[@book-part-type='case-report'])"/>
                    <xsl:if test="
                        count($parties) = 2 
                        and count($siblingCases) gt 0 
                        and $siblingCases/body/sec[@sec-type='casehead'][1][
                            (
                                .//named-content[@content-type='claimant'] = $parties[1]
                                or .//named-content[@content-type='defendant'] = $parties[1]
                            )
                            and
                            (
                                .//named-content[@content-type='claimant'] = $parties[2]
                                or .//named-content[@content-type='defendant'] = $parties[2]
                            )]">
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="
                            normalize-space(
                                string-join(
                                    body/sec[@sec-type='casehead'][1]/p[@content-type='court' or @content-type='stage-name'][1]//text()[not(ancestor::xref or ancestor::named-content[@content-type='law-date' or @content-type='page'])]
                                    , '')
                            )"/>               
                    </xsl:if>
                </xsl:if>
                
            </title>
        </heading>
    </xsl:template>

    <xsl:function name="cup:alphaSortString">
        <xsl:param as="xs:string" name="in"/>
        <xsl:variable as="xs:string" name="out">
            <xsl:analyze-string regex="^((The|A) )(.+)$" select="$in">
                <xsl:matching-substring>
                    <xsl:value-of select="concat(regex-group(3),', ',regex-group(2))"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:value-of select="$in"/>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:value-of select="normalize-unicode($out,'NFC')"/>
    </xsl:function>

</xsl:stylesheet>