<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cup cbml xlink xs xd m" version="2.0" xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../functions/functions.xsl"/>
    <xsl:variable name="image-path" select="''"/>

    <!--    
    Acts on BITS file
    Lists all images to go in the opf file
    Decisions as to which elements will be images done here
    -->

    <xsl:output indent="yes"/>
    <xsl:param as="xs:string" name="isbn"/>

    <xsl:template match="/">
        <images>
            <xsl:apply-templates select="descendant::graphic | descendant::inline-graphic"/>
            <!--may need to also add altimg for mathml if not done as graphic-->
        </images>
    </xsl:template>

    <xsl:template match="graphic">
        <xsl:variable name="link" select="@xlink:href"/>
        <xsl:choose>
            <xsl:when test="parent::alternatives/table"/>
            <!--no table images when there is an alternative-->
            <xsl:when test="preceding::graphic[@xlink:href=$link]"/>
            <!--avoid duplicates-->
            <xsl:otherwise>
                <image file="{concat(@xlink:href,'.',@mime-subtype)}" type="{@mime-subtype}" resize="yes"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="inline-graphic">
        <xsl:variable name="link" select="@xlink:href"/>
        <xsl:choose>
            <xsl:when test="preceding::inline-graphic[@xlink:href=$link]"/>
            <!--avoid duplicates-->
            <xsl:otherwise>
                <image file="{concat(@xlink:href,'.',@mime-subtype)}" type="{@mime-subtype}" resize="no"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>





</xsl:stylesheet>
