<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" indent="yes"></xsl:output>
    <!--<xsl:variable name="path" select="string('/$images'"></xsl:variable>-->
    
    <xsl:template match="/">
        <xsl:apply-templates select="images"/>
    </xsl:template>
    
    <xsl:template match="images">
        <project basedir="../" default="move-images" name="move-images">
            <target name="move-images">
                <xsl:apply-templates select="image[@resize='yes']"/>
            </target>
        </project>
        <xsl:result-document href="move-images2.xml">
            <project basedir="../" default="move-images" name="move-images">
                <target name="move-images">
                    <xsl:apply-templates select="image[@resize='no']"/>
                </target>
            </project>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="image[not(matches(@file,'logo\.(gif|png)'))]">
        <copy file="{concat('${input}/Images/' , @file)}" tofile="{concat('${ant.refid:working.dir}/${id}/OEBPS/Images/' , @file)}" failonerror="no"/>
    </xsl:template>
    
<!--    <xsl:template match="image[matches(@file,'logo\.(gif|png)')]">
        <copy file="{concat('${input}/Images/' , 'logo.png')}" tofile="{concat('${ant.refid:working.dir}/${id}/OEBPS/Images/' , 'logo.png')}" failonerror="no"/>
    </xsl:template>   -->

</xsl:stylesheet>