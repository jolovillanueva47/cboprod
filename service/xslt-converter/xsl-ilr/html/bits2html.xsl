<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">

<!--dependencies - valid CUP-BITS file -->
<!--         -->
<!-- 
    DEPENDENCY: valid CUP-BITS file processed by add-filenames.xsl
                which adds @chunk  and @filename to every element that is to be the root of an individual html file
                and @filename to every element with attribute id or rid
    PURPOSE: creates  html files for CUP-BITS content
    the decision as to what is to be a separate html fiel was made at the add-filenames.xsl stage
    LOOKUP: span-div.xml - list of which elements to be div and which span if called by the default template
    INPUT PARAMETERS: 
    USES FUNCTIONS:
    COMMENTS: default   - assigns each element to a div or a span
                        - attribute class is either element name or elem-name_-type-attribute-name
    PIPELINE: precede by add-filenames.xsl
    DECSIONS:
        class attribute - element name, value of type attribute, separated by space; if elements nest (eg sec) element name to be numbered with the top level being 1
        default - each element is div or span according to lookup
        paragraphs to be div not p
        def-list to be dl
    OVER-RIDES:
        This file to be over-ridden for each output type - cbo2html, bits2epub, etc.
    -->
    
    
    <xsl:import href="include/elements.xsl"/>
    <!--  include the functions  -->
<!--    <xsl:include href="../functions/functions.xsl"/> 
    <xsl:include href="include/table.xsl"/> 
    <xsl:include href="include/lists.xsl"/>
    <xsl:include href="include/index.xsl"/>
    <xsl:include href="include/images.xsl"/>
    <xsl:include href="include/headings.xsl"/>-->
   
    
    <xsl:variable name="css-path" select="'css/'"/>
    <xsl:variable name="book-title" select="/book/book-meta/book-title-group/book-title"/>
    <xsl:variable name="volume" select="/book/collection-meta/volume-in-collection/volume-number"/>
    

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:template match="book">
        <xsl:apply-templates select="front-matter | book-body | book-back"/>
    </xsl:template>

<!-- This seems superfluous as the HTML for each chunk is already done in cbo2html.xsl. But maybe it is for BITS2EPUB?
     <xsl:template match="*[@filename and @chunk]">
        <xsl:if test="not(@filename = preceding-sibling::*/@filename)">
            <xsl:result-document href="{@filename}">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta charset="UTF-8"/>
                        <link href="{concat($css-path, 'cup-bits.css')}" rel="stylesheet" type="text/css"></link> 
                        <title><xsl:value-of select="$book-title"/></title>
                    </head>
                    <body>
                        <xsl:apply-templates select="." mode="content"/>
                        <xsl:apply-templates select="following-sibling::*[@filename = current()/@filename]" mode="content"/>}}}
                    </body>
                </html>
            </xsl:result-document>
        </xsl:if>
    </xsl:template>-->
    
<!--    chunking in cbo is driven by the header file  -->
    <xsl:template match="*" mode="cbo">
         <xsl:apply-templates select="." mode="content"/>                     
    </xsl:template>
    
  
</xsl:stylesheet>
