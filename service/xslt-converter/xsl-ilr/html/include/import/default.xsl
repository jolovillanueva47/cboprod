<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
<!--    DEFAULT TEMPLATES
        every element - assigns span or div according to lookup span-div.xml using function cup:SpanDiv
        every element with @rid - assigns span (IS THAT OK?) and creates link
        every element without '-type' attribute - adds attribute class = the element name
        every element with '-type' attribute - adds attribute class = the element name '_' the -type attribute value
        MODE TEMPLATES
        mode id - copies the id attribute over
    -->

<!-- =========== DEFAULT ELEMENT TEMPLATE - assigns div or span ============-->

    <xsl:template match="*[@rid]">
           <span>
            <xsl:apply-templates select="." mode="class"/>
                    <xsl:variable name="link-att" select="@rid | @xlink:href" as="xs:string"/>
                    <a>
                        <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/> 
                        <xsl:apply-templates select="." mode="id"/>
                        <xsl:apply-templates/>  
                    </a>
               </span>
    </xsl:template>
    
    <!-- =========== DEFAULT TEMPLATE FOR LINKS - assigns  span  and adds link ============-->    
    <xsl:template match="*[not(@rid )]" mode="#default content"> 
        <xsl:variable name="elem" select="name(.)"/>
        <xsl:variable name="nm" select="cup:SpanDiv($elem)"/>
        <xsl:variable name="nm2">
            <xsl:choose>
                <xsl:when test="cup:SpanDiv($elem)=''">TODO</xsl:when>
                <xsl:otherwise><xsl:value-of select="cup:SpanDiv($elem)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:element name="{$nm2}">
                <xsl:apply-templates select="." mode="id"/>
                <xsl:apply-templates select="." mode="class"/>
                <xsl:apply-templates select="text()| *[not(@position='float')]"/>
        </xsl:element>
        <xsl:apply-templates select="*[@position='float']"/>
    </xsl:template>


<!-- ============== DEFAULT CLASS TEMPLATE - name of element assigned to class ===========-->
<!--  DECISION:  a combination of element name and type attribute, separated by space -->
    <xsl:template match="*" mode="class">
        <xsl:variable name="elem-name" select="name()"/>
        <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="parent::*[name()=$elem-name] or child::*[name()=$elem-name]">
                    <xsl:variable name="num" select="count(ancestor::*[local-name()=$elem-name])+1"/>
                    <xsl:choose>
                        <xsl:when test="cup:isFootnoteMarker(.)">
                            <xsl:value-of
                                select="concat(name(.),$num,' fn')"/>
                        </xsl:when>
                        <xsl:when test="@*[ends-with(name(), '-type')]">
                            <xsl:value-of
                                select="concat(name(.),$num,' ',@*[ends-with(name(), '-type')])"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat(name(.), $num)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="cup:isFootnoteMarker(.)">
                            <xsl:value-of
                                select="concat(name(.),' fn')"/>
                        </xsl:when>
                        <xsl:when test="@*[ends-with(name(), '-type')]">
                            <xsl:value-of
                                select="concat(name(.),' ',@*[ends-with(name(), '-type')])"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="name(.)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>
    
    <!-- ============== DEFAULT PARAGRAPH TEMPLATE  ===========-->

    <xsl:template match="p[not(@content-type)]" priority="2">
        <div>
            <xsl:attribute name="class">
                <xsl:copy-of select="@id"/>
                <xsl:choose>
                    <xsl:when test="not(preceding-sibling::p)">p first</xsl:when>
                    <xsl:otherwise>p</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    
    <!-- ============== DEFAULT id TEMPLATE  ===========-->
    <!-- add attribute id -->
    <xsl:template match="*" mode="id">
        <xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
    </xsl:template>



</xsl:stylesheet>
