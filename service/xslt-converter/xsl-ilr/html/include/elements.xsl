<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:cup="http://contentservices.cambridge.org"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                exclude-result-prefixes="#all"
                version="2.0">

            <!--templates, in alphabetical order , for BITS elements-->
<!--    BEWARE - THIS WAS CREATED FROM THE XSD AND STILL CONTAINS SPACES FOR ELEMENTS NOT IN BITS OR NOT USED IN CUP-BITS-->
    
<!--             
        1. templates with no content will be dealt with by the default
        2. templates for items containing more than one element to be listed under the main item, e/g def, term, listed under def-list
            in these cases listing under these elements (e.g. def and term) will be a comment indicating where it is listed , e.g.  - see def-list -
        3. these templates will be over-ridden by templates specifically for cbo, for epub, for different book streams
        4. mode templates to be in a separate file
    -->
     <xsl:import href="import/default.xsl"/>          
    <xsl:include href="../../functions/functions.xsl"/> 
     
     <xsl:output method="html" indent="no"/>
            
        <!--=============== ABBREV ===================-->
               
        
            
        <!--=============== ABBREV-JOURNAL-TITLE ===================-->
               
        
            
        <!--=============== ABSTRACT ===================-->
               
        
            
        <!--=============== ACCESS-DATE ===================-->
               
        
            
        <!--=============== ACK ===================-->
               
        
            
        <!--=============== ADDR-LINE ===================-->
               
        
            
        <!--=============== ADDRESS ===================-->
               
        
            
        <!--=============== AFF ===================-->
               
        
            
        <!--=============== AFF-ALTERNATIVES ===================-->
               
        
            
        <!--=============== ALT-TEXT ===================-->
               
        
            
        <!--=============== ALT-TITLE ===================-->
               
        
            
        <!--=============== ALTERNATIVES ===================-->
               
        
            
        <!--=============== ANNOTATION ===================-->
               
        
            
        <!--=============== ANONYMOUS ===================-->
               
        
            
        <!--=============== ANSWER ===================-->
               
        
            
        <!--=============== ANSWER-SET ===================-->
               
        
            
        <!--=============== APP ===================-->
               
        
            
        <!--=============== APP-GROUP ===================-->
               
        
            
        <!--=============== ARRAY ===================-->
    <xsl:template match="array">
        <div class="array">
            <xsl:apply-templates select="node()[not(self::tbody or self::attrib or self::permissions)]"/>
            <xsl:if test="child::tbody">
                <table>
                    <xsl:copy-of select="@id"/>

                    <xsl:apply-templates  select="tbody"/>
                </table>
            </xsl:if>
            <xsl:apply-templates  select="attrib | permissions"/>
        </div>
    </xsl:template> 
    
    <xsl:template match="alternatives[array and graphic]">
        <xsl:apply-templates select="array"/>
    </xsl:template>
        
            
        <!--=============== ARTICLE-CATEGORIES ===================-->
               
        
            
        <!--=============== ARTICLE-ID ===================-->
               
        
            
        <!--=============== ARTICLE-META ===================-->
               
        
            
        <!--=============== ARTICLE-TITLE ===================-->
               
        
            
        <!--=============== ATTRIB ===================-->
               
        
            
        <!--=============== AUTHOR-COMMENT ===================-->
               
        
            
        <!--=============== AUTHOR-NOTES ===================-->
               
        
            
        <!--=============== AWARD-GROUP ===================-->
               
        
            
        <!--=============== AWARD-ID ===================-->
               
        
            
        <!--=============== BACK ===================-->
               
        
            
        <!--=============== BIO ===================-->
               
        
            
        <!--=============== BODY ===================-->
               
        
            
        <!--=============== BOLD ===================-->

        
            
        <!--=============== BOOK ===================-->
               
        
            
        <!--=============== BOOK-BACK ===================-->
               
        
            
        <!--=============== BOOK-BODY ===================-->
               
        
            
        <!--=============== BOOK-ID ===================-->
               
        
            
        <!--=============== BOOK-META ===================-->
               
        
            
        <!--=============== BOOK-PART ===================-->
               
        
            
        <!--=============== BOOK-PART-ID ===================-->
               
        
            
        <!--=============== BOOK-PART-META ===================-->
               
        
            
        <!--=============== BOOK-PART-WRAPPER ===================-->
               
        
            
        <!--=============== BOOK-TITLE ===================-->
               
        
            
        <!--=============== BOOK-TITLE-GROUP ===================-->
               
        
            
        <!--=============== BOOK-VOLUME-ID ===================-->
               
        
            
        <!--=============== BOOK-VOLUME-NUMBER ===================-->
               
        
            
        <!--=============== BOXED-TEXT ===================-->
               
        
            
        <!--=============== BREAK ===================-->
               
        
            
        <!--=============== CAPTION ===================-->
               
        
            
        <!--=============== CHAPTER-TITLE ===================-->
               
        
            
        <!--=============== CHEM-STRUCT ===================-->
               
        
            
        <!--=============== CHEM-STRUCT-WRAP ===================-->
               
        
            
        <!--=============== CITATION-ALTERNATIVES ===================-->
               
        
            
        <!--=============== CODE ===================-->
               
        
            
        <!--=============== COL ===================-->
               
        
            
        <!--=============== COLGROUP ===================-->
               
        
            
        <!--=============== COLLAB ===================-->
               
        
            
        <!--=============== COLLAB-ALTERNATIVES ===================-->
               
        
            
        <!--=============== COLLECTION-ID ===================-->
               
        
            
        <!--=============== COLLECTION-META ===================-->
               
        
            
        <!--=============== COMMENT ===================-->
               
        
            
        <!--=============== COMPOUND-KWD ===================-->
               
        
            
        <!--=============== COMPOUND-KWD-PART ===================-->
               
        
            
        <!--=============== COMPOUND-SUBJECT ===================-->
               
        
            
        <!--=============== COMPOUND-SUBJECT-PART ===================-->
               
        
            
        <!--=============== CONF-ACRONYM ===================-->
               
        
            
        <!--=============== CONF-DATE ===================-->
               
        
            
        <!--=============== CONF-LOC ===================-->
               
        
            
        <!--=============== CONF-NAME ===================-->
               
        
            
        <!--=============== CONF-NUM ===================-->
               
        
            
        <!--=============== CONF-SPONSOR ===================-->
               
        
            
        <!--=============== CONF-THEME ===================-->
               
        
            
        <!--=============== CONFERENCE ===================-->
               
        
            
        <!--=============== CONTRIB ===================-->
               
        
            
        <!--=============== CONTRIB-GROUP ===================-->
               
        
            
        <!--=============== CONTRIB-ID ===================-->
               
        
            
        <!--=============== COPYRIGHT-HOLDER ===================-->
               
        
            
        <!--=============== COPYRIGHT-STATEMENT ===================-->
               
        
            
        <!--=============== COPYRIGHT-YEAR ===================-->
               
        
            
        <!--=============== CORRESP ===================-->
               
        
            
        <!--=============== COUNT ===================-->
               
        
            
        <!--=============== COUNTRY ===================-->
               
        
            
        <!--=============== COUNTS ===================-->
               
        
            
        <!--=============== CUSTOM-META ===================-->
               
        
            
        <!--=============== CUSTOM-META-GROUP ===================-->
               
        
            
        <!--=============== DATE ===================-->
               
        
            
        <!--=============== DATE-IN-CITATION ===================-->
               
        
            
        <!--=============== DAY ===================-->
               
        
            
        <!--=============== DEDICATION ===================-->
               
        
            
        <!--=============== DEF ===================-->
               
<!--        see def-list-->
            
        <!--=============== DEF-HEAD ===================-->
               
        
            
        <!--=============== DEF-ITEM ===================-->
               
<!--        see def-list-->
            
        <!--=============== DEF-LIST ===================-->
    
    <xsl:template match="def-list">
        <xsl:apply-templates select="label|title"/>
        <dl>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="*[not(self::label or self::title)]"/>
        </dl>
    </xsl:template>
    
    
    <xsl:template match="def-item">
        <xsl:if test="not(term)">
            <dt/>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--    BEWARE - need to handle id of term-->    
    <xsl:template match="term">
        <dt>
            <xsl:apply-templates select="parent::def-item" mode="id"/>
            <xsl:apply-templates/>
        </dt>
    </xsl:template>
    
    <xsl:template match="def">
        <dd>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </dd>
    </xsl:template>
               
        
            
        <!--=============== DEGREES ===================-->
               
        
            
        <!--=============== DISP-FORMULA ===================-->
               
        
            
        <!--=============== DISP-FORMULA-GROUP ===================-->
               
        
            
        <!--=============== DISP-QUOTE ===================-->
               
        
            
        <!--=============== EDITION ===================-->
               
        
            
        <!--=============== ELEMENT-CITATION ===================-->
               
        
            
        <!--=============== ELOCATION-ID ===================-->
               
        
            
        <!--=============== EMAIL ===================-->
               
        
            
        <!--=============== EQUATION-COUNT ===================-->
               
        
            
        <!--=============== ERA ===================-->
               
        
            
        <!--=============== ETAL ===================-->
               
        
            
        <!--=============== EVENT ===================-->
               
        
            
        <!--=============== EVENT-DESC ===================-->
               
        
            
        <!--=============== EXPLANATION ===================-->
               
        
            
        <!--=============== EXT-LINK ===================-->
               
        
            
        <!--=============== FAX ===================-->
               
        
            
        <!--=============== FIG ===================-->
               
        
            
        <!--=============== FIG-COUNT ===================-->
               
        
            
        <!--=============== FIG-GROUP ===================-->
               
        
            
        <!--=============== FIXED-CASE ===================-->
               
        
            
        <!--=============== FLOATS-GROUP ===================-->
               
        
            
        <!--=============== FN ===================-->
               
        
            
        <!--=============== FN-GROUP ===================-->
               
        
            
        <!--=============== FOREWORD ===================-->
               
        
            
        <!--=============== FPAGE ===================-->
               
        
            
        <!--=============== FRONT-MATTER ===================-->
               
        
            
        <!--=============== FRONT-MATTER-PART ===================-->
               
        
            
        <!--=============== FUNDING-GROUP ===================-->
               
        
            
        <!--=============== FUNDING-SOURCE ===================-->
               
        
            
        <!--=============== FUNDING-STATEMENT ===================-->
               
        
            
        <!--=============== GIVEN-NAMES ===================-->
               
        
            
        <!--=============== GLOSSARY ===================-->
               
        
            
        <!--=============== GLYPH-DATA ===================-->
               
        
            
        <!--=============== GLYPH-REF ===================-->
               
        
            
        <!--=============== GOV ===================-->
               
        
            
        <!--=============== GRAPHIC ===================-->
    
    <!--
			CBT-742, DPB-8410 01.20.2016 JBA
			* addition of attribute alt on element img for CUP logo
    -->
    <xsl:template match="graphic">
        <div class="graphic">
            <img src="{concat($image-path, @xlink:href, '.', @mime-subtype)}" alt="{if (@content-type = 'logo') then 'Cambridge University Press' else ''}"/>
        </div>
    </xsl:template>
    
    <xsl:template match="inline-graphic">
        <span class="inline-graphic">
            <img src="{concat($image-path, @xlink:href, '.', @mime-subtype)}"/>
        </span>
    </xsl:template>      
        
            
        <!--=============== HISTORY ===================-->
               
        
            
        <!--=============== HR ===================-->
               
        
            
        <!--=============== INDEX ===================-->
               
        
            
        <!--=============== INDEX-DIV ===================-->
               
        
            
        <!--=============== INDEX-ENTRY ===================-->
               
    <!--to distinguish from term in def-list-->
    <xsl:template match="index-entry/term">
        <span class="term">
            <xsl:apply-templates/>
        </span>
    </xsl:template>  
            
        <!--=============== INDEX-GROUP ===================-->
               
        
            
        <!--=============== INDEX-TERM ===================-->
               
        
            
        <!--=============== INDEX-TERM-RANGE-END ===================-->
               
        
            
        <!--=============== INLINE-FORMULA ===================-->
               
        
            
        <!--=============== INLINE-GRAPHIC ===================-->
               
<!--        see graphic-->
            
        <!--=============== INLINE-SUPPLEMENTARY-MATERIAL ===================-->
               
        
            
        <!--=============== INSTITUTION ===================-->
               
        
            
        <!--=============== INSTITUTION-ID ===================-->
               
        
            
        <!--=============== INSTITUTION-WRAP ===================-->
               
        
            
        <!--=============== ISBN ===================-->
               
        
            
        <!--=============== ISSN ===================-->
               
        
            
        <!--=============== ISSN-L ===================-->
               
        
            
        <!--=============== ISSUE ===================-->
               
        
            
        <!--=============== ISSUE-ID ===================-->
               
        
            
        <!--=============== ISSUE-PART ===================-->
               
        
            
        <!--=============== ISSUE-SPONSOR ===================-->
               
        
            
        <!--=============== ISSUE-TITLE ===================-->
               
        
            
        <!--=============== ITALIC ===================-->
               
        
            
        <!--=============== JOURNAL-ID ===================-->
               
        
            
        <!--=============== JOURNAL-META ===================-->
               
        
            
        <!--=============== JOURNAL-SUBTITLE ===================-->
               
        
            
        <!--=============== JOURNAL-TITLE ===================-->
               
        
            
        <!--=============== JOURNAL-TITLE-GROUP ===================-->
               
        
            
        <!--=============== KWD ===================-->
               
        
            
        <!--=============== KWD-GROUP ===================-->
               
        
            
        <!--=============== LABEL ===================-->
               
        
            
        <!--=============== LICENSE ===================-->
               
        
            
        <!--=============== LICENSE-P ===================-->
               
        
            
        <!--=============== LIST ===================-->
    <!--numbered paragraphs-->
    <xsl:template match="list[@list-content='paragraph']">
        <xsl:variable name="depth" as="xs:integer" select="count(ancestor::list[@list-content='paragraph'])"/>
        <xsl:choose>
            <xsl:when test="$depth = 0">
                <div class="numbered-paragraphs">
                    <xsl:apply-templates/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <xsl:template match="list[@list-content='paragraph']/list-item">
        <xsl:apply-templates select="label/named-content[@content-type='page']" mode="page"/>
        <div class="p numbered">
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    
    
    
    <xsl:template match="list[@list-content='paragraph']/list-item/label/named-content[@content-type='page']"></xsl:template>
    
    <xsl:template match="list[@list-content='paragraph']/list-item/p[1]">
        <xsl:apply-templates/>
    </xsl:template>                
        
        
    <xsl:template match="list[@list-content='paragraph']/list-item/label">
        <xsl:variable name="depth" as="xs:integer" select="count(ancestor::list[@list-content='paragraph'])"/>
        <span>
            <xsl:attribute name="class">
                <xsl:text>label</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
        <xsl:text> </xsl:text>
    </xsl:template>
            
        <!--=============== LIST-ITEM ===================-->
               
<!--  see list      -->
            
        <!--=============== LONG-DESC ===================-->
               
        
            
        <!--=============== LPAGE ===================-->
    
    <!--    =============== MATH ================== -->
    
    <xsl:template match="mml:*">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
                   
        <!--=============== MEDIA ===================-->
               
        
            
        <!--=============== META-NAME ===================-->
               
        
            
        <!--=============== META-VALUE ===================-->
               
        
            
        <!--=============== MILESTONE-END ===================-->
               
        
            
        <!--=============== MILESTONE-START ===================-->
               
        
            
        <!--=============== MIXED-CITATION ===================-->
               
        
            
        <!--=============== MONOSPACE ===================-->
               
        
            
        <!--=============== MONTH ===================-->
               
        
            
        <!--=============== NAME ===================-->
             <xsl:template match="mixed-citation/name">
                 <span class="name">
                     <xsl:apply-templates/>
                 </span>
             </xsl:template>  
        
            
        <!--=============== NAME-ALTERNATIVES ===================-->
               
        
            
        <!--=============== NAMED-BOOK-PART-BODY ===================-->
    <xsl:template match="named-book-part-body">
        <div class="{local-name()}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

        <!--=============== NAMED-CONTENT ===================-->
               
        
            
        <!--=============== NAV-POINTER ===================-->
    <xsl:template match="nav-pointer">
        <span class="nav-pointer"><a href="{cup:Link(. , @rid)}"><xsl:apply-templates/></a>  </span> <xsl:if test="parent::nav-pointer-group and @nav-pointer-type='start-of-range'"><xsl:text>&#x2013;</xsl:text></xsl:if>
        <xsl:if test="not(parent::nav-pointer-group) and (following-sibling::nav-pointer or following-sibling::nav-pointer-group)"><xsl:text>, </xsl:text></xsl:if>
    </xsl:template>       
        <!--=============== NAV-POINTER-GROUP ===================-->
               
    <xsl:template match="nav-pointer-group">
        <span class="nav-pointer-group">      
            <xsl:apply-templates/>
        </span> 
        <xsl:if test="following-sibling::nav-pointer or following-sibling::nav-pointer-group">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template> 
            
        <!--=============== NESTED-KWD ===================-->
               
        
            
        <!--=============== NLM-CITATION ===================-->
               
        
            
        <!--=============== NOTE ===================-->
               
        
            
        <!--=============== NOTES ===================-->
               
        
            
        <!--=============== OBJECT-ID ===================-->
               
        
            
        <!--=============== ON-BEHALF-OF ===================-->
               
        
            
        <!--=============== OPEN-ACCESS ===================-->
               
        
            
        <!--=============== OVERLINE ===================-->
               
        
            
        <!--=============== OVERLINE-END ===================-->
               
        
            
        <!--=============== OVERLINE-START ===================-->
               
        
            
        <!--=============== P ===================-->
               
        
            
        <!--=============== PAGE-COUNT ===================-->
               
        
            
        <!--=============== PAGE-RANGE ===================-->
               
        
            
        <!--=============== PART-TITLE ===================-->
               
        
            
        <!--=============== PATENT ===================-->
               
        
            
        <!--=============== PERMISSIONS ===================-->
               
        
            
        <!--=============== PERSON-GROUP ===================-->
               
        
            
        <!--=============== PHONE ===================-->
               
        
            
        <!--=============== PREFACE ===================-->
               
        
            
        <!--=============== PREFIX ===================-->
               
        
            
        <!--=============== PREFORMAT ===================-->
               
        
            
        <!--=============== PRICE ===================-->
               
        
            
        <!--=============== PRINCIPAL-AWARD-RECIPIENT ===================-->
               
        
            
        <!--=============== PRINCIPAL-INVESTIGATOR ===================-->
               
        
            
        <!--=============== PRIVATE-CHAR ===================-->
               
        
            
        <!--=============== PRODUCT ===================-->
               
        
            
        <!--=============== PUB-DATE ===================-->
               
        
            
        <!--=============== PUB-HISTORY ===================-->
               
        
            
        <!--=============== PUB-ID ===================-->
               
        
            
        <!--=============== PUBLISHER ===================-->
               
        
            
        <!--=============== PUBLISHER-LOC ===================-->
               
        
            
        <!--=============== PUBLISHER-NAME ===================-->
               
        
            
        <!--=============== QUESTION ===================-->
               
        
            
        <!--=============== QUESTION-WRAP ===================-->
               
        
            
        <!--=============== RB ===================-->
               
        
            
        <!--=============== REF ===================-->
               
        
            
        <!--=============== REF-COUNT ===================-->
               
        
            
        <!--=============== REF-LIST ===================-->
               
        
            
        <!--=============== RELATED-ARTICLE ===================-->
               
        
            
        <!--=============== RELATED-OBJECT ===================-->
               
        
            
        <!--=============== ROLE ===================-->
               
        
            
        <!--=============== ROMAN ===================-->
               
        
            
        <!--=============== RP ===================-->
               
        
            
        <!--=============== RT ===================-->
               
        
            
        <!--=============== RUBY ===================-->
               
        
            
        <!--=============== SANS-SERIF ===================-->
               
        
            
        <!--=============== SC ===================-->
               
        
            
        <!--=============== SEASON ===================-->
               
        
            
        <!--=============== SEC ===================-->
               
    <xsl:template match="sec/title">
        <xsl:element name="{cup:HeadingType(parent::*)}">
            <xsl:attribute name="class"/>
            <xsl:apply-templates select="preceding-sibling::label" mode="do-label"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="label" mode="do-label">
        <span class="label">
            <xsl:apply-templates/>
        </span>
        <xsl:text> </xsl:text>
    </xsl:template>
    
    <xsl:template match="sec/label">
        <xsl:choose>
            <xsl:when test="following-sibling::title"></xsl:when>
            <xsl:otherwise>
                <div class="label">
                    <xsl:apply-templates/>
                </div>               
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>      
            
        <!--=============== SEC-META ===================-->
               
        
            
        <!--=============== SEE ===================-->
               
        
            
        <!--=============== SEE-ALSO ===================-->
               
        
            
        <!--=============== SEE-ALSO-ENTRY ===================-->
               
        
            
        <!--=============== SEE-ENTRY ===================-->
               
        
            
        <!--=============== SELF-URI ===================-->
               
        
            
        <!--=============== SERIES ===================-->
               
        
            
        <!--=============== SERIES-TEXT ===================-->
               
        
            
        <!--=============== SERIES-TITLE ===================-->
               
        
            
        <!--=============== SIG ===================-->
               
        
            
        <!--=============== SIG-BLOCK ===================-->
               
        
            
        <!--=============== SIZE ===================-->
               
        
            
        <!--=============== SOURCE ===================-->
               
        
            
        <!--=============== SPEAKER ===================-->
               
        
            
        <!--=============== SPEECH ===================-->
               
        
            
        <!--=============== STATEMENT ===================-->
               
        
            
        <!--=============== STD ===================-->
               
        
            
        <!--=============== STD-ORGANIZATION ===================-->
               
        
            
        <!--=============== STRIKE ===================-->
               
        
            
        <!--=============== STRING-CONF ===================-->
               
        
            
        <!--=============== STRING-DATE ===================-->
               
        
            
        <!--=============== STRING-NAME ===================-->
               
        
            
        <!--=============== STYLED-CONTENT ===================-->
               
        
            
        <!--=============== SUB ===================-->
               
    <xsl:template match="sub |sup">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
            
        <!--=============== SUBJ-GROUP ===================-->
               
        
            
        <!--=============== SUBJECT ===================-->
               
        
            
        <!--=============== SUBTITLE ===================-->
               
        
            
        <!--=============== SUFFIX ===================-->
               
        
            
        <!--=============== SUP ===================-->
               
<!--        see sub -->
            
        <!--=============== SUPPLEMENT ===================-->
               
        
            
        <!--=============== SUPPLEMENTARY-MATERIAL ===================-->
               
        
            
        <!--=============== SURNAME ===================-->
               
        
            
        <!--=============== TABLE ===================-->
               
    <xsl:template match="table | td | tr | th |thead | tbody">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="table/@* |td/@* |th/@* | tr/@* | thead/@* | tbody/@*">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="table/@frame" priority="2">
<!--        to do -->
        <!--<xsl:attribute name="border" select="."/>-->
    </xsl:template>
            
        <!--=============== TABLE-COUNT ===================-->
               
        
            
        <!--=============== TABLE-WRAP ===================-->
          <xsl:template match="table-wrap/alternatives[table]">
              <xsl:element name="{local-name()}">
                  <xsl:apply-templates select="@*"/>
                  <xsl:apply-templates select="table"/>
              </xsl:element>
          </xsl:template>     
        
            
        <!--=============== TABLE-WRAP-FOOT ===================-->
               
        
            
        <!--=============== TABLE-WRAP-GROUP ===================-->
               
        
            
        <!--=============== TARGET ===================-->
               
        
            
        <!--=============== TBODY ===================-->
               
    <!--        see table -->       
            
        <!--=============== TD ===================-->
               
<!--        see table -->
            
        <!--=============== TERM ===================-->
               
<!--        see def-list-->
            
        <!--=============== TERM-HEAD ===================-->
               
        
            
        <!--=============== TEX-MATH ===================-->
               
        
            
        <!--=============== TEXTUAL-FORM ===================-->
               
        
            
        <!--=============== TFOOT ===================-->
               
        
            
        <!--=============== TH ===================-->
    <!--        see table -->              
        
            
        <!--=============== THEAD ===================-->
               
    <!--        see table -->       
            
        <!--=============== TIME-STAMP ===================-->
               
        
            
        <!--=============== TITLE ===================-->
               
        
            
        <!--=============== TITLE-GROUP ===================-->
               
        
            
        <!--=============== TOC ===================-->
               
        
            
        <!--=============== TOC-DIV ===================-->
               
        
            
        <!--=============== TOC-ENTRY ===================-->
               
        
            
        <!--=============== TOC-GROUP ===================-->
               
        
            
        <!--=============== TR ===================-->
               
    <!--        see table -->       
            
        <!--=============== TRANS-ABSTRACT ===================-->
               
        
            
        <!--=============== TRANS-SOURCE ===================-->
               
        
            
        <!--=============== TRANS-SUBTITLE ===================-->
               
        
            
        <!--=============== TRANS-TITLE ===================-->
               
        
            
        <!--=============== TRANS-TITLE-GROUP ===================-->
               
        
            
        <!--=============== UNDERLINE ===================-->
               
        
            
        <!--=============== UNDERLINE-END ===================-->
               
        
            
        <!--=============== UNDERLINE-START ===================-->
               
        
            
        <!--=============== UNSTRUCTURED-KWD-GROUP ===================-->
               
        
            
        <!--=============== URI ===================-->
             <xsl:template match="uri">
                 <a class="uri" href="{@xlink:href}"><xsl:apply-templates/></a>
             </xsl:template>  
        
            
        <!--=============== VERSE-GROUP ===================-->
               
        
            
        <!--=============== VERSE-LINE ===================-->
               
        
            
        <!--=============== VOLUME ===================-->
               
        
            
        <!--=============== VOLUME-ID ===================-->
               
        
            
        <!--=============== VOLUME-IN-COLLECTION ===================-->
               
        
            
        <!--=============== VOLUME-NUMBER ===================-->
               
        
            
        <!--=============== VOLUME-SERIES ===================-->
               
        
            
        <!--=============== VOLUME-TITLE ===================-->
               
        
            
        <!--=============== WORD-COUNT ===================-->
               
        
            
        <!--=============== X ===================-->
               
        
            
        <!--=============== XREF ===================-->
               
        
            
        <!--=============== YEAR ===================-->
               
        </xsl:stylesheet>
