<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="#all" version="2.0">

<!-- templates, in alphabetical order , for BITS elements-->
   
    
    <!-- ======================  DEF-LIST ===================== -->
    
    <xsl:template match="def-list">
        <xsl:apply-templates select="label|title"/>
        <dl>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="*[not(self::label or self::title)]"/>
        </dl>
    </xsl:template>
    
    
    <xsl:template match="def-item">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--    BEWARE - need to handle id of term-->    
    <xsl:template match="term">
        <dt>
            <xsl:apply-templates select="parent::def-item" mode="id"/>
            <xsl:apply-templates/>
        </dt>
    </xsl:template>
    
    <xsl:template match="def">
        <dd>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </dd>
    </xsl:template>
    
<!--  =================== GRAPHIC  ================-->
    <xsl:template match="graphic">
        <div class="graphic">
            <img src="{concat('Images/', @xlink:href, '.', @mime-subtype)}"/>
        </div>
    </xsl:template>
    
    <xsl:template match="inline-graphic">
        <span class="inline-graphic">
            <img src="{concat('Images/', @xlink:href, '.', @mime-subtype)}"/>
        </span>
    </xsl:template>
    
<!--    ================== INDEX-ENTRY ==============-->
    
    <!--to distinguish from term in def-list-->
    <xsl:template match="index-entry/term">
        <span class="term">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
<!-- ===============   INLINE-GRAPHIC ============== -->
    
<!--    see under graphic   -->
    
 
<!--  ================ LIST =============-->
    <!--numbered paragraphs-->
    <xsl:template match="list[@list-content='paragraph']">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="list[@list-content='paragraph']/list-item">
        <div class="p numbered">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="list[@list-content='paragraph']/list-item/p">
        <xsl:apply-templates/>
    </xsl:template> 
    
    <!--    =============== MATH ================== -->
    <xsl:template match="mml:*">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

<!-- ============ SEC ============== -->
    <xsl:template match="sec/title">
        <xsl:element name="{cup:HeadingType(parent::*)}">
            <xsl:attribute name="class"/>
            <xsl:apply-templates select="preceding-sibling::label" mode="do-label"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="label" mode="do-label">
        <span class="label">
            <xsl:apply-templates/>
        </span>
        <xsl:text> </xsl:text>
    </xsl:template>
    
    <xsl:template match="sec/label">
        <xsl:choose>
            <xsl:when test="following-sibling::title"></xsl:when>
            <xsl:otherwise>
                <div class="label">
                    <xsl:apply-templates/>
                </div>               
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--    =============== SUB SUP ================== -->
    <xsl:template match="sub |sup">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    
<!--    =============== TABLE ================== -->
    <xsl:template match="table | td | tr | th |thead | tbody">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    
<!--    =============== TERM =============== -->
    
<!--    see under def-list   -->
    
</xsl:stylesheet>
