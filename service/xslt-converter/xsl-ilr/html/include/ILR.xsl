<?xml version="1.0" encoding="UTF-8"?>

<!--
	************************************************************************************************
	** LIST OF UPDATE/MODIFICATION MADE ON THE SCRIPT
	** COLLECTION DATE STARTED: 2016-07-11
	** UPDATE SHOULD BE ADDED AT THE TOP
	** TC/AP/CG/PBT/JBA
	************************************************************************************************

    CBT-966 2016-07-11 JBA
	Affected File(s) : ILR.xsl
		* ILR - Proof URL index linking not working

    ************************************************************************************************
	** END OF UPDATE/MODIFICATION INFORMATION
	************************************************************************************************
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">

<!--initially optimised for ILR legacy project on CBO -->


    <!--    ====================== ILR ============================== -->
    
    
    <!--    ILR REQUIREMENT - one navigation file per case report-->
    <xsl:template match="book-part[@book-part-type='case-report']" priority="10">
        
          <xsl:result-document href="{@filename}">
          <html  xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta charset="UTF-8"/>
                    <link href="{concat($css-path, 'cup-bits.css')}" rel="stylesheet" type="text/css"></link> 
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                    <xsl:apply-templates select="descendant::named-content[@content-type='page'][1]" mode="first-page"/> 
                    <xsl:apply-templates select="." mode="content"/>
<!-- Moved to cbo2html.xsl
                    <xsl:apply-templates select="." mode="footnotes"/> 
                    <xsl:apply-templates select="." mode="pages"/>
-->
                </body>
            </html>
        </xsl:result-document>
        
        <xsl:apply-templates select="." mode="nav"/>
        <xsl:apply-templates select="." mode="ilr-cite"/>
    </xsl:template>
    
<!--    for CBO the chunking is driven by the cho header-->
    <xsl:template match="book-part[@book-part-type='case-report']" priority="10" mode="cbo">
        <xsl:apply-templates select="." mode="content"/>                     
        <!-- Moved to cbo2html.xsl
        <xsl:apply-templates select="." mode="footnotes"/> 
        <xsl:apply-templates select="." mode="pages"/>
-->
        <xsl:apply-templates select="." mode="nav"/>
        <xsl:apply-templates select="." mode="ilr-cite"/>
    </xsl:template>
    
<!--    don't output alt titles-->
    
    <xsl:template match="book-part[@book-part-type='case-report']/book-part-meta/title-group" priority="10"/>
    
    
    <!--  ILR  REQUIREMENT:  pages breaks to be shown; to have volume and page number; to have left or right running head -->
    <xsl:template match="book[@book-type='ilr']//named-content[contains(@content-type,'page')] | book-part-wrapper[@content-type='ilr']//named-content[contains(@content-type,'page')]" mode="#default content">       
<!--        anchor at page break location-->
        <a class="page-number" id="pg{string(.)}" data-num="{string(.)}">
            <xsl:if test="@id"><span id="{@id}"/></xsl:if>
        </a>
    </xsl:template>
    

    <xsl:template match="list[@list-content='paragraph']/list-item">
        <xsl:variable name="depth" as="xs:integer" select="count(ancestor::list[@list-content='paragraph'])"/>
        <xsl:apply-templates select="label/named-content[@content-type='page']" mode="do-pages"/>
        <div>
            <xsl:attribute name="class">
                <xsl:text>p numbered </xsl:text>
                <xsl:value-of select="concat('numbered', $depth)"/>
            </xsl:attribute>
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates select="label"/>
            <div class="contents">
                <xsl:apply-templates select="*[not(self::label)]"/>
            </div>
        </div>        
    </xsl:template>
    
    
    <xsl:template match="list[@list-content='paragraph']/list-item/label/named-content[@content-type='page']" mode="do-pages">
        <a class="page-number" id="pg{string(.)}" data-num="{string(.)}"></a>
    </xsl:template>
    
    <xsl:template match="list[@list-content='paragraph']/list-item/label/named-content[@content-type='page']" priority="10"/>


    
<!-- =================FOOTNOTES ============================   
    footnotes all at the end of the html file
    footnote to have data-locn to identifiy the location (end of page) where it is to appear-->

    
    <xsl:template match="book[@book-type='ilr']//fn-group"/>
    
<!--    fix duplicate output on HTML-->
    <xsl:template match="book-part | app | front-matter-part | index | toc | preface" mode="footnotes">     <!-- TODO: Change this to *[@chunk = 'yes'] ??? -->
        <xsl:if test="descendant::fn">
            <div class="footnotes" id="footnotes">
                <xsl:apply-templates select="descendant::fn" mode="footnotes"/>
            </div>
        </xsl:if>
    </xsl:template>
    
<!--    footnote to appear before the page break following the xref pointing to that footnote-->
    <xsl:template match="fn" mode="footnotes">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="page" select="string(preceding::xref[@rid=$id][last()]/following::named-content[@content-type='page'][1])"/>
        <div class="footnote" id="{@id}">
            <xsl:attribute name="data-locn" select="$page"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!--    Ambiguous error
    <xsl:template match="fn/p">-->
    <xsl:template match="fn/p[not(@content-type='counsel')]">
        <span>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="not(preceding-sibling::p)">p first</xsl:when>
                    <xsl:otherwise>p</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
<!--    footnote labels to be superscript-->
    <xsl:template match="book[@book-type='ilr']//fn/label">
        <span class="label">
            <span class="hidden">Footnote </span>
            <xsl:choose>
                <xsl:when test="not(.//sup)">
                    <sup>
                        <xsl:apply-templates/>
                    </sup>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
            
        </span>
    </xsl:template>
    
<!-- ============== PAGES ===================
    ILR REQUIREMENT  to have running head, volume number and page number created for each page break and placed at the end-->
    
    <!--    fix duplicate output on HTML-->
    <xsl:template match="book-part | app | front-matter-part | index | toc | preface" mode="pages">     <!-- TODO: Change this to *[@chunk = 'yes'] ??? -->
        <xsl:if test="descendant::named-content[@content-type='page']">
            <div id="pagebreaks">
                <xsl:apply-templates select="descendant::named-content[@content-type='page']" mode="pages"/>
            </div>
        </xsl:if>
    </xsl:template>

    <!--
			CBT-731 01.22.2016 JBA
			* fix to output the correct left and right running heads
    -->

    <xsl:template match="named-content[@content-type='page']" mode="pages">
        <xsl:variable name="volume" select="ancestor::book/collection-meta/volume-in-collection/volume-number"></xsl:variable>
        <xsl:variable name="left-rhead"  as="node()">
            <xsl:choose>
                <xsl:when test="ancestor::book-part[descendant::alt-title[@alt-title-type='left-running']]">
                    <xsl:value-of select="ancestor::book-part[descendant::alt-title[@alt-title-type='left-running']][1]/descendant::alt-title[@alt-title-type='left-running'][1]"/>
                </xsl:when>
                <xsl:otherwise><alt-title/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="right-rhead"  as="node()">
            <xsl:choose>
                <xsl:when test="ancestor::book-part[descendant::alt-title[@alt-title-type='right-running']]">
                    <xsl:value-of select="ancestor::book-part[descendant::alt-title[@alt-title-type='right-running']][1]/descendant::alt-title[@alt-title-type='left-running'][1]"/>
                </xsl:when>
                <xsl:otherwise><alt-title/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="r-head" as="node()">
            <xsl:choose>
                <xsl:when test="number(.) mod 2 =1">
                    <xsl:copy-of select="$right-rhead"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$left-rhead"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="r-head-text">
            <xsl:choose>
                <xsl:when test="$r-head/break"><xsl:value-of select="$r-head/text()[1]"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$r-head"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div class="label-border" datanum="{string(.)}">
            <span class='volume-page'>
                <xsl:value-of select="substring-after($volume, ' ')"/>
                <xsl:variable name="bookType" select="/book/@book-type"/>
                <xsl:variable name="is-icsid" as="xs:boolean" select="
                    $bookType = 'ilr' 
                    and matches(
                        normalize-space(
                            string-join(/book/collection-meta/title-group//text(), '')
                        ),
                        '(ICSID|International\s*Centre\s*for\s*Settlement\s*of\s*Investment\s*Disputes)', 
                        'is'
                    )"/>
                <xsl:choose>
                    <xsl:when test="$is-icsid">
                        <xsl:text> ICSID</xsl:text>
                    </xsl:when>
                    <xsl:when test="$bookType = 'ilr'">
                        <xsl:text> ILR</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message terminate="yes">This template should not run on non-ILR books (but I am not sure how that is prevented.)</xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
            <!--
		      CBT-731 – change the XSLT so that the HTML display uses the first right-hand running head in any case as the page head in the online service. In some cases the right-hand running head changes through the case
		      2016-02-04 JBA
            -->
            <xsl:value-of select="
                if 
                    (number(substring-after($volume, ' ')) &gt; 156) 
                then 
                    ancestor::book-part[descendant::alt-title[@alt-title-type='right-running']][last()]/descendant::alt-title[@alt-title-type='right-running'][1]//text()[not(ancestor::named-content[@content-type='page'])] 
                else 
                    $r-head-text
            "/>
            <span class='page-number'>
                <xsl:value-of select="string(.)"/>
            </span>
        </div>
        <xsl:if test="$r-head/break">
            <span class="citation-details"><xsl:value-of select="$r-head/text()[2]"/></span>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="named-content[@content-type='page']" mode="first-page">
        <xsl:variable name="right-rhead" select="ancestor::book-part[descendant::alt-title][1]/descendant::alt-title[@alt-title-type='right-running'][1]"/>
        <div class="pagebreak" datanum="{string(.)}">
            <span class='volume-at-page'>
                <xsl:value-of select="substring-after($volume, ' ')"/>
            </span>
            <span class="running-head">
                        <xsl:value-of select="$right-rhead"/>
            </span>
            <span class='page-number'>
                <xsl:value-of select="string(.)"/>
            </span>
        </div>
    </xsl:template>
    
    <!--   ILR requirement links to cases in other volumes -->
    
    <xsl:template match="mixed-citation">
        <xsl:choose>
            <xsl:when test="
                (@publication-type='ILR' or @publication-type='ICSID')
                and (source//text() = 'ILR' or source//text() = 'I.L.R' or source//text() = 'I.L.R.' or source//text() = 'ICSID Reports')
                and volume and fpage
            ">
                <xsl:apply-templates select="." mode="ilr-cite"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
   
<!--   ILR Requirement - all page numbers in an index entry to be nested within a div DPB-8412-->
    <!--    
        CBT-966 2016-07-11 JBA
        ILR - Proof URL index linking not working
    -->    
    <xsl:template match="index-entry/term">
        <span class="term">
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates/>
        </span>
        <div class="page-numbers">
            <xsl:apply-templates select="following-sibling::nav-pointer|following-sibling::nav-pointer-group" mode="do-pages"/>
        </div>
    </xsl:template>  
    
    <!--
        CBT-732 – output all nav-pointer, nav-pointer-group
        2016-02-05 JBA
    -->
    <xsl:template match="nav-pointer[parent::index-entry] | nav-pointer-group[parent::index-entry]"/>
   
<!--    ILR Requirement - spans around the generated text DPB-8412 -->
    <!--=============== NAV-POINTER ===================-->
    <xsl:template match="nav-pointer" mode="do-pages">
        <span class="nav-pointer"><a href="{cup:Link(. , @rid)}"><xsl:apply-templates/></a>  </span> <xsl:if test="parent::nav-pointer-group and @nav-pointer-type='start-of-range'"><span class="index-dash">&#x2013;</span></xsl:if>
        <xsl:if test="not(parent::nav-pointer-group) and (following-sibling::nav-pointer or following-sibling::nav-pointer-group)"><span class="index-comma">, </span></xsl:if>
    </xsl:template>       
    <!--=============== NAV-POINTER-GROUP ===================-->
    
    <xsl:template match="nav-pointer-group" mode="do-pages">
        <span class="nav-pointer-group">      
            <xsl:apply-templates mode="do-pages"/>
        </span> 
        <xsl:if test="following-sibling::nav-pointer or following-sibling::nav-pointer-group">
            <span class="index-comma">, </span>
        </xsl:if>
    </xsl:template> 
    
    
    <!--    ===================== nav file of ilr and icsid citations for ILR ================ -->
<!--    Requirement: to be a separate file-->
    <xsl:template match="book-part" mode="ilr-cite">
        <xsl:if test="
            descendant::mixed-citation[
                (@publication-type='ILR' or @publication-type='ICSID')
                and (source//text() = 'ILR' or source//text() = 'I.L.R' or source//text() = 'I.L.R.' or source//text() = 'ICSID Reports')
                and volume and fpage
            ]
        ">
            <xsl:result-document href="{concat('citations_',@filename)}">
                <li>
                    <xsl:text>Citations</xsl:text>
                    <ul>
                        <xsl:for-each select="descendant::mixed-citation[
                            (@publication-type='ILR' or @publication-type='ICSID')
                            and (source//text() = 'ILR' or source//text() = 'I.L.R' or source//text() = 'I.L.R.' or source//text() = 'ICSID Reports')
                            and volume and fpage
                            ]
                        ">
                            <li>
                                <xsl:apply-templates select="." mode="ilr-cite"/>
                            </li>
                        </xsl:for-each>
                        
                    </ul>
                </li> 
            </xsl:result-document>
        </xsl:if> 
    </xsl:template>
    







    
    <xsl:template match="mixed-citation" mode="ilr-cite">
        <xsl:variable name="vol" as="xs:string*" select="tokenize(./volume/text(), '\D+')[.!=''][1]"/>
        <xsl:variable name="pages" as="xs:string*" select="for $p in .//fpage return tokenize($p/text(), '\D+')[.!=''][1]"/>
        <xsl:variable name="source" as="xs:string">
            <xsl:choose>
                <xsl:when test="source//text() = 'ICSID Reports'">ICSID</xsl:when>
                <xsl:otherwise>ILR</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="count($vol) = 1 and count($pages) gt 0">
                <!-- Create multiple links when the mixed-citation lists multiple pages in a single volume -->
                <xsl:for-each select="$pages">
                    <xsl:variable name="normalizedCitationForCLR" as="xs:string" select="concat($vol, ' ', $source, ' ', .)"/>
                    <a>
                        <xsl:attribute name="href" select="$normalizedCitationForCLR"/>
                        <xsl:value-of select="$normalizedCitationForCLR"/>
                    </a>
                    <xsl:if test="not(position() = last())"><xsl:text> </xsl:text></xsl:if>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <!-- Don't attempt to create a link if the mixed-citation hasn't been marked up correctly -->
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
 
    <!--    ===================== nav file for ILR ================ -->
    
<!--     ================ FOR CBO =====================   -->
    <!--    ILR REQUIREMENT - one navigation file per book -->
    <xsl:template match="book" mode="vol-nav"> <!--where book is the book element in the cho header-->
        <xsl:result-document href="{concat('nav_',$isbn, '.html')}" indent="yes">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta charset="UTF-8"/>
                    <link href="{concat($css-path, 'cup-bits.css')}" rel="stylesheet" type="text/css"></link>
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                    <ul class="sb-unordered-list">
                        <xsl:choose>
                            <xsl:when test="$content-file//book-part[not(@chunk='yes') and (.//book-part[@chunk='yes'] or .//app)]">
                                <!-- This volume has non-chunk book-parts which contain chunk book-parts (as determined by add-filesnames.xsl
                                     That means it categorizes cases (and maybe stubs and annexes) under headings, represented by book-parts 
                                     of type "part" in BITS. --> 
                                
                                <!-- Ugly hack to get title page, etc in the volume navigation for this kind of volume -->
                                <!--    fix for the correct sequence on bookmark    -->
                                <xsl:apply-templates select="descendant::html[contains(@filename, '_f') or (contains(@filename, '_b') and starts-with(@rid, 'FMT'))]" mode="vol-nav"/>
                                
                                <!-- Go back to the BITS file to recreate the structure of book-parts in there, which was
                                     lost when the CBO header file was generated as a flat list of (mostly) cases because
                                     CLR doesn't want parts to be chunked. -->
                                <xsl:apply-templates select="$content-file" mode="get_structure_from_bits">
                                    <xsl:with-param name="headerFileHtmlElements" select="descendant::html"/>
                                </xsl:apply-templates>
                                
                                <!-- Ugly hack to get appendices and general back-matter in the volume navigation for this kind of volume  -->
                                <!--    fix for the correct sequence on bookmark    -->
                                <xsl:if test="not($content-file//book-back//book-part[not(@chunk='yes') and (.//book-part[@chunk='yes'] or .//app)])">
                                    <xsl:apply-templates select="descendant::html[contains(@filename, '_a') or (contains(@filename, '_b') and not(starts-with(@rid, 'FMT')))]" mode="vol-nav"/>
                                </xsl:if>
                            </xsl:when>
                            <xsl:otherwise>
                                
                                <!-- This volume is just a flat list of front-matter, cases and back-matter (typical of in late-volume ILR) -->
                                <xsl:apply-templates select="descendant::html" mode="vol-nav"/>
                                
                            </xsl:otherwise>
                        </xsl:choose>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
    
    <xsl:template match="*" mode="get_structure_from_bits">
        <xsl:param name="headerFileHtmlElements"/>
        <xsl:choose>
            <xsl:when test="self::book-part[@chunk='yes']">
                <!-- This BITS element corresponds to a content-item in the CBO header,
                     so use the existing template to output a bullet point and link 
                     for it in the volume navigation file -->
                <xsl:variable name="thisid" select="@id"/>
                <xsl:apply-templates select="$headerFileHtmlElements[@rid=$thisid]" mode="vol-nav"/>
            </xsl:when>
            <xsl:when test="self::book-part[descendant::book-part[@chunk='yes']]">
                <!-- This is a non-chunk part which wraps one or more chunks-parts,
                     so output out its title as text, under which chunk links will
                     be listed -->
                <li>
                    <p>
                        <!-- Display second-level part in bold if there is a third-level (top-level will be bold thanks to CSS) -->
                        <xsl:if test="count(ancestor::book-part[@book-part-type='part']) = 1 and ancestor::book-part[@book-part-type='part']//book-part[@book-part-type='part']//book-part[@book-part-type='part']">
                            <xsl:attribute name="class" select="'alpha-bold'"/>
                        </xsl:if>
                        
                        <!-- TODO: More restrictions in the next three lines, on BITS elements in title-group? -->
                        <!--    exclude text on xref-->
                        <xsl:value-of select="(book-part-meta/title-group/label)[1]//text()[not(ancestor::named-content[@content-type='page'] or ancestor::index-term or ancestor::xref)]"/>
                        <xsl:if test="book-part-meta/title-group/label"><xsl:text> </xsl:text></xsl:if>
                        <xsl:value-of select="(book-part-meta/title-group/title)[1]//text()[not(ancestor::named-content[@content-type='page'] or ancestor::index-term or ancestor::xref)]"/>
                    </p>
                    <ul>
                        <xsl:apply-templates mode="get_structure_from_bits">
                            <xsl:with-param name="headerFileHtmlElements" select="$headerFileHtmlElements"/>
                        </xsl:apply-templates>
                    </ul>
                </li>
            </xsl:when>
            <xsl:otherwise>
                <!-- This is neither a chunk nor a non-chunk book-part,
                     but could contain some (e.g. a body element) -->
                <xsl:apply-templates mode="get_structure_from_bits">
                    <xsl:with-param name="headerFileHtmlElements" select="$headerFileHtmlElements"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="text()" mode="get_structure_from_bits"></xsl:template>
    
    
    <xsl:template match="html" mode="vol-nav">
        <xsl:variable name="rid" select="@rid"/>
        <xsl:variable name="content" select="$content-file//*[@id=$rid]"></xsl:variable>
        <li>
            <span class="bullet-icon"></span>
            <a href="{@filename}">
                    <xsl:choose>
                        <xsl:when test="$content[@content-type='case-list-complex' ]">
                            <xsl:value-of select="concat(parent::content-item/heading/title[1], ' by court')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="parent::content-item/heading/title"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </a>
<!--            <xsl:if test="$content[@content-type='case-list-complex' ]">
                <xsl:apply-templates select="$content" mode="vol-nav"/>
            </xsl:if>-->
        </li>
    </xsl:template>
    
    <xsl:template match="index[@content-type='case-list-complex' ]" mode="vol-nav">
        <ul class="sb-unordered-list">
            <xsl:apply-templates select="index-div" mode="vol-nav"/>              
        </ul>
    </xsl:template>

    <xsl:template match="index-div" mode="vol-nav">
        <li>
            <span class="bullet-icon"/>
            <xsl:value-of select="title-group/title"/><!--only the title is needed-->
<!--            <xsl:if test="index-div">
                <ul>
                    <xsl:apply-templates select="index-div" mode="vol-nav"/>
                </ul>
            </xsl:if>-->
        </li>
    </xsl:template>
    
    <!--    ILR REQUIREMENT - one navigation file per case report-->
    
    <xsl:template match="book-part" mode="nav">
        <xsl:variable name="case-filename" select="@filename"></xsl:variable>
        <xsl:result-document href="{concat('nav_',@filename)}">
            <html  xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta charset="UTF-8"/>
                    <link href="{concat($css-path, 'cup-bits.css')}" rel="stylesheet" type="text/css"></link> 
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                    <ul class="sb-unordered-list">
                        <li>
                            <span class="bullet-icon"/><a href="{$case-filename}#{book-part-meta/kwd-group/@id}">Subject</a>
                        </li>
                        <li>
                            <span class="bullet-icon"/><a href="{$case-filename}#{body/sec[@sec-type='casehead'][1]/@id}">Casehead</a>
                        </li>
                        <li>
                            <span class="bullet-icon"/><a href="{$case-filename}#{body/sec[@sec-type='case-summary'][1]/@id}">Summary</a>
                        </li>
                        
                        <xsl:apply-templates select="descendant::p[@content-type='counsel'] 
                            | descendant::sec[@sec-type='judgement'] | descendant::sec[@sec-type='decision'] | descendant::sec[@sec-type='case-reference']" mode="nav">
                            <xsl:with-param name="case-filename" select="@filename"/>
                        </xsl:apply-templates>
<!--
                        <xsl:apply-templates select="descendant::sec[@sec-type='case-reference']" mode="nav">
                            <xsl:with-param name="case-filename" select="@filename"/>
                        </xsl:apply-templates>  -->                
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    

    
    <xsl:template match="sec[@sec-type='judgement']" mode="nav">
        <xsl:param name="case-filename"/>
        <li><span class="bullet-icon"/>
            <a href="{$case-filename}#{@id}">Judgment</a>
        </li>
    </xsl:template>
    
    <xsl:template match="sec[@sec-type='decision']" mode="nav">
        <xsl:param name="case-filename"/>
        <li><span class="bullet-icon"/>
            <a href="{$case-filename}#{@id}">Order</a>
        </li>
    </xsl:template>
    
<!--    use the id given to the counsel in the transformation-->
    <xsl:template match="p[@content-type='counsel']" mode="nav">
        <xsl:param name="case-filename"/>
        <xsl:variable name="id" select="concat('counsel',count(preceding::p[@content-type='counsel']))"/>
        <li><span class="bullet-icon"/>
            <a href="{$case-filename}#{$id}"><xsl:text>Counsel</xsl:text></a>
        </li>
    </xsl:template>
    
    <xsl:template match="sec[@sec-type='case-reference']" mode="nav">
        <xsl:param name="case-filename"/>
        <li><span class="bullet-icon"/><a  href="{$case-filename}#{@id}">Parallel Citation</a></li>
    </xsl:template>
    
<!--    counsel to be given an id-->
    <xsl:template match="p[@content-type='counsel']">
        <xsl:variable name="id" select="concat('counsel',count(preceding::p[@content-type='counsel']))"/>
        <div class="counsel" id="{$id}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!--  glossary title to be in div title-group -->
    
<xsl:template match="glossary/title">
    <div class="title-group">
        <span class="title"><xsl:apply-templates/></span>
    </div>
</xsl:template>
    
<!--    alt-titles in frontmatter not to be processed-->
    <xsl:template match="title-group[title or label]">
        <div class="title-group">
            <xsl:apply-templates select="node()[not(self::alt-title)]"/>
        </div>
    </xsl:template>
    
    <xsl:template match="xref">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:variable name="link-att" select="@rid | @xlink:href" as="xs:string"/>
            <a>
                <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/> 
                <xsl:apply-templates select="." mode="id"/>
                <xsl:choose>
                    <xsl:when test="cup:isFootnoteMarker(.)">
                        <span class="hidden">footnote </span>
                        <xsl:choose>
                            <xsl:when test="not(.//sup) and not(ancestor::fn)">
                                <sup>
                                    <xsl:apply-templates/>
                                </sup>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>  
                    </xsl:otherwise>
                </xsl:choose>
            </a>
        </span>
    </xsl:template>
    
<!--    treaty metadata to not be displayed -->
    
    <xsl:template match="index-term | custom-meta-group"/>
</xsl:stylesheet>