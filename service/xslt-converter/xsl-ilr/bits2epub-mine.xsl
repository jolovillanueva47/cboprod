<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cup xlink xs" version="2.0" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--    acts on a bits file that has had the filenames of the individual html files added as att filename-->

    <!--  include the default bits2html  -->
    <xsl:import href="cup-bits_baseline-html/kernel/kernel.xsl"/>
    <xsl:param name="bits2epub-version" required="yes"/>
    <xsl:param as="xs:string" name="path.css" required="no" select="'../Styles/'"/>
    <xsl:param as="xs:string" name="path.img" required="no" select="'../Images/'"/>

    <xsl:variable as="xs:string" name="book-title" select="/book/book-meta/book-title-group/book-title"/>
    <xsl:variable as="xs:string" name="volume" select="/book/collection-meta/volume-in-collection/volume-number"/>
    <xsl:variable as="element()*" name="lookup.tgts">
        <xsl:for-each select="//@id">
            <cup:target filename="{parent::node()/ancestor-or-self::node()[@filename][1]/@filename}" id="{string()}"/>
        </xsl:for-each>
    </xsl:variable>

    <xsl:output encoding="UTF-8" indent="no" method="xml"/>

    <xsl:template match="/">
        <xsl:for-each select="descendant::*[@chunk][@filename]">
            <xsl:variable as="document-node()" name="document">
                <xsl:document>
                    <xsl:copy-of select="."/>
                </xsl:document>
            </xsl:variable>
            <xsl:result-document href="{@filename}">
                <xsl:apply-templates mode="kernel" select="$document"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="*[matches(local-name(),$regex.meta)]" mode="kernel.body" priority="1.5"/>
    <!--omit direct presentation of -meta elems-->

    <xsl:template match="/" mode="kernel" priority="1.5">
        <html>
            <xsl:apply-templates mode="kernel.head" select="."/>
            <xsl:apply-templates mode="kernel.body"/>
        </html>
    </xsl:template>

    <xsl:template match="/child::*[1]" mode="kernel.body" priority="2.5">
        <body>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:if
                test="book-part-meta or self::glossary[title] or self::index[title-group]  or self::toc[title-group] or self::ref-list[title]">
                <xsl:sequence select="cup:headerConstructor(.,0)"/>
            </xsl:if>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </body>
    </xsl:template>

    <xsl:template
        match="/child::*[1]/book-part-meta | /child::*[1]/title-group | /child::*[1]/contrib-group  | /child::*[1]/title | /child::*[1]/label |  /child::*[1]/subtitle"
        mode="kernel.body" priority="1.5"/>
    <!--skip elements handled by header constructor function-->

    <xsl:template match="/" mode="kernel.head" priority="1.5">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when
                        test="
                        if (descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)]) 
                        then true()
                        else false()">
                        <xsl:apply-templates mode="kernel.head"
                            select="(descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)])[1]"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'BITS2EPUB'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <meta charset="UTF-8"/>
            <meta content="bits2epub v.{$bits2epub-version}" name="generator"/>
            <link href="{concat($path.css, 'cup-bits-epub3.css')}" media="all" rel="stylesheet" type="text/css"/>
        </head>
    </xsl:template>

    <xsl:template match="@xlink:href[parent::ext-link]" mode="kernel.body" priority="1.5">
        <xsl:variable as="xs:string" name="tgt.file" select="$lookup.tgts[@id eq current()/string()][1]/@filename"/>
        <xsl:attribute name="href" select="concat($tgt.file,'#',string())"/>
    </xsl:template>

    <xsl:template match="@rid" mode="kernel.body" priority="1.5">
        <xsl:variable as="xs:string" name="tgt.file" select="$lookup.tgts[@id eq current()/string()][1]/@filename"/>
        <xsl:attribute name="href" select="concat($tgt.file,'#',string())"/>
    </xsl:template>

    <!--    OVER-RIDES  - elements in alphabetical order -->

    <xsl:template match="alt-title" mode="kernel.body" priority="1.5"/>

<!--epub:switch for math-->
    <xsl:template match="alternatives[child::mml:math]" mode="kernel.body" priority="1.5">
        <epub:switch>
            <xsl:choose>
                <xsl:when test="@id">
                    <xsl:apply-templates mode="kernel.body" select="@id"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="id" select="generate-id()"/>
                </xsl:otherwise>
            </xsl:choose>
            <epub:case required-namespace="http://www.w3.org/1998/Math/MathML">
                <xsl:apply-templates mode="kernel.body" select="mml:math"/>
            </epub:case>
            <epub:default>
                <xsl:apply-templates mode="kernel.body" select="graphic | inline-graphic | textual-form"/>
            </epub:default>
        </epub:switch>
    </xsl:template>

<!--add label before caption to first p or title in caption-->
    <xsl:template match="caption/p | caption/title" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.label-before" select="parent::caption/preceding-sibling::*[1][self::label]"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

<!--collab to be a span when in mixed-citation-->
    <xsl:template match="collab[ancestor::mixed-citation]" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    


    <!--    labels after expression-->
    <xsl:template match="disp-formula" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
            <xsl:apply-templates mode="kernel.body" select="label"/>
        </div>
    </xsl:template>

    <xsl:template match="disp-formula-group" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
            <xsl:apply-templates mode="kernel.body" select="label"/>
        </div>
    </xsl:template>
    
<!--    caption to be at foot of figure-->
    <xsl:template match="fig" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label or self::caption or self::attrib  or self::permissions )]"/>
            <xsl:apply-templates mode="kernel.body" select="label | caption |attrib |permissions"/>
        </div>
    </xsl:template>
    
    
    <!--note 1: placing labels in their paras
    this method overrides default templates with preceding/following-sibling tests
    requires two overrides:
    1 - match labels that immediately precede a p, and apply templates to the p's *contents*
    2 - skip p's that immediately follow a label
-->
    <xsl:template match="label[following-sibling::*[1][self::p]]" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(following-sibling::p[1])"/>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::p[1]/@*"/>
            <span>
                <xsl:sequence select="cup:classConstructor(.)"/>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </span>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::p[1]/node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="p[local-name(preceding-sibling::*[1]) eq 'label']" mode="kernel.body" priority="1.5"/>
    <!--end note 1-->

    <!--    label followed by title - see also template for title below -->
    <!-- 2do - replace this with title constructor function-->
    <xsl:template match="label[not(ancestor::toc)][matches(local-name(following-sibling::*[1]),$regex.titles)]" mode="kernel.body"
        priority="1.5">
        <xsl:variable as="xs:integer" name="head.level" select="count(ancestor-or-self::*[child::*[matches(local-name(),$regex.titles)]])"/>
        <xsl:element name="{concat('h',$head.level)}">
            <xsl:apply-templates mode="kernel.body" select="following-sibling::*[1]/@*"/>
            <span>
                <xsl:sequence select="cup:classConstructor(.)"/>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </span>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::*[1]/node()"/>
        </xsl:element>
    </xsl:template>

    <!--    do label in caption-->
    <xsl:template match="label" mode="kernel.label-before" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="label[following-sibling::*[1][self::caption]]" mode="kernel.body" priority="1.5"/>
    
<!--    numbered paragraphs  -->
    <xsl:template match="list[@list-content eq 'paragraph']" mode="kernel.body" priority="1.5">
        <xsl:apply-templates mode="kernel.body"/>
    </xsl:template>
    
    <xsl:template match="list[@list-content eq 'paragraph']/list-item" mode="kernel.body" priority="1.5">
        <div class="p numbered">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>
    
    <xsl:template match="list[@list-content eq 'paragraph']/list-item/p" mode="kernel.body" priority="1.5">
        <xsl:apply-templates mode="kernel.body" select="node()"/>
    </xsl:template>
    
<!--    name in mixed-citation to be span-->
    <xsl:template match="name[ancestor::mixed-citation]" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
<!--    pages to be a for epub-->
    <xsl:template match="named-content[@content-type eq 'page']" mode="kernel.body" priority="1.5">
        <a>
            <xsl:attribute name="id" select="concat('page_' , string(.) )"/>
        </a>
    </xsl:template>
    
    
    <xsl:template match="sup" mode="kernel.body" priority="1.5">
        <sup>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </sup>
    </xsl:template>
    
    <xsl:template match="sub" mode="kernel.body" priority="1.5">
        <sub>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </sub>
    </xsl:template>

    
<!--    subtitle to be span-->
    <xsl:template match="subtitle" mode="kernel.subtitle" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>
    
    
<!--   just get text in styled-content element - added for font substitution-->
    <xsl:template match="text()[ancestor::styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']]"
        mode="kernel.header kernel.body" priority="1.5">
        <xsl:value-of select="."/>
    </xsl:template>
    <!--disable Unicode normalization for text in substitute fonts-->
    
    <xsl:template match="styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']" mode="kernel.header kernel.body"
        priority="1.5">
        <span>
            <xsl:attribute name="class" select="concat('unicode-altfont-',@style)"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

<!-- 2do - replace this with title constructor function-->
    <xsl:template match="title[not(ancestor::toc)]" mode="kernel.body" priority="1.5">
        <xsl:variable as="xs:integer" name="head.level" select="count(ancestor-or-self::*[child::*[matches(local-name(),$regex.titles)]])"/>
        <xsl:element name="{concat('h',$head.level)}">
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
            <xsl:if test="following-sibling::*[1][self::subtitle]">
                <xsl:apply-templates mode="kernel.subtitle" select="following-sibling::*[1][self::subtitle]"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[matches(local-name(),$regex.titles)][not(ancestor::toc)][preceding-sibling::*[1][self::label or self::title]]"
        mode="kernel.body" priority="1.5"/>

    <xsl:template match="title[@class eq 'unprocessed']" mode="kernel.header" priority="1.5"/><!--discard default no-candidate titles-->
    
<!--    toc-entry title to be live link; if no title label to be live link -->
    <xsl:template match="toc-entry/title" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <a>
                <xsl:apply-templates mode="kernel.body" select="following-sibling::nav-pointer/@rid"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </a>
        </span>
    </xsl:template>
    
    <xsl:template match="toc-entry/label[not(following-sibling::title)]" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <a>
                <xsl:apply-templates mode="kernel.body" select="following-sibling::nav-pointer/@rid"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </a>
        </span>
    </xsl:template>
    
    <xsl:template match="toc-entry/nav-pointer" mode="kernel.body" priority="1.5"/>
    
<!--    links within toc-entry title not to be live -->    
    <xsl:template match="toc-entry//xref">
        <xsl:apply-templates/>
    </xsl:template>
    

</xsl:stylesheet>
