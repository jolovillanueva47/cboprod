<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xlink cup oebpackage xs mml" version="2.0" xmlns="http://www.idpf.org/2007/opf"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:oebpackage="http://openebook.org/namespaces/oeb-package/1.0/"
    xmlns:opf="http://www.idpf.org/2007/opf" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--Acts on BITS file where attribute chunk has been added to each element to be the root of an html file
    needs image-lookup file to have been created

    creates opf to epub3 standard
    needs parameters: isbn, version and optional fonts
    -->

    <xsl:import href="../functions/functions.xsl"/>
    <xsl:param name="ver" required="no" select="'DEBUG'"/>
    <!--cbml2epub version-->
    <xsl:param name="fonts" required="no"/>

    <xsl:output indent="yes" method="xml"/>


    <xsl:variable name="prelims" select="book/book-meta"/>

    <xsl:variable name="endmatter" select="book-back"/>
    <xsl:variable name="book" select="book"/>
    <xsl:variable name="image-path" select="'Images/'"/>
    <xsl:variable name="version" select="'3.0'"/>
    <xsl:variable name="image-lookup" select="document('image-lookup.xml', .)"/>

    <xsl:param as="xs:string" name="isbn"/>


    <xsl:param as="xs:string" name="custom-css" select="'false'"/>
    <!--automatically set if CSS override file detected by Ant-->

    <xsl:param as="xs:string" name="variant-design-opf" select="'0'"/>
    <!--set if variant OPF template to be used, 0 is Classic Mode-->
    <!--current design variants
        0 - cbml2epub Classic
        1 - adjustments for AU EPUB-only titles 2013-08:
            # dc:title to come from CBML
            # dc:title to include series-name
        [add more here as necessary]
        -->

    <xsl:template match="/">
        <xsl:apply-templates select="book"/>
    </xsl:template>

    <xsl:template match="book">
        <package version="{$version}">
            <xsl:attribute name="unique-identifier">
                <xsl:value-of select="concat('p' , $isbn)"/>
            </xsl:attribute>
            <xsl:apply-templates mode="metadata" select="."/>
            <xsl:apply-templates mode="manifest" select="."/>
            <xsl:apply-templates mode="spine" select="."/>
            <xsl:apply-templates mode="guide-root" select="."/>
        </package>
    </xsl:template>

    <!--
    spine lists all files in sequence
    attribute idref matches attribue id of that item in the manifest
    -->
    <xsl:template match="book" mode="spine">
        <spine toc="ncx" xmlns="http://www.idpf.org/2007/opf">
            <itemref idref="coverpage" linear="no"/>
            <xsl:for-each select="descendant::*[@chunk]">
                <xsl:variable name="posn" select="count(preceding::*[@chunk]) + count(ancestor::*[@chunk]) + 1"/>
                <itemref idref="{concat('doc', string($posn) )}"/>

            </xsl:for-each>
        </spine>
    </xsl:template>

    <xsl:template match="book" mode="metadata">
        <xsl:variable as="element()" name="au-grp" select="book-meta/contrib-group[not(@content-type='contributors')]"/>
        <xsl:variable name="book-title">
            <xsl:choose>
                <xsl:when test="not(collection-meta/title-group/title)">
                    <xsl:value-of select="book-meta/book-title-group/book-title"/>
                </xsl:when>
                <xsl:when test="book-meta/book-title-group/book-title">
                    <xsl:value-of select="concat(collection-meta/title-group/title, ': ', book-meta/book-title-group/book-title)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="collection-meta/title-group/title"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="aut-str">
            <xsl:for-each select="$au-grp/contrib[following-sibling::contrib]">
                <xsl:value-of select="concat(descendant::surname,', ',descendant::given-names,' ',descendant::suffix)"/>
                <!--<xsl:value-of select="concat(string(name | string-name), ',')"/>-->
                <xsl:text> and </xsl:text>
            </xsl:for-each>
            <xsl:value-of
                select="$au-grp/contrib[not(following-sibling::contrib)]/concat(descendant::surname,', ',descendant::given-names,' ',descendant::suffix)"
            />
        </xsl:variable>
        <xsl:variable name="au-snms">
            <xsl:for-each select="$au-grp/contrib[following-sibling::contrib]">
                <xsl:value-of select="concat(string(//surname),', ')"/>
            </xsl:for-each>
            <xsl:if test="count($au-grp/contrib)&gt; 1">
                <xsl:text>and </xsl:text>
            </xsl:if>
            <xsl:value-of select="$au-grp/contrib[not(following-sibling::contrib)]//surname"/>
        </xsl:variable>
        <xsl:variable name="aus-type">
            <xsl:choose>
                <xsl:when test="$au-grp[@contrib-type='authors']">aut</xsl:when>
                <xsl:when test="$au-grp[@contrib-type='editors']">edt</xsl:when>
                <xsl:when test="$au-grp/contrib[1][@contrib-type='author']">aut</xsl:when>
                <xsl:when test="$au-grp/contrib[1][@contrib-type='editor']">edt</xsl:when>
                <xsl:otherwise>aut</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
            <dc:title>
                <xsl:value-of select="$book-title"/>
            </dc:title>
            <xsl:if test="count($au-grp/contrib) &gt; 1">
                <dc:creator>
                    <xsl:value-of select="normalize-space($aut-str)"/>
                </dc:creator>
            </xsl:if>
            <!-- one for each contrib -->
            <xsl:for-each select="$au-grp/contrib">
                <dc:creator>
                    <xsl:value-of select="normalize-space(concat(descendant::surname,', ',descendant::given-names,' ',descendant::suffix))"/>
                </dc:creator>
            </xsl:for-each>
            <!-- one for each contributor -->
            <xsl:for-each select="book-meta/contrib-group[@content-type='contributors']/contrib">
                <dc:contributor>
                    <xsl:value-of select="normalize-space(concat(descendant::surname,', ',descendant::given-names,' ',descendant::suffix))"/>
                </dc:contributor>
            </xsl:for-each>
            <dc:date>
                <xsl:value-of select="format-date(current-date(), '[Y,4]-[M,2]-[D,2]')"/>
            </dc:date>
            <dc:identifier id="{concat('p',$isbn)}">
                <xsl:value-of select="$isbn"/>
            </dc:identifier>
            <dc:format>
                <xsl:value-of select="concat(count($book//named-content[@content-type='page']), ' pages' )"/>
            </dc:format>
            <!-- mime type -->
            <dc:type>Text</dc:type>
            <dc:language>en</dc:language>
            <dc:rights>
                <xsl:value-of select="string(book-meta/permissions/copyright-statement[not(following::copyright-statement)])"/>
            </dc:rights>
            <dc:publisher>
                <xsl:value-of select="book-meta/publisher[1]/publisher-name"/>
            </dc:publisher>
            <meta content="cover-image" name="cover"/>
            <meta content="created by bits2epub version {$ver}" name="origin"/>
            <xsl:variable name="ut" select="xs:dayTimeDuration('PT0H')"/>
            <xsl:variable name="time" select="adjust-dateTime-to-timezone(current-dateTime(), $ut)"/>
            <xsl:variable name="date-str" select="string($time)"/>
            <xsl:variable name="adj-date-str" select="concat(substring-before($date-str, '.'), 'Z')"/>
            <meta property="dcterms:modified">
                <xsl:value-of select="$adj-date-str"/>
            </meta>
        </metadata>
    </xsl:template>

    <xsl:template match="book" mode="manifest">

        <manifest>
            <item href="toc.ncx" id="ncx" media-type="application/x-dtbncx+xml"/>
            <item href="Text/coverpage.html" id="coverpage" media-type="application/xhtml+xml"/>
            <item href="Images/cover.jpg" id="cover-image" media-type="image/jpeg"/>
            <item href="Text/nav.html" id="nav" media-type="application/xhtml+xml" properties="nav"/>
            <item id="pages" href="oebps-page-map.xml" media-type="application/oebpspage-map+xml"/>
            <xsl:apply-templates mode="manifest" select="descendant::*[@chunk]"/>
            <item href="Styles/cup-bits-epub3.css" id="style" media-type="text/css"/>
            <xsl:apply-templates select="$image-lookup//image"/>
            <xsl:apply-templates mode="items" select="descendant::mml:math[1]"/>
            <xsl:for-each select="tokenize($fonts,';')">
               
                <!--split optional $fonts param into strings for font filenames-->
                <item href="Fonts/{.}" id="{concat('font',position())}" media-type="application/x-font-truetype"/>
            </xsl:for-each>
        </manifest>
    </xsl:template>

    <xsl:template match="*[@chunk]" mode="manifest">
        <xsl:variable name="src-id" select="@id"></xsl:variable>
        <item>
            <xsl:attribute name="id" select="concat('doc', position() )"/>
            <xsl:attribute name="href" select="concat('Text/',@filename)"/>
            <xsl:attribute name="media-type" select="'application/xhtml+xml'"/>
            <xsl:if test="descendant::mml:math[not(ancestor::*[@chunk][1][@id!=$src-id])]"><!--parts not to count math in descendant chapters-->
                <xsl:attribute name="properties" select="'mathml switch scripted'"/> 

            </xsl:if>
        </item>
    </xsl:template>

    <!--image lookup file-->
    <xsl:template match="image">
        <xsl:variable name="count" select="count(preceding-sibling::image)+1"/>
        <item href="{concat('Images/',@file)}" id="{concat('img', $count)}" media-type="image/{substring-after(@file, '.')}"> </item>
    </xsl:template>


    <xsl:template match="image[contains(@file, 'logo.')]">
        <xsl:variable name="count" select="count(preceding-sibling::image)+1"/>
        <item href="Images/logo.png" id="{concat('img', $count)}" media-type="image/png"> </item>
    </xsl:template>

    <xsl:template match="mml:math" mode="items"><!--MathJax embedding is disabled for 0.9.1 release-->
        <!--<item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/BasicLatin.js" id="id0" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/MiscMathSymbolsB.js" id="id1" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/d.js" id="id2" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/jax.js" id="id3" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/cancel.js" id="id4" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/a.js" id="id5" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/AsciiMath/jax.js" id="id6" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/ms.js" id="id7" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/mathchoice.js" id="id8" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Script/Regular/Main.js" id="id9" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/extpfeil.js" id="id10" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/AsciiMath/config.js" id="id11" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/Arrows.js" id="id12" media-type="application/x-javascript"/>
        <item href="mathjax/images/CloseX-31.png" id="id13" media-type="image/png"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/LatinExtendedA.js" id="id14" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Italic/Other.js" id="id15" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Size3/Regular/Main.js" id="id16" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Size2/Regular/Main.js" id="id17" media-type="application/x-javascript"/>
        <item href="mathjax/images/MenuArrow-15.png" id="id18" media-type="image/png"/>
        <item href="mathjax/jax/input/TeX/jax.js" id="id19" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Size1/Regular/Main.js" id="id20" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/h.js" id="id21" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/l.js" id="id22" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Bold/PUA.js" id="id23" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/AMSsymbols.js" id="id24" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/CombDiacritMarks.js" id="id25" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Regular/PUA.js" id="id26" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/GreekAndCoptic.js" id="id27" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/LatinExtendedB.js" id="id28" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/LatinExtendedA.js" id="id29" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/MiscSymbols.js" id="id30" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/scr.js" id="id31" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/MiscSymbols.js" id="id32" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/MiscTechnical.js" id="id33" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/newcommand.js" id="id34" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/mmultiscripts.js" id="id35" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/p.js" id="id36" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Typewriter/Regular/Main.js" id="id37" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/o.js" id="id38" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/annotation-xml.js" id="id39" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/u.js" id="id40" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Bold/BasicLatin.js" id="id41" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Regular/BasicLatin.js" id="id42" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Size4/Regular/Main.js" id="id43" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/noUndefined.js" id="id44" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/k.js" id="id45" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/LetterlikeSymbols.js" id="id46" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/HTML.js" id="id47" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Regular/Main.js" id="id49" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/MiscSymbolsAndArrows.js" id="id50" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Bold/BasicLatin.js" id="id51" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/GeometricShapes.js" id="id52" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/GeometricShapes.js" id="id53" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/config.js" id="id54" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/enclose.js" id="id55" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/EnclosedAlphanum.js" id="id56" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/mhchem.js" id="id57" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/verb.js" id="id58" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/GeneralPunctuation.js" id="id59" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/MathMenu.js" id="id60" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/GeometricShapes.js" id="id61" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/PUA.js" id="id62" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/LetterlikeSymbols.js" id="id63" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/z.js" id="id64" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Bold/Main.js" id="id65" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/SpacingModLetters.js" id="id66" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Bold/Other.js" id="id67" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/autoload-all.js" id="id68" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Bold/CombDiacritMarks.js" id="id69" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/noErrors.js" id="id70" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/g.js" id="id71" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Regular/BasicLatin.js" id="id72" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/SpacingModLetters.js" id="id73" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/MiscSymbols.js" id="id74" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/GreekAndCoptic.js" id="id75" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/Main.js" id="id76" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/mml2jax.js" id="id77" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/GeometricShapes.js" id="id78" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/i.js" id="id79" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/q.js" id="id80" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/toMathML.js" id="id81" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/NativeMML/config.js" id="id82" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/BasicLatin.js" id="id83" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/boldsymbol.js" id="id84" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Typewriter/Regular/Other.js" id="id85" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/BoxDrawing.js" id="id86" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/begingroup.js" id="id87" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/v.js" id="id88" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/MiscMathSymbolsA.js" id="id89" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Math/BoldItalic/Main.js" id="id90" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Bold/Other.js" id="id91" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/menclose.js" id="id92" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/LetterlikeSymbols.js" id="id93" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Typewriter/Regular/CombDiacritMarks.js" id="id94" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/GeneralPunctuation.js" id="id95" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/MiscMathSymbolsB.js" id="id96" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/x.js" id="id97" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Regular/Other.js" id="id98" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/j.js" id="id99" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/MathZoom.js" id="id100" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/MiscMathSymbolsA.js" id="id101" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/unicode.js" id="id102" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/c.js" id="id103" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/NativeMML/jax.js" id="id104" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/SupplementalArrowsB.js" id="id105" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/autobold.js" id="id106" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/SuppMathOperators.js" id="id107" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Regular/Main.js" id="id108" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/action.js" id="id109" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/fr.js" id="id110" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/Dingbats.js" id="id111" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/Dingbats.js" id="id112" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Italic/CombDiacritMarks.js" id="id113" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/Arrows.js" id="id114" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/MathEvents.js" id="id115" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/MathOperators.js" id="id116" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Caligraphic/Bold/Main.js" id="id117" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/MathOperators.js" id="id118" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/Latin1Supplement.js" id="id119" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/CombDiacritMarks.js" id="id120" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/f.js" id="id121" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/Main.js" id="id122" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/bbox.js" id="id123" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/config.js" id="id124" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/CombDiactForSymbols.js" id="id125" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Regular/CombDiacritMarks.js" id="id126" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/Arrows.js" id="id127" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/SupplementalArrowsA.js" id="id128" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/r.js" id="id129" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/GeneralPunctuation.js" id="id130" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Italic/BasicLatin.js" id="id131" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/SuppMathOperators.js" id="id132" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/opf.js" id="id133" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/fontdata.js" id="id134" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/SpacingModLetters.js" id="id135" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/mglyph.js" id="id136" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/MathOperators.js" id="id137" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/GreekAndCoptic.js" id="id138" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/CombDiactForSymbols.js" id="id139" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Bold/Main.js" id="id140" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/w.js" id="id141" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/MiscTechnical.js" id="id142" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/Latin1Supplement.js" id="id143" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/MathOperators.js" id="id144" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/LetterlikeSymbols.js" id="id146" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/m.js" id="id147" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/GeneralPunctuation.js" id="id148" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/multiline.js" id="id149" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/LatinExtendedA.js" id="id150" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/mtable.js" id="id151" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/LatinExtendedA.js" id="id152" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/SansSerif/Italic/Main.js" id="id153" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/asciimath2jax.js" id="id154" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/jax.js" id="id155" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/AMSmath.js" id="id156" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/b.js" id="id157" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/fontdata-extra.js" id="id158" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/SuppMathOperators.js" id="id159" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/MathOperators.js" id="id160" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/s.js" id="id161" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Script/Regular/BasicLatin.js" id="id162" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Caligraphic/Regular/Main.js" id="id163" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/SpacingModLetters.js" id="id164" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/GreekAndCoptic.js" id="id165" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/BasicLatin.js" id="id166" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/SupplementalArrowsA.js" id="id167" media-type="application/x-javascript"/>
        <item href="mathjax/MathJax.js" id="id168" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/TeX/config.js" id="id169" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/BasicLatin.js" id="id170" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/CombDiacritMarks.js" id="id171" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/TeX/color.js" id="id172" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/Main.js" id="id173" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/LatinExtendedB.js" id="id174" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Italic/CombDiacritMarks.js" id="id175" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Fraktur/Regular/Other.js" id="id176" media-type="application/x-javascript"/>
        <item href="mathjax/extensions/tex2jax.js" id="id177" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/MiscTechnical.js" id="id178" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/autoload/maction.js" id="id179" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/GreekAndCoptic.js" id="id180" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Bold/LatinExtendedB.js" id="id181" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/n.js" id="id182" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/t.js" id="id183" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Typewriter/Regular/BasicLatin.js" id="id184" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/SuppMathOperators.js" id="id185" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/y.js" id="id186" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/jax.js" id="id187" media-type="application/x-javascript"/>
        <item href="mathjax/jax/input/MathML/entities/e.js" id="id188" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/LetterlikeSymbols.js" id="id189" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/AMS/Regular/Latin1Supplement.js" id="id190" media-type="application/x-javascript"/>
        <item href="mathjax/jax/element/mml/optable/CombDiacritMarks.js" id="id191" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Math/Italic/Main.js" id="id192" media-type="application/x-javascript"/>
        <item href="mathjax/jax/output/SVG/fonts/TeX/Main/Regular/Main.js" id="id193" media-type="application/x-javascript"/>-->
        <item href="Styles/fixmathml.js" id="id194" media-type="application/x-javascript"/>
        <item href="Styles/cup-bits-epub3-math.css"  id="style2" media-type="text/css"/>
        <xsl:result-document href="math.txt">hello</xsl:result-document>
    </xsl:template>
    
    
    <xsl:template match="styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']" mode="#all">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--guide has toc, index, begin reading, cover and dictionary letters-->
    <xsl:template match="book" mode="guide-root">
        <xsl:variable name="title-page" select="front-matter/front-matter-part[@book-part-type='title-page']/@filename"/>
        <guide>
            <reference type="text" title="Begin reading" href="Text/{$title-page}"/>
            <reference type="cover" title="Cover" href="Text/coverpage.html"/>
            <xsl:apply-templates select="front-matter/toc[1]" mode="guide"/>
            <xsl:apply-templates select="book-back/index[last()]" mode="guide"/>
        </guide>
    </xsl:template>
    
    <xsl:template match="toc" mode="guide">
        <reference type="toc" title="Table of Contents" href="{concat('Text/', @filename)}"/>
    </xsl:template>
    
    <xsl:template match="index" mode="guide">
        <reference type="index" title="Index" href="{concat('Text/', @filename)}"/>
    </xsl:template>

</xsl:stylesheet>
