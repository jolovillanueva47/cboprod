<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="2.0" xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param as="xs:string" name="css-filename" required="yes"/>
    <!-- must match filename of main CSS (e.g. epub or epub3 - also mobi?) -->
    <xsl:param name="css-uri-base" required="yes"/>
    <xsl:param name="profile-uri-base" required="yes"/>
    <xsl:param as="xs:string" name="isbn"/>

    <xsl:variable as="xs:anyURI" name="css-source"
        select="resolve-uri($css-filename, concat('file:',replace(replace($css-uri-base,'[A-Za-z]:',''),'\\','/'),'/'))"/>
    <xsl:variable as="document-node()" name="cbml-profile"
        select="doc(replace(concat('file:///',$profile-uri-base,'/',$isbn,'profile.xml'),'\\','/'))"/>
    <!--concat($isbn,'profile.xml')-->
    <xsl:variable name="font-families" select="$cbml-profile/book[1]/fonts/family"/>

    <xsl:output byte-order-mark="no" encoding="UTF-8" indent="yes" method="text"/>

    <xsl:template name="css-wrapper">
        <xsl:if test="not(unparsed-text-available($css-source,'UTF-8'))">
            <xsl:message terminate="yes">FATAL ERROR: CSS source files (<xsl:value-of select="string($css-source)"
                />)could not be read when embedding fonts.</xsl:message>
        </xsl:if>
        <xsl:call-template name="css-head"/>
        <xsl:call-template name="css-body"/>
        <!--<xsl:call-template name="css-tail"/>-->
        <!--if it ever proves necessary (mediaqueries?)-->
    </xsl:template>

    <xsl:template name="css-head">
        <xsl:text>/* Cambridge University Press EPUB/mobi CSS */&#x000A;</xsl:text>
        <xsl:text>/* generated </xsl:text>
        <xsl:value-of select="concat(current-date(),'-',current-time())"/>
        <xsl:text> */&#x000A;</xsl:text>
        <xsl:for-each select="$font-families">
            <xsl:variable name="type" select="if(@type) then (string(@type)) else('ttf')"/>
            <!--font delivered with the xml may be otf or ttf-->
            <xsl:variable as="xs:string" name="bin" select="concat('0000',cup:dec2bin(./@variations))"/>
            <xsl:variable as="xs:string" name="font-requirements" select="substring($bin,string-length($bin)-3)"/>
            <xsl:if test="substring($font-requirements,4,1) = '1'">
                <xsl:call-template name="css-rule.font-face">
                    <xsl:with-param name="font-weight" select="'normal'"/>
                    <xsl:with-param name="font-style" select="'normal'"/>
                    <xsl:with-param name="font-family" select="./@name"/>
                    <xsl:with-param name="font-path" select="concat(./@name,'.', $type)"/>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="substring($font-requirements,3,1) = '1'">
                <xsl:call-template name="css-rule.font-face">
                    <xsl:with-param name="font-weight" select="'bold'"/>
                    <xsl:with-param name="font-style" select="'normal'"/>
                    <xsl:with-param name="font-family" select="./@name"/>
                    <xsl:with-param name="font-path" select="concat(./@name,'-Bold.ttf')"/>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="substring($font-requirements,2,1) = '1'">
                <xsl:call-template name="css-rule.font-face">
                    <xsl:with-param name="font-weight" select="'bold'"/>
                    <xsl:with-param name="font-style" select="'italic'"/>
                    <xsl:with-param name="font-family" select="./@name"/>
                    <xsl:with-param name="font-path" select="concat(./@name,'-BoldItalic.ttf')"/>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="substring($font-requirements,1,1) = '1'">
                <xsl:call-template name="css-rule.font-face">
                    <xsl:with-param name="font-weight" select="'normal'"/>
                    <xsl:with-param name="font-style" select="'italic'"/>
                    <xsl:with-param name="font-family" select="./@name"/>
                    <xsl:with-param name="font-path" select="concat(./@name,'-Italic.ttf')"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="css-body">
        <!--TODO: be selective about what classes go in - use $cbml-profile/book[1]/element-list and assess potential of Less-->
        <xsl:value-of select="unparsed-text($css-source,'UTF-8')"/>
        <xsl:for-each select="$font-families">
            <xsl:text>.unicode-altfont-</xsl:text>
            <xsl:value-of select="./@name"/>
            <xsl:text> {font-family: '</xsl:text>
            <xsl:value-of select="./@name"/>
            <xsl:text>';}&#x000A;</xsl:text>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="css-rule.font-face">
        <xsl:param as="xs:string" name="font-weight"/>
        <xsl:param as="xs:string" name="font-style"/>
        <xsl:param as="xs:string" name="font-family"/>
        <xsl:param as="xs:string" name="font-path"/>
        <xsl:text>@font-face {&#x000A;</xsl:text>
        <xsl:text> font-family:</xsl:text>
        <xsl:value-of select="$font-family"/>
        <xsl:text>;&#x000A;</xsl:text>
        <xsl:text> font-weight:</xsl:text>
        <xsl:value-of select="$font-weight"/>
        <xsl:text>;&#x000A;</xsl:text>
        <xsl:text> font-style:</xsl:text>
        <xsl:value-of select="$font-style"/>
        <xsl:text>;&#x000A;</xsl:text>
        <xsl:text> src:url(../Fonts/</xsl:text>
        <xsl:value-of select="$font-path"/>
        <xsl:text>);&#x000A;</xsl:text>
        <xsl:text>}&#x000A;</xsl:text>
    </xsl:template>
    <!--
@font-face {
 font-family:DejaVuSans;
 font-weight:normal;
 font-style:normal;
 src: url(../Fonts/DejaVuSans.ttf);
}
@font-face {
 font-family:DejaVuSans;
 font-weight:bold;
 font-style:normal;
 src: url(../Fonts/DejaVuSans-Bold.ttf);
}
@font-face {
 font-family:DejaVuSans;
 font-weight:bold;
 font-style:italic;
 src: url(../Fonts/DejaVuSans-BoldOblique.ttf);
}
@font-face {
 font-family:DejaVuSans;
 font-weight:normal;
 font-style:italic;
 src: url(../Fonts/DejaVuSans-Oblique.ttf);
}
    -->
    <xsl:function as="xs:string" name="cup:dec2bin">
        <xsl:param as="xs:integer" name="dec"/>
        <xsl:sequence
            select="if ($dec eq 0) then '0' else concat(if ($dec ge 2) then cup:dec2bin($dec idiv 2) else '',substring('01',($dec mod 2) + 1,1))"
        />
    </xsl:function>
</xsl:stylesheet>
