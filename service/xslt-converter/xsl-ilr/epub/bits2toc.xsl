<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="#all" version="2.0" xmlns:cup="http://contentservices.cambridge.org"
    xmlns:epub="http://www.idpf.org/2007/ops" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--  made from the headings in the book  -->
    <!--    the output is to be the input for both bits2nav and bits2ncx
        decisions as to what is to be included in the nav file and the ncx file are to be made here-->

    <xsl:output indent="yes" method="xml"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="book">
        <xsl:copy>
            <!--            metadata required for ncx-->
            <book-meta>
                <xsl:copy-of select="book-meta/book-id"/>
                <xsl:copy-of select="book-meta/book-title"/>
                <page-count>
                    <xsl:value-of select="count(descendant::named-content[@content-type='page'])"/>
                </page-count>
                <lpage>
                    <xsl:value-of select="descendant::named-content[@content-type='page'][last()]"/>
                </lpage>
            </book-meta>
            <xsl:apply-templates select="*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="book-part">
        <xsl:copy>
            <xsl:apply-templates mode="attributes" select="."/>
            <xsl:apply-templates select="book-part-meta/title-group/*[not(self::alt-title)]" mode="title"/>
            <xsl:apply-templates select="*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="sec">
        <xsl:copy>
            <xsl:apply-templates mode="attributes" select="."/>
            <xsl:apply-templates select="label| title| subtitle" mode="title"/>
            <xsl:if test="not(label or title or subtitle)"><title>Section</title></xsl:if>
            <xsl:apply-templates select="*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="label | title | subtitle" mode="title">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()[not(self::target or self::named-content[@content-type='page'])]"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="target"/>

    <xsl:template match="front-matter/*|book-back/*">
        <xsl:copy>
            <xsl:apply-templates mode="attributes" select="."/>
            <xsl:apply-templates select="book-part-meta/title-group/*[not(self::alt-title)] | title-group/*[not(self::alt-title)] | title | subtitle | label" mode="title"/>
            <xsl:if test="not(title-group or title or label or book-part-meta/title-group or @book-part-type='series-page')">
                <title>
                    <xsl:value-of select="cup:titleName(.)"/>
                </title>
            </xsl:if>
            <xsl:apply-templates select="*"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="named-content[@content-type='page']">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="*"><xsl:apply-templates select="*"/></xsl:template>

    <xsl:template match="*" mode="attributes">
        <xsl:attribute name="id" select="@id"/>
        <xsl:choose>
            <xsl:when test="@chunk='yes'">
                <xsl:attribute name="filename" select="@filename"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="in-file" select="@filename"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!--
        from the spec:
        'Items before the contents list will be listed by their name – Half title, imprints page, epigraph, dedication, etc.'
        so no need for other generated names?
    -->
    <xsl:function as="xs:string" name="cup:titleName">
        <!--TODO: generalise this further - place type/name pairs in a stylesheet variable for overriding where necessary-->
        <xsl:param as="element()" name="elem"/>
        <xsl:variable as="attribute()*" name="type" select="$elem/@book-part-type"/>
        <xsl:choose>
            <xsl:when test="$elem[self::book-part | self::dedication | self::front-matter-part]">
                <xsl:choose>
                    <xsl:when test="$elem/self::dedication[not(@book-part-type)]">Dedication</xsl:when>
                    <xsl:when test="$elem/self::dedication[@book-part-type='epigraph']">Epigraph</xsl:when>
                    <xsl:when test="$type='act'">Act</xsl:when>
                    <xsl:when test="$type='answers'">Answers</xsl:when>
                    <xsl:when test="$type='backmatter'">Endmatter</xsl:when>
                    <xsl:when test="$type='biographical-information'">Biography</xsl:when>
                    <xsl:when test="$type='case-digest-headings'">Case digest</xsl:when>
                    <xsl:when test="$type='case-report'">Case report</xsl:when>
                    <xsl:when test="$type='case-report-group'">Case reports</xsl:when>
                    <xsl:when test="$type='chapter'">Chapter</xsl:when>
                    <xsl:when test="$type='characters'">Characters</xsl:when>
                    <xsl:when test="$type='chronology'">Chronology</xsl:when>
                    <xsl:when test="$type='conclusion'">Conclusion</xsl:when>
                    <xsl:when test="$type='contributors'">Contributors</xsl:when>
                    <xsl:when test="$type='dictionary'">Dictionary</xsl:when>
                    <xsl:when test="$type='dictionary-entry'">Entry</xsl:when>
                    <xsl:when test="$type='dictionary-letter'">Letter</xsl:when>
                    <xsl:when test="$type='dictionary-stub-entry'">Entry</xsl:when>
                    <xsl:when test="$type='document'">Document</xsl:when>
                    <xsl:when test="$type='document-endmatter'">Document</xsl:when>
                    <xsl:when test="$type='document-group'">Documents</xsl:when>
                    <xsl:when test="$type='document-prelims'">Document</xsl:when>
                    <xsl:when test="$type='drug'">Drug</xsl:when>
                    <xsl:when test="$type='editorial-procedures'">Editorial procedures</xsl:when>
                    <xsl:when test="$type='endmatter-answers'">Answers</xsl:when>
                    <xsl:when test="$type='epigraph'">Epigraph</xsl:when>
                    <xsl:when test="$type='endnotes'">Notes</xsl:when>
                    <xsl:when test="$type='exercises'">Exercises</xsl:when>
                    <xsl:when test="$type='figures'">Figures</xsl:when>
                    <xsl:when test="$type='frontispiece'">Frontispiece</xsl:when>
                    <xsl:when test="$type='half-title-page'">Half title</xsl:when>
                    <xsl:when test="$type='imprint-page'">Imprints page</xsl:when>
                    <xsl:when test="$type='introduction'">Introduction</xsl:when>
                    <xsl:when test="$type='law-case'">Case</xsl:when>
                    <xsl:when test="$type='law-case-headings'">Case headings</xsl:when>
                    <xsl:when test="$type='maps'">Maps</xsl:when>
                    <xsl:when test="$type='opinion'">Opinion</xsl:when>
                    <xsl:when test="$type='original-prelims'">Frontmatter</xsl:when>
                    <xsl:when test="$type='original-text'">Original text</xsl:when>
                    <xsl:when test="$type='other'">Uncategorised book part</xsl:when>
                    <xsl:when test="$type='part'">Part</xsl:when>
                    <xsl:when test="$type='patient-file'">Patient file</xsl:when>
                    <xsl:when test="$type='plates'">Plates</xsl:when>
                    <xsl:when test="$type='play'">Play</xsl:when>
                    <xsl:when test="$type='prelude'">Prelude</xsl:when>
                    <xsl:when test="$type='prologue'">Prologue</xsl:when>
                    <xsl:when test="$type='reviews'">Reviews</xsl:when>
                    <xsl:when test="$type='scene'">Scene</xsl:when>
                    <xsl:when test="$type='series-page'">Series page</xsl:when>
                    <xsl:when test="$type='stage'">Stage</xsl:when>
                    <xsl:when test="$type='sub-book-endmatter'">Endmatter</xsl:when>
                    <xsl:when test="$type='tables'">Tables</xsl:when>
                    <xsl:when test="$type='theatre-listing'">Theatres</xsl:when>
                    <xsl:when test="$type='title-page'">Title page</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                            select="error(xs:QName('cup:EPUB001'),concat('Unknown book part type ''',if ($type eq '') then '{empty}' else $type,''' encountered on ',$elem/local-name(),' with parent ',$elem/parent::*/local-name(),'.'))"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$elem[self::fn-group | self::notes]">Notes</xsl:when>
                    <xsl:when test="$elem[self::app-group]">Appendices</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                        select="error(xs:QName('cup:EPUB001'),concat('Unknown book part type ''',if ($type eq '') then '{empty}' else $type,''' encountered on ',$elem/local-name(),' with parent ',$elem/parent::*/local-name(),'.'))"
                    />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
<!--    
    <xsl:template match="styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']" mode="#all">
        <xsl:apply-templates/>
    </xsl:template>-->

</xsl:stylesheet>
