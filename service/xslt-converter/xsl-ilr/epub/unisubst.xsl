<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:cup="http://contentservices.cambridge.org/"
    xmlns:mml="http://www.w3.org/1998/Math/MathML" 
    exclude-result-prefixes="#all" version="2.0">

    <xsl:output encoding="UTF-8" indent="no" method="xml"/>
    
    <xsl:variable name="regex.combining" as="xs:string" select="'[\p{IsCombiningDiacriticalMarks}\p{IsCombiningHalfMarks}\p{IsCombiningMarksforSymbols}]'"/>
    <!--regex to match combining characters-->
    <xsl:variable name="regex.grk" as="xs:string" select="'([Ͱ-ϡ]|[ϰ-Ͽ]|\p{IsGreekExtended})'"/>
    <!--regex to match Greek chars to trigger GFS font insertion
        exercise caution when editing unescaped regex - first range is U+0370 to U+03E1, second is U+03F0 to U+03FF (basic Greek, omitting Coptic)
    -->
    <xsl:variable name="regex.fullhalf" as="xs:string" select="'\p{IsHalfwidthandFullwidthForms}'"/>
    <!--regex to match fullwidth and halfwidth letter forms-->
    <xsl:variable name="regex.hangul" as="xs:string" select="'\p{IsHangulJamo}|\p{IsHangulCompatibilityJamo}|\p{IsHangulJamoExtended-A}|\p{IsHangulSyllables}|\p{IsHangulJamoExtended-B}'"/>

    <xsl:template match="@* | node()[not(self::text() or ancestor-or-self::mml:math)]">
        <!--if a node isn't a text node, there's nothing worth replacing in it-->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*[ancestor-or-self::mml:math]" mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="math"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="text()" mode="math">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="text()" priority="2.0">
        <xsl:variable name="raw.spans" as="node()+">
            <xsl:apply-templates select="." mode="do.subst"/>
        </xsl:variable>
        <xsl:for-each-group select="$raw.spans" group-adjacent="if (self::styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']) then @style else 'no'">
            <xsl:choose>
                <xsl:when test="not(current-grouping-key() eq 'no')">
                    <!--current group is not a font-substituted span-->
                    <xsl:element name="{local-name(current-group()[1])}">
                        <xsl:copy-of select="current-group()[1]/@*[not(local-name() eq 'alt')]"/>
                        <xsl:attribute name="alt" select="string-join(current-group()//@alt,' ')"/>
                        <xsl:copy-of select="current-group()//text()"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <!--current group is a series of spans with the same font substituted-->
                    <xsl:copy-of select="current-group()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each-group>
    </xsl:template>

    <xsl:template match="text()" mode="do.subst" priority="2.0">
        <!--priority bumped up to ensure inbuilt text template doesn't get used-->
        <xsl:variable name="pstring" as="xs:string" select="string(.)"/>
        <!--get the whole text of the element into a var-->
        <xsl:for-each select="1 to string-length($pstring)">
            <xsl:variable name="char" as="xs:string" select="substring($pstring,.,1)"/>
            <!--$char becomes each character of the string in turn-->
            <xsl:variable name="is.combining" as="xs:boolean"
                select="matches($char,$regex.combining)"/>
            <!--true if $char is in any of the specified blocks-->
            <xsl:variable name="unicode.plane" as="xs:integer" select="string-to-codepoints($char) idiv 65536"/>
            <!--determine what plane the character is in for future use-->
            <xsl:choose>
                <!--test the codepoint of the char to see if it's one we want to replace-->
                <xsl:when
                    test="not(number(string-to-codepoints($char) = 8722)) and (
            ((number(string-to-codepoints($char)) gt 255) and (number(string-to-codepoints($char)) lt 994))
            or 
            ((number(string-to-codepoints($char)) gt 1007) and (number(string-to-codepoints($char)) lt 8191)) 
            or 
            number(string-to-codepoints($char)) eq 8266
            or
            number(string-to-codepoints($char)) eq 8214
            or 
            number(string-to-codepoints($char)) gt 8341)">
                    <xsl:variable name="hex" select="concat('0000',cup:int-to-hex(string-to-codepoints($char)))" as="xs:string"/>
                    <!--get the hex equivalent of the codepoint; pad it with zeroes-->
                    <xsl:element exclude-result-prefixes="#all" name="styled-content">
                        <xsl:attribute name="specific-use" select="'epub'"/>
                        <xsl:attribute name="style-type" select="'substitute'"/>
                        <xsl:attribute name="alt">
                            <xsl:value-of
                                select="substring($hex,string-length($hex) - (if
                    ($unicode.plane eq 0) 
                    then 3 
                    else (
                        if($unicode.plane eq 16) 
                        then 1 
                        else 2)))"/>
                            <!--chop off all but the relevant chars of the padded string - if greater than 65535 then we're in plane 1 and need a fifth, etc.-->
                        </xsl:attribute>
                        <xsl:variable name="font.by-block" as="xs:string">
                            <xsl:choose>
                                <xsl:when test="matches($char,$regex.grk) 
                                    or ($is.combining and matches(cup:findLastMatchingChar(substring($pstring,1,.),$regex.combining),$regex.grk))">
                                    <!--Greek text - subset of two blocks: basic Greek skipping Coptic, Greek Extended-->
                                    <xsl:value-of select="'GFSNeohellenicStd'"/>
                                </xsl:when>
                                <xsl:when test="$is.combining and not(matches(cup:findLastMatchingChar(substring($pstring,1,.),$regex.combining),$regex.grk))">
                                    <xsl:value-of select="'DejaVuSans'"/>
                                </xsl:when>
                                <xsl:when
                                    test="(string-to-codepoints($char) ge 11904) and (string-to-codepoints($char) le 40959) or matches($char,$regex.fullhalf)">
                                    <!--CJK block-->
                                    <xsl:value-of select="'wqy-zenhei'"/>
                                </xsl:when>
                                <xsl:when
                                    test="(string-to-codepoints($char) ge 2457) and (string-to-codepoints($char) le 4256)">
                                    <!--Burmese-->
                                    <xsl:value-of select="'ZawgyiOne'"/>
                                </xsl:when>
                                <xsl:when test="matches($char,$regex.hangul)">
                                    <!--Hangul block-->
                                    <xsl:value-of select="'NanumGothic'"/>
                                </xsl:when>
                                <xsl:when test="matches($char,'\p{IsMusicalSymbols}')">
                                    <!--Musical Notation block (U+1D1xx)-->
                                    <xsl:value-of select="'CUPMusicalSymbols'"/>
                                </xsl:when>
                                <xsl:when test="matches($char,'\p{IsMathematicalAlphanumericSymbols}')">
                                    <!--Mathematical Alphanumeric Symbols block (U+1D400-1D7FF)-->
                                    <xsl:value-of select="'CUPMathematicalAlphanumericSymbols'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="'DejaVuSans'"/>
                                </xsl:otherwise>
                                <!--fall back to DJV-->
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:attribute name="style" select="$font.by-block"/>
                        <!--incorporate Unicode plane into logic-->
                        <xsl:value-of select="$char"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$char"/>
                </xsl:otherwise>
                <!--char is okay for chosen subset of unicode, so NFA-->
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:function name="cup:int-to-hex" as="xs:string">
        <xsl:param name="in" as="xs:integer"/>
        <!--the codepoint of the char (or a component part thereof fed back in)-->
        <xsl:sequence
            select="
      if ($in eq 0) 
      then '0' 
      else concat(
                    if ($in ge 16) 
                    then cup:int-to-hex($in idiv 16) 
                    else '',
                  substring('0123456789ABCDEF', 
                            ($in mod 16) + 1, 
                            1)
                  )"/>
        <!--turn the decimal int into its hex equivalent - adapted from a post on Oxygen forums by Michael Kay-->
    </xsl:function>
    
    <xsl:function name="cup:findLastMatchingChar" as="xs:string">
        <!--wind back through a sequence of strings and return the last char that does not match a given regex-->
        <xsl:param name="s" as="xs:string"/>
        <xsl:param name="r.match" as="xs:string"/>
        <xsl:variable name="s.seq" as="xs:string*" select="for $t in string-to-codepoints($s) return codepoints-to-string($t)"/>
        <xsl:choose>
            <xsl:when test="matches($s.seq[position() eq last()],$r.match)">
                <xsl:value-of select="cup:findLastMatchingChar(substring($s,1,count($s.seq)-1),$r.match)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$s.seq[position() eq last()]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>
