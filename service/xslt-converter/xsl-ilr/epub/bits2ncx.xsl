<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns="http://www.daisy.org/z3986/2005/ncx/" 
    exclude-result-prefixes="#all" 
    version="2.0">
    
<!--  made from the headings in the book  -->
    
<!--    this acts on the file toc.xml - this file includes only those elements to be included in the navigation list and is 
    used to make both the ncx file and the nav file.
    Any decisions as to which items to include in the nav and ncx files are to be made in the process creating the file toc.xml, not here-->
    
    <xsl:import href="../functions/functions.xsl"/>
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

     <xsl:param name="isbn"/>
    <xsl:variable name="image-path" select="'../Images/'"/>

    
    <xsl:template match="/">
        
            <xsl:apply-templates select="book|book-part-wrapper"/>
        
    </xsl:template>
    
<!--    to do - book-part-wrapper-->
    
    <xsl:template match="book" priority="10">
        <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/"
            version="2005-1"
            xml:lang="en">
            <head>
                <meta name="dtb:uid" content="{book-meta/book-id}"/>
                <meta name="dtb:depth" content="todo"/>
                <meta name="dtb:totalPageCount" content="{count(descendant::named-content[@content-type='page'])}"/>
                <meta name="dtb:maxPageNumber" content="{string(descendant::named-content[@content-type='page'][last()])}"/>
            </head>
            <docTitle>
                <text><xsl:value-of select="book-meta/book-title-group/book-title"/></text>
            </docTitle>
        <navMap>
            <navPoint id="cover1" playOrder="1">
                <navLabel>
                    <text>Coverpage</text>
                </navLabel>
                <content src="Text/coverpage.html"/>
            </navPoint>
                <xsl:apply-templates select="*[(label or title) and not(self::named-content[contains(@content-type, 'page')])]"/>
        </navMap>
        </ncx>
    </xsl:template>
    

    
    <xsl:template match="*">
        <navPoint id="{@id}">
            <xsl:attribute name="playOrder" select="count(preceding::*[label or title]) + count(ancestor::*[label or title])+2"/>
             <navLabel>
                 <text>
                    <xsl:if test="label">
                        <xsl:apply-templates select="label" mode="title"/>
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="title" mode="title"/>
                 </text>
              </navLabel>
            <content>
                <xsl:attribute name="src">
                    <xsl:choose>
                        <xsl:when test="@filename"><xsl:value-of select="concat('Text/',@filename)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="concat('Text/',@in-file, '#', @id)"></xsl:value-of></xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </content>
            <xsl:if test="*[not(self::label or self::title or self::named-content)]">
                <xsl:apply-templates select="*[(label or title) and not(self::label or self::title or self::named-content[contains(@content-type, 'page')])]"/>
            </xsl:if>
        </navPoint>    

    </xsl:template>
    
    <xsl:template match="label | title" mode="title">
        <xsl:apply-templates mode="title"/>
    </xsl:template>
    
    <xsl:template match="named-content[@content-type='page']" mode="#all"/>
      
    
</xsl:stylesheet>
