<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="2.0" xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param as="xs:string" name="css-filename" required="yes"/>
    <!-- must match filename of main CSS (e.g. epub or epub3 - also mobi?) -->
    <xsl:param name="css-uri-base" required="yes"/>
    <xsl:param as="xs:string" name="isbn"/>

    <xsl:variable as="xs:anyURI" name="css-source"
        select="resolve-uri($css-filename, concat('file:',replace(replace($css-uri-base,'[A-Za-z]:',''),'\\','/'),'/'))"/>

    <xsl:output byte-order-mark="no" encoding="UTF-8" indent="yes" method="text"/>

    <xsl:template name="css-wrapper">
        <xsl:if test="not(unparsed-text-available($css-source,'UTF-8'))">
            <xsl:message terminate="yes">FATAL ERROR: CSS source files (<xsl:value-of select="string($css-source)"
                />)could not be read when embedding fonts.</xsl:message>
        </xsl:if>
        <xsl:call-template name="css-body"/>
        <!--if it ever proves necessary (mediaqueries?)-->
    </xsl:template>


    <xsl:template name="css-body">
        <xsl:value-of select="unparsed-text($css-source,'UTF-8')"/>
        <xsl:comment>------------------------------------ Media queries </xsl:comment>
        <xsl:text>
@media amzn-mobi7 {
h1{font-size:130%;font-weight:bold;}
h2{font-size:125%;font-weight:bold;}
h3{font-size:120%;font-weight:bold;}
h4{font-size:115%;font-weight:bold;}
h5{font-size:110%;font-weight:bold;}
h6{font-size:105%;font-weight:bold;}
div.break{width:100%;text-align:center;}.unicode-altfont-DejaVuSans {font-family: 'DejaVuSans';}
}
        </xsl:text>
    </xsl:template>

</xsl:stylesheet>
