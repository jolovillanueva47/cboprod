<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cup="http://contentservices.cambridge.org" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:oebpackage="http://openebook.org/namespaces/oeb-package/1.0/" 
    xmlns:opf="http://www.idpf.org/2007/opf"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns="http://www.idpf.org/2007/opf" 
    exclude-result-prefixes="cup oebpackage xs"
    version="2.0">


<!--default -->
    
    <xsl:template match="opf:spine" priority="10">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="page-map" select="'pages'"/>
            <xsl:apply-templates/>
            <item id="pages" href="oebps-page-map.xml" media-type="application/oebps-page-map+xml"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
