<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xlink cup xs" 
    version="2.0">
    
<!--  made from the headings in the book  -->
    
<!--    this acts on the file toc.xml - this file includes only those elements to be included in the navigation list and is 
    used to make both the ncx file and the nav file.
    Any decisions as to which items to include in the nav and ncx files are to be made in the process creating the file toc.xml, not here-->
    
    <xsl:import href="../functions/functions.xsl"/>
    
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>

     <xsl:param name="isbn"/>
    <xsl:variable name="image-path" select="'../Images/'"/>

    
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
            <head>
                <meta charset="utf-8"/>
                <link rel="stylesheet" type="text/css" href="../Styles/cup-bits-epub3.css"/>
            </head>
            <body epub:type="frontmatter">
                     <xsl:apply-templates select="book"/> 
                <xsl:apply-templates select="book" mode="pagelist"/> 
            </body>
        </html>         
    </xsl:template>
    
    <xsl:template match="book" priority="10">
        <nav  epub:type="toc" id="toc" hidden="">
            <h1>Contents</h1>  
            <ol> 
                    <li>
                        <a href="coverpage.html">Cover</a>
                    </li>
                <xsl:apply-templates select="*[(label or title) and not(self::named-content[contains(@content-type, 'page')])]"/>
            </ol>
        </nav>
        <nav epub:type="landmarks" hidden="">
            <ol>
                <li>
                    <a href="coverpage.html" epub:type="cover">Cover</a>
                </li>
                <xsl:apply-templates select="toc[1]" mode="landmarks"/>
                <xsl:apply-templates select="index[last()]" mode="landmarks"/>
            </ol>
        </nav>
    </xsl:template>
    

    
    <xsl:template match="*">
                <li>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:choose>
                                <xsl:when test="@filename"><xsl:value-of select="@filename"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="concat(@in-file, '#', @id)"></xsl:value-of></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        
                        <xsl:if test="label">
                            <xsl:apply-templates select="label" mode="title"/>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:apply-templates select="title" mode="title"/>
                    </a>
                    <xsl:if test="*[not(self::label or self::title or self::named-content)]">
                        <ol>
                            <xsl:apply-templates select="*[(label or title) and not(self::label or self::title or self::named-content[contains(@content-type, 'page')])]"/>
                        </ol>
                    </xsl:if>
                </li>     
    </xsl:template>
    
    <xsl:template match="label | title" mode="title">
        <xsl:apply-templates mode="title"/>
    </xsl:template>
    
    <xsl:template match="*" mode="title">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    
    <xsl:template match="named-content[@content-type='page']" mode="#all"/>
    
    <xsl:template match="toc" mode="landmarks">
        <li>
            <a href="{@filename}" epub:type="toc">Contents</a>
        </li> 
    </xsl:template>
    
    <xsl:template match="index" mode="landmarks">
        <li>
            <a href="{@filename}" epub:type="index">Index</a>
        </li> 
    </xsl:template>
    
<!--    ================  PAGE LIST ==============-->
    
    <xsl:template match="book" mode="pagelist">
        <nav epub:type="page-list">
            <ol>
                <xsl:apply-templates select="descendant::named-content[contains(@content-type, 'page')]" mode="pagelist"/>
            </ol>
        </nav>
    </xsl:template>
    
    <xsl:template match="named-content[contains(@content-type, 'page')]" mode="pagelist" priority="10">
        <xsl:variable name="fnm" select="ancestor::*[@filename][1]/@filename"/>
        <li>
            <a href="{concat($fnm, '#page_', string(.))}">
                <xsl:value-of select="."/>
            </a>
        </li>    
        
    </xsl:template>
   
   
    <xsl:template match="text()"  mode="title" priority="10">
        <xsl:value-of select="translate(., '&#x00A0;', ' ')"/>
    </xsl:template>

    
</xsl:stylesheet>
