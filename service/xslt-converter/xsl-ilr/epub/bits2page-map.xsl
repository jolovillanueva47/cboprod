<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns="http://www.idpf.org/2007/opf"
    exclude-result-prefixes="#all" 
    version="2.0">
    
<!--  made from the headings in the book  -->
    
<!--    this acts on the file toc.xml - this file includes only those elements to be included in the navigation list and is 
    used to make both the ncx file and the nav file.
    Any decisions as to which items to include in the nav and ncx files are to be made in the process creating the file toc.xml, not here-->
    
    <xsl:import href="../functions/functions.xsl"/>
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

     <xsl:param name="isbn"/>
   

    
    <xsl:template match="/">
        <page-map>
            <xsl:apply-templates select="descendant::named-content[@content-type='page']"/>
        </page-map> 
    </xsl:template>
    
    <xsl:template match="named-content[@content-type='page']">
        <page>
            <xsl:attribute name="name" select="string(.)"/>
            <xsl:attribute name="href" select="concat('Text/',parent::*/@filename|parent::*/@in-file, '#page_', string(.))"/>
        </page>
    </xsl:template>
  
</xsl:stylesheet>
