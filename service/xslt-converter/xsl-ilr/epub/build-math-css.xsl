<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="2.0" xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output byte-order-mark="no" encoding="UTF-8" indent="yes" method="text"/>
    <xsl:param as="xs:string" name="css-filename" required="yes"/>
    <xsl:param name="css-uri-base" required="yes"/>
    <xsl:variable as="xs:anyURI" name="css-source"
        select="resolve-uri($css-filename, concat('file:',replace(replace($css-uri-base,'[A-Za-z]:',''),'\\','/'),'/'))"/>
    
    <xsl:template name="css-wrapper">
        <xsl:call-template name="css-body"/>
    </xsl:template>

    <xsl:template name="css-body">
        <xsl:value-of select="unparsed-text($css-source,'UTF-8')"/>
         <xsl:text>.math-graphic {display:none;}</xsl:text>
    </xsl:template>
  
</xsl:stylesheet>
