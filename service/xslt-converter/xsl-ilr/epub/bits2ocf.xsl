<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns="urn:oasis:names:tc:opendocument:xmlns:container"
    exclude-result-prefixes="xlink cup"
    version="2.0">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
    <xsl:apply-templates/>
    </xsl:template>
    
<!--    opf file to be called 'package.opf'-->
    <xsl:template match="book">
                <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
            <rootfiles>
                <rootfile full-path="OEBPS/package.opf"
                    media-type="application/oebps-package+xml" />     
            </rootfiles>
        </container>
    </xsl:template>
    
</xsl:stylesheet>
