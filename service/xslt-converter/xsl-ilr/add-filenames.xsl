<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="cup xlink xs" version="2.0">
   
    <!--  
     DEPENDENCY: valid CUP-BITS file (now generalised from  ILR)
     PURPOSE: pre-process file before transforming to html files:
        Every element to be a root element in html: adds attribute filename (containing the filename of its html file) and chunk='yes'
        Every element with attribute id or rid: adds attribute filename of the parent html file
     LOOKUP: chunks.xml - a list of each element that is to be the root element of an html file
     INPUT PARAMETERS: none
     USES FUNCTIONS: cup:Filename
     PIPELINE: follow by conversion to html with e.g. bits2html or bits2epub
     REQUIREMENTS:
        filename to reference volume number and page range for references between files
-->
    
    <xsl:output method="xml" indent="no"/>

    <xsl:variable name="bookType" select="/book/@book-type"/>
    <xsl:variable name="chunk-list" select="document('chunks.xml')"/>
    <!-- Not needed anymore for file naming:
    <xsl:variable name="volume" select="book/collection-meta/volume-in-collection/volume-number"/>
    <xsl:variable name="volume-number">
        <xsl:choose>
            <xsl:when test="contains(string($volume), ' ')">
                <xsl:value-of select="substring-after($volume, ' ')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$volume"></xsl:value-of>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="isbn" as="xs:string">
        <xsl:choose>
            <xsl:when test="/book/book-meta/book-id[contains(@book-id-type, 'isbn')]">
                <xsl:value-of select="replace(/book/book-meta/book-id[contains(@book-id-type, 'isbn')][1]/text(), '\s', '')"/>
            </xsl:when>
            <xsl:when test="/book/book-meta/isbn">
                <xsl:value-of select="replace(/book/book-meta/isbn[1]//text(), '\s', '')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">Books must have at least one ISBN</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- consult the chunk list document to see whether an element is to be an html chunk-->
    <xsl:template match="*">
        <xsl:param name="file"/><!--set when the template is called - contains the filename of the ancestor 'chunk' element-->
        <xsl:variable name="nm" select="local-name()"/>
        <xsl:variable name="parent" select="local-name(parent::*)"/>
        <xsl:variable name="chunk">
            <xsl:choose>
                <xsl:when test="
                    (
                        $chunk-list/chunks/elem[@name=$nm]
                        or $chunk-list/chunks/*/elem[@name=$nm and name(parent::*) = $parent]
                        or self::app
                    )
                    and not(
                        ancestor::book-part[@book-part-type='case-report' or @book-part-type='case-report-xref'] 
                        or descendant::book-part[@book-part-type='case-report' or @book-part-type='case-report-xref']
                        or (self::book-part[@book-part-type='part'] and descendant::book-part)
                        or (self::toc and ancestor::book-back)
                        or self::app-group
                    )
                    ">yes</xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:choose>
                <xsl:when test="$chunk='yes'">
                    <xsl:if test="
                        not(descendant::book-part[@book-part-type='part'])
                        and not(
                            @book-part-type='case-report-xref'
                            and preceding-sibling::book-part[@book-part-type='case-report-xref']
                        )">
                        <xsl:attribute name="chunk" select="'yes'"/>
                    </xsl:if>
                    <xsl:variable name="fileName" select="cup:Filename(.)"/>
                    <xsl:if test="$fileName">
                        <xsl:attribute name="filename" select="$fileName"/>
                    </xsl:if>
                    <xsl:apply-templates>
                        <xsl:with-param name="file" select="$fileName"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="(@id or @rid) and not($file = '')"><!-- only make a filename attr if we have a filename to put in it-->
                    <xsl:attribute name="filename" select="$file"/>
                    <xsl:apply-templates>
                        <xsl:with-param name="file" select="$file"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates>
                        <xsl:with-param name="file" select="$file"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="mml:*" priority="10">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="processing-instruction()">
        <xsl:copy-of select="."></xsl:copy-of>
    </xsl:template>
    

    <xsl:function name="cup:Filename" as="xs:string">
        <xsl:param name="element" as="element()"/>
        
        <!-- Build a filename (of the HTML page on CBO on which, or in which, this element will be displayed.) -->
        
        <xsl:choose>
            <xsl:when test="$bookType = 'ilr'">
                <xsl:variable name="prefix" as="xs:string">
                    <xsl:value-of select="concat($isbn, '_')"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$element/@book-part-type='case-report'">
                        <xsl:variable name="caseNum" select="count($element/preceding::book-part[@book-part-type='case-report']) + 1"/>
                        <xsl:value-of select="concat($prefix, 'c', $caseNum, '.html')"/>
                    </xsl:when>
                    <xsl:when test="$element/@book-part-type='case-report-xref'">
                        <xsl:variable name="firstStubInGroup" select="
                            if ($element/preceding-sibling::book-part[@book-part-type='case-report-xref'])
                            then $element/preceding-sibling::book-part[@book-part-type='case-report-xref'][last()]
                            else $element"/>
                        <xsl:variable name="stubNum" select="count($firstStubInGroup/preceding::book-part[@book-part-type='case-report-xref']) + 1"/>
                        <xsl:value-of select="concat($prefix, 's', $stubNum, '.html')"/>
                    </xsl:when>
                    <xsl:when test="($element/@book-part-type='part' or $element/@book-part-type='chapter')
                        and not($element/descendant::book-part)">
                        <xsl:variable name="partNum" select="count($element/preceding::book-part[@book-part-type='part' or @book-part-type='chapter']) + count($element/ancestor::book-part[@book-part-type='part' or @book-part-type='chapter']) + 1"/>
                        <xsl:value-of select="concat($prefix, 'o', $partNum, '.html')"/>
                    </xsl:when>
                    <xsl:when test="$element/parent::front-matter and not($element/ancestor::book-body or $element/ancestor::book-back)">
                        <xsl:variable name="frontMatterNum" select="count($element/preceding-sibling::*) + 1"/>
                        <xsl:value-of select="concat($prefix, 'f', $frontMatterNum, '.html')"/>
                    </xsl:when>
                    <xsl:when test="$element/ancestor::book-back and not($element/self::app-group) and not($element/self::app) and not($element/descendant::book-part)">
                        <xsl:variable name="backMatterNum" select="count($element/preceding-sibling::*[not(self::app-group)]) + 1 + number(if ($element/parent::*[self::index-group]) then count($element/parent::*[self::index-group]/preceding-sibling::*[not(self::app-group)]) else '0')"/>
                        <xsl:value-of select="concat($prefix, 'b', $backMatterNum, '.html')"/>
                    </xsl:when>
                    <xsl:when test="$element/self::app">
                        <xsl:variable name="appendixNum" select="count($element/preceding-sibling::*) + 1"/>
                        <xsl:value-of select="concat($prefix, 'a', $appendixNum, '.html')"/>
                    </xsl:when>
                    <!-- Note, the old code, using page ranges in filenames, but that won't work in early volumes where there are mini-cases inside one page -->
                    <xsl:otherwise>
                        <!-- Nothing else in an ILR volume should be chunked. But this 
                             function must return a string, so output an empty one. -->
                        <xsl:text></xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <!-- The default for anything other than ILR, and probably what is 
                     expected by later stages of the BITS2EPUB scripts? -->
                <xsl:variable name="elem" select="local-name($element)" as="xs:string"/>
                <xsl:variable name="count"
                    select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1" as="xs:double"/>
                <xsl:variable name="total"
                    select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "
                    as="xs:double"/>
                <xsl:choose>
                    <xsl:when test="$total &gt; 1">
                        <xsl:value-of select="concat($elem, $count , '.html')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($elem, '.html')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>