<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cup xlink xs" version="2.0" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:epub="http://www.idpf.org/2007/ops"
    xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--    acts on a bits file that has had the filenames of the individual html files added as att filename-->

    <!--  include the default bits2html  -->
    <xsl:import href="cup-bits_baseline-html/kernel/kernel.xsl"/>

    <xsl:param name="bits2epub-version" required="no" select="'DEBUG'"/>
    <xsl:param as="xs:string" name="path.css" required="no" select="'../Styles/'"/>
    <xsl:param as="xs:string" name="path.img" required="no" select="'../Images/'"/>

    <xsl:variable as="xs:string" name="book-title" select="/book/book-meta/book-title-group/book-title"/>
    <xsl:variable as="xs:string" name="volume" select="/book/collection-meta/volume-in-collection/volume-number"/>
    <xsl:variable as="element()*" name="lookup.tgts">
        <xsl:for-each select="//@id">
            <cup:target filename="{parent::node()/ancestor-or-self::node()[@filename][1]/@filename}" id="{string()}"/>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable as="element()*" name="lookup.tgts.rid">
        <xsl:for-each select="//@rid[not(ancestor::fn)]"><!--TESTME: why exclude fn?-->
            <cup:target filename="{parent::node()/ancestor-or-self::node()[@filename][1]/@filename}" rid="{string()}"/>
        </xsl:for-each>
    </xsl:variable>

    <xsl:key match="*" name="element-by-id" use="@id"/>
    <xsl:key match="*" name="element-by-rid" use="@rid"/>

    <!--method xml causes problems with text underlining in firefox-->
    <xsl:output encoding="UTF-8" indent="no"/>

    <!--creates a variable containing all the content to go in a single html file
    all other templates act on this variable-->
    <xsl:template match="/">
        <xsl:for-each select="descendant::*[@chunk][@filename]">
            <xsl:variable as="document-node()" name="document">
                <xsl:document>
                    <!--<xsl:copy-of select="."/>-->
                    <xsl:apply-templates mode="document" select="."/>
                </xsl:document>
            </xsl:variable>
            <xsl:result-document href="{@filename}">
                <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html></xsl:text>
                <xsl:apply-templates mode="kernel" select="$document"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <!--output is all descendants not themselves in a separate chunk-->
    <xsl:template match="*" mode="document">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="document" select="node()[not(@chunk='yes' or preceding-sibling::node()[@chunk='yes'])]"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*[matches(local-name(),$regex.meta)]" mode="kernel.body" priority="1.5"/>
    <!--omit direct presentation of -meta elems-->

    <xsl:template match="/" mode="kernel" priority="1.5">
        <html>
            <xsl:apply-templates mode="kernel.head" select="."/>
            <xsl:apply-templates mode="kernel.body"/>
        </html>
    </xsl:template>

    <!--see below - headings dealt with differently so same method used for headings at all levels-->

    <xsl:template match="/" mode="kernel.head" priority="1.5">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when
                        test="
                        if (descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)]) 
                        then true()
                        else false()">
                        <xsl:apply-templates mode="kernel.head"
                            select="(descendant::*[matches(local-name(),$regex.meta)]/descendant::*[matches(local-name(),$regex.titles)])[1]"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'BITS2EPUB'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <meta charset="UTF-8"/>
       
            <meta content="bits2epub v.{$bits2epub-version}" name="generator"/>
            <link href="{concat($path.css, 'cup-bits-epub3.css')}" media="all" rel="stylesheet" type="text/css"/>
            <xsl:apply-templates mode="mathjax" select="descendant::mml:math[1]"/>
        </head>
    </xsl:template>

    <!--    maths -->

    <xsl:template match="disp-formula//graphic" mode="kernel.body" priority="1.5">
        <img class="math-graphic">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </img>
    </xsl:template>

    <xsl:template match="inline-formula//inline-graphic" mode="kernel.body" priority="1.5">
        <img class="math-graphic">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </img>
    </xsl:template>


    <xsl:template match="mml:math" mode="mathjax">
        <!--Mathjax disabled for 9.0.1-->
        <!--        TODO - add relative filepath so this file ends up in the right place-->
        <!--        <xsl:result-document method="text" href="mathjax-config.js">
            MathJax.Hub.Config(
            {
            jax: ["input/TeX","input/MathML","output/SVG"],
            extensions: ["tex2jax.js","mml2jax.js","MathEvents.js"], 
            tex2jax: {
            inlineMath: [ ['$','$'], ["\\(","\\)"] ],
            displayMath: [ ['$$','$$'],["\\[","\\]"] ],
            processEscapes: true
            }, 
            TeX: {
            extensions: ["noErrors.js","noUndefined.js","autoload-all.js"],
            TagIndent: "0em"
            }, 
            "SVG": {
            linebreaks: {
            automatic: true, width: "75% container"
            }
            }, 
            showMathMenu: true,
            menuSettings: {
            zoom: "None"
            },
            messageStyle: "none"
            });
        </xsl:result-document>-->

        <!--        <script src="../mathjax/mathjax-config.js" type="text/x-mathjax-config"/>
        
        <script src="../mathjax/MathJax.js" type="text/javascript"/>-->

        <!--        this is the bit for suppressing the math images on ipad-->
        <script src="{concat($path.css, 'fixmathml.js')}"/>
    </xsl:template>



    <!--add filename if destination in a different file-->
    <xsl:template match="@rid" mode="kernel.body" priority="1.5">
        <xsl:variable as="xs:string" name="tgt.file" select="$lookup.tgts[@id eq current()/string()][1]/@filename"/>
        <xsl:variable as="xs:string" name="curr.file" select="parent::node()/ancestor-or-self::node()[@filename][1]/@filename"/>
        <xsl:choose>
            <xsl:when test="$tgt.file=$curr.file">
                <xsl:attribute name="href" select="concat('#',string())"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="href" select="concat($tgt.file,'#',string())"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>


    <!-- ==========   OVER-RIDES  - elements in alphabetical order ============= -->


    <!--no output for alt titles-->
    <xsl:template match="alt-title" mode="#all" priority="1.5"/>

    <!--     =============  ALTERNATIVES ==============      -->
    <!--epub:switch for math-->
    <xsl:template match="alternatives[child::mml:math]" mode="kernel.body" priority="1.5">
        <epub:switch>
            <xsl:choose>
                <xsl:when test="@id">
                    <xsl:apply-templates mode="kernel.body" select="@id"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="id" select="generate-id()"/>
                </xsl:otherwise>
            </xsl:choose>
            <epub:case required-namespace="http://www.w3.org/1998/Math/MathML">
                <xsl:apply-templates mode="kernel.body" select="mml:math"/>
            </epub:case>
            <epub:default>
                <xsl:apply-templates mode="kernel.body" select="graphic | inline-graphic | textual-form"/>
            </epub:default>
        </epub:switch>
    </xsl:template>


    <xsl:template match="alternatives[table | array]" mode="kernel.body" priority="1.5">
        <xsl:apply-templates mode="kernel.body" select="table | array/tbody"/>
    </xsl:template>
    
    <xsl:template match="textual-form" mode="kernel.body" priority="1.5">
        <xsl:element name="{if (ancestor::inline-formula) then 'span' else 'div'}">
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </xsl:element>
    </xsl:template>

    <!--=============  CAPTION ==================    -->

<!--    <xsl:template match="caption[preceding-sibling::*[1][self::label]]" mode="kernel.body" priority="1.5"/>

    <xsl:template match="caption[not(preceding-sibling::*[1][self::label])]/title" mode="kernel.body" priority="2">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body"/>
        </div>
    </xsl:template>-->
    
<!--    caption to be p not div-->
    <xsl:template match="caption" mode="kernel.body" priority="1.5">
        <xsl:variable name="label">
            <xsl:apply-templates  select="preceding-sibling::*[1][self::label]" mode="do-label"/>
        </xsl:variable>
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:copy-of select="$label"/>
            <xsl:apply-templates mode="kernel.body"/>
        </div>
    </xsl:template>
    
    <xsl:template match="caption/title" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--  ====================    CHEM-STRUCT  ==============  -->
    <!--    label after expression-->
    <xsl:template match="chem-struct-wrap" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
            <xsl:apply-templates mode="kernel.body" select="label"/>
        </div>
    </xsl:template>

    <!--  ====================    COLLAB  ==============  -->

    <!--    collab in citation to be span-->
    <xsl:template match="collab[ancestor::mixed-citation]" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--  ====================    CONTRIB  ==============  -->
    <!--    book-part contrib to be span-->
    <xsl:template match="book-part//contrib-group[not(contrib/aff)]/contrib" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--  ====================    DIPS-FORMULA  ==============  -->

    <!--    label after expression-->
    <xsl:template match="disp-formula" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
            <xsl:apply-templates mode="kernel.body" select="label"/>
        </div>
    </xsl:template>

    <!--    label after expression-->
    <xsl:template match="disp-formula-group" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::label)]"/>
            <xsl:apply-templates mode="kernel.body" select="label"/>
        </div>
    </xsl:template>

    <!--  ====================    FIG  ==============  -->

    <!--    caption to be at foot of figure-->
    <xsl:template match="fig" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body"
                select="node()[not(self::label or self::caption or self::attrib  or self::permissions )]"/>
            <xsl:apply-templates mode="kernel.body" select="label | caption |attrib |permissions"/>
        </div>
    </xsl:template>

    <!--  ====================    LABEL  ==============  -->

    <!--   notes to have backlinks-->
    <!--    footnotes and references to have backlinks when linked to from one xref-->
    <!--    content of following p to be processed here - not now 
            this no longer works now that p has to be split into groups as it is element p which cannot have descendant div,
            so all p have to be dealt with from the p template.
            so note change of mode here as template is called from p-->
    
    <xsl:template match="label[following-sibling::*[1][self::p or self::caption]]" mode="kernel.body" priority="1.5"/>
    
    <xsl:template match="label[following-sibling::*[1][self::p or self::caption]]" mode="do-label" priority="3">
            <span>
                <xsl:sequence select="cup:classConstructor(.)"/>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:choose>
                    <xsl:when test="parent::fn[@id]">
                        <xsl:variable as="element()" name="anc-with-id" select="parent::fn"/>
                        <xsl:variable name="id" select="$anc-with-id/@id"/>
                        <xsl:variable name="filename" select="$anc-with-id/@filename"/>
                        <xsl:variable name="xref-count" select="count($lookup.tgts.rid[@rid eq string($id)])"/>
                        <xsl:choose>
                            <xsl:when test="$xref-count=1">
                                <xsl:variable as="xs:string" name="tgt.file" select="$lookup.tgts.rid[@rid eq string($id)][1]/@filename"/>
                                <xsl:choose>
                                    <xsl:when test="$tgt.file=string($filename)">
                                        <xsl:apply-templates mode="kernel.body" select="named-content[@content-type='page']"/>
                                        <!--page breaks not to be in back link-->
                                        <a href="{concat('#', $id, '-back')}">
                                            <xsl:apply-templates mode="kernel.body"
                                                select="node()[not(self::named-content[@content-type='page'])]"/>
                                        </a>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates mode="kernel.body" select="named-content[@content-type='page']"/>
                                        <!--page breaks not to be in back link-->
                                        <a href="{concat($tgt.file, '#', $id, '-back')}">
                                            <xsl:apply-templates mode="kernel.body"
                                                select="node()[not(self::named-content[@content-type='page'])]"/>
                                        </a>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates mode="kernel.body" select="node()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates mode="kernel.body" select="node()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
            <xsl:text> </xsl:text>

    </xsl:template>



<!--    <xsl:template match="label[following-sibling::*[1][self::caption[child::title]]]" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(following-sibling::caption[1])"/>
            <div>
                <xsl:sequence select="cup:classConstructor(following-sibling::caption[1]/title[1])"/>
                <xsl:apply-templates mode="kernel.body" select="following-sibling::caption[1]/title/@*"/>
                <xsl:next-match/>
                <xsl:text> </xsl:text>
                <xsl:apply-templates mode="kernel.body" select="following-sibling::caption[1]/title/node()"/>
            </div>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::caption[1]/p"/>
        </div>
    </xsl:template>

    <xsl:template match="label[following-sibling::*[1][self::caption[not(child::title)]]]" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(following-sibling::caption[1])"/>
            <div>
                <xsl:sequence select="cup:classConstructor(following-sibling::caption[1]/p[1])"/>
                <xsl:next-match/>
                <xsl:text> </xsl:text>
                <xsl:apply-templates mode="kernel.body" select="following-sibling::caption[1]/p[1]/node()"/>
            </div>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::caption[1]/p[preceding-sibling::p]"/>
        </div>
    </xsl:template>-->

    <!--  ====================    LIST  ==============  -->

    <!--    numbered paragraphs  -->
    <xsl:template match="list[@list-content eq 'paragraph']" mode="kernel.body" priority="1.5">
        <xsl:apply-templates mode="kernel.body"/>
    </xsl:template>

    <!--    numbered paragraphs  -->
    <xsl:template match="list[@list-content eq 'paragraph']/list-item" mode="kernel.body" priority="1.5">
        <div class="p numbered">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </div>
    </xsl:template>

    <!--  ====================    NAME  ==============  -->

    <!--    name in mixed-citation to be span-->
    <xsl:template match="name[ancestor::mixed-citation]" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--    order chapter author names-->
    <xsl:template match="book-part-meta//name" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="prefix"/>
            <xsl:apply-templates mode="kernel.body" select="given-names"/>
            <xsl:apply-templates mode="kernel.body" select="surname"/>
            <xsl:apply-templates mode="kernel.body" select="suffix"/>
        </span>
    </xsl:template>

    <!--space after given names if before surname-->
    <xsl:template match="book-part-meta//name/given-names" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
        <xsl:if test="preceding-sibling::surname">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- space after prefix-->
    <xsl:template match="book-part-meta//name/prefix" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
        <xsl:text> </xsl:text>
    </xsl:template>

    <!--  ====================    NAMED-CONTENT  PAGE NUMBERS  ==============  -->

    <!--    pages to be a for epub;   first page to be after body tag so not processed here-->
    <xsl:template match="named-content[@content-type eq 'page']" mode="kernel.body" priority="1.5">
        <a>
            <xsl:attribute name="id" select="concat('page_' , string(.) )"/>
        </a>
    </xsl:template>

    <!--  ====================    NAV-POINTER  INDEX  ==============  -->

    <!--    add punctuation for nav-pointers in index-->
    <xsl:template match="index//nav-pointer" mode="kernel.body" priority="1.5">
        <span class="nav-pointer">
            <a>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()[not(self::named-content[@content-type='page'])]"/>
            </a>
            <xsl:apply-templates mode="kernel.body" select="named-content[@content-type='page']"/>
        </span>
        <xsl:if test="parent::nav-pointer-group and @nav-pointer-type='start-of-range'">
            <xsl:text>&#x2013;</xsl:text>
        </xsl:if>
        <xsl:if test="not(parent::nav-pointer-group) and (following-sibling::nav-pointer or following-sibling::nav-pointer-group)">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="index//nav-pointer-group" mode="kernel.body" priority="1.5">
        <span class="nav-pointer-group">
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body"/>
        </span>
        <xsl:if test="following-sibling::nav-pointer or following-sibling::nav-pointer-group">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="index-entry/term" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--    =============== P ===================== -->

    <!--    p to be html p element not div -->
    <xsl:template match="p[not(disp-quote or list or verse-group or array or boxed-text[not(@position='float')] 
        or table-wrap[not(@position='float')] or table-wrap-group[not(@position='float')] or fig[not(@position='float')]
        or fig-group[not(@position='float')] or disp-formula or def-list or alternatives or chem-struct or statement or speech
        or disp-formula-group or preformat or code or fn)]" mode="kernel.body" priority="1.5">
        <xsl:variable name="label">
            <xsl:apply-templates  select="preceding-sibling::*[1][self::label]" mode="do-label"/>
        </xsl:variable>
        <p>
            <xsl:if test="@content-type">
                <xsl:attribute name="class" select="concat(@content-type,(if (preceding-sibling::p) then '' else ' noindent'))"/>
            </xsl:if>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:copy-of select="$label"/>
            <xsl:apply-templates mode="kernel.body"/>
        </p>
    </xsl:template>
    
<!--    split p containing block elements into separate p elements-->
    <xsl:template match="p[disp-quote or list or verse-group or array or boxed-text[not(@position='float')] 
        or table-wrap[not(@position='float')] or table-wrap-group[not(@position='float')] or fig[not(@position='float')]
        or fig-group[not(@position='float')] or disp-formula or def-list or alternatives or chem-struct or statement or speech
        or disp-formula-group or preformat or code or fn]" mode="kernel.body" priority="1.5">
        <xsl:variable name="label">
            <xsl:apply-templates  select="preceding-sibling::*[1][self::label]" mode="do-label"/>
        </xsl:variable>
        <xsl:variable name="class" select="string(@content-type)"/>
        <xsl:for-each-group select="node()"
            group-adjacent="if (self::node()[not(self::disp-quote or self::list or self::verse-group or self::array
            or self::boxed-text[not(@position='float')] or self::table-wrap[not(@position='float')] or self::table-wrap-group[not(@position='float')] or self::fig[not(@position='float')]
            or self::fig-group[not(@position='float')] or self::disp-formula or self::def-list or self::alternatives or self::chem-struct or self::statement or self::speech
            or self::disp-formula-group or self::preformat or self::code or self::fn
            )])then ('para') else('block')">
                <xsl:choose>
                    <xsl:when test="current-grouping-key()='para' and position()=1">
                        <p>
                            <xsl:if test="$class!=''">
                                <xsl:attribute name="class" select="@content-type"/>
                            </xsl:if>
                            <xsl:copy-of select="$label"/>
                            <xsl:apply-templates mode="kernel.body" select="parent::p/@*"/>
                            <xsl:apply-templates select="current-group()" mode="kernel.body"/>
                        </p>
                    </xsl:when>
                    <xsl:when test="current-grouping-key()='para' and position()!=1">
                        <p class="noindent">
                            <xsl:choose>
                                <xsl:when test="$class!=''">
                                    <xsl:attribute name="class" select="concat('noindent ',@content-type)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="class" select="'noindent'"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:apply-templates select="current-group()" mode="kernel.body"/>
                        </p>
                    </xsl:when>
                    <xsl:when test="current-grouping-key()='block' and position()=1">
                        <xsl:copy-of select="$label"/>
                        <xsl:apply-templates select="current-group()" mode="kernel.body"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="current-group()" mode="kernel.body"/>
                    </xsl:otherwise>
                </xsl:choose>   
        </xsl:for-each-group>
    </xsl:template>

    <!--  ====================    SPEAKER  ==============  -->

    <!--speaker to be on same line as first para of speech-->
    <xsl:template match="speaker[following-sibling::*[1][self::p]]" mode="kernel.body" priority="1.5">
        <div>
            <xsl:sequence select="cup:classConstructor(following-sibling::p[1])"/>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::p[1]/@*"/>
            <span>
                <xsl:sequence select="cup:classConstructor(.)"/>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </span>
            <xsl:apply-templates mode="kernel.body" select="following-sibling::p[1]/node()"/>
        </div>
    </xsl:template>

    <xsl:template match="p[local-name(preceding-sibling::*[1]) eq 'speaker']" mode="kernel.body" priority="1.5"/>



    <!--  ====================    SEE SEE-ALSO ==============  -->

    <!--    see-entry to contain a to make html valid-->
    <xsl:template match="see-entry | see-also-entry" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <a>
                <xsl:apply-templates mode="kernel.body" select="@*"/>
                <xsl:apply-templates mode="kernel.body" select="node()"/>
            </a>
        </span>
    </xsl:template>

    <!--  ====================    STYLED-CONTENT - FONT  ==============  -->

    <xsl:template match="text()[ancestor::styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']]"
        mode="kernel.header kernel.body" priority="1.5">
        <xsl:value-of select="."/>
    </xsl:template>

    <!--disable Unicode normalization for text in substitute fonts-->

    <xsl:template match="styled-content[@style-type eq 'substitute'][@specific-use eq 'epub']" mode="kernel.header kernel.body"
        priority="1.5">
        <span>
            <xsl:attribute name="class" select="concat('unicode-altfont-',@style)"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--  ====================    SUB SUP ==============  -->

    <xsl:template match="sub" mode="kernel.body" priority="1.5">
        <sub>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </sub>
    </xsl:template>

    <xsl:template match="sup" mode="kernel.body" priority="1.5">
        <sup>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </sup>
    </xsl:template>

    <!--  ====================    SUBTITLE ==============  -->

    <!--subtitle to be span-->
    <xsl:template match="subtitle" mode="kernel.subtitle" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </span>
    </xsl:template>

    <!--  ====================    TABLE ==============  -->

    <!--    just copy tables as is-->
    <xsl:template match="table | tgroup | thead | tbody |col | colgroup| td | tr |th" mode="kernel.body" priority="1.5">
        <xsl:choose>
            <xsl:when test="./parent::array">
                <table>
                    <xsl:element name="{local-name()}">
            <xsl:apply-templates mode="attributes" select="."/>
            <xsl:apply-templates mode="kernel.body"/>
        </xsl:element>
    </table>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{local-name()}">
                    <xsl:apply-templates mode="attributes" select="."/>
                    <xsl:apply-templates mode="kernel.body"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="table| col | tgroup| thead | tbody |col | colgroup| td | tr |th" mode="attributes">
        <xsl:copy-of select="@id | @rowspan | @colspan"/>
        <!--TODO: implement BITS @rules handling - requires CSS discussion and suitable params passed down template stack-->
        <xsl:if test="@width or @frame or @align">
            <xsl:attribute name="style">
                <xsl:apply-templates select="@width | @frame | @rules | @align" mode="attributes"/>
            </xsl:attribute>
        </xsl:if>
    </xsl:template>

    <xsl:template match="@align" mode="attributes">
        <xsl:value-of select="concat('text-align:',.,';')"/>
    </xsl:template>
    
    <xsl:template match="@rules" mode="attributes"/><!--2015-02-06 @rules unprocessed pending discussion of table styling-->
    
    <xsl:template match="@frame" mode="attributes">
        <xsl:variable name="framestyle" as="xs:string">
            <xsl:choose>
                <xsl:when test=". eq 'above'"><xsl:value-of select="'border-top:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'below'"><xsl:value-of select="'border-bottom:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'border'"><xsl:value-of select="'border:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'box'"><xsl:value-of select="'border:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'hsides'"><xsl:value-of select="'border-top:1px black solid;border-bottom:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'lhs'"><xsl:value-of select="'border-left:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'rhs'"><xsl:value-of select="'border-right:1px black solid;'"/></xsl:when>
                <xsl:when test=". eq 'void'"><xsl:value-of select="'border:none;'"/></xsl:when>
                <xsl:when test=". eq 'vsides'"><xsl:value-of select="'border-left:1px black solid;border-right:1px black solid;'"/></xsl:when>
                <xsl:otherwise>
                    <xsl:sequence select="error(xs:QName('cup:EPUB001'),concat('A disallowed value for the attribute ',name(.),' was encountered.'))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$framestyle"/>
    </xsl:template>
    
    <!--<xsl:template match="@rules" mode="attributes">
        <xsl:value-of select="concat(,.,';')"/>
    </xsl:template>-->

    <xsl:template match="@width" mode="attributes">
        <xsl:value-of select="concat('width:', ., ';')"/>
    </xsl:template>

    <!--  ====================    TITLES ==============  -->

    <xsl:template
        match="*[matches(local-name(),$regex.titles)][not(ancestor::toc-entry)][preceding-sibling::*[1][self::label or self::title]] |sec/label"
        mode="kernel.body" priority="1.5"/>

    <xsl:template match="title[@class eq 'unprocessed']" mode="kernel.header" priority="1.5"/>
    <!--discard default no-candidate titles-->

    <!--  ====================    TOC ==============  -->
    
<!--  CBT-142 - the label and title to be the link
    page numbers to be outside the link-->
    <xsl:template match="toc-entry" mode="kernel.body" priority="2">
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates select="label/named-content[@content-type='page'] | title/named-content[@content-type='page']" mode="kernel.body"/>
            <a>
                <xsl:apply-templates mode="kernel.body" select="nav-pointer/@rid | following-sibling::nav-pointer/@rid"/>
                <xsl:apply-templates mode="kernel.body" select="node()[not(self::toc-entry)]"/>
            </a>
            <xsl:apply-templates mode="kernel.body" select="toc-entry"/>
        </div>
    </xsl:template>

    <xsl:template match="toc-entry/title" mode="kernel.body" priority="2">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::named-content[@content-type='page'])]"/>
        </span>
    </xsl:template>

    <xsl:template match="toc-entry/label" mode="kernel.body" priority="1.5">
        <span>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::named-content[@content-type='page'])]"/>
        </span>
    </xsl:template>

    <!--    link in toc to be done as title or label-->
    <xsl:template match="toc-entry/nav-pointer | wrap/nav-pointer" mode="kernel.body" priority="1.5">
        <xsl:apply-templates select="named-content[@content-type='page']" mode="kernel.body"/>
    </xsl:template>

    <!--    links within toc-entry title not to be live -->
    <xsl:template match="toc-entry/title/xref | toc-entry/title/uri" mode="kernel.body" priority="2">
        <xsl:apply-templates/>
    </xsl:template>

    <!--  ====================    URI ==============  -->

    <!--    page break to be after uri-->
    <xsl:template match="uri" mode="kernel.body" priority="1.5">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()[not(self::named-content[@content-type='page'])]"/>
        </a>
        <xsl:apply-templates mode="kernel.body" select="named-content[@content-type='page']"/>
        <!--page breaks not to be in link-->
    </xsl:template>

    <!--  ====================    XREF ==============  -->

    <!--    add id to links to footnotes and references for backlink-->
    <xsl:template match="xref" mode="kernel.body" priority="1.5">
        <xsl:variable name="commentaries" select="'commentary-note textual-note expl-note emend-note eol-note variant-note apparatus-note'"/>
        <xsl:variable name="href" select="@rid"/>
        <xsl:apply-templates select="descendant::target" mode="do-target"/><!--cannot have a within a in html-->
        
<!--     no link to collation notes - only a link from the note   -->
        <xsl:choose>
            <xsl:when test="@ref-type='fn' and 
                (following::fn[@id=$href and (@fn-type='collation-note' or parent::fn-group[@content-type='collation-notes'])]
                or
                preceding::fn[@id=$href and (@fn-type='collation-note' or parent::fn-group[@content-type='collation-notes'])]
                )">
                <a>
                    <xsl:sequence select="cup:classConstructor(.)"/>
                    <xsl:if
                        test="not(following::xref[@rid=current()/@rid and not(ancestor::fn)] or preceding::xref[@rid=current()/@rid] )">
                        <xsl:attribute name="id" select="concat(@rid, '-back')"/>
                    </xsl:if>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <a>
                    <xsl:sequence select="cup:classConstructor(.)"/>
                    <xsl:apply-templates mode="kernel.body" select="@*"/>
                    <xsl:if
                        test="(@ref-type='fn' or @ref-type='table-fn') and not(following::xref[@rid=current()/@rid and not(ancestor::fn)] or preceding::xref[@rid=current()/@rid] )">
                        <xsl:attribute name="id" select="concat(@rid, '-back')"/>
                    </xsl:if>
                    <!--  add asterisk for commentary note                  -->
                    <xsl:if test="@ref-type='fn' and 
                        (following::fn[@id=$href and (contains(string(@fn-type), $commentaries) or parent::fn-group[@content-type='commentary-notes'])] and string(.)=''
                        or
                        preceding::fn[@id=$href and (contains(string(@fn-type), $commentaries) or parent::fn-group[@content-type='commentary-notes'])] and string(.)='')
                        ">*</xsl:if>
                    <xsl:apply-templates mode="kernel.body" select="node()[not(self::target)]"/>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="target" mode="do-target" priority="1.5">
        <a>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </a>
    </xsl:template>

    <xsl:template match="xref//target" mode="kernel.body" priority="1.5"/>

    <!--   
    REQUIREMENTS FOR HEADINGS
    
    when @chunk='yes'
    <header><h1 class="CT"><span class="label">label text</span> title text</h1><div class="subtitle">subtitle</div></header>
    where CT is the first part of the element ID
    
    where the heading may be one of:
    
        book-part/book-part-meta/title-group
        dedication/book-part-meta/title-group
        foreword/book-part-meta/title-group
        front-matter-part/book-part-meta/title-group
        preface/book-part-meta/title-group
        
        index/title-group
        index-div/title-group
        index-group/title-group
        toc/title-group
        toc-div/title-group
        toc-group/title-group
        
        ack/(label|title)
        app/(label|title)
        app-group/(label|title)
        glossary/(label|title)
        ref-list/(label|title)
        
    otherwise
        when the heading is part of the heading hierarchy:
        <h1 class="A"><span class="label">label text</span> title text: <span class="subtitle">subtitle</div></h1>
        
        where the heading may be one of:
        ack[label|title]
        app[label|title]
        app-group[label|title]
        glossary[label|title]
        ref-list[label|title]
        sec[label|title]
        
        otherwise (ie not part of the hierarchy)
        <div class="disp-quote title"><span class="label">label text</span> title text: <span class="subtitle">subtitle</div>
    -->

    <!--    the header-->
    <xsl:template match="/child::*[1]" mode="kernel.body" priority="20">
        <body>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="kernel.body" select="@*"/>
            <xsl:apply-templates mode="kernel.header" select="book-part-meta | title-group | title | label"/>
            <xsl:apply-templates mode="kernel.body" select="node()"/>
        </body>
    </xsl:template>

    <xsl:template match="/child::*[1]/label | /child::*[1]/title[not(preceding-sibling::label)]" mode="kernel.header" priority="2">
        <xsl:apply-templates select="parent::*" mode="kernel.header"/>
    </xsl:template>

    <xsl:template match="/child::*[1]/title-group" mode="kernel.body"/>

    <xsl:template match="book-part-meta" mode="kernel.header" priority="2">
        <xsl:apply-templates mode="kernel.header" select="title-group"/>
        <xsl:apply-templates mode="kernel.body" select="contrib-group"/>
    </xsl:template>

    <!--    those with a title group do the title group-->
    <xsl:template match="title-group | /child::*[1][label or title]" mode="kernel.header" priority="2">
        <xsl:variable name="elem-with-id" select="ancestor-or-self::*[@id][1]"> </xsl:variable>
        <xsl:variable name="style" select="substring-before($elem-with-id/@id, '-')"/>
        <header>
            <h1>
                <xsl:attribute name="class" select="$style"/>
                <xsl:apply-templates mode="do-heading" select="label | title"/>
            </h1>
        </header>
        <xsl:apply-templates mode="do-heading" select="subtitle">
            <xsl:with-param name="type" select="div"/>
        </xsl:apply-templates>
    </xsl:template>

    <!--    those without a title group do the parent as for title group-->
    <!--    <xsl:template match="/child::*[1]/label | /child::*[1]/title[not(preceding-sibling::label)]" mode="kernel.header" priority="2">
        <xsl:apply-templates mode="kernel.header" select="parent::*"/>
    </xsl:template>-->



    <!--call heading creation for parent element for label and title not enclosed in a title-group-->
    <xsl:template
        match="ack/label | ack/title[not(preceding-sibling::label)] | app/label | app/title[not(preceding-sibling::label)]
        | app-group/label | app-group/title[not(preceding-sibling::label)] | glossary/label | glossary/title[not(preceding-sibling::label)]
        | ref-list/label | ref-list/title[not(preceding-sibling::label)] | sec/label | sec/title[not(preceding-sibling::label)] |
        def-list/label |
        disp-quote/label |
        fn-group/label |
        statement/label[following-sibling::title] |
        verse-group/label |
        list/label |
        answer/label |
        question/label |
        def-list/title[not(preceding-sibling::label)] |
        disp-quote/title[not(preceding-sibling::label)] |
        fn-group/title[not(preceding-sibling::label)] |
        statement/title[not(preceding-sibling::label)] |
        verse-group/title[not(preceding-sibling::label)] |
        list/title[not(preceding-sibling::label)] |
        answer/title[not(preceding-sibling::label)] |
        question/title[not(preceding-sibling::label)]"
        mode="kernel.body" priority="2.5">
        <xsl:apply-templates mode="kernel.header" select="parent::*"/>
    </xsl:template>

    <xsl:template match="title| label" mode="kernel.header" priority="10"/>

    <!--    those in the hierarchy-->
    <xsl:template match="*/ack |  app-group/app | back/app |  */glossary | */ref-list  | sec" mode="kernel.header" priority="2">
        <xsl:variable name="style" select="substring-before(@id, '-')"/>
        <xsl:variable name="head.level" select="string(count(ancestor-or-self::*[label or title]))"/>
        <xsl:element name="{concat('h',$head.level)}">
            <xsl:attribute name="class" select="$style"/>
            <xsl:apply-templates mode="do-heading" select="label | title"/>
            <xsl:apply-templates mode="do-heading" select="subtitle">
                <xsl:with-param name="type" select="span"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <!--    those not in the hierarchy-->
    <xsl:template match="def-list | disp-quote | fn-group | statement | verse-group | list | answer | question" mode="kernel.header"
        priority="2">
        <xsl:variable name="style" select="substring-before(substring-before(@id, '-'), '-')"/>
        <xsl:variable name="head.level" select="string(count(ancestor-or-self::*[label or title]))"/>
        <div>
            <xsl:sequence select="cup:classConstructor(.)"/>
            <xsl:apply-templates mode="do-heading" select="label | title"/>
            <xsl:apply-templates mode="do-heading" select="subtitle">
                <xsl:with-param name="type" select="span"/>
            </xsl:apply-templates>
        </div>
    </xsl:template>

    <xsl:template match="label" mode="do-heading" priority="2">
        <span class="label">
            <xsl:apply-templates mode="kernel.body"/>
        </span>
        <xsl:if test="following-sibling::title">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="title" mode="do-heading" priority="2">
        <xsl:apply-templates mode="kernel.body"/>
    </xsl:template>

    <xsl:template match="subtitle" mode="do-heading" priority="2">
        <xsl:param name="type"/>
        <xsl:variable name="elem">
            <xsl:choose>
                <xsl:when test="$type!=''">
                    <xsl:value-of select="$type"/>
                </xsl:when>
                <xsl:otherwise>span</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:text> </xsl:text>
        <xsl:element name="{$elem}">
            <xsl:attribute name="class" select="'subtitle'"/>
            <xsl:apply-templates mode="kernel.body"/>
        </xsl:element>
    </xsl:template>

    <!--    transform fixed spaces to ordinary spaces to allow output html to remove firefox problem-->

    <xsl:template match="text()" mode="kernel.body">
        <xsl:value-of select="translate(., '&#x00A0;', ' ')"/>
    </xsl:template>
    
    <!--testing, delete or tidy before release-->
    <xsl:template match="
        *[local-name() = 
            ('label','named-content','nav-pointer','see-entry','see-also-entry','toc-entry','uri','xref','target','answer','ext-link')
            ][descendant::*[local-name() = 
            ('label','named-content','nav-pointer','see-entry','see-also-entry','toc-entry','uri','xref','target','answer','ext-link')
            ]][not(self::label[following-sibling::*[1][self::p | self::caption | self::title]])]
        " priority="3.0" mode="kernel.body"><!--hijack normal processing where there is a possibility of a/a emission-->
        <xsl:choose><!--decide whether to engage in funny business-->
            <xsl:when test="descendant::*[self::label[ancestor::toc][not(following-sibling::*[1][self::p | self::caption | self::title])] | self::named-content[@content-type eq 'page'] | self::nav-pointer | self::see-entry | self::see-also-entry | self::toc-entry | self::uri | self::xref | self::target | self::answer | self::ext-link][(@rid | @href | @id) | self::named-content]"><!--there is a descendant that will emit an HTML a-->
                <xsl:variable name="c" as="element()+">
                    <wrap filename="{./ancestor-or-self::*[@filename][1]/@filename}" xmlns=""><!--preserve filename info and nullify XHTML ns-->
                        <xsl:apply-templates select="." mode="rearrange-links"/>
                        <xsl:copy-of select="./descendant::*[self::label[ancestor::toc][not(following-sibling::*[1][self::p | self::caption | self::title])] | self::named-content[@content-type eq 'page'] | self::nav-pointer | self::see-entry | self::see-also-entry | self::toc-entry | self::uri | self::xref | self::target | self::answer | self::ext-link][(@rid | @href | @id) | self::named-content]"/><!--FIXME: streamline selection (external lookup?)-->
                    </wrap>
                </xsl:variable>
                <xsl:apply-templates select="$c/*" mode="#current"/><!--n.b. $c is the wrap element; we need to process its children-->
            </xsl:when>
            <xsl:otherwise><!--there is no such descendant - business as usual-->
                <xsl:next-match/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="@* | node()" mode="rearrange-links">
        <xsl:if test="not((self::label[ancestor::toc][not(following-sibling::*[1][self::p | self::caption | self::title])] | self::named-content[@content-type eq 'page'] | self::nav-pointer | self::see-entry | self::see-also-entry | self::toc-entry | self::uri | self::xref | self::target | self::answer | self::ext-link)[(@rid | @href | @id) | self::named-content])">
            <xsl:copy copy-namespaces="no">
                <xsl:apply-templates mode="rearrange-links"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
