<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
<!--lookup file to assign span or div to BITS element-->
    <xsl:variable name="span-div" select="document('span-div.xml')/elements" as="element()"/>

<!--assigns span or div to BITS element
    used by templates in default.xsl for html creation-->
    <xsl:function name="cup:SpanDiv" as="xs:string">
<!--        param element is the element name-->
        <xsl:param name="element" as="xs:string"/>
        <xsl:value-of select="$span-div//*[name(.)=$element]/@type"/>
    </xsl:function>
 
</xsl:stylesheet>