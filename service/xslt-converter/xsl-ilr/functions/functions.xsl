<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
    <xsl:include href="filename.xsl"/>
    <xsl:include href="imageFilename.xsl"/>
    <xsl:include href="SpanDiv.xsl"/>
    <xsl:include href="Link.xsl"/>
    <xsl:include href="HeadingType.xsl"/>
    <xsl:include href="pages.xsl"/>
    <xsl:include href="footnotes.xsl"/>

</xsl:stylesheet>