<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
    <!-- This stylesheet is for anything to do with pages in CUP-BITS documents, 
         such a counting page, finding page numbers for elements, etc. -->
    
    <xsl:function name="cup:containedPageNumbers" as="text()*">
        <xsl:param name="elem" as="element()"/>
        <!-- This function returns the text nodes from named-content elements
             which are descendents of the element passed to it. -->
        <!-- TODO: Also get page breaks in floating matter anchored within $elem -->
        <xsl:sequence select="$elem//named-content[@content-type='page']/text()"/>
    </xsl:function>

    <xsl:function name="cup:firstPage" as="xs:string">
        <xsl:param name="elem" as="element()"/>
        <!-- This function returns the page on which the element passed to it starts. This 
             should be the page numbered quoted in tables of contents, indexes, mixed-citations, etc. 
             It returns a string because not all page numbers are integers (even in book-body.) -->
        <xsl:variable name="ncs" as="text()*" select="cup:containedPageNumbers($elem)"/>
        <xsl:choose>
            <xsl:when test="count($ncs) = 0">
                <!-- Element contains no page breaks, so it must be entirely
                     contained in a page started in a previous element -->
                <xsl:value-of select="$elem/preceding::named-content[@content-type='page'][1]"/>
            </xsl:when>
            <xsl:when test="$elem//text()[
                not(ancestor::book-part-meta)
                and not(ancestor::sec-meta)
                and following::named-content[@content-type='page'][1]/text() = $ncs[1]]">
                <!-- There is text before the first page break, so must assume
                     this element starts halfway down a page -->
                <!-- TODO: Test the above logic on different types of books -->
                <xsl:value-of select="$elem/preceding::named-content[@content-type='page'][1]"/>
            </xsl:when>
            <xsl:when test="count($ncs) = 1">
                <!-- Only one page break in this element, so it must be the first page -->
                <xsl:value-of select="$ncs[1]"/>
            </xsl:when>
            <xsl:when test="every $p in $ncs satisfies string(number($p)) != 'NaN'">
                <!-- All page numbers in the element are arabic numbers, so  
                     pick the smallest (better than picking the first, because
                     of floating matter or facing pages) -->
                <xsl:value-of select="string(min($ncs))"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- At least one page number is not a number, so fallback 
                     to taking the first in XML document order -->
                <xsl:value-of select="($elem//named-content[@content-type='page'])[1]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="cup:lastPage" as="xs:string">
        <xsl:param name="elem" as="element()"/>
        <!-- This function returns the page on which the element passed to it ends. This 
             should be the page numbered quoted in page ranges in indexes, mixed-citations, etc. 
             It returns a string because not all page numbers are integers (even in book-body.) -->
        <xsl:variable name="ncs" as="text()*" select="cup:containedPageNumbers($elem)"/>
        <xsl:choose>
            <xsl:when test="count($ncs) = 0">
                <!-- Element contains no page breaks, so it must be entirely
                     contained in a page started in a previous element -->
                <xsl:value-of select="$elem/preceding::named-content[@content-type='page'][1]"/>
            </xsl:when>
            <xsl:when test="count($ncs) = 1">
                <!-- Only one page break in this element, so it must be the last page -->
                <xsl:value-of select="$ncs[1]"/>
            </xsl:when>
            <xsl:when test="every $p in $ncs satisfies string(number($p)) != 'NaN'">
                <!-- All page numbers in the element are arabic numbers, so  
                     pick the largest (better than picking the last, because
                     of floating matter or facing pages) -->
                <xsl:value-of select="string(max($ncs))"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- At least one page number is not a number, so fallback 
                     to taking the last in XML document order -->
                <xsl:value-of select="($elem//named-content[@content-type='page'])[position()=last()]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="cup:firstAndLastPages" as="xs:string*">
        <xsl:param name="elem" as="element()"/>
        <xsl:sequence select="(cup:firstPage($elem), cup:lastPage($elem))"/>
    </xsl:function>

    <xsl:function name="cup:pageRange" as="xs:string">
        <xsl:param name="elem" as="element()"/>
        <!-- This function returns the page range for the element passed to it. If
             the element starts and ends on the same page, the results is the same
             as cup:firstPage() -->
        <xsl:variable name="pages" as="xs:string*" select="cup:firstAndLastPages($elem)"/>
        <xsl:choose>
            <xsl:when test="$pages[1] = $pages[2]">
                <xsl:value-of select="$pages[1]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($pages[1], ' – ',  $pages[2])"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
</xsl:stylesheet>