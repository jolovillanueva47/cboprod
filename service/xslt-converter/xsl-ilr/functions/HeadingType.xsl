<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">

    
    <!-- =========== HEADING TYPE ===============   -->
    <!-- HEADING - h1 h2 h3 or h4 etc -->
    <!-- counts the number of ancestors to get the heading level -->
    <xsl:function name="cup:HeadingType" as="xs:string">
        <xsl:param name="parent" as="element()"/>
        <xsl:variable name="number" as="xs:double">
            <xsl:value-of select="count($parent/ancestor-or-self::sec) + count($parent/ancestor-or-self::book-part) 
                + count($parent/ancestor-or-self::front-matter-part)
                + count($parent/ancestor-or-self::preface)
                + count($parent/ancestor-or-self::foreword)
                + count($parent/ancestor-or-self::glossary)
                +1"/>
        </xsl:variable>
        <!-- html allows only h1 - h6 -->
        <xsl:variable name="number-below-seven" as="xs:double">
            <xsl:choose>
                <xsl:when test="$number &gt; 6">6</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$number"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="not($parent/ancestor::boxed-text)">
                <xsl:value-of select="concat('h', string($number-below-seven) )"/>
            </xsl:when>
            <xsl:otherwise>div</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>