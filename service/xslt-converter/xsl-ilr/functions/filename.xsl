<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
<!--
    gives the html filename of a BITS element
    used at the add-filenames stage
    -->
    <xsl:function name="cup:Filename" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="elem" select="local-name($element)" as="xs:string"/>
        <xsl:variable name="count"
            select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1" as="xs:double"/>
        <xsl:variable name="total"
            select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "
            as="xs:double"/>
        <xsl:choose>
            <xsl:when test="$total &gt; 1">
                <xsl:value-of select="concat($elem, $count , '.html')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($elem, '.html')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
</xsl:stylesheet>