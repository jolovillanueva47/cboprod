<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
    <!-- This stylesheet is for anything to do with footnotes and their marker (i.e. xrefs) in 
         CUP-BITS documents. -->
    
    <xsl:function name="cup:isFootnoteMarker" as="xs:boolean">
        <xsl:param name="elem" as="element()"/>
        <!-- Returns true if the element passed to it is an xref which either has been given a @ref-type which indicates it 
             should point to an fn element, or, as a last resort, this function will go and look to see if the @rid
             points to an fn element. -->
        <xsl:variable name="book" select="$elem/ancestor::book"/>
        <xsl:choose>
            <xsl:when test="not($elem/self::xref)">
                <xsl:value-of select="false()"/>
            </xsl:when>
            <xsl:when test="$elem/@ref-type = (
                'footnote',
                'endnote',
                'source-note',
                'margin-note',
                'ack-fn',
                'collation-note',
                'commentary-note',
                'textual-note',
                'expl-note',
                'emend-note',
                'eol-note',
                'variant-note',
                'apparatus-note',
                'hyphen-note',
                'table-fn'
                )">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- Last resort, for documents conforming to older schema versions, or if new types are created -->
                <xsl:value-of select="boolean($book//fn[@id=$elem/@rid])"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    
</xsl:stylesheet>