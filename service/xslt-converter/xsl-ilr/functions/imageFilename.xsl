<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">

<!--image-path could be added as a parameter if required-->
    <xsl:variable name="image-path" select="'../Images/'"/>

<!--creates image filename for graphic or inline-graphic-->
    <xsl:function name="cup:imageFilename" as="xs:string">
        <xsl:param name="link" as="xs:string"/>
        <xsl:param name="mime-subtype"/>
        <xsl:value-of select="concat($image-path, $link, '.', $mime-subtype)"/>
    </xsl:function>
    
   
</xsl:stylesheet>