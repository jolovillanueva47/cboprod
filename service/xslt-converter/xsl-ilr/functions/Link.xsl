<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:mml="http://www.w3.org/1998/Math/MathML"
    exclude-result-prefixes="cup xs"
    version="2.0">
    
    
    <xsl:key name="chunk-of-id" match="*" use="@id"/>

    
<!--    
        creates links in html where the link also includes the filename of the destination file
        this acts on a BITS file that has had filenames added to the xml as @filename
        -->
    <!-- =========== LINK    for chunked files ===============   -->
    <xsl:function name="cup:Link" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>

        <xsl:variable name="ancestor-with-filename">
            <xsl:call-template name="ancestorWithFilename">
                <xsl:with-param name="elem" select="$element" as="element()"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$element/key('chunk-of-id',$link-att)/@in-file | $element/key('chunk-of-id',$link-att)/@filename"/>
        
        <xsl:variable name="source-filename" select="$element/@in-file |$element/@filename" as="xs:string"/>
        <xsl:choose>
            
            <xsl:when test="$source-filename=$dest-filename">
                <xsl:value-of select="concat('#', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($dest-filename, '#', $link-att)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== ANCESTOR WITH FILENAME  used from link function===============   -->
    <xsl:template name="ancestorWithFilename" as="xs:string">
        <xsl:param name="elem" as="element()"/>
        <xsl:choose>
            <xsl:when test="$elem/@filename">
                <xsl:value-of select="$elem/@filename"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="ancestorWithFilename">
                    <xsl:with-param name="elem" select="$elem/parent::*" as="element()"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>