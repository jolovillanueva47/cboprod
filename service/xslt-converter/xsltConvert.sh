#!/bin/sh

HOME="/app/service/xslt-converter"
INPUT="/app/service/xslt-converter/"
LOG="$HOME/logs/"
EXEC_JAR=$HOME"/CboXsltConverter.jar"
JAVA="java"
NOW=`date '+xslt-converter.%Y-%m-%d'`
LOG_FILE="$LOG$NOW.txt"

LOCK=/app/service/xslt-converter/lockfile

#mkdir $LOG

echo "!-*-*-*-* xsltConvert.sh checking for lockfile" >> $LOG_FILE

if [ -f $LOCK ]; then
  chmod 777 $LOCK
  exit
fi
touch $LOCK

echo "!-*-*-*-* xsltConvert.sh lockfile not found" >> $LOG_FILE

nohup $JAVA -jar -Xms128m -Xmx512m $EXEC_JAR $APP_ENV.properties $INPUT"isbnlist.txt" >> $LOG_FILE &
echo "xslt-converter Started. Log file: "$LOG_FILE
