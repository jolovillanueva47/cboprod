<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs" version="2.0">

    <!--
cup:Filename
cup:imageFilename
cup:Punctuation [for authors]
cup:SpanDiv
cup:HeadingType
cup:HeadingClass
cup:Link
cup:AncestorWithId [called from Link]
cup:backLink
-->
<!--    <xsl:variable name="prd-filename" select="concat($isbn, 'prd.xml')" as="xs:string"/>-->
    <xsl:variable name="manifest-file" select="document('manifest.xml', .)" as="document-node()"/>
    <!--<xsl:variable name="cbml-ver" select="substring-before(substring-after($product-file//content[1]/@dtd, ' V' ), '//EN')" as="xs:string"/>
-->
    <!--  variables to allow templates tobe reused whatever the DTD and namespace  -->
    <!--<xsl:variable name="chunk-lookup" select="document('chunk-lookup.xml', .)" as="document-node()"/>-->
    <xsl:variable name="block-elements" select="document('blocks.xml')/elements" as="element()"/>
    <xsl:variable name="headings-file" select="document('headings.xml')" as="document-node()"/>
    <xsl:variable name="title-elem" select="$headings-file//title" as="element()+"/>
    <xsl:variable name="subtitle-elem" select="$headings-file//subtitle" as="element()+"/>
    <xsl:variable name="label-elem" select="$headings-file//label" as="element()+"/>
    <xsl:variable name="list-label-elem" select="$headings-file//label[1]" as="element()"/>
    <xsl:variable name="title-page-elem" select="$headings-file//title-page" as="element()+"/>
    <xsl:variable name="level-elems" select="$headings-file//levels" as="element()"/>
    <xsl:variable name="inline-labels" select="$headings-file//inline" as="element()+"/>
    <xsl:variable name="para-elem" select="$headings-file//para" as="element()+"/>
    <xsl:variable name="sec-elem" select="$headings-file//section" as="element()+"/>
    <xsl:variable name="chunk-lookup" select="document('chunk-lookup.xml', .)"/>
    <xsl:key name="chunk-of-id" match="*" use="@id"/>



    <!-- =========== FILENAME ===============   -->
    <!--  filename to be element name plus count  -->
    <xsl:function name="cup:Filename" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="elem" select="local-name($element)" as="xs:string"/>
        <xsl:variable name="count"
            select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1" as="xs:double"/>
        <xsl:variable name="total"
            select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "
            as="xs:double"/>
        <xsl:choose>
            <xsl:when test="$total &gt; 1">
                <xsl:value-of select="concat($elem, $count , '.html')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($elem, '.html')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="cup:imageFilename" as="xs:string">
        <xsl:param name="link" as="xs:string"/>
        <xsl:variable name="linkitem" select="concat('^',$link,'\.')" as="xs:string"/>
        <xsl:variable name="product-item"
            as="element()">
            <!--            <xsl:choose>
                <xsl:when test="$manifest-file//file[matches(substring-after(@filename,'/'), $linkitem) and contains(@filename, '.png')]">-->
            <xsl:copy-of select="$manifest-file//file[matches(substring-after(@filename,'/'), $linkitem) and (contains(@filename, '.jpeg') or contains(@filename, '.jpg')) ]"/>
<!--                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$manifest-file//file[matches(substring-after(@filename,'/'), $linkitem) and not(contains(@name, '.eps') or contains(@name, '.tif'))]"/>
                </xsl:otherwise>
            </xsl:choose>-->
        </xsl:variable>
        <xsl:variable name="path-item" select="substring-after($product-item/@filename, '/')" as="xs:string"/>
        <xsl:value-of select="concat($image-path, $path-item)"/>
    </xsl:function>
    
    <xsl:function name="cup:hi-resImageFilename" as="xs:string">
        <xsl:param name="link" as="xs:string"/>
        <xsl:variable name="linkitem" select="concat('^',$link,'\.')" as="xs:string"/>
        <xsl:variable name="product-item"
            as="element()">
            <xsl:copy-of select="$manifest-file//file[matches(substring-after(@filename,'/'), $link) and not(contains(@filename, '.png'))]"/>
        </xsl:variable>
        <xsl:variable name="path-item" select="substring-after($product-item/@filename, '/')" as="xs:string"/>
        <xsl:variable name="stub" select="substring-before($path-item, '.')"></xsl:variable>
        <xsl:value-of select="concat($image-path, $stub, '.jpeg')"/>
    </xsl:function>


    <!-- =========== PUNCTUATION ===============   -->
    <!-- PUNCTUATION added between elements (cbml - used only for author-group) -->
    <xsl:function name="cup:Punctuation" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="name" select="name($element)" as="xs:string"/>
        <xsl:choose>
            <xsl:when
                test="count($element/following-sibling::*[name()= $name and not(@type[.!=$element/@type or not($element/@type)]) ] )&gt;1">, </xsl:when>
            <xsl:when
                test="count($element/following-sibling::*[name()= $name and not(@type[.!=$element/@type or not($element/@type)]) ] )=1"> and
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- =========== SPANDIV ===============   -->
    <!-- SPAN OR DIV  for an element -->
    <xsl:function name="cup:SpanDiv" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="inline-elements" select="document('elements.xml')/elements" as="element()"/>
        <xsl:choose>
            <xsl:when test="$inline-elements/element[string(.)=local-name($element)]">span</xsl:when>
            <xsl:otherwise>div</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- =========== HEADING TYPE ===============   -->
    <!-- HEADING - h1 h2 h3 or h4 etc -->
    <!-- counts the number of ancestors to get the heading level -->
    <xsl:function name="cup:HeadingType" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="number" as="xs:double">
            <xsl:choose>
                <xsl:when test="$element[local-name()=$title-page-elem]">1</xsl:when>
                <xsl:when test="$element[not(ancestor::*[*[local-name()=$title-elem or local-name()=$label-elem]])]">2</xsl:when>
                <xsl:when test="$element[contains(local-name(), 'box')]">4</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="count($element/ancestor::*[local-name()=$level-elems/level ])+2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- html allows only h1 - h6 -->
        <xsl:variable name="number-below-seven" as="xs:double">
            <xsl:choose>
                <xsl:when test="$number &gt; 6">6</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$number"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="(some $e in $level-elems//text() satisfies ($e eq local-name($element))) and not($element/ancestor::cbml:boxed-text)">
                <xsl:value-of select="concat('h', string($number-below-seven) )"/>
            </xsl:when>
            <!--<xsl:when test="$element[local-name()='boxed-text']">h5</xsl:when>-->
            <xsl:otherwise>div</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- =========== HEADING CLASS ===============   -->
    <!--  used only in the heading template  -->
    <!--  add a nesting level number for secs etc.  -->
    <xsl:function name="cup:HeadingClass" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:variable name="name" select="name($element)" as="xs:string"/>
        <xsl:variable name="box">
        <xsl:choose>
                <xsl:when test="$element/ancestor::cbml:boxed-text">box_</xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="count">
            <xsl:choose>
                <xsl:when test="$element/ancestor::cbml:boxed-text">
                    <xsl:value-of select="string(count($element/ancestor::*[name()=$name and ancestor::cbml:boxed-text]) +1 )"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="string(count($element/ancestor::*[name()=$name]) +1 )"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="contains($name,'history')">
                <xsl:value-of select="concat(local-name($element) ,'_', $element/@type)"/>
            </xsl:when>
            <xsl:when test="$element[parent::*[name()=$name] or child::*[name()=$name]] or $sec-elem[.= $name]">
                <xsl:value-of select="concat($box,local-name($element) , $count )"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($box,local-name($element))"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- =========== BLOCKS ===============   -->
    <!-- BLOCK ELEMENTS within a paragraph -->
    <xsl:function name="cup:Block" as="xs:string">
        <xsl:param name="element" as="node()"/>
        <xsl:choose>
            <xsl:when test="local-name($element) = $block-elements/element">yes</xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- =========== LINK    for chunked files ===============   -->
    <xsl:function name="cup:Link" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
        <xsl:variable name="ancestor-with-id" as="xs:string">
            <xsl:call-template name="ancestorWithId">
                <xsl:with-param name="elem" select="$element" as="element()"/>
            </xsl:call-template>
        </xsl:variable>
        <!--<xsl:variable name="dest-filename" select="$element/ancestor::cbml:book/descendant::*[@id=$link-att]/@filename"/>-->
        <xsl:variable name="dest-filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/ancestor-or-self::chunk/@filename"/>
        <xsl:variable name="source-filename" select="$chunk-lookup/key('chunk-of-id', $ancestor-with-id)/ancestor-or-self::chunk/@filename"/>
        <xsl:choose>
            <xsl:when test="$source-filename=$dest-filename">
                <xsl:value-of select="concat('#', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($dest-filename, '#', $link-att)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
<!--    for link in nav file-->
    <xsl:function name="cup:navLink" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
        <xsl:variable name="filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/ancestor-or-self::chunk/@filename"/>
        <xsl:value-of select="concat($filename, '#', $link-att)"/>
    </xsl:function>
    
<!--    <xsl:function name="cup:seeLink" as="xs:string">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
        <xsl:variable name="filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/ancestor-or-self::chunk/@filename"/>
        <xsl:value-of select="concat($filename, '#', $link-att)"/>
    </xsl:function>-->

    <xsl:function name="cup:seeLink">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
                <xsl:value-of select="concat('#', $link-att)"/>
    </xsl:function>

    <!-- =========== ANCESTOR WITH ID  used from link function===============   -->
    <xsl:template name="ancestorWithId" as="xs:string">
        <xsl:param name="elem" as="element()"/>
        <xsl:choose>
            <xsl:when test="$elem/@id">
                <xsl:value-of select="$elem/@id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="ancestorWithId">
                    <xsl:with-param name="elem" select="$elem/parent::*" as="element()"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--=========BACK LINK TO XREF  from fn and numbered citations ===========-->
    <xsl:function name="cup:backLink">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
        <xsl:variable name="ancestor-with-id" as="xs:string">
            <xsl:call-template name="ancestorWithId">
                <xsl:with-param name="elem" select="$element" as="element()"/>
            </xsl:call-template>
        </xsl:variable>
        <!--        <xsl:variable name="dest-filename"
            select="$element/ancestor::cbml:book/descendant::*[@id=$link-att]/ancestor::*[@filename and not(child::*[@filename])]/@filename"
            as="xs:string"/>
        <xsl:variable name="source-filename" select="$element/ancestor::*[@filename and not(child::*[@filename])]/@filename"
            as="xs:string"/>-->
        <xsl:variable name="dest-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@href=$link-att and not (preceding-sibling::chunk [ */@href=$link-att ]) ]/@filename"/>
        <xsl:variable name="source-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@id=$ancestor-with-id]/@filename"/>
        <xsl:choose>
            <xsl:when test="$source-filename=''">ERROR</xsl:when>
            <xsl:when test="string($source-filename)=string($dest-filename) or $element[self::cbml:fn]">
                <xsl:value-of select="concat('#rp', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($dest-filename, '#rp', $link-att)"/>

            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="cup:authorType">
        <xsl:param name="elem" as="element()"/>
        <xsl:param name="count" as="xs:double"/>
        <xsl:choose>
            <xsl:when test="$count=1">
                <xsl:choose>
                    <xsl:when test="$elem/@type='authors'">Author</xsl:when>
                    <xsl:when test="$elem/@type='editors'">Editor</xsl:when>
                    <xsl:when test="$elem/@type='contributors'">Contributor</xsl:when>
                    <xsl:when test="$elem/@type='translators'">Translator</xsl:when>
                    <xsl:when test="$elem/@type='foreword-authors'">Foreword author</xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$elem/@type='authors'">Authors</xsl:when>
                    <xsl:when test="$elem/@type='editors'">Editors</xsl:when>
                    <xsl:when test="$elem/@type='contributors'">Contributors</xsl:when>
                    <xsl:when test="$elem/@type='translators'">Translators</xsl:when>
                    <xsl:when test="$elem/@type='foreword-authors'">Foreword authors</xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:function>

    <!--  ==============table tint strength ===================  -->
    <xsl:function name="cup:tintStrength" as="xs:string">
        <xsl:param name="tint" as="xs:string"/>
        <xsl:variable name="num" select="number(substring-before($tint, '%'))" as="xs:double"/>
        <xsl:variable name="num2" select="255 - ($num *2.55)" as="xs:double"/>
        <xsl:value-of select="concat('RGB(', $num2, ',', $num2, ',',$num2, ')')"/>
    </xsl:function>

    <xsl:function name="cup:int-to-hex" as="xs:string">
        <xsl:param name="in" as="xs:integer"/>
        <xsl:sequence
            select="if ($in eq 0) then '0' else concat(if ($in ge 16) then cup:int-to-hex($in idiv 16) else '',substring('0123456789ABCDEF',($in mod 16) + 1,1))"
        />
    </xsl:function>
    
    <xsl:function name="cup:hex-to-int" as="xs:integer">
        <xsl:param name="in" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="substring($in,string-length($in),1) castable as xs:integer">
                <xsl:value-of select="number(substring($in,string-length($in),1)) + (16 * cup:hex-to-int(substring($in,1,(string-length($in)-1))))"/>
            </xsl:when>
            <xsl:when test="matches(substring($in,string-length($in),1),'[A-F]','i')">
                <xsl:value-of select="number((string-length(substring-before('0123456789ABCDEF',substring($in,string-length($in),1)))) + (16 * cup:hex-to-int(substring($in,1,(string-length($in)-1)))))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="0"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>


    <!--  ==============table popoout test ===================  -->

    <xsl:function name="cup:tablePopout">
        <xsl:param name="elem" as="node()"/>
        <xsl:choose>
            <xsl:when
                test="$elem[cbml:label or cbml:table[count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land']]"
                >yes</xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>
