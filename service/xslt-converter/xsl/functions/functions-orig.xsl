<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs"
    version="2.0">
   
<!--  Amended functions for use in cbo2html -->
    <!--
cup:Filename
cup:imageFilename
cup:Punctuation [for authors]
cup:SpanDiv
cup:HeadingType
cup:HeadingClass
cup:Link
cup:AncestorWithId [called from Link]
cup:backLink
-->
   <!-- <xsl:variable name="prd-filename" select="concat($isbn, 'prd.xml')"/>
    <xsl:variable name="product-file" select="document($prd-filename, .)" as="node()"/>
    <xsl:variable name="cbml-ver" select="substring-before(substring-after($product-file//content[1]/@dtd, ' V' ), '//EN')"/>-->
    
    <!--  variables to allow templates tobe reused whatever the DTD and namespace  -->
    <!--<xsl:variable name="chunk-lookup" select="document('chunk-lookup.xml', .)"/>-->
    <xsl:variable name="block-elements" select="document('blocks.xml')/elements"/>
    <xsl:variable name="headings-file" select="document('headings.xml')"/>
    <xsl:variable name="title-elem" select="$headings-file//title"/>
    <xsl:variable name="subtitle-elem" select="$headings-file//subtitle"/>
    <xsl:variable name="label-elem" select="$headings-file//label"/>
    <xsl:variable name="list-label-elem" select="$headings-file//label[1]"/>
    <xsl:variable name="title-page-elem" select="$headings-file//title-page"/>
    <xsl:variable name="level-elems" select="$headings-file//levels"/>
    <xsl:variable name="inline-labels" select="$headings-file//inline"/>
    <xsl:variable name="para-elem" select="$headings-file//para"/>
    <xsl:variable name="sec-elem" select="$headings-file//section"/>
    <xsl:key name="chunk-of-id" match="chunk" use="*/@id"/>

    
    
    <!-- =========== FILENAME ===============   -->
<!--  not needed as filenames already in cbo header -->
<!--    <xsl:function name="cup:Filename">
        <xsl:param name="element"/>
        <xsl:variable name="elem" select="local-name($element)"/>
        <xsl:variable name="count" select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1"/>
        <xsl:variable name="total" select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "/>
        <xsl:choose>
            <xsl:when test="$total &gt; 1">
                <xsl:value-of select="concat($elem, $count , '.html')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($elem, '.html')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->
    
<!--    <xsl:function name="cup:imageFilename">
        <xsl:param name="link"/>
        <xsl:param name="type"/>

                <xsl:variable name="linkitem" select="concat($link, '.')"></xsl:variable>
                <xsl:variable name="product-item" select="$product-file//item[contains(@name, $linkitem) and not(contains(@name, '.eps'))][1]"/>
                <xsl:variable name="path-item" select="substring-after($product-item/@name, '/')"/>
                <xsl:value-of select="concat($image-path, $path-item)"/>

     </xsl:function>-->
    
    <xsl:function name="cup:imageFilename">
        <xsl:param name="link"/>
        <xsl:value-of select="concat($image-path, $link)"/>
        
    </xsl:function>
<!--    <xsl:function name="cup:imageFilename">
        <xsl:param name="link"/>

                <xsl:variable name="linkitem" select="concat($link, '.')"></xsl:variable>

        <xsl:variable name="product-item" select="$product-file//item[contains(@name, $linkitem) and not(contains(@name, '.eps'))]"/>
                <xsl:variable name="path-item" select="substring-after($product-item/@name, '/')"/>
                <xsl:value-of select="concat($image-path, $path-item)"/>

    </xsl:function>-->
    
    <!-- =========== PUNCTUATION ===============   -->
    <!-- PUNCTUATION added between elements (cbml - used only for author-group) -->
    <xsl:function name="cup:Punctuation">
        <xsl:param name="element"/>
        <xsl:variable name="name" select="name($element)"/>
        <xsl:choose>
            <xsl:when test="count($element/following-sibling::*[name()= $name and not(@type[.!=$element/@type or not($element/@type)]) ] )&gt;1">, </xsl:when>
            <xsl:when test="count($element/following-sibling::*[name()= $name and not(@type[.!=$element/@type or not($element/@type)]) ] )=1"> and </xsl:when>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== SPANDIV ===============   -->
    <!-- SPAN OR DIV  for an element -->
    <xsl:function name="cup:SpanDiv">
        <xsl:param name="element"/>
        <xsl:variable name="inline-elements" select="document('elements.xml')/elements"/>
        <xsl:choose>
            <xsl:when test="$inline-elements/element[string(.)=local-name($element)]">span</xsl:when>
            <xsl:otherwise>div</xsl:otherwise>
        </xsl:choose> 
    </xsl:function>
    
    <!-- =========== HEADING TYPE ===============   -->
    <!-- HEADING - h1 h2 h3 or h4 etc -->
    <!-- counts the number of ancestors to get the heading level -->
<!--    this could be different for cbo so the main heading in each html file is h1-->
    <xsl:function name="cup:HeadingType" as="xs:string">
        <xsl:param name="element"/>
        <xsl:variable name="number">
            <xsl:choose>
                <xsl:when test="$element[local-name()=$title-page-elem]">1</xsl:when>
                <xsl:when test="$element[not(ancestor::*[*[local-name()=$title-elem or local-name()=$label-elem]])]">2</xsl:when>
                <xsl:when test="$element[contains(local-name(), 'box')]">4</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="count($element/ancestor::*[local-name()=$level-elems/level ])+2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- html allows only h1 - h6 -->
        <xsl:variable name="number-below-seven">
            <xsl:choose>
                <xsl:when test="$number &gt; 6">6</xsl:when>
                <xsl:otherwise><xsl:value-of select="$number"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$element[contains($level-elems, local-name())]">
                <xsl:value-of select="concat('h', string($number-below-seven) )"/>
            </xsl:when>
            <!--<xsl:when test="$element[local-name()='boxed-text']">h5</xsl:when>-->
            <xsl:otherwise>div</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== HEADING CLASS ===============   -->
<!--  used only in the heading template  -->
<!--  add a nesting level number for secs etc.  -->
    <xsl:function name="cup:HeadingClass" as="xs:string">
        <xsl:param name="element"/>
        <xsl:variable name="name" select="name($element)"></xsl:variable>
        <xsl:choose>
            <xsl:when test="$element[parent::*[name()=$name] or child::*[name()=$name]] or $sec-elem[.= $name]">
                <xsl:value-of select="concat(local-name($element) , string(count($element/ancestor::*[name()=$name]) +1 ) )"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="local-name($element)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== BLOCKS ===============   -->
    <!-- BLOCK ELEMENTS within a paragraph -->
    <xsl:function name="cup:Block" as="xs:string">
        <xsl:param name="element" as="node()"/>
        <xsl:choose>
            <xsl:when test="local-name($element) = $block-elements/element">yes</xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== LINK    for chunked files ===============   -->
<!--    <xsl:function name="cup:Link">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:variable name="ancestor-with-id">
            <xsl:call-template name="anncestorWithId">
                <xsl:with-param name="elem" select="$element"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/@filename"/>
        <xsl:variable name="source-filename" select="$chunk-lookup/key('chunk-of-id', $ancestor-with-id)/@filename"/>

                <xsl:choose>
                    <xsl:when test="$source-filename=$dest-filename">
                        <xsl:value-of select="concat('#', $link-att)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($dest-filename, '#', $link-att)"/>
                    </xsl:otherwise>
                </xsl:choose>
    </xsl:function>-->
    
    <xsl:function name="cup:Link">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
       
        <xsl:variable name="str1" select="substring-before($link-att, '#')"/>
        <xsl:variable name="str2" select="substring-before($str1, '.')"/>
        <xsl:variable name="str3" select="substring-after($link-att, '#')"></xsl:variable>
        <xsl:choose>
            <xsl:when test="$str1=''">
                <xsl:value-of select="$link-att"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($str2, '.html#', $str3)"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:function>
    

    <xsl:function name="cup:seeLink">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:param name="prec-entry"/>
        <xsl:variable name="str1" select="substring-before($link-att, '#')"/>
        <xsl:variable name="str2" select="substring-before($str1, '.')"/>
        <xsl:variable name="str3" select="substring-after($link-att, '#')"></xsl:variable>
        <xsl:choose>
            <xsl:when test="$str1=''">
                <xsl:value-of select="$link-att"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($str2, '.html', $str3)"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function>
    
    
    <xsl:function name="cup:seeLink">
        <xsl:param name="element" as="element()"/>
        <xsl:param name="link-att" as="xs:string"/>
        <xsl:value-of select="concat('#', $link-att)"/>
    </xsl:function>
    
    <!-- =========== ANCESTOR WITH ID  used from link function===============   -->
    <xsl:template name="anncestorWithId">
        <xsl:param name="elem"/>
        <xsl:choose>
            <xsl:when test="$elem/@id">
                <xsl:value-of select="$elem/@id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="anncestorWithId">
                    <xsl:with-param name="elem" select="$elem/parent::*"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--=========BACK LINK TO XREF  from fn and numbered citations ===========-->
    <xsl:function name="cup:backLink">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:variable name="ancestor-with-id">
            <xsl:call-template name="anncestorWithId">
                <xsl:with-param name="elem" select="$element"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@href=$link-att and not (preceding-sibling::chunk [ */@href=$link-att ]) ]/@filename"/>
        <xsl:variable name="source-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@id=$ancestor-with-id]/@filename"/>
        <xsl:choose>
            <xsl:when test="$source-filename=$dest-filename">
                <xsl:value-of select="concat('#rp', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($dest-filename, '#rp', $link-att)"/>
            </xsl:otherwise>
        </xsl:choose>    
    </xsl:function>
    
    <!--  ==============table tint strength ===================  -->
    <xsl:function name="cup:tintStrength">
        <xsl:param name="tint"/>
        <xsl:variable name="num" select="number(substring-before($tint, '%'))"/>
        <xsl:variable name="num2" select="255 - ($num *2.55)"/>
        <xsl:value-of select="concat('RGB(', $num2, ',', $num2, ',',$num2, ')')"/>
    </xsl:function>
    
    
    <xsl:function name="cup:int-to-hex" as="xs:string">
        <xsl:param name="in" as="xs:integer"/><!--the codepoint of the char (or a component part thereof fed back in)-->
        <xsl:sequence select="if ($in eq 0) then '0' else concat(if ($in ge 16) then cup:int-to-hex($in idiv 16) else '',substring('0123456789ABCDEF',($in mod 16) + 1,1))"/>
    </xsl:function>
    
<xsl:function name="cup:hex-to-int" as="xs:integer">
        <xsl:param name="in" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="substring($in,string-length($in),1) castable as xs:integer">
                <xsl:value-of select="number(substring($in,string-length($in),1)) + (16 * cup:hex-to-int(substring($in,1,(string-length($in)-1))))"/>
            </xsl:when>
            <xsl:when test="matches(substring($in,string-length($in),1),'[A-F]','i')">
                <xsl:value-of select="number((string-length(substring-before('0123456789ABCDEF',substring($in,string-length($in),1)))) + (16 * cup:hex-to-int(substring($in,1,(string-length($in)-1)))))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="0"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>


    <!--  ==============table popoout test ===================  -->
    
    <xsl:function name="cup:tablePopout">
        <xsl:param name="elem" as="node()"/>
        <xsl:choose>
            <xsl:when test="$elem[cbml:label or cbml:table[count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land']]">yes</xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>


