<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xs m"
    version="2.0">
    
<!--  epub overrides  -->
    
    <xsl:import href="chunk-lookup.xsl"/>
    <xsl:import href="functions/epub-functions.xsl"/>
    
<!--  ============= ATTRIBUTE MODE =============  -->
    
    <xsl:template match="cbml:part" mode="attributes">
        <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
        <xsl:if test="*[not(self::cbml:chapter or self::cbml:conclusion or self::cbml:part or self::cbml:references)]//m:math[not(ancestor::cbml:table[../cbml:label or count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land'])]">
            <xsl:attribute name="math" select="'yes'"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="*" mode="attributes">
        <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
        <xsl:if test="descendant::m:math[not(ancestor::cbml:table-wrap[(cbml:label and cbml:table) or cbml:table[count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land']]
            or
            ancestor::cbml:table-wrap-group[(cbml:label and cbml:table-wrap/cbml:table) or cbml:table-wrap/cbml:table[count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land']]
            )]">
            <xsl:attribute name="math" select="'yes'"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:play" mode="attributes">
            <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
            <xsl:if test="cbml:character-list//m:math[not(ancestor::cbml:table[../cbml:label or count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land'])]">
                <xsl:attribute name="math" select="'yes'"/>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:dictionary-letter[not(preceding-sibling::cbml:dictionary-letter) and parent::cbml:dictionary]" mode="attributes">
            <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
            <xsl:if test="descendant::m:math[not(ancestor::cbml:table-wrap[contains(cup:tablePopout(.),'yes')])]">
                <!--<xsl:if test="descendant::m:math[not(ancestor::cbml:table[../cbml:label or count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land'])]">-->
                <xsl:attribute name="math" select="'yes'"/>
            </xsl:if>       
    </xsl:template>
    
    <xsl:template match="cbml:sub-book/cbml:prelims" mode="attributes">
            <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
            <xsl:if test="descendant::m:math[not(ancestor::cbml:table[../cbml:label or count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5 or @orient='land'])] ">
                <xsl:attribute name="math" select="'yes'"/>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:sub-book" mode="attributes">
            <xsl:attribute name="filename" select="cup:Filename(., $isbn)"/>
            <xsl:if test="cbml:title-page//m:math">
                <xsl:attribute name="math" select="'yes'"/>
            </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>