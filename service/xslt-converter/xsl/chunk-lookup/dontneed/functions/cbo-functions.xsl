<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0"
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup cbml  xs xd">


    <!-- =========== EXTENSION ===============   -->
    <!-- CHUNKED OUTPUT  file extension for chunked filename -->
   <!-- <xsl:variable name="file-extensions" select="document('file-extensions.xml',.)/elements"/>-->
    
   <!-- <xsl:function name="cup:cboFilename">
        <xsl:param name="element"/>
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="elem" select="local-name($element)"/>
        <xsl:variable name="count" select="(count($element/ancestor::*[local-name()=$elem]) + count($element/preceding::*[local-name() = $elem] ) + 1)"/>
        <xsl:variable name="total" select="count($element/ancestor::cbml:book/descendant::*[local-name() = $elem])"/>
        
               <xsl:variable name="extension">
            <xsl:choose>
                <xsl:when test="$total > 1">
                    <xsl:value-of select="concat(cup:cboExtension($elem), $count)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="cup:cboExtension($elem)"/>
                </xsl:otherwise>
            </xsl:choose> 
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$start=$end">
                <xsl:value-of select="concat($isbn, $extension, '_p', $start,  '_CBO.html')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($isbn, $extension,'_p', $start, '-', $end, '_CBO.html')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->
    
<!--    <xsl:function name="cup:cboExtension">
        <xsl:param name="elem"/>
              <xsl:choose>
            <xsl:when test="$file-extensions/elem[@name=$elem]/@CBOext">
                <xsl:value-of select="$file-extensions/elem[@name=$elem]/@CBOext"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$file-extensions/elem[@name=$elem]/@extension"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->
    
<!--    <xsl:function name="cup:pageStart">
        <xsl:param name="elem"/>
        <xsl:value-of select="$elem/preceding::processing-instruction('new-page')[1]"/>

    </xsl:function>-->
    
  <!--  <xsl:function name="cup:pageEnd">
        <xsl:param name="elem"/>
        <xsl:param name="id"/>
        <xsl:param name="foll-id"/>
        <xsl:choose>
            <xsl:when test="local-name($elem)='part'">
                <xsl:choose>
                    <xsl:when test="$elem/descendant::processing-instruction('new-page')
                        [not(ancestor::cbml:chapter or preceding::cbml:part[ancestor::cbml:part[@id=$id]] or preceding::cbml:chapter[ancestor::cbml:part[@id=$id]])]">
                        <xsl:choose>
							<xsl:when test="$elem/descendant::processing-instruction('new-page')[1] = $elem/descendant::*[@id=$foll-id][1]/preceding::processing-instruction('new-page')[1]">
                            		<xsl:value-of select="$elem/preceding::processing-instruction('new-page')[1]"/>
                            </xsl:when>
                            <xsl:when test="count(
                            $elem/descendant::processing-instruction('new-page')
                            		[not
                            			(
                            			ancestor::cbml:chapter or 
                            			preceding::cbml:part
                            									[ancestor::cbml:part[@id=$id]] or 
                            			preceding::cbml:chapter
                            									[ancestor::cbml:part[@id=$id]]
                            			)
                            		]
                            )-2 le 1">
                                <xsl:value-of select="$elem/descendant::processing-instruction('new-page')[not(ancestor::cbml:chapter or preceding::cbml:part[ancestor::cbml:part[@id=$id]] or preceding::cbml:chapter[ancestor::cbml:part[@id=$id]])][position()=last()-1]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$elem/descendant::*[@id=$foll-id][1]/preceding::processing-instruction('new-page')[2]"/>

                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$elem/preceding::processing-instruction('new-page')[1]"/>
                    </xsl:otherwise>
                </xsl:choose>  
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="count($elem/following::processing-instruction('new-page')[following::*[@id][1][@id=$foll-id]]) = 2">
                        <xsl:value-of select="$elem/following::processing-instruction('new-page')[1]"/>  
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="$elem/descendant::processing-instruction('new-page')">
                               <!-\-<xsl:value-of select="$elem/descendant::processing-instruction('new-page')[position()=last()]"/>-\-> 
                               <xsl:value-of select="$elem/descendant::processing-instruction('new-page')[position()=cup:CBOChunkPgMax($elem/descendant::processing-instruction('new-page'))]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$elem/preceding::processing-instruction('new-page')[1]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->


    <xsl:function name="cup:cboCatchIntros" as="xs:string">
        <xsl:param name="candidate"/>
        <xsl:choose>
            <xsl:when test="local-name($candidate) = 'part'">
                <xsl:value-of select="local-name($candidate)"/>
                <!--in case of parts called 'Introduction'-->
            </xsl:when>
						<xsl:when test="local-name($candidate) = 'play'">
						    <xsl:value-of select="'chapter'"/>
						</xsl:when>
            <xsl:when test="not(matches(local-name($candidate), 'drug|dictionary-entry|EGF|class|family|ack|act|answers|appendix|chapter|conclusion|contributors|endmatter-answers|endmatter-exercises|features|foreword|frontmatter|glossary|intro|list|notes|preface|references|series|sub-chapter|toc|index|play'))">
								<xsl:value-of select="generate-id($candidate)"/>
            </xsl:when>
            <xsl:when test="($candidate/cbml:label[1] = 'Abbreviations' or matches($candidate/cbml:title[1], 'abbreviation', 'i') or matches($candidate/cbml:title[2], 'abbreviation', 'i')) and (not($candidate/cbml:label[1] castable as xs:integer)) and (not(matches(string($candidate/cbml:label[1]), 'chapter', 'i')))">
                <xsl:value-of select="'abbreviations'"/>
            </xsl:when>
            <xsl:when test="($candidate/cbml:label[1] = 'Introduction' or matches($candidate/cbml:title[1], 'introduc', 'i') or matches($candidate/cbml:title[2], 'introduc', 'i')) and (not($candidate/cbml:label[1] castable as xs:integer)) and (not(matches(string($candidate/cbml:label[1]), 'chapter', 'i')))">
                <xsl:value-of select="'introduction'"/>
            </xsl:when>
            <xsl:when test="(matches($candidate/cbml:label[1], 'preface|prolog', 'i') or matches($candidate/cbml:label[1], 'preface|prolog', 'i') or matches($candidate/cbml:title[1], 'preface|prolog', 'i') or matches($candidate/cbml:title[2], 'preface|prolog', 'i')) and (not($candidate/cbml:label[1] castable as xs:integer)) and (not(matches(string($candidate/cbml:label[1]), 'chapter', 'i')))">
            		<xsl:value-of select="'preface'"/>
            </xsl:when>
            <xsl:when test="(matches($candidate/cbml:label[1], 'conclusion|afterword|epilog', 'i') or matches($candidate/cbml:title[1], 'conclusion|afterword|epilog', 'i') or matches($candidate/cbml:title[2], 'conclusion|afterword|epilog', 'i')) and (not($candidate/cbml:label[1] castable as xs:integer)) and (not(matches(string($candidate/cbml:label[1]), 'chapter', 'i')))">
                <xsl:value-of select="'conclusion'"/>
            </xsl:when>
            <xsl:when test="(matches($candidate/cbml:label[1], 'acknowledge*ments', 'i') or matches($candidate/cbml:title[1], 'acknowledge*ments', 'i')) and (not($candidate/cbml:label[1] castable as xs:integer)) and (not(matches(string($candidate/cbml:label[1]), 'chapter', 'i')))">
                <xsl:value-of select="'ack'"/>
            </xsl:when> 
            <xsl:when test="$candidate/parent::cbml:drug">
                <xsl:value-of select="'drug'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="local-name($candidate)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
<!--    <xsl:function name="cup:cboFilename">
        <xsl:param name="element"/>
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="elem" select="cup:cboCatchIntros($element)" as="xs:string"/>
        <xsl:variable name="count" select="(count($element/ancestor::*[cup:cboCatchIntros(.)=$elem]) + count($element/preceding::*[cup:cboCatchIntros(.) = $elem] ) + 1)"/>
        <xsl:variable name="total" select="(count($element/ancestor::*[cup:cboCatchIntros(.)=$elem]) + count($element/preceding::*[cup:cboCatchIntros(.) = $elem] ) + count($element/following::*[cup:cboCatchIntros(.) = $elem] ) + count($element/descendant::*[cup:cboCatchIntros(.) = $elem] ) + 1 )"/>
        <xsl:variable name="extension">
            <xsl:choose>
                <xsl:when test="$total > 1">
                    <xsl:value-of select="concat(cup:cboExtension($elem), $count)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="cup:cboExtension($elem)"/>
                </xsl:otherwise>
            </xsl:choose> 
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$start=$end">
                <xsl:value-of select="concat($isbn, $extension, '_p', $start,  '_CBO.html')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($isbn, $extension,'_p', $start, '-', $end, '_CBO.html')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->
    
<!--    <xsl:function name="cup:cboExtension">
        <xsl:param name="elem"/>
        <xsl:variable name="file-extensions" select="document('file-extensions.xml')/elements"/>
        <xsl:choose>
					<xsl:when test="$file-extensions/elem[@name=$elem]/@CBOext">
						<xsl:value-of select="$file-extensions/elem[@name=$elem]/@CBOext"/>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="$file-extensions/elem[@name=$elem]/@extension"/>
        	</xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->
   
    <xsl:function name="cup:CBOChunkPgMax">
        <!--accept sequence of new-page instructions as argument-->
        <!--return position of max value in list-->
        <!--pages numbered 'Plate' renumbered to -1 if not last in sequence, renumbered to max() + 1 if at end of sequence-->
        <xsl:param name="pagenum"/>
        <xsl:variable name="seq1" as="xs:double *">
        <xsl:for-each select="$pagenum">
         	<xsl:sequence select="if (. castable as xs:integer) then number(.) else cup:CBORomanToInteger(.)"/>
        </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="seq2" as="xs:double *">
        	<xsl:for-each select="$seq1">
						<xsl:choose>
						    <xsl:when test=". = -1 and position()=last()"><xsl:sequence select="number(max($seq1) + 1)"/></xsl:when>
						    <xsl:otherwise><xsl:sequence select="number(.)"/></xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="index-of($seq2, max($seq2))"/>
    </xsl:function>

<xsl:function name="cup:CBORomanToInteger" as="xs:integer">
  <xsl:param name="r" as="xs:string"/>
  <xsl:choose>
   <xsl:when test="matches($r,'^[^IVXLCDM]+','i')">
      <xsl:sequence select="-1"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'xc')">
      <xsl:sequence select="90 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'xl')">
      <xsl:sequence select="40 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'l')">
      <xsl:sequence select="50 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'c')">
      <xsl:sequence select="100 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'cd')">
   		<xsl:sequence select="400 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'d')">
      <xsl:sequence select="500 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'cm')">
   		<xsl:sequence select="900 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'m')">
      <xsl:sequence select="1000 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'iv')">
      <xsl:sequence select="4 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'ix')">
      <xsl:sequence select="9 + cup:CBORomanToInteger(substring($r,1,string-length($r)-2))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'iix')">
      <xsl:sequence select="8 + cup:CBORomanToInteger(substring($r,1,string-length($r)-3))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'i')">
      <xsl:sequence select="1 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'v')">
      <xsl:sequence select="5 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:when test="ends-with($r,'x')">
      <xsl:sequence select="10 + cup:CBORomanToInteger(substring($r,1,string-length($r)-1))"/>
   </xsl:when>
   <xsl:otherwise>
       <xsl:sequence select="0"/>
   </xsl:otherwise>
  </xsl:choose>
</xsl:function>
    
<xsl:function name="cup:cboMakeAbstract" as="item()*">
    <xsl:param name="in" as="item()*"/>
    
</xsl:function>

</xsl:stylesheet>