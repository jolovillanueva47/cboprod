<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs"
    version="2.0">
   
    <!--
cup:Filename
cup:imageFilename
cup:Punctuation [for authors]
cup:SpanDiv
cup:HeadingType
cup:HeadingClass
cup:Link
cup:AncestorWithId [called from Link]
cup:backLink
-->
    <xsl:variable name="prd-filename" select="concat($isbn, 'prd.xml')"/>
    <xsl:variable name="product-file" select="document($prd-filename, .)" as="node()"/>
    <xsl:variable name="cbml-ver" select="substring-before(substring-after($product-file//content[1]/@dtd, ' V' ), '//EN')"/>
    
    <!--  variables to allow templates tobe reused whatever the DTD and namespace  -->
    <xsl:variable name="chunk-lookup" select="document('chunk-lookup.xml', .)"/>
    <xsl:variable name="block-elements" select="document('blocks.xml')/elements"/>
    <xsl:variable name="headings-file" select="document('headings.xml')"/>
    <xsl:variable name="title-elem" select="$headings-file//title"/>
    <xsl:variable name="subtitle-elem" select="$headings-file//subtitle"/>
    <xsl:variable name="label-elem" select="$headings-file//label"/>
    <xsl:variable name="list-label-elem" select="$headings-file//label[1]"/>
    <xsl:variable name="title-page-elem" select="$headings-file//title-page"/>
    <xsl:variable name="level-elems" select="$headings-file//levels"/>
    <xsl:variable name="inline-labels" select="$headings-file//inline"/>
    <xsl:variable name="para-elem" select="$headings-file//para"/>
    <xsl:variable name="sec-elem" select="$headings-file//section"/>
    <xsl:key name="chunk-of-id" match="chunk" use="*/@id"/>

    
    
    <!-- =========== FILENAME ===============   -->
<!--  filename to be elemnent name plus count  -->
    <xsl:function name="cup:Filename">
        <xsl:param name="element"/>
        <xsl:variable name="elem" select="local-name($element)"/>
        <xsl:variable name="extension" select="cup:Extension($elem)"/>
        <xsl:variable name="count" select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1"/>
        <xsl:variable name="total" select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "/>
        <xsl:choose>
            <xsl:when test="$total &gt; 1">
                <xsl:value-of select="concat($elem, $count , '.xml')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($elem, '.xml')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="cup:Filename">
        <xsl:param name="element"/>
        <xsl:param name="isbn"/>
        <xsl:variable name="elem" select="local-name($element)"/>
        <xsl:variable name="extension" select="cup:Extension($elem)"/>
        <xsl:variable name="count" select="count($element/ancestor::*[local-name(.)=$elem] ) +count($element/preceding::*[local-name(.)=$elem] )+ 1"/>
        <xsl:variable name="total" select="count( $element/ancestor::*[local-name(.)=$elem] ) +count( $element/preceding::*[local-name(.)=$elem] ) +count( $element/following::*[local-name(.)=$elem] ) +1 "/>
        <xsl:choose>
            <xsl:when test="$total &gt; 1">
                <xsl:value-of select="concat($isbn,$extension, $count , '.xml')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($isbn,$extension, '.xml')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
<!--    <xsl:function name="cup:imageFilename">
        <xsl:param name="link"/>
        <xsl:param name="type"/>

                <xsl:variable name="linkitem" select="concat($link, '.')"></xsl:variable>
                <xsl:variable name="product-item" select="$product-file//item[contains(@name, $linkitem) and not(contains(@name, '.eps'))][1]"/>
                <xsl:variable name="path-item" select="substring-after($product-item/@name, '/')"/>
                <xsl:value-of select="concat($image-path, $path-item)"/>

     </xsl:function>-->
    
    
<!--    <xsl:function name="cup:imageFilename">
        <xsl:param name="link"/>

                <xsl:variable name="linkitem" select="concat($link, '.')"></xsl:variable>
        <xsl:variable name="product-item" select="$product-file//item[contains(@name, $linkitem) and not(contains(@name, '.eps'))]"/>
                <xsl:variable name="path-item" select="substring-after($product-item/@name, '/')"/>
                <xsl:value-of select="concat($image-path, $path-item)"/>

    </xsl:function>-->
    
 
    
    <!-- =========== LINK    for chunked files ===============   -->
    <xsl:function name="cup:Link">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:variable name="ancestor-with-id">
            <xsl:call-template name="anncestorWithId">
                <xsl:with-param name="elem" select="$element"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/@filename"/>
        <xsl:variable name="source-filename" select="$chunk-lookup/key('chunk-of-id', $ancestor-with-id)/@filename"/>

                <xsl:choose>
                    <xsl:when test="$source-filename=$dest-filename">
                        <xsl:value-of select="concat('#', $link-att)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($dest-filename, '#', $link-att)"/>
                    </xsl:otherwise>
                </xsl:choose>
    </xsl:function>
    
<!--  for chunked indexes  -->
    <xsl:function name="cup:seeLink">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:param name="prec-entry"/>
<!--   this bit won't work for chunked indexes as index-items don't have ids     -->
        <xsl:variable name="ancestor-with-id">
            <xsl:call-template name="anncestorWithId">
                <xsl:with-param name="elem" select="$element"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$chunk-lookup/key('chunk-of-id', $link-att)/@filename"/>
        <xsl:variable name="source-filename" select="$chunk-lookup/key('chunk-of-id', $prec-entry)/@filename"/>
        
        <xsl:choose>
            <xsl:when test="$source-filename=$dest-filename">
                <xsl:value-of select="concat('#', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
<!--       FOR TESTING         -->
                <!--<xsl:value-of select="concat($source-filename, '#', $link-att)"/>-->
                <xsl:value-of select="concat($dest-filename, '#', $link-att)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- =========== ANCESTOR WITH ID  used from link function===============   -->
    <xsl:template name="anncestorWithId">
        <xsl:param name="elem"/>
        <xsl:choose>
            <xsl:when test="$elem/@id">
                <xsl:value-of select="$elem/@id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="anncestorWithId">
                    <xsl:with-param name="elem" select="$elem/parent::*"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--=========BACK LINK TO XREF  from fn and numbered citations ===========-->
    <xsl:function name="cup:backLink">
        <xsl:param name="element"/>
        <xsl:param name="link-att"/>
        <xsl:variable name="ancestor-with-id">
            <xsl:call-template name="anncestorWithId">
                <xsl:with-param name="elem" select="$element"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dest-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@href=$link-att and not (preceding-sibling::chunk [ */@href=$link-att ]) ]/@filename"/>
        <xsl:variable name="source-filename" select="$element/document('chunk-lookup.xml', .)//chunk[*/@id=$ancestor-with-id]/@filename"/>
        <xsl:choose>
            <xsl:when test="$source-filename=$dest-filename">
                <xsl:value-of select="concat('#rp', $link-att)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($dest-filename, '#rp', $link-att)"/>
            </xsl:otherwise>
        </xsl:choose>    
    </xsl:function>
    
<!--  ==============table popoout test ===================  -->
    
    <xsl:function name="cup:tablePopout">
        <xsl:param name="elem" as="node()"/>
        <xsl:choose>
            <xsl:when test="$elem[cbml:label or cbml:table[count(cbml:tgroup/cbml:tbody/cbml:row)&gt;10 or count(cbml:tgroup[1]/cbml:colspec)&gt;5  or @orient='land']]">yes</xsl:when>
            <xsl:otherwise>no</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    
<!--  ==================== cbo filename ==============  -->
    
    <xsl:function name="cup:cboFilename">
        <xsl:param name="chunk" as="node()"/>
        <xsl:param name="isbn"></xsl:param>
        <xsl:variable name="fnm" select="substring-before($chunk-lookup/key('chunk-of-id', $chunk/@id)/@filename, '.xml')"/>
        <xsl:variable name="extension" select="cup:Extension(local-name($chunk))"/>
        <xsl:variable name="pis">
            <xsl:sequence select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')"></xsl:sequence>
        </xsl:variable>
        <xsl:variable name="first-page">
            <xsl:value-of  select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')[1]"/>
        </xsl:variable>
        <xsl:variable name="last-page">
            <xsl:value-of  select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')[last()]"/>
        </xsl:variable>
        <xsl:value-of select="concat($fnm, '_p', $first-page, '-', $last-page, '_CBO.xml')"/>
    </xsl:function>
    
    <xsl:function name="cup:Extension">
        <xsl:param name="elem"/>
        <xsl:variable name="file-extensions" select="document('file-extensions.xml')/elements"/>
        <xsl:value-of  select="$file-extensions/elem[@name=$elem]/@extension"/>
    </xsl:function>
    
<!--    <xsl:function name="cup:cboFilename">
        <xsl:param name="chunk" as="node()"/>
        <xsl:param name="isbn"></xsl:param>
        <xsl:variable name="pis">
            <xsl:sequence select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')"></xsl:sequence>
        </xsl:variable>
        <xsl:variable name="first-page">
            <xsl:value-of  select="max($pis)"/>
        </xsl:variable>
        <xsl:variable name="last-page">
            <xsl:value-of  select="min($pis)"/>
        </xsl:variable>
        <xsl:variable name="first-page1">
            <xsl:value-of  select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')[1]"/>
        </xsl:variable>
        <xsl:variable name="last-page2">
            <xsl:value-of  select="$chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')[last()]"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="number($chunk-lookup/key('chunk-of-id', $chunk/@id)//processing-instruction('new-page')[1]) &gt; 0">
                <xsl:value-of select="concat($isbn, '_p', $first-page, '-', $last-page, '_CBO.xml')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($isbn, '_p', $first-page1, '-', $last-page2, '_CBO.xml')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>-->

</xsl:stylesheet>


