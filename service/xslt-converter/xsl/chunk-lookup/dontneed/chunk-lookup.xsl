<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xs m"
    version="2.0">
    
<!--  MIAN CHUNK LOOKUP TO BE IMPORTED FROM CBO OR EPUB
-->
    
    <!--<xsl:import href="import/chunk-lookup-import.xsl"/>-->

    <xsl:param name="isbn"/>
    <xsl:key name="elem-id" match="*" use="@id"/>
    
    <xsl:output method="xml" indent="yes"/>

   <!-- <xsl:template match="/">
        <book>
            <xsl:apply-templates/>
        </book>
    </xsl:template>
    
    <xsl:template match="cbml:book">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:prelims|cbml:main |cbml:endmatter|cbml:dictionary | cbml:EGF">
        <xsl:apply-templates mode="chunks"/>
    </xsl:template>
    
    <xsl:template match="cbml:series" mode="chunks">
         <xsl:apply-imports/>
         <xsl:apply-templates select="cbml:series-books" mode="chunks"/>   
    </xsl:template>
    
    <xsl:template match="cbml:index" mode="chunks">
                <xsl:apply-imports/>
    </xsl:template>-->
    

<!--  ============MODE CHUNKS ================  -->

    <!--<xsl:template match="cbml:part" mode="chunks">  
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/> 
            
            <xsl:apply-templates select="." mode="ids"/> 
            <xsl:apply-templates select="cbml:label | cbml:title | cbml:author-group | cbml:disp-quote | cbml:part-toc | cbml:fig | cbml:verse-group |cbml:intro" mode="ids"/>
            <xsl:apply-templates select="." mode="pages"/>
            <xsl:apply-templates select="*[not(self::cbml:chapter or self::cbml:conclusion or self::cbml:part or self::cbml:references)]
                |processing-instruction()[not(preceding-sibling::cbml:chapter)]" mode="desc-pages"/>
        </chunk> 
        <xsl:apply-templates select="cbml:chapter | cbml:part | cbml:conclusion | cbml:references | cbml:appendix | cbml:document | cbml:drug | cbml:dictionary" mode="chunks"/>
    </xsl:template>
    
    
    <xsl:template match="cbml:EGF" mode="chunks">  
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/> 
            
            <xsl:apply-templates select="." mode="ids"/> 
            <xsl:apply-templates select="cbml:key" mode="ids"/>
            <xsl:apply-templates select="." mode="pages"/>
            <xsl:apply-templates select="cbml:key" mode="desc-pages"/>
        </chunk> 
        <xsl:apply-templates select="cbml:class" mode="chunks"/>
    </xsl:template>
    
    
    <xsl:template match="cbml:class" mode="chunks">  
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/> 
            
            <xsl:apply-templates select="." mode="ids"/> 
            <xsl:apply-templates select="cbml:key" mode="ids"/>
            <xsl:apply-templates select="." mode="pages"/>
            <xsl:apply-templates select="cbml:key" mode="desc-pages"/>
        </chunk> 
        <xsl:apply-templates select="cbml:family" mode="chunks"/>
    </xsl:template>
    
    <xsl:template match="cbml:play" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="cbml:character-list
                |processing-instruction()[not(preceding-sibling::cbml:chapter)]" mode="ids"/>
            <xsl:apply-templates select="." mode="all-pages"/>
        </chunk>
        <xsl:apply-templates select="cbml:act | cbml:play-notes | cbml:commentary" mode="chunks"/>
    </xsl:template>
    
    <xsl:template match="cbml:dictionary-letter[not(preceding-sibling::cbml:dictionary-letter) and parent::cbml:dictionary]" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="parent::cbml:dictionary" mode="ch-ids"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="." mode="all-pages"/>
        </chunk>
        <xsl:apply-templates select="cbml:dictionary-entry" mode="chunks"/>
    </xsl:template>
    
<!-\-  ==== SUB-BOOK ==== -\->
    
    <xsl:template match="cbml:sub-main|cbml:dictionary" mode="chunks">
        <xsl:apply-templates mode="chunks"/>
    </xsl:template>
    
    <xsl:template match="cbml:sub-book/cbml:prelims" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="." mode="all-pages"/>
        </chunk>
    </xsl:template>
    
    <xsl:template match="cbml:sub-book" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="cbml:title-page" mode="ids"/>
            <xsl:apply-templates select="." mode="pages"/>
            <xsl:apply-templates select="cbml:title-page" mode="desc-pages"/>
        </chunk>
        <xsl:apply-templates select="*[not(self::cbml:title-page)]" mode="chunks"/>
    </xsl:template>
    -->

<!--  ============== MODE PAGES ================  -->
    
    <xsl:template match="cbml:part" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:chapter])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:EGF" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:class])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="cbml:class" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:family])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:play" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:act])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
<!--  ================= MODE IDS =================  -->
    
    <xsl:template match="*" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:chapter" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="*[not(@position='float')] | processing-instruction('anchor')" mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:part|cbml:sub-book |cbml:dictionary |cbml:play | cbml:class" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="cbml:chapter//cbml:sub-book//cbml:part | cbml:chapter//cbml:sub-book" mode="ids"> 
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:EGF" mode="ids">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="cbml:key[1]/@id"/>
            </xsl:element>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="processing-instruction('anchor')" mode="ids"/>
    
    <xsl:template match="processing-instruction('new-page')">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="text()|processing-instruction()" mode="ids"/>


</xsl:stylesheet>
