<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0"
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup cbml  xs xd">
        
    <xsl:import href="chunk-lookup.xsl"/>
    <xsl:import href="functions/cbo-functions.xsl"/>
    <xsl:variable name="file-extensions" select="document('file-extensions.xml')/elements"/>

		<xsl:template match="cbml:book">
    		<xsl:apply-templates select="cbml:prelims"/>
    		<xsl:apply-templates select="cbml:main/*[not(self::cbml:plates)] | cbml:dictionary/*| cbml:endmatter/*" mode="chunks"/>
    		<xsl:apply-templates select="//cbml:plates" mode="chunks"/> <!--move plates sections to end of document... maybe?-->
		</xsl:template> 

    <xsl:template match="cbml:prelims">
        <xsl:variable name="id" select="descendant::*[1]/@id"/>
        <xsl:variable name="start" select="'i'"/>
        <xsl:variable name="end">
              <xsl:value-of select="//processing-instruction('new-page')[local-name(following::*[1])='toc'][1]/preceding::processing-instruction('new-page')[1]"/>
        </xsl:variable>
        <chunk>
            <xsl:attribute name="filename" select="concat($isbn, 'pre_pi-', $end, '_CBO.html')"/>
            <xsl:attribute name="id" select="$id"/>
            <xsl:attribute name="page-start" select="$start"/>
            <xsl:attribute name="page-end" select="$end"/>
            <xsl:attribute name="doi" select="'n'"/>
            <xsl:apply-templates select="*[not(preceding::cbml:toc or self::cbml:toc)]" mode="ids"/>
        </chunk>
        <xsl:apply-templates select="cbml:toc|node()[preceding::cbml:toc]" mode="chunks"/>
    </xsl:template>
    
<!--  xml for drug is not to be chunked into sections as drug info is needed  -->
<!--    <xsl:template match="cbml:drug" mode="chunks">
        <xsl:apply-templates select="*[not(self::cbml:title or self::cbml:label or self::cbml:subtitle or self::cbml:alt-title or self::cbml:author-group)]" mode="chunks"/>
    </xsl:template>-->
    

    <!--  ============MODE CHUNKS ================  -->
    
    <xsl:template match="cbml:plates"  mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
        </chunk>

    </xsl:template>
    
    <xsl:template match="cbml:dictionary" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="." mode="all-pages"/>
        </chunk>
        <xsl:apply-templates select="cbml:dictionary-letter" mode="chunks"/>
    </xsl:template>
    
    <xsl:template match="cbml:dictionary-letter" mode="chunks">
        <xsl:apply-templates select="cbml:dictionary-entry" mode="chunks"/>
        <xsl:if test="cbml:boxed-text">
            <chunk>
                <xsl:apply-templates select="." mode="attributes"/>
                <xsl:apply-templates select="*[not(self::cbml:dictonary-entry)]" mode="ids"/>
                <xsl:apply-templates select="." mode="pages"/>
            </chunk>
        </xsl:if>
    </xsl:template>
    
    <!--  ============ MODE ATTRIBUTES ============== -->
  
  
    <xsl:template match="cbml:part" mode="attributes">  
        <xsl:variable name="start" select="cup:pageStart(.)"/>
        <xsl:variable name="end" select="cup:pageEnd(., @id, descendant::*[not(self::cbml:intro or self::cbml:target) and @id][1]/@id)"/>
            <xsl:if test="processing-instruction('new-page')[not(preceding-sibling::cbml:chapter or preceding-sibling::cbml:part)] or cbml:intro or cbml:part-toc">
                <xsl:attribute name="filename" select="cup:cboFilename(., $start, $end)"/>
                <xsl:attribute name="page-start" select="$start"/>
                <xsl:attribute name="page-end" select="$end"/>
                <xsl:attribute name="doi" select="'y'"/>
                <xsl:attribute name="jpeg-page">
                    <xsl:choose>
                        <xsl:when test="descendant::cbml:p[not(ancestor::cbml:caption)]">
                            <xsl:value-of select="descendant::cbml:p[not(ancestor::cbml:chapter or ancestor::cbml:caption)][1]/preceding::processing-instruction('new-page')[1]"/>
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="$start"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="content-type" select="'part-title'"/>
            <xsl:attribute name="content-extn" select="'ptl'"/>                     
            <xsl:attribute name="id" select="@id"/>
    </xsl:template>
    
    
    <xsl:template match="cbml:EGF" mode="attributes">  
        <xsl:variable name="start" select="cup:pageStart(.)"/>
        <xsl:variable name="end" select="cup:pageEnd(., @id, descendant::*[not(ancestor-or-self::cbml:key) and @id][1]/@id)"/>
            <xsl:attribute name="filename" select="cup:cboFilename(., $start, $end)"/>
            <xsl:attribute name="page-start" select="$start"/>
            <xsl:attribute name="page-end" select="$end"/>
            <xsl:attribute name="doi" select="'y'"/>
            <xsl:attribute name="jpeg-page">
                <xsl:choose>
                    <xsl:when test="descendant::cbml:p[not(ancestor::cbml:caption)]">
                        <xsl:value-of select="descendant::cbml:p[not(ancestor::cbml:class or ancestor::cbml:caption)][1]/preceding::processing-instruction('new-page')[1]"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="$start"/></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
        <xsl:attribute name="content-type" select="'egf'"/>
        <xsl:attribute name="content-extn" select="'egf'"/>                     
        <xsl:attribute name="id" select="cbml:key[1]/@id"/>
    </xsl:template>
    
    <xsl:template match="cbml:class" mode="attributes">  
        <xsl:variable name="start" select="cup:pageStart(.)"/>
        <xsl:variable name="end" select="cup:pageEnd(., @id, descendant::*[not(ancestor-or-self::cbml:class)][@id][1]/@id)"/>

            <xsl:attribute name="filename" select="cup:cboFilename(., $start, $end)"/>
            <xsl:attribute name="page-start" select="$start"/>
            <xsl:attribute name="page-end" select="$end"/>
            <xsl:attribute name="doi" select="'y'"/>
            <xsl:attribute name="jpeg-page">
                <xsl:choose>
                    <xsl:when test="descendant::cbml:p[not(ancestor::cbml:caption)]">
                        <xsl:value-of select="descendant::cbml:p[not(ancestor::cbml:family or ancestor::cbml:caption)][1]/preceding::processing-instruction('new-page')[1]"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="$start"/></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
        
        <xsl:attribute name="content-type" select="'class'"/>
        <xsl:attribute name="content-extn" select="'cls'"/>                     
        <xsl:attribute name="id" select="@id"/>
    </xsl:template>
    
    <xsl:template match="*[not(self::cbml:play or self::cbml:part or self::cbml:EGF or self::cbml:class)]" mode="attributes">
        <xsl:variable name="name" select="local-name(.)"/>
        <xsl:variable name="start" select="cup:pageStart(.)"/>
        <xsl:variable name="end" select="cup:pageEnd(., @id, following::*[@id][1]/@id)"/>
        <!--<xsl:variable name="typename" select="cup:cboCatchIntros(.)"/>-->
        <xsl:variable name="typename" select="local-name(.)"/>
            <xsl:attribute name="filename" select="cup:cboFilename(., $start, $end)"/>
            <xsl:attribute name="id" select="@id"/>
            <xsl:attribute name="page-start" select="$start"/>          
            <xsl:attribute name="page-end" select="$end"/> 
        <xsl:attribute name="doi" select="$file-extensions//elem[@name=$typename]/@doi"/>
        <xsl:attribute name="content-type" select="$file-extensions//elem[@name=$typename]/@CBOname"/>
        <xsl:attribute name="content-extn" select="$file-extensions//elem[@name=$typename]/@CBOext"/>
            <xsl:attribute name="jpeg-page">
                <xsl:choose>
                    <xsl:when test="descendant::cbml:p[not(ancestor::cbml:caption)][not(child::cbml:fig)]">
                        <xsl:value-of select="descendant::cbml:p[not(ancestor::cbml:caption)][not(child::cbml:fig[contains(string-join(//processing-instruction('new-page')[string(.)=string($start)]/following::processing-instruction('new-page')[1]/preceding::processing-instruction('anchor'),':'),string(@id))])][1]/preceding::processing-instruction('new-page')[1]"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="$start"/></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="cbml:play" mode="attributes">
        <xsl:variable name="name" select="local-name(.)"/>
        <xsl:variable name="start" select="cup:pageStart(.)"/>
        <xsl:variable name="end" select="cup:pageEnd(., @id, descendant::cbml:act[count(descendant::cbml:act)]/following-sibling::*[@id][1]/@id)"/>
        <xsl:variable name="typename" select="'chapter'"/>
            <xsl:attribute name="filename" select="cup:cboFilename(., $start, $end)"/>
            <xsl:attribute name="id" select="@id"/>
            <xsl:attribute name="page-start" select="$start"/>          
            <xsl:attribute name="page-end" select="$end"/> 
            <xsl:attribute name="doi" select="'y'"/>
            <xsl:attribute name="content-type" select="document('file-extensions.xml',.)//elem[@name=$typename]/@CBOname"/>
            <xsl:attribute name="content-extn" select="document('file-extensions.xml',.)//elem[@name=$typename]/@CBOext"/>
            <xsl:attribute name="jpeg-page" select="descendant::cbml:act[1]/preceding::processing-instruction('new-page')[1]"/>
    </xsl:template>
    
    
    <xsl:template match="cbml:plates" mode="attributes">
            <xsl:attribute name="filename" select="concat($isbn, 'psl_CBO.pdf')"/>
            <xsl:attribute name="id" select="@id"/>
            <xsl:attribute name="doi" select="'y'"/>
            <xsl:attribute name="content-type" select="'plate'"/>
            <xsl:attribute name="content-extn" select="'psl'"/>
    </xsl:template>

</xsl:stylesheet>