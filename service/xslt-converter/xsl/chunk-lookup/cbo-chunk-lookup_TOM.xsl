<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0" xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup cbml  xs xd">


    <xsl:import href="import/chunk-lookup-import.xsl"/>

    <xsl:output method="xml" indent="yes"/>
    <!--   <xsl:import href="functions/cbo-functions.xsl"/>-->
    <xsl:param name="isbn"/>
    <xsl:param name="variant-chunking" as="xs:string" required="no" select="'none'"/>
    <xsl:variable name="header-filename" select="concat($isbn, '.xml')"/>
    <xsl:variable name="header-file" select="document($header-filename, .)"/>
    <xsl:key name="html-id" match="html" use="@id"/>
    <xsl:key name="elem-id" match="*" use="@id"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="cbml:book">
        <chunks>
            <xsl:apply-templates mode="cbo-chunks"/>
        </chunks>
    </xsl:template>

    <xsl:template match="*" mode="cbo-chunks">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="html" select="$header-file//html[@id=$id]"/>
        <xsl:choose>
            <xsl:when test="self::cbml:prelims">
                <chunk filename="{$header-file//html[@id='frontmatter']/@filename}" id="frontmatter"
                    start="{$header-file//html[@id='frontmatter']/parent::content-item/@page-start}"
                    end="{$header-file//html[@id='frontmatter']/parent::content-item/@page-end}">
                    <xsl:apply-templates select="*[not(preceding::cbml:toc or self::cbml:toc)]"
                        mode="ids"/>
                </chunk>
                <xsl:apply-templates select="*[preceding::cbml:toc or self::cbml:toc]"
                    mode="cbo-chunks"/>
                <!--<xsl:apply-templates select="*" mode="check-child-chunk"/>-->
            </xsl:when>
            <xsl:when test="$header-file//html[@id=$id]">
                <xsl:message select="concat('[INFO] $variant-chunking is: ',$variant-chunking)"/>
                <!--report var for testing-->
                <xsl:choose>
                    <xsl:when test="$variant-chunking eq 'sec-first-attached' and self::cbml:chapter">
                        <xsl:variable name="html.trimmed" select="cup:stripDescendants(.)" as="node()+"/>
                        <chunk filename="{replace($html/@filename,'^(.*_)(p.*)(_CBO\.html)$','$1sec1$3')}" id="{$id}" 
                            start="{$html/parent::content-item/@page-start}" 
                            end="{data($html.trimmed/descendant::processing-instruction('new-page')[position() eq last()])}">
                            <xsl:apply-templates select="$html.trimmed" mode="pages"/>
                            <xsl:apply-templates select="$html.trimmed" mode="ids"/>
                        </chunk>
                        <xsl:for-each select="./descendant::cbml:sec[preceding-sibling::cbml:sec]">
                            <chunk filename="{replace($html/@filename,'^(.*_)(p.*)(_CBO\.html)$',concat('$1sec',./count(preceding-sibling::cbml:sec) +1,'$3'))}" 
                                id="{$id}" 
                                start="{data($html.trimmed/descendant::processing-instruction('new-page')[1])}" 
                                end="{data($html.trimmed/descendant::processing-instruction('new-page')[position() eq last()])}">
                                <xsl:apply-templates select="." mode="pages"/>
                                <xsl:apply-templates select="." mode="ids"/>
                            </chunk>
                        </xsl:for-each>
                        <chunk filename="{replace($html/@filename,'^(.*_)(p.*)(_CBO\.html)$','$1back$3')}" 
                            id="{$id}" 
                            start="{data(./descendant::cbml:back/descendant::processing-instruction('new-page')[1])}" 
                            end="{data(./descendant::cbml:back/descendant::processing-instruction('new-page')[position() eq last()])}">
                            <xsl:apply-templates select="./descendant::cbml:back" mode="pages"/>
                            <xsl:apply-templates select="./descendant::cbml:back" mode="ids"/>
                        </chunk>
                    </xsl:when>
                    <xsl:when test="$variant-chunking eq 'sec-first-distinct' and self::cbml:chapter">
                        <xsl:variable name="html.trimmed" select="cup:stripDescendants(.)" as="node()+"/>
                        <chunk filename="{$html/@filename}" id="{$id}" 
                            start="{$html/parent::content-item/@page-start}" 
                            end="{data($html.trimmed/descendant::processing-instruction('new-page')[position() eq last()])}">
                            <xsl:apply-templates select="$html.trimmed" mode="pages"/>
                            <xsl:apply-templates select="$html.trimmed" mode="ids"/>
                        </chunk>
                        <xsl:for-each select="./descendant::cbml:sec">
                            <chunk filename="{replace($html/@filename,'^(.*_)(p.*)(_CBO\.html)$',concat('$1sec',./count(preceding-sibling::cbml:sec) +1,'$3'))}" 
                                id="{$id}" 
                                start="{data($html.trimmed/descendant::processing-instruction('new-page')[1])}" 
                                end="{data($html.trimmed/descendant::processing-instruction('new-page')[position() eq last()])}">
                                <xsl:apply-templates select="." mode="pages"/>
                                <xsl:apply-templates select="." mode="ids"/>
                            </chunk>
                        </xsl:for-each>
                        <chunk filename="{replace($html/@filename,'^(.*_)(p.*)(_CBO\.html)$','$1back$3')}" 
                            id="{$id}" 
                            start="{data(./descendant::cbml:back/descendant::processing-instruction('new-page')[1])}" 
                            end="{data(./descendant::cbml:back/descendant::processing-instruction('new-page')[position() eq last()])}">
                            <xsl:apply-templates select="./descendant::cbml:back" mode="pages"/>
                            <xsl:apply-templates select="./descendant::cbml:back" mode="ids"/>
                        </chunk>
                    </xsl:when>
                    <xsl:otherwise>
                        <chunk filename="{$html/@filename}" 
                            id="{$id}" 
                            start="{$html/parent::content-item/@page-start}" 
                            end="{$html/parent::content-item/@page-end}">
                            <xsl:apply-templates select="." mode="pages"/>
                            <xsl:apply-templates select="." mode="ids"/>
                        </chunk>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:apply-templates select="*" mode="check-child-chunk"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*" mode="cbo-chunks"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*" mode="check-child-chunk">
        <xsl:variable name="id" select="@id"/>
        <xsl:choose>
            <xsl:when test="self::cbml:body or self::cbml:back">
                <xsl:apply-templates mode="cbo-chunks"/>
            </xsl:when>
            <xsl:when test="$header-file//html[@id=$id]!=''">
                <xsl:apply-templates select="." mode="cbo-chunks"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()"/>

    <!--    <xsl:template match="*">
        <xsl:if test="@id or @href">
        <xsl:value-of select="local-name()"/>
        </xsl:if>
        <xsl:apply-templates select="*"/>
    </xsl:template>-->

    <!--  ============== MODE PAGES ================  -->

    <xsl:template match="cbml:part" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates
            select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:chapter])]"/>
        <xsl:if
            test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="cbml:EGF" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates
            select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:class])]"/>
        <xsl:if
            test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>


    <xsl:template match="cbml:class" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates
            select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:family])]"/>
        <xsl:if
            test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="cbml:play" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates
            select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:act])]"/>
        <xsl:if
            test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>

    <!--  ================= MODE IDS =================  -->

    <xsl:template match="*" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>

    <xsl:template match="cbml:dictionary-entry[not(preceding::cbml:dictionary-entry)]" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="ancestor::cbml:dictionary" mode="ids"/>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>

    <xsl:template match="cbml:chapter" mode="ids">
        <xsl:variable name="id" select="@id"/>
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="*[not(@position='float')] | processing-instruction('anchor')"
            mode="ids"/>
    </xsl:template>

    <xsl:template match="cbml:body| cbml:back" mode="ids">
        <xsl:variable name="id" select="parent::*/@id"/>
        <!--        if sections are to be html don't do the ids at chapter level 2DO -make this work for chapter back items other than ref-sec-->
        <xsl:choose>
            <xsl:when test="$header-file//*[@id=$id and following-sibling::toc/descendant::html]">
                <xsl:apply-templates select="*[not(self::cbml:sec or self::cbml:ref-sec)]"
                    mode="ids"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*" mode="ids"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="cbml:part|cbml:sub-book |cbml:dictionary |cbml:play | cbml:class"
        mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>


    <xsl:template match="cbml:chapter//cbml:sub-book//cbml:part | cbml:chapter//cbml:sub-book"
        mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>

    <xsl:template match="cbml:EGF" mode="ids">
        <xsl:element name="{local-name(.)}">
            <xsl:attribute name="id" select="cbml:key[1]/@id"/>
        </xsl:element>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>

    <xsl:template match="processing-instruction('anchor')" mode="ids"/>

    <xsl:template match="processing-instruction('new-page')">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="text()|processing-instruction()" mode="ids"/>

    <xsl:function name="cup:stripDescendants" as="node()+">
        <!--wrapper function for modified identity template, provided for readability when calling-->
        <xsl:param name="context" as="element()"/>
        <xsl:apply-templates select="$context" mode="trim-for-chunks"/>        
    </xsl:function>
    
    <xsl:template match="@*|node()" mode="trim-for-chunks">
        <!--return chapter node with specified descendants removed for alternate chunking requirements-->
        <!--if new chunking requirements emerge, new value for variant-chunking param must be included here or transformation will fail-->
        <!--can be adjusted to strip or transform named attributes as well-->
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:choose>
                <xsl:when test="$variant-chunking eq 'sec-first-attached'"><!--first subchunk includes first sec-->
                    <xsl:apply-templates select="node()[not(self::cbml:sec[preceding-sibling::cbml:sec])][not(self::cbml:back)]" mode="#current"/>
                </xsl:when>
                <xsl:when test="$variant-chunking eq 'sec-first-distinct'"><!--first subchunk does not include first sec-->
                    <xsl:apply-templates select="node()[not(self::cbml:sec)][not(self::cbml:back)]" mode="#current"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message select="'[FAIL] trim-for-chunks mode template called erroneously.'" terminate="yes"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
