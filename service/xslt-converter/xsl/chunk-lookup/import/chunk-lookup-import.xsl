<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xs m"
    version="2.0">
    
    
    <!--  chunk lookup for all builds
        this file specifies what to do for each chunk
        file importing this file specifies what the chunks are
    -->

    <!--<xsl:import href="../functions.xsl"/>-->
    
    <!--  ============MODE CHUNKS ================  -->
<!--
    <xsl:template match="*" mode="chunks">
        <chunk>
            <xsl:apply-templates select="." mode="attributes"/>
            <xsl:apply-templates select="." mode="ids"/>
            <xsl:apply-templates select="." mode="all-pages"/>
        </chunk>
    </xsl:template>-->
    
    <!--  ============== MODE PAGES ================  -->
    
    <xsl:template match="*" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
 
 
    <xsl:template match="*" mode="all-pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="descendant::processing-instruction('new-page')"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="*" mode="desc-pages">
        <xsl:apply-templates select="descendant::processing-instruction('new-page')"/>
    </xsl:template>
    
    <!--  ================= MODE IDS =================  -->

    <xsl:template match="*" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="*[not(@position='float')] | processing-instruction('anchor')" mode="ids"/>
    </xsl:template>
    
    
</xsl:stylesheet>
