<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0"
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:cup="http://contentservices.cambridge.org"
    exclude-result-prefixes="cup cbml  xs xd">
     

   <xsl:import href="import/chunk-lookup-import.xsl"/>
    
    <xsl:output method="xml" indent="yes"/>
  <!--   <xsl:import href="functions/cbo-functions.xsl"/>-->
    <xsl:param name="isbn"/>

    <xsl:variable name="header-filename" select="concat($isbn, '.xml')"/>
    <xsl:variable name="header-file" select="document($header-filename, .)"/>
    <xsl:param name="chunk-type">
        <xsl:choose>
            <xsl:when test="$header-file/descendant::html[contains(@filename, 'sec1_')]">sec</xsl:when>
            <xsl:otherwise>med-sec</xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:key name="html-id" match="html" use="@rid"/>
    <xsl:key name="elem-id" match="*" use="@rid"/>
 
<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>
    
    <xsl:template match="cbml:book">
        <chunks>
        <xsl:apply-templates mode="cbo-chunks"/>
        </chunks>
    </xsl:template>
    
    <xsl:template match="*" mode="cbo-chunks">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="html" select="$header-file//html[@rid=$id] | $header-file//nav-file[@rid=$id] "/>
        <xsl:choose>
            <xsl:when test="self::cbml:prelims">
                <chunk filename="{$header-file//html[@rid='frontmatter']/@filename}" id="frontmatter"
                    start="{$header-file//html[@rid='frontmatter']/parent::content-item/@page-start}" end="{$header-file//html[@rid='frontmatter']/parent::content-item/@page-end}">
                    <xsl:apply-templates select="*[not(preceding::cbml:toc or self::cbml:toc)]" mode="ids"/>
                </chunk>
                <xsl:apply-templates select="*[preceding::cbml:toc or self::cbml:toc]" mode="cbo-chunks"/>
                <!--<xsl:apply-templates select="*" mode="check-child-chunk"/>-->
            </xsl:when>
            <xsl:when test="$header-file//html[@rid=$id and not(following-sibling::nav-file)]">
                <chunk filename="{$html/@filename}" id="{$id}"
                    start="{$html/parent::content-item/@page-start}" end="{$html/parent::content-item/@page-end}">
                    <xsl:apply-templates select="." mode="pages"/>
                    <xsl:apply-templates select="." mode="ids"/>
                </chunk>
                <xsl:apply-templates select="*" mode="check-child-chunk"/>
            </xsl:when>
            <xsl:when test="$header-file//html[@rid=$id and following-sibling::nav-file]">
                <chunk filename="{$html/@filename}" id="{$id}"
                    start="{$html/parent::content-item/@page-start}" end="{$html/parent::content-item/@page-end}">
                    <xsl:apply-templates select="." mode="pages"/>
                    <xsl:apply-templates select="." mode="ids"/>
                </chunk>
                <xsl:apply-templates select="*" mode="cbo-chunks"/>
            </xsl:when>
            <xsl:when test="$header-file//nav-file[@rid=$id]">
                <chunk filename="{$html/@filename}" id="{$id}"
                    start="{$html/parent::content-item/@page-start}" end="{$html/parent::content-item/@page-end}">
                    <xsl:apply-templates select="." mode="pages"/>
                    <xsl:apply-templates select="." mode="ids"/>
                </chunk>
                <xsl:apply-templates select="*" mode="check-child-chunk"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*" mode="cbo-chunks"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="*" mode="check-child-chunk">
        <xsl:variable name="id" select="@id"/>
        <xsl:choose>
            <xsl:when test="self::cbml:body or self::cbml:back">
                <xsl:apply-templates mode="cbo-chunks"/>
            </xsl:when>
            <xsl:when test="$header-file//html[@rid=$id]">
                <xsl:apply-templates select="." mode="cbo-chunks"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="text()"/>
    
<!--    <xsl:template match="*">
        <xsl:if test="@id or @href">
        <xsl:value-of select="local-name()"/>
        </xsl:if>
        <xsl:apply-templates select="*"/>
    </xsl:template>-->
	
    <!--  ============== MODE PAGES ================  -->
    
    <xsl:template match="cbml:part" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:chapter])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:EGF" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:class])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="cbml:class" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:family])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:play" mode="pages">
        <xsl:variable name="prec-id" select="preceding-sibling::*[1]/@id"/>
        <xsl:apply-templates select="preceding-sibling::processing-instruction('new-page')[1]"/>
        <xsl:apply-templates select="processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:act])]"/>
        <xsl:if test="following::node()[1][self::processing-instruction('new-page')] and following::node()[2][self::processing-instruction('new-page')]">
            <xsl:apply-templates select="following::node()[1][self::processing-instruction()]"/>
        </xsl:if>
    </xsl:template>
  
    <!--  ================= MODE IDS =================  -->
    
    <xsl:template match="*" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:index" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:dictionary-entry[not(preceding::cbml:dictionary-entry)]" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="ancestor::cbml:dictionary" mode="ids"/>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:chapter" mode="ids">
        <xsl:variable name="id" select="@id"/>
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
         <xsl:apply-templates select="*[not(@position='float')] | processing-instruction('anchor')" mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:body| cbml:back" mode="ids">
        <xsl:variable name="id" select="parent::*/@id"/>
<!--        if sections are to be html don't do the ids at chapter level 2DO -make this work for chapter back items other than ref-sec-->
        <xsl:choose>
            <xsl:when test="$header-file//*[@rid=$id  and $chunk-type='med-sec'] and not(parent::cbml:appendix)"><!--appendix to be single html file-->
                <xsl:apply-templates select="*[not(self::cbml:sec[preceding-sibling::cbml:sec] or self::cbml:ref-sec)]" mode="ids"/>
            </xsl:when>
            <xsl:when test="$header-file//*[@rid=$id and $chunk-type='sec'] and not(parent::cbml:appendix)">
                <xsl:apply-templates select="*[not(self::cbml:sec or self::cbml:ref-sec)]" mode="ids"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*" mode="ids"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="cbml:part|cbml:sub-book |cbml:dictionary |cbml:play | cbml:class" mode="ids">
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="cbml:chapter//cbml:sub-book//cbml:part | cbml:chapter//cbml:sub-book" mode="ids"> 
        <xsl:if test="@id">
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="id" select="@id"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="@*[local-name()='href']">
            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
            <xsl:element name="{local-name(.)}">
                <xsl:attribute name="href" select="$link-att"/>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="cbml:EGF" mode="ids">
        <xsl:element name="{local-name(.)}">
            <xsl:attribute name="id" select="cbml:key[1]/@id"/>
        </xsl:element>
        <xsl:apply-templates mode="ids"/>
    </xsl:template>
    
    <xsl:template match="processing-instruction('anchor')" mode="ids"/>
    
    <xsl:template match="processing-instruction('new-page')">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="text()|processing-instruction()" mode="ids"/>
    
    
</xsl:stylesheet>