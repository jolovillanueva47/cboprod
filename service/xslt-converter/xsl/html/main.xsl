<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml xlink xs m"
    version="2.0">
    
<!--  import the defaults  -->
    <xsl:import href="include/import/headings.xsl"/>

    <!-- include everything else - these all override the defaults-->
    <xsl:include href="include/common.xsl"/>
    <xsl:include href="include/epub_toc.xsl"/>
    <xsl:include href="include/prelims.xsl"/>
    <xsl:include href="include/blocks.xsl"/>
    <xsl:include href="include/endmatter.xsl"/>
    <xsl:include href="include/epub-index.xsl"/>
    <xsl:include href="include/cbml2cals.xsl"/>
    <xsl:include href="include/play.xsl"/>
    <xsl:include href="include/case-report.xsl"/>
    <xsl:include href="include/pharma.xsl"/>
    <xsl:include href="include/egf.xsl"/>
    <!--  include the functions  -->
    <xsl:include href="../functions/functions.xsl"/> 
    
    <!--  anthing to override this lot to go in cbml2epub.xsl  -->
    
<!--    <xsl:output method="xhtml" indent="yes" encoding="UTF-8" 
    doctype-public="-//W3C//DTD XHTML 1.1//EN" 
        doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>-->

    <xsl:output indent="yes"/>

 
<!--    <xsl:template match="/">
        <html  xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <link href="cbml.css" rel="stylesheet" type="text/css"></link> 
                <title><xsl:value-of select="cbml:book/cbml:prelims/cbml:title-info/cbml:title[1]"/></title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>-->
    
    <xsl:template match="cbml:book">
        <xsl:apply-templates/> 
    </xsl:template>
    
    <xsl:template match="cbml:body |cbml:back">
        <xsl:apply-templates/>
    </xsl:template>
    
</xsl:stylesheet>
