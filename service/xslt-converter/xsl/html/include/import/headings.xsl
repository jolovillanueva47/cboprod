<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all"
    version="2.0">


    <xsl:import href="default.xsl"/>

    <!--  ======== DEFAULT HEADING TEMPLATE -does titles and labels ===========  -->

    <!--   enclose label and title in a div -->
    <xsl:template match="cbml:sec | cbml:ref-sec" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:label">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:icon" mode="heading"/>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/>
                    <xsl:apply-templates select="cbml:alt-title[not(@type='copyright-line')]//cbml:target"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  subtitle to be line below title  -->
<!--    chapter title to be h2; subtitle to be h3-->
    <xsl:template match="cbml:chapter | cbml:dictionary | cbml:document | cbml:document-group |cbml:appendix| cbml:case-report-group |  cbml:references" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:label">
                <h2>
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:icon" mode="heading"/>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                    <xsl:apply-templates select="cbml:alt-title[not(@type='copyright-line')]//cbml:target"/>
                </h2>
                <!--<xsl:if test="cbml:subtitle">
                    <xsl:element name="div">
                        <xsl:attribute name="class" select="concat(cup:HeadingClass(.), '_subtitle')"/>-->
                        <xsl:apply-templates select="cbml:subtitle" mode="main-heading"/>
                    <!--</xsl:element>
                </xsl:if>-->
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  subtitle to be line below title  -->
    <xsl:template match="cbml:part" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:label">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:icon" mode="heading"/>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                </xsl:element>
                <xsl:if test="cbml:subtitle">
                <xsl:element name="div">
                    <xsl:attribute name="class" select="concat(cup:HeadingClass(.), '_subtitle')"/>
                    <xsl:apply-templates select="cbml:subtitle" mode="main-heading"/>
                </xsl:element>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <!--       if part has no heading enclose id  in a         -->
                <xsl:if test="@id">
                    <a>
                        <xsl:attribute name="id" select="@id"/>
                    </a>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*" mode="heading">
        <xsl:choose>
            <xsl:when test="count(cbml:label)&gt; 1">
                <xsl:for-each select="cbml:label">
                    <div class="label">
                        <xsl:apply-templates/>
                    </div>
                </xsl:for-each>
                <xsl:if test="cbml:title">
                    <xsl:element name="{cup:HeadingType(.)}">
                        <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                        <xsl:if test="@id">
                            <xsl:attribute name="id" select="@id"/>
                        </xsl:if>
                        <xsl:apply-templates select="cbml:title" mode="heading"/>
                        <xsl:apply-templates select="cbml:subtitle" mode="heading"/>
                    </xsl:element>
                </xsl:if>
            </xsl:when>
            <xsl:when
                test="cbml:title or cbml:label[not(following-sibling::*[1][self::cbml:p[not(*[contains($block-elements, local-name(.)) and not(following-sibling::node() or preceding-sibling::node())])]])] or cbml:icon">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:icon" mode="heading"/>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
       

    <xsl:template match="cbml:disp-quote | cbml:exercise | cbml:boxed-text" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:label">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--  does label in heading  -->

    <xsl:template match="cbml:label" mode="heading">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- does label when no title   -->
    <xsl:template match="cbml:label|cbml:number" mode="do-label">
        <span class="label">
            <xsl:apply-templates/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>

    <!--does title in heading-->
    <xsl:template match="cbml:title |cbml:subtitle" mode="heading">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <!--    <xsl:template match="cbml:half-title/cbml:subtitle" mode="heading">
        <span> 
            <xsl:apply-templates select="." mode="class"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>-->

    <!-- avoid repeat of heading elements -->

    <xsl:template match="cbml:title | cbml:label[not(parent::cbml:fig and not(following-sibling::cbml:caption))] |cbml:subtitle"/>
    <xsl:template match="cbml:title | cbml:label[not(parent::cbml:fig and not(following-sibling::cbml:caption))] |cbml:subtitle" mode="toc"/>

    <!--===============mode heading templates ==================-->
    <!--    <xsl:template match="cbml:disp-quote/cbml:title" mode="heading">
        <div class="disp-quote_title">
            <xsl:apply-templates/>
        </div>
    </xsl:template>-->

    <!--  captions include preceding label  -->
    <xsl:template match="cbml:caption[cbml:title]" mode="heading">
        <div class="caption">
            <xsl:apply-templates select="preceding-sibling::cbml:label" mode="do-label"/>
            <xsl:apply-templates select="cbml:title" mode="heading"/>
        </div>
    </xsl:template>

    <!--subtitles on new line-->
        <xsl:template match="cbml:title-info/cbml:title | cbml:half-title/cbml:title |
        cbml:title-page/cbml:title " mode="heading">
        <div class="title">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="cbml:title-info/cbml:subtitle | cbml:half-title/cbml:subtitle |
        cbml:part/cbml:subtitle| cbml:title-page/cbml:subtitle " mode="heading">
        <div class="subtitle">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:example|cbml:question" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:subtitle or cbml:label[not(following-sibling::*[1][self::cbml:p or self::cbml:instruction] or following-sibling::cbml:caption)]">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
