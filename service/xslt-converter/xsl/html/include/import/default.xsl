<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    

    
    <!-- PROCESSING INSTRUCTIONS -->
    <xsl:template match="processing-instruction('new-page')">
        <a>
            <xsl:attribute name="id" select="concat('page_' , . )"/>
        </a>
    </xsl:template> 

<!-- =========== DEFAULT ELEMENT TEMPLATE - assigns div or span ============-->

    <xsl:template match="*[@*[local-name()='href']]">
           <span>
            <xsl:apply-templates select="." mode="class"/>
                    <xsl:variable name="link-att" select="@*[local-name()='href']" as="xs:string"/>
                    <a>
                        <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/> 
                        <xsl:apply-templates/>  
                    </a>
               </span>
    </xsl:template>
    
    
    <xsl:template match="*[not(@*[local-name()='href'])]">
        <xsl:element name="{cup:SpanDiv(.)}">
            <!--    id on the elem if no title        -->
            <xsl:if test="(not(*[local-name()=$title-elem or local-name()=$label-elem]))
                or
                (*[local-name()=$label-elem] and local-name()=$inline-labels) and not(*[local-name()=$title-elem])">
                <xsl:apply-templates select="." mode="id"/>
            </xsl:if>
            <xsl:apply-templates select="." mode="class"/>
            
                    <xsl:choose>
                        <xsl:when test="local-name()=$inline-labels and not(*[local-name()=$title-elem])">
                            <xsl:apply-templates select="*[local-name()=$label-elem and not(following-sibling::*[1][local-name()=$para-elem])]" mode="do-label"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="." mode="heading"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!--          do floating items in situ         -->
                    <xsl:apply-templates select="text() | processing-instruction() | *[not(@position='float')]"/>
        </xsl:element>
        <xsl:apply-templates select="*[@position='float']"/>
        <xsl:apply-templates select="@answer-link"/>
    </xsl:template>


<!-- ============== DEFAULT CLASS TEMPLATE - name of element assigned to class ===========-->
    <!-- add attribute class -->
    <!-- epub specific - classes for p - overrides this-->
    <xsl:template match="*" mode="class">
        <xsl:attribute name="class"><xsl:value-of select="name(.)"/></xsl:attribute>
    </xsl:template>
    
    
    <!-- ============== DEFAULT id TEMPLATE  ===========-->
    <!-- add attribute id -->
    <xsl:template match="*" mode="id">
        <xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
    </xsl:template>

</xsl:stylesheet>
