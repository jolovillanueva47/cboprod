<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs"
    version="2.0">
    
<!--
    toc
    toc-external
    -->
    
<!-- ========= MAIN TOC ============   -->
    
    <xsl:template match="cbml:toc | cbml:toc-feature | cbml:chapter-toc | cbml:part-toc | cbml:features">
        <!--<xsl:if test="//cbml:unicode-altfont and self::cbml:toc[not(preceding::cbml:toc)]"><div class="toc-sim-p">Please note: this book contains embedded fonts; you may need to adjust settings in your reading device or software to enable the correct display of certain characters.</div></xsl:if>-->
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates select="node()[not(self::cbml:fn-group)]" mode="toc"/>
        <xsl:apply-templates select="cbml:fn-group"/>
    </xsl:template>
    
    <xsl:template match="cbml:toc-feature" mode="toc">
        <xsl:apply-templates select="." mode="heading"/>        
        <xsl:apply-templates mode="toc"/>
    </xsl:template>
    
    <xsl:template match="cbml:p | cbml:fig" mode="toc">
        <xsl:apply-templates select="."/>
    </xsl:template>
    
    <xsl:template match="cbml:toc-feature | cbml:chapter-toc | cbml:part-toc | cbml:external-toc" mode="heading">
            <xsl:if test="cbml:title or cbml:label">
                <div>
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label"><xsl:text> </xsl:text></xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>  
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/> 
                </div>
            </xsl:if>    
    </xsl:template>
    
    <xsl:template match="cbml:toc-body" mode="toc">
            <xsl:apply-templates mode="toc"/>
    </xsl:template>
    
<!--  spacing before or after these required  -->
    <xsl:template match="cbml:toc-prelims | cbml:toc-endmatter" mode="toc">
        <div class="{local-name()}">
            <xsl:apply-templates mode="toc"/>
        </div>
    </xsl:template>
    
    <xsl:template match="processing-instruction()" mode="toc">
        <xsl:apply-templates select="."/>
    </xsl:template>
    
<!--    <xsl:template match="cbml:features">
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates select="cbml:p | cbml:list | cbml:def-list | cbml:index-list |cbml:toc-feature"/>
    </xsl:template>-->
    
    <!--  uses fixed nonbreak spaces for indents   -->
    <xsl:template match="cbml:toc-item | cbml:toc-feature-item | cbml:toc-part |cbml:toc-subchapter | 
        cbml:toc-chapter | cbml:toc-dict | 
        cbml:toc-sec | cbml:toc-group | cbml:toc-appendix |cbml:toc-sub-book | cbml:toc-volume |cbml:toc-patient-file | cbml:toc-plate |cbml:toc-doc |cbml:toc-doc-group" mode="toc"> 
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <div>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="self::cbml:toc-sec">
                        <xsl:choose>
                            <xsl:when test="cbml:label and cbml:toc-entry">
                                <xsl:value-of select="concat(name(.),'_',name(parent::*),'_label')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="concat(name(.),'_',name(parent::*))"/>       
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="cbml:label and cbml:toc-entry">
                                <xsl:value-of select="concat(name(.),'_label')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="name(.)"/>       
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose> 
            </xsl:attribute>
            <xsl:if test="self::cbml:toc-sec or self::cbml:toc-subchapter">&#160;&#160;&#160;</xsl:if>
            <xsl:if test="parent::cbml:toc-sec or parent::cbml:toc-subchapter or parent::cbml:toc-feature-item or parent::cbml:toc-item or parent::cbml:toc-group">&#160;&#160;&#160;</xsl:if>
            <xsl:if test="parent::cbml:toc-sec[parent::cbml:toc-sec]">&#160;&#160;&#160;</xsl:if>
            <xsl:if test="parent::cbml:toc-sec[parent::cbml:toc-sec[parent::cbml:toc-sec]]">&#160;&#160;&#160;</xsl:if>
            <xsl:choose>
                <xsl:when test="$link-att">
            <!--          separate link for xref in label          -->
                    <xsl:if test="cbml:label/cbml:xref">
                        <xsl:apply-templates select="cbml:label/cbml:xref"/>
                    </xsl:if>
                    <xsl:text>&#x00A0;&#x00A0;</xsl:text>
                    <a class="nounder">
                        <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/>
<!--     cannot have a within a in html               -->                        
                        <!--<xsl:apply-templates select="cbml:label | cbml:toc-entry[not(cbml:xref)]" mode="toc"/>-->
                        <xsl:apply-templates select="cbml:label | cbml:toc-entry" mode="toc"/>
                    </a>
                    <xsl:apply-templates select="cbml:toc-authors | cbml:sim-p |cbml:attrib" mode="toc"/>
                    <xsl:apply-templates select="cbml:toc-entry/processing-instruction()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="cbml:label | cbml:toc-entry | cbml:toc-authors | cbml:sim-p |cbml:attrib" mode="toc"/>
                    <xsl:apply-templates select="cbml:toc-entry/processing-instruction()"/>
                </xsl:otherwise>
            </xsl:choose>
        </div>  
        <!-- do nested entries -->
        <xsl:apply-templates select="cbml:toc-part |cbml:toc-chapter | cbml:toc-item |cbml:toc-prelims/cbml:toc-item|cbml:toc-endmatter/cbml:toc-item|cbml:toc-feature-item| cbml:toc-doc |
                                    cbml:toc-subchapter |cbml:toc-external| cbml:toc-sec | cbml:toc-body |cbml:toc-appendix|processing-instruction()" mode="toc"/>
    </xsl:template>
    
    <xsl:template match="cbml:toc-entry/cbml:xref">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:toc-entry" mode="toc">
        <span class="toc_content"><xsl:apply-templates select="text() |*"/></span>
        <xsl:if test="following-sibling::cbml:toc-entry"><br/></xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:label" mode="toc">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="node()[not(self::cbml:xref)]"/>
        </span>
        <xsl:text> </xsl:text>
    </xsl:template>
    
    <xsl:template match="cbml:toc-authors" mode="toc">
        <!-- bold, newline for authors -->
        <div class="toc-authors"><xsl:apply-templates/></div>
    </xsl:template>
    
    <xsl:template match="cbml:sim-p" mode="toc">
        <div class="toc-sim-p">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!--  ========= TOC-EXTERNAL ===========  -->
    
    <xsl:template match="cbml:external-toc" mode="toc">
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates select="cbml:p" mode="toc"/>
        <div class="external-toc" id="{@id}">
            <xsl:apply-templates select="cbml:toc-external"/>
        </div>
    </xsl:template>
<!--    
    <xsl:template match="cbml:toc-external">
        <xsl:variable name="level" select="count(parent::cbml:toc-external) + 1"/>
        <table width="100%">
           <tr>
               <td>
                   <div class="toc-external_{@type}{string($level)}">
                       <xsl:apply-templates select="node()[not(self::cbml:toc-external)]" mode="toc"/>
                   </div>
               </td>
               <td style="text-align:right; width:5%;"><xsl:value-of select="@pagenum"/></td>
           </tr>
       </table> 
        <xsl:apply-templates select="cbml:toc-external"/>
    </xsl:template>-->

    
    <xsl:template match="cbml:toc-external" mode="#default toc">
        <xsl:variable name="level" select="count(ancestor::cbml:toc-external) + 1"/>
        <div class="toc-external_{@type}{$level}">
            <xsl:choose>
                <xsl:when test="@uri">
                    <a href="{@uri}">
                        <xsl:apply-templates select="cbml:label | cbml:toc-entry | cbml:toc-authors" mode="toc"/> 
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="cbml:label | cbml:toc-entry | cbml:toc-authors" mode="toc"/>
                </xsl:otherwise>
            </xsl:choose>
        </div>
        <xsl:apply-templates select="cbml:toc-external" mode="toc"/>
    </xsl:template>
    
    <xsl:template match="cbml:toc/cbml:fn-group/cbml:fn">
        <div class="fn">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
</xsl:stylesheet>
