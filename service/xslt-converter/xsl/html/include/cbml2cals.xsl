<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:inuse="http://dtd.cambridge.org/2009/InUse"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
  
  
    
<!--<xsl:template match="cbml:table">
    <xsl:variable name="image" select="concat(@alt-graphic, '.png')"/>
    <xsl:variable name="image-in-prd" select="$product-file//item[matches(@name, concat('^',@alt-graphic,'\.png$'))]" as="node()*"/>
    <xsl:choose>
<!-\-    old spec for product version number less than 5    -\->
        <xsl:when test="((count(descendant::cbml:colspec) &gt; 6 
                            or (@orient='land' and count(descendant::cbml:colspec) &gt; 3)) and string-length(@alt-graphic) gt 0)
                            and number($product-version) &lt; 5">
            <xsl:apply-templates select="." mode="image"/>
        </xsl:when>
<!-\-        new spec - image only when specified in prd file-\->
        <xsl:when test="$image-in-prd/@function='epub'">
            <xsl:apply-templates select="." mode="image"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>-->
    
    <xsl:template match="cbml:table" mode="image">
        <xsl:variable name="imgs" select="tokenize(@alt-graphic, ' ')"/>
        <xsl:apply-templates select="descendant::cbml:target"/>
        <xsl:apply-templates select="descendant::cbml:xref" mode="do-backlink"/>
        <xsl:for-each select="$imgs">
            <img src="{cup:imageFilename(.)}" alt=""/>
        </xsl:for-each>
    <xsl:apply-templates select="descendant::processing-instruction('new-page')"/>
    </xsl:template>
    
     <xsl:template match="@frame">
        <!--<xsl:attribute name="frame">
            <xsl:value-of select="cup:Frame(.)"/>
        </xsl:attribute>-->
     </xsl:template> 
    
    <xsl:template match="cbml:tgroup">
        <table>
            <xsl:attribute name="class" select="if (ancestor::cbml:linguistics) then 'linguistics' else (if (ancestor::cbml:event) then 'event-tabular' else 'Tbl')"></xsl:attribute>
               <xsl:apply-templates select="parent::cbml:table/@frame"/>
               <xsl:apply-templates/>
         </table>
        
    </xsl:template>
    
<!--    <xsl:template match="cbml:colspec">
        <col>
            <xsl:if test="@colwidth">
                <xsl:attribute name="width" select="@colwidth"/>
            </xsl:if>
          <xsl:copy-of select="@align |@char|@charoff"/>  
        </col>
    </xsl:template>-->
    <xsl:template match="cbml:colspec">
        <xsl:if test="@colwidth">
        <col>
            <xsl:apply-templates select="@colwidth"/>
        </col>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="cbml:colspec/@colwidth">
        <xsl:attribute name="width" select="concat(.,'relative_length')"/>
    </xsl:template>
    
    
<!--<xsl:template match="cbml:thead">
    <thead>
        <xsl:copy-of select="@valign"/>
        <xsl:copy-of select="parent::cbml:tgroup/@align"/>
        <xsl:apply-templates/>
    </thead>
</xsl:template>-->
    
    <xsl:template match="cbml:thead">
        <thead>
            <!--<xsl:copy-of select="@valign"/>-->
            <!--<xsl:copy-of select="parent::cbml:tgroup/@align"/>-->
            <xsl:choose>
                <xsl:when test="@valign and parent::cbml:tgroup/@align">
                    <xsl:attribute name="style" select="concat('valign:',@valign, ' align:',parent::cbml:tgroup/@align)"/>
                </xsl:when>
                <xsl:when test="@valign">
                    <xsl:attribute name="style" select="concat('valign:',@valign)"/>
                </xsl:when>
                <xsl:when test="parent::cbml:tgroup/@align">
                    <xsl:attribute name="style" select="concat('align:',parent::cbml:tgroup/@align)"/>
                </xsl:when>
            </xsl:choose>
            
            <xsl:apply-templates select="*|text|()"/>
        </thead>
    </xsl:template>
    
<!--    <xsl:template match="cbml:thead/cbml:row/cbml:entry/cbml:p" priority="10">
        <xsl:if test="preceding-sibling::cbml:p"><br/></xsl:if>
        <xsl:if test="@id">
            <a id="{@id}"/>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>-->
    
    <xsl:template match="cbml:tbody">
        <tbody>
            <!--<xsl:copy-of select="@valign"/>-->
            <!--<xsl:copy-of select="parent::cbml:tgroup/@align"/>-->
            <xsl:choose>
                <xsl:when test="@valign and parent::cbml:tgroup/@align">
                    <xsl:attribute name="style" select="concat('valign:',@valign, ' align:',parent::cbml:tgroup/@align)"/>
                </xsl:when>
                <xsl:when test="@valign">
                    <xsl:attribute name="style" select="concat('valign:',@valign)"/>
                </xsl:when>
                <xsl:when test="parent::cbml:tgroup/@align">
                    <xsl:attribute name="style" select="concat('align:',parent::cbml:tgroup/@align)"/>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates select="cbml:row"/>
        </tbody>  
    </xsl:template>
    
    <xsl:template match="cbml:row">
        <xsl:variable name="style">
            <xsl:if test="@rowsep='1'">border-bottom </xsl:if>
            <xsl:if test="@valign"><xsl:value-of select="@valign"/> </xsl:if>
            <xsl:if test="parent::cbml:thead and not(following-sibling::cbml:row)">border-bottom </xsl:if>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="cbml:entry or cbml:sidehead">
                <tr>
                    <xsl:if test="$style!=''">
                        <xsl:attribute name="class" select="$style"/>
                    </xsl:if>
                <xsl:apply-templates select="*"/>
                </tr>
            </xsl:when>
            <xsl:otherwise>
                <tr>
                    <td colspan="{ancestor::cbml:tgroup/@cols}"></td>
                </tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="cbml:entry">

<!--   epub and firefox don't get align correctly from attribute cols so this puts the atribute align on every 
entry. It assumes that if a table contains morerows then every entry has the attribute colname  -->
        <xsl:variable name="cols" select="number(parent::cbml:row/parent::*/parent::cbml:tgroup/@cols)"/>
        <xsl:variable name="sibs" select="count(parent::cbml:row/cbml:entry)"/>
        <xsl:variable name="spans">
            <xsl:value-of select="sum(parent::cbml:row/cbml:entry/@namest) - sum(parent::cbml:row/cbml:entry/@nameend)"/>
        </xsl:variable>
        <xsl:variable name="pos">
            <xsl:choose>
                <xsl:when test="$spans=''">
                    <xsl:if test="$cols=$sibs">
                       <xsl:value-of select="count(preceding-sibling::cbml:entry)+1"/> 
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:if test="$cols=$sibs+$spans">
                            <xsl:choose>
                                <xsl:when test="preceding-sibling::cbml:entry[@namest]">
                                       <xsl:variable name="pre-spans">
                                        <xsl:for-each select="preceding-sibling::cbml:entry[@namest]">
                                            <xsl:value-of  select="number(@nameend)-number(@namest)"/>
                                        </xsl:for-each>
                                      </xsl:variable> 
                                    <xsl:choose>
                                        <xsl:when test="$cols=$sibs+$pre-spans">
                                            <xsl:value-of select="count(preceding-sibling::cbml:entry) + $pre-spans +1"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="count(preceding-sibling::cbml:entry)+1"/> 
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="col-align">
            <xsl:choose>
                <xsl:when test="@colname">
                    <xsl:value-of  select="ancestor::cbml:tgroup/cbml:colspec[@colname=current()/@colname]/@align"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::cbml:tgroup/cbml:colspec[@colname=$pos]/@align"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="style">
            <xsl:if test="@rowsep='1'">border-bottom </xsl:if>
            <xsl:if test="@colsep='1' or ancestor::cbml:tgroup/cbml:colspec[@colname=$pos]/@colsep='1'">border-right </xsl:if>
            <xsl:if test="@tint and not(@tint-strength)">tint </xsl:if>
            <xsl:if test="not(@align) and $col-align='center'"><xsl:value-of select="$col-align"/> </xsl:if>
            <xsl:if test="@align"><xsl:value-of select="@align"/><xsl:text> </xsl:text></xsl:if>
            <xsl:if test="@valign='top'"><xsl:value-of select="@valign"/> </xsl:if>
        </xsl:variable>

        <xsl:element name="{if(ancestor::cbml:thead) then 'th' else 'td'}">
            <xsl:if test="$style!=''">
            <xsl:attribute name="class" select="$style"/>
            </xsl:if>
<!--            <xsl:if test="@rowsep='1'">
                <xsl:attribute name="style" select="'border-bottom: solid 1px'"/>  
            </xsl:if>
            <xsl:if test="@colsep='1' or ancestor::cbml:tgroup/cbml:colspec[@colname=$pos]/@colsep='1'">
                <xsl:attribute name="style" select="'border-right: solid 1px'"/>  
            </xsl:if>-->
            <xsl:if test="@morerows">
                <xsl:attribute name="rowspan" select="number(@morerows)+1"/>
            </xsl:if>
            <xsl:if test="@namest and @nameend">
                <xsl:attribute name="colspan" select="number(@nameend)-number(@namest) +1"/>
            </xsl:if>
            <xsl:if test="@tint and @tint-strength">
                <xsl:attribute name="style" select="concat('background-color:',cup:tintStrength(@tint-strength))"/>
            </xsl:if>
<!--            <xsl:if test="@tint">
                <xsl:attribute name="style" select="'background-color:gray'"/> 
            </xsl:if>-->
<!--            <xsl:choose>
                <xsl:when test="@rowsep='1' and @align">
                    <xsl:attribute name="style" select="concat('border-bottom: solid 1px align:', @align)"/>
                </xsl:when>
                <xsl:when test="@rowsep='1'"><xsl:attribute name="style" select="'border-bottom: solid 1px'"/></xsl:when>
                <xsl:when test="@align"><xsl:attribute name="style" select="concat('align:',@align)"/></xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="col-align">
                        <xsl:choose>
                            <xsl:when test="@colname">
                                <xsl:value-of  select="ancestor::cbml:tgroup/cbml:colspec[@colname=current()/@colname]/@align"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="ancestor::cbml:tgroup/cbml:colspec[@colname=$pos]/@align"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$col-align!=''">
                            
                            <xsl:attribute name="style" select="concat('align:',$col-align)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="ancestor::cbml:tgroup/@align">
                                
                                <xsl:attribute name="style" select="concat('align:',ancestor::cbml:tgroup/@align)"/>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>-->
           <!-- <xsl:if test="not(@align)">
                <xsl:variable name="col-align">
                    <xsl:choose>
                        <xsl:when test="@colname">
                            <xsl:value-of  select="ancestor::cbml:tgroup/cbml:colspec[@colname=current()/@colname]/@align"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="ancestor::cbml:tgroup/cbml:colspec[@colname=$pos]/@align"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$col-align!=''">
                        <xsl:attribute name="align" select="$col-align"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="ancestor::cbml:tgroup/@align">
                            <xsl:attribute name="align" select="ancestor::cbml:tgroup/@align"/>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>-->
            <xsl:if test="not(preceding-sibling::*)">
                <xsl:apply-templates select="parent::cbml:row/preceding-sibling::node()[1][self::processing-instruction('new-page')]"/>
            </xsl:if>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

<xsl:template match="cbml:sidehead">
    <xsl:variable name="cols" select="ancestor::cbml:tgroup/@cols"/>
    <td colspan="{$cols}"><xsl:apply-templates/></td>
</xsl:template>
    
    <xsl:template match="cbml:entry/cbml:p" priority="10">
        <xsl:if test="preceding-sibling::cbml:p"><br/></xsl:if>
        <xsl:if test="@id"><a id="{@id}"/></xsl:if>
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:function name="cup:Frame">
        <xsl:param name="type"/>
            <xsl:choose>
                <xsl:when test="$type='top'">above</xsl:when>
                <xsl:when test="$type='bottom'">below</xsl:when>
                <xsl:when test="$type='topbot'">hsides</xsl:when>
                <xsl:when test="$type='all'">box</xsl:when>
                <xsl:when test="$type='sides'">vsides</xsl:when>
                <xsl:when test="$type='none'">void</xsl:when>
            </xsl:choose>
    </xsl:function>
</xsl:stylesheet>