<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
    
<!--   
half title
title page
contributors
preface and foreword author
series page
glossary
-->
    <xsl:param name="variant-title-info" as="xs:string" select="'0'"/><!--set if variant title-info template to be used, 0 is Classic Mode-->
    <!--current design variants
        0 - cbml2epub Classic
        1 - adjustments for AU EPUB-only titles 2013-08:
            # author to appear between title and subtitle
        [add more here as necessary]
        -->

    
    <!--=========== HALF TITLE PAGE    move bio from title-info=============-->
<!--reviews to go after bio-->
    <xsl:template match="cbml:half-title">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="node()[not(self::cbml:reviews)]"/>
            <xsl:if test="not(preceding::cbml:half-title)">
            <xsl:apply-templates select="../cbml:title-info//cbml:bio"/>
            </xsl:if>
            <xsl:apply-templates select="cbml:reviews"/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:bio/cbml:sim-p/cbml:name[not(ancestor::cbml:contributors)(: predicate fixes ambiguous rule match with line 218 -TC 2014-01-10 :)]">
        <xsl:choose>
            <xsl:when test="not(descendant::unicode-placeholder)">
                <span class="htl-name"><xsl:value-of select="upper-case(.)"/></span>
            </xsl:when>
            <xsl:otherwise>
                <span class="htl-name"><xsl:value-of 
                    select="for $n in 
                            (./descendant::text()) 
                            return 
                            upper-case($n)" 
                    separator=""/></span><!--TODO: I shouldn't have done this. Replace when time permits and avoid in future... -TC--> 
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <xsl:template match="cbml:half-title/cbml:title">
        <h1 class="half-title"><xsl:apply-templates/></h1>
    </xsl:template>
    
    <xsl:template match="cbml:half-title/cbml:subtitle">
        <h2 class="half-title-subtitle"><xsl:apply-templates/></h2>
    </xsl:template>
  
  
    <!--=========== IMPRINTS PAGE ======================-->
    <xsl:template match="cbml:imprints-list | cbml:disclaimer">  
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>   
        </div>
    </xsl:template>

    <xsl:template match="cbml:imprints-list[@type='mission']">
        <div>
            <xsl:attribute name="class" select="'imprints-list_mission'"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:catalog-data">
        <div class="noindent">
            <em><xsl:apply-templates select="cbml:title/* |cbml:title/text()"/></em>
        </div>
        <xsl:apply-templates select="cbml:preformat|cbml:line"/>     
    </xsl:template>

    <xsl:template match="cbml:imprints//cbml:title">
        <p class="noindent"><span class="disclaimer-title"><xsl:apply-templates/></span></p>
    </xsl:template>

    <xsl:template match="cbml:imprints//cbml:email">
        <a href="{concat('mailto:',string-join(descendant::text(),''))}"><xsl:apply-templates/></a>
    </xsl:template>
            

<!--============== TITLE PAGE ===============-->
    
    <xsl:template match="cbml:title-info">
        <xsl:choose>
            <xsl:when test="number($variant-title-info) eq 1">
                <div class="title-info">
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="cbml:series-name | cbml:title"/>
                    <xsl:apply-templates select="cbml:author-group"/>
                    <xsl:apply-templates select="*[not(ancestor-or-self::cbml:series-name)][not(ancestor-or-self::cbml:title)][not(ancestor-or-self::cbml:author-group)]"/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="title-info">
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="cbml:logo">
        <div class="logo">
            <xsl:apply-templates select="cbml:mime" mode="image"/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:title-info/cbml:title">
        <h1 class="title-info"><xsl:apply-templates/></h1>
    </xsl:template>
    
    <xsl:template match="cbml:title-info/cbml:subtitle">
        <h2 class="title-info-subtitle"><xsl:apply-templates/></h2>
    </xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:author-group">
        <xsl:variable name="au-gr_type" select="if (not(@type)) then 'author' else(substring-before(@type, 's'))"/>
        <xsl:apply-templates select="." mode="type"/> <!--in common.xsl line 338-->
        <div class="{$au-gr_type}-group">
            <xsl:apply-templates mode="title-info">
                <xsl:with-param name="au-gr_type" select="$au-gr_type"/>
            </xsl:apply-templates>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:author" mode="title-info">
        <xsl:param name="au-gr_type"/>
        <xsl:choose>
            <xsl:when test="@type = $au-gr_type and number($variant-title-info) eq 0"/><!--no prefix div required-->
            <xsl:otherwise>
               <xsl:apply-templates select="." mode="type"/> <!--in common.xsl line 322-->
            </xsl:otherwise>
        </xsl:choose>
        <div class="author">
            <xsl:apply-templates  select="node()[not(self::cbml:role[following-sibling::cbml:address])]" mode="title-info"/> 
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:name[not(parent::cbml:sim-p)]">
        <xsl:apply-templates mode="title-info"/>
    </xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:given-names"><span class="given-names"><xsl:apply-templates mode="title-info"/></span></xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:surname"><span class="surname"><xsl:apply-templates mode="title-info"/></span></xsl:template>
    
    <xsl:template match="cbml:title-info//cbml:bio" mode="title-info"/>

    <xsl:template match="cbml:title-info//cbml:address" mode="title-info">
        <div class="title-address">
            <xsl:apply-templates select="preceding-sibling::*[1][self::cbml:role]"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    
    <xsl:template match="cbml:role" mode="title-info">
        <div class="role">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:title-info/cbml:volume">
        <div class="volume">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!--============== CONTRIBUTORS =============-->

    <xsl:template match="cbml:contributors//cbml:sim-p[not(preceding-sibling::cbml:sim-p)]">
        <xsl:apply-templates select="parent::cbml:bio/preceding-sibling::*[1][self::cbml:name]" mode="do-name"/>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:contributors//cbml:sim-p[preceding-sibling::cbml:sim-p]">
            <br/>
        <xsl:apply-templates/>
    </xsl:template>
    

    <xsl:template match="cbml:name" mode="do-name">
        <span class="contrib-name">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    

    <xsl:template match="cbml:name[following-sibling::*[1][self::cbml:bio] and ancestor::cbml:contributors]"/>
    


<!--  author to be div not default of span  -->
    
 
    <xsl:template match="cbml:contributors//cbml:author">
        <div class="contrib">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:contributors//cbml:name[not(following-sibling::*[1][self::cbml:bio])]">
        <span class="contrib-name">
            <xsl:apply-templates/>
        </span> 
    </xsl:template>
<!--  overrides author-group template in common -->
    

    <xsl:template match="cbml:contributors//cbml:author-group">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!-- ============= PREFACE AND FOREWORD AUTHORS ================= -->
    
<!--  overrides author-group template in common  -->
    <xsl:template match="cbml:preface/cbml:author-group|cbml:foreword/cbml:author-group|cbml:ack/cbml:author-group">
            <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:preface/cbml:author-group/cbml:author|cbml:foreword/cbml:author-group/cbml:author|cbml:ack/cbml:author-group/cbml:author">
        <xsl:choose>
            <xsl:when test="parent::cbml:author-group[following-sibling::*[not(self::cbml:fn-group)]]">
                <div class="author">
                    <xsl:apply-templates/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="fwd-author">
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
    <xsl:template match="cbml:preface//cbml:address|cbml:foreword//cbml:address|cbml:ack//cbml:address">
        <div class="fwd-address">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:preface//cbml:date|cbml:foreword//cbml:date|cbml:ack//cbml:date">
        <div class="fwd-date">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
<!-- ========================= SERIES PAGE============================   -->
<!--  series page to be at the back of the book  -->
    
<xsl:template match="cbml:series">
    <div>
        <xsl:apply-templates select="." mode="class"/>
        <xsl:apply-templates select="." mode="id"/>
        <xsl:apply-templates select="node()[not(self::cbml:fn-group)]"/>
        <!--<xsl:apply-templates select="node()[not(self::cbml:series-books)]"/>-->
        <xsl:apply-templates select="cbml:series-books[not(preceding-sibling::cbml:series-books)]" mode="ser-link"/>
    </div>
</xsl:template>
    
    <xsl:template match="cbml:series/cbml:title">
        <h3><xsl:apply-templates/></h3>
    </xsl:template>
    
    <xsl:template match="cbml:series-books" mode="ser-link">
        <xsl:variable name="fnm" select="cup:Filename(.)"></xsl:variable>
        <p>For a list of titles published in the series, please see <a href="{$fnm}">end of book</a>.</p>
    </xsl:template>
    
    <xsl:template match="cbml:series-books">
        <div class="series-books">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
            <!--<xsl:apply-templates select="following-sibling::cbml:fn-group[cbml:fn[some $c in current()/descendant::cbml:xref/@href satisfies $c eq @id]]" mode="series-books"/>-->
            <xsl:apply-templates select="following-sibling::cbml:fn-group[1][cbml:fn]" mode="series-books"/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:fn" mode="series-books">
       <div>
           <xsl:apply-templates select="." mode="id"/>
           <xsl:apply-templates select="." mode="class"/>
           <xsl:apply-templates/>
       </div>
    </xsl:template>
    
<!--   glossary where all terms are short to be tabular -->

<xsl:template match="cbml:term-head" mode="table-deflist">
    <tr>
       <th><xsl:apply-templates/></th> 
        <th><xsl:apply-templates select="following-sibling::cbml:def-head/node()"/></th>
    </tr>
</xsl:template> 
    
    <xsl:template match="cbml:def-head" mode="table-deflist">
        <xsl:choose>
            <xsl:when test="preceding-sibling::cbml:term-head"></xsl:when>
            <xsl:otherwise>
                <tr>
                    <th></th> 
                    <th><xsl:apply-templates/></th>
                </tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="cbml:def-item" mode="table-deflist">
        <tr>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="cbml:term | cbml:def" mode="table-deflist"/>
        </tr>
    </xsl:template>
    
    <xsl:template match="cbml:def" mode="table-deflist">
        <td>
            <xsl:apply-templates mode="table-deflist"/>
            <xsl:apply-templates select="following-sibling::node()[1][self::processing-instruction('new-page')]" mode="table-deflist"/>
            <xsl:apply-templates select="parent::cbml:def-item/following-sibling::node()[1][self::processing-instruction('new-page')]" mode="table-deflist"/>
        </td>
    </xsl:template>
    
    <xsl:template match="cbml:term" mode="table-deflist">
        <td class="term">
            <xsl:apply-templates select="preceding-sibling::cbml:label"/>
            <xsl:apply-templates/>
        </td>
    </xsl:template>
    
    <xsl:template match="cbml:def/cbml:p" mode="table-deflist">
        <xsl:apply-templates/>
        <xsl:if test="following-sibling::cbml:p"><br/></xsl:if>
    </xsl:template>
       
</xsl:stylesheet>
