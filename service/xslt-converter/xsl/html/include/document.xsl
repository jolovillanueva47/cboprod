<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml xlink xs"
    version="2.0">
    
<xsl:template match="cbml:doc-text">
    <div>
        <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="@type='trans'">doc-text-trans</xsl:when>
                <xsl:otherwise>doc-text-orig</xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates/>
    </div>
</xsl:template>
    
    <xsl:template match="cbml:document">
        <div>
            <xsl:attribute name="class" select="concat(local-name(), ' ', @type)">
            </xsl:attribute>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

     
</xsl:stylesheet>