<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
<!--adds back link-->
    <xsl:template match="cbml:fn/cbml:p">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:choose>
<!--      do not enclose in p when only contains a block      -->
            <xsl:when test="*[contains($block-elements, local-name(.)) and not(self::cbml:citation) and not(following-sibling::node() or preceding-sibling::node())]">
                <xsl:if test="preceding-sibling::*[position()=1 and self::cbml:label]">
                    <xsl:apply-templates select="preceding-sibling::cbml:label" mode="do-label"/>
                </xsl:if>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
        <p>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:if test="preceding-sibling::*[position()=1 and self::cbml:label]">
                <xsl:apply-templates select="preceding-sibling::cbml:label" mode="do-label"/>
            </xsl:if>
            <xsl:apply-templates select="node()[not(@position='float')]"/>
        </p>
        <xsl:apply-templates select="*[@position='float']"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="cbml:xref" mode="do-backlink">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:variable name="link-to-fn">
            <xsl:choose>
                <xsl:when test="key('element-by-id',$link-att) 
                    [local-name(.) = 'fn' or local-name(.) = 'commentary-note' or local-name(.) = 'collation-note']
                    or key('element-by-id',$link-att) [local-name(.)='citation' and cbml:label]">yes</xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
<!--        <xsl:choose>
            <xsl:when test="number($product-version) &lt; 5">
              <xsl:variable name="in-table">
                  <xsl:choose>
                      <xsl:when test="ancestor::cbml:table">yes</xsl:when>
                      <xsl:otherwise>no</xsl:otherwise>
                  </xsl:choose>
              </xsl:variable> 
                <xsl:if test="$in-table='no' and $link-to-fn='yes'">
                    <xsl:if test="not(preceding::cbml:xref[not(ancestor::cbml:table)][@*[local-name()='href']=$link-att])">
                        <a id="{concat('rp', $link-att)}"/>
                    </xsl:if>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>-->
                <xsl:if test="$link-to-fn='yes'">
                    <xsl:if test="not(preceding::cbml:xref[@*[local-name()='href']=$link-att])">
                        <a id="{concat('rp', $link-att)}"/>
                    </xsl:if>
                </xsl:if>
<!--            </xsl:otherwise>
        </xsl:choose>-->
        <!--            <xsl:if test="(not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)) and
                (key('element-by-id',$link-att) [local-name(.) = 'fn' or local-name(.) = 'commentary-note' or local-name(.) = 'collation-note']
                or key('element-by-id',$link-att) [local-name(.)='citation' and cbml:label])">
                <xsl:if test="not(preceding::cbml:xref[not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)][@*[local-name()='href']=$link-att])">
                    <a id="{concat('rp', $link-att)}"></a>
                </xsl:if>
            </xsl:if>-->
    </xsl:template>
    
<!--  adds back link to numbered references  -->
    <xsl:template match="cbml:citation/cbml:label | cbml:fn/cbml:label" mode="do-label">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:variable name="cur-id" select="parent::*/@id"></xsl:variable>
        <xsl:variable name="local">
            <xsl:choose>
                <xsl:when test="ancestor::cbml:table or ancestor::cbml:boxed-text 
                    or ancestor::cbml:array or ancestor::cbml:toc">yes</xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span class="label">
          <!--  <xsl:choose>
                <xsl:when test="number($product-version) &lt; 5">
                    <xsl:variable name="ok-to-link">
                        <xsl:choose>
                            <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id   
                                and not(ancestor::cbml:table or ancestor::cbml:preformat[@type='poetry'])]">yes</xsl:when>
                            <xsl:otherwise>no</xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$ok-to-link='yes' 
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates/>
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>-->
                    <xsl:choose>
                        <!--   do not backlink table, box or toc footnotes or citations-->
                        <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id][not(ancestor::cbml:preformat[@type='poetry']) and not(preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id])]  
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates/>
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
<!--                </xsl:otherwise>
            </xsl:choose>-->
        </span>
        <xsl:text> </xsl:text> 
    </xsl:template>

</xsl:stylesheet>
