<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all"
    version="2.0">



    <!--
chapter
blocks:
    quote and verse
    lists
    speech
    playtest
    refs
    figures
    images
    equations
    preformat
    linguistics
-->

    <!-- BLOCKS -->
    <!--========== move blocks outside of paras    -->
    <xsl:template match="*" mode="in-para-start">
        <xsl:if
            test="parent::*[(self::cbml:p[not(parent::cbml:entry or parent::cbml:speech)] or self::cbml:dict-p or self::cbml:chronology-list[parent::p]) and not(parent::cbml:list-item or parent::cbml:caption/parent::cbml:fig or parent::cbml:caption/parent::cbml:fig-group)] 
            and not(@position='float' and not(parent::cbml:p/parent::cbml:commentary-note)) 
            and (preceding-sibling::node()or following-sibling::node()
                or parent::*[(self::cbml:p[not(parent::cbml:entry or parent::cbml:speech)] or self::cbml:dict-p) and not(parent::cbml:fn) and preceding-sibling::*[1][self::cbml:label[not(parent::cbml:answer)]]])">
            <xsl:text disable-output-escaping="yes">&lt;/p&gt;</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="*" mode="in-para-end">
        <xsl:if
            test="parent::*[(self::cbml:p[not(parent::cbml:entry or parent::cbml:speech)] or self::cbml:dict-p or self::cbml:chronology-list[parent::p]) and not(parent::cbml:list-item  or parent::cbml:caption/parent::cbml:fig or parent::cbml:caption/parent::cbml:fig-group)] 
            and not(@position='float' and not(parent::cbml:p/parent::cbml:commentary-note))  
            and (preceding-sibling::node()or following-sibling::node()
            or parent::*[(self::cbml:p[not(parent::cbml:entry or parent::cbml:speech)] or self::cbml:dict-p) and not(parent::cbml:fn) and preceding-sibling::*[1][self::cbml:label[not(parent::cbml:answer)]]] )">
            <xsl:text disable-output-escaping="yes">&lt;p class="noindent"&gt;</xsl:text>
        </xsl:if>

    </xsl:template>

    <!--  ========= CHRONOLOGY-LIST ============  -->

    <xsl:template match="cbml:chronology-list">
        <xsl:apply-templates mode="heading"/>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="cbml:event">
        <xsl:if test="parent::cbml:chronology-list[parent::cbml:p or parent::cbml:dict-p]">
            <xsl:message select="concat('TRUE AND FORCE-CLOSING A PARA AT ',descendant::text()[1])"/>
            <xsl:text disable-output-escaping="yes">&lt;/p&gt;</xsl:text>
        </xsl:if>
        <table class="event">
            <xsl:element name="tr">
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
                <xsl:if test="@xml:lang">
                    <xsl:attribute name="xml:lang" select="@xml:lang"/>
                </xsl:if>
                <td class="event-date">
                    <xsl:apply-templates select="cbml:date"/>
                </td>
                <td class="event-text">
                    <xsl:message select="for $n in (node()[not(self::cbml:date)]) return concat('|',local-name($n),'|')"/>
                    <xsl:apply-templates select="node()[not(self::cbml:date)]"/>
                </td>
            </xsl:element>
        </table>
        <xsl:if test="parent::cbml:chronology-list[parent::cbml:p or parent::cbml:dict-p]">
            <xsl:text disable-output-escaping="yes">&lt;p class="noindent"&gt;</xsl:text>
        </xsl:if>
    </xsl:template>

    <!--  ============== TABLES ================  -->

    <xsl:template match="cbml:table-wrap | cbml:table-wrap-group">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="table-wrap">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <!--  ========== DISP-QUOTE  AND VERSE ==================  -->
    <!--  the div is needed to allow page breaks if necessary -->
    <xsl:template match="cbml:disp-quote|cbml:verse-group">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <blockquote>
            <div>
                <!--has to contain a div to make it valid when there is a page break in it-->
                <xsl:apply-templates select="." mode="class"/>
                <xsl:apply-templates select="." mode="heading"/>
                <xsl:apply-templates
                    select="node()[not(self::cbml:attrib[cbml:xref[not(following-sibling::* or preceding-sibling::*)] and not(text()) ]) ]"
                />
            </div>
        </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:example | cbml:solution | cbml:array[not(ancestor::cbml:event)] | cbml:letter">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <blockquote>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <div>
                <xsl:apply-templates/>
            </div>
        </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:array[ancestor::cbml:event]">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:statement">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <blockquote class="statement-{@type}">
            <xsl:apply-templates select="." mode="heading"/>
            <div>
                <xsl:apply-templates/>
            </div>
        </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <!-- if attrib is only an xref then append to final verse-line -->
    <xsl:template match="cbml:verse-line">
        <div class="poetry">
            <xsl:apply-templates select="node()[not(self::cbml:marginal-number)]"/>
            <xsl:if
                test="not(following-sibling::cbml:verse-line) and following-sibling::cbml:attrib[cbml:xref[not(following-sibling::* or preceding-sibling::*)] and not(text())]">
                <xsl:apply-templates select="following-sibling::cbml:attrib"/>
            </xsl:if>
            <xsl:apply-templates select="*[self::cbml:marginal-number]"/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:verse-line[@indent='yes']">
        <div class="poetry">
            <!--<xsl:text>&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:text>-->
            <xsl:choose>
                <!--                <xsl:when test="@indent-level='8'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='7'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='6'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='5'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='4'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>-->
                <xsl:when test="@indent-level='3'"
                    >&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='2'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='1'">&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="@indent-level='0'"/>
                <xsl:otherwise>&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="node()[not(self::cbml:marginal-number)]"/>
            <xsl:if
                test="not(following-sibling::cbml:verse-line or following-sibling::cbml:part-verse-line) and following-sibling::cbml:attrib[cbml:xref[not(following-sibling::* or preceding-sibling::*)]and not(text())]">
                <xsl:apply-templates select="following-sibling::cbml:attrib"/>
            </xsl:if>
            <xsl:apply-templates select="*[self::cbml:marginal-number]"/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:part-verse-line">
        <xsl:variable name="len" select="string-length(preceding-sibling::cbml:verse-line[1]) div 2"/>
        <div>
            <!--            <xsl:choose>
                <xsl:when test="$len&gt;40">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="$len&gt;35">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="$len&gt;30">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="$len&gt;25">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:when test="$len&gt;20">&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:when>
                <xsl:otherwise>&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:otherwise>
            </xsl:choose>-->
            <xsl:variable name="len2">
                <xsl:choose>
                    <xsl:when test="$len&gt;40">40</xsl:when>
                    <xsl:when test="$len&gt;35">35</xsl:when>
                    <xsl:when test="$len&gt;30">30</xsl:when>
                    <xsl:when test="$len&gt;25">25</xsl:when>
                    <xsl:when test="$len&gt;20">20</xsl:when>
                    <xsl:otherwise>15</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="class" select="concat ('poetry-part-line', $len2)"/>
            <xsl:apply-templates select="node()[not(self::cbml:marginal-number)]"/>
            <xsl:if
                test="not(following-sibling::cbml:verse-line or following-sibling::cbml:part-verse-line) and following-sibling::cbml:attrib[cbml:xref[not(following-sibling::* or preceding-sibling::*)] and not(text())]">
                <xsl:apply-templates select="following-sibling::cbml:attrib"/>
            </xsl:if>
            <xsl:apply-templates select="*[self::cbml:marginal-number]"/>
        </div>
    </xsl:template>


    <xsl:template match="cbml:verse-line/cbml:marginal-number">
        <span class="marginal-number">
            <xsl:text>&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="cbml:attrib" mode="#default toc">
        <xsl:choose>
            <!--<xsl:when test="parent::cbml:p[parent::cbml:list-item] or parent::cbml:caption[parent::cbml:fig or parent::cbml:fig-group]"></xsl:when>-->
            <xsl:when test="parent::cbml:p[parent::cbml:list-item]"/>
            <xsl:when test="parent::cbml:table-wrap-foot">
                <div class="table-wrap-foot_attrib">
                    <xsl:apply-templates/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="attrib">
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- =========  LISTS  ======== -->


    <xsl:template match="cbml:list" mode="#default toc">
        <xsl:if test="not(parent::cbml:list-item)">
            <xsl:apply-templates select="." mode="in-para-start"/>
        </xsl:if>
        <xsl:if test="cbml:title">
            <xsl:apply-templates select="." mode="heading"/>
        </xsl:if>
        <ul class="{@type}">
            <xsl:if test="not(cbml:title)">
                <xsl:apply-templates select="." mode="id"/>
            </xsl:if>
            <xsl:apply-templates select="text() |*"/>
        </ul>
        <xsl:if test="not(parent::cbml:list-item)">
            <xsl:apply-templates select="." mode="in-para-end"/>
        </xsl:if>
    </xsl:template>


    <xsl:template match="cbml:list-item">
        <li>
            <xsl:apply-templates select="." mode="id"/>
            <!--            <xsl:choose>
                <xsl:when test="parent::cbml:list[@type='number' and starts-with(cbml:list-item[1]/cbml:label,'1') or starts-with(cbml:list-item[1]/cbml:label,'(1')
                    or starts-with(cbml:list-item[1]/cbml:label,'[1')]">
                    <xsl:apply-templates select="cbml:label/cbml:target"/>
                </xsl:when>
                <xsl:otherwise>-->
            <xsl:apply-templates select="cbml:label" mode="do-label"/>
            <!--                </xsl:otherwise>
            </xsl:choose>-->
            <xsl:apply-templates/>
            <xsl:apply-templates
                select="following-sibling::node()[not(self::processing-instruction('anchor'))][1][self::processing-instruction('new-page')] 
                | following-sibling::node()[not(self::processing-instruction('anchor'))][2][self::processing-instruction('new-page') and preceding-sibling::node()[not(self::processing-instruction('anchor'))][1][self::processing-instruction('new-page')]] "
            />
        </li>
    </xsl:template>

    <!--  stopp doing this : only use ol when the first list item is 1  -->
    <!--<xsl:template match="cbml:list[@type='number']">
        <xsl:variable name="first-lab" select="string(cbml:list-item[1]/cbml:label)"/>
        <xsl:variable name="num-lab" select="replace($first-lab, '[\W]', '')"/>
        <xsl:if test="not(parent::cbml:p/parent::cbml:list-item)">
            <xsl:apply-templates select="." mode="in-para-start"/>
        </xsl:if>       
        <xsl:if test="cbml:title">
            <xsl:apply-templates select="." mode="heading"/>
        </xsl:if>
          <xsl:choose>
              <!-\-<xsl:when test="(starts-with(cbml:list-item[1]/cbml:label,'1') or starts-with(cbml:list-item[1]/cbml:label,'(1')) and not(matches(cbml:list-item[1]/cbml:label,'[\w-[1]]'))">-\->
              <xsl:when test="$num-lab='1'"> 
              <ol class="{@type}">
                    <xsl:if test="not(cbml:title)">
                        <xsl:apply-templates select="." mode="id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:label/cbml:target"/>
                    <xsl:apply-templates select="text() |*"/>                       
                </ol> 
            </xsl:when>
              <xsl:otherwise>
                  <xsl:apply-templates select="cbml:label" mode="heading"/>
                <ul class="{@type}">
                    <xsl:if test="not(cbml:title)">
                        <xsl:apply-templates select="." mode="id"/>
                    </xsl:if>
                    <xsl:apply-templates select="text() |*"/>                       
                </ul>  
              </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="not(parent::cbml:p/parent::cbml:list-item)">
            <xsl:apply-templates select="." mode="in-para-end"/>
        </xsl:if>
    </xsl:template>-->

    <!--  do not have paras in list items  -->
    <xsl:template match="cbml:list-item/cbml:p | cbml:list-item/cbml:dict-p" priority="10">
        <xsl:if test="preceding-sibling::cbml:p">
            <br/>
        </xsl:if>
        <xsl:if test="@id">
            <a id="{@id}"/>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="cbml:list-item/cbml:p/cbml:label |cbml:list-item/cbml:dict-p/cbml:label">
        <span class="label">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--def list done simply with dl-->

    <xsl:template match="cbml:def-list" mode="#default toc">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <xsl:if test="cbml:title or cbml:label">
            <xsl:apply-templates select="." mode="heading"/>
        </xsl:if>
        <div>
            <xsl:if test="@id">
                <xsl:copy-of select="@id"/>
            </xsl:if>
            <xsl:attribute name="class" select="'def-list'"/>
            <xsl:if test="child::cbml:term-head or child::cbml:def-head">
                <dl class="def-item">
                    <xsl:apply-templates select="child::cbml:term-head | child::cbml:def-head"/>
                </dl>
            </xsl:if>
            <xsl:apply-templates select="cbml:def-item | cbml:def-list"/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <!-- kindle hates sub-def-lists  hence the horrible stuff here-->
    <xsl:template match="cbml:def-list" mode="fictionalmodethatdoesntexist">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <xsl:if test="cbml:title or cbml:label">
            <xsl:apply-templates select="." mode="heading"/>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="cbml:def-list and not(cbml:def-item)">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="parent::cbml:def-list">
                <xsl:if
                    test="(preceding-sibling::cbml:def-item or following-sibling::cbml:def-item) and not(preceding-sibling::*[1][self::cbml:def-list])">
                    <xsl:text disable-output-escaping="yes">&lt;/dl&gt;</xsl:text>
                </xsl:if>
                <div class="sub-def-list">
                    <xsl:if test="not(cbml:title or cbml:label)">
                        <xsl:copy-of select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="*" mode="sub-def-list"/>
                </div>
                <xsl:if
                    test="(preceding-sibling::cbml:def-item or following-sibling::cbml:def-item) and not(following-sibling::*[1][self::cbml:def-list])">
                    <xsl:text disable-output-escaping="yes">&lt;dl class="def-lst"&gt;&lt;dt/&gt;&lt;dd/&gt;</xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:when test="parent::cbml:p/parent::cbml:def">
                <div class="sub-def-list">
                    <xsl:if test="not(cbml:title or cbml:label)">
                        <xsl:copy-of select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="*" mode="sub-def-list"/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <dl class="def-list">
                    <xsl:if test="not(cbml:title or cbml:label)">
                        <xsl:copy-of select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="*"/>
                </dl>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:def-item" mode="sub-def-list">
        <xsl:apply-templates select="*" mode="sub-def-list"/>
    </xsl:template>

    <xsl:template match="cbml:def-item">
        <dl class="def-item">
            <xsl:apply-templates select="*"/>
        </dl>
    </xsl:template>


    <!--  do new page before the term  -->
    <xsl:template match="cbml:term | cbml:term-head">
        <dt>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="parent::cbml:def-item/@id">
                <a id="{parent::cbml:def-item/@id}"/>
            </xsl:if>
            <xsl:if test="parent::cbml:def-item[preceding-sibling::*] and preceding::node()[1][self::processing-instruction('new-page')]">
                <xsl:apply-templates select="preceding::node()[1]"/>
            </xsl:if>
            <xsl:apply-templates/>
        </dt>
        <xsl:if
            test="(self::cbml:term and not(following-sibling::cbml:def)) or (self::cbml:term-head and not(following-sibling::cbml:def-head))">
            <dd class="{if (self::cbml:term-head) then 'def-head' else 'def'}"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="cbml:term | cbml:term-head" mode="sub-def-list">
        <span class="sub-term">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="parent::cbml:def-item/@id">
                <a id="{parent::cbml:def-item/@id}"/>
            </xsl:if>
            <xsl:if test="parent::cbml:def-item[preceding-sibling::*] and preceding::node()[1][self::processing-instruction('new-page')]">
                <xsl:apply-templates select="preceding::node()[1]"/>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:text>&#x00A0;&#x00A0;&#x00A0;</xsl:text>
        </span>
    </xsl:template>

    <!--  do new page or anchored fig after the def  -->
    <xsl:template match="cbml:def |cbml:def-head">
        <dd>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>
            <xsl:if test="following::node()[1][self::processing-instruction]">
                <xsl:apply-templates select="following::node()[1]"/>
            </xsl:if>
        </dd>
    </xsl:template>

    <xsl:template match="cbml:def |cbml:def-head" mode="sub-def-list">
        <div class="sub-def">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
            <xsl:if test="following::node()[1][self::processing-instruction]">
                <xsl:apply-templates select="following::node()[1]"/>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="processing-instruction('new-page')" mode="def-list-in-def table-deflist toc">
        <a>
            <xsl:attribute name="id" select="concat('page_' , . )"/>
        </a>
    </xsl:template>

    <xsl:template match="processing-instruction('new-page')" mode="def-list">
        <a>
            <xsl:attribute name="id" select="concat('page_' , . )"/>
        </a>
    </xsl:template>


    <!-- =========  SPEECH  =========== -->

    <!--    <xsl:template match="cbml:speech">
        <xsl:apply-templates select="." mode="in-para-start"/>
            <blockquote>
                <xsl:apply-templates select="." mode="id"/>
                <xsl:apply-templates select="." mode="class"/>
                <xsl:apply-templates select="cbml:speaker|processing-instruction()"/>
          </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>-->

    <!--   floats to stay where coded and not float to location on page
        move floats outside of main elem-->
    <!--  speaker to be uppercase and kindle won't do this with css  -->
    <!--    <xsl:template match="cbml:speaker">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
        <p class="speech">
                <small>
                    <xsl:value-of select="upper-case(.)"/>
                    <xsl:apply-templates select="cbml:target"/>
                </small>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[not(@position='float')]"/>
        </p>
        <xsl:apply-templates select="*[@position='float']"/>
        <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[@position='float']"/>
        <xsl:call-template name="long-speech">
            <xsl:with-param name="para" select="following-sibling::*[2]"/>
        </xsl:call-template>
    </xsl:template>-->

    <xsl:template name="long-speech">
        <xsl:param name="para"/>
        <xsl:if test="$para[self::cbml:p]">
            <xsl:apply-templates select="$para"/>
            <xsl:apply-templates
                select="$para/following-sibling::node()[1][self::processing-instruction()[not(following-sibling::node()[1][self::cbml:speaker or self::cbml:p])]]"/>
            <xsl:if test="$para[following-sibling::*[1][self::cbml:p]]">
                <xsl:call-template name="long-speech">
                    <xsl:with-param name="para" select="$para/following-sibling::*[1]"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!--    <xsl:template match="cbml:speech/cbml:p">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
        <p>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="parent::cbml:speech" mode="class"/>  
            <xsl:apply-templates select="node()[not(@position='float')]"/>
            <xsl:apply-templates select="following-sibling::node()[1][self::processing-instruction('new-page')]" mode="speech-pi"/> 
        </p>
        <xsl:apply-templates select="*[@position='float']"/>
    </xsl:template>-->

    <!--  pi not to be direction in blockquote  -->
    <xsl:template match="cbml:speech/processing-instruction('new-page')" mode="speech-pi">
        <a>
            <xsl:attribute name="id" select="concat('page_' , . )"/>
        </a>
    </xsl:template>

    <!--<xsl:template match="cbml:speech/processing-instruction('new-page')"/>-->

    <!--  speech with preformat or other block in the speech     -->
    <!--    <xsl:template match="cbml:speech[descendant::cbml:preformat or descendant::cbml:verse-group  or descendant::cbml:list]">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <table class="speech">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="cbml:speaker|processing-instruction()"/>
        </table>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>-->

    <!--    <xsl:template match="cbml:speech">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <table class="speech">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="cbml:speaker|processing-instruction()"/>
        </table>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>-->
    <xsl:template match="cbml:speech">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <blockquote>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <!--<xsl:apply-templates select="cbml:speaker|processing-instruction()"/>-->
            <xsl:apply-templates select="cbml:speaker"/>
        </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:speech/cbml:speaker">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
        <xsl:variable name="txt">
            <xsl:apply-templates select="node()[not(self::cbml:xref or self::cbml:target)]"/>
        </xsl:variable>
        <div class="speech">
            <xsl:apply-templates select="preceding-sibling::node()[1][self::processing-instruction()]"/>
            <small>
                <xsl:value-of select="upper-case($txt)"/>
                <xsl:apply-templates select="descendant::cbml:target | descendant::cbml:xref | processing-instruction()"/>
            </small>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[not(@position='float')]"/>
            <xsl:apply-templates select="*[@position='float']"/>
            <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[@position='float']"/>
            <xsl:apply-templates
                select="following-sibling::cbml:p[1]/following-sibling::node()[1][self::processing-instruction()[not(following-sibling::node()[1][self::cbml:speaker])]]"/>
            <xsl:call-template name="long-speech">
                <xsl:with-param name="para" select="following-sibling::*[2]"/>
            </xsl:call-template>
        </div>
    </xsl:template>

    <!--    <xsl:template match="cbml:speech/cbml:speaker">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
        <tr>
        <td class="speech">
            <small>
                <xsl:value-of select="upper-case(.)"/>
                <xsl:apply-templates select="cbml:target"/>
            </small>   
        </td>
        <td>
            <xsl:choose>
                <xsl:when test="following-sibling::cbml:p[1][cbml:preformat]">
                    <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[not(@position='float')]"/>   
                    <xsl:apply-templates select="*[@position='float']"/>
                    <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[@position='float']"/>
                    <xsl:call-template name="long-speech">
                        <xsl:with-param name="para" select="following-sibling::*[2]"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                      <p>
                             <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[not(@position='float')]"/>   
                            <xsl:apply-templates select="*[@position='float']"/>
                            <xsl:apply-templates select="following-sibling::cbml:p[1]/node()[@position='float']"/>
                            <xsl:call-template name="long-speech">
                                <xsl:with-param name="para" select="following-sibling::*[2]"/>
                            </xsl:call-template>
                       </p> 
                </xsl:otherwise>
            </xsl:choose>
          
        </td>
        </tr>
    </xsl:template>-->

    <!--    
    <xsl:template match="cbml:speech[descendant::cbml:preformat  or descendant::cbml:verse-group or descendant::cbml:list]/cbml:p" priority="2">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
            <xsl:apply-templates select="node()[not(@position='float')]"/>
            <xsl:apply-templates select="following-sibling::node()[1][self::processing-instruction('new-page')]" mode="speech-pi"/> 
        <xsl:apply-templates select="*[@position='float']"/>
    </xsl:template>-->

    <xsl:template match="cbml:speech/cbml:p" priority="2">
        <xsl:variable name="pos" select="count(preceding-sibling::cbml:speaker)"/>
        <!--<br/>-->
        <div class="speech-nospeaker">
            <xsl:apply-templates select="node()[not(@position='float')]"/>
            <xsl:apply-templates
                select="following-sibling::node()[1][self::processing-instruction('new-page')[not(following-sibling::node()[1][self::cbml:speaker])]]"
                mode="speech-pi"/>
            <xsl:apply-templates select="*[@position='float']"/>
        </div>
    </xsl:template>

    <!-- =========PLAYTEXT =============   -->
    <xsl:template match="cbml:p[not(parent::cbml:disp-quote)]/cbml:playtext | cbml:dict-p[not(parent::cbml:disp-quote)]/cbml:playtext">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <blockquote>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <div>
                <xsl:apply-templates/>
            </div>
        </blockquote>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:p[parent::cbml:disp-quote]/cbml:playtext | cbml:dict-p[parent::cbml:disp-quote]/cbml:playtext">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:disp-quote/cbml:p[cbml:playtext]">
        <xsl:apply-templates/>
        <xsl:if test="following-sibling::cbml:p">
            <br/>
        </xsl:if>
    </xsl:template>
    <!-- ========= REFS========== -->

    <!-- citation within fn to be span - now moved to common and no span-->

    <xsl:template match="cbml:attrib/cbml:citation">
        <span>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="cbml:label" mode="do-label"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- =========FIGURES========= -->

    <!--  caption not to be before image  -->
    <xsl:template match="cbml:fig">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="fig">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="cbml:mime-group | cbml:mime"/>
            <xsl:apply-templates select="node()[not(self::cbml:mime or self::cbml:mime-group)]"/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:label[(parent::cbml:fig or parent::cbml:fig-group) and not(following-sibling::cbml:caption)]">
        <div class="label">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <xsl:template match="cbml:fig-group">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="fig">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="cbml:fig|processing-instruction()"/>
            <xsl:apply-templates select="*[not(self::cbml:fig)]"/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>


    <xsl:template match="cbml:fig/cbml:caption |cbml:fig-group/cbml:caption">
        <br/>
        <xsl:apply-templates select="node()[not(self::cbml:attrib[preceding-sibling::cbml:p])]"/>
    </xsl:template>

    <xsl:template match="cbml:fig/cbml:caption/cbml:p|cbml:fig-group/cbml:caption/cbml:p">
        <xsl:if test="not(preceding-sibling::cbml:p)">
            <xsl:apply-templates select="parent::cbml:caption/preceding-sibling::*[1][self::cbml:label]" mode="do-label"/>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:text> </xsl:text>
        <!--        <xsl:apply-templates select="following-sibling::cbml:attrib/node()"/>
        <xsl:if test="following-sibling::cbml:p">
            <br/>
        </xsl:if>-->
        <xsl:choose>
            <xsl:when test="following-sibling::cbml:p">
                <br/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="following-sibling::cbml:attrib/node()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="cbml:fig/cbml:mime | cbml:fig-group/cbml:mime">
        <xsl:apply-templates select="." mode="image"/>
    </xsl:template>


    <!--<xsl:template match="cbml:caption[cbml:title]" mode="heading">
            <div class="caption">
                <xsl:apply-templates select="preceding-sibling::cbml:label" mode="do-label"/>
                <xsl:apply-templates select="cbml:title" mode="heading"/>
            </div>
        </xsl:template>-->
    <!-- =============== IMAGES ============   -->

    <xsl:template match="cbml:mime-group">
        <xsl:apply-templates select="cbml:mime[@type='colour-graphic']"/>
    </xsl:template>

    <!-- calling image mode template -->
    <xsl:template match="cbml:mime">
        <div>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="image"/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:inline-mime | cbml:private-char">
        <span>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="image"/>
        </span>
    </xsl:template>

    <!--file ending from product file, do alt-text-->

    <xsl:template match="cbml:inline-mime |  cbml:private-char" mode="image">
        <xsl:variable name="link" select="@*[local-name()='href']" as="xs:string"/>
        <xsl:variable name="fnm" select="cup:imageFilename($link)"/>
        <xsl:variable name="imgdatafile" select="document('report.xml', .)"/>
        <xsl:variable name="imgdata" select="number($imgdatafile//report/file[contains($fnm,@name)]/dimensions/height)"/>
        <xsl:variable name="height" select="concat($imgdata div 40, 'em')"/>
        <!--   can't do this - get minute images in mobi     -->
        <!--<img src="{$fnm}" height="{$height}">-->
        <img src="{$fnm}">
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="$imgdata &gt; 90">v-large</xsl:when>
                    <xsl:when test="$imgdata &gt; 75">ex-large</xsl:when>
                    <xsl:when test="$imgdata &gt; 60">large</xsl:when>
                    <xsl:when test="$imgdata &gt; 40">med-large</xsl:when>
                    <xsl:otherwise>normal</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <!--                <xsl:choose>
                    <xsl:when test="parent::cbml:logo[ancestor::cbml:title-info[count(cbml:logo)=1]]">
                        <xsl:attribute name="width" select="'50%'"/>
                        <xsl:text>CAMBRIDGE UNIVERSITY PRESS</xsl:text>  
                    </xsl:when>-->
                <!-- <xsl:if test="preceding-sibling::cbml:label and not(cbml:alt-text!='')">
                        <xsl:value-of select="preceding-sibling::cbml:label"/>
                    </xsl:if>-->
                <!-- </xsl:choose>-->
            </xsl:attribute>
        </img>
    </xsl:template>

    <!--  image width to be 100% unless image file small -->
    <!-- uses  file report.xml created by img_check for epub2 build  -->
    <xsl:template match="cbml:mime" mode="image">
        <xsl:variable name="link" select="@*[local-name()='href']" as="xs:string"/>
        <xsl:variable name="size-lookup" select="document('report.xml', .)"/>
        <xsl:variable name="img-file" select="concat($link, '.png')"/>
        <xsl:variable name="small">
            <xsl:if test="$size-lookup//file[contains(@name,$img-file)]/dimensions[number(width)&lt; 520]">yes</xsl:if>
        </xsl:variable>

        <img src="{cup:imageFilename($link)}">
            <!--     use max-width instead       -->
            <!--            <xsl:if test="not($small='yes')">
                <xsl:attribute name="width" select="'100%'"/>   
            </xsl:if>-->
            <xsl:attribute name="alt">
                <!--                <xsl:choose>
                    <!-\-<xsl:when test="parent::cbml:logo[ancestor::cbml:title-info[count(cbml:logo)=1]]">-\->
                    <xsl:when test="parent::cbml:logo and contains('logo', $link)">
                        <xsl:text>CAMBRIDGE UNIVERSITY PRESS</xsl:text>  
                    </xsl:when>-->
                <!--<xsl:if test="preceding-sibling::cbml:label and not(cbml:alt-text!='')">
                        <xsl:value-of select="preceding-sibling::cbml:label"/>
                    </xsl:if>-->
                <!--                </xsl:choose>-->
            </xsl:attribute>
        </img>
    </xsl:template>

    <!--    <xsl:template match="cbml:logo/cbml:mime" mode="image">
        <xsl:variable name="link" select="@*[local-name()='href']" as="xs:string"/>
        <img src="{concat($image-path,$link, '.png')}" width="50%">
            <xsl:attribute name="alt">CAMBRIDGE UNIVERSITY PRESS</xsl:attribute>
        </img>
    </xsl:template>-->

    <xsl:template match="cbml:alt-text"/>


    <!-- =========EQUATIONS======== -->

    <!--        <xsl:template match="cbml:disp-formula">
            <xsl:apply-templates select="." mode="in-para-start"/>
            <xsl:apply-templates select="processing-instruction()[not(preceding-sibling::*)]"/>
            <xsl:apply-templates select="cbml:title" mode="heading"/>
            <xsl:choose>
                <xsl:when test="cbml:label">
                    <table width="100%">
                        <xsl:apply-templates select="." mode="id"/> 
                        <xsl:apply-templates select="." mode="class"/>
                        <tr>
                            <td>
                                <xsl:apply-templates select="cbml:mime|m:math"/>
                            </td>
                            <td align="right">
                                <xsl:apply-templates select="cbml:label" mode="class"/> 
                                <xsl:apply-templates select="cbml:label" mode="do-label"/>                   
                            </td>
                        </tr>
                    </table>  
                </xsl:when>
                <xsl:otherwise>
                    <div>
                        <xsl:apply-templates select="." mode="id"/> 
                        <xsl:apply-templates select="." mode="class"/> 
                        <xsl:apply-templates select="cbml:mime|m:math"/>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="processing-instruction()[not(following-sibling::*)]"/>
            <xsl:apply-templates select="." mode="in-para-end"/>
        </xsl:template>-->
    <xsl:template match="cbml:disp-formula | cbml:chem-struct">
        <xsl:variable name="img-file">> <xsl:for-each select="m:math">
                <xsl:value-of select="concat(@altimg, '.png')"/>
            </xsl:for-each>
            <!--     note: only works for png           -->
            <xsl:for-each select="cbml:mime">
                <xsl:value-of select="concat(cbml:mime[1]/@href, '.png')"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="size-lookup" select="document('report.xml', .)"/>
        <xsl:variable name="small">
            <xsl:if test="$size-lookup//file[contains($img-file,@name)]/dimensions[number(width)&lt; 520]">yes</xsl:if>
        </xsl:variable>

        <xsl:apply-templates select="." mode="in-para-start"/>
        <xsl:apply-templates select="processing-instruction()[not(preceding-sibling::*)]"/>
        <xsl:apply-templates select="cbml:title" mode="heading"/>
        <xsl:choose>
            <xsl:when test="cbml:label and ($small='yes' or cbml:p[string-length(.) &lt; 50])">
                <table class="{local-name()}">
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>
                    <tr>
                        <td style="width:90%">
                            <xsl:apply-templates select="cbml:mime|m:math|cbml:p"/>
                        </td>
                        <td>
                            <xsl:apply-templates select="cbml:label" mode="class"/>
                            <xsl:apply-templates select="cbml:label" mode="do-label"/>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <xsl:when test="cbml:label and not($small='yes')">
                <dl>
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>

                    <dt>
                        <xsl:apply-templates select="cbml:mime|m:math|cbml:p"/>
                    </dt>
                    <dd>
                        <xsl:apply-templates select="cbml:label" mode="class"/>
                        <xsl:apply-templates select="cbml:label" mode="do-label"/>
                    </dd>

                </dl>
            </xsl:when>
            <xsl:otherwise>
                <div>
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>
                    <xsl:apply-templates select="cbml:mime|m:math|cbml:p"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="processing-instruction()[not(following-sibling::*)]"/>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:disp-formula/cbml:mime | cbml:chem-struct/cbml:mime" priority="10">
        <xsl:apply-templates select="." mode="image"/>
    </xsl:template>


    <!--   maths to be image -->
    <xsl:template match="m:math">
        <xsl:choose>
            <xsl:when test="parent::cbml:disp-formula or parent::cbml:p/parent::cbml:entry or parent::cbml:chem-struct">
                <img src="{cup:imageFilename(@altimg)}" alt="{string(.)}"/>
                <xsl:if test="following-sibling::m:math">
                    <br/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="fnm" select="cup:imageFilename(@altimg)"/>
                <xsl:variable name="imgdatafile" select="document('report.xml', .)"/>
                <xsl:variable name="imgdata" select="number($imgdatafile//report/file[contains($fnm, @name)]/dimensions/height)"/>
                <!--   round for mobi             -->
                <xsl:variable name="height" select="concat(round($imgdata div 40), 'em')"/>
                <span>
                    <xsl:attribute name="class">
                        <xsl:choose>
                            <xsl:when test="$imgdata &gt; 60">lg-inline-math</xsl:when>
                            <xsl:otherwise>inline-math</xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <img src="{cup:imageFilename(@altimg)}" alt="{string(.)}">
                        <xsl:attribute name="class">
                            <xsl:choose>
                                <xsl:when test="$imgdata &gt; 90">v-large</xsl:when>
                                <xsl:when test="$imgdata &gt; 75">ex-large</xsl:when>
                                <xsl:when test="$imgdata &gt; 60">large</xsl:when>
                                <xsl:when test="$imgdata &gt; 40">med-large</xsl:when>
                                <xsl:otherwise>normal</xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                    </img>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="cbml:disp-formula/cbml:p | cbml:chem-struct/cbml:p " priority="10">
        <xsl:apply-templates/>
    </xsl:template>
    <!--=========== PREFORMAT =============-->

    <xsl:template match="cbml:preformat">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <xsl:choose>
            <xsl:when test="@type='poetry' and @altimg">
                <xsl:variable name="img-file" select="concat($image-path, '/', cup:imageFilename(@altimg))"/>
                <img src="{$img-file}" alt="{string(.)}"/>
            </xsl:when>
            <xsl:otherwise>
                <pre><xsl:if test="@type"><xsl:attribute name="class" select="@type"/></xsl:if><xsl:apply-templates select="node()[not(self::cbml:attrib)]"/></pre>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="cbml:attrib"/>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <!--=========== BOXED-TEXT =============-->

    <xsl:template match="cbml:boxed-text">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="@position='margin'">box-margin</xsl:when>
                    <xsl:otherwise>box</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>


    <!--  label to go at front of first paragraph [template in common.xsl]  -->
    <!--    <xsl:template match="cbml:statement" mode="heading">
            <xsl:if test="cbml:title"><xsl:apply-imports/></xsl:if>
    </xsl:template>-->

    <!-- =================  QUESTIONS AND ANSWERS ===============       -->
    <xsl:template match="@answer-link">
        <p class="answer-link">
            <a href="{cup:Link(parent::* ,.)}">link to answer</a>
        </p>
    </xsl:template>

    <xsl:template match="@question">
        <p class="answer-link">
            <a href="{cup:Link(parent::* ,.)}">back to question</a>
        </p>
    </xsl:template>

    <!--    <xsl:template match="cbml:question/cbml:label">
        <xsl:if test="not(following-sibling::*[1][self::cbml:p or self::cbml:instruction])">
            <xsl:apply-templates select="." mode="do-label"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="cbml:example/cbml:label">
        <xsl:if test="not(following-sibling::*[1][self::cbml:p])">
            <xsl:apply-templates select="." mode="do-label"/>
        </xsl:if>
    </xsl:template>-->

    <xsl:template match="cbml:exercise">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="exercise">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label)]"/>
        </div>
        <xsl:if test="not(cbml:question[@answer-link])">
            <xsl:apply-templates select="@answer-link"/>
        </xsl:if>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>


    <!--    <xsl:template match="cbml:solution">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="solution">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label)]"/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>-->

    <xsl:template match="cbml:question">
        <div class="question">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label)]"/>
        </div>
        <xsl:apply-templates select="@answer-link"/>
    </xsl:template>

    <xsl:template match="cbml:answer">
        <div class="answer">
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label)]"/>
        </div>
        <xsl:apply-templates select="@question"/>
    </xsl:template>

    <!-- =============LINGUISTICS==============   -->

    <xsl:template match="cbml:linguistics">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="linguistics">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:choose>
                <xsl:when test="cbml:label">
                    <table class="linguistics">
                        <tr>
                            <td class="linguistics">
                                <xsl:apply-templates select="cbml:label" mode="do-label"/>
                            </td>
                            <td class="linguistics">
                                <xsl:apply-templates select="node()[not(self::cbml:label)]"/>
                            </td>
                        </tr>
                    </table>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <xsl:template match="cbml:linguistics-item">
        <div class="linguistics-item">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:choose>
                <xsl:when test="cbml:label">
                    <table class="linguistics">
                        <tr>
                            <td class="linguistics">
                                <xsl:apply-templates select="cbml:label" mode="do-label"/>
                            </td>
                            <td class="linguistics">
                                <xsl:apply-templates select="node()[not(self::cbml:label)]"/>
                            </td>
                        </tr>
                    </table>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="cbml:linguistics/cbml:array | cbml:linguistics-item/cbml:array" priority="10">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="cbml:dictionary-letter">
        <xsl:choose>
            <xsl:when
                test="not(preceding-sibling::cbml:dictionary-letter) and parent::cbml:dictionary[parent::cbml:book or parent::cbml:main]">
                <div>
                    <xsl:apply-templates select="parent::cbml:dictionary" mode="heading"/>
                    <xsl:apply-templates select="preceding-sibling::cbml:p"/>
                    <xsl:apply-templates select="." mode="heading"/>
                    <div class="dictionary-letter">
                        <!--           <xsl:apply-templates select="." mode="dict-toc"/>-->
                        <xsl:apply-templates/>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="heading"/>
                <div class="dictionary-letter">
                    <!--           <xsl:apply-templates select="." mode="dict-toc"/>-->
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--    <xsl:template match="cbml:dictionary-letter" mode="dict-toc">
        <div class="dict-toc">
            <xsl:apply-templates select="cbml:dictionary-entry" mode="dict-toc"/> 
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:dictionary-entry" mode="dict-toc">
        <span class="dict-toc">
            <a href="#{@id}"><xsl:apply-templates select="cbml:label | cbml:title | cbml:subtitle" mode="heading"/></a>
        </span>
        <xsl:text> - </xsl:text>
    </xsl:template>-->

</xsl:stylesheet>
