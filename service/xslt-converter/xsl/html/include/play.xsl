<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs xd m"
    version="2.0">
    
    <xsl:template match="cbml:commentary-note/cbml:p">
            <xsl:if test="preceding-sibling::cbml:p"><br/></xsl:if>
            <xsl:apply-templates/>
    </xsl:template>
    
<!--    <xsl:template match="cbml:commentary-note/cbml:label | cbml:collation-note/cbml:label">
        <span class="label"><xsl:apply-templates/></span>
    </xsl:template>-->
    
    <xsl:template match="cbml:collation-note/cbml:p">
        <!--<span class="collation-note">-->
            <!--<xsl:apply-templates select="@*"/>-->
            <xsl:apply-templates/>
        <!--</span>-->
    </xsl:template>
    
<!--   add back links for commentary notes -->
<!--    <xsl:template match="cbml:commentary-note">
        <p  class="commentary-note" id="{@id}">
            <xsl:if test="preceding::cbml:xref[@*[local-name()='href']=current()/@id]">
                <a class="nounder" href="{cup:backLink(.,@id)}">
                    <xsl:apply-templates select="cbml:line-number"/>
                </a>
            </xsl:if>
        <xsl:apply-templates select="cbml:label | cbml:p"/>
        </p>
    </xsl:template>-->
    
    <xsl:template match="cbml:commentary-note">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:variable name="local">
            <xsl:choose>
                <xsl:when test="ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text 
                    or ancestor::cbml:array or ancestor::cbml:toc">yes</xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <p  class="commentary-note" id="{@id}">
            <xsl:choose>
                <xsl:when test="number($product-version) &lt; 5">
                    <xsl:variable name="ok-to-link">
                        <xsl:choose>
                            <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id   
                                and not(ancestor::cbml:preformat[@type='poetry'])]">yes</xsl:when>
                            <xsl:otherwise>no</xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$ok-to-link='yes' 
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates/>
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <!--   do not backlink table, box or toc footnotes or citations-->
                        <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id][not(ancestor::cbml:preformat[@type='poetry'])]  
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates select="cbml:line-number"/>
                            </a>
                            <xsl:apply-templates select="node()[not(self::cbml:line-number)]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
<!--                <xsl:choose>
                    <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/@id   
                        and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)]  
                        and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text or ancestor::cbml:array)">
                        <a class="nounder" href="{cup:backLink(.,@id)}">
                            <xsl:apply-templates select="cbml:line-number"/>
                        </a>
                        <xsl:apply-templates select="node()[not(self::cbml:line-number)]"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose> -->
        </p>
    </xsl:template>
    
    <xsl:template match="cbml:commentary-note/cbml:label | cbml:collation-note/cbml:label">
        <span class="label">
            <xsl:apply-templates/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:commentary-note/cbml:line-number | cbml:collation-note/cbml:line-number">
        <span class="note-line-number">
            <xsl:apply-templates/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>
    
<!--    <xsl:template match="cbml:collation-note">
        <p class="collation-note" id="{@id}">
            <xsl:if test="preceding::cbml:xref[@*[local-name()='href']=current()/@id]">
                <a class="nounder" href="{cup:backLink(.,@id)}">
                    <xsl:apply-templates select="cbml:line-number"/>
                </a>
            </xsl:if>
        <xsl:apply-templates/>
        </p>
    </xsl:template>-->
    <xsl:template match="cbml:collation-note">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:variable name="local">
            <xsl:choose>
                <xsl:when test="ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text 
                    or ancestor::cbml:array or ancestor::cbml:toc">yes</xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <p class="collation-note" id="{@id}">
            <xsl:choose>
                <xsl:when test="number($product-version) &lt; 5">
                    <xsl:variable name="ok-to-link">
                        <xsl:choose>
                            <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id   
                                and not(ancestor::cbml:preformat[@type='poetry'])]">yes</xsl:when>
                            <xsl:otherwise>no</xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$ok-to-link='yes' 
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates/>
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <!--   do not backlink table, box or toc footnotes or citations-->
                        <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::*/@id][not(ancestor::cbml:preformat[@type='poetry'])]  
                            and $local='no'">
                            <a class="nounder" href="{cup:backLink(parent::*, parent::*/@id)}">
                                <xsl:apply-templates select="cbml:line-number"/>
                            </a>
                            <xsl:apply-templates select="node()[not(self::cbml:line-number)]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
<!--            <xsl:choose>
                <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/@id   
                    and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)]  
                    and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text or ancestor::cbml:array)">
                    <a class="nounder" href="{cup:backLink(.,@id)}">
                    <xsl:apply-templates select="cbml:line-number"/>
                    </a>
                    <!-\-<xsl:apply-templates select="cbml:label" mode="add-label"/>-\->
                    <xsl:apply-templates select="node()[not(self::cbml:line-number)]"/> 
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose> -->  
        </p>
    </xsl:template>
    
    
    <xsl:template match="cbml:body/cbml:play">
        <div class="play">
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:play-verse | cbml:prose |cbml:play-text">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:play-line[not(preceding-sibling::*)]">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="cbml:play-text[1]/@id">
                <xsl:attribute name="id" select="cbml:play-text[1]/@id"/>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="cbml:part-line[not(preceding-sibling::*)]">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="cbml:play-text[1]/@id">
                <xsl:attribute name="id" select="cbml:play-text[1]/@id"/>
            </xsl:if>
            <xsl:text>&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:play-line[preceding-sibling::*]">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="cbml:play-text[1]/@id">
            <xsl:attribute name="id" select="cbml:play-text[1]/@id"/>
            </xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:part-line[preceding-sibling::*]">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="cbml:play-text[1]/@id">
                <xsl:attribute name="id" select="cbml:play-text[1]/@id"/>
            </xsl:if>
            <xsl:text>&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;</xsl:text>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:character-list-item/cbml:p[preceding-sibling::*[1][self::cbml:character]]" priority="10">
        <span><xsl:apply-templates/></span>
    </xsl:template>
    
    <xsl:template match="cbml:play-line/cbml:stage-direction | cbml:part-line/cbml:stage-direction">
        <span class="stage-direction"><xsl:apply-templates/></span>
    </xsl:template>
   
    <xsl:template match="cbml:character">
        <xsl:variable name="txt">
            <xsl:apply-templates select="node()[not(self::cbml:xref or self::cbml:target)]"/>
        </xsl:variable>
        <span class="character">
            <xsl:apply-templates select="." mode="id"/>
            <xsl:value-of select="upper-case($txt)"/>
            <xsl:apply-templates select="descendant::cbml:xref | descendant::cbml:target"/>
        </span>
    </xsl:template>
</xsl:stylesheet>
