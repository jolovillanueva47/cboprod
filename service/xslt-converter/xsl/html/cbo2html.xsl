<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
    <!-- This shares imported files with cbml2epub, so any changes specific to this build go in this file
    the functions file is not shared with cbml2epub so beware extra functions needed by the imported files
    -->
   
     <xsl:import href="main.xsl"/> 
    
    <xsl:output method="html" indent="yes"/>
    

<!--  pass in image-path as a parameter  -->
    <xsl:param name="image-path"/>
    <xsl:param name="css-path" select="'css/'"/>
    
<!--    currently not used-->
    <xsl:param name="filename"/>
	<xsl:param name="filepath"/>
    
    <!-- ============ HTML for CBO ================-->
  
    <xsl:key name="element-by-id" match="*" use="@id"/>
    <xsl:key name="element-by-href" match="*" use="@href"/>
    
    <!-- set isbn in build file -->
    <xsl:param name="isbn"/>
   
    <xsl:variable name="book-title" select="cbml:book/cbml:prelims/cbml:title-info/cbml:title"/>
    <xsl:variable name="extra-chunk"/>

   
<!--   one html file per chunk -->
<!--    <xsl:template match="/">
            <html  xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="{concat($css-path, 'cbo.css')}" rel="stylesheet" type="text/css"></link> 
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                       <xsl:apply-templates/>                     
                </body>
            </html>
    </xsl:template>-->
    
<!--  root element to be div  -->
    <xsl:template match="/">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    
<!--   ANYTHING DIFFERENT TO EPUB TO GO HERE -->
<!--    Oct 2014 - imported files branch from epub imported files (too many differences) -->
    

    
<!--    add ID to every paragraph - requirement for History Manifesto-->
    
    <xsl:template
        match="cbml:p [not(parent::cbml:commentary-note or parent::cbml:collation-note or parent::cbml:fn 
        or parent::cbml:speech or parent::cbml:caption/parent::cbml:fig or parent::cbml:caption/parent::cbml:fig-group or (parent::cbml:disp-quote and cbml:playtext) )]
        |cbml:dict-p">
        <xsl:variable name="id" select="generate-id()"/>
        <xsl:choose>
            <xsl:when
                test="*[contains($block-elements, local-name(.)) and not(following-sibling::node() or preceding-sibling::node())] and not(preceding-sibling::*[1][self::cbml:label and not(parent::cbml:answer)])">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                 <a name="{$id}"/>
                 <p class="paragraph">
                     <xsl:apply-templates select="." mode="id"/>
                     <xsl:if test="not(@id)"><xsl:attribute name="id" select="$id"/></xsl:if>
                     <xsl:apply-templates/>
                     <xsl:if
                         test="parent::cbml:disp-quote and following-sibling::*[1][self::cbml:attrib[not(text()) and cbml:xref[not(following-sibling::* or preceding-sibling::*)]]]">
                         <xsl:apply-templates select="following-sibling::cbml:attrib/*"/>
                     </xsl:if>
                 </p>
                <xsl:if test="parent::cbml:entry[not(following-sibling::cbml:entry)] and not(following-sibling::cbml:p)">
                    <xsl:apply-templates
                        select="following::cbml:row[1]/preceding-sibling::node()[1][self::processing-instruction('new-page')]"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
<!--    content may be chunked by section  -->
    
    <xsl:template match="cbml:chapter" mode="sections">
        <div class="chapter-panel">
            <xsl:apply-templates select="node()[not(preceding-sibling::cbml:title or preceding-sibling::cbml:label)]"/>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="cbml:author-group"/>
       </div>
        <div class="info-panel">
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label or self::cbml:author-group or following-sibling::cbml:title or following-sibling::cbml:label
                or self::cbml:body or self::cbml:back)]"/>
            <xsl:apply-templates select="cbml:body" mode="sections"/>
            <xsl:apply-templates select="cbml:alt-title[@type='copyright-line']" mode="do-copyright"/>
        </div>
    </xsl:template>
    
<!--    the chapter chunk to contain all text before the sections (Socrates requiement)-->
    <xsl:template match="cbml:body" mode="sections">
        <xsl:choose>
            <xsl:when test="$chunk-type='med-sec'">
                <xsl:apply-templates select="node()[not(preceding-sibling::cbml:sec)]"/>
<!--                footnotes for each sections to be pulled in and given the heading 'Footnotes'-->
                <xsl:if test="parent::*/descendant::cbml:xref[not(ancestor::cbml:sec[preceding-sibling::cbml:sec])][@href=following::cbml:fn[not(ancestor::cbml:table-wrap)]/@id]">
                    <div class="fn-group">
                        <h4>Footnotes</h4>
                        <xsl:for-each select="parent::*/descendant::cbml:xref[not(ancestor::cbml:sec[preceding-sibling::cbml:sec] or ancestor::cbml:back)]">
                            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
                            <xsl:apply-templates select="following::cbml:fn[@id=$link-att and not(ancestor::cbml:table-wrap)] "/> 
                        </xsl:for-each>
                   </div>     
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="node()[not(self::cbml:sec or preceding-sibling::cbml:sec)]"/>
                <xsl:if test="parent::*/descendant::cbml:xref[not(ancestor::cbml:sec)][@href=following::cbml:fn[not(ancestor::cbml:table-wrap)]/@id]">
                    <div class="fn-group">
                        <h4>Footnotes</h4>
                        <xsl:for-each select="parent::*/descendant::cbml:xref[not(ancestor::cbml:sec)]">
                            <xsl:variable name="link-att" select="@*[local-name()='href']"/>
                            <xsl:apply-templates select="following::cbml:fn[@id=$link-att and not(ancestor::cbml:table-wrap)]"/>
                        </xsl:for-each>
                    </div>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <!--    where content is chunked by section the footnotes for each section to be pulled into the section (Socrates requiement)-->
    <xsl:template match="cbml:sec | cbml:ref-sec" mode="sections">
        <div class="chapter-panel">
            <xsl:apply-templates select="ancestor::cbml:chapter" mode="sec-heading"/>
            <xsl:apply-templates select="node()[not(preceding-sibling::cbml:title or preceding-sibling::cbml:label)]"/>
            <div class="clear"></div>
        </div>
        <div class="info-panel">
           <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label  or following-sibling::cbml:title or following-sibling::cbml:label)]"/>
            
            <xsl:if test="descendant::cbml:xref[@href=following::cbml:fn[not(ancestor::cbml:table-wrap)]/@id]">
                <h4>Footnotes</h4>
            <xsl:for-each select="descendant::cbml:xref">
                <xsl:variable name="link-att" select="@*[local-name()='href']"/>
                <xsl:apply-templates select="following::cbml:fn[@id=$link-att and not(ancestor::cbml:table-wrap)] | preceding::cbml:fn[@id=$link-att and not(ancestor::cbml:table-wrap)]"/> 
            </xsl:for-each>
            </xsl:if>
            <xsl:apply-templates select="ancestor::cbml:chapter/cbml:alt-title[@type='copyright-line']" mode="do-copyright"/>
        </div>
    </xsl:template>
    
<!--    each section chunk to begin with the chapter heading-->
    <xsl:template match="cbml:chapter" mode="sec-heading">
        <h2 class="chapter subchunk">
            <xsl:apply-templates select="cbml:label" mode="heading"/>
            <xsl:if test="cbml:title  and cbml:label"><xsl:text> </xsl:text></xsl:if>
            <xsl:apply-templates select="cbml:title" mode="heading"/>  
        </h2>
        <xsl:apply-templates select="cbml:subtitle" mode="main-heading"/>
        <xsl:apply-templates select="cbml:author-group"/>
    </xsl:template>
    
    <!--    chapter subtitle to be h3 (CBUAT-444)-->
    <xsl:template match="cbml:subtitle" mode="main-heading">
        <h3><xsl:apply-templates/></h3>
    </xsl:template>
    

    
    <!--small-caps not pulled through?-->
    <xsl:template match="cbml:emphasis[@type='sc']">
        <xsl:apply-templates select="descendant::cbml:target"/>
        <small>
            <xsl:value-of select="upper-case(.)"/>
        </small>
    </xsl:template>
    
<!--    hr before copyright statement-->
    <xsl:template match="cbml:alt-title[@type='copyright-line']" mode="do-copyright">
        <div class="copyright-line">
            <hr/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
   
<!--   figures to include a link to the hi-res version-->
    <xsl:template match="cbml:fig/cbml:mime" mode="image">
        <xsl:variable name="link" select="@*[local-name()='href']" as="xs:string"/>
        <xsl:variable name="size-lookup" select="document('report.xml', .)"/>
        <xsl:variable name="img-file" select="concat($link, '.jpeg')"/>
        <xsl:variable name="hi-res" select="concat($link, 'hi-res.jpeg')"/>
        <xsl:variable name="small">
            <xsl:if test="$size-lookup//file[contains(@name,$img-file)]/dimensions[number(width)&lt; 520]">yes</xsl:if>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="parent::cbml:fig[not(parent::cbml:fig-group)] and $manifest-file//file[contains(@filename, $hi-res)]">
                <a class="lightbox"  href="{cup:hi-resImageFilename($hi-res)}" title="{concat(string(parent::cbml:fig/cbml:label), ': ',string(parent::cbml:fig/cbml:caption))}">
                    <img src="{cup:imageFilename($link)}">
                        <xsl:attribute name="alt" select="parent::cbml:fig/cbml:label"/>
                    </img>
                </a>
            </xsl:when>
            <xsl:when test="parent::cbml:fig/parent::cbml:fig-group and $manifest-file//file[contains(@filename, $hi-res)]">
                <a class="lightbox"  href="{cup:hi-resImageFilename($hi-res)}" title="{concat(string(parent::cbml:fig/cbml:label), ': ',string(parent::cbml:fig/parent::cbml:fig-group/cbml:label),string(parent::cbml:fig/parent::cbml:fig-group/cbml:caption))}">
                    <img src="{cup:imageFilename($link)}">
                        <xsl:attribute name="alt" select="parent::cbml:fig/cbml:label"/>
                    </img>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <img src="{cup:imageFilename($link)}">
                    <xsl:attribute name="alt" select="parent::cbml:fig/cbml:label"/>
                </img>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
<!--    do not add br tag before caption text-->  
    <xsl:template match="cbml:fig/cbml:caption |cbml:fig-group/cbml:caption">
        <xsl:apply-templates select="node()[not(self::cbml:attrib)]"/>
    </xsl:template>
    
<!--    add link to image-->
    <xsl:template match="cbml:table-wrap[cbml:table/@alt-graphic] | cbml:table-wrap-group[cbml:table/@alt-graphic] ">
        <xsl:variable name="link" as="xs:string">
            <xsl:choose>
                <xsl:when test="contains(cbml:table/@alt-graphic, ' ')">
                    <xsl:value-of select="substring-before(cbml:table/@alt-graphic, ' ')"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="string(cbml:table/@alt-graphic)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="table-wrap">
            <!--<xsl:apply-templates select="." mode="heading"/>--> 
<!--            add a link to image if table has more than 3 columns -->
            <!--the link to be within the heading div-->
            <xsl:choose>
                <xsl:when test="descendant::cbml:tgroup[number(@cols) &gt; 3]">
                    <div class="table-wrap">
                        <xsl:copy-of select="@id"/>
                        <xsl:apply-templates select="cbml:label | cbml:title | cbml:caption" mode="heading"/>
                        <xsl:text> </xsl:text>
                        <span class="xref">
                            <a class="lightbox"  href="{cup:imageFilename($link)}" title="{concat(string(cbml:label), ': ', string(cbml:title))}">
                                <xsl:text>expand</xsl:text>
                            </a>
                        </span>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="." mode="heading"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>
    
<!--  tables to have stripes (Silverberg requirement CBUAT 563 -->
    <xsl:template match="cbml:row">
        <xsl:variable name="style">
            <xsl:if test="@rowsep='1'">border-bottom </xsl:if>
            <xsl:if test="@valign"><xsl:value-of select="@valign"/> </xsl:if>
            <xsl:if test="parent::cbml:thead and not(following-sibling::cbml:row)">border-bottom </xsl:if>
        </xsl:variable>
        <xsl:variable name="odd-even">
            <xsl:choose>
                <xsl:when test="count(preceding-sibling::cbml:row) mod 2 = 1">even</xsl:when>
                <xsl:otherwise>odd</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="cbml:entry or cbml:sidehead">
                <tr>
                    <!--<xsl:if test="$style!=''">-->
                        <xsl:attribute name="class" select="concat($style, ' ', $odd-even)"/>
                    <!--</xsl:if>-->
                    <xsl:choose>
                        <xsl:when test="$style!=''">
                            <xsl:attribute name="class" select="concat($style, ' ', $odd-even)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class" select="$odd-even"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="*"/>
                </tr>
            </xsl:when>
            <xsl:otherwise>
                <tr>
                    <td colspan="{ancestor::cbml:tgroup/@cols}"></td>
                </tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
<!--    footnotes to have heading 'Footnotes'--> 
<!--    <xsl:template match="cbml:fn-group">
       <xsl:choose>
           <xsl:when test="ancestor::cbml:table-wrap">
               <xsl:apply-imports/>
           </xsl:when>
           <xsl:otherwise>
               <div class="fn-group">
                   <h4>Footnotes</h4>
                   <xsl:apply-templates/>
               </div>
           </xsl:otherwise>
       </xsl:choose> 
    </xsl:template>-->
    
<!--    tables to always be html tables-->
    
    <xsl:template match="cbml:table" priority="10">
                <xsl:apply-templates/>
    </xsl:template>
    
<!--    inline images do not reference an image size report as in epub-->
    <xsl:template match="cbml:inline-mime |  cbml:private-char" mode="image">
        <xsl:variable name="link" select="@*[local-name()='href']" as="xs:string"/>
        <xsl:variable name="fnm" select="cup:imageFilename($link)"/>
        <img src="{$fnm}" class="normal" alt=""/>       
    </xsl:template>
  

<!--all items to have header panel  so no applying templates mode heading-->

<xsl:template match="cbml:prelims/* | cbml:endmatter/*">
    <div class="{local-name()}">
        <xsl:copy-of select="@id"/>
        <xsl:apply-imports/>
    </div>
</xsl:template>
    
    <xsl:template match="cbml:toc | cbml:toc-feature | cbml:chapter-toc | cbml:part-toc | cbml:features" priority="10">
        <xsl:apply-templates select="node()[not(self::cbml:fn-group)]" mode="toc"/>
        <xsl:apply-templates select="cbml:fn-group"/>
    </xsl:template>

<!--footnotes to be in the same section as xref-->
    <xsl:template match="cbml:xref">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:variable name="target-elem" select="/cbml:book//*[@id=$link-att][1]"/>
        <span class="xref">
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates select="." mode="do-backlink"/>
            <a class="nounder">
                <xsl:choose>
                    <xsl:when test="local-name($target-elem)='fn'">
                        <xsl:attribute name="href" select="concat('#' ,$link-att)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/>
                    </xsl:otherwise>
                </xsl:choose>
                
                <xsl:choose>
                    <!--2014-01-08: span.added inserted for generated text as safety net for dead-link removal (see identity.xsl)-->
                    <xsl:when test="string(.)='' and key('element-by-id',$link-att) [local-name(.) = 'collation-note']">
                        <span class="added">†</span>
                    </xsl:when>
                    <xsl:when test="string(.)='' and not(*)">
                        <span class="added">*</span>
                    </xsl:when>
                </xsl:choose>
                <xsl:apply-templates select="* | text()"/>
            </a>
            <xsl:apply-templates select="processing-instruction()"/>
        </span>
    </xsl:template>
    

<!--override template in blocks which is specific to the epub build and requires image data-->
    <xsl:template match="cbml:disp-formula | cbml:chem-struct">       
        <xsl:apply-templates select="." mode="in-para-start"/>
        <xsl:apply-templates select="processing-instruction()[not(preceding-sibling::*)]"/>
        <xsl:apply-templates select="cbml:title" mode="heading"/>
        <xsl:choose>
            <xsl:when test="cbml:label">
                <dl>
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>
                    
                    <dt>
                        <xsl:apply-templates select="cbml:mime|m:math|cbml:p"/>
                    </dt>
                    <dd>
                        <xsl:apply-templates select="cbml:label" mode="class"/>
                        <xsl:apply-templates select="cbml:label" mode="do-label"/>
                    </dd>
                    
                </dl>
            </xsl:when>
            <xsl:otherwise>
                <div>
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>
                    <xsl:apply-templates select="cbml:mime|m:math|cbml:p"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="processing-instruction()[not(following-sibling::*)]"/>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>

    <!--override template in blocks which is specific to the epub build and requires image data-->
    <xsl:template match="m:math">
        <xsl:choose>
            <xsl:when test="parent::cbml:disp-formula or parent::cbml:p/parent::cbml:entry or parent::cbml:chem-struct">
                <img src="{cup:imageFilename(@altimg)}" alt="{string(.)}"/>
                <xsl:if test="following-sibling::m:math">
                    <br/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="fnm" select="cup:imageFilename(@altimg)"/>
                <span>
                    <xsl:attribute name="class">inline-math</xsl:attribute>
                    <img src="{cup:imageFilename(@altimg)}" alt="{string(.)}">
                        <xsl:attribute name="class">normal</xsl:attribute>
                    </img>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <!--   fig-group to be dealt with as per Silverberg spec as div with no class and the gifs within it as li -->
    
    <xsl:template match="cbml:fig-group">
        <xsl:apply-templates select="." mode="in-para-start"/>
        <div class="fig">
            <xsl:apply-templates select="." mode="id"/>
            <ul>
                <xsl:apply-templates select="cbml:fig|processing-instruction()"/>
            </ul>
            <div class="clear"/> 
            <xsl:apply-templates select="*[not(self::cbml:fig)]"/>
        </div>
        <xsl:apply-templates select="." mode="in-para-end"/>
    </xsl:template>
    
    <xsl:template match="cbml:fig-group/cbml:fig">
        <li class="sub-fig">
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates/> 
        </li>
    </xsl:template>
    
    <xsl:template match="cbml:fig-group/cbml:fig/cbml:label" priority="5">
        <p>
            <xsl:apply-templates/> 
        </p>
    </xsl:template>
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

<!--   PRESCRIBER'S GUIDES -->
<!--  root element of prescriber's guides to be sect within drug so no drug element  -->
    
    <xsl:template match="cbml:drug">
                 <div class="drug_icon">
                     <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                            <!-- use function to get image path -->
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                     </xsl:for-each> 
                       <hr/>  
                 </div>     
                <div class="drug">

                  
                    <div class="drugheading">
                        <xsl:value-of select="cbml:title"/>
                    </div>
                    <xsl:apply-templates select="*"/>
                </div>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:title">
        <xsl:value-of select="."/>                              
    </xsl:template>  
    
<!--  new file for all except therapeutics, which goes in the main file for the drug  -->
    <xsl:template match="cbml:sideEffects | cbml:dosing | cbml:specialPopulations | cbml:pharmacology | 
                        cbml:psychopharmacology |cbml:pharmacokinetics |cbml:interaction">
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),local-name(),'.html' )"/>

            <div class="drug">
<!--                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>-->
                <div>
                    <xsl:attribute name="class">
                        <xsl:variable name="class-stub">
                            <xsl:choose>
                                <xsl:when test="local-name()='sideEffects'">effects</xsl:when>
                                <xsl:when test="local-name()='dosing'">dosing-use</xsl:when>
                                <xsl:when test="local-name()='specialPopulations'">special-pop</xsl:when>
                                <xsl:when test="local-name()='pharmacology'">art-of-pharma</xsl:when>
                                <xsl:when test="local-name()='psychopharmacology'">psycho</xsl:when>
                                <xsl:when test="local-name()='pharmacokinetics'">pharma</xsl:when>
                                <xsl:when test="local-name()='interaction'">interact</xsl:when>
                                <xsl:when test="local-name()='ref-sec'">ref</xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:value-of select="concat('topicheading ', $class-stub)"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div> 
            </div> 
        
    </xsl:template>
    
    <xsl:template match="cbml:drug/cbml:ref-sec">
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),local-name(),'.html' )"/>

<!--        <div>
                <div>
                    <xsl:attribute name="class">
                        <xsl:variable name="class-stub">ref</xsl:variable>
                        <xsl:value-of select="concat('topicheading ', $class-stub)"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:apply-templates/>
                </div> 
            </div> -->
			<div class="drug">
				<h3 class="ref-sec1">
					<xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>
				</h3> 
				<xsl:apply-templates/>
			</div>
        
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics">
<!--                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>-->
                <div class="topicheading thera">
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div>             
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics/cbml:title | cbml:pharmacokinetics/cbml:title | cbml:interaction/cbml:title | cbml:sideEffects/cbml:title | cbml:dosing/cbml:title |
        cbml:specialPopulations/cbml:title | cbml:pharmacology/cbml:title  | cbml:psychopharmacology/cbml:title" mode="heading">
            <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template name="doSection">
        <xsl:for-each select="cbml:drug-sec">
            <div class="sectionheading" id="{@id}">                    
                <xsl:apply-templates select="cbml:icon | cbml:label | cbml:title" mode="heading"/>                              
            </div>            
            <xsl:apply-templates select="cbml:p|cbml:sec"/> <!-- add sec for subsections -->
        </xsl:for-each> 
        <xsl:apply-templates select="cbml:citation"/>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:title | cbml:drug-sec/cbml:label  | cbml:drug-sec/cbml:icon" mode="heading">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:sec/cbml:sec">
        <div class="Chead"><xsl:apply-templates select="cbml:title"/></div>
        <xsl:apply-templates select="*[not(self::cbml:title)]"/>
    </xsl:template>

    <xsl:template match="cbml:drug//cbml:icon/cbml:inline-mime[not(ancestor::cbml:ref-sec)]">
            <xsl:apply-templates select="." mode="image"/> 
    </xsl:template>
    
    <xsl:template match="cbml:drug//cbml:ref-sec//cbml:icon/cbml:inline-mime">
        <span class="icon"><xsl:apply-templates select="." mode="image"/></span> 
    </xsl:template>

</xsl:stylesheet>
