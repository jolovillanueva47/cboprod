<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml xlink xs xd m"
    version="2.0">
    
    <!-- This shares imported files with cbml2epub, so any changes specific to this build go in this file
    the functions file is not shared with cbml2epub so beware extra functions needed by the imported files
    -->
   
     <xsl:import href="main.xsl"/> 
    
    <xsl:output method="html" indent="yes"/>
    

<!--  pass in image-path as a parameter  -->
    <xsl:param name="image-path"/>
    <xsl:param name="css-path" select="'css/'"/>
    <xsl:param name="filename"/>
	<xsl:param name="filepath"/>
    
    <!-- ============ HTML for CBO ================-->
  
    <xsl:key name="element-by-id" match="*" use="@id"/>
    <xsl:key name="element-by-href" match="*" use="@href"/>
    
    <!-- set isbn in build file -->
    <xsl:param name="isbn"/>
   
    <xsl:variable name="book-title" select="cbml:book/cbml:prelims/cbml:title-info/cbml:title"/>
    <xsl:variable name="extra-chunk"/>

   
<!--   one html file per chunk -->
<!--    <xsl:template match="/">
            <html  xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="{concat($css-path, 'cbo.css')}" rel="stylesheet" type="text/css"></link> 
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                       <xsl:apply-templates/>                     
                </body>
            </html>
    </xsl:template>-->
    
<!--  root element to be div  -->
    <xsl:template match="/">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    
<!--   ANYTHING DIFFERENT TO EPUB TO GO HERE -->
    
    
    
<!--   PRESCRIBER'S GUIDES -->
<!--  root element of prescriber's guides to be sect within drug so no drug element  -->
    
    <xsl:template match="cbml:drug">
                 <div class="drug_icon">
                     <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                            <!-- use function to get image path -->
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                     </xsl:for-each> 
                       <hr/>  
                 </div>     
                <div class="drug">

                  
                    <div class="drugheading">
                        <xsl:value-of select="cbml:title"/>
                    </div>
                    <xsl:apply-templates select="*"/>
                </div>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:title">
        <xsl:value-of select="."/>                              
    </xsl:template>  
    
<!--  new file for all except therapeutics, which goes in the main file for the drug  -->
    <xsl:template match="cbml:sideEffects | cbml:dosing | cbml:specialPopulations | cbml:pharmacology | 
                        cbml:psychopharmacology |cbml:pharmacokinetics |cbml:interaction">
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),local-name(),'.html' )"/>
        <xsl:result-document method="html" encoding="utf-8" indent="yes" href="{concat($filepath, $fnm)}">
            <div class="drug">
<!--                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>-->
                <div>
                    <xsl:attribute name="class">
                        <xsl:variable name="class-stub">
                            <xsl:choose>
                                <xsl:when test="local-name()='sideEffects'">effects</xsl:when>
                                <xsl:when test="local-name()='dosing'">dosing-use</xsl:when>
                                <xsl:when test="local-name()='specialPopulations'">special-pop</xsl:when>
                                <xsl:when test="local-name()='pharmacology'">art-of-pharma</xsl:when>
                                <xsl:when test="local-name()='psychopharmacology'">psycho</xsl:when>
                                <xsl:when test="local-name()='pharmacokinetics'">pharma</xsl:when>
                                <xsl:when test="local-name()='interaction'">interact</xsl:when>
                                <xsl:when test="local-name()='ref-sec'">ref</xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:value-of select="concat('topicheading ', $class-stub)"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div> 
            </div> 
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="cbml:drug/cbml:ref-sec">
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),local-name(),'.html' )"/>
        <xsl:result-document method="html" encoding="utf-8" indent="yes" href="{concat($filepath, $fnm)}">
<!--        <div>
                <div>
                    <xsl:attribute name="class">
                        <xsl:variable name="class-stub">ref</xsl:variable>
                        <xsl:value-of select="concat('topicheading ', $class-stub)"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:apply-templates/>
                </div> 
            </div> -->
			<h3 class="ref-sec1">
                <div>
                    <xsl:attribute name="class">
                        <xsl:variable name="class-stub">ref</xsl:variable>
                        <xsl:value-of select="concat('topicheading ', $class-stub)"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>
            </h3> 
            <xsl:apply-templates/>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics">
<!--                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>-->
                <div class="topicheading thera">
                    <xsl:apply-templates select="cbml:title | cbml:icon | cbml:label" mode="heading"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div>             
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics/cbml:title | cbml:pharmacokinetics/cbml:title | cbml:interaction/cbml:title | cbml:sideEffects/cbml:title | cbml:dosing/cbml:title |
        cbml:specialPopulations/cbml:title | cbml:pharmacology/cbml:title  | cbml:psychopharmacology/cbml:title" mode="heading">
            <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template name="doSection">
        <xsl:for-each select="cbml:drug-sec">
            <div class="sectionheading" id="{@id}">                    
                <xsl:apply-templates select="cbml:icon | cbml:label | cbml:title" mode="heading"/>                              
            </div>            
            <xsl:apply-templates select="cbml:p|cbml:sec"/> <!-- add sec for subsections -->
        </xsl:for-each> 
        <xsl:apply-templates select="cbml:citation"/>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:title | cbml:drug-sec/cbml:label  | cbml:drug-sec/cbml:icon" mode="heading">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:sec/cbml:sec">
        <div class="Chead"><xsl:apply-templates select="cbml:title"/></div>
        <xsl:apply-templates select="*[not(self::cbml:title)]"/>
    </xsl:template>

    <xsl:template match="cbml:drug//cbml:icon/cbml:inline-mime">
            <xsl:apply-templates select="." mode="image"/> 
    </xsl:template>

</xsl:stylesheet>
