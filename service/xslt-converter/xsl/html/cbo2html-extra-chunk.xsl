<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml xlink xs xd m"
    version="2.0">
    
   
     <xsl:import href="main.xsl"/> 
    
    <xsl:output method="html" indent="yes"/>
    

    
    <xsl:param name="image-path" select="'Images/'"/>
    <xsl:param name="css-path" select="'css/'"/>
    <xsl:param name="filename"/>
    

    
    <!-- ============ HTML for CBO ================-->
  
    <xsl:key name="element-by-id" match="*" use="@id"/>
    <xsl:key name="element-by-href" match="*" use="@href"/>
    
    <!-- set isbn in build file -->
    <xsl:param name="isbn"/>
   
    <xsl:variable name="book-title" select="cbml:book/cbml:prelims/cbml:title-info/cbml:title"/>
    <xsl:variable name="extra-chunk"/>

   
<!--   one html file per chunk -->
<!--    <xsl:template match="/">
            <html  xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="{concat($css-path, 'cbo.css')}" rel="stylesheet" type="text/css"></link> 
                    <title><xsl:value-of select="$book-title"/></title>
                </head>
                <body>
                       <xsl:apply-templates/>                     
                </body>
            </html>
    </xsl:template>-->
    
<!--  root element to be div  -->
    <xsl:template match="/">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    
<!--   ANYTHING DIFFERENT TO EPUB TO GO HERE -->
    
    
    
<!--   PRESCRIBER'S GUIDES -->
<!--  root element of prescriber's guides to be sect within drug so no drug element  -->
    
    <xsl:template match="cbml:drug">
                <div class="drug">
                    <div class="drugheading">
                        <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                            <!-- use function to get image path -->
                            <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                        </xsl:for-each>
                        <xsl:value-of select="cbml:title"/>
                        <hr/>           
                    </div>
                    <xsl:apply-templates select="*"/>
                </div>
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:title">
        <xsl:value-of select="."/>                               
    </xsl:template>  
    
<!--  new file for all except therapeutics, which goes in the main file for the drug  -->
    <xsl:template match="cbml:sideEffects | cbml:dosing | cbml:specialPopulations | cbml:pharmacology | 
                        cbml:psychopharmacology |cbml:pharmacokinetics |cbml:interaction | cbml:drug/cbml:ref-sec">
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),local-name(),'.html' )"/>

            <div>
                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <!-- use function to get image path -->
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>
                <div class="topicheading">
                    <xsl:apply-templates select="cbml:title"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div> 
            </div> 
        
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics">
            <div>
                <div class="drugheading">
                    <xsl:for-each select="//cbml:drug-sec/cbml:icon/cbml:inline-mime">  
                        <!-- use function to get image path -->
                        <a class="hide" href="#{../../@id}"><img src="{cup:imageFilename(@*[local-name()='href'])}" width="20" height="20" border="0"/></a><xsl:text>      </xsl:text>              
                    </xsl:for-each>
                    <xsl:value-of select="cbml:title"/>
                    <hr/>           
                </div>
                <div class="topicheading">
                    <xsl:apply-templates select="cbml:title"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:call-template name="doSection"/>
                </div> 
            </div> 
    </xsl:template>
    
    <xsl:template match="cbml:therapeutics/cbml:title | cbml:pharmacokinetics/cbml:title | cbml:interaction/cbml:title | cbml:sideEffects/cbml:title | cbml:dosing/cbml:title |
        cbml:specialPopulations/cbml:title | cbml:pharmacology/cbml:title  | cbml:psychopharmacology/cbml:title">
            <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template name="doSection">
        <xsl:for-each select="cbml:drug-sec">
            <div class="sectionheading" id="{@id}">                    
                <xsl:apply-templates select="cbml:label"/>
                <xsl:apply-templates select="cbml:title"/>                               
            </div>            
            <xsl:apply-templates select="cbml:p|cbml:sec"/> <!-- add sec for subsections -->
        </xsl:for-each>        
    </xsl:template>
    
    <xsl:template match="cbml:drug-sec/cbml:sec/cbml:sec">
        <div class="Chead"><xsl:apply-templates select="cbml:title"/></div>
        <xsl:apply-templates select="*[not(self::cbml:title)]"/>
    </xsl:template>
    
<!-- =========== Stahl EP to be chunked by section ==============   -->
    
    <xsl:template match="cbml:chapter">
        <div>
            <xsl:apply-templates/>
        </div>  
    </xsl:template>
    
    <xsl:template match="cbml:chapter/cbml:body/cbml:sec">
        <xsl:variable name="count" select="count(preceding-sibling::cbml:sec) + 1"/>
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),'_',$count,'.html' )"/>

            <div>
                <div class="chapterheading">
                    <xsl:value-of select="ancestor::cbml:chapter/cbml:title"/>
                    <hr/>           
                </div>
                <div class="topicheading">
                    <xsl:apply-templates select="cbml:title"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:apply-imports/>
                </div> 
            </div> 
        
    </xsl:template>
    
    <xsl:template match="cbml:chapter/cbml:back/*">
        <xsl:variable name="count" select="count(preceding-sibling::*) + count(ancestor::cbml:chapter/cbml:main/cbml:sec) + 1"/>
        <xsl:variable name="fnm" select="concat(substring-before($filename, '.xml'),$count,'.html' )"/>

            <div>
                <div class="chapterheading">
                    <xsl:value-of select="ancestor::cbml:chapter/cbml:title"/>
                    <hr/>           
                </div>
                <div class="topicheading">
                    <xsl:apply-templates select="cbml:title"/>            
                </div>            
                <div class="{local-name()}">
                    <xsl:apply-imports/>
                </div> 
            </div> 
        
    </xsl:template>
    
<!-- change href to match new chunking   -->
    <xsl:template match="cbml:xref">
        <xsl:variable name="href" select="substring-after(@href, '#')"/>
        <xsl:variable name="fnm" select="substring-before($filename, '.xml')"/>
        <xsl:variable name="num">
            <xsl:choose>
                <xsl:when test="ancestor::cbml:body[cbml:sec]">
                    <xsl:value-of select="count(ancestor::cbml:main/cbml:sec/preceding-sibling::cbml:sec) + 1"/>
                </xsl:when>
                <xsl:when test="ancestor::cbml:back">
                    <xsl:value-of select="count(ancestor::cbml:chapter/cbml:main/cbml:sec) + count(ancestor::cbml:back/*/preceding-sibling::*) + 1"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="link-num">
            <xsl:choose>
                <xsl:when test="substring-before(@href, '#')=''">
                    <xsl:for-each select="/*//*[@id=$href]">
                        <xsl:choose>
                            <xsl:when test="ancestor::cbml:body[cbml:sec]">
                                <xsl:value-of select="count(ancestor::cbml:main/cbml:sec/preceding-sibling::cbml:sec) + 1"/>
                            </xsl:when>
                            <xsl:when test="ancestor::cbml:back">
                                <xsl:value-of select="count(ancestor::cbml:chapter/cbml:main/cbml:sec) + count(ancestor::cbml:back/*/preceding-sibling::*) + 1"/>
                            </xsl:when>
                            <xsl:otherwise>1</xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="doc-fnm" select="substring-before(@href, '#')"/>
                    <xsl:variable name="doc" select="document($doc-fnm, .)"/>
                    <xsl:for-each select="$doc//*[@id=$href]">
                        <xsl:choose>
                            <xsl:when test="ancestor::cbml:body[cbml:sec]">
                                <xsl:value-of select="count(ancestor::cbml:main/cbml:sec/preceding-sibling::cbml:sec) + 1"/>
                            </xsl:when>
                            <xsl:when test="ancestor::cbml:back">
                                <xsl:value-of select="count(ancestor::cbml:chapter/cbml:main/cbml:sec) + count(ancestor::cbml:back/*/preceding-sibling::*) + 1"/>
                            </xsl:when>
                            <xsl:otherwise>1</xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>

        </xsl:variable>
        <xsl:variable name="new-link">
            <xsl:choose>
                <xsl:when test="substring-before(@href, '#')=''">
                    <xsl:choose>
                        <xsl:when test="$num=$link-num">
                            <xsl:value-of select="@href"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat($fnm,'_', $link-num, '.html#', $href)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="pref" select="substring-before(@href, '.')"/>
                    <xsl:value-of select="concat($pref, '_',$link-num, '.html#', $href)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <a href="{$new-link}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>

</xsl:stylesheet>
