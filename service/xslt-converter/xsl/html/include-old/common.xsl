<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
<!--  
paragraphs
author group
inlines
-->
    <!--  ================ CHAPTER ================  -->
    <!--no div-->
    <xsl:template match="cbml:chapter">
        <xsl:apply-templates select="node()[not(preceding-sibling::cbml:title or preceding-sibling::cbml:label)]"/>
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates select="cbml:author-group"/>
        <xsl:apply-templates select="cbml:alt-title[@type='copyright-line']" mode="do-copyright"/>
        <xsl:apply-templates select="node()[not(self::cbml:title or self::cbml:label or self::cbml:author-group or following-sibling::cbml:title or following-sibling::cbml:label)]"/>
    </xsl:template>
    
    <!--no div-->
    <xsl:template match="cbml:sec | cbml:ref-sec">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:label">
                <xsl:apply-templates select="." mode="heading"/>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="@id">
                    <a id="{@id}"/>
                </xsl:if>
                <br/>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 
    <!--no div-->
    <xsl:template match="cbml:references | cbml:index">
                <xsl:apply-templates select="." mode="heading"/>
                <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:index-group[cbml:p or preceding::cbml:toc//*[@href=current()/@id]]">
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <!--no element span-->
    <xsl:template match="cbml:given-names  | cbml:publisher-name | cbml:publisher-loc | cbml:name
        |cbml:year | cbml:startpage | cbml:article-title | cbml:chapter-title 
        |cbml:citation/cbml:author">
       <xsl:apply-templates/> 
    </xsl:template>
    
    <xsl:template match="cbml:p/cbml:citation | cbml:dict-p/cbml:citation">
            <xsl:apply-templates select="cbml:label" mode="do-label"/>
        <xsl:if test="key('element-by-href',@id)"><a id="{@id}"/></xsl:if>
            <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="cbml:citation/cbml:surname">
            <xsl:if test="preceding-sibling::cbml:given-names"><xsl:text> </xsl:text></xsl:if>
            <xsl:apply-templates/>
    </xsl:template>
    
<!--  adds id  -->
    <xsl:template match="cbml:xref">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <span class="xref">
            <xsl:copy-of select="@id"/>
            <xsl:if test="(not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)) and
                (key('element-by-id',$link-att) [local-name(.) = 'fn' or local-name(.) = 'commentary-note' or local-name(.) = 'collation-note']  or key('element-by-id',$link-att) [local-name(.)='citation' and cbml:label])">
                <xsl:if test="not(preceding::cbml:xref[not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)][@*[local-name()='href']=$link-att])">
                    <a id="{concat('rp', $link-att)}"></a>
                </xsl:if>
            </xsl:if>
            <a class="nounder">
                <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/>
                <xsl:choose>
                   <xsl:when test="string(.)='' and key('element-by-id',$link-att) [local-name(.) = 'collation-note']">†</xsl:when>
                    <xsl:when test="string(.)='' and not(*)">*</xsl:when> 
                </xsl:choose>
                <xsl:apply-templates select="* | text()"/>
            </a>
            <xsl:apply-templates select="processing-instruction()"/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:see-also[parent::cbml:dictionary-entry or parent::cbml:dict-p]">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <span class="dict-see">
            <xsl:copy-of select="@id"/>
            <a class="nounder">
                <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/>
                <xsl:apply-templates select="* | text()"/>
            </a>
            <xsl:apply-templates select="processing-instruction()"/>
        </span>
    </xsl:template>
    
    
    <xsl:template match="cbml:see[parent::cbml:dictionary-entry]">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:if test="not(preceding-sibling::cbml:see or preceding-sibling::cbml:see-also)">See </xsl:if>
        <span class="dict-see">
            <xsl:copy-of select="@id"/>
            <a class="nounder">
                <xsl:attribute name="href" select="cup:Link(. ,$link-att)"/>
                <xsl:apply-templates select="* | text()"/>
            </a>
            <xsl:apply-templates select="processing-instruction()"/>
        </span>
    </xsl:template>

    <!--========= PARAGRAPHS ============-->
    <xsl:template match="cbml:sim-p">
        <p> 
            <xsl:apply-templates select="." mode="class"/>
            <xsl:if test="preceding-sibling::*[position()=1 and self::cbml:label]">
                <xsl:apply-templates select="preceding-sibling::cbml:label/node()"/>
                <xsl:text> </xsl:text> 
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <xsl:template match="cbml:disclaimer/cbml:sim-p">
        <div>
            <xsl:attribute name="class" select="'p_disclaimer'"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="cbml:sim-p" mode="class">
           <xsl:if test="parent::*[self::cbml:disclaimer or self::cbml:bio]"><xsl:attribute name="class" select="'noindent'"/></xsl:if>       
    </xsl:template>

    <xsl:template match="cbml:p" mode="class">
            <xsl:choose>
                <xsl:when test="not(preceding-sibling::cbml:p) and not(parent::cbml:list-item or parent::cbml:fn)"><xsl:attribute name="class" select="'noindent'"/></xsl:when>
                <xsl:when test="preceding-sibling::cbml:p and (parent::cbml:def or parent::cbml:disp-quote)"><xsl:attribute name="class" select="'noindent'"/></xsl:when>
                <xsl:when test="not(preceding-sibling::cbml:p) and (parent::cbml:fn)"><xsl:attribute name="class" select="'noindent'"/></xsl:when>
                <xsl:when test="not(preceding-sibling::cbml:p) and (parent::cbml:list-item)"><xsl:attribute name="class" select="'noindent'"/></xsl:when>
                <xsl:when test="preceding-sibling::cbml:p and (parent::cbml:list-item or parent::cbml:fn)"><xsl:attribute name="class" select="'followon'"/></xsl:when>
            </xsl:choose> 
    </xsl:template>
    
    <xsl:template match="cbml:p [not(parent::cbml:commentary-note or parent::cbml:collation-note or parent::cbml:fn 
        or parent::cbml:speech or parent::cbml:caption/parent::cbml:fig or parent::cbml:caption/parent::cbml:fig-group or (parent::cbml:disp-quote and cbml:playtext) )]
                                    |cbml:dict-p">
        <xsl:choose>
            <xsl:when test="*[contains($block-elements, local-name(.)) and not(following-sibling::node() or preceding-sibling::node())] and not(preceding-sibling::*[1][self::cbml:label])">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <p>  
                    <xsl:apply-templates select="." mode="id"/>
                    <xsl:apply-templates select="." mode="class"/>
                    <xsl:apply-templates select="." mode="add-label"/>
                    <xsl:apply-templates select="node()[not(@position='float')]"/>
                    <xsl:if test="parent::cbml:disp-quote and following-sibling::*[1][self::cbml:attrib[not(text()) and cbml:xref[not(following-sibling::* or preceding-sibling::*)]]]">
                        <xsl:apply-templates select="following-sibling::cbml:attrib/*"/>              
                    </xsl:if>
                    <!--        this bit will never be called            -->
                    <xsl:if test="parent::cbml:caption[parent::cbml:fig or parent::cbml:fig-group] and not(following-sibling::cbml:p)">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="following-sibling::cbml:attrib/node()"/>
                    </xsl:if>
                </p>
                <xsl:apply-templates select="*[@position='float']"/>
                <!-- page anchor to go within entry paras-->
                <!--<xsl:if test="parent::cbml:disp-quote and following-sibling::cbml:attrib[not(text()) and cbml:xref]">
                    <xsl:apply-templates select="following-sibling::cbml:attrib/*"/>              
                </xsl:if> --> 
                <xsl:if test="parent::cbml:entry[not(following-sibling::cbml:entry)] and not(following-sibling::cbml:p)">
                    <xsl:apply-templates select="following::cbml:row[1]/preceding-sibling::node()[1][self::processing-instruction('new-page')]"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    
    <xsl:template match="cbml:p | cbml:dict-p" mode="add-label">
        <!--  move label to within p div  -->
        <xsl:if test="preceding-sibling::*[position()=1 and (self::cbml:label or self::cbml:number)] 
            and not (parent::cbml:sec or parent::cbml:disp-quote or parent::cbml:boxed-text or parent::cbml:app-sec or parent::cbml:exercise 
            or parent::cbml:statement[cbml:title] or parent::cbml:linguistics or parent::cbml:linguistics-item)">
            <span class="label"><xsl:apply-templates select="preceding-sibling::cbml:label/node() |preceding-sibling::cbml:number/node() "/></span>
            <xsl:text> </xsl:text> 
        </xsl:if>
        <xsl:if test="self::*[not(preceding-sibling::*) and  parent::cbml:caption[preceding-sibling::cbml:label and not(preceding-sibling::cbml:title)]]">
            <span class="label"><xsl:apply-templates select="parent::cbml:caption/preceding-sibling::cbml:label/node()"/></span>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="self::*[not(preceding-sibling::*) and  parent::cbml:instruction[preceding-sibling::*[1][self::cbml:label]]]">
            <span class="label"><xsl:apply-templates select="parent::cbml:instruction/preceding-sibling::cbml:label/node()"/></span>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:apply-templates select="cbml:label" mode="do-label"/>
    </xsl:template>
    
    <!-- ========== INLINE elements =============== -->
    
    <xsl:template match="cbml:marginal-number">
        <span class="marginal-number">[<xsl:apply-templates/>]</span>
    </xsl:template>
    
    <xsl:template match="cbml:break">
        <xsl:choose>
            <xsl:when test="@symbol">
                <div class="break"><xsl:value-of select="@symbol"/></div>
            </xsl:when>
            <xsl:otherwise><br/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
   <xsl:template match="cbml:alt-title" mode="#all">
       <xsl:apply-templates select="cbml:target"/>
   </xsl:template>
    
    <xsl:template match="cbml:alt-title[@type='copyright-line']" mode="do-copyright">
        <div class="copyright-line"><xsl:apply-templates/></div>
    </xsl:template>
    
    <xsl:template match="cbml:emphasis">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="@type"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:emphasis[@type='sc']">
        <xsl:apply-templates select="descendant::cbml:target"/>
        <small>
        <xsl:value-of select="upper-case(.)"/>
        </small>
    </xsl:template>
    
    <xsl:template match="cbml:sup">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    
    <xsl:template match="cbml:sub">
        <sub>
            <xsl:apply-templates/>
        </sub>
    </xsl:template>
    
    <xsl:template match="cbml:uri">
        <a>
            <xsl:attribute name="href"><xsl:value-of select="@web-address"/></xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <xsl:template match="cbml:extra-space">
        <xsl:text>&#x00A0;&#x00A0;&#x00A0;</xsl:text>
    </xsl:template>
    
<!--=============  AUTHOR GROUP ================= -->
    
    <xsl:template match="cbml:surname">
        <span>
            <xsl:apply-templates select="." mode="class"/> 
            <xsl:if test="preceding-sibling::cbml:given-names"><xsl:text> </xsl:text></xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:author-group">
        <xsl:apply-templates select="." mode="heading"/>
        <xsl:if test="@type='editors'">       
            <div class="edited-by">Edited by </div>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="cbml:author[cbml:address or cbml:date or cbml:bio[not(ancestor::cbml:title-info)] or cbml:role]">
                    <xsl:for-each select="cbml:author">
                        <xsl:apply-templates select="." mode="type"/>
                        <xsl:choose>
                            <xsl:when test="ancestor::cbml:title-info">
                                <div class="author"><xsl:apply-templates select="cbml:name"/></div>
                            </xsl:when>
                            <xsl:when test="ancestor::cbml:contributors and cbml:bio"></xsl:when>
                            <xsl:otherwise>
                                 <span class="author"><xsl:apply-templates select="cbml:name"/></span>
                            <xsl:choose>
                                    <xsl:when test="following-sibling::cbml:author[following-sibling::cbml:author] and not(cbml:bio or cbml:address or cbml:role or cbml:fig)">, </xsl:when>
                                    <xsl:when test="following-sibling::cbml:author[not(following-sibling::cbml:author)] and not(cbml:bio or cbml:address or cbml:role or cbml:fig)"> and </xsl:when>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:apply-templates select="*[not(self::cbml:name or self::cbml:label or self::cbml:title or self::cbml:bio[ancestor::cbml:title-info]) ] | processing-instruction()"/>
                    </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                   <div class="author">
                    <xsl:for-each select="cbml:author">
                        <xsl:apply-templates select="." mode="type"/>
                            <xsl:apply-templates select="cbml:name|cbml:target |cbml:xref | processing-instruction()"/>
                        <xsl:value-of select="cup:Punctuation(.)"/>
                    </xsl:for-each>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="*[not(self::cbml:author or self::cbml:label or self::cbml:title or self::cbml:bio[ancestor::cbml:title-info]) ]"/>
    </xsl:template>
    
    <xsl:template match="cbml:author" mode="type">
        <xsl:variable name="type" select="@type"/>
        <xsl:if test="not(contains(parent::cbml:author-group/@type, $type))">
        <xsl:choose>
            <xsl:when test="@text"><xsl:value-of select="@text"/></xsl:when>
            <xsl:when test="@type = 'translator-and-editor'"><div class="edited-by">Translated and edited by</div></xsl:when>
            <xsl:when test="@type = 'illustrator'"><div class="edited-by">Illustrated by</div></xsl:when>
        <xsl:when test="@type = 'editor'"><div class="edited-by">Edited by</div></xsl:when>
                <xsl:when test="@type = 'contributor'"><div class="edited-by">With contributions by</div></xsl:when>
                <xsl:when test="@type = 'foreword-author'"><div class="edited-by">Foreword by</div></xsl:when>
                <xsl:when test="@type = 'translator'"><div class="edited-by">Translated by</div></xsl:when>
                <xsl:when test="@type = 'other'"><div class="edited-by"><xsl:value-of select="@text"/></div></xsl:when>
            </xsl:choose> 
        </xsl:if>
    </xsl:template>

    
<xsl:template match="cbml:author-group" mode="type">
        <xsl:variable name="type" select="@type"/>
        <xsl:if test="not(contains(parent::cbml:author-group/@type, $type))">
            <xsl:choose>
                <xsl:when test="@text"><xsl:value-of select="@text"/></xsl:when>
                <xsl:when test="@type = 'editors'"><div class="edited-by">Edited by</div></xsl:when>
                <xsl:when test="@type = 'contributors'"><div class="edited-by">With contributions by</div></xsl:when>
                <xsl:when test="@type = 'foreword-authors'"><div class="edited-by">Foreword by</div></xsl:when>
                <xsl:when test="@type = 'translators'"><div class="edited-by">Translated by</div></xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    
</xsl:stylesheet>
