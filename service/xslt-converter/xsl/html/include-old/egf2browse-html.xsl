<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:c="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup c xlink xs"
    version="2.0">
    
    <xsl:output method="xhtml" indent="yes" encoding="US-ASCII"/>
    
            <xsl:template match="/">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <link href="css/egf2.css" rel="stylesheet" type="text/css"/> 
                        <script src="css/openclose.js"/>
                        <title>European Garden Flora</title>
                    </head>
                    <body>
                        <img src="header.gif" alt="Garden Flora Online"/>
                       <table class="links" width="100%">
                            <tr>
                                <td><a class="link-bar" href="search.html"><img src="search-link.gif" alt="Search"/></a></td>
                                <td><img src="browse-link.gif" alt="Browse"/></td>
                                <td><a class="link-bar" href="quick-keys.html"><img src="quick-key-link.gif" alt="Search"/></a></td>
                                <td><a class="link-bar" href="keys.html"><img src="key-link.gif" alt="Search"/></a></td>
                                <td><a class="link-bar" href="illustrations.html"><img src="illustrations-link.gif" alt="Search"/></a></td>
                               
                            </tr>
                        </table>
                        <table class="instruct">
                            <tr>
                                <td><img src="closed.gif" alt="closed"/> click to drill down</td>
                                <td><img src="open.gif" alt="open"/> click to hide</td>
                                <td><img src="css/details.gif" alt="details"/> go to details</td>
                                <td><img src="css/key.gif" alt="closed"/> go to key</td>
                                <td><img src="css/quick-key.gif" alt="closed"/> go to quick key</td>
                            </tr>
                        </table>
                        <div class="instruct">hover over a name to show link icons</div>
                        <xsl:apply-templates select="c:book/c:main/c:EGF/c:class/c:family"/>
                    </body>
                </html>
            </xsl:template>
    
        <xsl:template match="c:family | c:genus | c:subgenus | c:species">
        <div class="{local-name()}">
            <!--<span class="type"><xsl:value-of select="local-name()"/></span>
            <span class="num"><xsl:value-of select="c:number"/></span>-->
            <!--<span class="title"><xsl:value-of select="c:name"/></span>-->
           <div class="info">             
<!--            <xsl:apply-templates select="." mode="details"/>
            <xsl:apply-templates select="." mode="simple-key"/>
           <xsl:apply-templates select="." mode="key"/>-->
           </div>
            <xsl:choose>
                <xsl:when test="c:genus or c:subgenus or c:species">
                    <div class="trigger" onmouseover="showInline('link{@id}')" onmouseout="hideInline('link{@id}')"  onClick="showBranch('{@id}');swapFolder('img{@id}')"><img width="10px" height="10px" id="img{@id}" src="closed.gif"/>
                        <span class="title"><xsl:value-of select="c:name"/></span>
                        <span class="links" id="link{@id}"  style="display:none"><img class="icon" src="css/key.gif" alt="key"/>
                        <img src="css/quick-key.gif" class="icon" alt="quick key"/>
                        <img src="css/details.gif" class="icon" alt="details"/></span>
                    </div>
                    <div class="branch" id="{@id}" style="display:none">
                    <xsl:apply-templates select="c:genus|c:subgenus |c:species"/>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <div class="species" onmouseover="showInline('link{@id}')" onmouseout="hideInline('link{@id}')" >
                    <span class="title"><xsl:value-of select="c:name"/></span>
                       <span class="links" id="link{@id}"  style="display:none"> <img class="icon" src="css/details.gif" alt="details"/></span>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
            

        </div>
        </xsl:template>
    
    <xsl:template match="*" mode="details">
        <span class="details"><a href="{@id}.html" target="_blank">Details</a></span>
        <xsl:result-document href="{@id}.html">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <link href="css/egf2.css" rel="stylesheet" type="text/css"/> 
                        <title><xsl:value-of select="c:name"/></title>
                    </head>
                    <body>
                        
                        <h1><xsl:apply-templates select="c:number |c:name"></xsl:apply-templates></h1>
                        <div class="main">
                        <xsl:apply-templates select="c:synonym |c:description | c:observations |
                            c:authority | c:author | c:literature | c:fig | 
                            c:distribution | c:hardiness | c:flowering | c:egf-illn | c:illustrations"/>
                        </div>
                    </body>
                </html>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="c:number | c:name | c:first/c:p | c:second/c:p">
        <span class="{local-name()}"><xsl:apply-templates/></span>
    </xsl:template>
    
    <xsl:template match="c:synonym |c:description | c:observations |
                            c:authority[not(parent::c:synonym)]  | c:author[not(parent::c:citation)] | c:literature | c:fig|
                            c:distribution | c:hardiness | c:flowering | c:egf-illn | c:illustrations">
        <dl>
            <dt><xsl:value-of select="local-name()"/></dt>
            <dd>  
            <xsl:apply-templates/>
            </dd>
        </dl>
    </xsl:template>
    
    <xsl:template match="*"><span class="{local-name()}"><xsl:apply-templates/></span></xsl:template>
    
    
    <xsl:template match="*" mode="key">
        <xsl:if test="c:key">
            <span class="key"><a href="{@id}key.html" target="_blank">Key</a></span>
            <xsl:result-document href="{@id}key.html">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <link href="css/egf2.css" rel="stylesheet" type="text/css"/> 
                        <title>Key: <xsl:value-of select="c:name"/></title>
                    </head>
                    <body>
                        <h1><xsl:apply-templates select="c:number |c:name"></xsl:apply-templates></h1>
                        <div class="main">
                        <xsl:apply-templates select="c:key"/>
                        </div>
                    </body>
                </html>
                </xsl:result-document>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="c:key | c:key-item |  c:second | c:p">
        <div class="{local-name()}">
            <xsl:copy-of select="@id"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="c:xref">
        <xsl:variable name="href" select="@href"></xsl:variable>
        <span class="xref">
        <a>
            <xsl:attribute name="href">
                <xsl:choose>
                    <xsl:when test="ancestor::c:key[descendant::c:key-item[@id=$href]]">
                        <xsl:value-of select="concat('#', $href)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($href, '.html')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
      </span>  
    </xsl:template>
        
    <xsl:template match="*" mode="simple-key">
        <xsl:if test="c:informal-key">
            <span class="key"><a href="{@id}simkey.html" target="_blank">Quick key</a></span>
            <xsl:result-document href="{@id}simkey.html">
                <html  xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <link href="css/egf2.css" rel="stylesheet" type="text/css"/> 
                        <title>Quick key: <xsl:value-of select="c:name"/></title>
                    </head>
                    <body>
                        <h1><xsl:apply-templates select="c:number |c:name"></xsl:apply-templates></h1>
                        <div class="main">
                        <xsl:apply-templates select="c:informal-key"/>
                        </div>
                    </body>
                </html>
                </xsl:result-document>
            </xsl:if>
    </xsl:template>


    
</xsl:stylesheet>