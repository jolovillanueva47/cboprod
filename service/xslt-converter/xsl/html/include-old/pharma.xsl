<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs xd m"
    version="2.0">
    

<!--    <xsl:template match="cbml:therapeutics | cbml:pharmacokinetics | cbml:interaction | cbml:sideEffects | cbml:dosing | cbml:specialPopulations | cbml:pharmacology">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>-->
    
    <xsl:template match="cbml:therapeutics/cbml:title | cbml:pharmacokinetics/cbml:title | cbml:interaction/cbml:title | cbml:sideEffects/cbml:title | cbml:dosing/cbml:title |
        cbml:specialPopulations/cbml:title | cbml:pharmacology/cbml:title" mode="heading">
        <xsl:variable name="drug" select="parent::*/parent::cbml:drug/cbml:title"/>
        <!--<h3><xsl:value-of select="$drug"/>: <xsl:apply-templates/></h3>-->
        <span> 
            <xsl:apply-templates select="." mode="class"/>
            <xsl:value-of select="$drug"/>
            <xsl:text>: </xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:icon">
    </xsl:template>
    
    <xsl:template match="cbml:icon" mode="heading">
        <xsl:apply-templates/>
        <xsl:text> </xsl:text>
    </xsl:template>
    
    <xsl:template match="cbml:icon/cbml:inline-mime">
        <span class="icon">
            <xsl:apply-templates select="." mode="id"/>  
            <xsl:apply-templates select="." mode="image"/> 
        </span>
    </xsl:template>
    

</xsl:stylesheet>
