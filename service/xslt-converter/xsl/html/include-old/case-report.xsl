<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml  xlink xs xd"
    version="2.0">
    
 <xsl:template match="cbml:kwd-group">
     <div class="kwd-group">
         <xsl:apply-templates/>
         </div>
 </xsl:template>
    
    <xsl:template match="cbml:kwd-group/cbml:kwd-group">
        <span class="kwd-group">
            <xsl:text> - </xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="cbml:keyword">
        <span class="keyword">
            <xsl:if test="preceding-sibling::*"> - </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
</xsl:stylesheet>
