<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="cup cbml xlink xs"
    version="2.0">
    
<xsl:template match="cbml:first | cbml:second">
    <div class="key-item-part">
        <xsl:apply-templates select="." mode="id"/>
        <xsl:apply-templates select="." mode="do-label"/>
        <xsl:apply-templates select="cbml:p/node()"/>
        <span class="key-xref"><xsl:apply-templates select="cbml:xref"/></span>
    </div>
</xsl:template>

     
</xsl:stylesheet>