<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    

<xsl:import href="default.xsl"/>
    
<!--  ======== DEFAULT HEADING TEMPLATE -does titles and labels ===========  -->
    
<!--   enclose label and title in a div -->
    <xsl:template match="*" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:number or cbml:bio-name or cbml:label[not(following-sibling::*[1][self::cbml:p])]">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:icon" mode="heading"/>
                    <xsl:apply-templates select="cbml:label | cbml:number" mode="heading"/>
                    <xsl:if test="(cbml:title or cbml:bio-name) and (cbml:label or cbml:number)"><xsl:text> </xsl:text></xsl:if>
                    <xsl:apply-templates select="cbml:title | cbml:bio-name" mode="heading"/>  
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/> 
                </xsl:element>
            </xsl:when> 
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!--  does label in heading  -->

    <xsl:template match="cbml:label | cbml:number" mode="heading">
        <span>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

<!-- does label when no title   -->
    <xsl:template match="cbml:label | cbml:number" mode="do-label">
        <span class="label">
            <xsl:apply-templates/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>
    
<!--does title in heading-->
    <xsl:template match="cbml:title |cbml:subtitle | cbml:bio-name" mode="heading">
        <span> 
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
<!--    <xsl:template match="cbml:half-title/cbml:subtitle" mode="heading">
        <span> 
            <xsl:apply-templates select="." mode="class"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates/>
        </span>
    </xsl:template>-->

    <!-- avoid repeat of heading elements -->
   
    <xsl:template match="cbml:title | cbml:label[not(parent::cbml:fig and not(following-sibling::cbml:caption))] |cbml:subtitle | cbml:number | cbml:bio-name"/>
    <xsl:template match="cbml:title | cbml:label[not(parent::cbml:fig and not(following-sibling::cbml:caption))] |cbml:subtitle" mode="toc"/>
    
<!--===============mode heading templates ==================-->
<!--    <xsl:template match="cbml:disp-quote/cbml:title" mode="heading">
        <div class="disp-quote_title">
            <xsl:apply-templates/>
        </div>
    </xsl:template>-->
    
<!--  captions include preceding label  -->
    <xsl:template match="cbml:caption[cbml:title]" mode="heading">
        <div class="caption">
            <xsl:apply-templates select="preceding-sibling::cbml:label" mode="do-label"/>
            <xsl:apply-templates select="cbml:title" mode="heading"/>
        </div>
    </xsl:template>
    
    <!--subtitles on new line-->
<!--    <xsl:template match="cbml:title-info/cbml:title | cbml:half-title/cbml:title |
        cbml:part/cbml:title| cbml:title-page/cbml:title " mode="heading">
        <div class="title">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="cbml:title-info/cbml:subtitle | cbml:half-title/cbml:subtitle |
        cbml:part/cbml:subtitle| cbml:title-page/cbml:subtitle " mode="heading">
        <div class="subtitle">
            <xsl:apply-templates/>
        </div>
    </xsl:template>-->
    
    <xsl:template match="cbml:example|cbml:question" mode="heading">
        <xsl:choose>
            <xsl:when test="cbml:title or cbml:subtitle">
                <xsl:element name="{cup:HeadingType(.)}">
                    <xsl:attribute name="class" select="cup:HeadingClass(.)"/>
                    <xsl:if test="@id">
                        <xsl:attribute name="id" select="@id"/>
                    </xsl:if>
                    <xsl:apply-templates select="cbml:label" mode="heading"/>
                    <xsl:if test="cbml:title and cbml:label"><xsl:text> </xsl:text></xsl:if>
                    <xsl:apply-templates select="cbml:title" mode="heading"/>  
                    <xsl:apply-templates select="cbml:subtitle" mode="heading"/> 
                </xsl:element>
            </xsl:when> 
            <xsl:otherwise>
                <xsl:if test="@id">
                    <xsl:attribute name="id" select="@id"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
