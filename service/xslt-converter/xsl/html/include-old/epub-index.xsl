<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    

<!-- 
index-list
index-item
see, see-also
see-under
-->
    
    <!-- ============= INDEX LIST =========== -->
    <xsl:template match="cbml:index-list" mode="#default toc">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates>
                <xsl:with-param name="level" select="0"/>
            </xsl:apply-templates>
        </div>
    </xsl:template>
    <xsl:template match="cbml:index-list/cbml:index-list">
        <div>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates>
                <xsl:with-param name="level" select="1"/>
            </xsl:apply-templates>
        </div>
    </xsl:template>
    
    
    <!-- =================INDEX ITEM =============== -->
    <!--  link on only first page in range  -->
    <xsl:template match="cbml:index-item">
        <xsl:param name="level"/>
        <xsl:variable name="class" select="concat('index', string($level) )"/>
        <div>
            <xsl:attribute name="class" select="$class"/>           
            <xsl:apply-templates select="cbml:index-entry"/>
            <xsl:for-each select="cbml:link[matches(. , '\S')]">
                
                <xsl:text> </xsl:text>
                <a>
                    <xsl:apply-templates select="." mode="class"/>
                    <xsl:attribute name="href" select="cup:Link(. ,@start)"/>
                    <xsl:choose>
                        <xsl:when test="@end and contains(. , '&#x2013;') and cbml:emphasis">
                            <span class="{cbml:emphasis[1]/@type}">
                            <xsl:value-of select="substring-before(. , '&#x2013;')"/>
                            </span>
                        </xsl:when>
                        <xsl:when test="@end and contains(. , '&#x2013;')">
                            <xsl:value-of select="substring-before(. , '&#x2013;')"/>
                        </xsl:when>
                        <xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
                    </xsl:choose>
<!--    link to contain entire page range           -->
                <xsl:choose>
                    <xsl:when test="@end and contains(. , '&#x2013;') and cbml:emphasis"><span class="{cbml:emphasis[1]/@type}">&#x2013;<xsl:value-of select="substring-after(. , '&#x2013;')"/></span></xsl:when>
                    <xsl:when test="@end and contains(. , '&#x2013;') and not(cbml:emphasis)">&#x2013;<xsl:value-of select="substring-after(. , '&#x2013;')"/></xsl:when>
                </xsl:choose>
                </a>
                <!--<xsl:if test="@end and contains(. , '&#x2013;')">&#x2013;<xsl:value-of select="substring-after(. , '&#x2013;')"/></xsl:if>-->
                <xsl:if test="following-sibling::cbml:link">
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:apply-templates select="cbml:see | cbml:ext-link | cbml:xref">
                <xsl:with-param name="level" select="number($level)+1"/>
            </xsl:apply-templates>
        </div>  
        <xsl:apply-templates select="cbml:index-item |cbml:see-also | cbml:see-under|processing-instruction()">
            <xsl:with-param name="level" select="number($level)+1"/>
        </xsl:apply-templates>  
    </xsl:template>
    
    
    <!--  ============== SEE-ALSO =================  -->
    <!-- combine see-also and see-under to avoid duplication of generated text 'see also'   -->
    <xsl:template match="cbml:see-also[not(parent::cbml:dict-p or parent::cbml:dictionary-entry)] | cbml:see-under">
        <xsl:param name="level"/>
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <div>
            <xsl:attribute name="class" select="concat('see-also', string($level) )"/>
            <xsl:choose>
                <xsl:when test="ends-with(preceding-sibling::cbml:index-entry, '.')"><span class="italic"> See also </span></xsl:when>
                <xsl:otherwise><span class="italic"> see also </span></xsl:otherwise>
            </xsl:choose>
            
            <xsl:choose>
                <xsl:when test="self::cbml:see-also">
                    <a>
                        <xsl:attribute name="href" select="cup:seeLink(. ,$link-att )"/>
                        <xsl:apply-templates/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose> 
            <xsl:for-each select="following-sibling::cbml:see-also | following-sibling::cbml:see-under ">
                <xsl:variable name="link-att2" select="@*[local-name()='href']"/>
                <xsl:text>; </xsl:text>
                <xsl:choose>
                    <xsl:when test="self::cbml:see-also">
                        <a>
                            <xsl:attribute name="href" select="cup:seeLink(. ,$link-att2 )"/>
                            <xsl:apply-templates/>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose>
                <!--<xsl:text> </xsl:text>-->
            </xsl:for-each>
        </div>
    </xsl:template>
    
    
    <!--  ============== SEE-UNDER ==============  -->
    <!--    <xsl:template match="cbml:see-under">
        <xsl:param name="level"/>
        <div class="see-under">
            <xsl:attribute name="class" select="concat('see-under', string($level) )"/>
             <xsl:choose>
                <xsl:when test="ends-with(preceding-sibling::cbml:index-entry, '.')"><span class="italic"> See also </span></xsl:when>
                <xsl:otherwise><span class="italic"> see also </span></xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
            <xsl:for-each select="following-sibling::cbml:see-under">
                <xsl:text>; </xsl:text>
                <xsl:apply-templates/>
            </xsl:for-each>
        </div>
    </xsl:template>-->

    <xsl:template
        match="cbml:see-also[preceding-sibling::*[1][self::cbml:see-also or self::cbml:see-under]  and not(parent::cbml:dict-p or parent::cbml:dictionary-entry)] |
        cbml:see-under[preceding-sibling::*[1][self::cbml:see-also or self::cbml:see-under]] |
        cbml:see[local-name()=local-name(preceding-sibling::*[1])  and not(parent::cbml:dict-p or parent::cbml:dictionary-entry)]"
        priority="10"/>


    
    <!--  ============== SEE =================  -->

    <xsl:template match="cbml:see[not(parent::cbml:dictionary-entry)]">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <span class="see">
            <xsl:choose>
                <xsl:when test="ends-with(preceding-sibling::cbml:index-entry, '.')"><span class="italic"> See </span></xsl:when>
                <xsl:otherwise>
                    <span class="italic"> see </span>
                </xsl:otherwise>
            </xsl:choose>
            <a>
                <xsl:attribute name="href" select="cup:seeLink(. ,$link-att)"/>
                <xsl:apply-templates/>
            </a> 
            <xsl:for-each select="following-sibling::cbml:see">
                <xsl:variable name="link-att2" select="@*[local-name()='href']"/>
                <xsl:text>; </xsl:text>
                <a>
                    <xsl:attribute name="href" select="cup:seeLink(. ,$link-att2)"/>
                    <xsl:apply-templates/>
                </a>
                <!--<xsl:text> </xsl:text>-->
            </xsl:for-each>
        </span>
    </xsl:template>
    
    <!--  add class nounder  -->
    <xsl:template match="cbml:link" mode="class">
        <xsl:attribute name="class" select="'nounder'"/>
    </xsl:template>


<!--    ext-link to link to url if given-->
<xsl:template match="cbml:ext-link">
    <xsl:text> </xsl:text>
    <xsl:choose>
        <xsl:when test="@web-address">
            <a href="{@web-address}">
               <xsl:apply-templates/> 
            </a>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
