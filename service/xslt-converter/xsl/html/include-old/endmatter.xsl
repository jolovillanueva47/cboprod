<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
<!--adds back link-->
    <xsl:template match="cbml:fn/cbml:p">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <xsl:choose>
            <!--            <xsl:when test="*[contains($block-elements, local-name(.)) and not(following-sibling::node() or preceding-sibling::node())]">
                <xsl:apply-templates select="preceding-sibling::*[1][self::cbml:label]" mode="do-label"/>
                <xsl:apply-templates/>
            </xsl:when>-->
<!--      do not enclose in p when only contains a block      -->
            <xsl:when test="*[contains($block-elements, local-name(.)) and not(self::cbml:citation) and not(following-sibling::node() or preceding-sibling::node())]">
                <xsl:if test="preceding-sibling::*[position()=1 and self::cbml:label]">
                    <span class="label">
                <xsl:choose>
                            <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::cbml:fn/@id   
                                                                and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)]  
                                                                and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text or ancestor::cbml:array or ancestor::cbml:toc)">
                                <a class="nounder" href="{cup:backLink(parent::cbml:fn,parent::cbml:fn/@id)}">
                                    <!--<a  class="nounder" onclick="history.back()">-->
                                    <xsl:apply-templates select="preceding-sibling::cbml:label/node()"/>
                                    <!--</a>-->
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="preceding-sibling::cbml:label/node()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </span>
                    <xsl:text> </xsl:text> 
                </xsl:if>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
        <p>
            <xsl:apply-templates select="." mode="class"/>
            <xsl:apply-templates select="." mode="id"/>
            <xsl:if test="preceding-sibling::*[position()=1 and self::cbml:label]">
                <span class="label">
                    <xsl:choose>
                        <xsl:when test="preceding::cbml:xref[@*[local-name()='href']=current()/parent::cbml:fn/@id   
                                                             and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:array)]  
                                                             and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group or ancestor::cbml:boxed-text or ancestor::cbml:array or ancestor::cbml:toc)">
                            <a class="nounder" href="{cup:backLink(parent::cbml:fn,parent::cbml:fn/@id)}">
                                <!--<a  class="nounder" onclick="history.back()">-->
                                <xsl:apply-templates select="preceding-sibling::cbml:label/node()"/>
                                <!--</a>-->
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="preceding-sibling::cbml:label/node()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </span>
                <xsl:text> </xsl:text> 
            </xsl:if>
            <xsl:apply-templates select="node()[not(@position='float')]"/>
        </p>
        <xsl:apply-templates select="*[@position='float']"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
<!--  adds back link to numbered references  -->
    <xsl:template match="cbml:citation/cbml:label" mode="do-label">
        <xsl:variable name="link-att" select="@*[local-name()='href']"/>
        <span class="label">
            <xsl:choose>
                <xsl:when test="preceding::cbml:xref[$link-att=current()/parent::cbml:citation/@id   and not(ancestor::cbml:table-wrap or ancestor::cbml:table-wrap-group)]">
                    
                    <a class="nounder" href="{cup:backLink(parent::cbml:citation,parent::cbml:citation/@id)}">
                        <xsl:apply-templates/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>

</xsl:stylesheet>
