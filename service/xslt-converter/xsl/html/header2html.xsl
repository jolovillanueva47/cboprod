<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cbml="http://dtd.cambridge.org/2007/CBML" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns:saxon="http://saxon.sf.net/"
    xmlns="http://www.w3.org/1999/xhtml" xmlns:cup="http://contentservices.cambridge.org"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all" version="2.0">

    <!-- Take the header file and create an html chunk for each content item in the header
    -->

    <xsl:import href="cbo2html.xsl"/>

    <!-- <xsl:output name="me" method="xml" indent="yes" saxon:suppress-indentation="part chapter appendix index references backmatter preface dictionary-letter foreword features contributors glossary frontmatter chronology play sub-book document-group notes index-group endmatter-answers endmatter-exercises "/>
-->
    <xsl:output method="html" indent="yes"/>

    <!--  pass in image-path as a parameter  -->
    <xsl:param name="isbn"/>
    <xsl:param name="image-path" select="'../images/'"/>
    <xsl:param name="css-path" select="'css/'"/>
    <xsl:variable name="content-filename" select="concat($isbn, '_CBML.xml')"/>
    <xsl:variable name="content" select="document($content-filename, .)"/>
    <xsl:param name="chunk-type">
        <xsl:choose>
            <xsl:when test="descendant::html[contains(@filename, 'sec1_')]">sec</xsl:when>
            <xsl:otherwise>med-sec</xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <!--    dummy varaibles for templates from epub build-->
    <xsl:variable name="product-file"/>
    <xsl:variable name="product-version" select="5"/>
    <!-- ============ HTML for CBO ================-->

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="book">
        <xsl:apply-templates select="//html"/>
        <xsl:apply-templates select="$content//cbml:book" mode="nav"/>
        <xsl:apply-templates select="$content//cbml:index[1]" mode="ind"/>
    </xsl:template>

    <xsl:template match="html">
        <xsl:variable name="id" select="@rid"/>
        <xsl:result-document method="html" href="{@filename}">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="css/cbo.css" rel="stylesheet" type="text/css"/>
                    <title>
                        <xsl:value-of select="ancestor::book/metadata/main-title"/>
                    </title>
                </head>
                <body>
                    <xsl:choose>
                        <!--                    <xsl:when test="following-sibling::toc/descendant::html">
                        <xsl:apply-templates select="$content//*[@id=$id]" mode="sections"/>
                    </xsl:when>-->
                        <!--                    all html elements to be in toc when content chunked by chapter-->
                        <xsl:when
                            test="ancestor::toc and not(ancestor::content-item[@type='dictionary'])">
                            <xsl:apply-templates select="$content//*[@id=$id]" mode="sections"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="$content//*[@id=$id]" mode="header-panel"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="*" mode="header-panel">
        <div class="chapter-panel">
            <xsl:if test="cbml:title or cbml:label">
                <xsl:apply-templates
                    select="node()[not(preceding-sibling::cbml:title or preceding-sibling::cbml:label)]"
                />
            </xsl:if>
            <xsl:apply-templates select="." mode="heading"/>
            <xsl:apply-templates select="cbml:author-group[not(ancestor::cbml:contributors)]"/>
        </div>
        <div class="info-panel">
            <xsl:apply-templates select="."/>
        </div>
    </xsl:template>

    <xsl:template match="html[@rid='frontmatter']">
        <xsl:result-document method="html" href="{@filename}">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="css/cbo.css" rel="stylesheet" type="text/css"/>
                    <title/>
                </head>
                <body>
                    <div class="chapter-panel">
                        <h2>Preliminary Pages</h2>
                    </div>
                    <div class="info-panel">
                        <xsl:apply-templates select="$content//cbml:prelims"/>
                    </div>

                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="cbml:prelims">
        <div class="prelims">
            <xsl:apply-templates select="node()[following-sibling::cbml:toc]"/>
        </div>
    </xsl:template>

    <xsl:template match="cbml:part">
        <xsl:apply-templates
            select="node()[not(preceding-sibling::cbml:title or preceding-sibling::cbml:label)]"/>
        <xsl:apply-templates select="cbml:author-group"/>
        <xsl:apply-templates select="cbml:alt-title[@type='copyright-line']" mode="do-copyright"/>
        <xsl:apply-templates
            select="node()[not(self::cbml:title or self::cbml:label or self::cbml:author-group or following-sibling::cbml:title or following-sibling::cbml:label
            or self::cbml:chapter or preceding-sibling::cbml:chapter)]"
        />
    </xsl:template>




    <!--    ==================== nav file ======================-->

    <xsl:template match="cbml:book" mode="nav">
        <!--<xsl:result-document method="xml" href="{$isbn}_nav.html" indent="yes" saxon:suppress-indentation="cbml:chapter cbml:part cbml:appendix cbml:references cbml:backmatter">-->
        <xsl:result-document method="html" href="{$isbn}_nav.html" indent="yes">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="css/cbo.css" rel="stylesheet" type="text/css"/>
                    <title>Sidebar</title>
                </head>
                <body>
                    <ul class="sb-unordered-list">
                        <!--<li><span class="bullet-icon"></span><a href="cover.html">Cover</a></li>-->
                        <xsl:apply-templates mode="nav"/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>

    </xsl:template>

    <xsl:template match="cbml:body | cbml:endmatter" mode="nav">
        <xsl:apply-templates mode="nav"/>
    </xsl:template>

    <xsl:template match="cbml:dictionary" mode="nav" priority="10">
        <li>
            <a href="#" class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">The Dictionary</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:prelims" mode="nav">
        <li>
            <a href="#" class="bullet-open"/>
            <a href="{cup:navLink(child::*[1], child::*[1][@id]/@id)}">Frontmatter</a>
            <ul class="level2">
                <xsl:apply-templates mode="nav"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="cbml:prelims/* | cbml:main/* | cbml:endmatter/*" mode="nav">
        <li>
            <xsl:choose>
                <xsl:when
                    test="descendant::cbml:chapter or descendant::cbml:sec[cbml:label or cbml:title] or descendant::cbml:ref-sec[cbml:label or cbml:title]">
                    <a href="#" class="bullet-open"/>
                </xsl:when>
                <xsl:otherwise>
                    <span class="bullet-icon"/>
                </xsl:otherwise>
            </xsl:choose>
            <a href="{cup:navLink(., @id)}">
                <xsl:apply-templates select="cbml:label |cbml:title | cbml:subtitle" mode="nav"/>
            </a>
            <xsl:if
                test="cbml:chapter or descendant::cbml:sec[cbml:label or cbml:title] or descendant::cbml:ref-sec[cbml:label or cbml:title]">
                <ul class="level2">
                    <xsl:apply-templates
                        select="cbml:chapter | cbml:sec[cbml:label or cbml:title] |cbml:body/cbml:sec[cbml:label or cbml:title] | cbml:back/cbml:ref-sec[cbml:label or cbml:title]"
                        mode="nav"/>
                </ul>
            </xsl:if>
        </li>
    </xsl:template>


    <xsl:template match="cbml:chapter" mode="nav" priority="10">
        <li>
            <xsl:choose>
                <xsl:when
                    test="descendant::cbml:sec[cbml:label or cbml:title] or descendant::cbml:ref-sec[cbml:label or cbml:title]">
                    <a href="#" class="bullet-open"/>
                </xsl:when>
                <xsl:otherwise>
                    <span class="bullet-icon"/>
                </xsl:otherwise>
            </xsl:choose>
            <a href="{cup:navLink(., @id)}">
                <xsl:apply-templates select="cbml:label |cbml:title | cbml:subtitle" mode="nav"/>
            </a>
            <xsl:choose>
                <xsl:when
                    test="$chunk-type='med-sec' and count(cbml:body/cbml:sec[cbml:label or cbml:title]) &gt; 1">
                    <ul class="level2">
                        <xsl:apply-templates
                            select="cbml:body/cbml:sec[preceding-sibling::cbml:sec and cbml:label or cbml:title] | cbml:back/cbml:ref-sec"
                            mode="nav"/>
                    </ul>
                </xsl:when>
                <xsl:when test="descendant::cbml:sec[cbml:label or cbml:title]">
                    <ul class="level2">
                        <xsl:apply-templates
                            select="cbml:body/cbml:sec[cbml:label or cbml:title] | cbml:back/cbml:ref-sec[cbml:label or cbml:title]"
                            mode="nav"/>
                    </ul>
                </xsl:when>
            </xsl:choose>
        </li>
    </xsl:template>

    <xsl:template match="cbml:title-info" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Title page</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:imprints" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Imprints page</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:half-title" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Half title page</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:dedication" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Dedication</a>
        </li>
    </xsl:template>


    <xsl:template match="cbml:series" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Series page</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:frontispiece" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Frontispiece</a>
        </li>
    </xsl:template>


    <xsl:template match="cbml:frontmatter[not(cbml:title)]" mode="nav" priority="10">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">Front matter</a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:part/cbml:chapter | cbml:sec | cbml:ref-sec" mode="nav">
        <li>
            <xsl:choose>
                <xsl:when test="descendant::cbml:sec[cbml:label or cbml:title]">
                    <a href="#" class="bullet-open"/>
                </xsl:when>
                <xsl:otherwise>
                    <span class="bullet-icon"/>
                </xsl:otherwise>
            </xsl:choose>
            <a href="{cup:navLink(., @id)}">
                <xsl:apply-templates select="cbml:label |cbml:title | cbml:subtitle" mode="nav"/>
            </a>
            <xsl:if
                test="cbml:sec[cbml:label or cbml:title] or cbml:body/cbml:sec[cbml:label or cbml:title] or cbml:p/cbml:exercise[cbml:label or cbml:title]">
                <xsl:variable name="num">
                    <xsl:value-of
                        select="count(ancestor::cbml:sec) + count(ancestor::cbml:chapter) + count(ancestor::cbml:part)"
                    />
                </xsl:variable>
                <ul class="level{$num}">
                    <xsl:apply-templates
                        select="cbml:sec[cbml:label or cbml:title] | cbml:body/cbml:sec[cbml:label or cbml:title]  | cbml:back/cbml:ref-sec | cbml:p/cbml:exercise[cbml:label or cbml:title]"
                        mode="nav"/>
                </ul>
            </xsl:if>
        </li>
    </xsl:template>

    <xsl:template match="cbml:exercise" mode="nav">
        <li>
            <span class="bullet-icon"/>
            <a href="{cup:navLink(., @id)}">
                <xsl:apply-templates select="cbml:label |cbml:title | cbml:subtitle" mode="nav"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="cbml:label |cbml:title | cbml:subtitle" mode="nav">
        <xsl:apply-templates select="node()[not(self::cbml:target or self::cbml:xref)]"/>
        <xsl:text> </xsl:text>
    </xsl:template>

    <!--================= extra index file ======================-->

    <xsl:template match="cbml:index-list" mode="ind">
        <xsl:variable name="letter" select="upper-case(substring(cbml:index-item[1], 1, 1))"/>
        <xsl:result-document method="html" href="{$isbn}_index_{$letter}.html">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    <link href="css/cbo.css" rel="stylesheet" type="text/css"/>
                    <title/>
                </head>
                <body>
                    <ul class="sb-unordered-list">
                        <xsl:apply-templates select="cbml:index-item" mode="ind"/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>


    <xsl:template match="cbml:index-item" mode="ind">
        <li>
            <xsl:copy-of select="cbml:index-entry/@id"/>
            <xsl:choose>
                <xsl:when test="cbml:index-item">
                    <a href="#" class="bullet-open"/>
                </xsl:when>
                <xsl:otherwise>
                    <span class="bullet-icon"/>
                    <!--<xsl:attribute name="class" select="'bullet-icon'"></xsl:attribute>-->
                </xsl:otherwise>
            </xsl:choose>
            <div>
                <xsl:apply-templates select="*[not(self::cbml:index-item)]" mode="ind"/>
            </div>
            <xsl:if test="cbml:index-item">
                <ul>
                    <xsl:variable name="num" select="count(ancestor::cbml:index-item) + 2"/>
                    <xsl:attribute name="class" select="concat('level',$num)"/>
                    <xsl:apply-templates select="cbml:index-item" mode="ind"/>
                </ul>
            </xsl:if>
        </li>

    </xsl:template>

    <xsl:template match="cbml:index-entry" mode="ind">
        <xsl:apply-templates/>
        <xsl:text> </xsl:text>
    </xsl:template>

    <xsl:template match="cbml:link" mode="ind">
        <span>
            <a href="{cup:navLink(., @start)}">
                <xsl:apply-templates/>
            </a>
        </span>
        <xsl:if test="following-sibling::cbml:link">, </xsl:if>
    </xsl:template>

    <!--    <xsl:template match="cbml:see | cbml:see-also" mode="ind">
        <xsl:text> </xsl:text>
        <xsl:value-of select="local-name()"/>
        <xsl:text> </xsl:text>
        <a href="{cup:Link(., @href)}"><xsl:apply-templates/></a>
    </xsl:template>-->

    <xsl:template match="cbml:see-also | cbml:see" mode="ind">
        <xsl:variable name="uc-name">
            <xsl:choose>
                <xsl:when test="self::cbml:see-also">See also </xsl:when>
                <xsl:otherwise>See </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="lc-name">
            <xsl:choose>
                <xsl:when test="self::cbml:see-also">see also </xsl:when>
                <xsl:otherwise>see </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- <xsl:param name="level"/>-->
        <span>
            <xsl:attribute name="class" select="local-name()"/>
            <span class="italic">
                <xsl:choose>
                    <xsl:when test="@text-before">
                        <!--override default behaviour with text in attribute-->
                        <xsl:value-of select="concat(' ',@text-before,' ')"/>
                    </xsl:when>
                    <xsl:when test="ends-with(preceding-sibling::cbml:index-entry, '.')">
                        <xsl:value-of select="$uc-name"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$lc-name"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
            <a>
                <xsl:attribute name="href" select="cup:navLink(. ,@href )"/>
                <xsl:apply-templates select="node()[not(self::processing-instruction())]"/>
            </a>
            <xsl:apply-templates select="processing-instruction()"/>

            <xsl:for-each select="following-sibling::cbml:see-also">
                <xsl:variable name="link-att2" select="@*[local-name()='href']"/>
                <xsl:text>; </xsl:text>
                <a>
                    <xsl:attribute name="href" select="cup:navLink(. ,$link-att2 )"/>
                    <xsl:apply-templates select="node()[not(self::processing-instruction())]"/>
                </a>
                <xsl:apply-templates select="processing-instruction()"/>
            </xsl:for-each>
            <xsl:if test="@text-after">
                <span class="italic">
                    <xsl:value-of select="@text-after"/>
                </span>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="cbml:see-under | cbml:ext-link" mode="ind"/>


</xsl:stylesheet>
