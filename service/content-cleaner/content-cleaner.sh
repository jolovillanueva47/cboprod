#!/bin/sh
jar_path="/app/service/content-cleaner/"

NOW=`date '+content_cleaner.log.%Y-%d-%m'`
LOGFILE="/app/service/content-cleaner/log/$NOW.txt"

java -Xms128m -Xmx256m -jar -Dproperty.file=$APP_ENV "$jar_path"cbo-content-cleaner.jar  >> $LOGFILE &
echo "Content Cleaner Finished Processing, check logfile for more info:" $LOGFILE
