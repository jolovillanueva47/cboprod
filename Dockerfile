FROM registry.gitlab.service.adnc.cambridge.org/adnc/base-cboprod:openjdk

ARG ENV=uat

#set JAVA_HOME,JBOSS_HOME, ANT_HOME and PATH 
ENV JAVA_HOME /usr/lib/jvm/jre-1.6.0-openjdk.x86_64/
ENV JAVA $JAVA_HOME/bin/java
ENV PATH $JAVA_HOME/bin:$PATH
ENV JBOSS_HOME=/app/jboss-5.1.0.GA
ENV PATH $JBOSS_HOME/bin:$PATH
ENV ANT_HOME /opt/ant
ENV PATH $ANT_HOME/bin:$PATH

#Copy generated application.ear from build
ADD application.ear  /app/jboss-5.1.0.GA/server/production/deploy/application.ear

ADD service /app/service

ENV APP_ENV=$ENV

COPY ds/$APP_ENV/production-ds.xml /app/jboss-5.1.0.GA/server/production/deploy/
COPY prop/$APP_ENV/$APP_ENV.properties /app/jboss-5.1.0.GA/server/production/deploy/application.ear/META-INF
COPY run/$APP_ENV/run.conf /app/jboss-5.1.0.GA/bin
COPY run_all /usr/local/bin
COPY sh/upload_cron.sh /app/jboss-5.1.0.GA/bin
COPY application.xml /app/jboss-5.1.0.GA/server/production/deploy/application.ear/META-INF
RUN chmod +x /usr/local/bin/run_all

#for cron job
RUN touch /var/log/cron.log
ADD crontab-file /etc/cron.d/cboprod-cron-file
RUN chmod 0644 /etc/cron.d/cboprod-cron-file
RUN crontab /etc/cron.d/cboprod-cron-file

#Set permissions
RUN chmod 744 /app/service/xslt-converter/xsltConvert.sh
RUN chmod 744 /app/service/content-cleaner/content-cleaner.sh
RUN chmod 744 /app/jboss-5.1.0.GA/bin/upload_cron.sh

#Make log directories
RUN mkdir /app/service/xslt-converter/logs
RUN mkdir /app/service/content-cleaner/log

#Make archive
RUN mkdir /app/service/content-cleaner/archive

#Make content.war to be mount for /app/ebooks/content
RUN mkdir /app/jboss-5.1.0.GA/server/production/deploy/application.ear/content.war

#expose ports
EXPOSE 80
EXPOSE 443
EXPOSE 8080
EXPOSE 8009

CMD ["run_all"]

